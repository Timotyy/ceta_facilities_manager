VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Begin VB.Form frmGetNewFileDetails 
   Caption         =   "Barcode"
   ClientHeight    =   1605
   ClientLeft      =   7530
   ClientTop       =   8580
   ClientWidth     =   6915
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmGetNewFileDetails.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   1605
   ScaleWidth      =   6915
   Begin VB.OptionButton optDeleteEmptyFolders 
      Caption         =   "Delete Empty Folders"
      Height          =   255
      Left            =   1560
      TabIndex        =   14
      Top             =   3000
      Width           =   2175
   End
   Begin VB.OptionButton optAutoDeleteDiscstores 
      Caption         =   "AutoDelete Discstores"
      Height          =   255
      Left            =   1500
      TabIndex        =   13
      Top             =   2520
      Width           =   2175
   End
   Begin VB.CheckBox chkPreserveFolderStructure 
      Caption         =   "Preserve Folder Structure"
      Height          =   315
      Left            =   120
      TabIndex        =   12
      Top             =   1200
      Visible         =   0   'False
      Width           =   2295
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbRestoreProfile 
      Height          =   315
      Left            =   2340
      TabIndex        =   11
      ToolTipText     =   "The company this tape belongs to"
      Top             =   480
      Width           =   4455
      DataFieldList   =   "Restore_Profile_Name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Restore_ProfileID"
      Columns(0).Name =   "Restore_ProfileID"
      Columns(0).DataField=   "Restore_ProfileID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   6218
      Columns(1).Caption=   "Restore_Profile_Name"
      Columns(1).Name =   "Restore_Profile_Name"
      Columns(1).DataField=   "Restore_Profile_Name"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "Restore_Profile_FileName_Suffix"
      Columns(2).Name =   "Restore_Profile_FileName_Suffix"
      Columns(2).DataField=   "Restore_Profile_FileName_Suffix"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "Restore_Profile_Tag"
      Columns(3).Name =   "Restore_Profile_Tag"
      Columns(3).DataField=   "Restore_Profile_Tag"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "Restore_Profile_File_Extension"
      Columns(4).Name =   "Restore_Profile_File_Extension"
      Columns(4).DataField=   "Restore_Profile_File_Extension"
      Columns(4).FieldLen=   256
      _ExtentX        =   7858
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   12648447
      DataFieldToDisplay=   "Restore_Profile_Name"
   End
   Begin VB.CheckBox chkRestoreWithTranscode 
      Caption         =   "With Transcode"
      Height          =   195
      Left            =   4080
      TabIndex        =   10
      Top             =   1860
      Width           =   1695
   End
   Begin VB.OptionButton optRestoreDiscStores 
      Caption         =   "Restore Discstores"
      Height          =   255
      Left            =   1500
      TabIndex        =   8
      Top             =   2160
      Width           =   2175
   End
   Begin VB.OptionButton optDiscstores 
      Caption         =   "Discstores"
      Height          =   255
      Left            =   1500
      TabIndex        =   7
      Top             =   1860
      Value           =   -1  'True
      Width           =   1155
   End
   Begin VB.CheckBox chkPreserveMasterfile 
      Caption         =   "Preserve Masterfile Status"
      Height          =   315
      Left            =   2820
      TabIndex        =   5
      Top             =   1200
      Width           =   2295
   End
   Begin VB.CommandButton cmbOK 
      Caption         =   "OK"
      Height          =   315
      Left            =   5520
      TabIndex        =   0
      ToolTipText     =   "Accept the offered Barcode"
      Top             =   1200
      Width           =   1275
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo txtBarcode 
      Height          =   315
      Left            =   2340
      TabIndex        =   6
      Top             =   60
      Width           =   4455
      DataFieldList   =   "Column 0"
      MaxDropDownItems=   56
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      HeadLines       =   0
      BackColorOdd    =   12632319
      RowHeight       =   423
      Columns(0).Width=   4233
      Columns(0).Caption=   "barcode"
      Columns(0).Name =   "barcode"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   7858
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   12648447
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.TextBox txtAltLocation 
      BackColor       =   &H00C0FFFF&
      Height          =   315
      Left            =   2340
      TabIndex        =   2
      ToolTipText     =   "Hit return to accept the offered barcode, or type or scan a new one"
      Top             =   480
      Width           =   4455
   End
   Begin VB.Label Label4 
      Caption         =   "Select the Restore Profile"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   540
      Width           =   1995
   End
   Begin VB.Label Label3 
      Caption         =   "If this is left blank, the File operations will use the same folders as the originals"
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   840
      Width           =   6675
   End
   Begin VB.Label Label2 
      Caption         =   "Type the New Folder"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   540
      Width           =   1995
   End
   Begin VB.Label Label1 
      Caption         =   "Select the Destination"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1995
   End
End
Attribute VB_Name = "frmGetNewFileDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmbOK_Click()

If txtAltLocation.Text <> "" Then

    If Left(txtAltLocation.Text, 1) = "\" Or Left(txtAltLocation.Text, 1) = "/" Or Right(txtAltLocation.Text, 1) = "\" Or Right(txtAltLocation.Text, 1) = "/" Then
        MsgBox "Folder cannot start or end with a slash", vbCritical
        Exit Sub
    ElseIf InStr(txtAltLocation.Text, ":") > 0 Then
        MsgBox "Folder cannot have ':' in it", vbCritical
        Exit Sub
    End If
    
End If

txtAltLocation.Text = Trim(txtAltLocation.Text)

Me.Hide
    
End Sub

Private Sub Form_Activate()

If optDiscstores.Value = True Then
    PopulateCombo "discstores", txtBarcode
    txtAltLocation.Enabled = True
    chkPreserveMasterfile.Enabled = True
    Label2.Visible = True
    Label3.Visible = True
    Label4.Visible = False
    cmbRestoreProfile.Visible = False
    txtAltLocation.Visible = True
    chkPreserveMasterfile.Visible = True
ElseIf optAutoDeleteDiscstores = True Then
    PopulateCombo "autodeletediscstores", txtBarcode
    txtAltLocation.Enabled = False
    chkPreserveMasterfile.Enabled = False
    Label2.Visible = False
    Label3.Visible = False
    Label4.Visible = False
    cmbRestoreProfile.Visible = False
ElseIf optDeleteEmptyFolders = True Then
    PopulateCombo "discstores", txtBarcode
    txtAltLocation.Enabled = True
    chkPreserveMasterfile.Enabled = False
    Label2.Visible = True
    Label3.Visible = False
    Label4.Visible = False
    cmbRestoreProfile.Visible = False
    txtAltLocation.Visible = True
    chkPreserveMasterfile.Visible = False
Else
    PopulateCombo "restorediscstores", txtBarcode
    txtAltLocation.Enabled = False
    chkPreserveMasterfile.Enabled = False
    Label2.Visible = False
    Label3.Visible = False
    If chkRestoreWithTranscode.Value <> 0 Then
        Label4.Visible = True
        cmbRestoreProfile.Visible = True
    Else
        Label4.Visible = False
        cmbRestoreProfile.Visible = False
    End If
    txtAltLocation.Visible = False
    chkPreserveMasterfile.Visible = False
End If
MakeLookLikeOffice Me

End Sub

Private Sub Form_Load()

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch1 As ADODB.Recordset
Dim l_strSQL As String

l_strSQL = "SELECT Restore_ProfileID, Restore_Profile_Name, Restore_Profile_FileName_Suffix, Restore_Profile_Tag FROM Restore_Profiles ORDER BY fd_orderby, Restore_Profile_Name;"

Set l_conSearch = New ADODB.Connection
Set l_rstSearch1 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch1
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch1.ActiveConnection = Nothing

Set cmbRestoreProfile.DataSourceList = l_rstSearch1


End Sub

Private Sub txtBarcode_GotFocus()
txtBarcode.SelStart = 0
txtBarcode.SelLength = Len(txtBarcode.Text)
End Sub

Private Sub txtBarcode_KeyPress(KeyAscii As Integer)

If KeyAscii = vbKeyEscape Then
    txtBarcode.Text = ""
    KeyAscii = 0
    Me.Hide
ElseIf KeyAscii = vbKeyReturn Then
    KeyAscii = 0
    Me.Hide
End If

End Sub
