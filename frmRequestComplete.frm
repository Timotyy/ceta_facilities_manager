VERSION 5.00
Begin VB.Form frmRequestComplete 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Complete User Request"
   ClientHeight    =   6045
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   8925
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6045
   ScaleWidth      =   8925
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtAdminComment 
      BackColor       =   &H00FFC0C0&
      Height          =   855
      Left            =   1020
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Top             =   2580
      Width           =   5835
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save Changes && Close"
      Height          =   315
      Left            =   6960
      TabIndex        =   19
      Top             =   3120
      Width           =   1875
   End
   Begin VB.TextBox txtUserComment 
      BackColor       =   &H00C0E0FF&
      Height          =   1515
      Left            =   1020
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   3
      Top             =   960
      Width           =   5835
   End
   Begin VB.ComboBox cmbSeverity 
      BackColor       =   &H00C0FFC0&
      Height          =   315
      Left            =   4080
      TabIndex        =   1
      Top             =   120
      Width           =   2775
   End
   Begin VB.ComboBox cmbArea 
      BackColor       =   &H00C0E0FF&
      Height          =   315
      Left            =   1020
      TabIndex        =   2
      Top             =   540
      Width           =   5835
   End
   Begin VB.ComboBox cmbType 
      BackColor       =   &H00C0FFC0&
      Height          =   315
      Left            =   1020
      TabIndex        =   0
      Top             =   120
      Width           =   2175
   End
   Begin VB.CommandButton cmdNotApplicable 
      Caption         =   "N/A"
      Height          =   315
      Left            =   3060
      TabIndex        =   6
      Top             =   3660
      Width           =   795
   End
   Begin VB.CommandButton cmdUnable 
      Caption         =   "Unable To Reproduce"
      Height          =   315
      Left            =   6960
      TabIndex        =   10
      Top             =   4980
      Width           =   1875
   End
   Begin VB.CommandButton cmdIncomplete 
      Caption         =   "Mark As Incomplete"
      Height          =   315
      Left            =   6960
      TabIndex        =   9
      Top             =   4560
      Width           =   1875
   End
   Begin VB.CommandButton cmdVoid 
      Caption         =   "Mark As Void"
      Height          =   315
      Left            =   6960
      TabIndex        =   8
      Top             =   4140
      Width           =   1875
   End
   Begin VB.TextBox txtComment 
      Height          =   1365
      Left            =   1020
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   7
      Top             =   4080
      Width           =   5835
   End
   Begin VB.TextBox txtvVersion 
      Height          =   285
      Left            =   1020
      TabIndex        =   5
      Top             =   3660
      Width           =   1935
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   315
      Left            =   7620
      TabIndex        =   12
      Top             =   5640
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Complete"
      Height          =   315
      Left            =   6300
      TabIndex        =   11
      Top             =   5640
      Width           =   1215
   End
   Begin VB.Label lblUserRequestID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   7260
      TabIndex        =   25
      Top             =   1680
      Width           =   1215
   End
   Begin VB.Label lblCreatedOn 
      ForeColor       =   &H00808080&
      Height          =   195
      Left            =   6960
      TabIndex        =   24
      Top             =   1020
      Width           =   1860
   End
   Begin VB.Label lblCreatedBy 
      ForeColor       =   &H00808080&
      Height          =   195
      Left            =   6960
      TabIndex        =   23
      Top             =   420
      Width           =   1860
   End
   Begin VB.Label lblCaption 
      Caption         =   "Created On"
      ForeColor       =   &H00808080&
      Height          =   195
      Index           =   6
      Left            =   6960
      TabIndex        =   22
      Top             =   720
      Width           =   1860
   End
   Begin VB.Label lblCaption 
      Caption         =   "Created By"
      ForeColor       =   &H00808080&
      Height          =   195
      Index           =   5
      Left            =   6960
      TabIndex        =   21
      Top             =   120
      Width           =   1860
   End
   Begin VB.Label lblCaption 
      Caption         =   "Admin Comment"
      Height          =   495
      Index           =   4
      Left            =   120
      TabIndex        =   20
      Top             =   2580
      Width           =   795
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000010&
      Index           =   1
      X1              =   60
      X2              =   8820
      Y1              =   5520
      Y2              =   5520
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000010&
      Index           =   0
      X1              =   0
      X2              =   8820
      Y1              =   3540
      Y2              =   3540
   End
   Begin VB.Label lblCaption 
      Caption         =   "User Comment"
      Height          =   615
      Index           =   3
      Left            =   120
      TabIndex        =   18
      Top             =   960
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Severity"
      Height          =   315
      Index           =   2
      Left            =   3300
      TabIndex        =   17
      Top             =   120
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Area"
      Height          =   315
      Index           =   1
      Left            =   120
      TabIndex        =   16
      Top             =   540
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Type"
      Height          =   315
      Index           =   0
      Left            =   120
      TabIndex        =   15
      Top             =   120
      Width           =   795
   End
   Begin VB.Label Label1 
      Caption         =   "Completed Comment"
      Height          =   435
      Index           =   1
      Left            =   60
      TabIndex        =   14
      Top             =   4140
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Version Completed"
      Height          =   435
      Index           =   0
      Left            =   60
      TabIndex        =   13
      Top             =   3660
      Width           =   855
   End
End
Attribute VB_Name = "frmRequestComplete"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub cmdCancel_Click()
Me.Tag = "CANCELLED"
Me.Hide
End Sub

Private Sub cmdIncomplete_Click()
Me.Tag = "INCOMPLETE"
Me.Hide
End Sub

Private Sub cmdNotApplicable_Click()
txtvVersion.Text = "N/A"
txtComment.SetFocus
End Sub

Private Sub cmdOK_Click()
Me.Hide
End Sub

Private Sub cmdSave_Click()

    Screen.MousePointer = vbDefault
    'save changes to the original comment
    Dim l_strSQL As String
    
    l_strSQL = "UPDATE usercomment SET comment = '" & QuoteSanitise(txtUserComment.Text) & "', area = '" & QuoteSanitise(cmbArea.Text) & "', severity = '" & QuoteSanitise(cmbSeverity.Text) & "', commenttype = '" & QuoteSanitise(cmbType.Text) & "', admincomment = '" & QuoteSanitise(txtAdminComment.Text) & "' "
    l_strSQL = l_strSQL & " WHERE usercommentID = '" & Val(lblUserRequestID.Caption) & "';"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError

    Screen.MousePointer = vbDefault
    
    Me.Tag = "SAVED"
    Me.Hide

End Sub

Private Sub cmdUnable_Click()
Me.Tag = "UNABLE"
Me.Hide
End Sub

Private Sub cmdVoid_Click()
Me.Tag = "VOID"
Me.Hide
End Sub

Private Sub Form_Load()
CenterForm Me
With cmbType
    .AddItem "Bug"
    .AddItem "Enhancement"
    .AddItem "Question"
    .AddItem "New Function"
End With

PopulateCombo "programareas", cmbArea

With cmbSeverity
    .AddItem "1 - Severe"
    .AddItem "2 - High"
    .AddItem "3 - Moderate"
    .AddItem "4 - Low"
    .AddItem "5 - Very Low"
End With
End Sub

