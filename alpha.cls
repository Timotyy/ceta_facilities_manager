VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "chrisalphablend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' ---------------------------------------------------------------------------
' Filename: CAlphaBlend.cls
' Author:   Laurent Michalkovic <laurent_m@hotmail.com>
' Requires:
'
' A class to provide alpha blending on Windows 2000 and up.
' ---------------------------------------------------------------------------
'
'// Copyright � 2002 Laurent Michalkovic
'// All rights reserved.
'//
'// This file is provided "as is" with no expressed or implied warranty.
'// The author accepts no liability for any damage/loss of business that
'// this product may cause.
'//
'/////////////////////////////////////////////////////////////////////////////

' PRIVATE DATA
' ------------
Private m_hWnd As Long              '
Private m_nOpacity As Byte          ' The level of transparency (0 - 255)
Private m_bTransparent As Boolean   '

' WINDOWS APIs
' ------------
Private Declare Function GetWindowLong Lib "user32" Alias _
  "GetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long) As Long
Private Declare Function SetWindowLong Lib "user32" Alias _
  "SetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long, _
  ByVal dwNewLong As Long) As Long
Private Declare Function SetLayeredWindowAttributes Lib "user32" _
  (ByVal hWnd As Long, ByVal crey As Byte, ByVal bAlpha As Byte, _
  ByVal dwFlags As Long) As Long

Private Declare Function GetWindowRect Lib "user32" (ByVal hWnd As Long, lpRect As RECT) As Long
Private Declare Function InvalidateRect Lib "user32" (ByVal hWnd As Long, lpRect As RECT, ByVal bErase As Long) As Long

Private Const GWL_EXSTYLE = (-20)
Private Const WS_EX_LAYERED = &H80000
Private Const WS_EX_TRANSPARENT = &H20&
Private Const LWA_ALPHA = &H2&

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type


Public Property Let hWnd(Wnd As Long)
    m_hWnd = Wnd
End Property

Public Property Let Opacity(n As Byte)
    m_nOpacity = n
End Property

Public Property Let Transparent(b As Boolean)
    m_bTransparent = b

    Dim lOldStyle As Long

    If (m_bTransparent) Then
        ' make it transparent
        lOldStyle = GetWindowLong(m_hWnd, GWL_EXSTYLE)
        SetWindowLong m_hWnd, GWL_EXSTYLE, lOldStyle Or WS_EX_LAYERED
        SetLayeredWindowAttributes m_hWnd, 0, m_nOpacity, LWA_ALPHA
    Else
        lOldStyle = GetWindowLong(m_hWnd, GWL_EXSTYLE)
        SetWindowLong m_hWnd, GWL_EXSTYLE, lOldStyle Xor WS_EX_LAYERED
        Dim r As RECT
        Call GetWindowRect(m_hWnd, r)
        Call InvalidateRect(m_hWnd, r, 1)
    End If
End Property

Private Sub Class_Initialize()
    m_hWnd = 0
    m_nOpacity = 0
    m_bTransparent = False
End Sub





