VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmViewEditor 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "View Editor"
   ClientHeight    =   9090
   ClientLeft      =   2880
   ClientTop       =   3885
   ClientWidth     =   10920
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9090
   ScaleWidth      =   10920
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdAddAll 
      Caption         =   "Add All >>"
      Height          =   315
      Left            =   120
      TabIndex        =   17
      ToolTipText     =   "Add resource"
      Top             =   8700
      Width           =   1455
   End
   Begin VB.CommandButton cmdReSaveOrder 
      Caption         =   "Re-Save Order"
      Height          =   315
      Left            =   7200
      TabIndex        =   16
      ToolTipText     =   "Delete selected view"
      Top             =   960
      Width           =   1455
   End
   Begin MSAdodcLib.Adodc adoResourceView 
      Height          =   330
      Left            =   1500
      Top             =   5520
      Visible         =   0   'False
      Width           =   2985
      _ExtentX        =   5265
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoResourceView"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   9660
      TabIndex        =   11
      ToolTipText     =   "Close form"
      Top             =   8700
      Width           =   1155
   End
   Begin VB.CommandButton cmdMoveDown 
      Caption         =   "Move Down"
      Height          =   315
      Left            =   9660
      TabIndex        =   6
      ToolTipText     =   "Move down one line"
      Top             =   960
      Width           =   1155
   End
   Begin VB.CommandButton cmdMoveUp 
      Caption         =   "Move Up"
      Height          =   315
      Left            =   9660
      TabIndex        =   5
      ToolTipText     =   "Move up one line"
      Top             =   540
      Width           =   1155
   End
   Begin VB.CommandButton cmdRemoveResourceFromView 
      Caption         =   "<< Remove"
      Height          =   315
      Left            =   5040
      TabIndex        =   10
      ToolTipText     =   "Remove resource"
      Top             =   8700
      Width           =   1155
   End
   Begin VB.CommandButton cmdAddResourceToView 
      Caption         =   "Add >>"
      Height          =   315
      Left            =   3780
      TabIndex        =   9
      ToolTipText     =   "Add resource"
      Top             =   8700
      Width           =   1155
   End
   Begin VB.CommandButton cmdDeleteView 
      Caption         =   "Delete View"
      Height          =   315
      Left            =   5700
      TabIndex        =   4
      ToolTipText     =   "Delete selected view"
      Top             =   960
      Width           =   1155
   End
   Begin VB.CommandButton cmdNewView 
      Caption         =   "New View"
      Height          =   315
      Left            =   4440
      TabIndex        =   3
      ToolTipText     =   "Create new view"
      Top             =   960
      Width           =   1155
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdResources 
      Height          =   6795
      Left            =   120
      TabIndex        =   7
      Top             =   1800
      Width           =   4815
      _Version        =   196617
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "conflict"
      stylesets(0).ForeColor=   0
      stylesets(0).BackColor=   255
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmViewEditor.frx":0000
      BeveColorScheme =   1
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   1
      BackColorOdd    =   12648384
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "resourceID"
      Columns(0).Name =   "resourceID"
      Columns(0).DataField=   "resourceID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   4048
      Columns(1).Caption=   "Resource Name"
      Columns(1).Name =   "name"
      Columns(1).DataField=   "name"
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   3200
      Columns(2).Caption=   "serialnumber"
      Columns(2).Name =   "serialnumber"
      Columns(2).DataField=   "serialnumber"
      Columns(2).FieldLen=   256
      _ExtentX        =   8493
      _ExtentY        =   11986
      _StockProps     =   79
      Caption         =   "Resource List"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSubCategory1 
      Height          =   315
      Left            =   1740
      TabIndex        =   1
      ToolTipText     =   "Select a Sub Category"
      Top             =   960
      Width           =   2595
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   5424
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4577
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCategory 
      Height          =   315
      Left            =   1740
      TabIndex        =   0
      ToolTipText     =   "Select Category"
      Top             =   540
      Width           =   2595
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   5424
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4577
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbView 
      Height          =   315
      Left            =   4440
      TabIndex        =   2
      ToolTipText     =   "Select View by Name"
      Top             =   540
      Width           =   4215
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   5424
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   7435
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   12648384
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdResourcesInView 
      Bindings        =   "frmViewEditor.frx":001C
      Height          =   7215
      Left            =   5100
      TabIndex        =   8
      Top             =   1380
      Width           =   5775
      _Version        =   196617
      ColumnHeaders   =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   1
      BackColorOdd    =   12632319
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   5
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "resourceviewID"
      Columns(0).Name =   "resourceviewID"
      Columns(0).DataField=   "resourceviewID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "resourceID"
      Columns(1).Name =   "resourceID"
      Columns(1).DataField=   "resourceID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   4048
      Columns(2).Caption=   "resourcename"
      Columns(2).Name =   "resourcename"
      Columns(2).DataField=   "resourcename"
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   1773
      Columns(3).Caption=   "fd_orderby"
      Columns(3).Name =   "fd_orderby"
      Columns(3).DataField=   "fd_orderby"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "serialnumber"
      Columns(4).Name =   "serialnumber"
      Columns(4).DataField=   "serialnumber"
      Columns(4).FieldLen=   256
      _ExtentX        =   10186
      _ExtentY        =   12726
      _StockProps     =   79
      Caption         =   "Resources In View"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSubCategory2 
      Height          =   315
      Left            =   1740
      TabIndex        =   18
      ToolTipText     =   "Select a Sub Category"
      Top             =   1380
      Width           =   2595
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   5424
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4577
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.Label lblCaption 
      Caption         =   "Sub Category 2"
      Height          =   315
      Index           =   3
      Left            =   120
      TabIndex        =   19
      Top             =   1380
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Please select from the boxes below to filter the list of resources you can add to a view"
      Height          =   375
      Index           =   2
      Left            =   120
      TabIndex        =   15
      Top             =   60
      Width           =   4155
   End
   Begin VB.Label lblCaption 
      Caption         =   "View Name"
      Height          =   255
      Index           =   1
      Left            =   4440
      TabIndex        =   14
      Top             =   180
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Category"
      Height          =   315
      Index           =   4
      Left            =   120
      TabIndex        =   13
      Top             =   540
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Sub Category"
      Height          =   315
      Index           =   0
      Left            =   120
      TabIndex        =   12
      Top             =   960
      Width           =   1455
   End
End
Attribute VB_Name = "frmViewEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmbCategory_Click()
Dim l_strCategory As String

l_strCategory = "subcategory" & LCase(cmbCategory.Text)

cmbSubCategory1.RemoveAll
cmbSubCategory1.Text = ""

PopulateCombo l_strCategory, cmbSubCategory1

PopulateResourceGrid
End Sub

Private Sub cmbSubCategory1_Click()
Dim l_strCategory As String

l_strCategory = "subcat2" & LCase(cmbSubCategory1.Text)

cmbSubCategory2.RemoveAll
cmbSubCategory2.Text = ""

PopulateCombo l_strCategory, cmbSubCategory2

PopulateResourceGrid
End Sub

Private Sub cmbSubCategory2_Click()
PopulateResourceGrid
End Sub

Private Sub cmbView_Click()

Dim l_strSQL As String
l_strSQL = "SELECT resourceviewID, resourceview.resourceID, fd_orderby, resource.name as resourcename, resource.serialnumber FROM resourceview LEFT JOIN resource ON resourceview.resourceID = resource.resourceID WHERE viewID = '" & GetViewID(cmbView.Text) & "' ORDER BY fd_orderby"

adoResourceView.ConnectionString = g_strConnection
adoResourceView.RecordSource = l_strSQL
adoResourceView.Refresh


End Sub


Private Sub cmdAddAll_Click()

If cmbView.Text = "" Then
    MsgBox "Please select a view before adding resources", vbExclamation
    Exit Sub
End If

Dim l_strSQL As String
l_strSQL = "SELECT resourceID, name, serialnumber FROM resource WHERE subcategory1 = '" & QuoteSanitise(cmbSubCategory1.Text) & "' AND category = '" & QuoteSanitise(cmbCategory.Text) & "' "

If cmbSubCategory2.Text <> "" Then
    l_strSQL = l_strSQL & " AND (subcategory2 LIKE '%" & QuoteSanitise(cmbSubCategory2.Text) & "%') "
End If

l_strSQL = l_strSQL & " AND (fd_status IS NULL OR (fd_status <> 'DELETED' AND fd_status <> 'SOLD'))"

l_strSQL = l_strSQL & " ORDER BY name"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

Dim l_lngViewID As Long
l_lngViewID = GetViewID(cmbView.Text)

If Not l_rstSearch.EOF Then l_rstSearch.MoveFirst

Do While Not l_rstSearch.EOF

    AddResourceToView l_rstSearch("resourceID"), l_lngViewID

    l_rstSearch.MoveNext
Loop

l_rstSearch.Close
Set l_rstSearch = Nothing

l_conSearch.Close
Set l_conSearch = Nothing



adoResourceView.Refresh

End Sub

Private Sub cmdAddResourceToView_Click()

If cmbView.Text = "" Then
    MsgBox "Please select a view before adding resources", vbExclamation
    Exit Sub
End If

AddResourceToView grdResources.Columns("resourceID").Text, GetViewID(cmbView.Text)

adoResourceView.Refresh

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdDeleteView_Click()

Dim l_intQuestion As Integer
l_intQuestion = MsgBox("Are you sure you want to delete this view?", vbYesNo + vbQuestion)

If l_intQuestion = vbYes Then
    
    Dim l_lngViewID As Long
    l_lngViewID = GetViewID(cmbView.Text)
    
    Dim l_strSQL As String
    l_strSQL = "DELETE FROM resourceview WHERE viewID = '" & l_lngViewID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_strSQL = "DELETE FROM xref WHERE xrefID = '" & l_lngViewID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    cmbView.RemoveAll
    PopulateCombo "view", cmbView
    cmbView.Text = ""
    
End If



End Sub

Private Sub cmdMoveDown_Click()

If grdResourcesInView.Row = grdResourcesInView.Rows - 1 Then Exit Sub

Dim l_lngViewID As Long
Dim l_lngOrderby As Long
Dim l_lngResourceViewID  As Long


l_lngViewID = GetViewID(cmbView.Text)
l_lngOrderby = grdResourcesInView.Columns("fd_orderby").Text
l_lngResourceViewID = grdResourcesInView.Columns("resourceviewID").Text

MoveItemDownInView l_lngViewID, l_lngOrderby, l_lngResourceViewID



grdResourcesInView.Row = grdResourcesInView.Row + 1
    
Dim l_lngBookmark As Long
l_lngBookmark = grdResourcesInView.Bookmark

adoResourceView.Refresh

grdResourcesInView.Bookmark = l_lngBookmark

End Sub

Private Sub cmdMoveUp_Click()

If grdResourcesInView.Row = 0 Then Exit Sub

Dim l_lngViewID As Long
Dim l_lngOrderby As Long
Dim l_lngResourceViewID  As Long

l_lngViewID = GetViewID(cmbView.Text)
l_lngOrderby = grdResourcesInView.Columns("fd_orderby").Text
l_lngResourceViewID = grdResourcesInView.Columns("resourceviewID").Text

MoveItemUpInView l_lngViewID, l_lngOrderby, l_lngResourceViewID

grdResourcesInView.Row = grdResourcesInView.Row - 1
    
Dim l_lngBookmark As Long
l_lngBookmark = grdResourcesInView.Bookmark

adoResourceView.Refresh

grdResourcesInView.Bookmark = l_lngBookmark


End Sub

Private Sub cmdNewView_Click()

Dim l_strViewName As String
l_strViewName = InputBox("Please enter the new view name")
If l_strViewName = "" Then Exit Sub

Dim l_strSQL As String
l_strSQL = "INSERT INTO xref (category, description, forder) VALUES ('VIEW', '" & l_strViewName & "','99');"

ExecuteSQL l_strSQL, g_strDebugSQLString
CheckForSQLError

cmbView.RemoveAll
PopulateCombo "view", cmbView


End Sub



Private Sub cmdRemoveResourceFromView_Click()

If cmbView.Text = "" Then
    MsgBox "Please select a view before trying to remove resources", vbExclamation
    Exit Sub
End If
If grdResourcesInView.Columns("resourceviewID").Text <> "" Then
    
    RemoveResourceFromView grdResourcesInView.Columns("resourceviewID").Text
    adoResourceView.Refresh
End If
End Sub

Private Sub cmdReSaveOrder_Click()


If grdResourcesInView.Rows = 0 Then Exit Sub

Dim l_lngViewID As Long, l_intCounter As Integer

l_lngViewID = GetViewID(cmbView.Text)

Dim l_strSQL As String
l_strSQL = "SELECT resourceviewID FROM resourceview WHERE viewID = '" & l_lngViewID & "' ORDER BY fd_orderby;"

Dim l_rst As New ADODB.Recordset
Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
l_rst.MoveFirst

l_intCounter = 1

Do While Not l_rst.EOF
    
    l_strSQL = "UPDATE resourceview SET fd_orderby = '" & l_intCounter & "' WHERE resourceviewID = '" & l_rst("resourceviewID") & "';"
    ExecuteSQL l_strSQL, g_strExecuteError

    l_rst.MoveNext
    l_intCounter = l_intCounter + 1
Loop

l_rst.Close
Set l_rst = Nothing


adoResourceView.Refresh

MsgBox "Order now updated", vbExclamation



End Sub

Private Sub Command1_Click()

End Sub

Private Sub Form_Load()

MakeLookLikeOffice Me

PopulateCombo "resourcecategory", cmbCategory

PopulateCombo "view", cmbView

CenterForm Me

End Sub

Sub PopulateResourceGrid()

Dim l_strSQL As String
l_strSQL = "SELECT resourceID, name, serialnumber FROM resource WHERE subcategory1 = '" & QuoteSanitise(cmbSubCategory1.Text) & "' AND category = '" & QuoteSanitise(cmbCategory.Text) & "' "

If cmbSubCategory2.Text <> "" Then
    l_strSQL = l_strSQL & " AND (subcategory2 LIKE '%" & QuoteSanitise(cmbSubCategory2.Text) & "%') "
End If

l_strSQL = l_strSQL & " AND (fd_status IS NULL OR (fd_status <> 'DELETED' AND fd_status <> 'SOLD'))"

l_strSQL = l_strSQL & " ORDER BY name"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set grdResources.DataSource = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub grdResources_DblClick()
cmdAddResourceToView_Click
End Sub

Private Sub grdResourcesInView_DblClick()
cmdRemoveResourceFromView_Click
End Sub

