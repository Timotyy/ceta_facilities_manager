VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPurchaseBudgets 
   Caption         =   "Purchase Order Budget Re-Allocation"
   ClientHeight    =   6945
   ClientLeft      =   1290
   ClientTop       =   1665
   ClientWidth     =   15195
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MDIChild        =   -1  'True
   ScaleHeight     =   6945
   ScaleWidth      =   15195
   WindowState     =   2  'Maximized
   Begin VB.Frame fraDateRanges 
      Caption         =   "Date Ranges"
      Height          =   1455
      Left            =   3360
      TabIndex        =   17
      Top             =   0
      Width           =   4455
      Begin MSComCtl2.DTPicker datDeliveryDate 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   1260
         TabIndex        =   18
         Tag             =   "NOCHECK"
         ToolTipText     =   "The delivery deadline date"
         Top             =   300
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   61865985
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datDeliveryDate 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   2820
         TabIndex        =   19
         Tag             =   "NOCHECK"
         ToolTipText     =   "The delivery deadline date"
         Top             =   300
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   61865985
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datReturnDate 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   2820
         TabIndex        =   20
         Tag             =   "NOCHECK"
         ToolTipText     =   "The return by date"
         Top             =   660
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   61865985
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datReturnDate 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   1260
         TabIndex        =   21
         Tag             =   "NOCHECK"
         ToolTipText     =   "The return by date"
         Top             =   660
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   61865985
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datCreatedDate 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   1260
         TabIndex        =   22
         Tag             =   "NOCHECK"
         ToolTipText     =   "The created date"
         Top             =   1020
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   61865985
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datCreatedDate 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   2820
         TabIndex        =   23
         Tag             =   "NOCHECK"
         ToolTipText     =   "The return by date"
         Top             =   1020
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   61865985
         CurrentDate     =   37870
      End
      Begin VB.Label lblCaption 
         Caption         =   "Return By"
         Height          =   285
         Index           =   11
         Left            =   120
         TabIndex        =   26
         Top             =   660
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Delivery By"
         Height          =   285
         Index           =   6
         Left            =   120
         TabIndex        =   25
         Top             =   300
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Created Date"
         Height          =   285
         Index           =   21
         Left            =   120
         TabIndex        =   24
         Top             =   1020
         Width           =   1035
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Budget Information"
      Height          =   2415
      Left            =   7920
      TabIndex        =   9
      Top             =   0
      Width           =   7155
      Begin VB.OptionButton optShow 
         Height          =   315
         Index           =   1
         Left            =   3180
         TabIndex        =   34
         Top             =   1800
         Value           =   -1  'True
         Width           =   255
      End
      Begin VB.OptionButton optShow 
         Height          =   315
         Index           =   0
         Left            =   3180
         TabIndex        =   33
         Top             =   1080
         Width           =   255
      End
      Begin MSComctlLib.ProgressBar ProgressBar1 
         Height          =   2055
         Left            =   3480
         TabIndex        =   10
         Top             =   240
         Width           =   435
         _ExtentX        =   767
         _ExtentY        =   3625
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   0
         Orientation     =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ProgressBar ProgressBar2 
         Height          =   2055
         Left            =   6600
         TabIndex        =   39
         Top             =   240
         Width           =   435
         _ExtentX        =   767
         _ExtentY        =   3625
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   0
         Orientation     =   1
         Scrolling       =   1
      End
      Begin VB.Label lblInfo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   11
         Left            =   5040
         TabIndex        =   51
         Top             =   2040
         Width           =   1500
      End
      Begin VB.Label lblInfo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   10
         Left            =   5040
         TabIndex        =   50
         Top             =   1740
         Width           =   1500
      End
      Begin VB.Label lblInfo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   9
         Left            =   5040
         TabIndex        =   49
         Top             =   1320
         Width           =   1500
      End
      Begin VB.Label lblInfo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   8
         Left            =   5040
         TabIndex        =   48
         Top             =   1020
         Width           =   1500
      End
      Begin VB.Label lblInfo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   7
         Left            =   5040
         TabIndex        =   47
         Top             =   600
         Width           =   1500
      End
      Begin VB.Label lblCaption 
         Caption         =   "Allocated"
         Height          =   255
         Index           =   13
         Left            =   4020
         TabIndex        =   46
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Ordered"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   12
         Left            =   4020
         TabIndex        =   45
         Top             =   1020
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Balance"
         Height          =   255
         Index           =   10
         Left            =   4020
         TabIndex        =   44
         Top             =   1320
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Invoiced"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   4020
         TabIndex        =   43
         Top             =   1740
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Balance"
         Height          =   255
         Index           =   8
         Left            =   4020
         TabIndex        =   42
         Top             =   2040
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Our Ref"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   4020
         TabIndex        =   41
         Top             =   300
         Width           =   795
      End
      Begin VB.Label lblInfo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   6
         Left            =   5040
         TabIndex        =   40
         Top             =   300
         Width           =   1500
      End
      Begin VB.Label lblInfo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   5
         Left            =   1620
         TabIndex        =   32
         Top             =   2040
         Width           =   1500
      End
      Begin VB.Label lblInfo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   4
         Left            =   1620
         TabIndex        =   31
         Top             =   1740
         Width           =   1500
      End
      Begin VB.Label lblInfo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   3
         Left            =   1620
         TabIndex        =   30
         Top             =   1320
         Width           =   1500
      End
      Begin VB.Label lblInfo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   2
         Left            =   1620
         TabIndex        =   29
         Top             =   1020
         Width           =   1500
      End
      Begin VB.Label lblInfo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   1
         Left            =   1620
         TabIndex        =   28
         Top             =   600
         Width           =   1500
      End
      Begin VB.Label lblInfo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   0
         Left            =   1620
         TabIndex        =   27
         Top             =   300
         Width           =   1500
      End
      Begin VB.Label lblCaption 
         Caption         =   "Balance"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   16
         Top             =   2040
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Invoiced"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   15
         Top             =   1740
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Balance"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   14
         Top             =   1320
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Ordered"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   13
         Top             =   1020
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Budget Amount"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   12
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Budget Code"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   11
         Top             =   300
         Width           =   1215
      End
   End
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "Refresh (F5)"
      Height          =   315
      Left            =   3360
      TabIndex        =   5
      Top             =   1740
      Width           =   1575
   End
   Begin MSAdodcLib.Adodc adoPurchaseDetail 
      Height          =   330
      Left            =   4260
      Top             =   720
      Visible         =   0   'False
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoPurchaseDetail"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Frame fraOrderDetail 
      Caption         =   "Order Detail Rows"
      Height          =   1155
      Left            =   120
      TabIndex        =   0
      Top             =   900
      Width           =   3135
      Begin VB.TextBox txtSupplierCode 
         BackColor       =   &H00C0E0FF&
         DataField       =   "purchasedetail.suppliercode"
         Height          =   315
         Left            =   1200
         TabIndex        =   2
         Tag             =   "%"
         ToolTipText     =   "The title for the job"
         Top             =   720
         Width           =   1815
      End
      Begin VB.TextBox txtFullDescription 
         BackColor       =   &H00C0E0FF&
         DataField       =   "purchasedetail.description"
         Height          =   315
         Left            =   1200
         TabIndex        =   1
         Tag             =   "%"
         ToolTipText     =   "The title for the job"
         Top             =   300
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Part Ref."
         Height          =   315
         Index           =   18
         Left            =   120
         TabIndex        =   4
         Top             =   720
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Item"
         Height          =   315
         Index           =   17
         Left            =   120
         TabIndex        =   3
         Top             =   300
         Width           =   1155
      End
   End
   Begin MSAdodcLib.Adodc adoOurReference 
      Height          =   330
      Left            =   11460
      Top             =   4980
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoOurReference"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnOurReference 
      Bindings        =   "frmPurchaseBudgets.frx":0000
      Height          =   2235
      Left            =   9480
      TabIndex        =   7
      Top             =   3300
      Width           =   5535
      DataFieldList   =   "code"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16051436
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3016
      Columns(0).Caption=   "Our Ref"
      Columns(0).Name =   "code"
      Columns(0).DataField=   "code"
      Columns(0).FieldLen=   256
      Columns(1).Width=   6085
      Columns(1).Caption=   "Description"
      Columns(1).Name =   "description"
      Columns(1).DataField=   "description"
      Columns(1).FieldLen=   256
      _ExtentX        =   9763
      _ExtentY        =   3942
      _StockProps     =   77
      DataFieldToDisplay=   "code"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnCostCodes 
      Bindings        =   "frmPurchaseBudgets.frx":001E
      Height          =   2235
      Left            =   2460
      TabIndex        =   8
      Top             =   3300
      Width           =   5535
      DataFieldList   =   "code"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16051436
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3016
      Columns(0).Caption=   "Budget Code"
      Columns(0).Name =   "code"
      Columns(0).DataField=   "code"
      Columns(0).FieldLen=   256
      Columns(1).Width=   6085
      Columns(1).Caption=   "Description"
      Columns(1).Name =   "description"
      Columns(1).DataField=   "description"
      Columns(1).FieldLen=   256
      _ExtentX        =   9763
      _ExtentY        =   3942
      _StockProps     =   77
      DataFieldToDisplay=   "code"
   End
   Begin MSAdodcLib.Adodc adoCostCodes 
      Height          =   330
      Left            =   3000
      Top             =   6540
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCostCodes"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdOrderDetail 
      Bindings        =   "frmPurchaseBudgets.frx":0039
      Height          =   3615
      Left            =   120
      TabIndex        =   6
      Top             =   2520
      Width           =   17055
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      BackColorOdd    =   12640511
      RowHeight       =   423
      Columns.Count   =   19
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "purchaseheaderID"
      Columns(0).Name =   "purchaseheaderID"
      Columns(0).DataField=   "purchaseheaderID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "resourcescheduleID"
      Columns(1).Name =   "resourcescheduleID"
      Columns(1).DataField=   "resourcescheduleID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   1931
      Columns(2).Caption=   "Auth #"
      Columns(2).Name =   "purchaseauthorisationnumber"
      Columns(2).DataField=   "authorisationnumber"
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   3200
      Columns(3).Caption=   "Supplier"
      Columns(3).Name =   "suppliername"
      Columns(3).DataField=   "companyname"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   1852
      Columns(4).Caption=   "Budget Code"
      Columns(4).Name =   "cetacode"
      Columns(4).DataField=   "cetacode"
      Columns(4).FieldLen=   256
      Columns(4).HasBackColor=   -1  'True
      Columns(4).BackColor=   16633544
      Columns(5).Width=   1799
      Columns(5).Caption=   "Our Ref"
      Columns(5).Name =   "ourreference"
      Columns(5).DataField=   "ourreference"
      Columns(5).FieldLen=   256
      Columns(5).HasBackColor=   -1  'True
      Columns(5).BackColor=   16100815
      Columns(6).Width=   1085
      Columns(6).Caption=   "Qty"
      Columns(6).Name =   "quantity"
      Columns(6).DataField=   "quantity"
      Columns(6).DataType=   3
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "Part Ref"
      Columns(7).Name =   "suppliercode"
      Columns(7).DataField=   "suppliercode"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   3836
      Columns(8).Caption=   "Description"
      Columns(8).Name =   "description"
      Columns(8).DataField=   "description"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "Unit �"
      Columns(9).Name =   "unitcharge"
      Columns(9).Alignment=   1
      Columns(9).DataField=   "unitcharge"
      Columns(9).DataType=   6
      Columns(9).NumberFormat=   "�#.00"
      Columns(9).FieldLen=   256
      Columns(9).Locked=   -1  'True
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "Disc %"
      Columns(10).Name=   "discount"
      Columns(10).DataField=   "discount"
      Columns(10).FieldLen=   256
      Columns(10).Locked=   -1  'True
      Columns(11).Width=   2011
      Columns(11).Caption=   "Total �"
      Columns(11).Name=   "total"
      Columns(11).Alignment=   1
      Columns(11).DataField=   "total"
      Columns(11).DataType=   6
      Columns(11).NumberFormat=   "�#.00"
      Columns(11).FieldLen=   256
      Columns(11).Locked=   -1  'True
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "Received"
      Columns(12).Name=   "quantityreceived"
      Columns(12).DataField=   "quantityreceived"
      Columns(12).FieldLen=   256
      Columns(12).Locked=   -1  'True
      Columns(12).HasBackColor=   -1  'True
      Columns(12).BackColor=   11926999
      Columns(13).Width=   1535
      Columns(13).Caption=   "Invoice #"
      Columns(13).Name=   "supplierinvoicenumber"
      Columns(13).DataField=   "supplierinvoicenumber"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      Columns(13).Locked=   -1  'True
      Columns(13).HasBackColor=   -1  'True
      Columns(13).BackColor=   8421631
      Columns(14).Width=   1905
      Columns(14).Caption=   "Invoice �"
      Columns(14).Name=   "supplierinvoiceamount"
      Columns(14).Alignment=   1
      Columns(14).DataField=   "supplierinvoiceamount"
      Columns(14).DataType=   6
      Columns(14).NumberFormat=   "�#.00"
      Columns(14).FieldLen=   256
      Columns(14).Locked=   -1  'True
      Columns(14).HasBackColor=   -1  'True
      Columns(14).BackColor=   8421631
      Columns(15).Width=   1852
      Columns(15).Caption=   "Invoice Date"
      Columns(15).Name=   "supplierinvoicedate"
      Columns(15).DataField=   "supplierinvoicedate"
      Columns(15).DataType=   7
      Columns(15).FieldLen=   256
      Columns(15).Locked=   -1  'True
      Columns(15).HasBackColor=   -1  'True
      Columns(15).BackColor=   8421631
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "Discrepency Reason"
      Columns(16).Name=   "discrepencereason"
      Columns(16).DataField=   "discrepencyreason"
      Columns(16).FieldLen=   256
      Columns(16).Locked=   -1  'True
      Columns(16).HasBackColor=   -1  'True
      Columns(16).BackColor=   8421631
      Columns(17).Width=   3200
      Columns(17).Visible=   0   'False
      Columns(17).Caption=   "Purchase Detail ID"
      Columns(17).Name=   "purchasedetailID"
      Columns(17).DataField=   "purchasedetailID"
      Columns(17).DataType=   3
      Columns(17).FieldLen=   256
      Columns(18).Width=   3200
      Columns(18).Visible=   0   'False
      Columns(18).Caption=   "Purchase ID"
      Columns(18).Name=   "purchaseID"
      Columns(18).DataField=   "purchaseheaderID"
      Columns(18).FieldLen=   256
      _ExtentX        =   30083
      _ExtentY        =   6376
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbOurReference 
      Bindings        =   "frmPurchaseBudgets.frx":0059
      DataField       =   "purchasedetail.ourreference"
      Height          =   315
      Left            =   1320
      TabIndex        =   35
      Top             =   540
      Width           =   1935
      DataFieldList   =   "code"
      _Version        =   196617
      Cols            =   2
      ColumnHeaders   =   0   'False
      BackColorOdd    =   12640511
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   3413
      _ExtentY        =   556
      _StockProps     =   93
      ForeColor       =   -2147483640
      BackColor       =   12640511
      DataFieldToDisplay=   "code"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo ddnCETACodes 
      Bindings        =   "frmPurchaseBudgets.frx":0077
      DataField       =   "purchasedetail.cetacode"
      Height          =   315
      Left            =   1320
      TabIndex        =   36
      ToolTipText     =   "The Budget code or CETA Code"
      Top             =   120
      Width           =   1935
      DataFieldList   =   "code"
      _Version        =   196617
      BorderStyle     =   0
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   12640511
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   2831
      Columns(0).Caption=   "Code"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "code"
      Columns(0).FieldLen=   256
      Columns(1).Width=   6535
      Columns(1).Caption=   "Description"
      Columns(1).Name =   "telephone"
      Columns(1).DataField=   "description"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "contactID"
      Columns(2).Name =   "contactID"
      Columns(2).DataField=   "contactID"
      Columns(2).FieldLen=   256
      _ExtentX        =   3413
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   12640511
      DataFieldToDisplay=   "code"
   End
   Begin VB.Label lblCaption 
      Caption         =   "Budget Code"
      Height          =   315
      Index           =   25
      Left            =   120
      TabIndex        =   38
      Top             =   120
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Our Ref"
      Height          =   315
      Index           =   29
      Left            =   120
      TabIndex        =   37
      Top             =   540
      Width           =   1155
   End
End
Attribute VB_Name = "frmPurchaseBudgets"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmbOurReference_Click()
LoadOurRefDetails CStr(ddnCETACodes.Text), CStr(cmbOurReference.Text)


End Sub

Private Sub cmbOurReference_GotFocus()

If adoOurReference.RecordSource <> "SELECT description AS code, information AS description FROM xref WHERE category = 'ourreference' AND format = '" & ddnCETACodes.Text & "' ORDER BY code, description;" Then
    adoOurReference.RecordSource = "SELECT description AS code, information AS description FROM xref WHERE category = 'ourreference' AND format = '" & ddnCETACodes.Text & "' ORDER BY code, description;"
    adoOurReference.ConnectionString = g_strConnection
    adoOurReference.Refresh
End If

End Sub

Private Sub cmdRefresh_Click()
LoadBudgetDetails CStr(ddnCETACodes.Text)


End Sub

Private Sub ddnCETACodes_Click()
LoadBudgetDetails ddnCETACodes.Text

End Sub


Sub LoadOurRefDetails(lp_strBudgetCode As String, lp_strOurReference As String)

    If lp_strOurReference = "" Or lp_strBudgetCode = "" Then Exit Sub
    
    lblInfo(6).Caption = lp_strOurReference
    
    Dim l_dblOrderedTotal As Double
    l_dblOrderedTotal = GetDataSQL("SELECT SUM(total) FROM purchasedetail WHERE cetacode = '" & lp_strBudgetCode & "' AND ourreference = '" & lp_strOurReference & "'")
    
    Dim l_dblInvoicedTotal As Double
    l_dblInvoicedTotal = GetDataSQL("SELECT SUM(supplierinvoiceamount) FROM purchasedetail WHERE cetacode = '" & lp_strBudgetCode & "' AND ourreference = '" & lp_strOurReference & "'")
    
    lblInfo(8).Caption = Format(l_dblOrderedTotal, "###,###,###.00")
    
    
    lblInfo(10).Caption = Format(l_dblInvoicedTotal, "###,###,###.00")
    
    

End Sub

Sub LoadBudgetDetails(lp_strBudgetCode As String)
    
    Dim l_dblBudgetTotal As Double
    l_dblBudgetTotal = GetData("budget", "budgettotal", "code", lp_strBudgetCode)
    
    Dim l_dblOrderedTotal As Double
    l_dblOrderedTotal = GetDataSQL("SELECT SUM(total) FROM purchasedetail WHERE cetacode = '" & lp_strBudgetCode & "'")
    
    Dim l_dblInvoicedTotal As Double
    l_dblInvoicedTotal = GetDataSQL("SELECT SUM(supplierinvoiceamount) FROM purchasedetail WHERE cetacode = '" & lp_strBudgetCode & "'")
    
    lblInfo(0).Caption = lp_strBudgetCode
    lblInfo(1).Caption = Format(l_dblBudgetTotal, "###,###,###.00")
    
    lblInfo(2).Caption = Format(l_dblOrderedTotal, "###,###,###.00")
    lblInfo(3).Caption = Format(Val(l_dblBudgetTotal) - Val(l_dblOrderedTotal), "###,###,###.00")
    
    lblInfo(4).Caption = Format(l_dblInvoicedTotal, "###,###,###.00")
    lblInfo(5).Caption = Format(Val(l_dblBudgetTotal) - Val(l_dblInvoicedTotal), "###,###,###.00")
    
    
    If l_dblBudgetTotal <= 0 Then
        l_dblBudgetTotal = 1
        ProgressBar1.Value = 0
    Else
    
        ProgressBar1.Max = l_dblBudgetTotal
        
        If optShow(0).Value = True Then
            ProgressBar1.Value = l_dblOrderedTotal
        Else
            ProgressBar1.Value = l_dblInvoicedTotal
        End If
        
    End If
    
    DoEvents
    
    
        
    Dim l_strSQL As String
    l_strSQL = "SELECT purchaseheader.authorisationnumber, purchaseheader.companyname, purchasedetail.* FROM purchasedetail INNER JOIN purchaseheader ON purchasedetail.purchaseheaderID = purchaseheader.purchaseheaderID WHERE (purchasedetailID > 0) "
    
    
    
    If ddnCETACodes.Text <> "" Then l_strSQL = l_strSQL & " AND (cetacode = '" & ddnCETACodes.Text & "') "
    If cmbOurReference.Text <> "" Then l_strSQL = l_strSQL & " AND (ourreference = '" & cmbOurReference.Text & "') "
    
    If Not IsNull(datDeliveryDate(0).Value) Then
        l_strSQL = l_strSQL & " AND (deliverydate >= '" & FormatSQLDate(Format(datDeliveryDate(0).Value, vbShortDateFormat) & " 00:00") & "')"
    End If
    
    If Not IsNull(datDeliveryDate(1).Value) Then
        l_strSQL = l_strSQL & " AND (deliverydate <= '" & FormatSQLDate(Format(datDeliveryDate(1).Value, vbShortDateFormat) & " 23:59") & "')"
    End If
    
    If Not IsNull(datReturnDate(0).Value) Then
        l_strSQL = l_strSQL & " AND (returndate >= '" & FormatSQLDate(Format(datReturnDate(0).Value, vbShortDateFormat) & " 00:00") & "')"
    End If
    
    If Not IsNull(datReturnDate(1).Value) Then
        l_strSQL = l_strSQL & " AND (returndate <= '" & FormatSQLDate(Format(datReturnDate(1).Value, vbShortDateFormat) & " 23:59") & "')"
    End If
    
    If Not IsNull(datCreatedDate(0).Value) Then
        l_strSQL = l_strSQL & " AND (cdate >= '" & FormatSQLDate(Format(datCreatedDate(0).Value, vbShortDateFormat) & " 00:00") & "')"
    End If
    
    If Not IsNull(datCreatedDate(1).Value) Then
        l_strSQL = l_strSQL & " AND (cdate <= '" & FormatSQLDate(Format(datCreatedDate(1).Value, vbShortDateFormat) & " 23:59") & "')"
    End If
    
    
    adoPurchaseDetail.ConnectionString = g_strConnection
    adoPurchaseDetail.RecordSource = l_strSQL
    adoPurchaseDetail.Refresh
    
End Sub

Private Sub Form_Load()
'pick up all the budget codes from the budget table
l_strSQL = "SELECT code, description FROM budget ORDER BY code;"

adoCostCodes.ConnectionString = g_strConnection
adoCostCodes.RecordSource = l_strSQL
adoCostCodes.Refresh

End Sub

Private Sub Form_Resize()
On Error Resume Next
grdOrderDetail.Height = Me.ScaleHeight - grdOrderDetail.Top - 300
grdOrderDetail.Width = Me.ScaleWidth - 200

End Sub

Private Sub grdOrderDetail_DblClick()

If grdOrderDetail.Row < 1 Then Exit Sub

ShowPurchaseOrder Val(grdOrderDetail.Columns("purchaseID").Text)


End Sub

Private Sub grdOrderDetail_InitColumnProps()
grdOrderDetail.Columns("cetacode").DropDownHwnd = ddnCostCodes.hWnd
grdOrderDetail.Columns("ourreference").DropDownHwnd = ddnOurReference.hWnd


End Sub

Private Sub grdOrderDetail_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

If adoOurReference.RecordSource <> "SELECT description AS code, information AS description FROM xref WHERE category = 'ourreference' AND format = '" & grdOrderDetail.Columns("cetacode").Text & "' ORDER BY code, description;" Then
    adoOurReference.RecordSource = "SELECT description AS code, information AS description FROM xref WHERE category = 'ourreference' AND format = '" & grdOrderDetail.Columns("cetacode").Text & "' ORDER BY code, description;"
    adoOurReference.ConnectionString = g_strConnection
    adoOurReference.Refresh
End If



End Sub

Private Sub optShow_Click(Index As Integer)

LoadBudgetDetails CStr(ddnCETACodes.Text)

End Sub
