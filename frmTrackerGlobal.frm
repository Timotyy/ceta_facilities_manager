VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmTrackerGlobal 
   Caption         =   "Global Tracker"
   ClientHeight    =   15795
   ClientLeft      =   3060
   ClientTop       =   3210
   ClientWidth     =   26010
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   15795
   ScaleWidth      =   26010
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.CheckBox chkHideDemo 
      Alignment       =   1  'Right Justify
      Caption         =   "Hide Demo"
      Height          =   255
      Left            =   180
      TabIndex        =   47
      Tag             =   "NOCLEAR"
      Top             =   480
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.CommandButton cmdUpdate 
      Caption         =   "Update"
      Height          =   435
      Left            =   23280
      TabIndex        =   44
      Top             =   840
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.TextBox txtTotalDurationEstimate 
      Height          =   315
      Left            =   19920
      TabIndex        =   42
      Top             =   480
      Width           =   1275
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Tape Sent Back"
      Height          =   255
      Index           =   4
      Left            =   13200
      TabIndex        =   41
      Tag             =   "NOCLEAR"
      Top             =   540
      Width           =   2355
   End
   Begin VB.CommandButton cmdReport 
      Caption         =   "Report"
      Height          =   435
      Left            =   21420
      TabIndex        =   40
      Top             =   840
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.CommandButton cmdAllFields 
      Caption         =   "All Fields"
      Height          =   375
      Left            =   15840
      TabIndex        =   38
      Top             =   2580
      Width           =   1875
   End
   Begin VB.CommandButton cmdSomeFields 
      Caption         =   "Operational Fields Only"
      Height          =   375
      Left            =   13800
      TabIndex        =   37
      Top             =   2580
      Width           =   1875
   End
   Begin VB.TextBox txtReference 
      Height          =   315
      Left            =   6360
      TabIndex        =   36
      Top             =   1200
      Width           =   3795
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "All Items"
      Height          =   255
      Index           =   3
      Left            =   13200
      TabIndex        =   32
      Tag             =   "NOCLEAR"
      Top             =   1440
      Width           =   2355
   End
   Begin VB.CheckBox chkLockGB 
      Alignment       =   1  'Right Justify
      Caption         =   "Lock GB size"
      Height          =   255
      Left            =   180
      TabIndex        =   31
      Top             =   1320
      Width           =   1815
   End
   Begin VB.CheckBox chkLockDur 
      Alignment       =   1  'Right Justify
      Caption         =   "Lock Duration"
      Height          =   255
      Left            =   180
      TabIndex        =   30
      Top             =   1020
      Width           =   1815
   End
   Begin VB.TextBox txtTotalSize 
      Height          =   315
      Left            =   19920
      TabIndex        =   28
      Top             =   1200
      Width           =   1275
   End
   Begin VB.TextBox txtTotalDuration 
      Height          =   315
      Left            =   19920
      TabIndex        =   26
      Top             =   840
      Width           =   1275
   End
   Begin VB.TextBox txtFormat 
      Height          =   315
      Left            =   6360
      TabIndex        =   23
      Top             =   1560
      Width           =   3795
   End
   Begin VB.TextBox txtBarcode 
      Height          =   315
      Left            =   6360
      TabIndex        =   22
      Top             =   840
      Width           =   3795
   End
   Begin VB.TextBox txtSeries 
      Height          =   315
      Left            =   11400
      TabIndex        =   18
      Top             =   120
      Width           =   1455
   End
   Begin VB.TextBox txtEpisode 
      Height          =   315
      Left            =   11400
      TabIndex        =   15
      Top             =   480
      Width           =   1455
   End
   Begin VB.TextBox txtSubtitle 
      Height          =   315
      Left            =   6360
      TabIndex        =   14
      Top             =   480
      Width           =   3795
   End
   Begin VB.TextBox txtTitle 
      Height          =   315
      Left            =   6360
      TabIndex        =   11
      Top             =   120
      Width           =   3795
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Billed"
      Height          =   255
      Index           =   2
      Left            =   13200
      TabIndex        =   8
      Tag             =   "NOCLEAR"
      Top             =   1140
      Width           =   2355
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Finished"
      Height          =   255
      Index           =   1
      Left            =   13200
      TabIndex        =   7
      Tag             =   "NOCLEAR"
      Top             =   840
      Width           =   2355
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete"
      Height          =   255
      Index           =   0
      Left            =   13200
      TabIndex        =   6
      Tag             =   "NOCLEAR"
      Top             =   240
      Width           =   2355
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   7860
      TabIndex        =   1
      Top             =   14760
      Width           =   17835
      Begin VB.CommandButton cmdUnbillAll 
         Caption         =   "Unmark Billing All"
         Height          =   315
         Left            =   1380
         TabIndex        =   34
         Top             =   0
         Width           =   2235
      End
      Begin VB.CommandButton cmdBillAll 
         Caption         =   "Bill All"
         Height          =   315
         Left            =   3780
         TabIndex        =   33
         Top             =   0
         Width           =   2235
      End
      Begin VB.CommandButton cmdUnbill 
         Caption         =   "Unmark as Billed"
         Height          =   315
         Left            =   6180
         TabIndex        =   21
         Top             =   0
         Visible         =   0   'False
         Width           =   2235
      End
      Begin VB.CommandButton cmdManualBillItem 
         Caption         =   "Manually Mark as Billed"
         Height          =   315
         Left            =   8580
         TabIndex        =   20
         Top             =   0
         Visible         =   0   'False
         Width           =   2235
      End
      Begin VB.CommandButton cmdBillItem 
         Caption         =   "Bill Item"
         Height          =   315
         Left            =   10920
         TabIndex        =   19
         Top             =   0
         Visible         =   0   'False
         Width           =   1755
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   16560
         TabIndex        =   5
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   15300
         TabIndex        =   4
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   14040
         TabIndex        =   3
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   12780
         TabIndex        =   2
         Top             =   0
         Width           =   1215
      End
   End
   Begin MSAdodcLib.Adodc adoItems 
      Height          =   330
      Left            =   180
      Top             =   2880
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdItems 
      Bindings        =   "frmTrackerGlobal.frx":0000
      Height          =   9375
      Left            =   180
      TabIndex        =   0
      Top             =   3240
      Width           =   25590
      ScrollBars      =   3
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   3
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTrackerGlobal.frx":0017
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmTrackerGlobal.frx":0033
      stylesets(2).Name=   "headerfield"
      stylesets(2).ForeColor=   0
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmTrackerGlobal.frx":004F
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      StyleSet        =   "headerfield"
      RowHeight       =   476
      ExtraHeight     =   26
      Columns.Count   =   29
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "tracker_itemID"
      Columns(0).Name =   "tracker_global_itemID"
      Columns(0).DataField=   "tracker_global_itemID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   2990
      Columns(2).Caption=   "Ref"
      Columns(2).Name =   "itemreference"
      Columns(2).DataField=   "itemreference"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3519
      Columns(3).Caption=   "Filename"
      Columns(3).Name =   "itemfilename"
      Columns(3).DataField=   "itemfilename"
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "headerfield"
      Columns(4).Width=   873
      Columns(4).Caption=   "E.Dur"
      Columns(4).Name =   "durationestmate"
      Columns(4).DataField=   "durationestimate"
      Columns(4).FieldLen=   256
      Columns(5).Width=   714
      Columns(5).Caption=   "Dur"
      Columns(5).Name =   "duration"
      Columns(5).DataField=   "duration"
      Columns(5).FieldLen=   256
      Columns(6).Width=   1508
      Columns(6).Caption=   "Length"
      Columns(6).Name =   "timecodeduration"
      Columns(6).DataField=   "timecodeduration"
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "rejected"
      Columns(7).Name =   "rejected"
      Columns(7).DataField=   "rejected"
      Columns(7).FieldLen=   256
      Columns(8).Width=   1773
      Columns(8).Caption=   "status"
      Columns(8).Name =   "status"
      Columns(8).DataField=   "Column 38"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(9).Width=   873
      Columns(9).Caption=   "GB Sent"
      Columns(9).Name =   "gbsent"
      Columns(9).DataField=   "gbsent"
      Columns(9).FieldLen=   256
      Columns(10).Width=   1588
      Columns(10).Caption=   "Stored On"
      Columns(10).Name=   "Stored On"
      Columns(10).FieldLen=   256
      Columns(11).Width=   3519
      Columns(11).Caption=   "Title"
      Columns(11).Name=   "title"
      Columns(11).DataField=   "title"
      Columns(11).FieldLen=   256
      Columns(12).Width=   3519
      Columns(12).Caption=   "Episode Title"
      Columns(12).Name=   "subtitle"
      Columns(12).DataField=   "subtitle"
      Columns(12).FieldLen=   256
      Columns(13).Width=   714
      Columns(13).Caption=   "Eps"
      Columns(13).Name=   "episode"
      Columns(13).DataField=   "episode"
      Columns(13).FieldLen=   256
      Columns(14).Width=   1773
      Columns(14).Caption=   "Barcode"
      Columns(14).Name=   "barcode"
      Columns(14).DataField=   "barcode"
      Columns(14).FieldLen=   256
      Columns(14).Style=   1
      Columns(14).StyleSet=   "headerfield"
      Columns(15).Width=   1588
      Columns(15).Caption=   "Format"
      Columns(15).Name=   "format"
      Columns(15).DataField=   "format"
      Columns(15).FieldLen=   256
      Columns(16).Width=   1931
      Columns(16).Caption=   "Alpha ID"
      Columns(16).Name=   "externalalphaID"
      Columns(16).DataField=   "externalalphaID"
      Columns(16).FieldLen=   256
      Columns(16).StyleSet=   "headerfield"
      Columns(17).Width=   1931
      Columns(17).Caption=   "Orig. Alpha ID"
      Columns(17).Name=   "originalexternalalphaID"
      Columns(17).DataField=   "originalexternalalphaID"
      Columns(17).FieldLen=   256
      Columns(17).StyleSet=   "headerfield"
      Columns(18).Width=   3175
      Columns(18).Caption=   "Legacy Line Standard"
      Columns(18).Name=   "legacylinestandard"
      Columns(18).DataField=   "legacylinestandard"
      Columns(18).FieldLen=   256
      Columns(18).StyleSet=   "headerfield"
      Columns(19).Width=   1931
      Columns(19).Caption=   "Aspect Ratio"
      Columns(19).Name=   "imageaspectratio"
      Columns(19).DataField=   "imageaspectratio"
      Columns(19).FieldLen=   256
      Columns(19).StyleSet=   "headerfield"
      Columns(20).Width=   6006
      Columns(20).Caption=   "Picture Elements"
      Columns(20).Name=   "legacypictureelement"
      Columns(20).DataField=   "legacypictureelement"
      Columns(20).FieldLen=   256
      Columns(20).StyleSet=   "headerfield"
      Columns(21).Width=   4948
      Columns(21).Caption=   "Audio"
      Columns(21).Name=   "audioconfiguration"
      Columns(21).DataField=   "audioconfiguration"
      Columns(21).FieldLen=   256
      Columns(21).StyleSet=   "headerfield"
      Columns(22).Width=   1058
      Columns(22).Caption=   "Batch"
      Columns(22).Name=   "batch"
      Columns(22).DataField=   "batch"
      Columns(22).FieldLen=   256
      Columns(22).StyleSet=   "headerfield"
      Columns(23).Width=   1588
      Columns(23).Caption=   "Language"
      Columns(23).Name=   "language"
      Columns(23).DataField=   "language"
      Columns(23).FieldLen=   256
      Columns(23).StyleSet=   "headerfield"
      Columns(24).Width=   1588
      Columns(24).Caption=   "Subtitles"
      Columns(24).Name=   "subtitleslanguage"
      Columns(24).DataField=   "subtitleslanguage"
      Columns(24).FieldLen=   256
      Columns(24).StyleSet=   "headerfield"
      Columns(25).Width=   1376
      Columns(25).Caption=   "Finished?"
      Columns(25).Name=   "readytobill"
      Columns(25).DataField=   "readytobill"
      Columns(25).FieldLen=   256
      Columns(25).Locked=   -1  'True
      Columns(25).Style=   2
      Columns(25).StyleSet=   "conclusionfield"
      Columns(26).Width=   3200
      Columns(26).Visible=   0   'False
      Columns(26).Caption=   "jobID"
      Columns(26).Name=   "jobID"
      Columns(26).DataField=   "jobID"
      Columns(26).FieldLen=   256
      Columns(27).Width=   1376
      Columns(27).Caption=   "Billed?"
      Columns(27).Name=   "billed"
      Columns(27).DataField=   "billed"
      Columns(27).FieldLen=   256
      Columns(27).Locked=   -1  'True
      Columns(27).Style=   2
      Columns(27).StyleSet=   "conclusionfield"
      Columns(28).Width=   3200
      Columns(28).Visible=   0   'False
      Columns(28).Caption=   "storagebarcode"
      Columns(28).Name=   "storagebarcode"
      Columns(28).DataField=   "storagebarcode"
      Columns(28).FieldLen=   256
      _ExtentX        =   45138
      _ExtentY        =   16536
      _StockProps     =   79
      Caption         =   "Tracker Items"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoComments 
      Height          =   330
      Left            =   13500
      Top             =   12660
      Visible         =   0   'False
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdComments 
      Bindings        =   "frmTrackerGlobal.frx":006B
      Height          =   1875
      Left            =   120
      TabIndex        =   39
      Top             =   12660
      Width           =   12555
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12582847
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   6
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "trackerhistoryID"
      Columns(0).Name =   "tracker_commentID"
      Columns(0).DataField=   "tracker_commentID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "trackerprogramID"
      Columns(1).Name =   "tracker_dadc_itemID"
      Columns(1).DataField=   "tracker_dadc_itemID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   13070
      Columns(2).Caption=   "Comments"
      Columns(2).Name =   "comment"
      Columns(2).DataField=   "comment"
      Columns(2).FieldLen=   255
      Columns(3).Width=   3360
      Columns(3).Caption=   "Date"
      Columns(3).Name =   "cdate"
      Columns(3).DataField=   "cdate"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).Style=   1
      Columns(4).Width=   3519
      Columns(4).Caption=   "Entered By"
      Columns(4).Name =   "cuser"
      Columns(4).DataField=   "cuser"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   1138
      Columns(5).Caption=   "Email?"
      Columns(5).Name =   "Email?"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   4
      UseDefaults     =   0   'False
      _ExtentX        =   22146
      _ExtentY        =   3307
      _StockProps     =   79
      Caption         =   "Tracker Comments"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   1770
      TabIndex        =   45
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The company this job is for"
      Top             =   120
      Width           =   2835
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   5001
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   10
      Left            =   180
      TabIndex        =   46
      Top             =   180
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Total Est. Duration"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   7
      Left            =   18180
      TabIndex        =   43
      Top             =   540
      Width           =   1755
   End
   Begin VB.Label lblCaption 
      Caption         =   "Reference"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   13
      Left            =   4860
      TabIndex        =   35
      Top             =   1260
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Total Size (GB)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   12
      Left            =   18180
      TabIndex        =   29
      Top             =   1260
      Width           =   1755
   End
   Begin VB.Label lblCaption 
      Caption         =   "Total Duration"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   11
      Left            =   18180
      TabIndex        =   27
      Top             =   900
      Width           =   1755
   End
   Begin VB.Label lblCaption 
      Caption         =   "Series"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   10500
      TabIndex        =   25
      Top             =   180
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Episode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   10500
      TabIndex        =   24
      Top             =   540
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Tape Format"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   4860
      TabIndex        =   17
      Top             =   1620
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Barcode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   4860
      TabIndex        =   16
      Top             =   900
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Subtitle"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   4860
      TabIndex        =   13
      Top             =   540
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   4860
      TabIndex        =   12
      Top             =   180
      Width           =   1395
   End
   Begin VB.Label lblTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   22260
      TabIndex        =   10
      Top             =   12900
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCompanyID 
      Height          =   255
      Left            =   23520
      TabIndex        =   9
      Top             =   12960
      Visible         =   0   'False
      Width           =   735
   End
End
Attribute VB_Name = "frmTrackerGlobal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strSearch As String
Dim m_strOrderby As String

Private Sub HideAllColumns()

'Eventually Hide Items from different buttons

End Sub

Private Sub adoItems_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

Dim l_strSQL As String

If adoItems.Recordset.EOF = True Then
    cmdBillItem.Visible = False
    Exit Sub
Else
    If adoItems.Recordset("tracker_publicis_itemID") = "" Then
        cmdBillItem.Visible = False
        Exit Sub
    End If
End If

lblTrackeritemID.Caption = adoItems.Recordset("tracker_publicis_itemID")

If Val(grdItems.Columns("tracker_publicis_itemID").Text) <> 0 Then
    
    l_strSQL = "SELECT * FROM tracker_publicis_comment WHERE tracker_publicis_itemID = " & lblTrackeritemID.Caption & " ORDER BY cdate;"
    
    adoComments.RecordSource = l_strSQL
    adoComments.ConnectionString = g_strConnection
    adoComments.Refresh

Else

    l_strSQL = "SELECT * FROM tracker_publicis_comment WHERE tracker_publicis_itemID = -1 ORDER BY cdate;"
    
    adoComments.RecordSource = l_strSQL
    adoComments.ConnectionString = g_strConnection
    adoComments.Refresh

End If

If adoItems.Recordset("readytobill") <> 0 And adoItems.Recordset("billed") = 0 Then
    cmdBillItem.Visible = True
    cmdManualBillItem.Visible = True
Else
    cmdBillItem.Visible = False
    cmdManualBillItem.Visible = False
End If

If adoItems.Recordset("billed") <> 0 Then
    cmdUnbill.Visible = True
Else
    cmdUnbill.Visible = False
End If

End Sub

Private Sub chkHideDemo_Click()

Dim l_strSQL As String

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/globaltracker%' and companyID > 100 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/globaltracker%' ORDER BY name;"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

cmdSearch.Value = True

End Sub

Private Sub cmbCompany_Click()

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text
m_strSearch = " WHERE companyID = " & lblCompanyID.Caption
'm_strOrderby = " ORDER BY headertext1, headerint1, headerint2, headerint3, headerint4, headerint5"

HideAllColumns

If InStr(GetData("company", "cetaclientcode", "companyID", cmbCompany.Columns("companyID").Text), "/trackerfilename") > 0 Then
    grdItems.Columns("itemfilename").Visible = True
    grdItems.Columns("itemfilename").Width = 3000
Else
    grdItems.Columns("itemfilename").Visible = False
End If

DoEvents

Dim l_rstChoices As ADODB.Recordset, l_blnWide As Boolean

If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
    l_blnWide = True
Else
    l_blnWide = False
End If

cmdSearch.Value = True

End Sub

Private Sub cmdAllFields_Click()

grdItems.Columns("externalalphaID").Visible = True
grdItems.Columns("originalexternalalphaID").Visible = True
grdItems.Columns("legacylinestandard").Visible = True
grdItems.Columns("imageaspectratio").Visible = True
grdItems.Columns("language").Visible = True
grdItems.Columns("subtitleslanguage").Visible = True
grdItems.Columns("legacypictureelement").Visible = True
grdItems.Columns("audioconfiguration").Visible = True

End Sub

Private Sub cmdBillAll_Click()

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then
    
    While Not adoItems.Recordset.EOF
        If cmdBillItem.Visible = True Then
            cmdBillItem.Value = True
        End If
        adoItems.Recordset.MoveNext
    Wend
End If

End Sub

Private Sub cmdBillItem_Click()

'Dim l_strChargeCode As String, l_rstTrackerChargeCode As ADODB.Recordset, l_strJobLine As String, l_lngJobID As Long, l_lngDuration As Long, l_lngMinuteBilling As Long
'Dim l_lngQuantity As Long, l_strCode As String, l_lngFactor As Long, l_blnFirstItem As Boolean
'
'l_blnFirstItem = True
'If frmJob.txtJobID.Text <> "" Then
'    'A job is loaded - now check that it isn't locked.
'    l_lngJobID = Val(frmJob.txtJobID.Text)
'    If frmJob.txtStatus.Text = "Confirmed" And frmJob.lblCompanyID.Caption = lblCompanyID.Caption Then
'        If grdItems.Columns("duration").Text <> "" Then
'            'The Job is valid - go ahead and create Job lines for this item.
'            l_lngQuantity = 0
'            l_strJobLine = QuoteSanitise(grdItems.Columns("itemreference").Text)
'            If grdItems.Columns("title").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("title").Text
'            If grdItems.Columns("episode").Text <> "" Then l_strJobLine = l_strJobLine & " - Ep " & grdItems.Columns("episode").Text
'            If grdItems.Columns("stagefield15").Text <> "" Then
'                l_strCode = "M"
'                l_lngDuration = Val(grdItems.Columns("duration").Text)
'                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield15';", g_strExecuteError)
'                CheckForSQLError
'                If l_rstTrackerChargeCode.RecordCount > 0 Then
'                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
'                    Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
'                        Case "N"
'                            l_lngQuantity = Val(grdItems.Columns("stagefield1").Text)
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "T"
'                            l_lngQuantity = 1
'                            l_lngDuration = Val(grdItems.Columns("stagefield1").Text)
'                        Case "M"
'                            l_lngMinuteBilling = 1
'                        Case "S"
'                            l_lngFactor = 0
'                            If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
'                            If l_lngFactor <> 0 Then
'                                If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
'                                    l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
'                                Else
'                                    l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                                End If
'                            Else
'                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                            End If
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "1"
'                            l_lngQuantity = 1
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "I"
'                            l_strCode = "I"
'                        Case Else
'                            l_lngMinuteBilling = 0
'                    End Select
'                    If l_lngQuantity = 0 Then l_lngQuantity = 1
'                End If
'                l_rstTrackerChargeCode.Close
'                If l_strChargeCode <> "" Then
'                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
'                    l_lngQuantity = 0
'                End If
'            End If
'            frmJob.adoJobDetail.Refresh
'            If grdItems.Columns("stagefield1").Text <> "" Then
'                l_strCode = "A"
'                l_lngDuration = Val(grdItems.Columns("duration").Text)
'                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield1';", g_strExecuteError)
'                CheckForSQLError
'                If l_rstTrackerChargeCode.RecordCount > 0 Then
'                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
'                    Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
'                        Case "N"
'                            l_lngQuantity = Val(grdItems.Columns("stagefield15").Text)
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "T"
'                            l_lngQuantity = 1
'                            l_lngDuration = Val(grdItems.Columns("stagefield15").Text)
'                        Case "M"
'                            l_lngMinuteBilling = 1
'                        Case "S"
'                            l_lngFactor = 1
'                            If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
'                            If l_lngFactor <> 0 Then
'                                If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
'                                    l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
'                                Else
'                                    l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                                End If
'                            Else
'                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                            End If
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "1"
'                            l_lngQuantity = 1
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "I"
'                            l_strCode = "I"
'                        Case Else
'                            l_lngMinuteBilling = 0
'                    End Select
'                    If l_lngQuantity = 0 Then l_lngQuantity = 1
'                End If
'                l_rstTrackerChargeCode.Close
'                If l_strChargeCode <> "" Then
'                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
'                    l_lngQuantity = 0
'                End If
'            End If
'            If grdItems.Columns("stagefield2").Text <> "" Then
'                l_strCode = "A"
'                l_lngDuration = Val(grdItems.Columns("duration").Text)
'                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield2';", g_strExecuteError)
'                CheckForSQLError
'                If l_rstTrackerChargeCode.RecordCount > 0 Then
'                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
'                    Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
'                        Case "N"
'                            l_lngQuantity = Val(grdItems.Columns("stagefield2").Text)
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "T"
'                            l_lngQuantity = 1
'                            l_lngDuration = Val(grdItems.Columns("stagefield2").Text)
'                        Case "M"
'                            l_lngMinuteBilling = 1
'                        Case "S"
'                            l_lngFactor = 1
'                            If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
'                            If l_lngFactor <> 0 Then
'                                If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
'                                    l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
'                                Else
'                                    l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                                End If
'                            Else
'                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                            End If
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "1"
'                            l_lngQuantity = 1
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "I"
'                            l_strCode = "I"
'                        Case Else
'                            l_lngMinuteBilling = 0
'                    End Select
'                    If l_lngQuantity = 0 Then l_lngQuantity = 1
'                End If
'                l_rstTrackerChargeCode.Close
'                If l_strChargeCode <> "" Then
'                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
'                    l_lngQuantity = 0
'                End If
'            End If
'            frmJob.adoJobDetail.Refresh
'            If grdItems.Columns("stagefield3").Text <> "" Then
'                l_strCode = "A"
'                l_lngDuration = Val(grdItems.Columns("duration").Text)
'                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield3';", g_strExecuteError)
'                CheckForSQLError
'                If l_rstTrackerChargeCode.RecordCount > 0 Then
'                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
'                    Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
'                        Case "N"
'                            l_lngQuantity = Val(grdItems.Columns("stagefield3").Text)
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "T"
'                            l_lngQuantity = 1
'                            l_lngDuration = Val(grdItems.Columns("stagefield3").Text)
'                        Case "M"
'                            l_lngMinuteBilling = 1
'                        Case "S"
'                            l_lngFactor = 1
'                            If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
'                            If l_lngFactor <> 0 Then
'                                If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
'                                    l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
'                                Else
'                                    l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                                End If
'                            Else
'                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                            End If
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "1"
'                            l_lngQuantity = 1
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "I"
'                            l_strCode = "I"
'                        Case Else
'                            l_lngMinuteBilling = 0
'                    End Select
'                    If l_lngQuantity = 0 Then l_lngQuantity = 1
'                End If
'                l_rstTrackerChargeCode.Close
'                If l_strChargeCode <> "" Then
'                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
'                    l_lngQuantity = 0
'                End If
'            End If
'            frmJob.adoJobDetail.Refresh
'            If grdItems.Columns("stagefield4").Text <> "" Then
'                l_strCode = "A"
'                l_lngDuration = Val(grdItems.Columns("duration").Text)
'                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield4';", g_strExecuteError)
'                CheckForSQLError
'                If l_rstTrackerChargeCode.RecordCount > 0 Then
'                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
'                    Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
'                        Case "N"
'                            l_lngQuantity = Val(grdItems.Columns("stagefield4").Text)
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "T"
'                            l_lngQuantity = 1
'                            l_lngDuration = Val(grdItems.Columns("stagefield4").Text)
'                        Case "M"
'                            l_lngMinuteBilling = 1
'                        Case "S"
'                            l_lngFactor = 1
'                            If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
'                            If l_lngFactor <> 0 Then
'                                If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
'                                    l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
'                                Else
'                                    l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                                End If
'                            Else
'                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                            End If
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "1"
'                            l_lngQuantity = 1
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "I"
'                            l_strCode = "I"
'                        Case Else
'                            l_lngMinuteBilling = 0
'                    End Select
'                    If l_lngQuantity = 0 Then l_lngQuantity = 1
'                End If
'                l_rstTrackerChargeCode.Close
'                If l_strChargeCode <> "" Then
'                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
'                    l_lngQuantity = 0
'                End If
'            End If
'            frmJob.adoJobDetail.Refresh
'            If grdItems.Columns("stagefield5").Text <> "" Then
'                l_strCode = "A"
'                l_lngDuration = Val(grdItems.Columns("duration").Text)
'                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield5';", g_strExecuteError)
'                CheckForSQLError
'                If l_rstTrackerChargeCode.RecordCount > 0 Then
'                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
'                    Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
'                        Case "N"
'                            l_lngQuantity = Val(grdItems.Columns("stagefield5").Text)
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "T"
'                            l_lngQuantity = 1
'                            l_lngDuration = Val(grdItems.Columns("stagefield5").Text)
'                        Case "M"
'                            l_lngMinuteBilling = 1
'                        Case "S"
'                            l_lngFactor = 1
'                            If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
'                            If l_lngFactor <> 0 Then
'                                If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
'                                    l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
'                                Else
'                                    l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                                End If
'                            Else
'                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                            End If
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "1"
'                            l_lngQuantity = 1
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "I"
'                            l_strCode = "I"
'                        Case Else
'                            l_lngMinuteBilling = 0
'                    End Select
'                    If l_lngQuantity = 0 Then l_lngQuantity = 1
'                End If
'                l_rstTrackerChargeCode.Close
'                If l_strChargeCode <> "" Then
'                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
'                    l_lngQuantity = 0
'                End If
'            End If
'            frmJob.adoJobDetail.Refresh
'            If grdItems.Columns("stagefield6").Text <> "" Then
'                l_strCode = "A"
'                l_lngDuration = Val(grdItems.Columns("duration").Text)
'                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield6';", g_strExecuteError)
'                CheckForSQLError
'                If l_rstTrackerChargeCode.RecordCount > 0 Then
'                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
'                    Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
'                        Case "N"
'                            l_lngQuantity = Val(grdItems.Columns("stagefield6").Text)
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "T"
'                            l_lngQuantity = 1
'                            l_lngDuration = Val(grdItems.Columns("stagefield6").Text)
'                        Case "M"
'                            l_lngMinuteBilling = 1
'                        Case "S"
'                            l_lngFactor = 1
'                            If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
'                            If l_lngFactor <> 0 Then
'                                If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
'                                    l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
'                                Else
'                                    l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                                End If
'                            Else
'                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                            End If
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "1"
'                            l_lngQuantity = 1
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "I"
'                            l_strCode = "I"
'                        Case Else
'                            l_lngMinuteBilling = 0
'                    End Select
'                    If l_lngQuantity = 0 Then l_lngQuantity = 1
'                End If
'                l_rstTrackerChargeCode.Close
'                If l_strChargeCode <> "" Then
'                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
'                    l_lngQuantity = 0
'                End If
'            End If
'            If grdItems.Columns("stagefield7").Text <> "" Then
'            frmJob.adoJobDetail.Refresh
'                l_strCode = "A"
'                l_lngDuration = Val(grdItems.Columns("duration").Text)
'                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield7';", g_strExecuteError)
'                CheckForSQLError
'                If l_rstTrackerChargeCode.RecordCount > 0 Then
'                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
'                    Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
'                        Case "N"
'                            l_lngQuantity = Val(grdItems.Columns("stagefield7").Text)
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "T"
'                            l_lngQuantity = 1
'                            l_lngDuration = Val(grdItems.Columns("stagefield7").Text)
'                        Case "M"
'                            l_lngMinuteBilling = 1
'                        Case "S"
'                            l_lngFactor = 1
'                            If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
'                            If l_lngFactor <> 0 Then
'                                If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
'                                    l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
'                                Else
'                                    l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                                End If
'                            Else
'                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                            End If
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "1"
'                            l_lngQuantity = 1
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "I"
'                            l_strCode = "I"
'                        Case Else
'                            l_lngMinuteBilling = 0
'                    End Select
'                    If l_lngQuantity = 0 Then l_lngQuantity = 1
'                End If
'                l_rstTrackerChargeCode.Close
'                If l_strChargeCode <> "" Then
'                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
'                    l_lngQuantity = 0
'                End If
'            End If
'            frmJob.adoJobDetail.Refresh
'            If grdItems.Columns("stagefield8").Text <> "" Then
'                l_strCode = "A"
'                l_lngDuration = Val(grdItems.Columns("duration").Text)
'                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield8';", g_strExecuteError)
'                CheckForSQLError
'                If l_rstTrackerChargeCode.RecordCount > 0 Then
'                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
'                    Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
'                        Case "N"
'                            l_lngQuantity = Val(grdItems.Columns("stagefield8").Text)
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "T"
'                            l_lngQuantity = 1
'                            l_lngDuration = Val(grdItems.Columns("stagefield8").Text)
'                        Case "M"
'                            l_lngMinuteBilling = 1
'                        Case "S"
'                            l_lngFactor = 1
'                            If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
'                            If l_lngFactor <> 0 Then
'                                If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
'                                    l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
'                                Else
'                                    l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                                End If
'                            Else
'                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                            End If
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "1"
'                            l_lngQuantity = 1
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "I"
'                            l_strCode = "I"
'                        Case Else
'                            l_lngMinuteBilling = 0
'                    End Select
'                    If l_lngQuantity = 0 Then l_lngQuantity = 1
'                End If
'                l_rstTrackerChargeCode.Close
'                If l_strChargeCode <> "" Then
'                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
'                    l_lngQuantity = 0
'                End If
'            End If
'            frmJob.adoJobDetail.Refresh
'            If grdItems.Columns("stagefield9").Text <> "" Then
'                l_strCode = "A"
'                l_lngDuration = Val(grdItems.Columns("duration").Text)
'                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield9';", g_strExecuteError)
'                CheckForSQLError
'                If l_rstTrackerChargeCode.RecordCount > 0 Then
'                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
'                    Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
'                        Case "N"
'                            l_lngQuantity = Val(grdItems.Columns("stagefield9").Text)
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "T"
'                            l_lngQuantity = 1
'                            l_lngDuration = Val(grdItems.Columns("stagefield9").Text)
'                        Case "M"
'                            l_lngMinuteBilling = 1
'                        Case "S"
'                            l_lngFactor = 1
'                            If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
'                            If l_lngFactor <> 0 Then
'                                If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
'                                    l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
'                                Else
'                                    l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                                End If
'                            Else
'                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                            End If
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "1"
'                            l_lngQuantity = 1
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "I"
'                            l_strCode = "I"
'                        Case Else
'                            l_lngMinuteBilling = 0
'                    End Select
'                    If l_lngQuantity = 0 Then l_lngQuantity = 1
'                End If
'                l_rstTrackerChargeCode.Close
'                If l_strChargeCode <> "" Then
'                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
'                    l_lngQuantity = 0
'                End If
'            End If
'            frmJob.adoJobDetail.Refresh
'            If grdItems.Columns("stagefield10").Text <> "" Then
'                l_strCode = "A"
'                l_lngDuration = Val(grdItems.Columns("duration").Text)
'                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield10';", g_strExecuteError)
'                CheckForSQLError
'                If l_rstTrackerChargeCode.RecordCount > 0 Then
'                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
'                    Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
'                        Case "N"
'                            l_lngQuantity = Val(grdItems.Columns("stagefield10").Text)
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "T"
'                            l_lngQuantity = 1
'                            l_lngDuration = Val(grdItems.Columns("stagefield10").Text)
'                        Case "M"
'                            l_lngMinuteBilling = 1
'                        Case "S"
'                            l_lngFactor = 1
'                            If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
'                            If l_lngFactor <> 0 Then
'                                If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
'                                    l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
'                                Else
'                                    l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                                End If
'                            Else
'                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                            End If
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "1"
'                            l_lngQuantity = 1
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "I"
'                            l_strCode = "I"
'                        Case Else
'                            l_lngMinuteBilling = 0
'                    End Select
'                    If l_lngQuantity = 0 Then l_lngQuantity = 1
'                End If
'                l_rstTrackerChargeCode.Close
'                If l_strChargeCode <> "" Then
'                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
'                    l_lngQuantity = 0
'                End If
'            End If
'            frmJob.adoJobDetail.Refresh
'            If grdItems.Columns("stagefield11").Text <> "" Then
'                l_strCode = "A"
'                l_lngDuration = Val(grdItems.Columns("duration").Text)
'                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield11';", g_strExecuteError)
'                CheckForSQLError
'                If l_rstTrackerChargeCode.RecordCount > 0 Then
'                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
'                    Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
'                        Case "N"
'                            l_lngQuantity = Val(grdItems.Columns("stagefield11").Text)
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "T"
'                            l_lngQuantity = 1
'                            l_lngDuration = Val(grdItems.Columns("stagefield11").Text)
'                        Case "M"
'                            l_lngMinuteBilling = 1
'                        Case "S"
'                            l_lngFactor = 1
'                            If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
'                            If l_lngFactor <> 0 Then
'                                If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
'                                    l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
'                                Else
'                                    l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                                End If
'                            Else
'                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                            End If
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "1"
'                            l_lngQuantity = 1
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "I"
'                            l_strCode = "I"
'                        Case Else
'                            l_lngMinuteBilling = 0
'                    End Select
'                    If l_lngQuantity = 0 Then l_lngQuantity = 1
'                End If
'                l_rstTrackerChargeCode.Close
'                If l_strChargeCode <> "" Then
'                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
'                    l_lngQuantity = 0
'                End If
'            End If
'            frmJob.adoJobDetail.Refresh
'            If grdItems.Columns("stagefield12").Text <> "" Then
'                l_strCode = "A"
'                l_lngDuration = Val(grdItems.Columns("duration").Text)
'                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield12';", g_strExecuteError)
'                CheckForSQLError
'                If l_rstTrackerChargeCode.RecordCount > 0 Then
'                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
'                    Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
'                        Case "N"
'                            l_lngQuantity = Val(grdItems.Columns("stagefield12").Text)
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "T"
'                            l_lngQuantity = 1
'                            l_lngDuration = Val(grdItems.Columns("stagefield12").Text)
'                        Case "M"
'                            l_lngMinuteBilling = 1
'                        Case "S"
'                            l_lngFactor = 1
'                            If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
'                            If l_lngFactor <> 0 Then
'                                If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
'                                    l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
'                                Else
'                                    l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                                End If
'                            Else
'                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                            End If
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "1"
'                            l_lngQuantity = 1
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "I"
'                            l_strCode = "I"
'                        Case Else
'                            l_lngMinuteBilling = 0
'                    End Select
'                    If l_lngQuantity = 0 Then l_lngQuantity = 1
'                End If
'                l_rstTrackerChargeCode.Close
'                If l_strChargeCode <> "" Then
'                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
'                    l_lngQuantity = 0
'                End If
'            End If
'            frmJob.adoJobDetail.Refresh
'            If grdItems.Columns("stagefield13").Text <> "" Then
'                l_strCode = "A"
'                l_lngDuration = Val(grdItems.Columns("duration").Text)
'                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield13';", g_strExecuteError)
'                CheckForSQLError
'                If l_rstTrackerChargeCode.RecordCount > 0 Then
'                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
'                    Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
'                        Case "N"
'                            l_lngQuantity = Val(grdItems.Columns("stagefield13").Text)
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "T"
'                            l_lngQuantity = 1
'                            l_lngDuration = Val(grdItems.Columns("stagefield13").Text)
'                        Case "M"
'                            l_lngMinuteBilling = 1
'                        Case "S"
'                            l_lngFactor = 1
'                            If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
'                            If l_lngFactor <> 0 Then
'                                If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
'                                    l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
'                                Else
'                                    l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                                End If
'                            Else
'                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                            End If
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "1"
'                            l_lngQuantity = 1
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "I"
'                            l_strCode = "I"
'                        Case Else
'                            l_lngMinuteBilling = 0
'                    End Select
'                    If l_lngQuantity = 0 Then l_lngQuantity = 1
'                End If
'                l_rstTrackerChargeCode.Close
'                If l_strChargeCode <> "" Then
'                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
'                    l_lngQuantity = 0
'                End If
'            End If
'            frmJob.adoJobDetail.Refresh
'            If grdItems.Columns("stagefield14").Text <> "" Then
'                l_strCode = "A"
'                l_lngDuration = Val(grdItems.Columns("duration").Text)
'                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield14';", g_strExecuteError)
'                CheckForSQLError
'                If l_rstTrackerChargeCode.RecordCount > 0 Then
'                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
'                    Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
'                        Case "N"
'                            l_lngQuantity = Val(grdItems.Columns("stagefield14").Text)
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "T"
'                            l_lngQuantity = 1
'                            l_lngDuration = Val(grdItems.Columns("stagefield14").Text)
'                        Case "M"
'                            l_lngMinuteBilling = 1
'                        Case "S"
'                            l_lngFactor = 1
'                            If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
'                            If l_lngFactor <> 0 Then
'                                If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
'                                    l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
'                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
'                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
'                                Else
'                                    l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                                End If
'                            Else
'                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
'                            End If
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "1"
'                            l_lngQuantity = 1
'                            l_lngDuration = 0
'                            l_strCode = "O"
'                        Case "I"
'                            l_strCode = "I"
'                        Case Else
'                            l_lngMinuteBilling = 0
'                    End Select
'                    If l_lngQuantity = 0 Then l_lngQuantity = 1
'                End If
'                l_rstTrackerChargeCode.Close
'                If l_strChargeCode <> "" Then
'                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
'                    l_lngQuantity = 0
'                End If
'            End If
'            frmJob.adoJobDetail.Refresh
'            frmJob.adoJobDetail.Refresh
'            grdItems.Columns("billed").Text = -1
'            grdItems.Columns("jobID").Text = l_lngJobID
'            grdItems.Update
'            cmdBillItem.Visible = False
'            cmdManualBillItem.Visible = False
'            m_blnDontVerifyXML = False
'        Else
'            MsgBox "The current Item must have a duration in order to bill it.", vbCritical, "Cannot Bill Item"
'        End If
'    Else
'        MsgBox "The current Job must belong to the correct company and be confirmed in order to bill an item.", vbCritical, "Cannot Bill Item"
'    End If
'Else
'    MsgBox "A Job must be created and loaded into the Jobs form in order to bill an item.", vbCritical, "Cannot Bill Item"
'End If
'
End Sub

Private Sub cmdClear_Click()

ClearFields Me

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdManualBillItem_Click()

grdItems.Columns("billed").Text = -1
grdItems.Columns("jobid").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False

End Sub

Private Sub cmdPrint_Click()

Dim l_strSQL As String

If lblCompanyID.Caption <> "" Then
    l_strSQL = adoItems.RecordSource
    PrintCrystalReportUsingCleanSQL App.Path & "\reports\GenericTrackerReport.rpt", l_strSQL, True
End If

End Sub

Private Sub cmdReport_Click()

Dim l_strSelectionFormula As String
Dim l_strReportFile As String

    If optComplete(0).Value = True Then
        l_strSelectionFormula = "(isnull({tracker_publicis_item.billed}) or {tracker_publicis_item.billed} = 0) AND (isnull({tracker_publicis_item.readytobill}) OR {tracker_publicis_item.readytobill} = 0)"
    ElseIf optComplete(1).Value = True Then
        l_strSelectionFormula = "(isnull({tracker_publicis_item.billed}) or {tracker_publicis_item.billed} = 0) and {tracker_publicis_item.readytobill} <> 0"
    ElseIf optComplete(2).Value = True Then
        l_strSelectionFormula = "not isnull({tracker_publicis_item.billed}) and {tracker_publicis_item.billed} <> 0"
    ElseIf optComplete(4).Value = True Then
        l_strSelectionFormula = "(isnull({tracker_publicis_item.billed}) or {tracker_publicis_item.billed} = 0) AND (isnull({tracker_publicis_item.readytobill}) OR {tracker_publicis_item.readytobill} = 0) AND not isnull({tracker_publicis_item.stagefield2}) AND {tracker_publicis_item.rejected} = 0"
    ElseIf optComplete(5).Value = True Then
        l_strSelectionFormula = "(isnull({tracker_publicis_item.billed}) or {tracker_publicis_item.billed} = 0) AND (isnull({tracker_publicis_item.readytobill}) OR {tracker_publicis_item.readytobill} = 0) AND not isnull({tracker_publicis_item.stagefield10}) AND {tracker_publicis_item.rejected} = 0"
    ElseIf optComplete(6).Value = True Then
        l_strSelectionFormula = "(isnull({tracker_publicis_item.billed}) or {tracker_publicis_item.billed} = 0) AND (isnull({tracker_publicis_item.readytobill}) OR {tracker_publicis_item.readytobill} = 0) AND not isnull({tracker_publicis_item.stagefield2}) AND isnull({tracker_publicis_item.stagefield10}) AND {tracker_publicis_item.rejected} = 0"
    ElseIf optComplete(7).Value = True Then
        l_strSelectionFormula = "{tracker_publicis_item.rejected}=1"
    ElseIf optComplete(8).Value = True Then
        l_strSelectionFormula = "(isnull({tracker_publicis_item.billed}) or {tracker_publicis_item.billed} = 0) AND (isnull({tracker_publicis_item.readytobill}) OR {tracker_publicis_item.readytobill} = 0) AND not isnull({tracker_publicis_item.stagefield3}) AND isnull({tracker_publicis_item.stagefield4}) AND {tracker_publicis_item.rejected} = 0"
    ElseIf optComplete(9).Value = True Then
        l_strSelectionFormula = "(isnull({tracker_publicis_item.billed}) or {tracker_publicis_item.billed} = 0) AND (isnull({tracker_publicis_item.readytobill}) OR {tracker_publicis_item.readytobill} = 0) AND not isnull({tracker_publicis_item.stagefield4}) AND Not isnull({tracker_publicis_item.stagefield10}) AND {tracker_publicis_item.rejected} = 0"
    ElseIf optComplete(10).Value = True Then
        l_strSelectionFormula = "(isnull({tracker_publicis_item.billed}) or {tracker_publicis_item.billed} = 0) AND (isnull({tracker_publicis_item.readytobill}) OR {tracker_publicis_item.readytobill} = 0) AND isnull({tracker_publicis_item.stagefield1}) AND {tracker_publicis_item.rejected} = 0"
    ElseIf optComplete(11).Value = True Then
        l_strSelectionFormula = "(isnull({tracker_publicis_item.billed}) or {tracker_publicis_item.billed} = 0) AND (isnull({tracker_publicis_item.readytobill}) OR {tracker_publicis_item.readytobill} = 0) AND isnull({tracker_publicis_item.stagefield2}) AND {tracker_publicis_item.rejected} = 0"
    ElseIf optComplete(12).Value = True Then
        l_strSelectionFormula = "(isnull({tracker_publicis_item.billed}) or {tracker_publicis_item.billed} = 0) AND (isnull({tracker_publicis_item.readytobill}) OR {tracker_publicis_item.readytobill} = 0) AND not isnull({tracker_publicis_item.stagefield3}) AND {tracker_publicis_item.rejected} = 0"
    ElseIf optComplete(13).Value = True Then
        l_strSelectionFormula = "(isnull({tracker_publicis_item.billed}) or {tracker_publicis_item.billed} = 0) AND (isnull({tracker_publicis_item.readytobill}) OR {tracker_publicis_item.readytobill} = 0) AND not isnull({tracker_publicis_item.stagefield4}) AND {tracker_publicis_item.rejected} = 0"
    End If
    
    If txtReference.Text <> "" Then
        l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_publicis_item.billed} LIKE '" & QuoteSanitise(txtReference.Text) & "*' "
    End If
    
    If txtTitle.Text <> "" Then
        l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_publicis_item.title} LIKE '" & QuoteSanitise(txtTitle.Text) & "*' "
    End If
    If txtSubtitle.Text <> "" Then
        l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_publicis_item.subtitle} LIKE '" & QuoteSanitise(txtSubtitle.Text) & "*' "
    End If
    If txtBarcode.Text <> "" Then
        l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_publicis_item.barcode} LIKE '" & QuoteSanitise(txtBarcode.Text) & "*' "
    End If
    If txtFormat.Text <> "" Then
        l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_publicis_item.format} LIKE '" & QuoteSanitise(txtFormat.Text) & "*' "
    End If
    If Val(txtEpisode.Text) <> 0 Then
        l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_publicis_item.episode} = " & Val(txtEpisode.Text) & " "
    End If
    If Val(txtSeries.Text) <> 0 Then
        l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_publicis_item.series} = " & Val(txtSeries.Text) & " "
    End If
    
l_strReportFile = App.Path & "\reports\DADC_report.rpt"
PrintCrystalReport l_strReportFile, l_strSelectionFormula, True
    
End Sub

Private Sub cmdSearch_Click()

Dim l_rstTotal As ADODB.Recordset, l_lngVideoTotal As Long

If lblCompanyID.Caption <> "" Then

    Dim l_strSQL As String
    If optComplete(0).Value = True Then
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND rejected = 0 "
    ElseIf optComplete(1).Value = True Then
        l_strSQL = "AND (billed IS NULL OR billed = 0) AND readytobill <> 0"
    ElseIf optComplete(2).Value = True Then
        l_strSQL = "AND billed IS NOT NULL AND billed <> 0"
    ElseIf optComplete(4).Value = True Then
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (tapereturned IS NOT NULL) "
    End If
    
    If txtReference.Text <> "" Then
        l_strSQL = l_strSQL & " AND itemreference LIKE '" & QuoteSanitise(txtReference.Text) & "%' "
    End If
    
    If txtTitle.Text <> "" Then
        l_strSQL = l_strSQL & " AND title LIKE '" & QuoteSanitise(txtTitle.Text) & "%' "
    End If
    If txtSubtitle.Text <> "" Then
        l_strSQL = l_strSQL & " AND subtitle LIKE '" & QuoteSanitise(txtSubtitle.Text) & "%' "
    End If
    If txtBarcode.Text <> "" Then
        l_strSQL = l_strSQL & " AND barcode LIKE '" & QuoteSanitise(txtBarcode.Text) & "%' "
    End If
    If txtFormat.Text <> "" Then
        l_strSQL = l_strSQL & " AND format LIKE '" & QuoteSanitise(txtFormat.Text) & "%' "
    End If
    If Val(txtEpisode.Text) <> 0 Then
        l_strSQL = l_strSQL & " AND (episode IS NOT NULL AND episode = " & Val(txtEpisode.Text) & ") "
    End If
    If Val(txtSeries.Text) <> 0 Then
        l_strSQL = l_strSQL & " AND (series IS NOT NULL AND series = " & Val(txtSeries.Text) & ") "
    End If

    adoItems.ConnectionString = g_strConnection
    adoItems.RecordSource = "SELECT * FROM tracker_publicis_item " & m_strSearch & l_strSQL & m_strOrderby & ";"
    adoItems.Refresh
    
    adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"

    Set l_rstTotal = ExecuteSQL("SELECT Sum(duration) FROM tracker_publicis_item " & m_strSearch & l_strSQL & ";", g_strExecuteError)
    CheckForSQLError
    
    If IsNull(l_rstTotal(0)) Then
        l_lngVideoTotal = 0
    Else
        l_lngVideoTotal = l_rstTotal(0)
    End If
    
    l_rstTotal.Close

    txtTotalDuration.Text = l_lngVideoTotal

    Set l_rstTotal = ExecuteSQL("SELECT Sum(durationestimate) FROM tracker_publicis_item " & m_strSearch & l_strSQL & ";", g_strExecuteError)
    CheckForSQLError
    
    If IsNull(l_rstTotal(0)) Then
        l_lngVideoTotal = 0
    Else
        l_lngVideoTotal = l_rstTotal(0)
    End If
    
    l_rstTotal.Close

    txtTotalDurationEstimate.Text = l_lngVideoTotal

    Set l_rstTotal = ExecuteSQL("SELECT Sum(gbsent) FROM tracker_publicis_item " & m_strSearch & l_strSQL & ";", g_strExecuteError)
    CheckForSQLError
    
    If IsNull(l_rstTotal(0)) Then
        l_lngVideoTotal = 0
    Else
        l_lngVideoTotal = l_rstTotal(0)
    End If
    
    l_rstTotal.Close

    txtTotalSize.Text = l_lngVideoTotal

    Set l_rstTotal = Nothing
End If

End Sub

Private Sub cmdSomeFields_Click()

grdItems.Columns("externalalphaID").Visible = False
grdItems.Columns("originalexternalalphaID").Visible = False
grdItems.Columns("legacylinestandard").Visible = False
grdItems.Columns("imageaspectratio").Visible = False
grdItems.Columns("language").Visible = False
grdItems.Columns("subtitleslanguage").Visible = False
grdItems.Columns("legacypictureelement").Visible = False
grdItems.Columns("audioconfiguration").Visible = False

End Sub

Private Sub cmdUnbill_Click()

grdItems.Columns("billed").Text = 0
grdItems.Columns("jobid").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False

End Sub

Private Sub cmdUnbillAll_Click()

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then

    While Not adoItems.Recordset.EOF
        adoItems.Recordset("billed") = 0
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
    Wend
End If

grdItems.Refresh

End Sub

Private Sub cmdUpdate_Click()

'If Not adoItems.Recordset.EOF Then
'    m_blnDontVerifyXML = True
'    adoItems.Recordset.MoveFirst
'    While Not adoItems.Recordset.EOF
'        adoItems.Recordset("storagebarcode") = grdItems.Columns("Stored On").Text
'        adoItems.Recordset.Update
'        adoItems.Recordset.MoveNext
'    Wend
'    m_blnDontVerifyXML = False
'End If

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

Dim l_blnWide As Boolean

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/globaltracker%' and companyID > 100 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/globaltracker%' ORDER BY name;"
End If

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

adoComments.ConnectionString = g_strConnection

grdItems.StyleSets("headerfield").BackColor = &HE7FFE7
grdItems.StyleSets("stagefield").BackColor = &HE7FFFF
grdItems.StyleSets("conclusionfield").BackColor = &HFFFFE7

grdItems.StyleSets.Add "Error"
grdItems.StyleSets("Error").BackColor = &HA0A0FF

optComplete(0).Value = True

m_strSearch = " WHERE companyID = 0"
m_strOrderby = " ORDER BY itemreference, title, episode, subtitle"

grdItems.Columns("itemfilename").Visible = True

HideAllColumns

DoEvents

cmdSearch.Value = True
cmdSomeFields.Value = True

End Sub

Private Sub Form_Resize()

On Error Resume Next

grdItems.Width = Me.ScaleWidth - grdItems.Left - 120
grdItems.Height = (Me.ScaleHeight - grdItems.Top - frmButtons.Height) * 0.75 - 240
frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120
grdComments.Top = grdItems.Top + grdItems.Height + 120
grdComments.Height = frmButtons.Top - grdComments.Top - 120
grdComments.Width = grdItems.Width

End Sub

Private Sub grdComments_BeforeUpdate(Cancel As Integer)

grdComments.Columns("tracker_publicis_itemID").Text = lblTrackeritemID.Caption
If grdComments.Columns("cdate").Text = "" Then
    grdComments.Columns("cdate").Text = Now
End If
grdComments.Columns("cuser").Text = g_strFullUserName

End Sub

Private Sub grdComments_BtnClick()

Dim l_strOurEmailContact As String, l_strOurContactName As String, l_strEmailBody As String

Dim l_rstWhoToEmail As ADODB.Recordset

If MsgBox("Send Email?", vbYesNo, "Automatic Email") = vbYes Then
    
    l_strEmailBody = "A comment was created " & _
        "By: " & grdComments.Columns("cuser").Text & vbCrLf & _
        "AlphaID: " & grdItems.Columns("externalalphaID").Text & ", Tape: " & grdItems.Columns("barcode").Text & vbCrLf & _
        "Comment: " & grdComments.Columns("comment").Text & vbCrLf
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND trackermessageID = 13;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "DADC Tracker Comment Created", "", l_strEmailBody, True, "", ""
    
            l_rstWhoToEmail.MoveNext
        Wend
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
    
End If

End Sub

Private Sub grdItems_BeforeUpdate(Cancel As Integer)

Dim temp As Boolean, l_strDuration As String, l_lngRunningTime As Long, l_curFileSize As Currency, l_strFilename As String, l_rst As ADODB.Recordset
Dim l_lngClipID As Long, l_strNetworkPath As String

temp = True

'Check ReadytoBill - setting temp to false if any items are not ready to bill

If temp = True Then
    grdItems.Columns("readytobill").Text = 1
Else
    grdItems.Columns("readytobill").Text = 0
End If

temp = True

'Check the error status - setting temp to false for any items that have error status

If temp = False Then
    grdItems.Columns("rejected").Text = 1
Else
    grdItems.Columns("rejected").Text = 0
End If

grdItems.Columns("companyID").Text = lblCompanyID.Caption

'Get and update the file related items from the reference field.

If grdItems.Columns("itemreference").Text <> "" Then
    grdItems.Columns("storagebarcode").Text = GetData("library", "barcode", "libraryID", GetDataSQL("SELECT libraryID FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND clipformat = 'MOV ProRes';"))
    'see if there is a clip record to get the duration from reference
    l_strDuration = GetDataSQL("SELECT fd_length FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'MOV ProRes';")
    grdItems.Columns("timecodeduration").Text = l_strDuration
    If l_strDuration <> "" Then
        l_lngRunningTime = 60 * Val(Mid(l_strDuration, 1, 2))
        l_lngRunningTime = l_lngRunningTime + Val(Mid(l_strDuration, 4, 2))
        If Val(Mid(l_strDuration, 7, 2)) > 30 Then
            l_lngRunningTime = l_lngRunningTime + 1
        End If
        If l_lngRunningTime = 0 Then l_lngRunningTime = 1
        If (grdItems.Columns("duration").Text = "") Or (grdItems.Columns("duration").Text <> "" And chkLockDur.Value = 0) Then
            grdItems.Columns("duration").Text = l_lngRunningTime
        End If
    End If
    
    'See if there is a size to go in the GB sent column
    l_curFileSize = 0
    Set l_rst = ExecuteSQL("SELECT bigfilesize FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'MOV ProRes';", g_strExecuteError)
    CheckForSQLError
    If l_rst.RecordCount > 0 Then
        l_rst.MoveFirst
        While Not l_rst.EOF
            If Not IsNull(l_rst("bigfilesize")) Then
                If l_rst("bigfilesize") > l_curFileSize Then l_curFileSize = l_rst("bigfilesize")
            End If
            l_rst.MoveNext
        Wend
    End If
    l_rst.Close
    If l_curFileSize <> 0 Then
        If (grdItems.Columns("gbsent").Text = "") Or (grdItems.Columns("gbsent").Text <> "" And chkLockGB.Value = 0) Then
            grdItems.Columns("gbsent").Text = Int(l_curFileSize / 1024 / 1024 / 1024 + 0.999)
        End If
    End If
    
    'See if there is a filename to go in the filename column
    l_strFilename = GetDataSQL("SELECT clipfilename FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'MOV ProRes';")
    If l_strFilename <> "" Then
        grdItems.Columns("itemfilename").Text = l_strFilename
    End If

    'See if there is a proxy ID to go in a ProxyID column
    Set l_rst = ExecuteSQL("SELECT eventID FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Flash MP4' AND cliphorizontalpixels = 640 and libraryID = 276356;", g_strExecuteError)
    CheckForSQLError
    If l_rst.RecordCount > 0 Then
        l_rst.MoveFirst
        grdItems.Columns("proxyID").Text = l_rst("eventID")
    End If
    l_rst.Close
    Set l_rst = Nothing
End If

End Sub

Private Sub grdItems_BtnClick()

Dim tempdate As String

Select Case LCase(grdItems.Columns(grdItems.Col).Name)

Case "barcode"
    
    MakeClipFromDADCTrackerEvent grdItems.Columns("tracker_publicis_itemID").Text

Case Else
    
    If grdItems.ActiveCell.Text <> "" Then
        grdItems.ActiveCell.Text = ""
    Else
        If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
            grdItems.ActiveCell.Text = Now
        Else
            tempdate = FormatDateTime(Now, vbLongDate)
            grdItems.ActiveCell.Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
        End If
    End If

End Select

End Sub

Private Sub grdItems_DblClick()

ShowClipSearch "", grdItems.Columns("itemreference").Text

End Sub

Private Sub grdItems_RowLoaded(ByVal bookmark As Variant)

'Set the error states on the individual columns that can have errors

If grdItems.Columns("rejected").Text <> 0 Then
    grdItems.Columns("status").Text = "PENDING"
    grdItems.Columns("status").CellStyleSet "Error"
End If

If grdItems.Columns("itemreference").Text <> "" Then
    grdItems.Columns("Stored On").Text = GetData("library", "barcode", "libraryID", GetDataSQL("SELECT libraryID FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND clipformat = 'MOV ProRes';"))
End If

'adoComments.RecordSource = "SELECT * FROM tracker_publicis_comment WHERE tracker_publicis_itemID = " & lblTrackeritemID.Caption & " ORDER BY cdate;"
'adoComments.ConnectionString = g_strConnection
'adoComments.Refresh
'
End Sub

Private Sub optComplete_Click(Index As Integer)

cmdSearch.Value = True

End Sub
