VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Begin VB.Form frmContactSearch 
   Caption         =   "Contact Search"
   ClientHeight    =   6450
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10455
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   6450
   ScaleWidth      =   10455
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtCompanyID 
      BackColor       =   &H00FFC0FF&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1200
      TabIndex        =   12
      ToolTipText     =   "The unique identifier for this company"
      Top             =   120
      Width           =   1395
   End
   Begin VB.TextBox txtAddress 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      DataField       =   "address"
      Height          =   855
      Left            =   1200
      MultiLine       =   -1  'True
      TabIndex        =   11
      ToolTipText     =   "Company Address"
      Top             =   840
      Width           =   3315
   End
   Begin VB.TextBox txtPostCode 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      DataField       =   "postcode"
      Height          =   255
      Left            =   1200
      TabIndex        =   10
      ToolTipText     =   "Company Postcode"
      Top             =   1800
      Width           =   2955
   End
   Begin VB.TextBox txtCountry 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      DataField       =   "country"
      Height          =   255
      Left            =   1200
      TabIndex        =   9
      ToolTipText     =   "Company Country"
      Top             =   2160
      Width           =   3315
   End
   Begin VB.TextBox txtTelephone 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      DataField       =   "telephone"
      Height          =   255
      Left            =   1200
      TabIndex        =   8
      ToolTipText     =   "Company Telephone"
      Top             =   2520
      Width           =   3315
   End
   Begin VB.TextBox txtFax 
      BorderStyle     =   0  'None
      DataField       =   "fax"
      Height          =   255
      Left            =   1200
      TabIndex        =   7
      ToolTipText     =   "Company Fax"
      Top             =   2880
      Width           =   3315
   End
   Begin VB.TextBox txtEmail 
      BorderStyle     =   0  'None
      DataField       =   "email"
      Height          =   255
      Left            =   1200
      TabIndex        =   6
      ToolTipText     =   "Company Email Address"
      Top             =   3240
      Width           =   2955
   End
   Begin VB.TextBox txtWebsite 
      BorderStyle     =   0  'None
      DataField       =   "website"
      Height          =   255
      Left            =   1200
      TabIndex        =   5
      ToolTipText     =   "Company Website"
      Top             =   3600
      Width           =   2955
   End
   Begin VB.TextBox txtMarketCode 
      BorderStyle     =   0  'None
      DataField       =   "marketcode"
      Height          =   255
      Left            =   1200
      TabIndex        =   4
      Top             =   3960
      Width           =   3315
   End
   Begin VB.TextBox txtSource 
      BorderStyle     =   0  'None
      DataField       =   "source"
      Height          =   255
      Left            =   1200
      TabIndex        =   3
      Top             =   4320
      Width           =   3315
   End
   Begin VB.TextBox txtTradeCode 
      BorderStyle     =   0  'None
      DataField       =   "tradecode"
      Height          =   255
      Left            =   1200
      TabIndex        =   2
      ToolTipText     =   "Company Trade Code"
      Top             =   4680
      Width           =   3315
   End
   Begin VB.TextBox txtComments 
      BorderStyle     =   0  'None
      DataField       =   "notes1"
      Height          =   1035
      Left            =   1200
      MultiLine       =   -1  'True
      TabIndex        =   1
      ToolTipText     =   "Notes for this Company"
      Top             =   5040
      Width           =   3315
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdSearchResults 
      Height          =   6255
      Left            =   4680
      TabIndex        =   0
      Top             =   120
      Width           =   5655
      _Version        =   196617
      BackColorOdd    =   12648384
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   9975
      _ExtentY        =   11033
      _StockProps     =   79
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Bindings        =   "frmContactSearch.frx":0000
      Height          =   255
      Left            =   1200
      TabIndex        =   13
      ToolTipText     =   "The company name"
      Top             =   480
      Width           =   3315
      BevelWidth      =   0
      DataFieldList   =   "name"
      BevelType       =   0
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   6376
      Columns(0).Caption=   "Name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "Post Code"
      Columns(2).Name =   "postcode"
      Columns(2).DataField=   "postcode"
      Columns(2).FieldLen=   256
      _ExtentX        =   5847
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company ID"
      Height          =   255
      Index           =   0
      Left            =   60
      TabIndex        =   26
      Top             =   120
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Name"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   60
      TabIndex        =   25
      Top             =   480
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Address"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   60
      TabIndex        =   24
      Top             =   840
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Post Code"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   60
      TabIndex        =   23
      Top             =   1800
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Country"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   60
      TabIndex        =   22
      Top             =   2160
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Telephone"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   60
      TabIndex        =   21
      Top             =   2520
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Fax"
      Height          =   255
      Index           =   6
      Left            =   60
      TabIndex        =   20
      Top             =   2880
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Email"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   7
      Left            =   60
      TabIndex        =   19
      Top             =   3240
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Website"
      Height          =   255
      Index           =   8
      Left            =   60
      TabIndex        =   18
      Top             =   3600
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Market Code"
      Height          =   255
      Index           =   9
      Left            =   60
      TabIndex        =   17
      Top             =   3960
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Source"
      Height          =   255
      Index           =   10
      Left            =   60
      TabIndex        =   16
      Top             =   4320
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Trade Code"
      Height          =   255
      Index           =   11
      Left            =   60
      TabIndex        =   15
      Top             =   4680
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Comments"
      Height          =   255
      Index           =   12
      Left            =   60
      TabIndex        =   14
      Top             =   5040
      Width           =   1035
   End
End
Attribute VB_Name = "frmContactSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
