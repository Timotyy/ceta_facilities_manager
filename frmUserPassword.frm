VERSION 5.00
Begin VB.Form frmUserPassword 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Please re-enter your password"
   ClientHeight    =   1650
   ClientLeft      =   5055
   ClientTop       =   3465
   ClientWidth     =   4755
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1650
   ScaleWidth      =   4755
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   315
      Left            =   2700
      TabIndex        =   2
      ToolTipText     =   "Close form"
      Top             =   1200
      Width           =   1155
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      Default         =   -1  'True
      Height          =   315
      Left            =   1440
      TabIndex        =   1
      ToolTipText     =   "Confirm password"
      Top             =   1200
      Width           =   1155
   End
   Begin VB.TextBox txtPassword 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   840
      PasswordChar    =   "*"
      TabIndex        =   0
      ToolTipText     =   "Enter password"
      Top             =   720
      Width           =   3015
   End
   Begin VB.Label lblInfo 
      Alignment       =   2  'Center
      Caption         =   "Please re-enter your password for security purposes."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   840
      TabIndex        =   3
      Top             =   180
      Width           =   3015
   End
End
Attribute VB_Name = "frmUserPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Label1_Click()

End Sub


Private Sub cmdCancel_Click()

Me.Tag = ""
Me.Hide

End Sub


Private Sub cmdOK_Click()

Me.Tag = txtPassword.Text
Me.Hide

End Sub


