VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmSundry 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Sundry Cost Information"
   ClientHeight    =   7065
   ClientLeft      =   3015
   ClientTop       =   2460
   ClientWidth     =   4500
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7065
   ScaleWidth      =   4500
   Begin VB.CommandButton cmdAuthoriseSundry 
      Caption         =   "Au&thorise"
      Height          =   315
      Left            =   1980
      TabIndex        =   14
      ToolTipText     =   "Authorise this sundry cost. This function should be done by ""Our Contact"" for this job"
      Top             =   6660
      Width           =   1155
   End
   Begin VB.CommandButton cmdApprove 
      Caption         =   "Appro&ve"
      Height          =   315
      Left            =   3240
      TabIndex        =   15
      ToolTipText     =   "Approve this sundry. This function should be done by the runners management"
      Top             =   6660
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adoOverheads 
      Height          =   330
      Left            =   1200
      Top             =   6360
      Visible         =   0   'False
      Width           =   1200
      _ExtentX        =   2117
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoOverheads"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbOverheads 
      Bindings        =   "frmSundry.frx":0000
      Height          =   315
      Left            =   1380
      TabIndex        =   11
      Top             =   5580
      Width           =   2895
      DataFieldList   =   "description"
      _Version        =   196617
      BackColorOdd    =   16777215
      Columns(0).Width=   3200
      _ExtentX        =   5106
      _ExtentY        =   556
      _StockProps     =   93
      ForeColor       =   -2147483640
      BackColor       =   65535
      DataFieldToDisplay=   "description"
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Delete"
      Height          =   315
      Left            =   120
      TabIndex        =   37
      ToolTipText     =   "Delete this sundry cost"
      Top             =   6120
      Width           =   1215
   End
   Begin VB.CheckBox chkPersonal 
      Alignment       =   1  'Right Justify
      Caption         =   "Personal?"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   195
      Left            =   3180
      TabIndex        =   33
      Top             =   1680
      Width           =   1215
   End
   Begin VB.ComboBox cmbRequestedBy 
      Height          =   315
      Left            =   1380
      TabIndex        =   2
      Text            =   "Client"
      Top             =   2700
      Width           =   3015
   End
   Begin VB.ComboBox ddnSundryEnteredBy 
      Height          =   315
      Left            =   1380
      TabIndex        =   3
      Top             =   3060
      Width           =   3015
   End
   Begin VB.TextBox txtSundryAmount 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0000FFFF&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1380
      TabIndex        =   0
      Top             =   1620
      Width           =   1395
   End
   Begin VB.TextBox txtSundryComments 
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   1380
      TabIndex        =   1
      Top             =   1980
      Width           =   3015
   End
   Begin VB.ComboBox cmbSundryTimeOrdered 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   3480
      TabIndex        =   41
      Text            =   "09:30"
      Top             =   900
      Width           =   915
   End
   Begin VB.ComboBox cmbSundryTimeWanted 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   3480
      TabIndex        =   43
      Text            =   "09:30"
      Top             =   1260
      Width           =   915
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   315
      Left            =   3240
      TabIndex        =   13
      ToolTipText     =   "Cancel and changes made to this sundry and close this form"
      Top             =   6120
      Width           =   1155
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      Height          =   315
      Left            =   1980
      TabIndex        =   12
      ToolTipText     =   "Save changes to this sundry"
      Top             =   6120
      Width           =   1155
   End
   Begin TabDlg.SSTab tabSundry 
      Height          =   2055
      Left            =   120
      TabIndex        =   16
      Top             =   3420
      Width           =   4275
      _ExtentX        =   7541
      _ExtentY        =   3625
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Taxi"
      TabPicture(0)   =   "frmSundry.frx":001B
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "lblCaption(70)"
      Tab(0).Control(1)=   "lblCaption(69)"
      Tab(0).Control(2)=   "lblCaption(68)"
      Tab(0).Control(3)=   "lblCaption(67)"
      Tab(0).Control(4)=   "lblCaption(66)"
      Tab(0).Control(5)=   "txtSundryDestination"
      Tab(0).Control(6)=   "txtSundryLocation"
      Tab(0).Control(7)=   "txtSundryPassenger"
      Tab(0).Control(8)=   "txtSundryAccountNumber"
      Tab(0).Control(9)=   "txtSundryPickupTime"
      Tab(0).ControlCount=   10
      TabCaption(1)   =   "Food / Client Costs"
      TabPicture(1)   =   "frmSundry.frx":0037
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "lblCaption(76)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "lblCaption(77)"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "txtSundryDetails"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "txtSundrySupplier"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).ControlCount=   4
      Begin VB.TextBox txtSundryPickupTime 
         BackColor       =   &H0080FF80&
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   -73740
         TabIndex        =   4
         Top             =   480
         Width           =   2895
      End
      Begin VB.TextBox txtSundryAccountNumber 
         BackColor       =   &H0080FF80&
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   -73740
         TabIndex        =   5
         Top             =   780
         Width           =   2895
      End
      Begin VB.TextBox txtSundryPassenger 
         BackColor       =   &H0080FF80&
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   -73740
         TabIndex        =   6
         Top             =   1080
         Width           =   2895
      End
      Begin VB.TextBox txtSundryLocation 
         BackColor       =   &H0080FF80&
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   -73740
         TabIndex        =   7
         Top             =   1380
         Width           =   2895
      End
      Begin VB.TextBox txtSundryDestination 
         BackColor       =   &H0080FF80&
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   -73740
         TabIndex        =   8
         Top             =   1680
         Width           =   2895
      End
      Begin VB.TextBox txtSundrySupplier 
         BackColor       =   &H0080FF80&
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   1260
         TabIndex        =   9
         Top             =   480
         Width           =   2895
      End
      Begin VB.TextBox txtSundryDetails 
         BackColor       =   &H0080FF80&
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   1155
         Left            =   1260
         MultiLine       =   -1  'True
         TabIndex        =   10
         Top             =   780
         Width           =   2895
      End
      Begin VB.Label lblCaption 
         Caption         =   "Pick Up Time"
         Height          =   255
         Index           =   66
         Left            =   -74880
         TabIndex        =   23
         Top             =   480
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "A/C Number"
         Height          =   255
         Index           =   67
         Left            =   -74880
         TabIndex        =   22
         Top             =   780
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Passenger(s)"
         Height          =   255
         Index           =   68
         Left            =   -74880
         TabIndex        =   21
         Top             =   1080
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Location"
         Height          =   255
         Index           =   69
         Left            =   -74880
         TabIndex        =   20
         Top             =   1380
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Destination"
         Height          =   255
         Index           =   70
         Left            =   -74880
         TabIndex        =   19
         Top             =   1680
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Supplier"
         Height          =   255
         Index           =   77
         Left            =   120
         TabIndex        =   18
         Top             =   480
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Details"
         Height          =   255
         Index           =   76
         Left            =   120
         TabIndex        =   17
         Top             =   780
         Width           =   1035
      End
   End
   Begin MSComCtl2.DTPicker datSundryDateOrdered 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   285
      Left            =   1380
      TabIndex        =   40
      Top             =   900
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   503
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   20709377
      CurrentDate     =   37870
   End
   Begin MSComCtl2.DTPicker datSundryDateWanted 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   285
      Left            =   1380
      TabIndex        =   42
      Top             =   1260
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   503
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   20709377
      CurrentDate     =   37870
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbJobID 
      Height          =   255
      Left            =   1380
      TabIndex        =   39
      ToolTipText     =   "The Job ID that this cost relates to. Very important for tracking costs and making sure the client is billed correctly."
      Top             =   420
      Width           =   3015
      BevelWidth      =   0
      DataFieldList   =   "job id"
      BevelType       =   0
      _Version        =   196617
      BorderStyle     =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   5318
      _ExtentY        =   450
      _StockProps     =   93
      ForeColor       =   -2147483640
      BackColor       =   16761024
      DataFieldToDisplay=   "job id"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbProject 
      Height          =   255
      Left            =   1380
      TabIndex        =   38
      ToolTipText     =   "The name of this project. In the previous version of CETA, this was known as the 'Special Job Number'"
      Top             =   60
      Width           =   3015
      BevelWidth      =   0
      DataFieldList   =   "projectnumber"
      BevelType       =   0
      _Version        =   196617
      BorderStyle     =   0
      BeveColorScheme =   1
      CheckBox3D      =   0   'False
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   12648447
      RowHeight       =   423
      Columns.Count   =   9
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "projectID"
      Columns(0).Name =   "projectID"
      Columns(0).DataField=   "projectID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   2566
      Columns(1).Caption=   "Project Number"
      Columns(1).Name =   "name"
      Columns(1).DataField=   "projectnumber"
      Columns(1).FieldLen=   256
      Columns(2).Width=   4604
      Columns(2).Caption=   "Product"
      Columns(2).Name =   "productname"
      Columns(2).DataField=   "productname"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3757
      Columns(3).Caption=   "Company"
      Columns(3).Name =   "companyname"
      Columns(3).DataField=   "companyname"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "Contact"
      Columns(4).Name =   "contactname"
      Columns(4).DataField=   "contactname"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "Status"
      Columns(5).Name =   "status"
      Columns(5).DataField=   "fd_status"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "productID"
      Columns(6).Name =   "productID"
      Columns(6).DataField=   "productID"
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "companyID"
      Columns(7).Name =   "companyID"
      Columns(7).DataField=   "companyID"
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "contactID"
      Columns(8).Name =   "contactID"
      Columns(8).DataField=   "contactID"
      Columns(8).FieldLen=   256
      _ExtentX        =   5318
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   12648447
      DataFieldToDisplay=   "projectnumber"
   End
   Begin VB.Line Line2 
      X1              =   120
      X2              =   4320
      Y1              =   780
      Y2              =   780
   End
   Begin VB.Label lblCaption 
      Caption         =   "Project #"
      Height          =   255
      Index           =   33
      Left            =   120
      TabIndex        =   36
      Top             =   60
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job ID"
      Height          =   255
      Index           =   16
      Left            =   120
      TabIndex        =   35
      Top             =   420
      Width           =   1035
   End
   Begin VB.Line Line1 
      Index           =   1
      X1              =   120
      X2              =   4380
      Y1              =   6540
      Y2              =   6540
   End
   Begin VB.Line Line1 
      Index           =   0
      X1              =   120
      X2              =   4380
      Y1              =   6000
      Y2              =   6000
   End
   Begin VB.Label lblCaption 
      Caption         =   "Overhead Code"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   34
      Top             =   5580
      Width           =   1155
   End
   Begin VB.Label lblSundryID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   32
      Top             =   6720
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Entered By"
      Height          =   255
      Index           =   75
      Left            =   120
      TabIndex        =   31
      Top             =   3060
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Date Wanted"
      Height          =   255
      Index           =   65
      Left            =   120
      TabIndex        =   30
      Top             =   1260
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Date Ordered"
      Height          =   255
      Index           =   64
      Left            =   120
      TabIndex        =   29
      Top             =   900
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Amount �"
      Height          =   255
      Index           =   81
      Left            =   120
      TabIndex        =   28
      Top             =   1620
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Comments"
      Height          =   255
      Index           =   78
      Left            =   120
      TabIndex        =   27
      Top             =   1980
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Time"
      Height          =   285
      Index           =   71
      Left            =   2940
      TabIndex        =   26
      Top             =   900
      Width           =   495
   End
   Begin VB.Label lblCaption 
      Caption         =   "Time"
      Height          =   285
      Index           =   73
      Left            =   2940
      TabIndex        =   25
      Top             =   1260
      Width           =   495
   End
   Begin VB.Label lblCaption 
      Caption         =   "Requested By"
      Height          =   255
      Index           =   72
      Left            =   120
      TabIndex        =   24
      Top             =   2700
      Width           =   1035
   End
End
Attribute VB_Name = "frmSundry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub cmbJobID_DropDown()
If cmbJobID.Tag = "STOP" Then Exit Sub

Dim l_strSQL As String
l_strSQL = "SELECT jobID AS 'Job ID', jobtype AS 'Job Type', companyname AS 'Company', startdate AS 'Start Date', productname as 'Product', title1 AS 'Title', fd_status AS 'Status' FROM job "
l_strSQL = l_strSQL & " WHERE fd_status <> 'Cancelled' "
l_strSQL = l_strSQL & " AND fd_status <> 'Cancel' "
l_strSQL = l_strSQL & " AND fd_status <> 'Unavavil' "
l_strSQL = l_strSQL & " AND fd_status <> 'Unavailable' "
l_strSQL = l_strSQL & " AND fd_status <> 'Quick' "
l_strSQL = l_strSQL & " AND fd_status <> 'Quickie' "
l_strSQL = l_strSQL & " AND fd_status <> 'aMeeting' "
l_strSQL = l_strSQL & " AND fd_status <> 'Meeting' "
l_strSQL = l_strSQL & " AND fd_status <> 'Pencil' "
l_strSQL = l_strSQL & " AND fd_status <> 'No charge' "
l_strSQL = l_strSQL & " AND fd_status <> 'Costed' "
l_strSQL = l_strSQL & " AND fd_status <> 'Cancel' "
l_strSQL = l_strSQL & " AND fd_status <> 'Sent To Accounts' "
l_strSQL = l_strSQL & " AND projectnumber = '" & cmbProject.Text & "' ORDER BY startdate;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbJobID.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

cmbJobID.Columns(0).Width = 800
cmbJobID.Columns(1).Width = 800
cmbJobID.Columns(2).Width = 1800
cmbJobID.Columns(3).Width = 1000
cmbJobID.Columns(4).Width = 2000
cmbJobID.Columns(5).Width = 2000
cmbJobID.Columns(6).Width = 1000

End Sub

Private Sub cmbOverheads_GotFocus()
HighLite cmbOverheads
End Sub

Private Sub cmbProject_DropDown()
If cmbProject.Tag = "STOP" Then Exit Sub

Dim l_strSQL As String
l_strSQL = "SELECT project.projectnumber, project.projectID, project.companyname, project.contactname, project.fd_status, product.name as productname, product.productID, project.companyID, project.contactID FROM project LEFT JOIN product ON project.productID = product.productID WHERE (projectnumber LIKE '" & QuoteSanitise(cmbProject.Text) & "%') AND (project.fd_status IS NULL OR (project.fd_status <> 'COMPLETED' AND project.fd_status <> 'CANCEL' AND project.fd_status <> 'CANCELLED'  AND project.fd_status <> 'SENT TO ACCOUNTS' AND project.fd_status <> 'INVOICED')) ORDER BY projectnumber;"
Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbProject.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmbRequestedBy_GotFocus()
HighLite cmbRequestedBy
End Sub

Private Sub cmbRequestedBy_KeyPress(KeyAscii As Integer)
AutoMatch cmbRequestedBy, KeyAscii
End Sub

Private Sub cmdApprove_Click()

If Val(lblSundryID.Caption) = 0 Then Exit Sub

If CheckAccess("/approvesundry") Then
    
    With frmSundry
        SaveSundry Val(.lblSundryID.Caption), IIf(.tabSundry.Caption = "Food", "Food", "Taxi"), -1, -1, .datSundryDateOrdered.Value & " " & .cmbSundryTimeOrdered.Text, .datSundryDateWanted & " " & .cmbSundryTimeWanted, .txtSundryPickupTime.Text, .txtSundryAccountNumber.Text, .txtSundryPassenger.Text, _
        .txtSundryLocation.Text, .txtSundryDestination.Text, .txtSundryComments.Text, .cmbRequestedBy.Text, .ddnSundryEnteredBy.Text, .txtSundryDetails.Text, .txtSundrySupplier.Text, Val(.txtSundryAmount.Text), .chkPersonal.Value, .cmbOverheads.Text
    End With
    
    ApproveSundry Val(lblSundryID.Caption)
    
    Dim l_lngBookmark As Long
    l_lngBookmark = frmSundryManagement.grdSundryCosts.Bookmark
    
    frmSundryManagement.adoSundry.Refresh
    
    frmSundryManagement.grdSundryCosts.Bookmark = l_lngBookmark
    
    If IsOverHeadCode(cmbProject.Text) Then Me.Hide
    
End If

End Sub

Private Sub cmdAuthoriseSundry_Click()

If Val(lblSundryID.Caption) = 0 Then Exit Sub

If CheckAccess("/authorisesundry") Then
    
    With frmSundry
        SaveSundry Val(.lblSundryID.Caption), IIf(.tabSundry.Caption = "Food", "Food", "Taxi"), -1, -1, .datSundryDateOrdered.Value & " " & .cmbSundryTimeOrdered.Text, .datSundryDateWanted & " " & .cmbSundryTimeWanted, .txtSundryPickupTime.Text, .txtSundryAccountNumber.Text, .txtSundryPassenger.Text, _
        .txtSundryLocation.Text, .txtSundryDestination.Text, .txtSundryComments.Text, .cmbRequestedBy.Text, .ddnSundryEnteredBy.Text, .txtSundryDetails.Text, .txtSundrySupplier.Text, Val(.txtSundryAmount.Text), .chkPersonal.Value, .cmbOverheads.Text
    End With
    
    AuthoriseSundry Val(lblSundryID.Caption)
    
    Dim l_lngBookmark As Long
    
    l_lngBookmark = frmSundryManagement.grdSundryCosts.Bookmark
    
    frmSundryManagement.adoSundry.Refresh
    
    frmSundryManagement.grdSundryCosts.Bookmark = l_lngBookmark
    
End If

End Sub


Private Sub cmdCancel_Click()
Me.Tag = "CANCELLED"
Me.Hide

End Sub

Private Sub cmdDelete_Click()

If Not CheckAccess("/deletesundry") Then Exit Sub

If IsDate(GetData("sundrycost", "approveddate", "sundrycostID", lblSundryID.Caption)) Then
    MsgBox "You can not delete this sundry cost, as it has already been approved", vbExclamation
    Exit Sub
End If

Dim l_intMsg As Integer
l_intMsg = MsgBox("Are you sure you want to delete this sundry cost?", vbYesNo + vbQuestion)

If l_intMsg = vbYes Then
    Dim l_strSQL As String
    l_strSQL = "DELETE FROM sundrycost WHERE sundrycostID = '" & lblSundryID.Caption & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
End If

Me.Hide

End Sub

Private Sub cmdOK_Click()

Dim l_intMsg As Integer

If Val(cmbProject.Text) = 0 Then
    MsgBox "You must have a project number before adding sundry costs", vbExclamation
    Exit Sub
End If


'dont bother requesting a job ID for non-project based jobs
If Not IsOverHeadCode(cmbProject.Text) Then
    If Val(cmbJobID.Text) = 0 Then
        l_intMsg = MsgBox("You have not entered a jobID. Are you sure you want to add a sundry cost without one?", vbYesNo + vbQuestion)
        If l_intMsg = vbNo Then Exit Sub
    End If
End If

Me.Hide

End Sub

Private Sub ddnSundryEnteredBy_GotFocus()
HighLite ddnSundryEnteredBy
End Sub

Private Sub ddnSundryEnteredBy_KeyPress(KeyAscii As Integer)
AutoMatch ddnSundryEnteredBy, KeyAscii
End Sub

Private Sub Form_Load()

PopulateCombo "times", cmbSundryTimeOrdered
PopulateCombo "times", cmbSundryTimeWanted
PopulateCombo "deliveredby", ddnSundryEnteredBy
cmbRequestedBy.AddItem "Client"
PopulateCombo "users", cmbRequestedBy


Dim l_strSQL As String
l_strSQL = "SELECT description, information FROM xref WHERE category = 'overheads-food' ORDER BY description"

adoOverheads.ConnectionString = g_strConnection
adoOverheads.RecordSource = l_strSQL
adoOverheads.Refresh


CenterForm Me
End Sub

Private Sub tabSundry_Click(PreviousTab As Integer)

Dim l_strCategory As String
If tabSundry.Caption <> "Taxi" Then
    l_strCategory = "overheads-food"
Else
    l_strCategory = "overheads-taxi"
End If


Dim l_strSQL As String
l_strSQL = "SELECT description, information FROM xref WHERE category = '" & l_strCategory & "' ORDER BY description"

adoOverheads.ConnectionString = g_strConnection
adoOverheads.RecordSource = l_strSQL
adoOverheads.Refresh


End Sub

Private Sub txtSundryAccountNumber_GotFocus()
HighLite txtSundryAccountNumber
End Sub

Private Sub txtSundryAmount_GotFocus()
HighLite txtSundryAmount
End Sub

Private Sub txtSundryComments_GotFocus()
HighLite txtSundryComments
End Sub

Private Sub txtSundryDestination_GotFocus()
HighLite txtSundryDestination
End Sub

Private Sub txtSundryDetails_GotFocus()
HighLite txtSundryDetails
End Sub

Private Sub txtSundryLocation_GotFocus()
HighLite txtSundryLocation
End Sub

Private Sub txtSundryPassenger_GotFocus()
HighLite txtSundryPassenger
End Sub

Private Sub txtSundryPickupTime_GotFocus()
HighLite txtSundryPickupTime
End Sub

Private Sub txtSundrySupplier_GotFocus()
HighLite txtSundrySupplier
End Sub
