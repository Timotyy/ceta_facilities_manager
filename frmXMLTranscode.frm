VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmXMLTranscode 
   Caption         =   "Submit XML Transcode"
   ClientHeight    =   6435
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9735
   Icon            =   "frmXMLTranscode.frx":0000
   LinkTopic       =   "Excel Orders"
   ScaleHeight     =   6435
   ScaleWidth      =   9735
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.CheckBox chkMOConvert 
      Caption         =   "MO Convert"
      Height          =   255
      Left            =   240
      TabIndex        =   54
      Top             =   7860
      Width           =   1695
   End
   Begin VB.CheckBox chkProgramOnly 
      Caption         =   "ProgramOnly"
      Height          =   255
      Left            =   240
      TabIndex        =   53
      Top             =   7560
      Width           =   1695
   End
   Begin VB.TextBox txtStartTimecodeOverride 
      Height          =   315
      Left            =   6300
      TabIndex        =   52
      ToolTipText     =   "End timecode"
      Top             =   4380
      Width           =   1395
   End
   Begin VB.CheckBox chkFutureRequest 
      Caption         =   "Future Request"
      Height          =   255
      Left            =   2280
      TabIndex        =   51
      Top             =   5460
      Width           =   1755
   End
   Begin VB.TextBox txtPatheFileID 
      Height          =   315
      Left            =   3240
      TabIndex        =   49
      ToolTipText     =   "Start timecode"
      Top             =   780
      Width           =   6255
   End
   Begin VB.CheckBox chkPatheStills 
      Caption         =   "Pathe - Use customfield1 for new Reference"
      Height          =   255
      Left            =   4380
      TabIndex        =   48
      Top             =   5460
      Width           =   3735
   End
   Begin VB.TextBox txtJobID 
      Height          =   315
      Left            =   3240
      TabIndex        =   46
      ToolTipText     =   "Start timecode"
      Top             =   1860
      Width           =   2175
   End
   Begin VB.TextBox txtTextToBurnInOpacity 
      Height          =   315
      Left            =   3240
      TabIndex        =   42
      ToolTipText     =   "Start timecode"
      Top             =   2580
      Width           =   2175
   End
   Begin VB.TextBox txtTextToBurnIn 
      Height          =   315
      Left            =   3240
      TabIndex        =   40
      ToolTipText     =   "Start timecode"
      Top             =   2220
      Width           =   6255
   End
   Begin VB.CheckBox chkSkipLogging 
      Caption         =   "Skip Logging"
      Height          =   255
      Left            =   3540
      TabIndex        =   38
      Top             =   7260
      Width           =   1695
   End
   Begin VB.CheckBox chkTimecodesFromCaller 
      Caption         =   "Timecodes From Caller"
      Height          =   255
      Left            =   7140
      TabIndex        =   37
      Top             =   60
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.CheckBox chkKidscoBulkTranscoding 
      Caption         =   "Kidsco Proxy Bulk Transcoding"
      Height          =   255
      Left            =   240
      TabIndex        =   28
      Top             =   7260
      Width           =   2595
   End
   Begin VB.CheckBox chkFilenameFromFile 
      Caption         =   "Set the Filename from the Original File"
      Height          =   255
      Left            =   4380
      TabIndex        =   27
      Top             =   5160
      Width           =   3315
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbTrackerLink 
      Height          =   315
      Left            =   3240
      TabIndex        =   24
      Top             =   1500
      Width           =   6255
      DataFieldList   =   "itemheading"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Caption=   "itemfield"
      Columns(0).Name =   "itemfield"
      Columns(0).DataField=   "itemfield"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "itemheading"
      Columns(1).Name =   "itemheading"
      Columns(1).DataField=   "itemheading"
      Columns(1).FieldLen=   256
      _ExtentX        =   11033
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   14737632
      DataFieldToDisplay=   "itemheading"
   End
   Begin VB.TextBox txtClipReference 
      Height          =   315
      Left            =   3240
      TabIndex        =   23
      ToolTipText     =   "Start timecode"
      Top             =   420
      Width           =   6255
   End
   Begin VB.TextBox txtTimecodestart 
      Height          =   315
      Left            =   6300
      TabIndex        =   19
      ToolTipText     =   "Start timecode"
      Top             =   3660
      Width           =   1395
   End
   Begin VB.TextBox txtTimecodestop 
      Height          =   315
      Left            =   6300
      TabIndex        =   21
      ToolTipText     =   "End timecode"
      Top             =   4020
      Width           =   1395
   End
   Begin VB.CheckBox chkBulkTranscoding 
      Caption         =   "Bulk Transcoding"
      Height          =   255
      Left            =   240
      TabIndex        =   18
      Top             =   6960
      Width           =   1695
   End
   Begin VB.Frame Frame1 
      Height          =   615
      Left            =   2160
      TabIndex        =   15
      Top             =   4200
      Width           =   1935
      Begin VB.OptionButton optHidden 
         Caption         =   "Visible"
         Height          =   255
         Index           =   1
         Left            =   1020
         TabIndex        =   17
         Top             =   240
         Width           =   795
      End
      Begin VB.OptionButton optHidden 
         Caption         =   "Hidden"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   1035
      End
   End
   Begin VB.Frame frmTimecode 
      Caption         =   "Select Timecode Type"
      Height          =   2715
      Left            =   300
      TabIndex        =   10
      Top             =   3300
      Width           =   1875
      Begin VB.OptionButton optTimecodeType 
         Caption         =   "29.97 fps NDF"
         Height          =   255
         Index           =   7
         Left            =   180
         TabIndex        =   55
         Top             =   1200
         Width           =   1635
      End
      Begin VB.OptionButton optTimecodeType 
         Caption         =   "60 fps"
         Height          =   255
         Index           =   6
         Left            =   180
         TabIndex        =   32
         Top             =   2400
         Width           =   1095
      End
      Begin VB.OptionButton optTimecodeType 
         Caption         =   "59.94 fps"
         Height          =   255
         Index           =   5
         Left            =   180
         TabIndex        =   31
         Top             =   2100
         Width           =   1095
      End
      Begin VB.OptionButton optTimecodeType 
         Caption         =   "50 fps"
         Height          =   255
         Index           =   4
         Left            =   180
         TabIndex        =   30
         Top             =   1800
         Width           =   1095
      End
      Begin VB.OptionButton optTimecodeType 
         Caption         =   "25 fps"
         Height          =   255
         Index           =   0
         Left            =   180
         TabIndex        =   14
         Top             =   600
         Value           =   -1  'True
         Width           =   1215
      End
      Begin VB.OptionButton optTimecodeType 
         Caption         =   "29.97 fps"
         Height          =   255
         Index           =   1
         Left            =   180
         TabIndex        =   13
         Top             =   900
         Width           =   1215
      End
      Begin VB.OptionButton optTimecodeType 
         Caption         =   "30 fps"
         Height          =   255
         Index           =   2
         Left            =   180
         TabIndex        =   12
         Top             =   1500
         Width           =   1095
      End
      Begin VB.OptionButton optTimecodeType 
         Caption         =   "24 fps"
         Height          =   255
         Index           =   3
         Left            =   180
         TabIndex        =   11
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.CheckBox chkOverWrite 
      Caption         =   "Overwrite if already exists"
      Height          =   255
      Left            =   4380
      TabIndex        =   9
      Top             =   4860
      Width           =   2295
   End
   Begin VB.OptionButton optVideoAudio 
      Caption         =   "Audio Only"
      Height          =   255
      Index           =   2
      Left            =   2280
      TabIndex        =   8
      Top             =   3900
      Width           =   1155
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Submit Transcode"
      Height          =   315
      Left            =   4080
      TabIndex        =   7
      Top             =   3300
      Width           =   2175
   End
   Begin VB.OptionButton optVideoAudio 
      Caption         =   "Video Only"
      Height          =   255
      Index           =   1
      Left            =   2280
      TabIndex        =   4
      Top             =   3600
      Width           =   1155
   End
   Begin VB.OptionButton optVideoAudio 
      Caption         =   "Video + Audio"
      Height          =   255
      Index           =   0
      Left            =   2280
      TabIndex        =   3
      Top             =   3300
      Width           =   1395
   End
   Begin VB.Timer timFormClose 
      Interval        =   20000
      Left            =   8580
      Top             =   5460
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   6300
      TabIndex        =   1
      Top             =   3300
      Width           =   1395
   End
   Begin MSAdodcLib.Adodc adoAudioFile1 
      Height          =   330
      Left            =   120
      Top             =   6600
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoAudioFile1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbTranscodeSpec 
      Bindings        =   "frmXMLTranscode.frx":0BC2
      Height          =   315
      Left            =   3240
      TabIndex        =   6
      Top             =   1140
      Width           =   6255
      DataFieldList   =   "transcodename"
      MaxDropDownItems=   24
      ListWidth       =   11033
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Transcode Spec ID"
      Columns(0).Name =   "transcodespecID"
      Columns(0).DataField=   "transcodespecID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   9313
      Columns(1).Caption=   "Transcode Name"
      Columns(1).Name =   "transcodename"
      Columns(1).DataField=   "transcodename"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "skiplogging"
      Columns(2).Name =   "skiplogging"
      Columns(2).DataField=   "skiplogging"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "transcodesystem"
      Columns(3).Name =   "transcodesystem"
      Columns(3).DataField=   "transcodesystem"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "NexGuard"
      Columns(4).Name =   "NexGuard"
      Columns(4).DataField=   "NexGuard"
      Columns(4).FieldLen=   256
      _ExtentX        =   11033
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "transcodename"
   End
   Begin MSAdodcLib.Adodc adoTranscodeSpecs 
      Height          =   330
      Left            =   3420
      Top             =   6600
      Visible         =   0   'False
      Width           =   2385
      _ExtentX        =   4207
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoTranscodeSpecs"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label lblCaption 
      Caption         =   "Pathe File.ID"
      Height          =   255
      Index           =   12
      Left            =   300
      TabIndex        =   50
      Top             =   840
      Width           =   2535
   End
   Begin VB.Label lblCaption 
      Caption         =   "Transcode JobID (For NexGuard Jobs)"
      Height          =   255
      Index           =   11
      Left            =   300
      TabIndex        =   47
      Top             =   1920
      Width           =   2835
   End
   Begin VB.Label lblSourceFormat 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   7080
      TabIndex        =   45
      Top             =   7200
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "0 is transparent, 1 is opaque"
      Height          =   255
      Index           =   10
      Left            =   5520
      TabIndex        =   44
      Top             =   2640
      Width           =   2235
   End
   Begin VB.Label lblCaption 
      Caption         =   "Opacity to burn in (FFMPG System Only)"
      Height          =   255
      Index           =   9
      Left            =   300
      TabIndex        =   43
      Top             =   2640
      Width           =   2895
   End
   Begin VB.Label lblCaption 
      Caption         =   "Text to burn in (FFMPG System Only)"
      Height          =   255
      Index           =   8
      Left            =   300
      TabIndex        =   41
      Top             =   2280
      Width           =   2895
   End
   Begin VB.Label lblCaption 
      Caption         =   "Start Timecode Override"
      Height          =   255
      Index           =   7
      Left            =   4140
      TabIndex        =   39
      Top             =   4440
      Width           =   1875
   End
   Begin VB.Label lblTimecodeStop 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   6300
      TabIndex        =   36
      Top             =   5760
      Width           =   1395
   End
   Begin VB.Label Label1 
      Caption         =   "Use Clip timecodes, not zero ascending for Start and Stop time"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   2160
      TabIndex        =   35
      Top             =   6180
      Width           =   5535
   End
   Begin VB.Label lblCaption 
      Caption         =   "Timecodes of Master File"
      Height          =   255
      Index           =   6
      Left            =   2700
      TabIndex        =   34
      Top             =   5820
      Width           =   2115
   End
   Begin VB.Label lblTimecodeStart 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4860
      TabIndex        =   33
      Top             =   5760
      Width           =   1395
   End
   Begin VB.Label lblCurrentFilename 
      Appearance      =   0  'Flat
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   300
      TabIndex        =   29
      Tag             =   "CLEARFIELDS"
      Top             =   3000
      Width           =   9255
   End
   Begin VB.Label lblCaption 
      Caption         =   "New File Ref"
      Height          =   255
      Index           =   5
      Left            =   300
      TabIndex        =   26
      Top             =   480
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Tracker Link"
      Height          =   255
      Index           =   4
      Left            =   300
      TabIndex        =   25
      Top             =   1560
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Start Time (Clip Timecodes)"
      Height          =   255
      Index           =   19
      Left            =   4140
      TabIndex        =   22
      Top             =   3720
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Stop Time (Clip Timecodes)"
      Height          =   255
      Index           =   20
      Left            =   4140
      TabIndex        =   20
      Top             =   4080
      Width           =   2115
   End
   Begin VB.Label lblVideoFile 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   3240
      TabIndex        =   5
      Top             =   60
      Width           =   2175
   End
   Begin VB.Label lblCaption 
      Caption         =   "Transcode Spec"
      Height          =   255
      Index           =   2
      Left            =   300
      TabIndex        =   2
      Top             =   1200
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Video File ID"
      Height          =   255
      Index           =   0
      Left            =   300
      TabIndex        =   0
      Top             =   120
      Width           =   1155
   End
End
Attribute VB_Name = "frmXMLTranscode"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_Framerate As Integer

Private Sub Check1_Click()

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdSave_Click()

Set_Framerate

Dim SQL As String, l_strFPS As String, l_lngTranscoderequestID As Long, l_rstTemp As ADODB.Recordset, l_rstTemp2 As ADODB.Recordset, SQL2 As String, l_lngAudioID As Long, Count As Long
Dim l_rstUsers As ADODB.Recordset, l_strSQL As String

If optTimecodeType(0).Value = True Then
    l_strFPS = "25"
ElseIf optTimecodeType(1).Value = True Then
    l_strFPS = "29.97"
ElseIf optTimecodeType(2).Value = True Then
    l_strFPS = "30"
ElseIf optTimecodeType(3).Value = True Then
    l_strFPS = "24"
End If

If cmbTranscodeSpec.Text <> "" Then
    
    If chkBulkTranscoding.Value <> 0 Then
    
        If cmbTranscodeSpec.Columns("NexGuard").Text <> 0 Then
            If txtJobID.Text = "" Then
                MsgBox "Cannot do a NexgGaurd Based Transcode without the JobID"
                Exit Sub
            End If
            l_strSQL = "SELECT * FROM jobcontact WHERE jobID = " & Val(txtJobID.Text) & " AND CCnotification = 0;"
            Set l_rstUsers = ExecuteSQL(l_strSQL, g_strExecuteError)
            If l_rstUsers.RecordCount <> 1 Then
                MsgBox "Cannot do a NexgGaurd Based Transcode without there being one unique Recipient for the job"
                Exit Sub
            End If
            l_rstUsers.Close
            Set l_rstUsers = Nothing
        End If
        
        With frmClipSearch
            Set l_rstTemp = ExecuteSQL(.adoClip.RecordSource, g_strExecuteError)
            CheckForSQLError
            
            If l_rstTemp.RecordCount > 0 Then
                l_rstTemp.MoveFirst
                Do While Not l_rstTemp.EOF
                    'Put up the progress indicator
                    lblCurrentFilename.Caption = l_rstTemp("clipfilename")
                    lblTimecodeStart.Caption = GetData("events", "timecodestart", "eventID", l_rstTemp("eventID"))
                    lblTimecodeStop.Caption = GetData("events", "timecodestop", "eventID", l_rstTemp("eventID"))
                    If chkTimecodesFromCaller.Value <> 0 Then
                        Set l_rstTemp2 = ExecuteSQL("SELECT * FROM eventlogging WHERE eventID = " & l_rstTemp("eventID") & ";", g_strExecuteError)
                        CheckForSQLError
                        If l_rstTemp2.RecordCount > 0 Then
                            l_rstTemp2.MoveFirst
                            Do While Not l_rstTemp2.EOF
                                If l_rstTemp2("segmentreference") = "Programme" Then
                                    txtTimecodeStart.Text = l_rstTemp2("timecodestart")
                                    txtTimecodeStop.Text = l_rstTemp2("timecodestop")
                                    Exit Do
                                Else
                                    txtTimecodeStart.Text = ""
                                    txtTimecodeStop.Text = ""
                                End If
                                l_rstTemp2.MoveNext
                            Loop
                        End If
                        If txtTimecodeStart.Text = "" Then
                            MsgBox "Clip " & l_rstTemp("eventID") & " does not have its Programme Start and End timecodes logged." & vbCrLf & "Prog only Transcoding cancelled.", vbCritical, "Error..."
                            Unload Me
                            Exit Sub
                        End If
                        l_rstTemp2.Close
                        Set l_rstTemp2 = Nothing
                    End If
                    DoEvents
                    
                    If 1 = 1 Then
'                    If (cmbTranscodeSpec.Columns("transcodesystem").Text = "FFMpeg_Clip" And (UCase(Left(l_rstTemp("clipformat"), 4)) = "MPEG" Or UCase(l_rstTemp("clipformat")) = "FLASH MP4")) Or cmbTranscodeSpec.Columns("transcodesystem").Text <> "FFMpeg_Clip" Then
                        'Insert the request
                        If cmbTranscodeSpec.Columns("transcodespecID").Text = 1 Or cmbTranscodeSpec.Columns("transcodespecID").Text = 2 Or cmbTranscodeSpec.Columns("transcodespecID").Text = 5 Then
                            SQL = "INSERT INTO transcoderequest (cetauserID, sourceclipID, transcodesystem, segmentreference, fps, TextToBurnIn, TextToBurnInOpacity, jobID, timecodestart, sourceaudio1clipID, transcodespecID, videoonly, audioonly, leavehidden, overwriteifexisting, status, trackerlinkedoperation, trackerlinkedstagefield, newfilenamefromname, skiplogging, starttimecodeoverride, cetauser, savedate) VALUES ("
                        ElseIf txtTimecodeStart.Text <> "" And txtTimecodeStop.Text <> "" Then
                            SQL = "INSERT INTO transcoderequest (cetauserID, sourceclipID, transcodesystem, segmentreference, fps, TextToBurnIn, TextToBurnInOpacity, jobID, timecodestart, timecodestop, sourceaudio1clipID, transcodespecID, videoonly, audioonly, leavehidden, overwriteifexisting, status, trackerlinkedoperation, trackerlinkedstagefield, newfilenamefromname, skiplogging, starttimecodeoverride, cetauser, savedate) VALUES ("
                        ElseIf txtTimecodeStart.Text <> "" And txtTimecodeStop.Text = "" Then
                            SQL = "INSERT INTO transcoderequest (cetauserID, sourceclipID, transcodesystem, segmentreference, fps, TextToBurnIn, TextToBurnInOpacity, jobID, timecodestart, timecodestop, sourceaudio1clipID, transcodespecID, videoonly, audioonly, leavehidden, overwriteifexisting, status, trackerlinkedoperation, trackerlinkedstagefield, newfilenamefromname, skiplogging, starttimecodeoverride, cetauser, savedate) VALUES ("
                        Else
                            SQL = "INSERT INTO transcoderequest (cetauserID, sourceclipID, transcodesystem, segmentreference, fps, TextToBurnIn, TextToBurnInOpacity, jobID, sourceaudio1clipID, transcodespecID, videoonly, audioonly, leavehidden, overwriteifexisting, status, trackerlinkedoperation, trackerlinkedstagefield, newfilenamefromname, skiplogging, starttimecodeoverride, cetauser, savedate) VALUES ("
                        End If
                        
                        SQL = SQL & g_lngUserID & ", "
                        SQL = SQL & l_rstTemp("eventID") & ", '" & cmbTranscodeSpec.Columns("transcodesystem").Text & "', "
                        If chkPatheStills.Value <> 0 Then
                            SQL = SQL & "'" & QuoteSanitise(l_rstTemp("customfield1")) & "', "
                        Else
                            SQL = SQL & "'" & QuoteSanitise(l_rstTemp("clipreference")) & IIf(cmbTranscodeSpec.Columns("NexGuard").Text <> 0, "_" & txtJobID.Text, "") & "', "
                        End If
                        SQL = SQL & "'" & GetData("events", "clipframerate", "eventID", l_rstTemp("eventID")) & "', "
                        
                        If txtTextToBurnIn.Text <> "" Then
                            SQL = SQL & "'" & QuoteSanitise(txtTextToBurnIn.Text) & "', "
                        Else
                            SQL = SQL & "Null, "
                        End If
                                            
                        If Val(txtTextToBurnInOpacity.Text) >= 0 And Val(txtTextToBurnInOpacity.Text) <= 1 Then
                            SQL = SQL & "'" & Val(txtTextToBurnInOpacity.Text) & "', "
                        Else
                            SQL = SQL & "Null, "
                        End If
                                            
                        If Val(txtJobID.Text) <> 0 Then
                            SQL = SQL & Val(txtJobID.Text) & ", "
                        Else
                            SQL = SQL & "Null, "
                        End If
                            
                        If cmbTranscodeSpec.Columns("transcodespecID").Text = 1 Or cmbTranscodeSpec.Columns("transcodespecID").Text = 2 Then
                            If txtTimecodeStart.Text <> "" Then
                                SQL = SQL & "'" & txtTimecodeStart.Text & "', "
                            ElseIf Trim(" " & GetData("events", "eventkeyframetimecode", "eventID", l_rstTemp("eventID"))) <> "" Then
                                SQL = SQL & "'" & Trim(" " & GetData("events", "eventkeyframetimecode", "eventID", l_rstTemp("eventID"))) & "', "
                            Else
                                SQL = SQL & "'', "
                            End If
                        End If
                        
                        If cmbTranscodeSpec.Columns("transcodespecID").Text = 5 Then
                            If txtTimecodeStart.Text <> "" Then
                                SQL = SQL & "'" & txtTimecodeStart.Text & "', "
                            Else
                                SQL = SQL & "'', "
                            End If
                        End If
                        
                        If txtTimecodeStart.Text <> "" And txtTimecodeStop.Text <> "" And cmbTranscodeSpec.Columns("transcodespecID").Text <> 1 And cmbTranscodeSpec.Columns("transcodespecID").Text <> 2 And cmbTranscodeSpec.Columns("transcodespecID").Text <> 5 Then
                            SQL = SQL & "'" & txtTimecodeStart.Text & "', '" & txtTimecodeStop.Text & "', "
                        ElseIf txtTimecodeStart.Text <> "" And txtTimecodeStop.Text = "" And cmbTranscodeSpec.Columns("transcodesystem").Text = "FFMpeg_Timecode_Restripe" Then
                            SQL = SQL & "'" & txtTimecodeStart.Text & "', '', "
                        ElseIf txtTimecodeStart.Text <> "" And txtTimecodeStop.Text = "" And cmbTranscodeSpec.Columns("transcodespecID").Text <> 1 And cmbTranscodeSpec.Columns("transcodespecID").Text <> 2 And cmbTranscodeSpec.Columns("transcodespecID").Text <> 5 Then
                            SQL = SQL & "'" & txtTimecodeStart.Text & "', '" & lblTimecodeStop.Caption & "', "
                        End If
                        
                        SQL = SQL & l_rstTemp("eventID") & ", "
                        
                        SQL = SQL & cmbTranscodeSpec.Columns("transcodespecID").Text & ", "
                        
                        If optVideoAudio(0).Value = True Then
                            SQL = SQL & "0, 0, "
                        ElseIf optVideoAudio(1).Value = True Then
                            SQL = SQL & "1, 0, "
                        Else
                            SQL = SQL & "0, 1, "
                        End If
                        
                        If optHidden(0).Value = True Then
                            SQL = SQL & "1, "
                        Else
                            SQL = SQL & "0, "
                        End If
                        
                        If chkOverWrite.Value <> 0 Then
                            SQL = SQL & "1, "
                        Else
                            SQL = SQL & "0, "
                        End If
                        
                        SQL = SQL & "(SELECT transcodestatusID FROM transcodestatus WHERE description = 'Requested'), "
                        
                        If cmbTrackerLink.Text <> "" Then
                            SQL = SQL & "1, '" & Mid(cmbTrackerLink.Columns("itemfield").Text, 11) & "', "
                        Else
                            SQL = SQL & "0, '', "
                        End If
                        
                        If chkFilenameFromFile.Value <> 0 Then
                            SQL = SQL & "1, "
                        Else
                            SQL = SQL & "0, "
                        End If
                        
                        If chkSkipLogging.Value <> 0 Or Val(cmbTranscodeSpec.Columns("skiplogging").Text) <> 0 Then
                            SQL = SQL & "1, "
                        Else
                            SQL = SQL & "0, "
                        End If
                                            
'                        If chkProgramOnly.Value <> 0 Then
'                            Select Case GetData("events", "clipframerate", "eventID", l_rstTemp("eventID"))
'                                Case "25", "24", "23.98"
'                                    SQL = SQL & "'10:00:00:00', "
'                                Case "29.97"
'                                    SQL = SQL & "'01:00:00;00', "
'                                Case "30"
'                                    SQL = SQL & "'01:00:00:00', "
'                            End Select
'                        ElseIf txtStartTimecodeOverride.Text <> "" Then
                        If txtStartTimecodeOverride.Text <> "" Then
                            SQL = SQL & "'" & txtStartTimecodeOverride.Text & "', "
                        Else
                            SQL = SQL & "NULL, "
                        End If
                                            
                        SQL = SQL & "'" & g_strUserInitials & "', " & IIf(chkFutureRequest.Value <> 0, "dateadd(d, 30, getdate())", "getdate()") & ");"
                        
                        ExecuteSQL SQL, g_strExecuteError
                        CheckForSQLError
                    Else
                        MsgBox "Clip: " & l_rstTemp("eventID") & "is nort of a format that can be handled by FFMpeg_Clip System." & vbCrLf & "That transcode has not been submitted."
                    End If
                    l_rstTemp.MoveNext
                Loop
                .lblCurrentFilename.Caption = ""
                DoEvents
                
            End If
            l_rstTemp.Close
            Set l_rstTemp = Nothing
        End With

    ElseIf chkKidscoBulkTranscoding.Value <> 0 Then
        If cmbTranscodeSpec.Columns("NexGuard").Text <> 0 Then
            If txtJobID.Text = "" Then
                MsgBox "Cannot do a NexgGaurd Based Transcode without the JobID"
                Exit Sub
            End If
            l_strSQL = "SELECT * FROM jobcontact WHERE jobID = " & Val(txtJobID.Text) & " AND CCnotification = 0;"
            Set l_rstUsers = ExecuteSQL(l_strSQL, g_strExecuteError)
            If l_rstUsers.RecordCount <> 1 Then
                MsgBox "Cannot do a NexgGaurd Based Transcode without there being one unique Recipient for the job"
                Exit Sub
            End If
            l_rstUsers.Close
            Set l_rstUsers = Nothing
        End If
        
        With frmClipSearch
            Set l_rstTemp = ExecuteSQL(.adoClip.RecordSource, g_strExecuteError)
            CheckForSQLError
            
            If l_rstTemp.RecordCount > 0 Then
                l_rstTemp.MoveFirst
                Count = 0
                Do While Not l_rstTemp.EOF
                    'Put up the progress indicator
                    lblCurrentFilename.Caption = l_rstTemp("clipfilename")
                    lblTimecodeStart.Caption = GetData("events", "timecodestart", "eventID", l_rstTemp("eventID"))
                    lblTimecodeStop.Caption = GetData("events", "timecodestop", "eventID", l_rstTemp("eventID"))
                    DoEvents
                    
                    If 1 = 1 Then
'If (cmbTranscodeSpec.Columns("transcodesystem").Text = "FFMpeg_Clip" And (UCase(Left(l_rstTemp("clipformat"), 4)) = "MPEG" Or UCase(l_rstTemp("clipformat")) = "FLASH MP4")) Or cmbTranscodeSpec.Columns("transcodesystem").Text <> "FFMpeg_Clip" Then
                        'Insert the request
                        If cmbTranscodeSpec.Columns("transcodespecID").Text = 1 Or cmbTranscodeSpec.Columns("transcodespecID").Text = 2 Or cmbTranscodeSpec.Columns("transcodespecID").Text = 5 Then
                            SQL = "INSERT INTO transcoderequest (cetauserID, sourceclipID, transcodesystem, segmentreference, fps, TextToBurnIn, TextToBurnInOpacity, jobID, timecodestart, sourceaudio1clipID, transcodespecID, videoonly, audioonly, leavehidden, overwriteifexisting, status, trackerlinkedoperation, trackerlinkedstagefield, newfilenamefromname, skiplogging, starttimecodeoverride, cetauser, savedate) VALUES ("
                        ElseIf txtTimecodeStart.Text <> "" And txtTimecodeStop.Text <> "" Then
                            SQL = "INSERT INTO transcoderequest (cetauserID, sourceclipID, transcodesystem, segmentreference, fps, TextToBurnIn, TextToBurnInOpacity, jobID, timecodestart, timecodestop, sourceaudio1clipID, transcodespecID, videoonly, audioonly, leavehidden, overwriteifexisting, status, trackerlinkedoperation, trackerlinkedstagefield, newfilenamefromname, skiplogging, starttimecodeoverride, cetauser, savedate) VALUES ("
                        ElseIf txtTimecodeStart.Text <> "" And txtTimecodeStop.Text = "" Then
                            SQL = "INSERT INTO transcoderequest (cetauserID, sourceclipID, transcodesystem, segmentreference, fps, TextToBurnIn, TextToBurnInOpacity, jobID, timecodestart, timecodestop, sourceaudio1clipID, transcodespecID, videoonly, audioonly, leavehidden, overwriteifexisting, status, trackerlinkedoperation, trackerlinkedstagefield, newfilenamefromname, skiplogging, starttimecodeoverride, cetauser, savedate) VALUES ("
                        Else
                            SQL = "INSERT INTO transcoderequest (cetauserID, sourceclipID, transcodesystem, segmentreference, fps, TextToBurnIn, TextToBurnInOpacity, jobID, sourceaudio1clipID, transcodespecID, videoonly, audioonly, leavehidden, overwriteifexisting, status, trackerlinkedoperation, trackerlinkedstagefield, newfilenamefromname, skiplogging, starttimecodeoverride, cetauser, savedate) VALUES ("
                        End If
                        
                        SQL = SQL & g_lngUserID & ", "
                        SQL = SQL & l_rstTemp("eventID") & ", '" & cmbTranscodeSpec.Columns("transcodesystem").Text & "', "
                        If chkPatheStills.Value <> 0 Then
                            SQL = SQL & "'" & QuoteSanitise(l_rstTemp("customfield1")) & "', "
                        Else
                            SQL = SQL & "'" & QuoteSanitise(l_rstTemp("clipreference")) & IIf(cmbTranscodeSpec.Columns("NexGuard").Text <> 0, "_" & txtJobID.Text, "") & "', "
                        End If
                        SQL = SQL & "'" & GetData("events", "clipframerate", "eventID", l_rstTemp("eventID")) & "', "
                        If txtTextToBurnIn.Text <> "" Then
                            SQL = SQL & "'" & QuoteSanitise(txtTextToBurnIn.Text) & "', "
                        Else
                            SQL = SQL & "Null, "
                        End If
                        
                        If Val(txtTextToBurnInOpacity.Text) >= 0 And Val(txtTextToBurnInOpacity.Text) <= 1 Then
                            SQL = SQL & "'" & Val(txtTextToBurnInOpacity.Text) & "', "
                        Else
                            SQL = SQL & "Null, "
                        End If
                                                                
                        If Val(txtJobID.Text) <> 0 Then
                            SQL = SQL & Val(txtJobID.Text) & ", "
                        Else
                            SQL = SQL & "Null, "
                        End If
                            
                        If cmbTranscodeSpec.Columns("transcodespecID").Text = 1 Or cmbTranscodeSpec.Columns("transcodespecID").Text = 2 Then
                            If txtTimecodeStart.Text <> "" Then
                                SQL = SQL & "'" & txtTimecodeStart.Text & "', "
                            ElseIf Trim(" " & GetData("events", "eventkeyframetimecode", "eventID", l_rstTemp("eventID"))) <> "" Then
                                SQL = SQL & "'" & Trim(" " & GetData("events", "eventkeyframetimecode", "eventID", l_rstTemp("eventID"))) & "', "
                            Else
                                SQL = SQL & "'', "
                            End If
                        End If
                        
                        If cmbTranscodeSpec.Columns("transcodespecID").Text = 5 Then
                            If txtTimecodeStart.Text <> "" Then
                                SQL = SQL & "'" & txtTimecodeStart.Text & "', "
                            Else
                                SQL = SQL & "'', "
                            End If
                        End If
                        
                        If txtTimecodeStart.Text <> "" And txtTimecodeStop.Text <> "" And cmbTranscodeSpec.Columns("transcodespecID").Text <> 1 And cmbTranscodeSpec.Columns("transcodespecID").Text <> 2 And cmbTranscodeSpec.Columns("transcodespecID").Text <> 5 Then
                            SQL = SQL & "'" & txtTimecodeStart.Text & "', '" & txtTimecodeStop.Text & "', "
                        ElseIf txtTimecodeStart.Text <> "" And txtTimecodeStop.Text = "" And cmbTranscodeSpec.Columns("transcodesystem").Text = "FFMpeg_Timecode_Restripe" Then
                            SQL = SQL & "'" & txtTimecodeStart.Text & "', '', "
                        ElseIf txtTimecodeStart.Text <> "" And txtTimecodeStop.Text = "" And cmbTranscodeSpec.Columns("transcodespecID").Text <> 1 And cmbTranscodeSpec.Columns("transcodespecID").Text <> 2 And cmbTranscodeSpec.Columns("transcodespecID").Text <> 5 Then
                            SQL = SQL & "'" & txtTimecodeStart.Text & "', '" & lblTimecodeStop.Caption & "', "
                        End If
                                            
                        On Error GoTo KIDSCOPROBLEM
                        SQL2 = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & l_rstTemp("libraryID") & " AND companyID = " & .lblCompanyID.Caption & " AND clipreference = '" & l_rstTemp("clipreference") _
                            & "' AND clipfilename = '" & Left(l_rstTemp("clipfilename"), Len(l_rstTemp("clipfilename")) - 4) & "ENGA.wav" & "';"
                        l_lngAudioID = GetDataSQL(SQL2)
                        SQL = SQL & l_lngAudioID & ", "
                        On Error GoTo 0
                        
                        SQL = SQL & cmbTranscodeSpec.Columns("transcodespecID").Text & ", "
                        
                        If optVideoAudio(0).Value = True Then
                            SQL = SQL & "0, 0, "
                        ElseIf optVideoAudio(1).Value = True Then
                            SQL = SQL & "1, 0, "
                        Else
                            SQL = SQL & "0, 1, "
                        End If
                        
                        If optHidden(0).Value = True Then
                            SQL = SQL & "1, "
                        Else
                            SQL = SQL & "0, "
                        End If
                        
                        If chkOverWrite.Value <> 0 Then
                            SQL = SQL & "1, "
                        Else
                            SQL = SQL & "0, "
                        End If
                        
                        SQL = SQL & "(SELECT transcodestatusID FROM transcodestatus WHERE description = 'Requested'), "
                        
                        If cmbTrackerLink.Text <> "" Then
                            SQL = SQL & "1, '" & Mid(cmbTrackerLink.Columns("itemfield").Text, 11) & "', "
                        Else
                            SQL = SQL & "0, '', "
                        End If
                        
                        If chkFilenameFromFile.Value <> 0 Then
                            SQL = SQL & "1, "
                        Else
                            SQL = SQL & "0, "
                        End If
                        
                        If chkSkipLogging.Value <> 0 Or Val(cmbTranscodeSpec.Columns("skiplogging").Text) <> 0 Then
                            SQL = SQL & "1, "
                        Else
                            SQL = SQL & "0, "
                        End If
                                            
                        If txtStartTimecodeOverride.Text <> "" Then
                            SQL = SQL & "'" & txtStartTimecodeOverride.Text & "', "
                        Else
                            SQL = SQL & "NULL, "
                        End If
                                            
                        SQL = SQL & "'" & g_strUserInitials & "', " & IIf(chkFutureRequest.Value <> 0, "dateadd(d, 30, getdate())", "getdate()") & ");"
                       
                        ExecuteSQL SQL, g_strExecuteError
                        CheckForSQLError
                    Else
                    End If
                    Count = Count + 1
                    
                    l_rstTemp.MoveNext
                Loop
                .lblCurrentFilename.Caption = ""
                DoEvents
                
            End If
            l_rstTemp.Close
            Set l_rstTemp = Nothing
        End With
    Else
        If cmbTranscodeSpec.Columns("transcodesystem").Text = "FFMpeg_Audio_Conform" Then
            If GetData("events", "audiotypegroup1", "eventID", Val(lblVideoFile.Caption)) = "" Then
                MsgBox "Cannot do an Audio Conform if the audio tracks are not logged."
                Exit Sub
            End If
        End If
        If cmbTranscodeSpec.Columns("NexGuard").Text <> 0 Then
            If txtJobID.Text = "" Then
                MsgBox "Cannot do a NexgGaurd Based Transcode without the JobID"
                Exit Sub
            End If
            l_strSQL = "SELECT * FROM jobcontact WHERE jobID = " & Val(txtJobID.Text) & " AND CCnotification = 0;"
            Set l_rstUsers = ExecuteSQL(l_strSQL, g_strExecuteError)
            If l_rstUsers.RecordCount <> 1 Then
                MsgBox "Cannot do a NexgGaurd Based Transcode without there being one unique Recipient for the job"
                Exit Sub
            End If
            l_rstUsers.Close
            Set l_rstUsers = Nothing
        End If
        If 1 = 1 Then
'        If (cmbTranscodeSpec.Columns("transcodesystem").Text = "FFMpeg_Clip" And (UCase(Left(lblSourceFormat.Caption, 4)) = "MPEG" Or UCase(lblSourceFormat.Caption) = "FLASH MP4")) Or cmbTranscodeSpec.Columns("transcodesystem").Text <> "FFMpeg_Clip" Then
            If cmbTranscodeSpec.Columns("transcodespecID").Text = 1 Or cmbTranscodeSpec.Columns("transcodespecID").Text = 2 Or cmbTranscodeSpec.Columns("transcodespecID").Text = 5 Then
                SQL = "INSERT INTO transcoderequest (cetauserID, sourceclipID, transcodesystem, segmentreference, fps, TextToBurnIn, TextToBurnInOpacity, jobID, timecodestart, sourceaudio1clipID, sourceaudio2clipID, transcodespecID, videoonly, audioonly, leavehidden, overwriteifexisting, status, trackerlinkedoperation, trackerlinkedstagefield, newfilenamefromname, skiplogging, starttimecodeoverride, cetauser, savedate) VALUES ("
            ElseIf txtTimecodeStart.Text <> "" And txtTimecodeStop.Text <> "" Then
                SQL = "INSERT INTO transcoderequest (cetauserID, sourceclipID, transcodesystem, segmentreference, fps, TextToBurnIn, TextToBurnInOpacity, jobID, timecodestart, timecodestop, sourceaudio1clipID, sourceaudio2clipID, transcodespecID, videoonly, audioonly, leavehidden, overwriteifexisting, status, trackerlinkedoperation, trackerlinkedstagefield, newfilenamefromname, skiplogging, starttimecodeoverride, cetauser, savedate) VALUES ("
            ElseIf txtTimecodeStart.Text <> "" And txtTimecodeStop.Text = "" Then
                SQL = "INSERT INTO transcoderequest (cetauserID, sourceclipID, transcodesystem, segmentreference, fps, TextToBurnIn, TextToBurnInOpacity, jobID, timecodestart, timecodestop, sourceaudio1clipID, sourceaudio2clipID, transcodespecID, videoonly, audioonly, leavehidden, overwriteifexisting, status, trackerlinkedoperation, trackerlinkedstagefield, newfilenamefromname, skiplogging, starttimecodeoverride, cetauser, savedate) VALUES ("
            Else
                SQL = "INSERT INTO transcoderequest (cetauserID, sourceclipID, transcodesystem, segmentreference, fps, TextToBurnIn, TextToBurnInOpacity, jobID, sourceaudio1clipID, sourceaudio2clipID, transcodespecID, videoonly, audioonly, leavehidden, overwriteifexisting, status, trackerlinkedoperation, trackerlinkedstagefield, newfilenamefromname, skiplogging, starttimecodeoverride, cetauser, savedate) VALUES ("
            End If
            
            SQL = SQL & g_lngUserID & ", "
            SQL = SQL & lblVideoFile.Caption & ", '" & cmbTranscodeSpec.Columns("transcodesystem").Text & "', "
    
            If chkPatheStills.Value <> 0 Then
                SQL = SQL & "'" & QuoteSanitise(txtPatheFileID.Text) & "', "
            Else
                SQL = SQL & "'" & QuoteSanitise(txtClipReference.Text) & IIf(cmbTranscodeSpec.Columns("NexGuard").Text <> 0, "_" & txtJobID.Text, "") & "', "
            End If
            
            SQL = SQL & "'"
            If optTimecodeType(0).Value = True Then
                SQL = SQL & "25"
            ElseIf optTimecodeType(1).Value = True Then
                SQL = SQL & "29.97"
            ElseIf optTimecodeType(2).Value = True Then
                SQL = SQL & "30"
            ElseIf optTimecodeType(3).Value = True Then
                SQL = SQL & "24"
            ElseIf optTimecodeType(7).Value = True Then
                SQL = SQL & "29.97 NDF"
            End If
            SQL = SQL & "', "
            
            If txtTextToBurnIn.Text <> "" Then
                SQL = SQL & "'" & QuoteSanitise(txtTextToBurnIn.Text) & "', "
            Else
                SQL = SQL & "Null, "
            End If
            
            If Val(txtTextToBurnInOpacity.Text) >= 0 And Val(txtTextToBurnInOpacity.Text) <= 1 Then
                SQL = SQL & "'" & Val(txtTextToBurnInOpacity.Text) & "', "
            Else
                SQL = SQL & "Null, "
            End If
            
            If Val(txtJobID.Text) <> 0 Then
                SQL = SQL & Val(txtJobID.Text) & ", "
            Else
                SQL = SQL & "Null, "
            End If
                
            If cmbTranscodeSpec.Columns("transcodespecID").Text = 1 Or cmbTranscodeSpec.Columns("transcodespecID").Text = 2 Then
                If txtTimecodeStart.Text <> "" Then
                    SQL = SQL & "'" & txtTimecodeStart.Text & "', "
                ElseIf Trim(" " & GetData("events", "eventkeyframetimecode", "eventID", lblVideoFile.Caption)) <> "" Then
                    SQL = SQL & "'" & Trim(" " & GetData("events", "eventkeyframetimecode", "eventID", lblVideoFile.Caption)) & "', "
                Else
                    SQL = SQL & "'', "
                End If
            End If
            
            If cmbTranscodeSpec.Columns("transcodespecID").Text = 5 Then
                If txtTimecodeStart.Text <> "" Then
                    SQL = SQL & "'" & txtTimecodeStart.Text & "', "
                Else
                    SQL = SQL & "'', "
                End If
            End If
            
            If txtTimecodeStart.Text <> "" And txtTimecodeStop.Text <> "" And cmbTranscodeSpec.Columns("transcodespecID").Text <> 1 And cmbTranscodeSpec.Columns("transcodespecID").Text <> 2 And cmbTranscodeSpec.Columns("transcodespecID").Text <> 5 Then
                SQL = SQL & "'" & txtTimecodeStart.Text & "', '" & txtTimecodeStop.Text & "', "
            ElseIf txtTimecodeStart.Text <> "" And txtTimecodeStop.Text = "" And cmbTranscodeSpec.Columns("transcodesystem").Text = "FFMpeg_Timecode_Restripe" Then
                SQL = SQL & "'" & txtTimecodeStart.Text & "', '', "
            ElseIf txtTimecodeStart.Text <> "" And txtTimecodeStop.Text = "" And cmbTranscodeSpec.Columns("transcodespecID").Text <> 1 And cmbTranscodeSpec.Columns("transcodespecID").Text <> 2 And cmbTranscodeSpec.Columns("transcodespecID").Text <> 5 Then
                SQL = SQL & "'" & txtTimecodeStart.Text & "', '" & lblTimecodeStop.Caption & "', "
            End If
                    
            SQL = SQL & lblVideoFile.Caption & ", "
            
            SQL = SQL & "Null, "
            
            SQL = SQL & cmbTranscodeSpec.Columns("transcodespecID").Text & ", "
            If optVideoAudio(0).Value = True Then
                SQL = SQL & "0, 0, "
            ElseIf optVideoAudio(1).Value = True Then
                SQL = SQL & "1, 0, "
            Else
                SQL = SQL & "0, 1, "
            End If
            
            If optHidden(0).Value = True Then
                SQL = SQL & "1, "
            Else
                SQL = SQL & "0, "
            End If
            
            If chkOverWrite.Value <> 0 Then
                SQL = SQL & "1, "
            Else
                SQL = SQL & "0, "
            End If
            
            SQL = SQL & "(SELECT transcodestatusID FROM transcodestatus WHERE description = 'Requested'), "
            
            If cmbTrackerLink.Text <> "" Then
                SQL = SQL & "1, '" & Mid(cmbTrackerLink.Columns("itemfield").Text, 11) & "', "
            Else
                SQL = SQL & "0, '', "
            End If
            
            If chkFilenameFromFile.Value <> 0 Then
                SQL = SQL & "1, "
            Else
                SQL = SQL & "0, "
            End If
            
            If chkSkipLogging.Value <> 0 Or Val(cmbTranscodeSpec.Columns("skiplogging").Text) <> 0 Then
                SQL = SQL & "1, "
            Else
                SQL = SQL & "0, "
            End If
                                
            If txtStartTimecodeOverride.Text <> "" Then
                SQL = SQL & "'" & txtStartTimecodeOverride.Text & "', "
            Else
                SQL = SQL & "NULL, "
            End If
                                
            SQL = SQL & "'" & g_strUserInitials & "', " & IIf(chkFutureRequest.Value <> 0, "dateadd(d, 30, getdate())", "getdate()") & ");"
            
            Debug.Print SQL
            ExecuteSQL SQL, g_strExecuteError
            CheckForSQLError
            
            'Record the clip usage
            SQL = "INSERT INTO eventusage ("
            SQL = SQL & "eventID, dateused) VALUES ("
            SQL = SQL & "'" & lblVideoFile.Caption & "', "
            SQL = SQL & "'" & FormatSQLDate(Now) & "'"
            SQL = SQL & ");"
            
            ExecuteSQL SQL, g_strExecuteError
            CheckForSQLError
            
        Else
            MsgBox "Clip is of a format that cannot be subclippped using FFMpeg_Clip system. Transcode not submitted."
        End If
    End If
    
End If

Me.Hide
Exit Sub

KIDSCOPROBLEM:

MsgBox "There was a problem locating the matching English Audio for " & l_rstTemp("clipfilename") & vbCrLf & "Bulk Transcode submission aborted." & vbCrLf & Count & " transocdes have already been submitted.", vbCritical, "Error."
l_rstTemp.Close
Set l_rstTemp = Nothing
Exit Sub

End Sub

Private Sub Form_Activate()

Dim l_strSQL As String, l_lngCompanyID As Long

optVideoAudio(0).Value = True

If chkBulkTranscoding.Value <> 0 Or chkKidscoBulkTranscoding.Value <> 0 Then
    chkFutureRequest.Value = frmClipSearch.chkFutureRequest.Value
Else
    lblVideoFile.Caption = frmClipControl.txtClipID.Text
    txtClipReference.Text = frmClipControl.txtReference.Text
    txtPatheFileID.Text = frmClipControl.cmbField1.Text
'    adoAudioFile1.ConnectionString = g_strConnection
'    adoAudioFile1.RecordSource = "SELECT eventID, clipfilename, barcode, altlocation FROM events INNER JOIN library ON events.libraryID = library.libraryID WHERE 1=1 " _
'        & "AND library.format = 'DISCSTORE' AND clipreference Like '" & QuoteSanitise(txtClipReference.Text) & "%' ORDER BY barcode, altlocation, clipfilename;"
'    adoAudioFile1.Refresh
End If

cmbTranscodeSpec.Text = ""
If chkTimecodesFromCaller.Value = 0 Then
    txtTimecodeStart.Text = ""
    txtTimecodeStop.Text = ""
    txtTimecodeStart.Enabled = True
    txtTimecodeStop.Enabled = True
Else
    txtTimecodeStart.Enabled = False
    txtTimecodeStop.Enabled = False
End If

optVideoAudio(0).Value = True

If chkKidscoBulkTranscoding.Value <> 0 Then
    chkOverWrite.Value = 1
Else
    chkOverWrite.Value = 0
End If

optHidden(0).Value = True

If chkMOConvert.Value = 0 Then
    l_strSQL = "SELECT transcodespecID, transcodename, transcodesystem, skiplogging, NexGuard, CASE WHEN companyID = 501 THEN 1 ELSE 2 END companyorder FROM transcodespec WHERE enabled <> 0 ORDER by forder, transcodename;"
Else
    l_strSQL = "SELECT transcodespecID, transcodename, transcodesystem, skiplogging, NexGuard, CASE WHEN companyID = 501 THEN 1 ELSE 2 END companyorder FROM transcodespec WHERE enabled <> 0 AND transcodesystem = 'FFMpeg_MO_Convert' ORDER by forder, transcodename;"
End If
adoTranscodeSpecs.ConnectionString = g_strConnection
adoTranscodeSpecs.RecordSource = l_strSQL
adoTranscodeSpecs.Refresh

If chkBulkTranscoding.Value = 0 Then
    l_lngCompanyID = GetData("events", "companyID", "eventID", lblVideoFile.Caption)
Else
    l_lngCompanyID = Val(frmClipSearch.lblCompanyID.Caption)
End If

l_strSQL = "SELECT itemfield, itemheading FROM tracker_itemchoice WHERE companyID = " & l_lngCompanyID & " AND itemfield LIKE 'stagefield%' order by itemfield;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch1 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch1 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch1
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch1.ActiveConnection = Nothing

Set cmbTrackerLink.DataSourceList = l_rstSearch1

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub timFormClose_Timer()

g_optCloseSystemDown = GetFlag(GetData("setting", "value", "name", "CloseSystemDown"))

If g_optCloseSystemDown <> 0 Then Unload Me

End Sub

Private Sub txtStartTimecodeOverride_KeyPress(KeyAscii As Integer)

Set_Framerate

If Len(txtStartTimecodeOverride.Text) < 2 Then
    Select Case m_Framerate
        Case TC_29
            txtStartTimecodeOverride.Text = "00:00:00;00"
        Case Else
            txtStartTimecodeOverride.Text = "00:00:00:00"
    End Select
    txtStartTimecodeOverride.SelStart = 0
    txtStartTimecodeOverride.SelLength = 1
Else
    Timecode_Check_Control txtStartTimecodeOverride, KeyAscii, m_Framerate
End If

End Sub

Private Sub txtTimecodestart_KeyPress(KeyAscii As Integer)

Set_Framerate

If Len(txtTimecodeStart.Text) < 2 Then
    Select Case m_Framerate
        Case TC_29
            txtTimecodeStart.Text = "00:00:00;00"
        Case Else
            txtTimecodeStart.Text = "00:00:00:00"
    End Select
    txtTimecodeStart.SelStart = 0
    txtTimecodeStart.SelLength = 1
Else
    Timecode_Check_Control txtTimecodeStart, KeyAscii, m_Framerate
End If

End Sub

Private Sub txtTimecodestop_KeyPress(KeyAscii As Integer)

Set_Framerate

If Len(txtTimecodeStart.Text) < 2 Then
    Select Case m_Framerate
        Case TC_29
            txtTimecodeStart.Text = "00:00:00;00"
        Case Else
            txtTimecodeStart.Text = "00:00:00:00"
    End Select
    txtTimecodeStart.SelStart = 0
    txtTimecodeStart.SelLength = 1
End If

If Len(txtTimecodeStop.Text) < 2 Then
    Select Case m_Framerate
        Case TC_29
            txtTimecodeStop.Text = "00:00:00;00"
        Case Else
            txtTimecodeStop.Text = "00:00:00:00"
    End Select
    txtTimecodeStop.SelStart = 0
    txtTimecodeStop.SelLength = 1
Else
    Timecode_Check_Control txtTimecodeStop, KeyAscii, m_Framerate
End If

End Sub

Public Sub Set_Framerate()

If optTimecodeType(0).Value = True Then
    m_Framerate = TC_25
ElseIf optTimecodeType(1).Value = True Then
    m_Framerate = TC_29
ElseIf optTimecodeType(2).Value = True Then
    m_Framerate = TC_30
ElseIf optTimecodeType(3).Value = True Then
    m_Framerate = TC_24
Else
    m_Framerate = TC_UN
End If

End Sub

