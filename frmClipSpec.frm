VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmClipSpec 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Media Specification"
   ClientHeight    =   9360
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   23895
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmClipSpec.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9360
   ScaleWidth      =   23895
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtGraphicOverlayOpacity 
      Height          =   315
      Left            =   16680
      TabIndex        =   78
      ToolTipText     =   "The clip bitrate"
      Top             =   1680
      Width           =   2175
   End
   Begin VB.TextBox txtGraphicOverlayFilename 
      Height          =   315
      Left            =   16680
      TabIndex        =   77
      ToolTipText     =   "The clip filename"
      Top             =   1320
      Width           =   5835
   End
   Begin VB.TextBox txtSecondAudioTrack 
      Height          =   315
      Left            =   16680
      TabIndex        =   73
      ToolTipText     =   "The clip bitrate"
      Top             =   2940
      Width           =   1935
   End
   Begin VB.TextBox txtFirstAudioTrack 
      Height          =   315
      Left            =   16680
      TabIndex        =   71
      ToolTipText     =   "The clip bitrate"
      Top             =   2580
      Width           =   1935
   End
   Begin VB.ComboBox cmbVodPlatform 
      Height          =   315
      Left            =   1920
      TabIndex        =   69
      ToolTipText     =   "The Clip Codec"
      Top             =   6960
      Width           =   1935
   End
   Begin VB.CommandButton cmdClearCompany 
      Caption         =   "Clear"
      Height          =   315
      Left            =   21240
      TabIndex        =   68
      Top             =   60
      Width           =   735
   End
   Begin MSAdodcLib.Adodc adoMediaSpec 
      Height          =   330
      Left            =   2340
      Top             =   240
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoMediaSpec"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox txtDestinationPassword 
      Height          =   315
      Left            =   16680
      TabIndex        =   56
      ToolTipText     =   "The clip bitrate"
      Top             =   6180
      Width           =   1935
   End
   Begin VB.TextBox txtDestinationLogin 
      Height          =   315
      Left            =   16680
      TabIndex        =   55
      ToolTipText     =   "The clip bitrate"
      Top             =   5820
      Width           =   1935
   End
   Begin VB.TextBox txtDestinationAddress 
      Height          =   315
      Left            =   16680
      TabIndex        =   54
      ToolTipText     =   "The clip filename"
      Top             =   5460
      Width           =   4455
   End
   Begin VB.ComboBox cmbDestination 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   16680
      TabIndex        =   53
      ToolTipText     =   "The Clip Codec"
      Top             =   5100
      Width           =   1935
   End
   Begin VB.TextBox txtSourcePassword 
      Height          =   315
      Left            =   16680
      TabIndex        =   52
      ToolTipText     =   "The clip bitrate"
      Top             =   4560
      Width           =   1935
   End
   Begin VB.TextBox txtSourceLogin 
      Height          =   315
      Left            =   16680
      TabIndex        =   51
      ToolTipText     =   "The clip bitrate"
      Top             =   4200
      Width           =   1935
   End
   Begin VB.TextBox txtSourceAddress 
      Height          =   315
      Left            =   16680
      TabIndex        =   50
      ToolTipText     =   "The clip filename"
      Top             =   3840
      Width           =   4455
   End
   Begin VB.ComboBox cmbSource 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   16680
      TabIndex        =   49
      ToolTipText     =   "The Clip Codec"
      Top             =   3480
      Width           =   1935
   End
   Begin VB.ComboBox cmbStreamType 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   1920
      TabIndex        =   47
      ToolTipText     =   "The Clip Codec"
      Top             =   2280
      Width           =   1935
   End
   Begin VB.ComboBox cmbInterlace 
      Height          =   315
      Left            =   1920
      TabIndex        =   43
      ToolTipText     =   "The Clip Codec"
      Top             =   4440
      Width           =   1935
   End
   Begin VB.ComboBox cmbCbrVbr 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   1920
      TabIndex        =   41
      ToolTipText     =   "The Clip Codec"
      Top             =   3000
      Width           =   1935
   End
   Begin VB.ComboBox cmbPasses 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   1920
      TabIndex        =   39
      ToolTipText     =   "The Clip Codec"
      Top             =   2640
      Width           =   1935
   End
   Begin VB.TextBox txtSortOrder 
      Height          =   315
      Left            =   16680
      TabIndex        =   37
      ToolTipText     =   "The clip bitrate"
      Top             =   2220
      Width           =   1935
   End
   Begin VB.TextBox txtAltLocation 
      Height          =   315
      Left            =   16680
      TabIndex        =   35
      ToolTipText     =   "The clip filename"
      Top             =   780
      Width           =   4455
   End
   Begin VB.ComboBox cmbPurpose 
      Height          =   315
      Left            =   1920
      TabIndex        =   33
      ToolTipText     =   "The Clip Codec"
      Top             =   1200
      Width           =   1935
   End
   Begin VB.ComboBox cmbDelivery 
      Height          =   315
      Left            =   1920
      TabIndex        =   29
      ToolTipText     =   "The Clip Codec"
      Top             =   840
      Width           =   1935
   End
   Begin VB.TextBox txtOtherSpecs 
      Height          =   1275
      Left            =   1920
      MultiLine       =   -1  'True
      TabIndex        =   16
      ToolTipText     =   "Notes for the clip"
      Top             =   7320
      Width           =   11175
   End
   Begin VB.ComboBox cmbGeometry 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   1920
      TabIndex        =   15
      ToolTipText     =   "The Clip Codec"
      Top             =   5160
      Width           =   1935
   End
   Begin VB.ComboBox cmbAspect 
      Height          =   315
      Left            =   1920
      TabIndex        =   14
      ToolTipText     =   "The Clip Codec"
      Top             =   4800
      Width           =   1935
   End
   Begin VB.TextBox txtAudioBitrate 
      Height          =   315
      Left            =   1920
      TabIndex        =   13
      ToolTipText     =   "The clip bitrate"
      Top             =   6240
      Width           =   1935
   End
   Begin VB.TextBox txtVideoBitrate 
      Height          =   315
      Left            =   1920
      TabIndex        =   12
      ToolTipText     =   "The clip bitrate"
      Top             =   5520
      Width           =   1935
   End
   Begin VB.TextBox txtBitrate 
      Height          =   315
      Left            =   1920
      TabIndex        =   11
      ToolTipText     =   "The clip bitrate"
      Top             =   6600
      Width           =   1935
   End
   Begin VB.ComboBox cmbFrameRate 
      Height          =   315
      Left            =   1920
      TabIndex        =   10
      ToolTipText     =   "The Clip Codec"
      Top             =   4080
      Width           =   1935
   End
   Begin VB.ComboBox cmbAudioCodec 
      Height          =   315
      Left            =   1920
      TabIndex        =   9
      ToolTipText     =   "The Clip Codec"
      Top             =   5880
      Width           =   1935
   End
   Begin VB.TextBox txtVertpixels 
      Height          =   315
      Left            =   1920
      TabIndex        =   8
      ToolTipText     =   "The number of vertical pixels"
      Top             =   3720
      Width           =   1935
   End
   Begin VB.TextBox txtHorizpixels 
      Height          =   315
      Left            =   1920
      TabIndex        =   7
      ToolTipText     =   "The number of horizontal pixels"
      Top             =   3360
      Width           =   1935
   End
   Begin VB.ComboBox cmbClipcodec 
      Height          =   315
      Left            =   1920
      TabIndex        =   6
      ToolTipText     =   "The Clip Codec"
      Top             =   1920
      Width           =   1935
   End
   Begin VB.ComboBox cmbClipformat 
      Height          =   315
      Left            =   1920
      TabIndex        =   5
      ToolTipText     =   "The Clip Format"
      Top             =   1560
      Width           =   1935
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3300
      TabIndex        =   2
      Top             =   8760
      Width           =   7515
      Begin VB.CommandButton cmdDeleteSpec 
         Caption         =   "Delete"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2760
         TabIndex        =   46
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3960
         TabIndex        =   45
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1560
         TabIndex        =   32
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6360
         TabIndex        =   4
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5160
         TabIndex        =   3
         Top             =   0
         Width           =   1155
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbMediaSpec 
      Bindings        =   "frmClipSpec.frx":0BC2
      DataField       =   "mediaspecname"
      Height          =   315
      Left            =   1920
      TabIndex        =   31
      Top             =   120
      Width           =   4035
      DataFieldList   =   "mediaspecname"
      MaxDropDownItems=   24
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "mediaspecID"
      Columns(0).Name =   "mediaspecID"
      Columns(0).DataField=   "mediaspecID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   7938
      Columns(1).Caption=   "mediaspecname"
      Columns(1).Name =   "mediaspecname"
      Columns(1).DataField=   "mediaspecname"
      Columns(1).FieldLen=   256
      _ExtentX        =   7117
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   16680
      TabIndex        =   65
      ToolTipText     =   "The company name"
      Top             =   60
      Width           =   3735
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "Name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   6588
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin VB.Label lblCaption 
      Caption         =   "0 transparent, 1 opaque"
      Height          =   255
      Index           =   65
      Left            =   18900
      TabIndex        =   82
      Top             =   1740
      Width           =   1875
   End
   Begin VB.Label lblCaption 
      Caption         =   "Graphic Overlay Opacity"
      Height          =   255
      Index           =   63
      Left            =   14760
      TabIndex        =   81
      Top             =   1740
      Width           =   1755
   End
   Begin VB.Label lblCaption 
      Caption         =   "Graphic Overlay Filename"
      Height          =   255
      Index           =   64
      Left            =   14760
      TabIndex        =   80
      Top             =   1380
      Width           =   1935
   End
   Begin VB.Label lblCaption 
      Caption         =   "(FFMPG Generic only)"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   66
      Left            =   20880
      TabIndex        =   79
      Top             =   1740
      Width           =   1695
   End
   Begin VB.Label lblAudioCodecFFMPEG 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   3960
      TabIndex        =   76
      Tag             =   "CLEARFIELDS"
      Top             =   5880
      Width           =   10155
   End
   Begin VB.Label lblVideoCodecFFMPEG 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   3960
      TabIndex        =   75
      Tag             =   "CLEARFIELDS"
      Top             =   1920
      Width           =   10095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Second Audio Track"
      Height          =   255
      Index           =   35
      Left            =   14760
      TabIndex        =   74
      Top             =   3000
      Width           =   1815
   End
   Begin VB.Label lblCaption 
      Caption         =   "First Audio Track"
      Height          =   255
      Index           =   34
      Left            =   14760
      TabIndex        =   72
      Top             =   2640
      Width           =   1815
   End
   Begin VB.Label lblCaption 
      Caption         =   "VOD Platform"
      Height          =   255
      Index           =   33
      Left            =   120
      TabIndex        =   70
      Top             =   7020
      Width           =   1455
   End
   Begin VB.Label lblCompanyID 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   20460
      TabIndex        =   67
      Top             =   60
      Width           =   675
   End
   Begin VB.Label lblCaption 
      Caption         =   "JCA Customer"
      Height          =   195
      Index           =   32
      Left            =   14760
      TabIndex        =   66
      Top             =   120
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Delivery Password"
      Height          =   255
      Index           =   31
      Left            =   14760
      TabIndex        =   64
      Top             =   6240
      Width           =   1815
   End
   Begin VB.Label lblCaption 
      Caption         =   "Delivery Login"
      Height          =   255
      Index           =   30
      Left            =   14760
      TabIndex        =   63
      Top             =   5880
      Width           =   1815
   End
   Begin VB.Label lblCaption 
      Caption         =   "Delivery Address"
      Height          =   255
      Index           =   29
      Left            =   14760
      TabIndex        =   62
      Top             =   5520
      Width           =   1815
   End
   Begin VB.Label lblCaption 
      Caption         =   "Eventual Media Delivery"
      Height          =   255
      Index           =   28
      Left            =   14760
      TabIndex        =   61
      Top             =   5160
      Width           =   1815
   End
   Begin VB.Label lblCaption 
      Caption         =   "Source Password"
      Height          =   255
      Index           =   27
      Left            =   14760
      TabIndex        =   60
      Top             =   4620
      Width           =   1815
   End
   Begin VB.Label lblCaption 
      Caption         =   "Source Login"
      Height          =   255
      Index           =   26
      Left            =   14760
      TabIndex        =   59
      Top             =   4260
      Width           =   1815
   End
   Begin VB.Label lblCaption 
      Caption         =   "Source Address"
      Height          =   255
      Index           =   25
      Left            =   14760
      TabIndex        =   58
      Top             =   3900
      Width           =   1815
   End
   Begin VB.Label lblCaption 
      Caption         =   "Original Media Source"
      Height          =   255
      Index           =   24
      Left            =   14760
      TabIndex        =   57
      Top             =   3540
      Width           =   1815
   End
   Begin VB.Label lblCaption 
      Caption         =   "Stream Type"
      Height          =   255
      Index           =   23
      Left            =   120
      TabIndex        =   48
      Top             =   2340
      Width           =   1755
   End
   Begin VB.Label lblCaption 
      Caption         =   "Interlace/Progressive"
      Height          =   255
      Index           =   22
      Left            =   120
      TabIndex        =   44
      Top             =   4500
      Width           =   1695
   End
   Begin VB.Label lblCaption 
      Caption         =   "CBR/VBR?"
      Height          =   255
      Index           =   21
      Left            =   120
      TabIndex        =   42
      Top             =   3060
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Number of Passes"
      Height          =   255
      Index           =   20
      Left            =   120
      TabIndex        =   40
      Top             =   2700
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "List Sorting Order"
      Height          =   255
      Index           =   19
      Left            =   14760
      TabIndex        =   38
      Top             =   2280
      Width           =   1815
   End
   Begin VB.Label lblCaption 
      Caption         =   "Usual Alt. Location"
      Height          =   255
      Index           =   18
      Left            =   14760
      TabIndex        =   36
      Top             =   840
      Width           =   1755
   End
   Begin VB.Label lblCaption 
      Caption         =   "Clip Purpose"
      Height          =   255
      Index           =   17
      Left            =   120
      TabIndex        =   34
      Top             =   1260
      Width           =   1755
   End
   Begin VB.Label lblCaption 
      Caption         =   "Delivery Method"
      Height          =   255
      Index           =   16
      Left            =   120
      TabIndex        =   30
      Top             =   900
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Other Specs"
      Height          =   255
      Index           =   15
      Left            =   120
      TabIndex        =   28
      Top             =   7380
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Geometry"
      Height          =   255
      Index           =   11
      Left            =   120
      TabIndex        =   27
      Top             =   5220
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Aspect Ratio"
      Height          =   255
      Index           =   10
      Left            =   120
      TabIndex        =   26
      Top             =   4860
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Audio Bitrate"
      Height          =   255
      Index           =   9
      Left            =   120
      TabIndex        =   25
      Top             =   6300
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Video Bitrate"
      Height          =   255
      Index           =   8
      Left            =   120
      TabIndex        =   24
      Top             =   5580
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Total Bitrate"
      Height          =   255
      Index           =   7
      Left            =   120
      TabIndex        =   23
      Top             =   6660
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Frame Rate"
      Height          =   255
      Index           =   6
      Left            =   120
      TabIndex        =   22
      Top             =   4140
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Audio Codec/Contents"
      Height          =   255
      Index           =   5
      Left            =   120
      TabIndex        =   21
      Top             =   5940
      Width           =   1755
   End
   Begin VB.Label lblCaption 
      Caption         =   "Vert Pixels"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   20
      Top             =   3780
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Horiz Pixels"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   19
      Top             =   3420
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Video Codec/Contents"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   18
      Top             =   1980
      Width           =   1755
   End
   Begin VB.Label lblCaption 
      Caption         =   "Media Format"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   17
      Top             =   1620
      Width           =   1455
   End
   Begin VB.Label lblMediaSpecID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   6000
      TabIndex        =   1
      Tag             =   "CLEARFIELDS"
      Top             =   120
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Caption         =   "Specification Name"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   180
      Width           =   1455
   End
End
Attribute VB_Name = "frmClipSpec"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmbAspect_GotFocus()
PopulateCombo "aspectratio", cmbAspect
HighLite cmbAspect
End Sub

Private Sub cmbAudioCodec_Click()
lblAudioCodecFFMPEG.Caption = GetDataSQL("SELECT information FROM xref WHERE category='audiocodec' AND description = '" & cmbAudioCodec.Text & "';")
End Sub

Private Sub cmbAudioCodec_GotFocus()
PopulateCombo "audiocodec", cmbAudioCodec
HighLite cmbAudioCodec
End Sub

Private Sub cmbClipcodec_Click()
lblVideoCodecFFMPEG.Caption = GetDataSQL("SELECT information FROM xref WHERE category='clipcodec' AND description = '" & cmbClipcodec.Text & "';")
End Sub

Private Sub cmbCompany_CloseUp()

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

Dim l_strSQL As String

l_strSQL = "SELECT * FROM mediaspec WHERE companyID = " & lblCompanyID.Caption & " ORDER BY fd_order, mediaspecname;"

adoMediaSpec.ConnectionString = g_strConnection
adoMediaSpec.RecordSource = l_strSQL
adoMediaSpec.Refresh

End Sub

Private Sub cmbVodPlatform_GotFocus()
PopulateCombo "vodplatform", cmbVodPlatform
HighLite cmbVodPlatform
End Sub

Private Sub cmdClear_Click()
ClearFields Me
End Sub

Private Sub cmbClipcodec_GotFocus()
PopulateCombo "clipcodec", cmbClipcodec
HighLite cmbClipcodec
End Sub

Private Sub cmbClipformat_GotFocus()
PopulateCombo "clipformat", cmbClipformat
HighLite cmbClipformat
End Sub

Private Sub cmbDelivery_GotFocus()
PopulateCombo "clipdelivery", cmbDelivery
HighLite cmbDelivery
End Sub

Private Sub cmbDestination_GotFocus()
PopulateCombo "clipdelivery", cmbDestination
HighLite cmbDestination
End Sub

Private Sub cmbFrameRate_GotFocus()
PopulateCombo "framerate", cmbFrameRate, "MEDIA"
HighLite cmbFrameRate
End Sub

Private Sub cmbGeometry_GotFocus()
PopulateCombo "geometry", cmbGeometry
HighLite cmbGeometry
End Sub

Private Sub cmbInterlace_GotFocus()
PopulateCombo "interlace", cmbInterlace
End Sub

Private Sub cmbMediaSpec_Click()

lblMediaSpecID.Caption = cmbMediaSpec.Columns("mediaspecID").Text

ShowClipSpec "mediaspec", frmClipSpec, Val(lblMediaSpecID.Caption), True, True

End Sub

Private Sub cmbMediaSpec_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then

    If Val(lblMediaSpecID.Caption) <> 0 Then
        ShowClipSpec "mediaspec", frmClipSpec, Val(lblMediaSpecID.Caption), True, True
    End If
    KeyAscii = 0
End If

End Sub

Private Sub cmbPasses_GotFocus()
PopulateCombo "encodepasses", cmbPasses
HighLite cmbPasses
End Sub

Private Sub cmbPurpose_GotFocus()
PopulateCombo "clippurpose", cmbPurpose
HighLite cmbPurpose
End Sub

Private Sub cmbCbrVbr_GotFocus()
PopulateCombo "cbrvbr", cmbCbrVbr
HighLite cmbCbrVbr

End Sub

Private Sub cmbSource_GotFocus()

PopulateCombo "clipdelivery", cmbSource
HighLite cmbSource

End Sub

Private Sub cmbStreamType_GotFocus()

PopulateCombo "clipstreamtype", cmbStreamType
HighLite cmbStreamType

End Sub

Private Sub cmdClearCompany_Click()

cmbCompany.Text = ""
lblCompanyID.Caption = "0"

Dim l_strSQL As String

l_strSQL = "SELECT * FROM mediaspec WHERE companyID = " & lblCompanyID.Caption & " ORDER BY fd_order, mediaspecname;"

adoMediaSpec.ConnectionString = g_strConnection
adoMediaSpec.RecordSource = l_strSQL
adoMediaSpec.Refresh


End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdDeleteSpec_Click()

DeleteMediaSpec

End Sub

Private Sub cmdPrint_Click()

Dim l_strSelectionFormula As String
Dim l_strReportFile As String

If Val(lblMediaSpecID.Caption) = 0 Then
    NoClipSelectedMessage
    Exit Sub
End If

If Not CheckAccess("/printrecordreport") Then Exit Sub

l_strSelectionFormula = "{mediaspec.mediaspecID} = " & lblMediaSpecID.Caption
l_strReportFile = g_strLocationOfCrystalReportFiles & "MediaSpec.rpt"

PrintCrystalReport l_strReportFile, l_strSelectionFormula, g_blnPreviewReport

End Sub

Private Sub cmdSave_Click()

SaveMediaSpec

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

l_strSQL = "SELECT * FROM mediaspec WHERE companyID = 0 ORDER BY fd_order, mediaspecname;"

adoMediaSpec.ConnectionString = g_strConnection
adoMediaSpec.RecordSource = l_strSQL
adoMediaSpec.Refresh

l_strSQL = "SELECT name, companyID FROM company WHERE (iscustomer = 1 OR isprospective = 1) AND system_active = 1 ORDER BY name"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub Form_Resize()

'frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
'frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120

End Sub

Private Sub txtAudioBitrate_GotFocus()
HighLite txtAudioBitrate
End Sub

Private Sub txtBitrate_GotFocus()
HighLite txtBitrate
End Sub

Private Sub txtBitrate_KeyPress(KeyAscii As Integer)

'If they hit the = key, then add up the video and audio bitrates to get a total.
If KeyAscii = 61 Then
    txtBitrate.Text = Val(txtAudioBitrate.Text) + Val(txtVideoBitrate.Text)
    KeyAscii = 0
End If

End Sub

Private Sub txtHorizpixels_GotFocus()
HighLite txtHorizpixels
End Sub

Private Sub txtVertpixels_GotFocus()
HighLite txtVertpixels
End Sub

Private Sub txtVideoBitrate_GotFocus()
HighLite txtVideoBitrate
End Sub
