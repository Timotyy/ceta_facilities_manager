VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmClipNetflix 
   Caption         =   "Clip Netflix"
   ClientHeight    =   9480
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   17625
   LinkTopic       =   "Form1"
   ScaleHeight     =   9480
   ScaleWidth      =   17625
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtiTunesTitle 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2100
      TabIndex        =   32
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   2955
      Width           =   3855
   End
   Begin VB.CommandButton cmdSaveiTunesPackage 
      Caption         =   "Save iTunes Data as Package"
      Height          =   330
      Left            =   2100
      TabIndex        =   31
      Top             =   8820
      Width           =   3855
   End
   Begin VB.TextBox txtiTunesISAN 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2100
      TabIndex        =   30
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   1860
      Width           =   3855
   End
   Begin VB.TextBox txtiTunesPreviewStartTime 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2100
      TabIndex        =   29
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   6180
      Width           =   3855
   End
   Begin VB.TextBox txtiTunesLongDescription 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   2100
      MultiLine       =   -1  'True
      TabIndex        =   28
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   4020
      Width           =   3855
   End
   Begin VB.TextBox txtiTunesCopyrightLine 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2100
      TabIndex        =   27
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   3660
      Width           =   3855
   End
   Begin VB.TextBox txtiTunesContainerPosition 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2100
      TabIndex        =   26
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   2580
      Width           =   3855
   End
   Begin VB.TextBox txtiTunesContainerID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2100
      TabIndex        =   25
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   2220
      Width           =   3855
   End
   Begin VB.TextBox txtiTunesEpProdNo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2100
      TabIndex        =   24
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   1500
      Width           =   3855
   End
   Begin VB.TextBox txtiTunesVendorID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2100
      TabIndex        =   23
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   780
      Width           =   3855
   End
   Begin VB.TextBox txtiTunesProvider 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2100
      TabIndex        =   22
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   420
      Width           =   3855
   End
   Begin VB.TextBox txtCropRight 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10740
      TabIndex        =   21
      Top             =   1080
      Width           =   1275
   End
   Begin VB.TextBox txtCropLeft 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10740
      TabIndex        =   20
      Top             =   720
      Width           =   1275
   End
   Begin VB.TextBox txtCropBottom 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10740
      TabIndex        =   19
      Top             =   360
      Width           =   1275
   End
   Begin VB.TextBox txtCropTop 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10740
      TabIndex        =   18
      Top             =   0
      Width           =   1275
   End
   Begin VB.ComboBox cmbJP 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      ItemData        =   "frmClipNetflix.frx":0000
      Left            =   7560
      List            =   "frmClipNetflix.frx":0002
      TabIndex        =   17
      ToolTipText     =   "The Version for the Tape"
      Top             =   2160
      Width           =   1515
   End
   Begin VB.ComboBox cmbUS 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      ItemData        =   "frmClipNetflix.frx":0004
      Left            =   7560
      List            =   "frmClipNetflix.frx":0006
      TabIndex        =   16
      ToolTipText     =   "The Version for the Tape"
      Top             =   1800
      Width           =   1515
   End
   Begin VB.ComboBox cmbUK 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      ItemData        =   "frmClipNetflix.frx":0008
      Left            =   7560
      List            =   "frmClipNetflix.frx":000A
      TabIndex        =   15
      ToolTipText     =   "The Version for the Tape"
      Top             =   1440
      Width           =   1515
   End
   Begin VB.ComboBox cmbFR 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      ItemData        =   "frmClipNetflix.frx":000C
      Left            =   7560
      List            =   "frmClipNetflix.frx":000E
      TabIndex        =   14
      ToolTipText     =   "The Version for the Tape"
      Top             =   1080
      Width           =   1515
   End
   Begin VB.ComboBox cmbDE 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      ItemData        =   "frmClipNetflix.frx":0010
      Left            =   7560
      List            =   "frmClipNetflix.frx":0012
      TabIndex        =   13
      ToolTipText     =   "The Version for the Tape"
      Top             =   720
      Width           =   1515
   End
   Begin VB.ComboBox cmbCA 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      ItemData        =   "frmClipNetflix.frx":0014
      Left            =   7560
      List            =   "frmClipNetflix.frx":0016
      TabIndex        =   12
      ToolTipText     =   "The Version for the Tape"
      Top             =   360
      Width           =   1515
   End
   Begin VB.ComboBox cmbAU 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      ItemData        =   "frmClipNetflix.frx":0018
      Left            =   7560
      List            =   "frmClipNetflix.frx":001A
      TabIndex        =   11
      ToolTipText     =   "The Version for the Tape"
      Top             =   0
      Width           =   1515
   End
   Begin VB.CommandButton cmdOutputiTunesXML 
      Caption         =   "iTunes XML File"
      Height          =   495
      Left            =   6180
      TabIndex        =   10
      Top             =   6660
      Width           =   1455
   End
   Begin VB.CommandButton cmdUpdateiTunes 
      Caption         =   "Update Ref Clip iTunes Data"
      Height          =   495
      Left            =   7740
      TabIndex        =   9
      ToolTipText     =   "Clear the form"
      Top             =   6660
      Width           =   1515
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save iTunes Data"
      Height          =   495
      Left            =   9360
      TabIndex        =   8
      Top             =   6660
      Width           =   1455
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   495
      Left            =   10920
      TabIndex        =   7
      Top             =   6660
      Width           =   1455
   End
   Begin VB.CheckBox chkCaptionsAvailability 
      Caption         =   "Check1"
      Height          =   255
      Left            =   2130
      TabIndex        =   6
      Top             =   7680
      Width           =   255
   End
   Begin VB.ComboBox cmbCaptionsReason 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2100
      TabIndex        =   5
      ToolTipText     =   "The Version for the Tape"
      Top             =   8340
      Width           =   3855
   End
   Begin VB.TextBox txtCaptionsClipID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2100
      TabIndex        =   4
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   7980
      Width           =   3855
   End
   Begin VB.TextBox txtiTunesTechComments 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   2100
      MultiLine       =   -1  'True
      TabIndex        =   3
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   5100
      Width           =   3855
   End
   Begin VB.TextBox txtiTunesVendorOfferCode 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2100
      TabIndex        =   2
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   1140
      Width           =   3855
   End
   Begin VB.CommandButton cmdOutputNetflixXML 
      Caption         =   "Netflix XML File"
      Height          =   495
      Left            =   6180
      TabIndex        =   1
      Top             =   7260
      Width           =   1455
   End
   Begin VB.CommandButton cmdOutputHawkXML 
      Caption         =   "Hawk XML File"
      Height          =   495
      Left            =   6180
      TabIndex        =   0
      Top             =   7860
      Width           =   1455
   End
   Begin MSComCtl2.DTPicker datiTunesReleaseDate 
      Height          =   315
      Left            =   2100
      TabIndex        =   33
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The date the tape was created"
      Top             =   3300
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   74055681
      CurrentDate     =   37870
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbiTunesPackage 
      Height          =   315
      Left            =   2100
      TabIndex        =   34
      ToolTipText     =   "The company this tape belongs to"
      Top             =   60
      Width           =   3855
      DataFieldList   =   "video_containerID"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "itunes_packageID"
      Columns(0).Name =   "itunes_packageID"
      Columns(0).DataField=   "itunes_packageID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   7329
      Columns(2).Caption=   "Video Container"
      Columns(2).Name =   "video_containerID"
      Columns(2).DataField=   "video_containerID"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2461
      Columns(3).Caption=   "Episode Number"
      Columns(3).Name =   "video_ep_prod_no"
      Columns(3).DataField=   "video_ep_prod_no"
      Columns(3).FieldLen=   256
      _ExtentX        =   6800
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   12632319
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "video_containerID"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnISO2A 
      Bindings        =   "frmClipNetflix.frx":001C
      Height          =   1155
      Left            =   6840
      TabIndex        =   35
      Tag             =   "CLEAR"
      Top             =   5100
      Width           =   4335
      DataFieldList   =   "country"
      ListAutoValidate=   0   'False
      _Version        =   196617
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   2
      Columns(0).Width=   6350
      Columns(0).Caption=   "Country"
      Columns(0).Name =   "Country"
      Columns(0).DataField=   "country"
      Columns(0).FieldLen=   256
      Columns(1).Width=   1244
      Columns(1).Caption=   "Code"
      Columns(1).Name =   "Code"
      Columns(1).DataField=   "iso2acode"
      Columns(1).FieldLen=   256
      _ExtentX        =   7646
      _ExtentY        =   2037
      _StockProps     =   77
      DataFieldToDisplay=   "country"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnAdvisory 
      Height          =   795
      Left            =   7320
      TabIndex        =   36
      Tag             =   "CLEAR"
      Top             =   3360
      Width           =   4155
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      MaxDropDownItems=   16
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   3200
      Columns(0).Caption=   "advisory"
      Columns(0).Name =   "advisory"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   7329
      _ExtentY        =   1402
      _StockProps     =   77
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnAdvisorySystem 
      Height          =   795
      Left            =   6480
      TabIndex        =   37
      Tag             =   "CLEAR"
      Top             =   2940
      Width           =   4155
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   3200
      Columns(0).Caption=   "advisorysystem"
      Columns(0).Name =   "advisorysystem"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   7329
      _ExtentY        =   1402
      _StockProps     =   77
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdProduct 
      Bindings        =   "frmClipNetflix.frx":0033
      Height          =   1695
      Left            =   6120
      TabIndex        =   38
      Top             =   4800
      Width           =   6255
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   7
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "itunes_productID"
      Columns(0).Name =   "itunes_productID"
      Columns(0).DataField=   "itunes_productID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "eventID"
      Columns(1).Name =   "eventID"
      Columns(1).DataField=   "eventID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   4710
      Columns(2).Caption=   "Territory"
      Columns(2).Name =   "territory"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "territorycode"
      Columns(3).Name =   "territorycode"
      Columns(3).DataField=   "territory"
      Columns(3).FieldLen=   256
      Columns(4).Width=   2408
      Columns(4).Caption=   "salesstartdate"
      Columns(4).Name =   "salesstartdate"
      Columns(4).DataField=   "salesstartdate"
      Columns(4).DataType=   7
      Columns(4).FieldLen=   256
      Columns(4).Style=   1
      Columns(5).Width=   2381
      Columns(5).Caption=   "Cleared For Sale"
      Columns(5).Name =   "clearedforsale"
      Columns(5).DataField=   "clearedforsale"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "Bundle Only"
      Columns(6).Name =   "bundleonly"
      Columns(6).DataField=   "bundleonly"
      Columns(6).FieldLen=   256
      _ExtentX        =   11033
      _ExtentY        =   2990
      _StockProps     =   79
      Caption         =   "iTunes Product Details"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdiTunesAdvisory 
      Bindings        =   "frmClipNetflix.frx":004C
      Height          =   1995
      Left            =   6120
      TabIndex        =   39
      Top             =   2640
      Width           =   6255
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   4
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "iTunes_Clip_AdvisoryID"
      Columns(0).Name =   "iTunes_Clip_AdvisoryID"
      Columns(0).DataField=   "iTunes_Clip_AdvisoryID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "eventID"
      Columns(1).Name =   "eventID"
      Columns(1).DataField=   "eventID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "Advisory System"
      Columns(2).Name =   "advisorysystem"
      Columns(2).DataField=   "advisorysystem"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "Advisory Item"
      Columns(3).Name =   "advisory"
      Columns(3).DataField=   "advisory"
      Columns(3).FieldLen=   256
      _ExtentX        =   11033
      _ExtentY        =   3519
      _StockProps     =   79
      Caption         =   "iTunes Content Advisory"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoAdvisory 
      Height          =   330
      Left            =   9360
      Top             =   8580
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoAdvisory"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoProduct 
      Height          =   330
      Left            =   9360
      Top             =   8880
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoProduct"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoISO2A 
      Height          =   330
      Left            =   9360
      Top             =   8280
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoISO2A"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoVideoLanguage 
      Height          =   330
      Left            =   2640
      Top             =   6540
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoVideoLanguage"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo txtiTunesVideoLanguage 
      Bindings        =   "frmClipNetflix.frx":0066
      Height          =   315
      Left            =   2100
      TabIndex        =   40
      ToolTipText     =   "The company this tape belongs to"
      Top             =   6540
      Width           =   3855
      DataFieldList   =   "description"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "description"
      Columns(0).FieldLen=   256
      Columns(1).Width=   8043
      Columns(1).Caption=   "information"
      Columns(1).Name =   "information"
      Columns(1).DataField=   "information"
      Columns(1).FieldLen=   256
      _ExtentX        =   6800
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "description"
   End
   Begin MSAdodcLib.Adodc adoMetadataLanguage 
      Height          =   330
      Left            =   2640
      Top             =   6900
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoMetadataLanguage"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo txtMetaDataLanguage 
      Bindings        =   "frmClipNetflix.frx":0085
      Height          =   315
      Left            =   2100
      TabIndex        =   41
      ToolTipText     =   "The company this tape belongs to"
      Top             =   6900
      Width           =   3855
      DataFieldList   =   "description"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "description"
      Columns(0).FieldLen=   256
      Columns(1).Width=   8043
      Columns(1).Caption=   "information"
      Columns(1).Name =   "information"
      Columns(1).DataField=   "information"
      Columns(1).FieldLen=   256
      _ExtentX        =   6800
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "description"
   End
   Begin MSAdodcLib.Adodc adoSpokenLocale 
      Height          =   330
      Left            =   2640
      Top             =   7260
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoSpokenLocale"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo txtiTunesOriginalSpokenLocale 
      Bindings        =   "frmClipNetflix.frx":00A7
      Height          =   315
      Left            =   2100
      TabIndex        =   42
      ToolTipText     =   "The company this tape belongs to"
      Top             =   7260
      Width           =   3855
      DataFieldList   =   "description"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "description"
      Columns(0).FieldLen=   256
      Columns(1).Width=   8043
      Columns(1).Caption=   "information"
      Columns(1).Name =   "information"
      Columns(1).DataField=   "information"
      Columns(1).FieldLen=   256
      _ExtentX        =   6800
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "description"
   End
   Begin VB.Label lblCaption 
      Caption         =   "MetaData Language"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   77
      Left            =   0
      TabIndex        =   75
      Top             =   6960
      Width           =   1935
   End
   Begin VB.Label lblCaption 
      Caption         =   "Original Spoken Locale"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   75
      Left            =   0
      TabIndex        =   74
      Top             =   7320
      Width           =   1935
   End
   Begin VB.Label lblCaption 
      Caption         =   "Video Language"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   64
      Left            =   0
      TabIndex        =   73
      Top             =   6600
      Width           =   1935
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   44
      Left            =   0
      TabIndex        =   72
      Top             =   3000
      Width           =   1935
   End
   Begin VB.Label lblCaption 
      Caption         =   "Select iTunes Package"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   88
      Left            =   0
      TabIndex        =   71
      Top             =   120
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "ISAN Number"
      Height          =   255
      Index           =   84
      Left            =   0
      TabIndex        =   70
      Top             =   1920
      Width           =   1935
   End
   Begin VB.Label lblCaption 
      Caption         =   "Preview Start Time"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   80
      Left            =   0
      TabIndex        =   69
      Top             =   6240
      Width           =   1935
   End
   Begin VB.Label lblCaption 
      Caption         =   "Description"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   76
      Left            =   0
      TabIndex        =   68
      Top             =   4080
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Copyright Line"
      Height          =   255
      Index           =   73
      Left            =   0
      TabIndex        =   67
      Top             =   3720
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Release Date"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   70
      Left            =   0
      TabIndex        =   66
      Top             =   3360
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Container Position"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   69
      Left            =   0
      TabIndex        =   65
      Top             =   2640
      Width           =   1935
   End
   Begin VB.Label lblCaption 
      Caption         =   "Container ID"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   68
      Left            =   0
      TabIndex        =   64
      Top             =   2280
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Episode Production Number"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   66
      Left            =   0
      TabIndex        =   63
      Top             =   1560
      Width           =   2235
   End
   Begin VB.Label lblCaption 
      Caption         =   "Package Vendor ID"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   65
      Left            =   0
      TabIndex        =   62
      Top             =   840
      Width           =   1875
   End
   Begin VB.Label lblCaption 
      Caption         =   "Provider"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   18
      Left            =   0
      TabIndex        =   61
      Top             =   480
      Width           =   1215
   End
   Begin VB.Label lblClipID 
      Height          =   195
      Left            =   12180
      TabIndex        =   60
      Top             =   60
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Crop Bottom"
      Height          =   195
      Index           =   91
      Left            =   9300
      TabIndex        =   59
      Top             =   420
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Crop Right"
      Height          =   195
      Index           =   90
      Left            =   9300
      TabIndex        =   58
      Top             =   1140
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Crop Top"
      Height          =   195
      Index           =   89
      Left            =   9300
      TabIndex        =   57
      Top             =   60
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Crop Left"
      Height          =   195
      Index           =   82
      Left            =   9300
      TabIndex        =   56
      Top             =   780
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "jp-tv rating"
      Height          =   255
      Index           =   85
      Left            =   6120
      TabIndex        =   55
      Top             =   2220
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "us-tv rating"
      Height          =   255
      Index           =   83
      Left            =   6120
      TabIndex        =   54
      Top             =   1860
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "uk-tv rating"
      Height          =   255
      Index           =   79
      Left            =   6120
      TabIndex        =   53
      Top             =   1500
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "fr-tv rating"
      Height          =   255
      Index           =   78
      Left            =   6120
      TabIndex        =   52
      Top             =   1140
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "de-tv rating"
      Height          =   255
      Index           =   72
      Left            =   6120
      TabIndex        =   51
      Top             =   780
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "ca-tv rating"
      Height          =   255
      Index           =   71
      Left            =   6120
      TabIndex        =   50
      Top             =   420
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "au-tv rating"
      Height          =   255
      Index           =   67
      Left            =   6120
      TabIndex        =   49
      Top             =   60
      Width           =   1215
   End
   Begin VB.Label lblCompanyID 
      Height          =   195
      Left            =   12180
      TabIndex        =   48
      Top             =   420
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Captions Available"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   0
      Left            =   0
      TabIndex        =   47
      Top             =   7680
      Width           =   1935
   End
   Begin VB.Label lblCaption 
      Caption         =   "Captions Non-Avail Reason"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   1
      Left            =   0
      TabIndex        =   46
      Top             =   8400
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Captions ClipID"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   2
      Left            =   0
      TabIndex        =   45
      Top             =   8040
      Width           =   1935
   End
   Begin VB.Label lblCaption 
      Caption         =   "Tech Comments"
      Height          =   255
      Index           =   3
      Left            =   0
      TabIndex        =   44
      Top             =   5160
      Width           =   1935
   End
   Begin VB.Label lblCaption 
      Caption         =   "Vendor Offer Code"
      Height          =   255
      Index           =   4
      Left            =   0
      TabIndex        =   43
      Top             =   1200
      Width           =   1935
   End
End
Attribute VB_Name = "frmClipNetflix"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub adoAdvisory_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

If Not adoAdvisory.Recordset.EOF Then
    If adoAdvisory.Recordset("advisorysystem") <> "" Then
        ddnAdvisory.RemoveAll
        PopulateCombo "iTunesAdvisory", ddnAdvisory, " AND FORMAT = '" & adoAdvisory.Recordset("advisorysystem") & "'"
        grdiTunesAdvisory.Columns("advisory").DropDownHwnd = ddnAdvisory.hWnd
    End If
End If

End Sub

Private Sub cmbAU_GotFocus()

PopulateCombo "iTunesRating", cmbAU, " AND format = 'au-tv'"
HighLite cmbAU

End Sub

Private Sub cmbCA_GotFocus()

PopulateCombo "iTunesRating", cmbCA, " AND format = 'ca-tv'"
HighLite cmbCA

End Sub

Private Sub cmbDE_GotFocus()

PopulateCombo "iTunesRating", cmbDE, " AND format = 'de-tv'"
HighLite cmbDE

End Sub
Private Sub cmbFR_GotFocus()

PopulateCombo "iTunesRating", cmbFR, " AND format = 'fr-tv'"
HighLite cmbFR

End Sub

Private Sub cmbJP_GotFocus()

PopulateCombo "iTunesRating", cmbJP, " AND format = 'jp-tv'"
HighLite cmbJP

End Sub

Private Sub cmbUK_GotFocus()

PopulateCombo "iTunesRating", cmbUK, " AND format = 'uk-tv'"
HighLite cmbUK

End Sub

Private Sub cmbUS_GotFocus()

PopulateCombo "iTunesRating", cmbUS, " AND format = 'us-tv'"
HighLite cmbUS

End Sub

Private Sub cmbiTunesPackage_Click()

Dim l_rstPackage As ADODB.Recordset, l_rstLookup As ADODB.Recordset, l_strSQL As String

If Val(lblClipID.Caption) <> 0 Then

    l_strSQL = "SELECT * FROM iTunes_package WHERE iTunes_packageID = " & Val(cmbiTunesPackage.Columns("iTunes_packageID").Text)
    Set l_rstPackage = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    If l_rstPackage.RecordCount > 0 Then
    
        If l_rstPackage("video_type") = "tv" Then
            txtiTunesProvider.Text = Trim(" " & l_rstPackage("provider"))
            txtiTunesVendorID.Text = Trim(" " & l_rstPackage("video_vendorID"))
            txtiTunesVendorOfferCode.Text = Trim(" " & l_rstPackage("vendor_offer_code"))
            txtiTunesEpProdNo.Text = Trim(" " & l_rstPackage("video_ep_prod_no"))
            txtiTunesContainerID.Text = Trim(" " & l_rstPackage("video_containerID"))
            txtiTunesTitle.Text = UTF8_Decode(Trim(" " & l_rstPackage("video_title")))
            txtiTunesContainerPosition.Text = Trim(" " & l_rstPackage("video_containerposition"))
            datiTunesReleaseDate.Value = Trim(" " & l_rstPackage("video_releasedate"))
            txtiTunesCopyrightLine.Text = Trim(" " & l_rstPackage("video_copyright_cline"))
            txtiTunesLongDescription.Text = UTF8_Decode(Trim(" " & l_rstPackage("video_longdescription")))
            txtiTunesVideoLanguage.Text = Trim(" " & l_rstPackage("video_language"))
            txtiTunesOriginalSpokenLocale.Text = Trim(" " & l_rstPackage("originalspokenlocale"))
            txtMetaDataLanguage.Text = Trim(" " & l_rstPackage("metadata_language"))
            txtiTunesPreviewStartTime.Text = Trim(" " & l_rstPackage("video_previewstarttime"))
            txtiTunesISAN.Text = Trim(" " & l_rstPackage("video_ISAN"))
            cmbAU.Text = Trim(" " & l_rstPackage("rating_au"))
            cmbCA.Text = Trim(" " & l_rstPackage("rating_ca"))
            cmbDE.Text = Trim(" " & l_rstPackage("rating_de"))
            cmbFR.Text = Trim(" " & l_rstPackage("rating_fr"))
            cmbUK.Text = Trim(" " & l_rstPackage("rating_uk"))
            cmbUS.Text = Trim(" " & l_rstPackage("rating_us"))
            cmbJP.Text = Trim(" " & l_rstPackage("rating_jp"))
            chkCaptionsAvailability = GetFlag(l_rstPackage("captionsavailable"))
            cmbCaptionsReason = Trim(" " & l_rstPackage("captionsreason"))
            txtCropTop.Text = Trim(" " & l_rstPackage("fullcroptop"))
            txtCropLeft.Text = Trim(" " & l_rstPackage("fullcropleft"))
            txtCropRight.Text = Trim(" " & l_rstPackage("fullcropright"))
            txtCropBottom.Text = Trim(" " & l_rstPackage("fullcropbottom"))
        
            ExecuteSQL "DELETE FROM itunes_clip_advisory WHERE eventID = " & Val(lblClipID.Caption) & ";", g_strExecuteError
            CheckForSQLError
            l_strSQL = "SELECT * FROM itunes_clip_advisory WHERE iTunes_packageID = " & cmbiTunesPackage.Columns("iTunes_packageID").Text & ";"
            Set l_rstLookup = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            
            If l_rstLookup.RecordCount > 0 Then
                l_rstLookup.MoveFirst
                Do While Not l_rstLookup.EOF
                    l_strSQL = "INSERT INTO itunes_clip_advisory (eventID, advisorysystem, advisory) VALUES ("
                    l_strSQL = l_strSQL & Val(lblClipID.Caption) & ", "
                    l_strSQL = l_strSQL & "'" & Trim(" " & l_rstLookup("advisorysystem")) & "', "
                    l_strSQL = l_strSQL & "'" & Trim(" " & l_rstLookup("advisory")) & "');"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    l_rstLookup.MoveNext
                Loop
            End If
            l_rstLookup.Close
            Set l_rstLookup = Nothing
            adoAdvisory.Refresh
        
            ExecuteSQL "DELETE FROM itunes_product WHERE eventID = " & Val(lblClipID.Caption) & ";", g_strExecuteError
            CheckForSQLError
            l_strSQL = "SELECT * FROM itunes_product WHERE iTunes_packageID = " & cmbiTunesPackage.Columns("iTunes_packageID").Text & ";"
            Set l_rstLookup = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            
            If l_rstLookup.RecordCount > 0 Then
                l_rstLookup.MoveFirst
                Do While Not l_rstLookup.EOF
                    l_strSQL = "INSERT INTO itunes_product (eventID, territory, salesstartdate, clearedforsale, bundleonly) VALUES ("
                    l_strSQL = l_strSQL & Val(lblClipID.Caption) & ", "
                    l_strSQL = l_strSQL & "'" & Trim(" " & l_rstLookup("territory")) & "', "
                    l_strSQL = l_strSQL & "'" & FormatSQLDate(l_rstLookup("salesstartdate")) & "', "
                    l_strSQL = l_strSQL & "'" & Trim(" " & l_rstLookup("clearedforsale")) & "', "
                    l_strSQL = l_strSQL & "'" & l_rstLookup("bundleonly") & "');"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    l_rstLookup.MoveNext
                Loop
            End If
            l_rstLookup.Close
            Set l_rstLookup = Nothing
            adoProduct.Refresh
            l_rstPackage("packageused") = 1
            l_rstPackage.Update
        End If
    End If
    
    l_rstPackage.Close
    Set l_rstPackage = Nothing

End If

End Sub

Private Sub cmdClose_Click()

Me.Hide
Unload Me

End Sub

Private Sub cmdOutputHawkXML_Click()

Dim l_strFileNameToSave, l_strSQL As String, l_rstLookup As ADODB.Recordset, l_blnUSProduct As Boolean

If lblClipID.Caption <> "" Then

   ' If frmClipControl.txtMD5Checksum.Text = "" Then
    '    MsgBox "The Checksum must be made for files to be sent to iTunes.", vbCritical, "Cannot Make iTunes XML"
    '    Exit Sub
   ' End If
    
'    If txtiTunesProvider.Text = "" Or txtiTunesVendorID.Text = "" _
'    Or txtiTunesEpProdNo.Text = "" Or txtiTunesContainerID.Text = "" Or txtiTunesContainerPosition.Text = "" _
'    Or datiTunesReleaseDate.Value = Null Or txtiTunesLongDescription.Text = "" _
'    Or Val(txtiTunesPreviewStartTime.Text) = 0 _
'    Or adoProduct.Recordset.RecordCount <= 0 Or txtiTunesOriginalSpokenLocale.Text = "" Then
 '       MsgBox "iTunes compulsory data has not all been entered.", vbCritical, "Cannot make iTunes XML"
 '       Exit Sub
   ' End If
    
    If chkCaptionsAvailability.Value <> 0 Then
        If Val(txtCaptionsClipID.Text) = 0 Then
            MsgBox "iTunes compulsory data has not all been entered.", vbCritical, "Cannot make iTunes XML"
          '  Exit Sub
        End If
        If GetData("events", "md5checksum", "eventID", Val(txtCaptionsClipID.Text)) = 0 Then
            MsgBox "iTunes compulsory data has not all been entered.", vbCritical, "Cannot make iTunes XML"
          '  Exit Sub
        End If
    End If
    cmdSave.Value = True
    
    If frmClipControl.lblFormat.Caption = "DISCSTORE" Then
        MDIForm1.dlgMain.InitDir = GetData("library", "subtitle", "libraryID", frmClipControl.lblLibraryID.Caption) & "\" & frmClipControl.txtAltFolder.Text
        MDIForm1.dlgMain.Filter = "XML files|*.xml"
        MDIForm1.dlgMain.Filename = "metadata.xml"
        
        MDIForm1.dlgMain.ShowSave
        
        l_strFileNameToSave = MDIForm1.dlgMain.Filename
        
        If l_strFileNameToSave <> "" And Right(l_strFileNameToSave, 5) <> "*.xml" Then
        
            Open l_strFileNameToSave For Output As 1
            Print #1, "<?xml version=""1.0"" encoding=""UTF-8""?>"
       Print #1, "<ADI>"
       Print #1, Chr(9) & "<Metadata>"
       Print #1, Chr(9) & Chr(9) & "<AMS Asset_Class=""; package; "" Asset_ID=""; qpmi201400000001; "" Asset_Name=""; Some; Show; S01; Ep; 1; package; "" Creation_Date=""; 2014 - 1 - 19; !"" Update_Time=""; 2014 - 1 - 19 ''T''11:52:01"" Description=""" & UTF8_Encode(XMLSanitise(txtiTunesLongDescription.Text)) & """ Product=""SVOD"" Provider=""QuickPlay Media"" Provider_ID=""quickplay.com"" Version_Major=""1"" Version_Minor=""1""/>"
       Print #1, Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Metadata_Spec_Version"" Value=""CableLabsVOD1.1""/>"
       Print #1, Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Provider_Content_Tier"" Value=""" & txtiTunesProvider.Text & """/>"
       Print #1, Chr(9) & "</Metadata>"
        Print #1, Chr(9) & "<Asset>"
       Print #1, Chr(9) & Chr(9) & "<Metadata>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<AMS Asset_Class=""title"" Asset_ID=""; qpmi201400000002; "" Asset_Name=""; Some; Show; S01; Ep; 01; package; "" Creation_Date=""; 2014 - 1 - 19; !"" Description=""" & UTF8_Encode(XMLSanitise(txtiTunesLongDescription.Text)) & """ Product=""SVOD"" Provider=""QuickPlay Media"" Provider_ID=""quickplay.com"" Version_Major=""1"" Version_Minor=""1""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Type"" Value=""title""/>"
       
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Title_Brief"" Value=""" & UTF8_Encode(XMLSanitise(txtiTunesTitle.Text)) & """/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Title"" Value=""" & UTF8_Encode(XMLSanitise(txtiTunesTitle.Text)) & """/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Summary_Long"" Value=""" & UTF8_Encode(XMLSanitise(txtiTunesLongDescription.Text)) & """/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Summary_Short"" Value=""" & UTF8_Encode(XMLSanitise(txtiTunesLongDescription.Text)) & """/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Rating"" Value=""U ""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Closed_Captioning"" Value=""Y""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Run_Time"" Value=""00:29:00""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Display_Run_Time"" Value=""00:29""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Year"" Value=""2012""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Country_of_Origin"" Value=""Canada""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Director"" Value=""Lastname,Firstname""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Actors"" Value=""Lastname,Firstname""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Actors"" Value=""Lastname,Firstname""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Genre"" Value=""Drama""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Billing_ID"" Value=""11111""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Category"" Value=""/TVShows""/>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Licensing_Window_Start"" Value=""2014-01-01T00:00:00""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Licensing_Window_End"" Value=""2014-12-31T00:00:00""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Provider_QA_Contact"" Value=""qa@quickplay.com""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Subscription_Packages"" Value=""package1;package2;package3""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Show_Type"" Value=""Series""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Series_Title"" Value=""" & UTF8_Encode(XMLSanitise(txtiTunesTitle.Text)) & """/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Series_ID"" Value=""SST""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Episode_Name"" Value=""Name 1""/>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Season_Number"" Value=""1""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Episode_Number"" Value=""" & XMLSanitise(txtiTunesEpProdNo.Text) & """/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Keywords"" Value=""keyword1, keyword2, keyword3""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Language"" Value=""eng""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Country"" Value=""IN""/>"
       
       Print #1, Chr(9) & Chr(9) & "</Metadata>"
        Print #1, Chr(9) & Chr(9) & "<Asset>"
       Print #1, Chr(9) & Chr(9) & "<Metadata>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<AMS Asset_Class=""movie"" Asset_ID=""; qpmi201400000003; "" Asset_Name=""; Some; Show; S01; Ep; 01; package; "" Creation_Date=""; 2014 - 1 - 19; !"" Description=""Some Show S01 Ep 01"" Product=""SVOD"" Provider=""QuickPlay Media"" Provider_ID=""quickplay.com"" Version_Major=""1"" Version_Minor=""1""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Type"" Value=""movie""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Audio_Type"" Value=""Stereo""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Languages"" Value=""eng""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Languages"" Value=""asm""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Languages"" Value=""tna""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Codec"" Value=""H264""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Resolution"" Value=""480i""/>       "
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Content_FileSize"" Value=""1678924756""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Content_CheckSum"" Value=""e653cffaabc93f4e107b31bcea33181e""/>"
       Print #1, Chr(9) & Chr(9) & "</Metadata>"
        Print #1, Chr(9) & Chr(9) & "<Content Value=""qpmi201400000001-feature.ts""/>"
       Print #1, Chr(9) & Chr(9) & "</Asset>"
        Print #1, Chr(9) & Chr(9) & "<Asset>"
       Print #1, Chr(9) & Chr(9) & "<Metadata>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<AMS Asset_Class=""movie"" Asset_ID=""; qpmi201400000004; "" Asset_Name=""; Some; Show; S01; Ep; 01; package; "" Creation_Date=""; 2014 - 1 - 19; !"" Description=""Some Show S01 Ep 01"" Product=""SVOD"" Provider=""QuickPlay Media"" Provider_ID=""quickplay.com"" Version_Major=""1"" Version_Minor=""1""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Type"" Value=""poster""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Image_Aspect_Ratio"" Value=""; 530; x764; ""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Content_FileSize"" Value=""25434756""/>       "
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Content_CheckSum"" Value=""e653cffaabc93f4e107b31bcea33181e""/>       "
        Print #1, Chr(9) & Chr(9) & "</Metadata>"
        Print #1, Chr(9) & Chr(9) & "<Content Value=""qpmi201400000001-poster.jpg""/>"
       Print #1, Chr(9) & Chr(9) & "</Asset>"
        Print #1, Chr(9) & Chr(9) & "<Asset>"
       Print #1, Chr(9) & Chr(9) & "<Metadata>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<AMS Asset_Class=""movie"" Asset_ID=""; qpmi201400000005; "" Asset_Name=""; Some; Show; S01; Ep; 01; package; "" Creation_Date=""; 2014 - 1 - 19; !"" Description=""Some Show S01 Ep 01"" Product=""SVOD"" Provider=""QuickPlay Media"" Provider_ID=""quickplay.com"" Version_Major=""1"" Version_Minor=""1""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Type"" Value=""box_cover""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Image_Aspect_Ratio"" Value=""530x764""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Content_FileSize"" Value=""""/>       "
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Content_CheckSum"" Value=""""/>       "
        Print #1, Chr(9) & Chr(9) & "</Metadata>"
        Print #1, Chr(9) & Chr(9) & "<Content Value=""qpmi201400000001-poster.jpg""/>"
       Print #1, Chr(9) & Chr(9) & "</Asset>"
        Print #1, Chr(9) & Chr(9) & "<Asset>"
       Print #1, Chr(9) & Chr(9) & "<Metadata>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<AMS Asset_Class=""movie"" Asset_ID=""; qpmi201400000008; "" Asset_Name=""; Some; Show; S01; Ep; 01; package; "" Creation_Date=""; 2014 - 1 - 19; !"" Description=""Some Show S01 Ep 01"" Product=""SVOD"" Provider=""QuickPlay Media"" Provider_ID=""quickplay.com"" Version_Major=""1"" Version_Minor=""1""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Type"" Value=""translation""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Language"" Value=""hin""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Title"" Value=""??? ?????? S01 Ep 01""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Summary_Long"" Value=""?? ???? ?????""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Summary_Short"" Value=""?? ????????? ?????""/>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Keywords"" Value=""??????1, ??????2, ??????3""/>"
         Print #1, Chr(9) & Chr(9) & "</Metadata>"
       Print #1, Chr(9) & Chr(9) & "</Asset>"
        Print #1, Chr(9) & Chr(9) & "<Asset>"
       Print #1, Chr(9) & Chr(9) & "<Metadata>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<AMS Asset_Class=""movie"" Asset_ID=""; qpmi201400000008; "" Asset_Name=""; Some; Show; S01; Ep; 01; package; "" Creation_Date=""; 2014 - 1 - 19; !"" Description=""Some Show S01 Ep 01"" Product=""SVOD"" Provider=""QuickPlay Media"" Provider_ID=""quickplay.com"" Version_Major=""1"" Version_Minor=""1""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Type"" Value=""translation""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Language"" Value=""tna""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Title"" Value=""??? ?????? S01 Ep 01""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Summary_Long"" Value=""?? ???? ?????""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Summary_Short"" Value=""?? ????????? ?????""/>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Keywords"" Value=""??????1, ??????2, ??????3""/>"
         Print #1, Chr(9) & Chr(9) & "</Metadata>"
       Print #1, Chr(9) & Chr(9) & "</Asset>"
        Print #1, Chr(9) & Chr(9) & "<Asset>"
       Print #1, Chr(9) & Chr(9) & "<Metadata>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<AMS Asset_Class=""movie"" Asset_ID=""; qpmi201400000008; "" Asset_Name=""; Some; Show; S01; Ep; 01; package; "" Creation_Date=""; 2014 - 1 - 19; !"" Description=""Some Show S01 Ep 01"" Product=""SVOD"" Provider=""QuickPlay Media"" Provider_ID=""quickplay.com"" Version_Major=""1"" Version_Minor=""1""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Type"" Value=""SMPTE-TT""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Source_Asset_Class"" Value=""movie""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Language"" Value=""eng""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Offset"" Value=""00:00:00:00""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""FrameRate"" Value=""29.97""/>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""dropFrame"" Value=""N""/>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Content_FileSize"" Value=""20114""/>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Content_CheckSum"" Value=""e653cffaabc93f4e107b31bcea33181e""/>"
        Print #1, Chr(9) & Chr(9) & "</Metadata>"
        Print #1, Chr(9) & Chr(9) & "<Content Value=""qpmi201400000005_cap_eng.xml""/>"
       Print #1, Chr(9) & Chr(9) & "</Asset>"
        Print #1, Chr(9) & Chr(9) & "<Asset>"
       Print #1, Chr(9) & Chr(9) & "<Metadata>"
         Print #1, Chr(9) & Chr(9) & Chr(9) & "<AMS Asset_Class=""movie"" Asset_ID=""; qpmi201400000007; "" Asset_Name=""; Some; Show; S01; Ep; 01; package; "" Creation_Date=""; 2014 - 1 - 19; !"" Description=""Some Show S01 Ep 01"" Product=""SVOD"" Provider=""QuickPlay Media"" Provider_ID=""quickplay.com"" Version_Major=""1"" Version_Minor=""1""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Type"" Value=""SRT""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Source_Asset_Class"" Value=""movie""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Language"" Value=""tna""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Offset"" Value=""00:00:00:00""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""FrameRate"" Value=""29.97""/>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""dropFrame"" Value=""Y""/>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Content_FileSize"" Value=""20114""/>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Content_CheckSum"" Value=""e653cffaabc93f4e107b31bcea33181e""/>"
        Print #1, Chr(9) & Chr(9) & "</Metadata>"
        Print #1, Chr(9) & Chr(9) & "<Content Value=""qpmi201400000005_cap_tna.srt""/>"
       Print #1, Chr(9) & Chr(9) & "</Asset>"
        Print #1, Chr(9) & Chr(9) & "<Asset>"
       Print #1, Chr(9) & Chr(9) & "<Metadata>"
         Print #1, Chr(9) & Chr(9) & Chr(9) & "<AMS Asset_Class=""movie"" Asset_ID=""; qpmi201400000007; "" Asset_Name=""; Some; Show; S01; Ep; 01; package; "" Creation_Date=""; 2014 - 1 - 19; !"" Description=""Some Show S01 Ep 01"" Product=""SVOD"" Provider=""QuickPlay Media"" Provider_ID=""quickplay.com"" Version_Major=""1"" Version_Minor=""1""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Type"" Value=""regional""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Country"" Value=""IN""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Rating"" Value=""G""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Licensing_Window_Start"" Value=""2014-02-01T00:00:00""/>"
       Print #1, Chr(9) & Chr(9) & Chr(9) & "<App_Data App=""SVOD"" Name=""Licensing_Window_End"" Value=""2015-01-31T00:00:00""/>"
        Print #1, Chr(9) & Chr(9) & "</Metadata>"
       Print #1, Chr(9) & Chr(9) & "</Asset>"
        Print #1, Chr(9) & "</Asset>"
       Print #1, "</ADI>"
            Close #1

      
        End If
    End If
End If

End Sub

Private Sub cmdOutputiTunesXML_Click()

Dim l_strFileNameToSave, l_strSQL As String, l_rstLookup As ADODB.Recordset, l_blnUSProduct As Boolean

If lblClipID.Caption <> "" Then

    If frmClipControl.txtMD5Checksum.Text = "" Then
        MsgBox "The Checksum must be made for files to be sent to iTunes.", vbCritical, "Cannot Make iTunes XML"
        Exit Sub
    End If
    
    If txtiTunesProvider.Text = "" Or txtiTunesVendorID.Text = "" _
    Or txtiTunesEpProdNo.Text = "" Or txtiTunesContainerID.Text = "" Or txtiTunesContainerPosition.Text = "" _
    Or datiTunesReleaseDate.Value = Null Or txtiTunesLongDescription.Text = "" _
    Or Val(txtiTunesPreviewStartTime.Text) = 0 _
    Or adoProduct.Recordset.RecordCount <= 0 Or txtiTunesOriginalSpokenLocale.Text = "" Then
        MsgBox "iTunes compulsory data has not all been entered.", vbCritical, "Cannot make iTunes XML"
        Exit Sub
    End If
    
    If chkCaptionsAvailability.Value <> 0 Then
        If Val(txtCaptionsClipID.Text) = 0 Then
            MsgBox "iTunes compulsory data has not all been entered.", vbCritical, "Cannot make iTunes XML"
            Exit Sub
        End If
        If GetData("events", "md5checksum", "eventID", Val(txtCaptionsClipID.Text)) = 0 Then
            MsgBox "iTunes compulsory data has not all been entered.", vbCritical, "Cannot make iTunes XML"
            Exit Sub
        End If
    End If
    cmdSave.Value = True
    
    If frmClipControl.lblFormat.Caption = "DISCSTORE" Then
        MDIForm1.dlgMain.InitDir = GetData("library", "subtitle", "libraryID", frmClipControl.lblLibraryID.Caption) & "\" & frmClipControl.txtAltFolder.Text
        MDIForm1.dlgMain.Filter = "XML files|*.xml"
        MDIForm1.dlgMain.Filename = "metadata.xml"
        
        MDIForm1.dlgMain.ShowSave
        
        l_strFileNameToSave = MDIForm1.dlgMain.Filename
        
        If l_strFileNameToSave <> "" And Right(l_strFileNameToSave, 5) <> "*.xml" Then
        
            Open l_strFileNameToSave For Output As 1
            Print #1, "<?xml version=""1.0"" encoding=""UTF-8""?>"
            Print #1, "<package xmlns=""http://apple.com/itunes/importer"" version=""tv5.2"">"
            If txtiTunesTechComments.Text <> "" Then
                Print #1, Chr(9) & "<comments>" & UTF8_Encode(XMLSanitise(txtiTunesTechComments.Text)) & "</comments>"
            End If
            Print #1, Chr(9) & "<provider>" & txtiTunesProvider.Text & "</provider>"
            If txtMetaDataLanguage.Text <> "" Then
                Print #1, Chr(9) & "<language>" & txtMetaDataLanguage.Text & "</language>"
            Else
                Print #1, Chr(9) & "<language>en-GB</language>"
            End If
            Print #1, Chr(9) & "<video>"
            
            Print #1, Chr(9) & Chr(9) & "<type>tv</type>"
            Print #1, Chr(9) & Chr(9) & "<vendor_id>" & XMLSanitise(txtiTunesVendorID.Text) & "</vendor_id>"
            If txtiTunesVendorOfferCode.Text <> "" Then
                Print #1, Chr(9) & Chr(9) & "<vendor_offer_code>" & XMLSanitise(txtiTunesVendorOfferCode.Text) & "</vendor_offer_code>"
            End If
            If txtiTunesISAN.Text <> "" Then
                Print #1, Chr(9) & Chr(9) & "<isan>" & txtiTunesISAN.Text & "</isan>"
            End If
            Print #1, Chr(9) & Chr(9) & "<episode_production_number>" & XMLSanitise(txtiTunesEpProdNo.Text) & "</episode_production_number>"
            Print #1, Chr(9) & Chr(9) & "<original_spoken_locale>" & txtiTunesOriginalSpokenLocale.Text & "</original_spoken_locale>"
            Print #1, Chr(9) & Chr(9) & "<title>" & UTF8_Encode(XMLSanitise(txtiTunesTitle.Text)) & "</title>"
            Print #1, Chr(9) & Chr(9) & "<container_id>" & XMLSanitise(txtiTunesContainerID.Text) & "</container_id>"
            Print #1, Chr(9) & Chr(9) & "<container_position>" & txtiTunesContainerPosition.Text & "</container_position>"
            Print #1, Chr(9) & Chr(9) & "<release_date>" & Format(datiTunesReleaseDate.Value, "YYYY-MM-DD") & "</release_date>"
            If adoAdvisory.Recordset.RecordCount > 0 Or cmbAU.Text <> "" Or cmbCA.Text <> "" Or cmbDE.Text <> "" Or cmbFR.Text <> "" _
                Or cmbUK.Text <> "" Or cmbUS.Text <> "" Or cmbJP.Text <> "" Then
                
                Print #1, Chr(9) & Chr(9) & "<ratings>"
                
                If cmbAU.Text <> "" Then Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""au-tv"">" & cmbAU.Text & "</rating>"
                If cmbCA.Text <> "" Then Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""ca-tv"">" & cmbCA.Text & "</rating>"
                If cmbDE.Text <> "" Then Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""de-tv"">" & cmbDE.Text & "</rating>"
                If cmbFR.Text <> "" Then Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""fr-tv"">" & cmbFR.Text & "</rating>"
                If cmbUK.Text <> "" Then Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""uk-tv"">" & cmbUK.Text & "</rating>"
                If cmbUS.Text <> "" Then Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""us-tv"">" & cmbUS.Text & "</rating>"
                If cmbJP.Text <> "" Then Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""jp-tv"">" & cmbJP.Text & "</rating>"
                
                If adoAdvisory.Recordset.RecordCount > 0 Then
                    l_strSQL = adoAdvisory.RecordSource
                    Set l_rstLookup = ExecuteSQL(l_strSQL, g_strExecuteError)
                    CheckForSQLError
                    l_rstLookup.MoveFirst
                    Do While Not l_rstLookup.EOF
                        Print #1, Chr(9) & Chr(9) & Chr(9) & "<advisory system=""" & l_rstLookup("advisorysystem") & """>" & l_rstLookup("advisory") & "</advisory>"
                        l_rstLookup.MoveNext
                    Loop
                    l_rstLookup.Close
                    Set l_rstLookup = Nothing
                End If
                
                Print #1, Chr(9) & Chr(9) & "</ratings>"
            
            End If

            If txtiTunesCopyrightLine.Text <> "" Then
                Print #1, Chr(9) & Chr(9) & "<copyright_cline>" & XMLSanitise(txtiTunesCopyrightLine.Text) & "</copyright_cline>"
            End If
            Print #1, Chr(9) & Chr(9) & "<description>" & UTF8_Encode(XMLSanitise(txtiTunesLongDescription.Text)) & "</description>"
            l_blnUSProduct = False
            If adoProduct.Recordset.RecordCount > 0 Then
                adoProduct.Recordset.MoveFirst
                Do While Not adoProduct.Recordset.EOF
                    If adoProduct.Recordset("territory") = "US" Then
                        l_blnUSProduct = True
                        Exit Do
                    End If
                    adoProduct.Recordset.MoveNext
                Loop
            End If
            If l_blnUSProduct = True Or chkCaptionsAvailability.Value <> 0 Then
                Print #1, Chr(9) & Chr(9) & "<accessibility_info>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & "<accessibility role=""captions"" ";
                If chkCaptionsAvailability.Value <> 0 Then
                    Print #1, "available=""true""/>"
                Else
                    Print #1, "available=""false"" reason_code=""" & cmbCaptionsReason.Text & """/>"
                End If
                Print #1, Chr(9) & Chr(9) & "</accessibility_info>"
            End If
            Print #1, Chr(9) & Chr(9) & "<assets>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<asset type=""full"">"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<data_file>"
            If txtiTunesVideoLanguage.Text <> "" Then
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & txtiTunesVideoLanguage.Text & """/>"
            End If
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<file_name>" & frmClipControl.txtClipfilename.Text & "</file_name>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<size>" & Format(frmClipControl.lblFileSize.Caption, "#") & "</size>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<checksum type=""md5"">" & frmClipControl.txtMD5Checksum.Text & "</checksum>"
            If Val(txtCropTop.Text) <> 0 And LCase(Right(frmClipControl.txtClipfilename.Text, 4)) = ".mov" Then
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<attribute name=""crop.top"">" & Val(txtCropTop.Text) & "</attribute>"
            End If
            If Val(txtCropBottom.Text) <> 0 And LCase(Right(frmClipControl.txtClipfilename.Text, 4)) = ".mov" Then
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<attribute name=""crop.bottom"">" & Val(txtCropBottom.Text) & "</attribute>"
            End If
            If Val(txtCropLeft.Text) <> 0 And LCase(Right(frmClipControl.txtClipfilename.Text, 4)) = ".mov" Then
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<attribute name=""crop.left"">" & Val(txtCropLeft.Text) & "</attribute>"
            End If
            If Val(txtCropRight.Text) <> 0 And LCase(Right(frmClipControl.txtClipfilename.Text, 4)) = ".mov" Then
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<attribute name=""crop.right"">" & Val(txtCropRight.Text) & "</attribute>"
            End If
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</data_file>"
            If chkCaptionsAvailability.Value <> 0 Then
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<data_file role=""captions"">"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtCaptionsClipID.Text)) & "</file_name>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtCaptionsClipID.Text)) & "</size>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtCaptionsClipID.Text)) & "</checksum>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</data_file>"
            End If
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</asset>"
            Print #1, Chr(9) & Chr(9) & "</assets>"
            If txtiTunesPreviewStartTime.Text <> 0 And txtiTunesPreviewStartTime.Text <> "" Then
                Print #1, Chr(9) & Chr(9) & "<preview starttime=""" & txtiTunesPreviewStartTime.Text & """/>"
            Else
                Print #1, Chr(9) & Chr(9) & "<preview starttime=""360""/>"
            End If
            If adoProduct.Recordset.RecordCount > 0 Then
                l_strSQL = adoProduct.RecordSource
                Set l_rstLookup = ExecuteSQL(l_strSQL, g_strExecuteError)
                CheckForSQLError
                l_rstLookup.MoveFirst
                Print #1, Chr(9) & Chr(9) & "<products>"
                Do While Not l_rstLookup.EOF
                    Print #1, Chr(9) & Chr(9) & Chr(9) & "<product>"
                    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & l_rstLookup("territory") & "</territory>"
                    If Trim(" " & l_rstLookup("salesstartdate")) <> "" Then
                        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<sales_start_date>" & Format(l_rstLookup("salesstartdate"), "YYYY-MM-DD") & "</sales_start_date>"
                    End If
                    If UCase(l_rstLookup("clearedforsale")) = "FALSE" Then
                        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<cleared_for_sale>false</cleared_for_sale>"
                    Else
                        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<cleared_for_sale>true</cleared_for_sale>"
                    End If
                    If UCase(l_rstLookup("bundleonly")) = "TRUE" Then
                        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<bundle_only>true</bundle_only>"
                    Else
                        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<bundle_only>false</bundle_only>"
                    End If
                    Print #1, Chr(9) & Chr(9) & Chr(9) & "</product>"
                    l_rstLookup.MoveNext
                Loop
                l_rstLookup.Close
                Set l_rstLookup = Nothing
                Print #1, Chr(9) & Chr(9) & "</products>"
            End If
            Print #1, Chr(9) & "</video>"
            Print #1, "</package>"
            Close #1
        End If
    End If
End If

End Sub

Private Sub cmdOutputNetflixXML_Click()

Dim l_strFileNameToSave, l_strSQL As String, l_rstLookup As ADODB.Recordset, l_blnUSProduct As Boolean

If lblClipID.Caption <> "" Then

'    If frmClipControl.txtMD5Checksum.Text = "" Then
'        MsgBox "The Checksum must be made for files to be sent to iTunes.", vbCritical, "Cannot Make iTunes XML"
'        Exit Sub
'    End If
'
'    If txtiTunesProvider.Text = "" Or txtiTunesVendorID.Text = "" _
'    Or txtiTunesEpProdNo.Text = "" Or txtiTunesContainerID.Text = "" Or txtiTunesContainerPosition.Text = "" _
'    Or datiTunesReleaseDate.Value = Null Or txtiTunesLongDescription.Text = "" _
'    Or Val(txtiTunesPreviewStartTime.Text) = 0 _
'    Or adoProduct.Recordset.RecordCount <= 0 Or txtiTunesOriginalSpokenLocale.Text = "" Then
'        MsgBox "iTunes compulsory data has not all been entered.", vbCritical, "Cannot make iTunes XML"
'        Exit Sub
'    End If
'
'    If chkCaptionsAvailability.Value <> 0 Then
'        If Val(txtCaptionsClipID.Text) = 0 Then
'            MsgBox "iTunes compulsory data has not all been entered.", vbCritical, "Cannot make iTunes XML"
'            Exit Sub
'        End If
'        If GetData("events", "md5checksum", "eventID", Val(txtCaptionsClipID.Text)) = 0 Then
'            MsgBox "iTunes compulsory data has not all been entered.", vbCritical, "Cannot make iTunes XML"
'            Exit Sub
'        End If
'    End If
    cmdSave.Value = True
    
    If frmClipControl.lblFormat.Caption = "DISCSTORE" Then
        MDIForm1.dlgMain.InitDir = GetData("library", "subtitle", "libraryID", frmClipControl.lblLibraryID.Caption) & "\" & frmClipControl.txtAltFolder.Text
        MDIForm1.dlgMain.Filter = "XML files|*.xml"
        MDIForm1.dlgMain.Filename = "metadata.xml"
        
        MDIForm1.dlgMain.ShowSave
        
        l_strFileNameToSave = MDIForm1.dlgMain.Filename
        
        If l_strFileNameToSave <> "" And Right(l_strFileNameToSave, 5) <> "*.xml" Then
        
            Open l_strFileNameToSave For Output As 1
            Print #1, "<?xml version=""1.0"" encoding=""UTF-8""?>"
            Print #1, "<Product xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">"
            Print #1, Chr(9) & "<ProductInformation>"
             Print #1, Chr(9) & Chr(9) & "<ContentProvider>" & txtiTunesProvider.Text & "</ContentProvider>"
            Print #1, Chr(9) & Chr(9) & "<Type>tv</Type>"
             Print #1, Chr(9) & Chr(9) & "<OriginalTitle>"
          Print #1, Chr(9) & Chr(9) & Chr(9) & "<Name>" & UTF8_Encode(XMLSanitise(txtiTunesTitle.Text)) & "</Name>"
            If txtMetaDataLanguage.Text <> "" Then
                Print #1, Chr(9) & Chr(9) & Chr(9) & "<LanguageCode>" & txtMetaDataLanguage.Text & "</LanguageCode>"
            Else
                Print #1, Chr(9) & Chr(9) & Chr(9) & "<LanguageCode>en-GB</LanguageCode>"
            End If
             Print #1, Chr(9) & Chr(9) & "</OriginalTitle>"
            Print #1, Chr(9) & Chr(9) & "<ShowName>" & UTF8_Encode(XMLSanitise(txtiTunesTitle.Text)) & "</ShowName>"
            Print #1, Chr(9) & Chr(9) & "<FirstReleaseYear>" & DatePart("yyyy", datiTunesReleaseDate.Value) & "</FirstReleaseYear>"
          Print #1, Chr(9) & "</ProductInformation>"
          Print #1, Chr(9) & "<ProviderPackages>"
           Print #1, Chr(9) & Chr(9) & "<ProviderPackage>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<ProviderPackageType>tv</ProviderPackageType>"
             Print #1, Chr(9) & Chr(9) & Chr(9) & "<VersionRegion>"
             Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<VersionRegionProviderDescriptor>International</VersionRegionProviderDescriptor>"
              Print #1, Chr(9) & Chr(9) & Chr(9) & "</VersionRegion>"
              Print #1, Chr(9) & Chr(9) & Chr(9) & "<Files>"
              Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<File>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<FileName>" & UTF8_Encode(XMLSanitise(txtiTunesTitle.Text)) & "</FileName>"
              Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Assets>"
              Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<TimedTexts>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<TimedText>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<NetflixRequestID>0ff4c679-8a57-4f37-af5b-a724c660df75</NetflixRequestID>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<LanguageCode>fr</LanguageCode>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<TimedTextType>SUBS</TimedTextType>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</TimedText>"
              Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</TimedTexts>"
              Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</Assets>"
              Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</File>"
              Print #1, Chr(9) & Chr(9) & Chr(9) & "</Files>"
           Print #1, Chr(9) & Chr(9) & "</ProviderPackage>"
           Print #1, Chr(9) & "</ProviderPackages>"
            Print #1, Chr(9) & "</Product>"




            Close #1
        End If
    End If
End If
End Sub

Private Sub cmdSave_Click()

SaveClipiTunes

End Sub

Private Sub cmdSaveiTunesPackage_Click()

Dim l_strSQL As String, l_lngPackageID As Long, l_rstLookup As ADODB.Recordset

If Val(lblClipID.Caption) <> 0 And Val(lblCompanyID.Caption) <> 0 Then

    l_strSQL = "INSERT INTO iTunes_package (companyID, provider, video_type, video_title, video_vendorID, Vendor_Offer_Code, video_ep_prod_no, video_containerID, video_containerposition, "
    l_strSQL = l_strSQL & "video_releasedate, video_copyright_cline, video_longdescription, itunestechcomment, video_language, originalspokenlocale, metadata_language, video_previewstarttime, "
    l_strSQL = l_strSQL & "video_isan, captionsavailable, captionsreason, "
    l_strSQL = l_strSQL & "rating_au, rating_ca, rating_de, rating_fr, rating_uk, rating_us, rating_jp, fullcroptop, fullcropleft, fullcropright, fullcropbottom) VALUES ("
    l_strSQL = l_strSQL & "'" & lblCompanyID.Caption & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtiTunesProvider.Text) & "', "
    l_strSQL = l_strSQL & "'tv', "
    l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(txtiTunesTitle.Text)) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtiTunesVendorID.Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtiTunesVendorOfferCode.Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtiTunesEpProdNo.Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtiTunesContainerID.Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtiTunesContainerPosition.Text) & "', "
    If Not IsNull(datiTunesReleaseDate.Value) Then
        l_strSQL = l_strSQL & "'" & FormatSQLDate(datiTunesReleaseDate.Value) & "', "
    Else
        l_strSQL = l_strSQL & "NULL, "
    End If
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtiTunesCopyrightLine.Text) & "', "
    l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(txtiTunesLongDescription.Text)) & "', "
    l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(txtiTunesTechComments.Text)) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtiTunesVideoLanguage.Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtiTunesOriginalSpokenLocale.Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtMetaDataLanguage.Text) & "', "
    If txtiTunesPreviewStartTime.Text <> "" Then
        l_strSQL = l_strSQL & Val(txtiTunesPreviewStartTime.Text) & ", "
    Else
        l_strSQL = l_strSQL & "360, "
    End If
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtiTunesISAN.Text) & "', "
    l_strSQL = l_strSQL & "'" & chkCaptionsAvailability.Value & "', "
    If cmbCaptionsReason.Text <> "" Then
        l_strSQL = l_strSQL & "'" & cmbCaptionsReason.Text & "', "
    Else
        l_strSQL = l_strSQL & "NULL, "
    End If
    l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbAU.Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbCA.Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbDE.Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbFR.Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbUK.Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbUS.Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbJP.Text) & "', "
    If txtCropTop.Text <> "" Then l_strSQL = l_strSQL & txtCropTop.Text & ", " Else l_strSQL = l_strSQL & "NULL, "
    If txtCropLeft.Text <> "" Then l_strSQL = l_strSQL & txtCropLeft.Text & ", " Else l_strSQL = l_strSQL & "NULL, "
    If txtCropRight.Text <> "" Then l_strSQL = l_strSQL & txtCropRight.Text & ", " Else l_strSQL = l_strSQL & "NULL, "
    If txtCropBottom.Text <> "" Then l_strSQL = l_strSQL & txtCropBottom.Text & ");" Else l_strSQL = l_strSQL & "NULL);"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_lngPackageID = g_lngLastID
    
    l_strSQL = "SELECT * FROM itunes_clip_advisory WHERE eventID = " & Val(lblClipID.Caption) & ";"
    Set l_rstLookup = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If l_rstLookup.RecordCount > 0 Then
        l_rstLookup.MoveFirst
        Do While Not l_rstLookup.EOF
            l_strSQL = "INSERT INTO itunes_clip_advisory (itunes_packageID, advisorysystem, advisory) VALUES ("
            l_strSQL = l_strSQL & l_lngPackageID & ", "
            l_strSQL = l_strSQL & "'" & Trim(" " & l_rstLookup("advisorysystem")) & "', "
            l_strSQL = l_strSQL & "'" & Trim(" " & l_rstLookup("advisory")) & "');"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            l_rstLookup.MoveNext
        Loop
    End If
    l_rstLookup.Close
    Set l_rstLookup = Nothing
    
    l_strSQL = "SELECT * FROM itunes_product WHERE eventID = " & Val(lblClipID.Caption) & ";"
    Set l_rstLookup = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If l_rstLookup.RecordCount > 0 Then
        l_rstLookup.MoveFirst
        Do While Not l_rstLookup.EOF
            l_strSQL = "INSERT INTO itunes_product (itunes_packageID, territory, salesstartdate, clearedforsale, bundleonly) VALUES ("
            l_strSQL = l_strSQL & l_lngPackageID & ", "
            l_strSQL = l_strSQL & "'" & Trim(" " & l_rstLookup("territory")) & "', "
            l_strSQL = l_strSQL & "'" & FormatSQLDate(l_rstLookup("salesstartdate")) & "', "
            l_strSQL = l_strSQL & "'" & Trim(" " & l_rstLookup("clearedforsale")) & "', "
            l_strSQL = l_strSQL & "'" & l_rstLookup("bundleonly") & "');"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            l_rstLookup.MoveNext
        Loop
    End If
    l_rstLookup.Close
    Set l_rstLookup = Nothing
    
    l_strSQL = "SELECT * FROM iTunes_package WHERE companyID = " & lblCompanyID.Caption & " AND video_type = 'tv';"
    
    Dim l_conSearch As ADODB.Connection
    Dim l_rstSearch As ADODB.Recordset
    
    Set l_conSearch = New ADODB.Connection
    Set l_rstSearch = New ADODB.Recordset
    
    l_conSearch.ConnectionString = g_strConnection
    l_conSearch.Open
    
    With l_rstSearch
         .CursorLocation = adUseClient
         .LockType = adLockBatchOptimistic
         .CursorType = adOpenDynamic
         .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch.ActiveConnection = Nothing
    
    Set frmClipiTunes.cmbiTunesPackage.DataSourceList = l_rstSearch
    
    l_conSearch.Close
    Set l_conSearch = Nothing

End If

End Sub

Private Sub cmdUpdateiTunes_Click()

Dim l_strSQL As String

If lblClipID.Caption <> "" And frmClipControl.txtInternalReference.Text <> "" And frmClipControl.txtReference.Text <> "" Then
    
    If MsgBox("Update iTunes (not Advisory or Products) on ALL CLIPS with this Reference." & vbCrLf & "Are you sure?", vbYesNo, "Confirm") = vbNo Then Exit Sub
    
    l_strSQL = "UPDATE events SET "
    l_strSQL = l_strSQL & "provider = '" & QuoteSanitise(txtiTunesProvider.Text) & "', "
    l_strSQL = l_strSQL & "video_vendorID = '" & QuoteSanitise(txtiTunesVendorID.Text) & "', "
    l_strSQL = l_strSQL & "iTunesVendorOfferCode = '" & txtiTunesVendorOfferCode.Text & "', "
    l_strSQL = l_strSQL & "video_ep_prod_no = '" & txtiTunesEpProdNo.Text & "', "
    l_strSQL = l_strSQL & "video_ISAN = '" & txtiTunesISAN.Text & "', "
    l_strSQL = l_strSQL & "video_containerID = '" & txtiTunesContainerID.Text & "', "
    l_strSQL = l_strSQL & "video_containerposition = '" & txtiTunesContainerPosition.Text & "', "
    l_strSQL = l_strSQL & "video_title = '" & QuoteSanitise(txtiTunesTitle.Text) & "', "
    If Not IsNull(datiTunesReleaseDate.Value) Then
        l_strSQL = l_strSQL & "video_releasedate = '" & FormatSQLDate(datiTunesReleaseDate.Value) & "', "
    Else
        l_strSQL = l_strSQL & "video_releasedate = Null, "
    End If
    l_strSQL = l_strSQL & "video_copyright_cline = '" & QuoteSanitise(txtiTunesCopyrightLine.Text) & "', "
    l_strSQL = l_strSQL & "video_longdescription = '" & QuoteSanitise(txtiTunesLongDescription.Text) & "', "
    l_strSQL = l_strSQL & "itunestechcomment = '" & QuoteSanitise(txtiTunesTechComments.Text) & "', "
    If txtiTunesPreviewStartTime.Text <> "" Then
        l_strSQL = l_strSQL & "video_previewstarttime = " & Val(txtiTunesPreviewStartTime.Text) & ", "
    Else
        l_strSQL = l_strSQL & "video_previewstarttime = 360, "
    End If
    l_strSQL = l_strSQL & "video_language = '" & txtiTunesVideoLanguage.Text & "', "
    l_strSQL = l_strSQL & "metadata_language = '" & txtMetaDataLanguage.Text & "', "
    l_strSQL = l_strSQL & "originalspokenlocale = '" & txtiTunesOriginalSpokenLocale.Text & "', "
    l_strSQL = l_strSQL & "rating_au = '" & cmbAU.Text & "', "
    l_strSQL = l_strSQL & "rating_ca = '" & cmbCA.Text & "', "
    l_strSQL = l_strSQL & "rating_de = '" & cmbDE.Text & "', "
    l_strSQL = l_strSQL & "rating_fr = '" & cmbFR.Text & "', "
    l_strSQL = l_strSQL & "rating_uk = '" & cmbUK.Text & "', "
    l_strSQL = l_strSQL & "rating_us = '" & cmbUS.Text & "', "
    l_strSQL = l_strSQL & "rating_jp = '" & cmbJP.Text & "', "
    l_strSQL = l_strSQL & "itunescroptop = " & Val(txtCropTop.Text) & ", "
    l_strSQL = l_strSQL & "itunescropbottom = " & Val(txtCropBottom.Text) & ", "
    l_strSQL = l_strSQL & "itunescropleft = " & Val(txtCropLeft.Text) & ", "
    l_strSQL = l_strSQL & "itunescropright = " & Val(txtCropRight.Text) & " "
    
    l_strSQL = l_strSQL & "WHERE clipreference = '" & QuoteSanitise(frmClipControl.txtReference.Text) & "';"
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    Me.Hide
    ShowClipControl Val(lblClipID.Caption)
    
End If

End Sub

Private Sub ddnAdvisorySystem_CloseUp()

If ddnAdvisorySystem.Columns(0).Text <> "" Then
    ddnAdvisory.RemoveAll
    PopulateCombo "iTunesAdvisory", ddnAdvisory, " AND format = '" & ddnAdvisorySystem.Columns(0).Text & "'"
    grdiTunesAdvisory.Columns("advisory").DropDownHwnd = ddnAdvisory.hWnd
End If
    
End Sub

Private Sub ddnISO2A_CloseUp()

grdProduct.Columns("territory").Text = ddnISO2A.Columns("country").Text
grdProduct.Columns("territorycode").Text = ddnISO2A.Columns("code").Text

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

l_strSQL = "SELECT description, information FROM xref WHERE category = 'iTunesLocale' ORDER BY information;"
adoVideoLanguage.ConnectionString = g_strConnection
adoVideoLanguage.RecordSource = l_strSQL
adoVideoLanguage.Refresh

l_strSQL = "SELECT description, information FROM xref WHERE category = 'iTunesLocale' and format = 'Y' ORDER BY information;"
adoMetadataLanguage.ConnectionString = g_strConnection
adoMetadataLanguage.RecordSource = l_strSQL
adoMetadataLanguage.Refresh

adoSpokenLocale.ConnectionString = g_strConnection
adoSpokenLocale.RecordSource = l_strSQL
adoSpokenLocale.Refresh

PopulateCombo "iTunesAdvisorySystem", ddnAdvisorySystem
PopulateCombo "iTunesAvailabilityReason", cmbCaptionsReason

grdiTunesAdvisory.Columns("advisorysystem").DropDownHwnd = ddnAdvisorySystem.hWnd

adoISO2A.ConnectionString = g_strConnection
adoISO2A.RecordSource = "SELECT country, iso2acode FROM iso2a;"
adoISO2A.Refresh
grdProduct.Columns("territory").DropDownHwnd = ddnISO2A.hWnd

lblClipID.Caption = frmClipControl.txtClipID.Text

l_strSQL = "SELECT * FROM events WHERE eventID = '" & Val(lblClipID.Caption) & "';"

Dim l_rstClip As New ADODB.Recordset
Set l_rstClip = ExecuteSQL(l_strSQL, g_strExecuteError)

End Sub

Private Sub grdiTunesAdvisory_BeforeUpdate(Cancel As Integer)

grdiTunesAdvisory.Columns("eventID").Text = Val(lblClipID.Caption)

End Sub

Private Sub grdProduct_BeforeUpdate(Cancel As Integer)

grdProduct.Columns("eventID").Text = Val(lblClipID.Caption)

End Sub

Private Sub grdProduct_BtnClick()

If grdProduct.Columns(grdProduct.Col).Name = "salesstartdate" Then
    If IsDate(grdProduct.Columns("salesstartdate").Text) Then
        grdProduct.Columns("salesstartdate").Text = Format(GetDate(grdProduct.Columns("salesstartdate").Text), "dd mmm yyyy")
    Else
        grdProduct.Columns("salesstartdate").Text = Format(GetDate(Now), "dd mmm yyyy")
    End If
End If

End Sub

Private Sub grdProduct_RowLoaded(ByVal Bookmark As Variant)

grdProduct.Columns("territory").Text = GetData("iso2a", "country", "iso2acode", grdProduct.Columns("territorycode").Text)

End Sub

Private Sub txtiTunesOriginalSpokenLocale_GotFocus()
HighLite txtiTunesOriginalSpokenLocale
End Sub

Private Sub txtiTunesVideoLanguage_GotFocus()
HighLite txtiTunesVideoLanguage
End Sub

Private Sub txtMetaDataLanguage_GotFocus()
HighLite txtMetaDataLanguage
End Sub




