VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmClipDistributionUsers 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Distribution Portal Users"
   ClientHeight    =   13755
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   22635
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmClipDistributionUsers.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   13755
   ScaleWidth      =   22635
   WindowState     =   2  'Maximized
   Begin MSAdodcLib.Adodc adoUserClipsExpired 
      Height          =   330
      Left            =   120
      Top             =   9840
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoUserClipsExpired"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "Search"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   16260
      TabIndex        =   11
      ToolTipText     =   "Close this form"
      Top             =   11940
      Width           =   1155
   End
   Begin VB.CommandButton cmdNotifyUser 
      Caption         =   "Notify User of Login and Password"
      Height          =   315
      Left            =   17520
      TabIndex        =   10
      Top             =   120
      Width           =   3615
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   15000
      TabIndex        =   9
      ToolTipText     =   "Close this form"
      Top             =   11940
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adoLogins 
      Height          =   330
      Left            =   14220
      Top             =   4800
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoLogins"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdLogins 
      Bindings        =   "frmClipDistributionUsers.frx":08CA
      Height          =   1935
      Left            =   14220
      TabIndex        =   4
      Top             =   4800
      Width           =   8055
      _Version        =   196617
      AllowDelete     =   -1  'True
      AllowUpdate     =   0   'False
      SelectTypeCol   =   3
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   7
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "distributionloginID"
      Columns(0).Name =   "distributionloginID"
      Columns(0).DataField=   "distributionloginID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "Login Date"
      Columns(1).Name =   "logindate"
      Columns(1).DataField=   "logindate"
      Columns(1).DataType=   7
      Columns(1).FieldLen=   256
      Columns(2).Width=   3175
      Columns(2).Caption=   "Full Name"
      Columns(2).Name =   "fullname"
      Columns(2).DataField=   "fullname"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2117
      Columns(3).Caption=   "Username"
      Columns(3).Name =   "username"
      Columns(3).DataField=   "username"
      Columns(3).FieldLen=   256
      Columns(4).Width=   2117
      Columns(4).Caption=   "Password"
      Columns(4).Name =   "password"
      Columns(4).DataField=   "password"
      Columns(4).FieldLen=   256
      Columns(5).Width=   2461
      Columns(5).Caption=   "IP Address"
      Columns(5).Name =   "ipaddress"
      Columns(5).DataField=   "ipaddress"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "Result"
      Columns(6).Name =   "result"
      Columns(6).DataField=   "result"
      Columns(6).FieldLen=   256
      _ExtentX        =   14208
      _ExtentY        =   3413
      _StockProps     =   79
      Caption         =   "Portal Logins"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoDeliveries 
      Height          =   330
      Left            =   120
      Top             =   6960
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoDeliveries"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoUserClips 
      Height          =   330
      Left            =   120
      Top             =   4800
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoUserClips"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDeliveries 
      Bindings        =   "frmClipDistributionUsers.frx":08E2
      Height          =   2715
      Left            =   120
      TabIndex        =   3
      Top             =   6960
      Width           =   21105
      _Version        =   196617
      AllowDelete     =   -1  'True
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   11
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "distributiondeliveryID"
      Columns(0).Name =   "distributiondeliveryID"
      Columns(0).DataField=   "distributiondeliveryID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "distributionuserID"
      Columns(1).Name =   "distributionuserID"
      Columns(1).DataField=   "distributionuserID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3625
      Columns(2).Caption=   "Delivered To"
      Columns(2).Name =   "fullname"
      Columns(2).DataField=   "fullname"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2990
      Columns(3).Caption=   "timewhen"
      Columns(3).Name =   "timewhen"
      Columns(3).DataField=   "timewhen"
      Columns(3).DataType=   7
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "clipID"
      Columns(4).Name =   "clipID"
      Columns(4).DataField=   "clipID"
      Columns(4).FieldLen=   256
      Columns(5).Width=   8811
      Columns(5).Caption=   "Clip Details"
      Columns(5).Name =   "clipdetails"
      Columns(5).DataField=   "clipdetails"
      Columns(5).FieldLen=   256
      Columns(6).Width=   2963
      Columns(6).Caption=   "Access Type"
      Columns(6).Name =   "accesstype"
      Columns(6).DataField=   "accesstype"
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Caption=   "Final Status"
      Columns(7).Name =   "finalstatus"
      Columns(7).DataField=   "finalstatus"
      Columns(7).FieldLen=   256
      Columns(8).Width=   2990
      Columns(8).Caption=   "Ack Good"
      Columns(8).Name =   "receivedgood"
      Columns(8).DataField=   "receivedgood"
      Columns(8).FieldLen=   256
      Columns(9).Width=   2990
      Columns(9).Caption=   "Ack Bad"
      Columns(9).Name =   "receivedbad"
      Columns(9).DataField=   "receivedbad"
      Columns(9).FieldLen=   256
      Columns(10).Width=   2990
      Columns(10).Caption=   "Ack Comments"
      Columns(10).Name=   "receivedcomments"
      Columns(10).DataField=   "receivedcomments"
      Columns(10).FieldLen=   256
      _ExtentX        =   37227
      _ExtentY        =   4789
      _StockProps     =   79
      Caption         =   "User Deliveries"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdUserClips 
      Bindings        =   "frmClipDistributionUsers.frx":08FE
      Height          =   1935
      Left            =   120
      TabIndex        =   2
      Top             =   4800
      Width           =   14010
      _Version        =   196617
      AllowDelete     =   -1  'True
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   11
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "portalpermissionID"
      Columns(0).Name =   "portalpermissionID"
      Columns(0).DataField=   "portalpermissionID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   1773
      Columns(1).Caption=   "eventID"
      Columns(1).Name =   "eventID"
      Columns(1).DataField=   "eventID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "portaluserID"
      Columns(2).Name =   "portaluserID"
      Columns(2).DataField=   "portaluserID"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2117
      Columns(3).Caption=   "Clip Store"
      Columns(3).Name =   "barcode"
      Columns(3).DataField=   "barcode"
      Columns(3).FieldLen=   256
      Columns(4).Width=   7938
      Columns(4).Caption=   "File Name"
      Columns(4).Name =   "clipfilename"
      Columns(4).DataField=   "clipfilename"
      Columns(4).FieldLen=   256
      Columns(5).Width=   4233
      Columns(5).Caption=   "Folder"
      Columns(5).Name =   "altlocation"
      Columns(5).DataField=   "altlocation"
      Columns(5).FieldLen=   256
      Columns(6).Width=   2514
      Columns(6).Caption=   "Format"
      Columns(6).Name =   "clipformat"
      Columns(6).DataField=   "clipformat"
      Columns(6).FieldLen=   256
      Columns(7).Width=   2037
      Columns(7).Caption=   "Bitrate (kbps)"
      Columns(7).Name =   "clipbitrate"
      Columns(7).DataField=   "clipbitrate"
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Caption=   "Date Assigned"
      Columns(8).Name =   "dateassigned"
      Columns(8).DataField=   "dateassigned"
      Columns(8).DataType=   7
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "companyID"
      Columns(9).Name =   "companyID"
      Columns(9).DataField=   "companyID"
      Columns(9).FieldLen=   256
      Columns(10).Width=   6165
      Columns(10).Caption=   "Company"
      Columns(10).Name=   "Company"
      Columns(10).FieldLen=   256
      _ExtentX        =   24712
      _ExtentY        =   3413
      _StockProps     =   79
      Caption         =   "User Clip Assignments"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   17520
      TabIndex        =   1
      ToolTipText     =   "Close this form"
      Top             =   11940
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adoPortalUsers 
      Height          =   330
      Left            =   120
      Top             =   600
      Width           =   2835
      _ExtentX        =   5001
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoDistribUsers"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdPortalUsers 
      Bindings        =   "frmClipDistributionUsers.frx":0919
      Height          =   4095
      Left            =   120
      TabIndex        =   0
      ToolTipText     =   "People who receive content from this company"
      Top             =   600
      Width           =   22155
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeveColorScheme =   1
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      ForeColorEven   =   0
      BackColorOdd    =   16772351
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   11
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "distributionuserID"
      Columns(0).Name =   "distributionuserID"
      Columns(0).DataField=   "distributionuserID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "Full Name"
      Columns(1).Name =   "fullname"
      Columns(1).DataField=   "fullname"
      Columns(1).FieldLen=   256
      Columns(2).Width=   5821
      Columns(2).Caption=   "fullname-UTF8"
      Columns(2).Name =   "fullname-UTF8"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3651
      Columns(3).Caption=   "User name"
      Columns(3).Name =   "username"
      Columns(3).DataField=   "username"
      Columns(3).DataType=   10
      Columns(3).FieldLen=   256
      Columns(4).Width=   5371
      Columns(4).Caption=   "Password"
      Columns(4).Name =   "password"
      Columns(4).DataField=   "password"
      Columns(4).DataType=   10
      Columns(4).FieldLen=   256
      Columns(4).Style=   1
      Columns(5).Width=   6033
      Columns(5).Caption=   "Email"
      Columns(5).Name =   "email"
      Columns(5).DataField=   "email"
      Columns(5).DataType=   10
      Columns(5).FieldLen=   256
      Columns(6).Width=   6720
      Columns(6).Caption=   "Administrator Email"
      Columns(6).Name =   "administratoremail"
      Columns(6).DataField=   "administratoremail"
      Columns(6).FieldLen=   256
      Columns(7).Width=   2461
      Columns(7).Caption=   "Date Created"
      Columns(7).Name =   "datecreated"
      Columns(7).DataField=   "datecreated"
      Columns(7).DataType=   7
      Columns(7).FieldLen=   256
      Columns(8).Width=   1561
      Columns(8).Caption=   "Pwd Lock"
      Columns(8).Name =   "lockpassword"
      Columns(8).DataField=   "lockpassword"
      Columns(8).DataType=   10
      Columns(8).FieldLen=   256
      Columns(8).Style=   2
      Columns(9).Width=   1138
      Columns(9).Caption=   "Setting"
      Columns(9).Name =   "usersettings"
      Columns(9).DataField=   "usersettings"
      Columns(9).FieldLen=   256
      Columns(10).Width=   5292
      Columns(10).Caption=   "Site Address"
      Columns(10).Name=   "siteaddress"
      Columns(10).DataField=   "siteaddress"
      Columns(10).FieldLen=   256
      _ExtentX        =   39079
      _ExtentY        =   7223
      _StockProps     =   79
      Caption         =   "Portal Users defined for this JCA Customer"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker datFrom 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   10320
      TabIndex        =   5
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   120
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   162136065
      CurrentDate     =   39580
   End
   Begin MSComCtl2.DTPicker datTo 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   13320
      TabIndex        =   6
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   120
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   33685505
      CurrentDate     =   39580
   End
   Begin MSAdodcLib.Adodc adoCompany 
      Height          =   330
      Left            =   2640
      Top             =   180
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCompany"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Bindings        =   "frmClipDistributionUsers.frx":0936
      Height          =   300
      Left            =   2280
      TabIndex        =   12
      ToolTipText     =   "The company name"
      Top             =   120
      Width           =   3735
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "Name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   6588
      _ExtentY        =   529
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdUserClipsExpired 
      Bindings        =   "frmClipDistributionUsers.frx":094F
      Height          =   1935
      Left            =   120
      TabIndex        =   15
      Top             =   9840
      Width           =   21105
      _Version        =   196617
      AllowDelete     =   -1  'True
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   13
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "portalpermissionID"
      Columns(0).Name =   "portalpermissionID"
      Columns(0).DataField=   "portalpermissionID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   1773
      Columns(1).Caption=   "eventID"
      Columns(1).Name =   "eventID"
      Columns(1).DataField=   "eventID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "portaluserID"
      Columns(2).Name =   "portaluserID"
      Columns(2).DataField=   "portaluserID"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2117
      Columns(3).Caption=   "Clip Store"
      Columns(3).Name =   "barcode"
      Columns(3).DataField=   "barcode"
      Columns(3).FieldLen=   256
      Columns(4).Width=   7938
      Columns(4).Caption=   "File Name"
      Columns(4).Name =   "clipfilename"
      Columns(4).DataField=   "clipfilename"
      Columns(4).FieldLen=   256
      Columns(5).Width=   4233
      Columns(5).Caption=   "Folder"
      Columns(5).Name =   "altlocation"
      Columns(5).DataField=   "altlocation"
      Columns(5).FieldLen=   256
      Columns(6).Width=   2514
      Columns(6).Caption=   "Format"
      Columns(6).Name =   "clipformat"
      Columns(6).DataField=   "clipformat"
      Columns(6).FieldLen=   256
      Columns(7).Width=   2037
      Columns(7).Caption=   "Bitrate (kbps)"
      Columns(7).Name =   "clipbitrate"
      Columns(7).DataField=   "clipbitrate"
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Caption=   "Date Assigned"
      Columns(8).Name =   "dateassigned"
      Columns(8).DataField=   "dateassigned"
      Columns(8).DataType=   7
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "companyID"
      Columns(9).Name =   "companyID"
      Columns(9).DataField=   "companyID"
      Columns(9).FieldLen=   256
      Columns(10).Width=   6165
      Columns(10).Caption=   "Company"
      Columns(10).Name=   "Company"
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Caption=   "Date Expired"
      Columns(11).Name=   "dateexpired"
      Columns(11).DataField=   "dateexpired"
      Columns(11).DataType=   7
      Columns(11).FieldLen=   256
      Columns(12).Width=   767
      Columns(12).Caption=   "Del"
      Columns(12).Name=   "system_deleted"
      Columns(12).DataField=   "events.system_deleted"
      Columns(12).FieldLen=   256
      Columns(12).Style=   2
      _ExtentX        =   37227
      _ExtentY        =   3413
      _StockProps     =   79
      Caption         =   "Expired User Clip Assignments"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblCompanyID 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   6060
      TabIndex        =   14
      Top             =   120
      Width           =   1635
   End
   Begin VB.Label lblCaption 
      Caption         =   "JCA Customer"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   840
      TabIndex        =   13
      Top             =   180
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Date Range To"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   2
      Left            =   11940
      TabIndex        =   8
      Top             =   180
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Date Range From"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   8880
      TabIndex        =   7
      Top             =   180
      Width           =   1275
   End
End
Attribute VB_Name = "frmClipDistributionUsers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub adoPortalUsers_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

Dim l_strSQL As String

If adoPortalUsers.Recordset.EOF Then
    Exit Sub
Else
    If adoPortalUsers.Recordset("distributionuserID") = "" Then
        Exit Sub
    End If
End If

l_strSQL = "SELECT distributionpermission.*, events.*, library.barcode FROM distributionpermission INNER JOIN (events INNER JOIN library ON events.libraryID = library.libraryID) ON distributionpermission.eventID = events.eventID WHERE distributionuserID = " & Val(adoPortalUsers.Recordset("distributionuserID")) & " ORDER BY dateassigned DESC;"

adoUserClips.RecordSource = l_strSQL
adoUserClips.ConnectionString = g_strConnection
adoUserClips.Refresh
adoUserClips.Caption = adoUserClips.Recordset.RecordCount & " Clips"

l_strSQL = "SELECT distributionpermissionexpired.*, events.*, library.barcode FROM distributionpermissionexpired LEFT JOIN (events INNER JOIN library ON events.libraryID = library.libraryID) ON distributionpermissionexpired.eventID = events.eventID WHERE distributionuserID = " & Val(adoPortalUsers.Recordset("distributionuserID")) & " ORDER BY dateassigned DESC;"

adoUserClipsExpired.RecordSource = l_strSQL
adoUserClipsExpired.ConnectionString = g_strConnection
adoUserClipsExpired.Refresh
adoUserClipsExpired.Caption = adoUserClipsExpired.Recordset.RecordCount & " Clips"

If lblCompanyID.Caption <> "" Then
    l_strSQL = "SELECT * FROM distributiondelivery WHERE companyID = " & Val(lblCompanyID.Caption)
Else
    l_strSQL = "SELECT * FROM distributiondelivery WHERE distributiondeliveryID > 0 "
End If

If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
l_strSQL = l_strSQL & " ORDER BY timewhen DESC;"

adoDeliveries.RecordSource = l_strSQL
adoDeliveries.ConnectionString = g_strConnection
adoDeliveries.Refresh
adoDeliveries.Caption = adoDeliveries.Recordset.RecordCount & " Del."

End Sub

Private Sub cmbCompany_CloseUp()

Dim l_strTemp As String

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

Refresh_All_Grids

End Sub

Private Sub cmdClear_Click()

Dim l_strSQL As String

cmbCompany.Text = ""
lblCompanyID.Caption = ""

adoUserClips.ConnectionString = g_strConnection
adoUserClips.RecordSource = "SELECT portalpermission.*, events.*, library.barcode FROM portalpermission INNER JOIN (events INNER JOIN library ON events.libraryID = library.libraryID) ON portalpermission.eventID = events.eventID WHERE portaluserID = -1;"
adoUserClips.Refresh

adoLogins.ConnectionString = g_strConnection
adoLogins.RecordSource = "SELECT * from webportallogin WHERE companyID = -1"
adoLogins.Refresh

adoDeliveries.ConnectionString = g_strConnection
adoDeliveries.RecordSource = "SELECT * from webdeliveryportal WHERE portaluserID = -1"
adoDeliveries.Refresh

Refresh_All_Grids

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub


Private Sub cmdNotifyUser_Click()

If ValidateEmail(grdPortalUsers.Columns("email").Text) = False Then Exit Sub

Dim l_strEmailTo As String, l_strEmailFrom As String, l_strEmailSubject As String, l_strEmailCC As String, l_strEmailBody As String, l_lngJobID As Long, l_rstCOntacts As ADODB.Recordset

l_strEmailTo = grdPortalUsers.Columns("email").Text

l_strEmailCC = ""

l_lngJobID = Val(InputBox("If there is a Job with a notification list, scan the JobID, or Cancel if not", "Notify Users for Job"))
If l_lngJobID <> 0 Then
    Set l_rstCOntacts = ExecuteSQL("SELECT * FROM jobcontact WHERE jobID = " & l_lngJobID & " ORDER BY name;", g_strExecuteError)
    CheckForSQLError
    If l_rstCOntacts.RecordCount > 0 Then
        l_rstCOntacts.MoveFirst
        Do While Not l_rstCOntacts.EOF
            If l_strEmailCC <> "" Then l_strEmailCC = l_strEmailCC & ";"
            l_strEmailCC = l_strEmailCC & l_rstCOntacts("email")
            l_rstCOntacts.MoveNext
        Loop
    End If
    l_rstCOntacts.Close
    Set l_rstCOntacts = Nothing
End If
   
If grdPortalUsers.Columns("administratoremail").Text = "" Then
    l_strEmailFrom = g_strOperationsEmailAddress
Else
    l_strEmailFrom = grdPortalUsers.Columns("administratoremail").Text
End If

If grdPortalUsers.Columns("administratoremail").Text <> "" And InStr(l_strEmailCC, grdPortalUsers.Columns("administratoremail").Text) <= 0 Then
    If l_strEmailCC <> "" Then l_strEmailCC = l_strEmailCC & ";"
    l_strEmailCC = l_strEmailCC & grdPortalUsers.Columns("administratoremail").Text
End If

l_strEmailSubject = "Distribution Media Window - User Details"

l_strEmailBody = "Material is available for you on the Distribution Media Window. Your user details are as follows:" & vbCrLf & vbCrLf _
& "Distribution Media Window User Full Name: " & grdPortalUsers.Columns("Fullname").Text & vbCrLf & _
"Login: " & grdPortalUsers.Columns("username").Text & vbCrLf & "Password: " & grdPortalUsers.Columns("password").Text & vbCrLf & _
"Email: " & grdPortalUsers.Columns("email").Text & vbCrLf

If grdPortalUsers.Columns("siteaddress").Text <> "" Then
    l_strEmailBody = l_strEmailBody & "Link to Distrinution Media Window Site: " & grdPortalUsers.Columns("siteaddress").Text & vbCrLf & vbCrLf
Else
    l_strEmailBody = l_strEmailBody & "Link to Distrinution Media Window Site: http://dist.mx1.tv" & vbCrLf & vbCrLf
End If

If adoUserClips.Recordset.RecordCount > 0 Then
    l_strEmailBody = l_strEmailBody & "The following media items are currently available:" & vbCrLf & vbCrLf
    adoUserClips.Recordset.MoveFirst
    Do While adoUserClips.Recordset.EOF = False
        l_strEmailBody = l_strEmailBody & adoUserClips.Recordset("clipfilename") & " - " & adoUserClips.Recordset("clipformat") & " " & adoUserClips.Recordset("clipbitrate") & "kbps" _
            & " - Provided by: " & GetData("company", "name", "companyID", adoUserClips.Recordset("companyID")) & vbCrLf
        adoUserClips.Recordset.MoveNext
    Loop
    l_strEmailBody = l_strEmailBody & vbCrLf & vbCrLf
End If

SendOutlookEmail l_strEmailTo, l_strEmailSubject, l_strEmailBody, "", l_strEmailCC, g_strAdministratorEmailAddress, True

End Sub

Private Sub cmdSearch_Click()

Refresh_All_Grids

End Sub

Private Sub datFrom_CloseUp()

Dim l_strSQL As String

If Val(lblCompanyID.Caption) <> 0 Then

    l_strSQL = "SELECT * FROM webdeliveryportal WHERE companyID = " & Val(lblCompanyID.Caption)
    If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
    If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
    l_strSQL = l_strSQL & " ORDER BY timewhen DESC;"
    
    adoDeliveries.RecordSource = l_strSQL
    adoDeliveries.ConnectionString = g_strConnection
    adoDeliveries.Refresh
    
    l_strSQL = "SELECT * from webportallogin WHERE companyID = '" & Val(lblCompanyID.Caption) & "'"
    If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND logindate >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
    If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND logindate <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
    l_strSQL = l_strSQL & " ORDER BY logindate DESC;"
    
    adoLogins.ConnectionString = g_strConnection
    adoLogins.RecordSource = l_strSQL
    adoLogins.Refresh
    
End If

End Sub

Private Sub datTo_CloseUp()

Dim l_strSQL As String

If Val(lblCompanyID.Caption) <> 0 Then
    
    l_strSQL = "SELECT * FROM webdeliveryportal WHERE companyID = " & Val(lblCompanyID.Caption)
    If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
    If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
    l_strSQL = l_strSQL & " ORDER BY timewhen DESC;"
    
    adoDeliveries.RecordSource = l_strSQL
    adoDeliveries.ConnectionString = g_strConnection
    adoDeliveries.Refresh
    
    l_strSQL = "SELECT * from webportallogin WHERE companyID = '" & Val(lblCompanyID.Caption) & "'"
    If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND logindate >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
    If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND logindate <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
    l_strSQL = l_strSQL & " ORDER BY logindate DESC;"
    
    adoLogins.ConnectionString = g_strConnection
    adoLogins.RecordSource = l_strSQL
    adoLogins.Refresh
    
End If

End Sub

Private Sub Form_Activate()

Refresh_All_Grids

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

l_strSQL = "SELECT name, companyID FROM company WHERE iscustomer = 1 AND system_active = 1 ORDER BY name"

adoCompany.ConnectionString = g_strConnection
adoCompany.RecordSource = l_strSQL
adoCompany.Refresh

CenterForm Me

End Sub

Private Sub Form_Resize()

If Me.WindowState = vbMinimized Then Exit Sub

cmdClose.Top = Me.ScaleHeight - cmdClose.Height - 120
cmdClose.Left = Me.ScaleWidth - cmdClose.Width - 120
cmdSearch.Top = cmdClose.Top
cmdSearch.Left = cmdClose.Left - cmdClose.Width - 120
cmdClear.Top = cmdClose.Top
cmdClear.Left = cmdSearch.Left - cmdSearch.Width - 120

grdPortalUsers.Height = (Me.ScaleHeight - grdPortalUsers.Top - cmdClose.Height - 120 - 480) / 3
grdPortalUsers.Width = Me.ScaleWidth - 240

grdUserClips.Top = grdPortalUsers.Top + grdPortalUsers.Height + 120
grdUserClips.Width = Me.ScaleWidth * 0.65 - 180
grdUserClips.Left = grdPortalUsers.Left
grdUserClips.Height = grdPortalUsers.Height / 2 - 120
adoUserClips.Top = grdUserClips.Top
adoUserClips.Left = grdUserClips.Left

grdUserClipsExpired.Top = grdUserClips.Top + grdUserClips.Height + 120
grdUserClipsExpired.Width = Me.ScaleWidth * 0.65 - 180
grdUserClipsExpired.Left = grdPortalUsers.Left
grdUserClipsExpired.Height = grdPortalUsers.Height / 2 - 120
adoUserClipsExpired.Top = grdUserClips.Top
adoUserClipsExpired.Left = grdUserClips.Left

grdLogins.Height = grdPortalUsers.Height
grdLogins.Top = grdPortalUsers.Top + grdPortalUsers.Height + 120
grdLogins.Width = Me.ScaleWidth * 0.35 - 180
grdLogins.Left = grdUserClips.Left + grdUserClips.Width + 120
adoLogins.Left = grdLogins.Left
adoLogins.Top = grdLogins.Top

grdDeliveries.Top = grdLogins.Top + grdLogins.Height + 120
grdDeliveries.Width = grdPortalUsers.Width
grdDeliveries.Height = grdPortalUsers.Height
grdDeliveries.Left = grdPortalUsers.Left
adoDeliveries.Left = grdDeliveries.Left
adoDeliveries.Top = grdDeliveries.Top

End Sub

Private Sub grdDeliveries_DblClick()

If grdDeliveries.Columns("clipID").Text <> "" Then
    ShowClipControl Val(grdDeliveries.Columns("clipID").Text)
End If

End Sub

Private Sub grdPortalUsers_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

Dim l_strSQL As String, l_intResp As Integer

l_intResp = MsgBox("You are about to delete portal user: " & grdPortalUsers.Columns("fullname").Text & vbCrLf & "Are you sure?", vbYesNo, "Delete Portal User")
If l_intResp = vbYes Then
    l_strSQL = "DELETE FROM distributionpermission WHERE distributionuserid = " & grdPortalUsers.Columns("distributionuserID").Text
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    DispPromptMsg = 0
Else
    Cancel = 1
End If

End Sub

Private Sub grdPortalUsers_BeforeUpdate(Cancel As Integer)

grdPortalUsers.Columns("fullname").Text = UTF8_Encode(grdPortalUsers.Columns("fullname-utf8").Text)

If grdPortalUsers.Columns("email").Text <> "" Then
    If ValidateEmail(grdPortalUsers.Columns("email").Text) = False Then
        MsgBox "Invalid Email address provided"
        Cancel = 1
    End If
Else
    MsgBox "No Email address provided"
    Cancel = 1
End If

If grdPortalUsers.Columns("administratoremail").Text <> "" Then
    If ValidateEmail(grdPortalUsers.Columns("administratoremail").Text) = False Then
        MsgBox "Invalid Administrator Email address provided"
        Cancel = 1
    End If
Else
    MsgBox "No Email address provided"
    Cancel = 1
End If

If grdPortalUsers.Columns("siteaddress").Text = "" Then
    If MsgBox("Use the default address", vbYesNo, "No Site address provided") = vbNo Then
        Cancel = 1
    Else
        grdPortalUsers.Columns("siteaddress").Text = "http://dist.mx1.tv"
    End If
Else
    If Left(grdPortalUsers.Columns("siteaddress").Text, 7) <> "http://" Then
        MsgBox "Invalid site address provided"
        Cancel = 1
    Else
        If ValidateHTTP(Mid(grdPortalUsers.Columns("siteaddress").Text, 8)) = False Then
            MsgBox "Invalid site address provided"
            Cancel = 1
        End If
    End If
End If

If grdPortalUsers.Columns("setting").Text <> "" Then
    grdPortalUsers.Columns("Setting").Text = 3
End If

If grdPortalUsers.Columns("datecreated").Text = "" Then grdPortalUsers.Columns("datecreated").Text = Format(Now, "dd mmm yyyy")

End Sub

Private Sub grdPortalUsers_BtnClick()

grdPortalUsers.Columns(grdPortalUsers.Col).Text = RndPassword(6)

End Sub

Private Sub grdPortalUsers_RowLoaded(ByVal Bookmark As Variant)

grdPortalUsers.Columns("fullname-utf8").Text = UTF8_Decode(grdPortalUsers.Columns("fullname").Text)

End Sub

Private Sub grdUserClips_DblClick()

If grdUserClips.Columns("eventID").Text <> "" Then
    ShowClipControl Val(grdUserClips.Columns("eventID").Text)
End If

End Sub

Sub Refresh_All_Grids()

Dim l_strSQL As String

adoPortalUsers.ConnectionString = g_strConnection
adoPortalUsers.RecordSource = "SELECT * from distributionuser ORDER BY fullname;"
adoPortalUsers.Refresh
adoPortalUsers.Caption = adoPortalUsers.Recordset.RecordCount & " Users"

l_strSQL = "SELECT * from distributionlogin WHERE distributionuserID >= 0 "
If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND logindate >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND logindate <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
l_strSQL = l_strSQL & " ORDER BY logindate DESC;"

adoLogins.ConnectionString = g_strConnection
adoLogins.RecordSource = l_strSQL
adoLogins.Refresh
adoLogins.Caption = adoLogins.Recordset.RecordCount & " Logins"

End Sub

Private Sub grdUserClips_RowLoaded(ByVal Bookmark As Variant)

grdUserClips.Columns("company").Text = GetData("events", "companyname", "eventID", grdUserClips.Columns("eventID").Text)

End Sub

Private Sub grdUserClipsExpired_RowLoaded(ByVal Bookmark As Variant)

grdUserClipsExpired.Columns("company").Text = GetData("events", "companyname", "eventID", grdUserClipsExpired.Columns("eventID").Text)

End Sub
