VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmTranscoding 
   Caption         =   "Transcoding Control"
   ClientHeight    =   14310
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   21750
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmTranscoding.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   14310
   ScaleWidth      =   21750
   WindowState     =   2  'Maximized
   Begin TabDlg.SSTab tabSpecs 
      Height          =   14715
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   28395
      _ExtentX        =   50086
      _ExtentY        =   25956
      _Version        =   393216
      Tabs            =   7
      TabsPerRow      =   7
      TabHeight       =   520
      TabCaption(0)   =   "Transcode Request Summary"
      TabPicture(0)   =   "frmTranscoding.frx":0BC2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label11"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "grdTranscodeRequests"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "adoTranscodeRequests"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "chkFilterRequested"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "chkFilterFailed"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "chkFilterCancelled"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "chkFilterInProgress"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "chkFilterComplete"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "ddnTranscodeSpecs"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "ddnStatus"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Frame1"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "chkFilterFinishingOff"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "chkShowHiddenRequests"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "cmbSearchTranscodeSystem"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).ControlCount=   14
      TabCaption(1)   =   "Transcode Specifications"
      TabPicture(1)   =   "frmTranscoding.frx":0BDE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "grdTranscodeSpecs"
      Tab(1).Control(1)=   "frmTranscoding"
      Tab(1).Control(2)=   "optShowTranscodeSpecs(0)"
      Tab(1).Control(3)=   "optShowTranscodeSpecs(1)"
      Tab(1).Control(4)=   "adoTranscodeSpecs"
      Tab(1).Control(5)=   "chkTranscodeSystem(3)"
      Tab(1).Control(6)=   "chkTranscodeSystem(11)"
      Tab(1).Control(7)=   "chkTranscodeSystem(17)"
      Tab(1).Control(8)=   "cmdChange"
      Tab(1).Control(9)=   "chkTranscodeSystem(4)"
      Tab(1).ControlCount=   10
      TabCaption(2)   =   "File Request Summary"
      TabPicture(2)   =   "frmTranscoding.frx":0BFA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "chkExludeMediaInfo"
      Tab(2).Control(1)=   "Frame4"
      Tab(2).Control(2)=   "chkShowHiddenFileRequests"
      Tab(2).Control(3)=   "chkJustMyStuff"
      Tab(2).Control(4)=   "chkExcludeFarFuture"
      Tab(2).Control(5)=   "Frame3"
      Tab(2).Control(6)=   "Frame2"
      Tab(2).Control(7)=   "adoFileRequests"
      Tab(2).Control(8)=   "grdFileRequests"
      Tab(2).Control(9)=   "cmbOperationType"
      Tab(2).Control(10)=   "datSearchDate"
      Tab(2).Control(11)=   "Label3"
      Tab(2).Control(12)=   "Label2"
      Tab(2).ControlCount=   13
      TabCaption(3)   =   "Excel Techrev Output Requests"
      TabPicture(3)   =   "frmTranscoding.frx":0C16
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "grdExcelRequests"
      Tab(3).Control(1)=   "adoExcelRequests"
      Tab(3).ControlCount=   2
      TabCaption(4)   =   "Workflow Monitoring"
      TabPicture(4)   =   "frmTranscoding.frx":0C32
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Label5(0)"
      Tab(4).Control(1)=   "grdInstanceVariables"
      Tab(4).Control(2)=   "grdWorkflows"
      Tab(4).Control(3)=   "adoWorkflows"
      Tab(4).Control(4)=   "chkHideAssociateFile"
      Tab(4).Control(5)=   "chkHideDeliveryHold"
      Tab(4).Control(6)=   "txtSearchCompanyID"
      Tab(4).Control(7)=   "cmdClear(1)"
      Tab(4).Control(8)=   "txtWorkflowSynopsis"
      Tab(4).Control(9)=   "adoInstanceVariables"
      Tab(4).Control(10)=   "fraWorkflowAdmin"
      Tab(4).Control(11)=   "chkHideCancelled"
      Tab(4).Control(12)=   "chkHideComplete"
      Tab(4).ControlCount=   13
      TabCaption(5)   =   "Workflow Specifications"
      TabPicture(5)   =   "frmTranscoding.frx":0C4E
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "cmdConfigSynopsis"
      Tab(5).Control(1)=   "fraSynopsis"
      Tab(5).Control(2)=   "fraBBCPremValues"
      Tab(5).Control(3)=   "cmdMakeNewBBCPCVariant"
      Tab(5).Control(4)=   "cmdCloneVariant"
      Tab(5).Control(5)=   "cmdCloneWorkflow"
      Tab(5).Control(6)=   "cmdApplyDefaaultVariables"
      Tab(5).Control(7)=   "ddnCompany"
      Tab(5).Control(8)=   "grdWorkflowTaskVariables"
      Tab(5).Control(9)=   "ddnWorkflowDefinitions(1)"
      Tab(5).Control(10)=   "adoWorkflowDefinitions(2)"
      Tab(5).Control(11)=   "grdWorkflowVariants"
      Tab(5).Control(12)=   "adoWorkflowTaskTypes(0)"
      Tab(5).Control(13)=   "grdWorkflowTaskDefaultVariables"
      Tab(5).Control(14)=   "ddnTaskTypes(1)"
      Tab(5).Control(15)=   "adoWorkflowTaskTypes(2)"
      Tab(5).Control(16)=   "ddnTaskTypes(0)"
      Tab(5).Control(17)=   "ddnWorkflowDefinitions(0)"
      Tab(5).Control(18)=   "adoWorkflowTaskTypeDefaultVariables(1)"
      Tab(5).Control(19)=   "adoWorkflowTaskTypes(1)"
      Tab(5).Control(20)=   "adoWorkflowTasks(1)"
      Tab(5).Control(21)=   "adoWorkflowDefinitions(1)"
      Tab(5).Control(22)=   "adoWorkflowTaskTypeDefaultVariables(0)"
      Tab(5).Control(23)=   "grdTaskTypeDefaultVariables"
      Tab(5).Control(24)=   "adoWorkflowTasks(0)"
      Tab(5).Control(25)=   "grdWorkflowTasks"
      Tab(5).Control(26)=   "adoWorkflowDefinitions(0)"
      Tab(5).Control(27)=   "grdWorkflowDefinitions"
      Tab(5).Control(28)=   "grdWorkflowTaskType"
      Tab(5).Control(29)=   "grdMissingVariables"
      Tab(5).Control(30)=   "adoStatus"
      Tab(5).Control(31)=   "adoNextTask"
      Tab(5).Control(32)=   "adoCompany"
      Tab(5).Control(33)=   "adoWorkflowVariants(0)"
      Tab(5).Control(34)=   "adoWorkflowTaskDefaultVariables(0)"
      Tab(5).Control(35)=   "adoWorkflowTaskVariables"
      Tab(5).Control(36)=   "adoMissingVariables"
      Tab(5).Control(37)=   "Label6(5)"
      Tab(5).Control(38)=   "Label6(4)"
      Tab(5).Control(39)=   "Label6(3)"
      Tab(5).Control(40)=   "Label6(2)"
      Tab(5).Control(41)=   "Label6(1)"
      Tab(5).Control(42)=   "Label6(0)"
      Tab(5).Control(43)=   "lblTaskID"
      Tab(5).Control(44)=   "lblLastTaskID"
      Tab(5).Control(45)=   "lblWorkflowID"
      Tab(5).Control(46)=   "lblTaskTypeID"
      Tab(5).Control(47)=   "lblWorkflowDefinitionID"
      Tab(5).Control(48)=   "lblVariantID"
      Tab(5).ControlCount=   49
      TabCaption(6)   =   "Hybrid View"
      TabPicture(6)   =   "frmTranscoding.frx":0C6A
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "adoTranscodeSpecsHybrid"
      Tab(6).Control(1)=   "ddnTranscodeSpecsHybrid"
      Tab(6).Control(2)=   "ddnStatusHybrid"
      Tab(6).Control(3)=   "grdFileRequestsHybrid"
      Tab(6).Control(4)=   "grdTranscodeRequestsHybrid"
      Tab(6).Control(5)=   "adoTranscodeRequestsHybrid"
      Tab(6).Control(6)=   "adoFileRequestsHybrid"
      Tab(6).ControlCount=   7
      Begin MSAdodcLib.Adodc adoFileRequestsHybrid 
         Height          =   330
         Left            =   -62520
         Top             =   540
         Width           =   4485
         _ExtentX        =   7911
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoFileRequests"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoTranscodeRequestsHybrid 
         Height          =   330
         Left            =   -74880
         Top             =   540
         Width           =   2745
         _ExtentX        =   4842
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoTranscodeRequests"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.CheckBox chkExludeMediaInfo 
         Caption         =   "Exclude MediaInfo"
         Height          =   255
         Left            =   -50640
         TabIndex        =   333
         Top             =   660
         Width           =   1755
      End
      Begin VB.ComboBox cmbSearchTranscodeSystem 
         Height          =   315
         ItemData        =   "frmTranscoding.frx":0C86
         Left            =   24900
         List            =   "frmTranscoding.frx":0C88
         TabIndex        =   326
         ToolTipText     =   "The Version for the Tape"
         Top             =   420
         Width           =   3075
      End
      Begin VB.Frame Frame4 
         BorderStyle     =   0  'None
         Height          =   225
         Left            =   -50640
         TabIndex        =   319
         Top             =   420
         Width           =   3135
         Begin VB.OptionButton optWidth 
            Caption         =   "Wide"
            Height          =   255
            Index           =   2
            Left            =   2280
            TabIndex        =   322
            Top             =   0
            Width           =   1155
         End
         Begin VB.OptionButton optWidth 
            Caption         =   "Medium"
            Height          =   255
            Index           =   1
            Left            =   1080
            TabIndex        =   321
            Top             =   0
            Width           =   1155
         End
         Begin VB.OptionButton optWidth 
            Caption         =   "Narrow"
            Height          =   255
            Index           =   0
            Left            =   0
            TabIndex        =   320
            Top             =   0
            Value           =   -1  'True
            Width           =   1155
         End
      End
      Begin VB.CommandButton cmdConfigSynopsis 
         Caption         =   "Show Workflow Synopsis"
         Height          =   555
         Left            =   -48180
         TabIndex        =   312
         ToolTipText     =   "Creates Task Variable for all Tasks in Current Workflow from TaskType Default Variables"
         Top             =   5820
         Width           =   1395
      End
      Begin VB.Frame fraSynopsis 
         Caption         =   "Workflow Synopsis"
         Height          =   13635
         Left            =   -66960
         TabIndex        =   309
         Top             =   1200
         Visible         =   0   'False
         Width           =   13515
         Begin VB.CommandButton cmdHideSynopsis 
            Caption         =   "Hide the Synopsis"
            Height          =   435
            Left            =   5760
            TabIndex        =   311
            ToolTipText     =   "Creates Task Variables and Values for all Tasks in New Variant from the Old VariantID"
            Top             =   13080
            Width           =   2115
         End
         Begin VB.TextBox txtWorkflowConfigurationSynopsis 
            Height          =   12735
            Left            =   120
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   310
            Top             =   240
            Width           =   13275
         End
      End
      Begin VB.Frame fraBBCPremValues 
         Caption         =   "New BBC Prem Variant Settings"
         Height          =   4995
         Left            =   -71940
         TabIndex        =   286
         Top             =   3240
         Visible         =   0   'False
         Width           =   6615
         Begin VB.CommandButton cmdCancelThisActivity 
            Caption         =   "Cancel This Activity"
            Height          =   555
            Left            =   180
            TabIndex        =   308
            ToolTipText     =   "Creates Task Variables and Values for all Tasks in New Variant from the Old VariantID"
            Top             =   4200
            Width           =   2115
         End
         Begin VB.CommandButton cmdActuallyMakeTheVariant 
            Caption         =   "Make the BBCPrem Variant with these Settings"
            Height          =   555
            Left            =   2400
            TabIndex        =   299
            ToolTipText     =   "Creates Task Variables and Values for all Tasks in New Variant from the Old VariantID"
            Top             =   4200
            Width           =   3735
         End
         Begin VB.TextBox txtSourceShape 
            Height          =   345
            Left            =   2340
            TabIndex        =   298
            Top             =   3660
            Width           =   3855
         End
         Begin VB.TextBox txtTranscodeSegment 
            Height          =   345
            Left            =   2340
            TabIndex        =   297
            Top             =   3300
            Width           =   3855
         End
         Begin VB.TextBox txtTranscodeSpecID 
            Height          =   345
            Left            =   2340
            TabIndex        =   296
            Top             =   2940
            Width           =   3855
         End
         Begin VB.TextBox txtDigitalDestinationID 
            Height          =   345
            Left            =   2340
            TabIndex        =   295
            Top             =   2580
            Width           =   3855
         End
         Begin VB.TextBox txtCetaJobID 
            Height          =   345
            Left            =   2340
            TabIndex        =   294
            Top             =   2220
            Width           =   3855
         End
         Begin VB.TextBox txtReleasePolicy 
            Height          =   345
            Left            =   2340
            TabIndex        =   293
            Top             =   1860
            Width           =   3855
         End
         Begin VB.TextBox txtOutputFilenamePattern 
            Height          =   345
            Left            =   2340
            TabIndex        =   291
            Top             =   1500
            Width           =   3855
         End
         Begin VB.TextBox txtOrganisation 
            Height          =   345
            Left            =   2340
            TabIndex        =   289
            Top             =   1140
            Width           =   3855
         End
         Begin VB.TextBox txtVariantName 
            Height          =   345
            Left            =   2340
            TabIndex        =   287
            Top             =   420
            Width           =   3855
         End
         Begin VB.Label Label7 
            Caption         =   "Workflow Name"
            Height          =   255
            Index           =   9
            Left            =   180
            TabIndex        =   307
            Top             =   840
            Width           =   1995
         End
         Begin VB.Label lblBBCPCWorkflowName 
            BackColor       =   &H0080FFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   345
            Left            =   2340
            TabIndex        =   306
            Tag             =   "NOCLEAR"
            Top             =   780
            Width           =   3855
         End
         Begin VB.Label Label7 
            Caption         =   "Source Shape"
            Height          =   255
            Index           =   8
            Left            =   180
            TabIndex        =   305
            Top             =   3720
            Width           =   1995
         End
         Begin VB.Label Label7 
            Caption         =   "Transcode Segment"
            Height          =   255
            Index           =   7
            Left            =   180
            TabIndex        =   304
            Top             =   3360
            Width           =   1995
         End
         Begin VB.Label Label7 
            Caption         =   "Transcode Spec ID"
            Height          =   255
            Index           =   6
            Left            =   180
            TabIndex        =   303
            Top             =   3000
            Width           =   1995
         End
         Begin VB.Label Label7 
            Caption         =   "Digital Destination ID"
            Height          =   255
            Index           =   5
            Left            =   180
            TabIndex        =   302
            Top             =   2640
            Width           =   1995
         End
         Begin VB.Label Label7 
            Caption         =   "CETA Job ID"
            Height          =   255
            Index           =   4
            Left            =   180
            TabIndex        =   301
            Top             =   2280
            Width           =   1995
         End
         Begin VB.Label Label7 
            Caption         =   "Release Policy"
            Height          =   255
            Index           =   3
            Left            =   180
            TabIndex        =   300
            Top             =   1920
            Width           =   1995
         End
         Begin VB.Label Label7 
            Caption         =   "Output Filename Pattern"
            Height          =   255
            Index           =   2
            Left            =   180
            TabIndex        =   292
            Top             =   1560
            Width           =   1995
         End
         Begin VB.Label Label7 
            Caption         =   "Organisation"
            Height          =   255
            Index           =   1
            Left            =   180
            TabIndex        =   290
            Top             =   1200
            Width           =   1995
         End
         Begin VB.Label Label7 
            Caption         =   "New Variant Name"
            Height          =   255
            Index           =   0
            Left            =   180
            TabIndex        =   288
            Top             =   480
            Width           =   1995
         End
      End
      Begin VB.CommandButton cmdMakeNewBBCPCVariant 
         Caption         =   "Make New BBCPrem Variant"
         Height          =   555
         Left            =   -48180
         TabIndex        =   285
         ToolTipText     =   "Creates Task Variables and Values for all Tasks in New Variant from the Old VariantID"
         Top             =   10380
         Width           =   1395
      End
      Begin VB.CommandButton cmdCloneVariant 
         Caption         =   "Clone Variant"
         Height          =   555
         Left            =   -48180
         TabIndex        =   284
         ToolTipText     =   "Creates Task Variables and Values for all Tasks in New Variant from the Old VariantID"
         Top             =   9720
         Width           =   1395
      End
      Begin VB.CommandButton cmdCloneWorkflow 
         Caption         =   "Clone Workflow"
         Height          =   555
         Left            =   -48180
         TabIndex        =   283
         ToolTipText     =   "Creates all Tasks and Variables from OldworkflowID into NewWorkflowID"
         Top             =   2400
         Width           =   1395
      End
      Begin VB.CommandButton cmdApplyDefaaultVariables 
         Caption         =   "Apply Default Variables"
         Height          =   555
         Left            =   -48180
         TabIndex        =   270
         ToolTipText     =   "Creates Task Variable for all Tasks in Current Workflow from TaskType Default Variables"
         Top             =   5100
         Width           =   1395
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnCompany 
         Bindings        =   "frmTranscoding.frx":0C8A
         Height          =   2415
         Left            =   -69240
         TabIndex        =   268
         Top             =   11460
         Width           =   4035
         DataFieldList   =   "Name"
         _Version        =   196617
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   2
         Columns(0).Width=   1746
         Columns(0).Caption=   "CompanyID"
         Columns(0).Name =   "CompanyID"
         Columns(0).DataField=   "CompanyID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   9684
         Columns(1).Caption=   "Name"
         Columns(1).Name =   "Name"
         Columns(1).DataField=   "Name"
         Columns(1).FieldLen=   256
         _ExtentX        =   7117
         _ExtentY        =   4260
         _StockProps     =   77
         DataFieldToDisplay=   "Name"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdWorkflowTaskVariables 
         Bindings        =   "frmTranscoding.frx":0CA3
         Height          =   2715
         Left            =   -59940
         TabIndex        =   267
         Top             =   9540
         Width           =   11565
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Caption=   "TaskVariableID"
         Columns(0).Name =   "TaskVariableID"
         Columns(0).DataField=   "TaskVariableID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "WorkflowTaskID"
         Columns(1).Name =   "WorkflowTaskID"
         Columns(1).DataField=   "WorkflowTaskID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "VariantID"
         Columns(2).Name =   "VariantID"
         Columns(2).DataField=   "VariantID"
         Columns(2).FieldLen=   256
         Columns(3).Width=   4895
         Columns(3).Caption=   "Variable"
         Columns(3).Name =   "Variable"
         Columns(3).DataField=   "Variable"
         Columns(3).FieldLen=   256
         Columns(4).Width=   11245
         Columns(4).Caption=   "Value"
         Columns(4).Name =   "Value"
         Columns(4).DataField=   "Value"
         Columns(4).FieldLen=   256
         _ExtentX        =   20399
         _ExtentY        =   4789
         _StockProps     =   79
         Caption         =   "Workflow Task Variables"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnWorkflowDefinitions 
         Bindings        =   "frmTranscoding.frx":0CCA
         Height          =   2415
         Index           =   1
         Left            =   -73740
         TabIndex        =   266
         Top             =   11460
         Width           =   4395
         DataFieldList   =   "WorkflowName"
         _Version        =   196617
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   2
         Columns(0).Width=   1905
         Columns(0).Caption=   "WorkflowID"
         Columns(0).Name =   "WorkflowID"
         Columns(0).DataField=   "WorkflowID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   5292
         Columns(1).Caption=   "WorkflowName"
         Columns(1).Name =   "WorkflowName"
         Columns(1).DataField=   "WorkflowName"
         Columns(1).FieldLen=   256
         _ExtentX        =   7752
         _ExtentY        =   4260
         _StockProps     =   77
         DataFieldToDisplay=   "WorkflowName"
      End
      Begin MSAdodcLib.Adodc adoWorkflowDefinitions 
         Height          =   330
         Index           =   2
         Left            =   -74400
         Top             =   2760
         Visible         =   0   'False
         Width           =   3240
         _ExtentX        =   5715
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoWorkflowDefinitions"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdWorkflowVariants 
         Bindings        =   "frmTranscoding.frx":0CF2
         Height          =   5655
         Left            =   -74640
         TabIndex        =   265
         Top             =   9540
         Width           =   14580
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   8
         Columns(0).Width=   1429
         Columns(0).Caption=   "VariantID"
         Columns(0).Name =   "VariantID"
         Columns(0).DataField=   "VariantID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   4763
         Columns(1).Caption=   "VariantName"
         Columns(1).Name =   "VariantName"
         Columns(1).DataField=   "VariantName"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "WorkflowID"
         Columns(2).Name =   "WorkflowID"
         Columns(2).DataField=   "WorkflowID"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "CompanyID"
         Columns(3).Name =   "CompanyID"
         Columns(3).DataField=   "CompanyID"
         Columns(3).FieldLen=   256
         Columns(4).Width=   5794
         Columns(4).Caption=   "WorkflowName"
         Columns(4).Name =   "WorkflowName"
         Columns(4).FieldLen=   256
         Columns(5).Width=   6244
         Columns(5).Caption=   "CompanyName"
         Columns(5).Name =   "CompanyName"
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Caption=   "FirstTaskID"
         Columns(6).Name =   "FirstTaskID"
         Columns(6).DataField=   "FirstTaskID"
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Caption=   "enabled"
         Columns(7).Name =   "enabled"
         Columns(7).DataField=   "enabled"
         Columns(7).FieldLen=   256
         _ExtentX        =   25717
         _ExtentY        =   9975
         _StockProps     =   79
         Caption         =   "Workflow Variants"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSAdodcLib.Adodc adoWorkflowTaskTypes 
         Height          =   330
         Index           =   0
         Left            =   -68460
         Top             =   2460
         Visible         =   0   'False
         Width           =   4260
         _ExtentX        =   7514
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoWorkflowTaskTypes"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdWorkflowTaskDefaultVariables 
         Bindings        =   "frmTranscoding.frx":0D17
         Height          =   4395
         Left            =   -53340
         TabIndex        =   263
         Top             =   5040
         Width           =   4965
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "DefaultVariableID"
         Columns(0).Name =   "DefaultVariableID"
         Columns(0).DataField=   "DefaultVariableID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "WorkflowID"
         Columns(1).Name =   "WorkflowID"
         Columns(1).DataField=   "WorkflowID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "TaskID"
         Columns(2).Name =   "TaskID"
         Columns(2).DataField=   "TaskID"
         Columns(2).FieldLen=   256
         Columns(3).Width=   4207
         Columns(3).Caption=   "Variable"
         Columns(3).Name =   "Variable"
         Columns(3).DataField=   "Variable"
         Columns(3).FieldLen=   256
         Columns(4).Width=   3572
         Columns(4).Caption=   "Value"
         Columns(4).Name =   "Value"
         Columns(4).DataField=   "Value"
         Columns(4).FieldLen=   256
         _ExtentX        =   8758
         _ExtentY        =   7752
         _StockProps     =   79
         Caption         =   "Workflow Task Default Variables"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnTaskTypes 
         Bindings        =   "frmTranscoding.frx":0D48
         Height          =   2415
         Index           =   1
         Left            =   -56940
         TabIndex        =   262
         Top             =   2340
         Width           =   4455
         DataFieldList   =   "TaskType"
         _Version        =   196617
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   2
         Columns(0).Width=   1799
         Columns(0).Caption=   "TaskTypeID"
         Columns(0).Name =   "TaskTypeID"
         Columns(0).DataField=   "TaskTypeID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   5292
         Columns(1).Caption=   "TaskType"
         Columns(1).Name =   "TaskType"
         Columns(1).DataField=   "TaskType"
         Columns(1).FieldLen=   256
         _ExtentX        =   7858
         _ExtentY        =   4260
         _StockProps     =   77
         DataFieldToDisplay=   "TaskType"
      End
      Begin MSAdodcLib.Adodc adoWorkflowTaskTypes 
         Height          =   330
         Index           =   2
         Left            =   -74400
         Top             =   7140
         Visible         =   0   'False
         Width           =   3000
         _ExtentX        =   5292
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoWorkflowTaskTypes"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnTaskTypes 
         Bindings        =   "frmTranscoding.frx":0D6E
         Height          =   2415
         Index           =   0
         Left            =   -64380
         TabIndex        =   261
         Top             =   6600
         Width           =   4455
         DataFieldList   =   "TaskType"
         _Version        =   196617
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   2
         Columns(0).Width=   1799
         Columns(0).Caption=   "TaskTypeID"
         Columns(0).Name =   "TaskTypeID"
         Columns(0).DataField=   "TaskTypeID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   5292
         Columns(1).Caption=   "TaskType"
         Columns(1).Name =   "TaskType"
         Columns(1).DataField=   "TaskType"
         Columns(1).FieldLen=   256
         _ExtentX        =   7858
         _ExtentY        =   4260
         _StockProps     =   77
         DataFieldToDisplay=   "TaskType"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnWorkflowDefinitions 
         Bindings        =   "frmTranscoding.frx":0D94
         Height          =   2415
         Index           =   0
         Left            =   -69840
         TabIndex        =   260
         Top             =   6660
         Width           =   4395
         DataFieldList   =   "WorkflowName"
         _Version        =   196617
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   2
         Columns(0).Width=   1905
         Columns(0).Caption=   "WorkflowID"
         Columns(0).Name =   "WorkflowID"
         Columns(0).DataField=   "WorkflowID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   5292
         Columns(1).Caption=   "WorkflowName"
         Columns(1).Name =   "WorkflowName"
         Columns(1).DataField=   "WorkflowName"
         Columns(1).FieldLen=   256
         _ExtentX        =   7752
         _ExtentY        =   4260
         _StockProps     =   77
         DataFieldToDisplay=   "WorkflowName"
      End
      Begin MSAdodcLib.Adodc adoWorkflowTaskTypeDefaultVariables 
         Height          =   330
         Index           =   1
         Left            =   -62340
         Top             =   2400
         Visible         =   0   'False
         Width           =   4260
         _ExtentX        =   7514
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoWorkflowTaskTypeDefaultVariables"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoWorkflowTaskTypes 
         Height          =   330
         Index           =   1
         Left            =   -62340
         Top             =   2760
         Visible         =   0   'False
         Width           =   4260
         _ExtentX        =   7514
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoWorkflowTaskTypes"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoWorkflowTasks 
         Height          =   330
         Index           =   1
         Left            =   -74400
         Top             =   6780
         Visible         =   0   'False
         Width           =   3000
         _ExtentX        =   5292
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoWorkflowTasks"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoWorkflowDefinitions 
         Height          =   330
         Index           =   1
         Left            =   -74400
         Top             =   2400
         Visible         =   0   'False
         Width           =   3240
         _ExtentX        =   5715
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoWorkflowDefinitions"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoWorkflowTaskTypeDefaultVariables 
         Height          =   330
         Index           =   0
         Left            =   -62340
         Top             =   2040
         Visible         =   0   'False
         Width           =   4260
         _ExtentX        =   7514
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoWorkflowTaskTypeDefaultVariables"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdTaskTypeDefaultVariables 
         Bindings        =   "frmTranscoding.frx":0DBC
         Height          =   4395
         Left            =   -63360
         TabIndex        =   259
         Top             =   540
         Width           =   14985
         _Version        =   196617
         AllowUpdate     =   0   'False
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   7
         Columns(0).Width=   3704
         Columns(0).Caption=   "TaskTypeDefaultVariableID"
         Columns(0).Name =   "TaskTypeDefaultVariableID"
         Columns(0).DataField=   "TaskTypeDefaultVariableID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "TaskTypeID"
         Columns(1).Name =   "TaskTypeID"
         Columns(1).DataField=   "TaskTypeID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   5292
         Columns(2).Caption=   "Task Type"
         Columns(2).Name =   "TaskType"
         Columns(2).DataField=   "Column 6"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   5292
         Columns(3).Caption=   "Variable"
         Columns(3).Name =   "Variable"
         Columns(3).DataField=   "Variable"
         Columns(3).FieldLen=   256
         Columns(4).Width=   5530
         Columns(4).Caption=   "Value"
         Columns(4).Name =   "Value"
         Columns(4).DataField=   "Value"
         Columns(4).FieldLen=   256
         Columns(5).Width=   3572
         Columns(5).Caption=   "Links To Instance Variable"
         Columns(5).Name =   "LinksToInstanceVariable"
         Columns(5).DataField=   "LinksToInstanceVariable"
         Columns(5).FieldLen=   256
         Columns(6).Width=   1905
         Columns(6).Caption=   "Task Specific"
         Columns(6).Name =   "TaskSpecific"
         Columns(6).DataField=   "TaskSpecific"
         Columns(6).FieldLen=   256
         _ExtentX        =   26432
         _ExtentY        =   7752
         _StockProps     =   79
         Caption         =   "Task Type Default Variables"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSAdodcLib.Adodc adoWorkflowTasks 
         Height          =   330
         Index           =   0
         Left            =   -74400
         Top             =   6420
         Visible         =   0   'False
         Width           =   3000
         _ExtentX        =   5292
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoWorkflowTasks"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdWorkflowTasks 
         Bindings        =   "frmTranscoding.frx":0DF1
         Height          =   4395
         Left            =   -74640
         TabIndex        =   258
         Top             =   5040
         Width           =   21135
         _Version        =   196617
         AllowAddNew     =   -1  'True
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   15
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "WorkflowID"
         Columns(0).Name =   "WorkflowID"
         Columns(0).DataField=   "WorkflowID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   1138
         Columns(1).Caption=   "TaskID"
         Columns(1).Name =   "TaskID"
         Columns(1).DataField=   "TaskID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   5980
         Columns(2).Caption=   "Workflow Name"
         Columns(2).Name =   "WorkflowName"
         Columns(2).FieldLen=   256
         Columns(3).Width=   4207
         Columns(3).Caption=   "StageName"
         Columns(3).Name =   "StageName"
         Columns(3).DataField=   "StageName"
         Columns(3).FieldLen=   256
         Columns(4).Width=   1799
         Columns(4).Caption=   "StagefieldID"
         Columns(4).Name =   "StagefieldID"
         Columns(4).DataField=   "StagefieldID"
         Columns(4).FieldLen=   256
         Columns(5).Width=   2302
         Columns(5).Caption=   "NextTask_PASS"
         Columns(5).Name =   "NextTask_PASS"
         Columns(5).DataField=   "NextTask_PASS"
         Columns(5).FieldLen=   256
         Columns(6).Width=   2170
         Columns(6).Caption=   "NextTask_FAIL"
         Columns(6).Name =   "NextTask_FAIL"
         Columns(6).DataField=   "NextTask_FAIL"
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "TaskTypeID"
         Columns(7).Name =   "TaskTypeID"
         Columns(7).DataField=   "TaskTypeID"
         Columns(7).FieldLen=   256
         Columns(8).Width=   3784
         Columns(8).Caption=   "Task Type"
         Columns(8).Name =   "TaskType"
         Columns(8).FieldLen=   256
         Columns(9).Width=   2355
         Columns(9).Caption=   "FieldStatus_Pass"
         Columns(9).Name =   "FieldStatus_Pass"
         Columns(9).DataField=   "FieldStatus_Pass"
         Columns(9).FieldLen=   256
         Columns(10).Width=   2275
         Columns(10).Caption=   "FieldStatus_Fail"
         Columns(10).Name=   "FieldStatus_Fail"
         Columns(10).DataField=   "FieldStatus_Fail"
         Columns(10).FieldLen=   256
         Columns(11).Width=   2540
         Columns(11).Caption=   "FieldStatus_Initial"
         Columns(11).Name=   "FieldStatus_Initial"
         Columns(11).DataField=   "FieldStatus_Initial"
         Columns(11).FieldLen=   256
         Columns(12).Width=   2778
         Columns(12).Caption=   "SetsConclusionField"
         Columns(12).Name=   "SetsConclusionField"
         Columns(12).DataField=   "SetsConclusionField"
         Columns(12).FieldLen=   256
         Columns(13).Width=   2275
         Columns(13).Caption=   "SetsItemBillable"
         Columns(13).Name=   "SetsItemBillable"
         Columns(13).DataField=   "SetsItemBillable"
         Columns(13).FieldLen=   256
         Columns(14).Width=   2619
         Columns(14).Caption=   "SetsItemRejected"
         Columns(14).Name=   "SetsItemRejected"
         Columns(14).DataField=   "SetsItemRejected"
         Columns(14).FieldLen=   256
         _ExtentX        =   37280
         _ExtentY        =   7752
         _StockProps     =   79
         Caption         =   "Workflow Tasks"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSAdodcLib.Adodc adoWorkflowDefinitions 
         Height          =   330
         Index           =   0
         Left            =   -74400
         Top             =   2040
         Visible         =   0   'False
         Width           =   3240
         _ExtentX        =   5715
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoWorkflowDefinitions"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdWorkflowDefinitions 
         Bindings        =   "frmTranscoding.frx":0E13
         Height          =   4395
         Left            =   -74640
         TabIndex        =   257
         Top             =   540
         Width           =   5475
         _Version        =   196617
         AllowAddNew     =   -1  'True
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   2
         Columns(0).Width=   1852
         Columns(0).Caption=   "Workflow ID"
         Columns(0).Name =   "WorkflowID"
         Columns(0).DataField=   "WorkflowID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   6720
         Columns(1).Caption=   "Workflow Name"
         Columns(1).Name =   "WorkflowName"
         Columns(1).DataField=   "WorkflowName"
         Columns(1).FieldLen=   256
         _ExtentX        =   9657
         _ExtentY        =   7752
         _StockProps     =   79
         Caption         =   "Workflow Definitions"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CheckBox chkHideComplete 
         Caption         =   "Hide Complete"
         Height          =   255
         Left            =   -68400
         TabIndex        =   251
         Top             =   540
         Value           =   1  'Checked
         Width           =   1755
      End
      Begin VB.CheckBox chkHideCancelled 
         Caption         =   "Hide Cancelled"
         Height          =   255
         Left            =   -70200
         TabIndex        =   250
         Top             =   540
         Value           =   1  'Checked
         Width           =   1755
      End
      Begin VB.Frame fraWorkflowAdmin 
         Caption         =   "Workflow Admin Controls"
         Height          =   7155
         Left            =   -50160
         TabIndex        =   244
         Top             =   8160
         Width           =   2895
         Begin VB.CommandButton cmdNextTaskToRun 
            Caption         =   "Set New Next Task"
            Height          =   375
            Left            =   120
            TabIndex        =   256
            ToolTipText     =   "Close this form"
            Top             =   3000
            Width           =   2595
         End
         Begin VB.CommandButton cmdSetNewDueDate 
            Caption         =   "Set New Due Date"
            Height          =   375
            Left            =   120
            TabIndex        =   247
            ToolTipText     =   "Close this form"
            Top             =   1620
            Width           =   2595
         End
         Begin MSComCtl2.DTPicker datDueDate 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "dd/MM/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   3
            EndProperty
            Height          =   315
            Left            =   1320
            TabIndex        =   245
            Tag             =   "NOCHECK"
            ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
            Top             =   1200
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   111214593
            CurrentDate     =   39580
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbNextTaskID 
            Bindings        =   "frmTranscoding.frx":0E3B
            Height          =   315
            Left            =   1500
            TabIndex        =   254
            Top             =   2640
            Width           =   1215
            DataFieldList   =   "TaskID"
            MaxDropDownItems=   32
            AllowInput      =   0   'False
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1217
            Columns(0).Caption=   "TaskID"
            Columns(0).Name =   "TaskID"
            Columns(0).DataField=   "TaskID"
            Columns(0).FieldLen=   256
            Columns(1).Width=   6456
            Columns(1).Caption=   "StageName"
            Columns(1).Name =   "StageName"
            Columns(1).DataField=   "StageName"
            Columns(1).FieldLen=   256
            _ExtentX        =   2143
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "TaskID"
         End
         Begin VB.Line Line4 
            X1              =   180
            X2              =   2700
            Y1              =   3540
            Y2              =   3540
         End
         Begin VB.Label Label5 
            Caption         =   "Next Task to Run"
            Height          =   255
            Index           =   5
            Left            =   120
            TabIndex        =   255
            Top             =   2700
            Width           =   1455
         End
         Begin VB.Label Label5 
            Alignment       =   2  'Center
            Caption         =   "Reset Instance Next Task"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   4
            Left            =   180
            TabIndex        =   253
            Top             =   2280
            Width           =   2535
         End
         Begin VB.Line Line3 
            X1              =   180
            X2              =   2700
            Y1              =   2160
            Y2              =   2160
         End
         Begin VB.Label Label5 
            Alignment       =   2  'Center
            Caption         =   "Reset Job Due Date"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   3
            Left            =   180
            TabIndex        =   252
            Top             =   840
            Width           =   2535
         End
         Begin VB.Label Label5 
            Caption         =   "InstanceID"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   249
            Top             =   360
            Width           =   1095
         End
         Begin VB.Label lblInstanceID 
            BackColor       =   &H0080FFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Left            =   1320
            TabIndex        =   248
            Top             =   300
            Width           =   1395
         End
         Begin VB.Line Line2 
            X1              =   180
            X2              =   2700
            Y1              =   720
            Y2              =   720
         End
         Begin VB.Label Label5 
            Caption         =   "New Due Date"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   246
            Top             =   1260
            Width           =   1455
         End
      End
      Begin MSAdodcLib.Adodc adoInstanceVariables 
         Height          =   330
         Left            =   -61500
         Top             =   8220
         Visible         =   0   'False
         Width           =   2865
         _ExtentX        =   5054
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "InstanceVariables"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.TextBox txtWorkflowSynopsis 
         Height          =   7155
         Left            =   -74880
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   242
         Top             =   8220
         Width           =   13275
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   375
         Index           =   1
         Left            =   -62760
         TabIndex        =   241
         ToolTipText     =   "Close this form"
         Top             =   480
         Width           =   615
      End
      Begin VB.TextBox txtSearchCompanyID 
         Height          =   345
         Left            =   -63600
         TabIndex        =   239
         Top             =   480
         Width           =   675
      End
      Begin VB.CheckBox chkHideDeliveryHold 
         Caption         =   "Hide Delivery Hold"
         Height          =   255
         Left            =   -72120
         TabIndex        =   238
         Top             =   540
         Width           =   1755
      End
      Begin VB.CheckBox chkHideAssociateFile 
         Caption         =   "Hide AssociateFile Workflows"
         Height          =   255
         Left            =   -74820
         TabIndex        =   230
         Top             =   540
         Width           =   2595
      End
      Begin MSAdodcLib.Adodc adoWorkflows 
         Height          =   330
         Left            =   -74880
         Top             =   960
         Width           =   2865
         _ExtentX        =   5054
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoWorkflows"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoExcelRequests 
         Height          =   330
         Left            =   -74880
         Top             =   960
         Width           =   2865
         _ExtentX        =   5054
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoExcelRequests"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.CheckBox chkShowHiddenFileRequests 
         Caption         =   "Show Hidden Requests"
         Height          =   255
         Left            =   -53220
         TabIndex        =   225
         Top             =   390
         Width           =   2235
      End
      Begin VB.CheckBox chkShowHiddenRequests 
         Caption         =   "Show Hidden Requests"
         Height          =   255
         Left            =   20760
         TabIndex        =   224
         Top             =   480
         Width           =   2235
      End
      Begin VB.CheckBox chkTranscodeSystem 
         Caption         =   "Vantage API Transcodes"
         Height          =   255
         Index           =   4
         Left            =   -71280
         TabIndex        =   222
         Top             =   420
         Value           =   1  'Checked
         Width           =   2415
      End
      Begin VB.CheckBox chkFilterFinishingOff 
         Caption         =   "Finishing Off"
         Height          =   195
         Left            =   5280
         TabIndex        =   218
         Top             =   480
         Value           =   1  'Checked
         Width           =   1635
      End
      Begin VB.CommandButton cmdChange 
         Caption         =   "Clear all Transcode Systems"
         Height          =   375
         Left            =   -74820
         TabIndex        =   212
         ToolTipText     =   "Close this form"
         Top             =   660
         Width           =   3315
      End
      Begin VB.CheckBox chkTranscodeSystem 
         Caption         =   "All Other Transcodes"
         Height          =   255
         Index           =   17
         Left            =   -68520
         TabIndex        =   211
         Top             =   720
         Width           =   2715
      End
      Begin VB.CheckBox chkTranscodeSystem 
         Caption         =   "FFMpeg_Thumb Transcodes"
         Height          =   255
         Index           =   11
         Left            =   -68520
         TabIndex        =   210
         Top             =   420
         Value           =   1  'Checked
         Width           =   2475
      End
      Begin VB.CheckBox chkTranscodeSystem 
         Caption         =   "FFMpeg_Generic Transcodes"
         Height          =   255
         Index           =   3
         Left            =   -71280
         TabIndex        =   209
         Top             =   720
         Value           =   1  'Checked
         Width           =   2475
      End
      Begin MSAdodcLib.Adodc adoTranscodeSpecs 
         Height          =   330
         Left            =   -74880
         Top             =   1200
         Width           =   2865
         _ExtentX        =   5054
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoTranscodeSpecs"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.OptionButton optShowTranscodeSpecs 
         Caption         =   "Show All Specs"
         Height          =   255
         Index           =   1
         Left            =   -72900
         TabIndex        =   152
         Top             =   420
         Width           =   1515
      End
      Begin VB.OptionButton optShowTranscodeSpecs 
         Caption         =   "Show Enabled Specs"
         Height          =   255
         Index           =   0
         Left            =   -74820
         TabIndex        =   151
         Top             =   420
         Value           =   -1  'True
         Width           =   1815
      End
      Begin VB.CheckBox chkJustMyStuff 
         Caption         =   "Just My Requests"
         Height          =   255
         Left            =   -63660
         TabIndex        =   142
         Top             =   480
         Width           =   1695
      End
      Begin VB.CheckBox chkExcludeFarFuture 
         Caption         =   "Exclude Far Future"
         Height          =   255
         Left            =   -53220
         TabIndex        =   139
         Top             =   660
         Width           =   1755
      End
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Left            =   -72600
         TabIndex        =   134
         Top             =   320
         Width           =   8415
         Begin VB.OptionButton optShowAllButComplete 
            Caption         =   "Everything except Complete"
            Height          =   255
            Left            =   0
            TabIndex        =   150
            Top             =   360
            Width           =   2595
         End
         Begin VB.OptionButton optShowNotStarted 
            Caption         =   "Not Started"
            Height          =   255
            Left            =   2700
            TabIndex        =   145
            Top             =   360
            Width           =   1515
         End
         Begin VB.OptionButton optShowRunning 
            Caption         =   "Running"
            Height          =   255
            Left            =   2700
            TabIndex        =   144
            Top             =   120
            Width           =   1035
         End
         Begin VB.OptionButton optShowAll 
            Caption         =   "All"
            Height          =   255
            Left            =   6060
            TabIndex        =   138
            Top             =   120
            Value           =   -1  'True
            Width           =   615
         End
         Begin VB.OptionButton optShowFailed 
            Caption         =   "Failed"
            Height          =   255
            Left            =   4500
            TabIndex        =   137
            Top             =   360
            Width           =   855
         End
         Begin VB.OptionButton optShowComplete 
            Caption         =   "Complete"
            Height          =   255
            Left            =   4500
            TabIndex        =   136
            Top             =   120
            Width           =   1095
         End
         Begin VB.OptionButton optShowIncomplete 
            Caption         =   "Incomplete"
            Height          =   255
            Left            =   0
            TabIndex        =   135
            Top             =   120
            Width           =   1215
         End
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   -74880
         TabIndex        =   131
         Top             =   320
         Width           =   2115
         Begin VB.OptionButton optShowMovementRequests 
            Caption         =   "Show All Requests"
            Height          =   255
            Index           =   1
            Left            =   0
            TabIndex        =   133
            Top             =   360
            Width           =   1755
         End
         Begin VB.OptionButton optShowMovementRequests 
            Caption         =   "Show 4 days  Requests"
            Height          =   255
            Index           =   0
            Left            =   0
            TabIndex        =   132
            Top             =   120
            Value           =   -1  'True
            Width           =   2115
         End
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   12540
         TabIndex        =   126
         Top             =   480
         Width           =   8475
         Begin VB.OptionButton optShowRequests 
            Caption         =   "Show 24 Hours"
            Height          =   255
            Index           =   3
            Left            =   0
            TabIndex        =   130
            Top             =   0
            Width           =   2115
         End
         Begin VB.OptionButton optShowRequests 
            Caption         =   "Show 48 Hours"
            Height          =   255
            Index           =   2
            Left            =   2100
            TabIndex        =   129
            Top             =   0
            Width           =   2115
         End
         Begin VB.OptionButton optShowRequests 
            Caption         =   "Show 4 days  Requests"
            Height          =   255
            Index           =   1
            Left            =   4200
            TabIndex        =   128
            Top             =   0
            Value           =   -1  'True
            Width           =   2115
         End
         Begin VB.OptionButton optShowRequests 
            Caption         =   "Show All Requests"
            Height          =   255
            Index           =   0
            Left            =   6300
            TabIndex        =   127
            Top             =   0
            Width           =   2115
         End
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnStatus 
         Bindings        =   "frmTranscoding.frx":0E55
         Height          =   1095
         Left            =   2580
         TabIndex        =   15
         Top             =   13560
         Width           =   6555
         DataFieldList   =   "description"
         MaxDropDownItems=   16
         _Version        =   196617
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   2
         Columns(0).Width=   3201
         Columns(0).Caption=   "transcodestatusID"
         Columns(0).Name =   "transcodestatusID"
         Columns(0).DataField=   "transcodestatusID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   4577
         Columns(1).Caption=   "description"
         Columns(1).Name =   "description"
         Columns(1).DataField=   "description"
         Columns(1).FieldLen=   256
         _ExtentX        =   11562
         _ExtentY        =   1931
         _StockProps     =   77
         DataFieldToDisplay=   "description"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnTranscodeSpecs 
         Bindings        =   "frmTranscoding.frx":0E6D
         Height          =   1095
         Left            =   11400
         TabIndex        =   14
         Top             =   13500
         Width           =   6555
         DataFieldList   =   "transcodename"
         MaxDropDownItems=   16
         _Version        =   196617
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   3
         Columns(0).Width=   926
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "transcodespecID"
         Columns(0).DataField=   "transcodespecID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   6879
         Columns(1).Caption=   "transcodename"
         Columns(1).Name =   "transcodename"
         Columns(1).DataField=   "transcodename"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3810
         Columns(2).Caption=   "transcodesystem"
         Columns(2).Name =   "transcodesystem"
         Columns(2).DataField=   "transcodesystem"
         Columns(2).FieldLen=   256
         _ExtentX        =   11562
         _ExtentY        =   1931
         _StockProps     =   77
         DataFieldToDisplay=   "transcodename"
      End
      Begin VB.Frame frmTranscoding 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   12195
         Left            =   -64200
         TabIndex        =   17
         Top             =   950
         Width           =   15600
         Begin VB.CheckBox chkReStripe 
            Caption         =   "Is a Re-Stripe Transcode"
            Height          =   315
            Left            =   5940
            TabIndex        =   330
            Top             =   1260
            Width           =   2115
         End
         Begin VB.CheckBox chkNoReferenceCheck 
            Caption         =   "Don't Validate ClipReference"
            Height          =   315
            Left            =   13080
            TabIndex        =   313
            Top             =   900
            Visible         =   0   'False
            Width           =   2655
         End
         Begin VB.CheckBox chkNoDurationCheck 
            Alignment       =   1  'Right Justify
            Caption         =   "No Duration Check"
            Height          =   315
            Index           =   5
            Left            =   12150
            TabIndex        =   237
            Top             =   10740
            Width           =   1965
         End
         Begin VB.CheckBox chkNoDurationCheck 
            Alignment       =   1  'Right Justify
            Caption         =   "No Duration Check"
            Height          =   315
            Index           =   4
            Left            =   12150
            TabIndex        =   236
            Top             =   8880
            Width           =   1965
         End
         Begin VB.CheckBox chkNoDurationCheck 
            Alignment       =   1  'Right Justify
            Caption         =   "No Duration Check"
            Height          =   315
            Index           =   3
            Left            =   12150
            TabIndex        =   235
            Top             =   7020
            Width           =   1965
         End
         Begin VB.CheckBox chkNoDurationCheck 
            Alignment       =   1  'Right Justify
            Caption         =   "No Duration Check"
            Height          =   315
            Index           =   2
            Left            =   12150
            TabIndex        =   234
            Top             =   5160
            Width           =   1965
         End
         Begin VB.CheckBox chkNoDurationCheck 
            Alignment       =   1  'Right Justify
            Caption         =   "No Duration Check"
            Height          =   315
            Index           =   1
            Left            =   12150
            TabIndex        =   233
            Top             =   3300
            Width           =   1965
         End
         Begin VB.ComboBox txtExplicitFFMpegVersion 
            Height          =   315
            Left            =   2100
            TabIndex        =   228
            ToolTipText     =   "The Clip Codec"
            Top             =   2580
            Width           =   1635
         End
         Begin VB.CommandButton cmdClearSpec 
            Caption         =   "Clr"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   6
            Left            =   15120
            TabIndex        =   221
            Top             =   1260
            Width           =   435
         End
         Begin VB.TextBox txtOtherSpecs 
            Height          =   675
            Left            =   2100
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   214
            ToolTipText     =   "Notes for the clip"
            Top             =   1620
            Width           =   5835
         End
         Begin VB.ComboBox cmbAudioMapping 
            Height          =   315
            Index           =   3
            Left            =   2100
            TabIndex        =   208
            ToolTipText     =   "The Clip Codec"
            Top             =   7020
            Width           =   5475
         End
         Begin VB.ComboBox cmbAudioMapping 
            Height          =   315
            Index           =   2
            Left            =   2100
            TabIndex        =   207
            ToolTipText     =   "The Clip Codec"
            Top             =   5160
            Width           =   5475
         End
         Begin VB.ComboBox cmbAudioMapping 
            Height          =   315
            Index           =   4
            Left            =   2100
            TabIndex        =   206
            ToolTipText     =   "The Clip Codec"
            Top             =   8880
            Width           =   5475
         End
         Begin VB.ComboBox cmbAudioMapping 
            Height          =   315
            Index           =   5
            Left            =   2100
            TabIndex        =   205
            ToolTipText     =   "The Clip Codec"
            Top             =   10740
            Width           =   5475
         End
         Begin VB.ComboBox cmbAudioMapping 
            Height          =   315
            Index           =   1
            Left            =   2100
            TabIndex        =   204
            ToolTipText     =   "The Clip Codec"
            Top             =   3300
            Width           =   5475
         End
         Begin VB.CheckBox chkReplaceOriginalFile 
            Alignment       =   1  'Right Justify
            Caption         =   "Replace Original File"
            Height          =   315
            Left            =   8670
            TabIndex        =   193
            Top             =   2580
            Width           =   2025
         End
         Begin VB.CheckBox chkDestinationDeliveryCopy 
            Alignment       =   1  'Right Justify
            Caption         =   "Dest 5 Delivery Copy"
            Height          =   315
            Index           =   5
            Left            =   12150
            TabIndex        =   192
            Top             =   11820
            Width           =   1965
         End
         Begin VB.CheckBox chkDestinationDeliveryCopy 
            Alignment       =   1  'Right Justify
            Caption         =   "Dest 4 Delivery Copy"
            Height          =   315
            Index           =   4
            Left            =   12150
            TabIndex        =   191
            Top             =   9960
            Width           =   1965
         End
         Begin VB.CheckBox chkDestinationDeliveryCopy 
            Alignment       =   1  'Right Justify
            Caption         =   "Dest 3 Delivery Copy"
            Height          =   315
            Index           =   3
            Left            =   12150
            TabIndex        =   190
            Top             =   8100
            Width           =   1965
         End
         Begin VB.CheckBox chkDestinationDeliveryCopy 
            Alignment       =   1  'Right Justify
            Caption         =   "Dest 2 Delivery Copy"
            Height          =   315
            Index           =   2
            Left            =   12150
            TabIndex        =   189
            Top             =   6240
            Width           =   1965
         End
         Begin VB.CheckBox chkDestinationDeliveryCopy 
            Alignment       =   1  'Right Justify
            Caption         =   "Dest 1 Delivery Copy"
            Height          =   315
            Index           =   1
            Left            =   12150
            TabIndex        =   188
            Top             =   4380
            Width           =   1965
         End
         Begin VB.TextBox txtDeliveryFolder 
            Height          =   315
            Index           =   5
            Left            =   2100
            TabIndex        =   162
            ToolTipText     =   "The clip filename"
            Top             =   11460
            Width           =   5475
         End
         Begin VB.TextBox txtDeliveryFolder 
            Height          =   315
            Index           =   4
            Left            =   2100
            TabIndex        =   161
            ToolTipText     =   "The clip filename"
            Top             =   9600
            Width           =   5475
         End
         Begin VB.TextBox txtDeliveryFolder 
            Height          =   315
            Index           =   3
            Left            =   2100
            TabIndex        =   160
            ToolTipText     =   "The clip filename"
            Top             =   7740
            Width           =   5475
         End
         Begin VB.TextBox txtDeliveryFolder 
            Height          =   315
            Index           =   2
            Left            =   2100
            TabIndex        =   159
            ToolTipText     =   "The clip filename"
            Top             =   5880
            Width           =   5475
         End
         Begin VB.TextBox txtDeliveryFolder 
            Height          =   315
            Index           =   1
            Left            =   2100
            TabIndex        =   158
            ToolTipText     =   "The clip filename"
            Top             =   4020
            Width           =   5475
         End
         Begin VB.CheckBox chkDestinationThrowawayFile 
            Alignment       =   1  'Right Justify
            Caption         =   "Dest 5 Throwaway "
            Height          =   315
            Index           =   5
            Left            =   8670
            TabIndex        =   157
            Top             =   11820
            Width           =   2025
         End
         Begin VB.CheckBox chkDestinationThrowawayFile 
            Alignment       =   1  'Right Justify
            Caption         =   "Dest 4 Throwaway "
            Height          =   315
            Index           =   4
            Left            =   8670
            TabIndex        =   156
            Top             =   9960
            Width           =   2025
         End
         Begin VB.CheckBox chkDestinationThrowawayFile 
            Alignment       =   1  'Right Justify
            Caption         =   "Dest 3 Throwaway "
            Height          =   315
            Index           =   3
            Left            =   8670
            TabIndex        =   155
            Top             =   8100
            Width           =   2025
         End
         Begin VB.CheckBox chkDestinationThrowawayFile 
            Alignment       =   1  'Right Justify
            Caption         =   "Dest 2 Throwaway "
            Height          =   315
            Index           =   2
            Left            =   8670
            TabIndex        =   154
            Top             =   6240
            Width           =   2025
         End
         Begin VB.CheckBox chkDestinationThrowawayFile 
            Alignment       =   1  'Right Justify
            Caption         =   "Dest 1 Throwaway"
            Height          =   315
            Index           =   1
            Left            =   8670
            TabIndex        =   153
            Top             =   4380
            Width           =   2025
         End
         Begin VB.CheckBox chkNexguard 
            Caption         =   "Transcode includes NexGuard Watermarking"
            Height          =   315
            Left            =   8040
            TabIndex        =   149
            Top             =   870
            Width           =   3495
         End
         Begin VB.ComboBox cmbPurpose 
            Height          =   315
            Index           =   5
            Left            =   13920
            TabIndex        =   124
            ToolTipText     =   "The Clip Codec"
            Top             =   10380
            Width           =   1635
         End
         Begin VB.ComboBox cmbPurpose 
            Height          =   315
            Index           =   4
            Left            =   13920
            TabIndex        =   122
            ToolTipText     =   "The Clip Codec"
            Top             =   8520
            Width           =   1635
         End
         Begin VB.ComboBox cmbPurpose 
            Height          =   315
            Index           =   3
            Left            =   13920
            TabIndex        =   120
            ToolTipText     =   "The Clip Codec"
            Top             =   6660
            Width           =   1635
         End
         Begin VB.ComboBox cmbPurpose 
            Height          =   315
            Index           =   2
            Left            =   13920
            TabIndex        =   118
            ToolTipText     =   "The Clip Codec"
            Top             =   4800
            Width           =   1635
         End
         Begin VB.ComboBox cmbPurpose 
            Height          =   315
            Index           =   1
            Left            =   13920
            TabIndex        =   116
            ToolTipText     =   "The Clip Codec"
            Top             =   2940
            Width           =   1635
         End
         Begin VB.TextBox txtWatchfolder 
            Height          =   315
            Left            =   2100
            TabIndex        =   47
            ToolTipText     =   "The clip bitrate"
            Top             =   900
            Width           =   5835
         End
         Begin VB.CommandButton cmdDeleteSpecification 
            Caption         =   "Delete Specification"
            Height          =   315
            Left            =   11160
            TabIndex        =   46
            ToolTipText     =   "Close this form"
            Top             =   120
            Width           =   2055
         End
         Begin VB.CommandButton cmdClearSpec 
            Caption         =   "Clr"
            Height          =   315
            Index           =   5
            Left            =   8160
            TabIndex        =   45
            Top             =   10380
            Width           =   435
         End
         Begin VB.CommandButton cmdClearSpec 
            Caption         =   "Clr"
            Height          =   315
            Index           =   3
            Left            =   8160
            TabIndex        =   44
            Top             =   6660
            Width           =   435
         End
         Begin VB.CommandButton cmdClearSpec 
            Caption         =   "Clr"
            Height          =   315
            Index           =   4
            Left            =   8160
            TabIndex        =   43
            Top             =   8520
            Width           =   435
         End
         Begin VB.CommandButton cmdClearSpec 
            Caption         =   "Clr"
            Height          =   315
            Index           =   1
            Left            =   8160
            TabIndex        =   42
            Top             =   2940
            Width           =   435
         End
         Begin VB.CommandButton cmdClearSpec 
            Caption         =   "Clr"
            Height          =   315
            Index           =   2
            Left            =   8160
            TabIndex        =   41
            Top             =   4800
            Width           =   435
         End
         Begin VB.TextBox txtDestinationExtender 
            Height          =   315
            Index           =   5
            Left            =   10500
            TabIndex        =   40
            ToolTipText     =   "The clip bitrate"
            Top             =   10380
            Width           =   1575
         End
         Begin VB.TextBox txtDestinationFolder 
            Height          =   315
            Index           =   5
            Left            =   2100
            TabIndex        =   39
            ToolTipText     =   "The clip filename"
            Top             =   11100
            Width           =   5475
         End
         Begin VB.TextBox txtDestinationExtender 
            Height          =   315
            Index           =   4
            Left            =   10500
            TabIndex        =   38
            ToolTipText     =   "The clip bitrate"
            Top             =   8520
            Width           =   1575
         End
         Begin VB.TextBox txtDestinationFolder 
            Height          =   315
            Index           =   4
            Left            =   2100
            TabIndex        =   37
            ToolTipText     =   "The clip filename"
            Top             =   9240
            Width           =   5475
         End
         Begin VB.TextBox txtDestinationExtender 
            Height          =   315
            Index           =   3
            Left            =   10500
            TabIndex        =   36
            ToolTipText     =   "The clip bitrate"
            Top             =   6660
            Width           =   1575
         End
         Begin VB.TextBox txtDestinationFolder 
            Height          =   315
            Index           =   3
            Left            =   2100
            TabIndex        =   35
            ToolTipText     =   "The clip filename"
            Top             =   7380
            Width           =   5475
         End
         Begin VB.TextBox txtDestinationExtender 
            Height          =   315
            Index           =   2
            Left            =   10500
            TabIndex        =   34
            ToolTipText     =   "The clip bitrate"
            Top             =   4800
            Width           =   1575
         End
         Begin VB.TextBox txtDestinationFolder 
            Height          =   315
            Index           =   2
            Left            =   2100
            TabIndex        =   33
            ToolTipText     =   "The clip filename"
            Top             =   5520
            Width           =   5475
         End
         Begin VB.TextBox txtTitle 
            Height          =   315
            Left            =   2100
            TabIndex        =   32
            ToolTipText     =   "The clip bitrate"
            Top             =   540
            Width           =   5835
         End
         Begin VB.CommandButton cmdSave 
            Caption         =   "Save Specification"
            Height          =   315
            Left            =   13320
            TabIndex        =   31
            ToolTipText     =   "Close this form"
            Top             =   120
            Width           =   2055
         End
         Begin VB.TextBox txtDestinationFolder 
            Height          =   315
            Index           =   1
            Left            =   2100
            TabIndex        =   30
            ToolTipText     =   "The clip filename"
            Top             =   3660
            Width           =   5475
         End
         Begin VB.CheckBox chkAutoTranscode 
            Alignment       =   1  'Right Justify
            Caption         =   "Web Auto Transcode?"
            Height          =   195
            Left            =   180
            TabIndex        =   29
            Top             =   180
            Width           =   2085
         End
         Begin VB.TextBox txtDestinationExtender 
            Height          =   315
            Index           =   1
            Left            =   10500
            TabIndex        =   28
            ToolTipText     =   "The clip bitrate"
            Top             =   2940
            Width           =   1575
         End
         Begin VB.CommandButton cmdInsertSpecification 
            Caption         =   "Insert Specification"
            Height          =   315
            Left            =   9000
            TabIndex        =   27
            ToolTipText     =   "Close this form"
            Top             =   120
            Width           =   2055
         End
         Begin VB.TextBox txtBillingCode 
            Height          =   315
            Left            =   4800
            TabIndex        =   26
            ToolTipText     =   "The clip bitrate"
            Top             =   120
            Width           =   3135
         End
         Begin VB.CheckBox chkEnabled 
            Caption         =   "Active"
            Height          =   315
            Left            =   8040
            TabIndex        =   25
            Top             =   540
            Width           =   855
         End
         Begin VB.CommandButton cmdClearNewCompany 
            Caption         =   "Clr"
            Height          =   315
            Index           =   5
            Left            =   8160
            TabIndex        =   24
            Top             =   11820
            Width           =   435
         End
         Begin VB.CommandButton cmdClearNewCompany 
            Caption         =   "Clr"
            Height          =   315
            Index           =   4
            Left            =   8160
            TabIndex        =   23
            Top             =   9960
            Width           =   435
         End
         Begin VB.CommandButton cmdClearNewCompany 
            Caption         =   "Clr"
            Height          =   315
            Index           =   3
            Left            =   8160
            TabIndex        =   22
            Top             =   8100
            Width           =   435
         End
         Begin VB.CommandButton cmdClearNewCompany 
            Caption         =   "Clr"
            Height          =   315
            Index           =   2
            Left            =   8160
            TabIndex        =   21
            Top             =   6240
            Width           =   435
         End
         Begin VB.CommandButton cmdClearNewCompany 
            Caption         =   "Clr"
            Height          =   315
            Index           =   1
            Left            =   8160
            TabIndex        =   20
            Top             =   4380
            Width           =   435
         End
         Begin VB.CheckBox chkSkipLogging 
            Caption         =   "Skip Checking of Products"
            Height          =   315
            Left            =   13080
            TabIndex        =   19
            Top             =   540
            Width           =   2655
         End
         Begin VB.CheckBox chkAsOrig 
            Caption         =   "As Orig"
            Height          =   315
            Left            =   12120
            TabIndex        =   18
            Top             =   540
            Width           =   1035
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbBarcode 
            Height          =   315
            Index           =   1
            Left            =   10500
            TabIndex        =   48
            Top             =   3660
            Width           =   1575
            DataFieldList   =   "Column 0"
            MaxDropDownItems=   56
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            HeadLines       =   0
            BackColorOdd    =   12632319
            RowHeight       =   423
            Columns(0).Width=   4233
            Columns(0).Caption=   "barcode"
            Columns(0).Name =   "barcode"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2778
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbMediaSpec 
            Bindings        =   "frmTranscoding.frx":0E8D
            DataField       =   "mediaspecname"
            Height          =   315
            Index           =   1
            Left            =   2100
            TabIndex        =   49
            Top             =   2940
            Width           =   5475
            DataFieldList   =   "mediaspecname"
            MaxDropDownItems=   24
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "mediaspecID"
            Columns(0).Name =   "mediaspecID"
            Columns(0).DataField=   "mediaspecID"
            Columns(0).FieldLen=   256
            Columns(1).Width=   7938
            Columns(1).Caption=   "mediaspecname"
            Columns(1).Name =   "mediaspecname"
            Columns(1).DataField=   "mediaspecname"
            Columns(1).FieldLen=   256
            _ExtentX        =   9657
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbMediaSpec 
            Bindings        =   "frmTranscoding.frx":0EA8
            DataField       =   "mediaspecname"
            Height          =   315
            Index           =   2
            Left            =   2100
            TabIndex        =   50
            Top             =   4800
            Width           =   5475
            DataFieldList   =   "mediaspecname"
            MaxDropDownItems=   24
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "mediaspecID"
            Columns(0).Name =   "mediaspecID"
            Columns(0).DataField=   "mediaspecID"
            Columns(0).FieldLen=   256
            Columns(1).Width=   7938
            Columns(1).Caption=   "mediaspecname"
            Columns(1).Name =   "mediaspecname"
            Columns(1).DataField=   "mediaspecname"
            Columns(1).FieldLen=   256
            _ExtentX        =   9657
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbBarcode 
            Height          =   315
            Index           =   2
            Left            =   10500
            TabIndex        =   51
            Top             =   5520
            Width           =   1575
            DataFieldList   =   "Column 0"
            MaxDropDownItems=   56
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            HeadLines       =   0
            BackColorOdd    =   12632319
            RowHeight       =   423
            Columns(0).Width=   4233
            Columns(0).Caption=   "barcode"
            Columns(0).Name =   "barcode"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2778
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbMediaSpec 
            Bindings        =   "frmTranscoding.frx":0EC3
            DataField       =   "mediaspecname"
            Height          =   315
            Index           =   3
            Left            =   2100
            TabIndex        =   52
            Top             =   6660
            Width           =   5475
            DataFieldList   =   "mediaspecname"
            MaxDropDownItems=   24
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "mediaspecID"
            Columns(0).Name =   "mediaspecID"
            Columns(0).DataField=   "mediaspecID"
            Columns(0).FieldLen=   256
            Columns(1).Width=   7938
            Columns(1).Caption=   "mediaspecname"
            Columns(1).Name =   "mediaspecname"
            Columns(1).DataField=   "mediaspecname"
            Columns(1).FieldLen=   256
            _ExtentX        =   9657
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbBarcode 
            Height          =   315
            Index           =   3
            Left            =   10500
            TabIndex        =   53
            Top             =   7380
            Width           =   1575
            DataFieldList   =   "Column 0"
            MaxDropDownItems=   56
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            HeadLines       =   0
            BackColorOdd    =   12632319
            RowHeight       =   423
            Columns(0).Width=   4233
            Columns(0).Caption=   "barcode"
            Columns(0).Name =   "barcode"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2778
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbMediaSpec 
            Bindings        =   "frmTranscoding.frx":0EDE
            DataField       =   "mediaspecname"
            Height          =   315
            Index           =   4
            Left            =   2100
            TabIndex        =   54
            Top             =   8520
            Width           =   5475
            DataFieldList   =   "mediaspecname"
            MaxDropDownItems=   24
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "mediaspecID"
            Columns(0).Name =   "mediaspecID"
            Columns(0).DataField=   "mediaspecID"
            Columns(0).FieldLen=   256
            Columns(1).Width=   7938
            Columns(1).Caption=   "mediaspecname"
            Columns(1).Name =   "mediaspecname"
            Columns(1).DataField=   "mediaspecname"
            Columns(1).FieldLen=   256
            _ExtentX        =   9657
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbBarcode 
            Height          =   315
            Index           =   4
            Left            =   10500
            TabIndex        =   55
            Top             =   9240
            Width           =   1575
            DataFieldList   =   "Column 0"
            MaxDropDownItems=   56
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            HeadLines       =   0
            BackColorOdd    =   12632319
            RowHeight       =   423
            Columns(0).Width=   4233
            Columns(0).Caption=   "barcode"
            Columns(0).Name =   "barcode"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2778
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbMediaSpec 
            Bindings        =   "frmTranscoding.frx":0EF9
            DataField       =   "mediaspecname"
            Height          =   315
            Index           =   5
            Left            =   2100
            TabIndex        =   56
            Top             =   10380
            Width           =   5475
            DataFieldList   =   "mediaspecname"
            MaxDropDownItems=   24
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "mediaspecID"
            Columns(0).Name =   "mediaspecID"
            Columns(0).DataField=   "mediaspecID"
            Columns(0).FieldLen=   256
            Columns(1).Width=   7938
            Columns(1).Caption=   "mediaspecname"
            Columns(1).Name =   "mediaspecname"
            Columns(1).DataField=   "mediaspecname"
            Columns(1).FieldLen=   256
            _ExtentX        =   9657
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbBarcode 
            Height          =   315
            Index           =   5
            Left            =   10500
            TabIndex        =   57
            Top             =   11100
            Width           =   1575
            DataFieldList   =   "Column 0"
            MaxDropDownItems=   56
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            HeadLines       =   0
            BackColorOdd    =   12632319
            RowHeight       =   423
            Columns(0).Width=   4233
            Columns(0).Caption=   "barcode"
            Columns(0).Name =   "barcode"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2778
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbNewCompany 
            Height          =   315
            Index           =   5
            Left            =   2100
            TabIndex        =   58
            ToolTipText     =   "The company this tape belongs to"
            Top             =   11820
            Width           =   5475
            DataFieldList   =   "name"
            AllowInput      =   0   'False
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   8811
            Columns(0).Caption=   "name"
            Columns(0).Name =   "name"
            Columns(0).DataField=   "name"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3201
            Columns(1).Caption=   "companyID"
            Columns(1).Name =   "companyID"
            Columns(1).DataField=   "companyID"
            Columns(1).FieldLen=   256
            _ExtentX        =   9657
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "name"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbNewCompany 
            Height          =   315
            Index           =   1
            Left            =   2100
            TabIndex        =   59
            ToolTipText     =   "The company this tape belongs to"
            Top             =   4380
            Width           =   5475
            DataFieldList   =   "name"
            AllowInput      =   0   'False
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   8811
            Columns(0).Caption=   "name"
            Columns(0).Name =   "name"
            Columns(0).DataField=   "name"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3201
            Columns(1).Caption=   "companyID"
            Columns(1).Name =   "companyID"
            Columns(1).DataField=   "companyID"
            Columns(1).FieldLen=   256
            _ExtentX        =   9657
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "name"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbNewCompany 
            Height          =   315
            Index           =   3
            Left            =   2100
            TabIndex        =   60
            ToolTipText     =   "The company this tape belongs to"
            Top             =   8100
            Width           =   5475
            DataFieldList   =   "name"
            AllowInput      =   0   'False
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   8811
            Columns(0).Caption=   "name"
            Columns(0).Name =   "name"
            Columns(0).DataField=   "name"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3201
            Columns(1).Caption=   "companyID"
            Columns(1).Name =   "companyID"
            Columns(1).DataField=   "companyID"
            Columns(1).FieldLen=   256
            _ExtentX        =   9657
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "name"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbNewCompany 
            Height          =   315
            Index           =   2
            Left            =   2100
            TabIndex        =   61
            ToolTipText     =   "The company this tape belongs to"
            Top             =   6240
            Width           =   5475
            DataFieldList   =   "name"
            AllowInput      =   0   'False
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   8811
            Columns(0).Caption=   "name"
            Columns(0).Name =   "name"
            Columns(0).DataField=   "name"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3201
            Columns(1).Caption=   "companyID"
            Columns(1).Name =   "companyID"
            Columns(1).DataField=   "companyID"
            Columns(1).FieldLen=   256
            _ExtentX        =   9657
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "name"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbNewCompany 
            Height          =   315
            Index           =   4
            Left            =   2100
            TabIndex        =   62
            ToolTipText     =   "The company this tape belongs to"
            Top             =   9960
            Width           =   5475
            DataFieldList   =   "name"
            AllowInput      =   0   'False
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   8811
            Columns(0).Caption=   "name"
            Columns(0).Name =   "name"
            Columns(0).DataField=   "name"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3201
            Columns(1).Caption=   "companyID"
            Columns(1).Name =   "companyID"
            Columns(1).DataField=   "companyID"
            Columns(1).FieldLen=   256
            _ExtentX        =   9657
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "name"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbTranscodeSystem 
            Height          =   315
            Left            =   9900
            TabIndex        =   63
            Top             =   540
            Width           =   2175
            DataFieldList   =   "Column 0"
            MaxDropDownItems=   32
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            HeadLines       =   0
            BackColorOdd    =   12632319
            RowHeight       =   423
            Columns(0).Width=   3200
            Columns(0).Caption=   "barcode"
            Columns(0).Name =   "barcode"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   3836
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbDeliveryBarcode 
            Height          =   315
            Index           =   1
            Left            =   10500
            TabIndex        =   168
            Top             =   4020
            Width           =   1575
            DataFieldList   =   "Column 0"
            MaxDropDownItems=   56
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            HeadLines       =   0
            BackColorOdd    =   12632319
            RowHeight       =   423
            Columns(0).Width=   4233
            Columns(0).Caption=   "barcode"
            Columns(0).Name =   "barcode"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2778
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbDeliveryBarcode 
            Height          =   315
            Index           =   2
            Left            =   10500
            TabIndex        =   169
            Top             =   5880
            Width           =   1575
            DataFieldList   =   "Column 0"
            MaxDropDownItems=   56
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            HeadLines       =   0
            BackColorOdd    =   12632319
            RowHeight       =   423
            Columns(0).Width=   4233
            Columns(0).Caption=   "barcode"
            Columns(0).Name =   "barcode"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2778
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbDeliveryBarcode 
            Height          =   315
            Index           =   3
            Left            =   10500
            TabIndex        =   170
            Top             =   7740
            Width           =   1575
            DataFieldList   =   "Column 0"
            MaxDropDownItems=   56
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            HeadLines       =   0
            BackColorOdd    =   12632319
            RowHeight       =   423
            Columns(0).Width=   4233
            Columns(0).Caption=   "barcode"
            Columns(0).Name =   "barcode"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2778
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbDeliveryBarcode 
            Height          =   315
            Index           =   4
            Left            =   10500
            TabIndex        =   171
            Top             =   9600
            Width           =   1575
            DataFieldList   =   "Column 0"
            MaxDropDownItems=   56
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            HeadLines       =   0
            BackColorOdd    =   12632319
            RowHeight       =   423
            Columns(0).Width=   4233
            Columns(0).Caption=   "barcode"
            Columns(0).Name =   "barcode"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2778
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbDeliveryBarcode 
            Height          =   315
            Index           =   5
            Left            =   10500
            TabIndex        =   172
            Top             =   11460
            Width           =   1575
            DataFieldList   =   "Column 0"
            MaxDropDownItems=   56
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            HeadLines       =   0
            BackColorOdd    =   12632319
            RowHeight       =   423
            Columns(0).Width=   4233
            Columns(0).Caption=   "barcode"
            Columns(0).Name =   "barcode"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2778
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo txtHeadbuildEventID 
            Height          =   315
            Left            =   9780
            TabIndex        =   220
            ToolTipText     =   "The company this tape belongs to"
            Top             =   1260
            Width           =   1635
            DataFieldList   =   "eventID"
            AllowInput      =   0   'False
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Caption=   "eventID"
            Columns(0).Name =   "eventID"
            Columns(0).DataField=   "eventID"
            Columns(0).FieldLen=   256
            Columns(1).Width=   8308
            Columns(1).Caption=   "clipfilename"
            Columns(1).Name =   "clipfilename"
            Columns(1).DataField=   "clipfilename"
            Columns(1).FieldLen=   256
            _ExtentX        =   2884
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "eventID"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbATSProfile 
            Height          =   315
            Left            =   2100
            TabIndex        =   314
            ToolTipText     =   "The company this tape belongs to"
            Top             =   1260
            Width           =   3555
            DataFieldList   =   "ATS_Profile_ID"
            AllowInput      =   0   'False
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Caption=   "ATS_Profile_ID"
            Columns(0).Name =   "ATS_Profile_ID"
            Columns(0).DataField=   "ATS_Profile_ID"
            Columns(0).FieldLen=   256
            Columns(1).Width=   8229
            Columns(1).Caption=   "ATS_Name"
            Columns(1).Name =   "ATS_Profile_Name"
            Columns(1).DataField=   "ATS_Profile_Name"
            Columns(1).FieldLen=   256
            _ExtentX        =   6271
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16777215
            DataFieldToDisplay=   "ATS_Profile_ID"
         End
         Begin VB.Label Label8 
            Caption         =   $"frmTranscoding.frx":0F14
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   615
            Left            =   8160
            TabIndex        =   316
            Top             =   1680
            Width           =   7215
         End
         Begin VB.Label lblCaption 
            Caption         =   "ATS Profile ID"
            Height          =   255
            Index           =   20
            Left            =   240
            TabIndex        =   315
            Top             =   1320
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "(leave blank for default FFMpeg version)"
            Height          =   255
            Index           =   8
            Left            =   3780
            TabIndex        =   227
            Top             =   2640
            Width           =   3015
         End
         Begin VB.Label lblCaption 
            Caption         =   "Explicit FFMpeg Version"
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   226
            Top             =   2640
            Width           =   1875
         End
         Begin VB.Line Line1 
            Index           =   4
            X1              =   240
            X2              =   15480
            Y1              =   10320
            Y2              =   10320
         End
         Begin VB.Line Line1 
            Index           =   3
            X1              =   240
            X2              =   15480
            Y1              =   8460
            Y2              =   8460
         End
         Begin VB.Line Line1 
            Index           =   2
            X1              =   240
            X2              =   15480
            Y1              =   6600
            Y2              =   6600
         End
         Begin VB.Line Line1 
            Index           =   1
            X1              =   240
            X2              =   15480
            Y1              =   4740
            Y2              =   4740
         End
         Begin VB.Label lblReplacementWarning 
            Caption         =   "(Delivery or Throwaway not possible if replacing Original)"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   10680
            TabIndex        =   223
            Top             =   2610
            Width           =   4935
         End
         Begin VB.Label lblHeadbuildFilename 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Left            =   13080
            TabIndex        =   219
            Tag             =   "CLEARFIELDS"
            Top             =   1260
            Width           =   1995
         End
         Begin VB.Label lblCaption 
            Caption         =   "(Vantage only)"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   70
            Left            =   11640
            TabIndex        =   217
            Top             =   930
            Width           =   1155
         End
         Begin VB.Label lblCaption 
            Caption         =   "Headbuild ClipID"
            Height          =   255
            Index           =   69
            Left            =   8340
            TabIndex        =   216
            Top             =   1320
            Width           =   1275
         End
         Begin VB.Label lblCaption 
            Caption         =   "Other Specs"
            Height          =   255
            Index           =   68
            Left            =   240
            TabIndex        =   215
            Top             =   1680
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "(FFMPG Generic only)"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   67
            Left            =   11460
            TabIndex        =   213
            Top             =   1320
            Width           =   1575
         End
         Begin VB.Label lblAudioMapping 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   4
            Left            =   7620
            TabIndex        =   203
            Tag             =   "CLEARFIELDS"
            Top             =   8880
            Width           =   4455
         End
         Begin VB.Label lblAudioMapping 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   3
            Left            =   7620
            TabIndex        =   202
            Tag             =   "CLEARFIELDS"
            Top             =   7020
            Width           =   4455
         End
         Begin VB.Label lblAudioMapping 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   5
            Left            =   7620
            TabIndex        =   201
            Tag             =   "CLEARFIELDS"
            Top             =   10740
            Width           =   4455
         End
         Begin VB.Label lblAudioMapping 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   2
            Left            =   7620
            TabIndex        =   200
            Tag             =   "CLEARFIELDS"
            Top             =   5160
            Width           =   4455
         End
         Begin VB.Label lblAudioMapping 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   7620
            TabIndex        =   199
            Tag             =   "CLEARFIELDS"
            Top             =   3300
            Width           =   4455
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.5 Audio Mapping"
            Height          =   255
            Index           =   62
            Left            =   240
            TabIndex        =   198
            Top             =   10800
            Width           =   1935
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.4 Audio Mapping"
            Height          =   255
            Index           =   61
            Left            =   240
            TabIndex        =   197
            Top             =   8940
            Width           =   1935
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.3 Audio Mapping"
            Height          =   255
            Index           =   60
            Left            =   240
            TabIndex        =   196
            Top             =   7080
            Width           =   1935
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.2 Audio Mapping"
            Height          =   255
            Index           =   59
            Left            =   240
            TabIndex        =   195
            Top             =   5220
            Width           =   1935
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.1 Audio Mapping"
            Height          =   255
            Index           =   58
            Left            =   240
            TabIndex        =   194
            Top             =   3360
            Width           =   1935
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.5 Del Library ID"
            Height          =   255
            Index           =   57
            Left            =   12180
            TabIndex        =   187
            Top             =   11520
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.4 Del Library ID"
            Height          =   255
            Index           =   56
            Left            =   12180
            TabIndex        =   186
            Top             =   9660
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.3 Del Library ID"
            Height          =   255
            Index           =   55
            Left            =   12180
            TabIndex        =   185
            Top             =   7800
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.2 Del Library ID"
            Height          =   255
            Index           =   54
            Left            =   12180
            TabIndex        =   184
            Top             =   5940
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.1 Del Library ID"
            Height          =   255
            Index           =   53
            Left            =   12180
            TabIndex        =   183
            Top             =   4080
            Width           =   1695
         End
         Begin VB.Label lblDeliveryLibraryID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   5
            Left            =   13920
            TabIndex        =   182
            Tag             =   "CLEARFIELDS"
            Top             =   11460
            Width           =   1635
         End
         Begin VB.Label lblDeliveryLibraryID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   4
            Left            =   13920
            TabIndex        =   181
            Tag             =   "CLEARFIELDS"
            Top             =   9600
            Width           =   1635
         End
         Begin VB.Label lblDeliveryLibraryID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   3
            Left            =   13920
            TabIndex        =   180
            Tag             =   "CLEARFIELDS"
            Top             =   7740
            Width           =   1635
         End
         Begin VB.Label lblDeliveryLibraryID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   2
            Left            =   13920
            TabIndex        =   179
            Tag             =   "CLEARFIELDS"
            Top             =   5880
            Width           =   1635
         End
         Begin VB.Label lblDeliveryLibraryID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   13920
            TabIndex        =   178
            Tag             =   "CLEARFIELDS"
            Top             =   4020
            Width           =   1635
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.5 Del Barcode"
            Height          =   255
            Index           =   52
            Left            =   8700
            TabIndex        =   177
            Top             =   11520
            Width           =   1575
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.4 Del Barcode"
            Height          =   255
            Index           =   51
            Left            =   8700
            TabIndex        =   176
            Top             =   9660
            Width           =   1575
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.3 Del Barcode"
            Height          =   255
            Index           =   50
            Left            =   8700
            TabIndex        =   175
            Top             =   7800
            Width           =   1575
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.2 Del Barcode"
            Height          =   255
            Index           =   49
            Left            =   8700
            TabIndex        =   174
            Top             =   5940
            Width           =   1575
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.1 Del Barcode"
            Height          =   255
            Index           =   48
            Left            =   8700
            TabIndex        =   173
            Top             =   4080
            Width           =   1575
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest 5 Delivery Folder"
            Height          =   255
            Index           =   47
            Left            =   240
            TabIndex        =   167
            Top             =   11520
            Width           =   1875
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest 4 Delivery Folder"
            Height          =   255
            Index           =   46
            Left            =   240
            TabIndex        =   166
            Top             =   9660
            Width           =   1875
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest 3 Delivery Folder"
            Height          =   255
            Index           =   45
            Left            =   240
            TabIndex        =   165
            Top             =   7800
            Width           =   1875
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest 2 Delivery Folder"
            Height          =   255
            Index           =   44
            Left            =   240
            TabIndex        =   164
            Top             =   5940
            Width           =   1875
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest 1 Delivery Folder"
            Height          =   255
            Index           =   43
            Left            =   240
            TabIndex        =   163
            Top             =   4080
            Width           =   1875
         End
         Begin VB.Label lblCaption 
            Caption         =   "Clip Purpose"
            Height          =   255
            Index           =   42
            Left            =   12180
            TabIndex        =   125
            Top             =   10440
            Width           =   1095
         End
         Begin VB.Label lblCaption 
            Caption         =   "Clip Purpose"
            Height          =   255
            Index           =   41
            Left            =   12180
            TabIndex        =   123
            Top             =   8580
            Width           =   1095
         End
         Begin VB.Label lblCaption 
            Caption         =   "Clip Purpose"
            Height          =   255
            Index           =   40
            Left            =   12180
            TabIndex        =   121
            Top             =   6720
            Width           =   1095
         End
         Begin VB.Label lblCaption 
            Caption         =   "Clip Purpose"
            Height          =   255
            Index           =   27
            Left            =   12180
            TabIndex        =   119
            Top             =   4860
            Width           =   1095
         End
         Begin VB.Label lblCaption 
            Caption         =   "Clip Purpose"
            Height          =   255
            Index           =   26
            Left            =   12180
            TabIndex        =   117
            Top             =   3000
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "Vantage Workflow ID"
            Height          =   255
            Index           =   14
            Left            =   240
            TabIndex        =   113
            Top             =   960
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.5 File Extender"
            Height          =   255
            Index           =   25
            Left            =   8700
            TabIndex        =   112
            Top             =   10440
            Width           =   1935
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.5 Trans LibraryID"
            Height          =   255
            Index           =   24
            Left            =   12180
            TabIndex        =   111
            Top             =   11160
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.5 Trans Barcode"
            Height          =   255
            Index           =   23
            Left            =   8700
            TabIndex        =   110
            Top             =   11160
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest 5 Folder"
            Height          =   255
            Index           =   22
            Left            =   240
            TabIndex        =   109
            Top             =   11160
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.5 MediaSpec"
            Height          =   255
            Index           =   21
            Left            =   240
            TabIndex        =   108
            Top             =   10440
            Width           =   1935
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.4 File Extender"
            Height          =   255
            Index           =   19
            Left            =   8700
            TabIndex        =   107
            Top             =   8580
            Width           =   1935
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.4 Trans LibraryID"
            Height          =   255
            Index           =   18
            Left            =   12180
            TabIndex        =   106
            Top             =   9300
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.4 Trans Barcode"
            Height          =   255
            Index           =   17
            Left            =   8700
            TabIndex        =   105
            Top             =   9300
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest 4 Folder"
            Height          =   255
            Index           =   16
            Left            =   240
            TabIndex        =   104
            Top             =   9300
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.4 MediaSpec"
            Height          =   255
            Index           =   15
            Left            =   240
            TabIndex        =   103
            Top             =   8580
            Width           =   1935
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.3 File Extender"
            Height          =   255
            Index           =   13
            Left            =   8700
            TabIndex        =   102
            Top             =   6720
            Width           =   1935
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.3 Trans LibraryID"
            Height          =   255
            Index           =   12
            Left            =   12180
            TabIndex        =   101
            Top             =   7440
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.3 Trans Barcode"
            Height          =   255
            Index           =   11
            Left            =   8700
            TabIndex        =   100
            Top             =   7440
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest 3 Folder"
            Height          =   255
            Index           =   10
            Left            =   240
            TabIndex        =   99
            Top             =   7440
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.3 MediaSpec"
            Height          =   255
            Index           =   9
            Left            =   240
            TabIndex        =   98
            Top             =   6720
            Width           =   1935
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.2 File Extender"
            Height          =   255
            Index           =   7
            Left            =   8700
            TabIndex        =   97
            Top             =   4860
            Width           =   1575
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.2 Trans LibraryID"
            Height          =   255
            Index           =   6
            Left            =   12180
            TabIndex        =   96
            Top             =   5580
            Width           =   1635
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.2 Trans Barcode"
            Height          =   255
            Index           =   5
            Left            =   8700
            TabIndex        =   95
            Top             =   5580
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest 2 Folder"
            Height          =   255
            Index           =   4
            Left            =   240
            TabIndex        =   94
            Top             =   5580
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.2 MediaSpec"
            Height          =   255
            Index           =   3
            Left            =   240
            TabIndex        =   93
            Top             =   4860
            Width           =   1935
         End
         Begin VB.Label lblDestinationLibraryID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   5
            Left            =   13920
            TabIndex        =   92
            Tag             =   "CLEARFIELDS"
            Top             =   11100
            Width           =   1635
         End
         Begin VB.Label lblMediaSpecID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   5
            Left            =   7620
            TabIndex        =   91
            Tag             =   "CLEARFIELDS"
            Top             =   10380
            Width           =   495
         End
         Begin VB.Label lblDestinationLibraryID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   4
            Left            =   13920
            TabIndex        =   90
            Tag             =   "CLEARFIELDS"
            Top             =   9240
            Width           =   1635
         End
         Begin VB.Label lblMediaSpecID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   4
            Left            =   7620
            TabIndex        =   89
            Tag             =   "CLEARFIELDS"
            Top             =   8520
            Width           =   495
         End
         Begin VB.Label lblDestinationLibraryID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   3
            Left            =   13920
            TabIndex        =   88
            Tag             =   "CLEARFIELDS"
            Top             =   7380
            Width           =   1635
         End
         Begin VB.Label lblMediaSpecID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   3
            Left            =   7620
            TabIndex        =   87
            Tag             =   "CLEARFIELDS"
            Top             =   6660
            Width           =   495
         End
         Begin VB.Label lblDestinationLibraryID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   2
            Left            =   13920
            TabIndex        =   86
            Tag             =   "CLEARFIELDS"
            Top             =   5520
            Width           =   1635
         End
         Begin VB.Label lblMediaSpecID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   2
            Left            =   7620
            TabIndex        =   85
            Tag             =   "CLEARFIELDS"
            Top             =   4800
            Width           =   495
         End
         Begin VB.Label lblMediaSpecID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   7620
            TabIndex        =   84
            Tag             =   "CLEARFIELDS"
            Top             =   2940
            Width           =   495
         End
         Begin VB.Label lblCaption 
            Caption         =   "Transcode Spec Title"
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   83
            Top             =   600
            Width           =   1875
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.1 MediaSpec"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   82
            Top             =   3000
            Width           =   1935
         End
         Begin VB.Label lblSpecID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Left            =   8040
            TabIndex        =   81
            Tag             =   "CLEARFIELDS"
            Top             =   120
            Width           =   795
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest 1 Trans Folder"
            Height          =   255
            Index           =   33
            Left            =   240
            TabIndex        =   80
            Top             =   3720
            Width           =   1875
         End
         Begin VB.Label lblDestinationLibraryID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   13920
            TabIndex        =   79
            Tag             =   "CLEARFIELDS"
            Top             =   3660
            Width           =   1635
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.1 Trans Barcode"
            Height          =   255
            Index           =   34
            Left            =   8700
            TabIndex        =   78
            Top             =   3720
            Width           =   1575
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.1 Trans Library ID"
            Height          =   255
            Index           =   35
            Left            =   12180
            TabIndex        =   77
            Top             =   3720
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest.1 File Extender"
            Height          =   255
            Index           =   36
            Left            =   8700
            TabIndex        =   76
            Top             =   3000
            Width           =   1935
         End
         Begin VB.Label lblCaption 
            Caption         =   "Billing Code"
            Height          =   255
            Index           =   28
            Left            =   3060
            TabIndex        =   75
            Top             =   180
            Width           =   915
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest 5 New Owner"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   29
            Left            =   240
            TabIndex        =   74
            Top             =   11880
            Width           =   1755
         End
         Begin VB.Label lblNewCompanyID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   5
            Left            =   7620
            TabIndex        =   73
            Tag             =   "CLEARFIELDS"
            Top             =   11820
            Width           =   495
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest 4 New Owner"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   30
            Left            =   240
            TabIndex        =   72
            Top             =   10020
            Width           =   1755
         End
         Begin VB.Label lblNewCompanyID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   7620
            TabIndex        =   71
            Tag             =   "CLEARFIELDS"
            Top             =   4380
            Width           =   495
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest 3 New Owner"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   31
            Left            =   240
            TabIndex        =   70
            Top             =   8160
            Width           =   1755
         End
         Begin VB.Label lblNewCompanyID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   3
            Left            =   7620
            TabIndex        =   69
            Tag             =   "CLEARFIELDS"
            Top             =   8100
            Width           =   495
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest 2 New Owner"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   37
            Left            =   240
            TabIndex        =   68
            Top             =   6300
            Width           =   1755
         End
         Begin VB.Label lblNewCompanyID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   2
            Left            =   7620
            TabIndex        =   67
            Tag             =   "CLEARFIELDS"
            Top             =   6240
            Width           =   495
         End
         Begin VB.Label lblCaption 
            Caption         =   "Dest 1 New Owner"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   38
            Left            =   240
            TabIndex        =   66
            Top             =   4440
            Width           =   1755
         End
         Begin VB.Label lblNewCompanyID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   4
            Left            =   7620
            TabIndex        =   65
            Tag             =   "CLEARFIELDS"
            Top             =   9960
            Width           =   495
         End
         Begin VB.Label lblCaption 
            Caption         =   "System"
            Height          =   255
            Index           =   39
            Left            =   9180
            TabIndex        =   64
            Top             =   600
            Width           =   615
         End
         Begin VB.Line Line1 
            Index           =   0
            X1              =   240
            X2              =   15480
            Y1              =   2400
            Y2              =   2400
         End
      End
      Begin VB.CheckBox chkFilterComplete 
         Caption         =   "Complete"
         Height          =   195
         Left            =   7020
         TabIndex        =   13
         Top             =   480
         Value           =   1  'Checked
         Width           =   1935
      End
      Begin VB.CheckBox chkFilterInProgress 
         Caption         =   "Processing"
         Height          =   195
         Left            =   3240
         TabIndex        =   12
         Top             =   480
         Value           =   1  'Checked
         Width           =   1935
      End
      Begin VB.CheckBox chkFilterCancelled 
         Caption         =   "Cancelled"
         Height          =   195
         Left            =   9060
         TabIndex        =   11
         Top             =   480
         Value           =   1  'Checked
         Width           =   1935
      End
      Begin VB.CheckBox chkFilterFailed 
         Caption         =   "Failed"
         Height          =   195
         Left            =   11040
         TabIndex        =   10
         Top             =   480
         Value           =   1  'Checked
         Width           =   1455
      End
      Begin VB.CheckBox chkFilterRequested 
         Caption         =   "Requested"
         Height          =   195
         Left            =   1140
         TabIndex        =   9
         Top             =   480
         Value           =   1  'Checked
         Width           =   1935
      End
      Begin MSAdodcLib.Adodc adoTranscodeRequests 
         Height          =   330
         Left            =   120
         Top             =   900
         Width           =   2745
         _ExtentX        =   4842
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoTranscodeRequests"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdTranscodeSpecs 
         Bindings        =   "frmTranscoding.frx":0FF0
         Height          =   14115
         Left            =   -74880
         TabIndex        =   114
         Top             =   1200
         Width           =   10380
         _Version        =   196617
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowColumnSizing=   0   'False
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "transcodespecID"
         Columns(0).Name =   "transcodespecID"
         Columns(0).DataField=   "transcodespecID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   10583
         Columns(1).Caption=   "Transcode Specification Name"
         Columns(1).Name =   "transcodename"
         Columns(1).DataField=   "transcodename"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "forder"
         Columns(2).Name =   "forder"
         Columns(2).DataField=   "forder"
         Columns(2).FieldLen=   256
         Columns(3).Width=   5292
         Columns(3).Caption=   "System"
         Columns(3).Name =   "transcodesystem"
         Columns(3).DataField=   "transcodesystem"
         Columns(3).FieldLen=   256
         Columns(4).Width=   1323
         Columns(4).Caption=   "Enabled"
         Columns(4).Name =   "enabled"
         Columns(4).DataField=   "enabled"
         Columns(4).FieldLen=   256
         Columns(4).Style=   2
         _ExtentX        =   18309
         _ExtentY        =   24897
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSAdodcLib.Adodc adoFileRequests 
         Height          =   330
         Left            =   -74880
         Top             =   940
         Width           =   4485
         _ExtentX        =   7911
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoFileRequests"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdTranscodeRequests 
         Bindings        =   "frmTranscoding.frx":1010
         Height          =   12255
         Left            =   120
         TabIndex        =   16
         Top             =   900
         Width           =   28245
         _Version        =   196617
         AllowDelete     =   -1  'True
         AllowRowSizing  =   0   'False
         SelectTypeRow   =   3
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   25
         Columns(0).Width=   1296
         Columns(0).Caption=   "Req No."
         Columns(0).Name =   "transcoderequestID"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "transcoderequestID"
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "status"
         Columns(1).Name =   "status"
         Columns(1).DataField=   "status"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "transcodespecID"
         Columns(2).Name =   "transcodespecID"
         Columns(2).DataField=   "transcodespecID"
         Columns(2).FieldLen=   256
         Columns(3).Width=   820
         Columns(3).Caption=   "Hide"
         Columns(3).Name =   "hidefromgrid"
         Columns(3).DataField=   "hidefromgrid"
         Columns(3).FieldLen=   256
         Columns(3).Style=   2
         Columns(4).Width=   3016
         Columns(4).Caption=   "Date Submitted"
         Columns(4).Name =   "Date Submitted"
         Columns(4).DataField=   "savedate"
         Columns(4).DataType=   7
         Columns(4).FieldLen=   256
         Columns(5).Width=   3016
         Columns(5).Caption=   "Date Started"
         Columns(5).Name =   "transcodestarted"
         Columns(5).DataField=   "transcodestarted"
         Columns(5).FieldLen=   256
         Columns(6).Width=   3016
         Columns(6).Caption=   "Date Completed"
         Columns(6).Name =   "transcodecompleted"
         Columns(6).DataField=   "transcodecompleted"
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(7).Width=   5292
         Columns(7).Caption=   "Transcode Specification"
         Columns(7).Name =   "transcodespecname"
         Columns(7).DataField=   "transcodename"
         Columns(7).FieldLen=   256
         Columns(8).Width=   2461
         Columns(8).Caption=   "System"
         Columns(8).Name =   "transcodesystem"
         Columns(8).DataField=   "transcodesystem"
         Columns(8).FieldLen=   256
         Columns(9).Width=   1773
         Columns(9).Caption=   "Owner"
         Columns(9).Name =   "Owner"
         Columns(9).FieldLen=   256
         Columns(9).Locked=   -1  'True
         Columns(10).Width=   1693
         Columns(10).Caption=   "Source Clip"
         Columns(10).Name=   "sourceclipID"
         Columns(10).Alignment=   2
         Columns(10).DataField=   "sourceclipID"
         Columns(10).FieldLen=   256
         Columns(11).Width=   4233
         Columns(11).Caption=   "Source Filename"
         Columns(11).Name=   "filename"
         Columns(11).DataField=   "clipfilename"
         Columns(11).FieldLen=   256
         Columns(11).Locked=   -1  'True
         Columns(12).Width=   6006
         Columns(12).Caption=   "Transcode Reference"
         Columns(12).Name=   "segmentreference"
         Columns(12).DataField=   "segmentreference"
         Columns(12).FieldLen=   256
         Columns(12).Locked=   -1  'True
         Columns(13).Width=   873
         Columns(13).Caption=   "Seg"
         Columns(13).Name=   "segmentneeded"
         Columns(13).Alignment=   2
         Columns(13).DataField=   "segmentneeded"
         Columns(13).FieldLen=   256
         Columns(13).Locked=   -1  'True
         Columns(13).Style=   2
         Columns(14).Width=   2117
         Columns(14).Caption=   "TimecodeStart"
         Columns(14).Name=   "timecodestart"
         Columns(14).DataField=   "timecodestart"
         Columns(14).FieldLen=   256
         Columns(15).Width=   2117
         Columns(15).Caption=   "TimecodeStop"
         Columns(15).Name=   "timecodestop"
         Columns(15).DataField=   "timecodestop"
         Columns(15).FieldLen=   256
         Columns(16).Width=   2117
         Columns(16).Caption=   "TC Override"
         Columns(16).Name=   "StartTimecodeOverride"
         Columns(16).DataField=   "StartTimecodeOverride"
         Columns(16).FieldLen=   256
         Columns(17).Width=   1111
         Columns(17).Caption=   "O'write"
         Columns(17).Name=   "OverwriteIfExisting"
         Columns(17).DataField=   "OverwriteIfExisting"
         Columns(17).FieldLen=   256
         Columns(17).Style=   2
         Columns(18).Width=   3254
         Columns(18).Caption=   "Transcode Engine"
         Columns(18).Name=   "Transcode Engine"
         Columns(18).FieldLen=   256
         Columns(18).Locked=   -1  'True
         Columns(19).Width=   4577
         Columns(19).Caption=   "Status"
         Columns(19).Name=   "statustext"
         Columns(19).DataField=   "description"
         Columns(19).FieldLen=   256
         Columns(20).Width=   3200
         Columns(20).Visible=   0   'False
         Columns(20).Caption=   "Transcode_Engine"
         Columns(20).Name=   "Transcode_Engine"
         Columns(20).DataField=   "Transcode_Engine"
         Columns(20).FieldLen=   256
         Columns(21).Width=   3200
         Columns(21).Visible=   0   'False
         Columns(21).Caption=   "Transcode_Stream"
         Columns(21).Name=   "Transcode_Stream"
         Columns(21).DataField=   "Transcode_Stream"
         Columns(21).FieldLen=   256
         Columns(22).Width=   3200
         Columns(22).Visible=   0   'False
         Columns(22).Caption=   "fps"
         Columns(22).Name=   "fps"
         Columns(22).DataField=   "fps"
         Columns(22).FieldLen=   256
         Columns(23).Width=   3200
         Columns(23).Visible=   0   'False
         Columns(23).Caption=   "name"
         Columns(23).Name=   "name"
         Columns(23).DataField=   "name"
         Columns(23).FieldLen=   256
         Columns(24).Width=   3200
         Columns(24).Visible=   0   'False
         Columns(24).Caption=   "cetasuer"
         Columns(24).Name=   "cetauser"
         Columns(24).DataField=   "cetauser"
         Columns(24).FieldLen=   256
         _ExtentX        =   49821
         _ExtentY        =   21616
         _StockProps     =   79
         Caption         =   "Transcode Requests"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdFileRequests 
         Bindings        =   "frmTranscoding.frx":1033
         Height          =   13335
         Left            =   -74880
         TabIndex        =   115
         Top             =   960
         Width           =   27750
         _Version        =   196617
         AllowDelete     =   -1  'True
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeRow   =   3
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   29
         Columns(0).Width=   767
         Columns(0).Caption=   "Hide"
         Columns(0).Name =   "Hide"
         Columns(0).DataField=   "Column 27"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   1323
         Columns(1).Caption=   "Req ID"
         Columns(1).Name =   "RequestID"
         Columns(1).DataField=   "event_File_RequestID"
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "HideFromGrid"
         Columns(2).Name =   "HideFromGrid"
         Columns(2).DataField=   "HideFromGrid"
         Columns(2).FieldLen=   256
         Columns(3).Width=   1588
         Columns(3).Caption=   "Clip ID"
         Columns(3).Name =   "Source File"
         Columns(3).DataField=   "Source File"
         Columns(3).FieldLen=   256
         Columns(4).Width=   2302
         Columns(4).Caption=   "Originally On"
         Columns(4).Name =   "Originally On"
         Columns(4).DataField=   "Originally On"
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   2302
         Columns(5).Caption=   "Now Stored on"
         Columns(5).Name =   "Store"
         Columns(5).DataField=   "Source Barcode"
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   4233
         Columns(6).Caption=   "Source Filename"
         Columns(6).Name =   "Source Filename"
         Columns(6).DataField=   "Source Filename"
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(7).Width=   5477
         Columns(7).Caption=   "ClipReference"
         Columns(7).Name =   "clipreference"
         Columns(7).DataField=   "clipreference"
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(8).Width=   2302
         Columns(8).Caption=   "bigfilesize"
         Columns(8).Name =   "bigfilesize"
         Columns(8).DataField=   "bigfilesize"
         Columns(8).DataType=   17
         Columns(8).FieldLen=   256
         Columns(8).Locked=   -1  'True
         Columns(9).Width=   2831
         Columns(9).Caption=   "Operation"
         Columns(9).Name =   "File Operation"
         Columns(9).DataField=   "File Operation"
         Columns(9).FieldLen=   256
         Columns(9).Locked=   -1  'True
         Columns(10).Width=   1323
         Columns(10).Caption=   "JobID"
         Columns(10).Name=   "JobID"
         Columns(10).DataField=   "JobID"
         Columns(10).FieldLen=   256
         Columns(10).Locked=   -1  'True
         Columns(11).Width=   1323
         Columns(11).Caption=   "Del #"
         Columns(11).Name=   "CDDeliveryNumber"
         Columns(11).DataField=   "CDDeliveryNumber"
         Columns(11).FieldLen=   256
         Columns(12).Width=   979
         Columns(12).Caption=   "Notify"
         Columns(12).Name=   "NotifyRecipient"
         Columns(12).DataField=   "NotifyRecipient"
         Columns(12).FieldLen=   256
         Columns(12).Locked=   -1  'True
         Columns(13).Width=   2831
         Columns(13).Caption=   "Intended New Store"
         Columns(13).Name=   "New Store"
         Columns(13).DataField=   "New Store"
         Columns(13).FieldLen=   256
         Columns(13).Locked=   -1  'True
         Columns(14).Width=   3175
         Columns(14).Caption=   "New Folder"
         Columns(14).Name=   "New Folder"
         Columns(14).DataField=   "New Folder"
         Columns(14).FieldLen=   256
         Columns(15).Width=   2646
         Columns(15).Caption=   "New Filename"
         Columns(15).Name=   "NewFileName"
         Columns(15).DataField=   "NewFileName"
         Columns(15).FieldLen=   256
         Columns(16).Width=   1032
         Columns(16).Caption=   "DNRC"
         Columns(16).Name=   "DoNotRecordCopy"
         Columns(16).Alignment=   2
         Columns(16).DataField=   "DoNotRecordCopy"
         Columns(16).FieldLen=   256
         Columns(16).Locked=   -1  'True
         Columns(17).Width=   2990
         Columns(17).Caption=   "Date"
         Columns(17).Name=   "Request Date"
         Columns(17).DataField=   "Request Date"
         Columns(17).DataType=   7
         Columns(17).FieldLen=   256
         Columns(18).Width=   1058
         Columns(18).Caption=   "Urgent"
         Columns(18).Name=   "Urgent"
         Columns(18).DataField=   "Urgent"
         Columns(18).FieldLen=   256
         Columns(18).Style=   2
         Columns(19).Width=   1164
         Columns(19).Caption=   "Paused"
         Columns(19).Name=   "Paused"
         Columns(19).FieldLen=   256
         Columns(19).Style=   2
         Columns(20).Width=   3200
         Columns(20).Visible=   0   'False
         Columns(20).Caption=   "RequestPaused"
         Columns(20).Name=   "RequestPaused"
         Columns(20).DataField=   "RequestPaused"
         Columns(20).FieldLen=   256
         Columns(21).Width=   2302
         Columns(21).Caption=   "By"
         Columns(21).Name=   "Requestname"
         Columns(21).DataField=   "Requestname"
         Columns(21).DataType=   10
         Columns(21).FieldLen=   256
         Columns(21).Locked=   -1  'True
         Columns(22).Width=   2990
         Columns(22).Caption=   "Started"
         Columns(22).Name=   "Request Started"
         Columns(22).DataField=   "Request Started"
         Columns(22).DataType=   7
         Columns(22).FieldLen=   256
         Columns(23).Width=   3969
         Columns(23).Caption=   "Handler"
         Columns(23).Name=   "Handler"
         Columns(23).DataField=   "Handler"
         Columns(23).FieldLen=   256
         Columns(24).Width=   2990
         Columns(24).Caption=   "Completed"
         Columns(24).Name=   "Request Completed"
         Columns(24).DataField=   "Request Completed"
         Columns(24).DataType=   7
         Columns(24).FieldLen=   256
         Columns(25).Width=   2990
         Columns(25).Caption=   "Failed"
         Columns(25).Name=   "Request Failed"
         Columns(25).DataField=   "Request Failed"
         Columns(25).DataType=   7
         Columns(25).FieldLen=   256
         Columns(26).Width=   5900
         Columns(26).Caption=   "Comment"
         Columns(26).Name=   "Comment"
         Columns(26).DataField=   "Comment"
         Columns(26).FieldLen=   256
         Columns(27).Width=   3200
         Columns(27).Visible=   0   'False
         Columns(27).Caption=   "Cleaned Up"
         Columns(27).Name=   "RequestCleanedUp"
         Columns(27).DataField=   "RequestCleanedUp"
         Columns(27).FieldLen=   256
         Columns(28).Width=   3200
         Columns(28).Visible=   0   'False
         Columns(28).Caption=   "NewFileID"
         Columns(28).Name=   "NewFileID"
         Columns(28).DataField=   "NewFileID"
         Columns(28).FieldLen=   256
         _ExtentX        =   48948
         _ExtentY        =   23521
         _StockProps     =   79
         Caption         =   "File Requests"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbOperationType 
         Height          =   315
         Left            =   -57120
         TabIndex        =   141
         Top             =   480
         Width           =   3735
         DataFieldList   =   "Column 0"
         MaxDropDownItems=   56
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         HeadLines       =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   4233
         Columns(0).Caption=   "barcode"
         Columns(0).Name =   "OperationType"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   6588
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 0"
      End
      Begin MSComCtl2.DTPicker datSearchDate 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Left            =   -60600
         TabIndex        =   146
         Tag             =   "NOCHECK"
         ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
         Top             =   420
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   111214593
         CurrentDate     =   39580
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdWorkflows 
         Bindings        =   "frmTranscoding.frx":1051
         Height          =   6975
         Left            =   -74880
         TabIndex        =   229
         Top             =   960
         Width           =   27735
         _Version        =   196617
         AllowDelete     =   -1  'True
         AllowUpdate     =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeRow   =   1
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   17
         Columns(0).Width=   1693
         Columns(0).Caption=   "instanceID"
         Columns(0).Name =   "instanceID"
         Columns(0).DataField=   "instanceID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   2196
         Columns(1).Caption=   "Tracker_itemID"
         Columns(1).Name =   "Tracker_itemID"
         Columns(1).DataField=   "Tracker_itemID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3519
         Columns(2).Caption=   "ItemReference"
         Columns(2).Name =   "ItemReference"
         Columns(2).DataField=   "ItemReference"
         Columns(2).FieldLen=   256
         Columns(3).Width=   1773
         Columns(3).Caption=   "WorkflowID"
         Columns(3).Name =   "WorkflowID"
         Columns(3).DataField=   "WorkflowID"
         Columns(3).FieldLen=   256
         Columns(4).Width=   7064
         Columns(4).Caption=   "Workflow"
         Columns(4).Name =   "Workflow"
         Columns(4).DataField=   "Workflow"
         Columns(4).FieldLen=   256
         Columns(5).Width=   1244
         Columns(5).Caption=   "Enabled"
         Columns(5).Name =   "Enabled"
         Columns(5).DataField=   "Column 4"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Caption=   "WorkflowStart"
         Columns(6).Name =   "WorkflowStart"
         Columns(6).DataField=   "WorkflowStart"
         Columns(6).FieldLen=   256
         Columns(7).Width=   3519
         Columns(7).Caption=   "CurrentStage"
         Columns(7).Name =   "CurrentStage"
         Columns(7).DataField=   "CurrentStage"
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Caption=   "CurrentTrackerStatus"
         Columns(8).Name =   "CurrentTrackerStatus"
         Columns(8).DataField=   "CurrentTrackerStatus"
         Columns(8).FieldLen=   256
         Columns(9).Width=   1826
         Columns(9).Caption=   "StageFieldID"
         Columns(9).Name =   "StageFieldID"
         Columns(9).DataField=   "StageFieldID"
         Columns(9).FieldLen=   256
         Columns(10).Width=   2381
         Columns(10).Caption=   "StageFieldStatus"
         Columns(10).Name=   "StageFieldStatus"
         Columns(10).DataField=   "StageFieldStatus"
         Columns(10).FieldLen=   256
         Columns(11).Width=   3200
         Columns(11).Caption=   "LastUpdate"
         Columns(11).Name=   "LastUpdate"
         Columns(11).DataField=   "LastUpdate"
         Columns(11).FieldLen=   256
         Columns(12).Width=   2646
         Columns(12).Caption=   "CurrentTaskType"
         Columns(12).Name=   "CurrentTaskType"
         Columns(12).DataField=   "CurrentTaskType"
         Columns(12).FieldLen=   256
         Columns(13).Width=   2699
         Columns(13).Caption=   "CurrentTaskTypeID"
         Columns(13).Name=   "CurrentTaskTypeID"
         Columns(13).DataField=   "CurrentTaskTypeID"
         Columns(13).FieldLen=   256
         Columns(14).Width=   1588
         Columns(14).Caption=   "RequestID"
         Columns(14).Name=   "RequestID"
         Columns(14).DataField=   "RequestID"
         Columns(14).FieldLen=   256
         Columns(15).Width=   2117
         Columns(15).Caption=   "RequestState"
         Columns(15).Name=   "RequestState"
         Columns(15).DataField=   "RequestState"
         Columns(15).FieldLen=   256
         Columns(16).Width=   4948
         Columns(16).Caption=   "Comments"
         Columns(16).Name=   "Comments"
         Columns(16).DataField=   "Comments"
         Columns(16).FieldLen=   256
         _ExtentX        =   48921
         _ExtentY        =   12303
         _StockProps     =   79
         Caption         =   "Workflow Monitoring"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdExcelRequests 
         Bindings        =   "frmTranscoding.frx":106C
         Height          =   14235
         Left            =   -74880
         TabIndex        =   143
         Top             =   960
         Width           =   27735
         _Version        =   196617
         AllowUpdate     =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeRow   =   3
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   19
         Columns(0).Width=   1323
         Columns(0).Caption=   "Req ID"
         Columns(0).Name =   "Excel_Techrev_Output_RequestID"
         Columns(0).DataField=   "Excel_Techrev_Output_RequestID"
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "RequestPaused"
         Columns(1).Name =   "RequestPaused"
         Columns(1).DataField=   "RequestPaused"
         Columns(1).FieldLen=   256
         Columns(2).Width=   1773
         Columns(2).Caption=   "Type"
         Columns(2).Name =   "Request_Type"
         Columns(2).DataField=   "Request_Type"
         Columns(2).FieldLen=   256
         Columns(3).Width=   1508
         Columns(3).Caption=   "TechrevID"
         Columns(3).Name =   "TechrevID"
         Columns(3).DataField=   "TechrevID"
         Columns(3).FieldLen=   256
         Columns(4).Width=   1323
         Columns(4).Caption=   "ClipID"
         Columns(4).Name =   "EventID"
         Columns(4).DataField=   "EventID"
         Columns(4).FieldLen=   256
         Columns(5).Width=   11456
         Columns(5).Caption=   "Excel to Save"
         Columns(5).Name =   "NewFilePath"
         Columns(5).DataField=   "NewFilePath"
         Columns(5).FieldLen=   256
         Columns(6).Width=   2990
         Columns(6).Caption=   "Request Date"
         Columns(6).Name =   "RequestDate"
         Columns(6).DataField=   "RequestDate"
         Columns(6).DataType=   7
         Columns(6).FieldLen=   256
         Columns(7).Width=   2990
         Columns(7).Caption=   "Request Complete"
         Columns(7).Name =   "RequestComplete"
         Columns(7).DataField=   "RequestComplete"
         Columns(7).DataType=   7
         Columns(7).FieldLen=   256
         Columns(8).Width=   2990
         Columns(8).Caption=   "Request Failed"
         Columns(8).Name =   "RequestFailed"
         Columns(8).DataField=   "RequestFailed"
         Columns(8).DataType=   7
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Caption=   "Request Name"
         Columns(9).Name =   "RequestName"
         Columns(9).DataField=   "RequestName"
         Columns(9).FieldLen=   256
         Columns(10).Width=   3200
         Columns(10).Caption=   "Request Email"
         Columns(10).Name=   "RequestEmail"
         Columns(10).DataField=   "RequestEmail"
         Columns(10).FieldLen=   256
         Columns(11).Width=   8811
         Columns(11).Caption=   "Message from Handler"
         Columns(11).Name=   "Error_Message"
         Columns(11).DataField=   "Error_Message"
         Columns(11).FieldLen=   256
         Columns(12).Width=   7938
         Columns(12).Caption=   "filename"
         Columns(12).Name=   "filename"
         Columns(12).DataField=   "filename"
         Columns(12).FieldLen=   256
         Columns(13).Width=   2117
         Columns(13).Caption=   "filesize"
         Columns(13).Name=   "bigfilesize"
         Columns(13).DataField=   "bigfilesize"
         Columns(13).FieldLen=   256
         Columns(14).Width=   5292
         Columns(14).Caption=   "md5"
         Columns(14).Name=   "md5checksum"
         Columns(14).DataField=   "md5checksum"
         Columns(14).FieldLen=   256
         Columns(15).Width=   3200
         Columns(15).Visible=   0   'False
         Columns(15).Caption=   "companyID"
         Columns(15).Name=   "companyID"
         Columns(15).DataField=   "companyID"
         Columns(15).FieldLen=   256
         Columns(16).Width=   3200
         Columns(16).Caption=   "company name"
         Columns(16).Name=   "companyname"
         Columns(16).DataField=   "companyname"
         Columns(16).FieldLen=   256
         Columns(17).Width=   2117
         Columns(17).Caption=   "review date"
         Columns(17).Name=   "reviewdate"
         Columns(17).DataField=   "reviewdate"
         Columns(17).DataType=   7
         Columns(17).FieldLen=   256
         Columns(18).Width=   3200
         Columns(18).Caption=   "review user"
         Columns(18).Name=   "reviewuser"
         Columns(18).DataField=   "reviewuser"
         Columns(18).FieldLen=   256
         _ExtentX        =   48921
         _ExtentY        =   25109
         _StockProps     =   79
         Caption         =   "File Requests"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdInstanceVariables 
         Bindings        =   "frmTranscoding.frx":108B
         Height          =   7095
         Left            =   -61500
         TabIndex        =   243
         Top             =   8220
         Width           =   11235
         _Version        =   196617
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeRow   =   3
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   5
         Columns(0).Width=   1138
         Columns(0).Caption=   "Type"
         Columns(0).Name =   "Type"
         Columns(0).DataField=   "Type"
         Columns(0).FieldLen=   256
         Columns(1).Width=   1138
         Columns(1).Caption=   "TaskID"
         Columns(1).Name =   "TaskID"
         Columns(1).DataField=   "TaskID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3228
         Columns(2).Caption=   "Name"
         Columns(2).Name =   "Name"
         Columns(2).DataField=   "Name"
         Columns(2).FieldLen=   256
         Columns(3).Width=   6006
         Columns(3).Caption=   "Value"
         Columns(3).Name =   "Value"
         Columns(3).DataField=   "Value"
         Columns(3).FieldLen=   256
         Columns(4).Width=   6006
         Columns(4).Caption=   "RawValue"
         Columns(4).Name =   "RawValue"
         Columns(4).DataField=   "RawValue"
         Columns(4).FieldLen=   256
         _ExtentX        =   19817
         _ExtentY        =   12515
         _StockProps     =   79
         Caption         =   "Instance Variables"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdWorkflowTaskType 
         Bindings        =   "frmTranscoding.frx":10AE
         Height          =   4395
         Left            =   -69000
         TabIndex        =   264
         Top             =   540
         Width           =   5475
         _Version        =   196617
         AllowUpdate     =   0   'False
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   2
         Columns(0).Width=   1905
         Columns(0).Caption=   "TaskTypeID"
         Columns(0).Name =   "TaskTypeID"
         Columns(0).DataField=   "TaskTypeID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   6641
         Columns(1).Caption=   "TaskType"
         Columns(1).Name =   "TaskType"
         Columns(1).DataField=   "TaskType"
         Columns(1).FieldLen=   256
         _ExtentX        =   9657
         _ExtentY        =   7752
         _StockProps     =   79
         Caption         =   "Workflow Task Types"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdMissingVariables 
         Bindings        =   "frmTranscoding.frx":10D4
         Height          =   2835
         Left            =   -59940
         TabIndex        =   269
         Top             =   12360
         Width           =   11565
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   8
         Columns(0).Width=   3440
         Columns(0).Caption=   "VariantName"
         Columns(0).Name =   "VariantName"
         Columns(0).DataField=   "VariantName"
         Columns(0).FieldLen=   256
         Columns(1).Width=   1667
         Columns(1).Caption=   "WorkflowID"
         Columns(1).Name =   "WorkflowID"
         Columns(1).DataField=   "WorkflowID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   1402
         Columns(2).Caption=   "VariantID"
         Columns(2).Name =   "VariantID"
         Columns(2).DataField=   "VariantID"
         Columns(2).FieldLen=   256
         Columns(3).Width=   1164
         Columns(3).Caption=   "TaskID"
         Columns(3).Name =   "TaskID"
         Columns(3).DataField=   "TaskID"
         Columns(3).FieldLen=   256
         Columns(4).Width=   3413
         Columns(4).Caption=   "StageName"
         Columns(4).Name =   "StageName"
         Columns(4).DataField=   "StageName"
         Columns(4).FieldLen=   256
         Columns(5).Width=   1720
         Columns(5).Caption=   "TaskTypeID"
         Columns(5).Name =   "TaskTypeID"
         Columns(5).DataField=   "TaskTypeID"
         Columns(5).FieldLen=   256
         Columns(6).Width=   3254
         Columns(6).Caption=   "DefaultVariable"
         Columns(6).Name =   "DefaultVariable"
         Columns(6).DataField=   "DefaultVariable"
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Caption=   "DefaultValue"
         Columns(7).Name =   "DefaultValue"
         Columns(7).DataField=   "DefaultValue"
         Columns(7).FieldLen=   256
         _ExtentX        =   20399
         _ExtentY        =   5001
         _StockProps     =   79
         Caption         =   "WorkflowMissing Variables"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSAdodcLib.Adodc adoStatus 
         Height          =   330
         Left            =   -74880
         Top             =   50
         Visible         =   0   'False
         Width           =   2700
         _ExtentX        =   4763
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoStatus"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoNextTask 
         Height          =   330
         Left            =   -74880
         Top             =   15780
         Visible         =   0   'False
         Width           =   2700
         _ExtentX        =   4763
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoNextTask"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoCompany 
         Height          =   330
         Left            =   -72180
         Top             =   15780
         Visible         =   0   'False
         Width           =   2700
         _ExtentX        =   4763
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoCompany"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoWorkflowVariants 
         Height          =   330
         Index           =   0
         Left            =   -72180
         Top             =   15420
         Visible         =   0   'False
         Width           =   2700
         _ExtentX        =   4763
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoWorkflowVariants"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoWorkflowTaskDefaultVariables 
         Height          =   330
         Index           =   0
         Left            =   -69420
         Top             =   15780
         Visible         =   0   'False
         Width           =   3120
         _ExtentX        =   5503
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoWorkflowTaskDefaultVariables"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoWorkflowTaskVariables 
         Height          =   330
         Left            =   -66300
         Top             =   15780
         Visible         =   0   'False
         Width           =   3480
         _ExtentX        =   6138
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoWorkflowTaskVariables"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoMissingVariables 
         Height          =   330
         Left            =   -62760
         Top             =   15780
         Visible         =   0   'False
         Width           =   3480
         _ExtentX        =   6138
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoWorkflowTaskVariables"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdTranscodeRequestsHybrid 
         Bindings        =   "frmTranscoding.frx":10F6
         Height          =   12255
         Left            =   -74880
         TabIndex        =   334
         Top             =   540
         Width           =   12045
         _Version        =   196617
         AllowDelete     =   -1  'True
         AllowRowSizing  =   0   'False
         SelectTypeRow   =   3
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   25
         Columns(0).Width=   1296
         Columns(0).Caption=   "Req No."
         Columns(0).Name =   "transcoderequestID"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "transcoderequestID"
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "status"
         Columns(1).Name =   "status"
         Columns(1).DataField=   "status"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "transcodespecID"
         Columns(2).Name =   "transcodespecID"
         Columns(2).DataField=   "transcodespecID"
         Columns(2).FieldLen=   256
         Columns(3).Width=   820
         Columns(3).Caption=   "Hide"
         Columns(3).Name =   "hidefromgrid"
         Columns(3).DataField=   "hidefromgrid"
         Columns(3).FieldLen=   256
         Columns(3).Style=   2
         Columns(4).Width=   3016
         Columns(4).Caption=   "Date Submitted"
         Columns(4).Name =   "Date Submitted"
         Columns(4).DataField=   "savedate"
         Columns(4).DataType=   7
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "Date Started"
         Columns(5).Name =   "transcodestarted"
         Columns(5).DataField=   "transcodestarted"
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "Date Completed"
         Columns(6).Name =   "transcodecompleted"
         Columns(6).DataField=   "transcodecompleted"
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(7).Width=   5292
         Columns(7).Caption=   "Transcode Specification"
         Columns(7).Name =   "transcodespecname"
         Columns(7).DataField=   "transcodename"
         Columns(7).FieldLen=   256
         Columns(8).Width=   2461
         Columns(8).Caption=   "System"
         Columns(8).Name =   "transcodesystem"
         Columns(8).DataField=   "transcodesystem"
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "Owner"
         Columns(9).Name =   "Owner"
         Columns(9).FieldLen=   256
         Columns(9).Locked=   -1  'True
         Columns(10).Width=   1693
         Columns(10).Caption=   "Source Clip"
         Columns(10).Name=   "sourceclipID"
         Columns(10).Alignment=   2
         Columns(10).DataField=   "sourceclipID"
         Columns(10).FieldLen=   256
         Columns(11).Width=   3200
         Columns(11).Visible=   0   'False
         Columns(11).Caption=   "Source Filename"
         Columns(11).Name=   "filename"
         Columns(11).DataField=   "clipfilename"
         Columns(11).FieldLen=   256
         Columns(11).Locked=   -1  'True
         Columns(12).Width=   6350
         Columns(12).Caption=   "Transcode Reference"
         Columns(12).Name=   "segmentreference"
         Columns(12).DataField=   "segmentreference"
         Columns(12).FieldLen=   256
         Columns(12).Locked=   -1  'True
         Columns(13).Width=   1005
         Columns(13).Caption=   "Seg"
         Columns(13).Name=   "segmentneeded"
         Columns(13).Alignment=   2
         Columns(13).DataField=   "segmentneeded"
         Columns(13).FieldLen=   256
         Columns(13).Locked=   -1  'True
         Columns(13).Style=   2
         Columns(14).Width=   3200
         Columns(14).Visible=   0   'False
         Columns(14).Caption=   "TimecodeStart"
         Columns(14).Name=   "timecodestart"
         Columns(14).DataField=   "timecodestart"
         Columns(14).FieldLen=   256
         Columns(15).Width=   3200
         Columns(15).Visible=   0   'False
         Columns(15).Caption=   "TimecodeStop"
         Columns(15).Name=   "timecodestop"
         Columns(15).DataField=   "timecodestop"
         Columns(15).FieldLen=   256
         Columns(16).Width=   3200
         Columns(16).Visible=   0   'False
         Columns(16).Caption=   "TC Override"
         Columns(16).Name=   "StartTimecodeOverride"
         Columns(16).DataField=   "StartTimecodeOverride"
         Columns(16).FieldLen=   256
         Columns(17).Width=   1111
         Columns(17).Caption=   "O'write"
         Columns(17).Name=   "OverwriteIfExisting"
         Columns(17).DataField=   "OverwriteIfExisting"
         Columns(17).FieldLen=   256
         Columns(17).Style=   2
         Columns(18).Width=   3254
         Columns(18).Caption=   "Transcode Engine"
         Columns(18).Name=   "Transcode Engine"
         Columns(18).FieldLen=   256
         Columns(18).Locked=   -1  'True
         Columns(19).Width=   4577
         Columns(19).Caption=   "Status"
         Columns(19).Name=   "statustext"
         Columns(19).DataField=   "description"
         Columns(19).FieldLen=   256
         Columns(20).Width=   3200
         Columns(20).Visible=   0   'False
         Columns(20).Caption=   "Transcode_Engine"
         Columns(20).Name=   "Transcode_Engine"
         Columns(20).DataField=   "Transcode_Engine"
         Columns(20).FieldLen=   256
         Columns(21).Width=   3200
         Columns(21).Visible=   0   'False
         Columns(21).Caption=   "Transcode_Stream"
         Columns(21).Name=   "Transcode_Stream"
         Columns(21).DataField=   "Transcode_Stream"
         Columns(21).FieldLen=   256
         Columns(22).Width=   3200
         Columns(22).Visible=   0   'False
         Columns(22).Caption=   "fps"
         Columns(22).Name=   "fps"
         Columns(22).DataField=   "fps"
         Columns(22).FieldLen=   256
         Columns(23).Width=   3200
         Columns(23).Visible=   0   'False
         Columns(23).Caption=   "name"
         Columns(23).Name=   "name"
         Columns(23).DataField=   "name"
         Columns(23).FieldLen=   256
         Columns(24).Width=   3200
         Columns(24).Visible=   0   'False
         Columns(24).Caption=   "cetasuer"
         Columns(24).Name=   "cetauser"
         Columns(24).DataField=   "cetauser"
         Columns(24).FieldLen=   256
         _ExtentX        =   21246
         _ExtentY        =   21616
         _StockProps     =   79
         Caption         =   "Transcode Requests"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdFileRequestsHybrid 
         Bindings        =   "frmTranscoding.frx":111F
         Height          =   12255
         Left            =   -62520
         TabIndex        =   335
         Top             =   540
         Width           =   15495
         _Version        =   196617
         AllowDelete     =   -1  'True
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeRow   =   3
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   29
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Hide"
         Columns(0).Name =   "Hide"
         Columns(0).DataField=   "Column 27"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   1323
         Columns(1).Caption=   "Req ID"
         Columns(1).Name =   "RequestID"
         Columns(1).DataField=   "event_File_RequestID"
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "HideFromGrid"
         Columns(2).Name =   "HideFromGrid"
         Columns(2).DataField=   "HideFromGrid"
         Columns(2).FieldLen=   256
         Columns(3).Width=   1588
         Columns(3).Caption=   "Clip ID"
         Columns(3).Name =   "Source File"
         Columns(3).DataField=   "Source File"
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "Originally On"
         Columns(4).Name =   "Originally On"
         Columns(4).DataField=   "Originally On"
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   2196
         Columns(5).Caption=   "Now Stored on"
         Columns(5).Name =   "Store"
         Columns(5).DataField=   "Source Barcode"
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "Source Filename"
         Columns(6).Name =   "Source Filename"
         Columns(6).DataField=   "Source Filename"
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(7).Width=   3519
         Columns(7).Caption=   "ClipReference"
         Columns(7).Name =   "clipreference"
         Columns(7).DataField=   "clipreference"
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(8).Width=   2302
         Columns(8).Caption=   "bigfilesize"
         Columns(8).Name =   "bigfilesize"
         Columns(8).DataField=   "bigfilesize"
         Columns(8).DataType=   17
         Columns(8).FieldLen=   256
         Columns(8).Locked=   -1  'True
         Columns(9).Width=   2831
         Columns(9).Caption=   "Operation"
         Columns(9).Name =   "File Operation"
         Columns(9).DataField=   "File Operation"
         Columns(9).FieldLen=   256
         Columns(9).Locked=   -1  'True
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "JobID"
         Columns(10).Name=   "JobID"
         Columns(10).DataField=   "JobID"
         Columns(10).FieldLen=   256
         Columns(10).Locked=   -1  'True
         Columns(11).Width=   3200
         Columns(11).Visible=   0   'False
         Columns(11).Caption=   "Del #"
         Columns(11).Name=   "CDDeliveryNumber"
         Columns(11).DataField=   "CDDeliveryNumber"
         Columns(11).FieldLen=   256
         Columns(12).Width=   3200
         Columns(12).Visible=   0   'False
         Columns(12).Caption=   "Notify"
         Columns(12).Name=   "NotifyRecipient"
         Columns(12).DataField=   "NotifyRecipient"
         Columns(12).FieldLen=   256
         Columns(12).Locked=   -1  'True
         Columns(13).Width=   2831
         Columns(13).Caption=   "Intended New Store"
         Columns(13).Name=   "New Store"
         Columns(13).DataField=   "New Store"
         Columns(13).FieldLen=   256
         Columns(13).Locked=   -1  'True
         Columns(14).Width=   3200
         Columns(14).Visible=   0   'False
         Columns(14).Caption=   "New Folder"
         Columns(14).Name=   "New Folder"
         Columns(14).DataField=   "New Folder"
         Columns(14).FieldLen=   256
         Columns(14).Locked=   -1  'True
         Columns(15).Width=   3200
         Columns(15).Visible=   0   'False
         Columns(15).Caption=   "New Filename"
         Columns(15).Name=   "NewFileName"
         Columns(15).DataField=   "NewFileName"
         Columns(15).FieldLen=   256
         Columns(15).Locked=   -1  'True
         Columns(16).Width=   3200
         Columns(16).Visible=   0   'False
         Columns(16).Caption=   "DNRC"
         Columns(16).Name=   "DoNotRecordCopy"
         Columns(16).Alignment=   2
         Columns(16).DataField=   "DoNotRecordCopy"
         Columns(16).FieldLen=   256
         Columns(16).Locked=   -1  'True
         Columns(17).Width=   2990
         Columns(17).Caption=   "Date"
         Columns(17).Name=   "Request Date"
         Columns(17).DataField=   "Request Date"
         Columns(17).DataType=   7
         Columns(17).FieldLen=   256
         Columns(18).Width=   1058
         Columns(18).Caption=   "Urgent"
         Columns(18).Name=   "Urgent"
         Columns(18).DataField=   "Urgent"
         Columns(18).FieldLen=   256
         Columns(18).Style=   2
         Columns(19).Width=   3200
         Columns(19).Visible=   0   'False
         Columns(19).Caption=   "Paused"
         Columns(19).Name=   "Paused"
         Columns(19).FieldLen=   256
         Columns(19).Style=   2
         Columns(20).Width=   3200
         Columns(20).Visible=   0   'False
         Columns(20).Caption=   "RequestPaused"
         Columns(20).Name=   "RequestPaused"
         Columns(20).DataField=   "RequestPaused"
         Columns(20).FieldLen=   256
         Columns(21).Width=   3200
         Columns(21).Visible=   0   'False
         Columns(21).Caption=   "By"
         Columns(21).Name=   "Requestname"
         Columns(21).DataField=   "Requestname"
         Columns(21).DataType=   10
         Columns(21).FieldLen=   256
         Columns(21).Locked=   -1  'True
         Columns(22).Width=   2990
         Columns(22).Caption=   "Started"
         Columns(22).Name=   "Request Started"
         Columns(22).DataField=   "Request Started"
         Columns(22).DataType=   7
         Columns(22).FieldLen=   256
         Columns(23).Width=   3200
         Columns(23).Visible=   0   'False
         Columns(23).Caption=   "Handler"
         Columns(23).Name=   "Handler"
         Columns(23).DataField=   "Handler"
         Columns(23).FieldLen=   256
         Columns(24).Width=   2990
         Columns(24).Caption=   "Completed"
         Columns(24).Name=   "Request Completed"
         Columns(24).DataField=   "Request Completed"
         Columns(24).DataType=   7
         Columns(24).FieldLen=   256
         Columns(25).Width=   2990
         Columns(25).Caption=   "Failed"
         Columns(25).Name=   "Request Failed"
         Columns(25).DataField=   "Request Failed"
         Columns(25).DataType=   7
         Columns(25).FieldLen=   256
         Columns(26).Width=   3519
         Columns(26).Caption=   "Comment"
         Columns(26).Name=   "Comment"
         Columns(26).DataField=   "Comment"
         Columns(26).FieldLen=   256
         Columns(27).Width=   3200
         Columns(27).Visible=   0   'False
         Columns(27).Caption=   "Cleaned Up"
         Columns(27).Name=   "RequestCleanedUp"
         Columns(27).DataField=   "RequestCleanedUp"
         Columns(27).FieldLen=   256
         Columns(28).Width=   3200
         Columns(28).Visible=   0   'False
         Columns(28).Caption=   "NewFileID"
         Columns(28).Name=   "NewFileID"
         Columns(28).DataField=   "NewFileID"
         Columns(28).FieldLen=   256
         _ExtentX        =   27331
         _ExtentY        =   21616
         _StockProps     =   79
         Caption         =   "File Requests"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnStatusHybrid 
         Bindings        =   "frmTranscoding.frx":1143
         Height          =   1095
         Left            =   -74400
         TabIndex        =   336
         Top             =   12960
         Width           =   6555
         DataFieldList   =   "description"
         MaxDropDownItems=   16
         _Version        =   196617
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   2
         Columns(0).Width=   3201
         Columns(0).Caption=   "transcodestatusID"
         Columns(0).Name =   "transcodestatusID"
         Columns(0).DataField=   "transcodestatusID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   4577
         Columns(1).Caption=   "description"
         Columns(1).Name =   "description"
         Columns(1).DataField=   "description"
         Columns(1).FieldLen=   256
         _ExtentX        =   11562
         _ExtentY        =   1931
         _StockProps     =   77
         DataFieldToDisplay=   "description"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnTranscodeSpecsHybrid 
         Bindings        =   "frmTranscoding.frx":115B
         Height          =   1095
         Left            =   -67080
         TabIndex        =   337
         Top             =   13080
         Width           =   6555
         DataFieldList   =   "transcodename"
         MaxDropDownItems=   16
         _Version        =   196617
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   3
         Columns(0).Width=   926
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "transcodespecID"
         Columns(0).DataField=   "transcodespecID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   6879
         Columns(1).Caption=   "transcodename"
         Columns(1).Name =   "transcodename"
         Columns(1).DataField=   "transcodename"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3810
         Columns(2).Caption=   "transcodesystem"
         Columns(2).Name =   "transcodesystem"
         Columns(2).DataField=   "transcodesystem"
         Columns(2).FieldLen=   256
         _ExtentX        =   11562
         _ExtentY        =   1931
         _StockProps     =   77
         DataFieldToDisplay=   "transcodename"
      End
      Begin MSAdodcLib.Adodc adoTranscodeSpecsHybrid 
         Height          =   330
         Left            =   -59880
         Top             =   13200
         Width           =   2865
         _ExtentX        =   5054
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoTranscodeSpecs"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.Label Label11 
         Caption         =   "Transcode System"
         Height          =   255
         Left            =   23460
         TabIndex        =   327
         Top             =   480
         Width           =   1335
      End
      Begin VB.Label Label6 
         Caption         =   "VariantID"
         Height          =   255
         Index           =   5
         Left            =   -48180
         TabIndex        =   282
         Top             =   11700
         Width           =   1395
      End
      Begin VB.Label Label6 
         Caption         =   "WorkflowDef ID"
         Height          =   255
         Index           =   4
         Left            =   -48180
         TabIndex        =   281
         Top             =   660
         Width           =   1395
      End
      Begin VB.Label Label6 
         Caption         =   "TaskTypeID"
         Height          =   255
         Index           =   3
         Left            =   -48180
         TabIndex        =   280
         Top             =   1560
         Width           =   1395
      End
      Begin VB.Label Label6 
         Caption         =   "WorkflowID"
         Height          =   255
         Index           =   2
         Left            =   -48180
         TabIndex        =   279
         Top             =   7380
         Width           =   1395
      End
      Begin VB.Label Label6 
         Caption         =   "LastTaskID"
         Height          =   255
         Index           =   1
         Left            =   -48180
         TabIndex        =   278
         Top             =   8820
         Width           =   1395
      End
      Begin VB.Label Label6 
         Caption         =   "TaskID"
         Height          =   255
         Index           =   0
         Left            =   -48180
         TabIndex        =   277
         Top             =   8100
         Width           =   1395
      End
      Begin VB.Label lblTaskID 
         BackColor       =   &H0080FFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   -48180
         TabIndex        =   276
         Top             =   8400
         Width           =   1395
      End
      Begin VB.Label lblLastTaskID 
         BackColor       =   &H0080FFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   -48180
         TabIndex        =   275
         Top             =   9120
         Width           =   1395
      End
      Begin VB.Label lblWorkflowID 
         BackColor       =   &H0080FFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   -48180
         TabIndex        =   274
         Top             =   7680
         Width           =   1395
      End
      Begin VB.Label lblTaskTypeID 
         BackColor       =   &H0080FFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   -48180
         TabIndex        =   273
         Top             =   1860
         Width           =   1395
      End
      Begin VB.Label lblWorkflowDefinitionID 
         BackColor       =   &H0080FFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   -48180
         TabIndex        =   272
         Top             =   960
         Width           =   1395
      End
      Begin VB.Label lblVariantID 
         BackColor       =   &H0080FFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   -48180
         TabIndex        =   271
         Top             =   12000
         Width           =   1395
      End
      Begin VB.Label Label5 
         Caption         =   "Search CompanyID"
         Height          =   255
         Index           =   0
         Left            =   -65040
         TabIndex        =   240
         Top             =   540
         Width           =   1455
      End
      Begin VB.Label Label3 
         Caption         =   "Significant Date"
         Height          =   255
         Left            =   -61860
         TabIndex        =   147
         Top             =   480
         Width           =   1275
      End
      Begin VB.Label Label2 
         Caption         =   "File Operation to Display"
         Height          =   255
         Left            =   -58920
         TabIndex        =   140
         Top             =   480
         Width           =   1875
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   480
      Top             =   15540
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   720
      Left            =   120
      TabIndex        =   0
      Top             =   14900
      Width           =   28155
      Begin VB.TextBox txtCountDown 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   22080
         TabIndex        =   332
         Text            =   "99"
         Top             =   270
         Width           =   375
      End
      Begin VB.CheckBox chkTop71 
         Alignment       =   1  'Right Justify
         Caption         =   "Top 71 Requests only"
         Height          =   255
         Left            =   18480
         TabIndex        =   331
         Top             =   270
         Width           =   1875
      End
      Begin VB.TextBox txtSearchJobNumber 
         Height          =   345
         Left            =   10560
         TabIndex        =   328
         Top             =   0
         Width           =   1335
      End
      Begin VB.TextBox txtSearchTrackerID 
         Height          =   345
         Left            =   16080
         TabIndex        =   324
         Top             =   0
         Width           =   1335
      End
      Begin VB.CheckBox chkTop42 
         Alignment       =   1  'Right Justify
         Caption         =   "Top 42 Requests only"
         Height          =   255
         Left            =   18480
         TabIndex        =   323
         Top             =   0
         Width           =   1875
      End
      Begin VB.TextBox txtSearchDeliveryNumber 
         Height          =   345
         Left            =   13260
         TabIndex        =   317
         Top             =   0
         Width           =   1335
      End
      Begin VB.TextBox txtSearchWorkflowInstance 
         Height          =   345
         Left            =   7920
         TabIndex        =   231
         Top             =   0
         Width           =   1335
      End
      Begin VB.CheckBox chkSuspendAutoRefresh 
         Alignment       =   1  'Right Justify
         Caption         =   "Suspend Auto-Refresh"
         Height          =   255
         Left            =   20520
         TabIndex        =   148
         Top             =   0
         Value           =   1  'Checked
         Width           =   1935
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   375
         Index           =   0
         Left            =   17640
         TabIndex        =   8
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   615
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Refresh"
         Height          =   375
         Left            =   22500
         TabIndex        =   7
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1335
      End
      Begin VB.TextBox txtSearchTranscodes 
         Height          =   345
         Left            =   1560
         TabIndex        =   5
         Top             =   0
         Width           =   3855
      End
      Begin VB.CommandButton cmdFail 
         Caption         =   "Fail Grid"
         Height          =   375
         Left            =   23940
         TabIndex        =   4
         ToolTipText     =   "Close this form"
         Top             =   0
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.CommandButton cmdSubmitAgain 
         Caption         =   "Do Grid Again"
         Height          =   375
         Left            =   25380
         TabIndex        =   3
         ToolTipText     =   "Close this form"
         Top             =   0
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   375
         Left            =   26820
         TabIndex        =   1
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1335
      End
      Begin VB.Label Label12 
         Caption         =   "Search Job #"
         Height          =   255
         Left            =   9540
         TabIndex        =   329
         Top             =   60
         Width           =   975
      End
      Begin VB.Label Label10 
         Caption         =   "Search TrackerID"
         Height          =   255
         Left            =   14700
         TabIndex        =   325
         Top             =   60
         Width           =   1275
      End
      Begin VB.Label Label9 
         Caption         =   "Search Del #"
         Height          =   255
         Left            =   12240
         TabIndex        =   318
         Top             =   60
         Width           =   975
      End
      Begin VB.Label Label4 
         Caption         =   "Search Workflow Instance"
         Height          =   255
         Left            =   5880
         TabIndex        =   232
         Top             =   60
         Width           =   1995
      End
      Begin VB.Label Label1 
         Caption         =   "Search Reference"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   60
         Width           =   1335
      End
   End
End
Attribute VB_Name = "frmTranscoding"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_lngRequested As Long
Dim m_lngProcessing As Long
Dim m_lngComplete As Long
Dim m_lngFinishingOff As Long
Dim m_lngOnHold As Long
Dim m_blnDelete As Boolean
Dim m_Framerate As Integer
Dim m_lngTimeSinceLastRefresh As Long
Dim m_blnTabUpdate As Boolean
Dim l_conSearch As ADODB.Connection
Dim l_rstSearch1 As ADODB.Recordset
Dim l_rstSearch2 As ADODB.Recordset
Dim l_rstSearch3 As ADODB.Recordset
Dim l_rstSearch4 As ADODB.Recordset
Dim l_rstSearch5 As ADODB.Recordset
Dim l_rstSearch6 As ADODB.Recordset
Dim l_rstSearch7 As ADODB.Recordset
Dim l_rstSearch8 As ADODB.Recordset
Dim l_rstSearch9 As ADODB.Recordset
Dim l_rstSearch10 As ADODB.Recordset
Dim l_rstSearch11 As ADODB.Recordset
Dim l_rstSearch12 As ADODB.Recordset
Dim l_rstSearch13 As ADODB.Recordset
Dim l_rstSearch14 As ADODB.Recordset
Dim l_rstSearch15 As ADODB.Recordset
Dim l_rstSearch16 As ADODB.Recordset
Dim l_rstSearch17 As ADODB.Recordset
Dim l_rstSearch18 As ADODB.Recordset
Dim l_rstSearch19 As ADODB.Recordset
Dim l_rstSearch20 As ADODB.Recordset
Dim l_rstSearch21 As ADODB.Recordset
Dim l_rstSearch22 As ADODB.Recordset
Dim m_blnSkipChange As Boolean
    
Private Sub Check1_Click()

End Sub

Private Sub chkReplaceOriginalFile_Click()

If chkReplaceOriginalFile.Value <> 0 Then
    txtDeliveryFolder(1).Enabled = False
    cmbDeliveryBarcode(1).Enabled = False
    chkDestinationThrowawayFile(1).Enabled = False
    chkDestinationDeliveryCopy(1).Enabled = False
Else
    txtDeliveryFolder(1).Enabled = True
    cmbDeliveryBarcode(1).Enabled = True
    chkDestinationThrowawayFile(1).Enabled = True
    chkDestinationDeliveryCopy(1).Enabled = True
End If

End Sub

Private Sub chkTranscodeSystem_Click(Index As Integer)

If Index = 17 And chkTranscodeSystem(Index).Value <> 0 And m_blnSkipChange = False Then
    m_blnSkipChange = True
    chkNoReferenceCheck.Visible = True
    cmdChange.Caption = "Clear all Transcode Systems"
    cmdChange_Click
    chkTranscodeSystem(Index).Value = 1
    m_blnSkipChange = False
ElseIf Index = 17 And chkTranscodeSystem(Index).Value = 0 And m_blnSkipChange = False Then
    chkNoReferenceCheck.Visible = False
End If

End Sub

Private Sub cmbAudioMapping_Click(Index As Integer)
lblAudioMapping(Index).Caption = GetDataSQL("SELECT information FROM xref WHERE category = 'audiomapping' AND description = '" & cmbAudioMapping(Index).Text & "';")
End Sub

Private Sub cmbBarcode_Click(Index As Integer)

lblDestinationLibraryID(Index).Caption = GetDataSQL("Select TOP 1 libraryID FROM library WHERE barcode = '" & cmbBarcode(Index).Text & "' AND system_deleted = 0")
If lblDestinationLibraryID(Index).Caption = "0" Then Beep: lblDestinationLibraryID(Index).Caption = ""

End Sub

Private Sub cmbBarcode_KeyPress(Index As Integer, KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    cmbBarcode(Index).Text = UCase(cmbBarcode(Index).Text)
    lblDestinationLibraryID(Index).Caption = GetDataSQL("Select TOP 1 libraryID FROM library WHERE barcode = '" & cmbBarcode(Index).Text & "' AND system_deleted = 0")
    If lblDestinationLibraryID(Index).Caption = "0" Then Beep: lblDestinationLibraryID(Index).Caption = ""
End If
End Sub

Private Sub cmbDeliveryBarcode_Click(Index As Integer)

lblDeliveryLibraryID(Index).Caption = GetDataSQL("Select TOP 1 libraryID FROM library WHERE barcode = '" & cmbDeliveryBarcode(Index).Text & "' AND system_deleted = 0")
If lblDeliveryLibraryID(Index).Caption = "0" Then Beep: lblDeliveryLibraryID(Index).Caption = ""

End Sub

Private Sub cmbDeliveryBarcode_KeyPress(Index As Integer, KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    cmbDeliveryBarcode(Index).Text = UCase(cmbDeliveryBarcode(Index).Text)
    lblDeliveryLibraryID(Index).Caption = GetDataSQL("Select TOP 1 libraryID FROM library WHERE barcode = '" & cmbDeliveryBarcode(Index).Text & "' AND system_deleted = 0")
    If lblDeliveryLibraryID(Index).Caption = "0" Then Beep: lblDeliveryLibraryID(Index).Caption = ""
End If
End Sub

Private Sub cmbMediaSpec_Click(Index As Integer)
lblMediaSpecID(Index).Caption = cmbMediaSpec(Index).Columns("mediaspecID").Text
End Sub

Private Sub cmbNewCompany_Click(Index As Integer)

lblNewCompanyID(Index).Caption = cmbNewCompany(Index).Columns("companyID").Text

End Sub

Private Sub cmbNextTaskID_DropDown()
chkSuspendAutoRefresh.Value = 1
End Sub

Private Sub cmbPurpose_GotFocus(Index As Integer)
PopulateCombo "clippurpose", cmbPurpose(Index)
HighLite cmbPurpose(Index)
End Sub

Private Sub cmbTranscodeSystem_CloseUp()

Select Case cmbTranscodeSystem.Text

    Case "FFMpeg_Audio_Conform"
        chkReplaceOriginalFile.Visible = True
    Case Else
        chkReplaceOriginalFile.Visible = False
        chkReplaceOriginalFile.Value = 0
End Select

End Sub

Private Sub cmbTranscodeSystem_Validate(Cancel As Boolean)

Select Case cmbTranscodeSystem.Text

    Case "FFMpeg_Audio_Conform"
        chkReplaceOriginalFile.Visible = True
    Case Else
        chkReplaceOriginalFile.Visible = False
        chkReplaceOriginalFile.Value = 0
End Select

End Sub

Private Sub cmdActuallyMakeTheVariant_Click()

Dim SQL As String, rs As ADODB.Recordset, l_strMessage As String

If MsgBox("You are about to make a new BBC Premium Content Variant, using these Settings." & vbCrLf & "Are you sure", vbYesNo + vbDefaultButton2, "Confirm Action") = vbYes Then
    SQL = "Exec Workflow_MakeNewVariant "
    SQL = SQL & "'" & txtVariantName.Text & "', "
    SQL = SQL & "'" & lblBBCPCWorkflowName.Caption & "', "
    SQL = SQL & "'" & txtOrganisation.Text & "', "
    SQL = SQL & IIf(txtOutputFilenamePattern.Text <> "", "'" & txtOutputFilenamePattern.Text & "', ", "Null, ")
    SQL = SQL & IIf(txtReleasePolicy.Text <> "", "'" & txtReleasePolicy.Text & "', ", "Null, ")
    SQL = SQL & Val(txtCetaJobID.Text) & ", "
    SQL = SQL & IIf(txtDigitalDestinationID.Text <> "", Val(txtDigitalDestinationID.Text) & ", ", "Null, ")
    SQL = SQL & Val(txtTranscodeSpecID.Text) & ", "
    SQL = SQL & IIf(txtTranscodeSegment.Text <> "", "'" & txtTranscodeSegment.Text & "', ", "Null, ")
    SQL = SQL & IIf(txtSourceShape.Text <> "", "'" & txtSourceShape.Text & "'", "Null")
    
    Set rs = ExecuteSQL(SQL, g_strExecuteError)
    l_strMessage = Trim(" " & rs(0))
    rs.Close
    MsgBox "Command Return:" & vbCrLf & Replace(l_strMessage, vbLf, vbCrLf)
    Set rs = Nothing
End If

fraBBCPremValues.Visible = False

End Sub

Private Sub cmdApplyDefaaultVariables_Click()

Dim db As ADODB.Connection

If MsgBox("You are about to apply Default Variables to all tasks in the curently selected Workflow, from the Defult Task Type Varables." & vbCrLf & "Are you sure", vbYesNo + vbDefaultButton2, "Confirm Action") = vbYes Then
    
    Set db = New ADODB.Connection
    db.ConnectionString = g_strConnection
    db.Open
    
    db.Execute "Exec Workflow_ApplyDefaultVariables_NewWorkflow " & lblWorkflowID.Caption
    
    db.Close
    Set db = Nothing
End If

End Sub

Private Sub cmdCancelThisActivity_Click()

fraBBCPremValues.Visible = False

End Sub

Private Sub cmdChange_Click()

Dim counter As Integer
If cmdChange.Caption = "Clear all Transcode Systems" Then
    For counter = 0 To 16
        If counter <> 0 And counter <> 1 And counter <> 2 And counter <> 5 And counter <> 6 And counter <> 7 And counter <> 8 And counter <> 9 And counter <> 10 And counter <> 12 And counter <> 13 And counter <> 14 And counter <> 15 And counter <> 16 Then
            chkTranscodeSystem(counter).Value = 0
        End If
    Next
    cmdChange.Caption = "Set all Transcode Systems"
Else
    For counter = 0 To 16
        If counter <> 0 And counter <> 1 And counter <> 2 And counter <> 5 And counter <> 6 And counter <> 7 And counter <> 8 And counter <> 9 And counter <> 10 And counter <> 12 And counter <> 13 And counter <> 14 And counter <> 15 And counter <> 16 Then
            chkTranscodeSystem(counter).Value = 1
        End If
    Next
    cmdChange.Caption = "Clear all Transcode Systems"
End If

End Sub

Private Sub cmdClear_Click(Index As Integer)

Select Case Index
Case 0
    txtSearchTranscodes.Text = ""
    txtSearchWorkflowInstance.Text = ""
    txtSearchDeliveryNumber.Text = ""
    txtSearchTrackerID.Text = ""
    txtSearchJobNumber.Text = ""
Case 1
    txtSearchCompanyID.Text = ""
End Select

End Sub

Private Sub cmdClearNewCompany_Click(Index As Integer)

cmbNewCompany(Index).Text = ""
lblNewCompanyID(Index).Caption = ""

End Sub

Private Sub cmdClearSpec_Click(Index As Integer)

If Index = 6 Then
    txtHeadbuildEventID.Text = ""
    lblHeadbuildFilename.Caption = ""
Else
    cmbMediaSpec(Index).Text = ""
    lblMediaSpecID(Index).Caption = ""
End If

End Sub

Private Sub cmdCloneVariant_Click()

Dim OldVariantID As Long, NewVariantID As Long, cnn As ADODB.Connection

OldVariantID = Val(InputBox("Give the Old Workflow ID", "Original Workflow ID"))
NewVariantID = Val(InputBox("GOve the New Workflow ID", "New WOrkflow ID"))

If MsgBox("You are about to clone all Variant Variables from ID " & OldVariantID & " into the Variables ID " & NewVariantID & vbCrLf & "Are you Sure", vbYesNo + vbDefaultButton2, "Confirm Clone Variant") = vbYes Then
    Set cnn = New ADODB.Connection
    cnn.Open g_strConnection
    cnn.Execute "Exec Workflow_CloneVariant " & OldVariantID & ", " & NewVariantID
    cnn.Close
    Set cnn = Nothing
End If

End Sub

Private Sub cmdCloneWorkflow_Click()

Dim OldWorkflowID As Long, NewWorkflowID As Long, cnn As ADODB.Connection

OldWorkflowID = Val(InputBox("Give the Old Workflow ID", "Original Workflow ID"))
NewWorkflowID = Val(InputBox("Give the New Workflow ID", "New WOrkflow ID"))

If MsgBox("You are about to clone all workflow tasks from ID " & OldWorkflowID & " into the workflow ID " & NewWorkflowID & vbCrLf & "Are you Sure", vbYesNo + vbDefaultButton2, "Confirm Clone Workflow") = vbYes Then
    Set cnn = New ADODB.Connection
    cnn.Open g_strConnection
    cnn.Execute "Exec Workflow_CloneWorkflow " & OldWorkflowID & ", " & NewWorkflowID
    cnn.Close
    Set cnn = Nothing
End If

End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdConfigSynopsis_Click()

Dim rs As ADODB.Recordset, l_strMessage As String
Set rs = ExecuteSQL("exec workflow_OutputSynopsis " & lblWorkflowID.Caption, g_strExecuteError)
On Error Resume Next
If Not rs.EOF Then
    l_strMessage = Trim(" " & rs(0))
End If
rs.Close
txtWorkflowConfigurationSynopsis.Text = Replace(l_strMessage, vbLf, vbCrLf)
fraSynopsis.Visible = True

End Sub

Private Sub cmdDeleteSpecification_Click()

Dim l_strSQL As String

If Val(grdTranscodeSpecs.Columns("transcodespecID").Text) = 0 Then Exit Sub

If MsgBox("Are you Sure?", vbYesNo, "Deleting Transcode Specification") = vbNo Then Exit Sub

l_strSQL = "UPDATE transcodespec SET system_deleted = 1, enabled = 0 WHERE transcodespecID = " & lblSpecID.Caption & ";"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError
adoTranscodeSpecs.Refresh

tabSpecs.Tab = 0

End Sub

Private Sub cmdFail_Click()

If Not CheckAccess("/RerunTranscodes") Then Exit Sub

If tabSpecs.Tab <> 0 Then Exit Sub

Dim l_strbody As String
If MsgBox("Are you Sure", vbYesNo, "Fail all Processing Items") = vbYes Then
    adoTranscodeRequests.Recordset.MoveFirst
    Do While Not adoTranscodeRequests.Recordset.EOF
        If adoTranscodeRequests.Recordset("status") = GetData("transcodestatus", "transcodestatusID", "description", "Processing") Then
            SetData "transcoderequest", "status", "transcoderequestID", adoTranscodeRequests.Recordset("transcoderequestID"), GetData("transcodestatus", "transcodestatusID", "description", "Transcode Failed")
            'Send out the notification emails
            l_strbody = "Source: " & GetData("events", "clipfilename", "eventID", adoTranscodeRequests.Recordset("sourceclipID")) & vbCrLf & "Transcode requested: " & GetData("transcodespec", "transcodename", "transcodespecID", adoTranscodeRequests.Recordset("transcodespecID")) & vbCrLf
            If Trim(" " & adoTranscodeRequests.Recordset("contactID")) <> "" Then
                If GetData("contact", "email", "contactID", Val(adoTranscodeRequests.Recordset("contactID"))) <> "" Then
                    SendSMTPMail GetData("contact", "email", "contactID", Val(adoTranscodeRequests.Recordset("contactID"))), adoTranscodeRequests.Recordset("contactname"), "MX1 Media Window Transcode Failed", l_strbody, "", True, g_strOperationsEmailAddress, g_strOperationsEmailName, g_strAdministratorEmailAddress
                End If
            End If
        End If
        adoTranscodeRequests.Recordset.MoveNext
    Loop
    cmdSearch.Value = True
End If

End Sub

Private Sub cmdHideSynopsis_Click()

txtWorkflowConfigurationSynopsis.Text = ""
fraSynopsis.Visible = False

End Sub

Private Sub cmdInsertSpecification_Click()

ExecuteSQL "INSERT INTO transcodespec (forder) VALUES (5);", g_strExecuteError
CheckForSQLError
lblSpecID.Caption = g_lngLastID
cmdSave.Value = True

End Sub

Private Sub cmdMakeNewBBCPCVariant_Click()

txtVariantName.Text = ""
txtOrganisation.Text = ""
txtOutputFilenamePattern.Text = ""
txtReleasePolicy.Text = ""
txtCetaJobID.Text = ""
txtDigitalDestinationID.Text = ""
txtTranscodeSpecID.Text = ""
txtTranscodeSegment.Text = ""
txtSourceShape.Text = ""
lblBBCPCWorkflowName.Caption = grdWorkflowDefinitions.Columns("WorkflowName").Text
fraBBCPremValues.Visible = True

End Sub

Private Sub cmdNextTaskToRun_Click()

Dim SQL As String, rs As ADODB.Recordset, l_strMessage As String, bkmk As Variant

If lblInstanceID.Caption <> "" Then
    If cmbNextTaskID.Text <> "" Then
        If MsgBox("Assign Instance " & lblInstanceID.Caption & " Next Task to Task: " & vbCrLf & cmbNextTaskID.Columns("TaskID").Text & ": " & cmbNextTaskID.Columns("StageName").Text & vbCrLf & "Are You Sure", vbYesNo + vbDefaultButton2) = vbYes Then
            SQL = "exec Workflow_AssignInstance_ToTask_Ceta '" & g_strUserName & "', " & lblInstanceID.Caption & ", " & cmbNextTaskID.Columns("TaskID").Text
            Set rs = ExecuteSQL(SQL, g_strExecuteError)
            l_strMessage = Trim(" " & rs(0))
            rs.Close
            MsgBox "Command Return:" & vbCrLf & Replace(l_strMessage, vbLf, vbCrLf)
            Set rs = Nothing
            bkmk = grdWorkflows.Bookmark
            adoWorkflows.Refresh
            grdWorkflows.Bookmark = bkmk
            cmbNextTaskID.Text = ""
        End If
    End If
End If

End Sub

Private Sub cmdSave_Click()

Dim l_strSQL As String, Bookmark As Variant, Index As Integer

If Val(lblSpecID.Caption) = 0 Then Exit Sub

l_strSQL = "UPDATE transcodespec SET "
l_strSQL = l_strSQL & "autotranscode = '" & chkAutoTranscode.Value & "', "
l_strSQL = l_strSQL & "billingcode = '" & txtBillingCode.Text & "', "
l_strSQL = l_strSQL & "transcodename = '" & QuoteSanitise(txtTitle.Text) & "', "
l_strSQL = l_strSQL & "enabled = " & chkEnabled.Value & ", "
l_strSQL = l_strSQL & "skiplogging = " & chkSkipLogging.Value & ", "
l_strSQL = l_strSQL & "watchfolder = '" & txtWatchfolder.Text & "', "
l_strSQL = l_strSQL & "flashpreviewwatchfolder = '" & cmbATSProfile.Text & "', "
l_strSQL = l_strSQL & "transcodesystem = '" & cmbTranscodeSystem.Text & "', "
l_strSQL = l_strSQL & "NexGuard = " & chkNexguard.Value & ", "
l_strSQL = l_strSQL & "NoReferenceCheck = " & chkNoReferenceCheck.Value & ", "
l_strSQL = l_strSQL & "ReplaceOriginal = " & chkReplaceOriginalFile.Value & ", "
l_strSQL = l_strSQL & "HeadbuildEventID = " & IIf(Val(txtHeadbuildEventID.Text) > 0, Val(txtHeadbuildEventID.Text), "Null") & ", "
l_strSQL = l_strSQL & "otherspecs = '" & QuoteSanitise(txtOtherSpecs.Text) & "', "
l_strSQL = l_strSQL & "ffmpeg_explicit_version = '" & txtExplicitFFMpegVersion.Text & "', "
l_strSQL = l_strSQL & "ReStripeTranscode = " & chkReStripe.Value & ", "
l_strSQL = l_strSQL & "transcodeasmaster = " & chkAsOrig.Value

For Index = 1 To 5
    If Val(lblDestinationLibraryID(Index).Caption) = 722000 Or Val(lblDestinationLibraryID(Index).Caption) = 722001 Or Val(lblDestinationLibraryID(Index).Caption) = 722002 Or Val(lblDestinationLibraryID(Index).Caption) = 722003 _
    Or Val(lblDestinationLibraryID(Index).Caption) = 722004 Or Val(lblDestinationLibraryID(Index).Caption) = 722005 Then
        MsgBox "Cannot Transcode directly to a DMZ store." & vbCrLf & "Transcode Spec NOT saved.", vbCritical, "Error ..."
        Exit Sub
    End If
    l_strSQL = l_strSQL & ", mediaspec" & Index & "ID = " & Val(lblMediaSpecID(Index).Caption)
    l_strSQL = l_strSQL & ", destination" & Index & "libraryID = " & Val(lblDestinationLibraryID(Index).Caption)
    l_strSQL = l_strSQL & ", destination" & Index & "folder = '" & txtDestinationFolder(Index).Text & "'"
    l_strSQL = l_strSQL & ", destination" & Index & "fileextender = '" & txtDestinationExtender(Index).Text & "'"
    l_strSQL = l_strSQL & ", destination" & Index & "newcompanyID = " & Val(lblNewCompanyID(Index).Caption)
    l_strSQL = l_strSQL & ", destination" & Index & "eventtype = '" & cmbPurpose(Index).Text & "'"
    l_strSQL = l_strSQL & ", destination" & Index & "nodurationcheck = " & GetFlag(chkNoDurationCheck(Index).Value)
    If chkReplaceOriginalFile.Value = 0 Then
        l_strSQL = l_strSQL & ", destination" & Index & "deliverylibraryID = " & Val(lblDeliveryLibraryID(Index).Caption)
        l_strSQL = l_strSQL & ", destination" & Index & "deliveryfolder = '" & txtDeliveryFolder(Index).Text & "'"
        l_strSQL = l_strSQL & ", destination" & Index & "throwaway = " & GetFlag(chkDestinationThrowawayFile(Index).Value)
        l_strSQL = l_strSQL & ", destination" & Index & "deliverycopy = " & GetFlag(chkDestinationDeliveryCopy(Index).Value)
    Else
        l_strSQL = l_strSQL & ", destination" & Index & "deliverylibraryID = 0"
        l_strSQL = l_strSQL & ", destination" & Index & "deliveryfolder = Null"
        l_strSQL = l_strSQL & ", destination" & Index & "throwaway = 0"
        l_strSQL = l_strSQL & ", destination" & Index & "deliverycopy = 0"
    End If
    l_strSQL = l_strSQL & ", destination" & Index & "audiomapping = '" & lblAudioMapping(Index).Caption & "'"
    l_strSQL = l_strSQL & ", destination" & Index & "audiomappingname = '" & cmbAudioMapping(Index).Text & "'"
Next

l_strSQL = l_strSQL & "WHERE transcodespecID = " & lblSpecID.Caption & ";"

Debug.Print l_strSQL
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

Bookmark = adoTranscodeSpecs.Recordset.Bookmark
adoTranscodeSpecs.Refresh
adoTranscodeSpecs.Recordset.Bookmark = Bookmark

'tabSpecs.Tab = 0

End Sub

Private Sub cmdSearch_Click()

Dim l_strSQL As String, l_strSystems As String

l_strSQL = "SELECT * FROM Transcodespec WHERE system_deleted = 0 "
If optShowTranscodeSpecs(0).Value = True Then
    l_strSQL = l_strSQL & "AND enabled <> 0 "
End If
l_strSQL = l_strSQL & "AND transcodesystem IN ("
l_strSystems = "'NoSystem'"
If chkTranscodeSystem(4).Value <> 0 Then
    l_strSystems = l_strSystems & ", " & "'Vantage_API'"
End If
If chkTranscodeSystem(3).Value <> 0 Then
    l_strSystems = l_strSystems & ", " & "'FFMpeg_Generic', 'FFMpeg_Generic_Test'"
End If
'If chkTranscodeSystem(9).Value <> 0 Then
'    l_strSystems = l_strSystems & ", " & "'FFMpeg_ProRes_Clip', 'FFMpeg_ProRes_Clip_Test'"
'End If
If chkTranscodeSystem(11).Value <> 0 Then
    l_strSystems = l_strSystems & ", " & "'FFMpeg_Thumb'"
End If
'If chkTranscodeSystem(5).Value <> 0 Then
'    l_strSystems = l_strSystems & ", " & "'BBCMG_Proxy', 'BBCMG_Proxy_Test'"
'End If
'If chkTranscodeSystem(15).Value <> 0 Then
'    l_strSystems = l_strSystems & ", " & "'BBCMG_Proxy_Duplicate'"
'End If
'If chkTranscodeSystem(16).Value <> 0 Then
'    l_strSystems = l_strSystems & ", " & "'BBCMG_Proxy_Silent'"
'End If
If chkTranscodeSystem(17).Value <> 0 Then
    l_strSystems = l_strSystems & ", " & "'Rapids', 'FileMoves', 'FFMpeg_ScreenGrab', 'FFMpeg_Speed_Change', 'FFMpeg_Audio_Conform', 'FFMpeg_Audio_Conform_Test'"
End If
l_strSQL = l_strSQL & l_strSystems
l_strSQL = l_strSQL & ") "
l_strSQL = l_strSQL & "ORDER BY forder, transcodename"
Debug.Print l_strSQL
adoTranscodeSpecs.ConnectionString = g_strConnection
adoTranscodeSpecs.RecordSource = l_strSQL
adoTranscodeSpecs.Refresh
adoTranscodeSpecs.Caption = adoTranscodeSpecs.Recordset.RecordCount & " Record(s)"

Select Case tabSpecs.Tab
    Case 0
        RefreshTranscodeRequests
    Case 1
        RefreshTranscodeSpecs
    Case 2
        RefreshFileRequests
    Case 3
        RefreshTechrevOutputRequests
    Case 4
        RefreshWorkflows
    Case 5
        RefreshWorkflowConfig
    Case 6
        RefreshTranscodeRequestsHybrid
        RefreshFileRequestsHybrid
End Select

m_lngTimeSinceLastRefresh = Val(txtCountDown.Text)

End Sub

Private Sub cmdSetNewDueDate_Click()

Dim SQL As String, bkmk As Variant

If lblInstanceID.Caption = "" Then Exit Sub

If Not IsNull(datDueDate.Value) Then
    If GetDataSQL("SELECT InstanceVariableID FROM Workflow_InstanceVariables WHERE InstanceID = " & lblInstanceID.Caption & " AND VariableName = 'JobDueDate';") <> "" Then
        SQL = "UPDATE Workflow_InstanceVariables SET Value = '" & Format(datDueDate.Value, "YYYY-MM-DD") & "' WHERE InstanceID = " & adoWorkflows.Recordset("InstanceID") & " AND VariableName = 'JobDueDate';"
        If MsgBox("Updating Due Date to " & Format(datDueDate.Value, "YYYY-MM-DD") & vbCrLf & "Are You Sure", vbYesNo + vbDefaultButton2, "Updating Job Due Date") = vbYes Then
            ExecuteSQL SQL, g_strExecuteError
            CheckForSQLError
            datDueDate.Value = Null
            bkmk = grdWorkflows.Bookmark
            adoWorkflows.Refresh
            grdWorkflows.Bookmark = bkmk
            adoInstanceVariables.Refresh
        End If
    Else
        If MsgBox("Updating Due Date to " & Format(datDueDate.Value, "YYYY-MM-DD") & vbCrLf & "Are You Sure", vbYesNo + vbDefaultButton2, "Updating Job Due Date") = vbYes Then
            SQL = "Insert into Workflow_InstanceVariables (InstanceID, VariableName, Value, TaskID) " & _
            "SELECT top (1) InstanceID, 'jobDueDate' as VariableName, '" & Format(datDueDate.Value, "YYYY-MM-DD") & "', FirstTaskId as TaskID " & _
            "From Workflow_Instance " & _
            " Join Workflow_Variant on Workflow_Variant.VariantID = Workflow_Instance.VariantID " & _
            " Where InstanceID =" & lblInstanceID.Caption
            Debug.Print SQL
            ExecuteSQL SQL, g_strExecuteError
            CheckForSQLError
            datDueDate.Value = Null
            bkmk = grdWorkflows.Bookmark
            adoWorkflows.Refresh
            grdWorkflows.Bookmark = bkmk
            adoInstanceVariables.Refresh
        End If
    End If
End If

End Sub

Private Sub cmdSubmitAgain_Click()

If Not CheckAccess("/RerunTranscodes") Then Exit Sub

If tabSpecs.Tab <> 0 Then Exit Sub

If MsgBox("Are you Sure", vbYesNo, "Re-Submit Entire List for Transcode") = vbYes Then
    adoTranscodeRequests.Recordset.MoveFirst
    Do While Not adoTranscodeRequests.Recordset.EOF
        SetData "transcoderequest", "Transcode_Engine", "transcoderequestID", adoTranscodeRequests.Recordset("transcoderequestID"), "Null"
        SetData "transcoderequest", "Transcode_Stream", "transcoderequestID", adoTranscodeRequests.Recordset("transcoderequestID"), "Null"
        SetData "transcoderequest", "status", "transcoderequestID", adoTranscodeRequests.Recordset("transcoderequestID"), GetData("transcodestatus", "transcodestatusID", "description", "Requested")
        adoTranscodeRequests.Recordset.MoveNext
    Loop
    cmdSearch.Value = True
End If

End Sub

Private Sub Command1_Click()

End Sub

Private Sub ddnCompany_CloseUp()

grdWorkflowVariants.Columns("companyID").Text = ddnCompany.Columns("companyID").Text

End Sub

Private Sub ddnStatus_CloseUp()

grdTranscodeRequests.Columns("status").Text = ddnStatus.Columns("transcodestatusID").Text

End Sub

Private Sub ddnStatus_DropDown()
adoStatus.Refresh
End Sub

Private Sub ddnStatusHybrid_CloseUp()

grdTranscodeRequestsHybrid.Columns("status").Text = ddnStatusHybrid.Columns("transcodestatusID").Text

End Sub

Private Sub ddnStatusHybrid_DropDown()
adoStatus.Refresh
End Sub

Private Sub ddnTaskTypes_CloseUp(Index As Integer)

Select Case Index
Case 0
    grdWorkflowTasks.Columns("TaskTypeID").Text = ddnTaskTypes(Index).Columns("TaskTypeID").Text
Case 1
    grdTaskTypeDefaultVariables.Columns("TaskTypeID").Text = ddnTaskTypes(Index).Columns("TaskTypeID").Text
End Select

End Sub

Private Sub ddnTranscodeSpecs_CloseUp()

grdTranscodeRequests.Columns("transcodespecID").Text = ddnTranscodeSpecs.Columns("transcodespecID").Text
grdTranscodeRequests.Columns("transcodesystem").Text = ddnTranscodeSpecs.Columns("transcodesystem").Text

End Sub

Private Sub ddnWorkflowDefinitions_CloseUp(Index As Integer)

Select Case Index
Case 0
    grdWorkflowTasks.Columns("WorkflowID").Text = ddnWorkflowDefinitions(Index).Columns("WorkflowID").Text
Case 1
    grdWorkflowVariants.Columns("WorkflowID").Text = ddnWorkflowDefinitions(Index).Columns("WorkflowID").Text
End Select

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

grdTranscodeRequests.StyleSets.Add "pending"
grdTranscodeRequests.StyleSets("pending").BackColor = &HFFC0C0
grdTranscodeRequests.StyleSets("pending").ForeColor = &H80000012
grdTranscodeRequests.StyleSets.Add "complete"
grdTranscodeRequests.StyleSets("complete").BackColor = &HFF00&
grdTranscodeRequests.StyleSets("complete").ForeColor = &H80000012
grdTranscodeRequests.StyleSets.Add "working"
grdTranscodeRequests.StyleSets("working").BackColor = &H80FFFF
grdTranscodeRequests.StyleSets("working").ForeColor = &H80000012
grdTranscodeRequests.StyleSets.Add "Error"
grdTranscodeRequests.StyleSets("Error").BackColor = &HA0A0FF
grdTranscodeRequests.StyleSets("Error").ForeColor = &H80000012
grdTranscodeRequests.StyleSets.Add "Nothing"
grdTranscodeRequests.StyleSets("Nothing").BackColor = &HFFFFFF
grdTranscodeRequests.StyleSets("Nothing").ForeColor = &H80000012

grdTranscodeRequestsHybrid.StyleSets.Add "pending"
grdTranscodeRequestsHybrid.StyleSets("pending").BackColor = &HFFC0C0
grdTranscodeRequestsHybrid.StyleSets("pending").ForeColor = &H80000012
grdTranscodeRequestsHybrid.StyleSets.Add "complete"
grdTranscodeRequestsHybrid.StyleSets("complete").BackColor = &HFF00&
grdTranscodeRequestsHybrid.StyleSets("complete").ForeColor = &H80000012
grdTranscodeRequestsHybrid.StyleSets.Add "working"
grdTranscodeRequestsHybrid.StyleSets("working").BackColor = &H80FFFF
grdTranscodeRequestsHybrid.StyleSets("working").ForeColor = &H80000012
grdTranscodeRequestsHybrid.StyleSets.Add "Error"
grdTranscodeRequestsHybrid.StyleSets("Error").BackColor = &HA0A0FF
grdTranscodeRequestsHybrid.StyleSets("Error").ForeColor = &H80000012
grdTranscodeRequestsHybrid.StyleSets.Add "Nothing"
grdTranscodeRequestsHybrid.StyleSets("Nothing").BackColor = &HFFFFFF
grdTranscodeRequestsHybrid.StyleSets("Nothing").ForeColor = &H80000012

grdFileRequests.StyleSets.Add "pending"
grdFileRequests.StyleSets("pending").BackColor = &HFFA5FF
grdFileRequests.StyleSets("pending").ForeColor = &H80000012
grdFileRequests.StyleSets.Add "complete"
grdFileRequests.StyleSets("complete").BackColor = &HFF00&
grdFileRequests.StyleSets("complete").ForeColor = &H80000012
grdFileRequests.StyleSets.Add "working"
grdFileRequests.StyleSets("working").BackColor = &H80FFFF
grdFileRequests.StyleSets("working").ForeColor = &H80000012
grdFileRequests.StyleSets.Add "Error"
grdFileRequests.StyleSets("Error").BackColor = &HA0A0FF
grdFileRequests.StyleSets("Error").ForeColor = &H80000012
grdFileRequests.StyleSets.Add "Nothing"
grdFileRequests.StyleSets("Nothing").BackColor = &HFFFFFF
grdFileRequests.StyleSets("Nothing").ForeColor = &H80000012
grdFileRequests.StyleSets.Add "Deleted"
grdFileRequests.StyleSets("Deleted").BackColor = &HFFE0B0
grdFileRequests.StyleSets("Deleted").ForeColor = &H80000012

grdFileRequestsHybrid.StyleSets.Add "pending"
grdFileRequestsHybrid.StyleSets("pending").BackColor = &HFFA5FF
grdFileRequestsHybrid.StyleSets("pending").ForeColor = &H80000012
grdFileRequestsHybrid.StyleSets.Add "complete"
grdFileRequestsHybrid.StyleSets("complete").BackColor = &HFF00&
grdFileRequestsHybrid.StyleSets("complete").ForeColor = &H80000012
grdFileRequestsHybrid.StyleSets.Add "working"
grdFileRequestsHybrid.StyleSets("working").BackColor = &H80FFFF
grdFileRequestsHybrid.StyleSets("working").ForeColor = &H80000012
grdFileRequestsHybrid.StyleSets.Add "Error"
grdFileRequestsHybrid.StyleSets("Error").BackColor = &HA0A0FF
grdFileRequestsHybrid.StyleSets("Error").ForeColor = &H80000012
grdFileRequestsHybrid.StyleSets.Add "Nothing"
grdFileRequestsHybrid.StyleSets("Nothing").BackColor = &HFFFFFF
grdFileRequestsHybrid.StyleSets("Nothing").ForeColor = &H80000012
grdFileRequestsHybrid.StyleSets.Add "Deleted"
grdFileRequestsHybrid.StyleSets("Deleted").BackColor = &HFFE0B0
grdFileRequestsHybrid.StyleSets("Deleted").ForeColor = &H80000012

grdExcelRequests.StyleSets.Add "complete"
grdExcelRequests.StyleSets("complete").BackColor = &HFF00&
grdExcelRequests.StyleSets("complete").ForeColor = &H80000012
grdExcelRequests.StyleSets.Add "Error"
grdExcelRequests.StyleSets("Error").BackColor = &HA0A0FF
grdExcelRequests.StyleSets("Error").ForeColor = &H80000012

PopulateCombo "transcodesystem", cmbTranscodeSystem
PopulateCombo "transcodesystem", cmbSearchTranscodeSystem

adoStatus.ConnectionString = g_strConnection
adoStatus.RecordSource = "SELECT transcodestatusID, description FROM transcodestatus ORDER BY transcodestatusID;"
adoStatus.Refresh
grdTranscodeRequests.Columns("statustext").DropDownHwnd = ddnStatus.hWnd
grdTranscodeRequestsHybrid.Columns("statustext").DropDownHwnd = ddnStatusHybrid.hWnd

g_intTranscodeGridClick = 0

tabSpecs.TabVisible(3) = False

If CheckAccess("/superuser", True) = True Then
    grdFileRequests.Columns("RequestCleanedUp").Visible = True
End If

If CheckAccess("/WorkflowAdmin", True) = True Then
    fraWorkflowAdmin.Visible = True
Else
    fraWorkflowAdmin.Visible = False
End If

If Me.Width <= 28980 Then
    optWidth(0).Value = True
    optWidth_Click 0
Else
    optWidth(1).Value = True
    optWidth_Click 1
End If
cmdSearch.Value = True
 
End Sub

Private Sub Form_Resize()

On Error Resume Next

frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120

tabSpecs.Height = Me.ScaleHeight - tabSpecs.Top - frmButtons.Height - 240
tabSpecs.Width = Me.ScaleWidth - tabSpecs.Left - 120
grdTranscodeRequests.Width = tabSpecs.Width - 240
grdTranscodeRequestsHybrid.Width = tabSpecs.Width / 2 - 240 - 600
grdFileRequests.Width = grdTranscodeRequests.Width
grdFileRequestsHybrid.Width = grdTranscodeRequestsHybrid.Width + 1200
grdFileRequestsHybrid.Left = grdTranscodeRequestsHybrid.Left + grdTranscodeRequestsHybrid.Width + 120
adoFileRequestsHybrid.Left = grdFileRequestsHybrid.Left
grdExcelRequests.Width = grdTranscodeRequests.Width
grdTranscodeSpecs.Height = tabSpecs.Height - grdTranscodeSpecs.Top - 120
grdTranscodeRequests.Height = tabSpecs.Height - grdTranscodeRequests.Top - 120
grdFileRequests.Height = tabSpecs.Height - grdFileRequests.Top - 120
grdTranscodeRequestsHybrid.Height = tabSpecs.Height - grdTranscodeRequestsHybrid.Top - 120
grdFileRequestsHybrid.Height = tabSpecs.Height - grdFileRequestsHybrid.Top - 120
grdExcelRequests.Height = tabSpecs.Height - grdExcelRequests.Top - 120
DoEvents
grdWorkflows.Width = grdTranscodeRequests.Width
grdWorkflows.Height = (tabSpecs.Height - grdWorkflows.Top) / 2 - 120
txtWorkflowSynopsis.Height = grdWorkflows.Height
txtWorkflowSynopsis.Width = grdWorkflows.Width / 2
txtWorkflowSynopsis.Top = grdWorkflows.Top + grdWorkflows.Height + 60
grdInstanceVariables.Top = txtWorkflowSynopsis.Top
grdInstanceVariables.Width = txtWorkflowSynopsis.Width * 0.75
grdInstanceVariables.Height = txtWorkflowSynopsis.Height
grdInstanceVariables.Left = txtWorkflowSynopsis.Left + txtWorkflowSynopsis.Width + 60
fraWorkflowAdmin.Top = txtWorkflowSynopsis.Top
fraWorkflowAdmin.Left = grdInstanceVariables.Left + grdInstanceVariables.Width + 60
fraWorkflowAdmin.Width = tabSpecs.Width - txtWorkflowSynopsis.Width - grdInstanceVariables.Width - 360
fraWorkflowAdmin.Height = grdInstanceVariables.Height
grdTaskTypeDefaultVariables.Height = (tabSpecs.Height - grdTaskTypeDefaultVariables.Top) / 3 - 120
grdWorkflowTaskDefaultVariables.Height = grdTaskTypeDefaultVariables.Height
grdWorkflowTaskType.Height = grdTaskTypeDefaultVariables.Height
grdWorkflowDefinitions.Height = grdTaskTypeDefaultVariables.Height
grdWorkflowTasks.Height = grdTaskTypeDefaultVariables.Height
grdWorkflowTasks.Top = grdWorkflowDefinitions.Top + grdWorkflowDefinitions.Height + 120
grdWorkflowTaskDefaultVariables.Top = grdWorkflowTasks.Top
grdWorkflowVariants.Height = grdTaskTypeDefaultVariables.Height
grdWorkflowVariants.Top = grdWorkflowTasks.Top + grdWorkflowTasks.Height + 120
grdWorkflowTaskVariables.Height = grdTaskTypeDefaultVariables.Height / 2 - 60
grdWorkflowTaskVariables.Top = grdWorkflowVariants.Top
grdMissingVariables.Height = grdTaskTypeDefaultVariables.Height / 2 - 60
grdMissingVariables.Top = grdWorkflowTaskVariables.Top + grdWorkflowTaskVariables.Height + 120
fraSynopsis.Height = Me.ScaleHeight / 5 * 4
txtWorkflowConfigurationSynopsis.Height = fraSynopsis.Height - cmdHideSynopsis.Height - 480
cmdHideSynopsis.Top = txtWorkflowConfigurationSynopsis.Top + txtWorkflowConfigurationSynopsis.Height + 120

End Sub

Private Sub grdExcelRequests_RowLoaded(ByVal Bookmark As Variant)

If grdExcelRequests.Columns("RequestComplete").Text <> "" Then
    grdExcelRequests.Columns("RequestComplete").CellStyleSet "complete"
ElseIf grdExcelRequests.Columns("RequestFailed").Text <> "" Then
    grdExcelRequests.Columns("RequestFailed").CellStyleSet "error"
End If

End Sub

Private Sub grdFileRequests_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

Dim l_strSQL As String, l_lngCount As Long, bkmrk As Variant

If MsgBox("You are about to delete " & grdFileRequests.SelBookmarks.Count & " lines.", vbYesNo, "Are you Sure?") = vbNo Then
    Cancel = 1
    Exit Sub
End If

For l_lngCount = 0 To grdFileRequests.SelBookmarks.Count - 1

    bkmrk = grdFileRequests.SelBookmarks(l_lngCount)
    l_strSQL = "DELETE FROM event_file_request WHERE event_file_requestID = " & Val(grdFileRequests.Columns("RequestID").CellText(bkmrk)) & ";"
    Debug.Print l_strSQL

    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError

Next

Cancel = 1
cmdSearch.Value = True

End Sub

Private Sub grdFileRequests_BeforeUpdate(Cancel As Integer)

If grdFileRequests.Columns("Urgent").Text <> adoFileRequests.Recordset("urgent") Then
    If grdFileRequests.Columns("urgent").Text <> 0 Then
        If Not CheckAccess("/setrequeststourgent") Then
            Cancel = 1
        End If
    End If
End If

If grdFileRequests.Columns("Hide").Value <> 0 Then
    grdFileRequests.Columns("HideFromGrid").Text = 1
Else
    grdFileRequests.Columns("HideFromGrid").Text = 0
End If

If grdFileRequests.Columns("New Folder").Text <> adoFileRequests.Recordset("New Folder") Then
    SetData "event_file_request", "NewAltLocation", "event_file_requestID", grdFileRequests.Columns("requestID").Text, grdFileRequests.Columns("New Folder").Text
End If

'If grdFileRequests.Columns("eventID").Text <> adoFileRequests.Recordset("eventID") Then
'    If Val(grdFileRequests.Columns("eventID").Text) <> 0 Then
'        SetData "event_file_request", "eventID", "event_file_requestID", grdFileRequests.Columns("event_file_requestID").Text, grdFileRequests.Columns("eventID").Text
'        Cancel = 1
'        adoFileRequests.Recordset.Requery
'    End If
'End If

End Sub

Private Sub grdFileRequests_DblClick()

If g_intFileRequestGridClick <> 0 Then Exit Sub
g_intFileRequestGridClick = 1
On Error GoTo NOCOLUMN
Select Case LCase(grdFileRequests.Columns(grdFileRequests.Col).Name)

Case "jobid"
    If Val(grdFileRequests.Columns("jobID").Text) <> 0 Then
        ShowJob Val(grdFileRequests.Columns("JobID").Text), 0, True
'        ShowJob Val(grdFileRequests.Columns("JobID").Text), 0, True
        g_intFileRequestGridClick = 0
    End If

Case "new folder", "newfilename"
    If grdFileRequests.Columns("NewFileID").Text <> "" Then
        ShowClipControl grdFileRequests.Columns("NewFileID").Text
    Else
        MsgBox "New File ID not available"
    End If

Case Else
SHOWFILE:
    If Val(grdFileRequests.Columns("Source File").Text) <> 0 Then ShowClipControl Val(grdFileRequests.Columns("Source File").Text)
End Select

g_intFileRequestGridClick = 0

Exit Sub

NOCOLUMN:
Resume SHOWFILE

End Sub

Private Sub grdFileRequests_RowLoaded(ByVal Bookmark As Variant)

If grdFileRequests.Columns("Request Completed").Text <> "" Then
    grdFileRequests.Columns("Request Completed").CellStyleSet "complete"
    grdFileRequests.Columns("RequestID").CellStyleSet "complete"
ElseIf grdFileRequests.Columns("Request Failed").Text <> "" Then
    grdFileRequests.Columns("Request Failed").CellStyleSet "error"
    grdFileRequests.Columns("RequestID").CellStyleSet "error"
ElseIf grdFileRequests.Columns("Request Completed").Text = "" And grdFileRequests.Columns("Request Failed").Text = "" And grdFileRequests.Columns("Request Started").Text <> "" Then
    grdFileRequests.Columns("Request Started").CellStyleSet "working"
    grdFileRequests.Columns("RequestID").CellStyleSet "working"
End If

If grdFileRequests.Columns("RequestPaused").Text <> "" Then
    grdFileRequests.Columns("Paused").Value = 1
Else
    grdFileRequests.Columns("Paused").Value = 0
End If

If grdFileRequests.Columns("HideFromGrid").Text <> "False" Then
    grdFileRequests.Columns("Hide").Value = 1
Else
    grdFileRequests.Columns("Hide").Value = 0
End If

If GetData("events", "system_deleted", "eventID", grdFileRequests.Columns("Source File").Text) <> 0 Then
    grdFileRequests.Columns("Source File").CellStyleSet "Deleted"
    grdFileRequests.Columns("Clipreference").CellStyleSet "Deleted"
End If

End Sub

Private Sub grdFileRequestsHybrid_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

Dim l_strSQL As String, l_lngCount As Long, bkmrk As Variant

If MsgBox("You are about to delete " & grdFileRequestsHybrid.SelBookmarks.Count & " lines.", vbYesNo, "Are you Sure?") = vbNo Then
    Cancel = 1
    Exit Sub
End If

For l_lngCount = 0 To grdFileRequestsHybrid.SelBookmarks.Count - 1

    bkmrk = grdFileRequestsHybrid.SelBookmarks(l_lngCount)
    l_strSQL = "DELETE FROM event_file_request WHERE event_file_requestID = " & Val(grdFileRequestsHybrid.Columns("RequestID").CellText(bkmrk)) & ";"
    Debug.Print l_strSQL

    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError

Next

Cancel = 1
cmdSearch.Value = True

End Sub

Private Sub grdFileRequestsHybrid_BeforeUpdate(Cancel As Integer)

If grdFileRequestsHybrid.Columns("Urgent").Text <> adoFileRequestsHybrid.Recordset("urgent") Then
    If grdFileRequestsHybrid.Columns("urgent").Text <> 0 Then
        If Not CheckAccess("/setrequeststourgent") Then
            Cancel = 1
        End If
    End If
End If

If grdFileRequestsHybrid.Columns("Hide").Value <> 0 Then
    grdFileRequestsHybrid.Columns("HideFromGrid").Text = 1
Else
    grdFileRequestsHybrid.Columns("HideFromGrid").Text = 0
End If
'
End Sub

Private Sub grdFileRequestsHybrid_DblClick()

If g_intFileRequestGridClick <> 0 Then Exit Sub
g_intFileRequestGridClick = 1
On Error GoTo NOCOLUMNHYBRID
Select Case LCase(grdFileRequestsHybrid.Columns(grdFileRequestsHybrid.Col).Name)

Case "jobid"
    If Val(grdFileRequestsHybrid.Columns("jobID").Text) <> 0 Then
        ShowJob Val(grdFileRequestsHybrid.Columns("JobID").Text), 0, True
'        ShowJob Val(grdFileRequests.Columns("JobID").Text), 0, True
        g_intFileRequestGridClick = 0
    End If

Case "new folder", "newfilename"
    If grdFileRequestsHybrid.Columns("NewFileID").Text <> "" Then
        ShowClipControl grdFileRequestsHybrid.Columns("NewFileID").Text
    Else
        MsgBox "New File ID not available"
    End If

Case Else
SHOWFILEHYBRID:
    If Val(grdFileRequestsHybrid.Columns("Source File").Text) <> 0 Then ShowClipControl Val(grdFileRequestsHybrid.Columns("Source File").Text)
End Select

g_intFileRequestGridClick = 0

Exit Sub

NOCOLUMNHYBRID:
Resume SHOWFILEHYBRID

End Sub

Private Sub grdFileRequestsHybrid_RowLoaded(ByVal Bookmark As Variant)

If grdFileRequestsHybrid.Columns("Request Completed").Text <> "" Then
    grdFileRequestsHybrid.Columns("Request Completed").CellStyleSet "complete"
    grdFileRequestsHybrid.Columns("RequestID").CellStyleSet "complete"
ElseIf grdFileRequestsHybrid.Columns("Request Failed").Text <> "" Then
    grdFileRequestsHybrid.Columns("Request Failed").CellStyleSet "error"
    grdFileRequestsHybrid.Columns("RequestID").CellStyleSet "error"
ElseIf grdFileRequestsHybrid.Columns("Request Completed").Text = "" And grdFileRequestsHybrid.Columns("Request Failed").Text = "" And grdFileRequestsHybrid.Columns("Request Started").Text <> "" Then
    grdFileRequestsHybrid.Columns("Request Started").CellStyleSet "working"
    grdFileRequestsHybrid.Columns("RequestID").CellStyleSet "working"
End If

If grdFileRequestsHybrid.Columns("RequestPaused").Text <> "" Then
    grdFileRequestsHybrid.Columns("Paused").Value = 1
Else
    grdFileRequestsHybrid.Columns("Paused").Value = 0
End If

If grdFileRequestsHybrid.Columns("HideFromGrid").Text <> "False" Then
    grdFileRequestsHybrid.Columns("Hide").Value = 1
Else
    grdFileRequestsHybrid.Columns("Hide").Value = 0
End If

If GetData("events", "system_deleted", "eventID", grdFileRequestsHybrid.Columns("Source File").Text) <> 0 Then
    grdFileRequestsHybrid.Columns("Source File").CellStyleSet "Deleted"
    grdFileRequestsHybrid.Columns("Clipreference").CellStyleSet "Deleted"
End If

End Sub

Private Sub grdTaskTypeDefaultVariables_RowLoaded(ByVal Bookmark As Variant)

grdTaskTypeDefaultVariables.Columns("TaskType").Text = GetData("Workflow_TaskType", "TaskType", "TaskTypeID", grdTaskTypeDefaultVariables.Columns("TaskTypeID").Text)

End Sub

Private Sub grdTranscodeRequests_AfterUpdate(RtnDispErrMsg As Integer)
MsgBox "After Update"
End Sub

Private Sub grdTranscodeRequests_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

Dim l_strSQL As String, l_lngCount As Long, bkmrk As Variant

If MsgBox("You are about to delete " & grdTranscodeRequests.SelBookmarks.Count & " lines.", vbYesNo, "Are you Sure?") = vbNo Then
    Cancel = 1
    Exit Sub
End If

For l_lngCount = 0 To grdTranscodeRequests.SelBookmarks.Count - 1

    bkmrk = grdTranscodeRequests.SelBookmarks(l_lngCount)
    l_strSQL = "DELETE FROM transcoderequest WHERE transcoderequestID = " & Val(grdTranscodeRequests.Columns("transcoderequestID").CellText(bkmrk)) & ";"
    Debug.Print l_strSQL

    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError

Next

Cancel = 1
cmdSearch.Value = True

End Sub

Private Sub grdTranscodeRequests_BeforeUpdate(Cancel As Integer)

If m_blnDelete = True Then Exit Sub

Dim Bookmark As Variant

'Manually do the updates to TranscodeSpecification, SourceClipID, Overwriteifexisting and Status using Setdata statements and then cancel the grid update

If grdTranscodeRequests.Columns("transcodespecID").Text <> adoTranscodeRequests.Recordset("transcodespecID") Then
    SetData "transcoderequest", "transcodespecID", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, grdTranscodeRequests.Columns("transcodespecID").Text
    SetData "transcoderequest", "transcodesystem", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, grdTranscodeRequests.Columns("transcodesystem").Text
    If grdTranscodeRequests.Columns("sourceclipID").Text <> adoTranscodeRequests.Recordset("sourceclipID") Then SetData "transcoderequest", "sourceclipID", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, grdTranscodeRequests.Columns("sourceclipID").Text
    If grdTranscodeRequests.Columns("hidefromgrid").Text <> adoTranscodeRequests.Recordset("hidefromgrid") Then SetData "transcoderequest", "hidefromgrid", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, grdTranscodeRequests.Columns("hidefromgrid").Text
    If grdTranscodeRequests.Columns("overwriteifexisting").Text <> adoTranscodeRequests.Recordset("overwriteifexisting") Then SetData "transcoderequest", "overwriteifexisting", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, grdTranscodeRequests.Columns("overwriteifexisting").Text
    If grdTranscodeRequests.Columns("status").Text <> adoTranscodeRequests.Recordset("status") Then
        SetData "transcoderequest", "status", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, grdTranscodeRequests.Columns("status").Text
        SetData "transcoderequest", "readyforcompletion", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, "Null"
        SetData "transcoderequest", "readyforfailure", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, "Null"
    End If
    If grdTranscodeRequests.Columns("timecodestart").Text <> adoTranscodeRequests.Recordset("timecodestart") Then SetData "transcoderequest", "timecodestart", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, grdTranscodeRequests.Columns("timecodestart").Text
    If grdTranscodeRequests.Columns("timecodestop").Text <> adoTranscodeRequests.Recordset("timecodestop") Then SetData "transcoderequest", "timecodestop", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, grdTranscodeRequests.Columns("timecodestop").Text
    If Format(grdTranscodeRequests.Columns("Date Submitted").Text, "YYYY-MM-DD HH:NN:SS") <> Format(adoTranscodeRequests.Recordset("savedate"), "YYYY-MM-DD HH:NN:SS") Then SetData "transcoderequest", "savedate", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, Format(grdTranscodeRequests.Columns("Date Submitted").Text, "YYYY-MM-DD HH:NN:SS")
    
Else
    If grdTranscodeRequests.Columns("transcodesystem").Text <> adoTranscodeRequests.Recordset("transcodesystem") Then SetData "transcoderequest", "transcodesystem", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, grdTranscodeRequests.Columns("transcodesystem").Text
    If grdTranscodeRequests.Columns("sourceclipID").Text <> adoTranscodeRequests.Recordset("sourceclipID") Then SetData "transcoderequest", "sourceclipID", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, grdTranscodeRequests.Columns("sourceclipID").Text
    If grdTranscodeRequests.Columns("hidefromgrid").Text <> adoTranscodeRequests.Recordset("hidefromgrid") Then SetData "transcoderequest", "hidefromgrid", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, grdTranscodeRequests.Columns("hidefromgrid").Text
    If grdTranscodeRequests.Columns("overwriteifexisting").Text <> adoTranscodeRequests.Recordset("overwriteifexisting") Then SetData "transcoderequest", "overwriteifexisting", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, grdTranscodeRequests.Columns("overwriteifexisting").Text
    If grdTranscodeRequests.Columns("status").Text <> adoTranscodeRequests.Recordset("status") Then
        SetData "transcoderequest", "status", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, grdTranscodeRequests.Columns("status").Text
        SetData "transcoderequest", "readyforcompletion", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, "Null"
        SetData "transcoderequest", "readyforfailure", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, "Null"
    End If
    If grdTranscodeRequests.Columns("timecodestart").Text <> adoTranscodeRequests.Recordset("timecodestart") Then SetData "transcoderequest", "timecodestart", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, grdTranscodeRequests.Columns("timecodestart").Text
    If grdTranscodeRequests.Columns("timecodestop").Text <> adoTranscodeRequests.Recordset("timecodestop") Then SetData "transcoderequest", "timecodestop", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, grdTranscodeRequests.Columns("timecodestop").Text
    If Format(grdTranscodeRequests.Columns("Date Submitted").Text, "YYYY-MM-DD HH:NN:SS") <> Format(adoTranscodeRequests.Recordset("savedate"), "YYYY-MM-DD HH:NN:SS") Then SetData "transcoderequest", "savedate", "transcoderequestID", grdTranscodeRequests.Columns("transcoderequestID").Text, Format(grdTranscodeRequests.Columns("Date Submitted").Text, "YYYY-MM-DD HH:NN:SS")
End If

Cancel = 1
On Error Resume Next
Bookmark = adoTranscodeRequests.Recordset.Bookmark
cmdSearch.Value = True
adoTranscodeRequests.Recordset.Bookmark = Bookmark
On Error GoTo 0

End Sub

Private Sub grdTranscodeRequests_BtnClick()

If Val(grdTranscodeRequests.Columns("sourceclipID").Text) <> 0 Then ShowClipControl grdTranscodeRequests.Columns("sourceclipID").Text

End Sub

Private Sub grdTranscodeRequests_DblClick()

If g_intTranscodeGridClick <> 0 Then Exit Sub
g_intTranscodeGridClick = 1
If Val(grdTranscodeRequests.Columns("sourceclipID").Text) <> 0 Then ShowClipControl Val(grdTranscodeRequests.Columns("sourceclipID").Text)

End Sub

Private Sub grdTranscodeRequests_KeyPress(KeyAscii As Integer)

On Error GoTo KEYPRESS_ERROR
If grdTranscodeRequests.Columns(grdTranscodeRequests.Col).Caption = "TimecodeStart" Or grdTranscodeRequests.Columns(grdTranscodeRequests.Col).Caption = "TimecodeStop" Then
    Set_Local_Framerate grdTranscodeRequests.Columns("fps").Text
    If Len(grdTranscodeRequests.ActiveCell.Text) < 2 Then
        If m_Framerate = TC_29 Or m_Framerate = TC_59 Then
            grdTranscodeRequests.ActiveCell.Text = "00:00:00;00"
        Else
            grdTranscodeRequests.ActiveCell.Text = "00:00:00:00"
        End If
        grdTranscodeRequests.ActiveCell.SelStart = 0
        grdTranscodeRequests.ActiveCell.SelLength = 1
    Else
        Timecode_Check_Grid grdTranscodeRequests, KeyAscii, m_Framerate
    End If
End If

KEYPRESS_ERROR:

Exit Sub

End Sub

Private Sub grdTranscodeRequests_RowLoaded(ByVal Bookmark As Variant)

If grdTranscodeRequests.Columns("transcoderequestid").Text <> "" Then
'    grdTranscodeRequests.Columns("transcodespecname").Text = GetData("transcodespec", "transcodename", "transcodespecID", grdTranscodeRequests.Columns("transcodespecid").Text)
End If
If grdTranscodeRequests.Columns("status").Text <> "" Then
'    grdTranscodeRequests.Columns("statustext").Text = GetData("transcodestatus", "description", "transcodestatusID", Val(grdTranscodeRequests.Columns("status").Text))
    Select Case Val(grdTranscodeRequests.Columns("status").Text)
        Case m_lngRequested
            grdTranscodeRequests.Columns("statustext").CellStyleSet "pending"
            grdTranscodeRequests.Columns("transcoderequestID").CellStyleSet "pending"
        Case m_lngProcessing
            grdTranscodeRequests.Columns("statustext").CellStyleSet "working"
            grdTranscodeRequests.Columns("transcoderequestID").CellStyleSet "working"
        Case m_lngComplete
            grdTranscodeRequests.Columns("statustext").CellStyleSet "complete"
            grdTranscodeRequests.Columns("transcoderequestID").CellStyleSet "complete"
        Case m_lngFinishingOff, m_lngOnHold
            grdTranscodeRequests.Columns("statustext").CellStyleSet "Nothing"
            grdTranscodeRequests.Columns("transcoderequestID").CellStyleSet "Nothing"
        Case Else
            grdTranscodeRequests.Columns("statustext").CellStyleSet "Error"
            grdTranscodeRequests.Columns("transcoderequestID").CellStyleSet "Error"
    End Select
        
End If

'grdTranscodeRequests.Columns("filename").Text = GetData("events", "clipfilename", "eventID", Val(grdTranscodeRequests.Columns("sourceclipID").Text))
grdTranscodeRequests.Columns("Transcode Engine").Text = grdTranscodeRequests.Columns("Transcode_Engine").Text & IIf(grdTranscodeRequests.Columns("Transcode_Stream").Text <> "", " (" & grdTranscodeRequests.Columns("Transcode_Stream").Text & ")", "")

If grdTranscodeRequests.Columns("name").Text <> "" Then
    grdTranscodeRequests.Columns("owner").Text = grdTranscodeRequests.Columns("name").Text
Else
    grdTranscodeRequests.Columns("owner").Text = grdTranscodeRequests.Columns("cetauser").Text
End If

End Sub

Private Sub grdTranscodeRequestsHybrid_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

Dim l_strSQL As String, l_lngCount As Long, bkmrk As Variant

If MsgBox("You are about to delete " & grdTranscodeRequestsHybrid.SelBookmarks.Count & " lines.", vbYesNo, "Are you Sure?") = vbNo Then
    Cancel = 1
    Exit Sub
End If

For l_lngCount = 0 To grdTranscodeRequestsHybrid.SelBookmarks.Count - 1

    bkmrk = grdTranscodeRequestsHybrid.SelBookmarks(l_lngCount)
    l_strSQL = "DELETE FROM transcoderequest WHERE transcoderequestID = " & Val(grdTranscodeRequestsHybrid.Columns("transcoderequestID").CellText(bkmrk)) & ";"
    Debug.Print l_strSQL

    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError

Next

Cancel = 1
cmdSearch.Value = True

End Sub

Private Sub grdTranscodeRequestsHybrid_BeforeUpdate(Cancel As Integer)

If m_blnDelete = True Then Exit Sub

Dim Bookmark As Variant

'Manually do the updates to TranscodeSpecification, SourceClipID, Overwriteifexisting and Status using Setdata statements and then cancel the grid update

If grdTranscodeRequestsHybrid.Columns("transcodespecID").Text <> adoTranscodeRequestsHybrid.Recordset("transcodespecID") Then
    SetData "transcoderequest", "transcodespecID", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, grdTranscodeRequestsHybrid.Columns("transcodespecID").Text
    SetData "transcoderequest", "transcodesystem", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, grdTranscodeRequestsHybrid.Columns("transcodesystem").Text
    If grdTranscodeRequestsHybrid.Columns("sourceclipID").Text <> adoTranscodeRequestsHybrid.Recordset("sourceclipID") Then SetData "transcoderequest", "sourceclipID", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, grdTranscodeRequestsHybrid.Columns("sourceclipID").Text
    If grdTranscodeRequestsHybrid.Columns("hidefromgrid").Text <> adoTranscodeRequestsHybrid.Recordset("hidefromgrid") Then SetData "transcoderequest", "hidefromgrid", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, grdTranscodeRequestsHybrid.Columns("hidefromgrid").Text
    If grdTranscodeRequestsHybrid.Columns("overwriteifexisting").Text <> adoTranscodeRequestsHybrid.Recordset("overwriteifexisting") Then SetData "transcoderequest", "overwriteifexisting", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, grdTranscodeRequestsHybrid.Columns("overwriteifexisting").Text
    If grdTranscodeRequestsHybrid.Columns("status").Text <> adoTranscodeRequestsHybrid.Recordset("status") Then
        SetData "transcoderequest", "status", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, grdTranscodeRequestsHybrid.Columns("status").Text
        SetData "transcoderequest", "readyforcompletion", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, "Null"
        SetData "transcoderequest", "readyforfailure", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, "Null"
    End If
    If grdTranscodeRequestsHybrid.Columns("timecodestart").Text <> adoTranscodeRequestsHybrid.Recordset("timecodestart") Then SetData "transcoderequest", "timecodestart", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, grdTranscodeRequestsHybrid.Columns("timecodestart").Text
    If grdTranscodeRequestsHybrid.Columns("timecodestop").Text <> adoTranscodeRequestsHybrid.Recordset("timecodestop") Then SetData "transcoderequest", "timecodestop", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, grdTranscodeRequestsHybrid.Columns("timecodestop").Text
    If Format(grdTranscodeRequestsHybrid.Columns("Date Submitted").Text, "YYYY-MM-DD HH:NN:SS") <> Format(adoTranscodeRequestsHybrid.Recordset("savedate"), "YYYY-MM-DD HH:NN:SS") Then SetData "transcoderequest", "savedate", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, Format(grdTranscodeRequestsHybrid.Columns("Date Submitted").Text, "YYYY-MM-DD HH:NN:SS")
Else
    If grdTranscodeRequestsHybrid.Columns("transcodesystem").Text <> adoTranscodeRequestsHybrid.Recordset("transcodesystem") Then SetData "transcoderequest", "transcodesystem", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, grdTranscodeRequestsHybrid.Columns("transcodesystem").Text
    If grdTranscodeRequestsHybrid.Columns("sourceclipID").Text <> adoTranscodeRequestsHybrid.Recordset("sourceclipID") Then SetData "transcoderequest", "sourceclipID", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, grdTranscodeRequestsHybrid.Columns("sourceclipID").Text
    If grdTranscodeRequestsHybrid.Columns("hidefromgrid").Text <> adoTranscodeRequestsHybrid.Recordset("hidefromgrid") Then SetData "transcoderequest", "hidefromgrid", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, grdTranscodeRequestsHybrid.Columns("hidefromgrid").Text
    If grdTranscodeRequestsHybrid.Columns("overwriteifexisting").Text <> adoTranscodeRequestsHybrid.Recordset("overwriteifexisting") Then SetData "transcoderequest", "overwriteifexisting", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, grdTranscodeRequestsHybrid.Columns("overwriteifexisting").Text
    If grdTranscodeRequestsHybrid.Columns("status").Text <> adoTranscodeRequestsHybrid.Recordset("status") Then
        SetData "transcoderequest", "status", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, grdTranscodeRequestsHybrid.Columns("status").Text
        SetData "transcoderequest", "readyforcompletion", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, "Null"
        SetData "transcoderequest", "readyforfailure", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, "Null"
    End If
    If grdTranscodeRequestsHybrid.Columns("timecodestart").Text <> adoTranscodeRequestsHybrid.Recordset("timecodestart") Then SetData "transcoderequest", "timecodestart", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, grdTranscodeRequestsHybrid.Columns("timecodestart").Text
    If grdTranscodeRequestsHybrid.Columns("timecodestop").Text <> adoTranscodeRequestsHybrid.Recordset("timecodestop") Then SetData "transcoderequest", "timecodestop", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, grdTranscodeRequestsHybrid.Columns("timecodestop").Text
    If Format(grdTranscodeRequestsHybrid.Columns("Date Submitted").Text, "YYYY-MM-DD HH:NN:SS") <> Format(adoTranscodeRequestsHybrid.Recordset("savedate"), "YYYY-MM-DD HH:NN:SS") Then SetData "transcoderequest", "savedate", "transcoderequestID", grdTranscodeRequestsHybrid.Columns("transcoderequestID").Text, Format(grdTranscodeRequestsHybrid.Columns("Date Submitted").Text, "YYYY-MM-DD HH:NN:SS")
End If

Cancel = 1
On Error Resume Next
Bookmark = adoTranscodeRequestsHybrid.Recordset.Bookmark
cmdSearch.Value = True
adoTranscodeRequestsHybrid.Recordset.Bookmark = Bookmark
On Error GoTo 0

End Sub

Private Sub grdTranscodeRequestsHybrid_DblClick()

If g_intTranscodeGridClick <> 0 Then Exit Sub
g_intTranscodeGridClick = 1
If Val(grdTranscodeRequestsHybrid.Columns("sourceclipID").Text) <> 0 Then ShowClipControl Val(grdTranscodeRequestsHybrid.Columns("sourceclipID").Text)

End Sub

Private Sub grdTranscodeRequestsHybrid_KeyPress(KeyAscii As Integer)

On Error GoTo KEYPRESS_ERROR_HYBRID
If grdTranscodeRequestsHybrid.Columns(grdTranscodeRequestsHybrid.Col).Caption = "TimecodeStart" Or grdTranscodeRequestsHybrid.Columns(grdTranscodeRequestsHybrid.Col).Caption = "TimecodeStop" Then
    Set_Local_Framerate grdTranscodeRequestsHybrid.Columns("fps").Text
    If Len(grdTranscodeRequestsHybrid.ActiveCell.Text) < 2 Then
        If m_Framerate = TC_29 Or m_Framerate = TC_59 Then
            grdTranscodeRequestsHybrid.ActiveCell.Text = "00:00:00;00"
        Else
            grdTranscodeRequestsHybrid.ActiveCell.Text = "00:00:00:00"
        End If
        grdTranscodeRequestsHybrid.ActiveCell.SelStart = 0
        grdTranscodeRequestsHybrid.ActiveCell.SelLength = 1
    Else
        Timecode_Check_Grid grdTranscodeRequestsHybrid, KeyAscii, m_Framerate
    End If
End If

KEYPRESS_ERROR_HYBRID:

Exit Sub

End Sub

Private Sub grdTranscodeRequestsHybrid_RowLoaded(ByVal Bookmark As Variant)

If grdTranscodeRequestsHybrid.Columns("transcoderequestid").Text <> "" Then
'    grdTranscodeRequests.Columns("transcodespecname").Text = GetData("transcodespec", "transcodename", "transcodespecID", grdTranscodeRequests.Columns("transcodespecid").Text)
End If
If grdTranscodeRequestsHybrid.Columns("status").Text <> "" Then
'    grdTranscodeRequests.Columns("statustext").Text = GetData("transcodestatus", "description", "transcodestatusID", Val(grdTranscodeRequests.Columns("status").Text))
    Select Case Val(grdTranscodeRequestsHybrid.Columns("status").Text)
        Case m_lngRequested
            grdTranscodeRequestsHybrid.Columns("statustext").CellStyleSet "pending"
            grdTranscodeRequestsHybrid.Columns("transcoderequestID").CellStyleSet "pending"
        Case m_lngProcessing
            grdTranscodeRequestsHybrid.Columns("statustext").CellStyleSet "working"
            grdTranscodeRequestsHybrid.Columns("transcoderequestID").CellStyleSet "working"
        Case m_lngComplete
            grdTranscodeRequestsHybrid.Columns("statustext").CellStyleSet "complete"
            grdTranscodeRequestsHybrid.Columns("transcoderequestID").CellStyleSet "complete"
        Case m_lngFinishingOff, m_lngOnHold
            grdTranscodeRequestsHybrid.Columns("statustext").CellStyleSet "Nothing"
            grdTranscodeRequestsHybrid.Columns("transcoderequestID").CellStyleSet "Nothing"
        Case Else
            grdTranscodeRequestsHybrid.Columns("statustext").CellStyleSet "Error"
            grdTranscodeRequestsHybrid.Columns("transcoderequestID").CellStyleSet "Error"
    End Select
        
End If

'grdTranscodeRequests.Columns("filename").Text = GetData("events", "clipfilename", "eventID", Val(grdTranscodeRequests.Columns("sourceclipID").Text))
grdTranscodeRequestsHybrid.Columns("Transcode Engine").Text = grdTranscodeRequestsHybrid.Columns("Transcode_Engine").Text & IIf(grdTranscodeRequestsHybrid.Columns("Transcode_Stream").Text <> "", " (" & grdTranscodeRequestsHybrid.Columns("Transcode_Stream").Text & ")", "")

If grdTranscodeRequestsHybrid.Columns("name").Text <> "" Then
    grdTranscodeRequestsHybrid.Columns("owner").Text = grdTranscodeRequestsHybrid.Columns("name").Text
Else
    grdTranscodeRequestsHybrid.Columns("owner").Text = grdTranscodeRequestsHybrid.Columns("cetauser").Text
End If

End Sub

Private Sub grdTranscodeSpecs_BeforeUpdate(Cancel As Integer)

grdTranscodeSpecs.Columns("forder").Text = 1

End Sub

Public Sub Set_Local_Framerate(lp_strFPS As String)

Select Case lp_strFPS

    Case "25"
        m_Framerate = TC_25
    Case "29.97"
        m_Framerate = TC_29
    Case "30"
        m_Framerate = TC_30
    Case "24", "23.98"
        m_Framerate = TC_24
    Case "50"
        m_Framerate = TC_50
    Case "60"
        m_Framerate = TC_60
    Case "59.94"
        m_Framerate = TC_59
    Case Else
        m_Framerate = TC_UN

End Select

End Sub

Private Sub RefreshTranscodeRequests()

Dim SQL As String, l_strFilter1 As String, l_strFilter2 As String, l_strFilter3 As String, l_strFilter4 As String, l_strFilter5 As String, l_strFilter6 As String, l_strFilter7 As String
Dim l_curCountRequestSize As Currency, l_strTempSQL As String, l_rstSize As ADODB.Recordset

adoTranscodeSpecs.ConnectionString = g_strConnection
If optShowTranscodeSpecs(0).Value = True Then
    adoTranscodeSpecs.RecordSource = "SELECT * FROM Transcodespec WHERE system_deleted = 0 AND enabled <> 0 ORDER BY case when companyID = 501 then 0 else 1 end, forder, transcodename"
Else
    adoTranscodeSpecs.RecordSource = "SELECT * FROM Transcodespec WHERE system_deleted = 0 ORDER BY case when companyID = 501 then 0 else 1 end, forder, transcodename"
End If
adoTranscodeSpecs.Refresh
grdTranscodeRequests.Columns("transcodespecname").DropDownHwnd = ddnTranscodeSpecs.hWnd

'grdtranscoderequests.Columns()

'SQL = "SELECT * FROM transcoderequest WHERE 1 = 1 "
If optShowRequests(0).Value = True Then
    SQL = "SELECT " & IIf(chkTop42.Value <> 0 And chkTop71.Value = 0, "Top 42 ", "") & IIf(chkTop71.Value <> 0 And chkTop42.Value = 0, "Top 71 ", "") & "* FROM vw_Ceta_TranscodeRequests_All WHERE 1 = 1 "
Else
    SQL = "SELECT " & IIf(chkTop42.Value <> 0 And chkTop71.Value = 0, "Top 42 ", "") & IIf(chkTop71.Value <> 0 And chkTop42.Value = 0, "Top 71 ", "") & "* FROM vw_Ceta_Transcoderequests WHERE 1 = 1 "
End If

If cmbSearchTranscodeSystem.Text <> "" Then
    SQL = SQL & "AND transcodesystem = '" & cmbSearchTranscodeSystem.Text & "' "
End If

If chkShowHiddenRequests.Value = 0 Then
    SQL = SQL & "AND hidefromgrid = 0 "
'Else
'    SQL = SQL & "AND hidefromgrid <> 0 "
End If

If txtSearchTranscodes.Text <> "" Then
    SQL = SQL & " AND segmentreference LIKE '" & txtSearchTranscodes.Text & "%' "
End If

If chkFilterComplete.Value <> 0 Then

    If optShowRequests(3).Value = True Then
        l_strFilter1 = "status = (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Completed') AND savedate > '" & FormatSQLDate(DateAdd("H", -24, Now)) & "' "
    ElseIf optShowRequests(2).Value = True Then
        l_strFilter1 = "status = (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Completed') AND savedate > '" & FormatSQLDate(DateAdd("H", -48, Now)) & "' "
    ElseIf optShowRequests(1).Value = True Then
        l_strFilter1 = "status = (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Completed') AND savedate > '" & FormatSQLDate(DateAdd("H", -120, Now)) & "' "
    ElseIf optShowRequests(0).Value = True Then
        l_strFilter1 = "status = (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Completed') "
    End If
Else
    l_strFilter1 = ""
End If

If chkFilterInProgress.Value <> 0 Then
    l_strFilter2 = "status = (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Processing') "
Else
    l_strFilter2 = ""
End If

If chkFilterCancelled.Value <> 0 Then
    l_strFilter3 = "status = (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Cancelled') "
Else
    l_strFilter3 = ""
End If

If chkFilterFailed.Value <> 0 Then
    l_strFilter4 = "(status <> (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Completed')" & _
    " AND status <> (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Processing')" & _
    " AND status <> (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Cancelled')" & _
    " AND status <> (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Finishing Off')" & _
    " AND status <> (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Requested')"
    If optShowRequests(3).Value = True Then
        l_strFilter4 = l_strFilter4 & " AND savedate > '" & FormatSQLDate(DateAdd("H", -24, Now)) & "' "
    ElseIf optShowRequests(2).Value = True Then
        l_strFilter4 = l_strFilter4 & " AND savedate > '" & FormatSQLDate(DateAdd("H", -48, Now)) & "' "
    ElseIf optShowRequests(1).Value = True Then
        l_strFilter4 = l_strFilter4 & " AND savedate > '" & FormatSQLDate(DateAdd("H", -120, Now)) & "' "
    End If
    l_strFilter4 = l_strFilter4 & ") "
Else
    l_strFilter4 = ""
End If

If chkFilterRequested.Value <> 0 Then
    l_strFilter5 = "status = (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Requested') "
Else
    l_strFilter5 = ""
End If

If chkFilterFinishingOff.Value <> 0 Then
    l_strFilter6 = "status = (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Finishing Off') "
Else
    l_strFilter6 = ""
End If

If l_strFilter1 <> "" Then SQL = SQL & "AND (" & l_strFilter1
If l_strFilter2 <> "" Then
    If l_strFilter1 <> "" Then
        SQL = SQL & "OR " & l_strFilter2
    Else
        SQL = SQL & "AND (" & l_strFilter2
    End If
End If
If l_strFilter3 <> "" Then
    If l_strFilter1 <> "" Or l_strFilter2 <> "" Then
        SQL = SQL & "OR " & l_strFilter3
    Else
        SQL = SQL & "AND (" & l_strFilter3
    End If
End If
If l_strFilter4 <> "" Then
    If l_strFilter1 <> "" Or l_strFilter2 <> "" Or l_strFilter3 <> "" Then
        SQL = SQL & "OR " & l_strFilter4
    Else
        SQL = SQL & "AND (" & l_strFilter4
    End If
End If
If l_strFilter5 <> "" Then
    If l_strFilter1 <> "" Or l_strFilter2 <> "" Or l_strFilter3 <> "" Or l_strFilter4 <> "" Then
        SQL = SQL & "OR " & l_strFilter5
    Else
        SQL = SQL & "AND (" & l_strFilter5
    End If
End If
If l_strFilter6 <> "" Then
    If l_strFilter1 <> "" Or l_strFilter2 <> "" Or l_strFilter3 <> "" Or l_strFilter4 <> "" Or l_strFilter5 <> "" Then
        SQL = SQL & "OR " & l_strFilter6
    Else
        SQL = SQL & "AND (" & l_strFilter6
    End If
End If

If l_strFilter1 <> "" Or l_strFilter2 <> "" Or l_strFilter3 <> "" Or l_strFilter4 <> "" Or l_strFilter5 <> "" Or l_strFilter6 <> "" Then
    SQL = SQL & ") "
End If

If txtSearchWorkflowInstance.Text <> "" Then
    SQL = SQL & " AND cetauser LIKE '%" & QuoteSanitise(txtSearchWorkflowInstance.Text) & "'"
End If

'SQL = SQL & "ORDER BY (CASE WHEN status = 2 THEN 0 WHEN status = 3 THEN 1 WHEN status = 6 THEN 2 WHEN status = 4 THEN 3 WHEN status = 5 THEN 4 WHEN status = 7 THEN 5 WHEN status = 9 THEN 6 WHEN status = 8 THEN 7 ELSE 8 END), savedate DESC;"
SQL = SQL & "ORDER BY status, savedate DESC;"

m_lngRequested = GetData("transcodestatus", "transcodestatusID", "description", "Requested")
m_lngProcessing = GetData("transcodestatus", "transcodestatusID", "description", "Processing")
m_lngComplete = GetData("transcodestatus", "transcodestatusID", "description", "Completed")
m_lngFinishingOff = GetData("transcodestatus", "transcodestatusID", "description", "Finishing Off")
m_lngOnHold = GetData("transcodestatus", "transcodestatusID", "description", "On Hold")

adoTranscodeRequests.ConnectionString = g_strConnection
adoTranscodeRequests.RecordSource = "SELECT * FROM vw_CETA_Transcoderequests WHERE 1 = 0 "
adoTranscodeRequests.Refresh
adoTranscodeRequests.RecordSource = SQL
adoTranscodeRequests.Refresh
adoTranscodeRequests.Caption = adoTranscodeRequests.Recordset.RecordCount & " items"

End Sub

Private Sub RefreshTranscodeRequestsHybrid()

Dim SQL As String, l_strFilter1 As String, l_strFilter2 As String, l_strFilter3 As String, l_strFilter4 As String, l_strFilter5 As String, l_strFilter6 As String, l_strFilter7 As String
Dim l_curCountRequestSize As Currency, l_strTempSQL As String, l_rstSize As ADODB.Recordset

adoTranscodeSpecsHybrid.ConnectionString = g_strConnection
If optShowTranscodeSpecs(0).Value = True Then
    adoTranscodeSpecsHybrid.RecordSource = "SELECT * FROM Transcodespec WHERE system_deleted = 0 AND enabled <> 0 ORDER BY case when companyID = 501 then 0 else 1 end, forder, transcodename"
Else
    adoTranscodeSpecsHybrid.RecordSource = "SELECT * FROM Transcodespec WHERE system_deleted = 0 ORDER BY case when companyID = 501 then 0 else 1 end, forder, transcodename"
End If
adoTranscodeSpecsHybrid.Refresh
grdTranscodeRequestsHybrid.Columns("transcodespecname").DropDownHwnd = ddnTranscodeSpecsHybrid.hWnd

'grdtranscoderequests.Columns()

'SQL = "SELECT * FROM transcoderequest WHERE 1 = 1 "
If optShowRequests(0).Value = True Then
    SQL = "SELECT " & IIf(chkTop42.Value <> 0 And chkTop71.Value = 0, "Top 42 ", "") & IIf(chkTop71.Value <> 0 And chkTop42.Value = 0, "Top 71 ", "") & "* FROM vw_Ceta_TranscodeRequests_All WHERE 1 = 1 "
Else
    SQL = "SELECT " & IIf(chkTop42.Value <> 0 And chkTop71.Value = 0, "Top 42 ", "") & IIf(chkTop71.Value <> 0 And chkTop42.Value = 0, "Top 71 ", "") & "* FROM vw_Ceta_Transcoderequests WHERE 1 = 1 "
End If

If cmbSearchTranscodeSystem.Text <> "" Then
    SQL = SQL & "AND transcodesystem = '" & cmbSearchTranscodeSystem.Text & "' "
End If

If chkShowHiddenRequests.Value = 0 Then
    SQL = SQL & "AND hidefromgrid = 0 "
'Else
'    SQL = SQL & "AND hidefromgrid <> 0 "
End If

If txtSearchTranscodes.Text <> "" Then
    SQL = SQL & " AND segmentreference LIKE '" & txtSearchTranscodes.Text & "%' "
End If

If optShowRequests(3).Value = True Then
    l_strFilter1 = "status = (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Completed') AND savedate > '" & FormatSQLDate(DateAdd("H", -24, Now)) & "' "
ElseIf optShowRequests(2).Value = True Then
    l_strFilter1 = "status = (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Completed') AND savedate > '" & FormatSQLDate(DateAdd("H", -48, Now)) & "' "
ElseIf optShowRequests(1).Value = True Then
    l_strFilter1 = "status = (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Completed') AND savedate > '" & FormatSQLDate(DateAdd("H", -120, Now)) & "' "
ElseIf optShowRequests(0).Value = True Then
    l_strFilter1 = "status = (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Completed') "
End If

l_strFilter2 = "status = (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Processing') "

l_strFilter3 = "status = (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Cancelled') "

l_strFilter4 = "(status <> (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Completed')" & _
" AND status <> (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Processing')" & _
" AND status <> (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Cancelled')" & _
" AND status <> (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Finishing Off')" & _
" AND status <> (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Requested')"
If optShowRequests(3).Value = True Then
    l_strFilter4 = l_strFilter4 & " AND savedate > '" & FormatSQLDate(DateAdd("H", -24, Now)) & "' "
ElseIf optShowRequests(2).Value = True Then
    l_strFilter4 = l_strFilter4 & " AND savedate > '" & FormatSQLDate(DateAdd("H", -48, Now)) & "' "
ElseIf optShowRequests(1).Value = True Then
    l_strFilter4 = l_strFilter4 & " AND savedate > '" & FormatSQLDate(DateAdd("H", -120, Now)) & "' "
End If
l_strFilter4 = l_strFilter4 & ") "

l_strFilter5 = "status = (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Requested') "

l_strFilter6 = "status = (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Finishing Off') "

If l_strFilter1 <> "" Then SQL = SQL & "AND (" & l_strFilter1
If l_strFilter2 <> "" Then
    If l_strFilter1 <> "" Then
        SQL = SQL & "OR " & l_strFilter2
    Else
        SQL = SQL & "AND (" & l_strFilter2
    End If
End If
If l_strFilter3 <> "" Then
    If l_strFilter1 <> "" Or l_strFilter2 <> "" Then
        SQL = SQL & "OR " & l_strFilter3
    Else
        SQL = SQL & "AND (" & l_strFilter3
    End If
End If
If l_strFilter4 <> "" Then
    If l_strFilter1 <> "" Or l_strFilter2 <> "" Or l_strFilter3 <> "" Then
        SQL = SQL & "OR " & l_strFilter4
    Else
        SQL = SQL & "AND (" & l_strFilter4
    End If
End If
If l_strFilter5 <> "" Then
    If l_strFilter1 <> "" Or l_strFilter2 <> "" Or l_strFilter3 <> "" Or l_strFilter4 <> "" Then
        SQL = SQL & "OR " & l_strFilter5
    Else
        SQL = SQL & "AND (" & l_strFilter5
    End If
End If
If l_strFilter6 <> "" Then
    If l_strFilter1 <> "" Or l_strFilter2 <> "" Or l_strFilter3 <> "" Or l_strFilter4 <> "" Or l_strFilter5 <> "" Then
        SQL = SQL & "OR " & l_strFilter6
    Else
        SQL = SQL & "AND (" & l_strFilter6
    End If
End If

If l_strFilter1 <> "" Or l_strFilter2 <> "" Or l_strFilter3 <> "" Or l_strFilter4 <> "" Or l_strFilter5 <> "" Or l_strFilter6 <> "" Then
    SQL = SQL & ") "
End If

If txtSearchWorkflowInstance.Text <> "" Then
    SQL = SQL & " AND cetauser LIKE '%" & QuoteSanitise(txtSearchWorkflowInstance.Text) & "'"
End If

'SQL = SQL & "ORDER BY (CASE WHEN status = 2 THEN 0 WHEN status = 3 THEN 1 WHEN status = 6 THEN 2 WHEN status = 4 THEN 3 WHEN status = 5 THEN 4 WHEN status = 7 THEN 5 WHEN status = 9 THEN 6 WHEN status = 8 THEN 7 ELSE 8 END), savedate DESC;"
SQL = SQL & "ORDER BY status, savedate DESC;"

m_lngRequested = GetData("transcodestatus", "transcodestatusID", "description", "Requested")
m_lngProcessing = GetData("transcodestatus", "transcodestatusID", "description", "Processing")
m_lngComplete = GetData("transcodestatus", "transcodestatusID", "description", "Completed")
m_lngFinishingOff = GetData("transcodestatus", "transcodestatusID", "description", "Finishing Off")
m_lngOnHold = GetData("transcodestatus", "transcodestatusID", "description", "On Hold")

adoTranscodeRequestsHybrid.ConnectionString = g_strConnection
adoTranscodeRequestsHybrid.RecordSource = "SELECT * FROM vw_CETA_Transcoderequests WHERE 1 = 0 "
adoTranscodeRequestsHybrid.Refresh
adoTranscodeRequestsHybrid.RecordSource = SQL
adoTranscodeRequestsHybrid.Refresh
adoTranscodeRequestsHybrid.Caption = adoTranscodeRequestsHybrid.Recordset.RecordCount & " items"

End Sub
Private Sub RefreshFileRequests()

Dim SQL As String, l_strFilter1 As String, l_strFilter2 As String, l_strFilter3 As String, l_strFilter4 As String, l_strFilter5 As String, l_strFilter6 As String
Dim l_datFilterDate As Date
Dim l_curCountRequestSize As Currency, l_strTempSQL As String, l_rstSize As ADODB.Recordset
Dim RequestCount As Long, rsCount As ADODB.Recordset

l_datFilterDate = DateAdd("d", 5, Now)

PopulateCombo "FileOperations", cmbOperationType

SQL = ""

If chkShowHiddenFileRequests.Value = 0 Then
    SQL = SQL & "AND hidefromgrid = 0 "
'Else
'    SQL = SQL & "AND hidefromgrid <> 0 "
End If

If txtSearchTranscodes.Text <> "" Then
    SQL = SQL & " AND clipreference LIKE '" & txtSearchTranscodes.Text & "%' "
End If
If optShowIncomplete.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NULL AND [Request Failed] IS NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Date] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Date] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowRunning.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NULL AND [Request Failed] IS NULL AND [Request Started] IS NOT NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Started] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Started] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowNotStarted.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NULL AND [Request Failed] IS NULL AND [Request Started] IS NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Date] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Date] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowComplete.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NOT NULL and [Request Failed] IS NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Completed] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Completed] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowFailed.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NULL and [Request Failed] IS NOT NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Failed] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Failed] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowAllButComplete.Value <> 0 Then
    SQL = SQL & " AND ([Request Completed] IS NULL OR ([Request Completed] IS NOT NULL AND [Request Failed] IS NOT NULL))"
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Date] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Date] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
Else
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Date] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Date] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
End If
If chkExcludeFarFuture.Value <> 0 Then
    SQL = SQL & " AND [Request Date] < '" & Format(l_datFilterDate, "yyyy-mm-dd") & "' "
End If
If chkExludeMediaInfo.Value <> 0 Then
    SQL = SQL & " AND [File Operation] <> 'MediaInfo + TC' "
End If
If chkJustMyStuff.Value <> 0 Then
    SQL = SQL & " AND ([Requestname] = '" & QuoteSanitise(g_strUserName) & "' OR [Requestname] = '" & QuoteSanitise(g_strUserInitials) & "') "
End If
'If chkExcludeAutoDelete.Value <> 0 Then
'    SQL = SQL & " AND [Event_file_Request_TypeID] <> 24 "
'End If
Select Case cmbOperationType.Text
    Case "All Items", ""
        'do nothing
    Case "All DIVA Archives"
        SQL = SQL & "AND [File Operation] IN ('Copy To 360', 'Move To 360', 'CETA To DIVA', 'CETA Move To DIVA') "
    Case "All DIVA Restores"
        SQL = SQL & "AND [File Operation] IN ('DIVA To CETA', 'Content Delivery From DIVA') "
    Case Else
        SQL = SQL & "AND [File Operation] = '" & cmbOperationType.Text & "'"
End Select
If txtSearchWorkflowInstance.Text <> "" Then
    SQL = SQL & " AND RequestName LIKE '%" & QuoteSanitise(txtSearchWorkflowInstance.Text) & "'"
End If
If txtSearchDeliveryNumber.Text <> "" Then
    SQL = SQL & " AND CDDeliveryNumber = " & Val(txtSearchDeliveryNumber.Text) & " "
End If
If txtSearchJobNumber.Text <> "" Then
    SQL = SQL & " AND JobID = " & Val(txtSearchJobNumber.Text) & " "
End If

If optShowMovementRequests(0).Value = True Then
    Set rsCount = ExecuteSQL("SELECT Count(*) FROM vw_Ceta_FileRequests WHERE 1=1 " & SQL, g_strExecuteError)
    RequestCount = rsCount(0)
    rsCount.Close
    SQL = "SELECT " & IIf(chkTop42.Value <> 0 And chkTop71.Value = 0, "Top 42 ", "") & IIf(chkTop71.Value <> 0 And chkTop42.Value = 0, "Top 71 ", "") & "* FROM vw_Ceta_FileRequests WHERE 1=1 " & SQL
Else
    Set rsCount = ExecuteSQL("SELECT Count(*) FROM vw_Ceta_FileRequests_all WHERE 1=1 " & SQL, g_strExecuteError)
    RequestCount = rsCount(0)
    rsCount.Close
    SQL = "SELECT " & IIf(chkTop42.Value <> 0 And chkTop71.Value = 0, "Top 42 ", "") & IIf(chkTop71.Value <> 0 And chkTop42.Value = 0, "Top 71 ", "") & "* FROM vw_Ceta_FileRequests_all WHERE 1=1 " & SQL
End If

If optShowIncomplete.Value <> 0 Then
    SQL = SQL & "ORDER BY case when [Request Completed] IS NULL and [Request Failed] is null and [request started] is not null then 0 when [request failed] is not null then 1 when [Request Started] IS Null then 2 else 3 END, case when Urgent <> 0 then 0 else 1 end, [Request Date] DESC, event_file_requestID DESC;"
End If
If optShowComplete.Value <> 0 Then
    SQL = SQL & "ORDER BY case when [Request Completed] IS NULL and [Request Failed] is null and [request started] is not null then 0 when [request failed] is not null then 1 when [Request Started] IS Null then 2 else 3 END, [Request Date] DESC, event_file_requestID DESC;"
End If
If optShowFailed.Value <> 0 Then
    SQL = SQL & "ORDER BY case when [Request Completed] IS NULL and [Request Failed] is null and [request started] is not null then 0 when [request failed] is not null then 1 when [Request Started] IS Null then 2 else 3 END, [Request Date] DESC, event_file_requestID DESC;"
End If
If optShowAll.Value <> 0 Or optShowAllButComplete.Value <> 0 Then
    SQL = SQL & "ORDER BY case when [Request Completed] IS NULL and [Request Failed] is null and [request started] is not null then 0 when [request failed] is not null then 1 when [Request Started] IS Null then 2 else 3 END, case when Urgent <> 0 then 0 else 1 end, [Request Date] DESC, event_file_requestID DESC;"
End If

Debug.Print SQL
adoFileRequests.ConnectionString = g_strConnection
adoFileRequests.RecordSource = SQL
adoFileRequests.Refresh

If optShowMovementRequests(0).Value = True Then
    SQL = "SELECT Sum(bigfilesize) FROM vw_Ceta_FileRequests WHERE 1=1 "
Else
    SQL = "SELECT Sum(bigfilesize) FROM vw_Ceta_FileRequests_all WHERE 1=1 "
End If
If chkShowHiddenFileRequests.Value = 0 Then
    SQL = SQL & "AND hidefromgrid = 0 "
'Else
'    SQL = SQL & "AND hidefromgrid <> 0 "
End If
If txtSearchTranscodes.Text <> "" Then
    SQL = SQL & " AND clipreference LIKE '" & txtSearchTranscodes.Text & "%' "
End If
If optShowIncomplete.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NULL AND [Request Failed] IS NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Date] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Date] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowRunning.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NULL AND [Request Failed] IS NULL AND [Request Started] IS NOT NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Started] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Started] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowNotStarted.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NULL AND [Request Failed] IS NULL AND [Request Started] IS NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Date] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Date] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowComplete.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NOT NULL and [Request Failed] IS NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Completed] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Completed] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowFailed.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NULL and [Request Failed] IS NOT NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Failed] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Failed] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowAllButComplete.Value <> 0 Then
    SQL = SQL & " AND ([Request Completed] IS NULL OR ([Request Started] IS NOT NULL AND [Request Failed] IS NOT NULL))"
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Date] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Date] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
Else
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Date] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Date] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
End If
If chkExcludeFarFuture.Value <> 0 Then
    SQL = SQL & " AND [Request Date] < '" & Format(l_datFilterDate, "yyyy-mm-dd") & "' "
End If
If chkJustMyStuff.Value <> 0 Then
    SQL = SQL & " AND ([Requestname] = '" & QuoteSanitise(g_strUserName) & "' OR [Requestname] = '" & QuoteSanitise(g_strUserInitials) & "') "
End If
'If chkExcludeAutoDelete.Value <> 0 Then
'    SQL = SQL & " AND [Event_file_Request_TypeID] <> 24 "
'End If
Select Case cmbOperationType.Text
    Case "All Items", ""
        'do nothing
    Case "All DIVA Archives"
        SQL = SQL & "AND [File Operation] IN ('Copy To 360', 'Move To 360', 'CETA To DIVA', 'CETA Move To DIVA') "
    Case "All DIVA Restores"
        SQL = SQL & "AND [File Operation] IN ('DIVA To CETA', 'Content Delivery From DIVA') "
    Case Else
        SQL = SQL & "AND [File Operation] = '" & cmbOperationType.Text & "'"
End Select
If txtSearchWorkflowInstance.Text <> "" Then
    SQL = SQL & " AND RequestName LIKE '%" & QuoteSanitise(txtSearchWorkflowInstance.Text) & "'"
End If
Debug.Print SQL
On Error Resume Next
Set l_rstSize = ExecuteSQL(SQL, g_strExecuteError)
If l_rstSize.RecordCount > 0 Then
    l_curCountRequestSize = l_rstSize(0)
End If
On Error GoTo 0

If optWidth(0).Value <> 0 Then
    adoFileRequests.Caption = adoFileRequests.Recordset.RecordCount & " items" & IIf(chkTop42.Value <> 0 Or chkTop71.Value <> 0, " (out of " & RequestCount & ")", "") & IIf(l_curCountRequestSize <> 0, " (" & Format(l_curCountRequestSize / 1024 / 1024 / 1024 / 1024, "#.##") & " TB)", "")
Else
    adoFileRequests.Caption = adoFileRequests.Recordset.RecordCount & " items" & IIf(chkTop42.Value <> 0 Or chkTop71.Value <> 0, " (out of " & RequestCount & ")", "") & IIf(l_curCountRequestSize <> 0, " (" & Format(l_curCountRequestSize / 1024 / 1024 / 1024 / 1024, "#.##") & " TB)", "")
End If

End Sub

Private Sub RefreshFileRequestsHybrid()

Dim SQL As String, l_strFilter1 As String, l_strFilter2 As String, l_strFilter3 As String, l_strFilter4 As String, l_strFilter5 As String, l_strFilter6 As String
Dim l_datFilterDate As Date
Dim l_curCountRequestSize As Currency, l_strTempSQL As String, l_rstSize As ADODB.Recordset
Dim RequestCount As Long, rsCount As ADODB.Recordset

l_datFilterDate = DateAdd("d", 5, Now)

PopulateCombo "FileOperations", cmbOperationType

SQL = ""

If chkShowHiddenFileRequests.Value = 0 Then
    SQL = SQL & "AND hidefromgrid = 0 "
'Else
'    SQL = SQL & "AND hidefromgrid <> 0 "
End If

If txtSearchTranscodes.Text <> "" Then
    SQL = SQL & " AND clipreference LIKE '" & txtSearchTranscodes.Text & "%' "
End If
If optShowIncomplete.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NULL AND [Request Failed] IS NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Date] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Date] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowRunning.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NULL AND [Request Failed] IS NULL AND [Request Started] IS NOT NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Started] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Started] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowNotStarted.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NULL AND [Request Failed] IS NULL AND [Request Started] IS NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Date] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Date] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowComplete.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NOT NULL and [Request Failed] IS NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Completed] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Completed] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowFailed.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NULL and [Request Failed] IS NOT NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Failed] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Failed] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowAllButComplete.Value <> 0 Then
    SQL = SQL & " AND ([Request Completed] IS NULL OR ([Request Completed] IS NOT NULL AND [Request Failed] IS NOT NULL))"
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Date] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Date] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
Else
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Date] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Date] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
End If
If chkExcludeFarFuture.Value <> 0 Then
    SQL = SQL & " AND [Request Date] < '" & Format(l_datFilterDate, "yyyy-mm-dd") & "' "
End If
If chkExludeMediaInfo.Value <> 0 Then
    SQL = SQL & " AND [File Operation] <> 'MediaInfo + TC' "
End If
If chkJustMyStuff.Value <> 0 Then
    SQL = SQL & " AND ([Requestname] = '" & QuoteSanitise(g_strUserName) & "' OR [Requestname] = '" & QuoteSanitise(g_strUserInitials) & "') "
End If
'If chkExcludeAutoDelete.Value <> 0 Then
'    SQL = SQL & " AND [Event_file_Request_TypeID] <> 24 "
'End If
Select Case cmbOperationType.Text
    Case "All Items", ""
        'do nothing
    Case "All DIVA Archives"
        SQL = SQL & "AND [File Operation] IN ('Copy To 360', 'Move To 360', 'CETA To DIVA', 'CETA Move To DIVA') "
    Case "All DIVA Restores"
        SQL = SQL & "AND [File Operation] IN ('DIVA To CETA', 'Content Delivery From DIVA') "
    Case Else
        SQL = SQL & "AND [File Operation] = '" & cmbOperationType.Text & "'"
End Select
If txtSearchWorkflowInstance.Text <> "" Then
    SQL = SQL & " AND RequestName LIKE '%" & QuoteSanitise(txtSearchWorkflowInstance.Text) & "'"
End If
If txtSearchDeliveryNumber.Text <> "" Then
    SQL = SQL & " AND CDDeliveryNumber = " & Val(txtSearchDeliveryNumber.Text) & " "
End If
If txtSearchJobNumber.Text <> "" Then
    SQL = SQL & " AND JobID = " & Val(txtSearchJobNumber.Text) & " "
End If

If optShowMovementRequests(0).Value = True Then
    Set rsCount = ExecuteSQL("SELECT Count(*) FROM vw_Ceta_FileRequests WHERE 1=1 " & SQL, g_strExecuteError)
    RequestCount = rsCount(0)
    rsCount.Close
    SQL = "SELECT " & IIf(chkTop42.Value <> 0 And chkTop71.Value = 0, "Top 42 ", "") & IIf(chkTop71.Value <> 0 And chkTop42.Value = 0, "Top 71 ", "") & "* FROM vw_Ceta_FileRequests WHERE 1=1 " & SQL
Else
    Set rsCount = ExecuteSQL("SELECT Count(*) FROM vw_Ceta_FileRequests_all WHERE 1=1 " & SQL, g_strExecuteError)
    RequestCount = rsCount(0)
    rsCount.Close
    SQL = "SELECT " & IIf(chkTop42.Value <> 0 And chkTop71.Value = 0, "Top 42 ", "") & IIf(chkTop71.Value <> 0 And chkTop42.Value = 0, "Top 71 ", "") & "* FROM vw_Ceta_FileRequests_all WHERE 1=1 " & SQL
End If

If optShowIncomplete.Value <> 0 Then
    SQL = SQL & "ORDER BY case when [Request Completed] IS NULL and [Request Failed] is null and [request started] is not null then 0 when [request failed] is not null then 1 when [Request Started] IS Null then 2 else 3 END, case when Urgent <> 0 then 0 else 1 end, [Request Date] DESC, event_file_requestID DESC;"
End If
If optShowComplete.Value <> 0 Then
    SQL = SQL & "ORDER BY case when [Request Completed] IS NULL and [Request Failed] is null and [request started] is not null then 0 when [request failed] is not null then 1 when [Request Started] IS Null then 2 else 3 END, [Request Date] DESC, event_file_requestID DESC;"
End If
If optShowFailed.Value <> 0 Then
    SQL = SQL & "ORDER BY case when [Request Completed] IS NULL and [Request Failed] is null and [request started] is not null then 0 when [request failed] is not null then 1 when [Request Started] IS Null then 2 else 3 END, [Request Date] DESC, event_file_requestID DESC;"
End If
If optShowAll.Value <> 0 Or optShowAllButComplete.Value <> 0 Then
    SQL = SQL & "ORDER BY case when [Request Completed] IS NULL and [Request Failed] is null and [request started] is not null then 0 when [request failed] is not null then 1 when [Request Started] IS Null then 2 else 3 END, case when Urgent <> 0 then 0 else 1 end, [Request Date] DESC, event_file_requestID DESC;"
End If

Debug.Print SQL
adoFileRequestsHybrid.ConnectionString = g_strConnection
adoFileRequestsHybrid.RecordSource = SQL
adoFileRequestsHybrid.Refresh

If optShowMovementRequests(0).Value = True Then
    SQL = "SELECT Sum(bigfilesize) FROM vw_Ceta_FileRequests WHERE 1=1 "
Else
    SQL = "SELECT Sum(bigfilesize) FROM vw_Ceta_FileRequests_all WHERE 1=1 "
End If
If chkShowHiddenFileRequests.Value = 0 Then
    SQL = SQL & "AND hidefromgrid = 0 "
'Else
'    SQL = SQL & "AND hidefromgrid <> 0 "
End If
If txtSearchTranscodes.Text <> "" Then
    SQL = SQL & " AND clipreference LIKE '" & txtSearchTranscodes.Text & "%' "
End If
If optShowIncomplete.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NULL AND [Request Failed] IS NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Date] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Date] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowRunning.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NULL AND [Request Failed] IS NULL AND [Request Started] IS NOT NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Started] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Started] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowNotStarted.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NULL AND [Request Failed] IS NULL AND [Request Started] IS NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Date] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Date] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowComplete.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NOT NULL and [Request Failed] IS NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Completed] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Completed] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowFailed.Value <> 0 Then
    SQL = SQL & " AND [Request Completed] IS NULL and [Request Failed] IS NOT NULL "
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Failed] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Failed] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
ElseIf optShowAllButComplete.Value <> 0 Then
    SQL = SQL & " AND ([Request Completed] IS NULL OR ([Request Started] IS NOT NULL AND [Request Failed] IS NOT NULL))"
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Date] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Date] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
Else
    If Not IsNull(datSearchDate.Value) Then SQL = SQL & "AND [Request Date] >= '" & Format(datSearchDate.Value, "YYYY-MM-DD") & "' AND [Request Date] < '" & Format(DateAdd("D", 1, datSearchDate.Value), "YYYY-MM-DD") & "'"
End If
If chkExcludeFarFuture.Value <> 0 Then
    SQL = SQL & " AND [Request Date] < '" & Format(l_datFilterDate, "yyyy-mm-dd") & "' "
End If
If chkJustMyStuff.Value <> 0 Then
    SQL = SQL & " AND ([Requestname] = '" & QuoteSanitise(g_strUserName) & "' OR [Requestname] = '" & QuoteSanitise(g_strUserInitials) & "') "
End If
'If chkExcludeAutoDelete.Value <> 0 Then
'    SQL = SQL & " AND [Event_file_Request_TypeID] <> 24 "
'End If
Select Case cmbOperationType.Text
    Case "All Items", ""
        'do nothing
    Case "All DIVA Archives"
        SQL = SQL & "AND [File Operation] IN ('Copy To 360', 'Move To 360', 'CETA To DIVA', 'CETA Move To DIVA') "
    Case "All DIVA Restores"
        SQL = SQL & "AND [File Operation] IN ('DIVA To CETA', 'Content Delivery From DIVA') "
    Case Else
        SQL = SQL & "AND [File Operation] = '" & cmbOperationType.Text & "'"
End Select
If txtSearchWorkflowInstance.Text <> "" Then
    SQL = SQL & " AND RequestName LIKE '%" & QuoteSanitise(txtSearchWorkflowInstance.Text) & "'"
End If
Debug.Print SQL
On Error Resume Next
Set l_rstSize = ExecuteSQL(SQL, g_strExecuteError)
If l_rstSize.RecordCount > 0 Then
    l_curCountRequestSize = l_rstSize(0)
End If
On Error GoTo 0

If optWidth(0).Value <> 0 Then
    adoFileRequestsHybrid.Caption = adoFileRequestsHybrid.Recordset.RecordCount & " items" & IIf(chkTop42.Value <> 0 Or chkTop71.Value <> 0, " (out of " & RequestCount & ")", "") & IIf(l_curCountRequestSize <> 0, " (" & Format(l_curCountRequestSize / 1024 / 1024 / 1024 / 1024, "#.##") & " TB)", "")
Else
    adoFileRequestsHybrid.Caption = adoFileRequestsHybrid.Recordset.RecordCount & " items" & IIf(chkTop42.Value <> 0 Or chkTop71.Value <> 0, " (out of " & RequestCount & ")", "") & IIf(l_curCountRequestSize <> 0, " (" & Format(l_curCountRequestSize / 1024 / 1024 / 1024 / 1024, "#.##") & " TB)", "")
End If

End Sub
Private Sub RefreshTechrevOutputRequests()

Dim SQL As String

SQL = "SELECT * FROM vw_excel_techrev_output_request ORDER BY RequestDate DESC"
adoExcelRequests.ConnectionString = g_strConnection
adoExcelRequests.RecordSource = SQL
adoExcelRequests.Refresh

adoExcelRequests.Caption = adoExcelRequests.Recordset.RecordCount & " items"

End Sub

Private Sub RefreshWorkflowConfig()

Dim SQL As String, i As Integer
Dim rs As ADODB.Recordset, l_strMessage As String

SQL = "SELECT * FROM Workflow WHERE 1=1 "
SQL = SQL & "ORDER BY WorkflowName;"
For i = 0 To 2
    adoWorkflowDefinitions(i).ConnectionString = g_strConnection
    adoWorkflowDefinitions(i).RecordSource = SQL
    adoWorkflowDefinitions(i).Refresh
Next

SQL = "SELECT * FROM Workflow_Tasks WHERE 1=1"
SQL = SQL & "ORDER BY WorkflowID, TaskID;"
adoWorkflowTasks(0).ConnectionString = g_strConnection
adoWorkflowTasks(0).RecordSource = SQL
adoWorkflowTasks(0).Refresh

SQL = "SELECT * FROM Workflow_TaskType WHERE 1=1 "
SQL = SQL & "Order by TaskType;"
For i = 0 To 2
    adoWorkflowTaskTypes(i).ConnectionString = g_strConnection
    adoWorkflowTaskTypes(i).RecordSource = SQL
    adoWorkflowTaskTypes(i).Refresh
Next

SQL = "SELECT * FROM Workflow_TaskType_DefaultVariables "
SQL = SQL & "ORDER BY Variable;"
adoWorkflowTaskTypeDefaultVariables(0).ConnectionString = g_strConnection
adoWorkflowTaskTypeDefaultVariables(0).RecordSource = SQL
adoWorkflowTaskTypeDefaultVariables(0).Refresh

SQL = "SELECT * FROM Workflow_Variant "
SQL = SQL & "ORDER BY VariantName"
adoWorkflowVariants(0).ConnectionString = g_strConnection
adoWorkflowVariants(0).RecordSource = SQL
adoWorkflowVariants(0).Refresh

SQL = "SELECT companyID, name FROM Company ORDER BY name;"
adoCompany.ConnectionString = g_strConnection
adoCompany.RecordSource = SQL
adoCompany.Refresh

grdWorkflowTasks.Columns("WorkflowName").DropDownHwnd = ddnWorkflowDefinitions(0).hWnd
grdWorkflowTasks.Columns("TaskType").DropDownHwnd = ddnTaskTypes(0).hWnd
grdTaskTypeDefaultVariables.Columns("TaskType").DropDownHwnd = ddnTaskTypes(1).hWnd
grdWorkflowVariants.Columns("WorkflowName").DropDownHwnd = ddnWorkflowDefinitions(1).hWnd
grdWorkflowVariants.Columns("CompanyName").DropDownHwnd = ddnCompany.hWnd

End Sub

Private Sub RefreshWorkflows()

Dim SQL As String

SQL = "SELECT * FROM vw_Workflow_Monitoring WHERE 1=1 "
If chkHideAssociateFile.Value <> 0 Then
    SQL = SQL & "AND CurrentTaskType <> 'AssociateFile' "
End If
If txtSearchWorkflowInstance.Text <> "" Then
    SQL = SQL & " AND instanceid = " & QuoteSanitise(txtSearchWorkflowInstance.Text)
End If
If txtSearchTranscodes.Text <> "" Then
    SQL = SQL & " AND itemreference LIKE '" & QuoteSanitise(txtSearchTranscodes.Text) & "%' "
End If
If chkHideDeliveryHold.Value <> 0 Then
    SQL = SQL & "AND CurrentTaskType <> 'WaitUntilTime' "
End If
If txtSearchCompanyID.Text <> "" Then
    SQL = SQL & " AND companyID = " & Val(txtSearchCompanyID.Text) & " "
End If
If txtSearchTrackerID.Text <> "" Then
    SQL = SQL & " AND tracker_itemID = " & Val(txtSearchTrackerID.Text) & " "
End If
If chkHideCancelled.Value <> 0 Then
    SQL = SQL & "AND CurrentTaskType <> 'Cancelled' "
End If
If chkHideComplete.Value <> 0 Then
    SQL = SQL & "AND CurrentTaskType <> 'AutomationStop' "
End If
SQL = SQL & "ORDER BY WorkflowStart DESC, InstanceID"
adoWorkflows.ConnectionString = g_strConnection
adoWorkflows.RecordSource = SQL
adoWorkflows.Refresh

adoWorkflows.Caption = adoWorkflows.Recordset.RecordCount & " items"

Dim rs As ADODB.Recordset, l_strMessage As String

If Not adoWorkflows.Recordset.EOF Then
    lblInstanceID.Caption = adoWorkflows.Recordset("instanceID")
    Set rs = ExecuteSQL("exec workflow_OutputSynopsis " & adoWorkflows.Recordset("workflowID"), g_strExecuteError)
    l_strMessage = Trim(" " & rs(0))
    rs.Close
    txtWorkflowSynopsis.Text = Replace(l_strMessage, vbLf, vbCrLf)
    SQL = "exec Workflow_Getvariables_withRaw " & adoWorkflows.Recordset("instanceID")
    adoInstanceVariables.ConnectionString = g_strConnection
    adoInstanceVariables.RecordSource = SQL
    adoInstanceVariables.Refresh
        
    adoNextTask.ConnectionString = g_strConnection
    adoNextTask.RecordSource = "SELECT TaskID, StageName FROM Workflow_Tasks WHERE WorkflowID = " & adoWorkflows.Recordset("workflowID")
    adoNextTask.Refresh
    
Else
    lblInstanceID.Caption = ""
    txtWorkflowSynopsis.Text = ""
    adoInstanceVariables.ConnectionString = g_strConnection
    adoInstanceVariables.RecordSource = "exec Workflow_Getvariables_withRaw 0"
    adoInstanceVariables.Refresh
    
    adoNextTask.ConnectionString = g_strConnection
    adoNextTask.RecordSource = "SELECT TaskID, StageName FROM Workflow_Tasks WHERE WorkflowID = 0"
    adoNextTask.Refresh
    
End If

End Sub

Private Sub RefreshTranscodeSpecs()

Dim l_strSQL As String, l_strSystems As String

If m_blnTabUpdate = False Then

    PopulateCombo "discstores", cmbBarcode(1)
    PopulateCombo "discstores", cmbBarcode(2)
    PopulateCombo "discstores", cmbBarcode(3)
    PopulateCombo "discstores", cmbBarcode(4)
    PopulateCombo "discstores", cmbBarcode(5)
    PopulateCombo "discstores", cmbDeliveryBarcode(1)
    PopulateCombo "discstores", cmbDeliveryBarcode(2)
    PopulateCombo "discstores", cmbDeliveryBarcode(3)
    PopulateCombo "discstores", cmbDeliveryBarcode(4)
    PopulateCombo "discstores", cmbDeliveryBarcode(5)
    PopulateCombo "audiomapping", cmbAudioMapping(1)
    PopulateCombo "audiomapping", cmbAudioMapping(2)
    PopulateCombo "audiomapping", cmbAudioMapping(3)
    PopulateCombo "audiomapping", cmbAudioMapping(4)
    PopulateCombo "audiomapping", cmbAudioMapping(5)
    PopulateCombo "ffmpeg_explicit_version", txtExplicitFFMpegVersion
    
    Set l_conSearch = New ADODB.Connection
    Set l_rstSearch1 = New ADODB.Recordset
    Set l_rstSearch2 = New ADODB.Recordset
    Set l_rstSearch3 = New ADODB.Recordset
    Set l_rstSearch4 = New ADODB.Recordset
    Set l_rstSearch5 = New ADODB.Recordset
    Set l_rstSearch6 = New ADODB.Recordset
    Set l_rstSearch7 = New ADODB.Recordset
    Set l_rstSearch8 = New ADODB.Recordset
    Set l_rstSearch9 = New ADODB.Recordset
    Set l_rstSearch10 = New ADODB.Recordset
    Set l_rstSearch11 = New ADODB.Recordset
    Set l_rstSearch12 = New ADODB.Recordset
    Set l_rstSearch13 = New ADODB.Recordset
    Set l_rstSearch14 = New ADODB.Recordset
    Set l_rstSearch15 = New ADODB.Recordset
    Set l_rstSearch16 = New ADODB.Recordset
    Set l_rstSearch17 = New ADODB.Recordset
    Set l_rstSearch18 = New ADODB.Recordset
    Set l_rstSearch19 = New ADODB.Recordset
    Set l_rstSearch20 = New ADODB.Recordset
    Set l_rstSearch21 = New ADODB.Recordset
    Set l_rstSearch22 = New ADODB.Recordset
    
    l_conSearch.ConnectionString = g_strConnection
    l_conSearch.Open
    
    l_strSQL = "SELECT * FROM mediaspec ORDER BY fd_order, mediaspecname;"
    
    With l_rstSearch1
        .CursorLocation = adUseClient
        .LockType = adLockBatchOptimistic
        .CursorType = adOpenDynamic
        .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch1.ActiveConnection = Nothing
    Set cmbMediaSpec(1).DataSourceList = l_rstSearch1
    
    With l_rstSearch2
        .CursorLocation = adUseClient
        .LockType = adLockBatchOptimistic
        .CursorType = adOpenDynamic
        .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch2.ActiveConnection = Nothing
    Set cmbMediaSpec(2).DataSourceList = l_rstSearch2
    
    With l_rstSearch3
        .CursorLocation = adUseClient
        .LockType = adLockBatchOptimistic
        .CursorType = adOpenDynamic
        .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch3.ActiveConnection = Nothing
    Set cmbMediaSpec(3).DataSourceList = l_rstSearch3
    
    With l_rstSearch4
        .CursorLocation = adUseClient
        .LockType = adLockBatchOptimistic
        .CursorType = adOpenDynamic
        .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch4.ActiveConnection = Nothing
    Set cmbMediaSpec(4).DataSourceList = l_rstSearch4
    
    With l_rstSearch5
        .CursorLocation = adUseClient
        .LockType = adLockBatchOptimistic
        .CursorType = adOpenDynamic
        .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch5.ActiveConnection = Nothing
    Set cmbMediaSpec(5).DataSourceList = l_rstSearch5
    
    l_strSQL = "SELECT ATS_Profile_ID, ATS_Profile_Name FROM ATS_Profile ORDER BY ATS_Profile_Name;"
    
    With l_rstSearch11
        .CursorLocation = adUseClient
        .LockType = adLockBatchOptimistic
        .CursorType = adOpenDynamic
        .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch11.ActiveConnection = Nothing
    Set cmbATSProfile.DataSourceList = l_rstSearch11
    
    l_strSQL = "SELECT name, companyID FROM company WHERE (iscustomer = 1 OR isprospective = 1) AND system_active = 1 ORDER BY name;"
    
    With l_rstSearch12
        .CursorLocation = adUseClient
        .LockType = adLockBatchOptimistic
        .CursorType = adOpenDynamic
        .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch12.ActiveConnection = Nothing
    Set cmbNewCompany(1).DataSourceList = l_rstSearch12
    
    With l_rstSearch13
        .CursorLocation = adUseClient
        .LockType = adLockBatchOptimistic
        .CursorType = adOpenDynamic
        .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch13.ActiveConnection = Nothing
    Set cmbNewCompany(2).DataSourceList = l_rstSearch13
    
    With l_rstSearch14
        .CursorLocation = adUseClient
        .LockType = adLockBatchOptimistic
        .CursorType = adOpenDynamic
        .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch14.ActiveConnection = Nothing
    Set cmbNewCompany(3).DataSourceList = l_rstSearch14
    
    With l_rstSearch15
        .CursorLocation = adUseClient
        .LockType = adLockBatchOptimistic
        .CursorType = adOpenDynamic
        .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch15.ActiveConnection = Nothing
    Set cmbNewCompany(4).DataSourceList = l_rstSearch15
    
    With l_rstSearch16
        .CursorLocation = adUseClient
        .LockType = adLockBatchOptimistic
        .CursorType = adOpenDynamic
        .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch16.ActiveConnection = Nothing
    Set cmbNewCompany(5).DataSourceList = l_rstSearch16
    
    l_strSQL = "SELECT eventID, clipfilename FROM events WHERE fileversion = 'Headbuild' and system_deleted = 0 AND libraryID IN (SELECT libraryID FROM library WHERE format = 'DISCSTORE') ORDER BY clipfilename;"
    
    With l_rstSearch22
        .CursorLocation = adUseClient
        .LockType = adLockBatchOptimistic
        .CursorType = adOpenDynamic
        .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch22.ActiveConnection = Nothing
    Set txtHeadbuildEventID.DataSourceList = l_rstSearch22
    
    l_conSearch.Close
    Set l_conSearch = Nothing

End If
    
l_strSQL = "SELECT * FROM Transcodespec WHERE system_deleted = 0 "
If optShowTranscodeSpecs(0).Value = True Then
    l_strSQL = l_strSQL & "AND enabled <> 0 "
End If
If chkTranscodeSystem(17).Value <> 0 Then
    l_strSQL = l_strSQL & "AND transcodesystem NOT IN ('Vantage_API', 'FFMpeg_Generic', 'FFMpeg_Generic_Test', 'FFMpeg_ProRes_Clip', 'FFMpeg_ProRes_Clip_Test', 'FFMpeg_Thumb', 'BBCMG_Proxy', 'BBCMG_Proxy_Test', " & _
    "'BBCMG_Proxy_Duplicate', 'BBCMG_Proxy_Silent') "
Else
    l_strSQL = l_strSQL & "AND transcodesystem IN ("
    l_strSystems = "'NoSystem'"
    If chkTranscodeSystem(4).Value <> 0 Then
        l_strSystems = l_strSystems & ", " & "'Vantage_API'"
    End If
    If chkTranscodeSystem(3).Value <> 0 Then
        l_strSystems = l_strSystems & ", " & "'FFMpeg_Generic', 'FFMpeg_Generic_Test'"
    End If
'    If chkTranscodeSystem(9).Value <> 0 Then
'        l_strSystems = l_strSystems & ", " & "'BBCMG_ProRes_Clip', 'BBCMG_ProRes_Clip_Test'"
'    End If
    If chkTranscodeSystem(11).Value <> 0 Then
        l_strSystems = l_strSystems & ", " & "'FFMpeg_Thumb'"
    End If
'    If chkTranscodeSystem(5).Value <> 0 Then
'        l_strSystems = l_strSystems & ", " & "'BBCMG_Proxy', 'BBCMG_Proxy_Test'"
'    End If
'    If chkTranscodeSystem(15).Value <> 0 Then
'        l_strSystems = l_strSystems & ", " & "'BBCMG_Proxy_Duplicate'"
'    End If
'    If chkTranscodeSystem(16).Value <> 0 Then
'        l_strSystems = l_strSystems & ", " & "'BBCMG_Proxy_Silent'"
'    End If
    l_strSQL = l_strSQL & l_strSystems
    l_strSQL = l_strSQL & ") "
End If
l_strSQL = l_strSQL & "ORDER BY forder, transcodename"
Debug.Print l_strSQL
adoTranscodeSpecs.ConnectionString = g_strConnection
adoTranscodeSpecs.RecordSource = l_strSQL
adoTranscodeSpecs.Refresh
adoTranscodeSpecs.Caption = adoTranscodeSpecs.Recordset.RecordCount & " Record(s)"

End Sub

Private Sub grdTranscodeSpecs_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim l_lngTranscodeSpecID As Long, l_rstSpec As ADODB.Recordset, Index As Integer

If Not adoTranscodeSpecs.Recordset.EOF Then
    l_lngTranscodeSpecID = Val(adoTranscodeSpecs.Recordset("transcodespecID"))
    Set l_rstSpec = ExecuteSQL("SELECT * FROM transcodespec WHERE transcodespecID = " & l_lngTranscodeSpecID, g_strExecuteError)
    CheckForSQLError
    If l_rstSpec.RecordCount > 0 Then
        lblSpecID.Caption = l_lngTranscodeSpecID
        chkAutoTranscode.Value = GetFlag(l_rstSpec("autotranscode"))
        txtBillingCode.Text = Trim(" " & l_rstSpec("billingcode"))
        txtTitle.Text = Trim(" " & l_rstSpec("transcodename"))
        chkEnabled.Value = l_rstSpec("enabled")
        chkSkipLogging.Value = l_rstSpec("skiplogging")
        txtWatchfolder.Text = Trim(" " & l_rstSpec("watchfolder"))
        cmbATSProfile.Text = Trim(" " & l_rstSpec("flashpreviewwatchfolder"))
        cmbTranscodeSystem.Text = Trim(" " & l_rstSpec("transcodesystem"))
        chkAsOrig.Value = GetFlag(l_rstSpec("transcodeasmaster"))
        chkNexguard.Value = GetFlag(l_rstSpec("NexGuard"))
        chkNoReferenceCheck.Value = GetFlag(l_rstSpec("NoReferenceCheck"))
        chkReplaceOriginalFile.Value = GetFlag(l_rstSpec("ReplaceOriginal"))
        chkReStripe.Value = GetFlag(l_rstSpec("ReStripeTranscode"))
        txtOtherSpecs.Text = Trim(" " & l_rstSpec("otherspecs"))
        txtHeadbuildEventID.Text = Trim(" " & l_rstSpec("HeadbuildEventID"))
        txtExplicitFFMpegVersion.Text = Trim(" " & l_rstSpec("ffmpeg_explicit_version"))
        If Val(txtHeadbuildEventID.Text) <> 0 Then
            lblHeadbuildFilename.Caption = GetData("events", "clipfilename", "eventID", Val(txtHeadbuildEventID.Text))
        Else
            lblHeadbuildFilename.Caption = ""
        End If
        Select Case cmbTranscodeSystem.Text
            Case "FFMpeg_Audio_Conform", "FFMpeg_Timecode_Restripe", "FFMpeg_Generic", "Vantage_API"
                chkReplaceOriginalFile.Visible = True
                lblReplacementWarning.Visible = True
            Case Else
                chkReplaceOriginalFile.Visible = False
                lblReplacementWarning.Visible = False
        End Select
        If chkReplaceOriginalFile.Value <> 0 Then
            txtDeliveryFolder(1).Enabled = False
            cmbDeliveryBarcode(1).Enabled = False
            chkDestinationThrowawayFile(1).Enabled = False
            chkDestinationDeliveryCopy(1).Enabled = False
        Else
            txtDeliveryFolder(1).Enabled = True
            cmbDeliveryBarcode(1).Enabled = True
            chkDestinationThrowawayFile(1).Enabled = True
            chkDestinationDeliveryCopy(1).Enabled = True
        End If
        For Index = 1 To 5
            lblDestinationLibraryID(Index).Caption = Val(Trim(" " & l_rstSpec("destination" & Index & "libraryID")))
            lblDeliveryLibraryID(Index).Caption = Val(Trim(" " & l_rstSpec("destination" & Index & "deliverylibraryID")))
            cmbBarcode(Index).Text = GetData("library", "barcode", "libraryID", lblDestinationLibraryID(Index).Caption)
            cmbDeliveryBarcode(Index).Text = GetData("library", "barcode", "libraryID", lblDeliveryLibraryID(Index).Caption)
            lblNewCompanyID(Index).Caption = Val(Trim(" " & l_rstSpec("destination" & Index & "newcompanyID")))
            cmbNewCompany(Index).Text = GetData("company", "name", "companyID", lblNewCompanyID(Index).Caption)
            txtDestinationFolder(Index).Text = Trim(" " & l_rstSpec("destination" & Index & "folder"))
            txtDeliveryFolder(Index).Text = Trim(" " & l_rstSpec("destination" & Index & "deliveryfolder"))
            txtDestinationExtender(Index).Text = Trim(" " & l_rstSpec("destination" & Index & "fileextender"))
            lblMediaSpecID(Index).Caption = Val(Trim(" " & l_rstSpec("mediaspec" & Index & "ID")))
            cmbMediaSpec(Index).Text = GetData("mediaspec", "mediaspecname", "mediaspecID", lblMediaSpecID(Index).Caption)
            cmbPurpose(Index).Text = Trim(" " & l_rstSpec("destination" & Index & "eventtype"))
            chkDestinationThrowawayFile(Index).Value = GetFlag(Trim(" " & l_rstSpec("destination" & Index & "throwaway")))
            chkDestinationDeliveryCopy(Index).Value = GetFlag(Trim(" " & l_rstSpec("destination" & Index & "deliverycopy")))
            chkNoDurationCheck(Index).Value = GetFlag(Trim(" " & l_rstSpec("destination" & Index & "nodurationcheck")))
            lblAudioMapping(Index).Caption = Trim(" " & l_rstSpec("destination" & Index & "audiomapping"))
            cmbAudioMapping(Index).Text = Trim(" " & l_rstSpec("destination" & Index & "audiomappingname"))
        Next
        
        If cmbTranscodeSystem.Text <> "Vantage_API" And cmbTranscodeSystem.Text <> "FFMpeg_Generic" Then
            For Index = 2 To 5
                cmbBarcode(Index).Enabled = False
                cmbDeliveryBarcode(Index).Enabled = False
                cmbNewCompany(Index).Enabled = False
                txtDestinationFolder(Index).Enabled = False
                txtDeliveryFolder(Index).Enabled = False
                txtDestinationExtender(Index).Enabled = False
                cmbMediaSpec(Index).Enabled = False
                cmbPurpose(Index).Enabled = False
                chkDestinationThrowawayFile(Index).Enabled = False
                chkDestinationDeliveryCopy(Index).Enabled = False
                cmbAudioMapping(Index).Enabled = False
            Next
        Else
            For Index = 2 To 5
                cmbBarcode(Index).Enabled = True
                cmbDeliveryBarcode(Index).Enabled = True
                cmbNewCompany(Index).Enabled = True
                txtDestinationFolder(Index).Enabled = True
                txtDeliveryFolder(Index).Enabled = True
                txtDestinationExtender(Index).Enabled = True
                cmbMediaSpec(Index).Enabled = True
                cmbPurpose(Index).Enabled = True
                chkDestinationThrowawayFile(Index).Enabled = True
                chkDestinationDeliveryCopy(Index).Enabled = True
                cmbAudioMapping(Index).Enabled = True
            Next
        End If
    End If
End If

End Sub

Private Sub grdWorkflowDefinitions_AfterUpdate(RtnDispErrMsg As Integer)

Dim i As Long, SQL As String

SQL = "SELECT * FROM Workflow WHERE 1=1 "
SQL = SQL & "ORDER BY WorkflowName;"
For i = 0 To 2
    adoWorkflowDefinitions(i).ConnectionString = g_strConnection
    adoWorkflowDefinitions(i).RecordSource = SQL
    adoWorkflowDefinitions(i).Refresh
Next

End Sub

Private Sub grdWorkflowDefinitions_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

lblWorkflowDefinitionID.Caption = grdWorkflowDefinitions.Columns("WorkflowID").Text

If lblWorkflowDefinitionID.Caption <> "" Then
    adoWorkflowVariants(0).RecordSource = "SELECT * FROM Workflow_Variant WHERE WorkflowID = " & lblWorkflowDefinitionID.Caption & ";"
    adoWorkflowVariants(0).ConnectionString = g_strConnection
    adoWorkflowVariants(0).Refresh
    
    adoWorkflowTasks(0).ConnectionString = g_strConnection
    adoWorkflowTasks(0).RecordSource = "SELECT * FROM Workflow_Tasks WHERE WorkflowID = " & lblWorkflowDefinitionID.Caption & " ORDER BY TaskID;"
    adoWorkflowTasks(0).Refresh
    
    adoMissingVariables.ConnectionString = g_strConnection
    adoMissingVariables.RecordSource = "Select * from vw_Workflow_MissingTaskVariables WHERE workflowID = " & lblWorkflowDefinitionID.Caption
    adoMissingVariables.Refresh
End If

End Sub

Private Sub grdWorkflows_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

If Not CheckAccess("/DeleteWorkflowInstances") Then
    Cancel = 1
    Exit Sub
End If

Dim SQL As String

If MsgBox("Are You Sure", vbYesNo + vbDefaultButton2, "Deleting Workflow Instance " & grdWorkflows.Columns("InstanceID").Text) = vbYes Then

    SQL = "DELETE FROM Workflow_InstanceVariables WHERE InstanceID = " & grdWorkflows.Columns("InstanceID").Text
    ExecuteSQL SQL, g_strExecuteError
    CheckForSQLError
    SQL = "DELETE FROM Workflow_Instance WHERE InstanceID = " & grdWorkflows.Columns("InstanceID").Text
    ExecuteSQL SQL, g_strExecuteError
    CheckForSQLError

End If

Cancel = 1

End Sub

Private Sub grdWorkflows_DblClick()

If g_intFileRequestGridClick <> 0 Then Exit Sub
g_intFileRequestGridClick = 1
On Error GoTo NOCOLUMN

If grdWorkflows.Columns("ItemReference").Text <> "" Then
    ShowClipSearch "", grdWorkflows.Columns("ItemReference").Text
End If


g_intFileRequestGridClick = 0

Exit Sub

NOCOLUMN:

End Sub

Private Sub grdWorkflows_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim rs As ADODB.Recordset, l_strMessage As String, SQL As String

If Not adoWorkflows.Recordset.EOF Then
    lblInstanceID.Caption = adoWorkflows.Recordset("instanceID")
    Set rs = ExecuteSQL("exec workflow_OutputSynopsis " & adoWorkflows.Recordset("workflowID"), g_strExecuteError)
    l_strMessage = Trim(" " & rs(0))
    rs.Close
    txtWorkflowSynopsis.Text = Replace(l_strMessage, vbLf, vbCrLf)
    SQL = "exec Workflow_Getvariables_withRaw " & adoWorkflows.Recordset("instanceID")
    adoInstanceVariables.ConnectionString = g_strConnection
    adoInstanceVariables.RecordSource = SQL
    adoInstanceVariables.Refresh
        
    adoNextTask.ConnectionString = g_strConnection
    adoNextTask.RecordSource = "SELECT TaskID, StageName FROM Workflow_Tasks WHERE WorkflowID = " & adoWorkflows.Recordset("workflowID")
    adoNextTask.Refresh
    
Else
    lblInstanceID.Caption = ""
    txtWorkflowSynopsis.Text = ""
    adoInstanceVariables.ConnectionString = g_strConnection
    adoInstanceVariables.RecordSource = "exec Workflow_Getvariables_withRaw 0"
    adoInstanceVariables.Refresh
    
    adoNextTask.ConnectionString = g_strConnection
    adoNextTask.RecordSource = "SELECT TaskID, StageName FROM Workflow_Tasks WHERE WorkflowID = 0"
    adoNextTask.Refresh
    
End If

End Sub

Private Sub grdWorkflowTaskDefaultVariables_BeforeUpdate(Cancel As Integer)

If grdWorkflowTaskDefaultVariables.Columns("Variable").Text = "" Then
Cancel = 1
Exit Sub
End If

If Val(lblTaskID.Caption) = 0 Or Val(lblWorkflowID.Caption) = 0 Then
    Cancel = 1
    Exit Sub
End If

grdWorkflowTaskDefaultVariables.Columns("WorkflowID").Text = lblWorkflowID.Caption
grdWorkflowTaskDefaultVariables.Columns("TaskID").Text = lblTaskID.Caption

End Sub

Private Sub grdWorkflowTasks_BeforeUpdate(Cancel As Integer)

If grdWorkflowTasks.Columns("WorkflowName").Text = "" Then grdWorkflowTasks.Columns("WorkflowID").Text = lblWorkflowDefinitionID.Caption

End Sub

Private Sub grdWorkflowTasks_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim l_strSQL As String

lblTaskID.Caption = grdWorkflowTasks.Columns("TaskID").Text
lblWorkflowID.Caption = grdWorkflowTasks.Columns("WorkflowID").Text

If Val(lblTaskID.Caption) <> Val(lblLastTaskID.Caption) Then
    
    If Val(lblTaskID.Caption) = 0 Then
        
        l_strSQL = "SELECT * FROM Workflow_Task_DefaultVariables WHERE taskID = -1;"
        adoWorkflowTaskDefaultVariables(0).RecordSource = l_strSQL
        adoWorkflowTaskDefaultVariables(0).ConnectionString = g_strConnection
        adoWorkflowTaskDefaultVariables(0).Refresh
        
        lblLastTaskID.Caption = ""
        
        Exit Sub
        
    End If
    
    l_strSQL = "SELECT * FROM Workflow_Task_DefaultVariables WHERE taskID = " & lblTaskID.Caption & " ORDER BY Variable;"
    
    adoWorkflowTaskDefaultVariables(0).RecordSource = l_strSQL
    adoWorkflowTaskDefaultVariables(0).ConnectionString = g_strConnection
    adoWorkflowTaskDefaultVariables(0).Refresh

    adoWorkflowTaskVariables.ConnectionString = g_strConnection
    adoWorkflowTaskVariables.RecordSource = "SELECT * FROM Workflow_TaskVariables WHERE variantID = " & Val(lblVariantID.Caption) & " AND WorkflowTaskID = " & Val(lblTaskID.Caption)
    adoWorkflowTaskVariables.Refresh
    
    lblLastTaskID.Caption = lblTaskID.Caption
    
End If
    
End Sub

Private Sub grdWorkflowTasks_RowLoaded(ByVal Bookmark As Variant)

grdWorkflowTasks.Columns("WorkflowName").Text = GetData("Workflow", "WorkflowName", "WorkflowID", grdWorkflowTasks.Columns("WorkflowID").Text)
grdWorkflowTasks.Columns("TaskType").Text = GetData("Workflow_TaskType", "TaskType", "TaskTypeID", grdWorkflowTasks.Columns("TaskTypeID").Text)

End Sub

Private Sub grdWorkflowTaskType_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

lblTaskTypeID.Caption = grdWorkflowTaskType.Columns("TaskTypeID").Text

adoWorkflowTaskTypeDefaultVariables(0).ConnectionString = g_strConnection
adoWorkflowTaskTypeDefaultVariables(0).RecordSource = "SELECT * FROM Workflow_TaskType_DefaultVariables WHERE TaskTypeID = " & lblTaskTypeID.Caption & ";"
adoWorkflowTaskTypeDefaultVariables(0).Refresh

End Sub

Private Sub grdWorkflowTaskVariables_BeforeUpdate(Cancel As Integer)

If grdWorkflowTaskVariables.Columns("VariantID").Text = "" Then grdWorkflowTaskVariables.Columns("VariantID").Text = lblVariantID.Caption
If grdWorkflowTaskVariables.Columns("WorkflowTaskID").Text = "" Then grdWorkflowTaskVariables.Columns("WorkflowTaskID").Text = lblTaskID.Caption

End Sub

Private Sub grdWorkflowVariants_BeforeUpdate(Cancel As Integer)

If grdWorkflowVariants.Columns("WorkflowID").Text = "" Then grdWorkflowVariants.Columns("WorkflowID").Text = lblWorkflowDefinitionID.Caption

End Sub

Private Sub grdWorkflowVariants_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

lblVariantID.Caption = grdWorkflowVariants.Columns("VariantID").Text

adoWorkflowTaskVariables.ConnectionString = g_strConnection
adoWorkflowTaskVariables.RecordSource = "SELECT * FROM Workflow_TaskVariables WHERE variantID = " & Val(lblVariantID.Caption) & " AND WorkflowTaskID = " & Val(lblTaskID.Caption)
adoWorkflowTaskVariables.Refresh

End Sub

Private Sub grdWorkflowVariants_RowLoaded(ByVal Bookmark As Variant)

grdWorkflowVariants.Columns("WorkflowName").Text = GetData("Workflow", "WorkflowName", "WorkflowID", grdWorkflowVariants.Columns("WorkflowID").Text)
grdWorkflowVariants.Columns("CompanyName").Text = GetData("Company", "name", "companyID", grdWorkflowVariants.Columns("companyID").Text)

End Sub

Private Sub optWidth_Click(Index As Integer)

Select Case Index
Case 0 'Narrow
    grdFileRequests.Columns("Source Filename").Visible = False
    grdFileRequests.Columns("Comment").Width = 2850 '2350
    grdFileRequests.Columns("New Folder").Visible = True
    grdFileRequests.Columns("New Folder").Width = 1500
    grdFileRequests.Columns("NewFileName").Visible = True
    grdFileRequests.Columns("NewFileName").Width = 1550
    grdFileRequests.Columns("Originally On").Visible = True
'    grdFileRequests.Columns("RequestID").Visible = True
'    grdFileRequests.Columns("Source file").Visible = True
'    grdFileRequests.Columns("Source file").Width = 2400
    grdFileRequests.Columns("RequestCleanedUp").Visible = False
    grdFileRequests.Columns("jobID").Visible = False
    grdFileRequests.Columns("CDDeliveryNumber").Visible = False
    grdFileRequests.Columns("NotifyRecipient").Visible = False
    grdFileRequests.Columns("clipreference").Width = 2400
    grdFileRequests.Columns("Request Date").Width = 1700
    grdFileRequests.Columns("Request Started").Width = 1700
    grdFileRequests.Columns("Request Completed").Width = 1700
    grdFileRequests.Columns("Request Failed").Width = 1050
    grdFileRequests.Columns("Paused").Visible = False
'    grdFileRequests.Columns("Originally On").Width = 1300
    grdFileRequests.Columns("Store").Width = 1450
    grdFileRequests.Columns("RequestName").Width = 1000
    grdFileRequests.Columns("DoNotRecordCopy").Visible = False
Case 1 'Medium
    grdFileRequests.Columns("Source Filename").Visible = False
    grdFileRequests.Columns("Comment").Width = 4100
    grdFileRequests.Columns("New Folder").Visible = True
    grdFileRequests.Columns("New Folder").Width = 3450
    grdFileRequests.Columns("NewFileName").Visible = True
    grdFileRequests.Columns("NewFileName").Width = 2900
    grdFileRequests.Columns("Originally On").Visible = True
'    grdFileRequests.Columns("RequestID").Visible = True
'    grdFileRequests.Columns("Source file").Visible = True
'    grdFileRequests.Columns("Source file").Width = 2400
    grdFileRequests.Columns("RequestCleanedUp").Visible = False
    grdFileRequests.Columns("jobID").Visible = True
    grdFileRequests.Columns("CDDeliveryNumber").Visible = True
    grdFileRequests.Columns("NotifyRecipient").Visible = True
    grdFileRequests.Columns("clipreference").Width = 3500
    grdFileRequests.Columns("Request Date").Width = 1700
    grdFileRequests.Columns("Request Started").Width = 1700
    grdFileRequests.Columns("Request Completed").Width = 1700
    grdFileRequests.Columns("Request Failed").Width = 1050
    grdFileRequests.Columns("Paused").Visible = True
'    grdFileRequests.Columns("Originally On").Width = 1450
    grdFileRequests.Columns("RequestName").Width = 1300
    grdFileRequests.Columns("Store").Width = 1800
    grdFileRequests.Columns("DoNotRecordCopy").Visible = True
Case 2 'Wide
    grdFileRequests.Columns("Source Filename").Visible = False
    grdFileRequests.Columns("Comment").Width = 10550
    grdFileRequests.Columns("New Folder").Visible = False
    grdFileRequests.Columns("New Folder").Width = 3000
    grdFileRequests.Columns("NewFileName").Visible = False
'    grdFileRequests.Columns("RequestID").Visible = False
'    grdFileRequests.Columns("Source file").Visible = False
'    grdFileRequests.Columns("Source file").Width = 2400
    grdFileRequests.Columns("RequestCleanedUp").Visible = False
    grdFileRequests.Columns("jobID").Visible = False
    grdFileRequests.Columns("CDDeliveryNumber").Visible = True
    grdFileRequests.Columns("NotifyRecipient").Visible = False
    grdFileRequests.Columns("clipreference").Width = 4600
    grdFileRequests.Columns("Request Date").Width = 1700
    grdFileRequests.Columns("Request Started").Width = 1700
    grdFileRequests.Columns("Request Completed").Width = 1050
    grdFileRequests.Columns("Request Failed").Width = 1050
    grdFileRequests.Columns("Paused").Visible = True
    grdFileRequests.Columns("Originally On").Visible = True
    grdFileRequests.Columns("Originally On").Width = 1200
    grdFileRequests.Columns("Store").Width = 1600
    grdFileRequests.Columns("RequestName").Width = 1500
    grdFileRequests.Columns("DoNotRecordCopy").Visible = True
End Select

End Sub

Private Sub tabSpecs_Click(PreviousTab As Integer)

Select Case tabSpecs.Tab
    Case 0
        'chkSuspendAutoRefresh.Value = 0
        RefreshTranscodeRequests
    Case 1
        Screen.MousePointer = vbHourglass
        'chkSuspendAutoRefresh = 1
        m_blnTabUpdate = True
        DoEvents
        RefreshTranscodeSpecs
        Screen.MousePointer = vbDefault
        m_blnTabUpdate = False
        DoEvents
    Case 2
        'chkSuspendAutoRefresh = 0
        RefreshFileRequests
        If optWidth(1).Value = True Then optWidth_Click 1
        If optWidth(0).Value = True Then optWidth_Click 0
    Case 3
        'chkSuspendAutoRefresh = 0
        RefreshTechrevOutputRequests
    Case 4
        RefreshWorkflows
    Case 5
        RefreshWorkflowConfig
    Case 6
        RefreshTranscodeRequestsHybrid
        RefreshFileRequestsHybrid
End Select

m_lngTimeSinceLastRefresh = Val(txtCountDown.Text)

End Sub

Private Sub Timer1_Timer()

m_lngTimeSinceLastRefresh = m_lngTimeSinceLastRefresh - 1
cmdSearch.Caption = "Refresh (" & m_lngTimeSinceLastRefresh & ")"
If m_lngTimeSinceLastRefresh < 1 Then
    If chkSuspendAutoRefresh.Value = 0 Then cmdSearch.Value = True
    m_lngTimeSinceLastRefresh = Val(txtCountDown.Text)
End If

End Sub

Private Sub txtHeadbuildEventID_CloseUp()
    If Val(txtHeadbuildEventID.Text) <> 0 Then
        lblHeadbuildFilename.Caption = GetData("events", "clipfilename", "eventID", Val(txtHeadbuildEventID.Text))
    End If
End Sub

Private Sub txtHeadbuildEventID_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    If Val(txtHeadbuildEventID.Text) <> 0 Then
        lblHeadbuildFilename.Caption = GetData("events", "clipfilename", "eventID", Val(txtHeadbuildEventID.Text))
    End If
End If
End Sub

Sub InitialiseFileRequestWidth()
'optWidth(0).Value = True
'optWidth_Click 0
End Sub
