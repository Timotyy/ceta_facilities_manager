VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmBBCAlias 
   Caption         =   "BBC Aliases"
   ClientHeight    =   6870
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12825
   Icon            =   "frmBBCAlias.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6870
   ScaleWidth      =   12825
   WindowState     =   2  'Maximized
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   240
      TabIndex        =   1
      Top             =   5520
      Width           =   12315
      Begin VB.TextBox txtAlias 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1020
         TabIndex        =   6
         Top             =   120
         Width           =   3375
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11100
         TabIndex        =   4
         ToolTipText     =   "Close this form"
         Top             =   120
         Width           =   1155
      End
      Begin VB.TextBox txtSearch 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6300
         TabIndex        =   3
         Top             =   120
         Width           =   3375
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9840
         TabIndex        =   2
         ToolTipText     =   "Close this form"
         Top             =   120
         Width           =   1155
      End
      Begin VB.Label Label1 
         Caption         =   "Search Alias"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   60
         TabIndex        =   7
         Top             =   180
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Search Typed String"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   4800
         TabIndex        =   5
         Top             =   180
         Width           =   1515
      End
   End
   Begin MSAdodcLib.Adodc adoBBCAlias 
      Height          =   330
      Left            =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoComboBoxOptions"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdBBCAlias 
      Bindings        =   "frmBBCAlias.frx":08CA
      Height          =   4515
      Left            =   0
      TabIndex        =   0
      ToolTipText     =   "The Options on a Combo Box"
      Top             =   0
      Width           =   8385
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      BackColorOdd    =   16777152
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "bbcaliasID"
      Columns(0).Name =   "bbcaliasID"
      Columns(0).DataField=   "Column 2"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   9869
      Columns(1).Caption=   "BBC Typed String"
      Columns(1).Name =   "typedstring"
      Columns(1).DataField=   "typedstring"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   255
      Columns(2).Width=   3863
      Columns(2).Caption=   "Ceta Alias"
      Columns(2).Name =   "bbcalias"
      Columns(2).DataField=   "bbcalias"
      Columns(2).FieldLen=   50
      _ExtentX        =   14790
      _ExtentY        =   7964
      _StockProps     =   79
      Caption         =   "BBC Aliases"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmBBCAlias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdSearch_Click()

Dim l_strSQL As String

l_strSQL = "SELECT * FROM bbcalias"

l_strSQL = l_strSQL & " WHERE 1=1 "
If txtSearch.Text <> "" Then l_strSQL = l_strSQL & " AND typedstring LIKE '" & txtSearch.Text & "%'"
If txtAlias.Text <> "" Then l_strSQL = l_strSQL & " AND bbcalias LIKE '" & txtAlias.Text & "%'"

l_strSQL = l_strSQL & "  ORDER BY bbcaliasID"

adoBBCAlias.ConnectionString = g_strConnection
adoBBCAlias.RecordSource = l_strSQL
adoBBCAlias.Refresh

End Sub

Private Sub Form_Load()

cmdSearch.Value = True

End Sub

Private Sub Form_Resize()

Frame1.Top = Me.ScaleHeight - Frame1.Height - 120
Frame1.Left = Me.ScaleWidth - Frame1.Width - 120

grdBBCAlias.Height = Me.ScaleHeight - cmdClose.Height - 120 - 120
'grdBBCAlias.Width = Me.ScaleWidth - 120 - 120

End Sub

