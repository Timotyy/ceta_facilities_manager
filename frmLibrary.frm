VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLibrary 
   Caption         =   "Library"
   ClientHeight    =   12195
   ClientLeft      =   825
   ClientTop       =   2070
   ClientWidth     =   28680
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLibrary.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   12195
   ScaleWidth      =   28680
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtDriveWriterPermissionInteger 
      Height          =   285
      Left            =   22500
      TabIndex        =   361
      ToolTipText     =   "Barcode of the Tape this Tape was made from"
      Top             =   2040
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.CheckBox chkRequireCompanyIDinFolders 
      Alignment       =   1  'Right Justify
      Caption         =   "CompanyID in Paths"
      Height          =   195
      Left            =   23340
      TabIndex        =   360
      Top             =   2760
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CheckBox chkNoRestores 
      Alignment       =   1  'Right Justify
      Caption         =   "Ban from Restores"
      Height          =   195
      Left            =   23340
      TabIndex        =   359
      Top             =   2460
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CheckBox chkIncludeInStorageReport 
      Alignment       =   1  'Right Justify
      Caption         =   "Put in Storage Rep."
      Height          =   255
      Left            =   23340
      TabIndex        =   358
      Top             =   2100
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CheckBox chkUseForLiveCFM 
      Alignment       =   1  'Right Justify
      Caption         =   "Used for Live CFM"
      Height          =   255
      Left            =   23340
      TabIndex        =   357
      Top             =   1380
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CheckBox chkCanBeUsedForAutoDelete 
      Alignment       =   1  'Right Justify
      Caption         =   "Used for AutoDelete"
      Height          =   255
      Left            =   23340
      TabIndex        =   356
      Top             =   1740
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton cmdVerifyDiscstore 
      Caption         =   "Verify DiscStore"
      Height          =   315
      Left            =   19680
      TabIndex        =   353
      ToolTipText     =   "Clear the form"
      Top             =   2400
      Visible         =   0   'False
      Width           =   1755
   End
   Begin VB.TextBox txtMinimumFreePercentage 
      Height          =   285
      Left            =   22500
      TabIndex        =   351
      ToolTipText     =   "Barcode of the Tape this Tape was made from"
      Top             =   1680
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Frame fraSource 
      Caption         =   "Source"
      Height          =   2535
      Left            =   25320
      TabIndex        =   333
      Top             =   0
      Width           =   2835
      Begin VB.TextBox txtSourceBarcode 
         Height          =   285
         Left            =   1140
         TabIndex        =   339
         ToolTipText     =   "Barcode of the Tape this Tape was made from"
         Top             =   300
         Width           =   1575
      End
      Begin VB.ComboBox cmbSourceMachine 
         Height          =   315
         Left            =   1140
         TabIndex        =   338
         ToolTipText     =   "Machine the Source Tape was made on"
         Top             =   2100
         Width           =   1575
      End
      Begin VB.ComboBox cmbSourceFormat 
         Height          =   315
         Left            =   1140
         TabIndex        =   337
         ToolTipText     =   "Format of the Source Tape"
         Top             =   660
         Width           =   1575
      End
      Begin VB.ComboBox cmbSourceStandard 
         Height          =   315
         Left            =   1140
         TabIndex        =   336
         ToolTipText     =   "Standard of the Source Tape"
         Top             =   1020
         Width           =   1575
      End
      Begin VB.ComboBox cmbSourceRatio 
         Height          =   315
         Left            =   1140
         TabIndex        =   335
         ToolTipText     =   "Aspect Ratio of the Source Tape"
         Top             =   1380
         Width           =   1575
      End
      Begin VB.ComboBox cmbSourceGeometry 
         Height          =   315
         Left            =   1140
         TabIndex        =   334
         ToolTipText     =   "Geometric Linearity of the Source Tape"
         Top             =   1740
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Machine"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   345
         Top             =   2160
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Format"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   344
         Top             =   720
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Standard"
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   343
         Top             =   1080
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Ratio"
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   342
         Top             =   1440
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Geometry"
         Height          =   255
         Index           =   12
         Left            =   120
         TabIndex        =   341
         Top             =   1800
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Barcode"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   340
         Top             =   360
         Width           =   1035
      End
   End
   Begin VB.TextBox txtSerialNumber 
      Height          =   315
      Left            =   11460
      TabIndex        =   327
      ToolTipText     =   "An Additional / External Tape Number"
      Top             =   780
      Width           =   1935
   End
   Begin VB.ComboBox cmbField3 
      Height          =   315
      Left            =   1140
      TabIndex        =   325
      Tag             =   "CLEAR"
      ToolTipText     =   "The Clip Codec"
      Top             =   2280
      Width           =   3855
   End
   Begin VB.CommandButton cmdUndeleteTape 
      Caption         =   "Undelete Tape"
      Height          =   315
      Left            =   19680
      TabIndex        =   324
      ToolTipText     =   "Clear the form"
      Top             =   1680
      Width           =   1755
   End
   Begin VB.OptionButton optStationaryType 
      Alignment       =   1  'Right Justify
      Caption         =   "Long Title"
      Height          =   195
      Index           =   4
      Left            =   19680
      TabIndex        =   103
      ToolTipText     =   "Use Standard stationary"
      Top             =   780
      Width           =   1035
   End
   Begin VB.ComboBox cmbBox 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   11460
      TabIndex        =   100
      ToolTipText     =   "The Current Location of the Tape"
      Top             =   1860
      Width           =   1935
   End
   Begin VB.TextBox txtSeriesID 
      Height          =   315
      Left            =   11460
      TabIndex        =   98
      ToolTipText     =   "An Additional / External Tape Number"
      Top             =   420
      Width           =   1935
   End
   Begin VB.TextBox txtLocationJobID 
      BackColor       =   &H00F7F7F7&
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   11460
      TabIndex        =   95
      ToolTipText     =   "The Job Detail Line that caused this tape to be made"
      Top             =   2220
      Width           =   1935
   End
   Begin VB.ComboBox cmbField1 
      Height          =   315
      Left            =   1140
      TabIndex        =   92
      Tag             =   "CLEAR"
      ToolTipText     =   "The Clip Codec"
      Top             =   1560
      Width           =   3855
   End
   Begin VB.ComboBox cmbField2 
      Height          =   315
      Left            =   1140
      TabIndex        =   91
      Tag             =   "CLEAR"
      ToolTipText     =   "The Clip Codec"
      Top             =   1920
      Width           =   3855
   End
   Begin VB.CommandButton cmdTimecodeOffset 
      Caption         =   "Apply"
      Height          =   315
      Left            =   23280
      TabIndex        =   88
      Top             =   960
      Width           =   675
   End
   Begin VB.TextBox txtTimecodeOffset 
      BackColor       =   &H0000FFFF&
      Height          =   315
      Left            =   22020
      TabIndex        =   86
      ToolTipText     =   "An Additional / External Tape Number"
      Top             =   960
      Width           =   1155
   End
   Begin VB.ComboBox cmbEpisodeTo 
      Height          =   315
      Left            =   9060
      TabIndex        =   85
      ToolTipText     =   "The Episode for the Tape"
      Top             =   1140
      Width           =   1275
   End
   Begin VB.CommandButton cmdInitialise 
      Caption         =   "Initialise Media Disc"
      Height          =   315
      Left            =   19680
      TabIndex        =   83
      ToolTipText     =   "Clear the form"
      Top             =   2040
      Visible         =   0   'False
      Width           =   1755
   End
   Begin VB.PictureBox picPrint 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   2475
      Left            =   16560
      ScaleHeight     =   2475
      ScaleWidth      =   1605
      TabIndex        =   29
      Top             =   8160
      Visible         =   0   'False
      Width           =   1600
      Begin VB.CommandButton cmdPrintSetOfTwoThings 
         Caption         =   "Set of Two"
         Height          =   315
         Left            =   0
         TabIndex        =   332
         ToolTipText     =   "Print a Barcode for this Tape"
         Top             =   2160
         Width           =   1600
      End
      Begin VB.CommandButton cmdPrintBarcode2 
         Caption         =   "Barcode x 2"
         Height          =   315
         Left            =   0
         TabIndex        =   321
         ToolTipText     =   "Print a Barcode for this Tape"
         Top             =   1440
         Width           =   1600
      End
      Begin VB.CommandButton cmdPrintSetOfThings 
         Caption         =   "Set of Three"
         Height          =   315
         Left            =   0
         TabIndex        =   78
         ToolTipText     =   "Print a Barcode for this Tape"
         Top             =   1800
         Width           =   1600
      End
      Begin VB.CommandButton cmdPrintBarcode 
         Caption         =   "Barcode"
         Height          =   315
         Left            =   0
         TabIndex        =   33
         ToolTipText     =   "Print a Barcode for this Tape"
         Top             =   1080
         Width           =   1600
      End
      Begin VB.CommandButton cmdLabels 
         Caption         =   "Standard Labels..."
         Height          =   315
         Left            =   0
         TabIndex        =   32
         ToolTipText     =   "Make VT Labels"
         Top             =   0
         Width           =   1600
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Record Report"
         Height          =   315
         Left            =   0
         TabIndex        =   31
         ToolTipText     =   "Print this Tape - topmost tab determines which report is printed"
         Top             =   720
         Width           =   1600
      End
      Begin VB.CommandButton cmdCDLabels 
         Caption         =   "CD Labels..."
         Height          =   315
         Left            =   0
         TabIndex        =   30
         ToolTipText     =   "Make CD Labels"
         Top             =   360
         Width           =   1600
      End
   End
   Begin VB.TextBox txtReference 
      Height          =   315
      Left            =   6000
      TabIndex        =   79
      ToolTipText     =   "An Additional / External Tape Number"
      Top             =   2220
      Width           =   4335
   End
   Begin VB.OptionButton optStationaryType 
      Alignment       =   1  'Right Justify
      Caption         =   "Extended"
      Height          =   195
      Index           =   3
      Left            =   19680
      TabIndex        =   77
      ToolTipText     =   "Use Standard stationary"
      Top             =   1020
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.ComboBox cmbShelf 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   11460
      TabIndex        =   76
      ToolTipText     =   "The Current Location of the Tape"
      Top             =   1500
      Width           =   1935
   End
   Begin VB.OptionButton optStationaryType 
      Alignment       =   1  'Right Justify
      Caption         =   "Blank"
      Height          =   195
      Index           =   2
      Left            =   19680
      TabIndex        =   75
      ToolTipText     =   "Use Standard stationary"
      Top             =   540
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.OptionButton optStationaryType 
      Alignment       =   1  'Right Justify
      Caption         =   "BBC"
      Height          =   195
      Index           =   1
      Left            =   19680
      TabIndex        =   74
      ToolTipText     =   "Use BBC stationary"
      Top             =   300
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.OptionButton optStationaryType 
      Alignment       =   1  'Right Justify
      Caption         =   "Standard"
      Height          =   195
      Index           =   0
      Left            =   19680
      TabIndex        =   73
      ToolTipText     =   "Use Standard stationary"
      Top             =   60
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.PictureBox picVideoSettings 
      BorderStyle     =   0  'None
      Height          =   2835
      Left            =   13560
      ScaleHeight     =   2835
      ScaleWidth      =   3195
      TabIndex        =   55
      Top             =   60
      Width           =   3195
      Begin VB.ComboBox cmbStockManufacturer 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1140
         TabIndex        =   354
         ToolTipText     =   "The Tape Type"
         Top             =   720
         Width           =   2055
      End
      Begin VB.ComboBox cmbRecordFormat 
         Height          =   315
         Left            =   1140
         TabIndex        =   62
         ToolTipText     =   "The Format of this Tape"
         Top             =   0
         Width           =   2055
      End
      Begin VB.ComboBox cmbCopyType 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1140
         TabIndex        =   61
         ToolTipText     =   "The Tape Type"
         Top             =   1080
         Width           =   2055
      End
      Begin VB.ComboBox cmbStockCode 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1140
         TabIndex        =   60
         ToolTipText     =   "The Tape Type"
         Top             =   360
         Width           =   2055
      End
      Begin VB.ComboBox cmbRecordStandard 
         Height          =   315
         Left            =   1140
         TabIndex        =   59
         ToolTipText     =   "The Standard of this Tape"
         Top             =   1440
         Width           =   2055
      End
      Begin VB.ComboBox cmbRecordRatio 
         Height          =   315
         Left            =   1140
         TabIndex        =   58
         ToolTipText     =   "The shape of the screen the video is set up to use"
         Top             =   1800
         Width           =   2055
      End
      Begin VB.ComboBox cmbRecordGeometry 
         Height          =   315
         Left            =   1140
         TabIndex        =   57
         ToolTipText     =   "The shape of the picture the video is set up to use"
         Top             =   2160
         Width           =   2055
      End
      Begin VB.ComboBox cmbRecordMachine 
         Height          =   315
         Left            =   1140
         TabIndex        =   56
         ToolTipText     =   "The Machine this Tape was recorded in"
         Top             =   2520
         Width           =   2055
      End
      Begin VB.Label lblCaption 
         Caption         =   "Stock Maker"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   75
         Left            =   0
         TabIndex        =   355
         Top             =   780
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Format"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   18
         Left            =   0
         TabIndex        =   69
         Top             =   60
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Copy Type"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   0
         TabIndex        =   68
         Top             =   1140
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Stock Code"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   86
         Left            =   0
         TabIndex        =   67
         Top             =   420
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Standard"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   17
         Left            =   0
         TabIndex        =   66
         Top             =   1500
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Machine"
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   19
         Left            =   0
         TabIndex        =   65
         Top             =   2580
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Geometry"
         Height          =   255
         Index           =   13
         Left            =   0
         TabIndex        =   64
         Top             =   2220
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Ratio"
         Height          =   255
         Index           =   15
         Left            =   0
         TabIndex        =   63
         Top             =   1860
         Width           =   1035
      End
   End
   Begin VB.CommandButton cmdAssignToLibrary 
      Caption         =   "Assign To Library"
      Height          =   315
      Left            =   19680
      TabIndex        =   49
      ToolTipText     =   "Clear the form"
      Top             =   1320
      Width           =   1755
   End
   Begin VB.ComboBox cmbEpisode 
      Height          =   315
      Left            =   6000
      TabIndex        =   45
      ToolTipText     =   "The Episode for the Tape"
      Top             =   1140
      Width           =   1155
   End
   Begin VB.ComboBox cmbSeries 
      Height          =   315
      Left            =   6000
      TabIndex        =   44
      ToolTipText     =   "The Series for the Tape"
      Top             =   780
      Width           =   1155
   End
   Begin VB.ComboBox cmbSet 
      Height          =   315
      Left            =   9060
      TabIndex        =   43
      ToolTipText     =   "The Series for the Tape"
      Top             =   780
      Width           =   1275
   End
   Begin VB.Frame fraIDfields 
      Height          =   2535
      Left            =   16860
      TabIndex        =   38
      Top             =   0
      Width           =   2655
      Begin VB.CheckBox chkFalseRecord 
         Alignment       =   1  'Right Justify
         Caption         =   "False Record"
         Height          =   255
         Left            =   120
         TabIndex        =   104
         Top             =   2040
         Width           =   1335
      End
      Begin VB.CheckBox chkTapeIncomplete 
         Alignment       =   1  'Right Justify
         Caption         =   "Incomplete"
         Height          =   255
         Left            =   300
         TabIndex        =   52
         Top             =   1740
         Width           =   1155
      End
      Begin VB.Label lblCompanyID 
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   1380
         TabIndex        =   330
         Tag             =   "CLEARFIELDS"
         Top             =   480
         Width           =   975
      End
      Begin VB.Label lblCaption 
         Caption         =   "Company ID"
         Height          =   195
         Index           =   71
         Left            =   120
         TabIndex        =   329
         Top             =   480
         Width           =   1155
      End
      Begin VB.Label lblInLibrary 
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   1680
         TabIndex        =   51
         Tag             =   "CLEARFIELDS"
         Top             =   1380
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Normally In Library?"
         Height          =   195
         Index           =   34
         Left            =   120
         TabIndex        =   50
         Top             =   1380
         Width           =   1455
      End
      Begin VB.Label lblLibraryID 
         Height          =   195
         Left            =   1380
         TabIndex        =   42
         Tag             =   "CLEARFIELDS"
         Top             =   180
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Library ID"
         Height          =   195
         Index           =   90
         Left            =   120
         TabIndex        =   41
         Top             =   180
         Width           =   1155
      End
      Begin VB.Label lblTechReviewAvalable 
         Caption         =   "Tech Review"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000C000&
         Height          =   255
         Left            =   120
         TabIndex        =   40
         Top             =   780
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.Label lblRollcallAvailable 
         Caption         =   "Rollcall Data"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   195
         Left            =   120
         TabIndex        =   39
         Top             =   1080
         Visible         =   0   'False
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdRefreshEvents 
      Caption         =   "REFRESH EVENTS"
      Height          =   315
      Left            =   3660
      TabIndex        =   36
      Top             =   11820
      Visible         =   0   'False
      Width           =   1515
   End
   Begin MSAdodcLib.Adodc adoCompany 
      Height          =   330
      Left            =   6840
      Top             =   60
      Visible         =   0   'False
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCompany"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox txtProjectNumber 
      BackColor       =   &H00C0FFFF&
      Height          =   315
      Left            =   1140
      TabIndex        =   4
      ToolTipText     =   "Job which caused this tape to be made"
      Top             =   840
      Width           =   3855
   End
   Begin VB.CommandButton cmdEditBarcode 
      Caption         =   "!"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4800
      TabIndex        =   1
      ToolTipText     =   "Re-barcode this tape"
      Top             =   120
      Width           =   195
   End
   Begin VB.TextBox txtJobDetailID 
      BackColor       =   &H00F7F7F7&
      ForeColor       =   &H00808080&
      Height          =   315
      Left            =   3720
      TabIndex        =   5
      ToolTipText     =   "The Job Detail Line that caused this tape to be made"
      Top             =   1200
      Width           =   1275
   End
   Begin VB.PictureBox picFooter 
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   1260
      ScaleHeight     =   315
      ScaleWidth      =   17415
      TabIndex        =   23
      Top             =   11460
      Width           =   17415
      Begin VB.CommandButton cmdBulkDuplicate 
         Caption         =   "Bulk Duplicate"
         Height          =   315
         Left            =   12480
         TabIndex        =   97
         ToolTipText     =   "Duplicate the Tape info into a new Tape"
         Top             =   0
         Width           =   1275
      End
      Begin VB.CommandButton cmdCopyToBarcode 
         Caption         =   "Copy Barcoded"
         Height          =   315
         Left            =   6420
         TabIndex        =   89
         ToolTipText     =   "Copy this tape to a new Barcoded Tape"
         Top             =   0
         Width           =   1275
      End
      Begin VB.CommandButton cmdExportLog 
         Caption         =   "Export Rapids"
         Height          =   315
         Left            =   8940
         TabIndex        =   82
         ToolTipText     =   "Clear the form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdImportLog 
         Caption         =   "Import Rapids"
         Height          =   315
         Left            =   7740
         TabIndex        =   81
         ToolTipText     =   "Clear the form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdTransfer 
         Caption         =   "Transfer"
         Height          =   315
         Left            =   3600
         TabIndex        =   37
         ToolTipText     =   "Overwrite this information from another Tape"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdNew 
         Caption         =   "New"
         Height          =   315
         Left            =   10200
         TabIndex        =   35
         ToolTipText     =   "Overwrite this information"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrintMenu 
         Caption         =   "   Print..."
         Height          =   315
         Left            =   11400
         TabIndex        =   34
         ToolTipText     =   "Overwrite this information"
         Top             =   0
         Width           =   1035
      End
      Begin VB.CommandButton cmdOverwriteExisting 
         Caption         =   "Overwrite"
         Height          =   315
         Left            =   2400
         TabIndex        =   12
         ToolTipText     =   "Overwrite an existing tapes information with that of this tape"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdDeleteTape 
         Caption         =   "Delete Tape"
         Height          =   315
         Left            =   1200
         TabIndex        =   11
         ToolTipText     =   "Delete this Tape from the system"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdCopy 
         Caption         =   "Copy Unbarcoded"
         Height          =   315
         Left            =   4800
         TabIndex        =   14
         ToolTipText     =   "Copy this tape to a new Unbarcoded Tape"
         Top             =   0
         Width           =   1575
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   0
         TabIndex        =   10
         ToolTipText     =   "Clear the form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   16200
         TabIndex        =   16
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         Height          =   315
         Left            =   15000
         TabIndex        =   15
         ToolTipText     =   "Save changes to this Tape"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdDuplicate 
         Caption         =   "Duplicate"
         Height          =   315
         Left            =   13800
         TabIndex        =   13
         ToolTipText     =   "Duplicate the Tape info into a new Tape"
         Top             =   0
         Width           =   1155
      End
   End
   Begin VB.ComboBox cmbVersion 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6000
      TabIndex        =   9
      ToolTipText     =   "The Version for the Tape"
      Top             =   1860
      Width           =   4335
   End
   Begin VB.TextBox txtSubTitle 
      Height          =   315
      Left            =   6000
      TabIndex        =   8
      ToolTipText     =   "The sub title (if known)"
      Top             =   1500
      Width           =   4335
   End
   Begin VB.ComboBox cmbLocation 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   11460
      TabIndex        =   6
      ToolTipText     =   "The Current Location of the Tape"
      Top             =   1140
      Width           =   1935
   End
   Begin VB.TextBox txtForeignRef 
      Height          =   315
      Left            =   1140
      TabIndex        =   2
      ToolTipText     =   "An Additional / External Tape Number"
      Top             =   480
      Width           =   3855
   End
   Begin VB.TextBox txtJobID 
      BackColor       =   &H00FFC0C0&
      Height          =   315
      Left            =   1140
      TabIndex        =   3
      ToolTipText     =   "Job which caused this tape to be made"
      Top             =   1200
      Width           =   1395
   End
   Begin VB.TextBox txtBarcode 
      BackColor       =   &H00C0FFC0&
      Height          =   315
      Left            =   1140
      TabIndex        =   0
      ToolTipText     =   "Scan or Type the Tape Barcode"
      Top             =   120
      Width           =   3555
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Bindings        =   "frmLibrary.frx":08CA
      Height          =   315
      Left            =   6000
      TabIndex        =   7
      ToolTipText     =   "The company this job is for"
      Top             =   60
      Width           =   4335
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      ExtraHeight     =   79
      Columns.Count   =   5
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "accountcode"
      Columns(2).Name =   "accountcode"
      Columns(2).DataField=   "accountcode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "telephone"
      Columns(3).Name =   "telephone"
      Columns(3).DataField=   "telephone"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "cetaclientcode"
      Columns(4).Name =   "cetaclientcode"
      Columns(4).DataField=   "cetaclientcode"
      Columns(4).FieldLen=   256
      _ExtentX        =   7646
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin MSComCtl2.DTPicker datMadeOnDate 
      Height          =   315
      Left            =   11460
      TabIndex        =   53
      Top             =   60
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   129105921
      CurrentDate     =   38469
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo txtTitle 
      Height          =   315
      Left            =   6000
      TabIndex        =   102
      ToolTipText     =   "The company this tape belongs to"
      Top             =   420
      Width           =   4335
      DataFieldList   =   "title"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "masterseriestitleID"
      Columns(0).Name =   "masterseriestitleID"
      Columns(0).DataField=   "masterseriestitleID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   1773
      Columns(1).Caption=   "seriesID"
      Columns(1).Name =   "seriesID"
      Columns(1).DataField=   "seriesID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   8149
      Columns(2).Caption=   "title"
      Columns(2).Name =   "title"
      Columns(2).DataField=   "title"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "companyID"
      Columns(3).Name =   "companyID"
      Columns(3).DataField=   "companyID"
      Columns(3).FieldLen=   256
      _ExtentX        =   7646
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16777215
      DataFieldToDisplay=   "title"
   End
   Begin TabDlg.SSTab tabLibraryInfo 
      Height          =   8295
      Left            =   0
      TabIndex        =   105
      ToolTipText     =   "Specific Details about the tape"
      Top             =   3060
      Width           =   25515
      _ExtentX        =   45006
      _ExtentY        =   14631
      _Version        =   393216
      Style           =   1
      Tabs            =   6
      TabsPerRow      =   6
      TabHeight       =   520
      TabCaption(0)   =   "Events"
      TabPicture(0)   =   "frmLibrary.frx":08E3
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblEventNotes"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "grdEvents"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "adoEvents"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "ddnEvents"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "adoEventsList"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "ddnEventType"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "ddnRatio"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "cmdCopyClips"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "chkNoTimecodeShuffle"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "picAudioresize"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "picEventResize"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "txtNotes"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "chkNoSorting"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).ControlCount=   13
      TabCaption(1)   =   "History"
      TabPicture(1)   =   "frmLibrary.frx":08FF
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdCorrectLocation"
      Tab(1).Control(1)=   "adoTransactions"
      Tab(1).Control(2)=   "grdTransactions"
      Tab(1).Control(3)=   "lblCaption(36)"
      Tab(1).Control(4)=   "lblCaption(33)"
      Tab(1).Control(5)=   "lblCreatedUser"
      Tab(1).Control(6)=   "lblCreatedDate"
      Tab(1).Control(7)=   "lblModifiedUser"
      Tab(1).Control(8)=   "lblModifiedDate"
      Tab(1).ControlCount=   9
      TabCaption(2)   =   "Tech. Review Parameters"
      TabPicture(2)   =   "frmLibrary.frx":091B
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraEBULoudness"
      Tab(2).Control(1)=   "cmdTechReviewPdf"
      Tab(2).Control(2)=   "cmdTechReviewCopyFaults"
      Tab(2).Control(3)=   "cmdDropTechReview"
      Tab(2).Control(4)=   "chkSpotCheckRev"
      Tab(2).Control(5)=   "cmdMakeTechReview"
      Tab(2).Control(6)=   "cmbTechReviewList"
      Tab(2).Control(7)=   "fraTechReview"
      Tab(2).ControlCount=   8
      TabCaption(3)   =   "Tech. Review Faults && Conclusions"
      TabPicture(3)   =   "frmLibrary.frx":0937
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraTechReviewConclusions"
      Tab(3).Control(1)=   "adoFaultsList"
      Tab(3).Control(2)=   "ddnFaultsList"
      Tab(3).Control(3)=   "adoOldFaults"
      Tab(3).Control(4)=   "adoFaults"
      Tab(3).Control(5)=   "grdFaults"
      Tab(3).ControlCount=   6
      TabCaption(4)   =   "Jobs Requiring Tape"
      TabPicture(4)   =   "frmLibrary.frx":0953
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Picture1"
      Tab(4).Control(1)=   "adoJobsForTape"
      Tab(4).Control(2)=   "grdJobsForTape"
      Tab(4).ControlCount=   3
      TabCaption(5)   =   "Advanced"
      TabPicture(5)   =   "frmLibrary.frx":096F
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "txtInternalReference"
      Tab(5).Control(1)=   "cmbChannelCue"
      Tab(5).Control(2)=   "lblCaption(114)"
      Tab(5).Control(3)=   "lblCaption(28)"
      Tab(5).Control(4)=   "lblProcessingSubtitle"
      Tab(5).ControlCount=   5
      Begin VB.TextBox txtInternalReference 
         BackColor       =   &H00F7F7F7&
         Height          =   285
         Left            =   -73500
         TabIndex        =   348
         ToolTipText     =   "The Job Detail Line that caused this tape to be made"
         Top             =   780
         Width           =   1215
      End
      Begin VB.ComboBox cmbChannelCue 
         Height          =   315
         Left            =   -73500
         TabIndex        =   347
         ToolTipText     =   "Contents of the Cue Track on this Tape"
         Top             =   420
         Width           =   2715
      End
      Begin VB.CheckBox chkNoSorting 
         Caption         =   "No Refreshing this grd after new line is added"
         Height          =   315
         Left            =   7620
         TabIndex        =   331
         Top             =   480
         Width           =   3975
      End
      Begin VB.Frame fraEBULoudness 
         Caption         =   "EBU R128 Loudness"
         Height          =   2055
         Left            =   -60420
         TabIndex        =   312
         Top             =   600
         Width           =   4215
         Begin VB.ComboBox cmbEBU128Loudness51ME 
            Height          =   315
            Left            =   1320
            TabIndex        =   316
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   1500
            Width           =   2775
         End
         Begin VB.ComboBox cmbEBU128Loudness51Main 
            Height          =   315
            Left            =   1320
            TabIndex        =   315
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   1140
            Width           =   2775
         End
         Begin VB.ComboBox cmbEBU128LoudnessStereoME 
            Height          =   315
            Left            =   1320
            TabIndex        =   314
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   780
            Width           =   2775
         End
         Begin VB.ComboBox cmbEBU128LoudnessStereoMain 
            Height          =   315
            Left            =   1320
            TabIndex        =   313
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   420
            Width           =   2775
         End
         Begin VB.Label lblCaption 
            Caption         =   "5.1 M&&E"
            Height          =   255
            Index           =   104
            Left            =   120
            TabIndex        =   320
            Top             =   1560
            Width           =   915
         End
         Begin VB.Label lblCaption 
            Caption         =   "5.1 Main"
            Height          =   255
            Index           =   103
            Left            =   120
            TabIndex        =   319
            Top             =   1200
            Width           =   915
         End
         Begin VB.Label lblCaption 
            Caption         =   "Stereo M&&E"
            Height          =   255
            Index           =   102
            Left            =   120
            TabIndex        =   318
            Top             =   840
            Width           =   915
         End
         Begin VB.Label lblCaption 
            Caption         =   "Stereo Main"
            Height          =   255
            Index           =   35
            Left            =   120
            TabIndex        =   317
            Top             =   480
            Width           =   915
         End
      End
      Begin VB.TextBox txtNotes 
         BorderStyle     =   0  'None
         Height          =   1395
         Left            =   180
         MultiLine       =   -1  'True
         TabIndex        =   168
         ToolTipText     =   "Notes about the Tape"
         Top             =   6180
         Width           =   6975
      End
      Begin VB.PictureBox picEventResize 
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   180
         ScaleHeight     =   315
         ScaleWidth      =   6975
         TabIndex        =   162
         Top             =   4980
         Width           =   6975
         Begin VB.TextBox txtTotalDuration 
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   5400
            TabIndex        =   164
            ToolTipText     =   "The total duration of the Tape"
            Top             =   0
            Width           =   1575
         End
         Begin VB.TextBox txtProgDuration 
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   2580
            TabIndex        =   163
            ToolTipText     =   "The Main Programme Duration"
            Top             =   0
            Width           =   1575
         End
         Begin VB.Label lblCaption 
            Caption         =   "Notes"
            Height          =   255
            Index           =   30
            Left            =   0
            TabIndex        =   167
            Top             =   0
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Total Duration"
            Height          =   255
            Index           =   31
            Left            =   4260
            TabIndex        =   166
            Top             =   0
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Prog Duration"
            Height          =   255
            Index           =   32
            Left            =   1440
            TabIndex        =   165
            Top             =   0
            Width           =   1035
         End
      End
      Begin VB.Frame fraTechReviewConclusions 
         BorderStyle     =   0  'None
         Height          =   6795
         Left            =   -67680
         TabIndex        =   147
         Top             =   360
         Width           =   7035
         Begin VB.OptionButton optReviewConclusion 
            BackColor       =   &H0080FF80&
            Caption         =   "Accept"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   5220
            TabIndex        =   156
            ToolTipText     =   "Tape meets required specifications"
            Top             =   5580
            Width           =   1335
         End
         Begin VB.OptionButton optReviewConclusion 
            BackColor       =   &H00C0C0FF&
            Caption         =   "Accepted by Client"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   2040
            TabIndex        =   155
            ToolTipText     =   "Tape has failed to meet required specifications, but client has accepted it"
            Top             =   5580
            Width           =   2595
         End
         Begin VB.OptionButton optReviewConclusion 
            BackColor       =   &H000000FF&
            Caption         =   "Reject"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   300
            TabIndex        =   154
            ToolTipText     =   "Reject the Tape as Failing to meet required specicfications"
            Top             =   5580
            Width           =   1215
         End
         Begin VB.TextBox txtTechNotesGeneral 
            Height          =   1695
            Left            =   0
            MultiLine       =   -1  'True
            TabIndex        =   153
            ToolTipText     =   "General Notes and Summary for the Tewchnical Review"
            Top             =   3780
            Width           =   6915
         End
         Begin VB.TextBox txtTechNotesAudio 
            Height          =   1575
            Left            =   0
            MultiLine       =   -1  'True
            TabIndex        =   152
            ToolTipText     =   "Notes regarding Audio on the Technical Review"
            Top             =   2040
            Width           =   6915
         End
         Begin VB.TextBox txtTechNotesVideo 
            Height          =   1635
            Left            =   0
            MultiLine       =   -1  'True
            TabIndex        =   151
            ToolTipText     =   "Notes regarding Video on the Technical Review"
            Top             =   240
            Width           =   6915
         End
         Begin VB.Frame fraiTunes 
            BorderStyle     =   0  'None
            Height          =   615
            Left            =   60
            TabIndex        =   148
            Top             =   5880
            Width           =   6735
            Begin VB.OptionButton optReviewConclusion 
               BackColor       =   &H0080FF80&
               Caption         =   "Accept for iTunes"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   4
               Left            =   2340
               TabIndex        =   150
               ToolTipText     =   "Tape meets required specifications"
               Top             =   120
               Width           =   1935
            End
            Begin VB.OptionButton optReviewConclusion 
               BackColor       =   &H000000FF&
               Caption         =   "Reject for iTunes"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   3
               Left            =   240
               TabIndex        =   149
               ToolTipText     =   "Reject the Tape as Failing to meet required specicfications"
               Top             =   120
               Width           =   1935
            End
         End
         Begin VB.Label lblCaption 
            Caption         =   "General Notes"
            Height          =   255
            Index           =   78
            Left            =   60
            TabIndex        =   159
            Top             =   3600
            Width           =   1155
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio Notes"
            Height          =   255
            Index           =   77
            Left            =   60
            TabIndex        =   158
            Top             =   1860
            Width           =   1155
         End
         Begin VB.Label lblCaption 
            Caption         =   "Video Notes"
            Height          =   255
            Index           =   76
            Left            =   60
            TabIndex        =   157
            Top             =   60
            Width           =   855
         End
      End
      Begin VB.CommandButton cmdTechReviewPdf 
         Caption         =   "Make TR  PDF"
         Enabled         =   0   'False
         Height          =   315
         Left            =   -66360
         TabIndex        =   146
         ToolTipText     =   "Construct a PDF of the Tech Review"
         Top             =   1740
         Width           =   1695
      End
      Begin VB.CommandButton cmdTechReviewCopyFaults 
         Caption         =   "Copy Fault List"
         Enabled         =   0   'False
         Height          =   315
         Left            =   -66360
         TabIndex        =   145
         ToolTipText     =   "Copy the Tech Review Fault List from another Tape to this Tape"
         Top             =   2160
         Width           =   1695
      End
      Begin VB.CommandButton cmdDropTechReview 
         Caption         =   "Delete Tech Review"
         Enabled         =   0   'False
         Height          =   315
         Left            =   -66360
         TabIndex        =   144
         ToolTipText     =   "Make a Tech Review for this Tape"
         Top             =   1320
         Width           =   1695
      End
      Begin VB.CommandButton cmdCorrectLocation 
         Caption         =   "Rollback Movement Error"
         Height          =   315
         Left            =   -69960
         TabIndex        =   143
         ToolTipText     =   "Clear the form"
         Top             =   780
         Width           =   2235
      End
      Begin VB.PictureBox Picture1 
         Height          =   1155
         Left            =   -71100
         ScaleHeight     =   1155
         ScaleWidth      =   15
         TabIndex        =   142
         Top             =   3000
         Width           =   15
      End
      Begin VB.PictureBox picAudioresize 
         BorderStyle     =   0  'None
         Height          =   1395
         Left            =   120
         ScaleHeight     =   1395
         ScaleWidth      =   17595
         TabIndex        =   112
         Top             =   3360
         Width           =   17595
         Begin VB.CheckBox chkTapeTextless 
            Caption         =   "Textless"
            Height          =   255
            Left            =   11700
            TabIndex        =   322
            Top             =   1080
            Width           =   1245
         End
         Begin VB.ComboBox cmbChannel1 
            Height          =   315
            Left            =   600
            TabIndex        =   127
            ToolTipText     =   "Contents of Audio 1 on this Tape"
            Top             =   0
            Width           =   1935
         End
         Begin VB.ComboBox cmbChannel2 
            Height          =   315
            Left            =   600
            TabIndex        =   126
            ToolTipText     =   "Contents of Audio 2 on this Tape"
            Top             =   360
            Width           =   1935
         End
         Begin VB.ComboBox cmbChannel3 
            Height          =   315
            Left            =   600
            TabIndex        =   125
            ToolTipText     =   "Contents of Audio 3 on this Tape"
            Top             =   720
            Width           =   1935
         End
         Begin VB.ComboBox cmbChannel5 
            Height          =   315
            Left            =   3300
            TabIndex        =   124
            ToolTipText     =   "Contents of Audio 5 on this Tape"
            Top             =   0
            Width           =   1935
         End
         Begin VB.ComboBox cmbChannel6 
            Height          =   315
            Left            =   3300
            TabIndex        =   123
            ToolTipText     =   "Contents of Audio 6 on this Tape"
            Top             =   360
            Width           =   1935
         End
         Begin VB.ComboBox cmbChannel7 
            Height          =   315
            Left            =   3300
            TabIndex        =   122
            ToolTipText     =   "Contents of Audio 7 on this Tape"
            Top             =   720
            Width           =   1935
         End
         Begin VB.ComboBox cmbChannel8 
            Height          =   315
            Left            =   3300
            TabIndex        =   121
            ToolTipText     =   "Contents of Audio 8 on this Tape"
            Top             =   1080
            Width           =   1935
         End
         Begin VB.ComboBox cmbChannel10 
            Height          =   315
            Left            =   6060
            TabIndex        =   120
            ToolTipText     =   "Contents of Audio 8 on this Tape"
            Top             =   360
            Width           =   1935
         End
         Begin VB.ComboBox cmbChannel9 
            Height          =   315
            Left            =   6060
            TabIndex        =   119
            ToolTipText     =   "Contents of Audio 8 on this Tape"
            Top             =   0
            Width           =   1935
         End
         Begin VB.ComboBox cmbChannel12 
            Height          =   315
            Left            =   6060
            TabIndex        =   118
            ToolTipText     =   "Contents of Audio 8 on this Tape"
            Top             =   1080
            Width           =   1935
         End
         Begin VB.ComboBox cmbChannel11 
            Height          =   315
            Left            =   6060
            TabIndex        =   117
            ToolTipText     =   "Contents of Audio 8 on this Tape"
            Top             =   720
            Width           =   1935
         End
         Begin VB.ComboBox cmbAudioStandard 
            BackColor       =   &H00FFC0C0&
            Height          =   315
            Left            =   9540
            TabIndex        =   116
            ToolTipText     =   "Audio Standard"
            Top             =   0
            Width           =   1935
         End
         Begin VB.ComboBox cmbTimeCode 
            Height          =   315
            Left            =   9540
            TabIndex        =   115
            ToolTipText     =   "Type of Timecode on this Tape"
            Top             =   360
            Width           =   1935
         End
         Begin VB.CheckBox chkDropFrame 
            Caption         =   "Unadjusted"
            Enabled         =   0   'False
            Height          =   315
            Left            =   9540
            TabIndex        =   114
            ToolTipText     =   "Audio Unadjusted?"
            Top             =   720
            Width           =   1335
         End
         Begin VB.ComboBox cmbChannel4 
            Height          =   315
            Left            =   600
            TabIndex        =   113
            ToolTipText     =   "Contents of Audio 4 on this Tape"
            Top             =   1080
            Width           =   1935
         End
         Begin VB.Label lblCaption 
            Caption         =   "Ch #1"
            Height          =   255
            Index           =   25
            Left            =   60
            TabIndex        =   141
            Top             =   60
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Ch #2"
            Height          =   255
            Index           =   24
            Left            =   60
            TabIndex        =   140
            Top             =   420
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Ch #3"
            Height          =   255
            Index           =   23
            Left            =   60
            TabIndex        =   139
            Top             =   780
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Ch #6"
            Height          =   255
            Index           =   27
            Left            =   2760
            TabIndex        =   138
            Top             =   420
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Ch #5"
            Height          =   255
            Index           =   21
            Left            =   2760
            TabIndex        =   137
            Top             =   60
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Ch #7"
            Height          =   255
            Index           =   26
            Left            =   2760
            TabIndex        =   136
            Top             =   780
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Ch #8"
            Height          =   255
            Index           =   20
            Left            =   2760
            TabIndex        =   135
            Top             =   1140
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Ch #10"
            Height          =   255
            Index           =   94
            Left            =   5460
            TabIndex        =   134
            Top             =   420
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Ch #9"
            Height          =   255
            Index           =   74
            Left            =   5460
            TabIndex        =   133
            Top             =   60
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Ch #12"
            Height          =   255
            Index           =   96
            Left            =   5460
            TabIndex        =   132
            Top             =   1140
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Ch #11"
            Height          =   255
            Index           =   95
            Left            =   5460
            TabIndex        =   131
            Top             =   780
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio Std."
            Height          =   315
            Index           =   88
            Left            =   8580
            TabIndex        =   130
            Top             =   60
            Width           =   1155
         End
         Begin VB.Label lblCaption 
            Caption         =   "Time Code"
            Height          =   255
            Index           =   29
            Left            =   8580
            TabIndex        =   129
            Top             =   420
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Ch #4"
            Height          =   255
            Index           =   22
            Left            =   60
            TabIndex        =   128
            Top             =   1140
            Width           =   1035
         End
      End
      Begin VB.CheckBox chkNoTimecodeShuffle 
         Caption         =   "No Timecode Shuffling Outs to Ins"
         Height          =   315
         Left            =   3780
         TabIndex        =   111
         Top             =   480
         Width           =   3975
      End
      Begin VB.CheckBox chkSpotCheckRev 
         Caption         =   "Full QC?"
         Height          =   315
         Left            =   -66360
         TabIndex        =   110
         Top             =   900
         Width           =   1815
      End
      Begin VB.CommandButton cmdCopyClips 
         Caption         =   "Copy Selected Clips to another Tape"
         Height          =   315
         Left            =   120
         TabIndex        =   109
         Top             =   480
         Width           =   2835
      End
      Begin VB.CommandButton cmdMakeTechReview 
         Caption         =   "Make Tech Review"
         Height          =   315
         Left            =   -66360
         TabIndex        =   106
         ToolTipText     =   "Make a Tech Review for this Tape"
         Top             =   480
         Width           =   1695
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnRatio 
         Height          =   1755
         Left            =   7620
         TabIndex        =   107
         Top             =   5160
         Width           =   1215
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   3200
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   2143
         _ExtentY        =   3096
         _StockProps     =   77
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnEventType 
         Height          =   1755
         Left            =   2520
         TabIndex        =   108
         Top             =   5520
         Width           =   1035
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   3200
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1826
         _ExtentY        =   3096
         _StockProps     =   77
         DataFieldToDisplay=   "Column 0"
      End
      Begin MSAdodcLib.Adodc adoFaultsList 
         Height          =   330
         Left            =   -72960
         Top             =   2700
         Visible         =   0   'False
         Width           =   2820
         _ExtentX        =   4974
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoFaultsList"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnFaultsList 
         Bindings        =   "frmLibrary.frx":098B
         Height          =   1755
         Left            =   -74460
         TabIndex        =   160
         Top             =   1740
         Width           =   6195
         DataFieldList   =   "description"
         ListAutoValidate=   0   'False
         ListWidthAutoSize=   0   'False
         MaxDropDownItems=   30
         ListWidth       =   8440
         _Version        =   196617
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   8281
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "description"
         Columns(0).DataField=   "description"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   10927
         _ExtentY        =   3096
         _StockProps     =   77
         DataFieldToDisplay=   "description"
      End
      Begin MSAdodcLib.Adodc adoEventsList 
         Height          =   330
         Left            =   3480
         Top             =   1500
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   8
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoEventList"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnEvents 
         Bindings        =   "frmLibrary.frx":09A7
         Height          =   1755
         Left            =   1020
         TabIndex        =   161
         Top             =   1140
         Width           =   6675
         DataFieldList   =   "description"
         ListAutoValidate=   0   'False
         ListWidthAutoSize=   0   'False
         MaxDropDownItems=   16
         ListWidth       =   11324
         _Version        =   196617
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   3
         Columns(0).Width=   6165
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "description"
         Columns(0).DataField=   "description"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2461
         Columns(1).Caption=   "Start Timecode"
         Columns(1).Name =   "information"
         Columns(1).DataField=   "information"
         Columns(1).FieldLen=   256
         Columns(2).Width=   2566
         Columns(2).Caption=   "End Timecode"
         Columns(2).Name =   "format"
         Columns(2).DataField=   "format"
         Columns(2).FieldLen=   256
         _ExtentX        =   11774
         _ExtentY        =   3096
         _StockProps     =   77
         DataFieldToDisplay=   "description"
      End
      Begin MSAdodcLib.Adodc adoOldFaults 
         Height          =   330
         Left            =   -58320
         Top             =   660
         Visible         =   0   'False
         Width           =   3000
         _ExtentX        =   5292
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoOldFaults"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoFaults 
         Height          =   330
         Left            =   -73620
         Top             =   600
         Visible         =   0   'False
         Width           =   2760
         _ExtentX        =   4868
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoFaults"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoTransactions 
         Height          =   330
         Left            =   -70140
         Top             =   1260
         Visible         =   0   'False
         Width           =   2100
         _ExtentX        =   3704
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoTrans"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoEvents 
         Height          =   330
         Left            =   7500
         Top             =   1020
         Visible         =   0   'False
         Width           =   2100
         _ExtentX        =   3704
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoEvents"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdTransactions 
         Bindings        =   "frmLibrary.frx":09C3
         Height          =   5355
         Left            =   -74880
         TabIndex        =   169
         TabStop         =   0   'False
         ToolTipText     =   "Details of the movement history of this Tape"
         Top             =   1200
         Width           =   7155
         _Version        =   196617
         AllowUpdate     =   0   'False
         BackColorOdd    =   12648447
         RowHeight       =   423
         Columns.Count   =   7
         Columns(0).Width=   5001
         Columns(0).Caption=   "Destination"
         Columns(0).Name =   "destination"
         Columns(0).DataField=   "destination"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3334
         Columns(1).Caption=   "Date/Time"
         Columns(1).Name =   "cdate"
         Columns(1).DataField=   "cdate"
         Columns(1).DataType=   7
         Columns(1).FieldLen=   256
         Columns(2).Width=   1296
         Columns(2).Caption=   "User"
         Columns(2).Name =   "cuser"
         Columns(2).DataField=   "cuser"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "D-Note"
         Columns(3).Name =   "despatchID"
         Columns(3).DataField=   "despatchID"
         Columns(3).FieldLen=   256
         Columns(4).Width=   1984
         Columns(4).Caption=   "DNote #"
         Columns(4).Name =   "despatchnumber"
         Columns(4).DataField=   "despatchnumber"
         Columns(4).FieldLen=   256
         Columns(5).Width=   2328
         Columns(5).Caption=   "Batch #"
         Columns(5).Name =   "batchID"
         Columns(5).DataField=   "batchID"
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "transID"
         Columns(6).Name =   "transID"
         Columns(6).DataField=   "transID"
         Columns(6).FieldLen=   256
         _ExtentX        =   12621
         _ExtentY        =   9446
         _StockProps     =   79
         Caption         =   "Transactions"
         BackColor       =   -2147483643
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbTechReviewList 
         Bindings        =   "frmLibrary.frx":09E1
         DataField       =   "tapetechrevID"
         Height          =   315
         Left            =   -73380
         TabIndex        =   170
         Top             =   840
         Width           =   1575
         DataFieldList   =   "tapetechrevID"
         _Version        =   196617
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Caption=   "tapetechrevID"
         Columns(0).Name =   "tapetechrevID"
         Columns(0).DataField=   "tapetechrevID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "reviewdate"
         Columns(1).Name =   "reviewdate"
         Columns(1).DataField=   "reviewdate"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "techrevUser"
         Columns(2).Name =   "reviewUser"
         Columns(2).DataField=   "reviewUser"
         Columns(2).FieldLen=   256
         _ExtentX        =   2778
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdFaults 
         Bindings        =   "frmLibrary.frx":09FE
         Height          =   6375
         Left            =   -74880
         TabIndex        =   302
         TabStop         =   0   'False
         Top             =   480
         Width           =   7035
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         SelectTypeRow   =   3
         BackColorOdd    =   12648447
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   7
         Columns(0).Width=   5636
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "description"
         Columns(0).DataField=   "description"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   255
         Columns(1).Width=   2461
         Columns(1).Caption=   "Start Timecode"
         Columns(1).Name =   "timecode"
         Columns(1).DataField=   "timecode"
         Columns(1).FieldLen=   256
         Columns(2).Width=   2461
         Columns(2).Caption=   "End Timecode"
         Columns(2).Name =   "endtimecode"
         Columns(2).DataField=   "endtimecode"
         Columns(2).FieldLen=   256
         Columns(3).Width=   1058
         Columns(3).Caption=   "Grade"
         Columns(3).Name =   "faultgrade"
         Columns(3).DataField=   "faultgrade"
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "techrevID"
         Columns(4).Name =   "techrevID"
         Columns(4).DataField=   "tapetechrevID"
         Columns(4).FieldLen=   256
         Columns(5).Width=   873
         Columns(5).Caption=   "fixed"
         Columns(5).Name =   "fixed"
         Columns(5).DataField=   "faultfixed"
         Columns(5).FieldLen=   256
         Columns(5).Style=   2
         Columns(6).Width=   3254
         Columns(6).Caption=   "fixeddate"
         Columns(6).Name =   "fixeddate"
         Columns(6).DataField=   "faultfixeddate"
         Columns(6).FieldLen=   256
         Columns(6).Style=   1
         _ExtentX        =   12409
         _ExtentY        =   11245
         _StockProps     =   79
         Caption         =   "Faults"
         BackColor       =   -2147483643
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdEvents 
         Bindings        =   "frmLibrary.frx":0A16
         Height          =   2235
         Left            =   120
         TabIndex        =   303
         Top             =   840
         Width           =   22335
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         SelectTypeRow   =   3
         BackColorOdd    =   12910591
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   31
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "libraryID"
         Columns(0).Name =   "libraryID"
         Columns(0).DataField=   "libraryID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   2196
         Columns(1).Caption=   "Start"
         Columns(1).Name =   "timecodestart"
         Columns(1).DataField=   "timecodestart"
         Columns(1).FieldLen=   256
         Columns(2).Width=   2196
         Columns(2).Caption=   "Stop"
         Columns(2).Name =   "timecodestop"
         Columns(2).DataField=   "timecodestop"
         Columns(2).FieldLen=   256
         Columns(3).Width=   2196
         Columns(3).Caption=   "Duration"
         Columns(3).Name =   "length"
         Columns(3).DataField=   "fd_length"
         Columns(3).FieldLen=   256
         Columns(3).Style=   1
         Columns(4).Width=   6588
         Columns(4).Caption=   "Title"
         Columns(4).Name =   "eventtitle"
         Columns(4).DataField=   "eventtitle"
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "createddate"
         Columns(5).Name =   "createddate"
         Columns(5).DataField=   "cdate"
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "editor1"
         Columns(6).Name =   "editor1"
         Columns(6).DataField=   "editor1"
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "editor2"
         Columns(7).Name =   "editor2"
         Columns(7).DataField=   "editor2"
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Caption=   "Type"
         Columns(8).Name =   "eventtype"
         Columns(8).DataField=   "eventtype"
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Caption=   "Clock Number"
         Columns(9).Name =   "clocknumber"
         Columns(9).DataField=   "clocknumber"
         Columns(9).FieldLen=   256
         Columns(9).Style=   1
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "soundlay"
         Columns(10).Name=   "soundlay"
         Columns(10).DataField=   "soundlay"
         Columns(10).FieldLen=   256
         Columns(11).Width=   3200
         Columns(11).Caption=   "Subtitle"
         Columns(11).Name=   "eventsubtitle"
         Columns(11).DataField=   "eventsubtitle"
         Columns(11).FieldLen=   256
         Columns(11).Style=   1
         Columns(12).Width=   2196
         Columns(12).Caption=   "Break 1"
         Columns(12).Name=   "break1"
         Columns(12).DataField=   "break1"
         Columns(12).FieldLen=   256
         Columns(12).Style=   1
         Columns(13).Width=   2196
         Columns(13).Caption=   "Break 2"
         Columns(13).Name=   "break2"
         Columns(13).DataField=   "break2"
         Columns(13).FieldLen=   256
         Columns(13).Style=   1
         Columns(14).Width=   2196
         Columns(14).Caption=   "Break 3"
         Columns(14).Name=   "break3"
         Columns(14).DataField=   "break3"
         Columns(14).FieldLen=   256
         Columns(14).Style=   1
         Columns(15).Width=   2196
         Columns(15).Caption=   "Break 4"
         Columns(15).Name=   "break4"
         Columns(15).DataField=   "break4"
         Columns(15).FieldLen=   256
         Columns(15).Style=   1
         Columns(16).Width=   2196
         Columns(16).Caption=   "Break 5"
         Columns(16).Name=   "break5"
         Columns(16).DataField=   "break5"
         Columns(16).FieldLen=   256
         Columns(16).Style=   1
         Columns(17).Width=   2196
         Columns(17).Caption=   "End Credits"
         Columns(17).Name=   "endcredits"
         Columns(17).DataField=   "endcredits"
         Columns(17).FieldLen=   256
         Columns(17).Style=   1
         Columns(18).Width=   4419
         Columns(18).Caption=   "Notes"
         Columns(18).Name=   "notes"
         Columns(18).DataField=   "notes"
         Columns(18).DataType=   10
         Columns(18).FieldLen=   256
         Columns(19).Width=   3200
         Columns(19).Visible=   0   'False
         Columns(19).Caption=   "break1elapsed"
         Columns(19).Name=   "break1elapsed"
         Columns(19).DataField=   "break1elapsed"
         Columns(19).FieldLen=   256
         Columns(20).Width=   3200
         Columns(20).Visible=   0   'False
         Columns(20).Caption=   "break2elapsed"
         Columns(20).Name=   "break2elapsed"
         Columns(20).DataField=   "break2elapsed"
         Columns(20).FieldLen=   256
         Columns(21).Width=   3200
         Columns(21).Visible=   0   'False
         Columns(21).Caption=   "break3elapsed"
         Columns(21).Name=   "break3elapsed"
         Columns(21).DataField=   "break3elapsed"
         Columns(21).FieldLen=   256
         Columns(22).Width=   3200
         Columns(22).Visible=   0   'False
         Columns(22).Caption=   "break4elapsed"
         Columns(22).Name=   "break4elapsed"
         Columns(22).DataField=   "break4elapsed"
         Columns(22).FieldLen=   256
         Columns(23).Width=   3200
         Columns(23).Visible=   0   'False
         Columns(23).Caption=   "break5elapsed"
         Columns(23).Name=   "break5elapsed"
         Columns(23).DataField=   "break5elapsed"
         Columns(23).FieldLen=   256
         Columns(24).Width=   3200
         Columns(24).Visible=   0   'False
         Columns(24).Caption=   "endcreditselapsed"
         Columns(24).Name=   "endcreditselapsed"
         Columns(24).DataField=   "endcreditselapsed"
         Columns(24).FieldLen=   256
         Columns(25).Width=   3200
         Columns(25).Caption=   "eventID"
         Columns(25).Name=   "eventID"
         Columns(25).DataField=   "eventID"
         Columns(25).FieldLen=   256
         Columns(25).Locked=   -1  'True
         Columns(26).Width=   3200
         Columns(26).Visible=   0   'False
         Columns(26).Caption=   "eventmediatype"
         Columns(26).Name=   "eventmediatype"
         Columns(26).DataField=   "eventmediatype"
         Columns(26).FieldLen=   256
         Columns(27).Width=   3200
         Columns(27).Visible=   0   'False
         Columns(27).Caption=   "jobID"
         Columns(27).Name=   "jobID"
         Columns(27).DataField=   "jobID"
         Columns(27).FieldLen=   256
         Columns(28).Width=   3200
         Columns(28).Visible=   0   'False
         Columns(28).Caption=   "projectnumber"
         Columns(28).Name=   "projectnumber"
         Columns(28).DataField=   "projectnumber"
         Columns(28).FieldLen=   256
         Columns(29).Width=   3200
         Columns(29).Visible=   0   'False
         Columns(29).Caption=   "eventdate"
         Columns(29).Name=   "eventdate"
         Columns(29).DataField=   "eventdate"
         Columns(29).FieldLen=   256
         Columns(30).Width=   3200
         Columns(30).Visible=   0   'False
         Columns(30).Caption=   "aspectratio"
         Columns(30).Name=   "aspectratio"
         Columns(30).DataField=   "aspectratio"
         Columns(30).FieldLen=   256
         _ExtentX        =   39396
         _ExtentY        =   3942
         _StockProps     =   79
         Caption         =   "Events"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame fraTechReview 
         BorderStyle     =   0  'None
         Height          =   6975
         Left            =   -74820
         TabIndex        =   171
         ToolTipText     =   "Details about the tape Technical Review"
         Top             =   600
         Width           =   14295
         Begin VB.CheckBox chkTitles 
            Caption         =   "Titles"
            Height          =   195
            Left            =   60
            TabIndex        =   278
            ToolTipText     =   "Are Titles present on the Tape?"
            Top             =   2340
            Width           =   795
         End
         Begin VB.CheckBox chkSubtitles 
            Caption         =   "Subtitles"
            Height          =   195
            Left            =   60
            TabIndex        =   277
            ToolTipText     =   "Are there Subtitles on the Tape?"
            Top             =   3060
            Width           =   975
         End
         Begin VB.CheckBox chkCaptions 
            Caption         =   "Captions"
            Height          =   195
            Left            =   60
            TabIndex        =   276
            ToolTipText     =   "Are Captions present in the body of the Programme?"
            Top             =   2700
            Width           =   975
         End
         Begin VB.CheckBox chkLogos 
            Caption         =   "Logos"
            Height          =   195
            Left            =   60
            TabIndex        =   275
            ToolTipText     =   "Are there any Logos on the Tape"
            Top             =   3420
            Width           =   975
         End
         Begin VB.CheckBox chkTextless 
            Caption         =   "Textless"
            Height          =   195
            Left            =   60
            TabIndex        =   274
            ToolTipText     =   "Are Textless Elements present on the Tape?"
            Top             =   3780
            Width           =   975
         End
         Begin VB.CheckBox chkTrailers 
            Caption         =   "Trailers"
            Height          =   195
            Left            =   60
            TabIndex        =   273
            ToolTipText     =   "Are there any Trailers on the Tape?"
            Top             =   4140
            Width           =   975
         End
         Begin VB.TextBox txtDetailsLogos 
            Height          =   315
            Left            =   2100
            MaxLength       =   100
            TabIndex        =   272
            ToolTipText     =   "Notes regarding the Logos / what Language they are in"
            Top             =   3360
            Width           =   5415
         End
         Begin VB.TextBox txtDetailsTextless 
            Height          =   315
            Left            =   2100
            MaxLength       =   100
            TabIndex        =   271
            ToolTipText     =   "Notes regarding the Textless Elements"
            Top             =   3720
            Width           =   5415
         End
         Begin VB.TextBox txtDetailsTrailers 
            Height          =   315
            Left            =   2100
            MaxLength       =   100
            TabIndex        =   270
            ToolTipText     =   "Notes regarding Trailers"
            Top             =   4080
            Width           =   5415
         End
         Begin VB.ComboBox cmbReviewMachine 
            Height          =   315
            Left            =   1440
            TabIndex        =   269
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   1200
            Width           =   1575
         End
         Begin VB.TextBox txtReviewJobID 
            Height          =   315
            Left            =   1440
            TabIndex        =   268
            ToolTipText     =   "The job number for the tech review"
            Top             =   1560
            Width           =   1575
         End
         Begin VB.ComboBox cmbTitlesLanguage 
            Height          =   315
            Left            =   2100
            TabIndex        =   267
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   2280
            Width           =   5415
         End
         Begin VB.ComboBox cmbCaptionsLanguage 
            Height          =   315
            Left            =   2100
            TabIndex        =   266
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   2640
            Width           =   5415
         End
         Begin VB.ComboBox cmbSubtitlesLanguage 
            Height          =   315
            Left            =   2100
            TabIndex        =   265
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   3000
            Width           =   5415
         End
         Begin VB.ComboBox cmbReviewUser 
            Height          =   315
            Left            =   4380
            TabIndex        =   264
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   840
            Width           =   3135
         End
         Begin VB.Frame fraDigitalParameters 
            Caption         =   "Digital Tape Parameters"
            Height          =   2655
            Left            =   7920
            TabIndex        =   246
            Top             =   4080
            Visible         =   0   'False
            Width           =   4335
            Begin VB.CheckBox chkverticalcenteringcorrect 
               Alignment       =   1  'Right Justify
               Caption         =   "Vertical Centering Good?"
               Height          =   315
               Left            =   120
               TabIndex        =   254
               ToolTipText     =   "Is the picture centred vertiically?"
               Top             =   2280
               Value           =   1  'Checked
               Width           =   2115
            End
            Begin VB.CheckBox chkhorizontalcenteringcorrect 
               Alignment       =   1  'Right Justify
               Caption         =   "Horiz Cent Good?"
               Height          =   315
               Left            =   2520
               TabIndex        =   253
               ToolTipText     =   "Is the picture centred horizontally?"
               Top             =   2280
               Value           =   1  'Checked
               Width           =   1635
            End
            Begin VB.TextBox txtlastactivepixel 
               Height          =   315
               Left            =   3000
               TabIndex        =   252
               Text            =   "1439"
               ToolTipText     =   "The last active pixel horizontally"
               Top             =   1620
               Width           =   615
            End
            Begin VB.TextBox txtfirstactivepixel 
               Height          =   315
               Left            =   3000
               TabIndex        =   251
               Text            =   "0"
               ToolTipText     =   "The first active pixel horizontally"
               Top             =   840
               Width           =   615
            End
            Begin VB.TextBox txtlastactivelinefield2 
               Height          =   315
               Left            =   1380
               TabIndex        =   250
               Text            =   "623"
               ToolTipText     =   "The last active line on field 2"
               Top             =   1860
               Width           =   855
            End
            Begin VB.TextBox txtfirstactivelinefield2 
               Height          =   315
               Left            =   1380
               TabIndex        =   249
               Text            =   "336"
               ToolTipText     =   "The first active line on field 2"
               Top             =   1500
               Width           =   855
            End
            Begin VB.TextBox txtlastactivelinefield1 
               Height          =   315
               Left            =   1380
               TabIndex        =   248
               Text            =   "310"
               ToolTipText     =   "The last active line on field 1"
               Top             =   840
               Width           =   855
            End
            Begin VB.TextBox txtfirstactivelinefield1 
               Height          =   315
               Left            =   1380
               TabIndex        =   247
               Text            =   "23"
               ToolTipText     =   "The first active line on field 1"
               Top             =   480
               Width           =   855
            End
            Begin VB.Label lblParameters 
               Alignment       =   2  'Center
               Caption         =   "1st Line (23)"
               Height          =   195
               Index           =   5
               Left            =   2400
               TabIndex        =   263
               Top             =   1380
               Width           =   1875
            End
            Begin VB.Label lblParameters 
               Alignment       =   2  'Center
               Caption         =   "1st Line (23)"
               Height          =   195
               Index           =   4
               Left            =   2400
               TabIndex        =   262
               Top             =   600
               Width           =   1875
            End
            Begin VB.Label lblParameters 
               Caption         =   "1st Line (23)"
               Height          =   195
               Index           =   3
               Left            =   120
               TabIndex        =   261
               Top             =   1920
               Width           =   1575
            End
            Begin VB.Label lblParameters 
               Caption         =   "1st Line (23)"
               Height          =   195
               Index           =   2
               Left            =   120
               TabIndex        =   260
               Top             =   1560
               Width           =   1575
            End
            Begin VB.Label lblParameters 
               Caption         =   "1st Line (23)"
               Height          =   195
               Index           =   1
               Left            =   120
               TabIndex        =   259
               Top             =   900
               Width           =   1575
            End
            Begin VB.Label lblCaption 
               Alignment       =   2  'Center
               Caption         =   "Horizontal"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   67
               Left            =   2820
               TabIndex        =   258
               Top             =   300
               Width           =   1035
            End
            Begin VB.Label lblCaption 
               Caption         =   "Field 2"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   69
               Left            =   120
               TabIndex        =   257
               Top             =   1260
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "Field 1"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   68
               Left            =   120
               TabIndex        =   256
               Top             =   300
               Width           =   855
            End
            Begin VB.Label lblParameters 
               Caption         =   "1st Line (23)"
               Height          =   195
               Index           =   0
               Left            =   120
               TabIndex        =   255
               Top             =   540
               Width           =   1575
            End
         End
         Begin VB.Frame fraSoundLevels 
            Caption         =   "Frame1"
            Height          =   3675
            Left            =   11040
            TabIndex        =   204
            Top             =   0
            Width           =   3195
            Begin VB.TextBox txtA1Prog 
               Height          =   255
               Left            =   2040
               TabIndex        =   230
               Text            =   "OK"
               ToolTipText     =   "The level of the programme in respect to its lineup"
               Top             =   420
               Width           =   1035
            End
            Begin VB.TextBox txtA1Test 
               Height          =   255
               Left            =   1020
               TabIndex        =   229
               Text            =   "-18 dB"
               ToolTipText     =   "The level of the line-up in respect to unity"
               Top             =   420
               Width           =   1035
            End
            Begin VB.TextBox txtA2Test 
               Height          =   255
               Left            =   1020
               LinkItem        =   "txtA2Test"
               TabIndex        =   228
               Text            =   "-18 dB"
               ToolTipText     =   "The level of the line-up in respect to unity"
               Top             =   660
               Width           =   1035
            End
            Begin VB.TextBox txtA3Test 
               Height          =   255
               Left            =   1020
               TabIndex        =   227
               Text            =   "-18 dB"
               ToolTipText     =   "The level of the line-up in respect to unity"
               Top             =   900
               Width           =   1035
            End
            Begin VB.TextBox txtA4Test 
               Height          =   255
               Left            =   1020
               TabIndex        =   226
               Text            =   "-18 dB"
               ToolTipText     =   "The level of the line-up in respect to unity"
               Top             =   1140
               Width           =   1035
            End
            Begin VB.TextBox txtA5Test 
               Height          =   255
               Left            =   1020
               TabIndex        =   225
               Text            =   "-18 dB"
               ToolTipText     =   "The level of the line-up in respect to unity"
               Top             =   1380
               Width           =   1035
            End
            Begin VB.TextBox txtA6Test 
               Height          =   255
               Left            =   1020
               TabIndex        =   224
               Text            =   "-18 dB"
               ToolTipText     =   "The level of the line-up in respect to unity"
               Top             =   1620
               Width           =   1035
            End
            Begin VB.TextBox txtA7Test 
               Height          =   255
               Left            =   1020
               TabIndex        =   223
               Text            =   "-18 dB"
               ToolTipText     =   "The level of the line-up in respect to unity"
               Top             =   1860
               Width           =   1035
            End
            Begin VB.TextBox txtA8Test 
               Height          =   255
               Left            =   1020
               TabIndex        =   222
               Text            =   "-18 dB"
               ToolTipText     =   "The level of the line-up in respect to unity"
               Top             =   2100
               Width           =   1035
            End
            Begin VB.TextBox txtA2Prog 
               Height          =   255
               Left            =   2040
               TabIndex        =   221
               Text            =   "OK"
               ToolTipText     =   "The level of the programme in respect to its lineup"
               Top             =   660
               Width           =   1035
            End
            Begin VB.TextBox txtA3Prog 
               Height          =   255
               Left            =   2040
               TabIndex        =   220
               Text            =   "OK"
               ToolTipText     =   "The level of the programme in respect to its lineup"
               Top             =   900
               Width           =   1035
            End
            Begin VB.TextBox txtA4Prog 
               Height          =   255
               Left            =   2040
               TabIndex        =   219
               Text            =   "OK"
               ToolTipText     =   "The level of the programme in respect to its lineup"
               Top             =   1140
               Width           =   1035
            End
            Begin VB.TextBox txtA5Prog 
               Height          =   255
               Left            =   2040
               TabIndex        =   218
               Text            =   "OK"
               ToolTipText     =   "The level of the programme in respect to its lineup"
               Top             =   1380
               Width           =   1035
            End
            Begin VB.TextBox txtA6Prog 
               Height          =   255
               Left            =   2040
               TabIndex        =   217
               Text            =   "OK"
               ToolTipText     =   "The level of the programme in respect to its lineup"
               Top             =   1620
               Width           =   1035
            End
            Begin VB.TextBox txtA7Prog 
               Height          =   255
               Left            =   2040
               TabIndex        =   216
               Text            =   "OK"
               ToolTipText     =   "The level of the programme in respect to its lineup"
               Top             =   1860
               Width           =   1035
            End
            Begin VB.TextBox txtA8Prog 
               Height          =   255
               Left            =   2040
               TabIndex        =   215
               Text            =   "OK"
               ToolTipText     =   "The level of the programme in respect to its lineup"
               Top             =   2100
               Width           =   1035
            End
            Begin VB.TextBox txtA9Test 
               Height          =   255
               Left            =   1020
               TabIndex        =   214
               Text            =   "-18 dB"
               ToolTipText     =   "The level of the line-up in respect to unity"
               Top             =   2340
               Width           =   1035
            End
            Begin VB.TextBox txtA10Test 
               Height          =   255
               Left            =   1020
               TabIndex        =   213
               Text            =   "-18 dB"
               ToolTipText     =   "The level of the line-up in respect to unity"
               Top             =   2580
               Width           =   1035
            End
            Begin VB.TextBox txtA11Test 
               Height          =   255
               Left            =   1020
               TabIndex        =   212
               Text            =   "-18 dB"
               ToolTipText     =   "The level of the line-up in respect to unity"
               Top             =   2820
               Width           =   1035
            End
            Begin VB.TextBox txtA12Test 
               Height          =   255
               Left            =   1020
               TabIndex        =   211
               Text            =   "-18 dB"
               ToolTipText     =   "The level of the line-up in respect to unity"
               Top             =   3060
               Width           =   1035
            End
            Begin VB.TextBox txtA9Prog 
               Height          =   255
               Left            =   2040
               TabIndex        =   210
               Text            =   "OK"
               ToolTipText     =   "The level of the programme in respect to its lineup"
               Top             =   2340
               Width           =   1035
            End
            Begin VB.TextBox txtA10Prog 
               Height          =   255
               Left            =   2040
               TabIndex        =   209
               Text            =   "OK"
               ToolTipText     =   "The level of the programme in respect to its lineup"
               Top             =   2580
               Width           =   1035
            End
            Begin VB.TextBox txtA11Prog 
               Height          =   255
               Left            =   2040
               TabIndex        =   208
               Text            =   "OK"
               ToolTipText     =   "The level of the programme in respect to its lineup"
               Top             =   2820
               Width           =   1035
            End
            Begin VB.TextBox txtA12Prog 
               Height          =   255
               Left            =   2040
               TabIndex        =   207
               Text            =   "OK"
               ToolTipText     =   "The level of the programme in respect to its lineup"
               Top             =   3060
               Width           =   1035
            End
            Begin VB.TextBox txtCueProg 
               Height          =   255
               Left            =   2040
               TabIndex        =   206
               Text            =   "OK"
               ToolTipText     =   "The level of the programme in respect to its lineup"
               Top             =   3300
               Width           =   1035
            End
            Begin VB.TextBox txtCueTest 
               Height          =   255
               Left            =   1020
               TabIndex        =   205
               Text            =   "0 dBm"
               ToolTipText     =   "The level of the line-up in respect to unity"
               Top             =   3300
               Width           =   1035
            End
            Begin VB.Label lblCaption 
               Caption         =   "Audio 1"
               Height          =   255
               Index           =   50
               Left            =   120
               TabIndex        =   245
               Top             =   480
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "Audio 7"
               Height          =   255
               Index           =   52
               Left            =   120
               TabIndex        =   244
               Top             =   1920
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "Audio 6"
               Height          =   255
               Index           =   53
               Left            =   120
               TabIndex        =   243
               Top             =   1680
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "Audio 5"
               Height          =   255
               Index           =   54
               Left            =   120
               TabIndex        =   242
               Top             =   1440
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "Audio 4"
               Height          =   255
               Index           =   55
               Left            =   120
               TabIndex        =   241
               Top             =   1200
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "Audio 3"
               Height          =   255
               Index           =   56
               Left            =   120
               TabIndex        =   240
               Top             =   960
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "Audio 2"
               Height          =   255
               Index           =   57
               Left            =   120
               TabIndex        =   239
               Top             =   720
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "Audio 8"
               Height          =   255
               Index           =   58
               Left            =   120
               TabIndex        =   238
               Top             =   2160
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "Audio 9"
               Height          =   255
               Index           =   98
               Left            =   120
               TabIndex        =   237
               Top             =   2400
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "Audio 10"
               Height          =   255
               Index           =   99
               Left            =   120
               TabIndex        =   236
               Top             =   2640
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "Audio 11"
               Height          =   255
               Index           =   100
               Left            =   120
               TabIndex        =   235
               Top             =   2880
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "Audio 12"
               Height          =   255
               Index           =   101
               Left            =   120
               TabIndex        =   234
               Top             =   3120
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "Programme"
               Height          =   255
               Index           =   48
               Left            =   2100
               TabIndex        =   233
               Top             =   180
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "Test"
               Height          =   255
               Index           =   49
               Left            =   1320
               TabIndex        =   232
               Top             =   180
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Cue Track"
               Height          =   255
               Index           =   51
               Left            =   120
               TabIndex        =   231
               Top             =   3360
               Width           =   855
            End
         End
         Begin VB.TextBox txtHighestAudioPeak 
            Height          =   315
            Left            =   13080
            TabIndex        =   203
            Text            =   "0 dBm"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   3780
            Width           =   1035
         End
         Begin VB.Frame fraVideoLevels 
            Caption         =   "Video Levels"
            Height          =   2055
            Left            =   4680
            TabIndex        =   188
            Top             =   4680
            Width           =   2895
            Begin VB.TextBox txtPhaseProg 
               Height          =   315
               Left            =   1980
               TabIndex        =   196
               Text            =   "OK"
               ToolTipText     =   "The phase of the programme in respect to its lineup"
               Top             =   1560
               Width           =   735
            End
            Begin VB.TextBox txtBlackProg 
               Height          =   315
               Left            =   1980
               TabIndex        =   195
               Text            =   "OK"
               ToolTipText     =   "The level of the programme in respect to its lineup"
               Top             =   1200
               Width           =   735
            End
            Begin VB.TextBox txtChromaProg 
               Height          =   315
               Left            =   1980
               TabIndex        =   194
               Text            =   "OK"
               ToolTipText     =   "The level of the programme in respect to its lineup"
               Top             =   840
               Width           =   735
            End
            Begin VB.TextBox txtPhaseTest 
               Height          =   315
               Left            =   1260
               TabIndex        =   193
               Text            =   "0"
               ToolTipText     =   "The phase of the line-up in respect to unity"
               Top             =   1560
               Width           =   735
            End
            Begin VB.TextBox txtBlackTest 
               Height          =   315
               Left            =   1260
               TabIndex        =   192
               Text            =   "0"
               ToolTipText     =   "The level of the line-up in respect to unity"
               Top             =   1200
               Width           =   735
            End
            Begin VB.TextBox txtChromaTest 
               Height          =   315
               Left            =   1260
               TabIndex        =   191
               Text            =   "75%"
               ToolTipText     =   "The level of the line-up in respect to unity"
               Top             =   840
               Width           =   735
            End
            Begin VB.TextBox txtVideoTest 
               Height          =   315
               Left            =   1260
               TabIndex        =   190
               Text            =   "100%"
               ToolTipText     =   "The level of the line-up in respect to unity"
               Top             =   480
               Width           =   735
            End
            Begin VB.TextBox txtVideoProg 
               Height          =   315
               Left            =   1980
               TabIndex        =   189
               Text            =   "OK"
               ToolTipText     =   "The level of the programme in respect to its lineup"
               Top             =   480
               Width           =   735
            End
            Begin VB.Label lblCaption 
               Caption         =   "Phase"
               Height          =   255
               Index           =   64
               Left            =   180
               TabIndex        =   202
               Top             =   1620
               Width           =   915
            End
            Begin VB.Label lblCaption 
               Caption         =   "Black Levels"
               Height          =   255
               Index           =   63
               Left            =   180
               TabIndex        =   201
               Top             =   1260
               Width           =   915
            End
            Begin VB.Label lblCaption 
               Caption         =   "Chroma Levels"
               Height          =   255
               Index           =   62
               Left            =   180
               TabIndex        =   200
               Top             =   900
               Width           =   1155
            End
            Begin VB.Label lblCaption 
               Caption         =   "Video Levels"
               Height          =   255
               Index           =   61
               Left            =   180
               TabIndex        =   199
               Top             =   540
               Width           =   915
            End
            Begin VB.Label lblCaption 
               Caption         =   "Programme"
               Height          =   255
               Index           =   60
               Left            =   1980
               TabIndex        =   198
               Top             =   240
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "Test"
               Height          =   255
               Index           =   59
               Left            =   1260
               TabIndex        =   197
               Top             =   240
               Width           =   435
            End
         End
         Begin VB.Frame fraTimecode 
            Caption         =   "Timecode Details"
            Height          =   1575
            Left            =   180
            TabIndex        =   179
            Top             =   4920
            Width           =   4155
            Begin VB.TextBox txtuserbits 
               Height          =   315
               Left            =   1200
               MaxLength       =   50
               TabIndex        =   185
               Text            =   "00:00:00:00"
               ToolTipText     =   "What is in the User bits of the Tape?"
               Top             =   1080
               Width           =   1035
            End
            Begin VB.TextBox txtvitclines 
               Height          =   315
               Left            =   1200
               MaxLength       =   50
               TabIndex        =   184
               Text            =   "19 & 21"
               ToolTipText     =   "What lines is the VITC on?"
               Top             =   720
               Width           =   1035
            End
            Begin VB.CheckBox chktimecodecontinuous 
               Alignment       =   1  'Right Justify
               Caption         =   "Timecode Continuous?"
               Height          =   195
               Left            =   120
               TabIndex        =   183
               ToolTipText     =   "Is the timecode continuous throughout the tape?"
               Top             =   360
               Width           =   2085
            End
            Begin VB.CheckBox chkltcvitcmatch 
               Alignment       =   1  'Right Justify
               Caption         =   "LTC/VITC Match?"
               Height          =   195
               Left            =   2400
               TabIndex        =   182
               ToolTipText     =   "Do the LTC and the VITC match?"
               Top             =   1080
               Width           =   1575
            End
            Begin VB.CheckBox chkvitc 
               Alignment       =   1  'Right Justify
               Caption         =   "VITC?"
               Height          =   195
               Left            =   2400
               TabIndex        =   181
               ToolTipText     =   "Is there VITC on the tape?"
               Top             =   720
               Width           =   1575
            End
            Begin VB.CheckBox chkltc 
               Alignment       =   1  'Right Justify
               Caption         =   "LTC?"
               Height          =   195
               Left            =   2400
               TabIndex        =   180
               ToolTipText     =   "Is there LiTC on the Tape?"
               Top             =   360
               Width           =   1575
            End
            Begin VB.Label lblCaption 
               Caption         =   "USer Bits"
               Height          =   255
               Index           =   66
               Left            =   180
               TabIndex        =   187
               Top             =   1080
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "VITC Lines"
               Height          =   255
               Index           =   65
               Left            =   180
               TabIndex        =   186
               Top             =   720
               Width           =   855
            End
         End
         Begin VB.CheckBox chkConvertedMaterial 
            Caption         =   "Converted Material?"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   60
            TabIndex        =   178
            ToolTipText     =   "Are there any Trailers on the Tape?"
            Top             =   4500
            Width           =   1935
         End
         Begin VB.Frame fraCaptionSafe 
            Caption         =   "Safe Areas"
            Height          =   1515
            Left            =   7920
            TabIndex        =   173
            Top             =   2160
            Width           =   2775
            Begin VB.CheckBox chkCaptionsafe169 
               Alignment       =   1  'Right Justify
               Caption         =   "Safe Area 16:9"
               Height          =   195
               Left            =   120
               TabIndex        =   177
               ToolTipText     =   "Are all graphics 16:9 safe?"
               Top             =   300
               Width           =   2475
            End
            Begin VB.CheckBox chkCaptionsafe149 
               Alignment       =   1  'Right Justify
               Caption         =   "Safe Area 14:9"
               Height          =   195
               Left            =   120
               TabIndex        =   176
               ToolTipText     =   "Are all graphics 14:9 safe?"
               Top             =   600
               Width           =   2475
            End
            Begin VB.CheckBox chkCaptionsafe43protect 
               Alignment       =   1  'Right Justify
               Caption         =   "Safe Area 4:3 EBU Protect"
               Height          =   195
               Left            =   120
               TabIndex        =   175
               ToolTipText     =   "Are all graphics 4:3 EBU Shoot to Protect safe?"
               Top             =   1200
               Width           =   2475
            End
            Begin VB.CheckBox chkCaptionsafe43ebu 
               Alignment       =   1  'Right Justify
               Caption         =   "Safe Area 4:3 10%"
               Height          =   195
               Left            =   120
               TabIndex        =   174
               ToolTipText     =   "Are all graphics 4:3 EBU 10% safe?"
               Top             =   900
               Width           =   2475
            End
         End
         Begin VB.CheckBox chkIdentInfoCorrect 
            Caption         =   "Ident Info Correct?"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   60
            TabIndex        =   172
            ToolTipText     =   "Are Titles present on the Tape?"
            Top             =   1980
            Width           =   2055
         End
         Begin MSAdodcLib.Adodc adoTechReviews 
            Height          =   330
            Left            =   540
            Top             =   1140
            Visible         =   0   'False
            Width           =   2580
            _ExtentX        =   4551
            _ExtentY        =   582
            ConnectMode     =   0
            CursorLocation  =   3
            IsolationLevel  =   -1
            ConnectionTimeout=   15
            CommandTimeout  =   30
            CursorType      =   3
            LockType        =   3
            CommandType     =   8
            CursorOptions   =   0
            CacheSize       =   50
            MaxRecords      =   0
            BOFAction       =   0
            EOFAction       =   0
            ConnectStringType=   1
            Appearance      =   1
            BackColor       =   8421631
            ForeColor       =   -2147483640
            Orientation     =   0
            Enabled         =   -1
            Connect         =   ""
            OLEDBString     =   ""
            OLEDBFile       =   ""
            DataSourceName  =   ""
            OtherAttributes =   ""
            UserName        =   ""
            Password        =   ""
            RecordSource    =   ""
            Caption         =   "adoTechReviews"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            _Version        =   393216
         End
         Begin MSAdodcLib.Adodc adoContact 
            Height          =   330
            Left            =   5280
            Top             =   1620
            Visible         =   0   'False
            Width           =   2265
            _ExtentX        =   3995
            _ExtentY        =   582
            ConnectMode     =   0
            CursorLocation  =   3
            IsolationLevel  =   -1
            ConnectionTimeout=   15
            CommandTimeout  =   30
            CursorType      =   3
            LockType        =   3
            CommandType     =   8
            CursorOptions   =   0
            CacheSize       =   50
            MaxRecords      =   0
            BOFAction       =   0
            EOFAction       =   0
            ConnectStringType=   1
            Appearance      =   1
            BackColor       =   8421631
            ForeColor       =   -2147483640
            Orientation     =   0
            Enabled         =   -1
            Connect         =   ""
            OLEDBString     =   ""
            OLEDBFile       =   ""
            DataSourceName  =   ""
            OtherAttributes =   ""
            UserName        =   ""
            Password        =   ""
            RecordSource    =   ""
            Caption         =   "adoContact"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            _Version        =   393216
         End
         Begin MSComCtl2.DTPicker datReviewDate 
            Height          =   315
            Left            =   1440
            TabIndex        =   279
            ToolTipText     =   "When the Tech review was done"
            Top             =   840
            Width           =   1575
            _ExtentX        =   2778
            _ExtentY        =   556
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CheckBox        =   -1  'True
            Format          =   129105921
            CurrentDate     =   37999
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbReviewCompany 
            Bindings        =   "frmLibrary.frx":0A2E
            Height          =   315
            Left            =   4380
            TabIndex        =   280
            ToolTipText     =   "The company this review is for"
            Top             =   1200
            Width           =   3135
            DataFieldList   =   "name"
            _Version        =   196617
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   4
            Columns(0).Width=   6376
            Columns(0).Caption=   "name"
            Columns(0).Name =   "name"
            Columns(0).DataField=   "name"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "companyID"
            Columns(1).Name =   "companyID"
            Columns(1).DataField=   "companyID"
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Caption=   "accountcode"
            Columns(2).Name =   "accountcode"
            Columns(2).DataField=   "accountcode"
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Caption=   "telephone"
            Columns(3).Name =   "telephone"
            Columns(3).DataField=   "telephone"
            Columns(3).FieldLen=   256
            _ExtentX        =   5530
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "name"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbReviewContact 
            Bindings        =   "frmLibrary.frx":0A47
            Height          =   315
            Left            =   4380
            TabIndex        =   281
            ToolTipText     =   "The contact this review is for"
            Top             =   1560
            Width           =   3135
            DataFieldList   =   "name"
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BackColorOdd    =   16761087
            RowHeight       =   370
            ExtraHeight     =   53
            Columns.Count   =   3
            Columns(0).Width=   6376
            Columns(0).Caption=   "name"
            Columns(0).Name =   "name"
            Columns(0).DataField=   "name"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "companyID"
            Columns(1).Name =   "companyID"
            Columns(1).DataField=   "companyID"
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "contactID"
            Columns(2).Name =   "contactID"
            Columns(2).DataField=   "contactID"
            Columns(2).FieldLen=   256
            _ExtentX        =   5530
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "name"
         End
         Begin VB.Label lblCaption 
            Caption         =   "Review Date"
            Height          =   255
            Index           =   38
            Left            =   60
            TabIndex        =   301
            Top             =   900
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Company"
            Height          =   255
            Index           =   39
            Left            =   3240
            TabIndex        =   300
            Top             =   1260
            Width           =   1095
         End
         Begin VB.Label lblCaption 
            Caption         =   "Contact"
            Height          =   255
            Index           =   40
            Left            =   3240
            TabIndex        =   299
            Top             =   1620
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Language"
            Height          =   255
            Index           =   41
            Left            =   1140
            TabIndex        =   298
            Top             =   2340
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Language"
            Height          =   255
            Index           =   42
            Left            =   1140
            TabIndex        =   297
            Top             =   2700
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Language"
            Height          =   255
            Index           =   43
            Left            =   1140
            TabIndex        =   296
            Top             =   3060
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Notes"
            Height          =   255
            Index           =   44
            Left            =   1140
            TabIndex        =   295
            Top             =   3420
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Notes"
            Height          =   255
            Index           =   45
            Left            =   1140
            TabIndex        =   294
            Top             =   3780
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Notes"
            Height          =   255
            Index           =   46
            Left            =   1140
            TabIndex        =   293
            Top             =   4140
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Review Machine"
            Height          =   195
            Index           =   47
            Left            =   60
            TabIndex        =   292
            Top             =   1260
            Width           =   1155
         End
         Begin VB.Label lblReviewCompanyID 
            Height          =   315
            Left            =   7560
            TabIndex        =   291
            Tag             =   "CLEARFIELDS"
            Top             =   1200
            Visible         =   0   'False
            Width           =   795
         End
         Begin VB.Label lblReviewContactID 
            Height          =   255
            Left            =   7500
            TabIndex        =   290
            Tag             =   "CLEARFIELDS"
            Top             =   1320
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Review Job Number"
            Height          =   195
            Index           =   79
            Left            =   60
            TabIndex        =   289
            Top             =   1620
            Width           =   1335
         End
         Begin VB.Label lblCaption 
            Caption         =   "Reviewed by"
            Height          =   255
            Index           =   81
            Left            =   3240
            TabIndex        =   288
            Top             =   900
            Width           =   1095
         End
         Begin VB.Label lbltechrevID 
            Height          =   195
            Left            =   6360
            TabIndex        =   287
            Tag             =   "CLEARFIELDS"
            Top             =   360
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.Label lblCaption 
            Caption         =   "Techrev ID"
            Height          =   195
            Index           =   80
            Left            =   4980
            TabIndex        =   286
            Top             =   360
            Width           =   915
         End
         Begin VB.Label lblLastTechrevID 
            Height          =   195
            Left            =   6360
            TabIndex        =   285
            Tag             =   "CLEARFIELDS"
            Top             =   60
            Visible         =   0   'False
            Width           =   1155
         End
         Begin VB.Label lblCaption 
            Caption         =   "Latst Techrev ID"
            Height          =   195
            Index           =   83
            Left            =   4980
            TabIndex        =   284
            Top             =   60
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Tech Review ID"
            Height          =   255
            Index           =   82
            Left            =   60
            TabIndex        =   283
            Top             =   540
            Width           =   2055
         End
         Begin VB.Label lblCaption 
            Caption         =   "Highest Audio Peak"
            Height          =   255
            Index           =   3
            Left            =   11100
            TabIndex        =   282
            Top             =   3840
            Width           =   1815
         End
      End
      Begin MSAdodcLib.Adodc adoJobsForTape 
         Height          =   330
         Left            =   -74580
         Top             =   1200
         Visible         =   0   'False
         Width           =   2100
         _ExtentX        =   3704
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoJobsForTape"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdJobsForTape 
         Bindings        =   "frmLibrary.frx":0A60
         Height          =   5355
         Left            =   -74880
         TabIndex        =   346
         Top             =   420
         Width           =   6135
         _Version        =   196617
         AllowUpdate     =   0   'False
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Caption=   "libraryID"
         Columns(0).Name =   "libraryID"
         Columns(0).DataField=   "libraryID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "jobID"
         Columns(1).Name =   "jobID"
         Columns(1).DataField=   "jobID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "jobstatus"
         Columns(2).Name =   "jobstatus"
         Columns(2).DataField=   "jobstatus"
         Columns(2).FieldLen=   256
         _ExtentX        =   10821
         _ExtentY        =   9446
         _StockProps     =   79
         Caption         =   "Jobs Requiring This Tape"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblCaption 
         Caption         =   "Internal Reference"
         Height          =   195
         Index           =   114
         Left            =   -74940
         TabIndex        =   350
         Top             =   840
         Width           =   1395
      End
      Begin VB.Label lblCaption 
         Caption         =   "Cue"
         Height          =   255
         Index           =   28
         Left            =   -74520
         TabIndex        =   349
         Top             =   480
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Modified By"
         Height          =   255
         Index           =   36
         Left            =   -74880
         TabIndex        =   311
         Top             =   840
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Created By"
         Height          =   255
         Index           =   33
         Left            =   -74880
         TabIndex        =   310
         Top             =   480
         Width           =   855
      End
      Begin VB.Label lblCreatedUser 
         BackColor       =   &H80000005&
         Height          =   255
         Left            =   -73620
         TabIndex        =   309
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "The initials of the user who created this Tape"
         Top             =   480
         Width           =   735
      End
      Begin VB.Label lblCreatedDate 
         BackColor       =   &H80000005&
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "dddd, d MMMM yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   0
         EndProperty
         Height          =   255
         Left            =   -72780
         TabIndex        =   308
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "When this Tape was created"
         Top             =   480
         Width           =   1755
      End
      Begin VB.Label lblModifiedUser 
         BackColor       =   &H80000005&
         Height          =   255
         Left            =   -73620
         TabIndex        =   307
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "The initials of the user who last modified this Tape"
         Top             =   840
         Width           =   735
      End
      Begin VB.Label lblModifiedDate 
         BackColor       =   &H80000005&
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "dddd, d MMMM yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   0
         EndProperty
         Height          =   255
         Left            =   -72780
         TabIndex        =   306
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "When this Tape was last modified"
         Top             =   840
         Width           =   1755
      End
      Begin VB.Label lblEventNotes 
         BackColor       =   &H80000018&
         Height          =   735
         Left            =   180
         TabIndex        =   305
         Top             =   5340
         Width           =   6975
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblProcessingSubtitle 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -64680
         TabIndex        =   304
         Tag             =   "CLEARFIELDS"
         Top             =   1620
         Width           =   4515
      End
   End
   Begin VB.Label lblCaption 
      Caption         =   "Integer"
      Height          =   255
      Index           =   97
      Left            =   21600
      TabIndex        =   362
      Top             =   2100
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Min Free %"
      Height          =   255
      Index           =   72
      Left            =   21600
      TabIndex        =   352
      Top             =   1740
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Serial #"
      Height          =   255
      Index           =   70
      Left            =   10500
      TabIndex        =   328
      Top             =   840
      Width           =   975
   End
   Begin VB.Label lblField3 
      Appearance      =   0  'Flat
      Caption         =   "Custom Fld3"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   326
      Tag             =   "3"
      Top             =   2340
      Width           =   975
   End
   Begin VB.Label lblDeletedTape 
      Alignment       =   2  'Center
      Caption         =   "DELETED TAPE"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000040C0&
      Height          =   195
      Left            =   21540
      TabIndex        =   323
      Top             =   1380
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Detail / Box"
      Height          =   255
      Index           =   117
      Left            =   10500
      TabIndex        =   101
      Top             =   1920
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Series ID"
      Height          =   255
      Index           =   73
      Left            =   10500
      TabIndex        =   99
      Top             =   480
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Here for Job"
      Height          =   255
      Index           =   116
      Left            =   10500
      TabIndex        =   96
      Top             =   2280
      Width           =   1035
   End
   Begin VB.Label lblField1 
      Appearance      =   0  'Flat
      Caption         =   "Custom Fld1"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   94
      Tag             =   "1"
      Top             =   1620
      Width           =   975
   End
   Begin VB.Label lblField2 
      Appearance      =   0  'Flat
      Caption         =   "Custom Fld2"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   93
      Tag             =   "2"
      Top             =   1980
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   $"frmLibrary.frx":0A7D
      ForeColor       =   &H00004080&
      Height          =   855
      Left            =   20880
      TabIndex        =   90
      Top             =   60
      Width           =   4275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Timecode Offset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000040C0&
      Height          =   375
      Index           =   119
      Left            =   20880
      TabIndex        =   87
      Top             =   900
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "to Last Episode"
      Height          =   255
      Index           =   118
      Left            =   7860
      TabIndex        =   84
      Top             =   1200
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Reference"
      Height          =   255
      Index           =   115
      Left            =   5160
      TabIndex        =   80
      Top             =   2280
      Width           =   795
   End
   Begin VB.Label lblStockCode 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   26940
      TabIndex        =   72
      Tag             =   "CLEARFIELDS"
      Top             =   2580
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label lblStockDuration 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   22740
      TabIndex        =   71
      Tag             =   "CLEARFIELDS"
      Top             =   3240
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Made On"
      Height          =   255
      Index           =   89
      Left            =   10500
      TabIndex        =   54
      Top             =   120
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Series"
      Height          =   255
      Index           =   93
      Left            =   5160
      TabIndex        =   48
      Top             =   840
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Episode"
      Height          =   255
      Index           =   92
      Left            =   5160
      TabIndex        =   47
      Top             =   1200
      Width           =   615
   End
   Begin VB.Label lblCaption 
      Caption         =   "Set"
      Height          =   255
      Index           =   91
      Left            =   8040
      TabIndex        =   46
      Top             =   840
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Sub Location"
      Height          =   255
      Index           =   87
      Left            =   10500
      TabIndex        =   28
      Top             =   1560
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Project #"
      Height          =   255
      Index           =   85
      Left            =   120
      TabIndex        =   27
      Top             =   900
      Width           =   1035
   End
   Begin VB.Label lblProductID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   24120
      TabIndex        =   26
      Tag             =   "CLEARFIELDS"
      Top             =   960
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Client"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   84
      Left            =   5160
      TabIndex        =   25
      Top             =   120
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job Detail ID"
      ForeColor       =   &H00808080&
      Height          =   255
      Index           =   37
      Left            =   2700
      TabIndex        =   24
      Top             =   1260
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Version"
      Height          =   255
      Index           =   4
      Left            =   5160
      TabIndex        =   22
      Top             =   1920
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   8
      Left            =   5160
      TabIndex        =   21
      Top             =   480
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Sub Title"
      Height          =   255
      Index           =   9
      Left            =   5160
      TabIndex        =   20
      Top             =   1560
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   16
      Left            =   10500
      TabIndex        =   19
      Top             =   1200
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Foreign Ref"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   18
      Top             =   540
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "MX1 Job ID"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   17
      Top             =   1260
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Barcode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   14
      Left            =   120
      TabIndex        =   70
      Top             =   180
      Width           =   1035
   End
End
Attribute VB_Name = "frmLibrary"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_intTimecodeShuffle As Integer
Dim m_Framerate As Integer
Sub PopulateCombos()
'populate all the combos

PopulateCombo "aspectratio", ddnRatio

PopulateCombo "copytype", ddnEventType

PopulateCombo "timecode", cmbTimeCode

End Sub

Private Sub chkSpotCheckRev_Click()

If chkSpotCheckRev.Value <> 0 Then
    FullQCFieldsOn
Else
    FullQCFieldsOff
End If

End Sub

Private Sub cmbAudioStandard_DropDown()

Dim l_strCategory  As String

If Val(g_optUseAudioStandardInLibraryToStoreLanguage) <> 0 Then
    l_strCategory = "language"
Else
    l_strCategory = "audiostandard"
End If

PopulateCombo l_strCategory, cmbAudioStandard
End Sub

Private Sub cmbBox_GotFocus()
PopulateCombo "box", cmbBox
End Sub

Private Sub cmbCaptionsLanguage_DropDown()
PopulateCombo "language", cmbCaptionsLanguage
End Sub

Private Sub cmbChannel1_DropDown()
PopulateCombo "soundchannel", cmbChannel1
End Sub

Private Sub cmbChannel1_KeyPress(KeyAscii As Integer)
AutoMatch cmbChannel1, KeyAscii
End Sub

Private Sub cmbChannel10_DropDown()
PopulateCombo "soundchannel", cmbChannel10
End Sub

Private Sub cmbChannel11_DropDown()
PopulateCombo "soundchannel", cmbChannel11
End Sub

Private Sub cmbChannel12_DropDown()
PopulateCombo "soundchannel", cmbChannel12
End Sub

Private Sub cmbChannel2_DropDown()
PopulateCombo "soundchannel", cmbChannel2
End Sub

Private Sub cmbChannel2_KeyPress(KeyAscii As Integer)
AutoMatch cmbChannel2, KeyAscii
End Sub

Private Sub cmbChannel3_DropDown()
PopulateCombo "soundchannel", cmbChannel3
End Sub

Private Sub cmbChannel3_KeyPress(KeyAscii As Integer)
AutoMatch cmbChannel3, KeyAscii
End Sub

Private Sub cmbChannel4_DropDown()
PopulateCombo "soundchannel", cmbChannel4
End Sub

Private Sub cmbChannel4_KeyPress(KeyAscii As Integer)
AutoMatch cmbChannel4, KeyAscii
End Sub

Private Sub cmbChannel5_DropDown()
PopulateCombo "soundchannel", cmbChannel5
End Sub

Private Sub cmbChannel5_KeyPress(KeyAscii As Integer)
AutoMatch cmbChannel5, KeyAscii
End Sub

Private Sub cmbChannel6_DropDown()
PopulateCombo "soundchannel", cmbChannel6
End Sub

Private Sub cmbChannel6_KeyPress(KeyAscii As Integer)
AutoMatch cmbChannel6, KeyAscii
End Sub

Private Sub cmbChannel7_DropDown()
PopulateCombo "soundchannel", cmbChannel7
End Sub

Private Sub cmbChannel7_KeyPress(KeyAscii As Integer)
AutoMatch cmbChannel7, KeyAscii
End Sub

Private Sub cmbChannel8_DropDown()
PopulateCombo "soundchannel", cmbChannel8
End Sub

Private Sub cmbChannel8_KeyPress(KeyAscii As Integer)
AutoMatch cmbChannel8, KeyAscii
End Sub

Private Sub cmbChannel9_DropDown()
PopulateCombo "soundchannel", cmbChannel9
End Sub

Private Sub cmbChannelCue_KeyPress(KeyAscii As Integer)
AutoMatch cmbChannelCue, KeyAscii
End Sub

Private Sub cmbCompany_Click()

Dim l_strTemp As String, l_strSQL As String

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch2 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch2 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

l_strSQL = "SELECT * FROM masterseriestitle WHERE companyID = " & Val(lblCompanyID.Caption) & " ORDER BY title;"

With l_rstSearch2
    .CursorLocation = adUseClient
    .LockType = adLockBatchOptimistic
    .CursorType = adOpenDynamic
    .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch2.ActiveConnection = Nothing

Set txtTitle.DataSourceList = l_rstSearch2

l_conSearch.Close
Set l_conSearch = Nothing

If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/notimecodeshuffle") Then chkNoTimecodeShuffle.Value = 1
If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/BBCSTATIONARY") Then
    optStationaryType(1).Value = 1
ElseIf InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/BLANKSTATIONARY") Then
    optStationaryType(2).Value = 1
ElseIf InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/LONGTITLESTATIONARY") Then
    optStationaryType(4).Value = 1
Else
    optStationaryType(0).Value = 1
End If
    
lblField1.Caption = Trim(" " & GetData("company", "customfield1label", "companyID", lblCompanyID.Caption))
    
If lblField1.Caption = "" Then
    lblField1.Visible = False
    cmbField1.Visible = False
Else
    lblField1.Visible = True
    cmbField1.Visible = True
    PopulateCustomFieldCombo Val(lblCompanyID.Caption), 1, cmbField1
End If

lblField2.Caption = Trim(" " & GetData("company", "customfield2label", "companyID", lblCompanyID.Caption))

If lblField2.Caption = "" Then
    lblField2.Visible = False
    cmbField2.Visible = False
Else
    lblField2.Visible = True
    cmbField2.Visible = True
    PopulateCustomFieldCombo Val(lblCompanyID.Caption), 2, cmbField2
End If

lblField3.Caption = Trim(" " & GetData("company", "customfield3label", "companyID", lblCompanyID.Caption))

If lblField3.Caption = "" Then
    lblField3.Visible = False
    cmbField3.Visible = False
Else
    lblField3.Visible = True
    cmbField3.Visible = True
    PopulateCustomFieldCombo Val(lblCompanyID.Caption), 3, cmbField3
End If

If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/shineversion") > 0 Then
    lblCaption(73).Caption = "Title Code"
Else
    lblCaption(73).Caption = "Series ID"
End If


End Sub

Private Sub cmbCompany_DropDown()
adoCompany.Refresh
End Sub

Private Sub cmbCopyType_GotFocus()
Dim l_strTemp As String
l_strTemp = cmbCopyType.Text
cmbCopyType.Clear
If cmbRecordFormat.Text = "FILM" Then
    PopulateCombo "copytype", cmbCopyType, "AND (format = 'FILM')"
Else
    PopulateCombo "copytype", cmbCopyType, "AND (format is Null OR format <> 'FILM')"
End If
cmbCopyType.Text = l_strTemp
End Sub

Private Sub cmbCopyType_KeyPress(KeyAscii As Integer)
AutoMatch cmbCopyType, KeyAscii
End Sub

Private Sub cmbEBU128Loudness51Main_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128Loudness51Main
End Sub

Private Sub cmbEBU128Loudness51ME_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128Loudness51ME
End Sub

Private Sub cmbEBU128LoudnessStereoMain_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessStereoMain
End Sub

Private Sub cmbEBU128LoudnessStereoME_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessStereoME
End Sub

Private Sub cmbEpisode_GotFocus()
If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/rigorous") > 0 Then
    PopulateCombo "episode", cmbEpisode
Else
    PopulateCombo "series", cmbEpisode
End If
End Sub

Private Sub cmbEpisodeTo_GotFocus()
If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/rigorous") > 0 Then
    PopulateCombo "episode", cmbEpisodeTo
Else
    PopulateCombo "series", cmbEpisodeTo
End If
End Sub

Private Sub cmbLocation_GotFocus()
PopulateCombo "location", cmbLocation
End Sub

Private Sub cmbLocation_KeyPress(KeyAscii As Integer)
AutoMatch cmbLocation, KeyAscii
End Sub

Private Sub cmbRecordFormat_Click()
If cmbRecordFormat.Text = "MOBILEDISC" Or cmbRecordFormat.Text = "DISCSTORE" Or cmbRecordFormat.Text = "LTO5" Then cmdInitialise.Visible = True Else cmdInitialise.Visible = False
If cmbRecordFormat.Text = "DISCSTORE" Then cmdVerifyDiscstore.Visible = True Else cmdVerifyDiscstore.Visible = False
If cmbRecordFormat.Text = "FILM" Then
    lblCaption(19).Caption = "Combined Sound?"
Else
    lblCaption(19).Caption = "Machine"
End If
End Sub

Private Sub cmbRecordFormat_GotFocus()
PopulateCombo "format", cmbRecordFormat
End Sub

Private Sub cmbRecordFormat_KeyPress(KeyAscii As Integer)
AutoMatch cmbRecordFormat, KeyAscii
End Sub

Private Sub cmbRecordGeometry_DropDown()

PopulateCombo "geometry", cmbRecordGeometry
End Sub

Private Sub cmbRecordGeometry_KeyPress(KeyAscii As Integer)
AutoMatch cmbRecordGeometry, KeyAscii
End Sub

Private Sub cmbRecordMachine_Click()

'Routine to set the format and standard for the given machine
If cmbRecordFormat.Text <> "FILM" Then
    If GetData("resource", "format", "name", cmbRecordMachine.Text) <> "" Then
        cmbRecordFormat.Text = GetData("resource", "format", "name", cmbRecordMachine.Text)
        If GetData("resource", "standard", "name", cmbRecordMachine.Text) <> "" Then cmbRecordStandard.Text = GetData("resource", "standard", "name", cmbRecordMachine.Text)
    End If
End If

End Sub

Private Sub cmbRecordMachine_DropDown()
Dim l_strTemp As String
l_strTemp = cmbRecordMachine.Text
cmbRecordMachine.Clear
If cmbRecordFormat = "FILM" Then
    PopulateCombo "audiostandard", cmbRecordMachine
Else
    PopulateCombo "machine", cmbRecordMachine
End If
cmbRecordMachine.Text = l_strTemp
End Sub

Private Sub cmbRecordMachine_KeyPress(KeyAscii As Integer)
AutoMatch cmbRecordMachine, KeyAscii
End Sub

Private Sub cmbRecordRatio_DropDown()
PopulateCombo "aspectratio", cmbRecordRatio

End Sub

Private Sub cmbRecordRatio_KeyPress(KeyAscii As Integer)
AutoMatch cmbRecordRatio, KeyAscii
End Sub

Private Sub cmbRecordStandard_GotFocus()
Dim l_strTemp As String
l_strTemp = cmbRecordStandard.Text
cmbRecordStandard.Clear
If cmbRecordFormat.Text = "FILM" Then
    PopulateCombo "videostandard", cmbRecordStandard, " AND format = 'FILM'"
Else
    PopulateCombo "videostandard", cmbRecordStandard, " AND (format is Null or format <> 'FILM')"
End If
cmbRecordStandard.Text = l_strTemp
End Sub

Private Sub cmbRecordStandard_KeyPress(KeyAscii As Integer)
AutoMatch cmbRecordStandard, KeyAscii
End Sub

Private Sub cmbRecordStandard_LostFocus()

If lblLibraryID.Caption <> "" Then Library_Set_Framerate

End Sub

Private Sub cmbReviewCompany_Click()

lblReviewCompanyID.Caption = cmbReviewCompany.Columns("companyID").Text

End Sub
Private Sub cmbReviewContact_Click()

lblReviewContactID.Caption = cmbReviewContact.Columns("contactID").Text

End Sub

Private Sub cmbReviewContact_DropDown()

If lblReviewCompanyID.Caption = "" Then
    NoCompanySelectedMessage
    Exit Sub
End If

Dim l_strSQL As String
l_strSQL = "SELECT company.companyID, contact.contactID, contact.name FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((company.companyID)=" & lblReviewCompanyID.Caption & ")) ORDER BY contact.name;"

adoContact.ConnectionString = g_strConnection
adoContact.RecordSource = l_strSQL
adoContact.Refresh

End Sub

Private Sub cmbReviewMachine_DropDown()
PopulateCombo "machine", cmbReviewMachine
End Sub

Private Sub cmbReviewUser_DropDown()
PopulateCombo "operator", cmbReviewUser
End Sub

Private Sub cmbSeries_GotFocus()
PopulateCombo "series", cmbSeries
End Sub

Private Sub cmbSet_GotFocus()
PopulateCombo "series", cmbSet
End Sub

Private Sub cmbShelf_GotFocus()
PopulateCombo "shelf", cmbShelf
End Sub

Private Sub cmbSourceFormat_DropDown()
PopulateCombo "format", cmbSourceFormat
End Sub

Private Sub cmbSourceFormat_KeyPress(KeyAscii As Integer)
AutoMatch cmbSourceFormat, KeyAscii
End Sub

Private Sub cmbSourceGeometry_DropDown()
PopulateCombo "geometry", cmbSourceGeometry
End Sub

Private Sub cmbSourceGeometry_KeyPress(KeyAscii As Integer)
AutoMatch cmbSourceGeometry, KeyAscii
End Sub

Private Sub cmbSourceMachine_Click()
'Routine to set the format and standard for the given machine

cmbSourceFormat.Text = GetData("resource", "format", "name", cmbSourceMachine.Text)
cmbSourceStandard.Text = GetData("resource", "standard", "name", cmbSourceMachine.Text)

End Sub

Private Sub cmbSourceMachine_DropDown()
PopulateCombo "machine", cmbSourceMachine
End Sub

Private Sub cmbSourceMachine_KeyPress(KeyAscii As Integer)
AutoMatch cmbSourceMachine, KeyAscii
End Sub

Private Sub cmbSourceRatio_DropDown()
PopulateCombo "aspectratio", cmbSourceRatio
End Sub

Private Sub cmbSourceRatio_KeyPress(KeyAscii As Integer)
AutoMatch cmbSourceRatio, KeyAscii
End Sub

Private Sub cmbSourceStandard_DropDown()
PopulateCombo "videostandard", cmbSourceStandard
End Sub

Private Sub cmbSourceStandard_KeyPress(KeyAscii As Integer)
AutoMatch cmbSourceStandard, KeyAscii
End Sub

Private Sub cmbStockCode_Change()

cmbStockCode_Click

End Sub

Private Sub cmbStockCode_Click()

lblStockDuration.Caption = GetData("xref", "information", "description", cmbStockCode.Text)
lblStockCode.Caption = GetData("xref", "videostandard", "description", cmbStockCode.Text)

End Sub

Private Sub cmbStockCode_GotFocus()
Dim l_strStockCode As String
l_strStockCode = cmbStockCode.Text
cmbStockCode.Clear
PopulateCombo "stockcodesforformat" & cmbRecordFormat.Text, cmbStockCode
cmbStockCode.Text = l_strStockCode
End Sub

Private Sub cmbStockManufacturer_GotFocus()
PopulateCombo "stockmanufacturer", cmbStockManufacturer
End Sub

Private Sub cmbSubtitlesLanguage_DropDown()
PopulateCombo "language", cmbSubtitlesLanguage
End Sub

Private Sub cmbTechReviewList_Click()

ShowTechrev (Val(cmbTechReviewList.Columns("tapetechrevID").Text))

End Sub
Private Sub cmbTimeCode_Change()

Dim l_blnFrameAccurate As Boolean

'Set the checkbox for non-dropframe timecode duration adjustments
If cmbTimeCode.Text = "SMPTE-NDF" Then
    chkDropFrame.Enabled = True
    chkDropFrame.Value = False
Else
    chkDropFrame.Value = False
    chkDropFrame.Enabled = False
End If

If InStr(Trim(" " & GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption))), "/noframe") <> 0 Then l_blnFrameAccurate = False Else l_blnFrameAccurate = True

If lblLibraryID.Caption <> "" Then Library_Set_Framerate

'Set the event timecode strings so that the semicolon is used for drop frame
If cmbTimeCode.Text = "SMPTE-DF" Then
    If cmbRecordStandard.Text <> "525NTSC" And cmbRecordStandard.Text <> "720/P/59.94" And cmbRecordStandard.Text <> "1080/I/59.94" And cmbRecordStandard.Text <> "1080/PsF/29.97" And cmbRecordStandard.Text <> "1080/PsF/29.97 444" Then
        MsgBox "Invalid Timecode Type with this Video Standard", vbCritical, "Error"
        Exit Sub
    End If
    ChangeTimecodeDF Val(lblLibraryID.Caption), ";", m_Framerate, l_blnFrameAccurate
Else
'    If cmbRecordStandard.Text = "525NTSC" Or cmbRecordStandard.Text = "720/P/59.94" Or cmbRecordStandard.Text = "1080/I/59.94" Or cmbRecordStandard.Text = "1080/PsF/29.97" Or cmbRecordStandard.Text <> "1080/PsF/29.97 444" Then
'        MsgBox "Invalid Timecode Type with this Video Standard", vbCritical, "Error"
'        Exit Sub
'    End If
    ChangeTimecodeDF Val(lblLibraryID.Caption), ":", m_Framerate, l_blnFrameAccurate
End If

DoEvents
If adoEvents.RecordSource <> "" Then adoEvents.Refresh

End Sub

Private Sub cmbTimeCode_Click()

Dim l_blnFrameAccurate As Boolean

If cmbTimeCode.Text = "SMPTE-NDF" Then
    chkDropFrame.Value = False
    chkDropFrame.Enabled = True
Else
    chkDropFrame.Value = False
    chkDropFrame.Enabled = False
End If

If InStr(Trim(" " & GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption))), "/noframe") <> 0 Then l_blnFrameAccurate = False Else l_blnFrameAccurate = True

If lblLibraryID.Caption <> "" Then Library_Set_Framerate

'Set the event timecode strings so that the semicolon is used for drop frame
If cmbTimeCode.Text = "SMPTE-DF" Then
    If cmbRecordStandard.Text <> "525NTSC" And cmbRecordStandard.Text <> "720/P/59.94" And cmbRecordStandard.Text <> "1080/I/59.94" And cmbRecordStandard.Text <> "1080/PsF/29.97" And cmbRecordStandard.Text <> "1080/PsF/29.97 444" Then
        MsgBox "Invalid Timecode Type with this Video Standard", vbCritical, "Error"
        Exit Sub
    End If
    ChangeTimecodeDF Val(lblLibraryID.Caption), ";", m_Framerate, l_blnFrameAccurate
Else
'    If cmbRecordStandard.Text = "525NTSC" Or cmbRecordStandard.Text = "720/P/59.94" Or cmbRecordStandard.Text = "1080/I/59.94" Or cmbRecordStandard.Text = "1080/PsF/29.97" Or cmbRecordStandard.Text <> "1080/PsF/29.97 444" Then
'        MsgBox "Invalid Timecode Type with this Video Standard", vbCritical, "Error"
'        Exit Sub
'    End If
    ChangeTimecodeDF Val(lblLibraryID.Caption), ":", m_Framerate, l_blnFrameAccurate
End If

DoEvents
adoEvents.Refresh

End Sub

Private Sub cmbTimeCode_KeyPress(KeyAscii As Integer)
AutoMatch cmbTimeCode, KeyAscii
End Sub

Private Sub cmbTimeCode_LostFocus()

If lblLibraryID.Caption <> "" Then Library_Set_Framerate

End Sub

Private Sub cmbTimeCode_Validate(Cancel As Boolean)

Dim l_blnFrameAccurate As Boolean

'Set the checkbox for non-dropframe timecode duration adjustments
If cmbTimeCode.Text = "SMPTE-NDF" Then
    chkDropFrame.Value = False
    chkDropFrame.Enabled = True
Else
    chkDropFrame.Value = False
    chkDropFrame.Enabled = False
End If

If InStr(Trim(" " & GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption))), "/noframe") <> 0 Then l_blnFrameAccurate = False Else l_blnFrameAccurate = True

If lblLibraryID.Caption <> "" Then Library_Set_Framerate

'Set the event timecode strings so that the semicolon is used for drop frame
If cmbTimeCode.Text = "SMPTE-DF" Then
    If cmbRecordStandard.Text <> "525NTSC" And cmbRecordStandard.Text <> "720/P/59.94" And cmbRecordStandard.Text <> "1080/I/59.94" And cmbRecordStandard.Text <> "1080/PsF/29.97" And cmbRecordStandard.Text <> "1080/PsF/29.97 444" Then
        Cancel = True
        MsgBox "Invalid Timecode Type with this Video Standard", vbCritical, "Error"
        Exit Sub
    End If
    ChangeTimecodeDF Val(lblLibraryID.Caption), ";", m_Framerate, l_blnFrameAccurate
Else
'    If cmbRecordStandard.Text = "525NTSC" Or cmbRecordStandard.Text = "720/P/59.94" Or cmbRecordStandard.Text = "1080/I/59.94" Or cmbRecordStandard.Text = "1080/PsF/29.97" Or cmbRecordStandard.Text <> "1080/PsF/29.97 444" Then
'        Cancel = True
'        MsgBox "Invalid Timecode Type with this Video Standard", vbCritical, "Error"
'        Exit Sub
'    End If
    ChangeTimecodeDF Val(lblLibraryID.Caption), ":", m_Framerate, l_blnFrameAccurate
End If

DoEvents
adoEvents.Refresh
End Sub

Private Sub cmbTitlesLanguage_DropDown()
PopulateCombo "language", cmbTitlesLanguage
End Sub

Private Sub cmbVersion_DropDown()

If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/shineversion") > 0 Then
    PopulateCombo "shineversion", cmbVersion, "", True
Else
    PopulateCombo "version", cmbVersion, "", True
End If

End Sub

Private Sub cmbVersion_KeyPress(KeyAscii As Integer)
AutoMatch cmbVersion, KeyAscii
End Sub

Private Sub cmdAssignToLibrary_Click()

If lblLibraryID.Caption = "" Then
    NoLibraryItemSelectedMessage
    Exit Sub
Else
    If cmdAssignToLibrary.Caption = "Assign To Library" Then
        LibraryAssign Val(lblLibraryID.Caption)
        lblInLibrary.Caption = "YES"
        cmbLocation.Text = "Library"
        cmdAssignToLibrary.Caption = "Remove From Library"
        lblInLibrary.BackColor = vbGreen
    Else
        LibraryRemove Val(lblLibraryID.Caption)
        lblInLibrary.Caption = "NO"
        cmdAssignToLibrary.Caption = "Assign To Library"
        lblInLibrary.BackColor = vbRed
    End If
End If

On Error Resume Next
adoTransactions.Refresh


End Sub

Private Sub cmdBulkDuplicate_Click()
PromptLibraryChanges (Val(lblLibraryID.Caption))
DuplicateTape True
End Sub

Private Sub cmdCDLabels_Click()
Dim l_strWorkstationprops As String

l_strWorkstationprops = GetData("xref", "descriptionalias", "description", CurrentMachineName)
If (InStr(l_strWorkstationprops, "/rquest") = 0) And (InStr(l_strWorkstationprops, "/everest") = 0) Then
    MsgBox "CD Labels can only be printed on computer with R-quest Printer or Thermal printer installed", vbCritical, "Cannot Print CD Labels"
    Exit Sub
End If

PromptLibraryChanges (Val(lblLibraryID.Caption))
With frmLabelsCD
    .lblWorkstationprops.Caption = l_strWorkstationprops
    .Text1(0) = txtJobID
    .Text1(1) = txtTitle
    .Text1(2) = txtSubTitle
    .Text1(3) = cmbVersion
    .Text1(4) = cmbRecordStandard
    .Text1(5) = cmbChannel1
    .Text1(6) = cmbChannel2
    .Text1(7) = txtBarcode
    .Text1(8).Text = CIA_CODE128(txtBarcode.Text)
    .Text1(9) = cmbRecordFormat
    .Text1(10) = cmbSeries
    .Text1(11) = cmbEpisode
    If cmbEpisodeTo <> "" Then .Text1(11) = .Text1(11) & "-" & cmbEpisodeTo
    .Text1(12) = cmbSet
    .Text1(13).Text = GetData("job", "orderreference", "jobID", Val(txtJobID.Text))
    If .Text1(13).Text = "STAR ORDER" Then .Text1(13).Text = GetData("extendedjobdetail", "bbcordernumber", "jobdetailID", txtJobDetailID)

End With

frmLabelsCD.Show vbModal
Unload frmLabelsCD
Set frmLabelsCD = Nothing

End Sub


Private Sub cmdClear_Click()
PromptLibraryChanges (Val(lblLibraryID.Caption))
lblLibraryID.Caption = ""
ClearFields Me
ShowLibrary -1
End Sub
Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdCopy_Click()

're-written by sam @ 14/3/2006
'now copies the data from the text fields on the library form, so as not to miss any new fields.
'before saving, it edits the "source" information - and takes the data from the current tape.

CopyTape

ShowLibrary Val(lblLibraryID.Caption)

End Sub

Private Sub cmdCopyClips_Click()
       
Dim l_strNewLibraryBarcode As String, l_lngNewLibraryID As Long, i As Long, l_lngTotalSelRows As Long, bkmrk As Variant

'Get a barcode number for copying the clips
frmGetBarcodeNumber.Caption = "Please give the Barcode to copy clips to..."
If g_optAllocateBarcodes = 1 Then frmGetBarcodeNumber.txtBarcode.Text = g_strCompanyPrefix & GetNextSequence("barcodenumber")
frmGetBarcodeNumber.Show vbModal

l_strNewLibraryBarcode = UCase(frmGetBarcodeNumber.txtBarcode.Text)
Unload frmGetBarcodeNumber
'Check whether the new barcode was accepted, or escape was pressed.
If l_strNewLibraryBarcode = "" Then
    Exit Sub
End If
    
l_lngNewLibraryID = GetData("Library", "libraryID", "barcode", l_strNewLibraryBarcode)
If l_lngNewLibraryID = 0 Then
    MsgBox "That tape does not exist.", vbCritical, "Error copying clips"
    Exit Sub
End If

If grdEvents.SelBookmarks.Count <= 0 Then
    CopyEventToLibraryID Val(grdEvents.Columns("eventID").Text), l_lngNewLibraryID
Else
    l_lngTotalSelRows = grdEvents.SelBookmarks.Count - 1
    
    For i = 0 To l_lngTotalSelRows
        bkmrk = grdEvents.SelBookmarks(i)
        CopyEventToLibraryID Val(grdEvents.Columns("eventID").CellText(bkmrk)), l_lngNewLibraryID
    Next i
End If

If MsgBox("Do you wish to switch to viewing the new tape?", vbYesNo, "Copying Clips") = vbYes Then ShowLibrary l_lngNewLibraryID

End Sub

Private Sub cmdCopyToBarcode_Click()

CopyTapeToBarcode

ShowLibrary Val(lblLibraryID.Caption)

End Sub

Private Sub cmdCorrectLocation_Click()


If lblLibraryID.Caption = "" Then Exit Sub

Dim l_msg As Integer
l_msg = MsgBox("Are you sure you want to roll back the last movement?", vbYesNo + vbQuestion, "Rollback Movement Error")

If l_msg = vbYes Then RollbackMovementError



End Sub

Private Sub cmdDeleteTape_Click()


Dim l_intMsg As Integer
l_intMsg = MsgBox("Are you sure you wish to mark this tape as deleted?", vbYesNo + vbQuestion, "Delete Tape...")
    
If l_intMsg = vbNo Then Exit Sub

If GetData("library", "format", "libraryID", Val(lblLibraryID.Caption)) = "DISCSTORE" Then
    l_intMsg = MsgBox("This is a DISCSTORE. Are you sure you want to delete it?", vbYesNo + vbQuestion, "Deleting a DiscStore")
    If l_intMsg = vbNo Then Exit Sub
End If

l_intMsg = DeleteLibrary(Val(lblLibraryID.Caption))

If l_intMsg = True Then

    ClearFields Me
    ShowLibrary 0

End If

End Sub

Private Sub cmdDropTechReview_Click()

Dim l_intResponse As Integer

l_intResponse = MsgBox("Are you sure you wish to delete this tech review?", vbYesNo, "Delet Tech Review")
If l_intResponse = vbYes Then DeleteTechReview (Val(lbltechrevID.Caption))

End Sub

Private Sub cmdDuplicate_Click()
PromptLibraryChanges (Val(lblLibraryID.Caption))
DuplicateTape
End Sub

Private Sub cmdEditBarcode_Click()

If Val(lblLibraryID.Caption) = 0 Then
    MsgBox "You can not re-barcode this item, as it has not yet been saved." & vbCrLf & vbCrLf & "If this is a new record, and you want to use the next available barcode number; simply save this record without entering anything in the barcode box.", vbInformation
    Exit Sub
End If

EditBarcode

End Sub

Private Sub cmdExportLog_Click()

Dim l_strFileNameToSave As String
Dim l_strSQL As String, l_strStart As String, l_strEnd As String

If lblLibraryID.Caption <> "" Then

    MDIForm1.dlgMain.InitDir = g_strLocationOfRapidsLogs
    MDIForm1.dlgMain.Filter = "XML files|*.xml"
    MDIForm1.dlgMain.Filename = g_strLocationOfRapidsLogs & "\*.xml"
    
    MDIForm1.dlgMain.ShowSave
    
    l_strFileNameToSave = MDIForm1.dlgMain.Filename
    
    If l_strFileNameToSave <> "" And Right(l_strFileNameToSave, 5) <> "*.xml" Then
    
        Open l_strFileNameToSave For Output As 1
        Print #1, "<?xml version=""1.0"" encoding=""UTF-8""?>"
        Print #1, "<KLAMATH version=""1"" type=""deckcapturelog"">"
        Print #1, Chr(9) & "<TAPENAMELIST>"
        Print #1, Chr(9) & Chr(9) & "<TAPENAME tapename=""" & txtBarcode.Text & """/>"
        Print #1, Chr(9) & "</TAPENAMELIST>"
        If cmbTimeCode.Text Like "SMPTE*" Then
            Print #1, Chr(9) & "<RECORDLIST fps=""30"" timecodewrapping=""nowrap"">"
        ElseIf cmbTimeCode.Text Like "24*" Then
            Print #1, Chr(9) & "<RECORDLIST fps=""24"" timecodewrapping=""nowrap"">"
        Else
            Print #1, Chr(9) & "<RECORDLIST fps=""25"" timecodewrapping=""nowrap"">"
        End If
        adoEvents.Recordset.MoveFirst
        Do While Not adoEvents.Recordset.EOF
            l_strStart = adoEvents.Recordset("timecodestart")
            l_strEnd = adoEvents.Recordset("timecodestop")
            If cmbTimeCode.Text Like "SMPTE-DF" Then
                If Mid(l_strStart, 9, 1) = ":" Then Mid(l_strStart, 9, 1) = ";"
                If Mid(l_strEnd, 9, 1) = ":" Then Mid(l_strEnd, 9, 1) = ";"
            End If
            Print #1, Chr(9) & Chr(9) & "<RECORD enable=""-1"" tapename=""" & txtBarcode.Text;
            Print #1, """ inpoint=""" & l_strStart & """ ";
            Print #1, "outpoint=""" & l_strEnd & """ clipname=""";
            If adoEvents.Recordset("clocknumber") <> "" Then
                Print #1, SanitiseBarcode(adoEvents.Recordset("clocknumber")) & """ ";
            Else
                Print #1, SanitiseBarcode(adoEvents.Recordset("eventtitle")) & """ ";
            End If
            Print #1, "comment=""" & adoEvents.Recordset("notes") & """/>"
            adoEvents.Recordset.MoveNext
        Loop
        Print #1, Chr(9) & "</RECORDLIST>"
        Print #1, "</KLAMATH>"
        Close #1
    End If
End If

End Sub

Private Sub cmdImportLog_Click()

If lblLibraryID.Caption <> "" Then

    ImportRapidsLog Val(lblLibraryID.Caption), txtBarcode.Text
    ShowLibrary Val(lblLibraryID.Caption)
    
End If

End Sub

Private Sub cmdInitialise_Click()

Dim l_strDriveLetter As String, l_strFilePath As String, l_strTest As String
'Can't initialise a disc unless the CETA record has been saved first
If lblLibraryID.Caption = "" Then Exit Sub

'We next to get the user to tell us which drive letter the mobile disc is in.
MDIForm1.dlgMain.InitDir = ""
MDIForm1.dlgMain.Filter = "All Files|*.*"
MDIForm1.dlgMain.Filename = "libraryID.sys"
MDIForm1.dlgMain.ShowSave

l_strFilePath = MDIForm1.dlgMain.Filename
If Left(l_strFilePath, 1) = "\" Then Exit Sub
If Right(l_strFilePath, 13) <> "libraryID.sys" Then Exit Sub

'Then we write the libraryID into libraryID.sys on the root of that drive, checking first to see whether one is there already

If Dir(l_strFilePath, vbHidden Or vbSystem) = "libraryID.sys" Then
    If MsgBox("libraryID.sys already exists on that disc - should it be overwritten?", vbYesNo, "Initialising Mobile Disc") <> vbYes Then
        Exit Sub
    Else
        SetAttr l_strFilePath, vbNormal
        DoEvents
        Kill l_strFilePath
        DoEvents
    End If
End If
Open l_strFilePath For Output As 1
Print #1, lblLibraryID.Caption
Close #1
DoEvents
SetAttr l_strFilePath, vbReadOnly Or vbHidden Or vbSystem
Open App.Path & "\temp.txt" For Output As 1
Close #1
Kill App.Path & "\temp.txt"
DoEvents

End Sub

Private Sub cmdLabels_Click()
PromptLibraryChanges (Val(lblLibraryID.Caption))

If g_optUseOriginalLabels = 1 Then
    PromptLibraryChanges (Val(lblLibraryID.Caption))
    'Now labels form is loaded via a function so that it can be loaded elsewhere too.
    LoadLabelsForm
    frmLabels.Show vbModal
    Unload frmLabels
    Set frmLabels = Nothing
Else

    ClearFields frmLabelsMPC
    frmLabelsMPC.Picture1.Cls
        
    'frmLabelsMPC.txtLabel(0).Text = cmbProduct.Text
    'frmLabelsMPC.txtLabel(10).Text = cmbProduct.Text
        
    
        
    'if we don't have a selected tape
    If grdEvents.Rows = 0 Then GoTo Proc_Skip_Events
    If grdEvents.Row = grdEvents.Rows Then GoTo Proc_Skip_Events
    
    'auto add the first four available rows if possible
    If grdEvents.SelBookmarks.Count = 0 Then
        grdEvents.SelBookmarks.Add grdEvents.RowBookmark(0)
        If grdEvents.Rows >= 2 Then grdEvents.SelBookmarks.Add grdEvents.RowBookmark(1)
        If grdEvents.Rows >= 3 Then grdEvents.SelBookmarks.Add grdEvents.RowBookmark(2)
        If grdEvents.Rows >= 4 Then grdEvents.SelBookmarks.Add grdEvents.RowBookmark(3)
    End If

    'variable
    Dim l_intBookmark As Integer, l_intTotalBookmarks As Integer, l_varCurrentBookmark As Variant
    
    Dim l_intIndexToUse As Integer
    
    'store the bookmark count
    l_intTotalBookmarks = grdEvents.SelBookmarks.Count
    
    'loop through the grid bookmarks
    For l_intBookmark = 0 To l_intTotalBookmarks - 1
    
        l_intIndexToUse = l_intIndexToUse + 1
    
        'get the bookmark
        l_varCurrentBookmark = grdEvents.SelBookmarks(l_intBookmark)
        frmLabelsMPC.txtLabel(l_intIndexToUse).Text = grdEvents.Columns("eventtitle").CellText(l_varCurrentBookmark)
        
        'l_intIndexToUse = l_intIndexToUse + 1
        frmLabelsMPC.txtLabel(l_intIndexToUse).Text = frmLabelsMPC.txtLabel(l_intIndexToUse).Text & IIf(grdEvents.Columns("clocknumber").CellText(l_varCurrentBookmark) = "", "", " / ") & grdEvents.Columns("clocknumber").CellText(l_varCurrentBookmark)

    Next

    
'    frmLabelsMPC(11).Text = "1"
    
Proc_Skip_Events:
        

    frmLabelsMPC.WindowState = vbMaximized
    frmLabelsMPC.ZOrder 0

On Error GoTo Proc_Skip_High_Numbers
    frmLabelsMPC.txtLabel(l_intIndexToUse + 1).Text = "Aspect Ratio : " & grdEvents.Columns("aspectratio").Text
    frmLabelsMPC.txtLabel(l_intIndexToUse + 2).Text = Format(Now, "DD MMM YYYY") & " / " & g_strCompanyPrefix & " J/ID : " & txtJobID.Text
    
    
    'taken out request by Jonny @ MPC until we can collect correctly
    'frmLabelsMPC.txtLabel(l_intIndexToUse + 3).Text = "Source tape no. : " '& txtBarcode.Text
    
    'taken out request by Jonny @ MPC
    'frmLabelsMPC.txtLabel(l_intIndexToUse + 4).Text = cmbProduct.Text
    
    
    frmLabelsMPC.Show
    
    'Unload frmLabelsMPC
    'Set frmLabelsMPC = Nothing
End If

picPrint.Visible = False

Exit Sub

Proc_Skip_High_Numbers:
Resume Next

End Sub

Private Sub cmdMakeTechReview_Click()
MakeTechReview
End Sub

Private Sub cmdNew_Click()
Dim l_strBarcode As String

l_strBarcode = NewLibraryItem()

If l_strBarcode <> "" Then txtBarcode.Text = l_strBarcode
txtBarcode_KeyPress 13

End Sub

Private Sub cmdOverwriteExisting_Click()

PromptLibraryChanges (Val(lblLibraryID.Caption))
OverwriteExistingTape

End Sub

Private Sub cmdPrint_Click()
PromptLibraryChanges (Val(lblLibraryID.Caption))
'If we are on the tech review tabs, then then print button should print the tech review.
'Otherwise, it should print the record report.

If Val(lblLibraryID.Caption) = 0 Then
    NoLibraryItemSelectedMessage
End If

If Not CheckAccess("/printrecordreport") Then Exit Sub

Dim l_strSelectionFormula As String
l_strSelectionFormula = "{library.libraryID} = " & lblLibraryID.Caption

Dim l_strReportFile As String

Select Case tabLibraryInfo.Tab
Case 1
    l_strReportFile = g_strLocationOfCrystalReportFiles & "tape.rpt"
    l_strSelectionFormula = "{library.libraryID} = " & lblLibraryID.Caption
Case 2, 3
    If chkSpotCheckRev.Value = 0 Then
        l_strReportFile = g_strLocationOfCrystalReportFiles & "Techrev_Runtime.rpt"
    Else
        If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/techrevincitunes") > 0 Then
            l_strReportFile = g_strLocationOfCrystalReportFiles & "Techrev_full_itunes.rpt"
        Else
            l_strReportFile = g_strLocationOfCrystalReportFiles & "Techrev_full.rpt"
        End If
    End If
    l_strSelectionFormula = "{techrev.tapetechrevID} = " & lbltechrevID.Caption
Case Else
    l_strSelectionFormula = "{library.libraryID} = " & lblLibraryID.Caption
    If optStationaryType(1).Value = True Then
        l_strReportFile = g_strLocationOfCrystalReportFiles & "record2.rpt"
    ElseIf optStationaryType(2).Value = True Then
        l_strReportFile = g_strLocationOfCrystalReportFiles & "record3.rpt"
    ElseIf optStationaryType(3).Value = True Then
        l_strReportFile = g_strLocationOfCrystalReportFiles & "recordextended.rpt"
    ElseIf optStationaryType(4).Value = True Then
        l_strReportFile = g_strLocationOfCrystalReportFiles & "record4.rpt"
    Else
        l_strReportFile = g_strLocationOfCrystalReportFiles & "record.rpt"
    End If
End Select

PrintCrystalReport l_strReportFile, l_strSelectionFormula, g_blnPreviewReport

picPrint.Visible = False
End Sub

Private Sub cmdPrintBarcode_Click()

On Error GoTo PrintBarcode_Error

PromptLibraryChanges (Val(lblLibraryID.Caption))

If Val(lblLibraryID.Caption) > 0 Then

    If g_optDontUseDynoBarcodeLabel = 1 Then
        
        Dim l_strReportToPrint As String
        
        If Not CheckAccess("/printrecordreport") Then Exit Sub
        
        l_strReportToPrint = g_strLocationOfCrystalReportFiles & GetData("xref", "information", "description", CurrentMachineName)
        
        If GetData("Xref", "descriptionalias", "description", cmbStockCode.Text) = "COMP" Then
            l_strReportToPrint = g_strLocationOfCrystalReportFiles & "DLT-LTO" & Mid(GetData("xref", "information", "description", CurrentMachineName), 8, 1) & ".rpt"
        End If
    
        PrintCrystalReport l_strReportToPrint, "{library.libraryID} = " & lblLibraryID.Caption, False
        
    Else
    
    'this just dumps a standard label to the printed
        
        Printer.CurrentX = 0
        Printer.CurrentY = 0
        Printer.FontName = "Tahoma"
        Printer.FontSize = "10"
        Printer.FontBold = True  'bold
        Printer.Print IIf(txtTitle.Text = "", "", " : " & txtTitle.Text)
        Printer.Print cmbCopyType.Text
        Printer.FontBold = False 'unbold
        
        Printer.FontSize = "16"
        Printer.FontName = "IDAutomation.com Code39"
        Printer.Print "*" & txtBarcode.Text & "*"
        Printer.EndDoc
        
        picPrint.Visible = False
    
    End If
    
    
Else
    NoLibraryItemSelectedMessage
End If

picPrint.Visible = False

Exit Sub
PrintBarcode_Error:

MsgBox Err.Description, vbExclamation
Exit Sub

End Sub

Private Sub cmdPrintBarcode2_Click()

On Error GoTo PrintBarcode2_Error

PromptLibraryChanges (Val(lblLibraryID.Caption))

If Val(lblLibraryID.Caption) > 0 Then

    If g_optDontUseDynoBarcodeLabel = 1 Then
        
        Dim l_strReportToPrint As String
        
        If Not CheckAccess("/printrecordreport") Then Exit Sub
        
        l_strReportToPrint = g_strLocationOfCrystalReportFiles & GetData("xref", "information", "description", CurrentMachineName)
        
        If GetData("Xref", "descriptionalias", "description", cmbStockCode.Text) = "COMP" Then
            l_strReportToPrint = g_strLocationOfCrystalReportFiles & "DLT-LTO" & Mid(GetData("xref", "information", "description", CurrentMachineName), 8, 1) & ".rpt"
        End If
    
        PrintCrystalReport l_strReportToPrint, "{library.libraryID} = " & lblLibraryID.Caption, False, 2
        
    Else
    
    'this just dumps a standard label to the printed
        
        Printer.CurrentX = 0
        Printer.CurrentY = 0
        Printer.FontName = "Tahoma"
        Printer.FontSize = "10"
        Printer.FontBold = True  'bold
        Printer.Print IIf(txtTitle.Text = "", "", " : " & txtTitle.Text)
        Printer.Print cmbCopyType.Text
        Printer.FontBold = False 'unbold
        
        Printer.FontSize = "16"
        Printer.FontName = "IDAutomation.com Code39"
        Printer.Print "*" & txtBarcode.Text & "*"
        Printer.EndDoc
        
        picPrint.Visible = False
    
    End If
    
    
Else
    NoLibraryItemSelectedMessage
End If

picPrint.Visible = False

Exit Sub
PrintBarcode2_Error:

MsgBox Err.Description, vbExclamation
Exit Sub

End Sub

Private Sub cmdPrintMenu_Click()
picPrint.Top = picFooter.Top - picPrint.Height - 120
picPrint.Left = cmdPrintMenu.Left + picFooter.Left
picPrint.Visible = Not picPrint.Visible
End Sub

Private Sub cmdPrintSetOfThings_Click()
cmdPrintBarcode.Value = True
cmdPrint.Value = True
LoadLabelsForm
frmLabels.cmdPrint.Value = True
Unload frmLabels
End Sub

Private Sub cmdPrintSetOfTwoThings_Click()
cmdPrint.Value = True
LoadLabelsForm
frmLabels.cmdPrint.Value = True
Unload frmLabels
End Sub

Private Sub cmdRefreshEvents_Click()
adoEvents.Refresh
If Not adoEvents.Recordset.EOF Then adoEvents.Recordset.MoveLast
End Sub

Private Sub cmdSave_Click()

Dim l_blnFrameAccurate As Boolean

If InStr(Trim(" " & GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption))), "/noframe") <> 0 Then l_blnFrameAccurate = False Else l_blnFrameAccurate = True

txtTotalDuration.Text = CalcTotalDuration(adoEvents.Recordset, m_Framerate, l_blnFrameAccurate)
txtProgDuration.Text = CalcProgDuration(adoEvents.Recordset, m_Framerate, l_blnFrameAccurate)

DoEvents

'check False Record flag
If chkFalseRecord.Value <> 0 Then
    If MsgBox("Is This Correct", vbYesNo, "Tape marked as a False Record") = vbNo Then
        MsgBox "Tape Not Saved", vbCritical
        Exit Sub
    End If
End If

'Check tape has an owner

If Val(lblCompanyID.Caption) = 0 Then
    MsgBox "Please Select the Company who own this tape" & vbCrLf & "from the Company dropdown before saving", vbExclamation, "No Company"
    Exit Sub
End If

'Check the tape has a location - exit function if not

If cmbLocation.Text = "" Then
    MsgBox "Tape must have a location - please correct this. Tape Not Saved.", vbCritical, "No Tape Location"
    Exit Sub
End If

'Check the jobID for 99999 - the number given when copy tape or duplicate tape is used.

If txtJobID.Text = "999999" Then
    MsgBox "Please set the Job number correctly. Tape not saved.", vbCritical, "Job Number not set."
    Exit Sub
End If

'Check the JobID is for a started job
If Val(txtJobID.Text) <> 0 And GetStatusNumber(GetData("job", "fd_status", "jobID", Val(txtJobID.Text))) < 5 Then
    If MsgBox("JobID refers to a Job that hasn't been started yet - please confirm this is correct", vbYesNo, "Saving Clip...") = vbNo Then
        Exit Sub
    End If
End If


'Check the Stock Code

If cmbStockCode.Text = "" Then
    MsgBox "Please select a stock code for this record before saving", vbExclamation
    cmbStockCode.SetFocus
    Exit Sub
End If

txtTitle.Text = Trim(Replace(txtTitle.Text, vbCrLf, ""))
txtSeriesID.Text = Trim(Replace(txtSeriesID.Text, vbCrLf, ""))

SaveLibrary

End Sub

Private Sub cmdTechReviewCopyFaults_Click()
CopyTechReviewFaults lbltechrevID.Caption
End Sub

Private Sub cmdTechReviewPdf_Click()
If chkSpotCheckRev.Value = 0 Then
    ReportToPDF g_strLocationOfCrystalReportFiles & "Techrev_Runtime.rpt", "{techrev.tapetechrevID} = " & frmLibrary.lbltechrevID.Caption, g_strTapePDFLocation & "\" & SanitiseBarcode(frmLibrary.txtBarcode.Text) & ".pdf"
Else
    If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/techrevincitunes") > 0 Then
        ReportToPDF g_strLocationOfCrystalReportFiles & "Techrev_Full_itunes.rpt", "{techrev.tapetechrevID} = " & frmLibrary.lbltechrevID.Caption, g_strTapePDFLocation & "\" & SanitiseBarcode(frmLibrary.txtBarcode.Text) & ".pdf"
    Else
        ReportToPDF g_strLocationOfCrystalReportFiles & "Techrev_Full.rpt", "{techrev.tapetechrevID} = " & frmLibrary.lbltechrevID.Caption, g_strTapePDFLocation & "\" & SanitiseBarcode(frmLibrary.txtBarcode.Text) & ".pdf"
    End If
End If
End Sub

Private Sub cmdTimecodeOffset_Click()

Dim l_int As Integer

If txtTimecodeOffset.Text <> "" Then

    If MsgBox("Do you wish to offset all timecodes by adding " & txtTimecodeOffset.Text & "?", vbYesNo, "Timecode Offset") = vbYes Then
    
        cmdSave.Value = True
        
        If Left(txtTimecodeOffset.Text, 1) = "-" Then
            Dim temphrs As Long, tempoffset As String
            temphrs = 24 + Val(txtTimecodeOffset.Text)
            tempoffset = ""
            If temphrs < 10 Then tempoffset = "0"
            tempoffset = tempoffset & temphrs & ":"
            tempoffset = tempoffset & Mid(txtTimecodeOffset.Text, 4)
            txtTimecodeOffset.Text = tempoffset
        End If
        If adoEvents.Recordset.RecordCount > 0 Then
            adoEvents.Recordset.MoveFirst
            l_int = chkNoSorting.Value
            chkNoSorting.Value = 1
            Do While Not adoEvents.Recordset.EOF
                If Validate_Timecode(adoEvents.Recordset("timecodestart"), m_Framerate, True) Then
                    adoEvents.Recordset("timecodestart").Value = Timecode_Add(adoEvents.Recordset("timecodestart"), txtTimecodeOffset.Text, m_Framerate)
                End If
                If Validate_Timecode(adoEvents.Recordset("timecodestop"), m_Framerate, True) Then
                    adoEvents.Recordset("timecodestop").Value = Timecode_Add(adoEvents.Recordset("timecodestop"), txtTimecodeOffset.Text, m_Framerate)
                End If
                If Validate_Timecode(adoEvents.Recordset("break1"), m_Framerate, True) Then
                    adoEvents.Recordset("break1").Value = Timecode_Add(adoEvents.Recordset("break1"), txtTimecodeOffset.Text, m_Framerate)
                End If
                If Validate_Timecode(adoEvents.Recordset("break2"), m_Framerate, True) Then
                    adoEvents.Recordset("break2").Value = Timecode_Add(adoEvents.Recordset("break2"), txtTimecodeOffset.Text, m_Framerate)
                End If
                If Validate_Timecode(adoEvents.Recordset("break3"), m_Framerate, True) Then
                    adoEvents.Recordset("break3").Value = Timecode_Add(adoEvents.Recordset("break3"), txtTimecodeOffset.Text, m_Framerate)
                End If
                If Validate_Timecode(adoEvents.Recordset("break4"), m_Framerate, True) Then
                    adoEvents.Recordset("break4").Value = Timecode_Add(adoEvents.Recordset("break4"), txtTimecodeOffset.Text, m_Framerate)
                End If
                If Validate_Timecode(adoEvents.Recordset("break5"), m_Framerate, True) Then
                    adoEvents.Recordset("break5").Value = Timecode_Add(adoEvents.Recordset("break5"), txtTimecodeOffset.Text, m_Framerate)
                End If
                If Validate_Timecode(adoEvents.Recordset("endcredits"), m_Framerate, True) Then
                    adoEvents.Recordset("endcredits").Value = Timecode_Add(adoEvents.Recordset("endcredits"), txtTimecodeOffset.Text, m_Framerate)
                End If
                adoEvents.Recordset.Update
                adoEvents.Recordset.MoveNext
            Loop
            chkNoSorting.Value = l_int
        End If
    End If

    txtTimecodeOffset.Text = ""
    ShowLibrary Val(lblLibraryID.Caption)

End If

End Sub

Private Sub cmdTransfer_Click()

If Not CheckAccess("/makeadub") Then Exit Sub

frmMakeADub.txtDestinationBarcode.Text = txtBarcode.Text
frmMakeADub.txtDestinationBarcode_KeyPress vbKeyReturn
frmMakeADub.Show


End Sub

Private Sub cmdVHSLabels_Click()
PromptLibraryChanges (Val(lblLibraryID.Caption))
With frmLabelsVHS
    .Text1(0) = txtJobID
    .Text1(1) = txtBarcode
    .Text1(2) = txtTitle
    .Text1(3) = txtSubTitle
    .Text1(4) = cmbVersion
    .Text1(5) = cmbRecordStandard
    .Text1(6) = txtProgDuration
    .Text1(8) = cmbSeries
    .Text1(9) = cmbEpisode
    If cmbEpisodeTo <> "" Then .Text1(9) = .Text1(9) & "-" & cmbEpisodeTo
    .Text1(10) = cmbSet
    .Option1(0).Value = True
    .Option2(0).Value = True
End With

frmLabelsVHS.Show vbModal
Unload frmLabelsVHS
Set frmLabelsVHS = Nothing
End Sub

Private Sub cmdUndeleteTape_Click()

Dim l_intMsg As Integer
l_intMsg = MsgBox("Are you sure you wish to mark this tape as not deleted?", vbYesNo + vbQuestion, "UnDelete Tape...")
    
If l_intMsg = vbNo Then Exit Sub

l_intMsg = UnDeleteLibrary(Val(lblLibraryID.Caption))

ShowLibrary Val(lblLibraryID.Caption)

End Sub

Private Sub cmdVerifyDiscstore_Click()

If lblLibraryID.Caption = "" Then Exit Sub

Dim l_strSQL As String

l_strSQL = "INSERT INTO event_file_request (event_file_request_typeID, NewLibraryID, RequestName, RequesterEmail) VALUES ("
l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Verify Store") & ", "
l_strSQL = l_strSQL & lblLibraryID.Caption & ", "
l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
Debug.Print l_strSQL
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError
MsgBox "Verify Requested"

End Sub

Private Sub ddnEvents_Click()

grdEvents.Columns("timecodestart").Text = ddnEvents.Columns("information").Text
grdEvents.Columns("timecodestop").Text = ddnEvents.Columns("format").Text

End Sub

Private Sub Form_Activate()

If Me.Tag = "ReturnFromAssign" Then
    SetData "library", "inlibrary", "libraryID", Val(lblLibraryID.Caption), "NO"
    frmLibrary.cmbLocation.Text = GetData("library", "location", "libraryID", Val(lblLibraryID.Caption))
    Me.Tag = ""
End If
    
End Sub

Private Sub Form_Load()

'populate the company drop down (data bound)
Dim l_strSQL As String
l_strSQL = "SELECT name, accountcode, telephone, companyID FROM company WHERE (iscustomer = 1 OR isprospective = 1) AND system_active = 1 ORDER BY name"

adoCompany.ConnectionString = g_strConnection
adoCompany.RecordSource = l_strSQL
adoCompany.Refresh

'local routine
PopulateCombos

'hide the audio panel?
If g_optHideAudioChannels <> 0 Then picAudioresize.Visible = False

l_strSQL = "SELECT description, information, format FROM xref WHERE category = 'event' ORDER BY forder"
adoEventsList.ConnectionString = g_strConnection
adoEventsList.RecordSource = l_strSQL
adoEventsList.Refresh

l_strSQL = "SELECT description FROM xref WHERE category = 'fault' ORDER BY description"
adoFaultsList.ConnectionString = g_strConnection
adoFaultsList.RecordSource = l_strSQL
adoFaultsList.Refresh

If Val(g_optUseAudioStandardInLibraryToStoreLanguage) <> 0 Then
    lblCaption(88).Caption = "Language"
End If

If g_optShowLibraryStationaryTypes = 1 Then
    optStationaryType(0).Visible = True
    optStationaryType(1).Visible = True
    optStationaryType(2).Visible = True
    optStationaryType(3).Visible = True
End If
    
If g_intDisableProjects = 1 Then
    lblCaption(85).Visible = False
    txtProjectNumber.Visible = False
    grdEvents.Columns("projectnumber").Visible = False
End If

'hide the labels buttons if required
If g_optHideStandardLabelsButton = 1 Then
    cmdLabels.Visible = False
End If
    
CenterForm Me
Exit Sub

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

PromptLibraryChanges (Val(lblLibraryID.Caption))

End Sub

Private Sub Form_Resize()

On Error Resume Next

picFooter.Top = Me.ScaleHeight - picFooter.Height - 120
picFooter.Left = Me.ScaleWidth - picFooter.Width - 120

tabLibraryInfo.Height = Me.ScaleHeight - picFooter.Height - 120 - 120 - tabLibraryInfo.Top
tabLibraryInfo.Width = Me.ScaleWidth - 120 - tabLibraryInfo.Left

If tabLibraryInfo.Tab = 3 Then
    fraTechReviewConclusions.Left = tabLibraryInfo.Width / 2
    fraTechReviewConclusions.Width = tabLibraryInfo.Width / 2 - 120
End If

grdFaults.Width = tabLibraryInfo.Width / 2 - 120 - 120
grdFaults.Height = tabLibraryInfo.Height - grdFaults.Top - 120

fraTechReview.Width = tabLibraryInfo.Width - 120 - 120
fraTechReview.Height = tabLibraryInfo.Height - fraTechReview.Top - 120

If g_optHideAudioChannels <> 0 Then
    picAudioresize.Height = 0
End If

grdEvents.Height = tabLibraryInfo.Height - 120 - grdEvents.Top - 120 - txtNotes.Height - picAudioresize.Height - 120 - 120 - 120 - 120 - 120
grdEvents.Width = tabLibraryInfo.Width - 120 - 120

picAudioresize.Top = grdEvents.Top + grdEvents.Height + 120

txtNotes.Top = tabLibraryInfo.Height - 120 - txtNotes.Height
txtNotes.Width = tabLibraryInfo.Width / 2 - 120 - 120

lblEventNotes.Top = txtNotes.Top
lblEventNotes.Width = txtNotes.Width
lblEventNotes.Height = txtNotes.Height
lblEventNotes.Left = txtNotes.Left + txtNotes.Width + 120
picEventResize.Top = lblEventNotes.Top - picEventResize.Height - 120


grdTransactions.Width = tabLibraryInfo.Width - 120 - 120
grdTransactions.Height = tabLibraryInfo.Height - grdTransactions.Top - 120
grdJobsForTape.Height = tabLibraryInfo.Height - grdJobsForTape.Top - 120

End Sub

Private Sub grdEvents_AfterUpdate(RtnDispErrMsg As Integer)
If chkNoSorting.Value = 0 Then adoEvents.Refresh
End Sub

Private Sub grdEvents_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

Dim l_int As Integer
l_int = MsgBox("Are you sure you want to delete the selected rows?", vbQuestion + vbOKCancel)

If l_int = vbCancel Then
    Cancel = True
    Exit Sub
End If

Dim nTotalSelRows       As Integer
Dim i                   As Integer
Dim bkmrk               As Variant ' Bookmarks are always defined as variants
Dim l_lngEventID        As Long
Dim l_strSQL            As String


'get the total count of selected rows
nTotalSelRows = grdEvents.SelBookmarks.Count - 1

'loop through each selected row
For i = 0 To nTotalSelRows
   
    'get the current row's bookmark
    bkmrk = grdEvents.SelBookmarks(i)
    
    'get the job ID of the current selected row
    l_lngEventID = grdEvents.Columns("eventID").CellText(bkmrk)
    
    'add something to the job history to say it was applied from group edit
    l_strSQL = "DELETE FROM tapeevents WHERE eventID = '" & l_lngEventID & "';"
    
    ExecuteSQL CStr(l_strSQL), g_strExecuteError
    CheckForSQLError
    

    DoEvents
    
Next i

adoEvents.Refresh

Cancel = True

Exit Sub
PROC_UserEscaped:

End Sub

Private Sub grdEvents_BeforeUpdate(Cancel As Integer)

grdEvents.Columns("libraryID").Text = lblLibraryID.Caption
grdEvents.Columns("eventmediatype").Text = "TAPE"

'update the project and job details
grdEvents.Columns("jobID").Text = Val(txtJobID.Text)
grdEvents.Columns("projectnumber").Text = Val(txtProjectNumber.Text)
If grdEvents.Columns("Break1").Text = "00:00:00:00" Or grdEvents.Columns("Break1").Text = "00:00:00;00" Then grdEvents.Columns("Break1").Text = "": grdEvents.Columns("Break1elapsed").Text = ""
If grdEvents.Columns("Break2").Text = "00:00:00:00" Or grdEvents.Columns("Break2").Text = "00:00:00;00" Then grdEvents.Columns("Break2").Text = "": grdEvents.Columns("Break2elapsed").Text = ""
If grdEvents.Columns("Break3").Text = "00:00:00:00" Or grdEvents.Columns("Break3").Text = "00:00:00;00" Then grdEvents.Columns("Break3").Text = "": grdEvents.Columns("Break3elapsed").Text = ""
If grdEvents.Columns("Break4").Text = "00:00:00:00" Or grdEvents.Columns("Break4").Text = "00:00:00;00" Then grdEvents.Columns("Break4").Text = "": grdEvents.Columns("Break4elapsed").Text = ""
If grdEvents.Columns("Break5").Text = "00:00:00:00" Or grdEvents.Columns("Break5").Text = "00:00:00;00" Then grdEvents.Columns("Break5").Text = "": grdEvents.Columns("Break5elapsed").Text = ""
If grdEvents.Columns("endcredits").Text = "00:00:00:00" Or grdEvents.Columns("endcredits").Text = "00:00:00;00" Then grdEvents.Columns("endcredits").Text = "": grdEvents.Columns("endcreditselapsed").Text = ""

End Sub

Private Sub grdEvents_BtnClick()

If grdEvents.Columns("eventID").Text = "" Then Exit Sub

Select Case LCase(grdEvents.Columns(grdEvents.Col).Name)
Case "length"
    MakeClipFromTapeEvent grdEvents.Columns("eventID").Text
    
Case "clocknumber"
    MakeClockReferencedClipFromTapeEvent grdEvents.Columns("eventID").Text
    
Case "eventsubtitle"
    MakeNetAPorterClipFromTapeEvent grdEvents.Columns("eventID").Text

Case Else
    'Nothing

End Select

End Sub

Private Sub grdEvents_GotFocus()
m_intTimecodeShuffle = 0
End Sub

Private Sub grdEvents_InitColumnProps()

grdEvents.Columns("eventtitle").DropDownHwnd = ddnEvents.hWnd

If g_optShowAdditionalEventFields = 0 Then
    grdEvents.Columns("editor1").Visible = False
    grdEvents.Columns("editor2").Visible = False
    
    'grdEvents.Columns("clocknumber").Visible = False
    
    'grdEvents.Columns("soundlay").Visible = False
    'grdEvents.Columns("notes").Visible = False
    
    
    grdEvents.Columns("jobID").Visible = False
    
    grdEvents.Columns("eventtype").Visible = False
    grdEvents.Columns("eventtitle").Position = 0
    
    

Else

    grdEvents.Columns("sub title").Visible = False
    
    grdEvents.Columns("editor1").Visible = False
    grdEvents.Columns("editor2").Visible = False
    grdEvents.Columns("clocknumber").Visible = True
    'grdEvents.Columns("soundlay").Visible = True
    grdEvents.Columns("notes").Visible = True
    grdEvents.Columns("aspectratio").Visible = True
    grdEvents.Columns("eventtype").Visible = True
    grdEvents.Columns("aspectratio").DropDownHwnd = ddnRatio.hWnd
    grdEvents.Columns("eventtype").DropDownHwnd = ddnEventType.hWnd
End If



If g_intDisableProjects = 1 Then
    lblCaption(85).Visible = False
    txtProjectNumber.Visible = False
    grdEvents.Columns("projectnumber").Visible = False
End If

End Sub

Private Sub grdEvents_KeyPress(KeyAscii As Integer)

If grdEvents.Rows < 1 Then Exit Sub
If grdEvents.Col < 1 Then Exit Sub

If g_optUseFormattedTimeCodesInEvents = 1 Then
    If grdEvents.Columns(grdEvents.Col).Caption = "Start" Or grdEvents.Columns(grdEvents.Col).Caption = "Stop" _
    Or grdEvents.Columns(grdEvents.Col).Caption = "Break 1" Or grdEvents.Columns(grdEvents.Col).Caption = "Break 2" _
    Or grdEvents.Columns(grdEvents.Col).Caption = "Break 3" Or grdEvents.Columns(grdEvents.Col).Caption = "Break 4" _
    Or grdEvents.Columns(grdEvents.Col).Caption = "Break 5" Or grdEvents.Columns(grdEvents.Col).Caption = "End Credits" Then
        If Len(grdEvents.ActiveCell.Text) < 2 Then
            Select Case m_Framerate
                Case TC_29, TC_59
                    grdEvents.ActiveCell.Text = "00:00:00;00"
                Case Else
                    grdEvents.ActiveCell.Text = "00:00:00:00"
            End Select
        Else
            Timecode_Check_Grid grdEvents, KeyAscii, m_Framerate
        End If
    End If
End If

On Error Resume Next
If grdEvents.Columns(grdEvents.Col).Name = "clocknumber" Then
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End If

End Sub

Private Sub grdEvents_LostFocus()

Dim l_blnFrameAccurate As Boolean

If InStr(Trim(" " & GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption))), "/noframe") <> 0 Then l_blnFrameAccurate = False Else l_blnFrameAccurate = True

If grdEvents.RowChanged = True Then grdEvents.Update

txtTotalDuration.Text = CalcTotalDuration(adoEvents.Recordset, m_Framerate, l_blnFrameAccurate)
txtProgDuration.Text = CalcProgDuration(adoEvents.Recordset, m_Framerate, l_blnFrameAccurate)

End Sub

Private Sub grdEvents_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Button = 2 Then
    g_CurrentlySelectedEventID = Val(grdEvents.Columns("eventid").Text)
    Me.PopupMenu MDIForm1.mnuHiddenEventsOptions
End If
End Sub

Private Sub grdEvents_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim l_strtemptcode As String
Dim l_lngtemprow As Long
Dim l_strStartTime As String, l_strEndTime As String, l_strCalculatedDuration As String, l_blnPalFlag As Boolean, l_blnAdjustDropFrame As Boolean, l_blnFrameAccurate As Boolean
Dim l_strSQL As String, l_rst As ADODB.Recordset, l_strLastEndTime As String

If grdEvents.Row = -1 Then Exit Sub

If grdEvents.Col = -1 Then Exit Sub

lblEventNotes.Caption = grdEvents.Columns("notes").Text

'This code is for setting the start timecode of a row
'to be the same as the end timecode of the previous row, as you move from the description
'into the start timecode field.

If g_optUseFormattedTimeCodesInEvents = 1 Then
    
    If m_intTimecodeShuffle = 0 And chkNoTimecodeShuffle.Value = 0 Then
    
        If LastCol <> -1 Then
            If grdEvents.Columns(grdEvents.Col).Caption = "Start" Then
                If grdEvents.Row > 0 Then
                    l_strSQL = adoEvents.RecordSource
                    Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
                    If l_rst.RecordCount > 0 Then
                        l_rst.MoveFirst
                        If grdEvents.Columns("start").Text <> "" Then
                            Do While Not l_rst.EOF
                                If l_rst("eventID") = grdEvents.Columns("eventID").Text Then
                                    If Not l_rst.BOF Then
                                        l_rst.MovePrevious
                                        l_strLastEndTime = l_rst("timecodestop")
                                        Exit Do
                                    Else
                                        l_strLastEndTime = l_rst("timecodestop")
                                    End If
                                End If
                                l_rst.MoveNext
                            Loop
                        Else
                            l_rst.MoveLast
                            l_strLastEndTime = l_rst("timecodestop")
                        End If
                        If l_strLastEndTime = "" And l_rst.EOF Then
                            l_rst.MovePrevious
                            l_strLastEndTime = l_rst("timecodestop")
                        End If
                    End If
                    l_rst.Close
                    grdEvents.ActiveCell.Text = l_strLastEndTime
                End If
            End If
        End If
    End If
End If

If g_optUseFormattedTimeCodesInEvents = 1 Then
    
    If grdEvents.Columns(grdEvents.Col).Caption = "Start" Or grdEvents.Columns(grdEvents.Col).Caption = "Stop" _
    Or grdEvents.Columns(grdEvents.Col).Caption = "Break 1" Or grdEvents.Columns(grdEvents.Col).Caption = "Break 2" _
    Or grdEvents.Columns(grdEvents.Col).Caption = "Break 3" Or grdEvents.Columns(grdEvents.Col).Caption = "Break 4" _
    Or grdEvents.Columns(grdEvents.Col).Caption = "Break 5" Or grdEvents.Columns(grdEvents.Col).Caption = "End Credits" Then
        If grdEvents.ActiveCell.Text = "" Then
            If m_Framerate = TC_29 Or m_Framerate = TC_59 Then
                grdEvents.ActiveCell.Text = "00:00:00;00"
            Else
                grdEvents.ActiveCell.Text = "00:00:00:00"
            End If
        End If
        grdEvents.ActiveCell.SelLength = 0
        grdEvents.ActiveCell.SelStart = 0
    End If

End If

If LastCol = -1 Then Exit Sub

If g_optUseFormattedTimeCodesInEvents = 1 Then
    If m_intTimecodeShuffle = 0 Then
        If cmbRecordStandard.Text Like "625*" Then l_blnPalFlag = True Else l_blnPalFlag = False
        If cmbTimeCode.Text = "SMPTE-NDF" Then
            If chkDropFrame.Value = False Then l_blnAdjustDropFrame = True Else l_blnAdjustDropFrame = False
        End If
        If InStr(Trim(" " & GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption))), "/noframe") <> 0 Then l_blnFrameAccurate = False Else l_blnFrameAccurate = True

        If grdEvents.Columns(grdEvents.Col).Caption = "Duration" And grdEvents.Columns(LastCol).Caption = "Stop" Then
        
            l_strStartTime = grdEvents.Columns("timecodestart").Text
            l_strEndTime = grdEvents.Columns("timecodestop").Text
            If Validate_Timecode(l_strStartTime, m_Framerate) And Validate_Timecode(l_strEndTime, m_Framerate) Then
               '  check if the calculated value is set to "" if so allow a typed entry
               '  for times before 10:00:00
                l_strCalculatedDuration = Timecode_Subtract(l_strEndTime, l_strStartTime, m_Framerate)
                If l_blnFrameAccurate = False Then l_strCalculatedDuration = Duration_From_Timecode(l_strCalculatedDuration, m_Framerate)
                grdEvents.ActiveCell.Text = l_strCalculatedDuration
            End If
        End If
        
        If grdEvents.Columns(LastCol).Caption = "Break 1" Then
            l_strStartTime = grdEvents.Columns("timecodestart").Text
            l_strEndTime = grdEvents.Columns("break1").Text
            If Validate_Timecode(l_strStartTime, m_Framerate) And Validate_Timecode(l_strEndTime, m_Framerate) Then
                l_strCalculatedDuration = Timecode_Subtract(l_strEndTime, l_strStartTime, m_Framerate)
                If l_blnFrameAccurate = False Then l_strCalculatedDuration = Duration_From_Timecode(l_strCalculatedDuration, m_Framerate)
                grdEvents.Columns("break1elapsed").Text = l_strCalculatedDuration
            Else
                grdEvents.Columns("break1elapsed").Text = ""
            End If
            
        End If
        
        If grdEvents.Columns(LastCol).Caption = "Break 2" Then
            l_strStartTime = grdEvents.Columns("timecodestart").Text
            l_strEndTime = grdEvents.Columns("Break2").Text
            If Validate_Timecode(l_strStartTime, m_Framerate) And Validate_Timecode(l_strEndTime, m_Framerate) Then
                l_strCalculatedDuration = Timecode_Subtract(l_strEndTime, l_strStartTime, m_Framerate)
                If l_blnFrameAccurate = False Then l_strCalculatedDuration = Duration_From_Timecode(l_strCalculatedDuration, m_Framerate)
                grdEvents.Columns("break2elapsed").Text = l_strCalculatedDuration
            Else
                grdEvents.Columns("break2elapsed").Text = ""
            End If
        End If
        
        If grdEvents.Columns(LastCol).Caption = "Break 3" Then
            l_strStartTime = grdEvents.Columns("timecodestart").Text
            l_strEndTime = grdEvents.Columns("Break3").Text
            If Validate_Timecode(l_strStartTime, m_Framerate) And Validate_Timecode(l_strEndTime, m_Framerate) Then
                l_strCalculatedDuration = Timecode_Subtract(l_strEndTime, l_strStartTime, m_Framerate)
                If l_blnFrameAccurate = False Then l_strCalculatedDuration = Duration_From_Timecode(l_strCalculatedDuration, m_Framerate)
                grdEvents.Columns("break3elapsed").Text = l_strCalculatedDuration
            Else
                grdEvents.Columns("break3elapsed").Text = ""
            End If
        End If
        
        If grdEvents.Columns(LastCol).Caption = "Break 4" Then
            l_strStartTime = grdEvents.Columns("timecodestart").Text
            l_strEndTime = grdEvents.Columns("Break4").Text
            If Validate_Timecode(l_strStartTime, m_Framerate) And Validate_Timecode(l_strEndTime, m_Framerate) Then
                l_strCalculatedDuration = Timecode_Subtract(l_strEndTime, l_strStartTime, m_Framerate)
                If l_blnFrameAccurate = False Then l_strCalculatedDuration = Duration_From_Timecode(l_strCalculatedDuration, m_Framerate)
                grdEvents.Columns("break4elapsed").Text = l_strCalculatedDuration
            Else
                grdEvents.Columns("break4elapsed").Text = ""
            End If
        End If
        
        If grdEvents.Columns(LastCol).Caption = "Break 5" Then
            l_strStartTime = grdEvents.Columns("timecodestart").Text
            l_strEndTime = grdEvents.Columns("Break5").Text
            If Validate_Timecode(l_strStartTime, m_Framerate) And Validate_Timecode(l_strEndTime, m_Framerate) Then
                l_strCalculatedDuration = Timecode_Subtract(l_strEndTime, l_strStartTime, m_Framerate)
                If l_blnFrameAccurate = False Then l_strCalculatedDuration = Duration_From_Timecode(l_strCalculatedDuration, m_Framerate)
                grdEvents.Columns("break5elapsed").Text = l_strCalculatedDuration
            Else
                grdEvents.Columns("break5elapsed").Text = ""
            End If
        End If
        
        If grdEvents.Columns(LastCol).Caption = "End Credits" Then
            l_strStartTime = grdEvents.Columns("timecodestart").Text
            l_strEndTime = grdEvents.Columns("endcredits").Text
            If Validate_Timecode(l_strStartTime, m_Framerate) And Validate_Timecode(l_strEndTime, m_Framerate) Then
                l_strCalculatedDuration = Timecode_Subtract(l_strEndTime, l_strStartTime, m_Framerate)
                If l_blnFrameAccurate = False Then l_strCalculatedDuration = Duration_From_Timecode(l_strCalculatedDuration, m_Framerate)
                grdEvents.Columns("endcreditselapsed").Text = l_strCalculatedDuration
            Else
                grdEvents.Columns("endcreditselapsed").Text = ""
            End If
        End If
    End If
End If

End Sub

Private Sub grdFaults_BeforeUpdate(Cancel As Integer)
grdFaults.Columns("techrevID").Text = lbltechrevID.Caption
End Sub

Private Sub grdFaults_BtnClick()

If grdFaults.Columns(grdFaults.Col).Name = "fixeddate" Then
    grdFaults.ActiveCell.Text = Now
    grdFaults.Update
End If

End Sub

Private Sub grdFaults_InitColumnProps()

grdFaults.Columns("description").DropDownHwnd = ddnFaultsList.hWnd

End Sub

Private Sub grdFaults_KeyPress(KeyAscii As Integer)
If grdFaults.Col = 1 Or grdFaults.Col = 2 Then
    If Len(grdFaults.ActiveCell.Text) < 2 Then
        grdFaults.ActiveCell.Text = "00:00:00:00"
    Else
        Timecode_Check_Grid grdFaults, KeyAscii, m_Framerate
    End If
End If
End Sub

Private Sub grdFaults_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
If grdFaults.Col = 1 Or grdFaults.Col = 2 Then
    If grdFaults.ActiveCell.Text = "" Then
        grdFaults.ActiveCell.Text = "00:00:00:00"
    End If
    grdFaults.ActiveCell.SelLength = 0
    grdFaults.ActiveCell.SelStart = 0
End If
End Sub

Private Sub grdJobsForTape_DblClick()

If Val(grdJobsForTape.Columns("jobid").Text) <> 0 Then
    ShowJob Val(grdJobsForTape.Columns("jobID").Text), 0, True
'    ShowJob Val(grdJobsForTape.Columns("jobID").Text), 0, True
End If

End Sub

Private Sub grdTransactions_DblClick()
If grdTransactions.Row = grdTransactions.Rows Then Exit Sub

If Val(grdTransactions.Columns("despatchID").Text) = 0 Then GoTo TryBatch

ShowDespatchDetail grdTransactions.Columns("despatchID").Text
        
        
TryBatch:

If Val(grdTransactions.Columns("batchID").Text) = 0 Then Exit Sub

frmMovement.txtBatchNumber.Text = grdTransactions.Columns("batchID").Text
frmMovement.cmdLoadBatch.Value = True
frmMovement.Show
frmMovement.ZOrder 0


End Sub

Private Sub tabLibraryInfo_Click(PreviousTab As Integer)

Form_Resize

Select Case tabLibraryInfo.Tab
Case 0, 4
    cmdPrint.Caption = "Record Report"
Case 1
    Dim l_strSQL As String
    l_strSQL = "SELECT trans.*, despatch.despatchnumber FROM trans LEFT JOIN despatch ON trans.despatchID = despatch.despatchID WHERE libraryID = " & Val(lblLibraryID.Caption) & " ORDER BY cdate DESC"
    
    'load the transactions
    frmLibrary.adoTransactions.RecordSource = l_strSQL
    frmLibrary.adoTransactions.ConnectionString = g_strConnection
    frmLibrary.adoTransactions.Refresh
    frmLibrary.grdTransactions.Refresh
    cmdPrint.Caption = "History Report"
Case 2, 3
    cmdPrint.Caption = "Tech Review"

End Select

End Sub

Private Sub txtBarcode_GotFocus()
HighLite txtBarcode
End Sub

Private Sub txtBarcode_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    If Val(lblLibraryID.Caption) <> 0 Then PromptLibraryChanges (Val(lblLibraryID.Caption))
    LoadLibraryBarcode txtBarcode.Text
    ShowCorrectTechRevParameters
End If
End Sub

Private Sub txtForeignRef_GotFocus()
HighLite txtForeignRef
End Sub

Private Sub txtJobDetailID_GotFocus()
HighLite txtJobDetailID
End Sub

Private Sub txtJobID_GotFocus()
HighLite txtJobID
End Sub

Private Sub txtJobID_KeyPress(KeyAscii As Integer)



If KeyAscii = vbKeyReturn Then
    KeyAscii = 0
    
    If g_optDisableUpdateLibraryRecordWithJobDetails = 1 Then Exit Sub
    
    cmdSave_Click
    
    'SaveJobDetailsToLibraryRecord Val(lblLibraryID.Caption), Val(txtJobID.Text), 0
    
    If Val(txtJobID.Text) > 99999999 Then
        MsgBox "This is not a valid job ID", vbExclamation
        Exit Sub
    End If
    
    'get thejob info
    UpdateLibraryRecordWithJobDetailInfo Val(lblLibraryID.Caption), Val(txtJobID.Text), 0
    
    DoEvents
    
    ShowLibrary Val(lblLibraryID.Caption)
    
End If

End Sub

Private Sub txtProgDuration_GotFocus()
HighLite txtProgDuration
End Sub

Private Sub txtProjectNumber_GotFocus()
HighLite txtProjectNumber
End Sub

Private Sub txtProjectNumber_KeyPress(KeyAscii As Integer)

If KeyAscii = vbKeyReturn Then
    KeyAscii = 0
    cmdSave_Click
    SaveJobDetailsToLibraryRecord Val(lblLibraryID.Caption), 0, Val(txtProjectNumber.Text)
    ShowLibrary Val(lblLibraryID.Caption)
End If
End Sub

Private Sub txtProjectNumber_LostFocus()

If Not IsNumeric(txtProjectNumber.Text) And txtProjectNumber.Text <> "" Then
    MsgBox "Project number must be numeric", vbExclamation
    txtProjectNumber.Text = ""
    Exit Sub
End If

End Sub


Private Sub txtReviewJobID_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    cmbReviewCompany.Text = Trim(" " & GetData("job", "CompanyName", "jobID", txtReviewJobID.Text))
    lblReviewCompanyID.Caption = Trim(" " & GetData("job", "companyID", "jobID", txtReviewJobID.Text))
    cmbReviewContact.Text = Trim(" " & GetData("job", "ContactName", "jobID", txtReviewJobID.Text))
    lblReviewContactID.Caption = Trim(" " & GetData("job", "contactID", "jobID", txtReviewJobID.Text))
End If
End Sub

Private Sub txtSourceBarcode_GotFocus()
HighLite txtSourceBarcode
End Sub

Private Sub txtSubTitle_GotFocus()
HighLite txtSubTitle
End Sub

Private Sub txtTimecodeOffset_KeyPress(KeyAscii As Integer)

If Len(txtTimecodeOffset.Text) < 2 Then
    Select Case m_Framerate
        Case TC_29, TC_59
            txtTimecodeOffset.Text = "00:00:00;00"
        Case Else
            txtTimecodeOffset.Text = "00:00:00:00"
    End Select
    txtTimecodeOffset.SelStart = 0
    txtTimecodeOffset.SelLength = 1
Else
    Timecode_Check_Control txtTimecodeOffset, KeyAscii, m_Framerate
End If

End Sub

Private Sub txtTitle_Click()
txtSeriesID.Text = txtTitle.Columns("seriesID").Text
End Sub

Private Sub txtTitle_GotFocus()
HighLite txtTitle
End Sub

Private Sub txtTotalDuration_GotFocus()
HighLite txtTotalDuration
End Sub

Public Sub Library_Set_Framerate()

Dim FPS As Double

If cmbRecordStandard.Text = "625PAL" Then
    m_Framerate = TC_25
ElseIf cmbRecordStandard.Text = "525NTSC" Then
    Select Case cmbTimeCode.Text
        Case "SMPTE-DF"
            m_Framerate = TC_29
        Case "SMPTE-NDF"
            m_Framerate = TC_30
        Case Else
            m_Framerate = TC_25
    End Select
ElseIf Left(cmbRecordStandard.Text, 6) = "720/P/" Then
    FPS = Val(Mid(cmbRecordStandard.Text, 7))
    Select Case FPS
        Case 50
            m_Framerate = TC_50
        Case 59.94
            If cmbTimeCode.Text = "SMPTE-NDF" Then
                m_Framerate = TC_30
            Else
                m_Framerate = TC_29
            End If
        Case Else
            m_Framerate = TC_25
    End Select
ElseIf Left(cmbRecordStandard.Text, 7) = "1080/I/" Then
    FPS = Val(Mid(cmbRecordStandard.Text, 8))
    Select Case FPS
        Case 50
            m_Framerate = TC_25
        Case 59.94
            If cmbTimeCode.Text = "SMPTE-NDF" Then
                m_Framerate = TC_30
            Else
                m_Framerate = TC_29
            End If
        Case Else
            m_Framerate = TC_25
    End Select
ElseIf Left(cmbRecordStandard.Text, 9) = "1080/PsF/" Then
    FPS = Val(Mid(cmbRecordStandard.Text, 10))
    Select Case FPS
        Case 23.98, 24
            m_Framerate = TC_24
        Case 25
            m_Framerate = TC_25
        Case 29.97
            If cmbTimeCode.Text = "SMPTE-NDF" Then
                m_Framerate = TC_30
            Else
                m_Framerate = TC_29
            End If
        Case 30
            m_Framerate = TC_30
        Case Else
            m_Framerate = TC_25
    End Select
Else
    m_Framerate = TC_25
End If

End Sub

