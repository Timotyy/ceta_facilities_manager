VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmKit 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Kit Definition"
   ClientHeight    =   6465
   ClientLeft      =   3360
   ClientTop       =   2760
   ClientWidth     =   9960
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6465
   ScaleWidth      =   9960
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkIncludeOriginalItem 
      Caption         =   "Booked original resource"
      Height          =   255
      Left            =   1260
      TabIndex        =   14
      Top             =   480
      Width           =   2415
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnProducts 
      Height          =   2295
      Left            =   4680
      TabIndex        =   12
      Top             =   2100
      Width           =   4875
      DataFieldList   =   "productcode"
      _Version        =   196617
      BackColorOdd    =   8454016
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   8599
      _ExtentY        =   4048
      _StockProps     =   77
      DataFieldToDisplay=   "productcode"
   End
   Begin MSAdodcLib.Adodc adoKitDetail 
      Height          =   330
      Left            =   5040
      Top             =   6060
      Visible         =   0   'False
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoKitDetail"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton cmdSave 
      BackColor       =   &H80000007&
      Caption         =   "Save"
      Height          =   315
      Left            =   2520
      TabIndex        =   10
      ToolTipText     =   "Save"
      Top             =   4440
      Width           =   1155
   End
   Begin VB.TextBox txtProductCode 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1260
      MaxLength       =   50
      TabIndex        =   8
      ToolTipText     =   "Production Code"
      Top             =   840
      Width           =   3675
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   8700
      TabIndex        =   7
      Top             =   6060
      Width           =   1155
   End
   Begin VB.TextBox txtDescription 
      BorderStyle     =   0  'None
      Height          =   3135
      Left            =   1260
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      ToolTipText     =   "Description"
      Top             =   1200
      Width           =   3675
   End
   Begin VB.CommandButton cmdNewKit 
      Caption         =   "New Kit"
      Height          =   315
      Left            =   1260
      TabIndex        =   1
      ToolTipText     =   "Add new kit"
      Top             =   4440
      Width           =   1155
   End
   Begin VB.CommandButton cmdDeleteKit 
      Caption         =   "Delete Kit"
      Height          =   315
      Left            =   3780
      TabIndex        =   0
      ToolTipText     =   "Delete kit"
      Top             =   4440
      Width           =   1155
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbKitName 
      Height          =   255
      Left            =   1260
      TabIndex        =   2
      ToolTipText     =   "Choose a Sub Category"
      Top             =   120
      Width           =   3675
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   5424
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   6482
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   12648384
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdKitDetail 
      Bindings        =   "frmKit.frx":0000
      Height          =   5835
      Left            =   5040
      TabIndex        =   4
      Top             =   120
      Width           =   4815
      _Version        =   196617
      stylesets.count =   1
      stylesets(0).Name=   "conflict"
      stylesets(0).ForeColor=   0
      stylesets(0).BackColor=   255
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmKit.frx":001B
      BeveColorScheme =   1
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   1
      BackColorOdd    =   12648384
      RowHeight       =   423
      Columns.Count   =   6
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "kitdetailID"
      Columns(0).Name =   "kitdetailID"
      Columns(0).DataField=   "kitdetailID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "kitID"
      Columns(1).Name =   "kitID"
      Columns(1).DataField=   "kitID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "Product Code"
      Columns(2).Name =   "productcode"
      Columns(2).DataField=   "productcode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   1402
      Columns(3).Caption=   "Qty"
      Columns(3).Name =   "quantity"
      Columns(3).DataField=   "quantity"
      Columns(3).FieldLen=   256
      Columns(4).Width=   1402
      Columns(4).Caption=   "Order"
      Columns(4).Name =   "fd_orderby"
      Columns(4).DataField=   "fd_orderby"
      Columns(4).FieldLen=   256
      Columns(5).Width=   1402
      Columns(5).Caption=   "Show"
      Columns(5).Name =   "showonquote"
      Columns(5).DataField=   "showonquote"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   4
      _ExtentX        =   8493
      _ExtentY        =   10292
      _StockProps     =   79
      Caption         =   "Kit Contents"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblCaption 
      Caption         =   "No not include KIT items in this kit, as  a loop might be created."
      Height          =   255
      Index           =   4
      Left            =   180
      TabIndex        =   13
      Top             =   5700
      Width           =   4755
   End
   Begin VB.Label lblKitID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   120
      TabIndex        =   11
      Top             =   6060
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Product Code"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   9
      Top             =   840
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Description"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   6
      Top             =   1200
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Kit Name"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   1035
   End
End
Attribute VB_Name = "frmKit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmbKitName_Click()

lblKitID.Caption = GetData("kit", "kitID", "name", cmbKitName.Text)
txtProductCode.Text = GetData("kit", "productcode", "name", cmbKitName.Text)
txtDescription.Text = GetData("kit", "description", "name", cmbKitName.Text)
chkIncludeOriginalItem.Value = GetData("kit", "bookoriginalitem", "name", cmbKitName.Text)


adoKitDetail.ConnectionString = g_strConnection
adoKitDetail.RecordSource = "SELECT * FROM kitcontents WHERE kitID = '" & lblKitID.Caption & "' ORDER BY fd_orderby, kitcontentID;"
adoKitDetail.Refresh

End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdDeleteKit_Click()

Dim l_intMsg As Integer
l_intMsg = MsgBox("Are you sure you want to delete this kit (the resources contained will not be deleted)?", vbYesNo + vbQuestion)
If l_intMsg = vbNo Then Exit Sub

Dim l_strSQL As String
l_strSQL = "DELETE FROM kit WHERE kitID = '" & lblKitID.Caption & "';"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_strSQL = "DELETE FROM kitcontents WHERE kitID = '" & lblKitID.Caption & "';"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

cmbKitName.RemoveAll
PopulateCombo "kit", cmbKitName

End Sub

Private Sub cmdNewKit_Click()
Dim l_strKitName As String
l_strKitName = InputBox("Please enter the kit name")
If l_strKitName = "" Then Exit Sub

Dim l_strSQL As String
l_strSQL = "INSERT INTO kit (name, cdate, cuser) VALUES ('" & QuoteSanitise(l_strKitName) & "', '" & FormatSQLDate(Now) & "', '" & g_strUserInitials & "');"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

cmbKitName.RemoveAll
PopulateCombo "kit", cmbKitName

End Sub

Private Sub cmdSave_Click()

Dim l_strSQL As String
l_strSQL = "UPDATE kit SET productcode = '" & QuoteSanitise(txtProductCode.Text) & "', bookoriginalitem = '" & chkIncludeOriginalItem.Value & "', description = '" & QuoteSanitise(txtDescription.Text) & "' WHERE kitID = '" & lblKitID.Caption & "';"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Sub

Private Sub Form_Load()
PopulateCombo "kit", cmbKitName

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

Dim l_strSQL As String
l_strSQL = "SELECT DISTINCT productcode, description FROM resource ORDER BY productcode;"

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic, adLockReadOnly
   
End With

l_rstSearch.ActiveConnection = Nothing

Set ddnProducts.DataSource = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

CenterForm Me
End Sub

Private Sub grdKitDetail_BeforeUpdate(Cancel As Integer)
grdKitDetail.Columns("kitID").Text = lblKitID.Caption
If grdKitDetail.Columns("quantity").Text = "" Then grdKitDetail.Columns("quantity").Text = "1"
If grdKitDetail.Columns("showonquote").Text = "" Then grdKitDetail.Columns("showonquote").Text = "Yes"

End Sub

Private Sub grdKitDetail_BtnClick()

Select Case grdKitDetail.Columns(grdKitDetail.Col).Name
Case "showonquote"
    Select Case Trim(UCase(grdKitDetail.Columns("showonquote").Text))
        Case ""
            grdKitDetail.Columns("showonquote").Value = "Yes"
        Case "NO"
            grdKitDetail.Columns("showonquote").Value = "Yes"
        Case "YES"
            grdKitDetail.Columns("showonquote").Value = "No"
    End Select
    
    grdKitDetail.Update
    
End Select
End Sub

Private Sub grdKitDetail_InitColumnProps()

grdKitDetail.Columns("productcode").DropDownHwnd = ddnProducts.hWnd
End Sub
