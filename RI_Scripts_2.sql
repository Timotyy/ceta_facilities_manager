USE [cetasoft]
GO

/* CLEAN AND IMPLEMENT R.I. */
/* ======================== */

/* movement table */
/* -------------- */
/* delete movement records where library items are missing */

Delete movement 
where movement.libraryID not in(select distinct libraryID from library)
GO

/* create FK for movement link to library */
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_movement_library]') AND parent_object_id = OBJECT_ID(N'[dbo].[movement]'))
ALTER TABLE [dbo].[movement] DROP CONSTRAINT [FK_movement_library]
GO

ALTER TABLE [dbo].[movement]  WITH CHECK ADD  CONSTRAINT [FK_movement_library] FOREIGN KEY([libraryID])
REFERENCES [dbo].[library] ([libraryID])
GO

ALTER TABLE [dbo].[movement] CHECK CONSTRAINT [FK_movement_library]
GO

/* employee table */
/* -------------- */
/* update dodgy employee records where company no longer exists and set to unassigned company */

update employee 
set companyID=1 where companyId not in (select Distinct companyid from company)
GO

/* update dodgy employee records where contact no longer exists */
delete employee 
where contactID not in (select Distinct contactID from contact)
GO

/* create FK for employee link to company */
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_employee_company]') AND parent_object_id = OBJECT_ID(N'[dbo].[employee]'))
ALTER TABLE [dbo].[employee] DROP CONSTRAINT [FK_employee_company]
GO

ALTER TABLE [dbo].[employee]  WITH CHECK ADD  CONSTRAINT [FK_employee_company] FOREIGN KEY([companyID])
REFERENCES [dbo].[company] ([companyID])
GO

ALTER TABLE [dbo].[employee] CHECK CONSTRAINT [FK_employee_company]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_employee_contact]') AND parent_object_id = OBJECT_ID(N'[dbo].[employee]'))
ALTER TABLE [dbo].[employee] DROP CONSTRAINT [FK_employee_contact]
GO

ALTER TABLE [dbo].[employee]  WITH CHECK ADD  CONSTRAINT [FK_employee_contact] FOREIGN KEY([contactID])
REFERENCES [dbo].[contact] ([contactID])
GO

ALTER TABLE [dbo].[employee] CHECK CONSTRAINT [FK_employee_contact]
GO


/* library table links to company */
/* ------------------------------ */
/* update all library entries with invalid companies to the new - unassigned company id 1 */
update library
set companyid=1 
where companyid not in (select distinct companyid from company)
GO

/* create FK for library to company */
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_library_company]') AND parent_object_id = OBJECT_ID(N'[dbo].[library]'))
ALTER TABLE [dbo].[library] DROP CONSTRAINT [FK_library_company]
GO

ALTER TABLE [dbo].[library]  WITH NOCHECK ADD  CONSTRAINT [FK_library_company] FOREIGN KEY([companyID])
REFERENCES [dbo].[company] ([companyID])
NOT FOR REPLICATION 
GO

ALTER TABLE [dbo].[library] CHECK CONSTRAINT [FK_library_company]
GO

/* despatch */
/* ======== */
/* despatch table - fixing of despatch records where companyid record is missing */
/* assign the records that have missing companies to the new co.1 unassigned company */

UPDATE despatch
SET billtocompanyID=1
where billtocompanyID not in (select Distinct companyid from company)
GO

/* create FK despatch to company */

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_despatch_company]') AND parent_object_id = OBJECT_ID(N'[dbo].[despatch]'))
ALTER TABLE [dbo].[despatch] DROP CONSTRAINT [FK_despatch_company]
GO

ALTER TABLE [dbo].[despatch]  WITH CHECK ADD  CONSTRAINT [FK_despatch_company] FOREIGN KEY([billtocompanyID])
REFERENCES [dbo].[company] ([companyID])
GO

ALTER TABLE [dbo].[despatch] CHECK CONSTRAINT [FK_despatch_company]
GO

/* despatch detail FK to despatch */

Delete despatchdetail
Where despatchID not in (select Distinct despatchID from despatch)
go 

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_despatchdetail_despatch]') AND parent_object_id = OBJECT_ID(N'[dbo].[despatchdetail]'))
ALTER TABLE [dbo].[despatchdetail] DROP CONSTRAINT [FK_despatchdetail_despatch]
GO

ALTER TABLE [dbo].[despatchdetail]  WITH CHECK ADD  CONSTRAINT [FK_despatchdetail_despatch] FOREIGN KEY([despatchID])
REFERENCES [dbo].[despatch] ([despatchID])
GO

ALTER TABLE [dbo].[despatchdetail] CHECK CONSTRAINT [FK_despatchdetail_despatch]
GO

/* techrevfault */
/* ============ */

/* delete items where its a null pointer */
Delete techrevfault
Where techrevID not in (select Distinct techrevID from techrev)
go 

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_techrevfault_techrev]') AND parent_object_id = OBJECT_ID(N'[dbo].[techrevfault]'))
ALTER TABLE [dbo].[techrevfault] DROP CONSTRAINT [FK_techrevfault_techrev]
GO

ALTER TABLE [dbo].[techrevfault]  WITH CHECK ADD  CONSTRAINT [FK_techrevfault_techrev] FOREIGN KEY([techrevID])
REFERENCES [dbo].[techrev] ([techrevID])
GO

ALTER TABLE [dbo].[techrevfault] CHECK CONSTRAINT [FK_techrevfault_techrev]
GO

/* events related */
/* ============== */
/* event usage */
/* ----------- */

DELETE eventusage
WHERE eventID not in(select distinct eventID from events)
go

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_eventusage_events]') AND parent_object_id = OBJECT_ID(N'[dbo].[eventusage]'))
ALTER TABLE [dbo].[eventusage] DROP CONSTRAINT [FK_eventusage_events]
GO

ALTER TABLE [dbo].[eventusage]  WITH CHECK ADD  CONSTRAINT [FK_eventusage_events] FOREIGN KEY([eventID])
REFERENCES [dbo].[events] ([eventID])
GO

ALTER TABLE [dbo].[eventusage] CHECK CONSTRAINT [FK_eventusage_events]
GO

/* event segment */
/* ------------- */

DELETE eventsegment
WHERE eventID not in(select distinct eventID from events)
go

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_eventsegment_events]') AND parent_object_id = OBJECT_ID(N'[dbo].[eventsegment]'))
ALTER TABLE [dbo].[eventsegment] DROP CONSTRAINT [FK_eventsegment_events]
GO

ALTER TABLE [dbo].[eventsegment]  WITH CHECK ADD  CONSTRAINT [FK_eventsegment_events] FOREIGN KEY([eventID])
REFERENCES [dbo].[events] ([eventID])
GO

ALTER TABLE [dbo].[eventsegment] CHECK CONSTRAINT [FK_eventsegment_events]
GO

/* event compilation */
/* ----------------- */

DELETE eventcompilation
WHERE eventID not in(select distinct eventID from events)
go

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_eventcompilation_events]') AND parent_object_id = OBJECT_ID(N'[dbo].[eventcompilation]'))
ALTER TABLE [dbo].[eventcompilation] DROP CONSTRAINT [FK_eventcompilation_events]
GO


ALTER TABLE [dbo].[eventcompilation]  WITH CHECK ADD  CONSTRAINT [FK_eventcompilation_events] FOREIGN KEY([sourceclipID])
REFERENCES [dbo].[events] ([eventID])
GO

ALTER TABLE [dbo].[eventcompilation] CHECK CONSTRAINT [FK_eventcompilation_events]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_eventcompilation_events1]') AND parent_object_id = OBJECT_ID(N'[dbo].[eventcompilation]'))
ALTER TABLE [dbo].[eventcompilation] DROP CONSTRAINT [FK_eventcompilation_events1]
GO

ALTER TABLE [dbo].[eventcompilation]  WITH CHECK ADD  CONSTRAINT [FK_eventcompilation_events1] FOREIGN KEY([eventID])
REFERENCES [dbo].[events] ([eventID])
GO

ALTER TABLE [dbo].[eventcompilation] CHECK CONSTRAINT [FK_eventcompilation_events1]
GO

/* event history */
/* delete orphaned records */
DELETE eventhistory
WHERE eventID not in(select distinct eventID from events)

GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_eventhistory_events]') AND parent_object_id = OBJECT_ID(N'[dbo].[eventhistory]'))
ALTER TABLE [dbo].[eventhistory] DROP CONSTRAINT [FK_eventhistory_events]
GO

ALTER TABLE [dbo].[eventhistory]  WITH CHECK ADD  CONSTRAINT [FK_eventhistory_events] FOREIGN KEY([eventID])
REFERENCES [dbo].[events] ([eventID])
GO

ALTER TABLE [dbo].[eventhistory] CHECK CONSTRAINT [FK_eventhistory_events]
GO

/* event logging */
/* only delete logging orphans where eventid not null as nulls are allowed */
/* no FK enforcement as optional link by eventid or clipreference */

DELETE eventhistory
WHERE eventID not in(select distinct eventID from events) and eventID is not NULL
go

/****** Object:  View [dbo].[vw_MasterSeriesTapes]    Script Date: 12/16/2014 15:57:16 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_MasterSeriesTapes]'))
DROP VIEW [dbo].[vw_MasterSeriesTapes]
GO

/* views needed for shine master series search/browse */

CREATE VIEW [dbo].[vw_MasterSeriesTapes]
AS
SELECT        dbo.masterseriestitle.masterseriestitleID, dbo.masterseriestitle.seriesID, dbo.masterseriestitle.title, dbo.masterseriestitle.companyID, dbo.masterseriestitle.oldseriesID, dbo.masterseriestitle.cdate, 
                         dbo.masterseriestitle.mdate, dbo.masterseriestitle.cuser, dbo.masterseriestitle.muser, dbo.masterseriestitle.numberofepisodes, dbo.company.name AS companyname
FROM            dbo.masterseriestitle INNER JOIN
                         dbo.company ON dbo.masterseriestitle.companyID = dbo.company.companyID

GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_TechRevFiles]'))
DROP VIEW [dbo].[vw_TechRevFiles]
GO


CREATE VIEW [dbo].[vw_TechRevFiles]
AS
SELECT        dbo.events.eventID, dbo.events.libraryID, dbo.events.timecodestart, dbo.events.timecodestop, dbo.events.eventtitle, dbo.events.fd_length, dbo.events.cdate, dbo.events.cuser, dbo.events.mdate, 
                         dbo.events.muser, dbo.events.eventtype, dbo.events.resourceID, dbo.events.editor1, dbo.events.editor2, dbo.events.editor3, dbo.events.soundlay, dbo.events.clocknumber, dbo.events.notes, dbo.events.jobID, 
                         dbo.events.sourcelibraryID, dbo.events.aspectratio, dbo.events.projectnumber, dbo.events.eventdate, dbo.events.geometriclinearity, dbo.events.clipstoreID, dbo.events.sourceeventID, dbo.events.clipformat, 
                         dbo.events.clipcodec, dbo.events.clipaudiocodec, dbo.events.cliphorizontalpixels, dbo.events.clipverticalpixels, dbo.events.clipsoundformat, dbo.events.clipbitrate, dbo.events.videobitrate, 
                         dbo.events.audiobitrate, dbo.events.clipfilename, dbo.events.companyID, dbo.events.companyname, dbo.events.eventseries, dbo.events.eventset, dbo.events.eventepisode, dbo.events.eventversion, 
                         dbo.events.altlocation, dbo.events.clipreference, dbo.events.eventmediatype, dbo.events.clipframerate, dbo.events.cliptype, dbo.events.clipgenre, dbo.events.internalnotes, dbo.events.eventkeyframetimecode, 
                         dbo.events.eventkeyframefilename, dbo.events.customfield1, dbo.events.customfield2, dbo.events.customfield3, dbo.events.customfield4, dbo.events.customfield5, dbo.events.customfield6, 
                         dbo.events.internalreference, dbo.events.eventhighreskeyframefilename, dbo.events.online, dbo.events.encodepasses, dbo.events.cbrvbr, dbo.events.interlace, dbo.events.break1, dbo.events.break2, 
                         dbo.events.break3, dbo.events.break4, dbo.events.break5, dbo.events.endcredits, dbo.events.break1elapsed, dbo.events.break2elapsed, dbo.events.break3elapsed, dbo.events.break4elapsed, 
                         dbo.events.break5elapsed, dbo.events.endcreditselapsed, dbo.events.eventflashpreviewfilename, dbo.events.fourbythreeflag, dbo.events.bigfilesize, dbo.events.eventsubtitle, dbo.events.hidefromweb, 
                         dbo.events.provider, dbo.events.video_type, dbo.events.video_networkname, dbo.events.video_vendorID, dbo.events.video_ep_prod_no, dbo.events.video_containerID, dbo.events.video_containerposition, 
                         dbo.events.video_releasedate, dbo.events.video_copyright_cline, dbo.events.video_shortdescription, dbo.events.video_longdescription, dbo.events.video_previewstarttime, dbo.events.video_ISAN, 
                         dbo.events.md5checksum, dbo.events.rating_au, dbo.events.rating_ca, dbo.events.rating_de, dbo.events.rating_fr, dbo.events.rating_uk, dbo.events.rating_us, dbo.events.rating_jp, dbo.events.video_title, 
                         dbo.events.sound_stereo_main, dbo.events.sound_stereo_me, dbo.events.sound_ltrt_main, dbo.events.sound_ltrt_me, dbo.events.sound_surround_main, dbo.events.sound_surround_me, 
                         dbo.events.sound_other, dbo.events.video_language, dbo.events.originalspokenlocale, dbo.events.metadata_language, dbo.events.mediastreamtype, dbo.events.itunescroptop, dbo.events.itunescropbottom, 
                         dbo.events.itunescropleft, dbo.events.itunescropright, dbo.events.sha1checksum, dbo.events.break6, dbo.events.break7, dbo.events.break8, dbo.events.break6elapsed, dbo.events.break7elapsed, 
                         dbo.events.break8elapsed, dbo.events.sound_stereo_russian, dbo.events.sourcefrommedia, dbo.events.sourcetapeeventID, dbo.events.audiotypegroup1, dbo.events.audiotypegroup2, 
                         dbo.events.audiotypegroup3, dbo.events.audiotypegroup4, dbo.events.audiotypegroup5, dbo.events.audiotypegroup6, dbo.events.audiotypegroup7, dbo.events.audiotypegroup8, dbo.events.audiotypegroup9, 
                         dbo.events.audiotypegroup10, dbo.events.audiolanguagegroup1, dbo.events.audiolanguagegroup2, dbo.events.audiolanguagegroup3, dbo.events.audiolanguagegroup4, dbo.events.audiolanguagegroup5, 
                         dbo.events.audiolanguagegroup6, dbo.events.audiolanguagegroup7, dbo.events.audiolanguagegroup8, dbo.events.audiolanguagegroup9, dbo.events.audiolanguagegroup10, dbo.events.audiocontentgroup1, 
                         dbo.events.audiocontentgroup2, dbo.events.audiocontentgroup3, dbo.events.audiocontentgroup4, dbo.events.audiocontentgroup5, dbo.events.audiocontentgroup6, dbo.events.audiocontentgroup7, 
                         dbo.events.audiocontentgroup8, dbo.events.audiocontentgroup9, dbo.events.audiocontentgroup10, dbo.events.istextinpicture, dbo.events.fileversion, dbo.events.language, dbo.events.vodplatform, 
                         dbo.events.break9, dbo.events.break10, dbo.events.break11, dbo.events.break12, dbo.events.break9elapsed, dbo.events.break10elapsed, dbo.events.break11elapsed, dbo.events.break12elapsed, 
                         dbo.events.break13, dbo.events.break14, dbo.events.break15, dbo.events.break16, dbo.events.break17, dbo.events.break18, dbo.events.break13elapsed, dbo.events.break14elapsed, 
                         dbo.events.break15elapsed, dbo.events.break16elapsed, dbo.events.break17elapsed, dbo.events.break18elapsed, dbo.events.oldseriesID, dbo.events.captionsavailable, dbo.events.captionsclipID, 
                         dbo.events.captionsreason, dbo.events.seriesID, dbo.events.trackcount, dbo.events.itunestechcomment, dbo.events.lastmediainfoquery, dbo.events.sound_stereo_mmn, dbo.events.sound_surround_mmn, 
                         dbo.events.textless, dbo.events.iTunesVendorOfferCode, dbo.events.svenskprojectnumber, dbo.events.system_deleted, dbo.techrev.techrevID, dbo.techrev.reviewdate, dbo.techrev.reviewuserID, 
                         dbo.techrev.reviewuser, dbo.techrev.companyID AS Expr1, dbo.techrev.companyname AS Expr2, dbo.techrev.notesgeneral, dbo.techrev.notesvideo, dbo.techrev.notesaudio, dbo.techrev.reviewconclusion, 
                         dbo.techrev.system_deleted AS techrev_system_deleted
FROM            dbo.techrev INNER JOIN
                         dbo.events ON dbo.techrev.eventID = dbo.events.eventID

GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_TechRevTapes]'))
DROP VIEW [dbo].[vw_TechRevTapes]
GO

CREATE VIEW [dbo].[vw_TechRevTapes]
AS
SELECT        dbo.library.libraryID, dbo.library.barcode, dbo.library.jobID, dbo.library.workordernumber, dbo.library.foreignref, dbo.library.title, dbo.library.subtitle, dbo.library.companyname, dbo.library.companyID, 
                         dbo.library.inlibrary, dbo.library.format, dbo.library.copytype, dbo.library.stocktype, dbo.library.audiostandard, dbo.library.videostandard, dbo.library.ch1, dbo.library.ch2, dbo.library.ch3, dbo.library.ch4, 
                         dbo.library.ch5, dbo.library.ch6, dbo.library.ch7, dbo.library.ch8, dbo.library.timecode, dbo.library.shelf, dbo.library.notes1, dbo.library.notes2, dbo.library.cuser, dbo.library.cdate, dbo.library.muser, 
                         dbo.library.mdate, dbo.library.modate, dbo.library.fd_status, dbo.library.reviewtype, dbo.library.aspectratio, dbo.library.geometriclinearity, dbo.library.gradevision, dbo.library.gradesound, 
                         dbo.library.sourcereference, dbo.library.sourceformat, dbo.library.sourcemachine, dbo.library.recordmachine, dbo.library.sourcevideostandard, dbo.library.sourceaspectratio, dbo.library.sourcegeometriclinearity, 
                         dbo.library.totalduration, dbo.library.nr1, dbo.library.nr2, dbo.library.nr3, dbo.library.nr4, dbo.library.nr5, dbo.library.nr6, dbo.library.nr7, dbo.library.nr8, dbo.library.cuetrack, dbo.library.progduration, 
                         dbo.library.sourcestock, dbo.library.version, dbo.library.jobdetailID, dbo.library.code128barcode, dbo.library.flagincomplete, dbo.library.stockduration, dbo.library.stockcode, dbo.library.series, 
                         dbo.library.episode, dbo.library.seriesset, dbo.library.productID, dbo.library.productname, dbo.library.projectID, dbo.library.projectnumber, dbo.library.labelnotes, dbo.library.ch9, dbo.library.ch10, 
                         dbo.library.ch11, dbo.library.ch12, dbo.library.nr9, dbo.library.nr10, dbo.library.nr11, dbo.library.nr12, dbo.library.previewclipfilename, dbo.library.internalreference, dbo.library.reference, 
                         dbo.library.checkedlabels, dbo.library.checkedrecreport, dbo.library.checkedtapespec, dbo.library.checkedby, dbo.library.endepisode, dbo.library.location, dbo.library.customfield1, dbo.library.customfield2, 
                         dbo.library.customfield3, dbo.library.customfield4, dbo.library.customfield5, dbo.library.customfield6, dbo.library.locationJobID, dbo.library.oldseriesID, dbo.library.box, dbo.library.seriesID, 
                         dbo.library.flagfalserecord, dbo.library.sound_stereo_main, dbo.library.sound_stereo_me, dbo.library.sound_ltrt_main, dbo.library.sound_ltrt_me, dbo.library.sound_surround_main, 
                         dbo.library.sound_surround_me, dbo.library.sound_stereo_mmn, dbo.library.sound_surround_mmn, dbo.library.textless, dbo.library.system_deleted, dbo.techrev.techrevID, dbo.techrev.reviewdate, 
                         dbo.techrev.reviewuserID, dbo.techrev.reviewuser, dbo.techrev.notesgeneral, dbo.techrev.notesvideo, dbo.techrev.notesaudio, dbo.techrev.reviewconclusion, dbo.techrev.system_deleted AS techrev_system_deleted
FROM            dbo.library INNER JOIN
                         dbo.techrev ON dbo.library.libraryID = dbo.techrev.libraryID

GO
