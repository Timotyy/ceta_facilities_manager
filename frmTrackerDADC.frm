VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTrackerDADC 
   Caption         =   "DADC Tracker"
   ClientHeight    =   14685
   ClientLeft      =   225
   ClientTop       =   -4275
   ClientWidth     =   24060
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   14685
   ScaleWidth      =   24060
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdExportGrid 
      Caption         =   "Export Grid"
      Height          =   315
      Left            =   2880
      TabIndex        =   153
      Top             =   4680
      Width           =   1635
   End
   Begin VB.CommandButton cmdManualMarkAllBilled 
      Caption         =   "Manually Mark All as Billed"
      Height          =   315
      Left            =   14280
      TabIndex        =   152
      Top             =   4680
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.TextBox txtAS11JobID 
      Height          =   315
      Left            =   10080
      TabIndex        =   145
      Top             =   1800
      Width           =   1455
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "AS-11 Billed"
      Height          =   255
      Index           =   28
      Left            =   11640
      TabIndex        =   144
      Tag             =   "NOCLEAR"
      Top             =   4260
      Width           =   2115
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "AS-11 FInished"
      Height          =   255
      Index           =   27
      Left            =   11640
      TabIndex        =   143
      Tag             =   "NOCLEAR"
      Top             =   3960
      Width           =   2115
   End
   Begin VB.CommandButton cmdAS11Fields 
      Caption         =   "AS-11 Fields Only"
      Height          =   315
      Left            =   1560
      TabIndex        =   142
      Top             =   4320
      Width           =   1515
   End
   Begin VB.Frame fraReport 
      BorderStyle     =   0  'None
      Caption         =   "v"
      Height          =   375
      Left            =   60
      TabIndex        =   139
      Top             =   4620
      Width           =   2895
      Begin VB.CommandButton cmdReport 
         Caption         =   "Report"
         Height          =   315
         Left            =   1980
         TabIndex        =   141
         Top             =   60
         Width           =   795
      End
      Begin VB.CommandButton cmdZipAndExport 
         Caption         =   "ZIP, Export and Send"
         Height          =   315
         Left            =   0
         TabIndex        =   140
         Top             =   60
         Width           =   1875
      End
   End
   Begin VB.CommandButton cmdBulkAudioFields 
      Caption         =   "Bulk Audio Fields Only"
      Height          =   315
      Left            =   3120
      TabIndex        =   138
      Top             =   4320
      Width           =   1815
   End
   Begin VB.CommandButton cmdDTFields 
      Caption         =   "DT Fields Only"
      Height          =   315
      Left            =   6480
      TabIndex        =   131
      Top             =   4320
      Width           =   1275
   End
   Begin VB.TextBox txtDTJobID 
      Height          =   315
      Left            =   10080
      TabIndex        =   129
      Top             =   1440
      Width           =   1455
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Decision Tree Billed"
      Height          =   255
      Index           =   26
      Left            =   11640
      TabIndex        =   128
      Tag             =   "NOCLEAR"
      Top             =   3660
      Width           =   2115
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Decision Tree Finished"
      Height          =   255
      Index           =   25
      Left            =   11640
      TabIndex        =   127
      Tag             =   "NOCLEAR"
      Top             =   3360
      Width           =   2115
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Cancelled"
      Height          =   255
      Index           =   24
      Left            =   13860
      TabIndex        =   124
      Tag             =   "NOCLEAR"
      Top             =   3060
      Width           =   1995
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Workable"
      Height          =   255
      Index           =   23
      Left            =   11640
      TabIndex        =   123
      Tag             =   "NOCLEAR"
      Top             =   1200
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "All Incomplete Items"
      Height          =   255
      Index           =   22
      Left            =   11640
      TabIndex        =   122
      Tag             =   "NOCLEAR"
      Top             =   900
      Width           =   1815
   End
   Begin VB.CommandButton cmdMakeFixJobsheet 
      Caption         =   "Make Fix Jobsheet for Item"
      Height          =   315
      Left            =   19260
      TabIndex        =   121
      Top             =   1800
      Visible         =   0   'False
      Width           =   2595
   End
   Begin VB.TextBox txtJobID 
      Height          =   315
      Left            =   10080
      TabIndex        =   118
      Top             =   1080
      Width           =   1455
   End
   Begin VB.TextBox txtSpecialProject 
      Height          =   315
      Left            =   5040
      TabIndex        =   116
      Top             =   3240
      Width           =   3675
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Rush Jobs"
      Height          =   255
      Index           =   21
      Left            =   11640
      TabIndex        =   113
      Tag             =   "NOCLEAR"
      Top             =   300
      Width           =   1815
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Update Target Dates"
      Height          =   315
      Left            =   19260
      TabIndex        =   112
      Top             =   1440
      Visible         =   0   'False
      Width           =   2595
   End
   Begin VB.TextBox txtContentVersionCode 
      Height          =   315
      Left            =   5040
      TabIndex        =   110
      Top             =   1800
      Width           =   3675
   End
   Begin VB.CheckBox chk48HrDue 
      Caption         =   "Target Date"
      ForeColor       =   &H000000FF&
      Height          =   315
      Left            =   8910
      TabIndex        =   91
      Top             =   4320
      Width           =   855
   End
   Begin VB.CheckBox chkHideDemo 
      Alignment       =   1  'Right Justify
      Caption         =   "Hide Demo"
      Height          =   255
      Left            =   120
      TabIndex        =   88
      Tag             =   "NOCLEAR"
      Top             =   3720
      Value           =   1  'Checked
      Width           =   1095
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Decision Tree"
      Height          =   255
      Index           =   20
      Left            =   11640
      TabIndex        =   87
      Tag             =   "NOCLEAR"
      Top             =   3060
      Width           =   2115
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   315
      Left            =   7800
      TabIndex        =   86
      Top             =   4680
      Visible         =   0   'False
      Width           =   6435
      _ExtentX        =   11351
      _ExtentY        =   556
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.TextBox txtTotalDurationEstimate 
      Height          =   315
      Left            =   21000
      TabIndex        =   82
      Top             =   300
      Visible         =   0   'False
      Width           =   795
   End
   Begin VB.TextBox txtTotalSize 
      Height          =   315
      Left            =   21000
      TabIndex        =   81
      Top             =   1020
      Width           =   795
   End
   Begin VB.TextBox txtTotalDuration 
      Height          =   315
      Left            =   21000
      TabIndex        =   80
      Top             =   660
      Width           =   795
   End
   Begin VB.Frame frmTotals 
      Caption         =   "Totals"
      Height          =   4455
      Left            =   15960
      TabIndex        =   73
      Top             =   60
      Width           =   3195
      Begin VB.TextBox txtXMLMade 
         Height          =   315
         Left            =   2460
         TabIndex        =   115
         Top             =   2760
         Width           =   615
      End
      Begin VB.TextBox txtHDSR 
         Height          =   315
         Left            =   2460
         TabIndex        =   104
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox txtHDCAM 
         Height          =   315
         Left            =   2460
         TabIndex        =   103
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox txtDBETA 
         Height          =   315
         Left            =   2460
         TabIndex        =   102
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox txtOTHER 
         Height          =   315
         Left            =   2460
         TabIndex        =   101
         Top             =   1320
         Width           =   615
      End
      Begin VB.TextBox txtCount48 
         Height          =   315
         Left            =   2460
         TabIndex        =   98
         Top             =   1680
         Width           =   615
      End
      Begin VB.TextBox txtCount48TapesHere 
         Height          =   315
         Left            =   2460
         TabIndex        =   97
         Top             =   2040
         Width           =   615
      End
      Begin VB.TextBox txtDecisionTree 
         Height          =   315
         Left            =   2460
         TabIndex        =   89
         Top             =   3660
         Width           =   615
      End
      Begin VB.TextBox txtQueuedForDelivery 
         Height          =   315
         Left            =   2460
         TabIndex        =   76
         Top             =   4020
         Width           =   615
      End
      Begin VB.TextBox txtItemsOnPending 
         Height          =   315
         Left            =   2460
         TabIndex        =   75
         Top             =   3300
         Width           =   615
      End
      Begin VB.TextBox txtFilesMade 
         Height          =   315
         Left            =   2460
         TabIndex        =   74
         Top             =   2400
         Width           =   615
      End
      Begin VB.Label lblCaption 
         Caption         =   "Count XML Made"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   30
         Left            =   120
         TabIndex        =   114
         Top             =   2820
         Width           =   1575
      End
      Begin VB.Line Line1 
         BorderColor     =   &H000000FF&
         X1              =   180
         X2              =   3060
         Y1              =   3180
         Y2              =   3180
      End
      Begin VB.Label lblCaption 
         Caption         =   "Count HDCAM-SR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   15
         Left            =   120
         TabIndex        =   108
         Top             =   300
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Count HDCAM"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   16
         Left            =   120
         TabIndex        =   107
         Top             =   660
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Count DBETA"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   17
         Left            =   120
         TabIndex        =   106
         Top             =   1020
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Count Other"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   18
         Left            =   120
         TabIndex        =   105
         Top             =   1380
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Count DueDate (Def 48Hr)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   25
         Left            =   120
         TabIndex        =   100
         Top             =   1740
         Width           =   2355
      End
      Begin VB.Label lblCaption 
         Caption         =   "Count DueDate Tape Here"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   26
         Left            =   120
         TabIndex        =   99
         Top             =   2100
         Width           =   2235
      End
      Begin VB.Label lblCaption 
         Caption         =   "Count Put on D-Tree"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   27
         Left            =   120
         TabIndex        =   90
         Top             =   3720
         Width           =   1875
      End
      Begin VB.Label lblCaption 
         Caption         =   "Count Queued to Send"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   24
         Left            =   120
         TabIndex        =   79
         Top             =   4080
         Width           =   1875
      End
      Begin VB.Label lblCaption 
         Caption         =   "Count Put on Pending"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   23
         Left            =   120
         TabIndex        =   78
         Top             =   3360
         Width           =   1875
      End
      Begin VB.Label lblCaption 
         Caption         =   "Count Files Made"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   77
         Top             =   2460
         Width           =   1575
      End
   End
   Begin VB.CommandButton cmdDuplicateRow 
      Caption         =   "Duplicate Row"
      Height          =   315
      Left            =   21840
      TabIndex        =   72
      Top             =   4200
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Sent file via Drive"
      Height          =   255
      Index           =   19
      Left            =   13860
      TabIndex        =   71
      Tag             =   "NOCLEAR"
      Top             =   900
      Width           =   1995
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Items being Fixed"
      Height          =   255
      Index           =   18
      Left            =   8880
      TabIndex        =   70
      Tag             =   "NOCLEAR"
      Top             =   3960
      Width           =   2355
   End
   Begin VB.TextBox txtStoredOn 
      Height          =   315
      Left            =   5040
      TabIndex        =   68
      Top             =   2880
      Width           =   3675
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Sent XML without File"
      Height          =   255
      Index           =   17
      Left            =   13860
      TabIndex        =   67
      Tag             =   "NOCLEAR"
      Top             =   600
      Width           =   1995
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "File Made but XML NOT Made"
      Height          =   255
      Index           =   16
      Left            =   8880
      TabIndex        =   66
      Tag             =   "NOCLEAR"
      Top             =   2760
      Width           =   2415
   End
   Begin VB.TextBox txtNewBarcode 
      Height          =   315
      Left            =   5040
      TabIndex        =   64
      Top             =   1080
      Width           =   3675
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Duplicate Items"
      Height          =   255
      Index           =   15
      Left            =   8880
      TabIndex        =   63
      Tag             =   "NOCLEAR"
      Top             =   3660
      Width           =   2355
   End
   Begin VB.CheckBox chkDontVerifyXML 
      Alignment       =   1  'Right Justify
      Caption         =   "Don't Verify XML"
      Height          =   255
      Left            =   60
      TabIndex        =   61
      Top             =   840
      Width           =   1695
   End
   Begin VB.TextBox txtRequestID 
      Height          =   315
      Left            =   10080
      TabIndex        =   59
      Top             =   720
      Width           =   1455
   End
   Begin VB.Frame fraOrderBy 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   1575
      Left            =   60
      TabIndex        =   54
      Top             =   1260
      Width           =   2475
      Begin VB.OptionButton optOrderBy 
         Caption         =   "DT Target Date"
         Height          =   315
         Index           =   4
         Left            =   0
         TabIndex        =   137
         Top             =   900
         Width           =   2415
      End
      Begin VB.OptionButton optOrderBy 
         Caption         =   "Item Reference"
         Height          =   315
         Index           =   3
         Left            =   0
         TabIndex        =   58
         Top             =   1200
         Width           =   1815
      End
      Begin VB.OptionButton optOrderBy 
         Caption         =   "Target Date (with Rush)"
         Height          =   315
         Index           =   2
         Left            =   0
         TabIndex        =   57
         Top             =   600
         Value           =   -1  'True
         Width           =   2355
      End
      Begin VB.OptionButton optOrderBy 
         Caption         =   "Sort by Request ID"
         Height          =   315
         Index           =   1
         Left            =   0
         TabIndex        =   56
         Top             =   300
         Width           =   1755
      End
      Begin VB.OptionButton optOrderBy 
         Caption         =   "Sort by 'Stored On'"
         Height          =   315
         Index           =   0
         Left            =   0
         TabIndex        =   55
         Top             =   0
         Width           =   1815
      End
   End
   Begin VB.TextBox txtActualSQL 
      Height          =   315
      Left            =   7800
      TabIndex        =   49
      Top             =   4680
      Width           =   6315
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Tape Sent Back"
      Height          =   255
      Index           =   14
      Left            =   13860
      TabIndex        =   48
      Tag             =   "NOCLEAR"
      Top             =   2460
      Width           =   1995
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "XML Not Made"
      Height          =   255
      Index           =   13
      Left            =   11640
      TabIndex        =   47
      Tag             =   "NOCLEAR"
      Top             =   2460
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "XML Made"
      Height          =   255
      Index           =   12
      Left            =   11640
      TabIndex        =   46
      Tag             =   "NOCLEAR"
      Top             =   2760
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "File Not Made"
      Height          =   255
      Index           =   11
      Left            =   11640
      TabIndex        =   45
      Tag             =   "NOCLEAR"
      Top             =   1860
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Tape Not Here"
      Height          =   255
      Index           =   10
      Left            =   11640
      TabIndex        =   44
      Tag             =   "NOCLEAR"
      Top             =   600
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Sent with XML"
      Height          =   255
      Index           =   9
      Left            =   13860
      TabIndex        =   43
      Tag             =   "NOCLEAR"
      Top             =   300
      Width           =   1995
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "XML Made but Not Sent"
      Height          =   255
      Index           =   8
      Left            =   8880
      TabIndex        =   42
      Tag             =   "NOCLEAR"
      Top             =   3060
      Width           =   2175
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Pending"
      Height          =   255
      Index           =   7
      Left            =   8880
      TabIndex        =   41
      Tag             =   "NOCLEAR"
      Top             =   3360
      Width           =   2355
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "File Made but not Sent"
      Height          =   255
      Index           =   6
      Left            =   8880
      TabIndex        =   40
      Tag             =   "NOCLEAR"
      Top             =   2460
      Width           =   2175
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Sent without XML"
      Height          =   255
      Index           =   5
      Left            =   13860
      TabIndex        =   39
      Tag             =   "NOCLEAR"
      Top             =   0
      Width           =   1995
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "File Made"
      Height          =   255
      Index           =   4
      Left            =   11640
      TabIndex        =   38
      Tag             =   "NOCLEAR"
      Top             =   2160
      Width           =   1815
   End
   Begin VB.CommandButton cmdAllFields 
      Caption         =   "All Fields"
      Height          =   315
      Left            =   7800
      TabIndex        =   36
      Top             =   4320
      Width           =   855
   End
   Begin VB.CommandButton cmdSomeFields 
      Caption         =   "Ingest Fields Only"
      Height          =   315
      Left            =   4920
      TabIndex        =   35
      Top             =   4320
      Width           =   1515
   End
   Begin VB.TextBox txtReference 
      Height          =   315
      Left            =   5040
      TabIndex        =   34
      Top             =   2160
      Width           =   3675
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "All Items"
      Height          =   255
      Index           =   3
      Left            =   13860
      TabIndex        =   30
      Tag             =   "NOCLEAR"
      Top             =   3660
      Width           =   2055
   End
   Begin VB.CheckBox chkLockGB 
      Alignment       =   1  'Right Justify
      Caption         =   "Lock GB size"
      Height          =   255
      Left            =   60
      TabIndex        =   29
      Top             =   480
      Width           =   1695
   End
   Begin VB.CheckBox chkLockDur 
      Alignment       =   1  'Right Justify
      Caption         =   "Lock Duration"
      Height          =   255
      Left            =   60
      TabIndex        =   28
      Top             =   120
      Width           =   1695
   End
   Begin VB.TextBox txtBBCCoreID 
      Height          =   315
      Left            =   5040
      TabIndex        =   24
      Top             =   2520
      Width           =   3675
   End
   Begin VB.TextBox txtFormat 
      Height          =   315
      Left            =   5040
      TabIndex        =   23
      Top             =   1440
      Width           =   3675
   End
   Begin VB.TextBox txtBarcode 
      Height          =   315
      Left            =   5040
      TabIndex        =   22
      Top             =   720
      Width           =   3675
   End
   Begin VB.TextBox txtBatch 
      Height          =   315
      Left            =   10080
      TabIndex        =   18
      Top             =   360
      Width           =   1455
   End
   Begin VB.TextBox txtEpisode 
      Height          =   315
      Left            =   10080
      TabIndex        =   15
      Top             =   0
      Width           =   1455
   End
   Begin VB.TextBox txtSubtitle 
      Height          =   315
      Left            =   5040
      TabIndex        =   14
      Top             =   360
      Width           =   3675
   End
   Begin VB.TextBox txtTitle 
      Height          =   315
      Left            =   5040
      TabIndex        =   11
      Top             =   0
      Width           =   3675
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Billed"
      Height          =   255
      Index           =   2
      Left            =   13860
      TabIndex        =   8
      Tag             =   "NOCLEAR"
      Top             =   3360
      Width           =   1995
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Finished"
      Height          =   255
      Index           =   1
      Left            =   13860
      TabIndex        =   7
      Tag             =   "NOCLEAR"
      Top             =   2760
      Width           =   1995
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete"
      Height          =   255
      Index           =   0
      Left            =   11640
      TabIndex        =   6
      Tag             =   "NOCLEAR"
      Top             =   0
      Width           =   1815
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   14160
      Width           =   26655
      Begin VB.CommandButton cmdAS11UnbillAll 
         Caption         =   "AS11 UnBill All"
         Height          =   315
         Left            =   4320
         TabIndex        =   151
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdAS11BillAll 
         Caption         =   "AS11 Bill All"
         Height          =   315
         Left            =   7500
         TabIndex        =   150
         Top             =   0
         Width           =   1095
      End
      Begin VB.CommandButton cmdUnbillAS11 
         Caption         =   "Unmark as AS11 Billed"
         Height          =   315
         Left            =   11760
         TabIndex        =   149
         Top             =   0
         Visible         =   0   'False
         Width           =   1695
      End
      Begin VB.CommandButton cmdManualBillAS11Item 
         Caption         =   "Manually Mark as AS11 Billed"
         Height          =   315
         Left            =   17580
         TabIndex        =   148
         Top             =   0
         Visible         =   0   'False
         Width           =   2355
      End
      Begin VB.CommandButton cmdAS11BillItem 
         Caption         =   "AS11 Bill Item"
         Height          =   315
         Left            =   22020
         TabIndex        =   147
         Top             =   0
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.CommandButton cmdDTUnbillAll 
         Caption         =   "DT UnBill All"
         Height          =   315
         Left            =   3240
         TabIndex        =   136
         Top             =   0
         Width           =   1035
      End
      Begin VB.CommandButton cmdDTBillAll 
         Caption         =   "DT Bill All"
         Height          =   315
         Left            =   6540
         TabIndex        =   135
         Top             =   0
         Width           =   915
      End
      Begin VB.CommandButton cmdUnbillDT 
         Caption         =   "Unmark as DT Billed"
         Height          =   315
         Left            =   10200
         TabIndex        =   134
         Top             =   0
         Visible         =   0   'False
         Width           =   1515
      End
      Begin VB.CommandButton cmdManualDTBillItem 
         Caption         =   "Manually Mark as DT Billed"
         Height          =   315
         Left            =   15420
         TabIndex        =   133
         Top             =   0
         Visible         =   0   'False
         Width           =   2115
      End
      Begin VB.CommandButton cmdDTBillItem 
         Caption         =   "DT Bill Item"
         Height          =   315
         Left            =   20940
         TabIndex        =   132
         Top             =   0
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CommandButton cmdUpdate 
         Caption         =   "Update 'Stored On' && Location"
         Height          =   315
         Left            =   -60
         TabIndex        =   51
         Top             =   0
         Width           =   2355
      End
      Begin VB.CommandButton cmdUnbillAll 
         Caption         =   "UnBill All"
         Height          =   315
         Left            =   2400
         TabIndex        =   32
         Top             =   0
         Width           =   795
      End
      Begin VB.CommandButton cmdBillAll 
         Caption         =   "Bill All"
         Height          =   315
         Left            =   5700
         TabIndex        =   31
         Top             =   0
         Width           =   795
      End
      Begin VB.CommandButton cmdUnbill 
         Caption         =   "Unmark as Billed"
         Height          =   315
         Left            =   8760
         TabIndex        =   21
         Top             =   0
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.CommandButton cmdManualBillItem 
         Caption         =   "Manually Mark as Billed"
         Height          =   315
         Left            =   13560
         TabIndex        =   20
         Top             =   0
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.CommandButton cmdBillItem 
         Caption         =   "Bill Item"
         Height          =   315
         Left            =   20040
         TabIndex        =   19
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   25920
         TabIndex        =   5
         Top             =   0
         Width           =   735
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   24780
         TabIndex        =   4
         Top             =   0
         Width           =   1095
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   24000
         TabIndex        =   3
         Top             =   0
         Width           =   735
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   23340
         TabIndex        =   2
         Top             =   0
         Width           =   615
      End
   End
   Begin MSAdodcLib.Adodc adoItems 
      Height          =   330
      Left            =   60
      Top             =   5100
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdItems 
      Bindings        =   "frmTrackerDADC.frx":0000
      Height          =   6915
      Left            =   60
      TabIndex        =   0
      Top             =   5100
      Width           =   26640
      ScrollBars      =   3
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   5
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTrackerDADC.frx":0017
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmTrackerDADC.frx":0033
      stylesets(2).Name=   "reference"
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmTrackerDADC.frx":004F
      stylesets(3).Name=   "headerfield"
      stylesets(3).ForeColor=   0
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmTrackerDADC.frx":006B
      stylesets(4).Name=   "dbb"
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmTrackerDADC.frx":0087
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      StyleSet        =   "headerfield"
      RowHeight       =   476
      Columns.Count   =   110
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "tracker_itemID"
      Columns(0).Name =   "tracker_dadc_itemID"
      Columns(0).DataField=   "tracker_dadc_itemID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   5106
      Columns(2).Caption=   "Ref"
      Columns(2).Name =   "itemreference"
      Columns(2).DataField=   "itemreference"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).HeadStyleSet=   "reference"
      Columns(2).StyleSet=   "reference"
      Columns(3).Width=   1773
      Columns(3).Caption=   "Filename"
      Columns(3).Name =   "itemfilename"
      Columns(3).DataField=   "itemfilename"
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "headerfield"
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "E.Dur"
      Columns(4).Name =   "durationestimate"
      Columns(4).DataField=   "durationestimate"
      Columns(4).FieldLen=   256
      Columns(5).Width=   714
      Columns(5).Caption=   "Dur"
      Columns(5).Name =   "duration"
      Columns(5).DataField=   "duration"
      Columns(5).FieldLen=   256
      Columns(5).StyleSet=   "headerfield"
      Columns(6).Width=   1508
      Columns(6).Caption=   "Length"
      Columns(6).Name =   "timecodeduration"
      Columns(6).DataField=   "timecodeduration"
      Columns(6).FieldLen=   256
      Columns(6).StyleSet=   "headerfield"
      Columns(7).Width=   847
      Columns(7).Caption=   "Pend"
      Columns(7).Name =   "rejected"
      Columns(7).DataField=   "rejected"
      Columns(7).DataType=   3
      Columns(7).FieldLen=   256
      Columns(7).Style=   2
      Columns(7).StyleSet=   "headerfield"
      Columns(8).Width=   2117
      Columns(8).Caption=   "Pend Date"
      Columns(8).Name =   "pendingdate"
      Columns(8).DataField=   "pendingdate"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).StyleSet=   "headerfield"
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "Recent Pend Date"
      Columns(9).Name =   "MostRecentPendingDate"
      Columns(9).DataField=   "MostRecentPendingDate"
      Columns(9).FieldLen=   256
      Columns(9).Locked=   -1  'True
      Columns(10).Width=   2117
      Columns(10).Caption=   "End Pend"
      Columns(10).Name=   "offpendingdate"
      Columns(10).DataField=   "offpendingdate"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(10).StyleSet=   "headerfield"
      Columns(11).Width=   1535
      Columns(11).Caption=   "Days Pend"
      Columns(11).Name=   "daysonpending"
      Columns(11).DataField=   "daysonpending"
      Columns(11).FieldLen=   256
      Columns(12).Width=   1005
      Columns(12).Caption=   "D-Tree"
      Columns(12).Name=   "decisiontree"
      Columns(12).DataField=   "decisiontree"
      Columns(12).FieldLen=   256
      Columns(12).Style=   2
      Columns(12).StyleSet=   "headerfield"
      Columns(13).Width=   1588
      Columns(13).Caption=   "D-Tree Date"
      Columns(13).Name=   "decisiontreedate"
      Columns(13).DataField=   "decisiontreedate"
      Columns(13).FieldLen=   256
      Columns(13).StyleSet=   "headerfield"
      Columns(14).Width=   1588
      Columns(14).Caption=   "End D-Tree"
      Columns(14).Name=   "OffDecisionTreeDate"
      Columns(14).DataField=   "OffDecisionTreeDate"
      Columns(14).FieldLen=   256
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "MostRecentDecisionTreeDate"
      Columns(15).Name=   "MostRecentDecisionTreeDate"
      Columns(15).DataField=   "MostRecentDecisionTreeDate"
      Columns(15).FieldLen=   256
      Columns(16).Width=   1535
      Columns(16).Caption=   "Day on DT"
      Columns(16).Name=   "DaysOnDecisionTree"
      Columns(16).DataField=   "DaysOnDecisionTree"
      Columns(16).FieldLen=   256
      Columns(17).Width=   3200
      Columns(17).Visible=   0   'False
      Columns(17).Caption=   "Fixing"
      Columns(17).Name=   "fixed"
      Columns(17).DataField=   "fixed"
      Columns(17).FieldLen=   256
      Columns(17).Style=   2
      Columns(17).StyleSet=   "headerfield"
      Columns(18).Width=   3200
      Columns(18).Visible=   0   'False
      Columns(18).Caption=   "Fix JobID"
      Columns(18).Name=   "fixedjobID"
      Columns(18).DataField=   "fixedjobID"
      Columns(18).FieldLen=   256
      Columns(18).StyleSet=   "headerfield"
      Columns(19).Width=   635
      Columns(19).Caption=   "GB"
      Columns(19).Name=   "gbsent"
      Columns(19).DataField=   "gbsent"
      Columns(19).FieldLen=   256
      Columns(19).StyleSet=   "headerfield"
      Columns(20).Width=   2037
      Columns(20).Caption=   "Stored On"
      Columns(20).Name=   "Stored On"
      Columns(20).FieldLen=   256
      Columns(20).StyleSet=   "headerfield"
      Columns(21).Width=   2646
      Columns(21).Caption=   "Stored on Folder"
      Columns(21).Name=   "storagefolder"
      Columns(21).DataField=   "storagefolder"
      Columns(21).FieldLen=   256
      Columns(21).StyleSet=   "headerfield"
      Columns(22).Width=   1138
      Columns(22).Caption=   "Req ID"
      Columns(22).Name=   "dbbcrid"
      Columns(22).DataField=   "dbbcrid"
      Columns(22).FieldLen=   256
      Columns(22).StyleSet=   "headerfield"
      Columns(23).Width=   2566
      Columns(23).Caption=   "Request By"
      Columns(23).Name=   "IngestRequestor"
      Columns(23).DataField=   "IngestRequestor"
      Columns(23).FieldLen=   256
      Columns(23).StyleSet=   "headerfield"
      Columns(24).Width=   2831
      Columns(24).Caption=   "Title"
      Columns(24).Name=   "title"
      Columns(24).DataField=   "title"
      Columns(24).FieldLen=   256
      Columns(24).StyleSet=   "headerfield"
      Columns(25).Width=   1508
      Columns(25).Caption=   "Series ID"
      Columns(25).Name=   "series"
      Columns(25).DataField=   "series"
      Columns(25).FieldLen=   256
      Columns(25).StyleSet=   "headerfield"
      Columns(26).Width=   1773
      Columns(26).Caption=   "Episode Title"
      Columns(26).Name=   "subtitle"
      Columns(26).DataField=   "subtitle"
      Columns(26).FieldLen=   256
      Columns(26).StyleSet=   "headerfield"
      Columns(27).Width=   741
      Columns(27).Caption=   "Eps"
      Columns(27).Name=   "episode"
      Columns(27).DataField=   "episode"
      Columns(27).FieldLen=   256
      Columns(27).StyleSet=   "headerfield"
      Columns(28).Width=   688
      Columns(28).Caption=   "Dupl"
      Columns(28).Name=   "duplicateitem"
      Columns(28).DataField=   "duplicateitem"
      Columns(28).FieldLen=   256
      Columns(28).Style=   2
      Columns(28).StyleSet=   "headerfield"
      Columns(29).Width=   1720
      Columns(29).Caption=   "Orig Barcode"
      Columns(29).Name=   "barcode"
      Columns(29).DataField=   "barcode"
      Columns(29).FieldLen=   256
      Columns(29).Style=   1
      Columns(29).StyleSet=   "headerfield"
      Columns(30).Width=   1720
      Columns(30).Caption=   "Barcode"
      Columns(30).Name=   "newbarcode"
      Columns(30).DataField=   "newbarcode"
      Columns(30).FieldLen=   256
      Columns(30).Style=   1
      Columns(30).StyleSet=   "headerfield"
      Columns(31).Width=   3200
      Columns(31).Visible=   0   'False
      Columns(31).Caption=   "LegacyOracTVAENumber"
      Columns(31).Name=   "LegacyOracTVAENumber"
      Columns(31).DataField=   "LegacyOracTVAENumber"
      Columns(31).DataType=   17
      Columns(31).FieldLen=   256
      Columns(32).Width=   2117
      Columns(32).Caption=   "Clock UID"
      Columns(32).Name=   "UID"
      Columns(32).DataField=   "UID"
      Columns(32).FieldLen=   256
      Columns(32).StyleSet=   "headerfield"
      Columns(33).Width=   1349
      Columns(33).Caption=   "Format"
      Columns(33).Name=   "format"
      Columns(33).DataField=   "format"
      Columns(33).FieldLen=   256
      Columns(33).StyleSet=   "headerfield"
      Columns(34).Width=   1905
      Columns(34).Caption=   "Location"
      Columns(34).Name=   "tapelocation"
      Columns(34).DataField=   "tapelocation"
      Columns(34).FieldLen=   256
      Columns(34).StyleSet=   "headerfield"
      Columns(35).Width=   1614
      Columns(35).Caption=   "Sub Location"
      Columns(35).Name=   "tapeshelf"
      Columns(35).DataField=   "tapeshelf"
      Columns(35).FieldLen=   256
      Columns(35).StyleSet=   "headerfield"
      Columns(36).Width=   1588
      Columns(36).Caption=   "Due Date"
      Columns(36).Name=   "duedateupload"
      Columns(36).DataField=   "duedateupload"
      Columns(36).DataType=   8
      Columns(36).FieldLen=   256
      Columns(36).StyleSet=   "headerfield"
      Columns(37).Width=   3200
      Columns(37).Visible=   0   'False
      Columns(37).Caption=   "Cont Vers Code"
      Columns(37).Name=   "contentversioncode"
      Columns(37).DataField=   "contentversioncode"
      Columns(37).FieldLen=   256
      Columns(37).StyleSet=   "headerfield"
      Columns(38).Width=   3200
      Columns(38).Visible=   0   'False
      Columns(38).Caption=   "Cont Vers ID"
      Columns(38).Name=   "contentversionID"
      Columns(38).DataField=   "contentversionID"
      Columns(38).FieldLen=   256
      Columns(38).StyleSet=   "headerfield"
      Columns(39).Width=   3200
      Columns(39).Visible=   0   'False
      Columns(39).Caption=   "BBC Core ID"
      Columns(39).Name=   "BBCCoreID"
      Columns(39).DataField=   "BBCCoreID"
      Columns(39).FieldLen=   256
      Columns(39).StyleSet=   "headerfield"
      Columns(40).Width=   3200
      Columns(40).Visible=   0   'False
      Columns(40).Caption=   "kitID"
      Columns(40).Name=   "kitID"
      Columns(40).DataField=   "kitID"
      Columns(40).FieldLen=   256
      Columns(40).StyleSet=   "headerfield"
      Columns(41).Width=   3200
      Columns(41).Visible=   0   'False
      Columns(41).Caption=   "Alpha ID"
      Columns(41).Name=   "externalalphaID"
      Columns(41).DataField=   "externalalphaID"
      Columns(41).FieldLen=   256
      Columns(41).StyleSet=   "headerfield"
      Columns(42).Width=   3200
      Columns(42).Visible=   0   'False
      Columns(42).Caption=   "Conform Request?"
      Columns(42).Name=   "needsconform"
      Columns(42).DataField=   "needsconform"
      Columns(42).FieldLen=   256
      Columns(42).StyleSet=   "headerfield"
      Columns(43).Width=   3200
      Columns(43).Visible=   0   'False
      Columns(43).Caption=   "Orig. Alpha ID"
      Columns(43).Name=   "originalexternalalphaID"
      Columns(43).DataField=   "originalexternalalphaID"
      Columns(43).FieldLen=   256
      Columns(43).StyleSet=   "headerfield"
      Columns(44).Width=   3200
      Columns(44).Visible=   0   'False
      Columns(44).Caption=   "Workflow ID"
      Columns(44).Name=   "workflowID"
      Columns(44).DataField=   "workflowID"
      Columns(44).FieldLen=   256
      Columns(44).StyleSet=   "headerfield"
      Columns(45).Width=   3200
      Columns(45).Visible=   0   'False
      Columns(45).Caption=   "External Alpha Key"
      Columns(45).Name=   "externalalphakey"
      Columns(45).DataField=   "externalalphakey"
      Columns(45).FieldLen=   256
      Columns(45).StyleSet=   "headerfield"
      Columns(46).Width=   820
      Columns(46).Caption=   "Rush"
      Columns(46).Name=   "priority"
      Columns(46).DataField=   "priority"
      Columns(46).FieldLen=   256
      Columns(46).Style=   2
      Columns(46).StyleSet=   "headerfield"
      Columns(47).Width=   1588
      Columns(47).Caption=   "1st Work'le"
      Columns(47).Name=   "firstcompleteable"
      Columns(47).DataField=   "firstcompleteable"
      Columns(47).FieldLen=   256
      Columns(48).Width=   1588
      Columns(48).Caption=   "Workable"
      Columns(48).Name=   "completeable"
      Columns(48).DataField=   "completeable"
      Columns(48).FieldLen=   256
      Columns(49).Width=   1588
      Columns(49).Caption=   "Target Date"
      Columns(49).Name=   "targetdate"
      Columns(49).DataField=   "targetdate"
      Columns(49).FieldLen=   256
      Columns(50).Width=   3200
      Columns(50).Visible=   0   'False
      Columns(50).Caption=   "Days Left"
      Columns(50).Name=   "targetdatecountdown"
      Columns(50).DataField=   "targetdatecountdown"
      Columns(50).FieldLen=   256
      Columns(51).Width=   1244
      Columns(51).Caption=   "Video"
      Columns(51).Name=   "legacylinestandard"
      Columns(51).DataField=   "legacylinestandard"
      Columns(51).FieldLen=   256
      Columns(51).StyleSet=   "headerfield"
      Columns(52).Width=   979
      Columns(52).Caption=   "F Rate"
      Columns(52).Name=   "legacyframerate"
      Columns(52).DataField=   "legacyframerate"
      Columns(52).FieldLen=   256
      Columns(52).StyleSet=   "headerfield"
      Columns(53).Width=   1455
      Columns(53).Caption=   "Language"
      Columns(53).Name=   "language"
      Columns(53).DataField=   "language"
      Columns(53).FieldLen=   256
      Columns(53).StyleSet=   "headerfield"
      Columns(54).Width=   1773
      Columns(54).Caption=   "Aspect Ratio"
      Columns(54).Name=   "imageaspectratio"
      Columns(54).DataField=   "imageaspectratio"
      Columns(54).FieldLen=   256
      Columns(54).StyleSet=   "headerfield"
      Columns(55).Width=   1773
      Columns(55).Caption=   "EPV Duration"
      Columns(55).Name=   "EPVDuration"
      Columns(55).DataField=   "EPVDuration"
      Columns(55).FieldLen=   256
      Columns(55).StyleSet=   "headerfield"
      Columns(56).Width=   926
      Columns(56).Caption=   "El Rq"
      Columns(56).Name=   "additionalelementsrequested"
      Columns(56).DataField=   "additionalelementsrequested"
      Columns(56).FieldLen=   256
      Columns(56).Style=   2
      Columns(56).StyleSet=   "headerfield"
      Columns(57).Width=   661
      Columns(57).Caption=   "5.1"
      Columns(57).Name=   "fivepointonerequested"
      Columns(57).DataField=   "fivepointonerequested"
      Columns(57).FieldLen=   256
      Columns(57).Style=   2
      Columns(57).StyleSet=   "headerfield"
      Columns(58).Width=   661
      Columns(58).Caption=   "Ste"
      Columns(58).Name=   "stereorequested"
      Columns(58).DataField=   "stereorequested"
      Columns(58).FieldLen=   256
      Columns(58).Style=   2
      Columns(58).StyleSet=   "headerfield"
      Columns(59).Width=   661
      Columns(59).Caption=   "Mo"
      Columns(59).Name=   "monorequested"
      Columns(59).DataField=   "monorequested"
      Columns(59).FieldLen=   256
      Columns(59).Style=   2
      Columns(59).StyleSet=   "headerfield"
      Columns(60).Width=   767
      Columns(60).Caption=   "M&E"
      Columns(60).Name=   "musicandeffectsrequested"
      Columns(60).DataField=   "musicandeffectsrequested"
      Columns(60).FieldLen=   256
      Columns(60).Style=   2
      Columns(60).StyleSet=   "headerfield"
      Columns(61).Width=   847
      Columns(61).Caption=   "Dol E"
      Columns(61).Name=   "dolbyErequested"
      Columns(61).DataField=   "dolbyErequested"
      Columns(61).FieldLen=   256
      Columns(61).Style=   2
      Columns(61).StyleSet=   "headerfield"
      Columns(62).Width=   1508
      Columns(62).Caption=   "Text in Pict"
      Columns(62).Name=   "textinpicture"
      Columns(62).DataField=   "textinpicture"
      Columns(62).FieldLen=   256
      Columns(62).StyleSet=   "headerfield"
      Columns(63).Width=   1402
      Columns(63).Caption=   "Subtitles"
      Columns(63).Name=   "subtitleslanguage"
      Columns(63).DataField=   "subtitleslanguage"
      Columns(63).FieldLen=   256
      Columns(63).StyleSet=   "headerfield"
      Columns(64).Width=   873
      Columns(64).Caption=   "Batch"
      Columns(64).Name=   "batch"
      Columns(64).DataField=   "batch"
      Columns(64).FieldLen=   256
      Columns(64).StyleSet=   "headerfield"
      Columns(65).Width=   1773
      Columns(65).Caption=   "Sp. Proj."
      Columns(65).Name=   "specialproject"
      Columns(65).DataField=   "specialproject"
      Columns(65).FieldLen=   256
      Columns(65).StyleSet=   "headerfield"
      Columns(66).Width=   1773
      Columns(66).Caption=   "DTTargetDate"
      Columns(66).Name=   "DTTargetDate"
      Columns(66).DataField=   "DTTargetDate"
      Columns(66).FieldLen=   256
      Columns(66).StyleSet=   "headerfield"
      Columns(67).Width=   1270
      Columns(67).Caption=   "DT Clock"
      Columns(67).Name=   "DTClock"
      Columns(67).DataField=   "DTClock"
      Columns(67).FieldLen=   256
      Columns(67).Style=   2
      Columns(67).StyleSet=   "stagefield"
      Columns(68).Width=   1270
      Columns(68).Caption=   "BT B&T"
      Columns(68).Name=   "DTBarsAndTone"
      Columns(68).DataField=   "DTBarsAndTone"
      Columns(68).FieldLen=   256
      Columns(68).Style=   2
      Columns(68).StyleSet=   "stagefield"
      Columns(69).Width=   1270
      Columns(69).Caption=   "DT T/C"
      Columns(69).Name=   "DTTimeCode"
      Columns(69).DataField=   "DTTimeCode"
      Columns(69).FieldLen=   256
      Columns(69).Style=   2
      Columns(69).StyleSet=   "stagefield"
      Columns(70).Width=   2514
      Columns(70).Caption=   "DT Src Not To Spec"
      Columns(70).Name=   "DTSourceNotToSpec"
      Columns(70).DataField=   "DTSourceNotToSpec"
      Columns(70).FieldLen=   256
      Columns(70).Style=   2
      Columns(70).StyleSet=   "stagefield"
      Columns(71).Width=   2593
      Columns(71).Caption=   "DT Trk Layout Wrong"
      Columns(71).Name=   "DTTrackLayoutWrong"
      Columns(71).DataField=   "DTTrackLayoutWrong"
      Columns(71).FieldLen=   256
      Columns(71).Style=   2
      Columns(71).StyleSet=   "stagefield"
      Columns(72).Width=   2752
      Columns(72).Caption=   "DT Dup Tks Removed"
      Columns(72).Name=   "DTDuplicatedTracksRemoved"
      Columns(72).DataField=   "DTDuplicatedTracksRemoved"
      Columns(72).FieldLen=   256
      Columns(72).Style=   2
      Columns(72).StyleSet=   "stagefield"
      Columns(73).Width=   1667
      Columns(73).Caption=   "Edit 15 mins"
      Columns(73).Name=   "DTEdit15mins"
      Columns(73).DataField=   "DTEdit15mins"
      Columns(73).FieldLen=   256
      Columns(73).Style=   2
      Columns(73).StyleSet=   "stagefield"
      Columns(74).Width=   1667
      Columns(74).Caption=   "Edit 30 mins"
      Columns(74).Name=   "DTEdit30mins"
      Columns(74).DataField=   "DTEdit30mins"
      Columns(74).FieldLen=   256
      Columns(74).Style=   2
      Columns(74).StyleSet=   "stagefield"
      Columns(75).Width=   1667
      Columns(75).Caption=   "Edit 45 mins"
      Columns(75).Name=   "DTEdit45mins"
      Columns(75).DataField=   "DTEdit45mins"
      Columns(75).FieldLen=   256
      Columns(75).Style=   2
      Columns(75).StyleSet=   "stagefield"
      Columns(76).Width=   1667
      Columns(76).Caption=   "Edit 60 mins"
      Columns(76).Name=   "DTEdit60mins"
      Columns(76).DataField=   "DTEdit60mins"
      Columns(76).FieldLen=   256
      Columns(76).Style=   2
      Columns(76).StyleSet=   "stagefield"
      Columns(77).Width=   1111
      Columns(77).Caption=   "DT Bill?"
      Columns(77).Name=   "DTBilled"
      Columns(77).DataField=   "DTBilled"
      Columns(77).FieldLen=   256
      Columns(77).Locked=   -1  'True
      Columns(77).Style=   2
      Columns(77).StyleSet=   "conclusionfield"
      Columns(78).Width=   1349
      Columns(78).Caption=   "DTJob #"
      Columns(78).Name=   "DTJobID"
      Columns(78).DataField=   "DTJobID"
      Columns(78).FieldLen=   256
      Columns(78).Locked=   -1  'True
      Columns(78).StyleSet=   "headerfield"
      Columns(79).Width=   820
      Columns(79).Caption=   "Done"
      Columns(79).Name=   "readytobill"
      Columns(79).DataField=   "readytobill"
      Columns(79).FieldLen=   256
      Columns(79).Locked=   -1  'True
      Columns(79).Style=   2
      Columns(79).StyleSet=   "conclusionfield"
      Columns(80).Width=   2117
      Columns(80).Caption=   "stagefield1"
      Columns(80).Name=   "stagefield1"
      Columns(80).DataField=   "stagefield1"
      Columns(80).FieldLen=   256
      Columns(80).Style=   1
      Columns(80).StyleSet=   "stagefield"
      Columns(81).Width=   2752
      Columns(81).Caption=   "stagefield5"
      Columns(81).Name=   "stagefield5"
      Columns(81).DataField=   "stagefield5"
      Columns(81).FieldLen=   256
      Columns(81).Style=   1
      Columns(81).StyleSet=   "stagefield"
      Columns(82).Width=   2752
      Columns(82).Caption=   "stagefield2"
      Columns(82).Name=   "stagefield2"
      Columns(82).DataField=   "stagefield2"
      Columns(82).FieldLen=   256
      Columns(82).Style=   1
      Columns(82).StyleSet=   "stagefield"
      Columns(83).Width=   2752
      Columns(83).Caption=   "stagefield3"
      Columns(83).Name=   "stagefield3"
      Columns(83).DataField=   "stagefield3"
      Columns(83).FieldLen=   256
      Columns(83).Style=   1
      Columns(83).StyleSet=   "stagefield"
      Columns(84).Width=   2752
      Columns(84).Caption=   "stagefield4"
      Columns(84).Name=   "stagefield4"
      Columns(84).DataField=   "stagefield4"
      Columns(84).FieldLen=   256
      Columns(84).Style=   1
      Columns(84).StyleSet=   "stagefield"
      Columns(85).Width=   1085
      Columns(85).Caption=   "xml by"
      Columns(85).Name=   "xmlmadeby"
      Columns(85).DataField=   "xmlmadeby"
      Columns(85).FieldLen=   256
      Columns(85).Locked=   -1  'True
      Columns(85).StyleSet=   "stagefield"
      Columns(86).Width=   2752
      Columns(86).Caption=   "stagefield6"
      Columns(86).Name=   "stagefield6"
      Columns(86).DataField=   "stagefield6"
      Columns(86).FieldLen=   256
      Columns(86).Style=   1
      Columns(86).StyleSet=   "stagefield"
      Columns(87).Width=   2752
      Columns(87).Caption=   "stagefield7"
      Columns(87).Name=   "stagefield7"
      Columns(87).DataField=   "stagefield7"
      Columns(87).FieldLen=   256
      Columns(87).Style=   1
      Columns(87).StyleSet=   "stagefield"
      Columns(88).Width=   2752
      Columns(88).Caption=   "stagefield8"
      Columns(88).Name=   "stagefield8"
      Columns(88).DataField=   "stagefield8"
      Columns(88).FieldLen=   256
      Columns(88).Style=   1
      Columns(88).StyleSet=   "stagefield"
      Columns(89).Width=   2752
      Columns(89).Caption=   "stagefield9"
      Columns(89).Name=   "stagefield9"
      Columns(89).DataField=   "stagefield9"
      Columns(89).FieldLen=   256
      Columns(89).Style=   1
      Columns(89).StyleSet=   "stagefield"
      Columns(90).Width=   2752
      Columns(90).Caption=   "stagefield10"
      Columns(90).Name=   "stagefield10"
      Columns(90).DataField=   "stagefield10"
      Columns(90).FieldLen=   256
      Columns(90).Style=   1
      Columns(90).StyleSet=   "stagefield"
      Columns(91).Width=   2752
      Columns(91).Caption=   "stagefield11"
      Columns(91).Name=   "stagefield11"
      Columns(91).DataField=   "stagefield11"
      Columns(91).FieldLen=   256
      Columns(91).Style=   1
      Columns(91).StyleSet=   "stagefield"
      Columns(92).Width=   2752
      Columns(92).Caption=   "stagefield12"
      Columns(92).Name=   "stagefield12"
      Columns(92).DataField=   "stagefield12"
      Columns(92).FieldLen=   256
      Columns(92).Style=   1
      Columns(92).StyleSet=   "stagefield"
      Columns(93).Width=   2752
      Columns(93).Caption=   "stagefield13"
      Columns(93).Name=   "stagefield13"
      Columns(93).DataField=   "stagefield13"
      Columns(93).FieldLen=   256
      Columns(93).Style=   1
      Columns(93).StyleSet=   "stagefield"
      Columns(94).Width=   2752
      Columns(94).Caption=   "stagefield14"
      Columns(94).Name=   "stagefield14"
      Columns(94).DataField=   "stagefield14"
      Columns(94).FieldLen=   256
      Columns(94).Style=   1
      Columns(94).StyleSet=   "stagefield"
      Columns(95).Width=   2752
      Columns(95).Caption=   "stagefield15"
      Columns(95).Name=   "stagefield15"
      Columns(95).DataField=   "stagefield15"
      Columns(95).FieldLen=   256
      Columns(95).Style=   1
      Columns(95).StyleSet=   "stagefield"
      Columns(96).Width=   3200
      Columns(96).Visible=   0   'False
      Columns(96).Caption=   "conclusionfield1"
      Columns(96).Name=   "conclusionfield1"
      Columns(96).DataField=   "conclusionfield1"
      Columns(96).FieldLen=   256
      Columns(96).Locked=   -1  'True
      Columns(96).Style=   2
      Columns(96).StyleSet=   "conclusionfield"
      Columns(97).Width=   3200
      Columns(97).Visible=   0   'False
      Columns(97).Caption=   "conclusionfield2"
      Columns(97).Name=   "conclusionfield2"
      Columns(97).DataField=   "conclusionfield2"
      Columns(97).FieldLen=   256
      Columns(97).Locked=   -1  'True
      Columns(97).Style=   2
      Columns(97).StyleSet=   "conclusionfield"
      Columns(98).Width=   3200
      Columns(98).Visible=   0   'False
      Columns(98).Caption=   "conclusionfield3"
      Columns(98).Name=   "conclusionfield3"
      Columns(98).DataField=   "conclusionfield3"
      Columns(98).FieldLen=   256
      Columns(98).Locked=   -1  'True
      Columns(98).Style=   2
      Columns(98).StyleSet=   "conclusionfield"
      Columns(99).Width=   767
      Columns(99).Caption=   "Bill?"
      Columns(99).Name=   "billed"
      Columns(99).DataField=   "billed"
      Columns(99).FieldLen=   256
      Columns(99).Locked=   -1  'True
      Columns(99).Style=   2
      Columns(99).StyleSet=   "conclusionfield"
      Columns(100).Width=   1349
      Columns(100).Caption=   "Inv Job #"
      Columns(100).Name=   "jobID"
      Columns(100).DataField=   "jobID"
      Columns(100).FieldLen=   256
      Columns(100).StyleSet=   "reference"
      Columns(101).Width=   3200
      Columns(101).Visible=   0   'False
      Columns(101).Caption=   "storagebarcode"
      Columns(101).Name=   "storagebarcode"
      Columns(101).DataField=   "storagebarcode"
      Columns(101).FieldLen=   256
      Columns(102).Width=   1482
      Columns(102).Caption=   "AS11 Billed"
      Columns(102).Name=   "AS11billed"
      Columns(102).DataField=   "AS11billed"
      Columns(102).FieldLen=   256
      Columns(102).Style=   2
      Columns(102).StyleSet=   "conclusionfield"
      Columns(103).Width=   1588
      Columns(103).Caption=   "AS11 Job #"
      Columns(103).Name=   "AS11JobID"
      Columns(103).DataField=   "AS11JobID"
      Columns(103).FieldLen=   256
      Columns(103).StyleSet=   "headerfield"
      Columns(104).Width=   926
      Columns(104).Caption=   "Faulty"
      Columns(104).Name=   "complainedabout"
      Columns(104).DataField=   "complainedabout"
      Columns(104).FieldLen=   256
      Columns(104).Style=   2
      Columns(105).Width=   820
      Columns(105).Caption=   "Redo"
      Columns(105).Name=   "complaintredoitem"
      Columns(105).DataField=   "complaintredoitem"
      Columns(105).FieldLen=   256
      Columns(105).Style=   2
      Columns(106).Width=   1588
      Columns(106).Caption=   "Entered"
      Columns(106).Name=   "cdate"
      Columns(106).DataField=   "cdate"
      Columns(106).FieldLen=   256
      Columns(107).Width=   1588
      Columns(107).Caption=   "Mod Date"
      Columns(107).Name=   "mdate"
      Columns(107).DataField=   "mdate"
      Columns(107).FieldLen=   256
      Columns(108).Width=   3200
      Columns(108).Visible=   0   'False
      Columns(108).Caption=   "audioconfiguration"
      Columns(108).Name=   "audioconfiguration"
      Columns(108).DataField=   "audioconfiguration"
      Columns(108).FieldLen=   256
      Columns(109).Width=   926
      Columns(109).Caption=   "Purged"
      Columns(109).Name=   "file_purged"
      Columns(109).DataField=   "file_purged"
      Columns(109).FieldLen=   256
      Columns(109).Style=   2
      Columns(109).StyleSet=   "conclusionfield"
      _ExtentX        =   46990
      _ExtentY        =   12197
      _StockProps     =   79
      Caption         =   "Tracker Items"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoComments 
      Height          =   330
      Left            =   60
      Top             =   12120
      Visible         =   0   'False
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdComments 
      Bindings        =   "frmTrackerDADC.frx":00A3
      Height          =   1875
      Left            =   60
      TabIndex        =   37
      Top             =   12120
      Width           =   18825
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   1
      stylesets(0).Name=   "Comment"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTrackerDADC.frx":00BD
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12582847
      RowHeight       =   476
      Columns.Count   =   14
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "trackerhistoryID"
      Columns(0).Name =   "tracker_commentID"
      Columns(0).DataField=   "tracker_commentID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "trackerprogramID"
      Columns(1).Name =   "tracker_dadc_itemID"
      Columns(1).DataField=   "tracker_dadc_itemID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   9446
      Columns(2).Caption=   "Comments - PLEASE FOLLOW THE NEW DECISION TREE INSTRUCTIONS"
      Columns(2).Name =   "comment"
      Columns(2).DataField=   "comment"
      Columns(2).FieldLen=   255
      Columns(2).HasForeColor=   -1  'True
      Columns(2).StyleSet=   "Comment"
      Columns(3).Width=   3387
      Columns(3).Caption=   "Date"
      Columns(3).Name =   "cdate"
      Columns(3).DataField=   "cdate"
      Columns(3).DataType=   7
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).Style=   1
      Columns(3).HasForeColor=   -1  'True
      Columns(3).ForeColor=   8388608
      Columns(4).Width=   2117
      Columns(4).Caption=   "Entered By"
      Columns(4).Name =   "cuser"
      Columns(4).DataField=   "cuser"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   1005
      Columns(5).Caption=   "Clock?"
      Columns(5).Name =   "Email - Clock?"
      Columns(5).DataField=   "emailclock"
      Columns(5).FieldLen=   256
      Columns(5).Style=   2
      Columns(6).Width=   900
      Columns(6).Caption=   "B&T?"
      Columns(6).Name =   "Email - B&T?"
      Columns(6).DataField=   "emailbarsandtone"
      Columns(6).FieldLen=   256
      Columns(6).Style=   2
      Columns(7).Width=   820
      Columns(7).Caption=   "T/C?"
      Columns(7).Name =   "Email - T/C?"
      Columns(7).DataField=   "emailtimecode"
      Columns(7).FieldLen=   256
      Columns(7).Style=   2
      Columns(8).Width=   2355
      Columns(8).Caption=   "Source Not to Spec"
      Columns(8).Name =   "Email - Black at End?"
      Columns(8).DataField=   "emailblackatend"
      Columns(8).FieldLen=   256
      Columns(8).Style=   2
      Columns(9).Width=   2778
      Columns(9).Caption=   "AS-11 Headbuild check"
      Columns(9).Name =   "Email - Textless Not Rq"
      Columns(9).DataField=   "emailtextlesspresent"
      Columns(9).FieldLen=   256
      Columns(9).Style=   2
      Columns(10).Width=   2619
      Columns(10).Caption=   "Track Layout Wrong?"
      Columns(10).Name=   "emailtracklayoutwrong"
      Columns(10).DataField=   "emailtracklayoutwrong"
      Columns(10).FieldLen=   256
      Columns(10).Style=   2
      Columns(11).Width=   3440
      Columns(11).Caption=   "Duplicated Tracks Removed"
      Columns(11).Name=   "emailblackonwhitetexstless"
      Columns(11).DataField=   "emailblackonwhitetexstless"
      Columns(11).FieldLen=   256
      Columns(11).Style=   2
      Columns(12).Width=   1376
      Columns(12).Caption=   "IR Conflict"
      Columns(12).Name=   "emailirconflict"
      Columns(12).DataField=   "emailirconflict"
      Columns(12).FieldLen=   256
      Columns(12).Style=   2
      Columns(13).Width=   1773
      Columns(13).Caption=   "Send Email?"
      Columns(13).Name=   "Email?"
      Columns(13).DataField=   "Column 5"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      Columns(13).Style=   4
      UseDefaults     =   0   'False
      _ExtentX        =   33205
      _ExtentY        =   3307
      _StockProps     =   79
      Caption         =   "Tracker Comments"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   60
      TabIndex        =   52
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The company this job is for"
      Top             =   3300
      Width           =   2895
      DataFieldList   =   "name"
      _Version        =   196617
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin MSComCtl2.DTPicker datDueDate 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   9900
      TabIndex        =   92
      Tag             =   "NOCHECK"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   4320
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   103546881
      CurrentDate     =   39580
   End
   Begin MSComCtl2.DTPicker datFrom 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   14460
      TabIndex        =   93
      Tag             =   "NOCHECK"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   1620
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   103546881
      CurrentDate     =   39580
   End
   Begin MSComCtl2.DTPicker datTo 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   14460
      TabIndex        =   94
      Tag             =   "NOCHECK"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   1980
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   103546881
      CurrentDate     =   39580
   End
   Begin MSAdodcLib.Adodc adoInternalComments 
      Height          =   330
      Left            =   18960
      Top             =   12120
      Visible         =   0   'False
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdInternalComments 
      Bindings        =   "frmTrackerDADC.frx":00D9
      Height          =   1875
      Left            =   18960
      TabIndex        =   120
      Top             =   12120
      Width           =   7740
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   1
      stylesets(0).Name=   "comment"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTrackerDADC.frx":00FB
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12582847
      RowHeight       =   476
      Columns.Count   =   12
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "trackerhistoryID"
      Columns(0).Name =   "tracker_commentID"
      Columns(0).DataField=   "tracker_commentID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "trackerprogramID"
      Columns(1).Name =   "tracker_DADC_itemID"
      Columns(1).DataField=   "tracker_DADC_itemID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   8361
      Columns(2).Caption=   "Comments "
      Columns(2).Name =   "comment"
      Columns(2).DataField=   "comment"
      Columns(2).FieldLen=   255
      Columns(2).HasForeColor=   -1  'True
      Columns(2).StyleSet=   "comment"
      Columns(3).Width=   2117
      Columns(3).Caption=   "Date"
      Columns(3).Name =   "cdate"
      Columns(3).DataField=   "cdate"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).Style=   1
      Columns(3).HasForeColor=   -1  'True
      Columns(3).ForeColor=   8388608
      Columns(3).StyleSet=   "comment"
      Columns(4).Width=   2117
      Columns(4).Caption=   "Entered By"
      Columns(4).Name =   "cuser"
      Columns(4).DataField=   "cuser"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(4).StyleSet=   "comment"
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "Clock?"
      Columns(5).Name =   "Email - Clock?"
      Columns(5).DataField=   "emailclock"
      Columns(5).FieldLen=   256
      Columns(5).Style=   2
      Columns(5).StyleSet=   "comment"
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "B&T?"
      Columns(6).Name =   "Email - B&T?"
      Columns(6).DataField=   "emailbarsandtone"
      Columns(6).FieldLen=   256
      Columns(6).Style=   2
      Columns(6).StyleSet=   "comment"
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "T/C?"
      Columns(7).Name =   "Email - T/C?"
      Columns(7).DataField=   "emailtimecode"
      Columns(7).FieldLen=   256
      Columns(7).Style=   2
      Columns(7).StyleSet=   "comment"
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "Black at End?"
      Columns(8).Name =   "Email - Black at End?"
      Columns(8).DataField=   "emailblackatend"
      Columns(8).FieldLen=   256
      Columns(8).Style=   2
      Columns(8).StyleSet=   "comment"
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "Black on White?"
      Columns(9).Name =   "emailblackonwhitetexstless"
      Columns(9).DataField=   "emailblackonwhitetexstless"
      Columns(9).FieldLen=   256
      Columns(9).Style=   2
      Columns(9).StyleSet=   "comment"
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "Track Layout Wrong?"
      Columns(10).Name=   "emailtracklayoutwrong"
      Columns(10).DataField=   "emailtracklayoutwrong"
      Columns(10).FieldLen=   256
      Columns(10).Style=   2
      Columns(10).StyleSet=   "comment"
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "internalcomment"
      Columns(11).Name=   "internalcomment"
      Columns(11).DataField=   "internalcomment"
      Columns(11).FieldLen=   256
      UseDefaults     =   0   'False
      _ExtentX        =   13652
      _ExtentY        =   3307
      _StockProps     =   79
      Caption         =   "Internal Comments"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblCaption 
      Caption         =   "AS11 Job #"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   34
      Left            =   8940
      TabIndex        =   146
      Top             =   1920
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "DT Job #"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   33
      Left            =   8940
      TabIndex        =   130
      Top             =   1500
      Width           =   2055
   End
   Begin VB.Label lblDADCItems 
      Caption         =   "DADC Items Requested"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   19320
      TabIndex        =   126
      Top             =   2220
      Width           =   6015
   End
   Begin VB.Label lblAudioConfiguration 
      Height          =   1575
      Left            =   19320
      TabIndex        =   125
      Top             =   2520
      Width           =   7395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Inv Job #"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   32
      Left            =   8940
      TabIndex        =   119
      Top             =   1140
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Special Project"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   31
      Left            =   3120
      TabIndex        =   117
      Top             =   3300
      Width           =   1815
   End
   Begin VB.Label lblCaption 
      Caption         =   "Content Version Code"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   29
      Left            =   3120
      TabIndex        =   111
      Top             =   1860
      Width           =   1875
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "File Made Dates"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   28
      Left            =   13860
      TabIndex        =   109
      Top             =   1320
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   22
      Left            =   13860
      TabIndex        =   96
      Top             =   1680
      Width           =   495
   End
   Begin VB.Label lblCaption 
      Caption         =   "End"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   21
      Left            =   13860
      TabIndex        =   95
      Top             =   2040
      Width           =   315
   End
   Begin VB.Label lblCaption 
      Caption         =   "Total Est. Duration"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   7
      Left            =   19260
      TabIndex        =   85
      Top             =   360
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label lblCaption 
      Caption         =   "Total Size (GB)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   12
      Left            =   19260
      TabIndex        =   84
      Top             =   1080
      Width           =   1575
   End
   Begin VB.Label lblCaption 
      Caption         =   "Total Duration"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   11
      Left            =   19260
      TabIndex        =   83
      Top             =   720
      Width           =   1635
   End
   Begin VB.Label lblCaption 
      Caption         =   "Stored On"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   20
      Left            =   3120
      TabIndex        =   69
      Top             =   2940
      Width           =   1635
   End
   Begin VB.Label lblCaption 
      Caption         =   "Barcode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   19
      Left            =   3120
      TabIndex        =   65
      Top             =   1140
      Width           =   1635
   End
   Begin VB.Label lblLastTrackerItemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   20700
      TabIndex        =   62
      Top             =   4200
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Workflow ID"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   14
      Left            =   8940
      TabIndex        =   60
      Top             =   780
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   10
      Left            =   120
      TabIndex        =   53
      Top             =   3000
      Width           =   825
   End
   Begin VB.Label lblCaption 
      Caption         =   "Actual SQL"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   8
      Left            =   6120
      TabIndex        =   50
      Top             =   4740
      Width           =   1635
   End
   Begin VB.Label lblCaption 
      Caption         =   "Reference"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   13
      Left            =   3120
      TabIndex        =   33
      Top             =   2220
      Width           =   1635
   End
   Begin VB.Label lblCaption 
      Caption         =   "Batch"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   8940
      TabIndex        =   27
      Top             =   420
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Episode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   8940
      TabIndex        =   26
      Top             =   60
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "BBC Core ID"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   3120
      TabIndex        =   25
      Top             =   2580
      Width           =   1575
   End
   Begin VB.Label lblCaption 
      Caption         =   "Tape Format"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   3120
      TabIndex        =   17
      Top             =   1500
      Width           =   1635
   End
   Begin VB.Label lblCaption 
      Caption         =   "Original Barcode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   3120
      TabIndex        =   16
      Top             =   780
      Width           =   1635
   End
   Begin VB.Label lblCaption 
      Caption         =   "Subtitle"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   3120
      TabIndex        =   13
      Top             =   420
      Width           =   1875
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   3120
      TabIndex        =   12
      Top             =   60
      Width           =   1695
   End
   Begin VB.Label lblTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   19500
      TabIndex        =   10
      Top             =   4200
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCompanyID 
      Height          =   255
      Left            =   1080
      TabIndex        =   9
      Top             =   2940
      Visible         =   0   'False
      Width           =   735
   End
End
Attribute VB_Name = "frmTrackerDADC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strSearch As String
Dim m_strOrderby As String
Dim m_blnDontVerifyXML As Boolean
Dim m_blnBilling As Boolean
Dim m_blnDelete As Boolean

Dim m_blnStageField1conc As Boolean, m_blnStageField2conc As Boolean, m_blnStageField3conc As Boolean, m_blnStageField4conc As Boolean, m_blnStageField5conc As Boolean
Dim m_blnStageField6conc As Boolean, m_blnStageField7conc As Boolean, m_blnStageField8conc As Boolean, m_blnStageField9conc As Boolean, m_blnStageField10conc As Boolean
Dim m_blnStageField11conc As Boolean, m_blnStageField12conc As Boolean, m_blnStageField13conc As Boolean, m_blnStageField14conc As Boolean, m_blnStageField15conc As Boolean

Private Sub HideAllColumns()

grdItems.Columns("IngestRequestor").Visible = False
grdItems.Columns("newbarcode").Visible = False
grdItems.Columns("format").Visible = False
grdItems.Columns("location").Visible = False
grdItems.Columns("tapeshelf").Visible = False
grdItems.Columns("duedateupload").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("targetdate").Visible = False
grdItems.Columns("legacylinestandard").Visible = False
grdItems.Columns("legacyframerate").Visible = False
grdItems.Columns("language").Visible = False
grdItems.Columns("pendingdate").Visible = False
grdItems.Columns("offpendingdate").Visible = False
grdItems.Columns("daysonpending").Visible = False
grdItems.Columns("fivepointonerequested").Visible = False
grdItems.Columns("stereorequested").Visible = False
grdItems.Columns("monorequested").Visible = False
grdItems.Columns("musicandeffectsrequested").Visible = False
grdItems.Columns("dolbyErequested").Visible = False
grdItems.Columns("decisiontreedate").Visible = False
grdItems.Columns("offdecisiontreedate").Visible = False
grdItems.Columns("daysondecisiontree").Visible = False
grdItems.Columns("DTTargetDate").Visible = False
grdItems.Columns("DTClock").Visible = False
grdItems.Columns("DTBarsAndTone").Visible = False
grdItems.Columns("DTTimeCode").Visible = False
grdItems.Columns("DTSourceNotToSpec").Visible = False
grdItems.Columns("DTTrackLayoutWrong").Visible = False
grdItems.Columns("DTDuplicatedTracksRemoved").Visible = False
grdItems.Columns("DTEdit15mins").Visible = False
grdItems.Columns("DTEdit30mins").Visible = False
grdItems.Columns("DTEdit45mins").Visible = False
grdItems.Columns("DTEdit60mins").Visible = False
grdItems.Columns("DTBilled").Visible = False
grdItems.Columns("DTJobID").Visible = False
grdItems.Columns("batch").Visible = False
grdItems.Columns("specialproject").Visible = False
grdItems.Columns("stagefield1").Visible = False
grdItems.Columns("stagefield2").Visible = False
grdItems.Columns("stagefield3").Visible = False
grdItems.Columns("stagefield4").Visible = False
grdItems.Columns("stagefield5").Visible = False
grdItems.Columns("stagefield6").Visible = False
grdItems.Columns("stagefield7").Visible = False
grdItems.Columns("stagefield8").Visible = False
grdItems.Columns("stagefield9").Visible = False
grdItems.Columns("stagefield10").Visible = False
grdItems.Columns("stagefield11").Visible = False
grdItems.Columns("stagefield12").Visible = False
grdItems.Columns("stagefield13").Visible = False
grdItems.Columns("stagefield14").Visible = False
grdItems.Columns("stagefield15").Visible = False
grdItems.Columns("AS11billed").Visible = False
grdItems.Columns("AS11JobID").Visible = False

End Sub

Private Sub chkHideDemo_Click()

Dim l_strSQL As String

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/dadctracker %' and companyID > 100 AND system_active = 1 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/dadctracker %' AND system_active = 1 ORDER BY name;"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

cmdSearch.Value = True

End Sub

Private Sub cmbCompany_Click()

Dim l_blnWide As Boolean, l_rstChoices As ADODB.Recordset

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text
m_strSearch = " WHERE companyID = " & lblCompanyID.Caption
'If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
'    l_blnWide = True
'Else
'    l_blnWide = False
'End If

Set l_rstChoices = ExecuteSQL("SELECT * FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & ";", g_strExecuteError)
CheckForSQLError

HideAllColumns

If l_rstChoices.RecordCount > 0 Then
    l_rstChoices.MoveFirst
    Do While Not l_rstChoices.EOF
        Select Case l_rstChoices("itemfield")
            Case "stagefield1"
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField1conc = True
                End If
                If l_blnWide = True Then
                    grdItems.Columns("stagefield1").Width = 1400
                Else
                    grdItems.Columns("stagefield1").Width = 1100
                End If
                grdItems.Columns("stagefield1").Caption = l_rstChoices("itemheading")
                
            Case "stagefield2"
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField2conc = True
                End If
                If l_blnWide = True Then
                    grdItems.Columns("stagefield2").Width = 1400
                Else
                    grdItems.Columns("stagefield2").Width = 1100
                End If
                grdItems.Columns("stagefield2").Caption = l_rstChoices("itemheading")
                
            Case "stagefield3"
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField3conc = True
                End If
                If l_blnWide = True Then
                    grdItems.Columns("stagefield3").Width = 1400
                Else
                    grdItems.Columns("stagefield3").Width = 1100
                End If
                grdItems.Columns("stagefield3").Caption = l_rstChoices("itemheading")
                
            Case "stagefield4"
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField4conc = True
                End If
                If l_blnWide = True Then
                    grdItems.Columns("stagefield4").Width = 1400
                Else
                    grdItems.Columns("stagefield4").Width = 1100
                End If
                grdItems.Columns("stagefield4").Caption = l_rstChoices("itemheading")
                
            Case "stagefield5"
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField5conc = True
                End If
                If l_blnWide = True Then
                    grdItems.Columns("stagefield5").Width = 1400
                Else
                    grdItems.Columns("stagefield5").Width = 1100
                End If
                grdItems.Columns("stagefield5").Caption = l_rstChoices("itemheading")
                
            Case "stagefield6"
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField6conc = True
                End If
                If l_blnWide = True Then
                    grdItems.Columns("stagefield6").Width = 1400
                Else
                    grdItems.Columns("stagefield6").Width = 1100
                End If
                grdItems.Columns("stagefield6").Caption = l_rstChoices("itemheading")
                
            Case "stagefield7"
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField7conc = True
                End If
                If l_blnWide = True Then
                    grdItems.Columns("stagefield7").Width = 1400
                Else
                    grdItems.Columns("stagefield7").Width = 1100
                End If
                grdItems.Columns("stagefield7").Caption = l_rstChoices("itemheading")
                
            Case "stagefield8"
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField8conc = True
                End If
                If l_blnWide = True Then
                    grdItems.Columns("stagefield8").Width = 1400
                Else
                    grdItems.Columns("stagefield8").Width = 1100
                End If
                grdItems.Columns("stagefield8").Caption = l_rstChoices("itemheading")
                
            Case "stagefield9"
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField9conc = True
                End If
                If l_blnWide = True Then
                    grdItems.Columns("stagefield9").Width = 1400
                Else
                    grdItems.Columns("stagefield9").Width = 1100
                End If
                grdItems.Columns("stagefield9").Caption = l_rstChoices("itemheading")
                
            Case "stagefield10"
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField10conc = True
                End If
                If l_blnWide = True Then
                    grdItems.Columns("stagefield10").Width = 1400
                Else
                    grdItems.Columns("stagefield10").Width = 1100
                End If
                grdItems.Columns("stagefield10").Caption = l_rstChoices("itemheading")
                
            Case "stagefield11"
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField11conc = True
                End If
                If l_blnWide = True Then
                    grdItems.Columns("stagefield11").Width = 1400
                Else
                    grdItems.Columns("stagefield11").Width = 1100
                End If
                grdItems.Columns("stagefield11").Caption = l_rstChoices("itemheading")
                
            Case "stagefield12"
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField12conc = True
                End If
                If l_blnWide = True Then
                    grdItems.Columns("stagefield12").Width = 1400
                Else
                    grdItems.Columns("stagefield12").Width = 1100
                End If
                grdItems.Columns("stagefield12").Caption = l_rstChoices("itemheading")
                
            Case "stagefield13"
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField13conc = True
                End If
                If l_blnWide = True Then
                    grdItems.Columns("stagefield13").Width = 1400
                Else
                    grdItems.Columns("stagefield13").Width = 1100
                End If
                grdItems.Columns("stagefield13").Caption = l_rstChoices("itemheading")
                
            Case "stagefield14"
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField14conc = True
                End If
                If l_blnWide = True Then
                    grdItems.Columns("stagefield14").Width = 1400
                Else
                    grdItems.Columns("stagefield14").Width = 1100
                End If
                grdItems.Columns("stagefield14").Caption = l_rstChoices("itemheading")
                
            Case "stagefield15"
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField15conc = True
                End If
                If l_blnWide = True Then
                    grdItems.Columns("stagefield15").Width = 1400
                Else
                    grdItems.Columns("stagefield15").Width = 1100
                End If
                grdItems.Columns("stagefield15").Caption = l_rstChoices("itemheading")
        
        End Select
        
        l_rstChoices.MoveNext
    Loop
End If

l_rstChoices.Close
Set l_rstChoices = Nothing

cmdSearch.Value = True
cmdSomeFields.Value = True

End Sub

Private Sub cmdAllFields_Click()

grdItems.Columns("IngestRequestor").Visible = True
grdItems.Columns("contentversioncode").Visible = True
grdItems.Columns("contentversionID").Visible = True
grdItems.Columns("BBCCoreID").Visible = True
grdItems.Columns("kitID").Visible = True
grdItems.Columns("needsconform").Visible = True
'grdItems.Columns("externalalphaID").Visible = True
'grdItems.Columns("originalexternalalphaID").Visible = True
grdItems.Columns("workflowID").Visible = True
'grdItems.Columns("externalalphakey").Visible = True
grdItems.Columns("dbbcrid").Visible = True
grdItems.Columns("LegacyOracTVAENumber").Visible = True
grdItems.Columns("imageaspectratio").Visible = True
'grdItems.Columns("language").Visible = True
grdItems.Columns("subtitleslanguage").Visible = True
grdItems.Columns("barcode").Visible = True
grdItems.Columns("newbarcode").Visible = True
grdItems.Columns("format").Visible = True
grdItems.Columns("location").Visible = True
grdItems.Columns("tapeshelf").Visible = True
grdItems.Columns("duedateupload").Visible = True
grdItems.Columns("firstcompleteable").Visible = True
grdItems.Columns("completeable").Visible = True
grdItems.Columns("targetdate").Visible = True
grdItems.Columns("legacylinestandard").Visible = True
grdItems.Columns("legacyframerate").Visible = True
grdItems.Columns("language").Visible = True
grdItems.Columns("pendingdate").Visible = True
grdItems.Columns("offpendingdate").Visible = True
grdItems.Columns("daysonpending").Visible = True
grdItems.Columns("decisiontreedate").Visible = True
grdItems.Columns("offdecisiontreedate").Visible = True
grdItems.Columns("daysondecisiontree").Visible = True
grdItems.Columns("DTTargetDate").Visible = True
grdItems.Columns("DTClock").Visible = True
grdItems.Columns("DTBarsAndTone").Visible = True
grdItems.Columns("DTTimeCode").Visible = True
grdItems.Columns("DTSourceNotToSpec").Visible = True
grdItems.Columns("DTTrackLayoutWrong").Visible = True
grdItems.Columns("DTDuplicatedTracksRemoved").Visible = True
grdItems.Columns("DTEdit15mins").Visible = True
grdItems.Columns("DTEdit30mins").Visible = True
grdItems.Columns("DTEdit45mins").Visible = True
grdItems.Columns("DTEdit60mins").Visible = True
grdItems.Columns("DTBilled").Visible = True
grdItems.Columns("DTJobID").Visible = True
grdItems.Columns("batch").Visible = True
grdItems.Columns("specialproject").Visible = True
grdItems.Columns("stagefield1").Visible = True
grdItems.Columns("stagefield2").Visible = True
grdItems.Columns("stagefield3").Visible = True
grdItems.Columns("stagefield4").Visible = True
grdItems.Columns("stagefield5").Visible = True
'grdItems.Columns("stagefield6").Visible = True
'grdItems.Columns("stagefield7").Visible = True
grdItems.Columns("stagefield8").Visible = True
'grdItems.Columns("stagefield9").Visible = True
grdItems.Columns("stagefield10").Visible = True
'grdItems.Columns("stagefield11").Visible = True
'grdItems.Columns("stagefield12").Visible = True
grdItems.Columns("stagefield13").Visible = True
grdItems.Columns("stagefield14").Visible = True
grdItems.Columns("stagefield15").Visible = True
grdItems.Columns("xmlmadeby").Visible = True
grdItems.Columns("AS11billed").Visible = True
grdItems.Columns("AS11JobID").Visible = True

End Sub

Private Sub cmdAS11BillAll_Click()

Dim l_lngTemp As Long
l_lngTemp = chkDontVerifyXML.Value
chkDontVerifyXML.Value = 1
adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then
    
    Do While Not adoItems.Recordset.EOF
        If cmdAS11BillItem.Visible = True Then
            cmdAS11BillItem.Value = True
        End If
        adoItems.Recordset.MoveNext
        DoEvents
    Loop
End If
chkDontVerifyXML.Value = l_lngTemp
MsgBox "Done"

End Sub

Private Sub cmdAS11BillItem_Click()

Dim l_strChargeCode As String, l_rstTrackerChargeCode As ADODB.Recordset, l_strJobLine As String, l_lngJobID As Long, l_lngDuration As Long, l_lngMinuteBilling As Long
Dim l_lngQuantity As Long, l_strCode As String, l_lngFactor As Long, l_blnFirstItem As Boolean, l_blnBulkBilling As Boolean, l_lngLoggingItemCount As Long
Dim l_lngEventID As Long, l_strLastComment As String, l_strLastCommentCuser As String, l_datLastCommentDate As Date, l_dat_AS11Date As Date
Dim l_lngAS11BillingCompanyID As Long

l_blnFirstItem = True
m_blnBilling = True
If frmJob.txtJobID.Text <> "" Then
    'A job is loaded - now check that it isn't locked.
    l_lngJobID = Val(frmJob.txtJobID.Text)
    l_lngAS11BillingCompanyID = GetData("company", "AS11BillingCompanyID", "companyID", Val(lblCompanyID.Caption))
    If frmJob.txtStatus.Text = "Confirmed" And Val(frmJob.lblCompanyID.Caption) = l_lngAS11BillingCompanyID Then
        If grdItems.Columns("duration").Text <> "" Then
            'The Job is valid - go ahead and create Job lines for this item.
            m_blnDontVerifyXML = True
            l_strJobLine = QuoteSanitise(grdItems.Columns("itemreference").Text)
            If grdItems.Columns("title").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("title").Text
            If grdItems.Columns("episode").Text <> "" Then l_strJobLine = l_strJobLine & " - Ep " & grdItems.Columns("episode").Text
            If grdItems.Columns("LegacyOracTVAENumber").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("LegacyOracTVAENumber").Text
            l_strJobLine = l_strJobLine & " - AS11 Transcode"
            l_lngDuration = Val(grdItems.Columns("duration").Text)
            l_lngQuantity = 1
            If grdItems.Columns("stagefield5").Text <> "" Then
                l_dat_AS11Date = grdItems.Columns("stagefield5").Text
            Else
                l_dat_AS11Date = 0
            End If
            l_lngEventID = Val(GetDataSQL("SELECT TOP 1 eventID FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Quicktime' AND clipcodec Like 'ProRes%' AND system_deleted = 0;"))
            If l_lngEventID = 0 Then
                l_lngEventID = Val(GetDataSQL("SELECT TOP 1 eventID FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Quicktime' AND clipcodec Like 'ProRes%' AND system_deleted <> 0;"))
            End If
            If l_lngEventID <> 0 Then
                If grdItems.Columns("stagefield5").Text <> "" Then
                    Select Case Left(grdItems.Columns("legacylinestandard").Text, 4)
                        Case "1080"
                            l_strChargeCode = "WWTRANHD"
                        Case "2160"
                            l_strChargeCode = "WWTRANUHD"
                        Case Else
                            l_strChargeCode = "WWTRANSD"
                    End Select
                    l_strCode = "I"
                    If l_strLastComment <> "" Then
                        MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, l_dat_AS11Date, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, Null, grdItems.Columns("tracker_dadc_itemID").Text
                    Else
                        MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, l_dat_AS11Date, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, Null, grdItems.Columns("tracker_dadc_itemID").Text
                    End If
                    grdItems.Columns("as11billed").Text = -1
                    grdItems.Columns("as11jobID").Text = l_lngJobID
                    grdItems.Update
                    cmdAS11BillItem.Visible = False
                    cmdManualBillAS11Item.Visible = False
                    cmdUnbillAS11.Visible = True
                    frmJob.adoJobDetail.Refresh
                End If
            Else
                MsgBox "Need to be able to find the File record in order to bill AS11 Items.", vbCritical, "Cannot AS11 Bill Item"
            End If
            m_blnDontVerifyXML = False
        Else
            MsgBox "The current Item must have a duration in order to bill it.", vbCritical, "Cannot DT Bill Item"
        End If
    Else
        MsgBox "The current Job must belong to the correct company and be confirmed in order to bill an item.", vbCritical, "Cannot Bill Item"
    End If
Else
    MsgBox "A Job must be created and loaded into the Jobs form in order to bill an item.", vbCritical, "Cannot Bill Item"
End If

m_blnBilling = False

End Sub

Private Sub cmdAS11UnbillAll_Click()

Dim l_lngTemp As Long
l_lngTemp = chkDontVerifyXML.Value
chkDontVerifyXML.Value = 1
adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then

    Do While Not adoItems.Recordset.EOF
        adoItems.Recordset("as11billed") = 0
        adoItems.Recordset("AS11JobID") = Null
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
    Loop
End If

grdItems.Refresh
chkDontVerifyXML.Value = l_lngTemp

End Sub

Private Sub cmdBillAll_Click()

Dim l_lngTemp As Long
l_lngTemp = chkDontVerifyXML.Value
chkDontVerifyXML.Value = 1
adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then
    
    Do While Not adoItems.Recordset.EOF
        If cmdBillItem.Visible = True Then
            cmdBillItem.Value = True
        End If
        adoItems.Recordset.MoveNext
        DoEvents
    Loop
End If
chkDontVerifyXML.Value = l_lngTemp
MsgBox "Done"

End Sub

Private Sub cmdBillItem_Click()

Dim l_strChargeCode As String, l_rstTrackerChargeCode As ADODB.Recordset, l_strJobLine As String, l_lngJobID As Long, l_lngDuration As Long, l_lngMinuteBilling As Long
Dim l_lngQuantity As Long, l_strCode As String, l_lngFactor As Long, l_blnFirstItem As Boolean, l_blnBulkBilling As Boolean, l_lngLoggingItemCount As Long
Dim l_lngEventID As Long, l_strLastComment As String, l_strLastCommentCuser As String, l_datLastCommentDate As Date, l_datCompletedDate As Date

l_blnFirstItem = True
m_blnBilling = True
If frmJob.txtJobID.Text <> "" Then
    'A job is loaded - now check that it isn't locked.
    l_lngJobID = Val(frmJob.txtJobID.Text)
    If frmJob.txtStatus.Text = "Confirmed" And frmJob.lblCompanyID.Caption = lblCompanyID.Caption Then
        If grdItems.Columns("duration").Text <> "" Then
            If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/bulkbilling") > 0 Then
                l_blnBulkBilling = True
            Else
                l_blnBulkBilling = False
            End If
            'The Job is valid - go ahead and create Job lines for this item.
            m_blnDontVerifyXML = True
            l_strJobLine = QuoteSanitise(grdItems.Columns("itemreference").Text)
            If grdItems.Columns("title").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("title").Text
            If grdItems.Columns("episode").Text <> "" Then l_strJobLine = l_strJobLine & " - Ep " & grdItems.Columns("episode").Text
            If grdItems.Columns("stagefield15").Text <> "" Or grdItems.Columns("stagefield13").Text <> "" Then
                If l_blnBulkBilling = True Then
                    'Bulk Audio Billing...
                    'Validation at �30, plus XML and Send as per Video Ingest
                    l_strChargeCode = "DADCAUDFILECHECK"
                    l_strCode = "O"
                    l_lngDuration = 0
                    l_lngQuantity = 1
                    If grdItems.Columns("stagefield15").Text <> "" Then
                        l_datCompletedDate = grdItems.Columns("stagefield15").Text
                    ElseIf grdItems.Columns("stagefield13").Text <> "" Then
                        l_datCompletedDate = grdItems.Columns("stagefield13").Text
                    Else
                        l_datCompletedDate = 0
                    End If
                    If adoComments.Recordset.RecordCount > 0 Then
                        adoComments.Recordset.MoveLast
                        l_strLastCommentCuser = adoComments.Recordset("cuser")
                        l_datLastCommentDate = adoComments.Recordset("cdate")
                        l_strLastComment = adoComments.Recordset("comment")
                        If adoComments.Recordset("emailclock") <> 0 Then l_strLastComment = l_strLastComment & ", MX1 to Replace Clock"
                        If adoComments.Recordset("emailbarsandtone") <> 0 Then l_strLastComment = l_strLastComment & ", MX1 to Replace Bars and Tone"
                        If adoComments.Recordset("emailtimecode") <> 0 Then l_strLastComment = l_strLastComment & ", MX1 to Correct Timecode"
                        If adoComments.Recordset("emailblackatend") <> 0 Then l_strLastComment = l_strLastComment & ", MX1 to correct Black at End"
                    Else
                        l_strLastCommentCuser = ""
                        l_datLastCommentDate = 0
                        l_strLastComment = ""
                    End If
                    If grdItems.Columns("stagefield2").Text <> "" Then
                        If l_strLastComment <> "" Then
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text
                        Else
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text
                        End If
                    ElseIf grdItems.Columns("stagefield2").Text = "" And grdItems.Columns("stagefield15").Text <> "" Then
                        MsgBox "Item marked as Complete but no file made. ", vbCritical, "Item not billed"
                        m_blnBilling = False
                        Exit Sub
                    End If
                    If l_strLastComment <> "" Then
                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING15", 0, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text
                    Else
                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING15", 0, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text
                    End If
                Else
                    'DADC Manual DBB Billing
                    l_strCode = "I"
                    l_lngLoggingItemCount = 0
                    l_lngDuration = Val(grdItems.Columns("duration").Text)
                    l_lngQuantity = 1
                    If grdItems.Columns("stagefield15").Text <> "" Then
                        l_datCompletedDate = grdItems.Columns("stagefield15").Text
                    ElseIf grdItems.Columns("stagefield13").Text <> "" Then
                        l_datCompletedDate = grdItems.Columns("stagefield13").Text
                    Else
                        l_datCompletedDate = 0
                    End If
                    If adoComments.Recordset.RecordCount > 0 Then
                        adoComments.Recordset.MoveLast
                        l_strLastCommentCuser = adoComments.Recordset("cuser")
                        l_datLastCommentDate = adoComments.Recordset("cdate")
                        l_strLastComment = adoComments.Recordset("comment")
                        If adoComments.Recordset("emailclock") <> 0 Then l_strLastComment = l_strLastComment & ", MX1 to Replace Clock"
                        If adoComments.Recordset("emailbarsandtone") <> 0 Then l_strLastComment = l_strLastComment & ", MX1 to Replace Bars and Tone"
                        If adoComments.Recordset("emailtimecode") <> 0 Then l_strLastComment = l_strLastComment & ", MX1 to Correct Timecode"
                        If adoComments.Recordset("emailblackatend") <> 0 Then l_strLastComment = l_strLastComment & ", MX1 to correct Black at End"
                    Else
                        l_strLastCommentCuser = ""
                        l_datLastCommentDate = 0
                        l_strLastComment = ""
                    End If
                    l_lngEventID = Val(GetDataSQL("SELECT TOP 1 eventID FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Quicktime' AND (clipcodec Like 'ProRes%' OR clipcodec IS NULL or clipcodec = '' or clipcodec = 'AUD only') AND system_deleted = 0;"))
                    If l_lngEventID = 0 Then
                        l_lngEventID = Val(GetDataSQL("SELECT TOP 1 eventID FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Quicktime' AND clipcodec Like 'ProRes%' AND system_deleted <> 0;"))
                    End If
                    If grdItems.Columns("format").Text = "HDCAM-SR" Then
                        l_strChargeCode = "DADCINGESTHDSR"
                    ElseIf grdItems.Columns("format").Text = "HDCAM" Then
                        l_strChargeCode = "DADCINGESTHD"
                    ElseIf Left(grdItems.Columns("newbarcode").Text, 3) = "EVR" Or UCase(Left(grdItems.Columns("format").Text, 4)) = "FILE" Or UCase(grdItems.Columns("format").Text) = "MOBILEDISC" Then
                        l_strCode = "O"
                        l_lngDuration = 0
                        If GetData("events", "cliphorizontalpixels", "eventID", l_lngEventID) <= 720 Then
                            l_strChargeCode = "DADCBBCFILECHECK"
                        ElseIf GetData("events", "cliphorizontalpixels", "eventID", l_lngEventID) <= 1920 Then
                            l_strChargeCode = "DADCBBCFILECHECK-HD"
                        Else
                            l_strChargeCode = "DADCBBCFILECHECK-UHD"
                        End If
                    ElseIf grdItems.Columns("format").Text = "AUDIO" Then
                        l_strChargeCode = "DADCAUCONF"
                        l_strCode = "O"
                        l_lngDuration = 0
                    ElseIf grdItems.Columns("format").Text = "C-FORM" Then
                        l_strChargeCode = "DADCINGESTCFORM"
                    ElseIf grdItems.Columns("Format").Text = "D3" Then
                        l_strChargeCode = "DADCINGESTD3"
                    Else
                        l_strChargeCode = "DADCINGESTSD"
                    End If
                    
                    If l_lngEventID <> 0 Then
                        Set l_rstTrackerChargeCode = ExecuteSQL("SELECT Count(eventlogging.eventloggingID) FROM eventlogging WHERE eventID = " & l_lngEventID & ";", g_strExecuteError)
                        CheckForSQLError
                        If l_rstTrackerChargeCode.RecordCount > 0 Then
                            l_lngLoggingItemCount = l_rstTrackerChargeCode(0)
                        End If
                        l_rstTrackerChargeCode.Close
                        Set l_rstTrackerChargeCode = Nothing
                    ElseIf grdItems.Columns("stagefield15").Text <> "" Then
                        MsgBox "Could not find the Clip Record associated with this tracker line, to count logging items", vbCritical, "Error Billing..."
                        m_blnBilling = False
                        Exit Sub
                    Else
                        l_lngLoggingItemCount = 0
                    End If
                    If grdItems.Columns("stagefield2").Text <> "" Then
                        If l_strLastComment <> "" Then
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, , grdItems.Columns("tracker_dadc_itemID").Text
                        Else
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, , grdItems.Columns("tracker_dadc_itemID").Text
                        End If
                    ElseIf grdItems.Columns("stagefield2").Text = "" And grdItems.Columns("stagefield15").Text <> "" Then
                        MsgBox "Item marked as Complete but no file made. ", vbCritical, "Item not billed"
                        m_blnBilling = False
                        Exit Sub
                    End If
                    
                    If grdItems.Columns("stagefield2").Text <> "" Then
                        If l_lngLoggingItemCount > 15 Then
                            If l_strLastComment <> "" Then
                                MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING25", 0, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, , grdItems.Columns("tracker_dadc_itemID").Text
                            Else
                                MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING25", 0, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, , grdItems.Columns("tracker_dadc_itemID").Text
                            End If
                        ElseIf l_lngLoggingItemCount > 4 Then
                            If l_strLastComment <> "" Then
                                MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING15", 0, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, , grdItems.Columns("tracker_dadc_itemID").Text
                            Else
                                MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING15", 0, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, , grdItems.Columns("tracker_dadc_itemID").Text
                            End If
                        ElseIf l_lngLoggingItemCount > 0 Then
                            If l_strLastComment <> "" Then
                                MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING15", 0, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, , grdItems.Columns("tracker_dadc_itemID").Text
                            Else
                                MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING15", 0, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, , grdItems.Columns("tracker_dadc_itemID").Text
                            End If
                        End If
                    End If
                End If
            End If
            frmJob.adoJobDetail.Refresh
            grdItems.Columns("billed").Text = -1
            grdItems.Columns("jobID").Text = l_lngJobID
            grdItems.Update
            cmdBillItem.Visible = False
            cmdManualBillItem.Visible = False
            m_blnDontVerifyXML = False
        ElseIf grdItems.Columns("stagefield15").Text <> "" Then
            MsgBox "The current Item must have a duration in order to bill it.", vbCritical, "Cannot Bill Item"
        Else
            grdItems.Columns("billed").Text = -1
            grdItems.Columns("jobID").Text = l_lngJobID
            grdItems.Update
        End If
    Else
        MsgBox "The current Job must belong to the correct company and be confirmed in order to bill an item.", vbCritical, "Cannot Bill Item"
    End If
Else
    MsgBox "A Job must be created and loaded into the Jobs form in order to bill an item.", vbCritical, "Cannot Bill Item"
End If

m_blnBilling = False

End Sub

Private Sub cmdBulkAudioFields_Click()

grdItems.Columns("IngestRequestor").Visible = False
grdItems.Columns("contentversioncode").Visible = True
grdItems.Columns("contentversionID").Visible = True
grdItems.Columns("BBCCoreID").Visible = False
grdItems.Columns("kitID").Visible = True
grdItems.Columns("needsconform").Visible = True
grdItems.Columns("externalalphaID").Visible = False
grdItems.Columns("originalexternalalphaID").Visible = False
grdItems.Columns("workflowID").Visible = False
grdItems.Columns("externalalphakey").Visible = False
grdItems.Columns("dbbcrid").Visible = False
grdItems.Columns("LegacyOracTVAENumber").Visible = False
grdItems.Columns("imageaspectratio").Visible = False
'grdItems.Columns("language").Visible = False
grdItems.Columns("subtitleslanguage").Visible = False
grdItems.Columns("barcode").Visible = False
grdItems.Columns("newbarcode").Visible = True
grdItems.Columns("format").Visible = False
grdItems.Columns("location").Visible = False
grdItems.Columns("tapeshelf").Visible = False
grdItems.Columns("duedateupload").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("completeable").Visible = True
grdItems.Columns("targetdate").Visible = True
grdItems.Columns("legacylinestandard").Visible = False
grdItems.Columns("legacyframerate").Visible = False
grdItems.Columns("language").Visible = False
grdItems.Columns("pendingdate").Visible = False
grdItems.Columns("offpendingdate").Visible = False
grdItems.Columns("daysonpending").Visible = False
grdItems.Columns("decisiontreedate").Visible = False
grdItems.Columns("offdecisiontreedate").Visible = False
grdItems.Columns("daysondecisiontree").Visible = False
grdItems.Columns("DTTargetDate").Visible = False
grdItems.Columns("DTClock").Visible = False
grdItems.Columns("DTBarsAndTone").Visible = False
grdItems.Columns("DTTimeCode").Visible = False
grdItems.Columns("DTSourceNotToSpec").Visible = False
grdItems.Columns("DTTrackLayoutWrong").Visible = False
grdItems.Columns("DTDuplicatedTracksRemoved").Visible = False
grdItems.Columns("DTEdit15mins").Visible = False
grdItems.Columns("DTEdit30mins").Visible = False
grdItems.Columns("DTEdit45mins").Visible = False
grdItems.Columns("DTEdit60mins").Visible = False
grdItems.Columns("DTBilled").Visible = False
grdItems.Columns("DTJobID").Visible = False
grdItems.Columns("batch").Visible = True
grdItems.Columns("specialproject").Visible = True
grdItems.Columns("stagefield1").Visible = True
grdItems.Columns("stagefield2").Visible = True
grdItems.Columns("stagefield3").Visible = True
grdItems.Columns("stagefield4").Visible = True
grdItems.Columns("stagefield5").Visible = False
'grdItems.Columns("stagefield6").Visible = True
'grdItems.Columns("stagefield7").Visible = True
grdItems.Columns("stagefield8").Visible = True
'grdItems.Columns("stagefield9").Visible = True
grdItems.Columns("stagefield10").Visible = True
'grdItems.Columns("stagefield11").Visible = True
'grdItems.Columns("stagefield12").Visible = True
grdItems.Columns("stagefield13").Visible = True
grdItems.Columns("stagefield14").Visible = True
grdItems.Columns("stagefield15").Visible = True
grdItems.Columns("xmlmadeby").Visible = True

End Sub

Private Sub cmdClear_Click()

ClearFields Me

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdDTBillAll_Click()

Dim l_lngTemp As Long
l_lngTemp = chkDontVerifyXML.Value
chkDontVerifyXML.Value = 1
adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then
    
    Do While Not adoItems.Recordset.EOF
        If cmdDTBillItem.Visible = True Then
            cmdDTBillItem.Value = True
        End If
        adoItems.Recordset.MoveNext
        DoEvents
    Loop
End If
chkDontVerifyXML.Value = l_lngTemp
MsgBox "Done"

End Sub

Private Sub cmdDTBillItem_Click()

Dim l_strChargeCode As String, l_rstTrackerChargeCode As ADODB.Recordset, l_strJobLine As String, l_lngJobID As Long, l_lngDuration As Long, l_lngMinuteBilling As Long
Dim l_lngQuantity As Long, l_strCode As String, l_lngFactor As Long, l_blnFirstItem As Boolean, l_blnBulkBilling As Boolean, l_lngLoggingItemCount As Long
Dim l_lngEventID As Long, l_strLastComment As String, l_strLastCommentCuser As String, l_datLastCommentDate As Date, l_datCompletedDate As Date
Dim l_lngDTBBillingCompanyID As Long

l_blnFirstItem = True
m_blnBilling = True

'Check item has valid BT boxes ticked.
If Val(grdItems.Columns("DTClock").Text) = 0 And Val(grdItems.Columns("DTBarsAndTone").Text) = 0 And Val(grdItems.Columns("DTTimecode").Text) = 0 _
And Val(grdItems.Columns("DTSourceNotToSpec").Text) = 0 And Val(grdItems.Columns("DTTrackLayoutWrong").Text) = 0 _
And Val(grdItems.Columns("DTDuplicatedTracksRemoved").Text) = 0 And Val(grdItems.Columns("DTEdit15mins").Text) = 0 And Val(grdItems.Columns("DTEdit30mins").Text) = 0 _
And Val(grdItems.Columns("DTEdit45mins").Text) = 0 And Val(grdItems.Columns("DTEdit60mins").Text) = 0 Then
    MsgBox "The current Item must have at least one DT checkbox ticked in order to DT Bill it.", vbCritical, "Cannot DT Bill Item"
    Exit Sub
End If


If frmJob.txtJobID.Text <> "" Then
    'A job is loaded - now check that it isn't locked.
    l_lngJobID = Val(frmJob.txtJobID.Text)
    l_lngDTBBillingCompanyID = GetData("company", "DTBillingCompanyID", "companyID", Val(lblCompanyID.Caption))
    If frmJob.txtStatus.Text = "Confirmed" And Val(frmJob.lblCompanyID.Caption) = l_lngDTBBillingCompanyID Then
        If grdItems.Columns("duration").Text <> "" Then
            'The Job is valid - go ahead and create Job lines for this item.
            m_blnDontVerifyXML = True
            l_strJobLine = QuoteSanitise(grdItems.Columns("itemreference").Text)
            If grdItems.Columns("title").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("title").Text
            If grdItems.Columns("episode").Text <> "" Then l_strJobLine = l_strJobLine & " - Ep " & grdItems.Columns("episode").Text
            If grdItems.Columns("LegacyOracTVAENumber").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("LegacyOracTVAENumber").Text
            l_strJobLine = l_strJobLine & " - fixes"
            l_lngDuration = Val(grdItems.Columns("duration").Text)
            l_lngQuantity = 1
            l_datCompletedDate = grdItems.Columns("OffDecisionTreeDAte").Text
            l_lngEventID = Val(GetDataSQL("SELECT TOP 1 eventID FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Quicktime' AND system_deleted = 0;"))
            'AND clipcodec Like 'ProRes%' 'was originally on the search
            If l_lngEventID = 0 Then
                l_lngEventID = Val(GetDataSQL("SELECT TOP 1 eventID FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Quicktime' AND system_deleted <> 0;"))
            End If
            If l_lngEventID <> 0 Then
                If Val(grdItems.Columns("DTClock").Text) <> 0 And Val(grdItems.Columns("DTBarsAndTone").Text) <> 0 And Val(grdItems.Columns("DTTimecode").Text) <> 0 Then
                    'Clock, Bars and Timecode Fix
                    l_strCode = "O"
                    l_strLastCommentCuser = ""
                    l_datLastCommentDate = 0
                    l_strLastComment = ""
                    If GetData("events", "cliphorizontalpixels", "eventID", l_lngEventID) > 720 Then
                        l_strChargeCode = "DADCTCBARS-HD"
                    Else
                        l_strChargeCode = "DADCTCBARS"
                    End If
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, 0, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "Clock, Bars and Timedcode Fix", Now, "", Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, Null, grdItems.Columns("tracker_dadc_itemID").Text
                End If
                If Val(grdItems.Columns("DTClock").Text) <> 0 And (Val(grdItems.Columns("DTBarsAndTone").Text) = 0 Or Val(grdItems.Columns("DTTimecode").Text) = 0) Then
                    'Clock Fix Item
                    l_strCode = "O"
                    l_strLastCommentCuser = ""
                    l_datLastCommentDate = 0
                    l_strLastComment = ""
                    If GetData("events", "cliphorizontalpixels", "eventID", l_lngEventID) > 720 Then
                        l_strChargeCode = "DADCCLOCKHD"
                    Else
                        l_strChargeCode = "DADCCLOCKSD"
                    End If
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, 0, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "Clock replaced", Now, "", Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, Null, grdItems.Columns("tracker_dadc_itemID").Text
                End If
                If Val(grdItems.Columns("DTBarsAndTone").Text) <> 0 And (Val(grdItems.Columns("DTClock").Text) = 0 Or Val(grdItems.Columns("DTTimecode").Text) = 0) Then
                    'Bars and Tone Fix
                    l_strCode = "O"
                    l_strLastCommentCuser = ""
                    l_datLastCommentDate = 0
                    l_strLastComment = ""
                    If GetData("events", "cliphorizontalpixels", "eventID", l_lngEventID) > 720 Then
                        l_strChargeCode = "DADCBARSHD"
                    Else
                        l_strChargeCode = "DADCBARSSD"
                    End If
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, 0, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "Bars and Tone Fixed", Now, "", Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, Null, grdItems.Columns("tracker_dadc_itemID").Text
                End If
                If Val(grdItems.Columns("DTTimecode").Text) <> 0 And (Val(grdItems.Columns("DTClock").Text) = 0 Or Val(grdItems.Columns("DTBarsAndTone").Text) = 0) Then
                    'New Timecode Fix
                    l_strCode = "O"
                    l_strLastCommentCuser = ""
                    l_datLastCommentDate = 0
                    l_strLastComment = ""
                    If GetData("events", "cliphorizontalpixels", "eventID", l_lngEventID) > 720 Then
                        l_strChargeCode = "DADCTC-HD"
                    Else
                        l_strChargeCode = "DADCTC"
                    End If
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, 0, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "Timecode replaced", Now, "", Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, Null, grdItems.Columns("tracker_dadc_itemID").Text
                End If
                If Val(grdItems.Columns("DTSourceNotToSpec").Text) <> 0 Then
                    'Source not to spec - a Transcode of duration length
                    l_strCode = "I"
                    l_strLastCommentCuser = ""
                    l_datLastCommentDate = 0
                    l_strLastComment = ""
                    If GetData("events", "cliphorizontalpixels", "eventID", l_lngEventID) > 720 Then
                        l_strChargeCode = "DADCTRANBHD"
                    Else
                        l_strChargeCode = "DADCTRANBSD"
                    End If
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "Source not to spec - Transcoded to fix", Now, "", Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, Null, grdItems.Columns("tracker_dadc_itemID").Text
                End If
                If Val(grdItems.Columns("DTTrackLayoutWrong").Text) <> 0 Then
                    'Track Layout wrong - a C-EDIT of 15 minutes duration
                    l_strJobLine = l_strJobLine & " Track Layout Corrected"
                    l_strCode = "A"
                    l_strLastCommentCuser = ""
                    l_datLastCommentDate = 0
                    l_strLastComment = ""
                    l_strChargeCode = "C-EDIT"
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, 15, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "Track Layout Wrong - Edited to fix", Now, "", Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, Null, grdItems.Columns("tracker_dadc_itemID").Text
                End If
                If Val(grdItems.Columns("DTDuplicatedTracksRemoved").Text) <> 0 Then
                    'Duplicate Tracks Removed - a C-EDIT of 15 minutes duration
                    l_strJobLine = l_strJobLine & " Duplicated Tracker Removed"
                    l_strCode = "A"
                    l_strLastCommentCuser = ""
                    l_datLastCommentDate = 0
                    l_strLastComment = ""
                    l_strChargeCode = "C-EDIT"
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, 15, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "Duplicate Tracks Removed", Now, "", Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, Null, grdItems.Columns("tracker_dadc_itemID").Text
                End If
                If Val(grdItems.Columns("DTEdit15mins").Text) <> 0 Then
                    'Unspeciifed editing taking 15 minutes - a C-EDIT of 15 minutes duration
                    l_strJobLine = l_strJobLine & " Editing 15 minutes"
                    l_strCode = "A"
                    l_strLastCommentCuser = ""
                    l_datLastCommentDate = 0
                    l_strLastComment = ""
                    l_strChargeCode = "C-EDIT"
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, 15, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "15 Mins Editing", Now, "", Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, Null, grdItems.Columns("tracker_dadc_itemID").Text
                End If
                If Val(grdItems.Columns("DTEdit30mins").Text) <> 0 Then
                    'Unspeciifed editing taking 30 minutes - a C-EDIT of 30 minutes duration
                    l_strJobLine = l_strJobLine & " Editing 30 minutes"
                    l_strCode = "A"
                    l_strLastCommentCuser = ""
                    l_datLastCommentDate = 0
                    l_strLastComment = ""
                    l_strChargeCode = "C-EDIT"
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, 30, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "30 Mins Editing", Now, "", Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, Null, grdItems.Columns("tracker_dadc_itemID").Text
                End If
                If Val(grdItems.Columns("DTEdit45mins").Text) <> 0 Then
                    'Unspeciifed editing taking 45 minutes - a C-EDIT of 45 minutes duration
                    l_strJobLine = l_strJobLine & " Editing 45 minutes"
                    l_strCode = "A"
                    l_strLastCommentCuser = ""
                    l_datLastCommentDate = 0
                    l_strLastComment = ""
                    l_strChargeCode = "C-EDIT"
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, 45, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "45 Mins Editing", Now, "", Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, Null, grdItems.Columns("tracker_dadc_itemID").Text
                End If
                If Val(grdItems.Columns("DTEdit60mins").Text) <> 0 Then
                    'Unspeciifed editing taking 60 minutes - a C-EDIT of 60 minutes duration
                    l_strJobLine = l_strJobLine & " Editing 60 minutes"
                    l_strCode = "A"
                    l_strLastCommentCuser = ""
                    l_datLastCommentDate = 0
                    l_strLastComment = ""
                    l_strChargeCode = "C-EDIT"
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, 60, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "60 Mins Editing", Now, "", Null, Null, Null, Null, Null, Null, grdItems.Columns("series").Text, Null, Null, Null, Null, grdItems.Columns("WorkflowID").Text, grdItems.Columns("contentversioncode").Text, grdItems.Columns("LegacyOracTVAENumber").Text, Null, grdItems.Columns("tracker_dadc_itemID").Text
                End If
                grdItems.Columns("dtbilled").Text = -1
                grdItems.Columns("dtjobID").Text = l_lngJobID
                grdItems.Update
                cmdBillItem.Visible = False
                cmdManualBillItem.Visible = False
                cmdUnbillDT.Visible = True
                frmJob.adoJobDetail.Refresh
            Else
                MsgBox "Need to be able to find the File record in order to DT Items.", vbCritical, "Cannot DT Bill Item"
            End If
            m_blnDontVerifyXML = False
        Else
            MsgBox "The current Item must have a duration in order to bill it.", vbCritical, "Cannot DT Bill Item"
        End If
    Else
        MsgBox "The current Job must belong to the correct company and be confirmed in order to bill an item.", vbCritical, "Cannot Bill Item"
    End If
Else
    MsgBox "A Job must be created and loaded into the Jobs form in order to bill an item.", vbCritical, "Cannot Bill Item"
End If

m_blnBilling = False

End Sub

Private Sub cmdDTFields_Click()

grdItems.Columns("IngestRequestor").Visible = False
grdItems.Columns("contentversioncode").Visible = False
grdItems.Columns("contentversionID").Visible = False
grdItems.Columns("BBCCoreID").Visible = False
grdItems.Columns("kitID").Visible = False
grdItems.Columns("needsconform").Visible = False
grdItems.Columns("externalalphaID").Visible = False
grdItems.Columns("originalexternalalphaID").Visible = False
grdItems.Columns("workflowID").Visible = False
grdItems.Columns("externalalphakey").Visible = False
grdItems.Columns("dbbcrid").Visible = False
grdItems.Columns("LegacyOracTVAENumber").Visible = True
grdItems.Columns("imageaspectratio").Visible = False
'grdItems.Columns("language").Visible = False
grdItems.Columns("subtitleslanguage").Visible = False
grdItems.Columns("barcode").Visible = False
grdItems.Columns("newbarcode").Visible = False
grdItems.Columns("format").Visible = False
grdItems.Columns("location").Visible = False
grdItems.Columns("tapeshelf").Visible = False
grdItems.Columns("duedateupload").Visible = True
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("targetdate").Visible = False
grdItems.Columns("legacylinestandard").Visible = False
grdItems.Columns("legacyframerate").Visible = False
grdItems.Columns("language").Visible = False
grdItems.Columns("pendingdate").Visible = False
grdItems.Columns("offpendingdate").Visible = False
grdItems.Columns("daysonpending").Visible = False
grdItems.Columns("decisiontreedate").Visible = False
grdItems.Columns("offdecisiontreedate").Visible = False
grdItems.Columns("daysondecisiontree").Visible = False
grdItems.Columns("DTTargetDate").Visible = True
grdItems.Columns("DTClock").Visible = True
grdItems.Columns("DTBarsAndTone").Visible = True
grdItems.Columns("DTTimeCode").Visible = True
grdItems.Columns("DTSourceNotToSpec").Visible = True
grdItems.Columns("DTTrackLayoutWrong").Visible = True
grdItems.Columns("DTDuplicatedTracksRemoved").Visible = True
grdItems.Columns("DTEdit15mins").Visible = True
grdItems.Columns("DTEdit30mins").Visible = True
grdItems.Columns("DTEdit45mins").Visible = True
grdItems.Columns("DTEdit60mins").Visible = True
grdItems.Columns("DTBilled").Visible = True
grdItems.Columns("DTJobID").Visible = True
grdItems.Columns("batch").Visible = False
grdItems.Columns("specialproject").Visible = False
grdItems.Columns("stagefield1").Visible = False
grdItems.Columns("stagefield2").Visible = False
grdItems.Columns("stagefield3").Visible = False
grdItems.Columns("stagefield4").Visible = False
grdItems.Columns("stagefield5").Visible = False
'grdItems.Columns("stagefield6").Visible = True
'grdItems.Columns("stagefield7").Visible = True
grdItems.Columns("stagefield8").Visible = False
'grdItems.Columns("stagefield9").Visible = True
grdItems.Columns("stagefield10").Visible = False
'grdItems.Columns("stagefield11").Visible = True
'grdItems.Columns("stagefield12").Visible = True
grdItems.Columns("stagefield13").Visible = False
grdItems.Columns("stagefield14").Visible = False
grdItems.Columns("stagefield15").Visible = False
grdItems.Columns("xmlmadeby").Visible = False

End Sub

Private Sub cmdDTUnbillAll_Click()

Dim l_lngTemp As Long
l_lngTemp = chkDontVerifyXML.Value
chkDontVerifyXML.Value = 1
adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then

    Do While Not adoItems.Recordset.EOF
        adoItems.Recordset("dtbilled") = 0
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
    Loop
End If

grdItems.Refresh
chkDontVerifyXML.Value = l_lngTemp

End Sub

Private Sub cmdDuplicateRow_Click()

Dim l_strSQL As String

l_strSQL = "INSERT INTO tracker_dadc_item (companyID, "

End Sub

Private Sub cmdExportGrid_Click()

If grdItems.Rows < 1 Then Exit Sub

Dim l_strFilePath As String
l_strFilePath = fGetSpecialFolder(CSIDL_DOCUMENTS)

On Error GoTo PROBLEM
grdItems.Export ssExportTypeText, ssExportColumnHeaders + ssExportAllRows, l_strFilePath & "DADCTrackerGrid.txt"
MsgBox "Exported to a text file in 'My Documents' called 'DADCTrackerGrid.txt'", vbInformation

ENDING:
Exit Sub

PROBLEM:
MsgBox "Could not export the grid - perhaps DADCTrackerGrid.txt is already open?"
GoTo ENDING

End Sub

Private Sub cmdMakeFixJobsheet_Click()

Dim l_strSQL As String, l_lngJobID As Long

If Val(lblTrackeritemID.Caption) <> 0 Then

    l_strSQL = "INSERT INTO job (createduserID, createduser, createddate, modifieddate, modifieduserID, modifieduser, fd_status, jobtype, joballocation, deadlinedate, despatchdate, projectID, productID, companyID, companyname) VALUES ("
    
    l_strSQL = l_strSQL & g_lngUserID & ", "
    l_strSQL = l_strSQL & "'" & g_strUserName & "', "
    l_strSQL = l_strSQL & "getdate(), "
    l_strSQL = l_strSQL & "getdate(), "
    l_strSQL = l_strSQL & g_lngUserID & ","
    l_strSQL = l_strSQL & "'" & g_strUserName & "', "
    l_strSQL = l_strSQL & "'Confirmed', "
    l_strSQL = l_strSQL & "'Dubbing', "
    l_strSQL = l_strSQL & "'Regular', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(DateAdd("d", 2, Now)) & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(DateAdd("d", 2, Now)) & "', "
    l_strSQL = l_strSQL & "0, "
    l_strSQL = l_strSQL & "0, "
    l_strSQL = l_strSQL & "1292, "
    l_strSQL = l_strSQL & "'Sony DADC Ltd (UK Fixes)'); "
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_lngJobID = g_lngLastID
    
    l_strSQL = grdItems.Columns("title").Text
    SetData "job", "title1", "jobID", l_lngJobID, l_strSQL
    l_strSQL = grdItems.Columns("contentversioncode").Text
    SetData "job", "title2", "jobID", l_lngJobID, l_strSQL
    SetData "job", "orderreference", "jobID", l_lngJobID, "Decision Tree"
    SetData "job", "contactID", "jobID", l_lngJobID, 3569
    SetData "job", "contactname", "jobID", l_lngJobID, "Nia Thomas"
    
    grdItems.Columns("fixedjobID").Text = l_lngJobID
    grdItems.Update
    
    ShowJob l_lngJobID, 1, True
'    ShowJob l_lngJobID, 1, True

End If

End Sub

Private Sub cmdManualBillAS11Item_Click()

grdItems.Columns("as11billed").Text = -1
grdItems.Columns("as11jobid").Text = 0
grdItems.Update
cmdAS11BillItem.Visible = False
cmdManualBillAS11Item.Visible = False
cmdUnbillAS11.Visible = True

End Sub

Private Sub cmdManualBillItem_Click()

grdItems.Columns("billed").Text = -1
grdItems.Columns("jobid").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False
cmdUnbill.Visible = True

End Sub

Private Sub cmdManualDTBillItem_Click()

grdItems.Columns("dtbilled").Text = -1
grdItems.Columns("dtjobid").Text = 0
grdItems.Update
cmdDTBillItem.Visible = False
cmdManualDTBillItem.Visible = False
cmdUnbillDT.Visible = True

End Sub

Private Sub cmdManualMarkAllBilled_Click()

Dim l_lngTemp As Long
l_lngTemp = chkDontVerifyXML.Value
chkDontVerifyXML.Value = 1
adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then
    
    Do While Not adoItems.Recordset.EOF
        If cmdManualBillItem.Visible = True Then
            cmdManualBillItem.Value = True
        End If
        adoItems.Recordset.MoveNext
        DoEvents
    Loop
End If
chkDontVerifyXML.Value = l_lngTemp
MsgBox "Done"

End Sub

Private Sub cmdPrint_Click()

Dim l_strSQL As String

If lblCompanyID.Caption <> "" Then
    l_strSQL = adoItems.RecordSource
    PrintCrystalReportUsingCleanSQL g_strLocationOfCrystalReportFiles & "GenericTrackerReport.rpt", l_strSQL, True
End If

End Sub

Private Sub cmdReport_Click()

Dim l_strSelectionFormula As String
Dim l_strReportFile As String

If optComplete(0).Value = True Then
    'not complete
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND (isnull({tracker_dadc_item.rejected}) or {tracker_dadc_item.rejected} = 0)"
ElseIf optComplete(1).Value = True Then
    'Finished
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) and {tracker_dadc_item.readytobill} <> 0"
ElseIf optComplete(2).Value = True Then
    'Billed
    l_strSelectionFormula = "not isnull({tracker_dadc_item.billed}) and {tracker_dadc_item.billed} <> 0"
ElseIf optComplete(3).Value = True Then
    'All Items
    l_strSelectionFormula = "1 = 1"
ElseIf optComplete(4).Value = True Then
    'File Made
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield2}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(5).Value = True Then
    'Sent without xml
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield10}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(6).Value = True Then
    'File made but not sent
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield2}) AND isnull({tracker_dadc_item.stagefield10}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(7).Value = True Then
    'Pending
    l_strSelectionFormula = "{tracker_dadc_item.readytobill} = 0 AND NOT isnull({tracker_dadc_item.rejected}) AND {tracker_dadc_item.rejected} <> 0 AND (isnull({tracker_dadc_item.duplicateitem}) OR {tracker_dadc_item.duplicateitem} = 0) "
ElseIf optComplete(8).Value = True Then
    'xml made but not sent
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield3}) AND isnull({tracker_dadc_item.stagefield4}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(9).Value = True Then
    'Sent with xml
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield4}) AND Not isnull({tracker_dadc_item.stagefield10}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(10).Value = True Then
    'Tape not here
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND isnull({tracker_dadc_item.stagefield1}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(11).Value = True Then
    'File not made
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND isnull({tracker_dadc_item.stagefield2}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(12).Value = True Then
    'xml Made
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield3}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(13).Value = True Then
    'xml not made
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND isnull({tracker_dadc_item.stagefield3}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(14).Value = True Then
    'Tape Sent Back
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield14})"
ElseIf optComplete(15).Value = True Then
    'Duplicate Items
    l_strSelectionFormula = "{tracker_dadc_item.duplicateitem} <> 0 "
ElseIf optComplete(16).Value = True Then
    'File Made and XML Not Made
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield2}) AND isnull({tracker_dadc_item.stagefield3}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(17).Value = True Then
    'XML Sent and File Not Sent
ElseIf optComplete(18).Value = True Then
    'Items being fixed.
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND not isnull({tracker_dadc_item.fixed}) AND {tracker_dadc_item.fixed} <> 0 "
ElseIf optComplete(19).Value = True Then
    'Sent by Drive
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield11}) "
ElseIf optComplete(20).Value = True Then
    'Sen t by Drive
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND (isnull({tracker_dadc_item.rejected}) or {tracker_dadc_item.rejected} = 0) AND ({tracker_dadc_item.decisiontree} <> 0)"
ElseIf optComplete(21).Value = True Then
    'Rush Jobs
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND (isnull({tracker_dadc_item.rejected}) or {tracker_dadc_item.rejected} = 0) AND ({tracker_dadc_item.priority} <>  0)"
ElseIf optComplete(22).Value = True Then
    'All Incomplete Jobs
    l_strSelectionFormula = "(isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) "
ElseIf optComplete(23).Value = True Then
    'Workable
    l_strSelectionFormula = "(isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_DADC_item.completeable})"
ElseIf optComplete(27).Value = True Then
    'AS11 Finished
    l_strSelectionFormula = "not isnull({tracker_dadc_item.stagefield5}) AND {tracker_dadc_item.AS11Billed} = 0"
    If txtSpecialProject.Text <> "" Then l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.specialproject} = """ & txtSpecialProject.Text & """"
ElseIf optComplete(28).Value = True Then
    'AS11 Billed
    l_strSelectionFormula = "not isnull({tracker_dadc_item.stagefield5}) AND {tracker_dadc_item.AS11Billed} <> 0 "
    If txtAS11JobID.Text <> "" Then l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.AS11JobID} = " & Val(txtAS11JobID.Text)
    If txtSpecialProject.Text <> "" Then l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.specialproject} = """ & txtSpecialProject.Text & """"
End If

If txtReference.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.itemreference} LIKE '" & QuoteSanitise(txtReference.Text) & "*' "
End If

If txtTitle.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.title} LIKE '" & QuoteSanitise(txtTitle.Text) & "*' "
End If
If txtSubtitle.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.subtitle} LIKE '" & QuoteSanitise(txtSubtitle.Text) & "*' "
End If
If txtBarcode.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.barcode} LIKE '" & QuoteSanitise(txtBarcode.Text) & "*' "
End If
If txtNewBarcode.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.newbarcode} LIKE '" & QuoteSanitise(txtNewBarcode.Text) & "*' "
End If
If txtFormat.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.format} LIKE '" & QuoteSanitise(txtFormat.Text) & "*' "
End If
If txtBBCCoreID.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.BBCCoreID} LIKE '" & QuoteSanitise(txtBBCCoreID.Text) & "*' "
End If
If Val(txtEpisode.Text) <> 0 Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.episode} = " & Val(txtEpisode.Text) & " "
End If
If Val(txtBatch.Text) <> 0 Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.batch} = " & Val(txtBatch.Text) & " "
End If
If Val(txtRequestID.Text) <> 0 Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.workflowID} = '" & txtRequestID.Text & "' "
End If
If txtStoredOn.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.storagebarcode} = '" & txtStoredOn.Text & "' "
End If
If txtSpecialProject.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.specialproject} = '" & txtSpecialProject.Text & "' "
End If
If txtJobID.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.jobID} = " & Val(txtJobID.Text) & " "
End If
If Not IsNull(datFrom.Value) Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.stagefield2} >= DateTimeValue('" & Format(datFrom.Value, "yyyy-mm-dd") & "') "
End If
If Not IsNull(datTo.Value) Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.stagefield2} < DateTimeValue('" & Format(datTo.Value, "yyyy-mm-dd") & "') "
End If


If chk48HrDue.Value <> 0 Then
    If Not IsNull(datDueDate.Value) Then
        l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.duedateupload} = DateTimeValue('" & Format(datDueDate.Value, "yyyy-mm-dd") & "') "
    Else
        l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.duedateupload} = DateTimeValue('" & Format(DateAdd("d", 2, Now), "yyyy-mm-dd") & "') "
    End If
End If

l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.companyID} = " & Val(lblCompanyID.Caption) & " "
    
l_strReportFile = g_strLocationOfCrystalReportFiles & "DADC_report_lastcommentonly.rpt"

PrintCrystalReport l_strReportFile, l_strSelectionFormula, True
    
End Sub

Private Sub cmdSearch_Click()

Dim l_rstTotal As ADODB.Recordset, l_lngVideoTotal As Long
Dim l_strSQL As String, l_strDateSearch As String

If lblCompanyID.Caption <> "" Then

    If optComplete(0).Value = True Then
        'Not Complete
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (rejected IS NULL or rejected = 0) "
'        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0)  "
    ElseIf optComplete(1).Value = True Then
        'Finished
        l_strSQL = "AND (billed IS NULL OR billed = 0) AND readytobill <> 0 AND stagefield15 IS NOT NULL "
    ElseIf optComplete(24).Value = True Then
        'Cancelled
        l_strSQL = "AND (billed IS NULL OR billed = 0) AND readytobill <> 0 AND stagefield13 IS NOT NULL "
    ElseIf optComplete(2).Value = True Then
        'Billed
        l_strSQL = "AND billed IS NOT NULL AND billed <> 0"
    ElseIf optComplete(4).Value = True Then
        'File Made
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (stagefield2 IS NOT NULL) AND rejected = 0 "
    ElseIf optComplete(5).Value = True Then
        'Sent without XML
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (stagefield10 IS NOT NULL) AND (stagefield4 IS NULL) and rejected = 0 "
    ElseIf optComplete(6).Value = True Then
        'File Made but not sent
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (stagefield2 IS NOT NULL) AND (stagefield10 IS NULL) AND rejected = 0 "
    ElseIf optComplete(7).Value = True Then
        'Pending
        l_strSQL = " AND readytobill = 0 AND rejected IS NOT NULL AND rejected <> 0 AND (duplicateitem IS NULL OR duplicateitem = 0) "
    ElseIf optComplete(8).Value = True Then
        'XMLMade but not sent
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (stagefield3 IS NOT NULL) AND (stagefield4 IS NULL) AND rejected = 0 "
    ElseIf optComplete(9).Value = True Then
        'Sent with XML
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (stagefield4 IS NOT NULL) AND (stagefield10 IS NOT NULL) and rejected = 0 "
    ElseIf optComplete(10).Value = True Then
        'Tape Not Here
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (stagefield1 IS NULL OR (stagefield1 IS NOT NULL and stagefield14 IS NOT NULL AND stagefield2 IS NULL)) AND rejected = 0 "
    ElseIf optComplete(11).Value = True Then
        'File Not Made
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (stagefield2 IS NULL) AND rejected = 0 "
    ElseIf optComplete(12).Value = True Then
        'XML Made
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (stagefield3 IS NOT NULL) AND rejected = 0 "
    ElseIf optComplete(13).Value = True Then
        'XML Not Made
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (stagefield3 IS NULL) and rejected = 0 "
    ElseIf optComplete(14).Value = True Then
        'Tape Sent Back
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (stagefield14 IS NOT NULL) "
    ElseIf optComplete(15).Value = True Then
        'Duplicate Items
        l_strSQL = " AND duplicateitem <> 0 "
    ElseIf optComplete(16).Value = True Then
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (stagefield2 IS NOT NULL) AND (stagefield3 IS NULL) AND rejected = 0 "
    ElseIf optComplete(17).Value = True Then
        'Sent XML without File
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (stagefield10 IS NULL) AND (stagefield4 IS NOT NULL) and rejected = 0 "
    ElseIf optComplete(18).Value = True Then
        'Items being fixed
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND fixed IS NOT NULL AND fixed <> 0 "
    ElseIf optComplete(19).Value = True Then
        'Sent by Drive
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (stagefield11 IS NOT NULL) "
    ElseIf optComplete(20).Value = True Then
        'Decision Tree
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (rejected IS NULL or rejected = 0) AND (decisiontree <> 0)"
    ElseIf optComplete(21).Value = True Then
        'Rush Jobs
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (rejected IS NULL or rejected = 0) AND (priority <>  0)"
    ElseIf optComplete(22).Value = True Then
        'All Incomplete Jobs
        l_strSQL = " AND (readytobill IS NULL OR readytobill = 0)"
    ElseIf optComplete(23).Value = True Then
        'Workable
        l_strSQL = " AND (readytobill IS NULL OR readytobill = 0) AND (completeable IS NOT NULL)"
    ElseIf optComplete(25).Value = True Then
        'Decision Tree Finished
        l_strSQL = " AND decisiontree = 0 AND decisiontreedate IS NOT NULL AND offDecisionTReeDate IS NOT NULL AND readytobill <> 0 AND DTBilled = 0"
    ElseIf optComplete(26).Value = True Then
        'DecisionTreeBilled
        l_strSQL = " AND decisiontree = 0 AND decisiontreedate IS NOT NULL AND offDecisionTReeDate IS NOT NULL AND readytobill <> 0 AND DTBilled <> 0"
    ElseIf optComplete(27).Value = True Then
        'AS-11 Finished
        l_strSQL = " AND stagefield5 IS NOT NULL AND AS11Billed = 0"
    ElseIf optComplete(28).Value = True Then
        'AS-11 Billed
        l_strSQL = " AND stagefield5 IS NOT NULL AND AS11Billed <> 0"
    End If
    
    If txtReference.Text <> "" Then
        l_strSQL = l_strSQL & " AND itemreference LIKE '" & QuoteSanitise(txtReference.Text) & "%' "
    End If
    
    If txtTitle.Text <> "" Then
        l_strSQL = l_strSQL & " AND title LIKE '" & QuoteSanitise(txtTitle.Text) & "%' "
    End If
    If txtSubtitle.Text <> "" Then
        l_strSQL = l_strSQL & " AND subtitle LIKE '" & QuoteSanitise(txtSubtitle.Text) & "%' "
    End If
    If txtBarcode.Text <> "" Then
        l_strSQL = l_strSQL & " AND barcode LIKE '" & QuoteSanitise(txtBarcode.Text) & "%' "
    End If
    If txtNewBarcode.Text <> "" Then
        l_strSQL = l_strSQL & " AND newbarcode LIKE '" & QuoteSanitise(txtNewBarcode.Text) & "%' "
    End If
    If txtFormat.Text <> "" Then
        l_strSQL = l_strSQL & " AND format LIKE '" & QuoteSanitise(txtFormat.Text) & "%' "
    End If
    If txtContentVersionCode.Text <> "" Then
        l_strSQL = l_strSQL & " AND contentversioncode LIKE '" & QuoteSanitise(txtContentVersionCode.Text) & "%' "
    End If
    If txtBBCCoreID.Text <> "" Then
        l_strSQL = l_strSQL & " AND BBCCoreID LIKE '" & QuoteSanitise(txtBBCCoreID.Text) & "%' "
    End If
    If Val(txtEpisode.Text) <> 0 Then
        l_strSQL = l_strSQL & " AND (episode IS NOT NULL AND episode = " & Val(txtEpisode.Text) & ") "
    End If
    If Val(txtBatch.Text) <> 0 Then
        l_strSQL = l_strSQL & " AND (batch IS NOT NULL AND batch = " & Val(txtBatch.Text) & ") "
    End If
    If Val(txtRequestID.Text) <> 0 Then
        l_strSQL = l_strSQL & " AND (workflowID IS NOT NULL AND workflowID = " & Val(txtRequestID.Text) & ") "
    End If
    If txtStoredOn.Text <> "" Then
        l_strSQL = l_strSQL & " AND storagebarcode = '" & txtStoredOn.Text & "' "
    End If
    If txtSpecialProject.Text <> "" Then
        l_strSQL = l_strSQL & " AND specialproject = '" & txtSpecialProject.Text & "' "
    End If
    If txtJobID.Text <> "" Then
        l_strSQL = l_strSQL & " AND jobID = " & Val(txtJobID.Text) & " "
    End If
    If txtDTJobID.Text <> "" Then
        l_strSQL = l_strSQL & " AND DTjobID = " & Val(txtDTJobID.Text) & " "
    End If
    If txtAS11JobID.Text <> "" Then
        l_strSQL = l_strSQL & " AND AS11jobID = " & Val(txtAS11JobID.Text) & " "
    End If

    If chk48HrDue.Value <> 0 Then
        If Not IsNull(datDueDate.Value) Then
            l_strSQL = l_strSQL & " AND (targetdate >= '" & Format(datDueDate.Value, "mm/dd/yy") & "' AND targetdate < '" & Format(DateAdd("d", 1, datDueDate.Value), "mm/dd/yy") & "') "
        Else
            l_strSQL = l_strSQL & " AND (targetdate >= '" & Format(Now, "mm/dd/yy") & "' AND targetdate < '" & Format(DateAdd("d", 3, Now), "mm/dd/yy") & "') "
        End If
    End If
    
    If Not IsNull(datFrom.Value) Then
        l_strDateSearch = " AND stagefield2 >= '" & ConvertDate(datFrom.Value) & "'"
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND stagefield2 < '" & ConvertDate(datTo.Value) & "'"
        Else
            l_strDateSearch = l_strDateSearch & " AND stagefield2 < '" & ConvertDate(DateAdd("d", 1, datFrom.Value)) & "'"
        End If
    End If
    
    If txtActualSQL.Text <> "" Then l_strSQL = l_strSQL & " AND (" & txtActualSQL.Text & ") "

    Dim l_rstTest As ADODB.Recordset
    Set l_rstTest = ExecuteSQL("SELECT * FROM tracker_dadc_item " & m_strSearch & l_strSQL & l_strDateSearch & m_strOrderby & ";", g_strExecuteError)
    
    If InStr(UCase(g_strExecuteError), "ERROR") <= 0 Then
    
        l_rstTest.Close
        adoItems.ConnectionString = g_strConnection
        adoItems.RecordSource = "SELECT * FROM tracker_dadc_item WHERE 1 = 0;"
        adoItems.Refresh
        adoItems.RecordSource = "SELECT * FROM tracker_dadc_item " & m_strSearch & l_strSQL & l_strDateSearch & m_strOrderby & ";"
        adoItems.Refresh
        
        adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"
    
        Set l_rstTotal = ExecuteSQL("SELECT Sum(duration) FROM tracker_dadc_item " & m_strSearch & l_strSQL & l_strDateSearch & ";", g_strExecuteError)
        CheckForSQLError
        
        If IsNull(l_rstTotal(0)) Then
            l_lngVideoTotal = 0
        Else
            l_lngVideoTotal = l_rstTotal(0)
        End If
        
        l_rstTotal.Close
    
        txtTotalDuration.Text = l_lngVideoTotal
    
'        Set l_rstTotal = ExecuteSQL("SELECT Sum(durationestimate) FROM tracker_dadc_item " & m_strSearch & l_strSQL & ";", g_strExecuteError)
'        CheckForSQLError
'
'        If IsNull(l_rstTotal(0)) Then
'            l_lngVideoTotal = 0
'        Else
'            l_lngVideoTotal = l_rstTotal(0)
'        End If
'
'        l_rstTotal.Close
'
'        txtTotalDurationEstimate.Text = l_lngVideoTotal
'
        Set l_rstTotal = ExecuteSQL("SELECT Sum(gbsent) FROM tracker_dadc_item " & m_strSearch & l_strSQL & l_strDateSearch & ";", g_strExecuteError)
        CheckForSQLError
        
        If IsNull(l_rstTotal(0)) Then
            l_lngVideoTotal = 0
        Else
            l_lngVideoTotal = l_rstTotal(0)
        End If
        
        l_rstTotal.Close
    
        txtTotalSize.Text = l_lngVideoTotal
    
        Set l_rstTotal = ExecuteSQL("SELECT count(tracker_dadc_itemID) FROM tracker_dadc_item " & m_strSearch & l_strSQL & l_strDateSearch & " AND format = 'HDCAM-SR';", g_strExecuteError)
        CheckForSQLError
        
        If IsNull(l_rstTotal(0)) Then
            l_lngVideoTotal = 0
        Else
            l_lngVideoTotal = l_rstTotal(0)
        End If
        
        l_rstTotal.Close
    
        txtHDSR.Text = l_lngVideoTotal
    
        Set l_rstTotal = ExecuteSQL("SELECT count(tracker_dadc_itemID) FROM tracker_dadc_item " & m_strSearch & l_strSQL & l_strDateSearch & " AND format = 'HDCAM';", g_strExecuteError)
        CheckForSQLError
        
        If IsNull(l_rstTotal(0)) Then
            l_lngVideoTotal = 0
        Else
            l_lngVideoTotal = l_rstTotal(0)
        End If
        
        l_rstTotal.Close
    
        txtHDCAM.Text = l_lngVideoTotal
    
        Set l_rstTotal = ExecuteSQL("SELECT count(tracker_dadc_itemID) FROM tracker_dadc_item " & m_strSearch & l_strSQL & l_strDateSearch & " AND format = 'DBETA';", g_strExecuteError)
        CheckForSQLError
        
        If IsNull(l_rstTotal(0)) Then
            l_lngVideoTotal = 0
        Else
            l_lngVideoTotal = l_rstTotal(0)
        End If
        
        l_rstTotal.Close
    
        txtDBETA.Text = l_lngVideoTotal
    
        Set l_rstTotal = ExecuteSQL("SELECT count(tracker_dadc_itemID) FROM tracker_dadc_item " & m_strSearch & l_strSQL & l_strDateSearch & " AND NOT (format = 'DBETA' OR format = 'HDCAM-SR' OR format = 'HDCAM');", g_strExecuteError)
            
        CheckForSQLError
        
        If IsNull(l_rstTotal(0)) Then
            l_lngVideoTotal = 0
        Else
            l_lngVideoTotal = l_rstTotal(0)
        End If
        
        l_rstTotal.Close
    
        txtOTHER.Text = l_lngVideoTotal
    
        If Not IsNull(datDueDate.Value) Then
            Set l_rstTotal = ExecuteSQL("SELECT count(tracker_dadc_itemID) FROM tracker_dadc_item " & m_strSearch & l_strSQL & l_strDateSearch & " AND duedateupload = '" & Format(datDueDate.Value, "mm/dd/yyyy") & "';", g_strExecuteError)
        Else
            Set l_rstTotal = ExecuteSQL("SELECT count(tracker_dadc_itemID) FROM tracker_dadc_item " & m_strSearch & l_strSQL & l_strDateSearch & " AND duedateupload = '" & Format(DateAdd("d", 2, Now), "mm/dd/yyyy") & "';", g_strExecuteError)
        End If
        CheckForSQLError

        If IsNull(l_rstTotal(0)) Then
            l_lngVideoTotal = 0
        Else
            l_lngVideoTotal = l_rstTotal(0)
        End If

        l_rstTotal.Close

        txtCount48.Text = l_lngVideoTotal

        If Not IsNull(datDueDate.Value) Then
            Set l_rstTotal = ExecuteSQL("SELECT count(tracker_dadc_itemID) FROM tracker_dadc_item " & m_strSearch & l_strSQL & l_strDateSearch & _
            " AND duedateupload = '" & Format(datDueDate.Value, "mm/dd/yyyy") & "' AND stagefield1 IS NOT NULL;", g_strExecuteError)
        Else
            Set l_rstTotal = ExecuteSQL("SELECT count(tracker_dadc_itemID) FROM tracker_dadc_item " & m_strSearch & l_strSQL & l_strDateSearch & _
            " AND duedateupload = '" & Format(DateAdd("d", 2, Now), "mm/dd/yyyy") & "' AND stagefield1 IS NOT NULL;", g_strExecuteError)
        End If
        CheckForSQLError

        If IsNull(l_rstTotal(0)) Then
            l_lngVideoTotal = 0
        Else
            l_lngVideoTotal = l_rstTotal(0)
        End If

        l_rstTotal.Close

        txtCount48TapesHere.Text = l_lngVideoTotal

        l_strDateSearch = ""
        If Not IsNull(datFrom.Value) Then
            l_strDateSearch = " AND stagefield2 >= '" & ConvertDate(datFrom.Value) & "'"
            If Not IsNull(datTo.Value) Then
                l_strDateSearch = l_strDateSearch & " AND stagefield2 < '" & ConvertDate(datTo.Value) & "'"
            Else
                l_strDateSearch = l_strDateSearch & " AND stagefield2 < '" & ConvertDate(DateAdd("d", 1, datFrom.Value)) & "'"
            End If
            Set l_rstTotal = ExecuteSQL("SELECT count(tracker_dadc_itemID) FROM tracker_dadc_item " & m_strSearch & l_strSQL & l_strDateSearch & ";", g_strExecuteError)
            CheckForSQLError
            
            If IsNull(l_rstTotal(0)) Then
                l_lngVideoTotal = 0
            Else
                l_lngVideoTotal = l_rstTotal(0)
            End If
            
            l_rstTotal.Close
        
            txtFilesMade.Text = l_lngVideoTotal
        
            l_strDateSearch = ""
            l_strDateSearch = " AND stagefield3 >= '" & ConvertDate(datFrom.Value) & "'"
            If Not IsNull(datTo.Value) Then
                l_strDateSearch = l_strDateSearch & " AND stagefield3 < '" & ConvertDate(datTo.Value) & "'"
            Else
                l_strDateSearch = l_strDateSearch & " AND stagefield3 < '" & ConvertDate(DateAdd("d", 1, datFrom.Value)) & "'"
            End If
            Set l_rstTotal = ExecuteSQL("SELECT count(tracker_dadc_itemID) FROM tracker_dadc_item " & m_strSearch & l_strSQL & l_strDateSearch & ";", g_strExecuteError)
            CheckForSQLError
            
            If IsNull(l_rstTotal(0)) Then
                l_lngVideoTotal = 0
            Else
                l_lngVideoTotal = l_rstTotal(0)
            End If
            
            l_rstTotal.Close
        
            txtXMLMade.Text = l_lngVideoTotal
        
            l_strDateSearch = ""
            l_strDateSearch = " AND pendingdate >= '" & ConvertDate(datFrom.Value) & "'"
            If Not IsNull(datTo.Value) Then
                l_strDateSearch = l_strDateSearch & " AND pendingdate < '" & ConvertDate(datTo.Value) & "'"
            Else
                l_strDateSearch = l_strDateSearch & " AND pendingdate < '" & ConvertDate(DateAdd("d", 1, datFrom.Value)) & "'"
            End If
            Set l_rstTotal = ExecuteSQL("SELECT count(tracker_dadc_itemID) FROM tracker_dadc_item " & m_strSearch & l_strSQL & l_strDateSearch & ";", g_strExecuteError)
            CheckForSQLError
            
            If IsNull(l_rstTotal(0)) Then
                l_lngVideoTotal = 0
            Else
                l_lngVideoTotal = l_rstTotal(0)
            End If
            
            l_rstTotal.Close
        
            txtItemsOnPending.Text = l_lngVideoTotal
        
            l_strDateSearch = ""
            l_strDateSearch = " AND decisiontreedate >= '" & ConvertDate(datFrom.Value) & "'"
            If Not IsNull(datTo.Value) Then
                l_strDateSearch = l_strDateSearch & " AND decisiontreedate < '" & ConvertDate(datTo.Value) & "'"
            Else
                l_strDateSearch = l_strDateSearch & " AND decisiontreedate < '" & ConvertDate(DateAdd("d", 1, datFrom.Value)) & "'"
            End If
            Set l_rstTotal = ExecuteSQL("SELECT count(tracker_dadc_itemID) FROM tracker_dadc_item " & m_strSearch & l_strSQL & l_strDateSearch & ";", g_strExecuteError)
            CheckForSQLError
            
            If IsNull(l_rstTotal(0)) Then
                l_lngVideoTotal = 0
            Else
                l_lngVideoTotal = l_rstTotal(0)
            End If
            
            l_rstTotal.Close
        
            txtDecisionTree.Text = l_lngVideoTotal
        
            l_strDateSearch = ""
            l_strDateSearch = " AND stagefield4 >= '" & ConvertDate(datFrom.Value) & "'"
            If Not IsNull(datTo.Value) Then
                l_strDateSearch = l_strDateSearch & " AND stagefield4 < '" & ConvertDate(datTo.Value) & "'"
            Else
                l_strDateSearch = l_strDateSearch & " AND stagefield4 < '" & ConvertDate(DateAdd("d", 1, datFrom.Value)) & "'"
            End If
            Set l_rstTotal = ExecuteSQL("SELECT count(tracker_dadc_itemID) FROM tracker_dadc_item " & m_strSearch & l_strSQL & l_strDateSearch & ";", g_strExecuteError)
            CheckForSQLError
            
            If IsNull(l_rstTotal(0)) Then
                l_lngVideoTotal = 0
            Else
                l_lngVideoTotal = l_rstTotal(0)
            End If
            
            l_rstTotal.Close
        
            txtQueuedForDelivery.Text = l_lngVideoTotal
        
        Else
            txtFilesMade.Text = ""
            txtXMLMade.Text = ""
            txtItemsOnPending = ""
            txtQueuedForDelivery.Text = ""
        End If
        
        Set l_rstTotal = Nothing
    
    Else
        DoEvents
        MsgBox "The Search as specified is invalid - please correct." & vbCrLf & g_strExecuteError, vbCritical, "Error..."
    End If
    
    Set l_rstTest = Nothing

End If

End Sub

Private Sub cmdSomeFields_Click()

grdItems.Columns("IngestRequestor").Visible = False
grdItems.Columns("contentversioncode").Visible = False
grdItems.Columns("contentversionID").Visible = False
grdItems.Columns("BBCCoreID").Visible = False
grdItems.Columns("kitID").Visible = False
grdItems.Columns("needsconform").Visible = False
grdItems.Columns("externalalphaID").Visible = False
grdItems.Columns("originalexternalalphaID").Visible = False
grdItems.Columns("workflowID").Visible = False
grdItems.Columns("externalalphakey").Visible = False
grdItems.Columns("dbbcrid").Visible = False
grdItems.Columns("LegacyOracTVAENumber").Visible = False
grdItems.Columns("imageaspectratio").Visible = True
'grdItems.Columns("language").Visible = False
grdItems.Columns("subtitleslanguage").Visible = False
grdItems.Columns("barcode").Visible = False
grdItems.Columns("newbarcode").Visible = True
grdItems.Columns("format").Visible = True
grdItems.Columns("location").Visible = True
grdItems.Columns("tapeshelf").Visible = True
grdItems.Columns("duedateupload").Visible = True
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("completeable").Visible = True
grdItems.Columns("targetdate").Visible = True
grdItems.Columns("legacylinestandard").Visible = False
grdItems.Columns("legacyframerate").Visible = False
grdItems.Columns("language").Visible = False
grdItems.Columns("pendingdate").Visible = False
grdItems.Columns("offpendingdate").Visible = False
grdItems.Columns("daysonpending").Visible = False
grdItems.Columns("decisiontreedate").Visible = False
grdItems.Columns("offdecisiontreedate").Visible = False
grdItems.Columns("daysondecisiontree").Visible = False
grdItems.Columns("DTTargetDate").Visible = False
grdItems.Columns("DTClock").Visible = False
grdItems.Columns("DTBarsAndTone").Visible = False
grdItems.Columns("DTTimeCode").Visible = False
grdItems.Columns("DTSourceNotToSpec").Visible = False
grdItems.Columns("DTTrackLayoutWrong").Visible = False
grdItems.Columns("DTDuplicatedTracksRemoved").Visible = False
grdItems.Columns("DTEdit15mins").Visible = False
grdItems.Columns("DTEdit30mins").Visible = False
grdItems.Columns("DTEdit45mins").Visible = False
grdItems.Columns("DTEdit60mins").Visible = False
grdItems.Columns("DTBilled").Visible = False
grdItems.Columns("DTJobID").Visible = False
grdItems.Columns("batch").Visible = True
grdItems.Columns("specialproject").Visible = True
grdItems.Columns("stagefield1").Visible = True
grdItems.Columns("stagefield2").Visible = True
grdItems.Columns("stagefield3").Visible = True
grdItems.Columns("stagefield4").Visible = True
grdItems.Columns("stagefield5").Visible = True
'grdItems.Columns("stagefield6").Visible = True
'grdItems.Columns("stagefield7").Visible = True
grdItems.Columns("stagefield8").Visible = True
'grdItems.Columns("stagefield9").Visible = True
grdItems.Columns("stagefield10").Visible = True
'grdItems.Columns("stagefield11").Visible = True
'grdItems.Columns("stagefield12").Visible = True
grdItems.Columns("stagefield13").Visible = True
grdItems.Columns("stagefield14").Visible = False
grdItems.Columns("stagefield15").Visible = True
grdItems.Columns("xmlmadeby").Visible = True

End Sub

Private Sub cmdUnbill_Click()

grdItems.Columns("billed").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False

End Sub

Private Sub cmdUnbillAll_Click()

Dim l_lngTemp As Long
l_lngTemp = chkDontVerifyXML.Value
chkDontVerifyXML.Value = 1
adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then

    Do While Not adoItems.Recordset.EOF
        adoItems.Recordset("billed") = 0
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
    Loop
End If

grdItems.Refresh
chkDontVerifyXML.Value = l_lngTemp

End Sub

Private Sub cmdUnbillAS11_Click()

grdItems.Columns("AS11billed").Text = 0
grdItems.Columns("AS11jobID").Text = ""
grdItems.Update
cmdAS11BillItem.Visible = True
cmdManualBillAS11Item.Visible = True
cmdUnbillAS11.Visible = False

End Sub

Private Sub cmdUnbillDT_Click()

grdItems.Columns("dtbilled").Text = 0
grdItems.Update
cmdDTBillItem.Visible = True
cmdManualDTBillItem.Visible = True
cmdUnbillDT.Visible = False

End Sub

Private Sub cmdUpdate_Click()

Dim l_lngVerifyValue As Long, l_lngLibraryID As Variant, l_strBarcode As String, l_strMessage As String
Dim Count As Long
Dim l_rst As ADODB.Recordset, l_strSQL As String

If Not adoItems.Recordset.EOF Then
    l_lngVerifyValue = chkDontVerifyXML.Value
    chkDontVerifyXML.Value = 1
    adoItems.Recordset.MoveFirst
    ProgressBar1.Max = adoItems.Recordset.RecordCount
    ProgressBar1.Value = 0
    Count = 0
    ProgressBar1.Visible = True
    Do While Not adoItems.Recordset.EOF
        ProgressBar1.Value = Count
        DoEvents
        adoItems.Recordset("storagebarcode") = grdItems.Columns("Stored On").Text
        adoItems.Recordset("storagefolder") = GetDataSQL("SELECT TOP 1 altlocation FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND clipformat = 'Quicktime' AND clipcodec = 'ProRes HQ' AND system_deleted = 0;")
        l_strBarcode = grdItems.Columns("newbarcode").Text
        If GetData("library", "libraryID", "barcode", l_strBarcode) = 0 And l_strBarcode <> "" Then
            If IsNumeric(Right(l_strBarcode, 1)) Then
                l_strSQL = "SELECT libraryID FROM library WHERE barcode LIKE '" & Left(l_strBarcode, 1) & "%" & Right(l_strBarcode, 4) & "' AND system_deleted = 0 AND companyID in (SELECT companyID FROM company WHERE name LIKE 'BBC%' OR name LIKE 'Sony DADC%');"
            Else
                l_strSQL = "SELECT libraryID FROM library WHERE barcode LIKE '" & Left(l_strBarcode, 1) & "%" & Right(l_strBarcode, 5) & "' AND system_deleted = 0 AND companyID in (SELECT companyID FROM company WHERE name LIKE 'BBC%' OR name LIKE 'Sony DADC%');"
            End If
            Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
            If l_rst.RecordCount > 0 Then
                l_rst.MoveFirst
                Do While Not l_rst.EOF
                    l_lngLibraryID = l_rst("libraryID")
                    l_strMessage = "Is " & GetData("library", "barcode", "libraryID", Val(l_lngLibraryID)) & " - " & GetData("library", "title", "libraryID", Val(l_lngLibraryID)) & " Ep. " & GetData("library", "episode", "libraryID", Val(l_lngLibraryID))
                    l_strMessage = l_strMessage & vbCrLf & "likely to be the correct barcode for " & l_strBarcode & " - " & adoItems.Recordset("title") & " Ep. " & adoItems.Recordset("episode")
                    If MsgBox(l_strMessage, vbYesNo, "Possible Fuzzy Search Tape Found") = vbYes Then
                        adoItems.Recordset("newbarcode") = GetData("library", "barcode", "libraryID", Val(l_lngLibraryID))
                        adoItems.Recordset.Update
                        l_strBarcode = adoItems.Recordset("newbarcode")
                        Exit Do
                    End If
                    l_rst.MoveNext
                Loop
            End If
        End If
        If GetData("library", "format", "barcode", l_strBarcode) <> "" Then
            adoItems.Recordset("format") = GetData("library", "format", "barcode", l_strBarcode)
        End If
        adoItems.Recordset("tapelocation") = GetData("library", "location", "barcode", l_strBarcode)
        If UCase(adoItems.Recordset("tapelocation")) = "OPERATIONS" Or UCase(adoItems.Recordset("tapelocation")) = "LIBRARY" Or UCase(adoItems.Recordset("tapelocation")) = "OFF SITE" Then
'        Or UCase(adoItems.Recordset("tapelocation")) = "SONY DADC HOLDING" Or UCase(adoItems.Recordset("tapelocation")) = "SONY DADC INGEST SHELF" _
'        Or UCase(adoItems.Recordset("tapelocation")) = "SONY DADC PENDING" Or UCase(adoItems.Recordset("tapelocation")) = "SONY DADC XML" _
'        Or UCase(adoItems.Recordset("tapelocation")) = "MANUAL TROLLEY" Or UCase(adoItems.Recordset("tapelocation")) = "FRONT DESKS" Then
            adoItems.Recordset("tapeshelf") = GetData("library", "shelf", "barcode", l_strBarcode)
        Else
            adoItems.Recordset("tapeshelf") = ""
        End If
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
        Count = Count + 1
    Loop
    chkDontVerifyXML.Value = l_lngVerifyValue
End If

ProgressBar1.Visible = False

End Sub

Private Sub Command1_Click()

Dim l_lngVerifyValue As Long, l_varLibraryID As Variant, l_strBarcode As String, l_strMessage As String
Dim Count As Long

If Not adoItems.Recordset.EOF Then
    l_lngVerifyValue = chkDontVerifyXML.Value
    chkDontVerifyXML.Value = 1
    adoItems.Recordset.MoveFirst
    ProgressBar1.Max = adoItems.Recordset.RecordCount
    ProgressBar1.Value = 0
    Count = 0
    ProgressBar1.Visible = True
    Do While Not adoItems.Recordset.EOF
        ProgressBar1.Value = Count
        DoEvents
        adoItems.Recordset("targetdatecountdown") = 0
        If adoItems.Recordset("stagefield1") <> "" And Val(adoItems.Recordset("rejected")) = 0 And adoItems.Recordset("readytobill") = 0 Then
        
            If Trim(" " & adoItems.Recordset("Completeable")) = "" Then adoItems.Recordset("Completeable") = Now()
            If Trim(" " & adoItems.Recordset("firstcompleteable")) = "" Then adoItems.Recordset("firstCompleteable") = adoItems.Recordset("Completeable")
            If Trim(" " & adoItems.Recordset("TargetDate")) = "" Then adoItems.Recordset("TargetDate") = DateAdd("d", Val(adoItems.Recordset("targetdatecountdown")), Now)
        
        ElseIf adoItems.Recordset("readytobill") = 0 Then
        
            adoItems.Recordset("Completeable") = Null
            adoItems.Recordset("TargetDate") = Null
                
        End If
        
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
        Count = Count + 1
    Loop
    chkDontVerifyXML.Value = l_lngVerifyValue
End If

ProgressBar1.Visible = False

End Sub

Private Sub cmdZipAndExport_Click()

Dim l_strSelectionFormula As String
Dim l_strReportFile As String

If optComplete(0).Value = True Then
    'not complete
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND (isnull({tracker_dadc_item.rejected}) or {tracker_dadc_item.rejected} = 0)"
ElseIf optComplete(1).Value = True Then
    'Finished
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) and {tracker_dadc_item.readytobill} <> 0"
ElseIf optComplete(2).Value = True Then
    'Billed
    l_strSelectionFormula = "not isnull({tracker_dadc_item.billed}) and {tracker_dadc_item.billed} <> 0"
ElseIf optComplete(3).Value = True Then
    'All Items
    l_strSelectionFormula = "1 = 1"
ElseIf optComplete(4).Value = True Then
    'File Made
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield2}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(5).Value = True Then
    'Sent without xml
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield10}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(6).Value = True Then
    'File made but not sent
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield2}) AND isnull({tracker_dadc_item.stagefield10}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(7).Value = True Then
    'Pending
    l_strSelectionFormula = "{tracker_dadc_item.rejected} <> 0 AND (isnull({tracker_dadc_item.duplicateitem}) OR {tracker_dadc_item.duplicateitem} = 0) "
ElseIf optComplete(8).Value = True Then
    'xml made but not sent
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield3}) AND isnull({tracker_dadc_item.stagefield4}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(9).Value = True Then
    'Sent with xml
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield4}) AND Not isnull({tracker_dadc_item.stagefield10}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(10).Value = True Then
    'Tape not here
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND isnull({tracker_dadc_item.stagefield1}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(11).Value = True Then
    'File not made
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND isnull({tracker_dadc_item.stagefield2}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(12).Value = True Then
    'xml Made
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield3}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(13).Value = True Then
    'xml not made
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND isnull({tracker_dadc_item.stagefield3}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(14).Value = True Then
    'Tape Sent Back
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield14})"
ElseIf optComplete(15).Value = True Then
    'Duplicate Items
    l_strSelectionFormula = "{tracker_dadc_item.duplicateitem} <> 0 "
ElseIf optComplete(16).Value = True Then
    'File Made and XML Not Made
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield2}) AND isnull({tracker_dadc_item.stagefield3}) AND {tracker_dadc_item.rejected} = 0"
ElseIf optComplete(17).Value = True Then
    'XML Sent and File Not Sent
ElseIf optComplete(18).Value = True Then
    'Items being fixed.
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND not isnull({tracker_dadc_item.fixed}) AND {tracker_dadc_item.fixed} <> 0 "
ElseIf optComplete(19).Value = True Then
    'Sent by Drive
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_dadc_item.stagefield11}) "
ElseIf optComplete(20).Value = True Then
    'Sen t by Drive
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND (isnull({tracker_dadc_item.rejected}) or {tracker_dadc_item.rejected} = 0) AND ({tracker_dadc_item.decisiontree} <> 0)"
ElseIf optComplete(21).Value = True Then
    'Rush Jobs
    l_strSelectionFormula = "(isnull({tracker_dadc_item.billed}) or {tracker_dadc_item.billed} = 0) AND (isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND (isnull({tracker_dadc_item.rejected}) or {tracker_dadc_item.rejected} = 0) AND ({tracker_dadc_item.priority} <>  0)"
ElseIf optComplete(22).Value = True Then
    'All Incomplete Jobs
    l_strSelectionFormula = "(isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) "
ElseIf optComplete(23).Value = True Then
    'Workable
    l_strSelectionFormula = "(isnull({tracker_dadc_item.readytobill}) OR {tracker_dadc_item.readytobill} = 0) AND not isnull({tracker_DADC_item.completeable})"
End If

If txtReference.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.itemreference} LIKE '" & QuoteSanitise(txtReference.Text) & "*' "
End If

If txtTitle.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.title} LIKE '" & QuoteSanitise(txtTitle.Text) & "*' "
End If
If txtSubtitle.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.subtitle} LIKE '" & QuoteSanitise(txtSubtitle.Text) & "*' "
End If
If txtBarcode.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.barcode} LIKE '" & QuoteSanitise(txtBarcode.Text) & "*' "
End If
If txtNewBarcode.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.newbarcode} LIKE '" & QuoteSanitise(txtNewBarcode.Text) & "*' "
End If
If txtFormat.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.format} LIKE '" & QuoteSanitise(txtFormat.Text) & "*' "
End If
If txtBBCCoreID.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.BBCCoreID} LIKE '" & QuoteSanitise(txtBBCCoreID.Text) & "*' "
End If
If Val(txtEpisode.Text) <> 0 Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.episode} = " & Val(txtEpisode.Text) & " "
End If
If Val(txtBatch.Text) <> 0 Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.batch} = " & Val(txtBatch.Text) & " "
End If
If Val(txtRequestID.Text) <> 0 Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.workflowID} = '" & txtRequestID.Text & "' "
End If
If txtStoredOn.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.storagebarcode} = '" & txtStoredOn.Text & "' "
End If
If txtSpecialProject.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.specialproject} = '" & txtSpecialProject.Text & "' "
End If
If txtJobID.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.jobID} = " & Val(txtJobID.Text) & " "
End If

If chk48HrDue.Value <> 0 Then
    If Not IsNull(datDueDate.Value) Then
        l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.duedateupload} = '" & Format(datDueDate.Value, "yyyy-mm-dd") & "' "
    Else
        l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.duedateupload} = '" & Format(DateAdd("d", 2, Now), "yyyy-mm-dd") & "' "
    End If
End If

l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_dadc_item.companyID} = " & Val(lblCompanyID.Caption) & " "
    
l_strReportFile = g_strLocationOfCrystalReportFiles & "DADC_report_lastcommentonly.rpt"

Dim crxApplication As CRAXDRT.Application
Dim crxReport As CRAXDRT.Report

Set crxApplication = New CRAXDRT.Application

Set crxReport = crxApplication.OpenReport(GetUNCNameNT(l_strReportFile))

If Len(l_strSelectionFormula) > 0 Then
    crxReport.RecordSelectionFormula = l_strSelectionFormula
End If

crxReport.DiscardSavedData

Dim FSO As Scripting.FileSystemObject, l_strExportName As String
Set FSO = New Scripting.FileSystemObject
l_strExportName = "DADC_report_lastcommentonly"
If FSO.FileExists(App.Path & "\" & l_strExportName & ".xls") Then FSO.DeleteFile App.Path & "\" & l_strExportName & ".xls", True
If FSO.FileExists(App.Path & "\" & l_strExportName & ".zip") Then FSO.DeleteFile App.Path & "\" & l_strExportName & ".zip", True
crxReport.ExportOptions.DiskFileName = App.Path & "\" & l_strExportName & ".xls"
crxReport.ExportOptions.DestinationType = crEDTDiskFile
crxReport.ExportOptions.FormatType = crEFTExcelDataOnly
crxReport.Export False
If Create_Empty_Zip(App.Path & "\" & l_strExportName & ".zip") = True Then
    Zip_Activity "ZIPFILE", App.Path & "\" & l_strExportName & ".xls", App.Path & "\" & l_strExportName & ".zip"

    Dim rs As ADODB.Recordset, SQL As String
    
    SQL = "SELECT email, fullname FROM trackernotification WHERE contractgroup = 'DADC' and trackermessageID = 53;"
    Set rs = ExecuteSQL(SQL, g_strExecuteError)
    
    If rs.RecordCount > 0 Then
        rs.MoveFirst
        Do While Not rs.EOF
            SendSMTPMail rs("email"), rs("fullname"), "MX1 DADC BBC Tracker Data", App.Path & "\" & l_strExportName & ".zip", "A Zipped MX1 DADC BBC Tracker Data export is attached", True, "", ""
            rs.MoveNext
        Loop
    End If
    rs.Close
    Set rs = Nothing
    
End If

Set crxReport = Nothing
crxApplication.CanClose
Set crxApplication = Nothing

DoEvents

Screen.MousePointer = vbDefault


End Sub

Private Sub cmdAS11Fields_Click()

grdItems.Columns("IngestRequestor").Visible = False
grdItems.Columns("contentversioncode").Visible = False
grdItems.Columns("contentversionID").Visible = False
grdItems.Columns("BBCCoreID").Visible = False
grdItems.Columns("kitID").Visible = False
grdItems.Columns("needsconform").Visible = False
grdItems.Columns("externalalphaID").Visible = False
grdItems.Columns("originalexternalalphaID").Visible = False
grdItems.Columns("workflowID").Visible = False
grdItems.Columns("externalalphakey").Visible = False
grdItems.Columns("dbbcrid").Visible = False
grdItems.Columns("LegacyOracTVAENumber").Visible = False
grdItems.Columns("imageaspectratio").Visible = False
'grdItems.Columns("language").Visible = False
grdItems.Columns("subtitleslanguage").Visible = False
grdItems.Columns("barcode").Visible = False
grdItems.Columns("newbarcode").Visible = True
grdItems.Columns("format").Visible = True
grdItems.Columns("location").Visible = False
grdItems.Columns("tapeshelf").Visible = False
grdItems.Columns("duedateupload").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("targetdate").Visible = False
grdItems.Columns("legacylinestandard").Visible = False
grdItems.Columns("legacyframerate").Visible = False
grdItems.Columns("language").Visible = False
grdItems.Columns("pendingdate").Visible = False
grdItems.Columns("offpendingdate").Visible = False
grdItems.Columns("daysonpending").Visible = False
grdItems.Columns("decisiontreedate").Visible = False
grdItems.Columns("offdecisiontreedate").Visible = False
grdItems.Columns("daysondecisiontree").Visible = False
grdItems.Columns("DTTargetDate").Visible = False
grdItems.Columns("DTClock").Visible = False
grdItems.Columns("DTBarsAndTone").Visible = False
grdItems.Columns("DTTimeCode").Visible = False
grdItems.Columns("DTSourceNotToSpec").Visible = False
grdItems.Columns("DTTrackLayoutWrong").Visible = False
grdItems.Columns("DTDuplicatedTracksRemoved").Visible = False
grdItems.Columns("DTEdit15mins").Visible = False
grdItems.Columns("DTEdit30mins").Visible = False
grdItems.Columns("DTEdit45mins").Visible = False
grdItems.Columns("DTEdit60mins").Visible = False
grdItems.Columns("DTBilled").Visible = False
grdItems.Columns("DTJobID").Visible = False
grdItems.Columns("batch").Visible = True
grdItems.Columns("specialproject").Visible = True
grdItems.Columns("stagefield1").Visible = False
grdItems.Columns("stagefield2").Visible = False
grdItems.Columns("stagefield3").Visible = False
grdItems.Columns("stagefield4").Visible = False
grdItems.Columns("stagefield5").Visible = True
'grdItems.Columns("stagefield6").Visible = True
'grdItems.Columns("stagefield7").Visible = True
grdItems.Columns("stagefield8").Visible = False
'grdItems.Columns("stagefield9").Visible = True
grdItems.Columns("stagefield10").Visible = False
'grdItems.Columns("stagefield11").Visible = True
'grdItems.Columns("stagefield12").Visible = True
grdItems.Columns("stagefield13").Visible = False
grdItems.Columns("stagefield14").Visible = False
grdItems.Columns("stagefield15").Visible = True
grdItems.Columns("xmlmadeby").Visible = False
grdItems.Columns("AS11billed").Visible = True
grdItems.Columns("AS11JobID").Visible = True

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

adoComments.ConnectionString = g_strConnection

grdItems.StyleSets("headerfield").BackColor = &HE7FFE7
grdItems.StyleSets("stagefield").BackColor = &HE7FFFF
grdItems.StyleSets("conclusionfield").BackColor = &HFFFFE7
grdItems.StyleSets("dbb").BackColor = &H80FFFF

grdItems.StyleSets.Add "Error"
grdItems.StyleSets("Error").BackColor = &HA0A0FF
grdItems.StyleSets.Add "Fixed"
grdItems.StyleSets("Fixed").BackColor = &HA0FFA0
grdItems.StyleSets.Add "Notify"
grdItems.StyleSets("Notify").BackColor = &HA0C0FF

optComplete(0).Value = True

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/dadctracker %' AND companyID > 100 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/dadctracker %' ORDER BY name;"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

m_blnDontVerifyXML = False

grdItems.Columns("itemfilename").Visible = True

HideAllColumns

DoEvents

Dim l_rstChoices As ADODB.Recordset, l_blnWide As Boolean

cmdSomeFields.Value = True

m_strOrderby = " ORDER BY CASE WHEN priority <> 0 THEN 0 ELSE 1 END, CASE WHEN targetdate IS NULL THEN 1 ELSE 0 END, targetdate, itemreference, title, episode, subtitle"

m_blnDelete = False
m_blnBilling = False

If CheckAccess("/DADCManualMarkAllBilled", True) Then
    cmdManualMarkAllBilled.Visible = True
Else
    cmdManualMarkAllBilled.Visible = False
End If

End Sub

Private Sub Form_Resize()

On Error Resume Next

grdItems.Width = Me.ScaleWidth - grdItems.Left - 120
grdItems.Height = (Me.ScaleHeight - grdItems.Top - frmButtons.Height) * 0.7 - 240
frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120
grdComments.Top = grdItems.Top + grdItems.Height + 120
grdComments.Height = frmButtons.Top - grdComments.Top - 120
grdInternalComments.Width = grdItems.Width - grdComments.Width - 120
grdInternalComments.Left = grdComments.Left + grdComments.Width + 120
grdInternalComments.Top = grdItems.Top + grdItems.Height + 120
grdInternalComments.Height = frmButtons.Top - grdInternalComments.Top - 120

End Sub

Private Sub grdComments_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdComments_AfterUpdate(RtnDispErrMsg As Integer)

If Val(grdComments.Columns("Email - Clock?").Text) <> 0 Or Val(grdComments.Columns("Email - B&T?").Text) <> 0 _
    Or Val(grdComments.Columns("Email - T/C?").Text) <> 0 Or Val(grdComments.Columns("Email - Black at End?").Text) <> 0 Then
    
    grdComments_BtnClick
End If

End Sub

Private Sub grdComments_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdComments_BeforeUpdate(Cancel As Integer)

If m_blnDelete = True Then Exit Sub

If grdComments.Columns("comment").Text = "" Then
    MsgBox "Cannot Save a Comment with no actual comment", vbCritical, "Comment Not Saved"
    Cancel = True
    Exit Sub
End If

grdComments.Columns("tracker_dadc_itemID").Text = lblTrackeritemID.Caption
If grdComments.Columns("cdate").Text = "" Then
    grdComments.Columns("cdate").Text = Now
End If
grdComments.Columns("cuser").Text = g_strFullUserName

End Sub

Private Sub grdComments_BtnClick()

Dim l_strOurEmailContact As String, l_strOurContactName As String, l_strEmailBody As String

Dim l_rstWhoToEmail As ADODB.Recordset

If MsgBox("Send Email?", vbYesNo, "Automatic Email") = vbYes Then
    
    l_strEmailBody = "A comment was created " & _
        "By: " & grdComments.Columns("cuser").Text & vbCrLf & _
        "Content Version Code: " & grdItems.Columns("contentversioncode").Text & ", Workflow ID: " & grdItems.Columns("workflowID").Text & ", Tape: " & grdItems.Columns("barcode").Text & vbCrLf & _
        "Title: " & grdItems.Columns("title").Text & ", Episode: " & grdItems.Columns("episode").Text & ", Episode Title: " & grdItems.Columns("subtitle").Text & vbCrLf & _
        "Comment: " & grdComments.Columns("comment").Text & vbCrLf
    
    If Val(grdComments.Columns("Email - Clock?").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "MX1 will replace Clock." & vbCrLf
    If Val(grdComments.Columns("Email - B&T?").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "MX1 will replace Bars and Tone." & vbCrLf
    If Val(grdComments.Columns("Email - T/C?").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "MX1 will re-stripe the Timecode." & vbCrLf
    If Val(grdComments.Columns("Email - Black at End?").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "MX1 will Transcode to ProRes." & vbCrLf
    If Val(grdComments.Columns("Email - Textless Not Rq").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "AS-11 File." & vbCrLf & "MX1 will confirm Headbuild is correct." & vbCrLf
    If Val(grdComments.Columns("emailtracklayoutwrong").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "MX1 will Fix Track Layout." & vbCrLf
    If Val(grdComments.Columns("emailblackonwhitetexstless").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "MX1 will remove duplicated Audio Tracks." & vbCrLf
    If Val(grdComments.Columns("emailirconflict").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "Items are present on the master that were not requested. Please Update the Request." & vbCrLf
    
    Debug.Print l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND trackermessageID = 13;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
    
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "DADC Tracker Comment Created", "", l_strEmailBody, True, "", ""
            l_rstWhoToEmail.MoveNext
        
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
    
End If

End Sub

Private Sub grdInternalComments_AfterDelete(RtnDispErrMsg As Integer)

m_blnDelete = False

End Sub

Private Sub grdInternalComments_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

m_blnDelete = True

End Sub

Private Sub grdInternalComments_BeforeUpdate(Cancel As Integer)

If m_blnDelete = False Then
    If grdInternalComments.Columns("comment").Text = "" Then
        MsgBox "Cannot Save a Comment with no actual comment", vbCritical, "Comment Not Saved"
        Cancel = True
        Exit Sub
    End If
    
    
    grdInternalComments.Columns("internalcomment").Text = True
    grdInternalComments.Columns("tracker_DADC_itemID").Text = lblTrackeritemID.Caption
    If grdInternalComments.Columns("cdate").Text = "" Then
        grdInternalComments.Columns("cdate").Text = Now
    End If
    grdInternalComments.Columns("cuser").Text = g_strFullUserName
    
End If

End Sub

Private Sub grdItems_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdItems_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
If Not CheckAccess("/DeleteDADCTrackerLines") Then
    Cancel = 1
    Exit Sub
End If
m_blnDelete = True
End Sub

Private Sub grdItems_BeforeUpdate(Cancel As Integer)

If m_blnDelete = True Then Exit Sub

Dim temp As Boolean, l_strDuration As String, l_lngRunningTime As Long, l_curFileSize As Currency, l_strFilename As String, l_rst As ADODB.Recordset
Dim l_lngClipID As Long, l_lngLibraryID As Long, l_strAltLocation As String, l_strNetworkPath As String, l_strSQL As String, Count As Long, l_datNewTargetDate As Date

Dim l_datRejectDateNow As Date
Dim l_datRejectDateCur As Date
Dim l_datMostRecentdDateCur As Date

If grdItems.Columns("cdate").Text = "" Then
    grdItems.Columns("cdate").Text = Now
End If

grdItems.Columns("mdate").Text = Now

temp = False

'Check ReadytoBill
If grdItems.Columns("stagefield13").Text <> "" Then temp = True
If grdItems.Columns("stagefield15").Text <> "" Then temp = True

If temp = True Then
    grdItems.Columns("readytobill").Text = 1
Else
    grdItems.Columns("readytobill").Text = 0
End If

grdItems.Columns("companyID").Text = lblCompanyID.Caption

'Check the duplicate item situation
If Val(grdItems.Columns("duplicateitem").Text) <> 0 Then
    If Val(Trim(" " & adoItems.Recordset("duplicateitem"))) = 0 Then
        grdItems.Columns("rejected").Text = -1
        If Val(lblCompanyID.Caption) = 1261 Then
            ExecuteSQL "INSERT INTO tracker_dadc_comment (tracker_dadc_itemID, cdate, cuser, comment) VALUES (" & grdItems.Columns("tracker_dadc_itemID").Text & ", '" & FormatSQLDate(Now) & "', '" & g_strFullUserName & "', 'Item Duplicated in Bulk Tracker');", g_strExecuteError
            CheckForSQLError
        Else
            ExecuteSQL "INSERT INTO tracker_dadc_comment (tracker_dadc_itemID, cdate, cuser, comment) VALUES (" & grdItems.Columns("tracker_dadc_itemID").Text & ", '" & FormatSQLDate(Now) & "', '" & g_strFullUserName & "', 'Item Duplicated in Manual Ingest Tracker');", g_strExecuteError
            CheckForSQLError
        End If
    End If
End If

'Check the Pending situation

If Val(grdItems.Columns("rejected").Text) <> 0 And (IsNull(grdItems.Columns("Pendingdate").Text) Or grdItems.Columns("Pendingdate").Text = "") Then
    l_datRejectDateNow = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("Pendingdate").Text = Format(l_datRejectDateNow, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("MostRecentPendingDate").Text = Format(l_datRejectDateNow, "yyyy-mm-dd hh:nn:ss")
      
ElseIf Val(grdItems.Columns("rejected").Text) <> 0 And grdItems.Columns("pendingdate").Text <> "" Then
     
    grdItems.Columns("MostRecentPendingDate").Text = Format(Now, "yyyy-mm-dd hh:nn:ss")
    
ElseIf Val(grdItems.Columns("rejected").Text) = 0 And adoItems.Recordset("rejected") <> 0 Then
    l_datRejectDateNow = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("offPendingDate").Text = l_datRejectDateNow

    l_datRejectDateCur = FormatDateTime(grdItems.Columns("Pendingdate").Text, vbShortDate)
    l_datMostRecentdDateCur = FormatDateTime(grdItems.Columns("MostRecentPendingDate").Text, vbShortDate)

    Set l_rst = ExecuteSQL("exec fn_getWorkingDaysForRange @fromdate='" & Format(grdItems.Columns("MostRecentPendingDate").Text, "YYYY-MM-DD hh:nn:ss") & "', @todate='" & Format(grdItems.Columns("offpendingDate").Text, "YYYY-MM-DD hh:nn:ss") & "'", g_strExecuteError)
    Count = l_rst(0)
    l_rst.Close
    
    grdItems.Columns("daysonpending").Text = Val(grdItems.Columns("daysonpending").Text) + Count
    
'    If IsItHere(GetData("library", "location", "barcode", grdItems.Columns("newbarcode").Text)) = False Then
'        MsgBox "You removed this item from pending, " & vbCrLf & "but there is not a tape on the premises." & vbCrLf & "It therefore cannot be put in the XML bin." & vbCrLf & _
'        "You are responsible for making sure this item is progressed somehow!", vbCritical
'    End If
End If

'Check the Decisiontree situation
l_datRejectDateNow = Format(Now, "yyyy-mm-dd hh:nn:ss")
If Val(grdItems.Columns("decisiontree").Text) <> 0 And (IsNull(grdItems.Columns("decisiontreedate").Text) Or grdItems.Columns("decisiontreedate").Text = "") Then
    
    grdItems.Columns("decisiontreedate").Text = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("MostRecentDecisionTreeDate").Text = Format(l_datRejectDateNow, "yyyy-mm-dd hh:nn:ss")
    If grdItems.Columns("priority").Text <> 0 Then
        Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(l_datRejectDateNow, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=1", g_strExecuteError)
    Else
        Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(l_datRejectDateNow, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=2", g_strExecuteError)
    End If
    l_datNewTargetDate = l_rst(0)
    l_rst.Close
    grdItems.Columns("DTTargetDate").Text = l_datNewTargetDate

ElseIf Val(grdItems.Columns("decisiontree").Text) <> 0 And adoItems.Recordset("decisiontree") = 0 Then
     
    grdItems.Columns("MostRecentDecisionTreeDate").Text = l_datRejectDateNow
    If grdItems.Columns("priority").Text <> 0 Then
        Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(l_datRejectDateNow, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=1", g_strExecuteError)
    Else
        Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(l_datRejectDateNow, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=2", g_strExecuteError)
    End If
    l_datNewTargetDate = l_rst(0)
    l_rst.Close
    grdItems.Columns("DTTargetDate").Text = l_datNewTargetDate
    
ElseIf Val(grdItems.Columns("decisiontree").Text) = 0 And adoItems.Recordset("decisiontree") <> 0 Then
    grdItems.Columns("OffDecisionTreeDate").Text = l_datRejectDateNow

    l_datRejectDateCur = FormatDateTime(grdItems.Columns("decisiontreedate").Text, vbShortDate)
    l_datMostRecentdDateCur = FormatDateTime(grdItems.Columns("MostRecentdecisiontreeDate").Text, vbShortDate)
    
    Set l_rst = ExecuteSQL("exec fn_getWorkingDaysForRange @fromdate='" & Format(grdItems.Columns("MostRecentdecisiontreeDate").Text, "YYYY-MM-DD hh:nn:ss") & "', @todate='" & Format(grdItems.Columns("OffDecisionTreeDate").Text, "YYYY-MM-DD hh:nn:ss") & "'", g_strExecuteError)
    Count = l_rst(0)
    l_rst.Close
    
    grdItems.Columns("daysondecisiontree").Text = Val(grdItems.Columns("daysondecisiontree").Text) + Count

    If IsItHere(GetData("library", "location", "barcode", grdItems.Columns("newbarcode").Text)) = False Then
        MsgBox "You removed this item from Decision Tree, " & vbCrLf & "but there is not a tape on the premises." & vbCrLf & "It therefore cannot be put in the XML bin." & vbCrLf & _
        "You are responsible for making sure this item is progressed somehow!", vbCritical
    End If

End If

'Update the tape details (particularly the location) if possible

If grdItems.Columns("newbarcode").Text <> "" Then
    If GetData("library", "format", "barcode", grdItems.Columns("newbarcode").Text) <> "" Then grdItems.Columns("format").Text = GetData("library", "format", "barcode", grdItems.Columns("newbarcode").Text)
    grdItems.Columns("tapelocation").Text = GetData("library", "location", "barcode", grdItems.Columns("newbarcode").Text)
    If UCase(grdItems.Columns("tapelocation").Text) = "LIBRARY" Or UCase(grdItems.Columns("tapelocation").Text) = "OPERATIONS" Or UCase(grdItems.Columns("tapelocation").Text) = "OFF SITE" Then
'    Or UCase(grdItems.Columns("tapelocation").Text) = "SONY DADC HOLDING" Or UCase(grdItems.Columns("tapelocation").Text) = "SONY DADC INGEST SHELF" _
'    Or UCase(grdItems.Columns("tapelocation").Text) = "SONY DADC XML" Or UCase(grdItems.Columns("tapelocation").Text) = "MANUAL TROLLEY" Or UCase(grdItems.Columns("tapelocation").Text) = "FRONT DESKS" Then
        grdItems.Columns("tapeshelf").Text = GetData("library", "shelf", "barcode", grdItems.Columns("newbarcode").Text)
    Else
        grdItems.Columns("tapeshelf").Text = ""
    End If
    If Left(grdItems.Columns("newbarcode").Text, 3) = "EVR" Then grdItems.Columns("format").Text = "PRORES"
End If

If grdItems.Columns("itemreference").Text <> "" Then
    grdItems.Columns("storagebarcode").Text = GetData("library", "barcode", "libraryID", GetDataSQL("SELECT TOP 1 libraryID FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND clipformat = 'Quicktime' AND (clipcodec = 'ProRes HQ' OR clipaudiocodec = 'LPCM 24bit 48kHz') AND system_deleted = 0;"))
    grdItems.Columns("storagefolder").Text = GetDataSQL("SELECT TOP 1 altlocation FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND clipformat = 'Quicktime' AND (clipcodec = 'ProRes HQ' OR clipaudiocodec = 'LPCM 24bit 48kHz') AND system_deleted = 0;")
    'see if there is a clip record to get the duration from reference
    l_strDuration = GetDataSQL("SELECT TOP 1 fd_length FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Quicktime' AND (clipcodec Like 'ProRes%' OR clipaudiocodec = 'LPCM 24bit 48kHz') AND system_deleted = 0;")
    If l_strDuration = "" Then
        l_strDuration = GetDataSQL("SELECT TOP 1 fd_length FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Quicktime' AND (clipcodec Like 'ProRes%' OR clipaudiocodec = 'LPCM 24bit 48kHz') AND system_deleted <> 0;")
    End If
    grdItems.Columns("timecodeduration").Text = l_strDuration
    If l_strDuration <> "" Then
        l_lngRunningTime = 60 * Val(Mid(l_strDuration, 1, 2))
        l_lngRunningTime = l_lngRunningTime + Val(Mid(l_strDuration, 4, 2))
        If Val(Mid(l_strDuration, 7, 2)) > 30 Then
            l_lngRunningTime = l_lngRunningTime + 1
        End If
        If l_lngRunningTime = 0 Then l_lngRunningTime = 1
        If (grdItems.Columns("duration").Text = "") Or (grdItems.Columns("duration").Text <> "" And chkLockDur.Value = 0) Then
            grdItems.Columns("duration").Text = l_lngRunningTime
        End If
    End If
    
    'See if there is a size to go in the GB sent column
    l_curFileSize = 0
    l_lngClipID = 0
    l_lngLibraryID = 0
    l_strAltLocation = ""
    Set l_rst = ExecuteSQL("SELECT eventID, libraryID, altlocation, bigfilesize FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Quicktime' AND (clipcodec Like 'ProRes%' OR clipaudiocodec = 'LPCM 24bit 48kHz') AND system_deleted = 0;", g_strExecuteError)
    CheckForSQLError
    If l_rst.RecordCount > 0 Then
        l_rst.MoveFirst
        Do While Not l_rst.EOF
            l_lngClipID = l_rst("eventID")
            l_lngLibraryID = l_rst("libraryID")
            l_strAltLocation = l_rst("altlocation")
            If Not IsNull(l_rst("bigfilesize")) Then
                If l_rst("bigfilesize") > l_curFileSize Then l_curFileSize = l_rst("bigfilesize")
            End If
            l_rst.MoveNext
        Loop
    ElseIf l_rst.RecordCount = 0 Then
        l_rst.Close
        Set l_rst = ExecuteSQL("SELECT eventID, libraryID, altlocation, bigfilesize FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Quicktime' AND (clipcodec Like 'ProRes%' OR clipaudiocodec = 'LPCM 24bit 48kHz') AND system_deleted <> 0;", g_strExecuteError)
        CheckForSQLError
        If l_rst.RecordCount > 0 Then
            l_rst.MoveFirst
            Do While Not l_rst.EOF
                l_lngClipID = l_rst("eventID")
                l_lngLibraryID = l_rst("libraryID")
                l_strAltLocation = l_rst("altlocation")
                If Not IsNull(l_rst("bigfilesize")) Then
                    If l_rst("bigfilesize") > l_curFileSize Then l_curFileSize = l_rst("bigfilesize")
                End If
                l_rst.MoveNext
            Loop
        End If
    End If
    l_rst.Close
    
    If l_curFileSize <> 0 Then
        If (grdItems.Columns("gbsent").Text = "") Or (grdItems.Columns("gbsent").Text <> "" And chkLockGB.Value = 0) Then
            grdItems.Columns("gbsent").Text = Int(l_curFileSize / 1024 / 1024 / 1024 + 0.999)
        End If
    End If
    
    'See if there is a filename to go in the filename column
    l_strFilename = GetDataSQL("SELECT TOP 1 clipfilename FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Quicktime' AND (clipcodec Like 'ProRes%' OR clipaudiocodec = 'LPCM 24bit 48kHz') AND system_deleted = 0;")
    If l_strFilename = "" Then
        l_strFilename = GetDataSQL("SELECT TOP 1 clipfilename FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Quicktime' AND (clipcodec Like 'ProRes%' OR clipaudiocodec = 'LPCM 24bit 48kHz') AND system_deleted <> 0;")
    End If
    If l_strFilename <> "" Then
        grdItems.Columns("itemfilename").Text = l_strFilename
    End If

    'See if there is a proxy ID to go in a ProxyID column
    Set l_rst = ExecuteSQL("SELECT eventID FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Flash MP4' AND cliphorizontalpixels = 640 and libraryID = 276356 AND system_deleted = 0;", g_strExecuteError)
    CheckForSQLError
    If l_rst.RecordCount > 0 Then
        l_rst.MoveFirst
        grdItems.Columns("proxyID").Text = l_rst("eventID")
    End If
    l_rst.Close
    Set l_rst = Nothing
    
End If

If m_blnBilling = False Then
    If chkDontVerifyXML.Value = 0 Then
        m_blnDontVerifyXML = False
    Else
        m_blnDontVerifyXML = True
    End If
End If

If grdItems.Columns("stagefield3").Text <> "" And m_blnDontVerifyXML = False Then
'XML Made is present, so we ned to verify existence of an xml file
    l_lngClipID = GetDataSQL("SELECT TOP 1 eventID FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Quicktime' AND system_deleted = 0;")
    If l_lngClipID <> 0 Then

        If GetData("library", "format", "libraryID", GetData("events", "libraryID", "eventID", l_lngClipID)) = "DISCSTORE" Then
        
            l_strNetworkPath = GetData("library", "subtitle", "libraryID", GetData("events", "libraryID", "eventID", l_lngClipID))
            l_strNetworkPath = l_strNetworkPath & "\" & Trim(" " & GetData("events", "altlocation", "eventID", l_lngClipID))
            l_strNetworkPath = l_strNetworkPath & "\" & grdItems.Columns("itemreference").Text & ".xml"
            
            'Verify existence of file.
            On Error GoTo VERIFYFAIL
            If UCase(Dir(l_strNetworkPath)) = UCase(grdItems.Columns("itemreference").Text & ".xml") Then
            
                'Do Nothing
                On Error GoTo 0
            
            Else
VERIFYFAIL:
                On Error GoTo 0
                If MsgBox("You have said that the XML is made, but no XML file appears to be present with the main clip" & vbCrLf & "Are you sure", vbYesNo, "XML Verification") = vbNo Then
                    grdItems.Columns("stagefield3").Text = ""
                End If
                
            End If
        Else
        
            MsgBox "You have said that the XML is made, but the file is not stored on a DISCSTORE, so this cannot be checked.", vbCritical, "XML Verification"
'            grdItems.Columns("stagefield3").Text = ""
            
        End If
        
    Else
        
        MsgBox "You have said that the XML is made, but there appears to be no main clip for this item, so how can it be?", vbCritical, "XML Verification"
        grdItems.Columns("stagefield3").Text = ""
    
    End If
    
End If

'Check the Complaint situation
If Val(grdItems.Columns("complainedabout").Text) <> 0 And adoItems.Recordset("complainedabout") = 0 Then
    l_strSQL = "INSERT INTO complaint (companyID, contactID, complainttype, originalDADCtrackerID, MasterSourceID, cdate, cuser, mdate, muser) VALUES (" & lblCompanyID.Caption & ", 2840, 1, " & grdItems.Columns("tracker_dadc_itemID").Text & ", '" & QuoteSanitise(grdItems.Columns("tracker_dadc_itemID").Text) & "', getdate(), '" & g_strFullUserName & "', getdate(), '" & g_strFullUserName & "');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
End If

'Check the Rush Job hasn't been changed and adjust target dates if it is.
If grdItems.Columns("priority").Text <> 0 And adoItems.Recordset("priority") = 0 Then
    If Val(grdItems.Columns("targetdatecountdown").Text) > 1 Then
        Count = 1 - Val(grdItems.Columns("targetdatecountdown").Text)
        grdItems.Columns("targetdatecountdown").Text = Val(grdItems.Columns("targetdatecountdown").Text) + Count
        If grdItems.Columns("TargetDate").Text <> "" Then
            Set l_rst = ExecuteSQL("exec fn_getWorkingDaysBackFromDate @fromdate='" & Format(grdItems.Columns("TargetDate").Text, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & -Count, g_strExecuteError)
            l_datNewTargetDate = l_rst(0)
            l_rst.Close
            grdItems.Columns("TargetDate").Text = l_datNewTargetDate
        End If
    End If
    If grdItems.Columns("decisiontree").Text <> 0 Then
        Set l_rst = ExecuteSQL("exec fn_getWorkingDaysBackFromDate @fromdate='" & Format(grdItems.Columns("DTTargetDate").Text, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=1", g_strExecuteError)
        l_datNewTargetDate = l_rst(0)
        l_rst.Close
        grdItems.Columns("DTTargetDate").Text = l_datNewTargetDate
    End If
ElseIf grdItems.Columns("priority").Text = 0 And adoItems.Recordset("priority") <> 0 Then
    grdItems.Columns("targetdatecountdown").Text = Val(grdItems.Columns("targetdatecountdown").Text) + 2
    If Val(grdItems.Columns("targetdatecountdown").Text) > 3 Then grdItems.Columns("targetdatecountdown").Text = 3
    If grdItems.Columns("TargetDate").Text <> "" Then
        Count = Val(grdItems.Columns("targetdatecountdown").Text) - Val(adoItems.Recordset("targetdatecountdown"))
        Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(grdItems.Columns("TargetDate").Text, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Count, g_strExecuteError)
        l_datNewTargetDate = l_rst(0)
        l_rst.Close
        grdItems.Columns("TargetDate").Text = l_datNewTargetDate
    End If
    If grdItems.Columns("decisiontree").Text <> 0 Then
        Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(grdItems.Columns("DTTargetDate").Text, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=1", g_strExecuteError)
        l_datNewTargetDate = l_rst(0)
        l_rst.Close
        grdItems.Columns("DTTargetDate").Text = l_datNewTargetDate
    End If
End If

'Check the Target Date situation
If grdItems.Columns("stagefield1").Text <> "" And Val(grdItems.Columns("rejected").Text) = 0 And Val(grdItems.Columns("decisiontree").Text) = 0 And grdItems.Columns("readytobill").Text = 0 Then

    Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Val(grdItems.Columns("targetdatecountdown").Text), g_strExecuteError)
    l_datNewTargetDate = l_rst(0)
    l_rst.Close
    If Trim(" " & grdItems.Columns("Completeable").Text) = "" Then grdItems.Columns("Completeable").Text = Now()
    If Trim(" " & grdItems.Columns("firstcompleteable").Text) = "" Then grdItems.Columns("firstCompleteable").Text = grdItems.Columns("Completeable").Text
    If Trim(" " & grdItems.Columns("TargetDate").Text) = "" Then grdItems.Columns("TargetDate").Text = l_datNewTargetDate

ElseIf grdItems.Columns("readytobill").Text = 0 Then

    grdItems.Columns("Completeable").Text = ""
    grdItems.Columns("TargetDate").Text = ""
        
End If

If m_blnBilling = False Then m_blnDontVerifyXML = False
'grdItems.Columns("duedateupload").Text = FormatSQLDate(grdItems.Columns("duedateupload").Text)
    
End Sub

Private Sub grdItems_BtnClick()

Dim tempdate As String

Select Case LCase(grdItems.Columns(grdItems.Col).Name)

Case "barcode", "newbarcode"
    
    MakeClipFromDADCTrackerEvent grdItems.Columns("tracker_dadc_itemID").Text

Case "duedateupload"
    
    If grdItems.ActiveCell.Text <> "" Then
        grdItems.ActiveCell.Text = ""
    Else
        grdItems.ActiveCell.Text = Format(Now, "yyyy-mm-dd")
    End If
    
Case "pendingdate"

    If grdItems.ActiveCell.Text <> "" Then
        grdItems.ActiveCell.Text = ""
    Else
        grdItems.ActiveCell.Text = Format(Now, "yyyy-mm-dd hh:nn:ss")
    End If

Case Else
    
    If grdItems.ActiveCell.Text <> "" Then
        grdItems.ActiveCell.Text = ""
    Else
        If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
            grdItems.ActiveCell.Text = Now
        Else
            tempdate = FormatDateTime(Now, vbLongDate)
            grdItems.ActiveCell.Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
        End If
    End If

End Select

End Sub

Private Sub grdItems_DblClick()

Select Case LCase(grdItems.Columns(grdItems.Col).Name)

Case "itemfilename"
    ShowClipSearch "", "", grdItems.Columns("itemfilename").Text

Case Else
    ShowClipSearch "", grdItems.Columns("itemreference").Text
    
End Select

End Sub

Private Sub grdItems_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim l_strSQL As String

lblTrackeritemID.Caption = grdItems.Columns("tracker_dadc_itemID").Text

If GetCETASetting("DADCUpdateInProgress") <> "1" Then
    lblAudioConfiguration.Caption = Replace(grdItems.Columns("audioconfiguration").Text, "&", "&&")
Else
    lblAudioConfiguration.Caption = "The DADC Tracker is currently being updated - please return later to see this information"
End If

If Val(lblTrackeritemID.Caption) <> Val(lblLastTrackerItemID.Caption) Then
    
    If Val(lblTrackeritemID.Caption) = 0 Then
        cmdBillItem.Visible = False
        
        l_strSQL = "SELECT * FROM tracker_dadc_comment WHERE tracker_dadc_itemID = -1 ORDER BY cdate;"
        
        adoComments.RecordSource = l_strSQL
        adoComments.ConnectionString = g_strConnection
        adoComments.Refresh
        
        lblLastTrackerItemID.Caption = ""
    
        Exit Sub
    
    End If
    
    l_strSQL = "SELECT * FROM tracker_dadc_comment WHERE tracker_dadc_itemID = " & lblTrackeritemID.Caption & " AND internalcomment = 0 ORDER BY cdate;"
    
    adoComments.RecordSource = l_strSQL
    adoComments.ConnectionString = g_strConnection
    adoComments.Refresh

    l_strSQL = "SELECT * FROM tracker_dadc_comment WHERE tracker_dadc_itemID = " & lblTrackeritemID.Caption & " AND internalcomment <> 0 ORDER BY cdate;"
    
    adoInternalComments.RecordSource = l_strSQL
    adoInternalComments.ConnectionString = g_strConnection
    adoInternalComments.Refresh

    If grdItems.Columns("readytobill").Text <> 0 And grdItems.Columns("billed").Text = 0 And grdItems.Columns("complaintredoitem").Text = 0 Then
        cmdBillItem.Visible = True
        cmdManualBillItem.Visible = True
    ElseIf grdItems.Columns("complaintredoitem").Text <> 0 Then
        cmdBillItem.Visible = False
        cmdManualBillItem.Visible = True
    Else
        cmdBillItem.Visible = False
        cmdManualBillItem.Visible = False
    End If
    
    If Val(grdItems.Columns("decisiontree").Text) = 0 And grdItems.Columns("decisiontreedate").Text <> "" And grdItems.Columns("offdecisiontreedate").Text <> "" And Val(grdItems.Columns("dtbilled").Text) = 0 Then
        cmdDTBillItem.Visible = True
        cmdManualDTBillItem.Visible = True
        cmdUnbillDT.Visible = False
    ElseIf Val(grdItems.Columns("decisiontree").Text) = 0 And grdItems.Columns("decisiontreedate").Text <> "" And grdItems.Columns("offdecisiontreedate").Text <> "" And Val(grdItems.Columns("dtbilled").Text) <> 0 Then
        cmdDTBillItem.Visible = False
        cmdManualDTBillItem.Visible = False
        cmdUnbillDT.Visible = True
    Else
        cmdDTBillItem.Visible = False
        cmdManualDTBillItem.Visible = False
        cmdUnbillDT.Visible = False
    End If
    
    If grdItems.Columns("stagefield5").Text <> "" And grdItems.Columns("As11billed").Text = 0 Then
        cmdAS11BillItem.Visible = True
        cmdManualBillAS11Item.Visible = True
        cmdUnbillAS11.Visible = False
    ElseIf grdItems.Columns("stagefield5").Text <> "" And grdItems.Columns("As11billed").Text <> 0 Then
        cmdAS11BillItem.Visible = False
        cmdManualBillAS11Item.Visible = False
        cmdUnbillAS11.Visible = True
    Else
        cmdAS11BillItem.Visible = False
        cmdManualBillAS11Item.Visible = False
        cmdUnbillAS11.Visible = False
    End If
    
    If grdItems.Columns("billed").Text <> 0 Then
        cmdUnbill.Visible = True
    Else
        cmdUnbill.Visible = False
    End If

    lblLastTrackerItemID.Caption = lblTrackeritemID.Caption
    
End If

End Sub

Private Sub grdItems_RowLoaded(ByVal Bookmark As Variant)

Dim l_datTargetDate As Date

If grdItems.Columns("externalalphaID").Text <> "" And grdItems.Columns("externalalphaID").Text <> Left(grdItems.Columns("itemreference").Text, 11) Then
    grdItems.Columns("rejected").CellStyleSet "Error"
End If

If Val(grdItems.Columns("rejected").Text) <> 0 Then
    grdItems.Columns("rejected").CellStyleSet "Error"
    grdItems.Columns("readytobill").CellStyleSet "Error"
    grdItems.Columns("billed").CellStyleSet "Error"
    grdItems.Columns("fixed").CellStyleSet "Error"
End If

If Val(grdItems.Columns("priority").Text) <> 0 Then grdItems.Columns("priority").CellStyleSet "Error"

If Val(grdItems.Columns("fixed").Text) <> 0 Then
    grdItems.Columns("rejected").CellStyleSet "Fixed"
    grdItems.Columns("readytobill").CellStyleSet "Fixed"
    grdItems.Columns("billed").CellStyleSet "Fixed"
    grdItems.Columns("fixed").CellStyleSet "Fixed"
End If

If Val(grdItems.Columns("decisiontree").Text) <> 0 Then
    grdItems.Columns("decisiontree").CellStyleSet "Fixed"
    grdItems.Columns("decisiontreedate").CellStyleSet "Fixed"
    If IsDate(grdItems.Columns("DTTargetDate").Text) Then l_datTargetDate = grdItems.Columns("DTTargetDate").Text
    If l_datTargetDate < Now Then
        grdItems.Columns("DTTargetDate").CellStyleSet "Error"
    Else
        grdItems.Columns("DTTargetDate").CellStyleSet "Fixed"
    End If
End If

If Val(grdItems.Columns("duration").Text) > 90 Then
    grdItems.Columns("duration").CellStyleSet "Notify"
    grdItems.Columns("timecodeduration").CellStyleSet "Notify"
End If

If Val(grdItems.Columns("complainedabout").Text) <> 0 Then
    grdItems.Columns("complainedabout").Locked = True
Else
    grdItems.Columns("complainedabout").Locked = False
End If

If Val(grdItems.Columns("complaintredoitem").Text) <> 0 Then
    grdItems.Columns("complaintredoitem").Locked = True
Else
    grdItems.Columns("complaintredoitem").Locked = False
End If

If grdItems.Columns("itemreference").Text <> "" Then
    grdItems.Columns("Stored On").Text = GetData("library", "barcode", "libraryID", GetDataSQL("SELECT TOP 1 events.libraryID FROM events INNER JOIN library ON events.libraryID = library.libraryID WHERE events.clipreference = '" & grdItems.Columns("itemreference").Text & "' AND events.clipformat = 'Quicktime' AND events.clipcodec = 'ProRes HQ' AND (library.format = 'DISCSTORE' OR library.format = 'LTO5' OR library.format = 'MOBILEDISC') and events.system_deleted = 0;"))
End If

'adoComments.RecordSource = "SELECT * FROM tracker_dadc_comment WHERE tracker_dadc_itemID = " & lblTrackeritemID.Caption & " ORDER BY cdate;"
'adoComments.ConnectionString = g_strConnection
'adoComments.Refresh
'
End Sub

Private Sub optComplete_Click(Index As Integer)

cmdSearch.Value = True

End Sub

Private Sub optOrderBy_Click(Index As Integer)

If optOrderBy(0).Value = True Then
    m_strOrderby = " ORDER BY storagebarcode, itemreference, title, episode, subtitle"
ElseIf optOrderBy(1).Value = True Then
    m_strOrderby = " ORDER BY dbbcrid, itemreference, title, episode, subtitle"
ElseIf optOrderBy(2).Value = True Then
    m_strOrderby = " ORDER BY CASE WHEN priority <> 0 THEN 0 ELSE 1 END, duedateupload, itemreference, title, episode, subtitle"
ElseIf optOrderBy(4).Value = True Then
    m_strOrderby = " ORDER BY CASE WHEN DTTargetDate IS NOT NULL THEN 0 ELSE 1 END, DTTargetDate, itemreference, title, episode, subtitle"
Else
    m_strOrderby = " ORDER BY itemreference, title, episode, subtitle"
End If

cmdSearch.Value = True

End Sub
