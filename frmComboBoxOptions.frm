VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmComboBoxOptions 
   Caption         =   "Combo Box Options"
   ClientHeight    =   5310
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   15465
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmComboBoxOptions.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5310
   ScaleWidth      =   15465
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdAddNewCategory 
      Caption         =   "Add New Category"
      Height          =   315
      Left            =   11880
      TabIndex        =   8
      Top             =   120
      Width           =   2415
   End
   Begin VB.TextBox txtOrder 
      Height          =   315
      Left            =   8640
      TabIndex        =   6
      Top             =   120
      Width           =   2115
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "Search"
      Height          =   315
      Left            =   9840
      TabIndex        =   5
      ToolTipText     =   "Close this form"
      Top             =   4920
      Visible         =   0   'False
      Width           =   1155
   End
   Begin VB.CommandButton cmdPrintShelfLabel 
      Caption         =   "Print Shelf Label"
      Height          =   315
      Left            =   4620
      TabIndex        =   4
      Top             =   120
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   11100
      TabIndex        =   2
      ToolTipText     =   "Close this form"
      Top             =   4920
      Width           =   1155
   End
   Begin VB.ComboBox cmbComboBox 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1260
      Sorted          =   -1  'True
      TabIndex        =   0
      ToolTipText     =   "Which Options to Edit"
      Top             =   120
      Width           =   3135
   End
   Begin MSAdodcLib.Adodc adoComboBoxOptions 
      Height          =   330
      Left            =   60
      Top             =   540
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   2
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoComboBoxOptions"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdComboBoxOptions 
      Bindings        =   "frmComboBoxOptions.frx":08CA
      Height          =   4275
      Left            =   60
      TabIndex        =   1
      Top             =   540
      Width           =   14955
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      MultiLine       =   0   'False
      SelectTypeRow   =   3
      BackColorOdd    =   16777152
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   7
      Columns(0).Width=   11456
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "description"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   255
      Columns(1).Width=   3200
      Columns(1).Caption=   "Alias"
      Columns(1).Name =   "descriptionalias"
      Columns(1).DataField=   "descriptionalias"
      Columns(1).FieldLen=   256
      Columns(2).Width=   11456
      Columns(2).Caption=   "Additional Information"
      Columns(2).Name =   "information"
      Columns(2).DataField=   "information"
      Columns(2).FieldLen=   255
      Columns(3).Width=   3175
      Columns(3).Caption=   "Format"
      Columns(3).Name =   "format"
      Columns(3).DataField=   "format"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3175
      Columns(4).Caption=   "Standard"
      Columns(4).Name =   "videostandard"
      Columns(4).DataField=   "videostandard"
      Columns(4).FieldLen=   256
      Columns(5).Width=   1376
      Columns(5).Caption=   "Order Position"
      Columns(5).Name =   "forder"
      Columns(5).DataField=   "forder"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "category"
      Columns(6).Name =   "category"
      Columns(6).DataField=   "category"
      Columns(6).FieldLen=   256
      _ExtentX        =   26379
      _ExtentY        =   7541
      _StockProps     =   79
      Caption         =   "Combo Box Options"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order"
      Height          =   255
      Index           =   0
      Left            =   8040
      TabIndex        =   7
      Top             =   180
      Width           =   555
   End
   Begin VB.Label lblCaption 
      Caption         =   "Combo Box"
      Height          =   255
      Index           =   19
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   1035
   End
End
Attribute VB_Name = "frmComboBoxOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmbComboBox_Click()

Dim l_strSQL As String

If txtOrder.Text = "" Then
    l_strSQL = "SELECT * FROM xref WHERE category = '" & cmbComboBox.Text & "' ORDER BY forder, description"
Else
    l_strSQL = "SELECT * FROM xref WHERE category = '" & cmbComboBox.Text & "' AND forder = '" & txtOrder.Text & "' ORDER BY forder, description"
End If

adoComboBoxOptions.ConnectionString = g_strConnection
adoComboBoxOptions.RecordSource = l_strSQL
adoComboBoxOptions.Refresh

adoComboBoxOptions.Caption = adoComboBoxOptions.Recordset.RecordCount & " item(s)"

grdComboBoxOptions.Columns(0).Caption = "Description"
grdComboBoxOptions.Columns(1).Caption = "Alias"
grdComboBoxOptions.Columns(2).Caption = "Additional Information"
grdComboBoxOptions.Columns(3).Caption = "Format"
grdComboBoxOptions.Columns(4).Caption = "Standard"


Select Case cmbComboBox.Text

Case "event"
    grdComboBoxOptions.Columns(2).Caption = "Start Time Code"
    grdComboBoxOptions.Columns(3).Caption = "End Time Code"

Case "format"
    grdComboBoxOptions.Columns(2).Caption = "Additional Information"
    grdComboBoxOptions.Columns(3).Caption = "Digi/Analog"
    grdComboBoxOptions.Columns(4).Caption = "No Of Tracks"

Case "stockcode"
    grdComboBoxOptions.Columns(2).Caption = "Duration (In Mins)"
    grdComboBoxOptions.Columns(4).Caption = "EBU Stock Def"
    
Case "workstation"
    grdComboBoxOptions.Columns(1).Caption = "Options Code"
    grdComboBoxOptions.Columns(2).Caption = "Barcode Report To Use"
    
Case "pipelineunit"
    grdComboBoxOptions.Columns(0).Caption = "Pipeline Unit Name"
    grdComboBoxOptions.Columns(2).Caption = "IP Address"
        
Case "TWDC-Title"
    grdComboBoxOptions.Columns(0).Caption = "Name / Programme Title"
    grdComboBoxOptions.Columns(1).Caption = "Dub Card (if different from name)"
    grdComboBoxOptions.Columns(2).Caption = "Arran's Code"
    grdComboBoxOptions.Columns(3).Caption = "Jellyroll"
    grdComboBoxOptions.Columns(4).Caption = "(Not Used)"
    
Case "TWDC-Language"
    grdComboBoxOptions.Columns(0).Caption = "Name"
    grdComboBoxOptions.Columns(1).Caption = "Dub Card"
    grdComboBoxOptions.Columns(2).Caption = "Disney Audio"
    grdComboBoxOptions.Columns(3).Caption = "Jellyroll"
    grdComboBoxOptions.Columns(4).Caption = "Secondary Audio"
    
    
End Select

grdComboBoxOptions.Refresh


cmdPrintShelfLabel.Visible = cmbComboBox.Text = "shelf"

End Sub

Private Sub cmdAddNewCategory_Click()

Dim l_strSQL As String, l_strCategory As String, l_strTemp As String

l_strCategory = InputBox("Give the new Category")
If l_strCategory <> "" Then
    If MsgBox("New Category: " & l_strCategory & vbCrLf & "Are you sure", vbYesNo + vbDefaultButton2) = vbYes Then
        l_strSQL = "INSERT INTO xref (category) VALUES ('" & l_strCategory & "');"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_strTemp = cmbComboBox.Text
        cmbComboBox.Clear
        PopulateCombo "xreftypes", cmbComboBox
        cmbComboBox.Text = l_strTemp
    End If
End If

End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdPrintShelfLabel_Click()

Dim l_strReportToPrint As String, l_strSQL As String, l_lngID As Long

l_strReportToPrint = g_strLocationOfCrystalReportFiles & "shelfbarcode.rpt"

'load up the labelprint table with the values from the form

l_strSQL = "INSERT INTO labelprinting ("
l_strSQL = l_strSQL & "cuser, cdate, field0, field1) VALUES ("

l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'" & CIA_CODE128(grdComboBoxOptions.Columns("description").Text) & "', "
l_strSQL = l_strSQL & "'" & grdComboBoxOptions.Columns("description").Text & "'"
l_strSQL = l_strSQL & ")"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_lngID = g_lngLastID

'print the report
PrintCrystalReport l_strReportToPrint, "{labelprinting.labelprintingID} =" & Str(l_lngID), g_blnPreviewReport, 1
'get rid of that labelprint record again.

l_strSQL = "DELETE FROM labelprinting WHERE labelprintingID = " & Str(l_lngID)
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Sub

Private Sub cmdSearch_Click()
cmbComboBox_Click
End Sub

Private Sub Form_Load()
MakeLookLikeOffice Me
CenterForm Me
PopulateCombo "xreftypes", cmbComboBox
End Sub

Private Sub Form_Resize()

cmdClose.Top = Me.ScaleHeight - cmdClose.Height - 120
cmdClose.Left = Me.ScaleWidth - cmdClose.Width - 120

If Me.ScaleHeight > cmdClose.Height - grdComboBoxOptions.Top - 240 Then
    grdComboBoxOptions.Height = Me.ScaleHeight - cmdClose.Height - grdComboBoxOptions.Top - 240
    grdComboBoxOptions.Width = Me.ScaleWidth - 120
End If

End Sub

Private Sub grdComboBoxOptions_BeforeUpdate(Cancel As Integer)
grdComboBoxOptions.Columns("category").Text = cmbComboBox.Text
End Sub
