VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTrackerSonypictures 
   Caption         =   "Sony Pictures Tracker"
   ClientHeight    =   17595
   ClientLeft      =   3060
   ClientTop       =   3210
   ClientWidth     =   28725
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   17595
   ScaleWidth      =   28725
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.OptionButton optComplete 
      Caption         =   "Pending"
      ForeColor       =   &H00C00000&
      Height          =   255
      Index           =   1
      Left            =   4440
      TabIndex        =   37
      Tag             =   "NOCLEAR"
      Top             =   420
      Width           =   1395
   End
   Begin VB.TextBox txtTitle 
      Height          =   315
      Left            =   1620
      TabIndex        =   34
      Top             =   420
      Width           =   2595
   End
   Begin VB.TextBox txtAlphaCode 
      Height          =   315
      Left            =   1620
      TabIndex        =   33
      Top             =   60
      Width           =   2595
   End
   Begin VB.TextBox txtJobID 
      Height          =   315
      Left            =   1620
      TabIndex        =   30
      Top             =   1140
      Width           =   2595
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "All Items"
      Height          =   255
      Index           =   4
      Left            =   4440
      TabIndex        =   29
      Tag             =   "NOCLEAR"
      Top             =   1920
      Width           =   1455
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Master Not Here"
      Height          =   255
      Index           =   5
      Left            =   6240
      TabIndex        =   21
      Tag             =   "NOCLEAR"
      Top             =   120
      Width           =   1575
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "XML Made"
      Height          =   255
      Index           =   6
      Left            =   6240
      TabIndex        =   20
      Tag             =   "NOCLEAR"
      Top             =   420
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Billed"
      Height          =   255
      Index           =   3
      Left            =   4440
      TabIndex        =   19
      Tag             =   "NOCLEAR"
      Top             =   1620
      Width           =   1395
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Sent with XML"
      ForeColor       =   &H00C00000&
      Height          =   255
      Index           =   8
      Left            =   6240
      TabIndex        =   18
      Tag             =   "NOCLEAR"
      Top             =   1020
      Width           =   1995
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "XML Not Made"
      Height          =   255
      Index           =   7
      Left            =   6240
      TabIndex        =   17
      Tag             =   "NOCLEAR"
      Top             =   720
      Width           =   1815
   End
   Begin VB.CheckBox chkHideDemo 
      Alignment       =   1  'Right Justify
      Caption         =   "Hide Demo"
      Height          =   255
      Left            =   4560
      TabIndex        =   14
      Tag             =   "NOCLEAR"
      Top             =   2400
      Value           =   1  'Checked
      Width           =   1215
   End
   Begin VB.CheckBox chkLockDur 
      Alignment       =   1  'Right Justify
      Caption         =   "Lock Duration"
      Height          =   255
      Left            =   6480
      TabIndex        =   12
      Top             =   2400
      Width           =   1335
   End
   Begin VB.TextBox txtSubtitle 
      Height          =   315
      Left            =   1620
      TabIndex        =   11
      Top             =   780
      Width           =   2595
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Finished"
      Height          =   255
      Index           =   2
      Left            =   4440
      TabIndex        =   6
      Tag             =   "NOCLEAR"
      Top             =   1320
      Width           =   1395
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete"
      Height          =   255
      Index           =   0
      Left            =   4440
      TabIndex        =   5
      Tag             =   "NOCLEAR"
      Top             =   120
      Value           =   -1  'True
      Width           =   1395
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   4920
      TabIndex        =   1
      Top             =   14880
      Width           =   20775
      Begin VB.CommandButton cmdBillItem 
         Caption         =   "Bill Item"
         Height          =   315
         Left            =   10560
         TabIndex        =   26
         Top             =   0
         Width           =   1755
      End
      Begin VB.CommandButton cmdManualBillItem 
         Caption         =   "Manually Mark as Billed"
         Height          =   315
         Left            =   8160
         TabIndex        =   25
         Top             =   0
         Width           =   2235
      End
      Begin VB.CommandButton cmdUnbill 
         Caption         =   "Unmark as Billed"
         Height          =   315
         Left            =   6180
         TabIndex        =   24
         Top             =   0
         Width           =   1815
      End
      Begin VB.CommandButton cmdBillAll 
         Caption         =   "Bill All"
         Height          =   315
         Left            =   4320
         TabIndex        =   23
         Top             =   0
         Width           =   1695
      End
      Begin VB.CommandButton cmdUnbillAll 
         Caption         =   "Unmark Billing All"
         Height          =   315
         Left            =   2580
         TabIndex        =   22
         Top             =   0
         Width           =   1575
      End
      Begin VB.CommandButton cmdUpdateAll 
         Caption         =   "Update File Information"
         Height          =   315
         Left            =   12420
         TabIndex        =   16
         Top             =   0
         Width           =   2235
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   18480
         TabIndex        =   4
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   17220
         TabIndex        =   3
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   15960
         TabIndex        =   2
         Top             =   0
         Width           =   1215
      End
   End
   Begin MSAdodcLib.Adodc adoItems 
      Height          =   330
      Left            =   60
      Top             =   2700
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdItems 
      Bindings        =   "frmTrackerSonypictures.frx":0000
      Height          =   8715
      Left            =   60
      TabIndex        =   0
      Top             =   2700
      Width           =   28530
      ScrollBars      =   3
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   3
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTrackerSonypictures.frx":0017
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmTrackerSonypictures.frx":0033
      stylesets(2).Name=   "headerfield"
      stylesets(2).ForeColor=   0
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmTrackerSonypictures.frx":004F
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      StyleSet        =   "headerfield"
      RowHeight       =   450
      ExtraHeight     =   26
      Columns.Count   =   25
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "tracker_spe_itemID"
      Columns(0).Name =   "tracker_spe_itemID"
      Columns(0).DataField=   "tracker_spe_itemID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   2699
      Columns(2).Caption=   "Ref"
      Columns(2).Name =   "Clipreference"
      Columns(2).DataField=   "Clipreference"
      Columns(2).FieldLen=   256
      Columns(2).StyleSet=   "headerfield"
      Columns(3).Width=   1561
      Columns(3).Caption=   "Alpha ID"
      Columns(3).Name =   "Alpha_ID"
      Columns(3).DataField=   "Alpha_ID"
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "headerfield"
      Columns(4).Width=   2117
      Columns(4).Caption=   "Barcode"
      Columns(4).Name =   "Barcode"
      Columns(4).DataField=   "Barcode"
      Columns(4).FieldLen=   256
      Columns(4).StyleSet=   "headerfield"
      Columns(5).Width=   3175
      Columns(5).Caption=   "Title"
      Columns(5).Name =   "title"
      Columns(5).DataField=   "title"
      Columns(5).FieldLen=   256
      Columns(5).StyleSet=   "headerfield"
      Columns(6).Width=   3175
      Columns(6).Caption=   "Title Abbrev"
      Columns(6).Name =   "Title_Abbrev"
      Columns(6).DataField=   "Title_Abbrev"
      Columns(6).FieldLen=   256
      Columns(6).StyleSet=   "headerfield"
      Columns(7).Width=   1588
      Columns(7).Caption=   "Language"
      Columns(7).Name =   "language"
      Columns(7).DataField=   "language"
      Columns(7).FieldLen=   256
      Columns(7).StyleSet=   "headerfield"
      Columns(8).Width=   2302
      Columns(8).Caption=   "Aspect Ratio"
      Columns(8).Name =   "AspectRatio"
      Columns(8).DataField=   "AspectRatio"
      Columns(8).FieldLen=   256
      Columns(9).Width=   2117
      Columns(9).Caption=   "Due Date"
      Columns(9).Name =   "Deadline_Date"
      Columns(9).DataField=   "Deadline_Date"
      Columns(9).FieldLen=   256
      Columns(9).Style=   1
      Columns(9).StyleSet=   "headerfield"
      Columns(10).Width=   1085
      Columns(10).Caption=   "Pending"
      Columns(10).Name=   "rejected"
      Columns(10).DataField=   "rejected"
      Columns(10).FieldLen=   256
      Columns(10).Style=   2
      Columns(10).StyleSet=   "headerfield"
      Columns(11).Width=   2117
      Columns(11).Caption=   "Date Arrived"
      Columns(11).Name=   "date_arrived"
      Columns(11).DataField=   "date_arrived"
      Columns(11).FieldLen=   256
      Columns(11).Style=   1
      Columns(11).StyleSet=   "stagefield"
      Columns(12).Width=   2117
      Columns(12).Caption=   "Date Checked"
      Columns(12).Name=   "Date_checked"
      Columns(12).DataField=   "Date_checked"
      Columns(12).FieldLen=   256
      Columns(12).Style=   1
      Columns(12).StyleSet=   "stagefield"
      Columns(13).Width=   2117
      Columns(13).Caption=   "XML Made"
      Columns(13).Name=   "xmlmade"
      Columns(13).DataField=   "Date_Xml_Made"
      Columns(13).FieldLen=   256
      Columns(13).Style=   1
      Columns(13).StyleSet=   "stagefield"
      Columns(14).Width=   2117
      Columns(14).Caption=   "Date Sent"
      Columns(14).Name=   "Date_Sent"
      Columns(14).DataField=   "Date_Sent"
      Columns(14).FieldLen=   256
      Columns(14).Style=   1
      Columns(14).StyleSet=   "stagefield"
      Columns(15).Width=   2117
      Columns(15).Caption=   "Complete"
      Columns(15).Name=   "Date_Complete"
      Columns(15).DataField=   "Date_Complete"
      Columns(15).FieldLen=   256
      Columns(15).Style=   1
      Columns(15).StyleSet=   "stagefield"
      Columns(16).Width=   767
      Columns(16).Caption=   "Done"
      Columns(16).Name=   "readytobill"
      Columns(16).DataField=   "readytobill"
      Columns(16).FieldLen=   256
      Columns(16).Locked=   -1  'True
      Columns(16).Style=   2
      Columns(16).StyleSet=   "conclusionfield"
      Columns(17).Width=   794
      Columns(17).Caption=   "Billed"
      Columns(17).Name=   "billed"
      Columns(17).DataField=   "billed"
      Columns(17).FieldLen=   256
      Columns(17).Style=   2
      Columns(17).StyleSet=   "conclusionfield"
      Columns(18).Width=   1270
      Columns(18).Caption=   "Inv Job #"
      Columns(18).Name=   "jobID"
      Columns(18).DataField=   "jobID"
      Columns(18).FieldLen=   256
      Columns(18).StyleSet=   "headerfield"
      Columns(19).Width=   1826
      Columns(19).Caption=   "Date Created"
      Columns(19).Name=   "Date_Created"
      Columns(19).DataField=   "Date_Created"
      Columns(19).FieldLen=   256
      Columns(19).StyleSet=   "headerfield"
      Columns(20).Width=   1826
      Columns(20).Caption=   "Date Mod"
      Columns(20).Name=   "Date_Modified"
      Columns(20).DataField=   "Date_Modified"
      Columns(20).FieldLen=   256
      Columns(20).VertScrollBar=   -1  'True
      Columns(20).StyleSet=   "headerfield"
      Columns(21).Width=   3201
      Columns(21).Caption=   "Filename"
      Columns(21).Name=   "clipfilename"
      Columns(21).DataField=   "clipfilename"
      Columns(21).FieldLen=   256
      Columns(21).StyleSet=   "conclusionfield"
      Columns(22).Width=   3200
      Columns(22).Caption=   "File Size"
      Columns(22).Name=   "bigfilesize"
      Columns(22).DataField=   "bigfilesize"
      Columns(22).FieldLen=   256
      Columns(22).StyleSet=   "conclusionfield"
      Columns(23).Width=   1773
      Columns(23).Caption=   "Timecode Dur"
      Columns(23).Name=   "Timecode_Duration"
      Columns(23).DataField=   "Timecode_Duration"
      Columns(23).FieldLen=   256
      Columns(23).StyleSet=   "conclusionfield"
      Columns(24).Width=   1323
      Columns(24).Caption=   "Duration"
      Columns(24).Name=   "Duration"
      Columns(24).DataField=   "Duration"
      Columns(24).FieldLen=   256
      Columns(24).StyleSet=   "conclusionfield"
      _ExtentX        =   50324
      _ExtentY        =   15372
      _StockProps     =   79
      Caption         =   "Tracker Items"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   1620
      TabIndex        =   27
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The company this job is for"
      Top             =   1500
      Width           =   2595
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   4577
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   315
      Left            =   8040
      TabIndex        =   35
      Top             =   2340
      Visible         =   0   'False
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   556
      _Version        =   393216
      Appearance      =   1
   End
   Begin MSAdodcLib.Adodc adoComments 
      Height          =   330
      Left            =   1860
      Top             =   12600
      Visible         =   0   'False
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoComments"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdComments 
      Bindings        =   "frmTrackerSonypictures.frx":006B
      Height          =   1875
      Left            =   60
      TabIndex        =   36
      Top             =   11520
      Width           =   12735
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12582847
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   6
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "trackerhistoryID"
      Columns(0).Name =   "tracker_commentID"
      Columns(0).DataField=   "tracker_commentID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "trackerprogramID"
      Columns(1).Name =   "tracker_spe_itemID"
      Columns(1).DataField=   "tracker_spe_itemID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   13070
      Columns(2).Caption=   "Comments"
      Columns(2).Name =   "comment"
      Columns(2).DataField=   "comment"
      Columns(2).FieldLen=   255
      Columns(3).Width=   3360
      Columns(3).Caption=   "Date"
      Columns(3).Name =   "cdate"
      Columns(3).DataField=   "cdate"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).Style=   1
      Columns(4).Width=   3519
      Columns(4).Caption=   "Entered By"
      Columns(4).Name =   "cuser"
      Columns(4).DataField=   "cuser"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   1429
      Columns(5).Caption=   "Email?"
      Columns(5).Name =   "Email?"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   4
      UseDefaults     =   0   'False
      _ExtentX        =   22463
      _ExtentY        =   3307
      _StockProps     =   79
      Caption         =   "Tracker Comments"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblCaption 
      Caption         =   "Alpha Code"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   78
      Left            =   120
      TabIndex        =   32
      Top             =   120
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Inv. Job #"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   50
      Left            =   120
      TabIndex        =   31
      Top             =   1200
      Width           =   1455
   End
   Begin VB.Label lblSearchCompanyID 
      Height          =   315
      Left            =   3240
      TabIndex        =   28
      Top             =   14820
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblLastTrackerItemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   1260
      TabIndex        =   15
      Top             =   14820
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   10
      Left            =   120
      TabIndex        =   13
      Top             =   1560
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title Abbrev"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   10
      Top             =   840
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   9
      Top             =   480
      Width           =   1395
   End
   Begin VB.Label lblTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   180
      TabIndex        =   8
      Top             =   14820
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCompanyID 
      Height          =   315
      Left            =   2340
      TabIndex        =   7
      Top             =   14820
      Visible         =   0   'False
      Width           =   735
   End
End
Attribute VB_Name = "frmTrackerSonypictures"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strSearch As String
Dim msp_strSearch As String
Dim m_strOrderby As String
Dim m_blnDelete As Boolean
Dim m_blnDontVerifyXML As Boolean
Dim m_blnBilling As Boolean
Dim m_blnSilent As Boolean
Dim l_strDateSearch As String
'Things for the Billing

Dim m_blnBillAll As Boolean
Dim m_strLastUniqueID As String
Dim m_strUniqueID As String
Dim m_strLastComponent As String
Dim m_datLastCompleteDate As Date
Dim m_lngAudioValidateCount As Long
Dim m_lngAudioConformCount As Long
Dim m_lngSubsValidateCount As Long
Dim m_lngSubsConformCount As Long
Dim m_strLastProjectManager As String, m_lngLastProjectNumber As Long, m_strLastSeries As String, m_strLastEpisodeNo As String, m_strLastEpisodeTitle As String, m_strLastTitle As String, m_strLastRightsOwner As String

Private Sub chkHideDemo_Click()

Dim l_strSQL As String

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/sonypicturestracker%' and companyID > 100 AND system_active = 1 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/sonypicturestracker%' AND system_active = 1 ORDER BY name;"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

cmdSearch.Value = True

End Sub

Private Sub cmbCompany_Click()

lblSearchCompanyID.Caption = cmbCompany.Columns("companyID").Text
m_strSearch = " WHERE companyID = " & lblSearchCompanyID.Caption

DoEvents

Dim l_rstChoices As ADODB.Recordset, l_blnWide As Boolean, l_strSQL As String

If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
    l_blnWide = True
Else
    l_blnWide = False
End If

'Dim l_conSearch As ADODB.Connection
'Dim l_rstSearch1 As ADODB.Recordset
'
'Set l_conSearch = New ADODB.Connection
'Set l_rstSearch1 = New ADODB.Recordset
'
'l_conSearch.ConnectionString = g_strConnection
'l_conSearch.Open
'
'l_strSQL = "SELECT name, contactID FROM contact WHERE contactID IN (SELECT contactID FROM employee WHERE companyID = " & Val(lblSearchCompanyID.Caption) & ") ORDER BY name;"
'
'With l_rstSearch1
'     .CursorLocation = adUseClient
'     .LockType = adLockBatchOptimistic
'     .CursorType = adOpenDynamic
'     .Open l_strSQL, l_conSearch, adOpenDynamic
'End With
'
'Set ddnProjectManager.DataSource = l_rstSearch1
'
'l_rstSearch1.ActiveConnection = Nothing
'
'grdItems.Columns("projectmanager").DropDownHwnd = ddnProjectManager.hWnd
'grdItems.Columns("projectmanager").Locked = False
'
'l_conSearch.Close
'Set l_conSearch = Nothing
'
cmdSearch.Value = True

End Sub

Private Sub cmdBillAll_Click()

Dim Count As Long, Bookmark As Variant

If Not adoItems.Recordset.EOF And cmbCompany.Text <> "" Then

    adoItems.Recordset.MoveFirst
    Count = 0
    ProgressBar1.Max = adoItems.Recordset.RecordCount
    ProgressBar1.Value = 0
    ProgressBar1.Visible = True

    m_blnBillAll = True
    m_strLastUniqueID = ""
    m_strUniqueID = ""
    m_strLastComponent = ""
    m_strLastTitle = ""
    m_datLastCompleteDate = 0
    m_lngAudioValidateCount = 0
    m_lngAudioConformCount = 0
    m_lngSubsValidateCount = 0
    m_lngSubsConformCount = 0
    m_strLastUniqueID = Trim(" " & adoItems.Recordset("uniqueID"))
    
    Do While Not adoItems.Recordset.EOF
        ProgressBar1.Value = Count
        'Bookmark = adoItems.Recordset.Bookmark
        If adoItems.Recordset("readytobill") <> 0 And adoItems.Recordset("billed") = 0 And adoItems.Recordset("complaintredoitem") = 0 Then
            cmdBillItem.Value = True
            'adoItems.Refresh
            'adoItems.Recordset.Bookmark = Bookmark
            m_strLastUniqueID = Trim(" " & adoItems.Recordset("uniqueID"))
            m_strLastComponent = Trim(" " & adoItems.Recordset("Componenttype"))
            m_datLastCompleteDate = adoItems.Recordset("complete")
            m_strLastTitle = adoItems.Recordset("title")
            m_strLastProjectManager = Trim(" " & adoItems.Recordset("ProjectManager"))
            m_lngLastProjectNumber = Val(Trim(" " & adoItems.Recordset("ProjectNumber")))
            m_strLastSeries = Trim(" " & adoItems.Recordset("series"))
            m_strLastEpisodeNo = Trim(" " & adoItems.Recordset("Episode"))
            m_strLastEpisodeTitle = Trim(" " & adoItems.Recordset("subtitle"))
            m_strLastRightsOwner = Trim(" " & adoItems.Recordset("RightsOwner"))
        End If
        adoItems.Recordset.MoveNext
        DoEvents
        Count = Count + 1
    Loop
    ProgressBar1.Visible = False
    m_blnBillAll = False
    If m_lngAudioConformCount > 0 Then
        MakeJobDetailLine Val(frmJob.txtJobID.Text), "O", m_strLastTitle & " - Simple Audio Conform 1st File", 1, "DADCAUCONF", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
        If m_lngAudioConformCount > 1 Then
            MakeJobDetailLine Val(frmJob.txtJobID.Text), "O", m_strLastTitle & " - Simple Audio Conform 2+ Files", m_lngAudioConformCount - 1, "DADCAUCONF2+", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
        End If
        m_lngAudioConformCount = 0
    End If
    If m_lngAudioValidateCount > 0 Then
        MakeJobDetailLine Val(frmJob.txtJobID.Text), "O", m_strLastTitle & " - Audio Validate 1st File", 1, "DADCAUDFILECHECK", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
        If m_lngAudioValidateCount > 1 Then
            MakeJobDetailLine Val(frmJob.txtJobID.Text), "O", m_strLastTitle & " - Audio Validate 2+ files", m_lngAudioValidateCount - 1, "DADCAUDFILECHECK2+", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
        End If
        m_lngAudioValidateCount = 0
    End If
    If m_lngSubsConformCount > 0 Then
        MakeJobDetailLine Val(frmJob.txtJobID.Text), "O", m_strLastTitle & " - Simple Conform Subtitles 1st File", 1, "DADCSUBSCONF", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
        If m_lngSubsConformCount > 1 Then
            MakeJobDetailLine Val(frmJob.txtJobID.Text), "O", m_strLastTitle & " - Simple Conform Subtitles 2+ Files", m_lngSubsConformCount - 1, "DADCSUBSCONF2+", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
        End If
        m_lngSubsConformCount = 0
    End If
    If m_lngSubsValidateCount > 0 Then
        MakeJobDetailLine Val(frmJob.txtJobID.Text), "O", m_strLastTitle & " - Validate Subtitles 1st File", 1, "DADCSUBSCHECK", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
        If m_lngSubsValidateCount > 1 Then
            MakeJobDetailLine Val(frmJob.txtJobID.Text), "O", m_strLastTitle & " - Validate Subtitles Subtitle 2+ Files", m_lngSubsValidateCount - 1, "DADCSUBSCHECK2+", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
        End If
        m_lngSubsValidateCount = 0
    End If
    
    
Else

    MsgBox "Problem with billing - either no items or no company chosen.", vbCritical, "Error..."
    
End If

'm_strOrderby = " ORDER BY urgent, targetdate, duedate, uniqueID, title, componenttype DESC, complete;"
'cmdSearch.Value = True
'
End Sub

Private Sub cmdBillItem_Click()

Dim l_strChargeCode As String, l_rstTrackerChargeCode As ADODB.Recordset, l_strJobLine As String, l_lngJobID As Long, l_lngDuration As Long, l_lngMinuteBilling As Long
Dim l_lngQuantity As Long, l_strCode As String, l_lngFactor As Long, l_blnFirstItem As Boolean, l_blnBulkBilling As Boolean, l_lngLoggingItemCount As Long
Dim l_lngEventID As Long, l_strLastComment As String, l_strLastCommentCuser As String, l_datLastCommentDate As Date, l_blnError As Boolean, l_lngSubsQuantity As Long
Dim l_strSeries As String, l_strEpisode As String, l_strEpisodeTitle As String, l_lngProjectNumber As Long, l_strProjectManager As String
Dim l_lngFixCount As Long, l_strFixList As String

l_blnFirstItem = True
m_blnBilling = True
l_blnError = False
If frmJob.txtJobID.Text <> "" Then
    'A job is loaded - now check that it isn't locked.
    l_lngJobID = Val(frmJob.txtJobID.Text)
    'Check that a company has been chosen in the Drop down - we can only add to a job sheet of the correct company
    If cmbCompany.Text <> "" Then
        If frmJob.txtStatus.Text = "Confirmed" And frmJob.lblCompanyID.Caption = lblCompanyID.Caption Then
            'The Job is valid - go ahead and create Job lines for this item.
            m_blnDontVerifyXML = True
            l_lngQuantity = 0
            'l_strJobLine = grdItems.Columns("itemreference").Text
            l_strJobLine = grdItems.Columns("title").Text & " " & grdItems.Columns("programtype").Text
            l_strSeries = grdItems.Columns("series").Text
            l_strEpisode = grdItems.Columns("episode").Text
            l_strEpisodeTitle = grdItems.Columns("subtitle").Text
            l_lngProjectNumber = grdItems.Columns("ProjectNumber").Text
            l_strProjectManager = grdItems.Columns("ProjectManager").Text
            If grdItems.Columns("complete").Text <> "" Then
                If m_blnBillAll = False Then
                'DADC Svensk Tracker Billing single item
                'If it a tape encode - if so we charge for an Encode Process
                'If it is a file submission, we only charge for the Validation process.
                'If it is a fix job, we don't charge validation from the tracker.
                    If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/svenskbillonlylogging") <= 0 Then
                        If grdItems.Columns("barcode").Text <> "" Then
                            'We have a tape
                            If grdItems.Columns("duration").Text <> "" Then
                                l_strCode = "I"
                                l_lngLoggingItemCount = 0
                                l_lngDuration = Val(grdItems.Columns("duration").Text)
                                l_lngQuantity = 1
                                If grdItems.Columns("format").Text = "HDCAM-SR" Then
                                    l_strChargeCode = "DADCINGESTHDSR"
                                ElseIf grdItems.Columns("format").Text = "HDCAM" Then
                                    l_strChargeCode = "DADCINGESTHD"
                                ElseIf grdItems.Columns("format").Text = "C-FORM" Then
                                    l_strChargeCode = "DADCINGESTCFORM"
                                ElseIf grdItems.Columns("Format").Text = "D3" Then
                                    l_strChargeCode = "DADCINGESTD3"
                                Else
                                    l_strChargeCode = "DADCINGESTSD"
                                End If
                                If l_strLastComment <> "" Then
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                Else
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                End If
                            Else
                                l_blnError = True
                                MsgBox "The current Item must have a duration in order to bill it.", vbCritical, "Cannot Bill Item"
                            End If
                        Else
                            'We have file(s)
                            If grdItems.Columns("componenttype").Text <> "" Then
                                If UCase(grdItems.Columns("componenttype").Text) = "VIDEO" Or UCase(grdItems.Columns("componenttype").Text) = "TRAILER" Then
                                    l_strCode = "O"
                                    l_lngQuantity = 1
                                    l_lngDuration = 0
                                    l_lngSubsQuantity = 0
                                    If Val(grdItems.Columns("duration").Text) < 30 Then
                                        If (UCase(grdItems.Columns("componenttype").Text) = "VIDEO" Or UCase(grdItems.Columns("componenttype").Text) = "TRAILER") And grdItems.Columns("validation").Text <> "" Then
                                            If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK-HD0", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                            Else
                                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK0", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                            End If
                                        End If
                                    Else
                                        If (UCase(grdItems.Columns("componenttype").Text) = "VIDEO" Or UCase(grdItems.Columns("componenttype").Text) = "TRAILER") And grdItems.Columns("validation").Text <> "" Then
                                            If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                            Else
                                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                            End If
                                        End If
                                    End If
                                ElseIf UCase(grdItems.Columns("componenttype").Text) = "AUDIO" Then
                                    If grdItems.Columns("DTAudioSimpleConform").Text = 0 Then
                                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Validate " & grdItems.Columns("language").Text, 1, "DADCAUDFILECHECKPOST", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                    Else
                                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Simple Audio Conform " & grdItems.Columns("language").Text, 1, "DADCAUCONF", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                    End If
                                ElseIf UCase(grdItems.Columns("componenttype").Text) = "SUBTITLE" Then
                                    If grdItems.Columns("DTAudioSimpleConform").Text = 0 Then
                                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Validate Subtitle File", 1, "DADCSUBSCHECK", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                        
                                    Else
                                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Simple Conform Subtitle File", 1, "DADCSUBSCONF", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                    End If
                                End If
                            Else
                                MsgBox "Unexpected Component type"
                                l_blnError = True
                            End If
                        End If
                    End If
                Else
                    'DADC Svensk Tracker Billing Bill-All in progress
                    If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/svenskbillonlylogging") <= 0 Then
                        If grdItems.Columns("barcode").Text <> "" Then
                            'We have a tape
                            If grdItems.Columns("duration").Text <> "" Then
                                l_strCode = "I"
                                l_lngLoggingItemCount = 0
                                l_lngDuration = Val(grdItems.Columns("duration").Text)
                                l_lngQuantity = 1
                                If grdItems.Columns("format").Text = "HDCAM-SR" Then
                                    l_strChargeCode = "DADCINGESTHDSR"
                                ElseIf grdItems.Columns("format").Text = "HDCAM" Then
                                    l_strChargeCode = "DADCINGESTHD"
                                ElseIf grdItems.Columns("format").Text = "C-FORM" Then
                                    l_strChargeCode = "DADCINGESTCFORM"
                                ElseIf grdItems.Columns("Format").Text = "D3" Then
                                    l_strChargeCode = "DADCINGESTD3"
                                Else
                                    l_strChargeCode = "DADCINGESTSD"
                                End If
                                If l_strLastComment <> "" Then
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                Else
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                End If
                            Else
                                l_blnError = True
                                MsgBox "The current Item must have a duration in order to bill it.", vbCritical, "Cannot Bill Item"
                            End If
                        End If
                        'We have file(s)
                        If grdItems.Columns("componenttype").Text <> "" Then
                            If (UCase(m_strLastComponent) = "AUDIO" Or UCase(m_strLastComponent) = "SUBTITLE") And Not (UCase(m_strLastComponent) = UCase(grdItems.Columns("componenttype").Text) And grdItems.Columns("uniqueID").Text = m_strLastUniqueID And Format(grdItems.Columns("complete").Text, "YYYY/MM/DD") = Format(m_datLastCompleteDate, "YYYY/MM/DD")) Then
                                If m_lngAudioConformCount > 0 Then
                                    MakeJobDetailLine l_lngJobID, "O", m_strLastTitle & " - Simple Audio Conform 1st File", 1, "DADCAUCONF", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
                                    If m_lngAudioConformCount > 1 Then
                                        MakeJobDetailLine l_lngJobID, "O", m_strLastTitle & " - Simple Audio Conform 2+ Files", m_lngAudioConformCount - 1, "DADCAUCONF2+", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
                                    End If
                                    m_lngAudioConformCount = 0
                                End If
                                If m_lngAudioValidateCount > 0 Then
                                    MakeJobDetailLine l_lngJobID, "O", m_strLastTitle & " - Audio Validate 1st File", 1, "DADCAUDFILECHECK", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
                                    If m_lngAudioValidateCount > 1 Then
                                        MakeJobDetailLine l_lngJobID, "O", m_strLastTitle & " - Audio Validate 2+ files", m_lngAudioValidateCount - 1, "DADCAUDFILECHECK2+", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
                                    End If
                                    m_lngAudioValidateCount = 0
                                End If
                                If m_lngSubsConformCount > 0 Then
                                    MakeJobDetailLine l_lngJobID, "O", m_strLastTitle & " - Simple Conform Subtitles 1st File", 1, "DADCSUBSCONF", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
                                    If m_lngSubsConformCount > 1 Then
                                        MakeJobDetailLine l_lngJobID, "O", m_strLastTitle & " - Simple Conform Subtitles 2+ Files", m_lngSubsConformCount - 1, "DADCSUBSCONF2+", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
                                    End If
                                    m_lngSubsConformCount = 0
                                End If
                                If m_lngSubsValidateCount > 0 Then
                                    MakeJobDetailLine l_lngJobID, "O", m_strLastTitle & " - Validate Subtitles 1st File", 1, "DADCSUBSCHECK", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
                                    If m_lngSubsValidateCount > 1 Then
                                        MakeJobDetailLine l_lngJobID, "O", m_strLastTitle & " - Validate Subtitles Subtitle 2+ Files", m_lngSubsValidateCount - 1, "DADCSUBSCHECK2+", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
                                    End If
                                    m_lngSubsValidateCount = 0
                                End If
                                m_strLastUniqueID = Trim(" " & adoItems.Recordset("uniqueID"))
                            End If
                            If (UCase(grdItems.Columns("componenttype").Text) = "VIDEO" Or UCase(grdItems.Columns("componenttype").Text) = "TRAILER") And grdItems.Columns("barcode").Text = "" Then
                                l_strCode = "O"
                                l_lngQuantity = 1
                                l_lngDuration = 0
                                l_lngSubsQuantity = 0
                                If Val(grdItems.Columns("duration").Text) < 30 Then
                                    If (UCase(grdItems.Columns("componenttype").Text) = "VIDEO" Or UCase(grdItems.Columns("componenttype").Text) = "TRAILER") And grdItems.Columns("validation").Text <> "" Then
                                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK-HD0", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                        Else
                                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK0", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                        End If
                                    End If
                                Else
                                    If (UCase(grdItems.Columns("componenttype").Text) = "VIDEO" Or UCase(grdItems.Columns("componenttype").Text) = "TRAILER") And grdItems.Columns("validation").Text <> "" Then
                                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                        Else
                                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                        End If
                                    End If
                                End If
                            ElseIf UCase(grdItems.Columns("componenttype").Text) = "AUDIO" Then
                                If grdItems.Columns("uniqueID").Text = m_strLastUniqueID Then
                                    If grdItems.Columns("DTAudioSimpleConform").Text <> 0 Then
                                        m_lngAudioConformCount = m_lngAudioConformCount + 1
                                    ElseIf grdItems.Columns("barcode").Text = "" Then
                                        m_lngAudioValidateCount = m_lngAudioValidateCount + 1
                                    End If
                                End If
                            ElseIf UCase(grdItems.Columns("componenttype").Text) = "SUBTITLE" Then
                                If grdItems.Columns("uniqueID").Text = m_strLastUniqueID Then
                                    If grdItems.Columns("DTAudioSimpleConform").Text <> 0 Then
                                        m_lngSubsConformCount = m_lngSubsConformCount + 1
                                    Else
                                        m_lngSubsValidateCount = m_lngSubsValidateCount + 1
                                    End If
                                End If
                            End If
                        Else
                            MsgBox "Unexpected Component type"
                            l_blnError = True
                        End If
                    End If
                End If
                If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/svenskbillonlylogging") > 0 And grdItems.Columns("xmlmade").Text <> "" Then
                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING15-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                    Else
                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING15", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                    End If
                ElseIf grdItems.Columns("xmlmade").Text <> "" Then
                    l_lngLoggingItemCount = 0
                    l_lngEventID = Val(GetDataSQL("SELECT TOP 1 eventID FROM events WHERE clipreference = '" & QuoteSanitise(grdItems.Columns("Itemreference").Text) & "' AND libraryID <> 307744;"))
                    If l_lngEventID <> 0 Then
                        Set l_rstTrackerChargeCode = ExecuteSQL("SELECT Count(eventlogging.eventloggingID) FROM eventlogging WHERE eventID = " & l_lngEventID & ";", g_strExecuteError)
                        CheckForSQLError
                        If l_rstTrackerChargeCode.RecordCount >= 0 Then
                            l_lngLoggingItemCount = l_rstTrackerChargeCode(0)
                        End If
                        l_rstTrackerChargeCode.Close
                        Set l_rstTrackerChargeCode = Nothing
                    Else
                        MsgBox "problem counting logging items for this clip - assuming min 5 items", vbCritical, "Error Billing..."
                    End If
                    If l_lngLoggingItemCount > 15 Then
                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - " & grdItems.Columns("language").Text, 1, "DADCLOGGING25-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                        Else
                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - " & grdItems.Columns("language").Text, 1, "DADCLOGGING25", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                        End If
'                    ElseIf (l_lngLoggingItemCount <= 4 And UCase(grdItems.Columns("componenttype").Text) = "VIDEO") Or l_lngLoggingItemCount > 4 Then
                    ElseIf l_lngLoggingItemCount > 4 Then
                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - " & grdItems.Columns("language").Text, 1, "DADCLOGGING15-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                        Else
                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - " & grdItems.Columns("language").Text, 1, "DADCLOGGING15", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                        End If
                    Else
                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - " & grdItems.Columns("language").Text, 1, "DADCLOGGING04-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                        Else
                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - " & grdItems.Columns("language").Text, 1, "DADCLOGGING04", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                        End If
                    End If
                End If
                l_lngFixCount = 0
'                If grdItems.Columns("DTAddClock").Text <> 0 And grdItems.Columns("DTBarsAndTone").Text <> 0 And grdItems.Columns("DTTimecode").Text <> 0 Then
'                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Replace or Remove bars, Clock and Timecode", 1, "DADCTCBARS-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    Else
'                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Replace or Remove bars, Clock and Timecode", 1, "DADCTCBARS", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    End If
'                Else
'                    If grdItems.Columns("DTAddClock").Text <> 0 Then
'                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Replace or Remove Clock", 1, "DADCCLOCKHD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                        Else
'                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Replace or Remove Clock", 1, "DADCCLOCKSD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                        End If
'                    End If
'                    If grdItems.Columns("DTBarsAndTone").Text <> 0 Then
'                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Replace or Remove Bars and Tone", 1, "DADCBARSHD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                        Else
'                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Replace or Remove Bars and Tone", 1, "DADCBARSSD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                        End If
'                    End If
'                    If grdItems.Columns("DTTimecode").Text <> 0 Then
'                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Replace Timecode", 1, "DADCTC-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                        Else
'                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Replace Timecode", 1, "DADCTC", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                        End If
'                    End If
'                End If
                If grdItems.Columns("DTAddClock").Text <> 0 Then
                    l_lngFixCount = l_lngFixCount + 1
                    l_strFixList = "Removal of Clock"
                End If
                If grdItems.Columns("DTBarsAndTone").Text <> 0 Then
                    l_lngFixCount = l_lngFixCount + 1
                    If Len(l_strFixList) > 0 Then l_strFixList = l_strFixList & ", "
                    l_strFixList = l_strFixList & "Removal of Bars & Tone"
                End If
                If grdItems.Columns("DTTimecode").Text <> 0 Then
                    l_lngFixCount = l_lngFixCount + 1
                    If Len(l_strFixList) > 0 Then l_strFixList = l_strFixList & ", "
                    l_strFixList = l_strFixList & "Replacing Timecode"
                End If
                If grdItems.Columns("DTBlackAtEnd").Text <> 0 Then
'                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - Replace or Remove Black at End", 1, "C-EDIT-HD", 15, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    Else
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - Replace or Remove Black at End", 1, "C-EDIT", 15, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    End If
                    l_lngFixCount = l_lngFixCount + 1
                    If Len(l_strFixList) > 0 Then l_strFixList = l_strFixList & ", "
                    l_strFixList = l_strFixList & "Removing Black at End"
                End If
                If grdItems.Columns("DTAudioChannelConfig").Text <> 0 Then
'                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - Correcting audio channel configuration", 1, "PROTOOLS-HD", 15, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    Else
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - Correcting audio channel configuration", 1, "PROTOOLS", 15, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    End If
                    l_lngFixCount = l_lngFixCount + 1
                    If Len(l_strFixList) > 0 Then l_strFixList = l_strFixList & ", "
                    l_strFixList = l_strFixList & "Audio Channel Reconfiguration"
                End If
                If grdItems.Columns("DTQuicktimeTagging").Text <> 0 Then
'                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - Labelling audio channels in QuickTime", 1, "PROTOOLS-HD", 15, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    Else
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - Labelling audio channels in QuickTime", 1, "PROTOOLS", 15, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    End If
                    l_lngFixCount = l_lngFixCount + 1
                    If Len(l_strFixList) > 0 Then l_strFixList = l_strFixList & ", "
                    l_strFixList = l_strFixList & "Audio Track Tagging"
                End If
                If grdItems.Columns("DTStereoDownmix").Text <> 0 Then
'                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - 5.1 to 2.0 audio down mix", 1, "PROTOOLS-HD", 30, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    Else
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - 5.1 to 2.0 audio down mix", 1, "PROTOOLS", 30, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    End If
                    l_lngFixCount = l_lngFixCount + 1
                    If Len(l_strFixList) > 0 Then l_strFixList = l_strFixList & ", "
                    l_strFixList = l_strFixList & "Downmixing Stereo Audio"
                End If
                If grdItems.Columns("DTRemoveReleaseDates").Text <> 0 Then
'                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - Video edit for removing release dates, URL�s, MPAA cards", 1, "C-EDIT-HD", 15, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    Else
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - Video edit for removing release dates, URL�s, MPAA cards", 1, "C-EDIT", 15, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    End If
                    l_lngFixCount = l_lngFixCount + 1
                    If Len(l_strFixList) > 0 Then l_strFixList = l_strFixList & ", "
                    l_strFixList = l_strFixList & "Removal of Release Dates"
                End If
                If grdItems.Columns("DTAudioFramerateConvert").Text <> 0 Then
'                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Audio frame rate conversion", 1, "DADCAUDIOX", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    Else
'                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Audio frame rate conversion", 1, "DADCAUDIOX", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    End If
                    l_lngFixCount = l_lngFixCount + 1
                    If Len(l_strFixList) > 0 Then l_strFixList = l_strFixList & ", "
                    l_strFixList = l_strFixList & "Audio Framerate Conversion"
                End If
                If l_lngFixCount > 0 And l_lngFixCount < 3 Then
                    MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - 1-2 Fixes, " & l_strFixList, 1, "DADCFIXING1-2", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                ElseIf l_lngFixCount > 2 And l_lngFixCount < 6 Then
                    MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - 1-2 Fixes, " & l_strFixList, 1, "DADCFIXING3-5", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                ElseIf l_lngFixCount > 5 Then
                    MsgBox "More than 5 fixes performed - cannot bill line", vbCritical, "Error"
                    l_blnError = True
                End If
            End If
            frmJob.adoJobDetail.Refresh
            frmJob.adoJobDetail.Refresh
            If l_blnError = False Then
                grdItems.Columns("billed").Text = -1
                grdItems.Columns("jobID").Text = l_lngJobID
                grdItems.Update
                cmdBillItem.Visible = False
                cmdManualBillItem.Visible = False
            End If
            m_blnDontVerifyXML = False
        Else
            MsgBox "The current Job must belong to the correct company and be confirmed in order to bill an item.", vbCritical, "Cannot Bill Item"
        End If
    Else
        MsgBox "There must be a company chosen in order to do billing.", vbCritical, "Cannot Bill Item"
    End If
Else
    MsgBox "A Job must be created and loaded into the Jobs form in order to bill an item.", vbCritical, "Cannot Bill Item"
End If

m_blnBilling = False

End Sub

Private Sub cmdClear_Click()

ClearFields Me
cmbCompany.Text = ""
lblCompanyID.Caption = ""
lblSearchCompanyID.Caption = ""
m_strSearch = " WHERE 1 = 1 "
cmdSearch.Value = True

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdManualBillItem_Click()

grdItems.Columns("billed").Text = -1
grdItems.Columns("jobid").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False

End Sub

Private Sub cmdSearch_Click()

'Dim l_strDateSearch As String

If Val(lblSearchCompanyID.Caption) = 0 Then Exit Sub

l_strDateSearch = ""
m_blnSilent = True
adoItems.ConnectionString = g_strConnection
msp_strSearch = SearchSQLstr()
adoItems.RecordSource = "SELECT * FROM tracker_spe_item " & m_strSearch & l_strDateSearch & msp_strSearch & m_strOrderby & ";"
Debug.Print adoItems.RecordSource

adoItems.Refresh

adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"
m_blnSilent = False

If Not adoItems.Recordset.EOF Then
    adoItems.Recordset.MoveLast
    adoItems.Recordset.MoveFirst
End If

End Sub

Function SearchSQLstr() As String

Dim l_strSQL As String

l_strSQL = ""

If chkHideDemo.Value <> 0 Then
    l_strSQL = l_strSQL & " AND companyID > 100 "
End If

If optComplete(0).Value = True Then 'Not Complete
    l_strSQL = l_strSQL & " AND (readytobill IS NULL OR readytobill = 0) "
ElseIf optComplete(1).Value = True Then 'Pending
    l_strSQL = l_strSQL & " AND (readytobill IS NULL OR readytobill = 0) AND rejected <> 0 "
ElseIf optComplete(2).Value = True Then 'Complete
    l_strSQL = l_strSQL & "AND readytobill <> 0  AND billed = 0 "
ElseIf optComplete(3).Value = True Then 'Billed
    l_strSQL = l_strSQL & "AND readytobill <> 0 AND billed <> 0 "
ElseIf optComplete(4).Value = True Then
    l_strSQL = l_strSQL & "AND 1=1 "
ElseIf optComplete(5).Value = True Then 'Master Not Here
    l_strSQL = l_strSQL & "AND (Date_Arrived IS NULL) "
ElseIf optComplete(6).Value = True Then 'XML Made
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND (Date_xml_made <> 0) "
ElseIf optComplete(7).Value = True Then ' XML not made
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND (Date_xml_made IS NULL OR Date_xml_made = 0) "
ElseIf optComplete(8).Value = True Then 'Sent with XML
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND Date_Sent <> 0  "
End If

If txtAlphaCode.Text <> "" Then
    l_strSQL = l_strSQL & " AND Alpha_ID LIKE '" & QuoteSanitise(txtAlphaCode.Text) & "%' "
End If
If txtTitle.Text <> "" Then
    l_strSQL = l_strSQL & " AND Title like '" & QuoteSanitise(txtTitle.Text) & "%' "
End If
If txtSubTitle.Text <> "" Then
    l_strSQL = l_strSQL & " AND Title_Abbrev LIKE '" & QuoteSanitise(txtSubTitle.Text) & "%' "
End If
If txtJobID.Text <> "" Then
    l_strSQL = l_strSQL & " AND jobID = " & Val(txtJobID.Text) & " "
End If

If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strSQL = l_strSQL & " AND companyID = " & Val(lblSearchCompanyID.Caption) & " "
End If

SearchSQLstr = l_strSQL

End Function

Private Sub cmdUnbill_Click()

grdItems.Columns("billed").Text = 0
grdItems.Columns("jobid").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False

End Sub

Private Sub cmdUnbillAll_Click()

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then

    Do While Not adoItems.Recordset.EOF
        adoItems.Recordset("billed") = 0
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
    Loop
End If

grdItems.Refresh

End Sub

Private Sub cmdUpdateAll_Click()

If adoItems.Recordset.RecordCount > 0 Then

    adoItems.Recordset.MoveFirst
    Do While Not adoItems.Recordset.EOF
        adoItems.Recordset("itemreference") = adoItems.Recordset("itemreference") & " "
        adoItems.Recordset("itemreference") = Trim(adoItems.Recordset("itemreference"))
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
    Loop
    adoItems.Recordset.MoveFirst
End If

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

Dim l_blnWide As Boolean

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch1 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch1 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/sonypicturestracker%' and companyID > 100 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/sonypicturestracker%' ORDER BY name;"
End If

With l_rstSearch1
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch1.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch1

grdItems.StyleSets("headerfield").BackColor = &HE7FFE7
grdItems.StyleSets("stagefield").BackColor = &HE7FFFF
grdItems.StyleSets("conclusionfield").BackColor = &HFFFFE7

grdItems.StyleSets.Add "Error"
grdItems.StyleSets("Error").BackColor = &HA0A0FF

grdItems.StyleSets.Add "NotRequired"
grdItems.StyleSets("NotRequired").BackColor = &H10101

grdItems.StyleSets.Add "Priority"
grdItems.StyleSets("Priority").BackColor = &H70B0FF

grdItems.StyleSets.Add "Fixed"
grdItems.StyleSets("Fixed").BackColor = &HA0FFA0
grdItems.StyleSets.Add "Notify"
grdItems.StyleSets("Notify").BackColor = &HFFFF40

grdItems.StyleSets.Add "Internal"
grdItems.StyleSets("Internal").BackColor = &HCCCC99

optComplete(0).Value = True

m_strSearch = " WHERE 1 = 1 "
'm_strOrderby = " ORDER BY urgent, CASE WHEN newworkontoday IS NULL THEN 1 ELSE 0 END, newworkontoday, CASE WHEN targetdate IS NULL THEN 1 ELSE 0 END, targetdate, CASE WHEN duedate IS NULL THEN 1 ELSE 0 END, duedate, uniqueID, title, componenttype DESC, complete;"
m_blnSilent = False

DoEvents

End Sub

Private Sub Form_Resize()

On Error Resume Next

grdItems.Width = Me.ScaleWidth - grdItems.Left - 120
grdItems.Height = (Me.ScaleHeight - grdItems.Top - frmButtons.Height) * 0.75 - 240
grdComments.Top = grdItems.Top + grdItems.Height + 120
grdComments.Height = (Me.ScaleHeight - grdComments.Top - frmButtons.Height) - 240
frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120

End Sub

Private Sub grdComments_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdComments_AfterUpdate(RtnDispErrMsg As Integer)

If m_blnDelete = False Then
    adoComments.Refresh
End If

End Sub

Private Sub grdComments_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdComments_BeforeUpdate(Cancel As Integer)

If m_blnDelete = False Then

    If Val(lblTrackeritemID.Caption) = 0 Then
        Cancel = 1
        Exit Sub
    End If
    
    If grdComments.Columns("comment").Text = "" Then
        MsgBox "Cannot save a Comment with no actual comment", vbCritical, "Comment Not Saved"
        Cancel = True
        Exit Sub
    End If
           
    grdComments.Columns("tracker_spe_itemID").Text = lblTrackeritemID.Caption
    grdComments.Columns("cuser").Text = g_strFullUserName
    
End If

End Sub

Private Sub grdComments_BtnClick()

Dim l_strOurEmailContact As String, l_strOurContactName As String, l_strEmailBody As String

Dim l_rstWhoToEmail As ADODB.Recordset

If MsgBox("Send Email?", vbYesNo, "Automatic Email") = vbYes Then
    
    l_strEmailBody = "A comment was created " & _
    "By: " & grdComments.Columns("cuser").Text & vbCrLf & _
    "Ref: " & grdItems.Columns("clipreference").Text & ", Title: " & grdItems.Columns("title").Text & ", Language: " & grdItems.Columns("language").Text & vbCrLf
    
    l_strEmailBody = l_strEmailBody & "Comment: " & grdComments.Columns("comment").Text & vbCrLf & vbCrLf
    
    Debug.Print l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND trackermessageID = 13;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
    
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "Sony Pictures Tracker Comment Created", "", l_strEmailBody, True, "", ""
            l_rstWhoToEmail.MoveNext
        
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
    
End If

End Sub

Private Sub grdItems_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdItems_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdItems_BeforeUpdate(Cancel As Integer)

If m_blnDelete = True Then Exit Sub

If Val(lblSearchCompanyID.Caption) = 0 Then
    MsgBox "Please select an MX1 Client." & vbCrLf & "Row not saved", vbCritical, "Error..."
    Cancel = 1
    Exit Sub
End If

If grdItems.Columns("companyID").Text = "" Then grdItems.Columns("companyID").Text = lblSearchCompanyID.Caption

Dim temp As Boolean, Count As Long, l_strDuration As String, l_strFrameRate As String, l_lngRunningTime As Long, l_curFileSize As Currency, l_strFilename As String, l_rst As ADODB.Recordset
Dim l_lngClipID As Long, l_strNetworkPath As String, l_strSQL As String, l_lngProjectManagerContactID As Long, l_datNewTargetDate As Date

'Check ReadytoBill - setting temp to false if any items are not ready to bill
If grdItems.Columns("complete").Text <> "" Then
    grdItems.Columns("readytobill").Text = 1
Else
    grdItems.Columns("readytobill").Text = 0
End If

'Get and update the file related items from the reference field.

If grdItems.Columns("Clipreference").Text <> "" Then
    grdItems.Columns("clipfilename").Text = grdItems.Columns("clipreference").Text & ".mov"
    'see if there is a clip record to get the duration from reference
    l_strSQL = "SELECT TOP 1 fd_length FROM events WHERE clipreference = '" & grdItems.Columns("Clipreference").Text & "' "
    l_strSQL = l_strSQL & "AND companyID = " & lblCompanyID.Caption & " "
    l_strSQL = l_strSQL & "AND clipfilename = '" & grdItems.Columns("Clipreference").Text & ".mov' AND system_deleted = 0;"
    Debug.Print l_strSQL
    l_strDuration = GetDataSQL(l_strSQL)
    grdItems.Columns("Timecode_Duration").Text = l_strDuration
    'work out the duration in minutes from the timecode type duration
    If l_strDuration <> "" Then
        l_lngRunningTime = 60 * Val(Mid(l_strDuration, 1, 2))
        l_lngRunningTime = l_lngRunningTime + Val(Mid(l_strDuration, 4, 2))
        If Val(Mid(l_strDuration, 7, 2)) > 30 Then
            l_lngRunningTime = l_lngRunningTime + 1
        End If
        If l_lngRunningTime = 0 Then l_lngRunningTime = 1
        If (grdItems.Columns("duration").Text = "") Or (grdItems.Columns("duration").Text <> "" And chkLockDur.Value = 0) Then
            grdItems.Columns("duration").Text = l_lngRunningTime
        End If
    End If
    'See if there is a size to go in the GB sent column
    l_curFileSize = 0
    Set l_rst = ExecuteSQL("SELECT bigfilesize FROM events WHERE clipreference = '" & QuoteSanitise(grdItems.Columns("Clipreference").Text) & "' AND companyID = " & Val(grdItems.Columns("companyID").Text) & " AND clipfilename = '" & QuoteSanitise(grdItems.Columns("Clipreference").Text) & ".mov' AND system_deleted = 0;", g_strExecuteError)
    CheckForSQLError
    If l_rst.RecordCount > 0 Then
        l_rst.MoveFirst
        Do While Not l_rst.EOF
            If Not IsNull(l_rst("bigfilesize")) Then
                If l_rst("bigfilesize") > l_curFileSize Then l_curFileSize = l_rst("bigfilesize")
            End If
            l_rst.MoveNext
        Loop
    End If
    l_rst.Close
    If l_curFileSize <> 0 Then
        grdItems.Columns("bigfilesize").Text = l_curFileSize
    Else
        grdItems.Columns("bigfilesize").Text = ""
    End If
    Set l_rst = Nothing
End If

grdItems.Columns("Date_Modified").Text = Format(Now, "DD mmm YY HH:nn:SS")

End Sub

Private Sub grdItems_BtnClick()

Dim tempdate As String

'If grdItems.Columns(grdItems.Col).Name = "Validation" Then
'    picValidationMasterFiles.Visible = True
'    lblValidationTitle.Caption = grdItems.Columns("title").Text
'    lblValidationSeries.Caption = Format(Val(grdItems.Columns("series").Text), "00")
'    lblValidationEpisode.Caption = Format(Val(grdItems.Columns("episode").Text), "00")
'    If grdItems.Columns("validation").Text <> "" Then
'        optMasterValidationComplete(0).Value = True
'    Else
'        optMasterValidationComplete(1).Value = True
'    End If
'    If GetData("tracker_svensk_item", "videoinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption) <> 0 Then
'        optVideoInSpec(0).Value = True
'    Else
'        optVideoInSpec(1).Value = True
'    End If
'    cmbFrameRate.Text = GetData("tracker_svensk_item", "framerate", "tracker_svensk_itemID", lblTrackeritemID.Caption)
'    If GetData("tracker_svensk_item", "hdflag", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
'        optHDFlag(0).Value = True
'    Else
'        optHDFlag(1).Value = True
'    End If
'    If GetData("tracker_svensk_item", "MainStereoPresent", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
'        optMainStereoPresent(0).Value = True
'    Else
'        optMainStereoPresent(1).Value = True
'    End If
'    If GetData("tracker_svensk_item", "MEStereoPresent", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
'        optMEStereoPresent(0).Value = True
'    Else
'        optMEStereoPresent(1).Value = True
'    End If
'    If GetData("tracker_svensk_item", "SurroundPresent", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
'        optSurroundPresent(0).Value = True
'    Else
'        optSurroundPresent(1).Value = True
'    End If
'    If GetData("tracker_svensk_item", "TextlessPresent", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
'        optTextlessPresent(0).Value = True
'    Else
'        optTextlessPresent(1).Value = True
'    End If
'    If GetData("tracker_svensk_item", "BlackAtEnd", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
'        optBlackAtEnd(0).Value = True
'    Else
'        optBlackAtEnd(1).Value = True
'    End If
'    If GetData("tracker_svensk_item", "TextlessSeparator", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
'        optTextlessSeparator(0).Value = True
'    Else
'        optTextlessSeparator(1).Value = True
'    End If
'
'ElseIf grdItems.Columns(grdItems.Col).Name = "englishvalidation" Or grdItems.Columns(grdItems.Col).Name = "norwegianvalidation" _
'Or grdItems.Columns(grdItems.Col).Name = "swedishvalidation" Or grdItems.Columns(grdItems.Col).Name = "danishvalidation" Or grdItems.Columns(grdItems.Col).Name = "finnishvalidation" _
'Or grdItems.Columns(grdItems.Col).Name = "icelandicvalidation" Then
'    If grdItems.Columns(grdItems.Col).Text <> "" Then
'        optAudioValidationComplete(0).Value = True
'    Else
'        optAudioValidationComplete(1).Value = True
'    End If
'    picValidationAudioFiles.Visible = True
'    lblValidationTitle.Caption = grdItems.Columns("title").Text
'    lblValidationSeries.Caption = Format(Val(grdItems.Columns("series").Text), "00")
'    lblValidationEpisode.Caption = Format(Val(grdItems.Columns("episode").Text), "00")
'    optEnglishInSpec(Val(GetData("tracker_svensk_item", "englishinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optNorwegianInSpec(Val(GetData("tracker_svensk_item", "norwegianinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optSwedishInSpec(Val(GetData("tracker_svensk_item", "swedishinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optDanishInSpec(Val(GetData("tracker_svensk_item", "danishinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optFinnishInSpec(Val(GetData("tracker_svensk_item", "finnishinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optIcelandicInSpec(Val(GetData("tracker_svensk_item", "icelandicinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    If grdItems.Columns("englishsepaudio").Text = 0 Then optEnglishInSpec(2).Value = True
'    If grdItems.Columns("norwegiansepaudio").Text = 0 Then optNorwegianInSpec(2).Value = True
'    If grdItems.Columns("swedishsepaudio").Text = 0 Then optSwedishInSpec(2).Value = True
'    If grdItems.Columns("danishsepaudio").Text = 0 Then optDanishInSpec(2).Value = True
'    If grdItems.Columns("finnishsepaudio").Text = 0 Then optFinnishInSpec(2).Value = True
'    If grdItems.Columns("icelandicsepaudio").Text = 0 Then optIcelandicInSpec(2).Value = True
'ElseIf grdItems.Columns(grdItems.Col).Name = "englishsubsvalidation" Or grdItems.Columns(grdItems.Col).Name = "norwegiansubsvalidation" _
'Or grdItems.Columns(grdItems.Col).Name = "swedishsubsvalidation" Or grdItems.Columns(grdItems.Col).Name = "danishsubsvalidation" Or grdItems.Columns(grdItems.Col).Name = "finnishsubsvalidation" _
'Or grdItems.Columns(grdItems.Col).Name = "icelandicsubsvalidation" Then
'    If grdItems.Columns(grdItems.Col).Text <> "" Then
'        optSubsValidationComplete(0).Value = True
'    Else
'        optSubsValidationComplete(1).Value = True
'    End If
'    picValidationSubs.Visible = True
'    lblValidationTitle.Caption = grdItems.Columns("title").Text
'    lblValidationSeries.Caption = Format(Val(grdItems.Columns("series").Text), "00")
'    lblValidationEpisode.Caption = Format(Val(grdItems.Columns("episode").Text), "00")
'    optEnglishSubsInSpec(Val(GetData("tracker_svensk_item", "englishsubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optNorwegianSubsInSpec(Val(GetData("tracker_svensk_item", "norwegiansubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optSwedishSubsInSpec(Val(GetData("tracker_svensk_item", "swedishsubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optDanishSubsInSpec(Val(GetData("tracker_svensk_item", "danishsubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optFinnishSubsInSpec(Val(GetData("tracker_svensk_item", "finnishsubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optIcelandicSubsInSpec(Val(GetData("tracker_svensk_item", "icelandicsubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    If grdItems.Columns("englishsubs").Text = 0 Then optEnglishSubsInSpec(2).Value = True
'    If grdItems.Columns("norwegiansubs").Text = 0 Then optNorwegianSubsInSpec(2).Value = True
'    If grdItems.Columns("swedishsubs").Text = 0 Then optSwedishSubsInSpec(2).Value = True
'    If grdItems.Columns("danishsubs").Text = 0 Then optDanishSubsInSpec(2).Value = True
'    If grdItems.Columns("finnishsubs").Text = 0 Then optFinnishSubsInSpec(2).Value = True
'    If grdItems.Columns("icelandicsubs").Text = 0 Then optIcelandicSubsInSpec(2).Value = True

Select Case LCase(grdItems.Columns(grdItems.Col).Name)

Case Else
    If grdItems.ActiveCell.Text <> "" Then
        grdItems.ActiveCell.Text = ""
    Else
        If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
            grdItems.ActiveCell.Text = Now
        Else
            tempdate = FormatDateTime(Now, vbLongDate)
            grdItems.ActiveCell.Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
        End If
    End If

End Select

End Sub

Private Sub grdItems_DblClick()

ShowClipSearch "", grdItems.Columns("Clipreference").Text

End Sub

Private Sub grdItems_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim l_strSQL As String

If m_blnSilent = False Then
    lblTrackeritemID.Caption = grdItems.Columns("tracker_spe_itemID").Text
    lblCompanyID.Caption = grdItems.Columns("companyID").Text
    
    If Val(lblTrackeritemID.Caption) <> Val(lblLastTrackeritemID.Caption) Then
        
        If Val(lblTrackeritemID.Caption) = 0 Then
            
            l_strSQL = "SELECT * FROM tracker_spe_comment WHERE tracker_spe_itemID = -1 ORDER BY cdate;"
            
            adoComments.RecordSource = l_strSQL
            adoComments.ConnectionString = g_strConnection
            adoComments.Refresh
            
            lblLastTrackeritemID.Caption = ""
        
            Exit Sub
        
        End If
        
        l_strSQL = "SELECT * FROM tracker_spe_comment WHERE tracker_spe_itemID = " & lblTrackeritemID.Caption & " ORDER BY cdate;"
        
        adoComments.RecordSource = l_strSQL
        adoComments.ConnectionString = g_strConnection
        adoComments.Refresh
    
        lblLastTrackeritemID.Caption = lblTrackeritemID.Caption
        
        If grdItems.Columns("readytobill").Text <> 0 And grdItems.Columns("billed").Text = 0 Then
            cmdBillItem.Visible = True
            cmdManualBillItem.Visible = True
        Else
            cmdBillItem.Visible = False
            cmdManualBillItem.Visible = False
        End If

        If grdItems.Columns("billed").Text <> 0 Then
            cmdUnbill.Visible = True
        Else
            cmdUnbill.Visible = False
        End If

    End If

End If

End Sub

Private Sub grdItems_RowLoaded(ByVal Bookmark As Variant)

If grdItems.Columns("rejected").Text <> 0 Then grdItems.Columns("rejected").CellStyleSet "Error"

End Sub

Private Sub optComplete_Click(Index As Integer)

cmdSearch.Value = True

End Sub

