VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmFileTechRev 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "File Tech Review"
   ClientHeight    =   13860
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   23895
   Icon            =   "frmFileTechRev.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   13860
   ScaleWidth      =   23895
   Visible         =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.OptionButton chk5PointCheck 
      Caption         =   "5 Point Check"
      Height          =   255
      Left            =   9660
      TabIndex        =   251
      Top             =   660
      Width           =   1695
   End
   Begin VB.TextBox txtNormalGrading 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   615
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   240
      Text            =   "frmFileTechRev.frx":0BC2
      Top             =   6600
      Width           =   11355
   End
   Begin VB.TextBox txtDisneyFaultGrading 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   615
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   239
      Text            =   "frmFileTechRev.frx":0C18
      Top             =   6600
      Width           =   11355
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnFaultGrade 
      Height          =   855
      Left            =   12480
      TabIndex        =   237
      Top             =   12120
      Width           =   3315
      DataFieldList   =   "column 0"
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   4419
      Columns(0).Caption=   "value"
      Columns(0).Name =   "value"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   5847
      _ExtentY        =   1508
      _StockProps     =   77
      DataFieldToDisplay=   "column 0"
   End
   Begin VB.OptionButton chkRuntimeReview 
      Caption         =   "Runtime Review"
      Height          =   255
      Left            =   9660
      TabIndex        =   232
      Top             =   360
      Value           =   -1  'True
      Width           =   1695
   End
   Begin VB.OptionButton chkiTunesFinal 
      Caption         =   "iTunes Final"
      Height          =   255
      Left            =   11460
      TabIndex        =   231
      Top             =   360
      Width           =   1215
   End
   Begin VB.OptionButton chkiTunesReview 
      Caption         =   "iTunes Review?"
      Height          =   255
      Left            =   11460
      TabIndex        =   230
      Top             =   60
      Width           =   1515
   End
   Begin VB.OptionButton chkSpotCheckRev 
      Caption         =   "Full Review?"
      Height          =   255
      Left            =   9660
      TabIndex        =   229
      Top             =   60
      Width           =   1455
   End
   Begin VB.TextBox txtWarning 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1035
      Left            =   16140
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   194
      Text            =   "frmFileTechRev.frx":0C7C
      Top             =   120
      Width           =   5715
   End
   Begin VB.Frame fraButtons 
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   15420
      TabIndex        =   173
      Top             =   6000
      Width           =   6495
      Begin VB.CommandButton cmdVDMSQC 
         Caption         =   "VDMS QC"
         Height          =   315
         Left            =   1680
         TabIndex        =   253
         ToolTipText     =   "Save changes to this Tape"
         Top             =   360
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.CommandButton cmdP4MReview 
         Caption         =   "P4M Report"
         Height          =   315
         Left            =   1680
         TabIndex        =   252
         ToolTipText     =   "Save changes to this Tape"
         Top             =   0
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.CommandButton cmdZodiakQC 
         Caption         =   "Zodiak QC"
         Height          =   315
         Left            =   480
         TabIndex        =   248
         ToolTipText     =   "Save changes to this Tape"
         Top             =   360
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.CommandButton cmdDisneyQC 
         Caption         =   "Disney QC"
         Height          =   315
         Left            =   480
         TabIndex        =   241
         ToolTipText     =   "Save changes to this Tape"
         Top             =   0
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   5280
         TabIndex        =   176
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         Height          =   315
         Left            =   4080
         TabIndex        =   175
         ToolTipText     =   "Save changes to this Tape"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   2880
         TabIndex        =   174
         ToolTipText     =   "Save changes to this Tape"
         Top             =   0
         Width           =   1155
      End
   End
   Begin VB.Frame fraTechReviewConclusions 
      BorderStyle     =   0  'None
      Height          =   4755
      Left            =   16200
      TabIndex        =   57
      Top             =   1200
      Width           =   5655
      Begin VB.Frame fraiTunes 
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   300
         TabIndex        =   67
         Top             =   4320
         Width           =   4995
         Begin VB.OptionButton optReviewConclusion 
            BackColor       =   &H004040FF&
            Caption         =   "Reject for iTunes"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   300
            TabIndex        =   69
            ToolTipText     =   "Reject the Tape as Failing to meet required specicfications"
            Top             =   0
            Width           =   1935
         End
         Begin VB.OptionButton optReviewConclusion 
            BackColor       =   &H0080FF80&
            Caption         =   "Accept for iTunes"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   2460
            TabIndex        =   68
            ToolTipText     =   "Tape meets required specifications"
            Top             =   0
            Width           =   1935
         End
      End
      Begin VB.OptionButton optReviewConclusion 
         BackColor       =   &H0080FF80&
         Caption         =   "Accept"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   3660
         TabIndex        =   63
         ToolTipText     =   "Tape meets required specifications"
         Top             =   3900
         Width           =   1155
      End
      Begin VB.OptionButton optReviewConclusion 
         BackColor       =   &H00C0C0FF&
         Caption         =   "Accepted by Client"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   1500
         TabIndex        =   62
         ToolTipText     =   "Tape has failed to meet required specifications, but client has accepted it"
         Top             =   3900
         Width           =   1995
      End
      Begin VB.OptionButton optReviewConclusion 
         BackColor       =   &H004040FF&
         Caption         =   "Reject"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   300
         TabIndex        =   61
         ToolTipText     =   "Reject the Tape as Failing to meet required specicfications"
         Top             =   3900
         Width           =   1035
      End
      Begin VB.TextBox txtTechNotesGeneral 
         Height          =   1035
         Left            =   -60
         MultiLine       =   -1  'True
         TabIndex        =   60
         ToolTipText     =   "General Notes and Summary for the Tewchnical Review"
         Top             =   2760
         Width           =   5535
      End
      Begin VB.TextBox txtTechNotesAudio 
         Height          =   1035
         Left            =   -60
         MultiLine       =   -1  'True
         TabIndex        =   59
         ToolTipText     =   "Notes regarding Audio on the Technical Review"
         Top             =   1500
         Width           =   5535
      End
      Begin VB.TextBox txtTechNotesVideo 
         Height          =   1035
         Left            =   -60
         MultiLine       =   -1  'True
         TabIndex        =   58
         ToolTipText     =   "Notes regarding Video on the Technical Review"
         Top             =   240
         Width           =   5535
      End
      Begin VB.Label lblCaption 
         Caption         =   "General Notes"
         Height          =   255
         Index           =   78
         Left            =   0
         TabIndex        =   66
         Top             =   2580
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Audio Notes"
         Height          =   255
         Index           =   77
         Left            =   0
         TabIndex        =   65
         Top             =   1320
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Video Notes"
         Height          =   255
         Index           =   76
         Left            =   0
         TabIndex        =   64
         Top             =   60
         Width           =   855
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbTechReviewList 
      Bindings        =   "frmFileTechRev.frx":0CFC
      DataField       =   "techrevID"
      Height          =   315
      Left            =   1560
      TabIndex        =   54
      Top             =   120
      Width           =   1575
      DataFieldList   =   "techrevID"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Caption=   "techrevID"
      Columns(0).Name =   "techrevID"
      Columns(0).DataField=   "techrevID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "reviewdate"
      Columns(1).Name =   "reviewdate"
      Columns(1).DataField=   "reviewdate"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "reviewUser"
      Columns(2).Name =   "reviewUser"
      Columns(2).DataField=   "reviewUser"
      Columns(2).FieldLen=   256
      _ExtentX        =   2778
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.CommandButton cmdMakeTechReview 
      Caption         =   "Make Tech Review"
      Height          =   315
      Left            =   7800
      TabIndex        =   53
      ToolTipText     =   "Make a Tech Review for this Tape"
      Top             =   60
      Width           =   1695
   End
   Begin VB.CommandButton cmdTechReviewPdf 
      Caption         =   "Make TR  PDF"
      Enabled         =   0   'False
      Height          =   315
      Left            =   7800
      TabIndex        =   52
      ToolTipText     =   "Construct a PDF of the Tech Review"
      Top             =   900
      Width           =   1695
   End
   Begin VB.CommandButton cmdTechReviewCopyFaults 
      Caption         =   "Copy Fault List"
      Enabled         =   0   'False
      Height          =   315
      Left            =   7800
      TabIndex        =   51
      ToolTipText     =   "Copy the Tech Review Fault List from another Tape to this Tape"
      Top             =   1320
      Width           =   1695
   End
   Begin VB.CommandButton cmdDropTechReview 
      Caption         =   "Delete Tech Review"
      Enabled         =   0   'False
      Height          =   315
      Left            =   7800
      TabIndex        =   50
      ToolTipText     =   "Make a Tech Review for this Tape"
      Top             =   480
      Width           =   1695
   End
   Begin MSAdodcLib.Adodc adoFaultsList 
      Height          =   330
      Left            =   2040
      Top             =   9600
      Visible         =   0   'False
      Width           =   2820
      _ExtentX        =   4974
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoFaultsList"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnFaultsList 
      Bindings        =   "frmFileTechRev.frx":0D19
      Height          =   1755
      Left            =   540
      TabIndex        =   55
      Top             =   8640
      Width           =   6195
      DataFieldList   =   "description"
      ListAutoValidate=   0   'False
      ListWidthAutoSize=   0   'False
      MaxDropDownItems=   30
      ListWidth       =   8440
      _Version        =   196617
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   8281
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "description"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   10927
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "description"
   End
   Begin MSAdodcLib.Adodc adoFaults 
      Height          =   330
      Left            =   120
      Top             =   7320
      Visible         =   0   'False
      Width           =   2760
      _ExtentX        =   4868
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoFaults"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdFaults 
      Bindings        =   "frmFileTechRev.frx":0D35
      Height          =   6075
      Left            =   120
      TabIndex        =   56
      TabStop         =   0   'False
      Top             =   7320
      Width           =   12135
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12648447
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   11
      Columns(0).Width=   5292
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "description"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   255
      Columns(1).Width=   2275
      Columns(1).Caption=   "Start Timecode"
      Columns(1).Name =   "timecode"
      Columns(1).DataField=   "timecode"
      Columns(1).FieldLen=   256
      Columns(2).Width=   2328
      Columns(2).Caption=   "End Timecode"
      Columns(2).Name =   "endtimecode"
      Columns(2).DataField=   "endtimecode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   1640
      Columns(3).Caption=   "Grade"
      Columns(3).Name =   "faultgrade"
      Columns(3).DataField=   "faultgrade"
      Columns(3).FieldLen=   256
      Columns(4).Width=   873
      Columns(4).Caption=   "Area"
      Columns(4).Name =   "VideoArea"
      Columns(4).DataField=   "VideoArea"
      Columns(4).FieldLen=   256
      Columns(5).Width=   714
      Columns(5).Caption=   "Ch"
      Columns(5).Name =   "AudioChannel"
      Columns(5).DataField=   "AudioChannel"
      Columns(5).FieldLen=   256
      Columns(6).Width=   1588
      Columns(6).Caption=   "Type"
      Columns(6).Name =   "faulttype"
      Columns(6).DataField=   "faulttype"
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "techrevID"
      Columns(7).Name =   "techrevID"
      Columns(7).DataField=   "techrevID"
      Columns(7).FieldLen=   256
      Columns(8).Width=   2884
      Columns(8).Caption=   "Action"
      Columns(8).Name =   "faultaction"
      Columns(8).DataField=   "faultaction"
      Columns(8).FieldLen=   256
      Columns(9).Width=   873
      Columns(9).Caption=   "fixed"
      Columns(9).Name =   "fixed"
      Columns(9).DataField=   "faultfixed"
      Columns(9).FieldLen=   256
      Columns(9).Style=   2
      Columns(10).Width=   2249
      Columns(10).Caption=   "Fixed Date"
      Columns(10).Name=   "fixeddate"
      Columns(10).DataField=   "faultfixeddate"
      Columns(10).FieldLen=   256
      Columns(10).Style=   1
      _ExtentX        =   21405
      _ExtentY        =   10716
      _StockProps     =   79
      Caption         =   "Faults"
      BackColor       =   -2147483643
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraTechReview 
      BorderStyle     =   0  'None
      Height          =   10635
      Left            =   120
      TabIndex        =   0
      ToolTipText     =   "Details about the tape Technical Review"
      Top             =   120
      Width           =   22155
      Begin VB.CheckBox chkAudioPhaseError 
         Alignment       =   1  'Right Justify
         Caption         =   "Audio Phase Errors"
         Height          =   195
         Left            =   10500
         TabIndex        =   250
         ToolTipText     =   "Are all graphics 16:9 safe?"
         Top             =   3300
         Width           =   1755
      End
      Begin VB.Frame fraR128Summary 
         Caption         =   "R128 Summary"
         Height          =   975
         Left            =   20580
         TabIndex        =   245
         Top             =   6840
         Width           =   1515
         Begin VB.OptionButton optR128Summary 
            Caption         =   "Fail"
            Height          =   315
            Index           =   0
            Left            =   240
            TabIndex        =   247
            Top             =   600
            Width           =   1095
         End
         Begin VB.OptionButton optR128Summary 
            Caption         =   "Pass"
            Height          =   315
            Index           =   1
            Left            =   240
            TabIndex        =   246
            Top             =   300
            Value           =   -1  'True
            Width           =   1095
         End
      End
      Begin VB.CommandButton cmdRedFinalCutMarkers 
         Caption         =   "Read FC Markers File"
         Height          =   315
         Left            =   4560
         TabIndex        =   238
         ToolTipText     =   "Copy the Tech Review Fault List from another Tape to this Tape"
         Top             =   360
         Width           =   2055
      End
      Begin VB.TextBox txtiTunesVendorID 
         Height          =   315
         Left            =   1440
         TabIndex        =   235
         ToolTipText     =   "The job number for the tech review"
         Top             =   1080
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.ComboBox cmbiTunesVideoType 
         Height          =   315
         Left            =   1440
         TabIndex        =   233
         ToolTipText     =   "The machine the Tape was reviewed on"
         Top             =   1440
         Width           =   1575
      End
      Begin VB.ComboBox cmbForcedLanguage 
         Height          =   315
         Left            =   3360
         TabIndex        =   226
         ToolTipText     =   "The machine the Tape was reviewed on"
         Top             =   3240
         Width           =   4155
      End
      Begin VB.CheckBox chkForcedNarratives 
         Caption         =   "Forced Narratives"
         Height          =   195
         Left            =   60
         TabIndex        =   225
         ToolTipText     =   "Are there Subtitles on the Tape?"
         Top             =   3300
         Width           =   1635
      End
      Begin VB.CommandButton cmdSwitchLineupLevel 
         Caption         =   "Flip Line-up Level"
         Height          =   315
         Left            =   9480
         TabIndex        =   224
         ToolTipText     =   "Copy the Tech Review Fault List from another Tape to this Tape"
         Top             =   1200
         Width           =   1575
      End
      Begin VB.Frame fraLoudness 
         Caption         =   "EBU R128 Loudness"
         Height          =   3675
         Left            =   12480
         TabIndex        =   177
         Top             =   6840
         Width           =   7995
         Begin VB.ComboBox cmbEBU128MaxShortLoundnessSet8 
            Height          =   315
            Left            =   6240
            TabIndex        =   222
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   3180
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128MaxShortLoundnessSet7 
            Height          =   315
            Left            =   6240
            TabIndex        =   221
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   2820
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128MaxShortLoundnessSet6 
            Height          =   315
            Left            =   6240
            TabIndex        =   220
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   2460
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128MaxShortLoundnessSet5 
            Height          =   315
            Left            =   6240
            TabIndex        =   219
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   2100
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128MaxShortLoundnessSet4 
            Height          =   315
            Left            =   6240
            TabIndex        =   218
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   1740
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128MaxShortLoundnessSet3 
            Height          =   315
            Left            =   6240
            TabIndex        =   217
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   1380
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128MaxShortLoundnessSet2 
            Height          =   315
            Left            =   6240
            TabIndex        =   216
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   1020
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128MaxShortLoundnessSet1 
            Height          =   315
            Left            =   6240
            TabIndex        =   215
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   660
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128LoudnessRangeSet8 
            Height          =   315
            Left            =   4560
            TabIndex        =   213
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   3180
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128LoudnessRangeSet7 
            Height          =   315
            Left            =   4560
            TabIndex        =   212
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   2820
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128LoudnessRangeSet6 
            Height          =   315
            Left            =   4560
            TabIndex        =   211
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   2460
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128LoudnessRangeSet5 
            Height          =   315
            Left            =   4560
            TabIndex        =   210
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   2100
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128LoudnessRangeSet4 
            Height          =   315
            Left            =   4560
            TabIndex        =   209
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   1740
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128LoudnessRangeSet3 
            Height          =   315
            Left            =   4560
            TabIndex        =   208
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   1380
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128LoudnessRangeSet2 
            Height          =   315
            Left            =   4560
            TabIndex        =   207
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   1020
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128LoudnessRangeSet1 
            Height          =   315
            Left            =   4560
            TabIndex        =   206
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   660
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128TruePeakSet8 
            Height          =   315
            Left            =   2880
            TabIndex        =   204
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   3180
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128TruePeakSet7 
            Height          =   315
            Left            =   2880
            TabIndex        =   203
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   2820
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128TruePeakSet6 
            Height          =   315
            Left            =   2880
            TabIndex        =   202
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   2460
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128TruePeakSet5 
            Height          =   315
            Left            =   2880
            TabIndex        =   201
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   2100
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128TruePeakSet4 
            Height          =   315
            Left            =   2880
            TabIndex        =   200
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   1740
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128TruePeakSet3 
            Height          =   315
            Left            =   2880
            TabIndex        =   199
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   1380
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128TruePeakSet2 
            Height          =   315
            Left            =   2880
            TabIndex        =   198
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   1020
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128TruePeakSet1 
            Height          =   315
            Left            =   2880
            TabIndex        =   197
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   660
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128LoudnessSet5 
            Height          =   315
            Left            =   1200
            TabIndex        =   185
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   2100
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128LoudnessSet6 
            Height          =   315
            Left            =   1200
            TabIndex        =   184
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   2460
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128LoudnessSet7 
            Height          =   315
            Left            =   1200
            TabIndex        =   183
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   2820
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128LoudnessSet8 
            Height          =   315
            Left            =   1200
            TabIndex        =   182
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   3180
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128LoudnessSet1 
            Height          =   315
            Left            =   1200
            TabIndex        =   181
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   660
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128LoudnessSet2 
            Height          =   315
            Left            =   1200
            TabIndex        =   180
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   1020
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128LoudnessSet3 
            Height          =   315
            Left            =   1200
            TabIndex        =   179
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   1380
            Width           =   1600
         End
         Begin VB.ComboBox cmbEBU128LoudnessSet4 
            Height          =   315
            Left            =   1200
            TabIndex        =   178
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   1740
            Width           =   1600
         End
         Begin VB.Label lblCaption 
            Alignment       =   2  'Center
            Caption         =   "Maximum Short Term Loudness (LU)"
            Height          =   495
            Index           =   27
            Left            =   6180
            TabIndex        =   223
            Top             =   180
            Width           =   1635
         End
         Begin VB.Label lblCaption 
            Alignment       =   2  'Center
            Caption         =   "Loudness Range (LUFS)"
            Height          =   495
            Index           =   26
            Left            =   4620
            TabIndex        =   214
            Top             =   240
            Width           =   1395
         End
         Begin VB.Label lblCaption 
            Caption         =   "True Peak (dBTP)"
            Height          =   255
            Index           =   25
            Left            =   2940
            TabIndex        =   205
            Top             =   300
            Width           =   1395
         End
         Begin VB.Label lblCaption 
            Caption         =   "R128 (LUFS)"
            Height          =   255
            Index           =   24
            Left            =   1440
            TabIndex        =   196
            Top             =   300
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio Set 8"
            Height          =   255
            Index           =   21
            Left            =   120
            TabIndex        =   193
            Top             =   3240
            Width           =   975
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio Set 7"
            Height          =   255
            Index           =   20
            Left            =   120
            TabIndex        =   192
            Top             =   2880
            Width           =   975
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio Set 6"
            Height          =   255
            Index           =   19
            Left            =   120
            TabIndex        =   191
            Top             =   2520
            Width           =   975
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio Set 5"
            Height          =   255
            Index           =   18
            Left            =   120
            TabIndex        =   190
            Top             =   2160
            Width           =   975
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio Set 4"
            Height          =   255
            Index           =   17
            Left            =   120
            TabIndex        =   189
            Top             =   1800
            Width           =   975
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio Set 3"
            Height          =   255
            Index           =   16
            Left            =   120
            TabIndex        =   188
            Top             =   1440
            Width           =   975
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio Set 2"
            Height          =   255
            Index           =   15
            Left            =   120
            TabIndex        =   187
            Top             =   1080
            Width           =   975
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio Set 1"
            Height          =   255
            Index           =   14
            Left            =   120
            TabIndex        =   186
            Top             =   720
            Width           =   975
         End
      End
      Begin MSAdodcLib.Adodc adoCompany 
         Height          =   330
         Left            =   4920
         Top             =   960
         Visible         =   0   'False
         Width           =   2205
         _ExtentX        =   3889
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoCompany"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.TextBox txtHighestAudioPeak 
         Height          =   315
         Left            =   9300
         TabIndex        =   171
         ToolTipText     =   "The level of the programme in respect to its lineup"
         Top             =   3240
         Width           =   735
      End
      Begin VB.CheckBox chkConvertedMaterial 
         Caption         =   "Converted Material"
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   3360
         TabIndex        =   170
         ToolTipText     =   "Are Titles present on the Tape?"
         Top             =   1860
         Width           =   1995
      End
      Begin VB.CheckBox chkIdentInfoCorrect 
         Caption         =   "Ident Info Correct"
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   60
         TabIndex        =   169
         ToolTipText     =   "Are Titles present on the Tape?"
         Top             =   1860
         Width           =   1995
      End
      Begin VB.Frame fraTimecode 
         Caption         =   "Timecode Details"
         Height          =   1635
         Left            =   120
         TabIndex        =   162
         Top             =   4740
         Width           =   4395
         Begin VB.TextBox txtvitclines 
            Height          =   315
            Left            =   1200
            MaxLength       =   50
            TabIndex        =   167
            Text            =   "19 & 21"
            ToolTipText     =   "What lines is the VITC on?"
            Top             =   720
            Width           =   1035
         End
         Begin VB.CheckBox chkFileTimecode 
            Alignment       =   1  'Right Justify
            Caption         =   "File Timecode Present?"
            Height          =   195
            Left            =   120
            TabIndex        =   166
            ToolTipText     =   "Is the timecode continuous throughout the tape?"
            Top             =   480
            Width           =   2085
         End
         Begin VB.CheckBox chkltcvitcmatch 
            Alignment       =   1  'Right Justify
            Caption         =   "File/VITC Match?"
            Height          =   195
            Left            =   2400
            TabIndex        =   165
            ToolTipText     =   "Do the LTC and the VITC match?"
            Top             =   1200
            Width           =   1815
         End
         Begin VB.CheckBox chkvitc 
            Alignment       =   1  'Right Justify
            Caption         =   "VITC Present?"
            Height          =   195
            Left            =   2400
            TabIndex        =   164
            ToolTipText     =   "Is there VITC on the tape?"
            Top             =   480
            Width           =   1815
         End
         Begin VB.CheckBox chkContinuous 
            Alignment       =   1  'Right Justify
            Caption         =   "VITC Throughout?"
            Height          =   195
            Left            =   2400
            TabIndex        =   163
            ToolTipText     =   "Is there VITC on the tape?"
            Top             =   840
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "VITC Lines"
            Height          =   255
            Index           =   65
            Left            =   180
            TabIndex        =   168
            Top             =   900
            Width           =   855
         End
      End
      Begin VB.Frame fraVideoLevels 
         Caption         =   "Vision Levels"
         Height          =   1635
         Left            =   4680
         TabIndex        =   150
         Top             =   4740
         Width           =   2835
         Begin VB.TextBox txtBlackProg 
            Height          =   315
            Left            =   1860
            TabIndex        =   156
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   1140
            Width           =   735
         End
         Begin VB.TextBox txtChromaProg 
            Height          =   315
            Left            =   1860
            TabIndex        =   155
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   780
            Width           =   735
         End
         Begin VB.TextBox txtBlackTest 
            Height          =   315
            Left            =   1140
            TabIndex        =   154
            Text            =   "0"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   1140
            Width           =   735
         End
         Begin VB.TextBox txtChromaTest 
            Height          =   315
            Left            =   1140
            TabIndex        =   153
            Text            =   "75%"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   780
            Width           =   735
         End
         Begin VB.TextBox txtVideoTest 
            Height          =   315
            Left            =   1140
            TabIndex        =   152
            Text            =   "100%"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   420
            Width           =   735
         End
         Begin VB.TextBox txtVideoProg 
            Height          =   315
            Left            =   1860
            TabIndex        =   151
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   420
            Width           =   735
         End
         Begin VB.Label lblCaption 
            Caption         =   "Black Levels"
            Height          =   255
            Index           =   63
            Left            =   60
            TabIndex        =   161
            Top             =   1200
            Width           =   915
         End
         Begin VB.Label lblCaption 
            Caption         =   "Chroma Levels"
            Height          =   255
            Index           =   62
            Left            =   60
            TabIndex        =   160
            Top             =   840
            Width           =   1155
         End
         Begin VB.Label lblCaption 
            Caption         =   "Video Levels"
            Height          =   255
            Index           =   61
            Left            =   60
            TabIndex        =   159
            Top             =   480
            Width           =   915
         End
         Begin VB.Label lblCaption 
            Caption         =   "Programme"
            Height          =   255
            Index           =   60
            Left            =   1860
            TabIndex        =   158
            Top             =   180
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Test"
            Height          =   255
            Index           =   59
            Left            =   1140
            TabIndex        =   157
            Top             =   180
            Width           =   435
         End
      End
      Begin VB.Frame fraSafeAreas 
         Caption         =   "Safe Areas"
         Height          =   1515
         Left            =   7620
         TabIndex        =   145
         Top             =   1620
         Width           =   4755
         Begin VB.CheckBox chkActionsafe43 
            Alignment       =   1  'Right Justify
            Caption         =   "Action Safe 4:3"
            Height          =   195
            Left            =   3060
            TabIndex        =   244
            ToolTipText     =   "Are all graphics 16:9 safe?"
            Top             =   900
            Width           =   1575
         End
         Begin VB.CheckBox chkActionsafe149 
            Alignment       =   1  'Right Justify
            Caption         =   "Action Safe 14:9"
            Height          =   195
            Left            =   3060
            TabIndex        =   243
            ToolTipText     =   "Are all graphics 16:9 safe?"
            Top             =   600
            Width           =   1575
         End
         Begin VB.CheckBox chkActionsafe169 
            Alignment       =   1  'Right Justify
            Caption         =   "Action Safe 16:9"
            Height          =   195
            Left            =   3060
            TabIndex        =   242
            ToolTipText     =   "Are all graphics 16:9 safe?"
            Top             =   300
            Width           =   1575
         End
         Begin VB.CheckBox chkCaptionsafe169 
            Alignment       =   1  'Right Justify
            Caption         =   "Caption Safe 16:9"
            Height          =   195
            Left            =   120
            TabIndex        =   149
            ToolTipText     =   "Are all graphics 16:9 safe?"
            Top             =   300
            Width           =   2475
         End
         Begin VB.CheckBox chkCaptionsafe149 
            Alignment       =   1  'Right Justify
            Caption         =   "Caption Safe 14:9"
            Height          =   195
            Left            =   120
            TabIndex        =   148
            ToolTipText     =   "Are all graphics 14:9 safe?"
            Top             =   600
            Width           =   2475
         End
         Begin VB.CheckBox chkCaptionsafe43protect 
            Alignment       =   1  'Right Justify
            Caption         =   "Caption Safe 4:3 EBU Protect"
            Height          =   195
            Left            =   120
            TabIndex        =   147
            ToolTipText     =   "Are all graphics 4:3 EBU Shoot to Protect safe?"
            Top             =   1200
            Width           =   2475
         End
         Begin VB.CheckBox chkCaptionsafe43ebu 
            Alignment       =   1  'Right Justify
            Caption         =   "Caption Safe 4:3 10%"
            Height          =   195
            Left            =   120
            TabIndex        =   146
            ToolTipText     =   "Are all graphics 4:3 EBU 10% safe?"
            Top             =   900
            Width           =   2475
         End
      End
      Begin VB.Frame fraAudioLevels 
         Caption         =   "Audio Levels"
         Height          =   6375
         Left            =   13020
         TabIndex        =   70
         Top             =   0
         Width           =   2655
         Begin VB.TextBox txtA24Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   118
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   6000
            Width           =   795
         End
         Begin VB.TextBox txtA24Test 
            Height          =   255
            Left            =   840
            TabIndex        =   117
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   6000
            Width           =   795
         End
         Begin VB.TextBox txtA23Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   116
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   5760
            Width           =   795
         End
         Begin VB.TextBox txtA23Test 
            Height          =   285
            Left            =   840
            TabIndex        =   115
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   5760
            Width           =   795
         End
         Begin VB.TextBox txtA22Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   114
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   5520
            Width           =   795
         End
         Begin VB.TextBox txtA22Test 
            Height          =   285
            Left            =   840
            TabIndex        =   113
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   5520
            Width           =   795
         End
         Begin VB.TextBox txtA21Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   112
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   5280
            Width           =   795
         End
         Begin VB.TextBox txtA21Test 
            Height          =   285
            Left            =   840
            TabIndex        =   111
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   5280
            Width           =   795
         End
         Begin VB.TextBox txtA20Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   110
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   5040
            Width           =   795
         End
         Begin VB.TextBox txtA20Test 
            Height          =   285
            Left            =   840
            TabIndex        =   109
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   5040
            Width           =   795
         End
         Begin VB.TextBox txtA19Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   108
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   4800
            Width           =   795
         End
         Begin VB.TextBox txtA19Test 
            Height          =   285
            Left            =   840
            TabIndex        =   107
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   4800
            Width           =   795
         End
         Begin VB.TextBox txtA18Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   106
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   4560
            Width           =   795
         End
         Begin VB.TextBox txtA18Test 
            Height          =   285
            Left            =   840
            TabIndex        =   105
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   4560
            Width           =   795
         End
         Begin VB.TextBox txtA17Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   104
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   4320
            Width           =   795
         End
         Begin VB.TextBox txtA17Test 
            Height          =   285
            Left            =   840
            TabIndex        =   103
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   4320
            Width           =   795
         End
         Begin VB.TextBox txtA16Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   102
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   4080
            Width           =   795
         End
         Begin VB.TextBox txtA16Test 
            Height          =   285
            Left            =   840
            TabIndex        =   101
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   4080
            Width           =   795
         End
         Begin VB.TextBox txtA15Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   100
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   3840
            Width           =   795
         End
         Begin VB.TextBox txtA15Test 
            Height          =   285
            Left            =   840
            TabIndex        =   99
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   3840
            Width           =   795
         End
         Begin VB.TextBox txtA14Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   98
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   3600
            Width           =   795
         End
         Begin VB.TextBox txtA14Test 
            Height          =   285
            Left            =   840
            LinkItem        =   "txtA2Test"
            TabIndex        =   97
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   3600
            Width           =   795
         End
         Begin VB.TextBox txtA13Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   96
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   3360
            Width           =   795
         End
         Begin VB.TextBox txtA13Test 
            Height          =   285
            Left            =   840
            TabIndex        =   95
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   3360
            Width           =   795
         End
         Begin VB.TextBox txtA12Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   94
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   3120
            Width           =   795
         End
         Begin VB.TextBox txtA11Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   93
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   2880
            Width           =   795
         End
         Begin VB.TextBox txtA10Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   92
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   2640
            Width           =   795
         End
         Begin VB.TextBox txtA9Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   91
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   2400
            Width           =   795
         End
         Begin VB.TextBox txtA12Test 
            Height          =   285
            Left            =   840
            TabIndex        =   90
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   3120
            Width           =   795
         End
         Begin VB.TextBox txtA11Test 
            Height          =   285
            Left            =   840
            TabIndex        =   89
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   2880
            Width           =   795
         End
         Begin VB.TextBox txtA10Test 
            Height          =   285
            Left            =   840
            TabIndex        =   88
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   2640
            Width           =   795
         End
         Begin VB.TextBox txtA9Test 
            Height          =   285
            Left            =   840
            TabIndex        =   87
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   2400
            Width           =   795
         End
         Begin VB.TextBox txtA8Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   86
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   2160
            Width           =   795
         End
         Begin VB.TextBox txtA7Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   85
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   1920
            Width           =   795
         End
         Begin VB.TextBox txtA6Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   84
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   1680
            Width           =   795
         End
         Begin VB.TextBox txtA5Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   83
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   1440
            Width           =   795
         End
         Begin VB.TextBox txtA4Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   82
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   1200
            Width           =   795
         End
         Begin VB.TextBox txtA3Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   81
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   960
            Width           =   795
         End
         Begin VB.TextBox txtA2Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   80
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   720
            Width           =   795
         End
         Begin VB.TextBox txtA8Test 
            Height          =   285
            Left            =   840
            TabIndex        =   79
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   2160
            Width           =   795
         End
         Begin VB.TextBox txtA7Test 
            Height          =   285
            Left            =   840
            TabIndex        =   78
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   1920
            Width           =   795
         End
         Begin VB.TextBox txtA6Test 
            Height          =   285
            Left            =   840
            TabIndex        =   77
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   1680
            Width           =   795
         End
         Begin VB.TextBox txtA5Test 
            Height          =   285
            Left            =   840
            TabIndex        =   76
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   1440
            Width           =   795
         End
         Begin VB.TextBox txtA4Test 
            Height          =   285
            Left            =   840
            TabIndex        =   75
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   1200
            Width           =   795
         End
         Begin VB.TextBox txtA3Test 
            Height          =   285
            Left            =   840
            TabIndex        =   74
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   960
            Width           =   795
         End
         Begin VB.TextBox txtA2Test 
            Height          =   285
            Left            =   840
            LinkItem        =   "txtA2Test"
            TabIndex        =   73
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   720
            Width           =   795
         End
         Begin VB.TextBox txtA1Prog 
            Height          =   285
            Left            =   1740
            TabIndex        =   72
            Text            =   "OK"
            ToolTipText     =   "The level of the programme in respect to its lineup"
            Top             =   480
            Width           =   795
         End
         Begin VB.TextBox txtA1Test 
            Height          =   285
            Left            =   840
            TabIndex        =   71
            Text            =   "-18 dBfs"
            ToolTipText     =   "The level of the line-up in respect to unity"
            Top             =   480
            Width           =   795
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 12"
            Height          =   255
            Index           =   101
            Left            =   120
            TabIndex        =   144
            Top             =   3180
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 11"
            Height          =   255
            Index           =   100
            Left            =   120
            TabIndex        =   143
            Top             =   2940
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 10"
            Height          =   255
            Index           =   99
            Left            =   120
            TabIndex        =   142
            Top             =   2700
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 9"
            Height          =   255
            Index           =   98
            Left            =   120
            TabIndex        =   141
            Top             =   2460
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 8"
            Height          =   255
            Index           =   58
            Left            =   120
            TabIndex        =   140
            Top             =   2220
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 2"
            Height          =   255
            Index           =   57
            Left            =   120
            TabIndex        =   139
            Top             =   780
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 3"
            Height          =   255
            Index           =   56
            Left            =   120
            TabIndex        =   138
            Top             =   1020
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 4"
            Height          =   255
            Index           =   55
            Left            =   120
            TabIndex        =   137
            Top             =   1260
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 5"
            Height          =   255
            Index           =   54
            Left            =   120
            TabIndex        =   136
            Top             =   1500
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 6"
            Height          =   255
            Index           =   53
            Left            =   120
            TabIndex        =   135
            Top             =   1740
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 7"
            Height          =   255
            Index           =   52
            Left            =   120
            TabIndex        =   134
            Top             =   1980
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 1"
            Height          =   255
            Index           =   50
            Left            =   120
            TabIndex        =   133
            Top             =   540
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Test"
            Height          =   255
            Index           =   49
            Left            =   1080
            TabIndex        =   132
            Top             =   240
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Programme"
            Height          =   255
            Index           =   48
            Left            =   1740
            TabIndex        =   131
            Top             =   240
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 13"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   130
            Top             =   3420
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 19"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   129
            Top             =   4860
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 18"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   128
            Top             =   4620
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 17"
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   127
            Top             =   4380
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 16"
            Height          =   255
            Index           =   4
            Left            =   120
            TabIndex        =   126
            Top             =   4140
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 15"
            Height          =   255
            Index           =   5
            Left            =   120
            TabIndex        =   125
            Top             =   3900
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 14"
            Height          =   255
            Index           =   6
            Left            =   120
            TabIndex        =   124
            Top             =   3660
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 20"
            Height          =   255
            Index           =   7
            Left            =   120
            TabIndex        =   123
            Top             =   5100
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 21"
            Height          =   255
            Index           =   8
            Left            =   120
            TabIndex        =   122
            Top             =   5340
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 22"
            Height          =   255
            Index           =   9
            Left            =   120
            TabIndex        =   121
            Top             =   5580
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 23"
            Height          =   255
            Index           =   10
            Left            =   120
            TabIndex        =   120
            Top             =   5820
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio 24"
            Height          =   255
            Index           =   11
            Left            =   120
            TabIndex        =   119
            Top             =   6060
            Width           =   855
         End
      End
      Begin VB.CheckBox chkTitles 
         Caption         =   "Titles"
         Height          =   195
         Left            =   60
         TabIndex        =   28
         ToolTipText     =   "Are Titles present on the Tape?"
         Top             =   2220
         Width           =   795
      End
      Begin VB.CheckBox chkSubtitles 
         Caption         =   "Subtitles"
         Height          =   195
         Left            =   60
         TabIndex        =   27
         ToolTipText     =   "Are there Subtitles on the Tape?"
         Top             =   2940
         Width           =   975
      End
      Begin VB.CheckBox chkCaptions 
         Caption         =   "Captions"
         Height          =   195
         Left            =   60
         TabIndex        =   26
         ToolTipText     =   "Are Captions present in the body of the Programme?"
         Top             =   2580
         Width           =   975
      End
      Begin VB.CheckBox chkLogos 
         Caption         =   "Logos"
         Height          =   195
         Left            =   60
         TabIndex        =   25
         ToolTipText     =   "Are there any Logos on the Tape"
         Top             =   3660
         Width           =   975
      End
      Begin VB.CheckBox chkTextless 
         Caption         =   "Textless"
         Height          =   195
         Left            =   60
         TabIndex        =   24
         ToolTipText     =   "Are Textless Elements present on the Tape?"
         Top             =   4020
         Width           =   975
      End
      Begin VB.CheckBox chkTrailers 
         Caption         =   "Trailers"
         Height          =   195
         Left            =   60
         TabIndex        =   23
         ToolTipText     =   "Are there any Trailers on the Tape?"
         Top             =   4380
         Width           =   975
      End
      Begin VB.TextBox txtDetailsLogos 
         Height          =   315
         Left            =   3360
         MaxLength       =   100
         TabIndex        =   22
         ToolTipText     =   "Notes regarding the Logos / what Language they are in"
         Top             =   3600
         Width           =   4155
      End
      Begin VB.TextBox txtDetailsTextless 
         Height          =   315
         Left            =   3360
         MaxLength       =   100
         TabIndex        =   21
         ToolTipText     =   "Notes regarding the Textless Elements"
         Top             =   3960
         Width           =   4155
      End
      Begin VB.TextBox txtDetailsTrailers 
         Height          =   315
         Left            =   3360
         MaxLength       =   100
         TabIndex        =   20
         ToolTipText     =   "Notes regarding Trailers"
         Top             =   4320
         Width           =   4155
      End
      Begin VB.ComboBox cmbReviewMachine 
         Height          =   315
         Left            =   1440
         TabIndex        =   19
         ToolTipText     =   "The machine the Tape was reviewed on"
         Top             =   720
         Width           =   1575
      End
      Begin VB.TextBox txtReviewJobID 
         Height          =   315
         Left            =   1440
         TabIndex        =   18
         ToolTipText     =   "The job number for the tech review"
         Top             =   1080
         Width           =   1575
      End
      Begin VB.ComboBox cmbTitlesLanguage 
         Height          =   315
         Left            =   3360
         TabIndex        =   17
         ToolTipText     =   "The machine the Tape was reviewed on"
         Top             =   2160
         Width           =   4155
      End
      Begin VB.ComboBox cmbCaptionsLanguage 
         Height          =   315
         Left            =   3360
         TabIndex        =   16
         ToolTipText     =   "The machine the Tape was reviewed on"
         Top             =   2520
         Width           =   4155
      End
      Begin VB.ComboBox cmbSubtitlesLanguage 
         Height          =   315
         Left            =   3360
         TabIndex        =   15
         ToolTipText     =   "The machine the Tape was reviewed on"
         Top             =   2880
         Width           =   4155
      End
      Begin VB.ComboBox cmbReviewUser 
         Height          =   315
         Left            =   4560
         TabIndex        =   14
         ToolTipText     =   "The machine the Tape was reviewed on"
         Top             =   720
         Width           =   2955
      End
      Begin VB.Frame fraDigitalParameters 
         Caption         =   "Pixel Counting"
         Height          =   2715
         Left            =   7620
         TabIndex        =   1
         Top             =   3660
         Width           =   3435
         Begin VB.CheckBox chkverticalcenteringcorrect 
            Alignment       =   1  'Right Justify
            Caption         =   "Vertical Centering Good?"
            Height          =   315
            Left            =   1050
            TabIndex        =   7
            ToolTipText     =   "Is the picture centred vertiically?"
            Top             =   1590
            Value           =   1  'Checked
            Width           =   2145
         End
         Begin VB.CheckBox chkhorizontalcenteringcorrect 
            Alignment       =   1  'Right Justify
            Caption         =   "Horiz Cent Good?"
            Height          =   315
            Left            =   1260
            TabIndex        =   6
            ToolTipText     =   "Is the picture centred horizontally?"
            Top             =   420
            Value           =   1  'Checked
            Width           =   1935
         End
         Begin VB.TextBox txtlastactivevertpixel 
            Height          =   315
            Left            =   2400
            TabIndex        =   5
            Text            =   "623"
            ToolTipText     =   "The last active line on field 2"
            Top             =   2280
            Width           =   795
         End
         Begin VB.TextBox txtfirstactivevertpixel 
            Height          =   315
            Left            =   2400
            TabIndex        =   4
            Text            =   "336"
            ToolTipText     =   "The first active line on field 2"
            Top             =   1920
            Width           =   795
         End
         Begin VB.TextBox txtlastactivehorizpixel 
            Height          =   315
            Left            =   2400
            TabIndex        =   3
            Text            =   "310"
            ToolTipText     =   "The last active line on field 1"
            Top             =   1080
            Width           =   795
         End
         Begin VB.TextBox txtfirstactivehorizpixel 
            Height          =   315
            Left            =   2400
            TabIndex        =   2
            Text            =   "23"
            ToolTipText     =   "The first active line on field 1"
            Top             =   720
            Width           =   795
         End
         Begin VB.Label lblParameters 
            Caption         =   "Worst case last active Pixel"
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   13
            Top             =   2340
            Width           =   1575
         End
         Begin VB.Label lblParameters 
            Caption         =   "Worst case first active Pixel"
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   12
            Top             =   1980
            Width           =   1575
         End
         Begin VB.Label lblParameters 
            Caption         =   "Worst case last active Pixel"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   11
            Top             =   1140
            Width           =   2235
         End
         Begin VB.Label lblCaption 
            Caption         =   "Vertical"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   69
            Left            =   120
            TabIndex        =   10
            Top             =   1620
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Horizontal"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   68
            Left            =   120
            TabIndex        =   9
            Top             =   450
            Width           =   855
         End
         Begin VB.Label lblParameters 
            Caption         =   "Worst case first active Pixel"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   8
            Top             =   780
            Width           =   2115
         End
      End
      Begin MSAdodcLib.Adodc adoTechReviews 
         Height          =   330
         Left            =   4980
         Top             =   1860
         Visible         =   0   'False
         Width           =   2580
         _ExtentX        =   4551
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoTechReviews"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoContact 
         Height          =   330
         Left            =   4620
         Top             =   1560
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoContact"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSComCtl2.DTPicker datReviewDate 
         Height          =   315
         Left            =   1440
         TabIndex        =   29
         ToolTipText     =   "When the Tech review was done"
         Top             =   360
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CheckBox        =   -1  'True
         Format          =   111673345
         CurrentDate     =   37999
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbReviewCompany 
         Bindings        =   "frmFileTechRev.frx":0D4D
         Height          =   315
         Left            =   4560
         TabIndex        =   30
         ToolTipText     =   "The company this review is for"
         Top             =   1080
         Width           =   2955
         DataFieldList   =   "name"
         _Version        =   196617
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   6376
         Columns(0).Caption=   "name"
         Columns(0).Name =   "name"
         Columns(0).DataField=   "name"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "companyID"
         Columns(1).Name =   "companyID"
         Columns(1).DataField=   "companyID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "accountcode"
         Columns(2).Name =   "accountcode"
         Columns(2).DataField=   "accountcode"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "telephone"
         Columns(3).Name =   "telephone"
         Columns(3).DataField=   "telephone"
         Columns(3).FieldLen=   256
         _ExtentX        =   5212
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "name"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbReviewContact 
         Bindings        =   "frmFileTechRev.frx":0D66
         Height          =   315
         Left            =   4560
         TabIndex        =   31
         ToolTipText     =   "The contact this review is for"
         Top             =   1440
         Width           =   2955
         DataFieldList   =   "name"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColorOdd    =   16761087
         RowHeight       =   370
         ExtraHeight     =   53
         Columns.Count   =   3
         Columns(0).Width=   6376
         Columns(0).Caption=   "name"
         Columns(0).Name =   "name"
         Columns(0).DataField=   "name"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "companyID"
         Columns(1).Name =   "companyID"
         Columns(1).DataField=   "companyID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "contactID"
         Columns(2).Name =   "contactID"
         Columns(2).DataField=   "contactID"
         Columns(2).FieldLen=   256
         _ExtentX        =   5212
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "name"
      End
      Begin VB.Label lblCaption 
         Caption         =   "iTunes Video Type"
         Height          =   195
         Index           =   29
         Left            =   60
         TabIndex        =   234
         Top             =   1500
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tech Review ID"
         Height          =   255
         Index           =   82
         Left            =   60
         TabIndex        =   228
         Top             =   60
         Width           =   2055
      End
      Begin VB.Label lblCaption 
         Caption         =   "Language"
         Height          =   255
         Index           =   28
         Left            =   1920
         TabIndex        =   227
         Top             =   3300
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Highest Audio Peak"
         Height          =   255
         Index           =   13
         Left            =   7800
         TabIndex        =   172
         Top             =   3300
         Width           =   1455
      End
      Begin VB.Label lblCaption 
         Caption         =   "Review Date"
         Height          =   255
         Index           =   38
         Left            =   60
         TabIndex        =   49
         Top             =   420
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Company"
         Height          =   255
         Index           =   39
         Left            =   3360
         TabIndex        =   48
         Top             =   1140
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Contact"
         Height          =   255
         Index           =   40
         Left            =   3360
         TabIndex        =   47
         Top             =   1500
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Language"
         Height          =   255
         Index           =   41
         Left            =   1920
         TabIndex        =   46
         Top             =   2220
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Language"
         Height          =   255
         Index           =   42
         Left            =   1920
         TabIndex        =   45
         Top             =   2580
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Language"
         Height          =   255
         Index           =   43
         Left            =   1920
         TabIndex        =   44
         Top             =   2940
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Notes"
         Height          =   255
         Index           =   44
         Left            =   1920
         TabIndex        =   43
         Top             =   3660
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Notes (Graded etc)"
         Height          =   255
         Index           =   45
         Left            =   1920
         TabIndex        =   42
         Top             =   4020
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Notes"
         Height          =   255
         Index           =   46
         Left            =   1920
         TabIndex        =   41
         Top             =   4380
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Review Machine"
         Height          =   195
         Index           =   47
         Left            =   60
         TabIndex        =   40
         Top             =   780
         Width           =   1395
      End
      Begin VB.Label lblReviewCompanyID 
         Height          =   255
         Left            =   360
         TabIndex        =   39
         Tag             =   "CLEARFIELDS"
         Top             =   4440
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label lblReviewContactID 
         Height          =   255
         Left            =   7500
         TabIndex        =   38
         Tag             =   "CLEARFIELDS"
         Top             =   1320
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Job # / Vendor ID"
         Height          =   195
         Index           =   79
         Left            =   60
         TabIndex        =   37
         Top             =   1140
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Reviewed by"
         Height          =   255
         Index           =   81
         Left            =   3360
         TabIndex        =   36
         Top             =   780
         Width           =   1095
      End
      Begin VB.Label lbltechrevID 
         Height          =   195
         Left            =   4560
         TabIndex        =   35
         Tag             =   "CLEARFIELDS"
         Top             =   420
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Techrev ID"
         Height          =   195
         Index           =   80
         Left            =   3360
         TabIndex        =   34
         Top             =   420
         Width           =   915
      End
      Begin VB.Label lblLastTechrevID 
         Height          =   195
         Left            =   4560
         TabIndex        =   33
         Tag             =   "CLEARFIELDS"
         Top             =   60
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Last Techrev ID"
         Height          =   195
         Index           =   83
         Left            =   3360
         TabIndex        =   32
         Top             =   60
         Width           =   1215
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnFaultAction 
      Height          =   855
      Left            =   12480
      TabIndex        =   236
      Top             =   11220
      Width           =   3315
      DataFieldList   =   "column 0"
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   4419
      Columns(0).Caption=   "value"
      Columns(0).Name =   "value"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   5847
      _ExtentY        =   1508
      _StockProps     =   77
      DataFieldToDisplay=   "column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnFaultType 
      Height          =   855
      Left            =   15840
      TabIndex        =   249
      Top             =   11220
      Width           =   3315
      DataFieldList   =   "column 0"
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   4419
      Columns(0).Caption=   "value"
      Columns(0).Name =   "value"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   5847
      _ExtentY        =   1508
      _StockProps     =   77
      DataFieldToDisplay=   "column 0"
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1035
      Index           =   12
      Left            =   13020
      TabIndex        =   195
      Top             =   11940
      Width           =   5595
   End
End
Attribute VB_Name = "frmFileTechRev"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_Framerate As Integer

Private Sub chk5PointCheck_Click()

    If chk5PointCheck.Value <> 0 Then
        FileFullQCFieldsOff
        txtiTunesVendorID.Visible = False
        grdFaults.Columns("Fixed").Visible = True
        grdFaults.Columns("faultaction").Visible = False
    End If
    
End Sub

Private Sub chkiTunesFinal_Click()

    If chk5PointCheck.Value <> 0 Then
        FileiTunesQCFieldsOn
        txtiTunesVendorID.Visible = True
    End If
    
End Sub

Private Sub chkiTunesReview_Click()

    If chkiTunesReview.Value <> 0 Then
        FileiTunesQCFieldsOn
        txtiTunesVendorID.Visible = True
        grdFaults.Columns("fixed").Visible = False
        grdFaults.Columns("faultaction").Visible = True
    End If
    
End Sub

Private Sub chkRuntimeReview_Click()

    If chkSpotCheckRev.Value <> 0 Then
        FileFullQCFieldsOn
        txtiTunesVendorID.Visible = False
        grdFaults.Columns("Fixed").Visible = True
        grdFaults.Columns("faultaction").Visible = False
    End If
    
End Sub

Private Sub chkSpotCheckRev_Click()

    If chkSpotCheckRev.Value <> 0 Then
        FileFullQCFieldsOn
        txtiTunesVendorID.Visible = False
        grdFaults.Columns("Fixed").Visible = True
        grdFaults.Columns("faultaction").Visible = False
    End If
    
End Sub

Private Sub cmbCaptionsLanguage_DropDown()

PopulateCombo "language", cmbCaptionsLanguage

End Sub

Private Sub cmbEBU128LoudnessRangeSet1_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessRangeSet1
End Sub

Private Sub cmbEBU128LoudnessRangeSet2_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessRangeSet2
End Sub

Private Sub cmbEBU128LoudnessRangeSet3_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessRangeSet3
End Sub

Private Sub cmbEBU128LoudnessRangeSet4_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessRangeSet4
End Sub

Private Sub cmbEBU128LoudnessRangeSet5_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessRangeSet5
End Sub

Private Sub cmbEBU128LoudnessRangeSet6_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessRangeSet6
End Sub

Private Sub cmbEBU128LoudnessRangeSet7_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessRangeSet7
End Sub

Private Sub cmbEBU128LoudnessRangeSet8_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessRangeSet8
End Sub

'Private Sub cmbEBU128LoudnessRangeSet9_GotFocus()
'PopulateCombo "ebu-loudness", cmbEBU128LoudnessRangeSet9
'End Sub
'
'Private Sub cmbEBU128LoudnessRangeSet10_GotFocus()
'PopulateCombo "ebu-loudness", cmbEBU128LoudnessRangeSet10
'End Sub
'
Private Sub cmbEBU128LoudnessSet1_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessSet1
End Sub

Private Sub cmbEBU128LoudnessSet2_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessSet2
End Sub

Private Sub cmbEBU128LoudnessSet3_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessSet3
End Sub

Private Sub cmbEBU128LoudnessSet4_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessSet4
End Sub

Private Sub cmbEBU128LoudnessSet5_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessSet5
End Sub

Private Sub cmbEBU128LoudnessSet6_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessSet6
End Sub

Private Sub cmbEBU128LoudnessSet7_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessSet7
End Sub

Private Sub cmbEBU128LoudnessSet8_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128LoudnessSet8
End Sub

'Private Sub cmbEBU128LoudnessSet9_GotFocus()
'PopulateCombo "ebu-loudness", cmbEBU128LoudnessSet9
'End Sub
'
'Private Sub cmbEBU128LoudnessSet10_GotFocus()
'PopulateCombo "ebu-loudness", cmbEBU128LoudnessSet10
'End Sub
'
Private Sub cmbEBU128MaxShortLoundnessSet1_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128MaxShortLoundnessSet1
End Sub

Private Sub cmbEBU128MaxShortLoundnessSet2_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128MaxShortLoundnessSet2
End Sub

Private Sub cmbEBU128MaxShortLoundnessSet3_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128MaxShortLoundnessSet3
End Sub

Private Sub cmbEBU128MaxShortLoundnessSet4_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128MaxShortLoundnessSet4
End Sub

Private Sub cmbEBU128MaxShortLoundnessSet5_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128MaxShortLoundnessSet5
End Sub

Private Sub cmbEBU128MaxShortLoundnessSet6_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128MaxShortLoundnessSet6
End Sub

Private Sub cmbEBU128MaxShortLoundnessSet7_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128MaxShortLoundnessSet7
End Sub

Private Sub cmbEBU128MaxShortLoundnessSet8_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128MaxShortLoundnessSet8
End Sub

'Private Sub cmbEBU128MaxShortLoundnessSet9_GotFocus()
'PopulateCombo "ebu-loudness", cmbEBU128MaxShortLoundnessSet9
'End Sub
'
'Private Sub cmbEBU128MaxShortLoundnessSet10_GotFocus()
'PopulateCombo "ebu-loudness", cmbEBU128MaxShortLoundnessSet10
'End Sub
'
Private Sub cmbEBU128TruePeakSet1_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128TruePeakSet1
End Sub

Private Sub cmbEBU128TruePeakSet2_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128TruePeakSet2
End Sub

Private Sub cmbEBU128TruePeakSet3_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128TruePeakSet3
End Sub

Private Sub cmbEBU128TruePeakSet4_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128TruePeakSet4
End Sub

Private Sub cmbEBU128TruePeakSet5_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128TruePeakSet5
End Sub

Private Sub cmbEBU128TruePeakSet6_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128TruePeakSet6
End Sub

Private Sub cmbEBU128TruePeakSet7_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128TruePeakSet7
End Sub

Private Sub cmbEBU128TruePeakSet8_GotFocus()
PopulateCombo "ebu-loudness", cmbEBU128TruePeakSet8
End Sub

'Private Sub cmbEBU128TruePeakSet9_GotFocus()
'PopulateCombo "ebu-loudness", cmbEBU128TruePeakSet9
'End Sub
'
'Private Sub cmbEBU128TruePeakSet10_GotFocus()
'PopulateCombo "ebu-loudness", cmbEBU128TruePeakSet10
'End Sub
'
Private Sub cmbForcedLanguage_DropDown()

PopulateCombo "language", cmbForcedLanguage

End Sub

Private Sub cmbiTunesVideoType_DropDown()

PopulateCombo "iTunesVideoType", cmbiTunesVideoType

End Sub

Private Sub cmbReviewCompany_Click()

lblReviewCompanyID.Caption = cmbReviewCompany.Columns("companyID").Text

End Sub

Private Sub cmbReviewContact_Click()

lblReviewContactID.Caption = cmbReviewContact.Columns("contactID").Text

End Sub

Private Sub cmbReviewContact_DropDown()

If lblReviewCompanyID.Caption = "" Then
    NoCompanySelectedMessage
    Exit Sub
End If

Dim l_strSQL As String
l_strSQL = "SELECT company.companyID, contact.contactID, contact.name FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((company.companyID)=" & lblReviewCompanyID.Caption & ")) ORDER BY contact.name;"

adoContact.ConnectionString = g_strConnection
adoContact.RecordSource = l_strSQL
adoContact.Refresh

End Sub

Private Sub cmbReviewMachine_DragOver(Source As Control, X As Single, Y As Single, state As Integer)

PopulateCombo "machine", cmbReviewMachine

End Sub

Private Sub cmbReviewUser_DropDown()

PopulateCombo "operator", cmbReviewUser

End Sub

Private Sub cmbSubtitlesLanguage_DropDown()

PopulateCombo "language", cmbSubtitlesLanguage

End Sub

Private Sub cmbTechReviewList_Click()

ShowFileTechRev (Val(cmbTechReviewList.Columns("techrevID").Text))

End Sub

Private Sub cmbTitlesLanguage_DropDown()

PopulateCombo "language", cmbTitlesLanguage

End Sub

Private Sub cmdClose_Click()

If CheckFileTechReviewChanges(Val(lbltechrevID.Caption)) = True Then
    If MsgBox("Unsaved Changes have been Detected" & vbCrLf & "Are you sure you wish to Close the Form", vbYesNo, "Changes Detected") = vbNo Then
        Exit Sub
    End If
End If

Me.Hide
ShowClipControl Val(frmClipControl.txtClipID.Text)
Unload frmFileTechRev
frmClipControl.WindowState = vbMaximized

End Sub

Private Sub cmdDisneyQC_Click()

Dim l_strFilename As String, l_strSQL As String

'Routines to output this tech review into a Zodiak QC Excel sheet and save it in the location of the asset.

If MsgBox("Are you Sure", vbYesNo, "Output Techrev to Disney QC Sheet") = vbYes Then
    
    MDIForm1.dlgMain.InitDir = g_strFilePDFLocation
    MDIForm1.dlgMain.Filter = "Excel Files|*.xlsx"
    MDIForm1.dlgMain.Filename = frmClipControl.txtReference.Text & "_" & frmClipControl.txtClipID.Text & "_QC.xlsx"
    
    MDIForm1.dlgMain.ShowSave
    
    l_strFilename = MDIForm1.dlgMain.Filename
       
    l_strSQL = "INSERT INTO Excel_TEchrev_Output_Request (Request_Type, TechrevID, EventID, NewFilePath, RequestName, RequestEmail) VALUES ("
    l_strSQL = l_strSQL & "'DISNEY', "
    l_strSQL = l_strSQL & lbltechrevID.Caption & ", "
    l_strSQL = l_strSQL & frmClipControl.txtClipID.Text & ", "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strFilename) & "',"
    l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strFullUserName) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strUserEmailAddress) & "');"
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    MsgBox "This procedure has been changed." & vbCrLf & "This process is now handled by a Request Handler" & vbCrLf & "Your Request has been Submitted"
End If

End Sub

Private Sub cmdDropTechReview_Click()

Dim l_intResponse As Integer

l_intResponse = MsgBox("Are you sure you wish to delete this tech review?", vbYesNo, "Delet Tech Review")
If l_intResponse = vbYes Then
    DeleteFileTechReview (Val(lbltechrevID.Caption))
    Me.Hide
    ShowClipControl Val(frmClipControl.txtClipID.Text)
    Unload Me
    frmClipControl.WindowState = vbMaximized
End If

End Sub

Private Sub cmdMakeTechReview_Click()
MakeFileTechReview
End Sub

Private Sub cmdP4MReview_Click()

Dim l_strFilename As String, l_strSQL As String, l_strNewFilename As String, l_lngDotCount As Long

'Routines to output this tech review into a P4M Excel sheet and save it in the location of the asset.

If MsgBox("Are you Sure", vbYesNo, "Output P4M Excel Pre-Qual Sheet") = vbYes Then
    
    l_lngDotCount = InStr(frmClipControl.txtClipfilename.Text, ".")
    Do While InStr(l_lngDotCount, frmClipControl.txtClipfilename.Text, ".") > l_lngDotCount
        l_lngDotCount = InStr(l_lngDotCount, frmClipControl.txtClipfilename.Text, ".")
    Loop
    MDIForm1.dlgMain.InitDir = GetData("library", "subtitle", "libraryID", frmClipControl.lblLibraryID.Caption) & IIf(frmClipControl.txtAltFolder.Text <> "", "\" & frmClipControl.txtAltFolder.Text, "")
    MDIForm1.dlgMain.Filter = "Excel Files|*.xlsx"
    MDIForm1.dlgMain.Filename = Left(frmClipControl.txtClipfilename.Text, l_lngDotCount - 1) & ".xlsx"
    
    MDIForm1.dlgMain.ShowSave
    
    l_strFilename = MDIForm1.dlgMain.Filename
       
    l_strSQL = "INSERT INTO Excel_Techrev_Output_Request (Request_Type, TechrevID, EventID, NewFilePath, RequestName, RequestEmail) VALUES ("
    l_strSQL = l_strSQL & "'P4M', "
    l_strSQL = l_strSQL & lbltechrevID.Caption & ", "
    l_strSQL = l_strSQL & frmClipControl.txtClipID.Text & ", "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strFilename) & "',"
    l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strFullUserName) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strUserEmailAddress) & "');"
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    MsgBox "Your Output Request has been Submitted"
End If

End Sub

Private Sub cmdPrint_Click()

Dim l_strReportFile As String, l_strSelectionFormula As String

If chkSpotCheckRev.Value = True Then
    If InStr(GetData("company", "cetaclientcode", "companyID", frmClipControl.lblCompanyID.Caption), "/techrevincitunes") > 0 Then
        l_strReportFile = g_strLocationOfCrystalReportFiles & "Filetechrev_Full_itunes.rpt"
    Else
        l_strReportFile = g_strLocationOfCrystalReportFiles & "Filetechrev_Full.rpt"
    End If
ElseIf chkiTunesReview.Value = True Then
    l_strReportFile = g_strLocationOfCrystalReportFiles & "Filetechrev_iTunes.rpt"
ElseIf chkiTunesFinal.Value = True Then
    l_strReportFile = g_strLocationOfCrystalReportFiles & "Filetechrev_iTunes_Final.rpt"
Else
    l_strReportFile = g_strLocationOfCrystalReportFiles & "Filetechrev_Runtime.rpt"
End If
l_strSelectionFormula = "{techrev.techrevID} = " & lbltechrevID.Caption
PrintCrystalReport l_strReportFile, l_strSelectionFormula, g_blnPreviewReport

End Sub

Private Sub cmdRedFinalCutMarkers_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_lngCount As Long
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long, l_strSQL As String
Dim l_strComment As String, l_strTimecodeStart As String, l_strTimecodeDuration As String, l_strColour As String, l_strTimecodeStop As String

MDIForm1.dlgMain.InitDir = g_strExcelOrderLocation
MDIForm1.dlgMain.Filter = "All Files|*.*"
MDIForm1.dlgMain.ShowOpen

l_strExcelFileName = MDIForm1.dlgMain.Filename
l_strExcelFileTitle = MDIForm1.dlgMain.FileTitle

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_lngCount = InStr(l_strExcelFileTitle, ".")
l_strExcelFileTitle = Left(l_strExcelFileTitle, l_lngCount - 1)

l_strExcelSheetName = l_strExcelFileTitle
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = 2

Do While GetXLData(l_lngXLRowCounter, 1) <> ""

    l_strComment = Trim(" " & GetXLData(l_lngXLRowCounter, 4))
    l_strTimecodeStart = GetXLData(l_lngXLRowCounter, 5)
    l_strTimecodeDuration = GetXLData(l_lngXLRowCounter, 6)
    l_strColour = GetXLData(l_lngXLRowCounter, 7)
    If FrameNumber(l_strTimecodeDuration, m_Framerate) > 0 Then
        l_strTimecodeStop = Timecode_Add(l_strTimecodeStart, l_strTimecodeDuration, m_Framerate)
    Else
        l_strTimecodeStop = ""
    End If
    l_strSQL = "INSERT INTO techrevfault (techrevID, timecode, endtimecode, description, faultgrade) VALUES ("
    l_strSQL = l_strSQL & lbltechrevID.Caption & ", "
    l_strSQL = l_strSQL & "'" & l_strTimecodeStart & "', "
    l_strSQL = l_strSQL & "'" & l_strTimecodeStop & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strComment) & "', "
    Select Case UCase(l_strColour)
        Case "RED"
            l_strSQL = l_strSQL & "1);"
        Case "ORANGE"
            l_strSQL = l_strSQL & "2);"
        Case "YELLOW"
            l_strSQL = l_strSQL & "3);"
        Case "Turquoise"
            l_strSQL = l_strSQL & "4);"
        Case "BLUE"
            l_strSQL = l_strSQL & "5);"
        Case Else
            l_strSQL = l_strSQL & "5);"
    End Select
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError

    l_lngXLRowCounter = l_lngXLRowCounter + 1

Loop

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing
adoFaults.Recordset.Requery

Beep

End Sub

Private Sub cmdSave_Click()

'If g_blnRedNetwork <> 0 Then
    SaveFileTechReview
    
    If GetData("events", "md5checksum", "eventid", Val(frmClipControl.txtClipID.Text)) <> frmClipControl.txtMD5Checksum.Text Then SaveClip True
    
    ShowFileTechRev Val(lbltechrevID.Caption)
'Else
'    MsgBox "You must be ion a Red network machine to save tech reviews", vbCritical
'End If

End Sub

Private Sub cmdSwitchLineupLevel_Click()

If txtA1Test.Text = "-18dBfs" Then txtA1Test.Text = "-20dBfs" Else If txtA1Test.Text = "-20dBfs" Then txtA1Test.Text = "-18dBfs"
If txtA2Test.Text = "-18dBfs" Then txtA2Test.Text = "-20dBfs" Else If txtA2Test.Text = "-20dBfs" Then txtA2Test.Text = "-18dBfs"
If txtA3Test.Text = "-18dBfs" Then txtA3Test.Text = "-20dBfs" Else If txtA3Test.Text = "-20dBfs" Then txtA3Test.Text = "-18dBfs"
If txtA4Test.Text = "-18dBfs" Then txtA4Test.Text = "-20dBfs" Else If txtA4Test.Text = "-20dBfs" Then txtA4Test.Text = "-18dBfs"
If txtA5Test.Text = "-18dBfs" Then txtA5Test.Text = "-20dBfs" Else If txtA5Test.Text = "-20dBfs" Then txtA5Test.Text = "-18dBfs"
If txtA6Test.Text = "-18dBfs" Then txtA6Test.Text = "-20dBfs" Else If txtA6Test.Text = "-20dBfs" Then txtA6Test.Text = "-18dBfs"
If txtA7Test.Text = "-18dBfs" Then txtA7Test.Text = "-20dBfs" Else If txtA7Test.Text = "-20dBfs" Then txtA7Test.Text = "-18dBfs"
If txtA8Test.Text = "-18dBfs" Then txtA8Test.Text = "-20dBfs" Else If txtA8Test.Text = "-20dBfs" Then txtA8Test.Text = "-18dBfs"
If txtA9Test.Text = "-18dBfs" Then txtA9Test.Text = "-20dBfs" Else If txtA9Test.Text = "-20dBfs" Then txtA9Test.Text = "-18dBfs"
If txtA10Test.Text = "-18dBfs" Then txtA10Test.Text = "-20dBfs" Else If txtA10Test.Text = "-20dBfs" Then txtA10Test.Text = "-18dBfs"
If txtA11Test.Text = "-18dBfs" Then txtA11Test.Text = "-20dBfs" Else If txtA11Test.Text = "-20dBfs" Then txtA11Test.Text = "-18dBfs"
If txtA12Test.Text = "-18dBfs" Then txtA12Test.Text = "-20dBfs" Else If txtA12Test.Text = "-20dBfs" Then txtA12Test.Text = "-18dBfs"
If txtA13Test.Text = "-18dBfs" Then txtA13Test.Text = "-20dBfs" Else If txtA13Test.Text = "-20dBfs" Then txtA13Test.Text = "-18dBfs"
If txtA14Test.Text = "-18dBfs" Then txtA14Test.Text = "-20dBfs" Else If txtA14Test.Text = "-20dBfs" Then txtA14Test.Text = "-18dBfs"
If txtA15Test.Text = "-18dBfs" Then txtA15Test.Text = "-20dBfs" Else If txtA15Test.Text = "-20dBfs" Then txtA15Test.Text = "-18dBfs"
If txtA16Test.Text = "-18dBfs" Then txtA16Test.Text = "-20dBfs" Else If txtA16Test.Text = "-20dBfs" Then txtA16Test.Text = "-18dBfs"
If txtA17Test.Text = "-18dBfs" Then txtA17Test.Text = "-20dBfs" Else If txtA17Test.Text = "-20dBfs" Then txtA17Test.Text = "-18dBfs"
If txtA18Test.Text = "-18dBfs" Then txtA18Test.Text = "-20dBfs" Else If txtA18Test.Text = "-20dBfs" Then txtA18Test.Text = "-18dBfs"
If txtA19Test.Text = "-18dBfs" Then txtA19Test.Text = "-20dBfs" Else If txtA19Test.Text = "-20dBfs" Then txtA19Test.Text = "-18dBfs"
If txtA20Test.Text = "-18dBfs" Then txtA20Test.Text = "-20dBfs" Else If txtA20Test.Text = "-20dBfs" Then txtA20Test.Text = "-18dBfs"
If txtA21Test.Text = "-18dBfs" Then txtA21Test.Text = "-20dBfs" Else If txtA21Test.Text = "-20dBfs" Then txtA21Test.Text = "-18dBfs"
If txtA22Test.Text = "-18dBfs" Then txtA22Test.Text = "-20dBfs" Else If txtA22Test.Text = "-20dBfs" Then txtA22Test.Text = "-18dBfs"
If txtA23Test.Text = "-18dBfs" Then txtA23Test.Text = "-20dBfs" Else If txtA23Test.Text = "-20dBfs" Then txtA23Test.Text = "-18dBfs"
If txtA24Test.Text = "-18dBfs" Then txtA24Test.Text = "-20dBfs" Else If txtA24Test.Text = "-20dBfs" Then txtA24Test.Text = "-18dBfs"

End Sub

Private Sub cmdTechReviewCopyFaults_Click()
CopyFileTechReviewFaults lbltechrevID.Caption
End Sub

Private Sub cmdTechReviewPdf_Click()

If chkSpotCheckRev.Value = True Then
    If InStr(GetData("company", "cetaclientcode", "companyID", frmClipControl.lblCompanyID.Caption), "/techrevincitunes") > 0 Then
        ReportToPDF g_strLocationOfCrystalReportFiles & "\filetechrev_Full_itunes.rpt", "{techrev.techrevID} = " & lbltechrevID.Caption, g_strFilePDFLocation & "\" & frmClipControl.txtReference.Text & "_" & frmClipControl.txtClipID.Text & ".pdf"
    Else
        ReportToPDF g_strLocationOfCrystalReportFiles & "\filetechrev_Full.rpt", "{techrev.techrevID} = " & lbltechrevID.Caption, g_strFilePDFLocation & "\" & frmClipControl.txtReference.Text & "_" & frmClipControl.txtClipID.Text & ".pdf"
    End If
ElseIf chkiTunesReview.Value = True Then
    ReportToPDF g_strLocationOfCrystalReportFiles & "\Filetechrev_iTunes.rpt", "{techrev.techrevID} = " & lbltechrevID.Caption, g_strFilePDFLocation & "\" & frmClipControl.txtReference.Text & "_" & frmClipControl.txtClipID.Text & ".pdf"
ElseIf chkiTunesFinal.Value = True Then
    ReportToPDF g_strLocationOfCrystalReportFiles & "\Filetechrev_iTunes_Final.rpt", "{techrev.techrevID} = " & lbltechrevID.Caption, g_strFilePDFLocation & "\" & frmClipControl.txtReference.Text & "_" & frmClipControl.txtClipID.Text & ".pdf"
Else
    ReportToPDF g_strLocationOfCrystalReportFiles & "\Filetechrev_Runtime.rpt", "{techrev.techrevID} = " & lbltechrevID.Caption, g_strFilePDFLocation & "\" & frmClipControl.txtReference.Text & "_" & frmClipControl.txtClipID.Text & ".pdf"
End If

End Sub

Private Sub cmdVDMSQC_Click()

Dim l_strFilename As String, l_strSQL As String

'Routines to output this tech review into a Zodiak QC Excel sheet and save it in the location of the asset.

If MsgBox("Are you Sure", vbYesNo, "Output Techrev to VD QC Sheet") = vbYes Then
    
    MDIForm1.dlgMain.InitDir = g_strFilePDFLocation
    MDIForm1.dlgMain.Filter = "Excel Files|*.xlsx"
    MDIForm1.dlgMain.Filename = frmClipControl.txtReference.Text & "_" & frmClipControl.txtClipID.Text & "_QC.xlsx"
    
    MDIForm1.dlgMain.ShowSave
    
    l_strFilename = MDIForm1.dlgMain.Filename
       
    l_strSQL = "INSERT INTO Excel_TEchrev_Output_Request (Request_Type, TechrevID, EventID, NewFilePath, RequestName, RequestEmail) VALUES ("
    l_strSQL = l_strSQL & "'VDMS', "
    l_strSQL = l_strSQL & lbltechrevID.Caption & ", "
    l_strSQL = l_strSQL & frmClipControl.txtClipID.Text & ", "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strFilename) & "',"
    l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strFullUserName) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strUserEmailAddress) & "');"
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    MsgBox "This procedure has been changed." & vbCrLf & "This process is now handled by a Request Handler" & vbCrLf & "Your Request has been Submitted"
End If

End Sub

Private Sub cmdZodiakQC_Click()

Dim l_strFilename As String, l_strSQL As String

'Routines to output this tech review into a Zodiak QC Excel sheet and save it in the location of the asset.

If MsgBox("Are you Sure", vbYesNo, "Output Techrev to Zodiak QC Sheet") = vbYes Then
    
    MDIForm1.dlgMain.InitDir = g_strFilePDFLocation
    MDIForm1.dlgMain.Filter = "Excel Files|*.xlsx"
    MDIForm1.dlgMain.Filename = frmClipControl.txtReference.Text & "_" & frmClipControl.txtClipID.Text & "_QC.xlsx"
    
    MDIForm1.dlgMain.ShowSave
    
    l_strFilename = MDIForm1.dlgMain.Filename
       
    l_strSQL = "INSERT INTO Excel_TEchrev_Output_Request (Request_Type, TechrevID, EventID, NewFilePath, RequestName, RequestEmail) VALUES ("
    l_strSQL = l_strSQL & "'ZODIAK', "
    l_strSQL = l_strSQL & lbltechrevID.Caption & ", "
    l_strSQL = l_strSQL & frmClipControl.txtClipID.Text & ", "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strFilename) & "',"
    l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strFullUserName) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strUserEmailAddress) & "');"
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    MsgBox "This procedure has been changed." & vbCrLf & "This process is now handled by a Request Handler" & vbCrLf & "Your Request has been Submitted"
End If

End Sub

Private Sub Form_Load()

Dim l_rstTechRev As ADODB.Recordset, l_strSQL As String, l_lngtechrevID As Long, l_intFoundTechrev As Integer

l_intFoundTechrev = 0
'check if Tech Review exists

With frmClipControl
    If Trim(" " & .txtClipfilename.Text) <> "" And Trim(" " & .txtMD5Checksum.Text) <> "" And Trim(" " & .lblFileSize.Caption) <> "" Then
    
        l_strSQL = "SELECT techrevID FROM techrev WHERE system_deleted = 0 AND filename = '" & .txtClipfilename.Text & "' AND bigfilesize = " & Format(.lblFileSize.Caption, "#") & " AND md5checksum = '" & .txtMD5Checksum.Text & "' AND companyID = " & Val(frmClipControl.lblCompanyID.Caption) & ";"
    
        Set l_rstTechRev = ExecuteSQL(l_strSQL, g_strExecuteError)
        CheckForSQLError
        
        If Not l_rstTechRev.EOF Then
            
            'Found a tech review for that tape
            l_rstTechRev.MoveLast
            l_lngtechrevID = l_rstTechRev("techrevID")
            lblLastTechrevID.Caption = l_lngtechrevID
            l_intFoundTechrev = 2
            
        End If
        
        l_rstTechRev.Close
        Set l_rstTechRev = Nothing
    
    End If

End With

If l_intFoundTechrev = 0 Then
    
    l_strSQL = "SELECT techrevID FROM techrev WHERE system_deleted = 0 AND eventID = " & frmClipControl.txtClipID.Text & " ORDER BY techrevID"
    
    Set l_rstTechRev = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If Not l_rstTechRev.EOF Then
        
        'Found a tech review for that tape
        l_rstTechRev.MoveLast
        l_lngtechrevID = l_rstTechRev("techrevID")
        lblLastTechrevID.Caption = l_lngtechrevID
        l_intFoundTechrev = 1
    
    End If
    
    l_rstTechRev.Close
    Set l_rstTechRev = Nothing

End If

    
If l_intFoundTechrev <> 0 Then

    'Show the lastest Techrevies.
    ShowFileTechRev (l_lngtechrevID)

Else

    l_lngtechrevID = 0
    lbltechrevID.Caption = l_lngtechrevID
    lblLastTechrevID.Caption = l_lngtechrevID
    lbltechrevID.Visible = False
    grdFaults.Visible = False
    fraTechReview.Visible = False
    cmdMakeTechReview.Enabled = True
    cmdTechReviewPdf.Enabled = False
    cmdTechReviewCopyFaults.Enabled = False
    fraTechReviewConclusions.Visible = False

End If

If l_intFoundTechrev = 1 Then
    l_strSQL = "SELECT techrevID,reviewdate,reviewUser FROM techrev WHERE system_deleted = 0 AND eventID = " & frmClipControl.txtClipID.Text & ";"
Else ' l_intFoundTechrev = 2 then
    l_strSQL = "SELECT techrevID,reviewdate,reviewUser FROM techrev WHERE system_deleted = 0 AND filename = '" & frmClipControl.txtClipfilename.Text & "' AND bigfilesize = " & Format(frmClipControl.lblFileSize.Caption, "#") & " AND md5checksum = '" & frmClipControl.txtMD5Checksum.Text & "' and companyID = " & Val(frmClipControl.lblCompanyID.Caption) & ";"
'Else
'    l_strSQL = "SELECT techrevID,reviewdate,reviewUser FROM techrev WHERE eventID = 0"
End If

adoTechReviews.ConnectionString = g_strConnection
adoTechReviews.RecordSource = l_strSQL
adoTechReviews.Refresh

l_strSQL = "SELECT description FROM xref WHERE category = 'fault' ORDER BY description"
adoFaultsList.ConnectionString = g_strConnection
adoFaultsList.RecordSource = l_strSQL
adoFaultsList.Refresh

l_strSQL = "SELECT name, accountcode, telephone, companyID FROM company WHERE (iscustomer = 1 OR isprospective = 1) ORDER BY name"

adoCompany.ConnectionString = g_strConnection
adoCompany.RecordSource = l_strSQL
adoCompany.Refresh

Select Case frmClipControl.cmbFrameRate.Text

    Case "25"
        m_Framerate = TC_25
    Case "29.97"
        m_Framerate = TC_29
    Case "30"
        m_Framerate = TC_30
    Case "24", "23.98"
        m_Framerate = TC_24
    Case Else
        m_Framerate = TC_UN

End Select

PopulateCombo "faultaction", ddnFaultAction
grdFaults.Columns("faultaction").DropDownHwnd = ddnFaultAction.hWnd
PopulateCombo "faultgrade", ddnFaultGrade
grdFaults.Columns("grade").DropDownHwnd = ddnFaultGrade.hWnd
PopulateCombo "faulttype", ddnFaultType
grdFaults.Columns("faulttype").DropDownHwnd = ddnFaultType.hWnd

'If g_blnRedNetwork <> 0 Then
'    cmdSave.Enabled = True
'    cmdTechReviewPdf.Enabled = True
'Else
'    cmdSave.Enabled = False
'    cmdTechReviewPdf.Enabled = False
'End If
'
CenterForm Me
Exit Sub

End Sub

Private Sub Form_Resize()

On Error GoTo ERROR_SUB

grdFaults.Height = Me.ScaleHeight - grdFaults.Top - 120
'fraButtons.Top = Me.ScaleHeight - fraButtons.Height - 120

ERROR_SUB:

End Sub

Private Sub grdFaults_BeforeUpdate(Cancel As Integer)

grdFaults.Columns("techrevID").Text = lbltechrevID.Caption

End Sub

Private Sub grdFaults_BtnClick()

If grdFaults.Columns(grdFaults.Col).Name = "fixeddate" Then
    grdFaults.ActiveCell.Text = Now
    grdFaults.Update
End If

End Sub

Private Sub grdFaults_InitColumnProps()

grdFaults.Columns("description").DropDownHwnd = ddnFaultsList.hWnd

End Sub

Private Sub grdFaults_KeyPress(KeyAscii As Integer)

If grdFaults.Col = 1 Or grdFaults.Col = 2 Then
    If Len(grdFaults.ActiveCell.Text) < 2 Then
        grdFaults.ActiveCell.Text = "00:00:00:00"
    Else
        Timecode_Check_Grid grdFaults, KeyAscii, m_Framerate
    End If
End If

End Sub

Private Sub grdFaults_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

If grdFaults.Col = 1 Or grdFaults.Col = 2 Then
    If grdFaults.ActiveCell.Text = "" Then
        grdFaults.ActiveCell.Text = "00:00:00:00"
    End If
    grdFaults.ActiveCell.SelLength = 0
    grdFaults.ActiveCell.SelStart = 0
End If

End Sub

Private Sub Text1_Change()

End Sub

Private Sub txtReviewJobID_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    cmbReviewCompany.Text = Trim(" " & GetData("job", "CompanyName", "jobID", txtReviewJobID.Text))
    lblReviewCompanyID.Caption = Trim(" " & GetData("job", "companyID", "jobID", txtReviewJobID.Text))
    cmbReviewContact.Text = Trim(" " & GetData("job", "ContactName", "jobID", txtReviewJobID.Text))
    lblReviewContactID.Caption = Trim(" " & GetData("job", "contactID", "jobID", txtReviewJobID.Text))
End If
End Sub
