VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Begin VB.Form frmAddResource 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Add resource to existing job"
   ClientHeight    =   6645
   ClientLeft      =   3660
   ClientTop       =   5490
   ClientWidth     =   12045
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAddResource.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6645
   ScaleWidth      =   12045
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtSearch 
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   4920
      TabIndex        =   4
      Top             =   540
      Width           =   2535
   End
   Begin VB.CommandButton cmdAddResource 
      Caption         =   "Add"
      Height          =   315
      Left            =   9480
      TabIndex        =   5
      ToolTipText     =   "Save changes to this job"
      Top             =   6240
      Width           =   1155
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   10800
      TabIndex        =   6
      ToolTipText     =   "Close this form"
      Top             =   6240
      Width           =   1155
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdResources 
      Height          =   5175
      Left            =   120
      TabIndex        =   0
      Top             =   960
      Width           =   11775
      _Version        =   196617
      BorderStyle     =   0
      stylesets.count =   1
      stylesets(0).Name=   "conflict"
      stylesets(0).ForeColor=   0
      stylesets(0).BackColor=   255
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmAddResource.frx":6852
      BeveColorScheme =   1
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   20770
      _ExtentY        =   9128
      _StockProps     =   79
      Caption         =   "Resources and Availability"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSubCategory1 
      Height          =   315
      Left            =   4920
      TabIndex        =   3
      ToolTipText     =   "Choose a Sub Category"
      Top             =   120
      Width           =   2595
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   5424
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4577
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCategory 
      Height          =   315
      Left            =   1200
      TabIndex        =   1
      ToolTipText     =   "Choose Category"
      Top             =   120
      Width           =   2415
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   5424
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbDepartment 
      Height          =   315
      Left            =   1200
      TabIndex        =   2
      ToolTipText     =   "Choose a Sub Category"
      Top             =   540
      Width           =   2415
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   5424
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.Label lblCaption 
      Caption         =   "Category"
      Height          =   315
      Index           =   4
      Left            =   180
      TabIndex        =   11
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Sub Category"
      Height          =   315
      Index           =   0
      Left            =   3780
      TabIndex        =   10
      Top             =   120
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Department"
      Height          =   315
      Index           =   1
      Left            =   180
      TabIndex        =   9
      Top             =   540
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Search Name"
      Height          =   315
      Index           =   2
      Left            =   3780
      TabIndex        =   8
      Top             =   540
      Width           =   1035
   End
   Begin VB.Label lblJobID 
      Height          =   195
      Left            =   120
      TabIndex        =   7
      Top             =   6420
      Width           =   1215
   End
End
Attribute VB_Name = "frmAddResource"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Public Sub cmbCategory_Click()

Dim l_strCategory As String

l_strCategory = "subcategory" & LCase(cmbCategory.Text)

cmbSubCategory1.RemoveAll
cmbSubCategory1.Text = ""

PopulateCombo l_strCategory, cmbSubCategory1

PopulateResourceGrid

End Sub

Private Sub cmbDepartment_Click()
PopulateResourceGrid
End Sub

Private Sub cmbSubCategory1_Click()

PopulateResourceGrid

End Sub

Private Sub cmdAddResource_Click()

Dim l_intQuestion As Integer

l_intQuestion = MsgBox("Are you sure you want to add this resource (" & grdResources.Columns("resource").Text & ") to your job?", vbQuestion + vbYesNo)
If l_intQuestion = vbNo Then Exit Sub

Dim l_lngJobID As Long
Dim l_datStartDate As Date
Dim l_datEndDate As Date
Dim l_lngResourceID As Long
Dim l_strJobStatus As String

l_lngResourceID = Val(grdResources.Columns("resourceID").Text)
l_lngJobID = Val(lblJobID.Caption)
l_datStartDate = GetData("job", "startdate", "jobID", l_lngJobID)
l_datEndDate = GetData("job", "enddate", "jobID", l_lngJobID)
l_strJobStatus = GetData("job", "fd_status", "jobID", l_lngJobID)

CreateNewResourceSchedule l_lngJobID, l_lngResourceID, l_datStartDate, l_datEndDate, l_strJobStatus

grdResources.Refresh

End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub Form_Load()

MakeLookLikeOffice Me

PopulateCombo "resourcecategory", cmbCategory
PopulateCombo "department", cmbDepartment
CenterForm Me

PopulateResourceGrid

End Sub

Sub PopulateResourceGrid()

Dim l_strSQL As String
l_strSQL = "SELECT resourceID, name as 'Resource', Description , serialnumber AS 'Serial No', barcode AS 'Barcode', location AS 'Current Location' FROM resource WHERE subcategory1 Like '" & QuoteSanitise(cmbSubCategory1.Text) & "%' AND category LIKE '" & QuoteSanitise(cmbCategory.Text) & "%' "

If cmbDepartment.Text <> "" Then
    l_strSQL = l_strSQL & " AND department LIKE '" & QuoteSanitise(cmbDepartment.Text) & "%'"
End If

If txtSearch.Text <> "" Then
    l_strSQL = l_strSQL & " AND name LIKE '%" & QuoteSanitise(txtSearch.Text) & "%'"
End If

l_strSQL = l_strSQL & " AND (fd_status IS NULL OR (fd_status <> 'DELETED' AND fd_status <> 'SOLD'))"


l_strSQL = l_strSQL & " ORDER BY name"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set grdResources.DataSource = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing



End Sub

Private Sub grdResources_DblClick()
cmdAddResource.Value = True
End Sub

Private Sub grdResources_RowLoaded(ByVal Bookmark As Variant)

Dim l_lngJobID As Long
Dim l_datStartDate As Date
Dim l_datEndDate As Date
Dim l_lngResourceID As Long

l_lngResourceID = Val(grdResources.Columns("resourceID").Text)
l_lngJobID = Val(lblJobID.Caption)
l_datStartDate = GetData("job", "startdate", "jobID", l_lngJobID)
l_datEndDate = GetData("job", "enddate", "jobID", l_lngJobID)

Dim l_lngConflict As Long
l_lngConflict = IsConflict(l_lngJobID, l_lngResourceID, l_datStartDate, l_datEndDate, False)

If l_lngConflict <> 0 Then
    grdResources.Columns("resource").CellStyleSet "conflict"
    grdResources.Columns("description").Text = GetData("job", "companyname", "jobID", GetData("resourceschedule", "jobID", "resourcescheduleID", l_lngConflict))
    
End If

End Sub

Private Sub txtSearch_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then
    KeyAscii = 0
    PopulateResourceGrid
End If

End Sub


