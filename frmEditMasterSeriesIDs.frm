VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmEditMasterSeriesIDs 
   Caption         =   "Edit Master Series IDs"
   ClientHeight    =   8730
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   22920
   Icon            =   "frmEditMasterSeriesIDs.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8730
   ScaleWidth      =   22920
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdCleanUp 
      Caption         =   "Clean Up!"
      Height          =   375
      Left            =   12060
      TabIndex        =   7
      Top             =   180
      Visible         =   0   'False
      Width           =   2955
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnChannel 
      Bindings        =   "frmEditMasterSeriesIDs.frx":08CA
      Height          =   1755
      Left            =   3060
      TabIndex        =   6
      Top             =   3120
      Width           =   5775
      DataFieldList   =   "name"
      ListAutoValidate=   0   'False
      MaxDropDownItems=   20
      _Version        =   196617
      ColumnHeaders   =   0   'False
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   2
      Columns(0).Width=   9710
      Columns(0).Caption=   "Name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   8819
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   10186
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "name"
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear"
      Height          =   315
      Left            =   7020
      TabIndex        =   5
      ToolTipText     =   "Close this form"
      Top             =   180
      Width           =   1335
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdSeriesIDs 
      Bindings        =   "frmEditMasterSeriesIDs.frx":08E3
      Height          =   7935
      Left            =   180
      TabIndex        =   1
      Top             =   660
      Width           =   22620
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   14
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "masterseriestitleID"
      Columns(0).Name =   "masterseriestitleID"
      Columns(0).DataField=   "masterseriestitleID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   2646
      Columns(1).Caption=   "seriesID"
      Columns(1).Name =   "seriesID"
      Columns(1).DataField=   "seriesID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "SerialNumber"
      Columns(2).Name =   "SerialNumber"
      Columns(2).DataField=   "SerialNumber"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   8811
      Columns(3).Caption=   "title"
      Columns(3).Name =   "title"
      Columns(3).DataField=   "title"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "companyID"
      Columns(4).Name =   "companyID"
      Columns(4).DataField=   "companyID"
      Columns(4).FieldLen=   256
      Columns(5).Width=   6509
      Columns(5).Caption=   "company"
      Columns(5).Name =   "company"
      Columns(5).DataField=   "Column 4"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "EpisodeNumber"
      Columns(6).Name =   "EpisodeNumber"
      Columns(6).DataField=   "EpisodeNumber"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "oldtitle"
      Columns(7).Name =   "oldtitle"
      Columns(7).DataField=   "oldtitle"
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "cdate"
      Columns(8).Name =   "cdate"
      Columns(8).DataField=   "cdate"
      Columns(8).FieldLen=   256
      Columns(9).Width=   3043
      Columns(9).Caption=   "# of Episodes"
      Columns(9).Name =   "numberofepisodes"
      Columns(9).DataField=   "numberofepisodes"
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "mdate"
      Columns(10).Name=   "mdate"
      Columns(10).DataField=   "mdate"
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "cuser"
      Columns(11).Name=   "cuser"
      Columns(11).DataField=   "cuser"
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "muser"
      Columns(12).Name=   "muser"
      Columns(12).DataField=   "muser"
      Columns(12).FieldLen=   256
      Columns(13).Width=   2646
      Columns(13).Caption=   "Old Series ID"
      Columns(13).Name=   "oldseriesID"
      Columns(13).DataField=   "oldseriesID"
      Columns(13).FieldLen=   256
      _ExtentX        =   39899
      _ExtentY        =   13996
      _StockProps     =   79
      Caption         =   "Master Series IDs"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   8640
      TabIndex        =   0
      ToolTipText     =   "Close this form"
      Top             =   180
      Width           =   1335
   End
   Begin MSAdodcLib.Adodc adoSeriesIDs 
      Height          =   330
      Left            =   180
      Top             =   360
      Width           =   2100
      _ExtentX        =   3704
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "SeriesIDs"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Bindings        =   "frmEditMasterSeriesIDs.frx":08FE
      Height          =   315
      Left            =   3600
      TabIndex        =   2
      ToolTipText     =   "Company for Specific Rates"
      Top             =   180
      Width           =   3315
      DataFieldList   =   "name"
      _Version        =   196617
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5609
      Columns(0).Caption=   "Company Name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   5847
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "name"
   End
   Begin MSAdodcLib.Adodc adoCompany 
      Height          =   390
      Left            =   9300
      Top             =   0
      Visible         =   0   'False
      Width           =   2340
      _ExtentX        =   4128
      _ExtentY        =   688
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCompany"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label lblCompanyID 
      BackColor       =   &H0000FFFF&
      Height          =   195
      Left            =   10080
      TabIndex        =   4
      Top             =   480
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      Height          =   255
      Index           =   2
      Left            =   2460
      TabIndex        =   3
      Top             =   240
      Width           =   1035
   End
End
Attribute VB_Name = "frmEditMasterSeriesIDs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_blnDelete As Boolean

Private Sub cmbCompany_Click()

Dim l_strSQL As String
lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

l_strSQL = "SELECT * FROM masterseriestitle WHERE companyID = " & Val(lblCompanyID.Caption) & " ORDER BY companyID, title;"
adoSeriesIDs.RecordSource = l_strSQL
adoSeriesIDs.ConnectionString = g_strConnection
adoSeriesIDs.Refresh
adoSeriesIDs.Caption = adoSeriesIDs.Recordset.RecordCount & " Items"

End Sub

Private Sub cmdCleanUp_Click()

adoSeriesIDs.Recordset.MoveFirst

Do While Not adoSeriesIDs.Recordset.EOF

    Replace adoSeriesIDs.Recordset("seriesID"), vbCrLf, ""
    Replace adoSeriesIDs.Recordset("title"), vbCrLf, ""
    adoSeriesIDs.Recordset.Update
    adoSeriesIDs.Recordset.MoveNext
    
Loop

End Sub

Private Sub cmdClear_Click()

Dim l_strSQL As String

lblCompanyID.Caption = ""
cmbCompany.Text = ""

l_strSQL = "SELECT * FROM masterseriestitle ORDER BY companyID, title, EpisodeNumber;"
adoSeriesIDs.RecordSource = l_strSQL
adoSeriesIDs.ConnectionString = g_strConnection
adoSeriesIDs.Refresh
adoSeriesIDs.Caption = adoSeriesIDs.Recordset.RecordCount & " Items"

End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub ddnChannel_CloseUp()

grdSeriesIDs.Columns("companyID").Text = ddnChannel.Columns("companyID").Text

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch2 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch2 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

l_strSQL = "SELECT name, companyID FROM company WHERE system_active = 1 ORDER BY name;"

With l_rstSearch2
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch2.ActiveConnection = Nothing

Set ddnChannel.DataSource = l_rstSearch2
grdSeriesIDs.Columns("company").DropDownHwnd = ddnChannel.hWnd

l_conSearch.Close
Set l_conSearch = Nothing

l_strSQL = "SELECT name, companyID FROM company ORDER BY name"

adoCompany.ConnectionString = g_strConnection
adoCompany.RecordSource = l_strSQL
adoCompany.Refresh

l_strSQL = "SELECT * FROM masterseriestitle ORDER BY companyID, title;"
adoSeriesIDs.RecordSource = l_strSQL
adoSeriesIDs.ConnectionString = g_strConnection
adoSeriesIDs.Refresh

adoSeriesIDs.Caption = adoSeriesIDs.Recordset.RecordCount & " Items"

End Sub

Private Sub Form_Resize()

grdSeriesIDs.Height = Me.ScaleHeight - grdSeriesIDs.Top - 240

End Sub

Private Sub grdSeriesIDs_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdSeriesIDs_AfterUpdate(RtnDispErrMsg As Integer)

If m_blnDelete = True Then Exit Sub
adoSeriesIDs.Caption = adoSeriesIDs.Recordset.RecordCount & " Items"

End Sub

Private Sub grdSeriesIDs_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdSeriesIDs_BeforeUpdate(Cancel As Integer)

If m_blnDelete = True Then Exit Sub

If Val(lblCompanyID.Caption) <> 0 Then
    grdSeriesIDs.Columns("CompanyID").Text = Val(lblCompanyID.Caption)
End If
If Val(grdSeriesIDs.Columns("CompanyID").Text) = 0 Then
    MsgBox "You must select a oompany for a new Series ID", vbCritical, "Error..."
    Cancel = 1
End If

If grdSeriesIDs.Columns("cdate").Text = "" Then grdSeriesIDs.Columns("cdate").Text = Format(Now, "yyyy,mm,dd hh:nn:ss")
If grdSeriesIDs.Columns("cuser").Text = "" Then grdSeriesIDs.Columns("cuser").Text = g_strFullUserName
grdSeriesIDs.Columns("mdate").Text = Format(Now, "yyyy,mm,dd hh:nn:ss")
grdSeriesIDs.Columns("muser").Text = g_strFullUserName
grdSeriesIDs.Columns("title").Text = Trim(grdSeriesIDs.Columns("title").Text)
grdSeriesIDs.Columns("title").Text = Replace(grdSeriesIDs.Columns("title").Text, vbCrLf, "")
grdSeriesIDs.Columns("seriesID").Text = Trim(grdSeriesIDs.Columns("seriesID").Text)
grdSeriesIDs.Columns("seriesID").Text = Replace(grdSeriesIDs.Columns("seriesID").Text, vbCrLf, "")

'If grdSeriesIDs.Columns("seriesID").Text = "" Or grdSeriesIDs.Columns("seriesID").Text = "999999" Then
'    If MsgBox("Do you wish CETA to generate the Next Series ID Number?", vbYesNo, "Series ID is not set") = vbYes Then
'        grdSeriesIDs.Columns("SeriesID").Text = GetNextSequence("MasterSeriesID")
'    End If
'End If

End Sub

Private Sub grdSeriesIDs_RowLoaded(ByVal Bookmark As Variant)
grdSeriesIDs.Columns("Company").Text = GetData("company", "name", "companyID", Val(grdSeriesIDs.Columns("companyID").Text))
End Sub
