Attribute VB_Name = "modPurchase"
Option Explicit
Sub CancelPurchaseOrder(lp_lngPurchaseHeaderID As Long)

    Dim l_strSQL As String

    'CANCEL THE PO
    l_strSQL = "UPDATE purchaseheader SET fd_status = 'Cancelled', cancelleddate = '" & FormatSQLDate(Now()) & "', cancelleduser = '" & g_strUserInitials & "' WHERE purchaseheaderID = '" & lp_lngPurchaseHeaderID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'REMOVE VALUE FROM THE PO LINES
    l_strSQL = "UPDATE purchasedetail SET unitcharge = 0, total = 0, vat = 0, totalincludingvat = 0 WHERE purchaseheaderID = '" & lp_lngPurchaseHeaderID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    Dim l_lngDespatchID As Long
    l_lngDespatchID = GetData("purchaseheader", "incomingdespatchID", "purchaseheaderID", lp_lngPurchaseHeaderID)
    
    'CANCEL THE INCOMING DESPATCH
    l_strSQL = "UPDATE despatch SET description = 'P.O. Cancelled By " & g_strUserInitials & "', deliverymethod = 'Cancelled' WHERE despatchID  = '" & l_lngDespatchID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_lngDespatchID = GetData("purchaseheader", "outgoingdespatchID", "purchaseheaderID", lp_lngPurchaseHeaderID)
    
    'CANCEL THE OUT GOING DESPATCH
    l_strSQL = "UPDATE despatch SET description = 'P.O. Cancelled By " & g_strUserInitials & "', deliverymethod = 'Cancelled' WHERE despatchID  = '" & l_lngDespatchID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    
    'REMOVE THE DETAILS ON THE HIRE ITEMS IN RESOURCE SCHEDULE
    l_strSQL = "UPDATE resourceschedule SET purchaseorderID = NULL WHERE purchaseorderID = '" & lp_lngPurchaseHeaderID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    Dim l_lngJobID As Long
    l_lngJobID = GetData("purchaseheader", "jobID", "purchaseheaderID", lp_lngPurchaseHeaderID)
    
    If l_lngJobID <> 0 Then AddJobHistory l_lngJobID, "Cancelled P/O (ID: " & lp_lngPurchaseHeaderID & ")"
    
    
        
End Sub

Sub NoPurchaseOrderSelectedMessage()
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    MsgBox "Please select a valid purchase order first", vbExclamation
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Function RequestPurchaseOrderAuthorisation(lp_lngPurchaseOrderNumber As Long)

SetData "purchaseheader", "authorisationrequestdate", "purchaseheaderID", lp_lngPurchaseOrderNumber, FormatSQLDate(Now())
SetData "purchaseheader", "authorisationrequestuser", "purchaseheaderID", lp_lngPurchaseOrderNumber, g_strUserInitials
SetData "purchaseheader", "fd_status", "purchaseheaderID", lp_lngPurchaseOrderNumber, "Awaiting Authorisation"

Dim l_strDepartment As String
l_strDepartment = GetData("purchaseheader", "department", "purchaseheaderID", lp_lngPurchaseOrderNumber)

Dim l_strDepartmentEmail As String
l_strDepartmentEmail = GetData("department", "email", "name", l_strDepartment)

Dim l_dblMaxOrdervalue As Double
l_dblMaxOrdervalue = GetData("cetauser", "purchaseorderlimit", "cetauserID", g_lngUserID)

If l_dblMaxOrdervalue = 0 Then
    l_dblMaxOrdervalue = GetData("department", "purchaseorderlimit", "name", l_strDepartment)
End If

Dim l_strOrderType As String
l_strOrderType = GetData("purchaseheader", "ordertype", "purchaseheaderID", lp_lngPurchaseOrderNumber)

Dim l_intMsg As Integer, l_strBodyText As String

If (LCase(l_strOrderType) = "capex" And Not CheckAccess("/authorisecapexorder", True)) Or (LCase(l_strOrderType) = "service contract" And Not CheckAccess("/authorisecontractorder", True)) Then
            
    l_intMsg = MsgBox("Sorry, you can not authorise this order yourself as it you do not have permission to authorise " & l_strOrderType & " orders." & vbCrLf & vbCrLf & "There is a designated CETA user who does have permission to authorise these orders." & vbCrLf & vbCrLf & "Do you want to send an email to " & g_strAdvancedPurchaseApproveEmail & " now?", vbExclamation + vbYesNo)
    
    If l_intMsg = vbYes Then
        
        Dim l_strAdvancedApprovalName As String
        l_strAdvancedApprovalName = GetData("cetauser", "fullname", "email", g_strAdvancedPurchaseApproveEmail)
        If l_strAdvancedApprovalName = "" Then l_strAdvancedApprovalName = g_strAdvancedPurchaseApproveEmail
        
        l_strBodyText = "Dear " & l_strAdvancedApprovalName & ","
        l_strBodyText = l_strBodyText & vbCrLf & ""
        l_strBodyText = l_strBodyText & vbCrLf & "I have just tried to authorise a purchase order (" & lp_lngPurchaseOrderNumber & ") to " & GetData("purchaseheader", "companyname", "purchaseheaderID", lp_lngPurchaseOrderNumber) & " for:"
        l_strBodyText = l_strBodyText & vbCrLf & ""
        l_strBodyText = l_strBodyText & vbCrLf & Replace(GetPODetail(lp_lngPurchaseOrderNumber), "/ ", vbCrLf)
        l_strBodyText = l_strBodyText & vbCrLf & ""
        l_strBodyText = l_strBodyText & vbCrLf & "Total Order Value = " & g_strCurrency & Format(GetTotalCostsForPurchaseOrder(lp_lngPurchaseOrderNumber, "total"), "00.00")
        l_strBodyText = l_strBodyText & vbCrLf & ""
        l_strBodyText = l_strBodyText & vbCrLf & "This is a formal request for authorisation."
        l_strBodyText = l_strBodyText & vbCrLf
        l_strBodyText = l_strBodyText & vbCrLf & g_strFullUserName
        
        SendSMTPMail g_strAdvancedPurchaseApproveEmail, l_strAdvancedApprovalName, "CETA Facilities Manager :: " & l_strOrderType & " Authorisation Request " & lp_lngPurchaseOrderNumber, "", l_strBodyText, False, g_strUserEmailAddress, g_strFullUserName
    
    End If
    
    Exit Function
    
End If


Dim l_strSQL As String
l_strSQL = "SELECT sum(total) FROM purchasedetail WHERE purchaseheaderID = '" & lp_lngPurchaseOrderNumber & "';"

Dim l_dblOrderValue As Double

l_dblOrderValue = GetCount(l_strSQL)


'check the jobs and projects are valid
Dim l_lngJobID As Long
l_lngJobID = Val(GetData("purchaseheader", "jobID", "purchaseheaderID", lp_lngPurchaseOrderNumber))
If l_lngJobID <> 0 Then
    'we need to check here to make sure the job hasnt been costed

End If

Dim l_lngProjectNumber As Long
l_lngProjectNumber = Val(GetData("purchaseheader", "projectnumber", "purchaseheaderID", lp_lngPurchaseOrderNumber))

If l_lngProjectNumber <> 0 Then
    'we need to check here to make sure the job hasnt been costed

End If



If l_dblOrderValue > l_dblMaxOrdervalue Then
   
    l_intMsg = MsgBox("Sorry, you can not authorise this order yourself as it is more than your current authorisation limit." & vbCrLf & vbCrLf & "Please contact your head of department, and ask them to authorise Purchase Order ID #" & lp_lngPurchaseOrderNumber & vbCrLf & vbCrLf & "Do you want to send an email to " & l_strDepartmentEmail & " now?", vbExclamation + vbYesNo)
    If l_intMsg = vbYes Then
        'OpenBrowser CStr("mailto:" & l_strDepartmentEmail & "?subject=CETA Facilities Manager :: P/O Authorisation Request " & lp_lngPurchaseOrderNumber & " &body=I have just tried to authorise a purchase order (" & lp_lngPurchaseOrderNumber & ") to " & GetData("purchaseheader", "companyname", "purchaseheaderID", lp_lngPurchaseOrderNumber) & " for " & Replace(GetPODetail(lp_lngPurchaseOrderNumber), Chr(10), " &body=") & " with a total of �" & Format(GetTotalCostsForPurchaseOrder(lp_lngPurchaseOrderNumber, "total"), "00.00") & " &body= " & " &body=This is a formal request for authorisation." & " &body= " & " &body=Regards," & " &body= " & " &body=" & g_strFullUSerName)
        'OpenBrowser CStr("mailto:" & l_strDepartmentEmail & "?subject=CETA Facilities Manager :: P/O Authorisation Request " & lp_lngPurchaseOrderNumber & " &body=I have just tried to authorise a purchase order (" & lp_lngPurchaseOrderNumber & ") to " & GetData("purchaseheader", "companyname", "purchaseheaderID", lp_lngPurchaseOrderNumber) & " for: &body= &body= " & Replace(GetPODetail(lp_lngPurchaseOrderNumber), "/", " &body=") & " &body= &body= Total Order Value = " & g_strCurrency & Format(GetTotalCostsForPurchaseOrder(lp_lngPurchaseOrderNumber, "total"), "00.00") & " &body= " & " &body=This is a formal request for authorisation." & " &body= " & " &body=Regards," & " &body= " & " &body=" & g_strFullUSerName)
    
    
        
        l_strBodyText = "Dear " & GetData("cetauser", "fullname", "email", l_strDepartmentEmail) & ","
        l_strBodyText = l_strBodyText & vbCrLf & ""
        l_strBodyText = l_strBodyText & vbCrLf & "I have just tried to authorise a purchase order (" & lp_lngPurchaseOrderNumber & ") to " & GetData("purchaseheader", "companyname", "purchaseheaderID", lp_lngPurchaseOrderNumber) & " for:"
        l_strBodyText = l_strBodyText & vbCrLf & ""
        l_strBodyText = l_strBodyText & vbCrLf & Replace(GetPODetail(lp_lngPurchaseOrderNumber), "/ ", vbCrLf)
        l_strBodyText = l_strBodyText & vbCrLf & ""
        l_strBodyText = l_strBodyText & vbCrLf & "Total Order Value = " & g_strCurrency & Format(GetTotalCostsForPurchaseOrder(lp_lngPurchaseOrderNumber, "total"), "00.00")
        l_strBodyText = l_strBodyText & vbCrLf & ""
        l_strBodyText = l_strBodyText & vbCrLf & "This is a formal request for authorisation."
        l_strBodyText = l_strBodyText & vbCrLf
        l_strBodyText = l_strBodyText & vbCrLf & g_strFullUserName

        
        SendSMTPMail l_strDepartmentEmail, GetData("cetauser", "fullname", "email", l_strDepartmentEmail), "CETA Facilities Manager :: " & l_strOrderType & " Authorisation Request " & lp_lngPurchaseOrderNumber, "", l_strBodyText, False, g_strUserEmailAddress, g_strFullUserName
        
    
    End If
    Exit Function
End If

'----------------------------------

Dim l_lngAuthNumber As Long
l_lngAuthNumber = GetNextPurchaseOrderAuthorisationNumber(l_strDepartment)

If l_lngAuthNumber <> 0 Then
    SetData "purchaseheader", "authorisationnumber", "purchaseheaderID", lp_lngPurchaseOrderNumber, l_lngAuthNumber
    SetData "purchaseheader", "authoriseddate", "purchaseheaderID", lp_lngPurchaseOrderNumber, FormatSQLDate(Now())
    SetData "purchaseheader", "authorisedbyuser", "purchaseheaderID", lp_lngPurchaseOrderNumber, g_strUserInitials
    SetData "purchaseheader", "fd_status", "purchaseheaderID", lp_lngPurchaseOrderNumber, "Authorised"
    
    Dim l_strBookedInitials As String
    l_strBookedInitials = GetData("purchaseheader", "cuser", "purchaseheaderID", lp_lngPurchaseOrderNumber)
    
    If l_strBookedInitials <> g_strUserInitials Then
        Dim l_strOwnerEmail As String
        l_strOwnerEmail = GetData("cetauser", "email", "initials", l_strBookedInitials)
        l_intMsg = MsgBox("Do you want to send an email to " & l_strOwnerEmail & " now?", vbExclamation + vbYesNo)
                
        If l_intMsg = vbYes Then
            
            
            l_strBodyText = "I have now authorised your purchase order ID (" & lp_lngPurchaseOrderNumber & ") for:"
            l_strBodyText = l_strBodyText & vbCrLf & Replace(GetPODetail(lp_lngPurchaseOrderNumber), "/ ", vbCrLf)
            l_strBodyText = l_strBodyText & vbCrLf & " with a total of " & g_strCurrency & Format(GetTotalCostsForPurchaseOrder(lp_lngPurchaseOrderNumber, "total"), "00.00")
            l_strBodyText = l_strBodyText & vbCrLf
            l_strBodyText = l_strBodyText & vbCrLf & g_strFullUserName
    
            
            SendSMTPMail l_strOwnerEmail, l_strOwnerEmail, "CETA Facilities Manager :: P/O Auth # " & l_lngAuthNumber, "", l_strBodyText, False, g_strUserEmailAddress, g_strFullUserName
            
            'OpenBrowser "mailto:" & l_strOwnerEmail & "?subject=CETA Facilities Manager :: P/O Auth # " & l_lngAuthNumber & " &body=I have now authorised your purchase order ID (" & lp_lngPurchaseOrderNumber & ") for " & Replace(GetPODetail(lp_lngPurchaseOrderNumber), Chr(10), " &body=") & " with a total of " & g_strCurrency & Format(GetTotalCostsForPurchaseOrder(lp_lngPurchaseOrderNumber, "total"), "00.00") & " &body= &body= &body=Regards, &body= &body=" & g_strFullUSerName
        End If
    End If
    
    RequestPurchaseOrderAuthorisation = l_lngAuthNumber
        
    UpdateDespatchForPurchaseOrder lp_lngPurchaseOrderNumber

End If

End Function

Function GetNextPurchaseOrderAuthorisationNumber(lp_strDepartment As String)

    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_lngNextAuthNumber  As Long
    
    If g_optUseDepartmentalPurchaseAuthorisationNumbers <> 0 Then
    
        
        Dim l_strSQL As String
        
        l_strSQL = "SELECT * FROM department WHERE name = '" & lp_strDepartment & "';"
        
        Dim l_rstDepartment As ADODB.Recordset
        Set l_rstDepartment = ExecuteSQL(l_strSQL, g_strExecuteError)
        
        CheckForSQLError
        
        If Not l_rstDepartment.EOF Then
            l_rstDepartment.MoveFirst
            l_lngNextAuthNumber = l_rstDepartment("nextpurchaseorderauthorisationnumber")
            
            
            l_strSQL = "UPDATE department SET nextpurchaseorderauthorisationnumber = '" & l_lngNextAuthNumber + 1 & "', muser = '" & g_strUserInitials & "', mdate = '" & FormatSQLDate(Now) & "' WHERE name = '" & lp_strDepartment & "';"
            ExecuteSQL l_strSQL, g_strExecuteError
            
        Else
            
            Dim l_strSequenceNumber As String
PROC_Input_Number:
    
            l_strSequenceNumber = InputBox("There is currently no default value for a purchase confirmation number for department: " & lp_strDepartment & ". Please specify the initial value now.", "No default value")
            If Not IsNumeric(l_strSequenceNumber) Then
                MsgBox "Please enter a valid numeric, positive integer.", vbExclamation
                GoTo PROC_Input_Number
            Else
                l_rstDepartment.AddNew
                l_rstDepartment("name") = lp_strDepartment
                l_rstDepartment("nextpurchaseorderauthorisationnumber") = Val(l_strSequenceNumber)
                l_rstDepartment("muser") = g_strUserInitials
                l_rstDepartment("mdate") = Now()
                l_rstDepartment.Update
                
                l_lngNextAuthNumber = Val(l_strSequenceNumber)
            End If
        End If
        
        l_rstDepartment.Close
        Set l_rstDepartment = Nothing
    
    Else
        
        l_lngNextAuthNumber = GetNextSequence("purchaseorderauthorisationnumber")
    End If
    
    GetNextPurchaseOrderAuthorisationNumber = l_lngNextAuthNumber
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd


End Function

Function SavePurchaseOrder()
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/savepurchaseorder") Then
        Exit Function
    End If
    
    frmPurchaseOrder.MousePointer = vbHourglass
    
    Dim l_strSQL As String
    
    With frmPurchaseOrder
        
        If Val(.txtPurchaseHeaderID.Text) = 0 Then
            l_strSQL = "INSERT INTO purchaseheader ("
            'fields to insert
            l_strSQL = l_strSQL & "jobID, "
            
            l_strSQL = l_strSQL & "projectnumber, "
            l_strSQL = l_strSQL & "projectID, "
            
            l_strSQL = l_strSQL & "productname, "
            l_strSQL = l_strSQL & "productID, "
            l_strSQL = l_strSQL & "ordertype, "
            l_strSQL = l_strSQL & "companyID, "
            l_strSQL = l_strSQL & "companyname, "
            l_strSQL = l_strSQL & "contactID, "
            l_strSQL = l_strSQL & "contactname, "
            l_strSQL = l_strSQL & "deliverydate, "
            l_strSQL = l_strSQL & "deliverymethod, "
            l_strSQL = l_strSQL & "returndate, "
            l_strSQL = l_strSQL & "returnmethod, "
            l_strSQL = l_strSQL & "department, "
            l_strSQL = l_strSQL & "supplierreference, "
            l_strSQL = l_strSQL & "title, "
            l_strSQL = l_strSQL & "cdate, "
            l_strSQL = l_strSQL & "cuser, "
            l_strSQL = l_strSQL & "mdate, "
            l_strSQL = l_strSQL & "muser, "
            l_strSQL = l_strSQL & "deliveryfor, "
            l_strSQL = l_strSQL & "returnfor, "
            l_strSQL = l_strSQL & "fd_status, "
            l_strSQL = l_strSQL & "delivertocompanyname, "
            l_strSQL = l_strSQL & "delivertocontactname, "
            l_strSQL = l_strSQL & "delivertoaddress, "
            l_strSQL = l_strSQL & "delivertopostcode, "
            l_strSQL = l_strSQL & "delivertotelephone, "
            l_strSQL = l_strSQL & "delivertocountry, "
            l_strSQL = l_strSQL & "delivertodifferentaddress, "
            l_strSQL = l_strSQL & "currency, "
            l_strSQL = l_strSQL & "notes1) VALUES ("
            
            'values to insert
            l_strSQL = l_strSQL & "'" & .txtJobID & "', "
            
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbProject.Text) & "', "
            l_strSQL = l_strSQL & "'" & .lblProjectID.Caption & "', "
            
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbProduct.Text) & "', "
            l_strSQL = l_strSQL & "'" & .lblProductID.Caption & "', "
            l_strSQL = l_strSQL & "'" & .cmbOrderType & "', "
            l_strSQL = l_strSQL & "'" & .lblCompanyID.Caption & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbCompany.Text) & "', "
            l_strSQL = l_strSQL & "'" & .lblContactID.Caption & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbContact.Text) & "', "
            
            If Not IsNull(.datDeliveryDate.Value) Then
                l_strSQL = l_strSQL & "'" & FormatSQLDate(CDate(.datDeliveryDate.Value) & " " & .cmbStartTime.Text) & "', "
            Else
                l_strSQL = l_strSQL & "NULL, "
            End If
            
            l_strSQL = l_strSQL & "'" & .cmbDeliveryMethod.Text & "', "
            
            If Not IsNull(.datReturnDate.Value) Then
                l_strSQL = l_strSQL & "'" & FormatSQLDate(CDate(.datReturnDate.Value) & " " & .cmbEndTime.Text) & "', "
            Else
                l_strSQL = l_strSQL & "NULL, "
            End If
            
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbReturnMethod.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbDepartment.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.txtSupplierReference.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.txtTitle.Text) & "', "
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now()) & "', "
            l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now()) & "', "
            l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbDeliveryFor.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbReturnFor.Text) & "', "
            l_strSQL = l_strSQL & "'New', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbDeliverToCompany.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbDeliverToContact.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.txtAddress.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.txtPostCode.Text) & "', "
            l_strSQL = l_strSQL & "'" & .txtTelephone.Text & "', "
            l_strSQL = l_strSQL & "'" & .txtCountry.Text & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.chkDeliverTo.Value) & "', "
            If .optCurrency(1).Value = True Then
                l_strSQL = l_strSQL & "1, "
            ElseIf .optCurrency(2).Value = True Then
                l_strSQL = l_strSQL & "2, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.txtNotes.Text) & "')"
            
        Else
            l_strSQL = "UPDATE purchaseheader SET "
            l_strSQL = l_strSQL & "jobID = '" & .txtJobID.Text & "', "
            
            l_strSQL = l_strSQL & "projectnumber = '" & QuoteSanitise(.cmbProject.Text) & "', "
            l_strSQL = l_strSQL & "projectID = '" & .lblProjectID.Caption & "', "
            
            l_strSQL = l_strSQL & "productname = '" & QuoteSanitise(.cmbProduct.Text) & "', "
            l_strSQL = l_strSQL & "productID = '" & .lblProductID.Caption & "', "
            l_strSQL = l_strSQL & "ordertype = '" & QuoteSanitise(.cmbOrderType.Text) & "', "
            l_strSQL = l_strSQL & "companyID = '" & .lblCompanyID.Caption & "', "
            l_strSQL = l_strSQL & "companyname = '" & QuoteSanitise(.cmbCompany.Text) & "', "
            l_strSQL = l_strSQL & "contactID = '" & .lblContactID.Caption & "', "
            l_strSQL = l_strSQL & "contactname = '" & QuoteSanitise(.cmbContact.Text) & "', "
            If Not IsNull(.datDeliveryDate.Value) Then
                l_strSQL = l_strSQL & "deliverydate = '" & FormatSQLDate(.datDeliveryDate.Value & " " & .cmbStartTime.Text) & "', "
            Else
                l_strSQL = l_strSQL & "deliverydate = NULL, "
            End If
            l_strSQL = l_strSQL & "deliverymethod = '" & QuoteSanitise(.cmbDeliveryMethod.Text) & "', "
            If Not IsNull(.datReturnDate.Value) Then
                l_strSQL = l_strSQL & "returndate = '" & FormatSQLDate(.datReturnDate.Value & " " & .cmbEndTime.Text) & "', "
            Else
                l_strSQL = l_strSQL & "returndate = NULL, "
            End If
            l_strSQL = l_strSQL & "returnmethod = '" & QuoteSanitise(.cmbReturnMethod.Text) & "', "
            l_strSQL = l_strSQL & "department = '" & QuoteSanitise(.cmbDepartment.Text) & "', "
            l_strSQL = l_strSQL & "supplierreference = '" & QuoteSanitise(.txtSupplierReference.Text) & "', "
            l_strSQL = l_strSQL & "title = '" & QuoteSanitise(.txtTitle.Text) & "', "
            l_strSQL = l_strSQL & "mdate = '" & FormatSQLDate(Now()) & "', "
            l_strSQL = l_strSQL & "muser = '" & g_strUserInitials & "', "
            l_strSQL = l_strSQL & "deliveryfor = '" & QuoteSanitise(.cmbDeliveryFor.Text) & "', "
            l_strSQL = l_strSQL & "returnfor = '" & QuoteSanitise(.cmbReturnFor.Text) & "', "
            l_strSQL = l_strSQL & "delivertocompanyname = '" & QuoteSanitise(.cmbDeliverToCompany.Text) & "', "
            l_strSQL = l_strSQL & "delivertocontactname = '" & QuoteSanitise(.cmbDeliverToContact.Text) & "', "
            l_strSQL = l_strSQL & "delivertoaddress = '" & QuoteSanitise(.txtAddress.Text) & "', "
            l_strSQL = l_strSQL & "delivertopostcode  = '" & QuoteSanitise(.txtPostCode.Text) & "', "
            l_strSQL = l_strSQL & "delivertotelephone  = '" & .txtTelephone.Text & "', "
            l_strSQL = l_strSQL & "delivertocountry  = '" & QuoteSanitise(.txtCountry.Text) & "', "
            l_strSQL = l_strSQL & "delivertodifferentaddress  = '" & QuoteSanitise(.chkDeliverTo.Value) & "', "
            If .optCurrency(1).Value = True Then
                l_strSQL = l_strSQL & "currency = 1, "
            ElseIf .optCurrency(2).Value = True Then
                l_strSQL = l_strSQL & "currency = 2, "
            Else
                l_strSQL = l_strSQL & "currency = 0, "
            End If
            
            l_strSQL = l_strSQL & "notes1 = '" & QuoteSanitise(.txtNotes.Text) & "' "
            l_strSQL = l_strSQL & " WHERE purchaseheaderID = " & .txtPurchaseHeaderID.Text
        End If
        
        l_strSQL = l_strSQL & ";"
        
    End With
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
            
    Dim l_lngNewPurchaseHeaderID As Long
    l_lngNewPurchaseHeaderID = g_lngLastID
    If frmPurchaseOrder.txtPurchaseHeaderID.Text <> "" Then l_lngNewPurchaseHeaderID = Val(frmPurchaseOrder.txtPurchaseHeaderID.Text)
    
    
    If Val(frmPurchaseOrder.txtPurchaseHeaderID.Text) = 0 Then
        
        frmPurchaseOrder.txtPurchaseHeaderID.Text = g_lngLastID
        
        If Val(frmPurchaseOrder.txtJobID.Text) > 0 Then
            AddJobHistory Val(frmPurchaseOrder.txtJobID.Text), "Created a Purchase Order - ID : " & g_lngLastID
            
        End If
        
        frmPurchaseOrder.adoPurchaseOrderDetail.Refresh
        
        frmPurchaseOrder.lblCreatedUser.Caption = g_strUserInitials
        frmPurchaseOrder.lblCreatedDate.Caption = Date
    Else
        
        If Val(frmPurchaseOrder.txtJobID.Text) > 0 Then
            AddJobHistory Val(frmPurchaseOrder.txtJobID.Text), "Saved Purchase Order - ID : " & frmPurchaseOrder.txtPurchaseHeaderID.Text & " ( Auth: " & frmPurchaseOrder.txtAuthorisationNumber.Text & ")"
        End If
    
    End If

    
    
        
    If Val(frmPurchaseOrder.txtJobID.Text) <> 0 Then
        'check the job for sub-hire items

        l_strSQL = "SELECT COUNT(resourcescheduleID) as 'TheCount', hiredescription, starttime, endtime FROM resourceschedule WHERE hiresupplierID = '" & frmPurchaseOrder.lblCompanyID.Caption & "' AND jobID = '" & Val(frmPurchaseOrder.txtJobID.Text) & "' AND (purchaseorderID IS NULL OR purchaseorderID = '0') GROUP BY hiredescription;"
        Dim l_con As New ADODB.Connection
        Dim l_rst As New ADODB.Recordset
        l_con.Open g_strConnection
        l_rst.Open l_strSQL, l_con, adOpenDynamic, adLockOptimistic
        
        'add these items to this purchase order
        
        If Not l_rst.EOF Then
            l_rst.MoveFirst
            Do While Not l_rst.EOF
            
                l_strSQL = "INSERT INTO purchasedetail (purchaseheaderID, description, quantity, fd_time) "
                l_strSQL = l_strSQL & " VALUES ('" & l_lngNewPurchaseHeaderID & "', '" & QuoteSanitise(l_rst("hiredescription")) & "', '" & l_rst("TheCount") & "', '" & GetDuration(l_rst("starttime"), l_rst("endtime")) & "'); "
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                
                l_rst.MoveNext
            Loop
        End If
        
        l_strSQL = "UPDATE resourceschedule SET purchaseorderID = '" & l_lngNewPurchaseHeaderID & "' WHERE hiresupplierID = '" & frmPurchaseOrder.lblCompanyID.Caption & "' AND jobID = '" & Val(frmPurchaseOrder.txtJobID.Text) & "' AND (purchaseorderID IS NULL OR purchaseorderID = '0');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        l_rst.Close
        Set l_rst = Nothing
        
        l_con.Close
        Set l_con = Nothing
    
    
    End If
    
    
    
 
    'If ZeroValueOnPurchaseOrder(frmPurchaseOrder.txtPurchaseHeaderID.Text) <> 0 Then
    '    MsgBox "You have no value on this purchase order. Please correct this.", vbExclamation
    'End If
    
    
    UpdateDespatchForPurchaseOrder frmPurchaseOrder.txtPurchaseHeaderID.Text
    
    frmPurchaseOrder.MousePointer = vbDefault
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function
Public Sub ShowPurchaseOrder(lp_lngPurchaseOrderID As Long, Optional lp_lngJobID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/showpurchaseorder") Then
        Exit Sub
    End If
    
    
'    If IsFormLoaded("frmPurchaseOrder") Then
'        frmPurchaseOrder.Show
'        frmPurchaseOrder.ZOrder 0
'        Exit Sub
'    End If
    
    Dim l_strSQL As String
    
    l_strSQL = "SELECT * FROM purchaseheader WHERE purchaseheaderID = " & lp_lngPurchaseOrderID
    
    Dim l_rstPurchaseHeader As New ADODB.Recordset
    Set l_rstPurchaseHeader = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    CheckForSQLError
    
    If Not l_rstPurchaseHeader.EOF Then
        
        l_rstPurchaseHeader.MoveFirst
        
        'check this user is allowed to view this order
        If Format(l_rstPurchaseHeader("cuser")) <> g_strUserInitials Then
            
            If GetData("cetauser", "department", "cetauserID", g_lngUserID) = l_rstPurchaseHeader("department") Then
                If (CheckAccess("/viewallpurchaseorders", True) <> True) And (CheckAccess("/viewdepartmentpurchaseorders", True) <> True) Then
                    GoTo Proc_No_Access
                End If
            Else
                If Not CheckAccess("/viewallpurchaseorders", True) Then GoTo Proc_No_Access
            End If
            
            GoTo PROC_Continue
            
            
Proc_No_Access:

            MsgBox "You can not view this order", vbExclamation
            l_rstPurchaseHeader.Close
            Set l_rstPurchaseHeader = Nothing
            Exit Sub
            
        End If
        
        
PROC_Continue:
        
        
        
        With frmPurchaseOrder
            
            .lblProjectID.Caption = Format(l_rstPurchaseHeader("projectID"))
            .cmbProject.Text = Format(l_rstPurchaseHeader("projectnumber"))
            
            .lblProductID.Caption = Format(l_rstPurchaseHeader("productID"))
            .cmbProduct.Text = Format(l_rstPurchaseHeader("productname"))
            .txtStatus.Text = Format(l_rstPurchaseHeader("fd_status"))
            .txtPurchaseHeaderID.Text = Format(l_rstPurchaseHeader("purchaseheaderID"))
            .txtAuthorisationNumber.Text = Format(l_rstPurchaseHeader("authorisationnumber"))
            .txtJobID.Text = Format(l_rstPurchaseHeader("jobID"))
            .cmbOrderType.ListIndex = GetListIndex(.cmbOrderType, Format(l_rstPurchaseHeader("ordertype")))
            .cmbCompany.Text = Format(l_rstPurchaseHeader("companyname"))
            .lblCompanyID.Caption = Format(l_rstPurchaseHeader("companyID"))
            .cmbContact.Text = Format(l_rstPurchaseHeader("contactname"))
            .lblContactID.Caption = Format(l_rstPurchaseHeader("contactID"))
            .datDeliveryDate.Value = Format(l_rstPurchaseHeader("deliverydate"), vbShortDateFormat)
            .cmbStartTime.Text = Format(l_rstPurchaseHeader("deliverydate"), "hh:nn")
            .cmbDeliveryMethod.Text = Format(l_rstPurchaseHeader("deliverymethod"))
            .datReturnDate.Value = Format(l_rstPurchaseHeader("returndate"), vbShortDateFormat)
            .cmbEndTime.Text = Format(l_rstPurchaseHeader("returndate"), "hh:nn")
            .cmbReturnMethod.Text = Format(l_rstPurchaseHeader("returnmethod"))
            .cmbDepartment.ListIndex = GetListIndex(.cmbDepartment, Format(l_rstPurchaseHeader("department")))
            .txtSupplierReference.Text = Format(l_rstPurchaseHeader("supplierreference"))
            .txtTitle.Text = Format(l_rstPurchaseHeader("title"))
            .txtTotal.Text = Format(GetTotalCostsForPurchaseOrder(lp_lngPurchaseOrderID, "total"), "CURRENCY")
            .txtInvoiceTotal.Text = Format(GetTotalCostsForPurchaseOrder(lp_lngPurchaseOrderID, "supplierinvoiceamount"), "CURRENCY")
            .grdOrderDetail.Enabled = True
            .cmbDeliveryFor.Text = Format(l_rstPurchaseHeader("deliveryfor"))
            .cmbReturnFor.Text = Format(l_rstPurchaseHeader("returnfor"))
            
            .cmbDeliverToCompany.Text = Format(l_rstPurchaseHeader("delivertocompanyname"))
            .cmbDeliverToContact.Text = Format(l_rstPurchaseHeader("delivertocontactname"))
            .txtAddress.Text = Format(l_rstPurchaseHeader("delivertoaddress"))
            .txtPostCode.Text = Format(l_rstPurchaseHeader("delivertopostcode"))
            .txtCountry.Text = Format(l_rstPurchaseHeader("delivertocountry"))
            .txtTelephone.Text = Format(l_rstPurchaseHeader("delivertotelephone"))
            .chkDeliverTo.Value = GetFlag(l_rstPurchaseHeader("delivertodifferentaddress"))
            .txtNotes.Text = Trim(" " & l_rstPurchaseHeader("notes1"))
            
            
            'fill in the history dates and user names
            .lblAccountsAuthorisedDate.Caption = Format(l_rstPurchaseHeader("accountsauthoriseddate"))
            .lblAccountsAuthorisedUser.Caption = Format(l_rstPurchaseHeader("accountsauthorisedbyuser"))
            .lblAuthorisedDate.Caption = Format(l_rstPurchaseHeader("authoriseddate"))
            .lblAuthorisedUser.Caption = Format(l_rstPurchaseHeader("authorisedbyuser"))
            .lblCancelledDate.Caption = Format(l_rstPurchaseHeader("cancelleddate"))
            .lblCancelledUser.Caption = Format(l_rstPurchaseHeader("cancelleduser"))
            
            
            .lblCreatedDate.Caption = Format(l_rstPurchaseHeader("cdate"))
            .lblCreatedUser.Caption = Format(l_rstPurchaseHeader("cuser"))
            .lblModifiedDate.Caption = Format(l_rstPurchaseHeader("mdate"))
            .lblModifiedUser.Caption = Format(l_rstPurchaseHeader("muser"))
            .lblReceivedDate.Caption = Format(l_rstPurchaseHeader("receiveddate"))
            .lblReceivedUser.Caption = Format(l_rstPurchaseHeader("receiveduser"))
            
            .lblRequestedDate.Caption = Format(l_rstPurchaseHeader("authorisationrequestdate"))
            .lblAuthRequestUser.Caption = Format(l_rstPurchaseHeader("authorisationrequestuser"))
                        
            Select Case Val(Trim(" " & l_rstPurchaseHeader("currency")))
                Case 1
                    .optCurrency(1).Value = True
                Case 2
                    .optCurrency(2).Value = True
                Case Else
                    .optCurrency(0).Value = True
            End Select
                        
            If .lblCancelledDate.Caption <> "" Then
                .lblCancelledDate.BackColor = vbRed
                .lblCancelledUser.BackColor = vbRed
                .Caption = "PURCHASE ORDER CANCELLED"
                .cmdEnterCompletionDetails.Enabled = False
                .cmdItemsReceived.Enabled = False
            Else
            
                .lblCancelledDate.BackColor = vbWindowBackground
                .lblCancelledUser.BackColor = vbWindowBackground
                .Caption = "Purchase Order"
                .cmdEnterCompletionDetails.Enabled = True
                .cmdItemsReceived.Enabled = True
                
                
                Dim l_dblMaxOrdervalue As Double, l_strDepartment As String
                l_dblMaxOrdervalue = GetData("cetauser", "purchaseorderlimit", "cetauserID", g_lngUserID)
                
                
                If l_dblMaxOrdervalue = 0 Then
                    l_strDepartment = GetData("cetauser", "department", "cetauserID", g_lngUserID)
                    l_dblMaxOrdervalue = GetData("department", "purchaseorderlimit", "name", l_strDepartment)
                End If
                
                
                
                If (.lblAuthorisedDate.Caption <> "") Or (.lblAuthorisedUser.Caption <> "") Then
                
                    .cmdSave.Enabled = False
                    .cmdGetAuthorisationNumber.Enabled = False
                    .grdOrderDetail.Columns("quantity").Locked = True
                    .grdOrderDetail.Columns("description").Locked = True
                    .grdOrderDetail.Columns("unitcharge").Locked = True
                    .grdOrderDetail.Columns("total").Locked = True
                    .grdOrderDetail.Columns("discount").Locked = True
                    .grdOrderDetail.Columns("suppliercode").Locked = True
                    .grdOrderDetail.Columns("suppliercode").DropDownHwnd = 0
                    .grdOrderDetail.AllowDelete = True
                    .txtNotes.Locked = True
                    
                    If Trim(.lblReceivedDate.Caption) = "" Then
                        .cmdItemsReceived.Enabled = True
                        .cmdCancel.Enabled = True
                    Else
                        .cmdItemsReceived.Enabled = False
                        .cmdCancel.Enabled = False
                    End If
                    
                    If .lblAccountsAuthorisedDate.Caption = "" And .lblReceivedDate.Caption <> "" Then
                        .cmdEnterCompletionDetails.Enabled = True
                    Else
                        .cmdEnterCompletionDetails.Enabled = False
                    End If

                    If (g_optAllowEditAuthorisedPurchaseOrders = 1) And (.lblAccountsAuthorisedDate.Caption = "") And (Val(.txtTotal.Text) <= l_dblMaxOrdervalue) Then GoTo Proc_Enable_PurchaseOrder

                    If CheckAccess("/allowentercompletiondetails", True) Then .cmdEnterCompletionDetails.Enabled = True

                Else
                
                
                    .cmdItemsReceived.Enabled = False
                    .cmdEnterCompletionDetails.Enabled = False
                
Proc_Enable_PurchaseOrder:
                    .cmdSave.Enabled = True
                    .cmdGetAuthorisationNumber.Enabled = True
                    .grdOrderDetail.Columns("quantity").Locked = False
                    .grdOrderDetail.Columns("description").Locked = False
                    .grdOrderDetail.Columns("unitcharge").Locked = False
                    .grdOrderDetail.Columns("total").Locked = False
                    .grdOrderDetail.Columns("discount").Locked = False
                    .grdOrderDetail.Columns("suppliercode").Locked = False
                    .grdOrderDetail.Columns("suppliercode").DropDownHwnd = .ddnSupplierRates.hWnd
                    .grdOrderDetail.AllowDelete = True
                    .txtNotes.Locked = False

                    
                End If
                
                
            End If
                        
                        
        End With
    Else
        If frmPurchaseOrder.txtPurchaseHeaderID.Text <> "" Then
            frmPurchaseOrder.Show
            frmPurchaseOrder.txtPurchaseHeaderID.SetFocus
            HighLite frmPurchaseOrder.txtPurchaseHeaderID
            If Not IsNull(lp_lngJobID) Then
                frmPurchaseOrder.txtJobID.Text = lp_lngJobID
            End If
            frmPurchaseOrder.ZOrder 0
            Exit Sub
            
        End If
        
        ClearFields frmPurchaseOrder
        
        If frmPurchaseOrder.cmbOrderType.ListCount <> 0 Then
            frmPurchaseOrder.cmbOrderType.ListIndex = GetListIndex(frmPurchaseOrder.cmbOrderType, "Purchase")
        End If
        
        If Not IsNull(lp_lngJobID) Then
            frmPurchaseOrder.txtJobID.Text = lp_lngJobID
        End If
        
        frmPurchaseOrder.grdOrderDetail.Enabled = False
        frmPurchaseOrder.cmdSave.Enabled = True
        frmPurchaseOrder.cmdEnterCompletionDetails.Enabled = False
        frmPurchaseOrder.cmdItemsReceived.Enabled = False
        frmPurchaseOrder.cmbDeliveryFor.Text = g_strFullUserName
        frmPurchaseOrder.optCurrency(0).Value = True
        
    End If
    
    l_rstPurchaseHeader.Close
    Set l_rstPurchaseHeader = Nothing
    
    l_strSQL = "SELECT * FROM purchasedetail WHERE purchaseheaderID = " & lp_lngPurchaseOrderID & " ORDER BY purchasedetailID"
    
    frmPurchaseOrder.adoPurchaseOrderDetail.ConnectionString = g_strConnection
    frmPurchaseOrder.adoPurchaseOrderDetail.RecordSource = l_strSQL
    frmPurchaseOrder.adoPurchaseOrderDetail.Refresh
    
    If Val(frmPurchaseOrder.txtAuthorisationNumber.Text) <> 0 Then
        frmPurchaseOrder.cmdGetAuthorisationNumber.Enabled = False
    Else
        frmPurchaseOrder.cmdGetAuthorisationNumber.Enabled = True
    End If
    
    
    frmPurchaseOrder.Show
    frmPurchaseOrder.ZOrder 0
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub ShowPurchaseOrderAllocation()
If Not CheckAccess("/showbudgetallocation") Then Exit Sub

frmPurchaseBudgets.Show


End Sub


Function CompletePurchaseOrder(lp_lngPurchaseHeaderID As Long) As Boolean

If Not CheckAccess("/completepurchaseorder") Then
    CompletePurchaseOrder = False
    Exit Function
End If

Dim l_strSQL As String

frmPurchaseCompletion.txtAuthorisationNumber.Text = GetData("purchaseheader", "authorisationnumber", "purchaseheaderID", lp_lngPurchaseHeaderID)
frmPurchaseCompletion.txtPurchaseHeaderID.Text = lp_lngPurchaseHeaderID

l_strSQL = "SELECT * FROM purchasedetail WHERE purchaseheaderID = " & lp_lngPurchaseHeaderID & " ORDER BY purchasedetailID"
    
frmPurchaseCompletion.adoPurchaseOrderDetail.ConnectionString = g_strConnection
frmPurchaseCompletion.adoPurchaseOrderDetail.RecordSource = l_strSQL
frmPurchaseCompletion.adoPurchaseOrderDetail.Refresh

frmPurchaseCompletion.Show vbModal

If frmPurchaseCompletion.Tag <> "CANCELLED" Then
    l_strSQL = "UPDATE purchaseheader SET fd_status = 'Completed', accountsauthoriseddate = '" & FormatSQLDate(Now) & "', accountsauthorisedbyuser = '" & g_strUserInitials & "' WHERE purchaseheaderID = '" & lp_lngPurchaseHeaderID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
End If

Unload frmPurchaseCompletion
Set frmPurchaseCompletion = Nothing
End Function


Sub CopyPurchaseLines(lp_lngSourcePurchaseID As Long, lp_lngDestinationPurchaseID As Long)


If Not CheckAccess("/copypurchaselines") Then Exit Sub

Dim l_conMain As New ADODB.Connection
l_conMain.Open g_strConnection

Dim l_strSQL As String
l_strSQL = "SELECT * FROM purchasedetail WHERE purchaseheaderID = '" & lp_lngSourcePurchaseID & "';"

Dim l_rstSourceDetail As New ADODB.Recordset
l_rstSourceDetail.Open l_strSQL, l_conMain, adOpenForwardOnly, adLockReadOnly

Do While Not l_rstSourceDetail.EOF

    Dim l_strDescription As String
    l_strDescription = Trim(" " & l_rstSourceDetail("description"))

    
    l_strSQL = "INSERT INTO purchasedetail ("
    l_strSQL = l_strSQL & "purchaseheaderID, "
    l_strSQL = l_strSQL & "resourcescheduleID, "
    l_strSQL = l_strSQL & "cetacode, "
    l_strSQL = l_strSQL & "suppliercode, "
    l_strSQL = l_strSQL & "quantity, "
    l_strSQL = l_strSQL & "description, "
    l_strSQL = l_strSQL & "unitcharge, "
    l_strSQL = l_strSQL & "discount, "
    l_strSQL = l_strSQL & "total, "
    l_strSQL = l_strSQL & "totalincludingvat, "
    l_strSQL = l_strSQL & "ourreference, "
    l_strSQL = l_strSQL & "fd_time) VALUES ("

    l_strSQL = l_strSQL & "'" & lp_lngDestinationPurchaseID & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("resourcescheduleID") & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstSourceDetail("cetacode")) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstSourceDetail("suppliercode")) & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("quantity") & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strDescription) & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("unitcharge") & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("discount") & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("total") & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("totalincludingvat") & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstSourceDetail("ourreference")) & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("fd_time") & "');"

    
    l_conMain.Execute l_strSQL
    
    l_rstSourceDetail.MoveNext
    
Loop

l_rstSourceDetail.Close
Set l_rstSourceDetail = Nothing



End Sub

Function GetPODetail(lp_lngPurchaseID As Long) As String
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_rstDetails As New ADODB.Recordset
    Dim l_strTextToAdd As String
    
    Set l_rstDetails = ExecuteSQL("SELECT quantity, description FROM purchasedetail WHERE purchaseheaderID = " & lp_lngPurchaseID & " ORDER BY purchasedetailID", g_strExecuteError)
    
    If Not l_rstDetails.EOF Then l_rstDetails.MoveFirst
    
    Do While Not l_rstDetails.EOF
        'build the tool tip
        l_strTextToAdd = l_strTextToAdd & l_rstDetails("quantity") & "x " & l_rstDetails("description") & " / "

        l_rstDetails.MoveNext
    Loop

    If l_strTextToAdd <> "" Then l_strTextToAdd = Left(l_strTextToAdd, Len(l_strTextToAdd) - Len(" / "))
    
    l_rstDetails.Close
    Set l_rstDetails = Nothing
    
    GetPODetail = l_strTextToAdd
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetTotalCostsForPurchaseOrder(lp_lngPurchaseOrderID As Long, lp_strField As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_rstTotal As New ADODB.Recordset
    
    l_strSQL = "SELECT SUM(" & lp_strField & ") FROM purchasedetail WHERE (purchaseheaderID = '" & lp_lngPurchaseOrderID & "')"
    
    Set l_rstTotal = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    CheckForSQLError
    
    GetTotalCostsForPurchaseOrder = Format(l_rstTotal(0))
    
    l_rstTotal.Close
    Set l_rstTotal = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function NoPurchaseOrderDiscrepencies(lp_lngPurchaseHeaderID As Long, lp_strInvoiceNumber As String, lp_datInvoiceDate As Date)

Dim l_strSQL As String
l_strSQL = "UPDATE purchasedetail SET supplierinvoicenumber  = '" & lp_strInvoiceNumber & "', quantityreceived = quantity, supplierinvoiceamount = total, supplierinvoicedate  = '" & FormatSQLDate(lp_datInvoiceDate) & "', discrepencyreason = Null WHERE purchaseheaderID = '" & lp_lngPurchaseHeaderID & "';"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

NoPurchaseOrderDiscrepencies = 1


End Function

