VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmCetaUserMessages 
   Caption         =   "Ceta User Messages"
   ClientHeight    =   4995
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12435
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   4995
   ScaleWidth      =   12435
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdSearch 
      Caption         =   "Search"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   9960
      TabIndex        =   2
      ToolTipText     =   "Close this form"
      Top             =   4620
      Visible         =   0   'False
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adoCetaUserMessages 
      Height          =   330
      Left            =   5340
      Top             =   300
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCetaUserMessages"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   11220
      TabIndex        =   1
      ToolTipText     =   "Close this form"
      Top             =   4620
      Width           =   1155
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdCetaUserMessages 
      Align           =   1  'Align Top
      Bindings        =   "frmCetaUserMessages.frx":0000
      Height          =   4515
      Left            =   0
      TabIndex        =   0
      ToolTipText     =   "Keep mouse over a message to read it in full"
      Top             =   0
      Width           =   12435
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AllowDelete     =   -1  'True
      AllowUpdate     =   0   'False
      BackColorOdd    =   16777152
      RowHeight       =   423
      ExtraHeight     =   1032
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "cetanoticeID"
      Columns(0).Name =   "cetanoticeID"
      Columns(0).DataField=   "cetanoticeID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3545
      Columns(1).Caption=   "Date Created"
      Columns(1).Name =   "datecreated"
      Columns(1).DataField=   "datecreated"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   11377
      Columns(2).Caption=   "Ceta User Message"
      Columns(2).Name =   "notice"
      Columns(2).DataField=   "notice"
      Columns(2).DataType=   17
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      _ExtentX        =   21934
      _ExtentY        =   7964
      _StockProps     =   79
      Caption         =   "Ceta User Messages"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmCetaUserMessages"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdSearch_Click()

adoCetaUserMessages.Refresh

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

l_strSQL = "Select * from cetanotice ORDER BY cetanoticeID;"

adoCetaUserMessages.ConnectionString = g_strConnection
adoCetaUserMessages.RecordSource = l_strSQL
adoCetaUserMessages.Refresh

End Sub

Private Sub Form_Resize()
cmdClose.Top = Me.ScaleHeight - cmdClose.Height - 120
cmdClose.Left = Me.ScaleWidth - cmdClose.Width - 120

grdCetaUserMessages.Height = Me.ScaleHeight - cmdClose.Height - 120 - 120

End Sub

Private Sub grdCetaUserMessages_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

If Not CheckAccess("/allowcetamessageupdate") Then Cancel = True

End Sub

Private Sub grdCetaUserMessages_BeforeUpdate(Cancel As Integer)

If Not CheckAccess("/allowcetamessageupdate") Then Cancel = True

End Sub

