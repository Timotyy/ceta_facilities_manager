VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmSundryManagement 
   Caption         =   "Sundry Management"
   ClientHeight    =   7605
   ClientLeft      =   3705
   ClientTop       =   5880
   ClientWidth     =   14955
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7605
   ScaleWidth      =   14955
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtPassenger 
      Height          =   285
      Left            =   12840
      TabIndex        =   39
      Top             =   840
      Width           =   1455
   End
   Begin VB.ComboBox cmbRunner 
      Height          =   315
      Left            =   12000
      TabIndex        =   37
      Tag             =   "NOCLEAR"
      ToolTipText     =   "Report to print"
      Top             =   420
      Width           =   2355
   End
   Begin VB.CheckBox chkShow 
      Caption         =   "Authorised"
      Height          =   195
      Index           =   0
      Left            =   4440
      TabIndex        =   20
      Top             =   720
      Width           =   1155
   End
   Begin VB.CheckBox chkShow 
      Caption         =   "Approved"
      Height          =   195
      Index           =   1
      Left            =   4440
      TabIndex        =   19
      Top             =   1020
      Width           =   1155
   End
   Begin VB.CheckBox chkShow 
      Caption         =   "New"
      Height          =   195
      Index           =   2
      Left            =   4440
      TabIndex        =   18
      Top             =   420
      Value           =   1  'Checked
      Width           =   1155
   End
   Begin VB.PictureBox picFooter 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   600
      ScaleHeight     =   315
      ScaleWidth      =   13275
      TabIndex        =   11
      Top             =   5760
      Width           =   13275
      Begin VB.CommandButton cmdRunnersExpeneses 
         Caption         =   "Overheads Report"
         Height          =   315
         Left            =   0
         TabIndex        =   43
         Top             =   0
         Width           =   1695
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   4320
         TabIndex        =   42
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdRunnersExpenseReport 
         Caption         =   "Runners Expense Report"
         Height          =   315
         Left            =   1920
         TabIndex        =   41
         Top             =   0
         Width           =   2175
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   12000
         TabIndex        =   17
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   5640
         TabIndex        =   16
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdNewSundry 
         Caption         =   "New"
         Height          =   315
         Left            =   10740
         TabIndex        =   15
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdRefresh 
         Caption         =   "Refresh (F5)"
         Height          =   315
         Left            =   9480
         TabIndex        =   14
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdApprove 
         Caption         =   "Approve"
         Height          =   315
         Left            =   8220
         TabIndex        =   13
         ToolTipText     =   "Clear the form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdAuthoriseSundry 
         Caption         =   "Authorise"
         Height          =   315
         Left            =   6960
         TabIndex        =   12
         ToolTipText     =   "Clear the form"
         Top             =   0
         Width           =   1155
      End
   End
   Begin VB.ComboBox cmbOrderBy 
      Height          =   315
      Left            =   1260
      TabIndex        =   7
      Tag             =   "NOCLEAR"
      Text            =   "cmbOrderBy"
      ToolTipText     =   "Order by"
      Top             =   7200
      Visible         =   0   'False
      Width           =   2235
   End
   Begin VB.ComboBox cmbDirection 
      Height          =   315
      ItemData        =   "frmSundryManagement.frx":0000
      Left            =   3540
      List            =   "frmSundryManagement.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   6
      ToolTipText     =   "View order"
      Top             =   7200
      Visible         =   0   'False
      Width           =   1095
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbJobID 
      Height          =   255
      Left            =   1380
      TabIndex        =   5
      Top             =   840
      Width           =   2895
      BevelWidth      =   0
      DataFieldList   =   "job id"
      BevelType       =   0
      _Version        =   196617
      BorderStyle     =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   5106
      _ExtentY        =   450
      _StockProps     =   93
      ForeColor       =   -2147483640
      BackColor       =   16761024
      DataFieldToDisplay=   "job id"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbAllocation 
      Height          =   255
      Left            =   1380
      TabIndex        =   0
      ToolTipText     =   "Job Type"
      Top             =   120
      Width           =   2895
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BackColorEven   =   -2147483643
      BackColorOdd    =   -2147483643
      RowHeight       =   423
      Columns(0).Width=   4868
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   12640511
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbProject 
      Height          =   255
      Left            =   1380
      TabIndex        =   1
      ToolTipText     =   "The name of this project. In the previous version of CETA, this was known as the 'Special Job Number'"
      Top             =   480
      Width           =   2895
      BevelWidth      =   0
      DataFieldList   =   "projectnumber"
      BevelType       =   0
      _Version        =   196617
      BorderStyle     =   0
      BeveColorScheme =   1
      CheckBox3D      =   0   'False
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   12648447
      RowHeight       =   423
      Columns.Count   =   9
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "projectID"
      Columns(0).Name =   "projectID"
      Columns(0).DataField=   "projectID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   2566
      Columns(1).Caption=   "Project Number"
      Columns(1).Name =   "name"
      Columns(1).DataField=   "projectnumber"
      Columns(1).FieldLen=   256
      Columns(2).Width=   4604
      Columns(2).Caption=   "Product"
      Columns(2).Name =   "productname"
      Columns(2).DataField=   "productname"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3757
      Columns(3).Caption=   "Company"
      Columns(3).Name =   "companyname"
      Columns(3).DataField=   "companyname"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "Contact"
      Columns(4).Name =   "contactname"
      Columns(4).DataField=   "contactname"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "Status"
      Columns(5).Name =   "status"
      Columns(5).DataField=   "fd_status"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "productID"
      Columns(6).Name =   "productID"
      Columns(6).DataField=   "productID"
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "companyID"
      Columns(7).Name =   "companyID"
      Columns(7).DataField=   "companyID"
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "contactID"
      Columns(8).Name =   "contactID"
      Columns(8).DataField=   "contactID"
      Columns(8).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   12648447
      DataFieldToDisplay=   "projectnumber"
   End
   Begin MSAdodcLib.Adodc adoSundry 
      Height          =   330
      Left            =   12300
      Top             =   540
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasql"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoSundry"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdSundryCosts 
      Bindings        =   "frmSundryManagement.frx":0019
      Height          =   3855
      Left            =   180
      TabIndex        =   9
      Top             =   1680
      Width           =   12735
      _Version        =   196617
      BorderStyle     =   0
      stylesets.count =   2
      stylesets(0).Name=   "authorised"
      stylesets(0).BackColor=   8454016
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSundryManagement.frx":0031
      stylesets(1).Name=   "approved"
      stylesets(1).BackColor=   9549311
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSundryManagement.frx":004D
      BeveColorScheme =   1
      AllowUpdate     =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   28
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "sundrycostID"
      Columns(0).Name =   "sundrycostID"
      Columns(0).Alignment=   1
      Columns(0).CaptionAlignment=   1
      Columns(0).DataField=   "sundrycostID"
      Columns(0).DataType=   20
      Columns(0).FieldLen=   256
      Columns(1).Width=   1164
      Columns(1).Caption=   "O/H #"
      Columns(1).Name =   "overheadcode"
      Columns(1).DataField=   "overheadcode"
      Columns(1).FieldLen=   256
      Columns(2).Width=   1746
      Columns(2).Caption=   "Job ID"
      Columns(2).Name =   "jobID"
      Columns(2).Alignment=   1
      Columns(2).CaptionAlignment=   1
      Columns(2).DataField=   "jobID"
      Columns(2).DataType=   20
      Columns(2).FieldLen=   256
      Columns(3).Width=   1720
      Columns(3).Caption=   "Project #"
      Columns(3).Name =   "projectnumber"
      Columns(3).Alignment=   1
      Columns(3).CaptionAlignment=   1
      Columns(3).DataField=   "projectnumber"
      Columns(3).DataType=   20
      Columns(3).FieldLen=   256
      Columns(4).Width=   1244
      Columns(4).Caption=   "Type"
      Columns(4).Name =   "sundrytype"
      Columns(4).CaptionAlignment=   0
      Columns(4).DataField=   "sundrytype"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   1032
      Columns(5).Caption=   "Personal"
      Columns(5).Name =   "personal"
      Columns(5).DataField=   "personal"
      Columns(5).DataType=   2
      Columns(5).FieldLen=   256
      Columns(5).Style=   2
      Columns(6).Width=   3175
      Columns(6).Caption=   "Date Ordered"
      Columns(6).Name =   "dateordered"
      Columns(6).Alignment=   1
      Columns(6).CaptionAlignment=   1
      Columns(6).DataField=   "dateordered"
      Columns(6).DataType=   135
      Columns(6).FieldLen=   256
      Columns(7).Width=   1720
      Columns(7).Caption=   "Amount"
      Columns(7).Name =   "amount"
      Columns(7).Alignment=   1
      Columns(7).CaptionAlignment=   1
      Columns(7).DataField=   "amount"
      Columns(7).DataType=   6
      Columns(7).NumberFormat=   "�0.00"
      Columns(7).FieldLen=   256
      Columns(8).Width=   2196
      Columns(8).Caption=   "Entered By"
      Columns(8).Name =   "enteredby"
      Columns(8).CaptionAlignment=   0
      Columns(8).DataField=   "enteredby"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Caption=   "Date Required"
      Columns(9).Name =   "daterequired"
      Columns(9).Alignment=   1
      Columns(9).CaptionAlignment=   1
      Columns(9).DataField=   "daterequired"
      Columns(9).DataType=   135
      Columns(9).FieldLen=   256
      Columns(10).Width=   2090
      Columns(10).Caption=   "Requested By"
      Columns(10).Name=   "requestedby"
      Columns(10).CaptionAlignment=   0
      Columns(10).DataField=   "requestedby"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   2963
      Columns(11).Caption=   "Comments"
      Columns(11).Name=   "comments"
      Columns(11).CaptionAlignment=   0
      Columns(11).DataField=   "comments"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(12).Width=   2143
      Columns(12).Caption=   "Passengers"
      Columns(12).Name=   "passengers"
      Columns(12).CaptionAlignment=   0
      Columns(12).DataField=   "passengers"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(13).Width=   2461
      Columns(13).Caption=   "Location"
      Columns(13).Name=   "location"
      Columns(13).CaptionAlignment=   0
      Columns(13).DataField=   "location"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      Columns(14).Width=   2566
      Columns(14).Caption=   "Pickup Time"
      Columns(14).Name=   "pickuptime"
      Columns(14).CaptionAlignment=   0
      Columns(14).DataField=   "pickuptime"
      Columns(14).DataType=   8
      Columns(14).FieldLen=   256
      Columns(15).Width=   2381
      Columns(15).Caption=   "A/C Number"
      Columns(15).Name=   "accountnumber"
      Columns(15).CaptionAlignment=   0
      Columns(15).DataField=   "accountnumber"
      Columns(15).DataType=   8
      Columns(15).FieldLen=   256
      Columns(16).Width=   2355
      Columns(16).Caption=   "Destination"
      Columns(16).Name=   "destination"
      Columns(16).DataField=   "destination"
      Columns(16).DataType=   8
      Columns(16).FieldLen=   256
      Columns(17).Width=   2170
      Columns(17).Caption=   "Supplier"
      Columns(17).Name=   "supplier"
      Columns(17).CaptionAlignment=   0
      Columns(17).DataField=   "supplier"
      Columns(17).DataType=   8
      Columns(17).FieldLen=   256
      Columns(18).Width=   3200
      Columns(18).Caption=   "Details"
      Columns(18).Name=   "details"
      Columns(18).CaptionAlignment=   0
      Columns(18).DataField=   "details"
      Columns(18).DataType=   8
      Columns(18).FieldLen=   256
      Columns(19).Width=   3200
      Columns(19).Caption=   "Auth Date"
      Columns(19).Name=   "authoriseddate"
      Columns(19).Alignment=   1
      Columns(19).CaptionAlignment=   1
      Columns(19).DataField=   "authoriseddate"
      Columns(19).DataType=   135
      Columns(19).FieldLen=   256
      Columns(20).Width=   3200
      Columns(20).Caption=   "Auth By"
      Columns(20).Name=   "authoriseduser"
      Columns(20).CaptionAlignment=   0
      Columns(20).DataField=   "authoriseduser"
      Columns(20).DataType=   8
      Columns(20).FieldLen=   256
      Columns(21).Width=   3200
      Columns(21).Caption=   "Approved Date"
      Columns(21).Name=   "approveddate"
      Columns(21).Alignment=   1
      Columns(21).CaptionAlignment=   1
      Columns(21).DataField=   "approveddate"
      Columns(21).DataType=   135
      Columns(21).FieldLen=   256
      Columns(22).Width=   3200
      Columns(22).Caption=   "Approved By"
      Columns(22).Name=   "approveduser"
      Columns(22).CaptionAlignment=   0
      Columns(22).DataField=   "approveduser"
      Columns(22).DataType=   8
      Columns(22).FieldLen=   256
      Columns(23).Width=   3200
      Columns(23).Caption=   "Cost Code"
      Columns(23).Name=   "costcode"
      Columns(23).CaptionAlignment=   0
      Columns(23).DataField=   "costcode"
      Columns(23).DataType=   8
      Columns(23).FieldLen=   256
      Columns(24).Width=   3200
      Columns(24).Visible=   0   'False
      Columns(24).Caption=   "cuser"
      Columns(24).Name=   "cuser"
      Columns(24).CaptionAlignment=   0
      Columns(24).DataField=   "cuser"
      Columns(24).DataType=   8
      Columns(24).FieldLen=   256
      Columns(25).Width=   3200
      Columns(25).Visible=   0   'False
      Columns(25).Caption=   "cdate"
      Columns(25).Name=   "cdate"
      Columns(25).Alignment=   1
      Columns(25).CaptionAlignment=   1
      Columns(25).DataField=   "cdate"
      Columns(25).DataType=   135
      Columns(25).FieldLen=   256
      Columns(26).Width=   3200
      Columns(26).Visible=   0   'False
      Columns(26).Caption=   "muser"
      Columns(26).Name=   "muser"
      Columns(26).CaptionAlignment=   0
      Columns(26).DataField=   "muser"
      Columns(26).DataType=   8
      Columns(26).FieldLen=   256
      Columns(27).Width=   3200
      Columns(27).Visible=   0   'False
      Columns(27).Caption=   "mdate"
      Columns(27).Name=   "mdate"
      Columns(27).Alignment=   1
      Columns(27).CaptionAlignment=   1
      Columns(27).DataField=   "mdate"
      Columns(27).DataType=   135
      Columns(27).FieldLen=   256
      _ExtentX        =   22463
      _ExtentY        =   6800
      _StockProps     =   79
      Caption         =   "Sundry Costs"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TabDlg.SSTab tabSelect 
      Height          =   4275
      Left            =   120
      TabIndex        =   10
      Top             =   1320
      Width           =   12855
      _ExtentX        =   22675
      _ExtentY        =   7541
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      TabCaption(0)   =   "All"
      TabPicture(0)   =   "frmSundryManagement.frx":0069
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).ControlCount=   0
      TabCaption(1)   =   "Taxi"
      TabPicture(1)   =   "frmSundryManagement.frx":0085
      Tab(1).ControlEnabled=   0   'False
      Tab(1).ControlCount=   0
      TabCaption(2)   =   "Food / Client Costs"
      TabPicture(2)   =   "frmSundryManagement.frx":00A1
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
   End
   Begin MSComCtl2.DTPicker datAuthorisedFrom 
      Height          =   315
      Left            =   8220
      TabIndex        =   21
      Tag             =   "NOCHECK"
      ToolTipText     =   "The deadline date from"
      Top             =   420
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   83623937
      CurrentDate     =   37870
   End
   Begin MSComCtl2.DTPicker datAuthorisedTo 
      Height          =   315
      Left            =   8220
      TabIndex        =   22
      Tag             =   "NOCHECK"
      ToolTipText     =   "The deadline date to"
      Top             =   840
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   83623937
      CurrentDate     =   37870
   End
   Begin MSComCtl2.DTPicker datApprovedFrom 
      Height          =   315
      Left            =   10320
      TabIndex        =   23
      Tag             =   "NOCHECK"
      ToolTipText     =   "The deadline date from"
      Top             =   420
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   83623937
      CurrentDate     =   37870
   End
   Begin MSComCtl2.DTPicker datApprovedTo 
      Height          =   315
      Left            =   10320
      TabIndex        =   24
      Tag             =   "NOCHECK"
      ToolTipText     =   "The deadline date to"
      Top             =   840
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   83623937
      CurrentDate     =   37870
   End
   Begin MSComCtl2.DTPicker datOrderedDateFrom 
      Height          =   315
      Left            =   6120
      TabIndex        =   25
      Tag             =   "NOCHECK"
      ToolTipText     =   "The deadline date from"
      Top             =   420
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   83623937
      CurrentDate     =   37870
   End
   Begin MSComCtl2.DTPicker datOrderedDateTo 
      Height          =   315
      Left            =   6120
      TabIndex        =   26
      Tag             =   "NOCHECK"
      ToolTipText     =   "The deadline date to"
      Top             =   840
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   83623937
      CurrentDate     =   37870
   End
   Begin VB.Label lblSelectionFormula 
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   6300
      TabIndex        =   44
      Top             =   6900
      Visible         =   0   'False
      Width           =   4635
   End
   Begin VB.Label lblCaption 
      Caption         =   "Passenger"
      Height          =   255
      Index           =   11
      Left            =   12000
      TabIndex        =   40
      Top             =   840
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Caption         =   "Entered By"
      Height          =   255
      Index           =   10
      Left            =   12000
      TabIndex        =   38
      Top             =   120
      Width           =   1995
   End
   Begin VB.Label lblCaption 
      Caption         =   "To"
      Height          =   255
      Index           =   7
      Left            =   7740
      TabIndex        =   36
      Top             =   780
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "From"
      Height          =   255
      Index           =   6
      Left            =   7740
      TabIndex        =   35
      Top             =   420
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Caption         =   "To"
      Height          =   255
      Index           =   0
      Left            =   9840
      TabIndex        =   34
      Top             =   780
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "From"
      Height          =   255
      Index           =   1
      Left            =   9840
      TabIndex        =   33
      Top             =   420
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "Approved Date"
      Height          =   255
      Index           =   3
      Left            =   9840
      TabIndex        =   32
      Top             =   120
      Width           =   1995
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "Authorised Date"
      Height          =   255
      Index           =   2
      Left            =   7740
      TabIndex        =   31
      Top             =   120
      Width           =   1995
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "Status"
      Height          =   255
      Index           =   4
      Left            =   4440
      TabIndex        =   30
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "Ordered Date"
      Height          =   255
      Index           =   5
      Left            =   5640
      TabIndex        =   29
      Top             =   120
      Width           =   1995
   End
   Begin VB.Label lblCaption 
      Caption         =   "From"
      Height          =   255
      Index           =   8
      Left            =   5700
      TabIndex        =   28
      Top             =   360
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Caption         =   "To"
      Height          =   255
      Index           =   9
      Left            =   5640
      TabIndex        =   27
      Top             =   780
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order By"
      Height          =   255
      Index           =   20
      Left            =   120
      TabIndex        =   8
      Top             =   7200
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job ID"
      Height          =   255
      Index           =   16
      Left            =   120
      TabIndex        =   4
      Top             =   840
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Project #"
      Height          =   255
      Index           =   33
      Left            =   120
      TabIndex        =   3
      Top             =   480
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Caption         =   "Project Type"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   49
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   1155
   End
End
Attribute VB_Name = "frmSundryManagement"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmbJobID_Click()
cmdRefresh_Click
End Sub

Private Sub cmbJobID_DropDown()

If cmbJobID.Tag = "STOP" Then Exit Sub

Dim l_strSQL As String
l_strSQL = "SELECT jobID AS 'Job ID', jobtype AS 'Job Type', companyname AS 'Company', startdate AS 'Start Date', productname as 'Product', title1 AS 'Title', fd_status AS 'Status' FROM job "
l_strSQL = l_strSQL & " WHERE fd_status <> 'Cancelled' "
l_strSQL = l_strSQL & " AND fd_status <> 'Cancel' "
l_strSQL = l_strSQL & " AND fd_status <> 'Unavavil' "
l_strSQL = l_strSQL & " AND fd_status <> 'Unavailable' "
l_strSQL = l_strSQL & " AND fd_status <> 'Quick' "
l_strSQL = l_strSQL & " AND fd_status <> 'Quickie' "
l_strSQL = l_strSQL & " AND fd_status <> 'aMeeting' "
l_strSQL = l_strSQL & " AND fd_status <> 'Meeting' "
l_strSQL = l_strSQL & " AND fd_status <> 'Pencil' "
l_strSQL = l_strSQL & " AND fd_status <> 'No charge' "
l_strSQL = l_strSQL & " AND fd_status <> 'Costed' "
l_strSQL = l_strSQL & " AND fd_status <> 'Cancel' "
l_strSQL = l_strSQL & " AND fd_status <> 'Sent To Accounts' "
l_strSQL = l_strSQL & " AND projectnumber = '" & cmbProject.Text & "' ORDER BY startdate;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbJobID.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

cmbJobID.Columns(0).Width = 800
cmbJobID.Columns(1).Width = 800
cmbJobID.Columns(2).Width = 1800
cmbJobID.Columns(3).Width = 1000
cmbJobID.Columns(4).Width = 2000
cmbJobID.Columns(5).Width = 2000
cmbJobID.Columns(6).Width = 1000


End Sub

Private Sub cmbProject_Click()
cmdRefresh_Click
End Sub

Private Sub cmbProject_DropDown()

If cmbProject.Tag = "STOP" Then Exit Sub

Dim l_strSQL As String
l_strSQL = "SELECT project.projectnumber, project.projectID, project.companyname, project.contactname, project.fd_status, product.name as productname, product.productID, project.companyID, project.contactID FROM project LEFT JOIN product ON project.productID = product.productID WHERE (allocation = '" & cmbAllocation.Text & "') AND (projectnumber LIKE '" & QuoteSanitise(cmbProject.Text) & "%') AND (project.fd_status IS NULL OR (project.fd_status <> 'COMPLETED' AND project.fd_status <> 'CANCEL' AND project.fd_status <> 'CANCELLED'  AND project.fd_status <> 'SENT TO ACCOUNTS' AND project.fd_status <> 'INVOICED')) ORDER BY projectnumber;"
Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbProject.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmbRunner_Click()
cmdRefresh_Click
End Sub

Private Sub cmbRunner_KeyPress(KeyAscii As Integer)
AutoMatch cmbRunner, KeyAscii
End Sub

Private Sub cmdApprove_Click()


If grdSundryCosts.Rows = 0 Then Exit Sub

If CheckAccess("/approvesundry") Then
    ApproveSundry grdSundryCosts.Columns("sundrycostID").Text
    Dim l_lngBookmark As Long
    l_lngBookmark = grdSundryCosts.Bookmark
    
    adoSundry.Refresh
    
    grdSundryCosts.Bookmark = l_lngBookmark
End If

End Sub

Private Sub cmdAuthoriseSundry_Click()

If grdSundryCosts.Rows = 0 Then Exit Sub

If CheckAccess("/authorisesundry") Then
    AuthoriseSundry grdSundryCosts.Columns("sundrycostID").Text
    Dim l_lngBookmark As Long
    l_lngBookmark = grdSundryCosts.Bookmark
    
    adoSundry.Refresh
    
    grdSundryCosts.Bookmark = l_lngBookmark
    
    
End If

End Sub

Private Sub cmdClear_Click()

ClearFields Me
cmdRefresh_Click

End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdNewSundry_Click()

Dim l_intMsg As Integer

If Not CheckAccess("/newsundry") Then Exit Sub

If Val(cmbProject.Text) = 0 Then
    MsgBox "You must have a project number before adding sundry costs", vbExclamation
    Exit Sub
End If

'only do this next section for non-overhead coded sundry costs
If Not IsOverHeadCode(cmbProject.Text) Then
    If Val(cmbJobID.Text) = 0 Then
        l_intMsg = MsgBox("WARNING " & UCase(g_strUserName) & ": READ THIS MESSAGE!" & vbCrLf & vbCrLf & "You have not entered a jobID!" & vbCrLf & vbCrLf & "In order that sundry costs can be verified, you should always use a job ID. If you are unsure as to which job ID to use, try to locate your job on using Schedule page. Open it by clicking, and then add sundry costs using the Sundry tab." & vbCrLf & vbCrLf & "Now that you have read this message, are you sure you want to add a sundry cost without one?", vbYesNo + vbExclamation)
        If l_intMsg = vbNo Then Exit Sub
    End If

    'skip the section about jobs
    GoTo PROC_NoJobNumber
End If

Dim l_strStatus As String
l_strStatus = GetData("job", "fd_status", "jobID", Val(cmbJobID.Text))

If Val(cmbJobID.Text) = 0 Then
    GoTo PROC_NoJobNumber
End If
      
If GetStatusNumber(l_strStatus) >= 2 And GetStatusNumber(l_strStatus) <= 5 Then

PROC_NoJobNumber:

    frmSundry.cmbJobID.Text = cmbJobID.Text
    frmSundry.cmbProject.Text = cmbProject.Text

    frmSundry.datSundryDateOrdered.Value = Date
    frmSundry.cmbSundryTimeOrdered.Text = Format(Time, "HH:MM")
    
    frmSundry.datSundryDateWanted.Value = Date
    frmSundry.cmbSundryTimeWanted.Text = Format(Time, "HH:MM")
    
    frmSundry.ddnSundryEnteredBy.Text = cmbRunner.Text
    
    If tabSelect.Caption = "Taxi" Then
        frmSundry.tabSundry.Tab = 0
    Else
        frmSundry.tabSundry.Tab = 1
    End If
    
    frmSundry.Show vbModal
    
    If frmSundry.Tag <> "CANCELLED" Then
    
        Dim l_strSundryType As String
        l_strSundryType = IIf(frmSundry.tabSundry.Caption = "Taxi", "Taxi", "Food")
        
        With frmSundry
        
            SaveSundry Val(.lblSundryID.Caption), l_strSundryType, Val(.cmbJobID.Text), Val(.cmbProject.Text), .datSundryDateOrdered.Value & " " & .cmbSundryTimeOrdered.Text, .datSundryDateWanted & " " & .cmbSundryTimeWanted, .txtSundryPickupTime.Text, .txtSundryAccountNumber.Text, .txtSundryPassenger.Text, _
                        .txtSundryLocation.Text, .txtSundryDestination.Text, .txtSundryComments.Text, .cmbRequestedBy.Text, .ddnSundryEnteredBy.Text, .txtSundryDetails.Text, .txtSundrySupplier.Text, Val(.txtSundryAmount.Text), .chkPersonal.Value, .cmbOverheads.Text
        End With
        
    End If
            
    Unload frmSundry
    Set frmSundry = Nothing

    adoSundry.Refresh
Else
    MsgBox "You can not add sundry costs for jobs of this status (" & l_strStatus & ")", vbExclamation
End If
    

End Sub

Private Sub cmdPrint_Click()

If Not CheckAccess("/printsundrysearch") Then Exit Sub

'PrintCrystalReportUsingSQL g_strLocationOfCrystalReportFiles & "sundrysearch.rpt", adoSundry.RecordSource, g_blnPreviewReport

PrintCrystalReport g_strLocationOfCrystalReportFiles & "sundrysearch.rpt", lblSelectionFormula.Caption, g_blnPreviewReport

End Sub

Private Sub cmdRefresh_Click()

Dim l_strSQL As String
Dim l_strFormula As String


l_strSQL = "SELECT * FROM sundrycost WHERE "
l_strSQL = l_strSQL & " (sundrycostID > 0)"

l_strFormula = "{sundrycost.sundrycostID} > 0"

'is there any search criterea
If cmbProject.Text <> "" Then
    l_strSQL = l_strSQL & " AND (projectnumber = '" & cmbProject.Text & "')"
    l_strFormula = l_strFormula & "AND {sundrycost.projectnumber} = " & cmbProject.Text
End If
    
If cmbJobID.Text <> "" Then
    l_strSQL = l_strSQL & " AND (jobID = '" & cmbJobID.Text & "')"
    l_strFormula = l_strFormula & "AND {sundrycost.jobID} = " & cmbJobID.Text
End If

If txtPassenger.Text <> "" Then
    l_strSQL = l_strSQL & " AND (passengers LIKE '%" & txtPassenger.Text & "%')"
    l_strFormula = l_strFormula & "AND {sundrycost.passengers} LIKE '*" & txtPassenger.Text & "*'"
End If

If Not IsNull(datAuthorisedFrom.Value) Then
    Dim l_datAuthorisedFrom As Date
    l_datAuthorisedFrom = Format(datAuthorisedFrom.Value, vbShortDateFormat)
    l_strSQL = l_strSQL & "AND (sundrycost.authoriseddate >= '" & FormatSQLDate(l_datAuthorisedFrom & " 00:00") & "') "
    
    l_strFormula = l_strFormula & "AND {sundrycost.authoriseddate} >= #" & Format(l_datAuthorisedFrom, "YYYY/MM/DD") & " 00:00#"
    
End If

If Not IsNull(datAuthorisedTo.Value) Then
    Dim l_datAuthorisedTo As Date
    l_datAuthorisedTo = Format(datAuthorisedTo.Value, vbShortDateFormat)
    l_strSQL = l_strSQL & "AND (sundrycost.authoriseddate <= '" & FormatSQLDate(l_datAuthorisedFrom & " 23:59") & "') "
    
    l_strFormula = l_strFormula & "AND {sundrycost.authoriseddate} <= #" & Format(l_datAuthorisedFrom, "YYYY/MM/DD") & " 23:59#"
    
End If

If Not IsNull(datApprovedFrom.Value) Then
    Dim l_datApprovedFrom As Date
    l_datApprovedFrom = Format(datApprovedFrom.Value, vbShortDateFormat)
    l_strSQL = l_strSQL & "AND (sundrycost.approveddate >= '" & FormatSQLDate(l_datApprovedFrom & " 00:00") & "') "
    
    l_strFormula = l_strFormula & "AND {sundrycost.approveddate } >= #" & Format(l_datApprovedFrom, "YYYY/MM/DD") & " 00:00#"
    
End If

If Not IsNull(datApprovedTo.Value) Then
    Dim l_datApprovedTo As Date
    l_datApprovedTo = Format(datApprovedTo.Value, vbShortDateFormat)
    l_strSQL = l_strSQL & "AND (sundrycost.approveddate <= '" & FormatSQLDate(l_datApprovedTo & " 23:59") & "') "
    
    l_strFormula = l_strFormula & "AND {sundrycost.approveddate } <= #" & Format(l_datApprovedTo, "YYYY/MM/DD") & " 23:59#"
End If

If Not IsNull(datOrderedDateFrom.Value) Then
    Dim l_datOrderedDateFrom As Date
    l_datOrderedDateFrom = Format(datOrderedDateFrom.Value, vbShortDateFormat)
    l_strSQL = l_strSQL & "AND (sundrycost.dateordered >= '" & FormatSQLDate(l_datOrderedDateFrom & " 00:00") & "') "
    
    l_strFormula = l_strFormula & "AND {sundrycost.dateordered} >= #" & Format(l_datOrderedDateFrom, "YYYY/MM/DD") & " 00:00#"
End If

If Not IsNull(datOrderedDateTo.Value) Then
    Dim l_datOrderedDateTo As Date
    l_datOrderedDateTo = Format(datOrderedDateTo.Value, vbShortDateFormat)
    l_strSQL = l_strSQL & "AND (sundrycost.dateordered <= '" & FormatSQLDate(l_datOrderedDateTo & " 23:59") & "') "
    
    l_strFormula = l_strFormula & "AND {sundrycost.dateordered} <= #" & Format(l_datOrderedDateTo, "YYYY/MM/DD") & " 23:59#"
End If

'the different types...



Dim l_strStatusString As String
Dim l_strStatusFormula As String
'authorised
If chkShow(0).Value = vbChecked Then
    
    If l_strStatusString = "" Then
        l_strStatusString = ""
        l_strStatusFormula = ""
        
    Else
        l_strStatusString = l_strStatusString & " OR "
        l_strStatusFormula = l_strStatusFormula & " OR "
    End If
    
    l_strStatusString = l_strStatusString & " (sundrycost.fd_status = 'Authorised')"
    
    l_strStatusFormula = l_strStatusFormula & "{sundrycost.fd_status} = 'Authorised'"
    
End If

'approved
If chkShow(1).Value = vbChecked Then
    
    If l_strStatusString = "" Then
        l_strStatusString = ""
        l_strStatusFormula = ""
    Else
        l_strStatusString = l_strStatusString & " OR "
        l_strStatusFormula = l_strStatusFormula & " OR "
    End If
    
    l_strStatusString = l_strStatusString & " (sundrycost.fd_status = 'Approved')"
    l_strStatusFormula = l_strStatusFormula & "{sundrycost.fd_status} = 'Approved'"
End If

'new
If chkShow(2).Value = vbChecked Then
    If l_strStatusString = "" Then
        l_strStatusString = ""
        l_strStatusFormula = ""
    Else
        l_strStatusString = l_strStatusString & " OR "
        l_strStatusFormula = l_strStatusFormula & " OR "
    End If
    
    l_strStatusString = l_strStatusString & " (sundrycost.fd_status = 'New')"
    l_strStatusFormula = l_strStatusFormula & "{sundrycost.fd_status} = 'New'"
End If

If l_strStatusString = "" Then
    l_strStatusString = ""
    l_strStatusFormula = ""
Else
    l_strStatusString = "(" & l_strStatusString & ")"
    l_strStatusFormula = "(" & l_strStatusFormula & ")"
End If

If l_strStatusString <> "" Then l_strSQL = l_strSQL & " AND " & l_strStatusString
If l_strStatusFormula <> "" Then l_strFormula = l_strFormula & " AND " & l_strStatusFormula

If cmbRunner.Text <> "" Then
    l_strSQL = l_strSQL & " AND (sundrycost.enteredby LIKE '" & cmbRunner.Text & "%')"
    l_strFormula = l_strFormula & "AND {sundrycost.enteredby} LIKE '*" & cmbRunner.Text & "*'"
End If

Select Case tabSelect.Caption
Case "All"

Case "Food / Client Costs"
    l_strSQL = l_strSQL & " AND (sundrytype = 'Food')"
    l_strFormula = l_strFormula & " AND {sundrycost.sundrytype} = 'Food' "

Case "Taxi"
    l_strSQL = l_strSQL & " AND (sundrytype = 'Taxi')"
    l_strFormula = l_strFormula & " AND {sundrycost.sundrytype} = 'Taxi' "
End Select

l_strSQL = l_strSQL & " ORDER BY '" & cmbOrderBy.Text & "' " & cmbDirection.Text

Screen.MousePointer = vbHourglass

'refresh the grid
adoSundry.ConnectionString = g_strConnection
adoSundry.RecordSource = l_strSQL
adoSundry.Refresh

Screen.MousePointer = vbDefault

lblSelectionFormula.Caption = l_strFormula

End Sub

Private Sub cmdRunnersExpeneses_Click()

cmdRefresh_Click

If Not CheckAccess("/printrunnersoverheads") Then Exit Sub

If IsNull(datOrderedDateFrom.Value) Or IsNull(datOrderedDateTo.Value) Then
    MsgBox "Please select both a start and end ordered by date", vbExclamation
    Exit Sub
End If


Dim l_strStartDate As String
Dim l_strEndDate As String
Dim l_strSQL As String
Dim l_strType As String

l_strStartDate = Format(datOrderedDateFrom.Value, "YYYY,MM,DD")
l_strEndDate = Format(datOrderedDateTo.Value, "YYYY,MM,DD")

l_strSQL = "{sundrycost.dateordered} IN (Date(" & l_strStartDate & ") to Date(" & l_strEndDate & "))"
l_strSQL = l_strSQL & " AND {sundrycost.fd_status} = 'Approved'"
l_strSQL = l_strSQL & " AND {sundrycost.sundrytype} = 'Food'"

PrintCrystalReport g_strLocationOfCrystalReportFiles & "runnersoverheads.rpt", l_strSQL, True

End Sub


Private Sub cmdRunnersExpenseReport_Click()

If Not CheckAccess("/printrunnersexpensereport") Then Exit Sub

If cmbRunner.Text = "" Then
    MsgBox "Please select a valid runner from the list before running an expense report", vbExclamation
    Exit Sub
End If

PrintCrystalReport g_strLocationOfCrystalReportFiles & "runnersexpense.rpt", lblSelectionFormula.Caption, False

End Sub

Private Sub Form_Load()

PopulateCombo "allocation", cmbAllocation
PopulateCombo "deliveredby", cmbRunner
cmbAllocation.AddItem ""

cmbOrderBy.Text = "dateordered"
cmbDirection.ListIndex = 1

cmbAllocation.Text = g_strDefaultJobAllocation


cmdRefresh_Click

End Sub

Private Sub Form_Resize()

On Error Resume Next
grdSundryCosts.Height = Me.ScaleHeight - 120 - grdSundryCosts.Top - 120 - picFooter.Height - 120
tabSelect.Width = Me.ScaleWidth - 120 - 120
grdSundryCosts.Width = Me.ScaleWidth - 120 - 120 - 120
tabSelect.Height = Me.ScaleHeight - 120 - tabSelect.Top - picFooter.Height - 120

picFooter.Top = Me.ScaleHeight - picFooter.Height - 120
picFooter.Left = Me.ScaleWidth - picFooter.Width - 60
End Sub

Private Sub grdSundryCosts_DblClick()

If grdSundryCosts.Rows = 0 Then Exit Sub
ShowSundryCost Val(grdSundryCosts.Columns("sundrycostID").Text)

Dim l_lngBookmark As Long
l_lngBookmark = grdSundryCosts.Bookmark

adoSundry.Refresh

grdSundryCosts.Bookmark = l_lngBookmark

End Sub

Private Sub grdSundryCosts_HeadClick(ByVal ColIndex As Integer)

If cmbOrderBy.Text <> grdSundryCosts.Columns(ColIndex).DataField Then
    cmbOrderBy.Text = grdSundryCosts.Columns(ColIndex).DataField
Else
    cmbDirection.ListIndex = 1 - cmbDirection.ListIndex
End If

cmdRefresh_Click

End Sub

Private Sub grdSundryCosts_RowLoaded(ByVal Bookmark As Variant)
Dim l_intLoop As Integer

If grdSundryCosts.Columns("authoriseddate").Text <> "" Then
    
    For l_intLoop = 0 To grdSundryCosts.Columns.Count - 1
        grdSundryCosts.Columns(l_intLoop).CellStyleSet "authorised"
    Next
    
Else
   For l_intLoop = 0 To grdSundryCosts.Columns.Count - 1
        grdSundryCosts.Columns(l_intLoop).CellStyleSet ""
    Next
End If

If grdSundryCosts.Columns("approveddate").Text <> "" Then
    For l_intLoop = 0 To grdSundryCosts.Columns.Count - 1
        grdSundryCosts.Columns(l_intLoop).CellStyleSet "approved"
    Next
End If
End Sub

Private Sub tabSelect_Click(PreviousTab As Integer)
cmdRefresh_Click
End Sub

