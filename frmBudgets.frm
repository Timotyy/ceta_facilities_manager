VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmBudgets 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Budget Information"
   ClientHeight    =   10905
   ClientLeft      =   3570
   ClientTop       =   3090
   ClientWidth     =   10590
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10905
   ScaleWidth      =   10590
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraLanguageAnalysisData 
      Height          =   1635
      Left            =   180
      TabIndex        =   5
      Top             =   5280
      Visible         =   0   'False
      Width           =   6135
      Begin VB.CommandButton cmdSubmitForLanguageAnalysis 
         Appearance      =   0  'Flat
         Caption         =   "Submit for Language Analysis"
         Height          =   510
         Left            =   180
         MaskColor       =   &H00FFFFFF&
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Play associated clips"
         Top             =   240
         UseMaskColor    =   -1  'True
         Width           =   2595
      End
      Begin VB.Label lblLangCode 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Index           =   1
         Left            =   3780
         TabIndex        =   16
         Tag             =   "CLEARFIELDS"
         Top             =   480
         Width           =   975
      End
      Begin VB.Label lblLangCode 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Index           =   2
         Left            =   3780
         TabIndex        =   15
         Tag             =   "CLEARFIELDS"
         Top             =   840
         Width           =   975
      End
      Begin VB.Label lblLangCode 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Index           =   3
         Left            =   3780
         TabIndex        =   14
         Tag             =   "CLEARFIELDS"
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label lblLangConfidence 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Index           =   1
         Left            =   4980
         TabIndex        =   13
         Tag             =   "CLEARFIELDS"
         Top             =   480
         Width           =   975
      End
      Begin VB.Label lblLangConfidence 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Index           =   2
         Left            =   4980
         TabIndex        =   12
         Tag             =   "CLEARFIELDS"
         Top             =   840
         Width           =   975
      End
      Begin VB.Label lblLangConfidence 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Index           =   3
         Left            =   4980
         TabIndex        =   11
         Tag             =   "CLEARFIELDS"
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label lblCaption 
         Caption         =   "Confidence %"
         Height          =   255
         Index           =   65
         Left            =   4980
         TabIndex        =   10
         Top             =   180
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Lang Codes"
         Height          =   255
         Index           =   64
         Left            =   3840
         TabIndex        =   9
         Top             =   180
         Width           =   915
      End
      Begin VB.Label lblAnalysisDate 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   180
         TabIndex        =   8
         Tag             =   "CLEARFIELDS"
         Top             =   1200
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "Date of Analysis"
         Height          =   255
         Index           =   69
         Left            =   240
         TabIndex        =   7
         Top             =   900
         Width           =   1515
      End
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "Search"
      Height          =   495
      Left            =   5760
      TabIndex        =   4
      ToolTipText     =   "Search resource list"
      Top             =   9720
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.ComboBox cmbDirection 
      Height          =   315
      ItemData        =   "frmBudgets.frx":0000
      Left            =   3960
      List            =   "frmBudgets.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   2
      ToolTipText     =   "Ascending/Descending"
      Top             =   9780
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ComboBox cmbOrderBy 
      Height          =   315
      Left            =   1680
      TabIndex        =   1
      Text            =   "cmbOrderBy"
      ToolTipText     =   "Order data by "
      Top             =   9780
      Visible         =   0   'False
      Width           =   2235
   End
   Begin MSAdodcLib.Adodc adoBudgets 
      Height          =   330
      Left            =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoBudgets"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdBudgets 
      Align           =   1  'Align Top
      Bindings        =   "frmBudgets.frx":0019
      Height          =   5115
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10590
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      BackColorOdd    =   16777152
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   18680
      _ExtentY        =   9022
      _StockProps     =   79
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order By"
      Height          =   255
      Index           =   20
      Left            =   540
      TabIndex        =   3
      Top             =   9780
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "frmBudgets"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdSearch_Click()
adoBudgets.ConnectionString = g_strConnection
adoBudgets.RecordSource = "SELECT * FROM budget ORDER BY " & cmbOrderBy.Text & " " & cmbDirection.Text
adoBudgets.Refresh
grdBudgets.Columns(0).Visible = False
End Sub

'Private Sub cmdSubmitForLanguageAnalysis_Click()
'
'Dim l_strSQL As String
'
'If Val(txtClipID.Text) = 0 Then Exit Sub
'
'l_strSQL = "INSERT INTO JeliMediaAnalysis (companyID, clipreference, eventID) VALUES ("
'l_strSQL = l_strSQL & Val(lblCompanyID.Caption) & ", "
'l_strSQL = l_strSQL & "'" & QuoteSanitise(txtReference.Text) & "', "
'l_strSQL = l_strSQL & Val(txtClipID.Text) & ");"
'
'ExecuteSQL l_strSQL, g_strExecuteError
'CheckForSQLError
'
'MsgBox "File submitted for language analysis.", vbInformation
'
'End Sub
'
Private Sub Form_Load()
CenterForm Me
cmbOrderBy.Text = "code"
cmbDirection.ListIndex = 1
cmdSearch_Click

End Sub

Private Sub Form_Resize()
On Error Resume Next
grdBudgets.height = Me.ScaleHeight
End Sub

Private Sub grdBudgets_HeadClick(ByVal ColIndex As Integer)

If cmbOrderBy.Text <> grdBudgets.Columns(ColIndex).DataField Then
    cmbOrderBy.Text = grdBudgets.Columns(ColIndex).DataField
Else
    cmbDirection.ListIndex = 1 - cmbDirection.ListIndex
End If

adoBudgets.ConnectionString = g_strConnection
adoBudgets.RecordSource = "SELECT * FROM budget ORDER BY " & cmbOrderBy.Text & " " & cmbDirection.Text
adoBudgets.Refresh


End Sub

