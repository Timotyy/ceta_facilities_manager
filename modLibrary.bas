Attribute VB_Name = "modLibrary"
Option Explicit

Public Function CalcDurationFromString(lp_strDurationString As String) As Long

CalcDurationFromString = 60 * Val(lp_strDurationString)
CalcDurationFromString = CalcDurationFromString + Val(Mid(lp_strDurationString, 4, Len(lp_strDurationString) - 4))

End Function

Public Sub PrintBarcodeLabels()
If Not CheckAccess("/printbarcodelabels") Then Exit Sub
frmBarcodePrint.Show vbModal
Unload frmBarcodePrint
Set frmBarcodePrint = Nothing
End Sub

Public Function CheckAllComplaintMastersHere(l_strSourceID1 As Variant, l_datMasterArrived1 As Variant, l_strSourceID2 As Variant, l_datMasterArrived2 As Variant, l_strSourceID3 As Variant, l_datMasterArrived3 As Variant) As Boolean

Dim l_blnMaster1 As Boolean
Dim l_blnMaster2 As Boolean
Dim l_blnMaster3 As Boolean

CheckAllComplaintMastersHere = False

If Trim(" " & l_strSourceID1) <> "" And Not IsNull(l_datMasterArrived1) Then
    l_blnMaster1 = True
ElseIf Trim(" " & l_strSourceID1) <> "" And IsNull(l_datMasterArrived1) Then
    l_blnMaster1 = False
ElseIf Trim(" " & l_strSourceID1) = "" Then
    l_blnMaster1 = True
End If
    
If Trim(" " & l_strSourceID2) <> "" And Not IsNull(l_datMasterArrived2) Then
    l_blnMaster2 = True
ElseIf Trim(" " & l_strSourceID2) <> "" And IsNull(l_datMasterArrived2) Then
    l_blnMaster2 = False
ElseIf Trim(" " & l_strSourceID2) = "" Then
    l_blnMaster2 = True
End If
    
If Trim(" " & l_strSourceID3) <> "" And Not IsNull(l_datMasterArrived3) Then
    l_blnMaster3 = True
ElseIf Trim(" " & l_strSourceID3) <> "" And IsNull(l_datMasterArrived3) Then
    l_blnMaster3 = False
ElseIf Trim(" " & l_strSourceID3) = "" Then
    l_blnMaster3 = True
End If
    
If l_blnMaster1 = True And l_blnMaster2 = True And l_blnMaster3 = True Then CheckAllComplaintMastersHere = True
If Trim(" " & l_strSourceID1) = "" And Trim(" " & l_strSourceID2) = "" And Trim(" " & l_strSourceID3) = "" Then CheckAllComplaintMastersHere = False

End Function

Public Sub ImportRapidsLog(lp_lngTapeID As Long, lp_strBarcode As String)

Dim l_rsXML As New ADODB.Recordset
Dim l_rsTapelist As New ADODB.Recordset
Dim l_rsEventList As New ADODB.Recordset
Dim l_rsTapename As New ADODB.Recordset
Dim l_rsEvent As New ADODB.Recordset
Dim l_fldChild As ADODB.Field
Dim l_fldGrandChild As ADODB.Field
Dim l_rsXMLCol As ADODB.Field
Dim l_rsEventListCol As ADODB.Field
Dim l_strSQL As String, l_strValue As String, l_strClipname As String, l_strClipcomment As String, l_strInpoint As String, l_strOutpoint As String, l_strEventTape As String

Dim l_strFullPathToFile As String

Dim l_blnFoundTape As Boolean

MDIForm1.dlgMain.Filter = "XML Files|*.xml"
MDIForm1.dlgMain.InitDir = g_strLocationOfRapidsLogs
MDIForm1.dlgMain.Filename = g_strLocationOfRapidsLogs & "\*.xml"
MDIForm1.dlgMain.ShowOpen

l_strFullPathToFile = MDIForm1.dlgMain.Filename

If l_strFullPathToFile <> "" And Right(l_strFullPathToFile, 5) <> "*.xml" Then
    
    Set l_rsXML = New ADODB.Recordset

    On Error GoTo PROC_EXIT
    l_rsXML.ActiveConnection = "Provider=MSDAOSP; Data Source=MSXML2.DSOControl.2.6;"
    l_rsXML.Open l_strFullPathToFile
    l_blnFoundTape = False
    Do While Not l_rsXML.EOF
        For Each l_rsXMLCol In l_rsXML.Fields
            If l_rsXMLCol.Type = adChapter Then
                If l_rsXMLCol.Name = "TAPENAMELIST" Then
                    Set l_rsTapelist = l_rsXMLCol.Value
                    l_rsTapelist.MoveFirst
                    Do While Not l_rsTapelist.EOF
                        For Each l_fldChild In l_rsTapelist.Fields
                            If l_fldChild.Type = adChapter Then
                                Set l_rsTapename = l_fldChild.Value
                                l_rsTapename.MoveFirst
                                Do While Not l_rsTapename.EOF
                                    For Each l_fldGrandChild In l_rsTapename.Fields
                                        If l_fldGrandChild.Name = "tapename" Then
                                            l_strValue = Trim(" " & l_fldGrandChild.Value)
                                            If l_strValue = lp_strBarcode Then l_blnFoundTape = True
                                        End If
                                    Next
                                    l_rsTapename.MoveNext
                                Loop
                                l_rsTapename.Close
                                Set l_rsTapename = Nothing
                            End If
                        Next
                        l_rsTapelist.MoveNext
                    Loop
                    l_rsTapelist.Close
                    If l_blnFoundTape = False Then MsgBox "No items found for this tape", vbInformation: GoTo PROC_EXIT
                ElseIf l_rsXMLCol.Name = "RECORDLIST" Then
                    Set l_rsEventList = l_rsXMLCol.Value
                    l_rsEventList.MoveFirst
                    Do While Not l_rsEventList.EOF
                        For Each l_fldChild In l_rsEventList.Fields
                            If l_fldChild.Type = adChapter Then
                                Set l_rsEvent = l_fldChild.Value
                                l_rsEvent.MoveFirst
                                Do While Not l_rsEvent.EOF
                                    For Each l_fldGrandChild In l_rsEvent.Fields
                                        If l_fldGrandChild.Name = "clipname" Then l_strClipname = l_fldGrandChild.Value
                                        If l_fldGrandChild.Name = "comment" Then l_strClipcomment = l_fldGrandChild.Value
                                        If l_fldGrandChild.Name = "inpoint" Then l_strInpoint = l_fldGrandChild.Value
                                        If l_fldGrandChild.Name = "outpoint" Then l_strOutpoint = l_fldGrandChild.Value
                                        If l_fldGrandChild.Name = "tapename" Then l_strEventTape = l_fldGrandChild.Value
                                    Next
                                    l_rsEvent.MoveNext
                                    If l_strEventTape = lp_strBarcode Then
                                        l_strSQL = "INSERT INTO tapeevents (libraryID, eventtitle, notes, timecodestart, timecodestop, fd_length) VALUES ("
                                        l_strSQL = l_strSQL & "'" & lp_lngTapeID & "', "
                                        l_strSQL = l_strSQL & "'" & l_strClipname & "', "
                                        l_strSQL = l_strSQL & "'" & l_strClipcomment & "', "
                                        l_strSQL = l_strSQL & "'" & l_strInpoint & "', "
                                        l_strSQL = l_strSQL & "'" & l_strOutpoint & "', "
                                        l_strSQL = l_strSQL & "'" & CalcDuration(l_strInpoint, l_strOutpoint, True, False) & "');"
                                        ExecuteSQL l_strSQL, g_strExecuteError
                                         
                                        CheckForSQLError
                                    End If
                                Loop
                                l_rsEvent.Close
                                Set l_rsEvent = Nothing
                            End If
                        Next
                        l_rsEventList.MoveNext
                    Loop
                    l_rsEventList.Close
                End If
            End If
        Next
    l_rsXML.MoveNext
    Loop
End If

PROC_EXIT:

If l_rsTapelist.state = adStateOpen Then l_rsTapelist.Close: Set l_rsTapelist = Nothing
If l_rsEventList.state = adStateOpen Then l_rsEventList.Close: Set l_rsEventList = Nothing
If l_rsXML.state = adStateOpen Then l_rsXML.Close: Set l_rsXML = Nothing

End Sub

Public Sub LoadLabelsForm()
With frmLabels
    .Text1(0) = frmLibrary.txtTitle
    .Text1(1) = frmLibrary.txtSubTitle
    .Text1(2) = frmLibrary.cmbVersion
    .Text1(3) = frmLibrary.cmbRecordStandard
    .Text1(4) = frmLibrary.txtProgDuration
    .Text1(5) = frmLibrary.txtJobID
    .Text1(6).Text = GetData("job", "orderreference", "jobID", Val(frmLibrary.txtJobID.Text))
    If .Text1(6).Text = "STAR ORDER" Then .Text1(6).Text = GetData("extendedjobdetail", "bbcordernumber", "jobdetailID", frmLibrary.txtJobDetailID)
    .Text1(7) = frmLibrary.txtBarcode
    .Text1(8).Text = CIA_CODE128(frmLibrary.txtBarcode.Text)
    .Text1(9) = frmLibrary.cmbRecordGeometry
    .Text1(10) = frmLibrary.cmbRecordRatio
    .Text1(11) = frmLibrary.cmbRecordFormat
    .Text1(12) = frmLibrary.cmbChannel1
    .Text1(13) = frmLibrary.cmbChannel2
    .Text1(14) = frmLibrary.cmbChannel3
    .Text1(15) = frmLibrary.cmbChannel4
    .Text1(16) = frmLibrary.cmbChannel5
    .Text1(17) = frmLibrary.cmbChannel6
    .Text1(18) = frmLibrary.cmbChannel7
    .Text1(19) = frmLibrary.cmbChannel8
    .Text1(20) = frmLibrary.cmbSeries
    .Text1(21) = frmLibrary.cmbEpisode
    If frmLibrary.cmbEpisodeTo <> "" Then .Text1(21) = .Text1(21) & "-" & frmLibrary.cmbEpisodeTo
    .Text1(22) = frmLibrary.cmbSet
    If frmLibrary.optStationaryType(0).Value = True Or frmLibrary.optStationaryType(3).Value = True Then
        .Option1(0).Value = True
    ElseIf frmLibrary.optStationaryType(1).Value = True Then
        .Option1(1).Value = True
    Else
        .Option1(2).Value = True
    End If
    .Check1.Value = False
End With

End Sub


Public Sub AddEventToTape(l_lngLibraryID As Long, l_strEventTitle As String, l_strEventType As String, l_strClockNumber As String, l_lngSourceLibaryID As Long, l_strAspectRatio As String, l_lngJobID As Long, l_strRunTime As String, lp_datEventDate As Date, Optional lp_strSubTitle As String, Optional lp_strTimecodeStart As String, Optional lp_strTimecodeStop As String)

'add the parameters to the events table
Dim l_strSQL As String

If l_lngJobID <> 0 Then
    Dim l_lngProjectNumber As Long
    l_lngProjectNumber = GetData("job", "projectnumber", "jobID", l_lngJobID)
End If

'create the SQL statement
l_strSQL = "INSERT INTO tapeevents ("
If Trim(" " & lp_strTimecodeStart) <> "" Then
    l_strSQL = l_strSQL & "timecodestart, timecodestop, "
End If
If lp_strSubTitle <> "" Then
    l_strSQL = l_strSQL & "eventmediatype, libraryID, eventtitle, eventsubtitle, cdate, cuser, eventtype, clocknumber, sourcelibraryID, aspectratio, jobid, projectnumber, fd_length, eventdate) VALUES ("
Else
    l_strSQL = l_strSQL & "eventmediatype, libraryID, eventtitle, cdate, cuser, eventtype, clocknumber, sourcelibraryID, aspectratio, jobid, projectnumber, fd_length, eventdate) VALUES ("
End If
If Trim(" " & lp_strTimecodeStart) <> "" Then
    l_strSQL = l_strSQL & "'" & lp_strTimecodeStart & "', '" & lp_strTimecodeStop & "', "
End If
l_strSQL = l_strSQL & "'TAPE', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_lngLibraryID) & "',"
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strEventTitle) & "',"
If lp_strSubTitle <> "" Then
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strSubTitle) & "', "
End If
l_strSQL = l_strSQL & "'" & Format(Date, "yyyy-mm-dd") & "',"
l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strUserName) & "',"
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strEventType) & "',"
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strClockNumber) & "',"
l_strSQL = l_strSQL & "'" & l_lngSourceLibaryID & "',"
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strAspectRatio) & "',"
l_strSQL = l_strSQL & "'" & l_lngJobID & "',"
l_strSQL = l_strSQL & "'" & l_lngProjectNumber & "',"
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strRunTime) & "',"
l_strSQL = l_strSQL & "'" & FormatSQLDate(lp_datEventDate) & "');"

'run the query
ExecuteSQL l_strSQL, g_strExecuteError

'make sure there were no errors
CheckForSQLError

End Sub

Public Function AddTimecodeEventToTape(lp_lngLibraryID As Long, lp_strEventTitle As String, lp_strTimecodeStart As String, lp_strTimecodeStop As String) As Long

'add the parameters to the events table
Dim l_strSQL As String

'create the SQL statement
l_strSQL = "INSERT INTO tapeevents "
l_strSQL = l_strSQL & " (libraryID, eventtitle, cdate, cuser, eventmediatype, timecodestart, timecodestop, fd_length) VALUES ("
l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_lngLibraryID) & "',"
l_strSQL = l_strSQL & "'" & Mid(QuoteSanitise(lp_strEventTitle), 1, 99) & "',"
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "',"
l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strUserName) & "',"
l_strSQL = l_strSQL & "'TAPE',"
l_strSQL = l_strSQL & "'" & lp_strTimecodeStart & "',"
l_strSQL = l_strSQL & "'" & lp_strTimecodeStop & "',"
l_strSQL = l_strSQL & "'" & CalcDuration(lp_strTimecodeStart, lp_strTimecodeStop, True, False, True) & "');"

'run the query
ExecuteSQL l_strSQL, g_strExecuteError

'make sure there were no errors
CheckForSQLError

If Not g_strExecuteError Like "ERROR*" Then AddTimecodeEventToTape = g_lngLastID Else AddTimecodeEventToTape = 0

End Function
Function MakeTechReview()
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/savelibrary") Then
        Exit Function
    End If
    
    frmLibrary.MousePointer = vbHourglass
    
    Dim l_strSQL As String, l_rsOldFaults As ADODB.Recordset
    Dim l_lngNewTechrevID As Long
    
    With frmLibrary
        
        l_lngNewTechrevID = Val(InsertTapeTechReview(Val(.lblLibraryID.Caption), .cmbRecordStandard.Text))
        If Val(.lbltechrevID.Caption) <> 0 Then
            'Its an existing techreview being re-reviewed - copy all existing fields and fault list to a new techrev.
            
            l_strSQL = "UPDATE techrev SET "
            l_strSQL = l_strSQL & "jobID = '" & .txtReviewJobID.Text & "', "
            l_strSQL = l_strSQL & "reviewdate = '" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "reviewuser = '" & g_strFullUserName & "', "
            l_strSQL = l_strSQL & "companyID = '" & .lblReviewCompanyID.Caption & "', "
            l_strSQL = l_strSQL & "companyname = '" & QuoteSanitise(.cmbReviewCompany.Text) & "', "
            l_strSQL = l_strSQL & "contactID = '" & .lblReviewContactID.Caption & "', "
            l_strSQL = l_strSQL & "contactname = '" & .cmbReviewContact & "', "
            l_strSQL = l_strSQL & "flagtitles = '" & .chkTitles.Value & "', "
            l_strSQL = l_strSQL & "flagcaptions = '" & .chkCaptions.Value & "', "
            l_strSQL = l_strSQL & "flagsubtitles = '" & .chkSubtitles.Value & "', "
            l_strSQL = l_strSQL & "flaglogos = '" & .chkLogos.Value & "', "
            l_strSQL = l_strSQL & "flagtextless = '" & .chkTextless.Value & "', "
            l_strSQL = l_strSQL & "flagtrailers = '" & .chkTrailers.Value & "', "
            l_strSQL = l_strSQL & "languagetitles = '" & .cmbTitlesLanguage.Text & "', "
            l_strSQL = l_strSQL & "languagecaptions = '" & .cmbCaptionsLanguage.Text & "', "
            l_strSQL = l_strSQL & "languagesubtitles = '" & .cmbSubtitlesLanguage.Text & "', "
            l_strSQL = l_strSQL & "detailslogos = '" & .txtDetailsLogos.Text & "', "
            l_strSQL = l_strSQL & "detailstextless = '" & .txtDetailsTextless.Text & "', "
            l_strSQL = l_strSQL & "detailstrailers = '" & .txtDetailsTrailers.Text & "', "
            l_strSQL = l_strSQL & "reviewmachine = '" & .cmbReviewMachine.Text & "', "
            l_strSQL = l_strSQL & "notesvideo = '" & QuoteSanitise(.txtTechNotesVideo.Text) & "', "
            l_strSQL = l_strSQL & "notesaudio = '" & QuoteSanitise(.txtTechNotesAudio.Text) & "', "
            l_strSQL = l_strSQL & "notesgeneral = '" & QuoteSanitise(.txtTechNotesGeneral.Text) & "', "
            If .optReviewConclusion(0).Value = True Then
                l_strSQL = l_strSQL & "reviewconclusion = '" & 0 & "', "
            End If
            If .optReviewConclusion(1).Value = True Then
                l_strSQL = l_strSQL & "reviewconclusion = '" & 1 & "', "
            End If
            If .optReviewConclusion(2).Value = True Then
                l_strSQL = l_strSQL & "reviewconclusion = '" & 2 & "', "
            End If
            l_strSQL = l_strSQL & "videotest = '" & .txtVideoTest.Text & "', "
            l_strSQL = l_strSQL & "videoprog = '" & .txtVideoProg.Text & "', "
            l_strSQL = l_strSQL & "chromatest = '" & .txtChromaTest.Text & "', "
            l_strSQL = l_strSQL & "chromaprog = '" & .txtChromaProg.Text & "', "
            l_strSQL = l_strSQL & "blacktest = '" & .txtBlackTest.Text & "', "
            l_strSQL = l_strSQL & "blackprog = '" & .txtBlackProg.Text & "', "
            l_strSQL = l_strSQL & "phasetest = '" & .txtPhaseTest.Text & "', "
            l_strSQL = l_strSQL & "phaseprog = '" & .txtPhaseProg.Text & "', "
            l_strSQL = l_strSQL & "audio1test = '" & .txtA1Test.Text & "', "
            l_strSQL = l_strSQL & "audio1prog = '" & .txtA1Prog.Text & "', "
            l_strSQL = l_strSQL & "audio2test = '" & .txtA2Test.Text & "', "
            l_strSQL = l_strSQL & "audio2prog = '" & .txtA2Prog.Text & "', "
            l_strSQL = l_strSQL & "audio3test = '" & .txtA3Test.Text & "', "
            l_strSQL = l_strSQL & "audio3prog = '" & .txtA3Prog.Text & "', "
            l_strSQL = l_strSQL & "audio4test = '" & .txtA4Test.Text & "', "
            l_strSQL = l_strSQL & "audio4prog = '" & .txtA4Prog.Text & "', "
            l_strSQL = l_strSQL & "audio5test = '" & .txtA5Test.Text & "', "
            l_strSQL = l_strSQL & "audio5prog = '" & .txtA5Prog.Text & "', "
            l_strSQL = l_strSQL & "audio6test = '" & .txtA6Test.Text & "', "
            l_strSQL = l_strSQL & "audio6prog = '" & .txtA6Prog.Text & "', "
            l_strSQL = l_strSQL & "audio7test = '" & .txtA7Test.Text & "', "
            l_strSQL = l_strSQL & "audio7prog = '" & .txtA7Prog.Text & "', "
            l_strSQL = l_strSQL & "audio8test = '" & .txtA8Test.Text & "', "
            l_strSQL = l_strSQL & "audio8prog = '" & .txtA8Prog.Text & "', "
            l_strSQL = l_strSQL & "audio9test = '" & .txtA9Test.Text & "', "
            l_strSQL = l_strSQL & "audio9prog = '" & .txtA9Prog.Text & "', "
            l_strSQL = l_strSQL & "audio10test = '" & .txtA10Test.Text & "', "
            l_strSQL = l_strSQL & "audio10prog = '" & .txtA10Prog.Text & "', "
            l_strSQL = l_strSQL & "audio11test = '" & .txtA11Test.Text & "', "
            l_strSQL = l_strSQL & "audio11prog = '" & .txtA11Prog.Text & "', "
            l_strSQL = l_strSQL & "audio12test = '" & .txtA12Test.Text & "', "
            l_strSQL = l_strSQL & "audio12prog = '" & .txtA12Prog.Text & "', "
            l_strSQL = l_strSQL & "cuetest = '" & .txtCueTest.Text & "', "
            l_strSQL = l_strSQL & "cueprog = '" & .txtCueProg.Text & "', "
            l_strSQL = l_strSQL & "vitclines = '" & .txtvitclines.Text & "', "
            l_strSQL = l_strSQL & "userbits = '" & .txtuserbits.Text & "', "
            l_strSQL = l_strSQL & "timecodecontinuous = '" & .chktimecodecontinuous.Value & "', "
            l_strSQL = l_strSQL & "ltc = '" & .chkltc.Value & "', "
            l_strSQL = l_strSQL & "vitc = '" & .chkvitc.Value & "', "
            l_strSQL = l_strSQL & "ltcvitcmatch = '" & .chkltcvitcmatch.Value & "', "
            l_strSQL = l_strSQL & "firstactivepixel = '" & .txtfirstactivepixel.Text & "', "
            l_strSQL = l_strSQL & "lastactivepixel = '" & .txtlastactivepixel.Text & "', "
            l_strSQL = l_strSQL & "firstactivelinefield1 = '" & .txtfirstactivelinefield1.Text & "', "
            l_strSQL = l_strSQL & "lastactivelinefield1 = '" & .txtlastactivelinefield1.Text & "', "
            l_strSQL = l_strSQL & "firstactivelinefield2= '" & .txtfirstactivelinefield2.Text & "', "
            l_strSQL = l_strSQL & "lastactivelinefield2 = '" & .txtlastactivelinefield2.Text & "', "
            l_strSQL = l_strSQL & "verticalcentreingcorrect = '" & .chkverticalcenteringcorrect.Value & "', "
            l_strSQL = l_strSQL & "horizontalcentreingcorrect = '" & .chkhorizontalcenteringcorrect.Value & "', "
            l_strSQL = l_strSQL & "captionsafe169 = '" & .chkCaptionsafe169.Value & "', "
            l_strSQL = l_strSQL & "captionsafe149 = '" & .chkCaptionsafe149.Value & "', "
            l_strSQL = l_strSQL & "captionsafe43protect = '" & .chkCaptionsafe43protect.Value & "', "
            l_strSQL = l_strSQL & "captionsafe43ebu = '" & .chkCaptionsafe43ebu.Value & "' "
            l_strSQL = l_strSQL & "WHERE techrevID = " & l_lngNewTechrevID
            
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            
            Set l_rsOldFaults = ExecuteSQL("SELECT * FROM techrevfault WHERE techrevID = '" & .lbltechrevID.Caption & "';", g_strExecuteError)
            CheckForSQLError
            
            If l_rsOldFaults.RecordCount > 0 Then
                l_rsOldFaults.MoveFirst
                Do While Not l_rsOldFaults.EOF
                    l_strSQL = "INSERT INTO techrevfault (techrevID, timecode, description, endtimecode, faultgrade) VALUES ("
                    l_strSQL = l_strSQL & "'" & l_lngNewTechrevID & "', "
                    l_strSQL = l_strSQL & "'" & l_rsOldFaults("timecode") & "', "
                    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rsOldFaults("description")) & "', "
                    l_strSQL = l_strSQL & "'" & l_rsOldFaults("endtimecode") & "', "
                    l_strSQL = l_strSQL & "'" & l_rsOldFaults("faultgrade") & "')"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    l_rsOldFaults.MoveNext
                Loop
            End If
            l_rsOldFaults.Close
            Set l_rsOldFaults = Nothing
            
        End If
    End With
    
    ShowLibrary (Val(frmLibrary.lblLibraryID.Caption))
    
    frmLibrary.MousePointer = vbDefault
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume 'PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function CopyTapeToBarcode() As Boolean

If Not CheckAccess("/savelibrary") Then
    
    CopyTapeToBarcode = False
    Exit Function
    
End If

Dim l_strNewLibraryBarcode As String
Dim l_lngNewLibraryID As Long
Dim l_lngOldLibraryID As Long


Dim l_strSQL As String

l_strNewLibraryBarcode = ""

Do While l_strNewLibraryBarcode = ""
    
    'prompt for a new barcode
    frmGetBarcodeNumber.Caption = "Please give the Barcode to copy to..."
    frmGetBarcodeNumber.Show vbModal

    'pick up barcode
    l_strNewLibraryBarcode = UCase(frmGetBarcodeNumber.txtBarcode.Text)
    
    'Check whether the new barcode was accepted, or escape was pressed.
    If l_strNewLibraryBarcode = "" Then
        If MsgBox("No Barcode provided - did you wish to abort the copy?", vbYesNo, "No Barcode") = vbYes Then
            Unload frmGetBarcodeNumber
            CopyTapeToBarcode = False
            Exit Function
        End If
    End If
Loop

l_lngNewLibraryID = GetData("library", "LibraryID", "barcode", l_strNewLibraryBarcode)

'this tape is already in the library, so dont allow this function to proceed
If l_lngNewLibraryID = 0 Then
    MsgBox "Tape Does Not Yet Exist.", vbExclamation, "Error"
    CopyTapeToBarcode = False
    Exit Function
Else
    If MsgBox("Are you sure?", vbYesNo, "Copying master tape onto Tape:" & l_strNewLibraryBarcode) = vbNo Then
        CopyTapeToBarcode = False
        Exit Function
    End If
End If

'clear any fields that are not requried in the copy

With frmLibrary
    
    l_lngOldLibraryID = .lblLibraryID.Caption
    
    .txtSourceBarcode.Text = .txtBarcode.Text
    
    .cmbSourceFormat.Text = .cmbRecordFormat.Text
    .cmbSourceStandard.Text = .cmbRecordStandard.Text
    .cmbSourceGeometry.Text = .cmbRecordGeometry.Text
    .cmbSourceRatio.Text = .cmbRecordRatio.Text
    
    .cmbSourceMachine.Text = ""
    .cmbStockCode.Text = ""
    .cmbRecordMachine.Text = ""
    .cmbRecordFormat.Text = Trim(" " & GetData("library", "format", "libraryID", l_lngNewLibraryID))
    .cmbRecordStandard.Text = ""
    
    .cmbCopyType.Text = "COPY"
    .cmbVersion.Text = "COPY"
    .cmbLocation.Text = "OPERATIONS"
    .cmbShelf.Text = ""
    
    .txtJobDetailID.Text = ""
    If g_optAddFakeJobIDToTapes = 1 Then .txtJobID.Text = "999999"
    .txtProjectNumber.Text = ""
    If Val(.txtInternalReference.Text) = 0 Then .txtInternalReference.Text = GetNextSequence("internalreference")

    .txtBarcode.Text = l_strNewLibraryBarcode
    .lblLibraryID.Caption = l_lngNewLibraryID
    .lbltechrevID.Caption = ""
    .cmbTechReviewList.Text = ""

    SaveLibrary
    
    DoEvents

    CopyAllEvents l_lngOldLibraryID, Val(.lblLibraryID.Caption)
        
    'create a transaction
    CreateLibraryTransaction l_lngNewLibraryID, "Copied Tape", 0
        
    
End With

End Function

Public Function CopyTape() As Boolean

If Not CheckAccess("/savelibrary") Then
    
    CopyTape = False
    Exit Function
    
End If

Dim l_strNewLibraryBarcode As String
Dim l_lngNewLibraryID As Long
Dim l_lngOldLibraryID As Long


Dim l_strSQL As String

'prompt for a new barcode
frmGetBarcodeNumber.Caption = "Please give the Barcode to copy to..."
If g_optAllocateBarcodes = 1 Then frmGetBarcodeNumber.txtBarcode.Text = g_strCompanyPrefix & GetNextSequence("barcodenumber")
'frmGetBarcodeNumber.txtBarcode.Text = g_strCompanyPrefix & GetNextSequence("barcodenumber")
frmGetBarcodeNumber.Show vbModal

'pick up barcode
l_strNewLibraryBarcode = UCase(frmGetBarcodeNumber.txtBarcode.Text)
Unload frmGetBarcodeNumber

'Check whether the new barcode was accepted, or escape was pressed.
If l_strNewLibraryBarcode = "" Then
    CopyTape = False
    Exit Function
End If

l_lngNewLibraryID = GetData("library", "LibraryID", "barcode", l_strNewLibraryBarcode)

'this tape is already in the library, so dont allow this function to proceed
If l_lngNewLibraryID <> 0 Then
    MsgBox "Tape Already Exists.", vbExclamation, "Error"
    CopyTape = False
    Exit Function
End If

'clear any fields that are not requried in the copy

With frmLibrary
    
    l_lngOldLibraryID = .lblLibraryID.Caption
    
    .txtSourceBarcode.Text = .txtBarcode.Text
    
    .cmbSourceFormat.Text = .cmbRecordFormat.Text
    .cmbSourceStandard.Text = .cmbRecordStandard.Text
    .cmbSourceGeometry.Text = .cmbRecordGeometry.Text
    .cmbSourceRatio.Text = .cmbRecordRatio.Text
    
    .cmbSourceMachine.Text = ""
    .cmbStockCode.Text = ""
    .cmbRecordMachine.Text = ""
    .cmbRecordFormat.Text = ""
    .cmbRecordStandard.Text = ""
    
    .cmbCopyType.Text = "COPY"
    .cmbVersion.Text = "COPY"
    .cmbLocation.Text = "OPERATIONS"
    .cmbShelf.Text = ""
    
    .txtJobDetailID.Text = ""
    If g_optAddFakeJobIDToTapes = 1 Then .txtJobID.Text = "999999"
    .txtProjectNumber.Text = ""
    If Val(.txtInternalReference.Text) = 0 Then .txtInternalReference.Text = GetNextSequence("internalreference")

    l_lngNewLibraryID = CreateLibraryItem(l_strNewLibraryBarcode, "NEW ITEM")

    .txtBarcode.Text = l_strNewLibraryBarcode
    .lblLibraryID.Caption = l_lngNewLibraryID
    .lbltechrevID.Caption = ""
    .cmbTechReviewList.Text = ""

    SaveLibrary
    
    DoEvents

    CopyAllEvents l_lngOldLibraryID, Val(.lblLibraryID.Caption)
        
    'create a transaction
    CreateLibraryTransaction l_lngNewLibraryID, "Copied Tape", 0
        
    
End With

End Function

Public Function AddEventToJobAsMaster()
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/addeventtojobasmaster") Then
        Exit Function
    End If
    
    Dim l_strStartTimeCode As String
    
    Dim l_lngJobID As Long
    
    'get the currently loaded job
    l_lngJobID = GetLoadedJobID()
    
    'if it returns -1 then there is no job loaded
    If l_lngJobID = -1 Then
        MsgBox "There is no job loaded, please load a job befsore trying to add events to it.", vbCritical, "No master job"
        Exit Function
    End If
    
    'otherwise we are good to add one
    Dim l_EventSQL As String
    Dim l_rstEvents As ADODB.Recordset
    
    'prepare the sql statement
    l_EventSQL = "select tapeevents.eventid, tapeevents.eventtitle, tapeevents.clocknumber, library.barcode, library.title, library.productname, library.format, library.videostandard, tapeevents.aspectratio, tapeevents.timecodestart, tapeevents.fd_length from tapeevents inner join library on library.libraryid = tapeevents.libraryid where tapeevents.eventid = " & g_CurrentlySelectedEventID
    
    'execute the new statement
    Set l_rstEvents = ExecuteSQL(l_EventSQL, g_strExecuteError)
    
    'clear the event id variable
    g_CurrentlySelectedEventID = 0
    
    'need to know if its added
    Dim l_blnAdded As Boolean
    
    Dim l_intOrder As Integer
    l_intOrder = GetNextJobOrderValue(l_lngJobID)
    
    Dim l_strDescription  As String
    
    'check for any errors
    If CheckForSQLError() = False Then
        
        'create an insert sql statement to add this event/library combo into the dubbings record
        Dim l_strInsert As String
        
        l_strStartTimeCode = Trim(" " & l_rstEvents("timecodestart"))
        
        If l_strDescription = "" Then l_strDescription = QuoteSanitise(l_rstEvents("eventtitle"))
        If l_strDescription = "" Then l_strDescription = QuoteSanitise(l_rstEvents("productname"))
        If l_strDescription = "" Then l_strDescription = QuoteSanitise(l_rstEvents("title1"))
        
        'build the insert statement
        l_strInsert = "INSERT INTO jobdetail (copytype, starttimecode, description, clocknumber, librarybarcode, format, videostandard, aspectratio, runningtime, fd_orderby, jobid, quantity) VALUES ("
        l_strInsert = l_strInsert & "'M',"
        l_strInsert = l_strInsert & "'" & QuoteSanitise(l_strStartTimeCode) & "',"
        l_strInsert = l_strInsert & "'" & QuoteSanitise(l_strDescription) & "',"
        l_strInsert = l_strInsert & "'" & QuoteSanitise(l_rstEvents("clocknumber")) & "',"
        l_strInsert = l_strInsert & "'" & QuoteSanitise(l_rstEvents("barcode")) & "',"
        l_strInsert = l_strInsert & "'" & QuoteSanitise(l_rstEvents("format")) & "',"
        l_strInsert = l_strInsert & "'" & QuoteSanitise(l_rstEvents("videostandard")) & "',"
        l_strInsert = l_strInsert & "'" & QuoteSanitise(l_rstEvents("aspectratio")) & "',"
        l_strInsert = l_strInsert & "'" & QuoteSanitise(l_rstEvents("fd_length")) & "',"
        l_strInsert = l_strInsert & "'" & l_intOrder & "',"
        l_strInsert = l_strInsert & "'" & l_lngJobID & "', '1');"
        
        'perform the query
        ExecuteSQL l_strInsert, g_strExecuteError
        
        'say we are added
        If CheckForSQLError() = False Then
            l_blnAdded = True
        End If
        
    End If
    
    'close the recordset
    l_rstEvents.Close
    Set l_rstEvents = Nothing
    
    'if it was NOT added then tell the user
    If l_blnAdded = False Then
        MsgBox "Event has NOT been added to job details.", vbExclamation, "In-Complete!"
    End If
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function CalcDuration(ByVal lp_strTimecodeStart As String, ByVal lp_strTimecodeEnd As String, lp_blnPalFlag As Boolean, lp_blnAdjustDropFrame As Boolean, Optional lp_blnFrameAccurate As Boolean) As String
' This routine will calclate the time difference between two time codes strings.
' The hours, minutes and second's are extracted for calculation and the frames are rounded to the nearest second.
' unless the optional parameter lp_blnFrameAccurate is true, in which case frames are also included.

Dim l_lngCalcHours As Long, l_lngCalcMins As Long, l_lngCalcSecs As Long, l_lngCalcFrames As Long
Dim l_lngStartHours As Long, l_lngEndHours As Long, l_lngStartMins As Long, l_lngEndMins As Long, l_lngStartSecs As Long, l_lngEndSecs As Long
Dim l_lngStartFrames As Long, l_lngEndFrames As Long, l_lngStartInSecs As Long, l_lngEndInSecs As Long

l_lngCalcHours = 0
l_lngCalcMins = 0
l_lngCalcSecs = 0
l_lngCalcFrames = 0

l_lngStartHours = Val(Mid(lp_strTimecodeStart, 1, 2))
l_lngStartMins = Val(Mid(lp_strTimecodeStart, 4, 2))
l_lngStartSecs = Val(Mid(lp_strTimecodeStart, 7, 2))
l_lngStartFrames = Val(Mid(lp_strTimecodeStart, 10, 2))

l_lngEndHours = Val(Mid(lp_strTimecodeEnd, 1, 2))
l_lngEndMins = Val(Mid(lp_strTimecodeEnd, 4, 2))
l_lngEndSecs = Val(Mid(lp_strTimecodeEnd, 7, 2))
l_lngEndFrames = Val(Mid(lp_strTimecodeEnd, 10, 2))

If lp_blnFrameAccurate = False Then
    If lp_blnPalFlag = True Then
        If l_lngStartFrames > 12 Then
            l_lngStartSecs = l_lngStartSecs + 1
        End If
        If l_lngEndFrames > 12 Then
            l_lngEndSecs = l_lngEndSecs + 1
        End If
    Else
        If l_lngStartFrames > 15 Then
            l_lngStartSecs = l_lngStartSecs + 1
        End If
        If l_lngEndFrames > 15 Then
            l_lngEndSecs = l_lngEndSecs + 1
        End If
    End If
    l_lngStartFrames = 0
    l_lngEndFrames = 0
End If
    
If l_lngStartSecs = 60 Then l_lngStartSecs = 0: l_lngStartMins = l_lngStartMins + 1
If l_lngStartMins = 60 Then l_lngStartMins = 0: l_lngStartHours = l_lngStartHours + 1
If l_lngStartHours = 24 Then l_lngStartHours = 0

If l_lngEndSecs = 60 Then l_lngEndSecs = 0: l_lngEndMins = l_lngEndMins + 1
If l_lngEndMins = 60 Then l_lngEndMins = 0: l_lngEndHours = l_lngEndHours + 1
If l_lngEndHours = 24 Then l_lngEndHours = 0

' If time is within the 24 hour freshold then calculate as follows:
If l_lngEndHours > l_lngStartHours Then
    l_lngCalcHours = l_lngEndHours - l_lngStartHours
    l_lngCalcMins = l_lngEndMins - l_lngStartMins
    l_lngCalcSecs = l_lngEndSecs - l_lngStartSecs
    l_lngCalcFrames = l_lngEndFrames - l_lngStartFrames
ElseIf (l_lngEndHours = l_lngStartHours) And (l_lngEndMins >= l_lngStartMins) Then
    l_lngCalcHours = l_lngEndHours - l_lngStartHours
    l_lngCalcMins = l_lngEndMins - l_lngStartMins
    l_lngCalcSecs = l_lngEndSecs - l_lngStartSecs
    l_lngCalcFrames = l_lngEndFrames - l_lngStartFrames
ElseIf (l_lngEndHours = l_lngStartHours) And (l_lngEndMins = l_lngStartMins) And (l_lngEndSecs >= l_lngStartSecs) Then
    l_lngCalcHours = l_lngEndHours - l_lngStartHours
    l_lngCalcMins = l_lngEndMins - l_lngStartMins
    l_lngCalcSecs = l_lngEndSecs - l_lngStartSecs
    l_lngCalcFrames = l_lngEndFrames - l_lngStartFrames
ElseIf (l_lngEndHours = l_lngStartHours) And (l_lngEndMins = l_lngStartMins) And (l_lngEndSecs = l_lngStartSecs) And (l_lngEndFrames >= l_lngStartFrames) Then
    l_lngCalcHours = l_lngEndHours - l_lngStartHours
    l_lngCalcMins = l_lngEndMins - l_lngStartMins
    l_lngCalcSecs = l_lngEndSecs - l_lngStartSecs
    l_lngCalcFrames = l_lngEndFrames - l_lngStartFrames
Else
    ' If time goes over the 24 hour freshold then calculate as follows:
    ' HOURS
    If l_lngStartHours = 0 Then
        l_lngCalcHours = (23 - l_lngStartHours) + l_lngEndHours
    ElseIf l_lngStartMins = 0 And l_lngStartSecs = 0 And l_lngStartFrames = 0 Then
        l_lngCalcHours = (24 - l_lngStartHours) + l_lngEndHours
    Else
        l_lngCalcHours = (23 - l_lngStartHours) + l_lngEndHours
    End If
    ' MINUTES
    If l_lngStartMins = 0 Then
        l_lngCalcMins = l_lngEndMins
    Else
        l_lngCalcMins = (60 - l_lngStartMins) + l_lngEndMins
    End If
    ' SECONDS
    If l_lngStartSecs = 0 Then
        l_lngCalcSecs = l_lngEndSecs
    Else
        l_lngCalcSecs = (60 - l_lngStartSecs) + l_lngEndSecs
    End If
    'Frames
    If l_lngStartFrames = 0 Then
        l_lngCalcFrames = l_lngEndFrames
    Else
        If lp_blnPalFlag = True Then
            l_lngCalcFrames = (25 - l_lngStartFrames) + l_lngEndFrames
        Else
            l_lngEndFrames = (30 - l_lngStartFrames) + l_lngEndFrames
        End If
    End If
End If

If lp_blnAdjustDropFrame = True Then

    If l_lngCalcHours >= 1 Then l_lngCalcSecs = l_lngCalcSecs + (3.6 * l_lngCalcHours)
    l_lngCalcSecs = l_lngCalcSecs + (l_lngCalcMins / 60 * 3.6)
    If l_lngCalcSecs > 59 Then
        l_lngCalcMins = l_lngCalcMins + 1
        l_lngCalcSecs = l_lngCalcSecs - 60
    End If
    
    If l_lngCalcMins > 59 Then
        l_lngCalcMins = l_lngCalcMins - 60
        l_lngCalcHours = l_lngCalcHours + 1
    End If

End If

If lp_blnPalFlag = True Then
    If l_lngCalcFrames < 0 Then
        l_lngCalcFrames = 25 + l_lngCalcFrames
        l_lngCalcSecs = l_lngCalcSecs - 1
    End If
    
    If l_lngCalcFrames > 24 Then
        l_lngCalcFrames = l_lngCalcFrames - 25
        l_lngCalcSecs = l_lngCalcSecs + 1
    End If
Else
    If l_lngCalcFrames < 0 Then
        l_lngCalcFrames = 30 + l_lngCalcFrames
        l_lngCalcSecs = l_lngCalcSecs - 1
    End If
    
    If l_lngCalcFrames > 29 Then
        l_lngCalcFrames = l_lngCalcFrames - 30
        l_lngCalcSecs = l_lngCalcSecs + 1
    End If
End If

If l_lngCalcSecs < 0 Then
    l_lngCalcSecs = 60 + l_lngCalcSecs
    l_lngCalcMins = l_lngCalcMins - 1
End If

If l_lngCalcSecs > 59 Then
    l_lngCalcSecs = l_lngCalcSecs - 60
    l_lngCalcMins = l_lngCalcMins + 1
End If

If l_lngCalcMins < 0 Then
    l_lngCalcMins = 60 + l_lngCalcMins
    l_lngCalcHours = l_lngCalcHours - 1
End If

If l_lngCalcMins > 59 Then
    l_lngCalcMins = l_lngCalcMins - 60
    l_lngCalcHours = l_lngCalcHours + 1
End If

If lp_blnFrameAccurate = True Then
    CalcDuration = Format(l_lngCalcHours, "00") & ":" & Format(l_lngCalcMins, "00") & ":" & Format(l_lngCalcSecs, "00") & ":" & Format(l_lngCalcFrames, "00")
Else
    CalcDuration = Format(l_lngCalcHours, "00") & ":" & Format(l_lngCalcMins, "00") & ":" & Format(l_lngCalcSecs, "00")
End If

End Function

Function CalcProgDuration(lp_rstEventsList As ADODB.Recordset, Framerate As Integer, lp_blnFrameAccurate As Boolean)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_rstClone As ADODB.Recordset
    Dim l_blnFoundProg As Boolean
    Dim l_lngPartFind As Long
    Dim l_strDurationTimecode As String
    Dim l_strProgDurationTimecode As String
    
    On Error GoTo PROC_NO_CLONE
    
    Set l_rstClone = lp_rstEventsList.Clone
    
    On Error GoTo PROC_ERR
        
    Select Case Framerate
        Case TC_29, TC_59
            l_strDurationTimecode = "00:00:00;00"
        Case Else
            l_strDurationTimecode = "00:00:00:00"
    End Select
    
    l_blnFoundProg = False
    
    If l_rstClone.RecordCount > 0 Then
        l_rstClone.MoveFirst
        
        Do While Not l_rstClone.EOF
            
            'Find the Program start item
            If Not IsNull(l_rstClone("Eventtitle")) Then
                If (l_rstClone("Eventtitle") = "Programme") Then
                    If Validate_Timecode(l_rstClone("fd_length"), Framerate, True) Then
                        l_strProgDurationTimecode = l_rstClone("fd_Length")
                    ElseIf Validate_Timecode(l_rstClone("fd_length") & ":00", Framerate, True) Then
                        l_strProgDurationTimecode = l_rstClone("fd_Length") & ":00"
                    ElseIf Validate_Timecode(l_rstClone("fd_length") & ";02", Framerate, True) Then
                        l_strProgDurationTimecode = l_rstClone("fd_Length") & ";02"
                    End If
                    l_blnFoundProg = True
                    Exit Do
                End If
                
                'Find the Parts and add up the times
                l_lngPartFind = InStr(1, l_rstClone("Eventtitle"), "Part", 0)
                If l_lngPartFind > 0 Then
                    
                    If Validate_Timecode(l_rstClone("fd_length"), Framerate, True) Then
                        l_strDurationTimecode = Timecode_Add(l_strDurationTimecode, l_rstClone("fd_length"), Framerate)
                    ElseIf Validate_Timecode(l_rstClone("fd_length") & ":00", Framerate, True) Then
                        l_strDurationTimecode = Timecode_Add(l_strDurationTimecode, l_rstClone("fd_length") & ":00", Framerate)
                    ElseIf Validate_Timecode(l_rstClone("fd_length") & ";02", Framerate, True) Then
                        l_strDurationTimecode = Timecode_Add(l_strDurationTimecode, l_rstClone("fd_length") & ";02", Framerate)
                    End If
                    
                Else
                    'Check if tape is comprised of several "Ep." items and add them up if it is.
                    l_lngPartFind = InStr(1, l_rstClone("Eventtitle"), "Ep.", 0)
                    If l_lngPartFind > 0 Then
                        If Validate_Timecode(l_rstClone("fd_Length"), Framerate, True) Then
                            l_strDurationTimecode = Timecode_Add(l_strDurationTimecode, l_rstClone("fd_length"), Framerate)
                        ElseIf Validate_Timecode(l_rstClone("fd_length") & ":00", Framerate, True) Then
                            l_strDurationTimecode = Timecode_Add(l_strDurationTimecode, l_rstClone("fd_length") & ":00", Framerate)
                        ElseIf Validate_Timecode(l_rstClone("fd_length") & ";02", Framerate, True) Then
                            l_strDurationTimecode = Timecode_Add(l_strDurationTimecode, l_rstClone("fd_length") & ";02", Framerate)
                        End If
                        
                    End If
                End If
            End If
            l_rstClone.MoveNext
        Loop
    End If
    
    l_rstClone.Close
    Set l_rstClone = Nothing
    
    If l_blnFoundProg = True Then
        If lp_blnFrameAccurate = False Then
            CalcProgDuration = Duration_From_Timecode(l_strProgDurationTimecode, Framerate)
        Else
            CalcProgDuration = l_strProgDurationTimecode
        End If
    Else
        If lp_blnFrameAccurate = False Then
            CalcProgDuration = Duration_From_Timecode(l_strDurationTimecode, Framerate)
        Else
            CalcProgDuration = l_strDurationTimecode
        End If
    End If
        
Exit Function

PROC_NO_CLONE:

    CalcProgDuration = "00:00"



    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function CalcTotalDuration(lp_rstEventsList As ADODB.Recordset, Framerate As Integer, lp_blnFrameAccurate As Boolean) As String
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_rstClone As ADODB.Recordset
    
    Dim l_strDurationTimecode As String
        
    On Error GoTo PROC_NO_CLONE
    
    Set l_rstClone = lp_rstEventsList.Clone
    
    On Error GoTo PROC_ERR
    
    If l_rstClone.RecordCount > 0 Then
        l_rstClone.MoveFirst
        
        Select Case Framerate
            Case TC_29, TC_59
                l_strDurationTimecode = "00:00:00;00"
            Case Else
                l_strDurationTimecode = "00:00:00:00"
        End Select
        
        Do While Not l_rstClone.EOF
            'Add up the times
            If Validate_Timecode(l_rstClone("fd_length"), Framerate, True) Then
                l_strDurationTimecode = Timecode_Add(l_strDurationTimecode, l_rstClone("fd_length"), Framerate)
            ElseIf Validate_Timecode(l_rstClone("fd_length") & ":00", Framerate, True) Then
                l_strDurationTimecode = Timecode_Add(l_strDurationTimecode, l_rstClone("fd_length") & ":00", Framerate)
            ElseIf Validate_Timecode(l_rstClone("fd_length") & ";02", Framerate, True) Then
                l_strDurationTimecode = Timecode_Add(l_strDurationTimecode, l_rstClone("fd_length") & ";02", Framerate)
            End If
            
            l_rstClone.MoveNext
            
        Loop
    Else
        Select Case Framerate
            Case TC_29, TC_59
                l_strDurationTimecode = "00:00:00;00"
            Case Else
                l_strDurationTimecode = "00:00:00:00"
        End Select
    End If
    
    l_rstClone.Close
    Set l_rstClone = Nothing
    
    If lp_blnFrameAccurate = True Then
        CalcTotalDuration = l_strDurationTimecode
    Else
        CalcTotalDuration = Duration_From_Timecode(l_strDurationTimecode, Framerate)
    End If
    Exit Function
    
PROC_NO_CLONE:
    
    CalcTotalDuration = "00:00"
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function CheckLibraryChanges(lp_lngLibraryID) As Boolean

CheckLibraryChanges = False
        
With frmLibrary

    If lp_lngLibraryID = 0 Then
    
        If .txtForeignRef.Text <> "" Then CheckLibraryChanges = True
        If .txtJobID.Text <> "" Then CheckLibraryChanges = True
        'If .cmbLocation.Text <> "" Then CheckLibraryChanges = True
        'If .cmbShelf.Text <> "" Then CheckLibraryChanges = True
        If .cmbCopyType.Text <> "" Then CheckLibraryChanges = True
        If .cmbCompany.Text <> "" Then CheckLibraryChanges = True
        If .txtTitle.Text <> "" Then CheckLibraryChanges = True
        If .txtSubTitle.Text <> "" Then CheckLibraryChanges = True
        If .cmbVersion.Text <> "" Then CheckLibraryChanges = True
        If .txtSourceBarcode.Text <> "" Then CheckLibraryChanges = True
        If .cmbSourceMachine.Text <> "" Then CheckLibraryChanges = True
        If .cmbSourceStandard.Text <> "" Then CheckLibraryChanges = True
        If .cmbSourceFormat.Text <> "" Then CheckLibraryChanges = True
        If .cmbSourceRatio.Text <> "" Then CheckLibraryChanges = True
        If .cmbSourceGeometry.Text <> "" Then CheckLibraryChanges = True
        If .cmbRecordMachine.Text <> "" Then CheckLibraryChanges = True
        If .cmbRecordFormat.Text <> "" Then CheckLibraryChanges = True
        If .cmbRecordStandard.Text <> "" Then CheckLibraryChanges = True
        If .cmbRecordRatio.Text <> "" Then CheckLibraryChanges = True
        If .cmbRecordGeometry.Text <> "" Then CheckLibraryChanges = True
        If .cmbStockCode.Text <> "" Then CheckLibraryChanges = True
        If .cmbSeries.Text <> "" Then CheckLibraryChanges = True
        If .cmbSet.Text <> "" Then CheckLibraryChanges = True
        If .cmbEpisode.Text <> "" Then CheckLibraryChanges = True
        If .cmbChannel1.Text <> "" Then CheckLibraryChanges = True
        If .cmbChannel2.Text <> "" Then CheckLibraryChanges = True
        If .cmbChannel3.Text <> "" Then CheckLibraryChanges = True
        If .cmbChannel4.Text <> "" Then CheckLibraryChanges = True
        If .cmbChannel5.Text <> "" Then CheckLibraryChanges = True
        If .cmbChannel6.Text <> "" Then CheckLibraryChanges = True
        If .cmbChannel7.Text <> "" Then CheckLibraryChanges = True
        If .cmbChannel8.Text <> "" Then CheckLibraryChanges = True
        If .cmbChannel9.Text <> "" Then CheckLibraryChanges = True
        If .cmbChannel10.Text <> "" Then CheckLibraryChanges = True
        If .cmbChannel11.Text <> "" Then CheckLibraryChanges = True
        If .cmbChannel12.Text <> "" Then CheckLibraryChanges = True
'        If .cmbNoiseReduction1.Text <> "" Then CheckLibraryChanges = True
'        If .cmbNoiseReduction2.Text <> "" Then CheckLibraryChanges = True
'        If .cmbNoiseReduction3.Text <> "" Then CheckLibraryChanges = True
'        If .cmbNoiseReduction4.Text <> "" Then CheckLibraryChanges = True
'        If .cmbNoiseReduction5.Text <> "" Then CheckLibraryChanges = True
'        If .cmbNoiseReduction6.Text <> "" Then CheckLibraryChanges = True
'        If .cmbNoiseReduction7.Text <> "" Then CheckLibraryChanges = True
'        If .cmbNoiseReduction8.Text <> "" Then CheckLibraryChanges = True
'        If .cmbNoiseReduction9.Text <> "" Then CheckLibraryChanges = True
'        If .cmbNoiseReduction10.Text <> "" Then CheckLibraryChanges = True
'        If .cmbNoiseReduction11.Text <> "" Then CheckLibraryChanges = True
'        If .cmbNoiseReduction12.Text <> "" Then CheckLibraryChanges = True
        If .cmbChannelCue.Text <> "" Then CheckLibraryChanges = True
        If .cmbTimeCode.Text <> "" Then CheckLibraryChanges = True
        If .txtProgDuration.Text <> "" Then CheckLibraryChanges = True
        If .txtTotalDuration.Text <> "" Then CheckLibraryChanges = True
        If .txtNotes.Text <> "" Then CheckLibraryChanges = True
        If .chkTapeIncomplete.Value <> 0 Then CheckLibraryChanges = True
        If .txtJobDetailID.Text <> "" Then CheckLibraryChanges = True
        If .cmbAudioStandard.Text <> "" Then CheckLibraryChanges = True
        If .txtInternalReference.Text <> "" Then CheckLibraryChanges = True
    
    Else
    
        If Trim(" " & GetData("library", "foreignref", "libraryID", lp_lngLibraryID)) <> .txtForeignRef.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "jobID", "libraryID", lp_lngLibraryID)) <> .txtJobID.Text Then CheckLibraryChanges = True
        'If Trim(" " & GetData("library", "location", "libraryID", lp_lngLibraryID)) <> .cmbLocation.Text Then CheckLibraryChanges = True
        'If Trim(" " & GetData("library", "shelf", "libraryID", lp_lngLibraryID)) <> .cmbShelf.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "copytype", "libraryID", lp_lngLibraryID)) <> .cmbCopyType.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "companyname", "libraryID", lp_lngLibraryID)) <> .cmbCompany.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "title", "libraryID", lp_lngLibraryID)) <> .txtTitle.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "subtitle", "libraryID", lp_lngLibraryID)) <> .txtSubTitle.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "version", "libraryID", lp_lngLibraryID)) <> .cmbVersion.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "sourcereference", "libraryID", lp_lngLibraryID)) <> .txtSourceBarcode.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "sourcemachine", "libraryID", lp_lngLibraryID)) <> .cmbSourceMachine.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "sourcevideostandard", "libraryID", lp_lngLibraryID)) <> .cmbSourceStandard.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "sourceformat", "libraryID", lp_lngLibraryID)) <> .cmbSourceFormat.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "sourceaspectratio", "libraryID", lp_lngLibraryID)) <> .cmbSourceRatio.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "sourcegeometriclinearity", "libraryID", lp_lngLibraryID)) <> .cmbSourceGeometry.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "recordmachine", "libraryID", lp_lngLibraryID)) <> .cmbRecordMachine.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "format", "libraryID", lp_lngLibraryID)) <> .cmbRecordFormat.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "videostandard", "libraryID", lp_lngLibraryID)) <> .cmbRecordStandard.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "aspectratio", "libraryID", lp_lngLibraryID)) <> .cmbRecordRatio.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "geometriclinearity", "libraryID", lp_lngLibraryID)) <> .cmbRecordGeometry.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "stocktype", "libraryID", lp_lngLibraryID)) <> .cmbStockCode.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "ch1", "libraryID", lp_lngLibraryID)) <> .cmbChannel1.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "ch2", "libraryID", lp_lngLibraryID)) <> .cmbChannel2.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "ch3", "libraryID", lp_lngLibraryID)) <> .cmbChannel3.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "ch4", "libraryID", lp_lngLibraryID)) <> .cmbChannel4.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "ch5", "libraryID", lp_lngLibraryID)) <> .cmbChannel5.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "ch6", "libraryID", lp_lngLibraryID)) <> .cmbChannel6.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "ch7", "libraryID", lp_lngLibraryID)) <> .cmbChannel7.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "ch8", "libraryID", lp_lngLibraryID)) <> .cmbChannel8.Text Then CheckLibraryChanges = True
        
        If Trim(" " & GetData("library", "ch9", "libraryID", lp_lngLibraryID)) <> .cmbChannel9.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "ch10", "libraryID", lp_lngLibraryID)) <> .cmbChannel10.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "ch11", "libraryID", lp_lngLibraryID)) <> .cmbChannel11.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "ch12", "libraryID", lp_lngLibraryID)) <> .cmbChannel12.Text Then CheckLibraryChanges = True
        
        
'        If Trim(" " & GetData("library", "nr1", "libraryID", lp_lngLibraryID)) <> .cmbNoiseReduction1.Text Then CheckLibraryChanges = True
'        If Trim(" " & GetData("library", "nr2", "libraryID", lp_lngLibraryID)) <> .cmbNoiseReduction2.Text Then CheckLibraryChanges = True
'        If Trim(" " & GetData("library", "nr3", "libraryID", lp_lngLibraryID)) <> .cmbNoiseReduction3.Text Then CheckLibraryChanges = True
'        If Trim(" " & GetData("library", "nr4", "libraryID", lp_lngLibraryID)) <> .cmbNoiseReduction4.Text Then CheckLibraryChanges = True
'        If Trim(" " & GetData("library", "nr5", "libraryID", lp_lngLibraryID)) <> .cmbNoiseReduction5.Text Then CheckLibraryChanges = True
'        If Trim(" " & GetData("library", "nr6", "libraryID", lp_lngLibraryID)) <> .cmbNoiseReduction6.Text Then CheckLibraryChanges = True
'        If Trim(" " & GetData("library", "nr7", "libraryID", lp_lngLibraryID)) <> .cmbNoiseReduction7.Text Then CheckLibraryChanges = True
'        If Trim(" " & GetData("library", "nr8", "libraryID", lp_lngLibraryID)) <> .cmbNoiseReduction8.Text Then CheckLibraryChanges = True
'
'        If Trim(" " & GetData("library", "nr9", "libraryID", lp_lngLibraryID)) <> .cmbNoiseReduction9.Text Then CheckLibraryChanges = True
'        If Trim(" " & GetData("library", "nr10", "libraryID", lp_lngLibraryID)) <> .cmbNoiseReduction10.Text Then CheckLibraryChanges = True
'        If Trim(" " & GetData("library", "nr11", "libraryID", lp_lngLibraryID)) <> .cmbNoiseReduction11.Text Then CheckLibraryChanges = True
'        If Trim(" " & GetData("library", "nr12", "libraryID", lp_lngLibraryID)) <> .cmbNoiseReduction12.Text Then CheckLibraryChanges = True
        
        If Trim(" " & GetData("library", "cuetrack", "libraryID", lp_lngLibraryID)) <> .cmbChannelCue.Text Then CheckLibraryChanges = True
        
        
        If Trim(" " & GetData("library", "timecode", "libraryID", lp_lngLibraryID)) <> .cmbTimeCode.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "progduration", "libraryID", lp_lngLibraryID)) <> .txtProgDuration.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "totalduration", "libraryID", lp_lngLibraryID)) <> .txtTotalDuration.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "notes1", "libraryID", lp_lngLibraryID)) <> .txtNotes.Text Then CheckLibraryChanges = True
        If GetFlag(GetData("library", "flagincomplete", "libraryID", lp_lngLibraryID)) <> .chkTapeIncomplete.Value Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "jobdetailID", "libraryID", lp_lngLibraryID)) <> .txtJobDetailID.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "series", "libraryID", lp_lngLibraryID)) <> .cmbSeries.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "seriesset", "libraryID", lp_lngLibraryID)) <> .cmbSet.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "episode", "libraryID", lp_lngLibraryID)) <> .cmbEpisode.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "audiostandard", "libraryID", lp_lngLibraryID)) <> .cmbAudioStandard.Text Then CheckLibraryChanges = True
        If Trim(" " & GetData("library", "internalreference", "libraryID", lp_lngLibraryID)) <> .txtInternalReference.Text Then CheckLibraryChanges = True
    
    End If

End With

If CheckLibraryChanges = False Then
    If Val(frmLibrary.lbltechrevID.Caption) <> 0 Then
        'tech review exists, so those fields need checking too.
        If Val(frmLibrary.lbltechrevID.Caption) = Val(frmLibrary.lblLastTechrevID.Caption) Then
            CheckLibraryChanges = CheckTechReviewChanges(Val(frmLibrary.lbltechrevID.Caption))
        End If
    End If
End If

End Function
Function CheckTechReviewChanges(lp_lngTechrevID As Long) As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    CheckTechReviewChanges = False
    
    With frmLibrary
        
        Dim l_datReviewdate As Variant
        If Not IsNull(.datReviewDate.Value) Then
            l_datReviewdate = Format(.datReviewDate.Value, vbShortDateFormat)
        Else
            l_datReviewdate = Null
        End If
        
        If Trim(" " & GetData("tapetechrev", "jobID", "tapetechrevID", lp_lngTechrevID)) <> .txtReviewJobID.Text Then
            CheckTechReviewChanges = True
        End If
        If Format(GetData("tapetechrev", "reviewdate", "tapetechrevID", lp_lngTechrevID), vbShortDateFormat) <> l_datReviewdate Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "reviewuser", "tapetechrevID", lp_lngTechrevID)) <> .cmbReviewUser.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "companyname", "tapetechrevID", lp_lngTechrevID)) <> .cmbReviewCompany.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "contactname", "tapetechrevID", lp_lngTechrevID)) <> .cmbReviewContact Then
            CheckTechReviewChanges = True
        End If
        If GetFlag(GetData("tapetechrev", "flagtitles", "tapetechrevID", lp_lngTechrevID)) <> .chkTitles.Value Then
            CheckTechReviewChanges = True
        End If
        If GetFlag(GetData("tapetechrev", "flagcaptions", "tapetechrevID", lp_lngTechrevID)) <> .chkCaptions.Value Then
            CheckTechReviewChanges = True
        End If
        If GetFlag(GetData("tapetechrev", "flagsubtitles", "tapetechrevID", lp_lngTechrevID)) <> .chkSubtitles.Value Then
            CheckTechReviewChanges = True
        End If
        If GetFlag(GetData("tapetechrev", "flaglogos", "tapetechrevID", lp_lngTechrevID)) <> .chkLogos.Value Then
            CheckTechReviewChanges = True
        End If
        If GetFlag(GetData("tapetechrev", "flagtextless", "tapetechrevID", lp_lngTechrevID)) <> .chkTextless.Value Then
            CheckTechReviewChanges = True
        End If
        If GetFlag(GetData("tapetechrev", "flagtrailers", "tapetechrevID", lp_lngTechrevID)) <> .chkTrailers.Value Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "languagetitles", "tapetechrevID", lp_lngTechrevID)) <> .cmbTitlesLanguage.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "languagecaptions", "tapetechrevID", lp_lngTechrevID)) <> .cmbCaptionsLanguage.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "languagesubtitles", "tapetechrevID", lp_lngTechrevID)) <> .cmbSubtitlesLanguage.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "detailslogos", "tapetechrevID", lp_lngTechrevID)) <> .txtDetailsLogos.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "detailstextless", "tapetechrevID", lp_lngTechrevID)) <> .txtDetailsTextless.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "detailstrailers", "tapetechrevID", lp_lngTechrevID)) <> .txtDetailsTrailers.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "reviewmachine", "tapetechrevID", lp_lngTechrevID)) <> .cmbReviewMachine.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "notesvideo", "tapetechrevID", lp_lngTechrevID)) <> .txtTechNotesVideo.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "notesaudio", "tapetechrevID", lp_lngTechrevID)) <> .txtTechNotesAudio.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "notesgeneral", "tapetechrevID", lp_lngTechrevID)) <> .txtTechNotesGeneral.Text Then
            CheckTechReviewChanges = True
        End If
        If .optReviewConclusion(0).Value = True Then
            If GetData("tapetechrev", "reviewconclusion", "tapetechrevID", lp_lngTechrevID) <> 0 Then CheckTechReviewChanges = True
        End If
        If .optReviewConclusion(1).Value = True Then
            If GetData("tapetechrev", "reviewconclusion", "tapetechrevID", lp_lngTechrevID) <> 1 Then CheckTechReviewChanges = True
        End If
        If .optReviewConclusion(2).Value = True Then
            If GetData("tapetechrev", "reviewconclusion", "tapetechrevID", lp_lngTechrevID) <> 2 Then CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "videotest", "tapetechrevID", lp_lngTechrevID)) <> .txtVideoTest.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "videoprog", "tapetechrevID", lp_lngTechrevID)) <> .txtVideoProg.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "chromatest", "tapetechrevID", lp_lngTechrevID)) <> .txtChromaTest.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "chromaprog", "tapetechrevID", lp_lngTechrevID)) <> .txtChromaProg.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "blacktest", "tapetechrevID", lp_lngTechrevID)) <> .txtBlackTest.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "blackprog", "tapetechrevID", lp_lngTechrevID)) <> .txtBlackProg.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "phasetest", "tapetechrevID", lp_lngTechrevID)) <> .txtPhaseTest.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "phaseprog", "tapetechrevID", lp_lngTechrevID)) <> .txtPhaseProg.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio1test", "tapetechrevID", lp_lngTechrevID)) <> .txtA1Test.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio1prog", "tapetechrevID", lp_lngTechrevID)) <> .txtA1Prog.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio2test", "tapetechrevID", lp_lngTechrevID)) <> .txtA2Test.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio2prog", "tapetechrevID", lp_lngTechrevID)) <> .txtA2Prog.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio3test", "tapetechrevID", lp_lngTechrevID)) <> .txtA3Test.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio3prog", "tapetechrevID", lp_lngTechrevID)) <> .txtA3Prog.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio4test", "tapetechrevID", lp_lngTechrevID)) <> .txtA4Test.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio4prog", "tapetechrevID", lp_lngTechrevID)) <> .txtA4Prog.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio5test", "tapetechrevID", lp_lngTechrevID)) <> .txtA5Test.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio5prog", "tapetechrevID", lp_lngTechrevID)) <> .txtA5Prog.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio6test", "tapetechrevID", lp_lngTechrevID)) <> .txtA6Test.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio6prog", "tapetechrevID", lp_lngTechrevID)) <> .txtA6Prog.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio7test", "tapetechrevID", lp_lngTechrevID)) <> .txtA7Test.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio7prog", "tapetechrevID", lp_lngTechrevID)) <> .txtA7Prog.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio8test", "tapetechrevID", lp_lngTechrevID)) <> .txtA8Test.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio8prog", "tapetechrevID", lp_lngTechrevID)) <> .txtA8Prog.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio9test", "tapetechrevID", lp_lngTechrevID)) <> .txtA9Test.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio9prog", "tapetechrevID", lp_lngTechrevID)) <> .txtA9Prog.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio10test", "tapetechrevID", lp_lngTechrevID)) <> .txtA10Test.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio10prog", "tapetechrevID", lp_lngTechrevID)) <> .txtA10Prog.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio11test", "tapetechrevID", lp_lngTechrevID)) <> .txtA11Test.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio11prog", "tapetechrevID", lp_lngTechrevID)) <> .txtA11Prog.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio12test", "tapetechrevID", lp_lngTechrevID)) <> .txtA12Test.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "audio12prog", "tapetechrevID", lp_lngTechrevID)) <> .txtA12Prog.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "EBU128LoudnessStereoMain", "tapetechrevID", lp_lngTechrevID)) <> .cmbEBU128LoudnessStereoMain.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "EBU128LoudnessStereoME", "tapetechrevID", lp_lngTechrevID)) <> .cmbEBU128LoudnessStereoME.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "EBU128Loudness51Main", "tapetechrevID", lp_lngTechrevID)) <> .cmbEBU128Loudness51Main.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "EBU128Loudness51ME", "tapetechrevID", lp_lngTechrevID)) <> .cmbEBU128Loudness51ME.Text Then
            CheckTechReviewChanges = True
        End If
        
        If Trim(" " & GetData("tapetechrev", "cuetest", "tapetechrevID", lp_lngTechrevID)) <> .txtCueTest.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "cueprog", "tapetechrevID", lp_lngTechrevID)) <> .txtCueProg.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "vitclines", "tapetechrevID", lp_lngTechrevID)) <> .txtvitclines.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "userbits", "tapetechrevID", lp_lngTechrevID)) <> .txtuserbits.Text Then
            CheckTechReviewChanges = True
        End If
'        If Trim(" " & GetData("tapetechrev", "horizblank", "tapetechrevID", lp_lngTechrevID)) <> .txthorizblank.Text Then
'            CheckTechReviewChanges = True
'        End If
'        If Trim(" " & GetData("tapetechrev", "vertblank", "tapetechrevID", lp_lngTechrevID)) <> .txtvertblank.Text Then
'            CheckTechReviewChanges = True
'        End If
'        If Trim(" " & GetData("tapetechrev", "backporch", "tapetechrevID", lp_lngTechrevID)) <> .txtbackporch.Text Then
'            CheckTechReviewChanges = True
'        End If
'        If Trim(" " & GetData("tapetechrev", "frontporch", "tapetechrevID", lp_lngTechrevID)) <> .txtfrontporch.Text Then
'            CheckTechReviewChanges = True
'        End If
        If GetFlag(GetData("tapetechrev", "timecodecontinuous", "tapetechrevID", lp_lngTechrevID)) <> .chktimecodecontinuous.Value Then
            CheckTechReviewChanges = True
        End If
        If GetFlag(GetData("tapetechrev", "ltc", "tapetechrevID", lp_lngTechrevID)) <> .chkltc.Value Then
            CheckTechReviewChanges = True
        End If
        If GetFlag(GetData("tapetechrev", "vitc", "tapetechrevID", lp_lngTechrevID)) <> .chkvitc.Value Then
            CheckTechReviewChanges = True
        End If
        If GetFlag(GetData("tapetechrev", "ltcvitcmatch", "tapetechrevID", lp_lngTechrevID)) <> .chkltcvitcmatch.Value Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "firstactivepixel", "tapetechrevID", lp_lngTechrevID)) <> .txtfirstactivepixel.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "lastactivepixel", "tapetechrevID", lp_lngTechrevID)) <> .txtlastactivepixel.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "firstactivelinefield1", "tapetechrevID", lp_lngTechrevID)) <> .txtfirstactivelinefield1.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "lastactivelinefield1", "tapetechrevID", lp_lngTechrevID)) <> .txtlastactivelinefield1.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "firstactivelinefield2", "tapetechrevID", lp_lngTechrevID)) <> .txtfirstactivelinefield2.Text Then
            CheckTechReviewChanges = True
        End If
        If Trim(" " & GetData("tapetechrev", "lastactivelinefield2", "tapetechrevID", lp_lngTechrevID)) <> .txtlastactivelinefield2.Text Then
            CheckTechReviewChanges = True
        End If
        If GetFlag(GetData("tapetechrev", "verticalcentreingcorrect", "tapetechrevID", lp_lngTechrevID)) <> .chkverticalcenteringcorrect.Value Then
            CheckTechReviewChanges = True
        End If
        If GetFlag(GetData("tapetechrev", "horizontalcentreingcorrect", "tapetechrevID", lp_lngTechrevID)) <> .chkhorizontalcenteringcorrect.Value Then
            CheckTechReviewChanges = True
        End If
        If GetFlag(GetData("tapetechrev", "captionsafe169", "tapetechrevID", lp_lngTechrevID)) <> .chkCaptionsafe169.Value Then
            CheckTechReviewChanges = True
        End If
        If GetFlag(GetData("tapetechrev", "captionsafe149", "tapetechrevID", lp_lngTechrevID)) <> .chkCaptionsafe149.Value Then
            CheckTechReviewChanges = True
        End If
        If GetFlag(GetData("tapetechrev", "captionsafe43protect", "tapetechrevID", lp_lngTechrevID)) <> .chkCaptionsafe43protect.Value Then
            CheckTechReviewChanges = True
        End If
        If GetFlag(GetData("tapetechrev", "captionsafe43ebu", "tapetechrevID", lp_lngTechrevID)) <> .chkCaptionsafe43ebu.Value Then
            CheckTechReviewChanges = True
        End If
        
    End With
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function InsertTapeTechReview(lp_lngLibraryID As Long, lp_strFormat As String) As Long

'Dim l_strSQL As String,, l_rstTemp As ADODB.Recordset

Dim l_lngNoOfTracks As Long, l_strSQL As String

'Get the number of audio tracks on this format
'l_strFormat = GetData("library", "format", "libraryID", lp_lnglibraryID)
'l_strSQL = "SELECT * from xref WHERE category = 'format' AND description = '" & l_strFormat & "'"
'Set l_rstTemp = ExecuteSQL(l_strSQL, g_strExecuteError)
'CheckForSQLError
'
'If Not l_rstTemp.EOF Then
'    l_rstTemp.MoveFirst
'    l_lngNoOfTracks = l_rstTemp("Videostandard")
'End If
'
'l_rstTemp.Close
'Set l_rstTemp = Nothing

l_lngNoOfTracks = Val(GetDataSQL("SELECT TOP 1 videostandard FROM xref WHERE category = 'format' AND description = '" & GetData("library", "format", "libraryID", lp_lngLibraryID) & "';"))


l_strSQL = "INSERT INTO tapetechrev ("
l_strSQL = l_strSQL & "libraryID, "
l_strSQL = l_strSQL & "reviewdate, "
l_strSQL = l_strSQL & "flagtitles, "
l_strSQL = l_strSQL & "flagcaptions, "
l_strSQL = l_strSQL & "flagsubtitles, "
l_strSQL = l_strSQL & "flaglogos, "
l_strSQL = l_strSQL & "flagtextless, "
l_strSQL = l_strSQL & "flagtrailers, "
l_strSQL = l_strSQL & "ltc, "
l_strSQL = l_strSQL & "vitc, "
l_strSQL = l_strSQL & "ltcvitcmatch, "
l_strSQL = l_strSQL & "timecodecontinuous, "
l_strSQL = l_strSQL & "verticalcentreingcorrect, "
l_strSQL = l_strSQL & "horizontalcentreingcorrect, "
l_strSQL = l_strSQL & "captionsafe169, "
l_strSQL = l_strSQL & "captionsafe149, "
l_strSQL = l_strSQL & "captionsafe43protect, "
l_strSQL = l_strSQL & "captionsafe43ebu, "
l_strSQL = l_strSQL & "videotest, "
l_strSQL = l_strSQL & "videoprog, "
l_strSQL = l_strSQL & "chromatest, "
l_strSQL = l_strSQL & "chromaprog, "
l_strSQL = l_strSQL & "blacktest, "
l_strSQL = l_strSQL & "blackprog, "
l_strSQL = l_strSQL & "phasetest, "
l_strSQL = l_strSQL & "phaseprog, "
l_strSQL = l_strSQL & "audio1test, "
l_strSQL = l_strSQL & "audio2test, "
l_strSQL = l_strSQL & "audio3test, "
l_strSQL = l_strSQL & "audio4test, "
l_strSQL = l_strSQL & "audio5test, "
l_strSQL = l_strSQL & "audio6test, "
l_strSQL = l_strSQL & "audio7test, "
l_strSQL = l_strSQL & "audio8test, "
l_strSQL = l_strSQL & "audio9test, "
l_strSQL = l_strSQL & "audio10test, "
l_strSQL = l_strSQL & "audio11test, "
l_strSQL = l_strSQL & "audio12test, "
l_strSQL = l_strSQL & "cuetest, "
l_strSQL = l_strSQL & "audio1prog, "
l_strSQL = l_strSQL & "audio2prog, "
l_strSQL = l_strSQL & "audio3prog, "
l_strSQL = l_strSQL & "audio4prog, "
l_strSQL = l_strSQL & "audio5prog, "
l_strSQL = l_strSQL & "audio6prog, "
l_strSQL = l_strSQL & "audio7prog, "
l_strSQL = l_strSQL & "audio8prog, "
l_strSQL = l_strSQL & "audio9prog, "
l_strSQL = l_strSQL & "audio10prog, "
l_strSQL = l_strSQL & "audio11prog, "
l_strSQL = l_strSQL & "audio12prog, "
l_strSQL = l_strSQL & "cueprog, "
l_strSQL = l_strSQL & "EBU128LoudnessStereoMain, "
l_strSQL = l_strSQL & "EBU128LoudnessStereoME, "
l_strSQL = l_strSQL & "EBU128Loudness51Main, "
l_strSQL = l_strSQL & "EBU128Loudness51ME, "
l_strSQL = l_strSQL & "firstactivelinefield1, "
l_strSQL = l_strSQL & "lastactivelinefield1, "
l_strSQL = l_strSQL & "firstactivelinefield2, "
l_strSQL = l_strSQL & "lastactivelinefield2, "
l_strSQL = l_strSQL & "firstactivepixel, "
l_strSQL = l_strSQL & "lastactivepixel, "
l_strSQL = l_strSQL & "vitclines, "
l_strSQL = l_strSQL & "userbits, "
l_strSQL = l_strSQL & "reviewconclusion, "
l_strSQL = l_strSQL & "reviewuser) VALUES ("

'now the values

l_strSQL = l_strSQL & "'" & lp_lngLibraryID & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'1', "
l_strSQL = l_strSQL & "'1', "
l_strSQL = l_strSQL & "'1', "
l_strSQL = l_strSQL & "'1', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'100%', "
l_strSQL = l_strSQL & "'OK', "
l_strSQL = l_strSQL & "'75%', "
l_strSQL = l_strSQL & "'OK', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'OK', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'OK', "
l_strSQL = l_strSQL & "'-18dB', "
l_strSQL = l_strSQL & "'-18dB', "
If l_lngNoOfTracks > 2 Then
    l_strSQL = l_strSQL & "'-18dB', "
    l_strSQL = l_strSQL & "'-18dB', "
Else
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
End If
If l_lngNoOfTracks > 4 Then
    l_strSQL = l_strSQL & "'-18dB', "
    l_strSQL = l_strSQL & "'-18dB', "
    l_strSQL = l_strSQL & "'-18dB', "
    l_strSQL = l_strSQL & "'-18dB', "
Else
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
End If
If l_lngNoOfTracks > 8 Then
    l_strSQL = l_strSQL & "'-18dB', "
    l_strSQL = l_strSQL & "'-18dB', "
    l_strSQL = l_strSQL & "'-18dB', "
    l_strSQL = l_strSQL & "'-18dB', "
Else
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
End If
l_strSQL = l_strSQL & "'0dBm', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'Not Checked', "
l_strSQL = l_strSQL & "'Not Checked', "
l_strSQL = l_strSQL & "'Not Present', "
l_strSQL = l_strSQL & "'Not Present', "
l_strSQL = l_strSQL & "'23', "
l_strSQL = l_strSQL & "'310', "
l_strSQL = l_strSQL & "'336', "
l_strSQL = l_strSQL & "'623', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'1439', "
If lp_strFormat = "525NTSC" Then
    l_strSQL = l_strSQL & "'16 & 18', "
Else
    l_strSQL = l_strSQL & "'19 & 21', "
End If
l_strSQL = l_strSQL & "'00:00:00:00', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'" & g_strFullUserName & "')"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError
        
InsertTapeTechReview = g_lngLastID

End Function

Sub LibraryAssign(lp_lngLibraryID As Long)
If lp_lngLibraryID <> 0 Then
    SetData "library", "location", "libraryID", lp_lngLibraryID, "Library"
    SetData "library", "inlibrary", "libraryID", lp_lngLibraryID, "YES"
    SetData "library", "muser", "libraryID", lp_lngLibraryID, g_strUserInitials
    SetData "library", "mdate", "libraryID", lp_lngLibraryID, FormatSQLDate(Now)
    CreateLibraryTransaction lp_lngLibraryID, "Library", 0
End If
End Sub

Sub LibraryRemove(lp_lngLibraryID As Long)
If lp_lngLibraryID <> 0 Then

    frmMovement.Tag = "AssignFromLibrary"
    frmMovement.txtBarcode.Text = GetData("library", "barcode", "libraryID", lp_lngLibraryID)
    frmMovement.Show
    
End If
End Sub

Sub LoadLibraryBarcode(lp_strBarcode As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_lngLibraryID As Long, l_lngJobID As Long, l_rst As ADODB.Recordset, l_strSQL As String, l_lngPreviewOrderID As Long, l_lngJobDetailID As Long, l_rstFuzzySearch As ADODB.Recordset
    Dim l_intMessage As Long, l_lngExistingMediaKeyID As Long, l_strExistingMediaKey As String, l_strMessage As String
    
    l_lngLibraryID = GetData("library", "libraryID", "barcode", lp_strBarcode)
    
    If l_lngLibraryID = 0 Then
        l_intMessage = MsgBox("Could not locate barcode. Do you want to add this barcode as a new library record?", vbQuestion + vbYesNo)
        Select Case l_intMessage
            Case vbYes
                l_lngLibraryID = CreateLibraryItem(UCase(lp_strBarcode), "NEW TAPE", 0)
            Case Else
                frmLibrary.txtBarcode.Text = ""
                ClearFields frmLibrary
        End Select
    End If
            
    If GetData("vw_Required_Tapes_BBCWW", "JobDetailID", "MasterBarcode", lp_strBarcode) <> 0 Then
        l_lngJobID = GetData("vw_Required_Tapes_BBCWW", "JobID", "MasterBarcode", lp_strBarcode)
        MsgBox "Tape is required for a BBCWW Order, JobID: " & l_lngJobID, vbInformation
    ElseIf lp_strBarcode <> "" Then
        If IsNumeric(Right(lp_strBarcode, 1)) Then
            l_strSQL = "SELECT JobDetailID FROM vw_Required_Tapes_BBCWW WHERE masterbarcode LIKE '" & Left(lp_strBarcode, 1) & "%" & Right(lp_strBarcode, 4) & "';"
        Else
            l_strSQL = "SELECT JobDetailID FROM vw_Required_Tapes_BBCWW WHERE masterbarcode LIKE '" & Left(lp_strBarcode, 1) & "%" & Right(lp_strBarcode, 5) & "';"
        End If
        Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
        If l_rst.RecordCount > 0 Then
            l_rst.MoveFirst
            Do While Not l_rst.EOF
                l_lngJobDetailID = l_rst("jobdetailID")
                l_strMessage = "Is BBCWW RequiredMedia Item " & GetData("jobdetail", "masterbarcode", "JobDetailID", l_lngJobDetailID) & " - " & GetData("jobdetail", "description", "jobdetailID", l_lngJobDetailID)
                l_strMessage = l_strMessage & vbCrLf & "likely to be a match for this tape"
                If MsgBox(l_strMessage, vbYesNo + vbDefaultButton2, "Possible Fuzzy Search Tape Found") = vbYes Then
                    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", GetData("jobdetail", "masterbarcode", "jobdetailID", l_lngJobDetailID)) = 0 Then
                        l_strSQL = "INSERT INTO bbc_barcode_correction (original_barcode, corrected_barcode, created_by) VALUES ("
                        l_strSQL = l_strSQL & "'" & GetData("jobdetail", "masterbarcode", "jobdetailID", l_lngJobDetailID) & "', "
                        l_strSQL = l_strSQL & "'" & UCase(lp_strBarcode) & "', "
                        l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                    End If
                    SetData "jobdetail", "masterbarcode", "jobdetailID", l_lngJobDetailID, lp_strBarcode
                Else
                    l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                    l_strSQL = l_strSQL & "'" & GetData("jobdetail", "masterbarcode", "jobdetailID", l_lngJobDetailID) & "', "
                    l_strSQL = l_strSQL & "'" & UCase(lp_strBarcode) & "', "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                End If
                l_rst.MoveNext
            Loop
        End If
    End If
    
    If GetData("vw_Required_Tapes_DADCCheck_ID1", "ComplaintID", "Barcode", lp_strBarcode) <> 0 Then
        l_lngJobID = GetData("vw_Required_Tapes_DADCCheck_ID1", "ComplaintID", "Barcode", lp_strBarcode)
        MsgBox "Tape is required for a DADC Issue Check, ComplaintID: " & l_lngJobID, vbInformation
    ElseIf lp_strBarcode <> "" Then
        If IsNumeric(Right(lp_strBarcode, 1)) Then
            l_strSQL = "SELECT ComplaintID FROM vw_Required_Tapes_DADCCheck_ID1 WHERE barcode LIKE '" & Left(lp_strBarcode, 1) & "%" & Right(lp_strBarcode, 4) & "';"
        Else
            l_strSQL = "SELECT ComplaintID FROM vw_Required_Tapes_DADCCheck_ID1 WHERE barcode LIKE '" & Left(lp_strBarcode, 1) & "%" & Right(lp_strBarcode, 5) & "';"
        End If
        Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
        If l_rst.RecordCount > 0 Then
            l_rst.MoveFirst
            Do While Not l_rst.EOF
                l_lngJobDetailID = l_rst("ComplaintID")
                l_strMessage = "Is Issues Check RequiredMedia Item " & GetData("complaint", "MasterSourceID", "ComplaintID", l_lngJobDetailID) & " - '" & GetData("complaint", "title", "complaintID", l_lngJobDetailID)
                l_strMessage = l_strMessage & vbCrLf & "' likely to be a match for this tape"
                If MsgBox(l_strMessage, vbYesNo + vbDefaultButton2, "Possible Fuzzy Search Tape Found") = vbYes Then
                    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", GetData("complaint", "MasterScourceID", "ComplaintID", l_lngJobDetailID)) = 0 Then
                        l_strSQL = "INSERT INTO bbc_barcode_correction (original_barcode, corrected_barcode, created_by) VALUES ("
                        l_strSQL = l_strSQL & "'" & GetData("complaint", "MasterScourceID", "ComplaintID", l_lngJobDetailID) & "', "
                        l_strSQL = l_strSQL & "'" & UCase(lp_strBarcode) & "', "
                        l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                    End If
                    SetData "complaint", "scourceID", "complaintID", l_lngJobDetailID, lp_strBarcode
                Else
                    l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                    l_strSQL = l_strSQL & "'" & GetData("complaint", "MasterSourceID", "ComplaintID", l_lngJobDetailID) & "', "
                    l_strSQL = l_strSQL & "'" & UCase(lp_strBarcode) & "', "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                End If
                l_rst.MoveNext
            Loop
        End If
    End If
    
    If GetData("vw_Required_Tapes_DADCCheck_ID2", "ComplaintID", "Barcode", lp_strBarcode) <> 0 Then
        l_lngJobID = GetData("vw_Required_Tapes_DADCCheck_ID2", "ComplaintID", "Barcode", lp_strBarcode)
        MsgBox "Tape is required for a DADC Issue Check, ComplaintID: " & l_lngJobID, vbInformation
    ElseIf lp_strBarcode <> "" Then
        If IsNumeric(Right(lp_strBarcode, 1)) Then
            l_strSQL = "SELECT ComplaintID FROM vw_Required_Tapes_DADCCheck_ID2 WHERE barcode LIKE '" & Left(lp_strBarcode, 1) & "%" & Right(lp_strBarcode, 4) & "';"
        Else
            l_strSQL = "SELECT ComplaintID FROM vw_Required_Tapes_DADCCheck_ID2 WHERE barcode LIKE '" & Left(lp_strBarcode, 1) & "%" & Right(lp_strBarcode, 5) & "';"
        End If
        Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
        If l_rst.RecordCount > 0 Then
            l_rst.MoveFirst
            Do While Not l_rst.EOF
                l_lngJobDetailID = l_rst("ComplaintID")
                l_strMessage = "Is Issues Check RequiredMedia Item " & GetData("complaint", "MasterSourceID2", "ComplaintID", l_lngJobDetailID) & " - '" & GetData("complaint", "title", "complaintID2", l_lngJobDetailID)
                l_strMessage = l_strMessage & vbCrLf & "' likely to be a match for this tape"
                If MsgBox(l_strMessage, vbYesNo, "Possible Fuzzy Search Tape Found") = vbYes Then
                    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", GetData("complaint", "MasterScourceID2", "ComplaintID", l_lngJobDetailID)) = 0 Then
                        l_strSQL = "INSERT INTO bbc_barcode_correction (original_barcode, corrected_barcode, created_by) VALUES ("
                        l_strSQL = l_strSQL & "'" & GetData("complaint", "MasterSourceID2", "ComplaintID", l_lngJobDetailID) & "', "
                        l_strSQL = l_strSQL & "'" & UCase(lp_strBarcode) & "', "
                        l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                    End If
                    SetData "complaint", "scourceID2", "complaintID", l_lngJobDetailID, lp_strBarcode
                Else
                    l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                    l_strSQL = l_strSQL & "'" & GetData("complaint", "MasterScourceID2", "ComplaintID", l_lngJobDetailID) & "', "
                    l_strSQL = l_strSQL & "'" & UCase(lp_strBarcode) & "', "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                End If
                l_rst.MoveNext
            Loop
        End If
    End If
    
    If GetData("vw_Required_Tapes_DADCCheck_ID3", "ComplaintID", "Barcode", lp_strBarcode) <> 0 Then
        l_lngJobID = GetData("vw_Required_Tapes_DADCCheck_ID3", "ComplaintID", "Barcode", lp_strBarcode)
        MsgBox "Tape is required for a DADC Issue Check, ComplaintID: " & l_lngJobID, vbInformation
    ElseIf lp_strBarcode <> "" Then
        If IsNumeric(Right(lp_strBarcode, 1)) Then
            l_strSQL = "SELECT ComplaintID FROM vw_Required_Tapes_DADCCheck_ID3 WHERE barcode LIKE '" & Left(lp_strBarcode, 1) & "%" & Right(lp_strBarcode, 4) & "';"
        Else
            l_strSQL = "SELECT ComplaintID FROM vw_Required_Tapes_DADCCheck_ID3 WHERE barcode LIKE '" & Left(lp_strBarcode, 1) & "%" & Right(lp_strBarcode, 5) & "';"
        End If
        Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
        If l_rst.RecordCount > 0 Then
            l_rst.MoveFirst
            Do While Not l_rst.EOF
                l_lngJobDetailID = l_rst("ComplaintID")
                l_strMessage = "Is Issues Check RequiredMedia Item " & GetData("complaint", "MasterSourceID3", "ComplaintID", l_lngJobDetailID) & " - '" & GetData("complaint", "title", "complaintID", l_lngJobDetailID)
                l_strMessage = l_strMessage & vbCrLf & "' likely to be a match for this tape"
                If MsgBox(l_strMessage, vbYesNo, "Possible Fuzzy Search Tape Found") = vbYes Then
                    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", GetData("complaint", "MasterSourceID3", "ComplaintID", l_lngJobDetailID)) = 0 Then
                        l_strSQL = "INSERT INTO bbc_barcode_correction (original_barcode, corrected_barcode, created_by) VALUES ("
                        l_strSQL = l_strSQL & "'" & GetData("complaint", "MasterSourceID3", "ComplaintID", l_lngJobDetailID) & "', "
                        l_strSQL = l_strSQL & "'" & UCase(lp_strBarcode) & "', "
                        l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                    End If
                    SetData "complaint", "scourceID3", "complaintID", l_lngJobDetailID, lp_strBarcode
                Else
                    l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                    l_strSQL = l_strSQL & "'" & GetData("complaint", "MasterSourceID3", "ComplaintID", l_lngJobDetailID) & "', "
                    l_strSQL = l_strSQL & "'" & UCase(lp_strBarcode) & "', "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                End If
                l_rst.MoveNext
            Loop
        End If
    End If
    
    l_lngPreviewOrderID = Val(Trim(" " & GetDataSQL("SELECT TOP 1 Preview_Order_Id FROM vw_BBCMG_WaitingForTape WHERE Tape_Number = '" & lp_strBarcode & "'")))
    If l_lngPreviewOrderID <> 0 Then
        'You found one - Update it
        MsgBox "Tape is required for a BBC MG Preview Order, Request Number: " & GetData("BBCMG_Preview_Order", "Request_Number", "Preview_Order_ID", l_lngPreviewOrderID) & vbCrLf & "SLA: " & _
        GetData("BBCMG_SLA", "SLA_Title", "SLA_Id", GetData("BBCMG_Preview_order", "Priority", "Preview_Order_ID", l_lngPreviewOrderID)), vbInformation
    ElseIf lp_strBarcode <> "" Then
        'You didn't find one, so time for a fuzzy search...
        l_strSQL = "SELECT * FROM vw_BBCMG_WaitingForTape WHERE (TAPE_NUMBER LIKE '%" & Right(lp_strBarcode, 4) & "' OR TAPE_NUMBER LIKE '%" & Right(lp_strBarcode, 4) & "%')"
        Set l_rstFuzzySearch = ExecuteSQL(l_strSQL, g_strExecuteError)
        If l_rstFuzzySearch.RecordCount > 0 Then
            l_rstFuzzySearch.MoveFirst
            Do While Not l_rstFuzzySearch.EOF
                If MsgBox("Is this tape likely to be Barcode '" & l_rstFuzzySearch("Tape_Number") & "' with title '" & l_rstFuzzySearch("Program_Title"), vbYesNo + vbDefaultButton2, "Possible BBCMG Tracker Match Found") = vbYes Then
                    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", l_rstFuzzySearch("Tape_Number")) = 0 Then
                        l_strSQL = "INSERT INTO bbc_barcode_correction (original_barcode, corrected_barcode, created_by) VALUES ("
                        l_strSQL = l_strSQL & "'" & l_rstFuzzySearch("Tape_Number") & "', "
                        l_strSQL = l_strSQL & "'" & UCase(lp_strBarcode) & "', "
                        l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                    End If
                    'We've found a fuzzy match so some Barcodes need correcting, then the trackers need adjusting
                    l_lngPreviewOrderID = l_rstFuzzySearch("Preview_Order_Id")
                    MsgBox "SLA: " & GetData("BBCMG_SLA", "SLA_Title", "SLA_Id", GetData("BBCMG_Preview_order", "Priority", "Preview_Order_ID", l_lngPreviewOrderID)), vbInformation
                    l_lngExistingMediaKeyID = GetData("BBCMG_Media_Key", "Media_Key_ID", "Media_ID", lp_strBarcode)
                    If l_lngExistingMediaKeyID <> 0 Then
                        l_strExistingMediaKey = GetData("BBCMG_Media_Key", "Media_Key", "Media_ID", lp_strBarcode)
                        l_strExistingMediaKey = Mid(l_strExistingMediaKey, 2, Len(l_strExistingMediaKey) - 2)
                        SetData "BBCMG_Preview_Tracker", "ItemReference", "Preview_Order_Id", l_lngPreviewOrderID, l_strExistingMediaKey
                        SetData "BBCMG_Preview_Tracker", "ItemFilename", "Preview_Order_Id", l_lngPreviewOrderID, l_strExistingMediaKey & ".mov"
                        SetData "BBCMG_Preview_Order", "Tape_Number", "Preview_Order_Id", l_lngPreviewOrderID, lp_strBarcode
                        SetData "BBCMG_Preview_Tracker", "BarCode", "Preview_Order_Id", l_lngPreviewOrderID, lp_strBarcode
                    Else
                        l_lngExistingMediaKeyID = GetData("BBCMG_Media_Key", "Media_Key_ID", "Media_ID", l_rstFuzzySearch("Tape_Number"))
                        If l_lngExistingMediaKeyID <> 0 Then
                            SetData "BBCMG_Media_Key", "Media_Id", "Media_Key_Id", l_lngExistingMediaKeyID, lp_strBarcode
                            SetData "BBCMG_Preview_Order", "Tape_Number", "Preview_Order_Id", l_lngPreviewOrderID, lp_strBarcode
                            SetData "BBCMG_Preview_Tracker", "BarCode", "Preview_Order_Id", l_lngPreviewOrderID, lp_strBarcode
                        Else
                            SetData "BBCMG_Preview_Order", "Tape_Number", "Preview_Order_Id", l_lngPreviewOrderID, lp_strBarcode
                            l_lngExistingMediaKeyID = GetData("BBCMG_Media_Key", "Media_Key_ID", "Media_ID", lp_strBarcode)
                            If l_lngExistingMediaKeyID <> 0 Then
                                l_strExistingMediaKey = GetData("BBCMG_Media_Key", "Media_Key", "Media_ID", lp_strBarcode)
                                l_strExistingMediaKey = Mid(l_strExistingMediaKey, 1, Len(l_strExistingMediaKey) - 2)
                            End If
                            SetData "BBCMG_Preview_Tracker", "ItemReference", "Preview_Order_Id", l_lngPreviewOrderID, l_strExistingMediaKey
                            SetData "BBCMG_Preview_Tracker", "ItemFilename", "Preview_Order_Id", l_lngPreviewOrderID, l_strExistingMediaKey & ".mov"
                            SetData "BBCMG_Preview_Tracker", "BarCode", "Preview_Order_Id", l_lngPreviewOrderID, lp_strBarcode
                        End If
                    End If
                Else
                    l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                    l_strSQL = l_strSQL & "'" & l_rstFuzzySearch("Tape_Number") & "', "
                    l_strSQL = l_strSQL & "'" & UCase(lp_strBarcode) & "', "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                End If
                l_rstFuzzySearch.MoveNext
            Loop
        End If
        l_rstFuzzySearch.Close
    End If
    
    If GetData("vw_Required_Tapes_DADCBBC", "tracker_dadc_itemID", "newbarcode", lp_strBarcode) <> 0 Then
        l_lngJobDetailID = GetData("vw_Required_Tapes_DADCBBC", "tracker_dadc_itemID", "newbarcode", lp_strBarcode)
        MsgBox "Tape is required for a DADC Item, CV Code: " & GetData("tracker_dadc_item", "contentversioncode", "tracker_dadc_itemID", l_lngJobDetailID), vbInformation
    ElseIf lp_strBarcode <> "" Then
        If IsNumeric(Right(lp_strBarcode, 1)) Then
            l_strSQL = "SELECT tracker_dadc_itemID FROM vw_Required_Tapes_DADCBBC WHERE newbarcode LIKE '" & Left(lp_strBarcode, 1) & "%" & Right(lp_strBarcode, 4) & "';"
        Else
            l_strSQL = "SELECT tracker_dadc_itemID FROM vw_Required_Tapes_DADCBBC WHERE newbarcode LIKE '" & Left(lp_strBarcode, 1) & "%" & Right(lp_strBarcode, 5) & "';"
        End If
        Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
        If l_rst.RecordCount > 0 Then
            l_rst.MoveFirst
            Do While Not l_rst.EOF
                l_lngJobDetailID = l_rst("tracker_dadc_itemID")
                l_strMessage = "Is DADC Item " & GetData("tracker_dadc_item", "newbarcode", "tracker_dadc_itemID", l_lngJobDetailID) & " - " & GetData("tracker_dadc_item", "title", "tracker_dadc_itemID", l_lngJobDetailID) & " Ep. " & GetData("tracker_dadc_item", "episode", "tracker_dadc_itemID", l_lngJobDetailID)
                l_strMessage = l_strMessage & vbCrLf & "likely to be a match for this tape"
                If MsgBox(l_strMessage, vbYesNo, "Possible Fuzzy Search Tape Found") = vbYes Then
                    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", GetData("tracker_dadc_item", "newbarcode", "tracker_dadc_itemID", l_lngJobDetailID)) = 0 Then
                        l_strSQL = "INSERT INTO bbc_barcode_correction (original_barcode, corrected_barcode, created_by) VALUES ("
                        l_strSQL = l_strSQL & "'" & GetData("tracker_dadc_item", "newbarcode", "tracker_dadc_itemID", l_lngJobDetailID) & "', "
                        l_strSQL = l_strSQL & "'" & UCase(lp_strBarcode) & "', "
                        l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                    End If
                    SetData "tracker_dadc_item", "newbarcode", "tracker_dadc_itemID", l_lngJobDetailID, lp_strBarcode
                Else
                    l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                    l_strSQL = l_strSQL & "'" & GetData("tracker_dadc_item", "newbarcode", "tracker_dadc_itemID", l_lngJobDetailID) & "', "
                    l_strSQL = l_strSQL & "'" & UCase(lp_strBarcode) & "', "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                End If
                l_rst.MoveNext
            Loop
        End If
        l_rst.Close
    End If
    
    ShowLibrary l_lngLibraryID
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Public Sub MoveLibraryItem(ByVal lp_lngLibraryID As Long, ByVal lp_strLocation As String, ByVal lp_lngDespatchID As Long, Optional ByVal lp_lngBatchNumber As Long, Optional ByVal lp_strShelf As String, Optional ByVal lp_strBox As String, Optional lp_lngForJobID As Long)
    
    Dim l_strBarcode As String, l_rstFuzzySearch As ADODB.Recordset, l_lngPreviewOrderID As Long
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/movelibraryitem") Then
        Exit Sub
    End If
    
    'first check to see library item exists
    If RecordExists("library", "libraryID", lp_lngLibraryID) Then
        Dim l_strSQL As String
        If lp_lngDespatchID <> 0 Then
            l_strSQL = "UPDATE library SET location = 'OFF SITE', muser = '" & g_strUserInitials & "', mdate = '" & FormatSQLDate(Now) & "'" & ", shelf = '" & QuoteSanitise(lp_strShelf) & "', locationjobID = NULL, box = NULL, inlibrary = 'NO' WHERE libraryID = '" & lp_lngLibraryID & "';"
        Else
            l_strSQL = "UPDATE library SET location = '" & QuoteSanitise(lp_strLocation) & "', muser = '" & g_strUserInitials & "', mdate = '" & FormatSQLDate(Now) & "'" & _
            IIf(lp_strShelf = "", "", ", shelf = '" & QuoteSanitise(lp_strShelf) & "'") & IIf(lp_strBox = "", "", ", box = '" & QuoteSanitise(lp_strBox) & "'") & _
            IIf(lp_lngForJobID = 0, ", locationjobID = NULL", ", locationjobID = " & lp_lngForJobID) & _
            IIf(lp_lngForJobID <> 0 And GetData("library", "companyID", "libraryID", lp_lngLibraryID) = 501 And GetData("library", "title", "libraryID", lp_lngLibraryID) = "Blank Stock", ", companyID = " & GetData("job", "companyID", "jobID", lp_lngForJobID) & ", companyname = '" & _
            GetData("job", "companyname", "jobID", lp_lngForJobID) & "'", "") & _
            " WHERE libraryID = '" & lp_lngLibraryID & "';"
        End If
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        Dim l_blnInLibrary As Boolean
        If LCase(lp_strLocation) = "library" Then l_blnInLibrary = True
        If l_blnInLibrary = True Then SetData "library", "inlibrary", "libraryID", lp_lngLibraryID, "YES"
        
        'set the shelf / box to be null
        If g_optDontClearShelfWhenMovingTapes = 0 Then
            SetData "library", "shelf", "libraryID", lp_lngLibraryID, ""
        End If
        
        CreateLibraryTransaction lp_lngLibraryID, lp_strLocation, lp_lngDespatchID, "", lp_lngBatchNumber
        
        'check to see if the tape which is being moved is actually open in the library form.
        'if it is, then update the details on the form, so that when the user switches over to it - the location is correct.
        If IsFormLoaded("frmLibrary") Then
            If frmLibrary.lblLibraryID.Caption = lp_lngLibraryID Then
                frmLibrary.cmbLocation.Text = lp_strLocation
                frmLibrary.cmbShelf.Text = GetData("library", "shelf", "libraryID", lp_lngLibraryID)
                frmLibrary.cmbBox.Text = GetData("library", "box", "libraryID", lp_lngLibraryID)
                If GetData("library", "locationjobID", "libraryID", lp_lngLibraryID) <> 0 Then frmLibrary.txtLocationJobID.Text = GetData("library", "locationjobID", "libraryID", lp_lngLibraryID)
                frmLibrary.lblInLibrary.Caption = GetData("library", "inlibrary", "libraryID", lp_lngLibraryID)
                DoEvents
            End If
        End If
        
    
        l_strBarcode = GetData("library", "barcode", "libraryID", lp_lngLibraryID)
        If RecordExists("tracker_dadc_Item", "barcode", l_strBarcode) Then
            SetData "tracker_dadc_item", "tapelocation", "barcode", l_strBarcode, lp_strLocation
            If lp_strShelf <> "" Then SetData "tracker_dadc_item", "tapeshelf", "barcode", l_strBarcode, lp_strShelf
        End If
        
        Set l_rstFuzzySearch = ExecuteSQL("SELECT Preview_Order_Id FROM vw_BBCMG_TapeArrived WHERE Tape_Number = '" & l_strBarcode & "'", g_strExecuteError)
        If l_rstFuzzySearch.RecordCount > 0 Then
            l_rstFuzzySearch.MoveFirst
            Do While Not l_rstFuzzySearch.EOF
                l_lngPreviewOrderID = l_rstFuzzySearch("Preview_Order_ID")
                SetData "BBCMG_Preview_Tracker", "TapeLocation", "Preview_order_Id", l_lngPreviewOrderID, lp_strLocation
                SetData "BBCMG_Preview_Tracker", "TapeShelf", "Preview_Order_Id", l_lngPreviewOrderID, lp_strShelf
                l_rstFuzzySearch.MoveNext
            Loop
        End If
        l_rstFuzzySearch.Close
        Set l_rstFuzzySearch = Nothing
        
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub
Sub MoveLibraryItemInternally(ByVal lp_lngLibraryID As Long, ByVal lp_strLocation As String, ByVal lp_strTakenBy As String, ByVal lp_lngBatchNumber As Long, Optional ByVal lp_strShelf As String, Optional ByVal lp_strBox As String, Optional ByVal lp_lngForJobID As Long)

Dim l_strSQL As String
l_strSQL = "INSERT INTO movement (libraryID, cuser, cdate, destination, takenby, batchnumber, shelf, box, forjobID) VALUES ('" & lp_lngLibraryID & "', '" & g_strUserInitials & "', '" & FormatSQLDate(Now) & "', '" & QuoteSanitise(lp_strLocation) & "', '" & lp_strTakenBy & "', '" & lp_lngBatchNumber & "', '" & QuoteSanitise(lp_strShelf) & "', '" & QuoteSanitise(lp_strBox) & "', " & lp_lngForJobID & ")"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Sub

Sub ShowLibraryMovement()

If Not CheckAccess("/showlibrarymovement") Then Exit Sub

frmMovement.Show

End Sub

Sub ShowShelfAudit()

If Not CheckAccess("/libraryshelfaudit") Then Exit Sub

frmLibraryAudit.Show
frmLibraryAudit.ZOrder 0

End Sub
Sub NoLibraryItemSelectedMessage()
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    MsgBox "Please select a valid library item first", vbExclamation
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub
Sub OverwriteExistingTape()
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/savelibrary") Then
        Exit Sub
    End If
    
    Dim l_strNewLibraryBarcode As String
    Dim l_lngNewLibraryID As Long
    
    Dim l_strSQL As String
    
    frmGetBarcodeNumber.Caption = "Please give the Barcode to overwrite to..."
    frmGetBarcodeNumber.txtBarcode.Text = ""
    frmGetBarcodeNumber.cmdIssueNext.Visible = False
    frmGetBarcodeNumber.Show vbModal
    l_strNewLibraryBarcode = UCase(frmGetBarcodeNumber.txtBarcode.Text)
    
    Unload frmGetBarcodeNumber
    'Check whether the new barcode was accepted, or escape was pressed.
    If l_strNewLibraryBarcode = "" Then
        Exit Sub
    End If
    
    l_lngNewLibraryID = GetData("library", "LibraryID", "barcode", l_strNewLibraryBarcode)

    If l_lngNewLibraryID = 0 Then
        MsgBox "Barcode was not found.", vbExclamation
        Exit Sub
    End If
    
    'pick up the existing ID
    Dim l_lngLibraryID As Long
    l_lngLibraryID = frmLibrary.lblLibraryID.Caption
    
    'set the new ID so we can overwrite
    frmLibrary.lblLibraryID.Caption = l_lngNewLibraryID
    frmLibrary.lbltechrevID.Caption = ""
    frmLibrary.cmbTechReviewList.Text = ""
    
    frmLibrary.txtSubTitle.Text = GetData("library", "subtitle", "libraryID", l_lngNewLibraryID)
    frmLibrary.cmbEpisode.Text = GetData("library", "episode", "libraryID", l_lngNewLibraryID)
    frmLibrary.txtInternalReference.Text = ""
    frmLibrary.txtReference.Text = ""
    
    SaveLibrary
    
    Dim l_lngCountOfEvents As Long
    l_lngCountOfEvents = GetCount("SELECT COUNT(eventID) FROM tapeevents WHERE libraryID = '" & l_lngNewLibraryID & "';")
    
    If l_lngCountOfEvents > 0 Then
        Dim l_intMsg As Integer
        l_intMsg = MsgBox("Do you want to delete the " & l_lngCountOfEvents & " existing events from " & l_strNewLibraryBarcode & " before you copy the events from your original tape?", vbQuestion + vbYesNo)
        If l_intMsg = vbYes Then
            ExecuteSQL "DELETE FROM tapeevents WHERE libraryID = '" & l_lngNewLibraryID & "';", g_strExecuteError
            CheckForSQLError
        End If
    End If
    
    CopyAllEvents l_lngLibraryID, l_lngNewLibraryID
    
    frmLibrary.MousePointer = vbDefault
    
    ShowLibrary l_lngNewLibraryID
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub PromptLibraryChanges(lp_lngLibraryID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_intResponse As Integer
    If CheckLibraryChanges(lp_lngLibraryID) = True Then
        l_intResponse = MsgBox("Data has changed. Do you wish to save?", vbYesNo + vbQuestion, "Data has changed.")
        If l_intResponse = vbYes Then
            SaveLibrary
        End If
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub
Function SanitiseBarcode(lp_strBarcode As String) As String
    
    'remove illegal file name chars
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strNewBarcode As String
    l_strNewBarcode = Replace(lp_strBarcode, "/", "-")
    l_strNewBarcode = Replace(l_strNewBarcode, "\", "-")
    l_strNewBarcode = Replace(l_strNewBarcode, "?", "-")
    l_strNewBarcode = Replace(l_strNewBarcode, "*", "-")
    l_strNewBarcode = Replace(l_strNewBarcode, "&", "-")
    l_strNewBarcode = Replace(l_strNewBarcode, "#", "-")
    'l_strNewBarcode = Replace(l_strNewBarcode, "%", "-")
    
    SanitiseBarcode = l_strNewBarcode
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function SaveLibrary()
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/savelibrary") Then
        Exit Function
    End If
    
    
    'Check if this is a new tape, and perform other initialisations if it is.
    Dim l_oldLocation As String
    Dim l_oldShelf As String
    Dim l_strSQL As String
    Dim l_strNewBarcode As String
    Dim l_rst As ADODB.Recordset, l_rst2 As ADODB.Recordset
    
    l_strNewBarcode = ""
        
    If Val(frmLibrary.lblLibraryID.Caption) = 0 Then
        'Tape has not previously been saved...
        'check that the barcode already in the barcode box is ok to use - i.e. doesn;t exist on another tape
        
        If frmLibrary.txtBarcode.Text = "" Or Val(GetData("library", "libraryID", "barcode", frmLibrary.txtBarcode.Text)) <> 0 Then
            'No barcode specified, or barcode specifried already exists on a tape, despite no tape having been previously loaded...
            'Get a new barcode for this tape
            If g_optAllocateBarcodes = 1 Then frmGetBarcodeNumber.txtBarcode.Text = g_strCompanyPrefix & GetNextSequence("barcodenumber")
            frmGetBarcodeNumber.Caption = "Please confirm or edit new tape barcode"
            frmGetBarcodeNumber.Show vbModal
            
            l_strNewBarcode = UCase(frmGetBarcodeNumber.txtBarcode.Text)
            If GetData("library", "libraryID", "barcode", l_strNewBarcode) <> 0 Then
                MsgBox "Barcode Already Exists", vbExclamation, "Wrong Barcode"
                Exit Function
            End If
            frmLibrary.txtBarcode.Text = l_strNewBarcode
            Unload frmGetBarcodeNumber
        
        ElseIf frmLibrary.txtBarcode.Text <> "" And Val(GetData("library", "libraryID", "barcode", frmLibrary.txtBarcode.Text)) = 0 Then
            'Barcode has been specified and is free to use...
            l_strNewBarcode = frmLibrary.txtBarcode.Text
        End If
    
        If l_strNewBarcode = "" Then
            'We have a barcode specified and it already exists on another tape...
            MsgBox "Barcode Already Exists", vbExclamation, "Wrong Barcode"
            Exit Function
        End If
        If frmLibrary.lblCompanyID.Caption = "" Then
            frmLibrary.lblCompanyID.Caption = 0
        End If
    Else
        'Tape has been preciously saved, so reset the entry in the barcode box to the barcode for that tape
        If Trim(" " & GetData("library", "copytype", "libraryID", frmLibrary.lblLibraryID.Caption)) = "MASTER" Then
            If MsgBox("You are saving new details for a MASTER tape." & vbCrLf & "Are you sure?", vbYesNo, "Saving Master Data") = vbNo Then
                Exit Function
            End If
        End If
        frmLibrary.txtBarcode.Text = GetData("library", "barcode", "libraryID", Val(frmLibrary.lblLibraryID.Caption))
    End If
    
    If frmLibrary.txtBarcode.Text = "" Then
        MsgBox "Tape Not Saved.", vbCritical, "Error - cannot have a blank barcode."
        Exit Function
    End If
    
    'check numeric or blank job number
    'check library client = job client
    
    Dim l_lngJobID As Long
    l_lngJobID = Val(frmLibrary.txtJobID.Text)
    
    If frmLibrary.txtJobID.Text <> "999999" Then
        
        If Not ((frmLibrary.txtJobID.Text = "") Or (IsNumeric(frmLibrary.txtJobID.Text))) Then
            MsgBox "Job ID must be numeric or blank", vbExclamation, "Wrong Job ID"
            frmLibrary.txtJobID.SetFocus
            Exit Function
        Else
            
            Dim l_lngJobCompanyID As Long
            Dim l_lngLibraryCompanyID As Long
            Dim l_lngJobCompanyName As String
            l_lngJobCompanyID = GetData("job", "companyID", "jobID", l_lngJobID)
            l_lngJobCompanyName = GetData("job", "companyname", "jobID", l_lngJobID)
            
            l_lngLibraryCompanyID = frmLibrary.lblCompanyID.Caption
            If l_lngJobCompanyID <> l_lngLibraryCompanyID And l_lngJobID <> 0 Then
                Dim l_intMsg As Integer
                l_intMsg = MsgBox("The job client is '" & l_lngJobCompanyName & "', however, the library media appears to be owned by someone else." & vbCrLf & vbCrLf & "Do you want to change the LIBRARY record so that the job client is the owner?", vbYesNoCancel + vbQuestion, "Out of sync client")
                Select Case l_intMsg
                    Case vbYes
                        'update the company name and ID then save
                        frmLibrary.cmbCompany.Text = l_lngJobCompanyName
                        frmLibrary.lblCompanyID.Caption = l_lngJobCompanyID
                    Case vbNo
                        'leave as is 7and continue with the same
                    Case vbCancel
                        'stop the save process
                        Exit Function
                End Select
                
            End If
        End If
        If Val(frmLibrary.txtJobID.Text) <> 0 Then
            If GetStatusNumber(GetData("job", "fd_status", "jobID", Val(frmLibrary.txtJobID.Text))) < 5 Then
                If MsgBox("JobID refers to a Job that hasn't been started yet - please confirm this is correct", vbYesNo, "Saving Clip...") = vbNo Then
                    Exit Function
                End If
            End If
        End If
        
    End If
    
    'If there is a valid job number for the tape, if this job has order number STAR ORDER, or any kind of BBC WW Int Ops order
    'then the Job line number must refer to a line from that job. Otherwise, this field to be left blank.
    
    If Not ((frmLibrary.txtJobDetailID.Text = "") Or (IsNumeric(frmLibrary.txtJobDetailID.Text))) Then
        MsgBox "Job detail ID must be numeric or blank", vbExclamation, "Wrong Job Detail ID"
        frmLibrary.txtJobDetailID.SetFocus
        Exit Function
    End If
    
    Dim l_strJobOrderNumber As String, l_lngJobOwnerID As Long, l_blnBBCWWJob As Boolean
    l_blnBBCWWJob = False
    l_lngJobOwnerID = GetData("job", "companyID", "jobID", l_lngJobID)
    l_strJobOrderNumber = GetData("job", "orderreference", "jobID", l_lngJobID)
'    If UCase(l_strJobOrderNumber) = "STAR ORDER" Or (l_lngJobOwnerID = 570 And g_intCheckAllBBCJobs <> 0) Then
    If l_lngJobOwnerID = 570 Then
        Dim l_lngJobDetailJobID As Long
        l_lngJobDetailJobID = GetData("jobdetail", "jobID", "jobdetailID", frmLibrary.txtJobDetailID.Text)
        If l_lngJobID <> l_lngJobDetailJobID Then
            MsgBox "Job is a BBCWW Int Ops job - a valid job detail ID is required.", vbExclamation, "Job Detail ID Required"
            Exit Function
        Else
            l_blnBBCWWJob = True
            If MsgBox("BBCWW Job item " & GetData("jobdetail", "description", "jobdetailID", frmLibrary.txtJobDetailID.Text) & vbCrLf & "Is this tape finished yet?", vbYesNo, "Mark BBCWW Tracker as Complete") = vbYes Then
                SetData "jobdetail", "DateCopyMade", "jobdetailID", frmLibrary.txtJobDetailID.Text, FormatSQLDate(Now)
                SetData "jobdetail", "CopyBarcode", "jobdetailID", frmLibrary.txtJobDetailID.Text, frmLibrary.txtBarcode.Text
                SetData "jobdetail", "CopyMadeBy", "jobdetailID", frmLibrary.txtJobDetailID.Text, g_strUserInitials
            End If
        End If
    End If

    'Check that the Title provided matches the title from the job sheet, and give option to ammend.
    
    Dim l_strTitleFromOrder As String
    
    If l_blnBBCWWJob = True And g_intCheckAllBBCJobs <> 0 Then
        l_strTitleFromOrder = GetData("jobdetail", "description", "jobdetailID", frmLibrary.txtJobDetailID.Text)
        If l_strTitleFromOrder <> frmLibrary.txtTitle.Text Then
            If MsgBox("Should title be set to match title provided in order details?", vbYesNo, "Title discepancy") = vbYes Then
                frmLibrary.txtTitle.Text = l_strTitleFromOrder
            End If
        End If
    End If
    
    'Check the Rigorous Fields
    If g_intEnforceRigorousLibraryChecks <> 0 And InStr(GetData("company", "cetaclientcode", "companyID", Val(frmLibrary.lblCompanyID.Caption)), "/rigorous") > 0 Then
        Set l_rst = ExecuteSQL("SELECT information FROM xref WHERE description COLLATE Latin1_General_CS_AS = '" & QuoteSanitise(frmLibrary.txtTitle.Text) _
        & "' AND information = " & Val(frmLibrary.lblCompanyID.Caption), g_strExecuteError)
        CheckForSQLError
        If l_rst.RecordCount <= 0 Then
            If MsgBox("Do you wish to define this title as a valid title for this Company?", vbYesNo + vbQuestion, "Title does not match existing list of Titles for " _
            & frmLibrary.cmbCompany.Text) = vbYes Then
                ExecuteSQL "INSERT INTO xref (description, information, category) VALUES ('" & QuoteSanitise(frmLibrary.txtTitle.Text) & "', " & Val(frmLibrary.lblCompanyID.Caption) _
                & ", 'librarytitle');", g_strExecuteError
                CheckForSQLError
            Else
                MsgBox "Please use a Title from the drop down list", vbCritical, "Title filed rigorouxs checking"
                Exit Function
            End If
        End If
        l_rst.Close
    End If
    
    If frmLibrary.lblLibraryID.Caption <> "" Then
        'Check the old Location so we can work out if its changed. Do a transaction of it has.
        l_oldLocation = GetData("library", "location", "libraryID", frmLibrary.lblLibraryID.Caption)
        l_oldShelf = GetData("library", "shelf", "libraryID", frmLibrary.lblLibraryID.Caption)
    End If
    
    'Enforce Rigorous Episode numbers
    If InStr(GetData("company", "cetaclientcode", "companyID", Val(frmLibrary.lblCompanyID.Caption)), "/rigorous") > 0 Then
        If Not IsDate(frmLibrary.cmbEpisode.Text) Then frmLibrary.cmbEpisode.Text = Format(frmLibrary.cmbEpisode.Text, "000")
        If Not IsDate(frmLibrary.cmbEpisodeTo.Text) Then frmLibrary.cmbEpisodeTo.Text = Format(frmLibrary.cmbEpisodeTo.Text, "000")
        If Not IsDate(frmLibrary.cmbSeries.Text) Then frmLibrary.cmbSeries.Text = Format(frmLibrary.cmbSeries.Text, "00")
    End If
    
    'Check LIBRARY and OPERATIONS locations for Shelf and Job Number...
    If InStr(LCase(frmLibrary.cmbLocation.Text), "library") > 0 Then
        If (frmLibrary.cmbShelf.Text = "" And frmLibrary.cmbBox.Text = "") Then
            MsgBox "Cannot save to Library unless a shelf or a box is specified", vbCritical, "Cannot save tape"
            Exit Function
        End If
    ElseIf InStr(LCase(frmLibrary.cmbLocation.Text), "operations") > 0 Then
        If Val(frmLibrary.txtLocationJobID.Text) = 0 Then
            If Val(frmLibrary.txtJobID.Text) <> 0 Then
                frmLibrary.txtLocationJobID.Text = frmLibrary.txtJobID.Text
            Else
                MsgBox "Cannot save to Operations unless a 'Here for Job' is specified", vbCritical, "Cannot save tape"
                Exit Function
            End If
        End If
    End If
    
    'Check the Shine type stuff
    If InStr(GetData("company", "cetaclientcode", "companyID", Val(frmLibrary.lblCompanyID.Caption)), "/shineversion") > 0 And frmLibrary.cmbCopyType.Text = "MASTER" Then
        If frmLibrary.cmbVersion.Text = "" Then
            MsgBox "Version must be set on clips for this customer.", vbCritical, "Error..."
            Exit Function
        End If
        If frmLibrary.cmbField1.Text = "" Then
            MsgBox "Technical Category (custom field) must be set on clips for this customer.", vbCritical, "Error..."
            Exit Function
        End If
        If frmLibrary.cmbField2.Text = "" Then
            MsgBox "Category (custom field) must be set on clips for this customer.", vbCritical, "Error..."
            Exit Function
        End If
'        If frmLibrary.cmbField3.Text = "" Then
'            MsgBox "Asset ID (custom field) must be set on clips for this customer.", vbCritical, "Error..."
'            Exit Function
'        End If
    End If
    
    frmLibrary.MousePointer = vbHourglass
    
    'Do a whole bunch of checks relating to BBCWW tapes for whether they are for BBCWW, DADC or BBCMG jobs or for Issues Tracker Checkking items.
    Dim l_lngJobDetailID As Long, l_strMessage As String
    Dim l_lngPreviewOrderID As Long, l_lngMediaKeyID As Long
    Dim l_rstFuzzySearch As ADODB.Recordset
    Dim l_lngExistingMediaKeyID As Long, l_strExistingMediaKey As String
    Dim l_blnFoundOne As Boolean
    Dim l_datNewTargetDate As Date, l_datNewTargetDate2 As Date
    l_blnFoundOne = False
    l_lngPreviewOrderID = 0
    If Val(frmLibrary.lblLibraryID.Caption) <> 0 Then
        If GetData("company", "source", "companyID", Val(frmLibrary.lblCompanyID.Caption)) = "BBC" Then
            If IsItHere(frmLibrary.cmbLocation.Text) Then
                'Tape is on the premises - see if it is a required master for BBCWW, BBCMG or DADC BBC
                'Check for BBCWW Orders with fuzzy search
                If GetData("vw_Required_Tapes_BBCWW", "JobDetailID", "MasterBarcode", frmLibrary.txtBarcode.Text) <> 0 Then
                    Do While GetData("vw_Required_Tapes_BBCWW", "JobDetailID", "MasterBarcode", frmLibrary.txtBarcode.Text) <> 0
                        l_lngJobDetailID = GetData("vw_Required_Tapes_BBCWW", "JobDetailID", "MasterBarcode", frmLibrary.txtBarcode.Text)
                        l_lngJobID = GetData("vw_Required_Tapes_BBCWW", "JobID", "MasterBarcode", frmLibrary.txtBarcode.Text)
                        l_blnFoundOne = True
                        SetData "jobdetail", "DateMasterArrived", "jobdetailID", l_lngJobDetailID, FormatSQLDate(Now)
                        SetData "jobdetail", "DateTapeSentBack", "jobdetailID", l_lngJobDetailID, Null
                        SetData "jobdetail", "Completeable", "jobdetailID", l_lngJobDetailID, FormatSQLDate(Now)
                        If GetData("jobdetail", "firstcompleteable", "jobdetailID", l_lngJobDetailID) = 0 Then SetData "jobdetail", "FirstCompleteable", "jobdetailID", l_lngJobDetailID, FormatSQLDate(Now)
                        Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Val(Trim(" " & GetData("jobdetail", "TargetDateCountdown", "jobdetailID", l_lngJobDetailID))), g_strExecuteError)
                        l_datNewTargetDate = l_rst(0)
                        l_rst.Close
                        If IsDate(GetData("vw_Tracker_BBCWW", "deadlinedate", "jobdetailID", l_lngJobDetailID)) Then
                            l_datNewTargetDate2 = GetData("vw_Tracker_BBCWW", "deadlinedate", "jobdetailID", l_lngJobDetailID)
                            l_datNewTargetDate2 = DateAdd("h", -12, l_datNewTargetDate2)
                        End If
                        If l_datNewTargetDate2 > l_datNewTargetDate And GetData("vw_Tracker_BBCWW", "priority", "jobdetailID", l_lngJobDetailID) = 0 Then l_datNewTargetDate = l_datNewTargetDate2
                        SetData "jobdetail", "Targetdate", "jobdetailID", l_lngJobDetailID, FormatSQLDate(l_datNewTargetDate)
                        If GetCount("SELECT COUNT(jobdetailID) FROM vw_Tracker_BBCWW WHERE jobID = " & l_lngJobID & " AND (DateMasterArrived IS NULL OR (DateMasterArrived IS NOT NULL AND DateTapeSentBack IS NOT NULL))") = 0 Then
                            SetData "job", "fd_status", "jobID", l_lngJobID, "Masters Here"
                        End If
                        l_datNewTargetDate = GetData("job", "deadlinedate", "jobID", l_lngJobID)
                        l_datNewTargetDate2 = GetData("jobdetail", "Targetdate", "jobdetailID", l_lngJobDetailID)
                        If l_datNewTargetDate2 > l_datNewTargetDate Then
                            SetData "jobdetail", "MasterIsLateFlag", "jobdetailID", l_lngJobDetailID, 1
                        Else
                            SetData "jobdetail", "MasterIsLateFlag", "jobdetailID", l_lngJobDetailID, 0
                        End If
                        MsgBox "Tape is required for a BBCWW Order, JobID: " & l_lngJobID & ", Due Date:" & GetData("jobdetail", "Targetdate", "jobdetailID", l_lngJobDetailID), vbInformation
                    Loop
                Else
                    If IsNumeric(Right(frmLibrary.txtBarcode.Text, 1)) Then
                        l_strSQL = "SELECT JobDetailID, deadlinedate FROM vw_Required_Tapes_BBCWW WHERE masterbarcode LIKE '" & Left(frmLibrary.txtBarcode.Text, 1) & "%" & Right(frmLibrary.txtBarcode.Text, 4) & "';"
                    Else
                        l_strSQL = "SELECT JobDetailID, deadlinedate FROM vw_Required_Tapes_BBCWW WHERE masterbarcode LIKE '" & Left(frmLibrary.txtBarcode.Text, 1) & "%" & Right(frmLibrary.txtBarcode.Text, 5) & "';"
                    End If
                    Set l_rstFuzzySearch = ExecuteSQL(l_strSQL, g_strExecuteError)
                    If l_rstFuzzySearch.RecordCount > 0 Then
                        l_rstFuzzySearch.MoveFirst
                        Do While Not l_rstFuzzySearch.EOF
                            l_lngJobDetailID = l_rstFuzzySearch("jobdetailID")
                            l_blnFoundOne = True
                            l_strMessage = "Is BBCWW RequiredMedia Item " & GetData("jobdetail", "masterbarcode", "JobDetailID", l_lngJobDetailID) & " - " & GetData("jobdetail", "description", "jobdetailID", l_lngJobDetailID)
                            l_strMessage = l_strMessage & vbCrLf & "likely to be a match for this tape: " & frmLibrary.txtBarcode.Text & " - " & frmLibrary.txtTitle.Text
                            If MsgBox(l_strMessage, vbYesNo + vbDefaultButton2, "Possible Fuzzy Search Tape Found") = vbYes Then
                                If MsgBox(l_strMessage, vbYesNo + vbDefaultButton2, "Please confirm a fuzzy search match") = vbYes Then
                                    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", GetData("jobdetail", "masterbarcode", "jobdetailID", l_lngJobDetailID)) = 0 Then
                                        l_strSQL = "INSERT INTO bbc_barcode_correction (original_barcode, corrected_barcode, created_by) VALUES ("
                                        l_strSQL = l_strSQL & "'" & GetData("jobdetail", "masterbarcode", "jobdetailID", l_lngJobDetailID) & "', "
                                        l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                        l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                        ExecuteSQL l_strSQL, g_strExecuteError
                                        CheckForSQLError
                                    End If
                                    l_lngJobID = GetData("Jobdetail", "jobID", "jobdetailID", l_lngJobDetailID)
                                    SetData "jobdetail", "masterbarcode", "jobdetailID", l_lngJobDetailID, frmLibrary.txtBarcode.Text
                                    SetData "jobdetail", "DateMasterArrived", "jobdetailID", l_lngJobDetailID, FormatSQLDate(Now)
                                    SetData "jobdetail", "DateTapeSentBack", "jobdetailID", l_lngJobDetailID, Null
                                    SetData "jobdetail", "Completeable", "jobdetailID", l_lngJobDetailID, FormatSQLDate(Now)
                                    If GetData("jobdetail", "firstcompleteable", "jobdetailID", l_lngJobDetailID) = 0 Then SetData "jobdetail", "FirstCompleteable", "jobdetailID", l_lngJobDetailID, FormatSQLDate(Now)
                                    Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Val(Trim(" " & GetData("jobdetail", "TargetDateCountdown", "jobdetailID", l_lngJobDetailID))), g_strExecuteError)
                                    l_datNewTargetDate = l_rst(0)
                                    l_rst.Close
                                    If IsDate(l_rstFuzzySearch("deadlinedate")) Then
                                        l_datNewTargetDate2 = l_rstFuzzySearch("deadlinedate")
                                        l_datNewTargetDate2 = DateAdd("h", -12, l_datNewTargetDate2)
                                    End If
                                    If l_datNewTargetDate2 > l_datNewTargetDate And GetData("vw_Tracker_BBCWW", "priority", "jobdetailID", l_lngJobDetailID) = 0 Then l_datNewTargetDate = l_datNewTargetDate2
                                    SetData "jobdetail", "Targetdate", "jobdetailID", l_lngJobDetailID, FormatSQLDate(l_datNewTargetDate)
                                    If GetCount("SELECT COUNT(jobdetailID) FROM vw_Tracker_BBCWW WHERE jobID = " & l_lngJobID & " AND (DateMasterArrived IS NULL OR (DateMasterArrived IS NOT NULL AND DateTapeSentBack IS NOT NULL))") = 0 Then
                                        SetData "job", "fd_status", "jobID", l_lngJobID, "Masters Here"
                                    End If
                                    l_datNewTargetDate = GetData("job", "deadlinedate", "jobID", l_lngJobID)
                                    l_datNewTargetDate2 = GetData("jobdetail", "Targetdate", "jobdetailID", l_lngJobDetailID)
                                    If l_datNewTargetDate2 > l_datNewTargetDate Then
                                        SetData "jobdetail", "MasterIsLateFlag", "jobdetailID", l_lngJobDetailID, 1
                                    Else
                                        SetData "jobdetail", "MasterIsLateFlag", "jobdetailID", l_lngJobDetailID, 0
                                    End If
                                    MsgBox "Tape is required for a BBCWW Order, JobID: " & l_lngJobID & ", Due Date:" & GetData("jobdetail", "Targetdate", "jobdetailID", l_lngJobDetailID), vbInformation
                                Else
                                    l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                                    l_strSQL = l_strSQL & "'" & GetData("jobdetail", "masterbarcode", "jobdetailID", l_lngJobDetailID) & "', "
                                    l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                    l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                    ExecuteSQL l_strSQL, g_strExecuteError
                                    CheckForSQLError
                                End If
                            Else
                                l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                                l_strSQL = l_strSQL & "'" & GetData("jobdetail", "masterbarcode", "jobdetailID", l_lngJobDetailID) & "', "
                                l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                ExecuteSQL l_strSQL, g_strExecuteError
                                CheckForSQLError
                            End If
                            l_rstFuzzySearch.MoveNext
                        Loop
                    End If
                    l_rstFuzzySearch.Close
                End If
                
                'Check for a BBC MG Preview Order, with possiible fuzzy search.
                
                If Val(Trim(" " & GetDataSQL("SELECT TOP 1 Preview_Order_Id FROM vw_BBCMG_WaitingForTape WHERE Tape_Number = '" & frmLibrary.txtBarcode.Text & "'"))) <> 0 Then
                    'You found one - Update it
                    Do While Val(Trim(" " & GetDataSQL("SELECT TOP 1 Preview_Order_Id FROM vw_BBCMG_WaitingForTape WHERE Tape_Number = '" & frmLibrary.txtBarcode.Text & "'"))) <> 0
                        l_lngPreviewOrderID = Val(Trim(" " & GetDataSQL("SELECT TOP 1 Preview_Order_Id FROM vw_BBCMG_WaitingForTape WHERE Tape_Number = '" & frmLibrary.txtBarcode.Text & "'")))
                        l_blnFoundOne = True
                        SetData "BBCMG_Preview_Tracker", "DateTapeArrived", "Preview_Order_Id", l_lngPreviewOrderID, FormatSQLDate(Now)
                        SetData "BBCMG_Preview_Tracker", "TimeTapeArrived", "Preview_Order_Id", l_lngPreviewOrderID, Format(Now, "HH:NN:SS")
                        SetData "BBCMG_Preview_Tracker", "DateTapeLeft", "Preview_Order_Id", l_lngPreviewOrderID, Null
                        SetData "BBCMG_Preview_Tracker", "TimeTapeLeft", "Preview_Order_Id", l_lngPreviewOrderID, Null
                        SetData "BBCMG_Preview_Tracker", "TapeLocation", "Preview_order_Id", l_lngPreviewOrderID, frmLibrary.cmbLocation.Text
                        SetData "BBCMG_Preview_Tracker", "TapeShelf", "Preview_Order_Id", l_lngPreviewOrderID, frmLibrary.cmbShelf.Text
                        ExecuteSQL "exec BBCMG_Tape_Arrived @Preview_Order_Id = " & l_lngPreviewOrderID & ";", g_strExecuteError
                        'SetData "BBCMG_Preview_Order", "Order_Status_Id", "Preview_Order_Id", l_lngPreviewOrderID, GetData("BBCMG_Order_Status", "Order_Status_Id", "Order_Status", "Tape Arrived")
                        MsgBox "Tape is required for a BBC MG Preview Order, Request Number: " & GetData("BBCMG_Preview_Order", "Request_Number", "Preview_Order_ID", l_lngPreviewOrderID) & vbCrLf & "SLA: " & _
                        GetData("BBCMG_SLA", "SLA_Title", "SLA_Id", GetData("BBCMG_Preview_order", "Priority", "Preview_Order_ID", l_lngPreviewOrderID)), vbInformation
                    Loop
                ElseIf Val(Trim(" " & GetDataSQL("SELECT TOP 1 Preview_Order_Id FROM vw_BBCMG_TapeArrived WHERE Tape_Number = '" & frmLibrary.txtBarcode.Text & "'"))) <> 0 Then
                    Set l_rstFuzzySearch = ExecuteSQL("SELECT Preview_Order_Id FROM vw_BBCMG_TapeArrived WHERE Tape_Number = '" & frmLibrary.txtBarcode.Text & "'", g_strExecuteError)
                    l_rstFuzzySearch.MoveFirst
                    Do While Not l_rstFuzzySearch.EOF
                        l_lngPreviewOrderID = l_rstFuzzySearch("Preview_Order_ID")
                        SetData "BBCMG_Preview_Tracker", "TapeLocation", "Preview_order_Id", l_lngPreviewOrderID, frmLibrary.cmbLocation.Text
                        SetData "BBCMG_Preview_Tracker", "TapeShelf", "Preview_Order_Id", l_lngPreviewOrderID, frmLibrary.cmbShelf.Text
                        l_rstFuzzySearch.MoveNext
                    Loop
                    l_rstFuzzySearch.Close
                Else
                    'You didn't find one, so time for a fuzzy search...
                    l_strSQL = "SELECT * FROM vw_BBCMG_WaitingForTape WHERE (TAPE_NUMBER LIKE '%" & Right(frmLibrary.txtBarcode.Text, 4) & "' OR TAPE_NUMBER LIKE '%" & Right(frmLibrary.txtBarcode.Text, 4) & "%')"
                    l_strSQL = l_strSQL & " AND TAPE_NUMBER NOT LIKE '%.%'"
                    Set l_rstFuzzySearch = ExecuteSQL(l_strSQL, g_strExecuteError)
                    If l_rstFuzzySearch.RecordCount > 0 Then
                        l_rstFuzzySearch.MoveFirst
                        Do While Not l_rstFuzzySearch.EOF
                            If MsgBox("Is this tape " & frmLibrary.txtBarcode.Text & " - " & frmLibrary.txtTitle.Text & " likely to be Barcode '" & l_rstFuzzySearch("Tape_Number") & "' with title '" & l_rstFuzzySearch("Program_Title"), vbYesNo + vbDefaultButton2, "Possible BBCMG Tracker Match Found") = vbYes Then
                                If MsgBox("Are you sure this tape " & frmLibrary.txtBarcode.Text & " - " & frmLibrary.txtTitle.Text & " is the ordered tape Barcode '" & l_rstFuzzySearch("Tape_Number") & "' with title '" & l_rstFuzzySearch("Program_Title"), vbYesNo + vbDefaultButton2, "Please Confirm BBCMG Tracker Match Found") = vbYes Then
                                    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", l_rstFuzzySearch("Tape_Number")) = 0 Then
                                        l_strSQL = "INSERT INTO bbc_barcode_correction (original_barcode, corrected_barcode, created_by) VALUES ("
                                        l_strSQL = l_strSQL & "'" & l_rstFuzzySearch("Tape_Number") & "', "
                                        l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                        l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                        ExecuteSQL l_strSQL, g_strExecuteError
                                        CheckForSQLError
                                    End If
                                    'We've found a fuzzy match so some Barcodes need correcting, then the trackers need adjusting
                                    l_lngPreviewOrderID = l_rstFuzzySearch("Preview_Order_Id")
                                    l_lngExistingMediaKeyID = GetData("BBCMG_Media_Key", "Media_Key_ID", "Media_ID", frmLibrary.txtBarcode.Text)
                                    If l_lngExistingMediaKeyID <> 0 Then
                                        l_strExistingMediaKey = GetData("BBCMG_Media_Key", "Media_Key", "Media_ID", frmLibrary.txtBarcode.Text)
                                        l_strExistingMediaKey = Mid(l_strExistingMediaKey, 2, Len(l_strExistingMediaKey) - 2)
                                        SetData "BBCMG_Preview_Tracker", "ItemReference", "Preview_Order_Id", l_lngPreviewOrderID, l_strExistingMediaKey
                                        SetData "BBCMG_Preview_Tracker", "ItemFilename", "Preview_Order_Id", l_lngPreviewOrderID, l_strExistingMediaKey & ".mov"
                                        SetData "BBCMG_Preview_Order", "Tape_Number", "Preview_Order_Id", l_lngPreviewOrderID, frmLibrary.txtBarcode.Text
                                        SetData "BBCMG_Preview_Tracker", "BarCode", "Preview_Order_Id", l_lngPreviewOrderID, frmLibrary.txtBarcode.Text
                                        l_blnFoundOne = True
                                        SetData "BBCMG_Preview_Tracker", "DateTapeArrived", "Preview_Order_Id", l_lngPreviewOrderID, FormatSQLDate(Now)
                                        SetData "BBCMG_Preview_Tracker", "TimeTapeArrived", "Preview_Order_Id", l_lngPreviewOrderID, Format(Now, "HH:NN:SS")
                                        SetData "BBCMG_Preview_Tracker", "DateTapeLeft", "Preview_Order_Id", l_lngPreviewOrderID, Null
                                        SetData "BBCMG_Preview_Tracker", "TimeTapeLeft", "Preview_Order_Id", l_lngPreviewOrderID, Null
                                        SetData "BBCMG_Preview_Tracker", "TapeLocation", "Preview_order_Id", l_lngPreviewOrderID, frmLibrary.cmbLocation.Text
                                        SetData "BBCMG_Preview_Tracker", "TapeShelf", "Preview_Order_Id", l_lngPreviewOrderID, frmLibrary.cmbShelf.Text
                                        ExecuteSQL "exec BBCMG_Tape_Arrived @Preview_Order_ID = " & l_lngPreviewOrderID & ";", g_strExecuteError
                                        'SetData "BBCMG_Preview_Order", "Order_Status_Id", "Preview_Order_Id", l_lngPreviewOrderID, GetData("BBCMG_Order_Status", "Order_Status_Id", "Order_Status", "Tape Arrived")
                                        MsgBox "Tape is required for a BBC MG Preview Order, Request Number: " & GetData("BBCMG_Preview_Order", "Request_Number", "Preview_Order_ID", l_lngPreviewOrderID) & vbCrLf & "SLA: " & _
                                        GetData("BBCMG_SLA", "SLA_Title", "SLA_Id", GetData("BBCMG_Preview_order", "Priority", "Preview_Order_ID", l_lngPreviewOrderID)), vbInformation
                                    Else
                                        l_lngExistingMediaKeyID = GetData("BBCMG_Media_Key", "Media_Key_ID", "Media_ID", l_rstFuzzySearch("Tape_Number"))
                                        If l_lngExistingMediaKeyID <> 0 Then
                                            SetData "BBCMG_Media_Key", "Media_Id", "Media_Key_Id", l_lngExistingMediaKeyID, frmLibrary.txtBarcode.Text
                                            SetData "BBCMG_Preview_Order", "Tape_Number", "Preview_Order_Id", l_lngPreviewOrderID, frmLibrary.txtBarcode.Text
                                            SetData "BBCMG_Preview_Tracker", "BarCode", "Preview_Order_Id", l_lngPreviewOrderID, frmLibrary.txtBarcode.Text
                                            l_blnFoundOne = True
                                            SetData "BBCMG_Preview_Tracker", "DateTapeArrived", "Preview_Order_Id", l_lngPreviewOrderID, FormatSQLDate(Now)
                                            SetData "BBCMG_Preview_Tracker", "TimeTapeArrived", "Preview_Order_Id", l_lngPreviewOrderID, Format(Now, "HH:NN:SS")
                                            SetData "BBCMG_Preview_Tracker", "DateTapeLeft", "Preview_Order_Id", l_lngPreviewOrderID, Null
                                            SetData "BBCMG_Preview_Tracker", "TimeTapeLeft", "Preview_Order_Id", l_lngPreviewOrderID, Null
                                            SetData "BBCMG_Preview_Tracker", "TapeLocation", "Preview_order_Id", l_lngPreviewOrderID, frmLibrary.cmbLocation.Text
                                            SetData "BBCMG_Preview_Tracker", "TapeShelf", "Preview_Order_Id", l_lngPreviewOrderID, frmLibrary.cmbShelf.Text
                                            ExecuteSQL "exec BBCMG_Tape_Arrived @Preview_Order_ID = " & l_lngPreviewOrderID & ";", g_strExecuteError
                                            'SetData "BBCMG_Preview_Order", "Order_Status_Id", "Preview_Order_Id", l_lngPreviewOrderID, GetData("BBCMG_Order_Status", "Order_Status_Id", "Order_Status", "Tape Arrived")
                                            MsgBox "Tape is required for a BBC MG Preview Order, Request Number: " & GetData("BBCMG_Preview_Order", "Request_Number", "Preview_Order_ID", l_lngPreviewOrderID) & vbCrLf & "SLA: " & _
                                            GetData("BBCMG_SLA", "SLA_Title", "SLA_Id", GetData("BBCMG_Preview_order", "Priority", "Preview_Order_ID", l_lngPreviewOrderID)), vbInformation
                                        Else
                                            SetData "BBCMG_Preview_Order", "Tape_Number", "Preview_Order_Id", l_lngPreviewOrderID, frmLibrary.txtBarcode.Text
                                            l_lngExistingMediaKeyID = GetData("BBCMG_Media_Key", "Media_Key_ID", "Media_ID", frmLibrary.txtBarcode.Text)
                                            If l_lngExistingMediaKeyID <> 0 Then
                                                l_strExistingMediaKey = GetData("BBCMG_Media_Key", "Media_Key", "Media_ID", frmLibrary.txtBarcode.Text)
                                                l_strExistingMediaKey = Mid(l_strExistingMediaKey, 1, Len(l_strExistingMediaKey) - 2)
                                            End If
                                            SetData "BBCMG_Preview_Tracker", "ItemReference", "Preview_Order_Id", l_lngPreviewOrderID, l_strExistingMediaKey
                                            SetData "BBCMG_Preview_Tracker", "ItemFilename", "Preview_Order_Id", l_lngPreviewOrderID, l_strExistingMediaKey & ".mov"
                                            SetData "BBCMG_Preview_Tracker", "BarCode", "Preview_Order_Id", l_lngPreviewOrderID, frmLibrary.txtBarcode.Text
                                            l_blnFoundOne = True
                                            SetData "BBCMG_Preview_Tracker", "DateTapeArrived", "Preview_Order_Id", l_lngPreviewOrderID, FormatSQLDate(Now)
                                            SetData "BBCMG_Preview_Tracker", "TimeTapeArrived", "Preview_Order_Id", l_lngPreviewOrderID, Format(Now, "HH:NN:SS")
                                            SetData "BBCMG_Preview_Tracker", "DateTapeLeft", "Preview_Order_Id", l_lngPreviewOrderID, Null
                                            SetData "BBCMG_Preview_Tracker", "TimeTapeLeft", "Preview_Order_Id", l_lngPreviewOrderID, Null
                                            SetData "BBCMG_Preview_Tracker", "TapeLocation", "Preview_order_Id", l_lngPreviewOrderID, frmLibrary.cmbLocation.Text
                                            SetData "BBCMG_Preview_Tracker", "TapeShelf", "Preview_Order_Id", l_lngPreviewOrderID, frmLibrary.cmbShelf.Text
                                            ExecuteSQL "exec BBCMG_Tape_Arrived @Preview_Order_ID = " & l_lngPreviewOrderID & ";", g_strExecuteError
                                            'SetData "BBCMG_Preview_Order", "Order_Status_Id", "Preview_Order_Id", l_lngPreviewOrderID, GetData("BBCMG_Order_Status", "Order_Status_Id", "Order_Status", "Tape Arrived")
                                            MsgBox "Tape is required for a BBC MG Preview Order, Request Number: " & GetData("BBCMG_Preview_Order", "Request_Number", "Preview_Order_ID", l_lngPreviewOrderID) & vbCrLf & "SLA: " & _
                                            GetData("BBCMG_SLA", "SLA_Title", "SLA_Id", GetData("BBCMG_Preview_order", "Priority", "Preview_Order_ID", l_lngPreviewOrderID)), vbInformation
                                        End If
                                    End If
                                Else
                                    l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                                    l_strSQL = l_strSQL & "'" & l_rstFuzzySearch("Tape_Number") & "', "
                                    l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                    l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                    ExecuteSQL l_strSQL, g_strExecuteError
                                    CheckForSQLError
                                End If
                            Else
                                l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                                l_strSQL = l_strSQL & "'" & l_rstFuzzySearch("Tape_Number") & "', "
                                l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                ExecuteSQL l_strSQL, g_strExecuteError
                                CheckForSQLError
                            End If
                            l_rstFuzzySearch.MoveNext
                        Loop
                    End If
                    l_rstFuzzySearch.Close
                End If
                
                'Check for DADC BBC Ingest orders
                
                If GetData("vw_Required_Tapes_DADCBBC", "tracker_dadc_itemID", "newbarcode", frmLibrary.txtBarcode.Text) <> 0 Then
                    l_lngJobDetailID = GetData("vw_Required_Tapes_DADCBBC", "tracker_dadc_itemID", "newbarcode", frmLibrary.txtBarcode.Text)
                    l_blnFoundOne = True
                    SetData "tracker_dadc_item", "stagefield1", "tracker_dadc_itemID", l_lngJobDetailID, FormatSQLDate(Now)
                    SetData "tracker_dadc_item", "stagefield14", "tracker_dadc_itemID", l_lngJobDetailID, Null
                    SetData "tracker_dadc_item", "Completeable", "tracker_dadc_itemID", l_lngJobDetailID, FormatSQLDate(Now)
                    If GetData("tracker_dadc_item", "firstcompleteable", "tracker_dadc_itemID", l_lngJobDetailID) = 0 Then SetData "tracker_dadc_item", "FirstCompleteable", "tracker_dadc_itemID", l_lngJobDetailID, FormatSQLDate(Now)
                    Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & GetData("tracker_dadc_item", "TargetDateCountdown", "tracker_dadc_itemID", l_lngJobDetailID), g_strExecuteError)
                    SetData "tracker_dadc_item", "Targetdate", "tracker_dadc_itemID", l_lngJobDetailID, FormatSQLDate(l_rst(0))
                    MsgBox "Tape is required for a DADC Item, CV Code: " & GetData("tracker_dadc_item", "contentversioncode", "tracker_dadc_itemID", l_lngJobDetailID) & ", Target Date: " & Format(l_rst(0), "YYYY-MM-DD HH:NN;SS"), vbInformation
                    l_rst.Close
                Else
                    If IsNumeric(Right(frmLibrary.txtBarcode.Text, 1)) Then
                        l_strSQL = "SELECT tracker_dadc_itemID FROM vw_Required_Tapes_DADCBBC WHERE newbarcode LIKE '" & Left(frmLibrary.txtBarcode.Text, 1) & "%" & Right(frmLibrary.txtBarcode.Text, 4) & "';"
                    Else
                        l_strSQL = "SELECT tracker_dadc_itemID FROM vw_Required_Tapes_DADCBBC WHERE newbarcode LIKE '" & Left(frmLibrary.txtBarcode.Text, 1) & "%" & Right(frmLibrary.txtBarcode.Text, 5) & "';"
                    End If
                    Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
                    If l_rst.RecordCount > 0 Then
                        l_rst.MoveFirst
                        Do While Not l_rst.EOF
                            l_lngJobDetailID = l_rst("tracker_dadc_itemID")
                            l_blnFoundOne = True
                            l_strMessage = "Is DADC Item " & GetData("tracker_dadc_item", "newbarcode", "tracker_dadc_itemID", l_lngJobDetailID) & " - " & GetData("tracker_dadc_item", "title", "tracker_dadc_itemID", l_lngJobDetailID) & " Ep. " & GetData("tracker_dadc_item", "episode", "tracker_dadc_itemID", l_lngJobDetailID)
                            l_strMessage = l_strMessage & vbCrLf & "likely to be a match for this tape: " & frmLibrary.txtBarcode.Text & " - " & frmLibrary.txtTitle.Text
                            If MsgBox(l_strMessage, vbYesNo + vbDefaultButton2, "Possible Fuzzy Search Tape Found") = vbYes Then
                                If MsgBox(l_strMessage, vbYesNo + vbDefaultButton2, "Please Confirm Fuzzy Search Tape Found") = vbYes Then
                                    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", GetData("tracker_dadc_item", "newbarcode", "tracker_dadc_itemID", l_lngJobDetailID)) = 0 Then
                                        l_strSQL = "INSERT INTO bbc_barcode_correction (original_barcode, corrected_barcode, created_by) VALUES ("
                                        l_strSQL = l_strSQL & "'" & GetData("tracker_dadc_item", "newbarcode", "tracker_dadc_itemID", l_lngJobDetailID) & "', "
                                        l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                        l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                        ExecuteSQL l_strSQL, g_strExecuteError
                                        CheckForSQLError
                                    End If
                                    SetData "tracker_dadc_item", "newbarcode", "tracker_dadc_itemID", l_lngJobDetailID, frmLibrary.txtBarcode.Text
                                    SetData "tracker_dadc_item", "stagefield1", "tracker_dadc_itemID", l_lngJobDetailID, FormatSQLDate(Now)
                                    SetData "tracker_dadc_item", "stagefield14", "tracker_dadc_itemID", l_lngJobDetailID, Null
                                    SetData "tracker_dadc_item", "Completeable", "tracker_dadc_itemID", l_lngJobDetailID, FormatSQLDate(Now)
                                    If GetData("tracker_dadc_item", "firstcompleteable", "tracker_dadc_itemID", l_lngJobDetailID) = 0 Then SetData "tracker_dadc_item", "FirstCompleteable", "tracker_dadc_itemID", l_lngJobDetailID, FormatSQLDate(Now)
                                    Set l_rst2 = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & GetData("tracker_dadc_item", "TargetDateCountdown", "tracker_dadc_itemID", l_lngJobDetailID), g_strExecuteError)
                                    SetData "tracker_dadc_item", "Targetdate", "tracker_dadc_itemID", l_lngJobDetailID, FormatSQLDate(l_rst2(0))
                                    MsgBox "Tape is required for a DADC Item, CV Code: " & GetData("tracker_dadc_item", "contentversioncode", "tracker_dadc_itemID", l_lngJobDetailID) & ", Target Date: " & Format(l_rst2(0), "YYYY-MM-DD HH:NN;SS"), vbInformation
                                    l_rst2.Close
                                Else
                                    l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                                    l_strSQL = l_strSQL & "'" & GetData("tracker_dadc_item", "newbarcode", "tracker_dadc_itemID", l_lngJobDetailID) & "', "
                                    l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                    l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                    ExecuteSQL l_strSQL, g_strExecuteError
                                    CheckForSQLError
                                End If
                            Else
                                l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                                l_strSQL = l_strSQL & "'" & GetData("tracker_dadc_item", "newbarcode", "tracker_dadc_itemID", l_lngJobDetailID) & "', "
                                l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                ExecuteSQL l_strSQL, g_strExecuteError
                                CheckForSQLError
                            End If
                            l_rst.MoveNext
                        Loop
                    End If
                    l_rst.Close
                End If
                
                'Check for Issues Tracker items SourceID ...
                If GetData("vw_Required_Tapes_DADCCheck_ID1", "ComplaintID", "Barcode", frmLibrary.txtBarcode.Text) <> 0 Then
                    l_lngJobDetailID = GetData("vw_Required_Tapes_DADCCheck_ID1", "ComplaintID", "Barcode", frmLibrary.txtBarcode.Text)
                    SetData "complaint", "DateMasterArrived", "complaintID", l_lngJobDetailID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                    If CheckAllComplaintMastersHere(GetData("complaint", "MasterSourceID", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived", "complaintID", l_lngJobDetailID, True), _
                    GetData("complaint", "MasterSourceID2", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived2", "complaintID", l_lngJobDetailID, True), _
                    GetData("complaint", "MasterSourceID3", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived3", "complaintID", l_lngJobDetailID, True)) Then
                        If IsNull(GetData("complaint", "TargetDate", "complaintID", l_lngJobDetailID, True)) Then
                            SetData "complaint", "TargetDate", "complaintID", l_lngJobDetailID, Format(GetNewTargetDate, "YYYY-MM-DD HH:NN:SS")
                        End If
                    Else
                        SetData "complaint", "TargetDate", "complaintID", l_lngJobDetailID, Null
                    End If
                    MsgBox "Tape is required for a DADC Issue Check, ComplaintID: " & l_lngJobDetailID & ", Target Date: " & GetData("complaint", "TargetDate", "complaintID", l_lngJobDetailID), vbInformation
                Else
                    If IsNumeric(Right(frmLibrary.txtBarcode.Text, 1)) Then
                        l_strSQL = "SELECT ComplaintID FROM vw_Required_Tapes_DADCCheck_ID1 WHERE barcode LIKE '" & Left(frmLibrary.txtBarcode.Text, 1) & "%" & Right(frmLibrary.txtBarcode.Text, 4) & "';"
                    Else
                        l_strSQL = "SELECT ComplaintID FROM vw_Required_Tapes_DADCCheck_ID1 WHERE barcode LIKE '" & Left(frmLibrary.txtBarcode.Text, 1) & "%" & Right(frmLibrary.txtBarcode.Text, 5) & "';"
                    End If
                    Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
                    If l_rst.RecordCount > 0 Then
                        l_rst.MoveFirst
                        Do While Not l_rst.EOF
                            l_lngJobDetailID = l_rst("ComplaintID")
                            l_strMessage = "Is Issues Check RequiredMedia Item " & GetData("complaint", "MasterSourceID", "ComplaintID", l_lngJobDetailID) & " - '" & GetData("complaint", "title", "complaintID", l_lngJobDetailID)
                            l_strMessage = l_strMessage & vbCrLf & "' likely to be a match for this tape: " & frmLibrary.txtBarcode.Text & " - " & frmLibrary.txtTitle.Text
                            If MsgBox(l_strMessage, vbYesNo + vbDefaultButton2, "Possible Fuzzy Search Tape Found") = vbYes Then
                                If MsgBox(l_strMessage, vbYesNo + vbDefaultButton2, "Pleave Confirm Fuzzy Search Tape Found") = vbYes Then
                                    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", GetData("complaint", "MasterScourceID", "ComplaintID", l_lngJobDetailID)) = 0 Then
                                        l_strSQL = "INSERT INTO bbc_barcode_correction (original_barcode, corrected_barcode, created_by) VALUES ("
                                        l_strSQL = l_strSQL & "'" & GetData("complaint", "MasterScourceID", "ComplaintID", l_lngJobDetailID) & "', "
                                        l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                        l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                        ExecuteSQL l_strSQL, g_strExecuteError
                                        CheckForSQLError
                                    End If
                                    SetData "complaint", "MasterScourceID", "complaintID", l_lngJobDetailID, frmLibrary.txtBarcode.Text
                                    SetData "complaint", "DateMasterArrived", "complaintID", l_lngJobDetailID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                                    If CheckAllComplaintMastersHere(GetData("complaint", "MasterSourceID", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived", "complaintID", l_lngJobDetailID, True), _
                                    GetData("complaint", "MasterSourceID2", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived2", "complaintID", l_lngJobDetailID, True), _
                                    GetData("complaint", "MasterSourceID3", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived3", "complaintID", l_lngJobDetailID, True)) Then
                                        If IsNull(GetData("complaint", "TargetDate", "complaintID", l_lngJobDetailID, True)) Then
                                            SetData "complaint", "TargetDate", "complaintID", l_lngJobDetailID, Format(GetNewTargetDate, "YYYY-MM-DD HH:NN:SS")
                                        End If
                                    Else
                                        SetData "complaint", "TargetDate", "complaintID", l_lngJobDetailID, Null
                                    End If
                                    MsgBox "Tape is required for a DADC Issue Check, ComplaintID: " & l_lngJobDetailID & ", Target Date: " & GetData("complaint", "TargetDate", "complaintID", l_lngJobDetailID), vbInformation
                                Else
                                    l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                                    l_strSQL = l_strSQL & "'" & GetData("complaint", "MasterScourceID", "ComplaintID", l_lngJobDetailID) & "', "
                                    l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                    l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                    ExecuteSQL l_strSQL, g_strExecuteError
                                    CheckForSQLError
                                End If
                            Else
                                l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                                l_strSQL = l_strSQL & "'" & GetData("complaint", "MasterScourceID", "ComplaintID", l_lngJobDetailID) & "', "
                                l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                ExecuteSQL l_strSQL, g_strExecuteError
                                CheckForSQLError
                            End If
                            l_rst.MoveNext
                        Loop
                    End If
                End If
            
                'Check for Issues Tracker items SourceID2 ...
                If GetData("vw_Required_Tapes_DADCCheck_ID2", "ComplaintID", "Barcode", frmLibrary.txtBarcode.Text) <> 0 Then
                    l_lngJobDetailID = GetData("vw_Required_Tapes_DADCCheck_ID2", "ComplaintID", "Barcode", frmLibrary.txtBarcode.Text)
                    SetData "complaint", "DateMasterArrived2", "complaintID", l_lngJobDetailID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                    If CheckAllComplaintMastersHere(GetData("complaint", "MasterSourceID", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived", "complaintID", l_lngJobDetailID, True), _
                    GetData("complaint", "MasterSourceID2", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived2", "complaintID", l_lngJobDetailID, True), _
                    GetData("complaint", "MasterSourceID3", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived3", "complaintID", l_lngJobDetailID, True)) Then
                        If IsNull(GetData("complaint", "TargetDate", "complaintID", l_lngJobDetailID, True)) Then
                            SetData "complaint", "TargetDate", "complaintID", l_lngJobDetailID, Format(GetNewTargetDate, "YYYY-MM-DD HH:NN:SS")
                        End If
                    Else
                        SetData "complaint", "TargetDate", "complaintID", l_lngJobDetailID, Null
                    End If
                    MsgBox "Tape is required for a DADC Issue Check, ComplaintID: " & l_lngJobDetailID & ", Target Date: " & GetData("complaint", "TargetDate", "complaintID", l_lngJobDetailID), vbInformation
                Else
                    If IsNumeric(Right(frmLibrary.txtBarcode.Text, 1)) Then
                        l_strSQL = "SELECT ComplaintID FROM vw_Required_Tapes_DADCCheck_ID2 WHERE barcode LIKE '" & Left(frmLibrary.txtBarcode.Text, 1) & "%" & Right(frmLibrary.txtBarcode.Text, 4) & "';"
                    Else
                        l_strSQL = "SELECT ComplaintID FROM vw_Required_Tapes_DADCCheck_ID2 WHERE barcode LIKE '" & Left(frmLibrary.txtBarcode.Text, 1) & "%" & Right(frmLibrary.txtBarcode.Text, 5) & "';"
                    End If
                    Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
                    If l_rst.RecordCount > 0 Then
                        l_rst.MoveFirst
                        Do While Not l_rst.EOF
                            l_lngJobDetailID = l_rst("ComplaintID")
                            l_strMessage = "Is Issues Check RequiredMedia Item " & GetData("complaint", "MasterSourceID2", "ComplaintID", l_lngJobDetailID) & " - '" & GetData("complaint", "title", "complaintID", l_lngJobDetailID)
                            l_strMessage = l_strMessage & vbCrLf & "' likely to be a match for this tape: " & frmLibrary.txtBarcode.Text & " - " & frmLibrary.txtTitle.Text
                            If MsgBox(l_strMessage, vbYesNo + vbDefaultButton2, "Possible Fuzzy Search Tape Found") = vbYes Then
                                If MsgBox(l_strMessage, vbYesNo + vbDefaultButton2, "Please confirm Fuzzy Search Tape Found") = vbYes Then
                                    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", GetData("complaint", "MasterScourceID2", "ComplaintID", l_lngJobDetailID)) = 0 Then
                                        l_strSQL = "INSERT INTO bbc_barcode_correction (original_barcode, corrected_barcode, created_by) VALUES ("
                                        l_strSQL = l_strSQL & "'" & GetData("complaint", "MasterScourceID2", "ComplaintID", l_lngJobDetailID) & "', "
                                        l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                        l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                        ExecuteSQL l_strSQL, g_strExecuteError
                                        CheckForSQLError
                                    End If
                                    SetData "complaint", "MasterScourceID2", "complaintID", l_lngJobDetailID, frmLibrary.txtBarcode.Text
                                    SetData "complaint", "DateMasterArrived2", "complaintID", l_lngJobDetailID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                                    If CheckAllComplaintMastersHere(GetData("complaint", "MasterSourceID", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived", "complaintID", l_lngJobDetailID, True), _
                                    GetData("complaint", "MasterSourceID2", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived2", "complaintID", l_lngJobDetailID, True), _
                                    GetData("complaint", "MasterSourceID3", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived3", "complaintID", l_lngJobDetailID, True)) Then
                                        If IsNull(GetData("complaint", "TargetDate", "complaintID", l_lngJobDetailID, True)) Then
                                            SetData "complaint", "TargetDate", "complaintID", l_lngJobDetailID, Format(GetNewTargetDate, "YYYY-MM-DD HH:NN:SS")
                                        End If
                                    Else
                                        SetData "complaint", "TargetDate", "complaintID", l_lngJobDetailID, Null
                                    End If
                                    MsgBox "Tape is required for a DADC Issue Check, ComplaintID: " & l_lngJobDetailID & ", Target Date: " & GetData("complaint", "TargetDate", "complaintID", l_lngJobDetailID), vbInformation
                                Else
                                    l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                                    l_strSQL = l_strSQL & "'" & GetData("complaint", "MasterScourceID2", "ComplaintID", l_lngJobDetailID) & "', "
                                    l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                    l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                    ExecuteSQL l_strSQL, g_strExecuteError
                                    CheckForSQLError
                                End If
                            Else
                                l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                                l_strSQL = l_strSQL & "'" & GetData("complaint", "MasterScourceID2", "ComplaintID", l_lngJobDetailID) & "', "
                                l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                ExecuteSQL l_strSQL, g_strExecuteError
                                CheckForSQLError
                            End If
                            l_rst.MoveNext
                        Loop
                    End If
                End If
            
                'Check for Issues Tracker items SourceID3 ...
                If GetData("vw_Required_Tapes_DADCCheck_ID3", "ComplaintID", "Barcode", frmLibrary.txtBarcode.Text) <> 0 Then
                    l_lngJobDetailID = GetData("vw_Required_Tapes_DADCCheck_ID3", "ComplaintID", "Barcode", frmLibrary.txtBarcode.Text)
                    SetData "complaint", "DateMasterArrived3", "complaintID", l_lngJobDetailID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                    If CheckAllComplaintMastersHere(GetData("complaint", "MasterSourceID", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived", "complaintID", l_lngJobDetailID, True), _
                    GetData("complaint", "MasterSourceID2", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived2", "complaintID", l_lngJobDetailID, True), _
                    GetData("complaint", "MasterSourceID3", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived3", "complaintID", l_lngJobDetailID, True)) Then
                        If IsNull(GetData("complaint", "TargetDate", "complaintID", l_lngJobDetailID, True)) Then
                            SetData "complaint", "TargetDate", "complaintID", l_lngJobDetailID, Format(GetNewTargetDate, "YYYY-MM-DD HH:NN:SS")
                        End If
                    Else
                        SetData "complaint", "TargetDate", "complaintID", l_lngJobDetailID, Null
                    End If
                    MsgBox "Tape is required for a DADC Issue Check, ComplaintID: " & l_lngJobDetailID & ", Target Date: " & GetData("complaint", "TargetDate", "complaintID", l_lngJobDetailID), vbInformation
                Else
                    If IsNumeric(Right(frmLibrary.txtBarcode.Text, 1)) Then
                        l_strSQL = "SELECT ComplaintID FROM vw_Required_Tapes_DADCCheck_ID3 WHERE barcode LIKE '" & Left(frmLibrary.txtBarcode.Text, 1) & "%" & Right(frmLibrary.txtBarcode.Text, 4) & "';"
                    Else
                        l_strSQL = "SELECT ComplaintID FROM vw_Required_Tapes_DADCCheck_ID3 WHERE barcode LIKE '" & Left(frmLibrary.txtBarcode.Text, 1) & "%" & Right(frmLibrary.txtBarcode.Text, 5) & "';"
                    End If
                    Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
                    If l_rst.RecordCount > 0 Then
                        l_rst.MoveFirst
                        Do While Not l_rst.EOF
                            l_lngJobDetailID = l_rst("ComplaintID")
                            l_strMessage = "Is Issues Check RequiredMedia Item " & GetData("complaint", "MasterSourceID3", "ComplaintID", l_lngJobDetailID) & " - '" & GetData("complaint", "title", "complaintID", l_lngJobDetailID)
                            l_strMessage = l_strMessage & vbCrLf & "' likely to be a match for this tape: " & frmLibrary.txtBarcode.Text & " - " & frmLibrary.txtTitle.Text
                            If MsgBox(l_strMessage, vbYesNo, "Possible Fuzzy Search Tape Found") = vbYes Then
                                If MsgBox(l_strMessage, vbYesNo, "Please Vonfirm Fuzzy Search Tape Found") = vbYes Then
                                    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", GetData("complaint", "MasterScourceID3", "ComplaintID", l_lngJobDetailID)) = 0 Then
                                        l_strSQL = "INSERT INTO bbc_barcode_correction (original_barcode, corrected_barcode, created_by) VALUES ("
                                        l_strSQL = l_strSQL & "'" & GetData("complaint", "MasterSourceID3", "ComplaintID", l_lngJobDetailID) & "', "
                                        l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                        l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                        ExecuteSQL l_strSQL, g_strExecuteError
                                        CheckForSQLError
                                    End If
                                    SetData "complaint", "MasterScourceID3", "complaintID", l_lngJobDetailID, frmLibrary.txtBarcode.Text
                                    SetData "complaint", "DateMasterArrived3", "complaintID", l_lngJobDetailID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                                    If CheckAllComplaintMastersHere(GetData("complaint", "MasterSourceID", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived", "complaintID", l_lngJobDetailID, True), _
                                    GetData("complaint", "MasterSourceID2", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived2", "complaintID", l_lngJobDetailID, True), _
                                    GetData("complaint", "MasterSourceID3", "complaintID", l_lngJobDetailID), GetData("complaint", "DateMasterArrived3", "complaintID", l_lngJobDetailID, True)) Then
                                        If IsNull(GetData("complaint", "TargetDate", "complaintID", l_lngJobDetailID, True)) Then
                                            SetData "complaint", "TargetDate", "complaintID", l_lngJobDetailID, Format(GetNewTargetDate, "YYYY-MM-DD HH:NN:SS")
                                        End If
                                    Else
                                        SetData "complaint", "TargetDate", "complaintID", l_lngJobDetailID, Null
                                    End If
                                    MsgBox "Tape is required for a DADC Issue Check, ComplaintID: " & l_lngJobDetailID & ", Target Date: " & GetData("complaint", "TargetDate", "complaintID", l_lngJobDetailID), vbInformation
                                Else
                                    l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                                    l_strSQL = l_strSQL & "'" & GetData("complaint", "MasterSourceID3", "ComplaintID", l_lngJobDetailID) & "', "
                                    l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                    l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                    ExecuteSQL l_strSQL, g_strExecuteError
                                    CheckForSQLError
                                End If
                            Else
                                l_strSQL = "INSERT INTO bbc_barcode_noncorrection (original_barcode, offered_barcode, created_by) VALUES ("
                                l_strSQL = l_strSQL & "'" & GetData("complaint", "MasterSourceID3", "ComplaintID", l_lngJobDetailID) & "', "
                                l_strSQL = l_strSQL & "'" & UCase(frmLibrary.txtBarcode.Text) & "', "
                                l_strSQL = l_strSQL & "'" & g_strUserName & "');"
                                ExecuteSQL l_strSQL, g_strExecuteError
                                CheckForSQLError
                            End If
                            l_rst.MoveNext
                        Loop
                    End If
                End If
            End If
        End If
    End If
    
    'Finally save the tape
    With frmLibrary
        
        If Val(.lblLibraryID.Caption) = 0 Then
            l_strSQL = "INSERT INTO library ("
            l_strSQL = l_strSQL & "projectID, "
            l_strSQL = l_strSQL & "projectnumber, "
            l_strSQL = l_strSQL & "cdate, "
            l_strSQL = l_strSQL & "cuser, "
            l_strSQL = l_strSQL & "barcode, "
            l_strSQL = l_strSQL & "code128barcode, "
            l_strSQL = l_strSQL & "foreignref, "
            l_strSQL = l_strSQL & "jobID, "
            l_strSQL = l_strSQL & "location, "
            l_strSQL = l_strSQL & "locationjobID, "
            l_strSQL = l_strSQL & "shelf, "
            l_strSQL = l_strSQL & "box, "
            l_strSQL = l_strSQL & "copytype, "
            l_strSQL = l_strSQL & "companyname, "
            l_strSQL = l_strSQL & "companyID, "
            l_strSQL = l_strSQL & "productname, "
            l_strSQL = l_strSQL & "productID, "
            l_strSQL = l_strSQL & "title, "
            l_strSQL = l_strSQL & "seriesID, "
            l_strSQL = l_strSQL & "serialnumber, "
            l_strSQL = l_strSQL & "subtitle, "
            l_strSQL = l_strSQL & "version,"
            l_strSQL = l_strSQL & "stocktype, "
            l_strSQL = l_strSQL & "stockmanufacturer, "
            l_strSQL = l_strSQL & "sourcereference, "
            l_strSQL = l_strSQL & "sourcemachine, "
            l_strSQL = l_strSQL & "sourceformat, "
            l_strSQL = l_strSQL & "sourcevideostandard, "
            l_strSQL = l_strSQL & "sourceaspectratio, "
            l_strSQL = l_strSQL & "sourcegeometriclinearity, "
            l_strSQL = l_strSQL & "recordmachine, "
            l_strSQL = l_strSQL & "format, "
            l_strSQL = l_strSQL & "videostandard, "
            l_strSQL = l_strSQL & "aspectratio, "
            l_strSQL = l_strSQL & "geometriclinearity, "
            l_strSQL = l_strSQL & "audiostandard, "
            l_strSQL = l_strSQL & "ch1, "
            l_strSQL = l_strSQL & "ch2, "
            l_strSQL = l_strSQL & "ch3, "
            l_strSQL = l_strSQL & "ch4, "
            l_strSQL = l_strSQL & "ch5, "
            l_strSQL = l_strSQL & "ch6, "
            l_strSQL = l_strSQL & "ch7, "
            l_strSQL = l_strSQL & "ch8, "
            l_strSQL = l_strSQL & "ch9, "
            l_strSQL = l_strSQL & "ch10, "
            l_strSQL = l_strSQL & "ch11, "
            l_strSQL = l_strSQL & "ch12, "
'            l_strSQL = l_strSQL & "nr1, "
'            l_strSQL = l_strSQL & "nr2, "
'            l_strSQL = l_strSQL & "nr3, "
'            l_strSQL = l_strSQL & "nr4, "
'            l_strSQL = l_strSQL & "nr5, "
'            l_strSQL = l_strSQL & "nr6, "
'            l_strSQL = l_strSQL & "nr7, "
'            l_strSQL = l_strSQL & "nr8, "
'            l_strSQL = l_strSQL & "nr9, "
'            l_strSQL = l_strSQL & "nr10, "
'            l_strSQL = l_strSQL & "nr11, "
'            l_strSQL = l_strSQL & "nr12, "
            l_strSQL = l_strSQL & "cuetrack, "
            l_strSQL = l_strSQL & "series, "
            l_strSQL = l_strSQL & "episode, "
            l_strSQL = l_strSQL & "endepisode, "
            l_strSQL = l_strSQL & "seriesset, "
            l_strSQL = l_strSQL & "flagincomplete, "
            l_strSQL = l_strSQL & "flagfalserecord, "
            
'            l_strSQL = l_strSQL & "sound_stereo_main, "
'            l_strSQL = l_strSQL & "sound_stereo_me, "
'            l_strSQL = l_strSQL & "sound_surround_main, "
'            l_strSQL = l_strSQL & "sound_surround_me, "
'            l_strSQL = l_strSQL & "sound_stereo_mmn, "
'            l_strSQL = l_strSQL & "sound_surround_mmn, "
'            l_strSQL = l_strSQL & "sound_ltrt_main, "
'            l_strSQL = l_strSQL & "sound_ltrt_me, "
            l_strSQL = l_strSQL & "textless, "
            l_strSQL = l_strSQL & "customfield4, "
            
            l_strSQL = l_strSQL & "timecode, "
            l_strSQL = l_strSQL & "progduration, "
            l_strSQL = l_strSQL & "totalduration, "
            l_strSQL = l_strSQL & "notes1, "
            l_strSQL = l_strSQL & "modate, "
            l_strSQL = l_strSQL & "jobdetailID, "
            l_strSQL = l_strSQL & "reference, "
            l_strSQL = l_strSQL & "customfield1, "
            l_strSQL = l_strSQL & "customfield2, "
            l_strSQL = l_strSQL & "customfield3, "
            l_strSQL = l_strSQL & "MinimumFreePercentage, "
            l_strSQL = l_strSQL & "CanBeUsedForAutoDelete, "
            l_strSQL = l_strSQL & "AllowToUseForLiveCFM, "
            l_strSQL = l_strSQL & "NoRestores, "
            l_strSQL = l_strSQL & "RequireCompanyIDinFolders, "
            l_strSQL = l_strSQL & "DriveWritePermissionInteger, "
            l_strSQL = l_strSQL & "internalreference) VALUES ("
            
            'now the values
            l_strSQL = l_strSQL & "'" & GetData("project", "projectID", "projectnumber", .txtProjectNumber.Text) & "', "
            l_strSQL = l_strSQL & "'" & .txtProjectNumber.Text & "', "
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
            l_strSQL = l_strSQL & "'" & .txtBarcode.Text & "', "
            l_strSQL = l_strSQL & "'" & CIA_CODE128(frmLibrary.txtBarcode.Text) & "', " ' create 128 barcode
            l_strSQL = l_strSQL & "'" & .txtForeignRef.Text & "', "
            l_strSQL = l_strSQL & "'" & .txtJobID.Text & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbLocation.Text) & "', "
            If .txtLocationJobID.Text <> "" Then
                l_strSQL = l_strSQL & "'" & .txtLocationJobID.Text & "', "
            Else
                l_strSQL = l_strSQL & "Null, "
            End If
            l_strSQL = l_strSQL & "'" & .cmbShelf.Text & "', "
            l_strSQL = l_strSQL & "'" & .cmbBox.Text & "', "
            l_strSQL = l_strSQL & "'" & .cmbCopyType.Text & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbCompany.Text) & "', "
            l_strSQL = l_strSQL & "'" & .lblCompanyID.Caption & "', "
            l_strSQL = l_strSQL & "Null, "
            l_strSQL = l_strSQL & "Null, "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.txtTitle.Text) & "', "
            If .txtSeriesID.Text <> "" Then
                l_strSQL = l_strSQL & "'" & .txtSeriesID.Text & "', "
            Else
                l_strSQL = l_strSQL & "Null, "
            End If
            If .txtSerialNumber.Text <> "" Then
                l_strSQL = l_strSQL & "'" & .txtSerialNumber.Text & "', "
            Else
                l_strSQL = l_strSQL & "Null, "
            End If
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.txtSubTitle.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbVersion.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbStockCode.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbStockManufacturer.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.txtSourceBarcode.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbSourceMachine.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbSourceFormat.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbSourceStandard.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbSourceRatio.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbSourceGeometry.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbRecordMachine.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbRecordFormat.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbRecordStandard.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbRecordRatio.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbRecordGeometry.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbAudioStandard.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbChannel1.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbChannel2.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbChannel3.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbChannel4.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbChannel5.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbChannel6.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbChannel7.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbChannel8.Text) & "', "
            
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbChannel9.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbChannel10.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbChannel11.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbChannel12.Text) & "', "
            
'            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbNoiseReduction1.Text) & "', "
'            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbNoiseReduction2.Text) & "', "
'            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbNoiseReduction3.Text) & "', "
'            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbNoiseReduction4.Text) & "', "
'            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbNoiseReduction5.Text) & "', "
'            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbNoiseReduction6.Text) & "', "
'            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbNoiseReduction7.Text) & "', "
'            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbNoiseReduction8.Text) & "', "
'
'            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbNoiseReduction9.Text) & "', "
'            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbNoiseReduction10.Text) & "', "
'            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbNoiseReduction11.Text) & "', "
'            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbNoiseReduction12.Text) & "', "
            
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbChannelCue.Text) & "', "
            
            
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbSeries.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbEpisode.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbEpisodeTo.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbSet.Text) & "', "
            l_strSQL = l_strSQL & "'" & .chkTapeIncomplete.Value & "', "
            l_strSQL = l_strSQL & "'" & .chkFalseRecord.Value & "', "
            
'            l_strSQL = l_strSQL & "'" & .chkStereoMain.Value & "', "
'            l_strSQL = l_strSQL & "'" & .chkStereoME.Value & "', "
'            l_strSQL = l_strSQL & "'" & .chkSurroundMain.Value & "', "
'            l_strSQL = l_strSQL & "'" & .chkSurroundME.Value & "', "
'            l_strSQL = l_strSQL & "'" & .chkStereoMMN.Value & "', "
'            l_strSQL = l_strSQL & "'" & .chkSurroundMMN.Value & "', "
'            l_strSQL = l_strSQL & "'" & .chkLtRtMain.Value & "', "
'            l_strSQL = l_strSQL & "'" & .chkLtRtME.Value & "', "
            l_strSQL = l_strSQL & "'" & .chkTapeTextless.Value & "', "
            If .chkIncludeInStorageReport.Value <> 0 Then
                l_strSQL = l_strSQL & "'In_DailyStorageReport', "
            Else
                l_strSQL = l_strSQL & "Null, "
            End If
            
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbTimeCode.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.txtProgDuration.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.txtTotalDuration.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.txtNotes.Text) & "', "
            If Not IsNull(.datMadeOnDate.Value) Then
                l_strSQL = l_strSQL & "'" & FormatSQLDate(.datMadeOnDate.Value) & "', "
            Else
                l_strSQL = l_strSQL & " Null, "
            End If
            l_strSQL = l_strSQL & "'" & .txtJobDetailID.Text & "', "
            l_strSQL = l_strSQL & "'" & .txtReference.Text & "', "
            l_strSQL = l_strSQL & "'" & .cmbField1.Text & "', "
            l_strSQL = l_strSQL & "'" & .cmbField2.Text & "', "
            l_strSQL = l_strSQL & "'" & .cmbField3.Text & "', "
            l_strSQL = l_strSQL & Int(Val(.txtMinimumFreePercentage.Text)) & ", "
            l_strSQL = l_strSQL & "'" & .chkCanBeUsedForAutoDelete.Value & "', "
            l_strSQL = l_strSQL & "'" & .chkUseForLiveCFM.Value & "', "
            l_strSQL = l_strSQL & "'" & .chkNoRestores.Value & "', "
            l_strSQL = l_strSQL & "'" & .chkRequireCompanyIDinFolders.Value & "', "
            l_strSQL = l_strSQL & Int(Val(.txtDriveWriterPermissionInteger.Text)) & ", "
            l_strSQL = l_strSQL & "'" & GetNextSequence("internalreference") & "')"
        
        Else
            
            l_strSQL = "UPDATE library SET "
            l_strSQL = l_strSQL & "code128barcode = '" & CIA_CODE128(frmLibrary.txtBarcode.Text) & "', " ' create 128 barcode
            l_strSQL = l_strSQL & "projectID = '" & GetData("project", "projectID", "projectnumber", .txtProjectNumber.Text) & "', "
            l_strSQL = l_strSQL & "projectnumber = '" & .txtProjectNumber.Text & "', "
            l_strSQL = l_strSQL & "mdate = '" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "muser = '" & g_strUserInitials & "', "
            l_strSQL = l_strSQL & "foreignref = '" & QuoteSanitise(.txtForeignRef.Text) & "', "
            l_strSQL = l_strSQL & "jobID = '" & .txtJobID.Text & "', "
            l_strSQL = l_strSQL & "location = '" & QuoteSanitise(.cmbLocation.Text) & "', "
            If .txtLocationJobID.Text <> "" Then
                l_strSQL = l_strSQL & "locationjobid = '" & .txtLocationJobID.Text & "', "
            Else
                l_strSQL = l_strSQL & "locationjobid = Null, "
            End If
            l_strSQL = l_strSQL & "shelf = '" & QuoteSanitise(.cmbShelf.Text) & "', "
            l_strSQL = l_strSQL & "box = '" & QuoteSanitise(.cmbBox.Text) & "', "
            l_strSQL = l_strSQL & "copytype = '" & QuoteSanitise(.cmbCopyType.Text) & "', "
            l_strSQL = l_strSQL & "companyname = '" & QuoteSanitise(.cmbCompany.Text) & "', "
            l_strSQL = l_strSQL & "companyID = '" & .lblCompanyID.Caption & "', "
            l_strSQL = l_strSQL & "productname = Null, "
            l_strSQL = l_strSQL & "productID = Null, "
            l_strSQL = l_strSQL & "title = '" & QuoteSanitise(.txtTitle.Text) & "', "
            If .txtSeriesID.Text <> "" Then
                l_strSQL = l_strSQL & "seriesID = '" & .txtSeriesID.Text & "', "
            Else
                l_strSQL = l_strSQL & "Seriesid = Null, "
            End If
            If .txtSerialNumber.Text <> "" Then
                l_strSQL = l_strSQL & "serialnumber = '" & .txtSerialNumber.Text & "', "
            Else
                l_strSQL = l_strSQL & "serialnumber = Null, "
            End If
            l_strSQL = l_strSQL & "subtitle = '" & QuoteSanitise(.txtSubTitle.Text) & "', "
            l_strSQL = l_strSQL & "version = '" & QuoteSanitise(.cmbVersion.Text) & "', "
            l_strSQL = l_strSQL & "stocktype = '" & QuoteSanitise(.cmbStockCode.Text) & "', "
            l_strSQL = l_strSQL & "stockmanufacturer = '" & QuoteSanitise(.cmbStockManufacturer.Text) & "', "
            l_strSQL = l_strSQL & "sourcereference = '" & QuoteSanitise(.txtSourceBarcode.Text) & "', "
            l_strSQL = l_strSQL & "sourcemachine = '" & QuoteSanitise(.cmbSourceMachine.Text) & "', "
            l_strSQL = l_strSQL & "sourcevideostandard = '" & QuoteSanitise(.cmbSourceStandard.Text) & "', "
            l_strSQL = l_strSQL & "sourceformat = '" & QuoteSanitise(.cmbSourceFormat.Text) & "', "
            l_strSQL = l_strSQL & "sourceaspectratio = '" & QuoteSanitise(.cmbSourceRatio.Text) & "', "
            l_strSQL = l_strSQL & "sourcegeometriclinearity = '" & QuoteSanitise(.cmbSourceGeometry.Text) & "', "
            l_strSQL = l_strSQL & "recordmachine = '" & QuoteSanitise(.cmbRecordMachine.Text) & "', "
            l_strSQL = l_strSQL & "format = '" & QuoteSanitise(.cmbRecordFormat.Text) & "', "
            l_strSQL = l_strSQL & "videostandard = '" & QuoteSanitise(.cmbRecordStandard.Text) & "', "
            l_strSQL = l_strSQL & "aspectratio = '" & QuoteSanitise(.cmbRecordRatio.Text) & "', "
            l_strSQL = l_strSQL & "geometriclinearity = '" & QuoteSanitise(.cmbRecordGeometry.Text) & "', "
            l_strSQL = l_strSQL & "audiostandard = '" & QuoteSanitise(.cmbAudioStandard.Text) & "', "
            l_strSQL = l_strSQL & "ch1 = '" & QuoteSanitise(.cmbChannel1.Text) & "', "
            l_strSQL = l_strSQL & "ch2 = '" & QuoteSanitise(.cmbChannel2.Text) & "', "
            l_strSQL = l_strSQL & "ch3 = '" & QuoteSanitise(.cmbChannel3.Text) & "', "
            l_strSQL = l_strSQL & "ch4 = '" & QuoteSanitise(.cmbChannel4.Text) & "', "
            l_strSQL = l_strSQL & "ch5 = '" & QuoteSanitise(.cmbChannel5.Text) & "', "
            l_strSQL = l_strSQL & "ch6 = '" & QuoteSanitise(.cmbChannel6.Text) & "', "
            l_strSQL = l_strSQL & "ch7 = '" & QuoteSanitise(.cmbChannel7.Text) & "', "
            l_strSQL = l_strSQL & "ch8 = '" & QuoteSanitise(.cmbChannel8.Text) & "', "
            
            l_strSQL = l_strSQL & "ch9 = '" & QuoteSanitise(.cmbChannel9.Text) & "', "
            l_strSQL = l_strSQL & "ch10 = '" & QuoteSanitise(.cmbChannel10.Text) & "', "
            l_strSQL = l_strSQL & "ch11 = '" & QuoteSanitise(.cmbChannel11.Text) & "', "
            l_strSQL = l_strSQL & "ch12 = '" & QuoteSanitise(.cmbChannel12.Text) & "', "
            
'            l_strSQL = l_strSQL & "nr1 = '" & QuoteSanitise(.cmbNoiseReduction1.Text) & "', "
'            l_strSQL = l_strSQL & "nr2 = '" & QuoteSanitise(.cmbNoiseReduction2.Text) & "', "
'            l_strSQL = l_strSQL & "nr3 = '" & QuoteSanitise(.cmbNoiseReduction3.Text) & "', "
'            l_strSQL = l_strSQL & "nr4 = '" & QuoteSanitise(.cmbNoiseReduction4.Text) & "', "
'            l_strSQL = l_strSQL & "nr5 = '" & QuoteSanitise(.cmbNoiseReduction5.Text) & "', "
'            l_strSQL = l_strSQL & "nr6 = '" & QuoteSanitise(.cmbNoiseReduction6.Text) & "', "
'            l_strSQL = l_strSQL & "nr7 = '" & QuoteSanitise(.cmbNoiseReduction7.Text) & "', "
'            l_strSQL = l_strSQL & "nr8 = '" & QuoteSanitise(.cmbNoiseReduction8.Text) & "', "
'
'            l_strSQL = l_strSQL & "nr9 = '" & QuoteSanitise(.cmbNoiseReduction9.Text) & "', "
'            l_strSQL = l_strSQL & "nr10 = '" & QuoteSanitise(.cmbNoiseReduction10.Text) & "', "
'            l_strSQL = l_strSQL & "nr11 = '" & QuoteSanitise(.cmbNoiseReduction11.Text) & "', "
'            l_strSQL = l_strSQL & "nr12 = '" & QuoteSanitise(.cmbNoiseReduction12.Text) & "', "
'
            l_strSQL = l_strSQL & "cuetrack = '" & QuoteSanitise(.cmbChannelCue.Text) & "', "
            
            l_strSQL = l_strSQL & "series = '" & QuoteSanitise(.cmbSeries.Text) & "', "
            If InStr(GetData("company", "cetaclientcode", "companyID", Val(.lblCompanyID.Caption)), "/2digitepnumbers") > 0 Then
                l_strSQL = l_strSQL & "episode = '" & Format(Val(.cmbEpisode.Text), "00") & "', "
            Else
                l_strSQL = l_strSQL & "episode = '" & QuoteSanitise(.cmbEpisode.Text) & "', "
            End If
            l_strSQL = l_strSQL & "endepisode = '" & QuoteSanitise(.cmbEpisodeTo.Text) & "', "
            l_strSQL = l_strSQL & "seriesset = '" & QuoteSanitise(.cmbSet.Text) & "', "
            l_strSQL = l_strSQL & "flagincomplete = '" & .chkTapeIncomplete.Value & "', "
            l_strSQL = l_strSQL & "flagfalserecord = '" & .chkFalseRecord.Value & "', "
            
'            l_strSQL = l_strSQL & "sound_stereo_main = '" & .chkStereoMain.Value & "', "
'            l_strSQL = l_strSQL & "sound_stereo_me = '" & .chkStereoME.Value & "', "
'            l_strSQL = l_strSQL & "sound_surround_main = '" & .chkSurroundMain.Value & "', "
'            l_strSQL = l_strSQL & "sound_surround_me = '" & .chkSurroundME.Value & "', "
'            l_strSQL = l_strSQL & "sound_stereo_mmn = '" & .chkStereoMMN.Value & "', "
'            l_strSQL = l_strSQL & "sound_surround_mmn = '" & .chkSurroundMMN.Value & "', "
'            l_strSQL = l_strSQL & "sound_ltrt_main = '" & .chkLtRtMain.Value & "', "
'            l_strSQL = l_strSQL & "sound_ltrt_me = '" & .chkLtRtME.Value & "', "
            l_strSQL = l_strSQL & "textless = '" & .chkTapeTextless.Value & "', "
            If .chkIncludeInStorageReport.Value <> 0 Then
                l_strSQL = l_strSQL & "customfield4 = 'In_DailyStorageReport', "
            Else
                l_strSQL = l_strSQL & "customfield4 = Null, "
            End If
            
            l_strSQL = l_strSQL & "timecode = '" & QuoteSanitise(.cmbTimeCode.Text) & "', "
            l_strSQL = l_strSQL & "progduration = '" & QuoteSanitise(.txtProgDuration.Text) & "', "
            l_strSQL = l_strSQL & "totalduration = '" & QuoteSanitise(.txtTotalDuration.Text) & "', "
            l_strSQL = l_strSQL & "notes1 = '" & QuoteSanitise(.txtNotes.Text) & "', "
            
            If Not IsNull(.datMadeOnDate.Value) Then
                l_strSQL = l_strSQL & "modate = '" & FormatSQLDate(.datMadeOnDate.Value) & "', "
            Else
                l_strSQL = l_strSQL & "modate = Null, "
            End If
            
            l_strSQL = l_strSQL & "customfield1 = '" & .cmbField1.Text & "', "
            l_strSQL = l_strSQL & "customfield2 = '" & .cmbField2.Text & "', "
            l_strSQL = l_strSQL & "customfield3 = '" & .cmbField3.Text & "', "
            
            l_strSQL = l_strSQL & "jobdetailID = '" & .txtJobDetailID.Text & "', "
            l_strSQL = l_strSQL & "reference = '" & .txtReference.Text & "', "
            l_strSQL = l_strSQL & "MinimumFreePercentage = " & Int(Val(.txtMinimumFreePercentage.Text)) & ", "
            l_strSQL = l_strSQL & "CanBeUsedForAutoDelete = " & .chkCanBeUsedForAutoDelete.Value & ", "
            l_strSQL = l_strSQL & "AllowToUseForLiveCFM = " & .chkUseForLiveCFM.Value & ", "
            l_strSQL = l_strSQL & "NoRestores = " & .chkNoRestores.Value & ", "
            l_strSQL = l_strSQL & "RequireCompanyIDinFolders = " & .chkRequireCompanyIDinFolders.Value & ", "
            l_strSQL = l_strSQL & "DriveWritePermissionInteger = " & Int(Val(.txtDriveWriterPermissionInteger.Text)) & ", "
            l_strSQL = l_strSQL & "internalreference = '" & .txtInternalReference.Text & "' "
            l_strSQL = l_strSQL & "WHERE libraryID = " & .lblLibraryID.Caption
            
        End If
        
    End With
    
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    
    CheckForSQLError
        
    If Val(frmLibrary.lblLibraryID.Caption) = 0 Then frmLibrary.lblLibraryID.Caption = g_lngLastID
    
    'Clear the checker details - if tape details are altered it needs checking again.
    SetData "library", "checkedlabels", "libraryID", frmLibrary.lblLibraryID.Caption, 0
    SetData "library", "checkedrecreport", "libraryID", frmLibrary.lblLibraryID.Caption, 0
    SetData "library", "checkedtapespec", "libraryID", frmLibrary.lblLibraryID.Caption, 0
    SetData "library", "checkedby", "libraryID", frmLibrary.lblLibraryID.Caption, ""

    If Val(frmLibrary.lbltechrevID.Caption) <> 0 Then
        'tech review exists, so those field need saving too.
        If frmLibrary.lblReviewCompanyID.Caption <> "" Then
            If Val(frmLibrary.lbltechrevID.Caption) = Val(frmLibrary.lblLastTechrevID.Caption) Then
                If g_blnRedNetwork = 0 Then
                    MsgBox "Tech Reviews can only be saved from a Red Network Machine." & vbCrLf & "Tech review changes have not been saved.", vbCritical
                Else
                    SaveTechReview
                End If
            End If
        Else
            MsgBox "Tech Review has no associated Company. Tech review Not saved", vbCritical, "Error..."
        End If
    End If
    
    
    If l_oldLocation <> "" And l_oldLocation <> frmLibrary.cmbLocation.Text Then
        MoveLibraryItem frmLibrary.lblLibraryID.Caption, frmLibrary.cmbLocation.Text, 0, 0, frmLibrary.cmbShelf.Text, frmLibrary.cmbBox.Text, Val(frmLibrary.txtLocationJobID.Text)
    End If
    
    'Finally show the screen for the saved tape
    
    ShowLibrary frmLibrary.lblLibraryID.Caption
    
    frmLibrary.MousePointer = vbDefault
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub SaveTechReview()
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    
    With frmLibrary
        
        Dim l_datReviewdate As Variant
        If Not IsNull(.datReviewDate.Value) Then
            l_datReviewdate = Format(.datReviewDate.Value, vbShortDateFormat)
        Else
            l_datReviewdate = Null
        End If
        
        Dim l_intResponse As Integer
        
        l_strSQL = "UPDATE tapetechrev SET "
        l_strSQL = l_strSQL & "jobID = '" & .txtReviewJobID.Text & "', "
        If Not IsNull(l_datReviewdate) Then
            l_strSQL = l_strSQL & "reviewdate = '" & FormatSQLDate(l_datReviewdate) & "', "
        Else
            l_strSQL = l_strSQL & "reviewdate = NULL, "
        End If
        l_strSQL = l_strSQL & "reviewuser = '" & .cmbReviewUser.Text & "', "
        l_strSQL = l_strSQL & "companyID = '" & .lblReviewCompanyID.Caption & "', "
        l_strSQL = l_strSQL & "companyname = '" & QuoteSanitise(.cmbReviewCompany.Text) & "', "
        l_strSQL = l_strSQL & "contactID = '" & .lblReviewContactID.Caption & "', "
        l_strSQL = l_strSQL & "contactname = '" & QuoteSanitise(.cmbReviewContact) & "', "
        l_strSQL = l_strSQL & "flagtitles = '" & .chkTitles.Value & "', "
        l_strSQL = l_strSQL & "flagcaptions = '" & .chkCaptions.Value & "', "
        l_strSQL = l_strSQL & "flagsubtitles = '" & .chkSubtitles.Value & "', "
        l_strSQL = l_strSQL & "flaglogos = '" & .chkLogos.Value & "', "
        l_strSQL = l_strSQL & "flagtextless = '" & .chkTextless.Value & "', "
        l_strSQL = l_strSQL & "flagtrailers = '" & .chkTrailers.Value & "', "
        l_strSQL = l_strSQL & "convertedmaterial = '" & .chkConvertedMaterial.Value & "', "
        l_strSQL = l_strSQL & "identinfocorrect = '" & .chkIdentInfoCorrect.Value & "', "
        l_strSQL = l_strSQL & "fullQCreview = '" & .chkSpotCheckRev.Value & "', "
        l_strSQL = l_strSQL & "languagetitles = '" & .cmbTitlesLanguage.Text & "', "
        l_strSQL = l_strSQL & "languagecaptions = '" & .cmbCaptionsLanguage.Text & "', "
        l_strSQL = l_strSQL & "languagesubtitles = '" & .cmbSubtitlesLanguage.Text & "', "
        l_strSQL = l_strSQL & "detailslogos = '" & QuoteSanitise(.txtDetailsLogos.Text) & "', "
        l_strSQL = l_strSQL & "detailstextless = '" & QuoteSanitise(.txtDetailsTextless.Text) & "', "
        l_strSQL = l_strSQL & "detailstrailers = '" & QuoteSanitise(.txtDetailsTrailers.Text) & "', "
        l_strSQL = l_strSQL & "reviewmachine = '" & .cmbReviewMachine.Text & "', "
        l_strSQL = l_strSQL & "notesvideo = '" & QuoteSanitise(.txtTechNotesVideo.Text) & "', "
        l_strSQL = l_strSQL & "notesaudio = '" & QuoteSanitise(.txtTechNotesAudio.Text) & "', "
        l_strSQL = l_strSQL & "notesgeneral = '" & QuoteSanitise(.txtTechNotesGeneral.Text) & "', "
        If .optReviewConclusion(0).Value = True Then
            l_strSQL = l_strSQL & "reviewconclusion = '" & 0 & "', "
        End If
        If .optReviewConclusion(1).Value = True Then
            l_strSQL = l_strSQL & "reviewconclusion = '" & 1 & "', "
        End If
        If .optReviewConclusion(2).Value = True Then
            l_strSQL = l_strSQL & "reviewconclusion = '" & 2 & "', "
        End If
        If .optReviewConclusion(3).Value = True Then
            l_strSQL = l_strSQL & "passedforitunes = '" & 0 & "', "
        End If
        If .optReviewConclusion(4).Value = True Then
            l_strSQL = l_strSQL & "passedforitunes = '" & 1 & "', "
        End If
        l_strSQL = l_strSQL & "videotest = '" & .txtVideoTest.Text & "', "
        l_strSQL = l_strSQL & "videoprog = '" & .txtVideoProg.Text & "', "
        l_strSQL = l_strSQL & "chromatest = '" & .txtChromaTest.Text & "', "
        l_strSQL = l_strSQL & "chromaprog = '" & .txtChromaProg.Text & "', "
        l_strSQL = l_strSQL & "blacktest = '" & .txtBlackTest.Text & "', "
        l_strSQL = l_strSQL & "blackprog = '" & .txtBlackProg.Text & "', "
        l_strSQL = l_strSQL & "phasetest = '" & .txtPhaseTest.Text & "', "
        l_strSQL = l_strSQL & "phaseprog = '" & .txtPhaseProg.Text & "', "
        l_strSQL = l_strSQL & "audio1test = '" & .txtA1Test.Text & "', "
        l_strSQL = l_strSQL & "audio1prog = '" & .txtA1Prog.Text & "', "
        l_strSQL = l_strSQL & "audio2test = '" & .txtA2Test.Text & "', "
        l_strSQL = l_strSQL & "audio2prog = '" & .txtA2Prog.Text & "', "
        l_strSQL = l_strSQL & "audio3test = '" & .txtA3Test.Text & "', "
        l_strSQL = l_strSQL & "audio3prog = '" & .txtA3Prog.Text & "', "
        l_strSQL = l_strSQL & "audio4test = '" & .txtA4Test.Text & "', "
        l_strSQL = l_strSQL & "audio4prog = '" & .txtA4Prog.Text & "', "
        l_strSQL = l_strSQL & "audio5test = '" & .txtA5Test.Text & "', "
        l_strSQL = l_strSQL & "audio5prog = '" & .txtA5Prog.Text & "', "
        l_strSQL = l_strSQL & "audio6test = '" & .txtA6Test.Text & "', "
        l_strSQL = l_strSQL & "audio6prog = '" & .txtA6Prog.Text & "', "
        l_strSQL = l_strSQL & "audio7test = '" & .txtA7Test.Text & "', "
        l_strSQL = l_strSQL & "audio7prog = '" & .txtA7Prog.Text & "', "
        l_strSQL = l_strSQL & "audio8test = '" & .txtA8Test.Text & "', "
        l_strSQL = l_strSQL & "audio8prog = '" & .txtA8Prog.Text & "', "
        l_strSQL = l_strSQL & "audio9test = '" & .txtA9Test.Text & "', "
        l_strSQL = l_strSQL & "audio9prog = '" & .txtA9Prog.Text & "', "
        l_strSQL = l_strSQL & "audio10test = '" & .txtA10Test.Text & "', "
        l_strSQL = l_strSQL & "audio10prog = '" & .txtA10Prog.Text & "', "
        l_strSQL = l_strSQL & "audio11test = '" & .txtA11Test.Text & "', "
        l_strSQL = l_strSQL & "audio11prog = '" & .txtA11Prog.Text & "', "
        l_strSQL = l_strSQL & "audio12test = '" & .txtA12Test.Text & "', "
        l_strSQL = l_strSQL & "audio12prog = '" & .txtA12Prog.Text & "', "
        l_strSQL = l_strSQL & "cuetest = '" & .txtCueTest.Text & "', "
        l_strSQL = l_strSQL & "cueprog = '" & .txtCueProg.Text & "', "
        
        l_strSQL = l_strSQL & "EBU128LoudnessStereoMain = '" & .cmbEBU128LoudnessStereoMain.Text & "', "
        l_strSQL = l_strSQL & "EBU128LoudnessStereoME = '" & .cmbEBU128LoudnessStereoME.Text & "', "
        l_strSQL = l_strSQL & "EBU128Loudness51Main = '" & .cmbEBU128Loudness51Main.Text & "', "
        l_strSQL = l_strSQL & "EBU128Loudness51ME = '" & .cmbEBU128Loudness51ME.Text & "', "
        
        l_strSQL = l_strSQL & "highestaudiopeak = '" & .txtHighestAudioPeak.Text & "', "
        l_strSQL = l_strSQL & "vitclines = '" & .txtvitclines.Text & "', "
        l_strSQL = l_strSQL & "userbits = '" & .txtuserbits.Text & "', "
        l_strSQL = l_strSQL & "timecodecontinuous = '" & .chktimecodecontinuous.Value & "', "
        l_strSQL = l_strSQL & "ltc = '" & .chkltc.Value & "', "
        l_strSQL = l_strSQL & "vitc = '" & .chkvitc.Value & "', "
        l_strSQL = l_strSQL & "ltcvitcmatch = '" & .chkltcvitcmatch.Value & "', "
        l_strSQL = l_strSQL & "firstactivepixel = '" & .txtfirstactivepixel.Text & "', "
        l_strSQL = l_strSQL & "lastactivepixel = '" & .txtlastactivepixel.Text & "', "
        l_strSQL = l_strSQL & "firstactivelinefield1 = '" & .txtfirstactivelinefield1.Text & "', "
        l_strSQL = l_strSQL & "lastactivelinefield1 = '" & .txtlastactivelinefield1.Text & "', "
        l_strSQL = l_strSQL & "firstactivelinefield2= '" & .txtfirstactivelinefield2.Text & "', "
        l_strSQL = l_strSQL & "lastactivelinefield2 = '" & .txtlastactivelinefield2.Text & "', "
        l_strSQL = l_strSQL & "verticalcentreingcorrect = '" & .chkverticalcenteringcorrect.Value & "', "
        l_strSQL = l_strSQL & "horizontalcentreingcorrect = '" & .chkhorizontalcenteringcorrect.Value & "', "
        l_strSQL = l_strSQL & "captionsafe169 = '" & .chkCaptionsafe169.Value & "', "
        l_strSQL = l_strSQL & "captionsafe149 = '" & .chkCaptionsafe149.Value & "', "
        l_strSQL = l_strSQL & "captionsafe43protect = '" & .chkCaptionsafe43protect.Value & "', "
        l_strSQL = l_strSQL & "captionsafe43ebu = '" & .chkCaptionsafe43ebu.Value & "', "
        l_strSQL = l_strSQL & "firstactivepixellabel = '" & .lblParameters(4).Caption & "', "
        l_strSQL = l_strSQL & "lastactivepixellabel = '" & .lblParameters(5).Caption & "', "
        l_strSQL = l_strSQL & "firstactlinefield1label = '" & .lblParameters(0).Caption & "', "
        l_strSQL = l_strSQL & "lastactlinefield1label = '" & .lblParameters(1).Caption & "', "
        l_strSQL = l_strSQL & "firstactlinefield2label = '" & .lblParameters(2).Caption & "', "
        l_strSQL = l_strSQL & "lastactlinefield2label = '" & .lblParameters(3).Caption & "' "
        l_strSQL = l_strSQL & "WHERE tapetechrevID = " & .lbltechrevID.Caption
        
    End With
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    If g_optCreatePDFFilesOfTechReviews <> 0 Then
        If frmLibrary.chkSpotCheckRev.Value = 0 Then
            ReportToPDF g_strLocationOfCrystalReportFiles & "Techrev_Runtime.rpt", "{techrev.tapetechrevID} = " & frmLibrary.lbltechrevID.Caption, g_strTapePDFLocation & "\" & SanitiseBarcode(frmLibrary.txtBarcode.Text) & ".pdf"
        Else
            If InStr(GetData("company", "cetaclientcode", "companyID", frmLibrary.lblCompanyID.Caption), "/techrevincitunes") > 0 Then
                ReportToPDF g_strLocationOfCrystalReportFiles & "Techrev_Full_itunes.rpt", "{techrev.tapetechrevID} = " & frmLibrary.lbltechrevID.Caption, g_strTapePDFLocation & "\" & SanitiseBarcode(frmLibrary.txtBarcode.Text) & ".pdf"
            Else
                ReportToPDF g_strLocationOfCrystalReportFiles & "Techrev_Full.rpt", "{techrev.tapetechrevID} = " & frmLibrary.lbltechrevID.Caption, g_strTapePDFLocation & "\" & SanitiseBarcode(frmLibrary.txtBarcode.Text) & ".pdf"
            End If
        End If

    End If
        
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Function DespatchLibraryItem(lp_lngDespatchID As Long, lp_strBarcode As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/despatchlibraryitem") Then
        Exit Function
    End If
    
    If lp_lngDespatchID = 0 Then
        Exit Function
    End If
    
CreateDespatchLine:
    
    'first check to see library item exists
    If RecordExists("library", "barcode", lp_strBarcode) Then
        
        Dim l_strSQL As String
        l_strSQL = "SELECT * FROM library WHERE barcode = '" & lp_strBarcode & "'"
        
        Dim l_rstLibrary As New ADODB.Recordset
        Set l_rstLibrary = ExecuteSQL(l_strSQL, g_strExecuteError)
        
        'Check status of company owning tape - HOLD companies should prompt for continue
        
        If GetData("company", "accountstatus", "companyID", l_rstLibrary("companyID")) = "HOLD" Then
            Dim l_intResult As Integer
            
            l_intResult = MsgBox("Tape owner is on Hold - continue to despatch tape?", vbYesNo)
            If l_intResult = vbNo Then
                l_rstLibrary.Close
                Set l_rstLibrary = Nothing
                GoTo PROC_EXIT
            End If
        End If
        
        Dim l_lngJobID As Long
        Dim l_lngProjectNumber As Long
        Dim l_intQuantity As Integer
        Dim l_strCopyType As String
        Dim l_strFormat As String
        Dim l_strVideoStandard As String
        Dim l_strTitle As String
        Dim l_strSubTitle As String
        Dim l_intPackageNumber As Integer
        Dim l_lngLibraryID As Long
        Dim l_strProduct As String
        Dim l_strSeries As String
        Dim l_strSet As String
        Dim l_strEpisode As String
        
        With l_rstLibrary
            l_lngProjectNumber = Val(Trim(" " & .Fields("projectnumber")))
            l_lngLibraryID = .Fields("libraryID")
            l_lngJobID = Val(Trim(" " & .Fields("jobID")))
            l_intQuantity = 1
            l_strCopyType = Trim(" " & .Fields("copytype"))
            l_strFormat = Trim(" " & .Fields("format"))
            l_strVideoStandard = Trim(" " & .Fields("videostandard"))
            l_strTitle = Trim(" " & .Fields("title"))
            l_strSubTitle = Trim(" " & .Fields("subtitle"))
            l_strProduct = Trim(" " & .Fields("productname"))
            l_strSeries = Trim(" " & .Fields("series"))
            l_strSet = Trim(" " & .Fields("seriesset"))
            l_strEpisode = Trim(" " & .Fields("episode"))
            If .Fields("endepisode") <> "" Then
                l_strEpisode = l_strEpisode & "-" & .Fields("endepisode")
            End If
            l_intPackageNumber = 1
        End With
        
        l_rstLibrary.Close
        Set l_rstLibrary = Nothing
        
        
        If g_optRequireProductBeforeSaving = 1 Then
            l_strSubTitle = l_strTitle
            l_strTitle = l_strProduct
        End If
        
        
        Dim l_strDirection As String
        l_strDirection = GetData("despatch", "direction", "despatchID", lp_lngDespatchID)
        
        Dim l_strLocation As String
        Dim l_strShelf As String
        l_strLocation = ""
        
        l_strLocation = "OFF SITE"
        l_strShelf = GetData("despatch", "companyname", "despatchID", lp_lngDespatchID)
        
        DespatchLibraryItem = AddRowToDespatchNote(lp_lngDespatchID, l_lngProjectNumber, l_lngJobID, l_intQuantity, l_strCopyType, l_strFormat, l_strVideoStandard, lp_strBarcode, l_strTitle, l_strSubTitle, l_intPackageNumber, l_lngLibraryID, "", "", "", l_strSeries, l_strSet, l_strEpisode)
        MoveLibraryItem l_lngLibraryID, l_strLocation, lp_lngDespatchID, 0, l_strShelf, ""
        
        
    Else
        'do you want add the record
        Dim l_intMessage As Integer
        l_intMessage = MsgBox("Could not locate barcode. Do you want to add this barcode as a new library record?", vbQuestion + vbYesNo)
        Select Case l_intMessage
            Case vbYes
                CreateLibraryItem lp_strBarcode, "NEW TAPE", lp_lngDespatchID
                GoTo CreateDespatchLine
        End Select
        
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function NewLibraryItem() As String

frmAddNewTape.Show vbModal

If frmAddNewTape.Tag <> "CANCELLED" Then
    NewLibraryItem = frmAddNewTape.txtInput(0).Text
End If

Unload frmAddNewTape
Set frmAddNewTape = Nothing

End Function

Function CreateNewITVLibraryItem(lp_strBarcode As String, lp_strTitle As String, lp_strSubTitle As String) As Long

'this adds a new item to the library

Dim l_strSQL As String, l_lngNewReference As Long, l_lngLibraryID As Long
l_lngNewReference = GetNextSequence("internalreference")
l_strSQL = "INSERT INTO library (cuser, cdate, format, copytype, videostandard, title, subtitle, location, modate, projectID, barcode, internalreference, companyID, companyname) VALUES ('"
l_strSQL = l_strSQL & g_strUserInitials & "',' "
l_strSQL = l_strSQL & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'DBETA', "
l_strSQL = l_strSQL & "'MASTER', "
l_strSQL = l_strSQL & "'625PAL', "
l_strSQL = l_strSQL & "'" & lp_strTitle & "', "
l_strSQL = l_strSQL & "'" & lp_strSubTitle & "', "
l_strSQL = l_strSQL & "'OPERATIONS', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'" & lp_strBarcode & "', "
l_strSQL = l_strSQL & "'" & l_lngNewReference & "', "
l_strSQL = l_strSQL & "904, 'ITV plc'"
l_strSQL = l_strSQL & ");"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

'pick up the new ID
l_lngLibraryID = g_lngLastID

l_strSQL = "INSERT INTO trans (libraryID, destination, cuser, cdate) VALUES ("
l_strSQL = l_strSQL & l_lngLibraryID & ", "
l_strSQL = l_strSQL & "'Created', "
l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "');"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

CreateNewITVLibraryItem = l_lngLibraryID

End Function
Function CreateNewLibraryItem(lp_strFormat As String, lp_strStockCode As String, lp_strLocation As String, lp_datMadeOn As Date, lp_lngProjectNumber As Long, lp_strBarcode As String, Optional lp_lngCompanyID As Long) As Long

'this adds a new item to the library

Dim l_lngProjectID As Long
l_lngProjectID = GetData("project", "projectID", "projectnumber", lp_lngProjectNumber)

'project invalid
If lp_lngProjectNumber <> 0 And l_lngProjectID = 0 Then
    CreateNewLibraryItem = -1
    Exit Function
End If

'barcode already exists
If GetData("library", "libraryID", "barcode", lp_strBarcode) <> 0 Then
    CreateNewLibraryItem = -2
    Exit Function
End If

Dim l_strSQL As String, l_lngNewReference As Long
l_lngNewReference = GetNextSequence("internalreference")
l_strSQL = "INSERT INTO library (cuser, cdate, format, stocktype, location, modate, projectID, barcode, internalreference, companyID, companyname) VALUES ('" & _
g_strUserInitials & "','" & FormatSQLDate(Now) & "','" & lp_strFormat & "','" & lp_strStockCode & "','" & lp_strLocation & "','" & _
FormatSQLDate(lp_datMadeOn) & "','" & l_lngProjectID & "','" & lp_strBarcode & "','" & l_lngNewReference & "', "
If lp_lngCompanyID <> 0 Then
    l_strSQL = l_strSQL & " " & lp_lngCompanyID & ", '"
    l_strSQL = l_strSQL & GetData("company", "name", "companyID", lp_lngCompanyID) & "'"
Else
    l_strSQL = l_strSQL & "0, ''"
End If
l_strSQL = l_strSQL & ");"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

'pick up the new ID
CreateNewLibraryItem = g_lngLastID

End Function
Function SaveJobDetailsToLibraryRecord(lp_lngLibraryID As Long, lp_lngJobID As Long, lp_lngProjectNumber As Long)

   If Not RecordExists("library", "libraryID", lp_lngLibraryID) Then
        MsgBox "This library record does not exist.", vbExclamation
        Exit Function
    End If

    Dim l_lngExistingJobID As Long, l_lngExistingProjectNumber  As Long, l_strExistingTitle As String
    
    l_lngExistingJobID = GetData("library", "jobID", "libraryID", lp_lngLibraryID)
    l_lngExistingProjectNumber = GetData("library", "projectnumber", "libraryID", lp_lngLibraryID)
    l_strExistingTitle = GetData("library", "title", "libraryID", lp_lngLibraryID)

    If l_strExistingTitle = "NEW TAPE" Then GoTo PROC_UPDATE

    'if no jobID was passed
    If lp_lngJobID = 0 Then
        '
        If lp_lngProjectNumber = 0 Then
            Exit Function
        Else
            lp_lngJobID = GetData("job", "jobID", "projectnumber", lp_lngProjectNumber)
            If lp_lngJobID = 0 Then Exit Function
        End If
    Else
        If lp_lngProjectNumber = 0 Then
            lp_lngProjectNumber = GetData("job", "projectnumber", "jobID", lp_lngJobID)
        End If
    End If
    
    If l_lngExistingProjectNumber = 0 And l_lngExistingJobID = 0 Then GoTo PROC_UPDATE

    If (l_lngExistingJobID <> lp_lngJobID) Or (l_lngExistingProjectNumber <> lp_lngProjectNumber) Then
        Dim l_intMsg As Integer
        l_intMsg = MsgBox("This library already record has different job ID or project number details. Are you sure you want to reassociate it with this job?", vbYesNo + vbQuestion)
        If l_intMsg = vbNo Then Exit Function
    End If

    Dim l_lngProjectID As Long

PROC_UPDATE:

    Screen.MousePointer = vbHourglass

    Dim l_strSQL As String
    l_strSQL = "UPDATE library SET jobID = '" & lp_lngJobID & "', projectnumber = '" & lp_lngProjectNumber & "', projectID = '" & l_lngProjectID & "' WHERE libraryID = '" & lp_lngLibraryID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    Screen.MousePointer = vbDefault
    
    If l_strExistingTitle = "NEW TAPE" Then
        l_intMsg = vbYes
    Else
        l_intMsg = MsgBox("Do you also want to overwrite the library item's key details with the details from this job (company name, product, title and subtitle)?", vbYesNo + vbQuestion)
    End If
    
    If l_intMsg = vbYes Then
        
        Dim l_strJobProductName As String
        Dim l_lngJobProductID  As Long
        Dim l_strJobCompanyName As String
        Dim l_lngJobCompanyID As Long
        Dim l_strJobContactName As String
        Dim l_lngJobContactID As Long
        Dim l_strJobSubTitle  As String
        Dim l_strJobTitle  As String
        
        l_strJobProductName = GetData("job", "productname", "jobID", lp_lngJobID)
        l_lngJobProductID = GetData("job", "productID", "jobID", lp_lngJobID)
        l_strJobCompanyName = GetData("job", "companyname", "jobID", lp_lngJobID)
        l_lngJobCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
        l_strJobContactName = GetData("job", "contactname", "jobID", lp_lngJobID)
        l_lngJobContactID = GetData("job", "contactID", "jobID", lp_lngJobID)
        l_strJobTitle = GetData("job", "title1", "jobID", lp_lngJobID)
        l_strJobSubTitle = GetData("job", "title2", "jobID", lp_lngJobID)
    
        l_strSQL = "UPDATE library SET companyname = '" & QuoteSanitise(l_strJobCompanyName) & "', productname = '" & QuoteSanitise(l_strJobProductName) & "', companyID = '" & l_lngJobCompanyID & "', productID = '" & l_lngJobProductID & "', title = '" & QuoteSanitise(l_strJobTitle) & "', subtitle = '" & QuoteSanitise(l_strJobSubTitle) & "' WHERE libraryID = '" & lp_lngLibraryID & "';"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    
    End If



End Function

Sub ShowLibrary(ByVal lp_lngLibraryID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/showlibrary") Then
        Exit Sub
    End If
    
  '  If IsFormLoaded("frmLibrary") Then
  '      frmLibrary.Show
  '      frmLibrary.ZOrder 0
  '      Exit Sub
  '  End If
    
    Dim l_strSQL As String
    Dim l_lngtechrevID As Long
    Dim l_lngParentInternalReference As Long
    Dim l_blnFoundOne As Boolean
    
    l_strSQL = "SELECT * FROM library WHERE libraryID = " & lp_lngLibraryID
    
    Dim l_rstLibrary As New ADODB.Recordset
    Set l_rstLibrary = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    CheckForSQLError
    
    If Not l_rstLibrary.EOF Then
        l_blnFoundOne = True
        l_rstLibrary.MoveFirst
        
        With frmLibrary
            '        .Caption = "Library Details - [Showing Barcode: " & Format(l_rstLibrary("barcode")) & "]"
            If l_rstLibrary("system_deleted") = 1 Then
                .lblDeletedTape.Visible = True
                If CheckAccess("/undeletetapes") = True Then
                    .cmdUndeleteTape.Visible = True
                Else
                    .cmdUndeleteTape.Visible = False
                End If
            Else
                .lblDeletedTape.Visible = False
                .cmdUndeleteTape.Visible = False
            End If
            .txtProjectNumber.Text = Format(l_rstLibrary("projectnumber")) 'GetData("project", "projectnumber", "projectID", l_rstLibrary("projectID"))
            .lblLibraryID.Caption = l_rstLibrary("libraryID")
            .txtBarcode.Text = Trim(" " & l_rstLibrary("barcode"))
            .txtForeignRef.Text = Trim(" " & l_rstLibrary("foreignref"))
            .txtJobID.Text = Trim(" " & l_rstLibrary("jobID"))
            .txtJobDetailID.Text = Trim(" " & l_rstLibrary("jobdetailID"))
            .cmbLocation.Text = Trim(" " & l_rstLibrary("location"))
            .txtLocationJobID.Text = Trim(" " & l_rstLibrary("locationjobID"))
            .cmbShelf.Text = Trim(" " & l_rstLibrary("shelf"))
            .cmbBox.Text = Trim(" " & l_rstLibrary("box"))
            .cmbCopyType.Text = Trim(" " & l_rstLibrary("copytype"))
            .txtSourceBarcode.Text = Trim(" " & l_rstLibrary("sourcereference"))
            .cmbSourceMachine.Text = Trim(" " & l_rstLibrary("sourcemachine"))
            .cmbSourceFormat.Text = Trim(" " & l_rstLibrary("sourceformat"))
            .cmbSourceStandard.Text = Trim(" " & l_rstLibrary("sourcevideostandard"))
            .cmbSourceRatio.Text = Trim(" " & l_rstLibrary("sourceaspectratio"))
            .cmbSourceGeometry.Text = Trim(" " & l_rstLibrary("sourcegeometriclinearity"))
            .cmbRecordMachine.Text = Trim(" " & l_rstLibrary("recordmachine"))
            .cmbRecordFormat.Text = Trim(" " & l_rstLibrary("format"))
            If .cmbRecordFormat.Text = "FILM" Then
                .lblCaption(19).Caption = "Combined Sound?"
            Else
                .lblCaption(19).Caption = "Machine"
            End If
            .cmbRecordStandard.Text = Trim(" " & l_rstLibrary("videostandard"))
            .cmbRecordRatio.Text = Trim(" " & l_rstLibrary("aspectratio"))
            .cmbRecordGeometry.Text = Trim(" " & l_rstLibrary("geometriclinearity"))
            .cmbAudioStandard.Text = Trim(" " & l_rstLibrary("audiostandard"))
            .cmbChannel1.Text = Trim(" " & l_rstLibrary("ch1"))
            .cmbChannel2.Text = Trim(" " & l_rstLibrary("ch2"))
            .cmbChannel3.Text = Trim(" " & l_rstLibrary("ch3"))
            .cmbChannel4.Text = Trim(" " & l_rstLibrary("ch4"))
            .cmbChannel5.Text = Trim(" " & l_rstLibrary("ch5"))
            .cmbChannel6.Text = Trim(" " & l_rstLibrary("ch6"))
            .cmbChannel7.Text = Trim(" " & l_rstLibrary("ch7"))
            .cmbChannel8.Text = Trim(" " & l_rstLibrary("ch8"))
            .cmbChannel9.Text = Trim(" " & l_rstLibrary("ch9"))
            .cmbChannel10.Text = Trim(" " & l_rstLibrary("ch10"))
            .cmbChannel11.Text = Trim(" " & l_rstLibrary("ch11"))
            .cmbChannel12.Text = Trim(" " & l_rstLibrary("ch12"))
            
'            .cmbNoiseReduction1.Text = Trim(" " & l_rstLibrary("nr1"))
'            .cmbNoiseReduction2.Text = Trim(" " & l_rstLibrary("nr2"))
'            .cmbNoiseReduction3.Text = Trim(" " & l_rstLibrary("nr3"))
'            .cmbNoiseReduction4.Text = Trim(" " & l_rstLibrary("nr4"))
'            .cmbNoiseReduction5.Text = Trim(" " & l_rstLibrary("nr5"))
'            .cmbNoiseReduction6.Text = Trim(" " & l_rstLibrary("nr6"))
'            .cmbNoiseReduction7.Text = Trim(" " & l_rstLibrary("nr7"))
'            .cmbNoiseReduction8.Text = Trim(" " & l_rstLibrary("nr8"))
'            .cmbNoiseReduction9.Text = Trim(" " & l_rstLibrary("nr9"))
'            .cmbNoiseReduction10.Text = Trim(" " & l_rstLibrary("nr10"))
'            .cmbNoiseReduction11.Text = Trim(" " & l_rstLibrary("nr11"))
'            .cmbNoiseReduction12.Text = Trim(" " & l_rstLibrary("nr12"))
            
'            .chkStereoMain.Value = GetFlag(Trim(" " & l_rstLibrary("sound_stereo_main")))
'            .chkSurroundMain.Value = GetFlag(Trim(" " & l_rstLibrary("sound_surround_main")))
'            .chkStereoME.Value = GetFlag(Trim(" " & l_rstLibrary("sound_stereo_me")))
'            .chkSurroundME.Value = GetFlag(Trim(" " & l_rstLibrary("sound_surround_me")))
'            .chkStereoMMN.Value = GetFlag(Trim(" " & l_rstLibrary("sound_stereo_mmn")))
'            .chkSurroundMMN.Value = GetFlag(Trim(" " & l_rstLibrary("sound_surround_mmn")))
'            .chkLtRtMain.Value = GetFlag(Trim(" " & l_rstLibrary("sound_ltrt_main")))
'            .chkLtRtME.Value = GetFlag(Trim(" " & l_rstLibrary("sound_ltrt_me")))
            .chkTapeTextless.Value = GetFlag(Trim(" " & l_rstLibrary("textless")))
            If Trim(" " & l_rstLibrary("customfield4")) = "In_DailyStorageReport" Then
                .chkIncludeInStorageReport.Value = 1
            Else
                .chkIncludeInStorageReport.Value = 0
            End If
            
            .cmbChannelCue.Text = Trim(" " & l_rstLibrary("cuetrack"))
            .cmbTimeCode.Text = Trim(" " & l_rstLibrary("timecode"))
            .chkTapeIncomplete.Value = GetFlag(l_rstLibrary("flagincomplete"))
            .chkFalseRecord.Value = GetFlag(l_rstLibrary("flagfalserecord"))
            
            If .cmbTimeCode.Text = "SMPTE-NDF" Then
                .chkDropFrame.Enabled = True
            Else
                .chkDropFrame.Value = False
                .chkDropFrame.Enabled = False
            End If
            
            'Set all the company fields and prepare the title combo for that company's titles
            .cmbCompany.Text = Trim(" " & l_rstLibrary("companyname"))
            .lblCompanyID.Caption = Trim(" " & l_rstLibrary("companyID"))
            .lblProductID.Caption = Trim(" " & l_rstLibrary("productID"))
            .txtTitle.Text = Trim(" " & l_rstLibrary("title"))
            .txtSeriesID.Text = Trim(" " & l_rstLibrary("seriesID"))
            .txtSerialNumber.Text = Trim(" " & l_rstLibrary("serialnumber"))
            .txtTitle.Columns("SeriesID").Text = Trim(" " & l_rstLibrary("seriesID"))
            .txtTitle.Columns("CompanyID").Text = Trim(" " & l_rstLibrary("companyID"))
            .txtSubTitle.Text = Trim(" " & l_rstLibrary("subtitle"))
            .txtNotes.Text = Trim(" " & l_rstLibrary("notes1"))
            .cmbVersion.Text = Trim(" " & l_rstLibrary("version"))
            .cmbStockCode.Text = Trim(" " & l_rstLibrary("stocktype"))
            .cmbStockManufacturer.Text = Trim(" " & l_rstLibrary("stockmanufacturer"))
            .txtProgDuration.Text = Trim(" " & l_rstLibrary("progduration"))
            .txtTotalDuration.Text = Trim(" " & l_rstLibrary("totalduration"))
            .lblCreatedDate.Caption = Trim(" " & l_rstLibrary("cdate"))
            .lblCreatedUser.Caption = Trim(" " & l_rstLibrary("cuser"))
            .lblModifiedDate.Caption = Trim(" " & l_rstLibrary("mdate"))
            .lblModifiedUser.Caption = Trim(" " & l_rstLibrary("muser"))
            .datMadeOnDate.Value = Trim(" " & l_rstLibrary("modate"))
            .txtReference.Text = Trim(" " & l_rstLibrary("reference"))
            .txtMinimumFreePercentage.Text = l_rstLibrary("MinimumFreePercentage")
            .chkCanBeUsedForAutoDelete.Value = GetFlag(l_rstLibrary("CanBeUsedForAutoDelete"))
            .chkUseForLiveCFM.Value = GetFlag(l_rstLibrary("AllowToUseForLiveCFM"))
            .chkNoRestores.Value = GetFlag(l_rstLibrary("NoRestores"))
            .chkRequireCompanyIDinFolders.Value = GetFlag(l_rstLibrary("RequireCompanyIDinFolders"))
            .txtDriveWriterPermissionInteger.Text = Trim(" " & l_rstLibrary("DriveWritePermissionInteger"))
            
            Dim l_strTempLabel As String, l_lngCompanyID As Long
            
            l_lngCompanyID = Val(.lblCompanyID.Caption)
            
            If l_lngCompanyID <> 0 Then
        
                Dim l_conSearch As ADODB.Connection
                Dim l_rstSearch2 As ADODB.Recordset
                
                Set l_conSearch = New ADODB.Connection
                Set l_rstSearch2 = New ADODB.Recordset
                
                l_conSearch.ConnectionString = g_strConnection
                l_conSearch.Open
                
                l_strSQL = "SELECT * FROM masterseriestitle WHERE companyID = " & l_lngCompanyID & " ORDER BY title;"
                
                With l_rstSearch2
                    .CursorLocation = adUseClient
                    .LockType = adLockBatchOptimistic
                    .CursorType = adOpenDynamic
                    .Open l_strSQL, l_conSearch, adOpenDynamic
                End With
                
                l_rstSearch2.ActiveConnection = Nothing
                
                Set .txtTitle.DataSourceList = l_rstSearch2
                
                l_conSearch.Close
                Set l_conSearch = Nothing
                
                .lblField1.Caption = Trim(" " & GetData("company", "customfield1label", "companyID", l_lngCompanyID))
                If .lblField1.Caption = "" Then
                    .lblField1.Visible = False
                    .cmbField1.Visible = False
                Else
                    .lblField1.Visible = True
                    .cmbField1.Visible = True
                    PopulateCustomFieldCombo l_lngCompanyID, 1, .cmbField1
                End If
                .lblField2.Caption = Trim(" " & GetData("company", "customfield2label", "companyID", l_lngCompanyID))
                If .lblField2.Caption = "" Then
                    .lblField2.Visible = False
                    .cmbField2.Visible = False
                Else
                    .lblField2.Visible = True
                    .cmbField2.Visible = True
                    PopulateCustomFieldCombo l_lngCompanyID, 2, .cmbField2
                End If
                .lblField3.Caption = Trim(" " & GetData("company", "customfield3label", "companyID", l_lngCompanyID))
                If .lblField3.Caption = "" Then
                    .lblField3.Visible = False
                    .cmbField3.Visible = False
                Else
                    .lblField3.Visible = True
                    .cmbField3.Visible = True
                    PopulateCustomFieldCombo l_lngCompanyID, 3, .cmbField3
                End If
                
            End If
        
            .cmbField1.Text = Trim(" " & l_rstLibrary("customfield1"))
            .cmbField2.Text = Trim(" " & l_rstLibrary("customfield2"))
            .cmbField3.Text = Trim(" " & l_rstLibrary("customfield3"))
            
            .lblInLibrary.Caption = UCase(Trim(" " & l_rstLibrary("inlibrary")))
            If .lblInLibrary.Caption = "YES" Or .lblInLibrary.Caption = "TRUE" Then
                .lblInLibrary.Caption = "YES"
                .lblInLibrary.BackColor = vbGreen
            Else
                .lblInLibrary.Caption = "NO"
                .lblInLibrary.BackColor = vbRed
            End If
            .cmbSeries.Text = Trim(" " & l_rstLibrary("series"))
            .cmbSet.Text = Trim(" " & l_rstLibrary("seriesset"))
            .cmbEpisode.Text = Trim(" " & l_rstLibrary("episode"))
            .cmbEpisodeTo.Text = Trim(" " & l_rstLibrary("endepisode"))
            If .lblInLibrary.Caption = "YES" Then .cmdAssignToLibrary.Caption = "Remove From Library" Else .cmdAssignToLibrary.Caption = "Assign To Library"
            
            .txtInternalReference.Text = Trim(" " & l_rstLibrary("internalreference"))
            If Val(.txtInternalReference.Text) = 0 Then
                If .txtSourceBarcode.Text <> "" Then
                    l_lngParentInternalReference = Val(Trim(" " & GetData("library", "internalreference", "barcode", .txtSourceBarcode.Text)))
                    If l_lngParentInternalReference = 0 Then
                        l_lngParentInternalReference = GetNextSequence("internalreference")
                        SetData "library", "internalreference", "barcode", .txtSourceBarcode.Text, l_lngParentInternalReference
                    End If
                    .txtInternalReference.Text = l_lngParentInternalReference
                    SetData "library", "internalreference", "libraryID", lp_lngLibraryID, l_lngParentInternalReference
                Else
                    .txtInternalReference.Text = GetNextSequence("internalreference")
                    SetData "library", "internalreference", "libraryID", lp_lngLibraryID, .txtInternalReference.Text
                End If
            End If
            
            If .cmbRecordFormat.Text = "MOBILEDISC" Or .cmbRecordFormat.Text = "DISCSTORE" Or .cmbRecordFormat.Text = "LTO5" Or .cmbRecordFormat.Text = "LTO6" Or .cmbRecordFormat.Text = "LTO7" Then
                .cmdInitialise.Visible = True
                If CheckAccess("/showminimumfreepercentage", True) = False Then
                    .txtMinimumFreePercentage.Visible = False
                    .lblCaption(72).Visible = False
                Else
                    .txtMinimumFreePercentage.Visible = True
                    .lblCaption(72).Visible = True
                End If
            Else
                .cmdInitialise.Visible = False
                .txtMinimumFreePercentage.Visible = False
                .lblCaption(72).Visible = False
            End If
            
            If .cmbRecordFormat.Text = "DISCSTORE" Then
                .cmdVerifyDiscstore.Visible = True
                .chkCanBeUsedForAutoDelete.Visible = True
                .chkUseForLiveCFM.Visible = True
                .chkIncludeInStorageReport.Visible = True
                .chkNoRestores.Visible = True
                .chkRequireCompanyIDinFolders.Visible = True
                .lblCaption(9).Caption = "UNC Path"
                .lblCaption(1).Caption = "Alt UNC Path"
                .lblCaption(4).Caption = "DIVA Mnt"
                If CheckAccess("/Superuser", True) = True Then
                    .txtDriveWriterPermissionInteger.Visible = True
                    .lblCaption(97).Visible = True
                Else
                    .txtDriveWriterPermissionInteger.Visible = False
                    .lblCaption(97).Visible = False
                End If
            Else
                .cmdVerifyDiscstore.Visible = False
                .chkCanBeUsedForAutoDelete.Visible = False
                .chkUseForLiveCFM.Visible = False
                .chkIncludeInStorageReport.Visible = False
                .chkNoRestores.Visible = False
                .chkRequireCompanyIDinFolders.Visible = False
                .lblCaption(9).Caption = "Sub Title"
                .lblCaption(1).Caption = "Foreign Ref"
                .lblCaption(4).Caption = "Version"
                .txtDriveWriterPermissionInteger.Visible = False
                .lblCaption(97).Visible = False
            End If
            
        End With
        frmLibrary.grdEvents.Enabled = True
        'If g_optShowAdditionalEventFields <> 0 Then frmLibrary.tabLibraryInfo.Tab = 0
    Else
        If frmLibrary.txtBarcode.Text <> "" Then
            frmLibrary.lblDeletedTape.Visible = True
            frmLibrary.Show
            frmLibrary.txtBarcode.SetFocus
            HighLite frmLibrary.txtBarcode
            
            frmLibrary.lblTechReviewAvalable.Visible = False
            
            If g_optShowAdditionalEventFields <> 0 Then frmLibrary.tabLibraryInfo.Tab = 0
            frmLibrary.ZOrder 0
            Exit Sub
        End If
        'frmLibrary.Caption = "Library Details"
        ClearFields frmLibrary
        If g_optShowAdditionalEventFields <> 0 Then frmLibrary.tabLibraryInfo.Tab = 0
        frmLibrary.grdEvents.Enabled = False
        frmLibrary.cmdInitialise.Visible = False
        frmLibrary.cmdVerifyDiscstore.Visible = False
        frmLibrary.chkCanBeUsedForAutoDelete.Visible = False
        frmLibrary.chkUseForLiveCFM.Visible = False
        frmLibrary.chkIncludeInStorageReport.Visible = False
        frmLibrary.chkNoRestores.Visible = False
        frmLibrary.chkCanBeUsedForAutoDelete.Visible = False
        frmLibrary.lblCaption(9).Caption = "Sub Title"
        frmLibrary.lblCaption(1).Caption = "Foreign Ref"
        frmLibrary.lblCaption(4).Caption = "Version"
    End If
    
    l_rstLibrary.Close
    Set l_rstLibrary = Nothing
    
    If l_blnFoundOne = True Then
    
        'load the events
        l_strSQL = "SELECT * from tapeevents WHERE libraryID = " & lp_lngLibraryID & " ORDER BY timecodestart, eventID"
        frmLibrary.adoEvents.RecordSource = l_strSQL
        frmLibrary.adoEvents.ConnectionString = g_strConnection
        frmLibrary.adoEvents.Refresh
        frmLibrary.grdEvents.Refresh
            
        'load the required media jos
        l_strSQL = "SELECT requiredmedia.libraryID, requiredmedia.jobID AS jobID, job.fd_status AS jobstatus FROM requiredmedia INNER JOIN job ON requiredmedia.jobID = job.jobID WHERE libraryID = " & lp_lngLibraryID & " ORDER BY requiredmedia.cdate DESC;"
        frmLibrary.adoJobsForTape.RecordSource = l_strSQL
        frmLibrary.adoJobsForTape.ConnectionString = g_strConnection
        frmLibrary.adoJobsForTape.Refresh
        frmLibrary.grdJobsForTape.Refresh
            
        'check if Tech Review exists
        
        l_strSQL = "SELECT tapetechrevID FROM tapetechrev WHERE system_deleted = 0 AND libraryID = " & lp_lngLibraryID & " ORDER BY tapetechrevID"
        
        Set l_rstLibrary = ExecuteSQL(l_strSQL, g_strExecuteError)
        CheckForSQLError
        If lp_lngLibraryID > 0 Then
            If Not l_rstLibrary.EOF Then
                
                'Found a tech review for that tape
                l_rstLibrary.MoveLast
                l_lngtechrevID = l_rstLibrary("tapetechrevID")
                frmLibrary.lblLastTechrevID.Caption = l_lngtechrevID
                
                l_rstLibrary.Close
                Set l_rstLibrary = Nothing
                
                'Show the lastest Techrevies.
                ShowTechrev (l_lngtechrevID)
                
                frmLibrary.lblTechReviewAvalable.Visible = True
                
            Else
                'no tech review for this tape, so
                'make the tech review controls invisible, and the 'Make tech review' button enabled.
                
                l_lngtechrevID = 0
                frmLibrary.lbltechrevID.Caption = l_lngtechrevID
                frmLibrary.lblLastTechrevID.Caption = l_lngtechrevID
                frmLibrary.lbltechrevID.Visible = False
                frmLibrary.grdFaults.Visible = False
                frmLibrary.fraTechReview.Visible = False
                frmLibrary.cmdMakeTechReview.Enabled = True
                frmLibrary.cmdTechReviewPdf.Enabled = False
                frmLibrary.cmdTechReviewCopyFaults.Enabled = False
                frmLibrary.fraTechReviewConclusions.Visible = False
                frmLibrary.lblTechReviewAvalable.Visible = False
                
            End If
        Else
            l_lngtechrevID = 0
            frmLibrary.lbltechrevID.Caption = l_lngtechrevID
            frmLibrary.lblLastTechrevID.Caption = l_lngtechrevID
            frmLibrary.lbltechrevID.Visible = False
            frmLibrary.grdFaults.Visible = False
            frmLibrary.fraTechReview.Visible = False
            frmLibrary.cmdMakeTechReview.Enabled = False
            frmLibrary.cmdTechReviewPdf.Enabled = False
            frmLibrary.cmdTechReviewCopyFaults.Enabled = False
            frmLibrary.fraTechReviewConclusions.Visible = False
            frmLibrary.datMadeOnDate.Value = Date
            frmLibrary.datMadeOnDate.Value = Null
            frmLibrary.lblTechReviewAvalable.Visible = False
        End If
        
        If frmLibrary.lblLibraryID.Caption <> "" Then
            l_strSQL = "SELECT tapetechrevID, reviewdate, reviewUser FROM tapetechrev WHERE system_deleted = 0 AND libraryID = " & frmLibrary.lblLibraryID.Caption
        Else
            l_strSQL = "SELECT tapetechrevID, reviewdate, reviewUser FROM tapetechrev WHERE libraryID = 0"
        End If
        
        frmLibrary.adoTechReviews.ConnectionString = g_strConnection
        frmLibrary.adoTechReviews.RecordSource = l_strSQL
        frmLibrary.adoTechReviews.Refresh
        
        If InStr(GetData("company", "cetaclientcode", "companyID", frmLibrary.lblCompanyID.Caption), "/BBCSTATIONARY") > 0 Then
            frmLibrary.optStationaryType(1).Value = True
        ElseIf InStr(GetData("company", "cetaclientcode", "companyID", frmLibrary.lblCompanyID.Caption), "/BLANKSTATIONARY") > 0 Then
            frmLibrary.optStationaryType(2).Value = True
        ElseIf InStr(GetData("company", "cetaclientcode", "companyID", frmLibrary.lblCompanyID.Caption), "/LONGTITLESTATIONARY") > 0 Then
            frmLibrary.optStationaryType(4).Value = True
        Else
            frmLibrary.optStationaryType(0).Value = True
        End If
    
        If InStr(GetData("company", "cetaclientcode", "companyID", frmLibrary.lblCompanyID.Caption), "/shineversion") > 0 Then
            frmLibrary.lblCaption(73).Caption = "Title Code"
        Else
            frmLibrary.lblCaption(73).Caption = "Series ID"
        End If
            
    
    '    Select Case frmLibrary.lblCompanyID.Caption
    '
    '        Case 287, 596, 598, 448, 392, 570, 597, 599:
    '
    '        Case Else
    '
    '    End Select
    '
        If InStr(GetData("company", "cetaclientcode", "companyID", frmLibrary.lblCompanyID.Caption), "/notimecodeshuffle") Then frmLibrary.chkNoTimecodeShuffle.Value = 1
    
    End If
    
    frmLibrary.Show
    If frmLibrary.lblLibraryID.Caption <> "" Then frmLibrary.Library_Set_Framerate
    frmLibrary.txtBarcode.SetFocus
    HighLite frmLibrary.txtBarcode
    
    frmLibrary.ZOrder 0
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume 'PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub RollbackMovementError()


Dim l_strSQL As String
l_strSQL = "SELECT transID, destination FROM trans WHERE libraryID = " & frmLibrary.lblLibraryID.Caption & " ORDER BY transID DESC"

Dim l_rst As New ADODB.Recordset
Dim l_con As New ADODB.Connection

l_con.Open g_strConnection
l_rst.Open l_strSQL, l_con, adOpenDynamic, adLockReadOnly

Dim l_lngTransID As Long, l_strLocation  As String

If Not l_rst.EOF Then
    l_rst.MoveFirst
    l_lngTransID = Format(l_rst("transID"))
    If l_rst.EOF Then GoTo PROC_EOF
    l_rst.MoveNext
    l_strLocation = Format(l_rst("destination"))
End If

l_rst.Close
Set l_rst = Nothing

If l_lngTransID <> 0 Then

    l_con.Execute "DELETE FROM trans WHERE transID = '" & l_lngTransID & "';"
    
    If l_strLocation <> "" Then
        SetData "library", "location", "libraryID", frmLibrary.lblLibraryID.Caption, l_strLocation
        frmLibrary.cmbLocation.Text = l_strLocation
    End If
    
Else

PROC_EOF:
    MsgBox "Could not locate a transaction to roll back to", vbExclamation
    

End If


l_con.Close
Set l_con = Nothing


ShowLibrary Val(frmLibrary.lblLibraryID.Caption)

End Sub

Sub ShowLibrarySearch(Optional lp_strDefaultSearch As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/showlibrarysearch") Then
        Exit Sub
    End If
    
    'If IsFormLoaded("frmSearchLibrary") Then
    '    frmSearchLibrary.Show
    '    frmSearchLibrary.ZOrder 0
    '    Exit Sub
    'End If
    
    If lp_strDefaultSearch <> "" Then
        Load frmSearchLibrary
        
        Dim l_conSearch As ADODB.Connection
        Dim l_rstSearch As ADODB.Recordset
        
        Set l_conSearch = New ADODB.Connection
        Set l_rstSearch = New ADODB.Recordset
        
        l_conSearch.ConnectionString = g_strConnection
        l_conSearch.Open
        
        With l_rstSearch
            .CursorLocation = adUseClient
            .LockType = adLockBatchOptimistic
            .CursorType = adOpenDynamic
            .Open lp_strDefaultSearch, l_conSearch, adOpenDynamic
        End With
        
        l_rstSearch.ActiveConnection = Nothing
        
        Set frmSearchLibrary.grdLibrary.DataSource = l_rstSearch
        
        l_conSearch.Close
        Set l_conSearch = Nothing
        
    End If
    frmSearchLibrary.Show
    frmSearchLibrary.ZOrder 0
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub ShowTechrev(lp_lngTechrevID As Long)
    
Dim l_rstLibrary As ADODB.Recordset
Dim l_strSQL As String

l_strSQL = "SELECT * FROM tapetechrev WHERE system_deleted = 0 AND tapetechrevID = " & lp_lngTechrevID
Set l_rstLibrary = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError
If l_rstLibrary.EOF = True Then
    l_rstLibrary.Close
    Set l_rstLibrary = Nothing
    Exit Sub
Else
    l_rstLibrary.MoveFirst
    With frmLibrary
    
        .lbltechrevID.Caption = l_rstLibrary("tapetechrevID")
        .cmbTechReviewList.Text = CStr(l_rstLibrary("tapetechrevID"))
        .cmbReviewCompany.Text = Trim(" " & l_rstLibrary("companyname"))
        .lblReviewCompanyID.Caption = Trim(" " & l_rstLibrary("companyID"))
        If l_rstLibrary("companyID") <> Null Then .lblReviewCompanyID.Caption = l_rstLibrary("companyID")
        .cmbReviewContact.Text = Trim(" " & l_rstLibrary("contactname"))
        If l_rstLibrary("contactID") <> Null Then .lblReviewContactID.Caption = l_rstLibrary("contactID")
        .datReviewDate.Value = Format(l_rstLibrary("reviewdate"), vbShortDateFormat)
        .cmbReviewMachine.Text = Trim(" " & l_rstLibrary("reviewmachine"))
        .txtReviewJobID.Text = Trim(" " & l_rstLibrary("jobID"))
        .cmbReviewUser.Text = Trim(" " & l_rstLibrary("reviewuser"))
        .chkTitles.Value = GetFlag(l_rstLibrary("flagtitles"))
        .chkCaptions.Value = GetFlag(l_rstLibrary("flagcaptions"))
        .chkSubtitles.Value = GetFlag(l_rstLibrary("flagsubtitles"))
        .chkLogos.Value = GetFlag(l_rstLibrary("flaglogos"))
        .chkTextless.Value = GetFlag(l_rstLibrary("flagtextless"))
        .chkTrailers.Value = GetFlag(l_rstLibrary("flagtrailers"))
        .chkIdentInfoCorrect.Value = GetFlag(l_rstLibrary("identinfocorrect"))
        .chkConvertedMaterial.Value = GetFlag(l_rstLibrary("convertedmaterial"))
        .chkSpotCheckRev.Value = GetFlag(l_rstLibrary("fullQCreview"))
        .cmbTitlesLanguage.Text = Trim(" " & l_rstLibrary("languagetitles"))
        .cmbCaptionsLanguage.Text = Trim(" " & l_rstLibrary("languagecaptions"))
        .cmbSubtitlesLanguage.Text = Trim(" " & l_rstLibrary("languagesubtitles"))
        .txtDetailsLogos.Text = Trim(" " & l_rstLibrary("detailslogos"))
        .txtDetailsTextless.Text = Trim(" " & l_rstLibrary("detailstextless"))
        .txtDetailsTrailers.Text = Trim(" " & l_rstLibrary("detailstrailers"))
        .txtVideoTest.Text = Trim(" " & l_rstLibrary("videotest"))
        .txtChromaTest.Text = Trim(" " & l_rstLibrary("chromatest"))
        .txtBlackTest.Text = Trim(" " & l_rstLibrary("blacktest"))
        .txtPhaseTest.Text = Trim(" " & l_rstLibrary("phasetest"))
        .txtVideoProg.Text = Trim(" " & l_rstLibrary("videoprog"))
        .txtChromaProg.Text = Trim(" " & l_rstLibrary("chromaprog"))
        .txtBlackProg.Text = Trim(" " & l_rstLibrary("blackprog"))
        .txtPhaseProg.Text = Trim(" " & l_rstLibrary("phaseprog"))
        .txtA1Test.Text = Trim(" " & l_rstLibrary("audio1test"))
        .txtA2Test.Text = Trim(" " & l_rstLibrary("audio2test"))
        .txtA3Test.Text = Trim(" " & l_rstLibrary("audio3test"))
        .txtA4Test.Text = Trim(" " & l_rstLibrary("audio4test"))
        .txtA5Test.Text = Trim(" " & l_rstLibrary("audio5test"))
        .txtA6Test.Text = Trim(" " & l_rstLibrary("audio6test"))
        .txtA7Test.Text = Trim(" " & l_rstLibrary("audio7test"))
        .txtA8Test.Text = Trim(" " & l_rstLibrary("audio8test"))
        .txtA9Test.Text = Trim(" " & l_rstLibrary("audio9test"))
        .txtA10Test.Text = Trim(" " & l_rstLibrary("audio10test"))
        .txtA11Test.Text = Trim(" " & l_rstLibrary("audio11test"))
        .txtA12Test.Text = Trim(" " & l_rstLibrary("audio12test"))
        .txtCueTest.Text = Trim(" " & l_rstLibrary("cuetest"))
        .txtA1Prog.Text = Trim(" " & l_rstLibrary("audio1prog"))
        .txtA2Prog.Text = Trim(" " & l_rstLibrary("audio2prog"))
        .txtA3Prog.Text = Trim(" " & l_rstLibrary("audio3prog"))
        .txtA4Prog.Text = Trim(" " & l_rstLibrary("audio4prog"))
        .txtA5Prog.Text = Trim(" " & l_rstLibrary("audio5prog"))
        .txtA6Prog.Text = Trim(" " & l_rstLibrary("audio6prog"))
        .txtA7Prog.Text = Trim(" " & l_rstLibrary("audio7prog"))
        .txtA8Prog.Text = Trim(" " & l_rstLibrary("audio8prog"))
        .txtA9Prog.Text = Trim(" " & l_rstLibrary("audio9prog"))
        .txtA10Prog.Text = Trim(" " & l_rstLibrary("audio10prog"))
        .txtA11Prog.Text = Trim(" " & l_rstLibrary("audio11prog"))
        .txtA12Prog.Text = Trim(" " & l_rstLibrary("audio12prog"))
        .txtCueProg.Text = Trim(" " & l_rstLibrary("cueprog"))
        .cmbEBU128LoudnessStereoMain.Text = Trim(" " & l_rstLibrary("EBU128LoudnessStereoMain"))
        .cmbEBU128LoudnessStereoME.Text = Trim(" " & l_rstLibrary("EBU128LoudnessStereoME"))
        .cmbEBU128Loudness51Main.Text = Trim(" " & l_rstLibrary("EBU128Loudness51Main"))
        .cmbEBU128Loudness51ME.Text = Trim(" " & l_rstLibrary("EBU128Loudness51ME"))
        .txtHighestAudioPeak.Text = Trim(" " & l_rstLibrary("highestaudiopeak"))
        .chkCaptionsafe43ebu.Value = GetFlag(l_rstLibrary("captionsafe43ebu"))
        .chkCaptionsafe43protect.Value = GetFlag(l_rstLibrary("captionsafe43protect"))
        .chkCaptionsafe149.Value = GetFlag(l_rstLibrary("captionsafe149"))
        .chkCaptionsafe169.Value = GetFlag(l_rstLibrary("captionsafe169"))
        .chkltc.Value = GetFlag(l_rstLibrary("ltc"))
        .chkvitc.Value = GetFlag(l_rstLibrary("vitc"))
        .chkltcvitcmatch.Value = GetFlag(l_rstLibrary("ltcvitcmatch"))
        .chktimecodecontinuous.Value = GetFlag(l_rstLibrary("timecodecontinuous"))
        .txtvitclines.Text = Trim(" " & l_rstLibrary("vitclines"))
        .txtuserbits.Text = Trim(" " & l_rstLibrary("userbits"))
        .txtfirstactivelinefield1.Text = Trim(" " & l_rstLibrary("firstactivelinefield1"))
        .txtlastactivelinefield1.Text = Trim(" " & l_rstLibrary("lastactivelinefield1"))
        .txtfirstactivelinefield2.Text = Trim(" " & l_rstLibrary("firstactivelinefield2"))
        .txtlastactivelinefield2.Text = Trim(" " & l_rstLibrary("lastactivelinefield2"))
        .txtfirstactivepixel.Text = Trim(" " & l_rstLibrary("firstactivepixel"))
        .txtlastactivepixel.Text = Trim(" " & l_rstLibrary("lastactivepixel"))
        .chkhorizontalcenteringcorrect.Value = GetFlag(l_rstLibrary("horizontalcentreingcorrect"))
        .chkverticalcenteringcorrect.Value = GetFlag(l_rstLibrary("verticalcentreingcorrect"))
'        .txthorizblank.Text = Trim(" " & l_rstLibrary("horizblank"))
'        .txtvertblank.Text = Trim(" " & l_rstLibrary("vertblank"))
'        .txtbackporch.Text = Trim(" " & l_rstLibrary("backporch"))
'        .txtfrontporch.Text = Trim(" " & l_rstLibrary("frontporch"))
        .txtTechNotesVideo.Text = Trim(" " & l_rstLibrary("notesvideo"))
        .txtTechNotesAudio.Text = Trim(" " & l_rstLibrary("notesaudio"))
        .txtTechNotesGeneral.Text = Trim(" " & l_rstLibrary("notesgeneral"))
        .optReviewConclusion(l_rstLibrary("reviewconclusion")).Value = True
        If Val(Trim(" " & l_rstLibrary("passedforitunes"))) = 1 Then
            .optReviewConclusion(4).Value = True
        Else
            .optReviewConclusion(3).Value = True
        End If
        
        If .chkSpotCheckRev.Value <> 0 Then
            FullQCFieldsOn
        Else
            FullQCFieldsOff
        End If
        
    End With
    l_rstLibrary.Close
    Set l_rstLibrary = Nothing
        
    If frmLibrary.lbltechrevID.Caption = frmLibrary.lblLastTechrevID.Caption Then
            
        frmLibrary.grdFaults.Visible = True
        frmLibrary.grdFaults.Enabled = True
        frmLibrary.fraTechReview.Visible = True
        frmLibrary.fraTechReview.Enabled = True
        frmLibrary.cmdMakeTechReview.Enabled = True
        frmLibrary.cmdDropTechReview.Enabled = True
        frmLibrary.cmdTechReviewPdf.Enabled = True
        frmLibrary.cmdTechReviewCopyFaults.Enabled = True
        frmLibrary.fraTechReviewConclusions.Visible = True
        frmLibrary.fraTechReviewConclusions.Enabled = True
        frmLibrary.lbltechrevID.Visible = True
        frmLibrary.lblLastTechrevID.Visible = True
    
    Else
    
        frmLibrary.grdFaults.Visible = True
        frmLibrary.grdFaults.Enabled = False
        frmLibrary.fraTechReview.Visible = True
        frmLibrary.fraTechReview.Enabled = False
        frmLibrary.cmdMakeTechReview.Enabled = False
        frmLibrary.cmdDropTechReview.Enabled = True
        frmLibrary.cmdTechReviewPdf.Enabled = True
        frmLibrary.cmdTechReviewCopyFaults.Enabled = True
        frmLibrary.fraTechReviewConclusions.Visible = True
        frmLibrary.fraTechReviewConclusions.Enabled = False
        frmLibrary.lbltechrevID.Visible = True
        frmLibrary.lblLastTechrevID.Visible = True
    
    End If
                
    'Load the tech review Faults grid.
    l_strSQL = "SELECT * FROM tapetechrevfault WHERE tapetechrevID = '" & lp_lngTechrevID & "' ORDER BY timecode"
    
    frmLibrary.adoFaults.RecordSource = l_strSQL
    frmLibrary.adoFaults.ConnectionString = g_strConnection
    frmLibrary.adoFaults.Refresh
    If CheckAccess("/savelibrary", True) = True Then frmLibrary.grdFaults.Enabled = True Else frmLibrary.grdFaults.Enabled = False

End If
End Sub

Public Sub UpdateLibraryRecordWithJobDetailInfo(ByVal lp_lngLibraryID As Long, ByVal lp_lngJobID As Long, ByVal lp_lngJobDetailID As Long, Optional lp_strUserInitials As String, Optional lp_strSourceMachine As String, Optional lp_strRecordMachine As String, Optional lp_strRecordsToUse As String)

Dim l_strSQL As String
Dim l_rstJobDetail As New ADODB.Recordset
Dim l_rstJob As New ADODB.Recordset

'get the jobdetail details
l_strSQL = "SELECT * FROM jobdetail WHERE jobdetailID = '" & lp_lngJobDetailID & "';"
Set l_rstJobDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

'get the job details
l_strSQL = "SELECT * FROM job WHERE jobID = '" & lp_lngJobID & "';"
Set l_rstJob = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

'what user initials do we use?
Dim l_strUserInitials As String
If lp_strUserInitials <> "" Then
    l_strUserInitials = lp_strUserInitials
Else
    l_strUserInitials = g_strUserInitials
End If

'put all the values in to varibles
Dim l_strCompanyName As String
Dim l_lngCompanyID As Long
Dim l_strProductName As String
Dim l_lngProductID As Long
Dim l_lngProjectID As Long
Dim l_strTitle As String
Dim l_strSubTitle As String
Dim l_strAspect As String
Dim l_strCopyType As String
Dim l_lngJobID As Long
Dim l_lngProjectNumber  As Long


Dim l_strVideoStandard As String

Dim l_strSourceBarcode As String
Dim l_strSourceFormat As String
Dim l_strSourceAspect As String
Dim l_strSourceStandard As String


If Not l_rstJob.EOF Then
    
    'get the details in to the variables
    l_strCompanyName = CStr(Format(l_rstJob!CompanyName))
    l_lngCompanyID = Val(Format(l_rstJob!companyID))
    l_strProductName = Format(l_rstJob!ProductName)
    l_lngProductID = Val(Format(l_rstJob!productID))
    l_lngProjectID = Val(Format(l_rstJob!projectID))
    l_strTitle = Format(l_rstJob!title1)
    l_strSubTitle = Format(l_rstJob!title2)
    l_strCopyType = "COPY" 'Format(l_rstJobDetail!copytype)
    
    
    
    If Not l_rstJobDetail.EOF Then
        l_strVideoStandard = Format(l_rstJobDetail!videostandard)
        l_strAspect = Format(l_rstJobDetail!aspectratio)
    End If
    
    l_lngJobID = l_rstJob!JobID
    l_lngProjectNumber = Format(l_rstJob!projectnumber)
    
    'update the library record with the job details
    l_strSQL = "UPDATE library SET projectnumber = '" & l_lngProjectNumber & "', companyname = '" & QuoteSanitise(l_strCompanyName) & "', companyID = '" & l_lngCompanyID & "', productname = '" & QuoteSanitise(l_strProductName) & "', productID = '" & l_lngProductID & _
                "', projectID = '" & l_lngProjectID & "', jobID = '" & lp_lngJobID & "', title = '" & QuoteSanitise(l_strTitle) & "', subtitle = '" & QuoteSanitise(l_strSubTitle) & "', aspectratio = '" & QuoteSanitise(l_strAspect) & "', copytype = '" & QuoteSanitise(l_strCopyType) & _
                "', videostandard = '" & QuoteSanitise(l_strVideoStandard) & "', sourcemachine = '" & QuoteSanitise(lp_strSourceMachine) & "', recordmachine = '" & QuoteSanitise(lp_strRecordMachine) & "', mdate = '" & FormatSQLDate(Now) & "', muser = '" & l_strUserInitials & "' WHERE libraryID = '" & lp_lngLibraryID & "';"
                
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    Dim l_lngNewLibraryID As Long
    l_lngNewLibraryID = lp_lngLibraryID
    
    Dim l_rsEvents As ADODB.Recordset
    
    'copy the master lines above the current copy line into the events for the new tape
    l_strSQL = "SELECT * FROM jobdetail WHERE jobID = '" & lp_lngJobID & "' ORDER BY jobdetailID ASC;"
    
    'get the list of dubs
    Set l_rsEvents = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    'dont do this if not being passed a job detail ID
    If lp_lngJobDetailID > 0 Then
        
        'go to the last one
        l_rsEvents.MoveLast
        
        'loop backwards until we find the current job
        Do While l_rsEvents("jobdetailid") <> lp_lngJobDetailID
            l_rsEvents.MovePrevious
            If l_rsEvents.BOF Then Exit Do
        Loop
        
        'now we should be on the copy job, so now we need to loop backwards until we either reach BOF
        'or another COPY line.
        If l_rsEvents.BOF <> True Then
        
            'start by moving back one
            l_rsEvents.MovePrevious
            
            Do
                
                'if we're at BOF then get out
                If l_rsEvents.BOF = True Then Exit Do
                
                'check to see if the previous line is a new job or a MASTER
                If UCase(l_rsEvents("copytype").Value) <> "M" Then
                
                    'we are at the previous C/D line, so move forward one again
                    l_rsEvents.MoveNext
                    
                    'and exit the loop
                    Exit Do
                    
                Else
                
                    'move to the previous one
                    l_rsEvents.MovePrevious
                    
                End If
            Loop
        End If
    
    End If
    
    
    If l_rsEvents.BOF = True And l_rsEvents.EOF <> True Then l_rsEvents.MoveFirst

    
    'now we loop back forwards until we get to the original C line
    Do Until l_rsEvents.EOF
        If (l_rsEvents("jobdetailid") = lp_lngJobDetailID) Then Exit Do
    
        'add the event
        If UCase(l_rsEvents("copytype").Value) <> "C" And UCase(l_rsEvents("copytype").Value) <> "D" Then
        
        If Not l_rstJobDetail.EOF Then
            l_strVideoStandard = Format(l_rstJobDetail!videostandard)
            l_strAspect = Format(l_rstJobDetail!aspectratio)
        
        Else
            l_strAspect = Format(l_rsEvents("aspectratio"))
        End If

        
            AddEventToTape l_lngNewLibraryID, Format(l_rsEvents("description").Value), "COPY", Format(l_rsEvents("clocknumber").Value), GetData("library", "libraryID", "barcode", l_rsEvents("librarybarcode").Value), l_strAspect, l_lngJobID, Format(l_rsEvents("runningtime").Value), Date
        End If
    
        l_rsEvents.MoveNext
        
        'do some nice bits
        DBEngine.Idle: DoEvents
    
    Loop
    
    l_rsEvents.Close
    Set l_rsEvents = Nothing

End If

'close all the connections
l_rstJob.Close
Set l_rstJob = Nothing

l_rstJobDetail.Close
Set l_rstJobDetail = Nothing

Dim l_strBarcode As String
l_strBarcode = GetData("library", "barcode", "libraryID", lp_lngLibraryID)

MsgBox "Library data successfully updated for barcode " & l_strBarcode & ".", vbInformation

End Sub
Sub CopyEventToLibraryID(lp_lngEventID As Long, lp_lngLibraryID As Long)

'copy the info from one job to another
Dim l_rstOriginalEvent As ADODB.Recordset
Dim l_rstNewEvent As ADODB.Recordset


'get the original job's details
Dim l_strSQL As String
l_strSQL = "SELECT * FROM tapeevents WHERE eventID = '" & lp_lngEventID & "';"

Set l_rstOriginalEvent = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

'allow adding of a new job
l_strSQL = "SELECT * FROM tapeevents WHERE eventID = '-1';"

Set l_rstNewEvent = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

Dim l_intLoop As Integer
Dim l_lngConflict As Long
    
Do While Not l_rstOriginalEvent.EOF
    
    l_rstNewEvent.AddNew
    
    l_rstNewEvent("libraryID") = lp_lngLibraryID
    
    For l_intLoop = 0 To l_rstOriginalEvent.Fields.Count - 1
        If l_rstOriginalEvent.Fields(l_intLoop).Name <> "libraryID" And l_rstOriginalEvent.Fields(l_intLoop).Name <> "eventID" Then
        
            l_rstNewEvent(l_intLoop) = l_rstOriginalEvent(l_intLoop)
        
        End If
    Next
    
    'blank out fields which are specific to the ORIGINAL event only
    
    l_rstNewEvent("cdate") = FormatSQLDate(Now)
    l_rstNewEvent("cuser") = g_strUserInitials
    
    l_rstNewEvent("mdate") = Null
    l_rstNewEvent("muser") = Null
    
    l_rstNewEvent.Update

    l_rstOriginalEvent.MoveNext

Loop

'MsgBox g_lngLastID, vbExclamation

'close the original record
l_rstOriginalEvent.Close
Set l_rstOriginalEvent = Nothing

'close the new record
l_rstNewEvent.Close
Set l_rstNewEvent = Nothing
            

End Sub
Function CreateLibraryItem(lp_strBarcode As String, lp_strTitle As String, Optional lp_despatchID As Long, Optional lp_lngReference As Long, Optional lp_lngCompanyID As Long, Optional lp_blnBlankStock As Boolean, _
    Optional lp_strEpisodeNumber As String, Optional lp_strEpisodeTitle As String, Optional lp_strFormat As String, Optional lp_strLanguage As String, Optional lp_strNotes As String) As Long
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_lngCompanyID As Long
    Dim l_strCompanyName As String
    
    If lp_despatchID <> 0 Then
        l_lngCompanyID = GetData("despatch", "billtocompanyID", "despatchID", lp_despatchID)
        l_strCompanyName = GetData("despatch", "billtocompanyname", "despatchID", lp_despatchID)
    ElseIf lp_lngCompanyID <> 0 Then
        l_lngCompanyID = lp_lngCompanyID
        l_strCompanyName = GetData("company", "name", "companyID", l_lngCompanyID)
    Else
        l_lngCompanyID = 1
        l_strCompanyName = "Unassigned Company"
    End If
    
    If lp_lngReference = 0 Then lp_lngReference = GetNextSequence("internalreference")
    
    l_strSQL = "INSERT INTO library ("
    l_strSQL = l_strSQL & "barcode, "
    l_strSQL = l_strSQL & "code128barcode, "
    l_strSQL = l_strSQL & "companyname, "
    l_strSQL = l_strSQL & "companyID, "
    If lp_blnBlankStock = True Then
        l_strSQL = l_strSQL & "copytype, "
        l_strSQL = l_strSQL & "BlankStock, "
    End If
    l_strSQL = l_strSQL & "title, "
    l_strSQL = l_strSQL & "cuser, "
    l_strSQL = l_strSQL & "cdate, "
    If Not IsEmpty(lp_strEpisodeTitle) Then
        l_strSQL = l_strSQL & "subtitle, "
    End If
    If Not IsEmpty(lp_strEpisodeNumber) Then
        l_strSQL = l_strSQL & "episode, "
    End If
    If Not IsEmpty(lp_strFormat) Then
        l_strSQL = l_strSQL & "format, "
    End If
    If Not IsEmpty(lp_strLanguage) Then
        l_strSQL = l_strSQL & "audiostandard, "
    End If
    If Not IsEmpty(lp_strNotes) Then
        l_strSQL = l_strSQL & "notes1, "
    End If
    l_strSQL = l_strSQL & "internalreference) VALUES ("
    l_strSQL = l_strSQL & "'" & lp_strBarcode & "', "
    l_strSQL = l_strSQL & "'" & CIA_CODE128(lp_strBarcode) & "', "
    l_strSQL = l_strSQL & "'" & l_strCompanyName & "', "
    l_strSQL = l_strSQL & "'" & l_lngCompanyID & "', "
    If lp_blnBlankStock = True Then
        l_strSQL = l_strSQL & "'BLANK STOCK', "
        l_strSQL = l_strSQL & "1, "
    End If
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strTitle) & "', "
    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "',"
    If Not IsEmpty(lp_strEpisodeTitle) Then
        l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strEpisodeTitle) & "', "
    End If
    If Not IsEmpty(lp_strEpisodeNumber) Then
        l_strSQL = l_strSQL & "'" & lp_strEpisodeNumber & "', "
    End If
    If Not IsEmpty(lp_strFormat) Then
        l_strSQL = l_strSQL & "'" & lp_strFormat & "', "
    End If
    If Not IsEmpty(lp_strLanguage) Then
        l_strSQL = l_strSQL & "'" & lp_strLanguage & "', "
    End If
    If Not IsEmpty(lp_strNotes) Then
        l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strNotes) & "', "
    End If
    l_strSQL = l_strSQL & "'" & lp_lngReference & "');"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    CreateLibraryItem = g_lngLastID
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function
Public Sub CreateLibraryTransaction(lp_lngLibraryID As Long, lp_strLocation As String, lp_lngDespatchID As Long, Optional lp_strUserInitialsToUse As String, Optional ByVal lp_lngBatchNumber As Long)
    
    'first check to see library item exists
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If RecordExists("library", "libraryID", lp_lngLibraryID) Then
        
        Dim l_strSQL As String
        l_strSQL = "INSERT INTO trans ("
        l_strSQL = l_strSQL & "libraryID, "
        l_strSQL = l_strSQL & "destination, "
        l_strSQL = l_strSQL & "despatchID, "
        l_strSQL = l_strSQL & "batchID, "
        l_strSQL = l_strSQL & "cdate, "
        l_strSQL = l_strSQL & "cuser) VALUES ("
        
        l_strSQL = l_strSQL & "'" & lp_lngLibraryID & "', "
        l_strSQL = l_strSQL & "'" & lp_strLocation & "', "
        l_strSQL = l_strSQL & "'" & lp_lngDespatchID & "', "
        l_strSQL = l_strSQL & "'" & lp_lngBatchNumber & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "'" & IIf(lp_strUserInitialsToUse = "", g_strUserInitials, lp_strUserInitialsToUse) & "');"
        
        ExecuteSQL l_strSQL, g_strExecuteError
        
        CheckForSQLError
        
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub
Function DeleteLibrary(lp_lngLibraryID As Long) As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/deletetapes") Then
        Exit Function
    End If
    
    Dim l_strSQL As String
    
'    l_strSQL = "DELETE FROM trans WHERE libraryID = '" & lp_lngLibraryID & "'"
'    ExecuteSQL l_strSQL, g_strExecuteError
'    CheckForSQLError
'    l_strSQL = "DELETE FROM tapeevents WHERE libraryID = '" & lp_lngLibraryID & "'"
'    ExecuteSQL l_strSQL, g_strExecuteError
'    CheckForSQLError

    l_strSQL = "UPDATE library SET system_deleted = 1 WHERE libraryID = '" & lp_lngLibraryID & "'"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError

    DeleteLibrary = True
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function UnDeleteLibrary(lp_lngLibraryID As Long) As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/undeletetapes") Then
        Exit Function
    End If
    
    Dim l_strSQL As String
    
'    l_strSQL = "DELETE FROM trans WHERE libraryID = '" & lp_lngLibraryID & "'"
'    ExecuteSQL l_strSQL, g_strExecuteError
'    CheckForSQLError
'    l_strSQL = "DELETE FROM tapeevents WHERE libraryID = '" & lp_lngLibraryID & "'"
'    ExecuteSQL l_strSQL, g_strExecuteError
'    CheckForSQLError

    l_strSQL = "UPDATE library SET system_deleted = 0 WHERE libraryID = '" & lp_lngLibraryID & "'"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError

    UnDeleteLibrary = True
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub CopyTechReviewFaults(ByVal lp_lngTechrevID As Long)
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/savelibrary") Then
        Exit Sub
    End If
    
    Dim l_strOtherTapeBarcode As String
    Dim l_lngOtherTapeID
    Dim l_lngOtherTechRevID As Long
    Dim l_strSQL As String, l_rsOldFaults As ADODB.Recordset
    
    l_lngOtherTechRevID = Val(InputBox("Please give the techRev ID of the Techrev to copy faults from"))
    
    If l_lngOtherTechRevID = 0 Then
        Exit Sub
    Else
        Set l_rsOldFaults = ExecuteSQL("SELECT * FROM tapetechrevfault WHERE tapetechrevID = '" & l_lngOtherTechRevID & "';", g_strExecuteError)
        CheckForSQLError
        If l_rsOldFaults.RecordCount > 0 Then
            l_rsOldFaults.MoveFirst
            Do While Not l_rsOldFaults.EOF
                l_strSQL = "INSERT INTO tapetechrevfault (tapetechrevID, timecode, description, endtimecode, faultgrade) VALUES ("
                l_strSQL = l_strSQL & "'" & lp_lngTechrevID & "', "
                l_strSQL = l_strSQL & "'" & l_rsOldFaults("timecode") & "', "
                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rsOldFaults("description")) & "', "
                l_strSQL = l_strSQL & "'" & l_rsOldFaults("endtimecode") & "', "
                l_strSQL = l_strSQL & "'" & l_rsOldFaults("faultgrade") & "')"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                l_rsOldFaults.MoveNext
            Loop
        End If
        l_rsOldFaults.Close
        Set l_rsOldFaults = Nothing
    End If
    
    frmLibrary.adoFaults.Refresh
    
    'Ask to see is they also want to copy the review conclusions comments.
    Dim l_intResponse As Integer
    Dim l_strComment As String
    
    l_intResponse = MsgBox("Do you wish to also copy the tech review Comments?", vbYesNo, "Copy From...")
    If l_intResponse = vbYes Then
        l_strComment = GetData("tapetechrev", "notesvideo", "tapetechrevID", l_lngOtherTechRevID)
        SetData "tapetechrev", "notesvideo", "tapetechrevID", lp_lngTechrevID, QuoteSanitise(l_strComment)
        frmLibrary.txtTechNotesVideo.Text = l_strComment
        l_strComment = GetData("tapetechrev", "Notesaudio", "tapetechrevID", l_lngOtherTechRevID)
        SetData "tapetechrev", "notesaudio", "tapetechrevID", lp_lngTechrevID, QuoteSanitise(l_strComment)
        frmLibrary.txtTechNotesAudio.Text = l_strComment
        l_strComment = GetData("tapetechrev", "notesgeneral", "techrevID", l_lngOtherTechRevID)
        SetData "tapetechrev", "notesgeneral", "tapetechrevID", lp_lngTechrevID, QuoteSanitise(l_strComment)
        frmLibrary.txtTechNotesGeneral.Text = l_strComment
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub DeleteTechReview(lp_lngTechrevID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_lngLibraryID As Long
    
    If lp_lngTechrevID <> 0 Then
        
'        l_lngLibraryID = GetData("techrev", "libraryID", "techrevID", lp_lngTechrevID)
'        l_strSQL = "DELETE from techrevfault WHERE techrevID = " & lp_lngTechrevID
'        ExecuteSQL l_strSQL, g_strExecuteError
'        CheckForSQLError
        l_strSQL = "UPDATE tapetechrev SET system_deleted = 1 WHERE techrevID = " & lp_lngTechrevID
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        ShowLibrary l_lngLibraryID
        
    End If
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub DuplicateTape(Optional lp_blnBulk As Boolean)

    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/savelibrary") Then
        Exit Sub
    End If
    
    Dim l_strNewLibraryBarcode As String
    Dim l_lngNewLibraryID As Long
    Dim l_strSQL As String
    Dim l_lngTapesToMake As Long
    
    If lp_blnBulk = True Then
    
        l_lngTapesToMake = Val(InputBox("How Many Tapes", "Bulk Duplication of this Tape", 1))
        If MsgBox("About to make " & l_lngTapesToMake & " duplicates of this Tape" & vbCrLf & "Is this correct", vbYesNo, "Bulk Duplication of this Tape") = vbNo Then Exit Sub
                
    Else
    
        l_lngTapesToMake = 1
        
    End If
        
    Do While l_lngTapesToMake > 0
    
            frmGetBarcodeNumber.Caption = "Please give the Barcode to duplicate to..."
            If g_optAllocateBarcodes = 1 Then frmGetBarcodeNumber.txtBarcode.Text = g_strCompanyPrefix & GetNextSequence("barcodenumber")
            frmGetBarcodeNumber.Show vbModal
            
            l_strNewLibraryBarcode = UCase(frmGetBarcodeNumber.txtBarcode.Text)
            Unload frmGetBarcodeNumber
        
        'Check whether the new barcode was accepted, or escape was pressed.
        If l_strNewLibraryBarcode = "" Then
            Exit Sub
        End If
        
        l_lngNewLibraryID = GetData("library", "LibraryID", "barcode", l_strNewLibraryBarcode)
        
        If l_lngNewLibraryID <> 0 Then
            Dim l_strTapeLocation As String, l_strNewTapeFormat As String, l_strNewTapeStockCode As String, l_strNewTapeTitle As String
            
            l_strTapeLocation = GetData("library", "location", "barcode", l_strNewLibraryBarcode)
            l_strNewTapeFormat = GetData("library", "format", "barcode", l_strNewLibraryBarcode)
            l_strNewTapeStockCode = GetData("library", "stocktype", "barcode", l_strNewLibraryBarcode)
            l_strNewTapeTitle = GetData("library", "title", "barcode", l_strNewLibraryBarcode)
            If UCase(l_strNewTapeTitle) = "BLANK STOCK" Then
            
                'DeleteLibrary l_lngNewLibraryID
                
                'l_strNewTapeFormat = GetData("library", "format", "barcode", l_strNewLibraryBarcode)
                'l_strNewTapeStockCode = GetData("library", "stocktype", "barcode", l_strNewLibraryBarcode)
                
                'frmLibrary.cmbRecordFormat.Text = l_strNewTapeFormat
                'frmLibrary.cmbStockCode.Text = l_strNewTapeStockCode
                
                GoTo PROC_AddTape
            End If
            
            MsgBox "Tape Already Exists.", vbExclamation, "Error"
            Exit Sub
        
        Else
        
            l_strNewTapeFormat = frmLibrary.cmbRecordFormat.Text
            l_strNewTapeStockCode = frmLibrary.cmbStockCode.Text
        
PROC_AddTape:
    
            'Copy the Tape info.
            frmLibrary.MousePointer = vbHourglass
            
            Dim l_lngOldLibraryID As Long
            l_lngOldLibraryID = frmLibrary.lblLibraryID.Caption
            
            
            'clears the important fields and saves a new tape
            frmLibrary.lblLibraryID.Caption = l_lngNewLibraryID
            frmLibrary.txtBarcode.Text = l_strNewLibraryBarcode
            frmLibrary.txtInternalReference.Text = ""
            frmLibrary.txtReference.Text = ""
            
            DoEvents
            If g_optAddFakeJobIDToTapes = 1 And lp_blnBulk = False Then
                frmLibrary.txtJobID.Text = "999999"
            End If
            
            frmLibrary.lbltechrevID.Caption = ""
            frmLibrary.cmbTechReviewList.Text = ""
            
            
            SaveLibrary
            
            l_lngNewLibraryID = GetData("library", "LibraryID", "barcode", l_strNewLibraryBarcode)
            
            If lp_blnBulk = False Then
                
                If MsgBox("Do you also want to copy the events?", vbYesNo + vbQuestion) = vbYes Then
                
                    If CopyAllEvents(l_lngOldLibraryID, l_lngNewLibraryID) = False Then MsgBox "Copy of events failed", vbExclamation
                
                End If
            
            Else
                
                CopyAllEvents l_lngOldLibraryID, l_lngNewLibraryID
            
            End If
                
        End If
        
        'create a transaction
        CreateLibraryTransaction l_lngNewLibraryID, "Duplicated", 0
        ShowLibrary l_lngNewLibraryID
        If lp_blnBulk = True Then
            frmLibrary.cmdPrintSetOfTwoThings.Value = True
        End If
        
        l_lngTapesToMake = l_lngTapesToMake - 1
        
    Loop
            
    'refresh the form
    ShowLibrary l_lngNewLibraryID
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Function CopyAllEvents(ByVal lp_lngOldLibraryID As Long, ByVal lp_lngNewLibraryID As Long) As Boolean

On Error GoTo PROC_CopyAllEvents_Error

Dim l_con As New ADODB.Connection
Dim l_rst As New ADODB.Recordset

Dim l_strSQL As String, l_strNewCode As String

l_con.Open g_strConnection
l_rst.Open "SELECT * FROM tapeevents WHERE libraryID = '" & lp_lngOldLibraryID & "' ORDER BY eventID", l_con, adOpenForwardOnly, adLockReadOnly

If Not l_rst.EOF Then
    l_rst.MoveFirst
    Do While Not l_rst.EOF
        With l_rst
            
            l_strSQL = "INSERT INTO tapeevents ("
            l_strSQL = l_strSQL & "libraryID, "
            l_strSQL = l_strSQL & "timecodestart, "
            l_strSQL = l_strSQL & "timecodestop, "
            l_strSQL = l_strSQL & "break1, "
            l_strSQL = l_strSQL & "break2, "
            l_strSQL = l_strSQL & "break3, "
            l_strSQL = l_strSQL & "break4, "
            l_strSQL = l_strSQL & "break5, "
            l_strSQL = l_strSQL & "endcredits, "
            l_strSQL = l_strSQL & "break1elapsed, "
            l_strSQL = l_strSQL & "break2elapsed, "
            l_strSQL = l_strSQL & "break3elapsed, "
            l_strSQL = l_strSQL & "break4elapsed, "
            l_strSQL = l_strSQL & "break5elapsed, "
            l_strSQL = l_strSQL & "endcreditselapsed, "
            l_strSQL = l_strSQL & "eventtitle, "
            l_strSQL = l_strSQL & "fd_length, "
            l_strSQL = l_strSQL & "eventdate, "
            l_strSQL = l_strSQL & "clocknumber, "
            l_strSQL = l_strSQL & "eventtype, "
            l_strSQL = l_strSQL & "aspectratio, "
            l_strSQL = l_strSQL & "notes, "
            l_strSQL = l_strSQL & "eventmediatype, "
            l_strSQL = l_strSQL & "cdate, "
            l_strSQL = l_strSQL & "cuser) VALUES ("
            l_strSQL = l_strSQL & "'" & lp_lngNewLibraryID & "', "
            l_strNewCode = Trim(" " & .Fields("timecodestart"))
            If Mid(l_strNewCode, 9, 1) = ";" Then Mid(l_strNewCode, 9, 1) = ":"
            l_strSQL = l_strSQL & "'" & l_strNewCode & "', "
            l_strNewCode = Trim(" " & .Fields("timecodestop"))
            If Mid(l_strNewCode, 9, 1) = ";" Then Mid(l_strNewCode, 9, 1) = ":"
            l_strSQL = l_strSQL & "'" & l_strNewCode & "', "
            l_strSQL = l_strSQL & "'" & Trim(" " & .Fields("break1")) & "', "
            l_strSQL = l_strSQL & "'" & Trim(" " & .Fields("break2")) & "', "
            l_strSQL = l_strSQL & "'" & Trim(" " & .Fields("break3")) & "', "
            l_strSQL = l_strSQL & "'" & Trim(" " & .Fields("break4")) & "', "
            l_strSQL = l_strSQL & "'" & Trim(" " & .Fields("break5")) & "', "
            l_strSQL = l_strSQL & "'" & Trim(" " & .Fields("endcredits")) & "', "
            l_strSQL = l_strSQL & "'" & Trim(" " & .Fields("break1elapsed")) & "', "
            l_strSQL = l_strSQL & "'" & Trim(" " & .Fields("break2elapsed")) & "', "
            l_strSQL = l_strSQL & "'" & Trim(" " & .Fields("break3elapsed")) & "', "
            l_strSQL = l_strSQL & "'" & Trim(" " & .Fields("break4elapsed")) & "', "
            l_strSQL = l_strSQL & "'" & Trim(" " & .Fields("break5elapsed")) & "', "
            l_strSQL = l_strSQL & "'" & Trim(" " & .Fields("endcreditselapsed")) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.Fields("eventtitle")) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.Fields("fd_length")) & "', "
            l_strSQL = l_strSQL & "'" & FormatSQLDate(.Fields("eventdate")) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.Fields("clocknumber")) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.Fields("eventtype")) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.Fields("aspectratio")) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.Fields("notes")) & "', "
            l_strSQL = l_strSQL & "'TAPE', "
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "'" & g_strUserInitials & "')"
            
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            
            .MoveNext
        End With
    Loop
End If


l_rst.Close
Set l_rst = Nothing
l_con.Close
Set l_con = Nothing

CopyAllEvents = True

Exit Function

PROC_CopyAllEvents_Error:

CopyAllEvents = False

Exit Function
End Function

Function EditBarcode()
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/savelibrary") Then
        Exit Function
    End If
    
    Dim l_strSQL As String
    Dim l_strNewBarcode As String
    Dim l_strOldBarcode As String
    Dim l_lngCheckTapeID As Long
    
    'Check we are on a valid tape:
    If Val(frmLibrary.lblLibraryID.Caption) <> 0 Then
        If g_optAllocateBarcodes = 1 Then frmGetBarcodeNumber.txtBarcode.Text = g_strCompanyPrefix & GetNextSequence("barcodenumber")
        frmGetBarcodeNumber.Caption = "Please confirm or edit new tape barcode"
        frmGetBarcodeNumber.Show vbModal
        l_strNewBarcode = UCase(frmGetBarcodeNumber.txtBarcode.Text)
        Unload frmGetBarcodeNumber
        If l_strNewBarcode = "" Then
            Exit Function
        Else
            'check whether that barcode doesn't exist:
            l_strOldBarcode = frmLibrary.txtBarcode.Text
            l_lngCheckTapeID = GetData("library", "LibraryID", "barcode", l_strNewBarcode)
            If l_lngCheckTapeID = 0 Then
                frmLibrary.txtBarcode.Text = l_strNewBarcode
                l_strSQL = "UPDATE library SET "
                l_strSQL = l_strSQL & "barcode = '" & l_strNewBarcode & "', "
                l_strSQL = l_strSQL & "code128barcode = '" & CIA_CODE128(l_strNewBarcode) & "' "
                l_strSQL = l_strSQL & "WHERE libraryID = " & frmLibrary.lblLibraryID.Caption
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                l_strSQL = "INSERT INTO libraryrenumber (libraryID, oldbarcode, newbarcode, cdate, cuser) VALUES ("
                l_strSQL = l_strSQL & "'" & frmLibrary.lblLibraryID.Caption & "', "
                l_strSQL = l_strSQL & "'" & l_strOldBarcode & "', "
                l_strSQL = l_strSQL & "'" & l_strNewBarcode & "', "
                l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                l_strSQL = l_strSQL & "'" & g_strUserInitials & "');"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            Else
                MsgBox "Tape Already Exists.", vbExclamation, "Error"
            End If
        End If
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function
Sub NoEventSelectedMessage()
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    MsgBox "Please select a valid event first", vbExclamation
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Public Sub ReadSubtitleFile(lp_lngLibraryID As Long)

Dim l_strFilename As String, l_lngTotalSubtitles
Dim l_lngSubtitle As Long, l_strSQL As String
Dim l_lngCount As Long, l_bytInput As Byte, l_strInTime As String, l_strOutTime As String, l_strText As String, l_strCleanText As String
Dim l_blnEndTitle As Boolean, l_blnFirstLF As Boolean

'Open up the file
MDIForm1.dlgMain.Filter = "STL Files|*.STL"
MDIForm1.dlgMain.ShowOpen

l_strFilename = MDIForm1.dlgMain.Filename

If l_strFilename <> "" Then

    Open l_strFilename For Binary Access Read As 1
    
    'Read in and process the total number of subtitles
    For l_lngCount = 0 To 242
        Get #1, , l_bytInput
    Next
    
    l_strText = ""
    
    For l_lngCount = 243 To 247
        Get #1, , l_bytInput
        l_strText = l_strText & Chr(l_bytInput)
    Next
    
    Debug.Print "Total Subtitles:";
    
    l_lngTotalSubtitles = Val(l_strText)
    
    Debug.Print l_lngTotalSubtitles
    
    'Read away the rest of the Header
    For l_lngCount = 248 To 1023
        Get #1, , l_bytInput
    Next
    
    'For each subtitle read in the information
    
    For l_lngSubtitle = 1 To l_lngTotalSubtitles
        l_strText = "": l_strCleanText = ""
        
        frmLibrary.lblProcessingSubtitle.Caption = "Processing Subtitle " & l_lngSubtitle
        DoEvents
        l_blnEndTitle = False

        Do While l_blnEndTitle = False
            For l_lngCount = 0 To 3
                Get #1, , l_bytInput
            Next
            
            If l_bytInput = 255 Then l_blnEndTitle = True
            
            Get #1, , l_bytInput
            
            'Get the InTime
            Get #1, , l_bytInput
            l_strInTime = Format(l_bytInput, "00") & ":"
            Get #1, , l_bytInput
            l_strInTime = l_strInTime & Format(l_bytInput, "00") & ":"
            Get #1, , l_bytInput
            l_strInTime = l_strInTime & Format(l_bytInput, "00") & ":"
            Get #1, , l_bytInput
            l_strInTime = l_strInTime & Format(l_bytInput, "00")
            
            'Get the OutTime
            Get #1, , l_bytInput
            l_strOutTime = Format(l_bytInput, "00") & ":"
            Get #1, , l_bytInput
            l_strOutTime = l_strOutTime & Format(l_bytInput, "00") & ":"
            Get #1, , l_bytInput
            l_strOutTime = l_strOutTime & Format(l_bytInput, "00") & ":"
            Get #1, , l_bytInput
            l_strOutTime = l_strOutTime & Format(l_bytInput, "00")
            
            For l_lngCount = 13 To 15
                Get #1, , l_bytInput
            Next
            
            'get the Subtitle text
            For l_lngCount = 16 To 127
                Get #1, , l_bytInput
                l_strText = l_strText & Chr(l_bytInput)
                If l_bytInput > 31 Then
                    If l_bytInput <> 138 Then
                        Select Case l_bytInput
                        Case 173
                            l_strCleanText = l_strCleanText & "-"
                        Case 160
                            l_strCleanText = l_strCleanText & " "
                        Case 127 To 159
                        Case 191 To 222
                        Case 251 To 255
                        Case Else
                            l_strCleanText = l_strCleanText & Chr(l_bytInput)
                        End Select
                        l_blnFirstLF = True
                    Else
                        If l_blnFirstLF = True Then
                            l_strCleanText = l_strCleanText & Chr(10)
                            l_blnFirstLF = False
                        End If
                    End If
                End If
            Next
        Loop
        
        l_strSQL = "INSERT INTO ebusubtitle (libraryID, subtitlenumber, intime, outtime, originaltext, sanitisedtext) VALUES ("
        l_strSQL = l_strSQL & "'" & lp_lngLibraryID & "', "
        l_strSQL = l_strSQL & "'" & l_lngSubtitle & "', "
        l_strSQL = l_strSQL & "'" & l_strInTime & "', "
        l_strSQL = l_strSQL & "'" & l_strOutTime & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strText) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strCleanText) & "');"
        
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
    Next
    
End If

Close #1

End Sub

Sub ClearSubtitles(lp_lngLibraryID As Long)

Dim l_strSQL As String

l_strSQL = "DELETE FROM ebusubtitle WHERE libraryID = '" & lp_lngLibraryID & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Sub

Public Function ShowCorrectTechRevParameters()

'Put the correct labels in the tech parameters box, depending on the Videostandard of the tape.
'The information is stored in the xref table, in the additional info column of the videostandard category

Dim l_rstTemporary As ADODB.Recordset, l_strParameter As String, l_strInfo As String, l_lngCounter As Long, l_strSQL As String

l_strSQL = "SELECT * from xref WHERE category = 'digiparameters' AND description = '" & frmLibrary.cmbRecordStandard.Text & "'"
Set l_rstTemporary = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError
If l_rstTemporary.EOF <> True Then
    'Information exists - 6 parameters in the form param/param/param/param/param/param/
    l_rstTemporary.MoveFirst
    l_strParameter = ""
    l_strInfo = l_rstTemporary("information")
    With frmLibrary
        Dim l_intIndex
        For l_intIndex = 0 To 5
            l_lngCounter = InStr(1, l_strInfo, "/", vbTextCompare)
            If l_lngCounter <> 0 Then l_strParameter = Left(l_strInfo, l_lngCounter - 1)
            .lblParameters(l_intIndex).Caption = l_strParameter
            l_strInfo = Right(l_strInfo, Len(l_strInfo) - l_lngCounter)
        Next
    End With
Else
    'Information doesn't exist, so set the labels for 625 line PAL.
    With frmLibrary
        .lblParameters(0).Caption = "1st Line (23)"
        .lblParameters(1).Caption = "Last Line (310)"
        .lblParameters(2).Caption = "1st Line (336)"
        .lblParameters(3).Caption = "Last Line (623)"
        .lblParameters(4).Caption = "1st Pixel (0 or 18)"
        .lblParameters(5).Caption = "Last Pixel (1421 or 1439)"
        .chkCaptionsafe43protect.Visible = True
    End With
End If
l_rstTemporary.Close
    
'Show the correct set of technical parameters depending on the format of the tape - whether
'it is DIGI or not is stored in the format field of the xref table, category - format.

l_strSQL = "SELECT * FROM xref WHERE category = 'format' AND description = '" & frmLibrary.cmbRecordFormat.Text & "'"
Set l_rstTemporary = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError
If l_rstTemporary.EOF <> True Then
    l_rstTemporary.MoveFirst
'    If l_rstTemporary("Format") = "DIGI" Then
'        frmLibrary.fraAnalogParameters.Visible = False
        frmLibrary.fraDigitalParameters.Visible = True
'    Else
'        frmLibrary.fraAnalogParameters.Visible = True
'        frmLibrary.fraDigitalParameters.Visible = False
'    End If
End If
l_rstTemporary.Close
Set l_rstTemporary = Nothing

End Function

Public Sub FullQCFieldsOn()

Dim l_strSQL As String, l_rstTemporary As ADODB.Recordset

frmLibrary.fraTimecode.Visible = True
frmLibrary.fraSoundLevels.Visible = True
frmLibrary.fraVideoLevels.Visible = True
frmLibrary.fraCaptionSafe.Visible = True

'Show the correct set of technical parameters depending on the format of the tape - whether
'it is DIGI or not is stored in the format field of the xref table, category - format.

l_strSQL = "SELECT * FROM xref WHERE category = 'format' AND description = '" & frmLibrary.cmbRecordFormat.Text & "'"
Set l_rstTemporary = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError
If l_rstTemporary.EOF <> True Then
    l_rstTemporary.MoveFirst
'    If l_rstTemporary("Format") = "DIGI" Then
'        frmLibrary.fraAnalogParameters.Visible = False
        frmLibrary.fraDigitalParameters.Visible = True
'    Else
'        frmLibrary.fraAnalogParameters.Visible = True
'        frmLibrary.fraDigitalParameters.Visible = False
'    End If
End If
l_rstTemporary.Close
Set l_rstTemporary = Nothing

DoEvents

End Sub

Public Sub FullQCFieldsOff()

frmLibrary.fraTimecode.Visible = False
frmLibrary.fraSoundLevels.Visible = False
frmLibrary.fraVideoLevels.Visible = False
frmLibrary.fraCaptionSafe.Visible = False
frmLibrary.fraDigitalParameters.Visible = False

DoEvents

End Sub

