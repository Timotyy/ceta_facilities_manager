VERSION 5.00
Begin VB.Form frmAdditionalControls 
   Caption         =   "Additional Controls"
   ClientHeight    =   6405
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   9195
   LinkTopic       =   "Form1"
   ScaleHeight     =   6405
   ScaleWidth      =   9195
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtBreak6elapsed 
      ForeColor       =   &H000000FF&
      Height          =   315
      Left            =   6660
      TabIndex        =   11
      ToolTipText     =   "End timecode"
      Top             =   1860
      Width           =   1095
   End
   Begin VB.TextBox txtBreak6 
      Height          =   315
      Left            =   5340
      TabIndex        =   10
      ToolTipText     =   "End timecode"
      Top             =   1860
      Width           =   1095
   End
   Begin VB.TextBox txtBreak5 
      Height          =   315
      Left            =   5340
      TabIndex        =   9
      ToolTipText     =   "End timecode"
      Top             =   1500
      Width           =   1095
   End
   Begin VB.TextBox txtBreak4 
      Height          =   315
      Left            =   5340
      TabIndex        =   8
      ToolTipText     =   "End timecode"
      Top             =   1140
      Width           =   1095
   End
   Begin VB.TextBox txtBreak3 
      Height          =   315
      Left            =   2100
      TabIndex        =   7
      ToolTipText     =   "End timecode"
      Top             =   1860
      Width           =   1095
   End
   Begin VB.TextBox txtBreak2 
      Height          =   315
      Left            =   2100
      TabIndex        =   6
      ToolTipText     =   "End timecode"
      Top             =   1500
      Width           =   1095
   End
   Begin VB.TextBox txtBreak1 
      Height          =   315
      Left            =   2100
      TabIndex        =   5
      ToolTipText     =   "End timecode"
      Top             =   1140
      Width           =   1095
   End
   Begin VB.TextBox txtBreak5elapsed 
      ForeColor       =   &H000000FF&
      Height          =   315
      Left            =   6660
      TabIndex        =   4
      ToolTipText     =   "End timecode"
      Top             =   1500
      Width           =   1095
   End
   Begin VB.TextBox txtBreak4elapsed 
      ForeColor       =   &H000000FF&
      Height          =   315
      Left            =   6660
      TabIndex        =   3
      ToolTipText     =   "End timecode"
      Top             =   1140
      Width           =   1095
   End
   Begin VB.TextBox txtBreak3elapsed 
      ForeColor       =   &H000000FF&
      Height          =   315
      Left            =   3420
      TabIndex        =   2
      ToolTipText     =   "End timecode"
      Top             =   1860
      Width           =   1095
   End
   Begin VB.TextBox txtBreak2elapsed 
      ForeColor       =   &H000000FF&
      Height          =   315
      Left            =   3420
      TabIndex        =   1
      ToolTipText     =   "End timecode"
      Top             =   1500
      Width           =   1095
   End
   Begin VB.TextBox txtBreak1elapsed 
      ForeColor       =   &H000000FF&
      Height          =   315
      Left            =   3420
      TabIndex        =   0
      ToolTipText     =   "End timecode"
      Top             =   1140
      Width           =   1095
   End
End
Attribute VB_Name = "frmAdditionalControls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
