Attribute VB_Name = "modGlobal"
Option Explicit

Public oExcel As Excel.Application
Public oWorkbook As Excel.Workbook
Public oWorksheet As Excel.Worksheet

'new settings to disable core functionality

Public g_intDisableCostingSheets As Integer
Public g_intDisableEditViews As Integer
Public g_intDisableEquipmentManagement As Integer
Public g_intDisableImport As Integer
Public g_intDisableKitDefinition As Integer
Public g_intDisableLibraryTransfer As Integer
Public g_intDisableProducts As Integer
Public g_intDisableProjects As Integer
Public g_intDisablePurchaseOrders As Integer
Public g_intDisableQuotes As Integer
Public g_intDisableResourceList As Integer
Public g_intDisableSchedule As Integer
Public g_intDisableSundry As Integer
Public g_intDisableTasks As Integer
Public g_intDisableToolBar As Integer
Public g_intDisableOurContact As Integer
Public g_intDisableJobVideoStandard As Integer
Public g_intDisableJobOperatorCommentsOnDubbingTab As Integer
Public g_intDisableDetailedDubbingCompletion As Integer
Public g_intDisableAllocatedTo As Integer
Public g_intEnableLibraryClipManagement As Integer
Public g_intUseMakePDFasExportButton As Integer
Public g_intUseJCALibrarySearchPrinting As Integer
Public g_intNoCompulsoryPostcodeAndCountry As Integer
Public g_intTapeOperatorsFromCetauserTable As Integer
Public g_intNoAutomaticClipNumbers As Integer
Public g_intAllowBigStatusJumps As Integer
Public g_intKidsCoContractRoutines As Integer
Public g_intUseTerseJobDetailCompletion As Integer
Public g_intCheckAllBBCJobs As Integer
Public g_intCompulsoryBikeRefs As Integer
Public g_intSendPortalEmails As Integer
Public g_intEnforceRigorousLibraryChecks As Integer
Public g_intPrintCopyJobSheets As Integer
Public g_intClipGridClick As Integer
Public g_intTranscodeGridClick As Integer
Public g_intFileRequestGridClick As Integer
Public g_blnRedNetwork As Boolean ' Switch to disable Media Store activity if not on Red Network
Public g_blnFFMPEG As Boolean ' Switch to use ffplay for playing media files
Public g_intUseSvenskAutoSend As Integer
Public g_intUseDADCFlipFlop As Integer

Public g_intWebInvoicing As Integer ' Switch to disable CETA features for new Web Invoicing Module.
Public g_intPostToPriority As Integer ' Switch to change Invoice and Customer posting from Access Accounts to PRIORITY accounts.
Public g_intNewStyleSvenskXML As Integer ' Switch to control which XML style used for DADC Svensk XML

Public g_optShowLibraryStationaryTypes As Integer

Public g_strEmailFooter As String

Public g_blnLoadingCostings As Boolean

Public g_optRequireProductOnQuotes As Integer

'public
Public g_optUseClipID As Integer
Public g_strClipIDCaption As String
Public g_optOverRide As Integer
'-----------------------------------

Public g_intPagesToScrollLeftRight As Integer

'Declare a setting that is not able to be altered on the form.
Public g_optJCASystemVariables As Integer

Public g_intMaxUsers As Integer
Public gUsers As Integer
Public nUserID As Integer
Public g_strUploadInvoiceLocation As String

Public g_strVDMSEmailForInvoices As String
Public g_optUseAutoDeleteWhenFinalisingJobs As Integer
Public g_optDoOfflineCFMRequests As Integer
Public g_optUseSingleCompletionScreenForDubbing As Integer
Public g_strRateCardType As String
Public g_optHideAudioChannels As Integer
Public g_optDescDeadNotCompulsory As Integer
Public g_optJCADeliverylabels As Integer
Public g_optShowBillToInformationOnDespatch As Integer
Public g_optDontClearShelfWhenMovingTapes As Integer
Public g_optAddFakeJobIDToTapes As Integer
Public g_optPrefixLibraryBarcodesWithCompanyPrefix As Integer
Public g_optDisableUpdateLibraryRecordWithJobDetails As Integer
Public g_optAllowCostingEntryAtAnyStatus As Integer
Public g_optDisableCostingPopUp As Integer
Public g_optUseMinimumCharging  As Integer
Public g_intBaseRateCard As Integer
Public g_optDontMakeMachineTimeADecimal As Integer
Public g_optDontUseDynoBarcodeLabel As Integer
Public g_optUseAudioStandardInLibraryToStoreLanguage As Integer
Public g_optDontPromptForConfirmationOnCompletionOfDespatches As Integer
Public g_strScreenGrabStore As String
Public g_strFlashServer As String
Public g_strFlashstorePath As String
Public g_strAsperaServer As String
Public g_optAsperaOnline As Integer
Public g_optVantageOnline As Integer
Public g_strKeyframeStore As String
Public g_strFlashThumbStore As String
Public g_strLocationOfRapidsLogs As String
Public g_strExcelOrderLocation As String
Public g_blnFrameAccurateClipDurations As Boolean
Public g_strLocationOfOmneonXLS As String
Public g_strLocationOfWatchfolders As String
Public g_strDefaultPortalAddress As String
Public g_strDefaultHeaderPage As String
Public g_strDefaultFooterPage As String
Public g_strDefaultMainPage As String
Public g_strJPEGextension As String
Public g_strMediaWindowSystemMessage As String
Public g_strLocationOfJellyroll1Folder As String
Public g_strLocationOfJellyroll2Folder As String
Public g_lngJellyroll1LibraryID As Long
Public g_lngJellyroll2LibraryID As Long
Public g_lngSprocketsPercentage As Long
Public g_strDefaultAdminLink1 As String
Public g_strDefaultAdminTitle1 As String
Public g_strDefaultAdminText1 As String
Public g_strDefaultAdminLink2 As String
Public g_strDefaultAdminTitle2 As String
Public g_strDefaultAdminText2 As String
Public g_strDefaultAdminLink3 As String
Public g_strDefaultAdminTitle3 As String
Public g_strDefaultAdminText3 As String

Public g_strAsperaRootDirectory As String
Public g_strBBCAkamaiFTPaddress As String
Public g_strBBCAkamaiFTPusername As String
Public g_strBBCAkamaiFTPpassword As String
Public g_strBBCAkamaiFTProotdirectory As String
Public g_strBBC16x9TranscodeWatchFolder As String
Public g_strBBC4x3TranscodeWatchFolder As String
Public g_strJCA16x9TranscodeWatchFolder As String
Public g_strJCA4x3TranscodeWatchFolder As String
Public g_strBBCMG_Archive As String
Public g_strBBCMG_ClipArchive As String
Public g_strBBCMG_ProxyArchive As String
Public g_strBBCMG_Aspera As String
Public g_strBBCMG_Restore As String
Public g_strBBCMG_Flashstore As String
Public g_strTranscodeOverlayFolder As String
Public g_strSigniantSendPath As String
Public g_strAsperaClient1SendPath As String
Public g_strAsperaClient360SendPath As String
Public g_strSolaRRArchivePath As String
Public g_strSolaRRRestorePath As String

Public g_strLabelPrinter As String

'used for copying events
Public g_lngEventID As Long

Public g_strConnection As String

Public g_intSundryMarkup As Integer

Public Const vbShortDateFormat = "dd/mm/yyyy"
Public Const vbTimeFormat = "hh:nn"
Public Const g_intLastRateCard = 9
Public Const vbLightBlue = &HFFC0C0
Public Const vbLightRed = &H8080FF
Public Const vbLightGreen = &HC0FFC0

Public g_strCurrentVersion As String



'user variables
Public g_strUserInitials As String
Public g_lngUserID As Long
Public g_strUserName As String
Public g_strUserAccessCode As String
Public g_strFullUserName As String
Public g_strUserEmailAddress As String
Public g_strWorkstation As String
Public g_lngSessionID As Long
Public g_datLastChangedPasswordDate As Date
Public g_intPasswordNeverExpires As Integer

'used to trap ExecuteSQL messages
Public g_strExecuteError As String
Public g_lngLastID As Long
Public g_strDebugSQLString As String

'to do with the database types used
Public g_dbtype As String

Public g_strCompanyPrefix As String
Public Const g_strCurrency = "�"

Public g_optUseProductRateCards As Integer

Public g_intHideVideoStandardOnJobForm As Integer
Public g_optAllowCostedJobsWhenNotComplete As Integer

Public g_intPasswordLength As Integer
Public g_intPasswordNumbers As Integer
Public g_intPasswordUpperCase As Integer
Public g_intPasswordChangeEvery As Integer

Public g_intUseHireRateCard As Integer

Public g_optCostDespatch As Integer
Public g_optDontForceUnitsChargedOnHireJobs As Integer

Public g_intLockDiscountColumnInCostings As Integer
Public g_intDontCostScheduledResourcesIfJobTypeIsADub As Integer
Public Const g_optPromptForResourceDefaults = 1

Public g_optAllocateBarcodes As Integer
Public g_strAdvancedPurchaseApproveEmail  As String
Public g_optRequireVideoStandardOnQuote As Integer
Public g_optUseDynoBarcodeLabel As Integer

Public g_optHideOverTime As Integer
Public g_optHideDownTime As Integer

Public g_optHideStandardLabelsButton As Integer
Public g_optBookDubsAsConfirmedStatus As Integer
Public g_optDADC_XML_File_Requests As Integer

Public g_optTrySMTPPing As Integer
Public g_strSMTPServer As String ' = "carole.mpc.local"
Public g_strSMTPUserName As String
Public g_strSMTPPassword As String
Public g_strSMTPServerAuthorised As String ' = "carole.mpc.local"
Public g_strSMTPUserNameAuthorised As String
Public g_strSMTPPasswordAuthorised As String

'email addresses

Public g_strCETAAdministratorEmail As String
Public g_strCetaMailFromEmailDefault As String
Public g_strAdministratorEmailAddress As String
Public g_strAdministratorEmailName As String
Public g_strOperationsEmailAddress As String
Public g_strOperationsEmailName As String
Public g_strBookingsEmailAddress As String
Public g_strBookingsEmailName As String
Public g_strQuotesEmailAddress As String
Public g_strQuotesEmailName  As String
Public g_strManagerEmailAddress As String

'options

Public g_optUseJCAPostingToAccounts As Integer
Public g_optUseMD5Passwords As Integer
Public g_strDefaultInternalMovementLocation As String
Public g_optJobFormMaximised As Integer
Public g_optUseFormattedTimeCodesInEvents As Integer
Public g_optMaximumQuoteDiscount As Integer
Public g_optRequireBudgetCodesBeforeAuthorisation As Integer
Public g_optHideOrderByColumnOnCostingGrid As Integer
Public g_optRequireCompletedDubsBeforeCosting  As Integer
Public Const g_optPrintStockCode = 1
Public Const g_optDontPrintBarcode = 0
Public g_optShowTimeColumnInPurchaseOrder As Integer
Public g_strClipStorePlayURL  As String
Public g_intDefaultCopiesToPrintOfInvoice As Integer
Public g_optUpperCaseDubbingDetails As Integer
Public g_optCreateDNotesForExternalDeliveryPurchaseOrders As Integer
Public g_optApplyDealPriceToActualsRatherThanRateCard As Integer
Public g_optRequireDeliveryMethodsOnHireAndEditJobs As Integer
Public g_optDontPromptForNumberOfInvoiceCopies As Integer
Public g_optNeedAccountNumberToConfirm As Integer
Public g_optDontPromptForInvoiceAddress As Integer
Public g_optAbortIfAZeroDuration As Integer

Public Const g_optAlsoInsertOpNotesToAccounts = True
Public Const g_optUpdateResourceScheduleStatusOnDespatch = True
Public Const g_optAllowNonBookedItemsInTech = 1
Public g_optRequireEventWhenAddingMasters As Integer

Public g_optUserIsNotDefaultOurContact As Integer
Public g_optCompleteJobsWhenVTCompleted As Integer
Public g_optAllowEditAuthorisedPurchaseOrders As Integer
Public g_optForceDealPrice  As Integer
Public g_optStopSavingJobsWhenCompanyOnHold  As Integer ' not in settings yet
Public g_optUseOriginalLabels  As Integer 'not in settings yet
Public g_optHideSoundChannelsOnVTDetailGrid As Integer
Public g_optAutoTickOurContactInJobLists As Integer
Public g_optHideZeroRateCardItemsInManagementReports As Integer
Public g_optHarshCheckingOnDubbingEntry  As Integer
Public g_strDefaultCategoryToAddResource As String
Public g_optCreateDeliveryNotesForJobsWithNoAddress As Integer
Public g_optHideHireDespatchTab As Integer
Public g_optRequireOurContactBeforeSaving As Integer
Public g_optShowHorizontalScrollBar As Integer
Public g_optSendToAccountsOnFinalise As Integer
Public g_optAllowNewPurchaseOrdersOnCostedJobs As Integer
Public g_optMakeEventsWide As Integer
Public g_optCheckJobStatusBeforePrintingDespatch As Integer
Public g_optUseDifferentRangeForInAndOutDespatchNumbers As Integer
Public g_optUseDepartmentalPurchaseAuthorisationNumbers As Integer
Public g_optUseJobDetailsWhenAddingJobDetailLinesToDespatch  As Integer
Public g_optAddDurationToRateCode As Integer
Public g_optAlwaysUseRateCardDescriptions As Integer
Public g_optMAPIEmail As Integer ' email mapi
Public g_intDefaultTab  As Integer
Public g_optLockSystem As Long
Public g_optPromptForLateDespatchReason As Integer
Public g_optCloseSystemDown As Integer
Public g_optStopFFMPGServices As Integer
Public g_optShowCompanyInsuranceOutDate As Integer
Public g_optUseAdditionalDescriptionsWhenCosting As Integer
Public g_optDeductPreSetDiscountsFromUnitPrice  As Integer
Public g_optUseMasterDetailOnInvoice As Integer
Public g_optPromptForDetailedCompletion As Integer
Public g_optAppendInitialsAndDateToNotes As Integer
Public g_intMinutesToCloseDown As Integer
Public g_optShowAdditionalEventFields As Integer
Public g_optCreatePDFFilesOfTechReviews As Integer
Public g_optAnimateSchedule As Integer
Public g_optAnimateLoop As Integer
Public g_optRequireActualsBeforeCosting As Integer
Public g_optDrawResourceBookingShadows  As Integer
Public g_optShowResourceLocation As Integer
Public g_optCostSchedule As Integer
Public g_optCostDubbing As Integer
Public g_optUseSpecialDubbingComboOptions As Integer
Public g_optShowSerialNumberInSchedule As Integer
Global g_optUseTitleInMaster As Integer
Global g_optUseTitleInCopy As Integer
Global g_optHideVATFieldsInCostings As Integer
Global g_optUseNewSearchLibraryFields As Integer
Global g_optDeadlineTimeNotCompulsory As Integer
Global g_optOrderByNumbers As Integer

Global g_optShowProjectNumberOnResourceSchedule  As Integer
Global g_optCalculateRunningTimeFromSchedule  As Integer
Global g_optSearchLibraryDefaultField As String
Global g_optDefaultEventSearchInLibrary As Integer

Global g_optRoundCurrencyDecimals As Integer

Global g_optConnection1Name As String
Global g_optConnection2Name As String
Global g_optConnection3Name As String
Global g_optConnection4Name As String

'need to update this for MPC
Public g_optRequireProductBeforeSaving As Integer

Public g_optHideTimeColumn As Integer

Public g_strResourceBookingCaption1FontName As String
Public g_strResourceBookingCaption2FontName As String
Public g_strResourceBookingCaption3FontName As String

Public g_intResourceBookingCaption1FontSize As Integer
Public g_intResourceBookingCaption2FontSize As Integer
Public g_intResourceBookingCaption3FontSize As Integer

Public g_lngResourceBookingCaption1ForeColour As Long
Public g_lngResourceBookingCaption2ForeColour As Long
Public g_lngResourceBookingCaption3ForeColour As Long

Public g_strPDFLocation As String
Public g_strFilePDFLocation As String
Public g_strTapePDFLocation As String

Public g_blnPreviewReport As Boolean

Public g_intNumberOfDespatchNoteCopies As Integer

'new job defaults
Public g_strDefaultJobAllocation As String

Public g_strWhenToAllocate As String
Public g_strDefaultJobType As String

'info for the schedule form - resizing
Public Const g_lngScheduleHeaderPanelHeight = 400
Public Const g_lngScheduleResourcePanelWidth = 2450
Public Const g_lngScheduleFooterPanelHeight = 375
Public Const g_lngScheduleControlsPanelHeight = 1155

Public Const g_intPagesToScrollUpDown = 1

'colours for bars
Global Const g_lngColourPencil = &HA0FFA0
Global Const g_lngColour2ndPencil = 12615935
Global Const g_lngColourConfirmed = &HFFC0C0
Global Const g_lngColourMastersHere = &HFFA0A0
Global Const g_lngColourScheduled = &HFF8080
Global Const g_lngColourCompleted = &HFF6060
Global Const g_lngColourInProgress = &H10FF00
Global Const g_lngColourVTDone = &HFFFF00
Global Const g_lngColourHoldCost = &H6868C8
Global Const g_lngColourRejected = &H4848FF
Global Const g_lngColourOnHold = &H4848C8
Global Const g_lngColourCosted = 7237230
Global Const g_lngColourSentToAccounts = 33023

Global Const g_lngColourCancelled = 98213
Global Const g_lngColourUnavailable = 821376
Global Const g_lngColourMeeting = 16777088
Global Const g_lngColourQuick = 10551295

Global Const g_lngColourPrepared = 16777215
Global Const g_lngColourDespatched = 11134206
Global Const g_lngColourReturned = 16744448




Public Const g_strBatchPrefix = "J"

Global g_strWebBrowserHomeDirectory As String
Global g_strWebBrowserResourceInfoPage As String


Global g_intDefaultTimeWhenZero As Integer

Global g_strLocationOfCrystalReportFiles As String

'mouse wheel

'************************************************************
'API
'************************************************************

Private Declare Function CallWindowProc Lib "user32.dll" Alias "CallWindowProcA" ( _
    ByVal lpPrevWndFunc As Long, _
    ByVal hWnd As Long, _
    ByVal Msg As Long, _
    ByVal wParam As Long, _
    ByVal lParam As Long) As Long
Private Declare Function SetWindowLong Lib "user32.dll" Alias "SetWindowLongA" ( _
    ByVal hWnd As Long, _
    ByVal nIndex As Long, _
    ByVal dwNewLong As Long) As Long

'************************************************************
'Constants
'************************************************************

Public Const MK_CONTROL = &H8
Public Const MK_LBUTTON = &H1
Public Const MK_RBUTTON = &H2
Public Const MK_MBUTTON = &H10
Public Const MK_SHIFT = &H4
Private Const GWL_WNDPROC = -4
Private Const WM_MOUSEWHEEL = &H20A

'************************************************************
'Variables
'************************************************************

Private hControl As Long
Private lPrevWndProc As Long
'Common Variables
Private Const QUERY_SUCCESS As Long = 0     'If Return value is sucessfull

'DSN Related Variables
Private Const ODBC_ADD_DSN = 1              'Add User DSN
Private Const ODBC_ADD_SYS_DSN = 4          'Add System DSN

Private Const vbAPINull = &O0               'NULL Pointer

'MYSQL Related Variables
Private Const MYSQLOptionValue = 131242     'To set options on MYSQL

Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As Long)

'DSN Related Functions
#If Win32 Then
    Private Declare Function SQLConfigDataSource Lib "ODBCCP32.DLL" (ByVal hWndParent As Long, ByVal fRequest As Long, ByVal lpszDriver As String, ByVal lpszAttributes As String) As Long
#Else
    Private Declare Function SQLConfigDataSource Lib "ODBCINST.DLL" (ByVal hWndParent As Integer, ByVal fRequest As Integer, ByVal lpszDriver As String, ByVal lpszAttributes As String) As Integer
#End If



Sub GetGlobalSettings()

' rem this line when at JCA
If g_optJCASystemVariables = 0 Then Exit Sub

g_intEnableLibraryClipManagement = 1
g_intDisableCostingSheets = 1
g_intDisableEditViews = 1
g_intDisableEquipmentManagement = 1
g_intDisableImport = 1
g_intDisableKitDefinition = 1
g_intDisableLibraryTransfer = 1
g_intDisableProducts = 1
'g_intDisableProjects = 1
'g_intDisablePurchaseOrders = 1
g_intDisableQuotes = 1
'g_intDisableResourceList = 1
g_intDisableSchedule = 1
g_intDisableSundry = 1
'g_intDisableTasks = 1
'g_intDisableToolBar = 1
g_intDisableOurContact = 1
g_intDisableJobVideoStandard = 1
g_intDisableJobOperatorCommentsOnDubbingTab = 1
'g_intDisableDetailedDubbingCompletion = 1
g_intDisableAllocatedTo = 1
g_optJobFormMaximised = 1
g_optShowLibraryStationaryTypes = 1
g_optUseOriginalLabels = 1
g_intUseMakePDFasExportButton = 1
g_intUseJCALibrarySearchPrinting = 1
g_intNoCompulsoryPostcodeAndCountry = 1
g_intTapeOperatorsFromCetauserTable = 1
g_intNoAutomaticClipNumbers = 1
g_intAllowBigStatusJumps = 1
g_intKidsCoContractRoutines = 1
g_intUseTerseJobDetailCompletion = 1
g_intSendPortalEmails = 0
g_intWebInvoicing = 0
g_intPostToPriority = 1
g_intNewStyleSvenskXML = 1

End Sub

'This function will create MYSQL DSN
Private Function CreateMYSQLDSN(ByVal svlServerName As String, ByVal svlDSNName As String, ByVal svlUserName As String, ByVal svlPassword As String, ByVal svlDatabaseName As String, Optional ByVal svlDSNDriver As String = "MySQL ODBC 3.51 Driver", Optional ByVal bvlIsUserDSN As Boolean) As Boolean

On Error GoTo ErrorHandler
    
    CreateMYSQLDSN = False
    #If Win32 Then
      Dim intRet As Long
    #Else
        Dim intRet As Integerz
    #End If
    
    Dim strAttributes As String

    strAttributes = "SERVER=" & svlServerName & Chr$(0) & _
        "DSN=" & svlDSNName & Chr$(0) & _
        "DESCRIPTION=Created Automatically on " & Format(Date, "DD-MMM-YYYY") & Chr$(0) & _
        "DATABASE=" & svlDatabaseName & Chr$(0) & _
        "UID=" & svlUserName & Chr$(0) & _
        "PWD=" & svlPassword & Chr$(0) & _
        "PORT=3306" & Chr$(0) & _
        "OPTION=" & MYSQLOptionValue & Chr$(0)
    If bvlIsUserDSN Then      'If its a User DSN
        intRet = SQLConfigDataSource(vbAPINull, ODBC_ADD_DSN, svlDSNDriver, strAttributes)
    Else                    'If its a System DSN
        intRet = SQLConfigDataSource(vbAPINull, ODBC_ADD_SYS_DSN, svlDSNDriver, strAttributes)
    End If
    
    If intRet Then
         CreateMYSQLDSN = True
         MsgBox "CETASQL - DSN was created successfully!", vbExclamation
    Else
        Err.Raise vbObjectError + 514, , "Failed to create DSN " & svlDSNName
    End If
    
Exit Function

ErrorHandler:
    MsgBox ("Error :" & Err.Number & " - " & Err.Description)
End Function
'*************************************************************
'WindowProc
'*************************************************************

'zDelta: The value of the high-order word of wParam.
'Indicates the distance that the wheel is rotated, expressed in multiples or
'divisions of WHEEL_DELTA, which is 120. A positive value indicates that the
'wheel was rotated forward, away from the user; a negative value indicates
'that the wheel was rotated backward, toward the user.
Private Function WindowProc(ByVal lWnd As Long, ByVal lMsg As Long, _
ByVal wParam As Long, ByVal lParam As Long) As Long

    Dim fwKeys As Long
    Dim zDelta As Long
    Dim xPos As Long
    Dim yPos As Long

    'Test if the message is WM_MOUSEWHEEL
    If lMsg = WM_MOUSEWHEEL Then
        fwKeys = wParam And 65535
        zDelta = wParam / 65536
        xPos = lParam And 65535
        yPos = lParam / 65536
        'Call the Form1's Procedure to handle the MouseWheel event
    End If
    'Sends message to previous procedure
    'This is VERY IMPORTANT!!!
    WindowProc = CallWindowProc(lPrevWndProc, lWnd, lMsg, wParam, lParam)
End Function

Sub CreateCETASQLdsn(lp_strHostName As String, lp_strPassword As String)

Dim l_strComputerName As String
l_strComputerName = CurrentMachineName()

CreateMYSQLDSN lp_strHostName, "cetasql", l_strComputerName, lp_strPassword, "cetasoft2"

End Sub

'*************************************************************
'Hook
'*************************************************************
Public Sub Hook(ByVal hControl_ As Long)
    hControl = hControl_
    lPrevWndProc = SetWindowLong(hControl, GWL_WNDPROC, AddressOf WindowProc)
End Sub

'*************************************************************
'UnHook
'*************************************************************
Public Sub UnHook()
    Call SetWindowLong(hControl, GWL_WNDPROC, lPrevWndProc)
End Sub
Public Function nNetUserID(ByVal nMaxUsers As Integer) As Integer

Dim bLocked As Integer
Dim X As Integer
nNetUserID = 0

On Error GoTo errUserID:

For X = 1 To nMaxUsers
    bLocked = False
    Lock #gUsers, X
    If Not bLocked Then
        nNetUserID = X
        GoTo endUserID:
    End If
Next

GoTo endUserID:

errUserID:
bLocked = True
Resume Next

endUserID:
End Function

Sub Main()

'gUsers = FreeFile
'Open App.Path & "\USERS.DAT" For Random Shared As #gUsers Len = 1
'nUserID = nNetUserID(g_intMaxUsers)

'If nUserID = 0 Then
'    MsgBox "The limit of " & CStr(g_intMaxUsers) & " concurrent users has been reached.", vbCritical
'    End
'End If

'If App.PrevInstance Then
'    AppActivate App.Title
'    End
'End If

frmLoading.Show

g_optUseMD5Passwords = 1


frmLoading.lblStatus.Caption = "Getting your computer name"

Dim l_strComputerName As String
Dim l_strWorkstationprops As String

l_strComputerName = CurrentMachineName()

Dim l_strServer As String
Dim TempStr As String

'g_strConnection = "DRIVER={MySQL ODBC 3.51 Driver};SERVER=192.168.0.5;DATABASE=cetasoft2;UID=" & LCase(l_strComputerName) & ";PWD=ce7a50ft;OPTION=16427"
'g_strConnection = "DRIVER={MySQL ODBC 3.51 Driver};SERVER=83.138.128.228;DATABASE=cfm_brightontv;UID=brightontv;PWD=ce7aDEMO;OPTION=16427"

frmLoading.lblStatus.Caption = "Checking DSN"
DoEvents

Dim l_strCommandLine As String
l_strCommandLine = Command()

If InStr(1, l_strCommandLine, "/lon-z820-tim", vbTextCompare) <> 0 Then
    g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=lon-z820-tim.office.refine-group.com;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
ElseIf InStr(1, l_strCommandLine, "/lon-bsdevsql-01", vbTextCompare) <> 0 Then
    g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=lon-bsdevsql-01.office.refine-group.com;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
Else
    g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=lon-bssql-01.office.refine-group.com;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
'    g_strConnection = "DRIVER=SQL Server;SERVER=vie-sql-1.jcatv.co.uk;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
End If

'g_strConnection = "DRIVER={MySQL ODBC 3.51 Driver};SERVER=10.0.0.1;DATABASE=cetasoftwarelocal;UID=" & UCase(l_strComputerName) & ";PWD=ce7a50ft;OPTION=16427"

'set up the email footer
g_strEmailFooter = "--------------------------------------------------------------" & vbCrLf
g_strEmailFooter = g_strEmailFooter & "This message was automatically generated by CETA Facilities Manager" & vbCrLf
'g_strEmailFooter = g_strEmailFooter & "Online Help Facility: http://www.cetasoft.net/mediawiki" & vbCrLf
'g_strEmailFooter = g_strEmailFooter & "CETA Support: +44 (0) 1993 774476 or press F1 to log a query."

'g_strConnection = "DSN=cetasql"

Dim l_intCheckAgain As Integer
PROC_CheckAgain:

frmLoading.lblStatus.Caption = "Checking Connection..."
DoEvents

Dim l_blnDatabaseWorking As Boolean
l_blnDatabaseWorking = CheckDatabaseConnection()

If l_blnDatabaseWorking = False Then
    
    If l_intCheckAgain = 0 Then
        frmLoading.lblStatus.Caption = "Creating Default DSN"
        Dim l_intMsg As Integer
        l_intMsg = MsgBox("WARNING: Could not connect to the database server." & vbCrLf & vbCrLf & "Do you want to create a new DSN?" & vbCrLf & vbCrLf & "Only do this if you are setting up a new computer, as the default settings will be used!", vbExclamation + vbYesNo)
        
        If l_intMsg = vbYes Then
        
            Dim l_strDSNInfo As String
            l_strDSNInfo = InputBox("Please enter the name of your Database Server", "Create New DSN")
            If l_strDSNInfo <> "" Then
                CreateCETASQLdsn l_strDSNInfo, "ce7a50ft"
                l_intCheckAgain = 1
                GoTo PROC_CheckAgain
            End If
        Else
            MsgBox "Couldnt connect to the database server.", vbExclamation
        End If
        
    End If

    End
End If

g_intMinutesToCloseDown = 5

g_strLabelPrinter = GetSetting("CETA Facilities Manager 2", "Defaults", "LabelPrinter")

g_intMaxUsers = Val(GetData("setting", "value", "name", "maxusers"))

frmLoading.lblStatus.Caption = "Loading settings from database.........."
DoEvents

GetSettingsFromDatabase
GetGlobalSettings

If InStr(1, l_strCommandLine, "/WebInvoicing", vbTextCompare) Then
    g_intWebInvoicing = 1
End If

If InStr(1, l_strCommandLine, "/PostToPriority", vbTextCompare) Then
    g_intPostToPriority = 1
End If

g_intPagesToScrollLeftRight = 1 + g_optShowHorizontalScrollBar

frmLoading.lblStatus.Caption = "Settings Collected"
DoEvents

frmLoading.lblStatus.Caption = "Tidying Database..."
DoEvents
TidyDatabase

If g_strUploadInvoiceLocation = "" Then g_strUploadInvoiceLocation = "\\account\imports"

l_strWorkstationprops = GetData("xref", "descriptionalias", "description", CurrentMachineName)
g_strLocationOfCrystalReportFiles = App.Path & "\newreports\"

If InStr(1, l_strCommandLine, "/unlock", vbTextCompare) Then
    g_optLockSystem = 8
    SaveCETASetting "LockSystem", g_optLockSystem
    g_optCloseSystemDown = False
    SaveCETASetting "CloseSystemDown", g_optCloseSystemDown
    g_optStopFFMPGServices = False
    SaveCETASetting "StopFFMPGServices", g_optStopFFMPGServices
End If

If InStr(l_strWorkstationprops, "/rednetwork") > 0 Then
    g_blnRedNetwork = True
Else
    g_blnRedNetwork = False
End If

If InStr(l_strWorkstationprops, "/ffmpeg") > 0 Then
    g_blnFFMPEG = True
Else
    g_blnFFMPEG = False
End If

If (g_optLockSystem And 1) = 1 Then
    
    'this allows the user to go straight in, without the lock out
    If InStr(1, l_strCommandLine, "/override", vbTextCompare) Then
        g_optOverRide = 1
        GoTo PROC_AllowLogin
    Else
        SystemLockedMessage
        End
    End If
    
End If

PROC_AllowLogin:

frmLoading.lblStatus.Caption = "Trying automatic logon"

Unload frmLoading
Set frmLoading = Nothing

DoEvents

Dim l_strUserName As String, l_strPassword As String, Count As Long

l_strUserName = ""
l_strPassword = ""

If InStr(l_strCommandLine, "/username=") > 0 Then
    Count = InStr(l_strCommandLine, "/username=")
    l_strUserName = Mid(l_strCommandLine, Count + 10)
    If InStr(l_strUserName, " ") > 0 Then
        l_strUserName = Left(l_strUserName, InStr(l_strUserName, " ") - 1)
    ElseIf InStr(l_strUserName, "/") > 0 Then
        l_strUserName = Left(l_strUserName, InStr(l_strUserName, "/") - 1)
    End If
End If

If InStr(l_strCommandLine, "/password=") > 0 Then
    Count = InStr(l_strCommandLine, "/password=")
    l_strPassword = Mid(l_strCommandLine, Count + 10)
    If InStr(l_strPassword, " ") > 0 Then
        l_strPassword = Left(l_strPassword, InStr(l_strPassword, " ") - 1)
    ElseIf InStr(l_strPassword, "/") > 0 Then
        l_strPassword = Left(l_strPassword, InStr(l_strPassword, "/") - 1)
    End If
End If

AutomaticLogin l_strUserName, l_strPassword

If InStr(1, l_strCommandLine, "/media", vbTextCompare) <> 0 Then
    Dim l_strDrivePath As String, l_strPath As String, l_strFilename As String, l_strParam As String, l_lngCount As Long, DotPosition As Long
    Dim l_strClipReference As String
    MDIForm1.AutoShowChildren = True
    l_lngCount = InStr(1, l_strCommandLine, "/drive:", vbTextCompare)
    l_strParam = Mid(l_strCommandLine, l_lngCount + 7)
    l_lngCount = InStr(1, l_strParam, "/path:", vbTextCompare)
    l_strDrivePath = Left(l_strParam, l_lngCount - 1)
    frmClipControl.lblLibraryID.Caption = l_strDrivePath
    frmClipControl.cmbBarcode.Text = GetData("library", "barcode", "libraryID", l_strDrivePath)
    frmClipControl.lblFormat.Caption = GetData("library", "format", "libraryID", l_strDrivePath)
    l_strParam = Mid(l_strParam, l_lngCount + 6)
    l_lngCount = InStr(1, l_strParam, " /filename:", vbTextCompare)
    l_strPath = Left(l_strParam, l_lngCount - 1)
    l_strFilename = Trim(Mid(l_strParam, l_lngCount + 11))
    
    DotPosition = InStr(l_strFilename, ".")
    Do While InStr(Mid(l_strFilename, DotPosition + 1), ".") > 0
        DotPosition = DotPosition + InStr(Mid(l_strFilename, DotPosition + 1), ".")
    Loop
    l_strClipReference = Mid(l_strFilename, 1, DotPosition - 1)

    frmClipControl.txtAltFolder.Text = l_strPath
    frmClipControl.txtClipfilename.Text = l_strFilename
    frmClipControl.txtReference.Text = l_strClipReference
    frmClipControl.cmbPurpose = "Original Master"
    frmClipControl.txtJobID = 0
Else
    ShowNotices
    MDIForm1.Show
End If
    

End Sub

Sub TidyDatabase()

Dim l_strSQL As String

'set all PO's to 0 that are cancelled

Dim l_con As New ADODB.Connection
l_con.Open g_strConnection

On Error GoTo dbtype_check_error

g_dbtype = "mysql"

TryAgain:

If g_dbtype = "mysql" Then
    l_strSQL = "UPDATE purchasedetail INNER JOIN purchaseheader ON purchaseheader.purchaseheaderID = purchasedetail.purchaseheaderID Set purchasedetail.total = 0 Where purchasedetail.total > 0 AND purchaseheader.fd_status = 'Cancelled';"
Else
    l_strSQL = "UPDATE purchasedetail Set purchasedetail.total = 0  FROM purchasedetail INNER JOIN purchaseheader ON purchaseheader.purchaseheaderID = purchasedetail.purchaseheaderID Where purchasedetail.total > 0 AND purchaseheader.fd_status = 'Cancelled';"
End If

l_con.Execute l_strSQL

'delete all orphan library events
l_strSQL = "DELETE FROM tapeevents WHERE libraryID = 0"
l_con.Execute l_strSQL

'delete all orphan purchase order detail lines
l_strSQL = "DELETE FROM purchasedetail WHERE purchaseheaderID = 0"
l_con.Execute l_strSQL

'delete all orphan quote detail lines
l_strSQL = "DELETE FROM quotedetail WHERE quoteID = 0"
l_con.Execute l_strSQL

'delete all orphan despatch detail lines
l_strSQL = "DELETE FROM despatchdetail WHERE despatchID = 0"
l_con.Execute l_strSQL

'delete all orphan jobdetail lines
l_strSQL = "DELETE FROM jobdetail WHERE jobID = 0"
l_con.Execute l_strSQL

'''recreate the correct categories in costing
'If g_dbtype = "mysql" Then
'    l_strSQL = "UPDATE costing INNER JOIN ratecard ON ratecard.cetacode = costing.costcode SET costing.ratecardcategory = ratecard.category;"
'Else
'    l_strSQL = "UPDATE costing SET costing.ratecardcategory = ratecard.category FROM costing INNER JOIN ratecard ON ratecard.cetacode = costing.costcode;"
'End If
'l_con.Execute l_strSQL
'
''recreate the vat amount
'l_strSQL = "UPDATE costing SET vat = ROUND((total * 0.175)," & g_optRoundCurrencyDecimals & ");"
'l_con.Execute l_strSQL
'
''update the total including vat
'l_strSQL = "UPDATE costing SET totalincludingvat = total + vat;"
'l_con.Execute l_strSQL

''set the heights of resources
'l_strSQL = "UPDATE resourceview SET height = 580"
'l_con.Execute l_strSQL


l_con.Close
Set l_con = Nothing

Exit Sub
dbtype_check_error:

'first check if its the wrong kind of database
If Err.Number = -2147217900 Then
    g_dbtype = "mssql"
    Resume TryAgain
Else
    MsgBox Err.Description
    Exit Sub
End If


End Sub
