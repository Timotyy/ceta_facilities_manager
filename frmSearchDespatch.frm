VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmSearchDespatch 
   Caption         =   "Despatch Search"
   ClientHeight    =   8850
   ClientLeft      =   2355
   ClientTop       =   3465
   ClientWidth     =   15240
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSearchDespatch.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8850
   ScaleWidth      =   15240
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtReference 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1200
      TabIndex        =   51
      ToolTipText     =   "Company contact"
      Top             =   1920
      Width           =   2235
   End
   Begin VB.TextBox txtDescription 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1200
      TabIndex        =   38
      ToolTipText     =   "Description of the tape"
      Top             =   480
      Width           =   2235
   End
   Begin VB.ComboBox cmbDeliveryMethod 
      Height          =   315
      Left            =   4740
      TabIndex        =   37
      ToolTipText     =   "Method of Tape delivery"
      Top             =   1200
      Width           =   2415
   End
   Begin VB.ComboBox cmbDirection 
      Height          =   315
      Left            =   6060
      TabIndex        =   36
      ToolTipText     =   "Direction of despatch: in/out"
      Top             =   120
      Width           =   1095
   End
   Begin VB.ComboBox cmbLocation 
      Height          =   315
      Left            =   4740
      TabIndex        =   35
      ToolTipText     =   "Current location of the tape"
      Top             =   840
      Width           =   2415
   End
   Begin VB.TextBox txtJobID 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   4740
      TabIndex        =   34
      ToolTipText     =   "The unique job ID number"
      Top             =   480
      Width           =   2415
   End
   Begin VB.TextBox txtAddress 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1200
      TabIndex        =   33
      ToolTipText     =   "Company address"
      Top             =   840
      Width           =   2235
   End
   Begin VB.TextBox txtPostCode 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1200
      TabIndex        =   32
      ToolTipText     =   "Company post code"
      Top             =   1200
      Width           =   2235
   End
   Begin VB.TextBox txtContact 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1200
      TabIndex        =   31
      ToolTipText     =   "Company contact"
      Top             =   1560
      Width           =   2235
   End
   Begin VB.Frame fraDespatchDetail 
      Caption         =   "Despatch Detail"
      Height          =   2175
      Left            =   9900
      TabIndex        =   24
      Top             =   60
      Width           =   3435
      Begin VB.TextBox txtTitle 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   1260
         TabIndex        =   4
         ToolTipText     =   "Title of the tape"
         Top             =   300
         Width           =   2055
      End
      Begin VB.TextBox txtSubTitle 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   1260
         TabIndex        =   5
         ToolTipText     =   "Sub title of the tape"
         Top             =   660
         Width           =   2055
      End
      Begin VB.TextBox txtBarcode 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   1260
         TabIndex        =   6
         ToolTipText     =   "Barcode for the tape"
         Top             =   1020
         Width           =   2055
      End
      Begin VB.TextBox txtFormat 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   1260
         TabIndex        =   7
         ToolTipText     =   "Format of the tape"
         Top             =   1380
         Width           =   2055
      End
      Begin VB.TextBox txtStandard 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   1260
         TabIndex        =   8
         ToolTipText     =   "Standard of the tape"
         Top             =   1740
         Width           =   2055
      End
      Begin VB.Label lblCaption 
         Caption         =   "Title"
         Height          =   255
         Index           =   15
         Left            =   120
         TabIndex        =   29
         Top             =   300
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Sub Title"
         Height          =   255
         Index           =   16
         Left            =   120
         TabIndex        =   28
         Top             =   660
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Barcode"
         Height          =   255
         Index           =   17
         Left            =   120
         TabIndex        =   27
         Top             =   1020
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Format"
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   26
         Top             =   1380
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Standard"
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   25
         Top             =   1740
         Width           =   1035
      End
   End
   Begin VB.PictureBox picFooter 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1500
      ScaleHeight     =   375
      ScaleWidth      =   11355
      TabIndex        =   22
      Top             =   7800
      Width           =   11355
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   6420
         TabIndex        =   11
         ToolTipText     =   "Print the Search results"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdRun 
         Caption         =   "Run"
         Height          =   315
         Left            =   5160
         TabIndex        =   10
         ToolTipText     =   "Run the selected search"
         Top             =   0
         Width           =   1155
      End
      Begin VB.ComboBox cmbMacroSearch 
         Height          =   315
         Left            =   1620
         Style           =   2  'Dropdown List
         TabIndex        =   9
         ToolTipText     =   "Previously Stored Searches"
         Top             =   0
         Width           =   3195
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   8940
         TabIndex        =   13
         ToolTipText     =   "Search using set criterea"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   10200
         TabIndex        =   14
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   7680
         TabIndex        =   12
         ToolTipText     =   "Clear the form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Stored searches:"
         Height          =   255
         Index           =   10
         Left            =   180
         TabIndex        =   23
         Top             =   0
         Width           =   1275
      End
   End
   Begin MSAdodcLib.Adodc adoLibrary 
      Height          =   315
      Left            =   6060
      Top             =   4140
      Visible         =   0   'False
      Width           =   2955
      _ExtentX        =   5212
      _ExtentY        =   556
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   0
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Record Count"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Frame Frame1 
      Caption         =   "Modified Date..."
      Height          =   1095
      Left            =   7260
      TabIndex        =   19
      Top             =   60
      Width           =   2595
      Begin MSComCtl2.DTPicker datModifiedDateFrom 
         Height          =   315
         Left            =   960
         TabIndex        =   0
         ToolTipText     =   "The date the tape was modified"
         Top             =   300
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   3538945
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datModifiedDateTo 
         Height          =   315
         Left            =   960
         TabIndex        =   1
         ToolTipText     =   "The deadline date"
         Top             =   660
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   3538945
         CurrentDate     =   37870
      End
      Begin VB.Label lblCaption 
         Caption         =   "From"
         Height          =   255
         Index           =   6
         Left            =   240
         TabIndex        =   21
         Top             =   240
         Width           =   735
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Height          =   255
         Index           =   7
         Left            =   240
         TabIndex        =   20
         Top             =   600
         Width           =   855
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Created Date..."
      Height          =   1095
      Left            =   7260
      TabIndex        =   16
      Top             =   1140
      Width           =   2595
      Begin MSComCtl2.DTPicker datCreatedDateFrom 
         Height          =   315
         Left            =   960
         TabIndex        =   2
         ToolTipText     =   "The date the tape was created"
         Top             =   240
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   3538945
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datCreatedDateTo 
         Height          =   315
         Left            =   960
         TabIndex        =   3
         ToolTipText     =   "The date the job was booked"
         Top             =   600
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   3538945
         CurrentDate     =   37870
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   18
         Top             =   600
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "From"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   17
         Top             =   240
         Width           =   1035
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdLibrary 
      Bindings        =   "frmSearchDespatch.frx":08CA
      Height          =   2175
      Left            =   60
      TabIndex        =   15
      ToolTipText     =   "Search results"
      Top             =   2280
      Width           =   13275
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowUpdate     =   0   'False
      BackColorOdd    =   16446965
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   23416
      _ExtentY        =   3836
      _StockProps     =   79
   End
   Begin MSAdodcLib.Adodc adoDespatchDetail 
      Height          =   315
      Left            =   60
      Top             =   5100
      Visible         =   0   'False
      Width           =   2955
      _ExtentX        =   5212
      _ExtentY        =   556
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   0
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Record Count"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDespatchDetail 
      Bindings        =   "frmSearchDespatch.frx":08E3
      Height          =   2475
      Left            =   60
      TabIndex        =   30
      ToolTipText     =   "Information about the items in the Despatch"
      Top             =   4560
      Width           =   10515
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   1
      BackColorOdd    =   12648384
      RowHeight       =   423
      Columns.Count   =   12
      Columns(0).Width=   1349
      Columns(0).Caption=   "Job ID"
      Columns(0).Name =   "Job No"
      Columns(0).DataField=   "jobID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   1561
      Columns(1).Caption=   "Type"
      Columns(1).Name =   "copytype"
      Columns(1).DataField=   "copytype"
      Columns(1).FieldLen=   256
      Columns(2).Width=   661
      Columns(2).Caption=   "Qu."
      Columns(2).Name =   "quantity"
      Columns(2).DataField=   "quantity"
      Columns(2).FieldLen=   256
      Columns(3).Width=   1614
      Columns(3).Caption=   "Format"
      Columns(3).Name =   "format"
      Columns(3).DataField=   "format"
      Columns(3).FieldLen=   256
      Columns(4).Width=   1535
      Columns(4).Caption=   "Standard"
      Columns(4).Name =   "videostandard"
      Columns(4).DataField=   "videostandard"
      Columns(4).FieldLen=   256
      Columns(5).Width=   2249
      Columns(5).Caption=   "Barcode"
      Columns(5).Name =   "barcode"
      Columns(5).DataField=   "barcode"
      Columns(5).FieldLen=   256
      Columns(6).Width=   4710
      Columns(6).Caption=   "Description"
      Columns(6).Name =   "title"
      Columns(6).DataField=   "title"
      Columns(6).FieldLen=   256
      Columns(7).Width=   4180
      Columns(7).Caption=   "Sub Description"
      Columns(7).Name =   "subtitle"
      Columns(7).DataField=   "subtitle"
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "P/No"
      Columns(8).Name =   "packagenumber"
      Columns(8).DataField=   "packagenumber"
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "despatchID"
      Columns(9).Name =   "despatchID"
      Columns(9).DataField=   "despatchID"
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "despatchdetailID"
      Columns(10).Name=   "despatchdetailID"
      Columns(10).DataField=   "despatchdetailID"
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "jobdetailID"
      Columns(11).Name=   "jobdetailID"
      Columns(11).DataField=   "jobdetailID"
      Columns(11).FieldLen=   256
      _ExtentX        =   18547
      _ExtentY        =   4366
      _StockProps     =   79
      Caption         =   "Items Being Despatched"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoCompany 
      Height          =   330
      Left            =   13500
      Top             =   2040
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCompany"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Bindings        =   "frmSearchDespatch.frx":0903
      Height          =   255
      Left            =   1200
      TabIndex        =   39
      ToolTipText     =   "The Company the tape belongs to"
      Top             =   120
      Width           =   3975
      DataFieldList   =   "name"
      BevelType       =   0
      _Version        =   196617
      BorderStyle     =   0
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "accountcode"
      Columns(2).Name =   "accountcode"
      Columns(2).DataField=   "accountcode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "telephone"
      Columns(3).Name =   "telephone"
      Columns(3).DataField=   "telephone"
      Columns(3).FieldLen=   256
      _ExtentX        =   7011
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbProduct 
      Height          =   255
      Left            =   4740
      TabIndex        =   40
      ToolTipText     =   "The product on this job"
      Top             =   1560
      Width           =   2415
      BevelWidth      =   0
      DataFieldList   =   "name"
      BevelType       =   0
      _Version        =   196617
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      CheckBox3D      =   0   'False
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   8438015
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "productID"
      Columns(0).Name =   "productID"
      Columns(0).DataField=   "productID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   5212
      Columns(1).Caption=   "Name"
      Columns(1).Name =   "name"
      Columns(1).DataField=   "name"
      Columns(1).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   8438015
      DataFieldToDisplay=   "name"
   End
   Begin VB.Label lblCaption 
      Caption         =   "Courier Ref"
      Height          =   255
      Index           =   19
      Left            =   60
      TabIndex        =   52
      Top             =   1920
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      Height          =   255
      Index           =   2
      Left            =   60
      TabIndex        =   50
      Top             =   120
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Description"
      Height          =   255
      Index           =   5
      Left            =   60
      TabIndex        =   49
      Top             =   480
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Post Code"
      Height          =   255
      Index           =   3
      Left            =   60
      TabIndex        =   48
      Top             =   1200
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Address"
      Height          =   255
      Index           =   4
      Left            =   60
      TabIndex        =   47
      Top             =   840
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Method"
      Height          =   255
      Index           =   11
      Left            =   3600
      TabIndex        =   46
      Top             =   1200
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Direction"
      Height          =   255
      Index           =   12
      Left            =   5280
      TabIndex        =   45
      Top             =   120
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Caption         =   "Location"
      Height          =   255
      Index           =   13
      Left            =   3600
      TabIndex        =   44
      Top             =   840
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job ID"
      Height          =   255
      Index           =   14
      Left            =   3600
      TabIndex        =   43
      Top             =   480
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Contact"
      Height          =   255
      Index           =   18
      Left            =   60
      TabIndex        =   42
      Top             =   1560
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Product"
      Height          =   255
      Index           =   46
      Left            =   3600
      TabIndex        =   41
      Top             =   1560
      Width           =   1035
   End
End
Attribute VB_Name = "frmSearchDespatch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim m_strSQLSearch As String
Dim m_strSQLOrderby As String


Private Sub adoLibrary_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

If adoLibrary.Recordset.EOF = True Then Exit Sub

l_strSQL = "SELECT * FROM despatchdetail WHERE despatchID = '" & GetData("despatch", "despatchID", "despatchnumber", adoLibrary.Recordset("Dnote #")) & "' ORDER BY despatchdetailID;"

adoDespatchDetail.ConnectionString = g_strConnection
adoDespatchDetail.RecordSource = l_strSQL
adoDespatchDetail.Refresh


End Sub

Private Sub cmbLocation_KeyPress(KeyAscii As Integer)
AutoMatch cmbLocation, KeyAscii
End Sub


Private Sub cmbProduct_DropDown()

Dim l_strSQL As String
l_strSQL = "SELECT name, productID FROM product WHERE name LIKE '" & QuoteSanitise(cmbProduct.Text) & "%' ORDER BY name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbProduct.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing
End Sub

Private Sub cmdClear_Click()
ClearFields Me
'Me.Caption = "Library Search"
End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdPrint_Click()
Dim l_searchstring As String
l_searchstring = adoLibrary.RecordSource
If l_searchstring <> "" Then
    PrintCrystalReportUsingSQL g_strLocationOfCrystalReportFiles & "despatchsearch.rpt", l_searchstring, g_blnPreviewReport
End If
End Sub

Private Sub cmdRun_Click()

If cmbMacroSearch.Text = "" Then Exit Sub

Dim l_strSQL As String

Dim l_strFields As String

l_strFields = "despatchID, jobID, companyname, description, postcode, direction, location, createddate, createduser, deadlinedate, deliverymethod"

l_strSQL = "SELECT " & l_strFields & " FROM despatch "

l_strSQL = l_strSQL & "WHERE (despatchID IS NOT NULL) "
l_strSQL = l_strSQL & "AND "
l_strSQL = l_strSQL & DecodeMacroSearch(cmbMacroSearch.Text)


m_strSQLSearch = l_strSQL

m_strSQLOrderby = "ORDER BY deadlinedate"

adoLibrary.RecordSource = m_strSQLSearch & m_strSQLOrderby
adoLibrary.ConnectionString = g_strConnection
adoLibrary.Refresh

adoLibrary.Caption = "Records: " & grdLibrary.Rows


End Sub

Private Sub cmdSearch_Click()
Dim l_strSQL As String
Dim l_strFields As String

l_strFields = "despatch.despatchnumber AS 'Dnote #', despatch.companyname AS 'Client', despatch.contactname AS 'Contact Name', despatch.postcode AS 'Postcode', despatch.projectnumber AS 'Project #', despatch.createddate AS 'Created Date', job.productname AS 'Product', despatch.createduser AS 'Created By', despatch.deliverymethod AS 'Method', despatch.description AS 'Description', despatch.notes AS 'Delivery Notes'"

'l_strFields = "despatchnumber, jobID, companyname, contactname, description, postcode, direction, location, createddate, createduser, deadlinedate, deliverymethod, despatchID"


If cmbCompany.Text <> "" Then l_strSQL = l_strSQL & "AND (despatch.companyname LIKE '" & QuoteSanitise(cmbCompany.Text) & "%') "
If txtDescription.Text <> "" Then l_strSQL = l_strSQL & "AND (despatch.description LIKE '" & QuoteSanitise(txtDescription.Text) & "%') "
If cmbDirection.Text <> "" Then l_strSQL = l_strSQL & "AND (despatch.direction LIKE '" & QuoteSanitise(cmbDirection.Text) & "%') "
If txtJobID.Text <> "" Then l_strSQL = l_strSQL & "AND (despatch.jobID LIKE '" & QuoteSanitise(txtJobID.Text) & "%') "
If txtAddress.Text <> "" Then l_strSQL = l_strSQL & "AND (despatch.address LIKE '%" & QuoteSanitise(txtAddress.Text) & "%') "
If txtPostCode.Text <> "" Then l_strSQL = l_strSQL & "AND (despatch.postcode LIKE '" & QuoteSanitise(txtPostCode.Text) & "%') "
If cmbLocation.Text <> "" Then l_strSQL = l_strSQL & "AND (despatch.location LIKE '" & QuoteSanitise(cmbLocation.Text) & "%') "
If txtContact.Text <> "" Then l_strSQL = l_strSQL & "AND (despatch.contactname LIKE '" & QuoteSanitise(txtContact.Text) & "%') "
If cmbDeliveryMethod.Text <> "" Then l_strSQL = l_strSQL & "AND (despatch.deliverymethod LIKE '" & QuoteSanitise(cmbDeliveryMethod.Text) & "%') "

If txtReference.Text <> "" Then l_strSQL = l_strSQL & "AND (despatch.courierreference LIKE '" & QuoteSanitise(txtReference.Text) & "%') "

If cmbProduct.Text <> "" Then l_strSQL = l_strSQL & "AND (job.productname LIKE '" & QuoteSanitise(cmbProduct.Text) & "%') "

If Not IsNull(datModifiedDateFrom.Value) Then
    Dim l_datModifiedDateFrom As Date
    l_datModifiedDateFrom = Format(datModifiedDateFrom.Value, vbShortDateFormat)
    l_strSQL = l_strSQL & "AND (despatch.modifieddate >= '" & FormatSQLDate(l_datModifiedDateFrom & " 00:00") & "') "
End If

If Not IsNull(datModifiedDateTo.Value) Then
    Dim l_datModifiedDateTo As Date
    l_datModifiedDateTo = Format(datModifiedDateTo.Value, vbShortDateFormat)
    l_strSQL = l_strSQL & "AND (despatch.modifieddate <= '" & FormatSQLDate(l_datModifiedDateTo & " 23:59") & "') "
End If

If Not IsNull(datCreatedDateFrom.Value) Then
    Dim l_datCreatedDateFrom As Date
    l_datCreatedDateFrom = Format(datCreatedDateFrom.Value, vbShortDateFormat)
    l_strSQL = l_strSQL & "AND (despatch.createddate >= '" & FormatSQLDate(l_datCreatedDateFrom & " 00:00") & "') "
End If

If Not IsNull(datCreatedDateTo.Value) Then
    Dim l_datCreatedDateTo As Date
    l_datCreatedDateTo = Format(datCreatedDateTo.Value, vbShortDateFormat)
    l_strSQL = l_strSQL & "AND (despatch.createddate <= '" & FormatSQLDate(l_datCreatedDateTo & " 23:59") & "') "
End If

'this bit here is for searching the despatch detail forms

If txtTitle.Text <> "" Then
    l_strSQL = l_strSQL & " AND (despatchdetail.title LIKE '" & QuoteSanitise(txtTitle.Text) & "%') "
    l_blnIncludeDetail = True
End If
If txtSubTitle.Text <> "" Then
    l_strSQL = l_strSQL & " AND (despatchdetail.subtitle LIKE '" & QuoteSanitise(txtSubTitle.Text) & "%') "
    l_blnIncludeDetail = True
End If
If txtBarcode.Text <> "" Then
    l_strSQL = l_strSQL & " AND (despatchdetail.barcode LIKE '" & QuoteSanitise(txtBarcode.Text) & "%') "
    l_blnIncludeDetail = True
End If
If txtFormat.Text <> "" Then
    l_strSQL = l_strSQL & " AND (despatchdetail.format LIKE '" & QuoteSanitise(txtFormat.Text) & "%') "
    l_blnIncludeDetail = True
End If
If txtStandard.Text <> "" Then
    l_strSQL = l_strSQL & " AND (despatchdetail.videostandard LIKE '" & QuoteSanitise(txtStandard.Text) & "%') "
    l_blnIncludeDetail = True
End If

'create the SQL
m_strSQLSearch = "SELECT " & l_strFields & " FROM despatch "
m_strSQLSearch = m_strSQLSearch & "LEFT JOIN job ON despatch.jobID = job.jobID "
If l_blnIncludeDetail = True Then m_strSQLSearch = m_strSQLSearch & "LEFT JOIN despatchdetail ON despatch.despatchID = despatchdetail.despatchID "
m_strSQLSearch = m_strSQLSearch & "WHERE (despatch.despatchID IS NOT NULL) "
m_strSQLSearch = m_strSQLSearch & l_strSQL

m_strSQLOrderby = "ORDER BY despatch.createddate"

adoLibrary.RecordSource = m_strSQLSearch & m_strSQLOrderby
adoLibrary.ConnectionString = g_strConnection
adoLibrary.Refresh

adoLibrary.Caption = "Records: " & grdLibrary.Rows

End Sub

Private Sub Form_Load()

MakeLookLikeOffice Me

'populate the company drop down (data bound)
Dim l_strSQL As String
l_strSQL = "SELECT name, accountcode, telephone, companyID FROM company WHERE system_active = 1 ORDER BY name"

adoCompany.ConnectionString = g_strConnection
adoCompany.RecordSource = l_strSQL
adoCompany.Refresh

'populate all the other combos, including ones bound to the grid
PopulateCombo "despatchmethod", cmbDeliveryMethod
PopulateCombo "location", cmbLocation

cmbDirection.AddItem "OUT"
cmbDirection.AddItem "IN"
cmbDirection.Text = "OUT"

CenterForm Me
End Sub

Private Sub Form_Resize()

On Error Resume Next


picFooter.Top = Me.ScaleHeight - picFooter.height - 120
picFooter.Left = Me.ScaleWidth - picFooter.width - 120
grdLibrary.width = Me.ScaleWidth - 240
grdDespatchDetail.width = Me.ScaleWidth - 240

grdLibrary.height = Me.ScaleHeight - picFooter.height - 120 - 120 - grdLibrary.Top - grdDespatchDetail.height
grdDespatchDetail.Top = grdLibrary.Top + grdLibrary.height - 10

End Sub

Private Sub grdLibrary_DblClick()
If grdLibrary.Row = grdLibrary.Rows Then Exit Sub
ShowDespatchDetail GetData("despatch", "despatchID", "despatchnumber", grdLibrary.Columns("Dnote #").Text)
End Sub

Private Sub grdLibrary_HeadClick(ByVal ColIndex As Integer)

m_strSQLOrderby = "ORDER BY '" & grdLibrary.Columns(ColIndex).DataField & "'"

adoLibrary.RecordSource = m_strSQLSearch & m_strSQLOrderby
adoLibrary.ConnectionString = g_strConnection
adoLibrary.Refresh

End Sub
