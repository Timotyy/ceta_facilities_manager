VERSION 5.00
Object = "{3C62B3DD-12BE-4941-A787-EA25415DCD27}#10.0#0"; "crviewer.dll"
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmJob 
   Caption         =   "Job Details"
   ClientHeight    =   15615
   ClientLeft      =   2055
   ClientTop       =   3825
   ClientWidth     =   28560
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmJob.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   15615
   ScaleWidth      =   28560
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtDADCLegacyOracTVAENumber 
      Height          =   285
      Left            =   19740
      TabIndex        =   329
      ToolTipText     =   "The clients purchase order reference number"
      Top             =   1800
      Visible         =   0   'False
      Width           =   3975
   End
   Begin VB.TextBox txtDADCSeriesID 
      Height          =   285
      Left            =   19740
      TabIndex        =   327
      ToolTipText     =   "The clients purchase order reference number"
      Top             =   1440
      Visible         =   0   'False
      Width           =   3975
   End
   Begin VB.CommandButton cmdMakeSMVJobs 
      Caption         =   "Create Batch of SMV Job #"
      Height          =   615
      Left            =   16080
      TabIndex        =   316
      Top             =   900
      Width           =   1275
   End
   Begin VB.TextBox txtClientContractReference 
      Height          =   285
      Left            =   11400
      TabIndex        =   308
      ToolTipText     =   "The clients purchase order reference number"
      Top             =   1860
      Width           =   2895
   End
   Begin VB.TextBox txtSeriesID 
      Height          =   285
      Left            =   1380
      TabIndex        =   299
      ToolTipText     =   "The title for the job"
      Top             =   780
      Width           =   1155
   End
   Begin VB.CommandButton cmdConfirmWebOrder 
      Caption         =   "Confirm Submitted Job"
      Height          =   495
      Left            =   16080
      TabIndex        =   298
      Top             =   1560
      Width           =   1275
   End
   Begin VB.TextBox txtAllocatedTo 
      Height          =   285
      Left            =   6180
      TabIndex        =   295
      ToolTipText     =   "The clients purchase order reference number"
      Top             =   2220
      Width           =   2895
   End
   Begin VB.CommandButton cmdNewOrderEntry 
      Caption         =   "Create New Multi-Episode Job"
      Height          =   795
      Left            =   16080
      TabIndex        =   293
      Top             =   60
      Width           =   1275
   End
   Begin VB.TextBox txtTimeRequired 
      Height          =   285
      Left            =   1380
      TabIndex        =   287
      ToolTipText     =   "The title for the job"
      Top             =   1860
      Width           =   1155
   End
   Begin TabDlg.SSTab tabReplacementTabs 
      Height          =   345
      Left            =   300
      TabIndex        =   87
      Top             =   2760
      Width           =   27015
      _ExtentX        =   47651
      _ExtentY        =   609
      _Version        =   393216
      Tabs            =   14
      Tab             =   1
      TabsPerRow      =   14
      TabHeight       =   520
      TabCaption(0)   =   "Schedule"
      TabPicture(0)   =   "frmJob.frx":6852
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Line2(1)"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Dubbing"
      TabPicture(1)   =   "frmJob.frx":686E
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Line2(0)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Media"
      TabPicture(2)   =   "frmJob.frx":688A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Line2(2)"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Notes"
      TabPicture(3)   =   "frmJob.frx":68A6
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Line2(3)"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "DADC SVensk"
      TabPicture(4)   =   "frmJob.frx":68C2
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Line2(4)"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "Costing"
      TabPicture(5)   =   "frmJob.frx":68DE
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "Line2(5)"
      Tab(5).ControlCount=   1
      TabCaption(6)   =   "Invoice"
      TabPicture(6)   =   "frmJob.frx":68FA
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "Line2(6)"
      Tab(6).ControlCount=   1
      TabCaption(7)   =   "Sundry"
      TabPicture(7)   =   "frmJob.frx":6916
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "Line2(7)"
      Tab(7).ControlCount=   1
      TabCaption(8)   =   "Del/Col"
      TabPicture(8)   =   "frmJob.frx":6932
      Tab(8).ControlEnabled=   0   'False
      Tab(8).Control(0)=   "Line2(8)"
      Tab(8).ControlCount=   1
      TabCaption(9)   =   "Delivery Contacts"
      TabPicture(9)   =   "frmJob.frx":694E
      Tab(9).ControlEnabled=   0   'False
      Tab(9).Control(0)=   "Line2(9)"
      Tab(9).ControlCount=   1
      TabCaption(10)  =   "History"
      TabPicture(10)  =   "frmJob.frx":696A
      Tab(10).ControlEnabled=   0   'False
      Tab(10).Control(0)=   "Line2(10)"
      Tab(10).ControlCount=   1
      TabCaption(11)  =   "BBC WW"
      TabPicture(11)  =   "frmJob.frx":6986
      Tab(11).ControlEnabled=   0   'False
      Tab(11).ControlCount=   0
      TabCaption(12)  =   "Portal Billing"
      TabPicture(12)  =   "frmJob.frx":69A2
      Tab(12).ControlEnabled=   0   'False
      Tab(12).ControlCount=   0
      TabCaption(13)  =   "Wellcome"
      TabPicture(13)  =   "frmJob.frx":69BE
      Tab(13).ControlEnabled=   0   'False
      Tab(13).ControlCount=   0
      Begin VB.Line Line2 
         BorderColor     =   &H8000000F&
         BorderWidth     =   5
         Index           =   7
         X1              =   -69080
         X2              =   -69720
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line2 
         BorderColor     =   &H8000000F&
         BorderWidth     =   5
         Index           =   10
         X1              =   -66730
         X2              =   -67440
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line2 
         BorderColor     =   &H8000000F&
         BorderWidth     =   5
         Index           =   9
         X1              =   -67470
         X2              =   -68340
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line2 
         BorderColor     =   &H8000000F&
         BorderWidth     =   5
         Index           =   8
         X1              =   -68350
         X2              =   -69060
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line2 
         BorderColor     =   &H8000000F&
         BorderWidth     =   5
         Index           =   6
         X1              =   -69720
         X2              =   -70440
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line2 
         BorderColor     =   &H8000000F&
         BorderWidth     =   5
         Index           =   5
         X1              =   -70450
         X2              =   -71170
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line2 
         BorderColor     =   &H8000000F&
         BorderWidth     =   5
         Index           =   4
         X1              =   -71210
         X2              =   -72060
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line2 
         BorderColor     =   &H8000000F&
         BorderWidth     =   5
         Index           =   3
         X1              =   -72070
         X2              =   -72670
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line2 
         BorderColor     =   &H8000000F&
         BorderWidth     =   5
         Index           =   2
         X1              =   -72710
         X2              =   -73320
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line2 
         BorderColor     =   &H8000000F&
         BorderWidth     =   5
         Index           =   1
         X1              =   -74135
         X2              =   -74970
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line2 
         BorderColor     =   &H8000000F&
         BorderWidth     =   5
         Index           =   0
         X1              =   1670
         X2              =   900
         Y1              =   600
         Y2              =   600
      End
   End
   Begin TabDlg.SSTab tab1 
      Height          =   8775
      Left            =   60
      TabIndex        =   45
      TabStop         =   0   'False
      Top             =   2580
      Width           =   28635
      _ExtentX        =   50509
      _ExtentY        =   15478
      _Version        =   393216
      Tabs            =   14
      Tab             =   11
      TabsPerRow      =   14
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Dubbing"
      TabPicture(0)   =   "frmJob.frx":69DA
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "cmdMarkFaulty"
      Tab(0).Control(1)=   "cmdResetRowNumbering"
      Tab(0).Control(2)=   "ddnDiscStores"
      Tab(0).Control(3)=   "cmdCompleteAllRows"
      Tab(0).Control(4)=   "cmdChargeAllItems"
      Tab(0).Control(5)=   "picScheduleFooter(1)"
      Tab(0).Control(6)=   "cmdOperations"
      Tab(0).Control(7)=   "chkRealMinutes"
      Tab(0).Control(8)=   "cmdAddMaster"
      Tab(0).Control(9)=   "ddnDigital"
      Tab(0).Control(10)=   "txtVTStartedBy"
      Tab(0).Control(11)=   "ddnPlayouts"
      Tab(0).Control(12)=   "ddnCopyList"
      Tab(0).Control(13)=   "cmdAddLibraryEvent(0)"
      Tab(0).Control(14)=   "cmbDeadlineTime"
      Tab(0).Control(15)=   "ddnTimeCode"
      Tab(0).Control(16)=   "cmdMoveDown"
      Tab(0).Control(17)=   "cmdMoveUp"
      Tab(0).Control(18)=   "cmdDuplicateRow"
      Tab(0).Control(19)=   "ddnOthers"
      Tab(0).Control(20)=   "ddnStockType"
      Tab(0).Control(21)=   "ddnStandard"
      Tab(0).Control(22)=   "ddnSoundChannel"
      Tab(0).Control(23)=   "ddnRatio"
      Tab(0).Control(24)=   "ddnFormat"
      Tab(0).Control(25)=   "datDeadlineDate"
      Tab(0).Control(26)=   "ddnInclusive"
      Tab(0).Control(27)=   "grdDetail"
      Tab(0).Control(28)=   "adoJobDetail"
      Tab(0).Control(29)=   "lblCaption(59)"
      Tab(0).Control(30)=   "lblCaption(7)"
      Tab(0).Control(31)=   "Des(6)"
      Tab(0).ControlCount=   32
      TabCaption(1)   =   "Costing"
      TabPicture(1)   =   "frmJob.frx":69F6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "picCostingTotals"
      Tab(1).Control(1)=   "picCostingFooter"
      Tab(1).Control(2)=   "ddnUnit"
      Tab(1).Control(3)=   "txtAccounts"
      Tab(1).Control(4)=   "ddnRateCard"
      Tab(1).Control(5)=   "grdCostingDetail"
      Tab(1).Control(6)=   "lblCaption(97)"
      Tab(1).Control(7)=   "lblSentToAccountsDate(1)"
      Tab(1).Control(8)=   "lblInvoicedDate"
      Tab(1).Control(9)=   "lblCostedDate"
      Tab(1).Control(10)=   "lblHoldCostDate"
      Tab(1).Control(11)=   "lblCaption(96)"
      Tab(1).Control(12)=   "lblCaption(95)"
      Tab(1).Control(13)=   "lblCaption(85)"
      Tab(1).Control(14)=   "lblCaption(22)"
      Tab(1).ControlCount=   15
      TabCaption(2)   =   "Media"
      TabPicture(2)   =   "frmJob.frx":6A12
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "grdMediaFiles"
      Tab(2).Control(1)=   "txtMediaRequiredBarcode"
      Tab(2).Control(2)=   "txtBarcode"
      Tab(2).Control(3)=   "cmdAddLibraryEvent(1)"
      Tab(2).Control(4)=   "grdMedia"
      Tab(2).Control(5)=   "grdRequiredMedia"
      Tab(2).Control(6)=   "grdTechReviewMedia"
      Tab(2).Control(7)=   "grdRequiredFiles"
      Tab(2).Control(8)=   "grdTechReviewMediaFiles"
      Tab(2).Control(9)=   "lblCaption(29)"
      Tab(2).Control(10)=   "lblTotalMediaSize"
      Tab(2).Control(11)=   "lblCaption(98)"
      Tab(2).Control(12)=   "lblCaption(70)"
      Tab(2).ControlCount=   13
      TabCaption(3)   =   "Notes"
      TabPicture(3)   =   "frmJob.frx":6A2E
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "cmdReplaceProducerNotes"
      Tab(3).Control(1)=   "txtProducerNotes"
      Tab(3).Control(2)=   "cmdAddProducerNotes"
      Tab(3).Control(3)=   "chkClearAfterAdd"
      Tab(3).Control(4)=   "cmdAddDespatchNotes"
      Tab(3).Control(5)=   "cmdAddOperatorNotes"
      Tab(3).Control(6)=   "cmdAddOffice"
      Tab(3).Control(7)=   "txtInsertNotes"
      Tab(3).Control(8)=   "txtDespatch"
      Tab(3).Control(9)=   "txtOfficeClient"
      Tab(3).Control(10)=   "txtOperator"
      Tab(3).Control(11)=   "cmdReplaceOffice"
      Tab(3).Control(12)=   "cmdReplaceOperator"
      Tab(3).Control(13)=   "cmdReplaceDespatch"
      Tab(3).Control(14)=   "lblCaption(88)"
      Tab(3).Control(15)=   "lblCaption(84)"
      Tab(3).Control(16)=   "Line5"
      Tab(3).Control(17)=   "lblCaption(11)"
      Tab(3).Control(18)=   "lblCaption(23)"
      Tab(3).Control(19)=   "lblCaption(21)"
      Tab(3).Control(20)=   "lblCaption(20)"
      Tab(3).Control(21)=   "lblCaption(19)"
      Tab(3).ControlCount=   22
      TabCaption(4)   =   "Invoice"
      TabPicture(4)   =   "frmJob.frx":6A4A
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "lblCaption(68)"
      Tab(4).Control(1)=   "lblCaption(69)"
      Tab(4).Control(2)=   "lblCaption(71)"
      Tab(4).Control(3)=   "Line6"
      Tab(4).Control(4)=   "lblCaption(72)"
      Tab(4).Control(5)=   "lblInvoiceNumber"
      Tab(4).Control(6)=   "lblCaption(27)"
      Tab(4).Control(7)=   "lblCaption(91)"
      Tab(4).Control(8)=   "lblCreditNoteNumber"
      Tab(4).Control(9)=   "datInvoiceDate"
      Tab(4).Control(10)=   "CRViewer91"
      Tab(4).Control(11)=   "txtInvoiceText"
      Tab(4).Control(12)=   "txtInvoiceNotes"
      Tab(4).Control(13)=   "cmdInsertInvoiceNotes"
      Tab(4).Control(14)=   "txtInsertInvoiceNotes"
      Tab(4).Control(15)=   "cmdPreviewInvoice"
      Tab(4).Control(16)=   "cmdResetInvoiceNumber"
      Tab(4).Control(17)=   "cmdPasteDubbingClocks"
      Tab(4).Control(18)=   "fraChoseInvoice"
      Tab(4).ControlCount=   19
      TabCaption(5)   =   "BBC WW"
      TabPicture(5)   =   "frmJob.frx":6A66
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "grdExtendedJobDetail"
      Tab(5).Control(1)=   "txtBBCBusinessArea"
      Tab(5).Control(2)=   "txtBBCBookingNumber"
      Tab(5).Control(3)=   "txtBBCCountryCode"
      Tab(5).Control(4)=   "txtBBCNominalCode"
      Tab(5).Control(5)=   "txtBBCBusinessAreaCode"
      Tab(5).Control(6)=   "txtBBCChargeCode"
      Tab(5).Control(7)=   "txtBBCCustomername"
      Tab(5).Control(8)=   "lblCaption(106)"
      Tab(5).Control(9)=   "lblCaption(105)"
      Tab(5).Control(10)=   "lblCaption(104)"
      Tab(5).Control(11)=   "lblCaption(103)"
      Tab(5).Control(12)=   "lblCaption(102)"
      Tab(5).Control(13)=   "lblCaption(101)"
      Tab(5).Control(14)=   "lblCaption(100)"
      Tab(5).ControlCount=   15
      TabCaption(6)   =   "DADC Svensk"
      TabPicture(6)   =   "frmJob.frx":6A82
      Tab(6).ControlEnabled=   0   'False
      Tab(6).ControlCount=   0
      TabCaption(7)   =   "Schedule"
      TabPicture(7)   =   "frmJob.frx":6A9E
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "picScheduleFooter(0)"
      Tab(7).Control(1)=   "picChargeDetails"
      Tab(7).ControlCount=   2
      TabCaption(8)   =   "Notification Contacts"
      TabPicture(8)   =   "frmJob.frx":6ABA
      Tab(8).ControlEnabled=   0   'False
      Tab(8).Control(0)=   "txtNotificationMessageBody"
      Tab(8).Control(1)=   "txtNotificationSubjectLine"
      Tab(8).Control(2)=   "chkEnforceContacts"
      Tab(8).Control(3)=   "ddnCompanyContact"
      Tab(8).Control(4)=   "adoCompanyContact"
      Tab(8).Control(5)=   "grdJobContacts"
      Tab(8).Control(6)=   "adoJobContact"
      Tab(8).Control(7)=   "lblCaption(60)"
      Tab(8).Control(8)=   "lblCaption(55)"
      Tab(8).ControlCount=   9
      TabCaption(9)   =   "Sundry Costs"
      TabPicture(9)   =   "frmJob.frx":6AD6
      Tab(9).ControlEnabled=   0   'False
      Tab(9).Control(0)=   "Shape1"
      Tab(9).Control(1)=   "Shape2"
      Tab(9).Control(2)=   "Shape3"
      Tab(9).Control(3)=   "lblCaption(64)"
      Tab(9).Control(4)=   "lblCaption(65)"
      Tab(9).Control(5)=   "lblCaption(66)"
      Tab(9).Control(6)=   "cmdNewSundry"
      Tab(9).Control(7)=   "cmdAuthoriseSundry"
      Tab(9).Control(8)=   "grdSundryCosts"
      Tab(9).Control(9)=   "cmdApprove"
      Tab(9).ControlCount=   10
      TabCaption(10)  =   "Del/Col"
      TabPicture(10)  =   "frmJob.frx":6AF2
      Tab(10).ControlEnabled=   0   'False
      Tab(10).Control(0)=   "cmdCopyToCollection"
      Tab(10).Control(0).Enabled=   0   'False
      Tab(10).Control(1)=   "adoDelAddress"
      Tab(10).Control(1).Enabled=   0   'False
      Tab(10).Control(2)=   "chkUseCompany"
      Tab(10).Control(2).Enabled=   0   'False
      Tab(10).Control(3)=   "Frame3"
      Tab(10).Control(3).Enabled=   0   'False
      Tab(10).Control(4)=   "fraDeliveryAddress"
      Tab(10).Control(4).Enabled=   0   'False
      Tab(10).Control(5)=   "lblCaption(89)"
      Tab(10).Control(5).Enabled=   0   'False
      Tab(10).ControlCount=   6
      TabCaption(11)  =   "Technical"
      TabPicture(11)  =   "frmJob.frx":6B0E
      Tab(11).ControlEnabled=   -1  'True
      Tab(11).Control(0)=   "lblCaption(53)"
      Tab(11).Control(0).Enabled=   0   'False
      Tab(11).Control(1)=   "lblCaption(58)"
      Tab(11).Control(1).Enabled=   0   'False
      Tab(11).Control(2)=   "lblTotalPrints"
      Tab(11).Control(2).Enabled=   0   'False
      Tab(11).Control(3)=   "lblCaption(61)"
      Tab(11).Control(3).Enabled=   0   'False
      Tab(11).Control(4)=   "lblPrintedBy"
      Tab(11).Control(4).Enabled=   0   'False
      Tab(11).Control(5)=   "lblSentToAccountsBatchNumber"
      Tab(11).Control(5).Enabled=   0   'False
      Tab(11).Control(6)=   "lblCaption(62)"
      Tab(11).Control(6).Enabled=   0   'False
      Tab(11).Control(7)=   "lblSentToAccountsDate(0)"
      Tab(11).Control(7).Enabled=   0   'False
      Tab(11).Control(8)=   "lblCaption(63)"
      Tab(11).Control(8).Enabled=   0   'False
      Tab(11).Control(9)=   "adoHistory"
      Tab(11).Control(9).Enabled=   0   'False
      Tab(11).Control(10)=   "txtMasterJobID"
      Tab(11).Control(10).Enabled=   0   'False
      Tab(11).Control(11)=   "SSOleDBGrid1"
      Tab(11).Control(11).Enabled=   0   'False
      Tab(11).Control(12)=   "Frame1"
      Tab(11).Control(12).Enabled=   0   'False
      Tab(11).Control(13)=   "grdHistory"
      Tab(11).Control(13).Enabled=   0   'False
      Tab(11).ControlCount=   14
      TabCaption(12)  =   "Portal Billing"
      TabPicture(12)  =   "frmJob.frx":6B2A
      Tab(12).ControlEnabled=   0   'False
      Tab(12).Control(0)=   "cmdAssignSelectedUnbilledItems"
      Tab(12).Control(1)=   "cmdSelectAllUnbilled"
      Tab(12).Control(2)=   "cmdClearJunk"
      Tab(12).Control(3)=   "cmdAssignAllUnbilledItems"
      Tab(12).Control(4)=   "cmdAssignUnbilledItem"
      Tab(12).Control(5)=   "grdDetailCopy"
      Tab(12).Control(6)=   "grdUnbilled"
      Tab(12).ControlCount=   7
      TabCaption(13)  =   "Tracker Billing"
      TabPicture(13)  =   "frmJob.frx":6B46
      Tab(13).ControlEnabled=   0   'False
      Tab(13).Control(0)=   "cmdBillProgressItem"
      Tab(13).Control(1)=   "grdDetailAnotherCopy"
      Tab(13).Control(2)=   "grdProgressItems"
      Tab(13).ControlCount=   3
      Begin VB.TextBox txtNotificationMessageBody 
         Height          =   6165
         Left            =   -60780
         MultiLine       =   -1  'True
         TabIndex        =   323
         ToolTipText     =   "The title for the job"
         Top             =   1320
         Width           =   5535
      End
      Begin VB.TextBox txtNotificationSubjectLine 
         Height          =   285
         Left            =   -60780
         TabIndex        =   321
         ToolTipText     =   "The title for the job"
         Top             =   960
         Width           =   5535
      End
      Begin VB.CommandButton cmdMarkFaulty 
         Caption         =   "Mark Current Row as Faulty"
         Enabled         =   0   'False
         Height          =   315
         Left            =   -51840
         TabIndex        =   317
         ToolTipText     =   "Move the selected row down"
         Top             =   420
         Width           =   2295
      End
      Begin VB.CommandButton cmdAssignSelectedUnbilledItems 
         Cancel          =   -1  'True
         Caption         =   "<< Sel"
         Height          =   615
         Left            =   -63420
         TabIndex        =   315
         Top             =   1080
         Width           =   555
      End
      Begin VB.CommandButton cmdSelectAllUnbilled 
         Caption         =   "Select All"
         Height          =   615
         Left            =   -63420
         TabIndex        =   314
         Top             =   2520
         Width           =   555
      End
      Begin VB.CommandButton cmdResetRowNumbering 
         Caption         =   "Reset Row Numbers"
         Height          =   315
         Left            =   -56340
         TabIndex        =   312
         ToolTipText     =   "Move the selected row down"
         Top             =   420
         Width           =   1635
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnDiscStores 
         Height          =   1755
         Left            =   -59040
         TabIndex        =   311
         Top             =   2580
         Width           =   6675
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         DefColWidth     =   8819
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   9710
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Column 0"
         Columns(0).FieldLen=   256
         Columns(1).Width=   8819
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "format"
         Columns(1).Name =   "format"
         Columns(1).DataField=   "Column 1"
         Columns(1).FieldLen=   256
         _ExtentX        =   11774
         _ExtentY        =   3096
         _StockProps     =   77
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.CheckBox chkEnforceContacts 
         Caption         =   "Enforce Company Contacts"
         Height          =   315
         Left            =   -63600
         TabIndex        =   310
         Tag             =   "NOCLEAR"
         Top             =   540
         Value           =   1  'Checked
         Width           =   2295
      End
      Begin VB.Frame fraChoseInvoice 
         Caption         =   "Invoice Type"
         Height          =   495
         Left            =   -60480
         TabIndex        =   304
         Top             =   360
         Width           =   5415
         Begin VB.OptionButton optInvoiceType 
            Caption         =   "VDMS Temp"
            Height          =   195
            Index           =   1
            Left            =   1440
            TabIndex        =   306
            Top             =   240
            Width           =   1395
         End
         Begin VB.OptionButton optInvoiceType 
            Caption         =   "MX1"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   305
            Tag             =   "NOCLEAR"
            Top             =   240
            Value           =   -1  'True
            Width           =   1275
         End
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnCompanyContact 
         Bindings        =   "frmJob.frx":6B62
         Height          =   1575
         Left            =   -53760
         TabIndex        =   302
         Top             =   5580
         Width           =   6855
         DataFieldList   =   "email"
         MaxDropDownItems=   12
         _Version        =   196617
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "companycontactID"
         Columns(0).Name =   "companycontactID"
         Columns(0).DataField=   "companycontactID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "companyID"
         Columns(1).Name =   "companyID"
         Columns(1).DataField=   "companyID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   5847
         Columns(2).Caption=   "name"
         Columns(2).Name =   "name"
         Columns(2).DataField=   "name"
         Columns(2).FieldLen=   256
         Columns(3).Width=   5662
         Columns(3).Caption=   "email"
         Columns(3).Name =   "email"
         Columns(3).DataField=   "email"
         Columns(3).FieldLen=   256
         _ExtentX        =   12091
         _ExtentY        =   2778
         _StockProps     =   77
         DataFieldToDisplay=   "email"
      End
      Begin MSAdodcLib.Adodc adoCompanyContact 
         Height          =   330
         Left            =   -53760
         Top             =   4680
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoCompanyContact"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.CommandButton cmdClearJunk 
         Caption         =   "Clear Junk"
         Height          =   615
         Left            =   -63420
         TabIndex        =   297
         Top             =   3240
         Width           =   555
      End
      Begin VB.CommandButton cmdCompleteAllRows 
         Caption         =   "Complete All Rows"
         Height          =   315
         Left            =   -58440
         TabIndex        =   292
         ToolTipText     =   "Move the selected row down"
         Top             =   420
         Width           =   1635
      End
      Begin VB.CommandButton cmdChargeAllItems 
         Caption         =   "Invoice All"
         Height          =   315
         Left            =   -60120
         TabIndex        =   291
         ToolTipText     =   "Move the selected row down"
         Top             =   420
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.CommandButton cmdAssignAllUnbilledItems 
         Caption         =   "<< All"
         Height          =   615
         Left            =   -63420
         TabIndex        =   289
         Top             =   1800
         Width           =   555
      End
      Begin VB.PictureBox picScheduleFooter 
         BorderStyle     =   0  'None
         Height          =   1815
         Index           =   1
         Left            =   -74820
         ScaleHeight     =   1815
         ScaleWidth      =   14235
         TabIndex        =   280
         Top             =   5640
         Width           =   14235
         Begin VB.TextBox txtOperator2 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0E0FF&
            Height          =   1515
            Index           =   1
            Left            =   0
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   284
            ToolTipText     =   "Operators Notes"
            Top             =   300
            Width           =   9135
         End
         Begin VB.TextBox txtInsertOperatorNotes2 
            Appearance      =   0  'Flat
            Height          =   1455
            Index           =   1
            Left            =   10620
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   283
            ToolTipText     =   "Notes to be inserted into Operator's notes field"
            Top             =   240
            Width           =   3435
         End
         Begin VB.CommandButton cmdAddOperatorNotes2 
            Caption         =   "<< Insert"
            Height          =   315
            Index           =   1
            Left            =   9240
            TabIndex        =   282
            ToolTipText     =   "Insert Notes"
            Top             =   300
            Width           =   1275
         End
         Begin VB.CheckBox chkClearOperatorNotes2 
            Alignment       =   1  'Right Justify
            Caption         =   "Clear after insert"
            Height          =   195
            Index           =   1
            Left            =   12480
            TabIndex        =   281
            Top             =   0
            Value           =   1  'Checked
            Width           =   1575
         End
         Begin VB.Label lblCaption 
            Caption         =   "Notes To Insert"
            Height          =   255
            Index           =   14
            Left            =   10620
            TabIndex        =   286
            Top             =   0
            Width           =   1635
         End
         Begin VB.Label lblCaption 
            Caption         =   "Operator / Technician Notes - SPECIFY JOB REQUIREMENTS IN THE ABOVE GRID... NOT IN THESE NOTES!!!!"
            Height          =   255
            Index           =   13
            Left            =   0
            TabIndex        =   285
            Top             =   0
            Width           =   8775
         End
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdMediaFiles 
         Bindings        =   "frmJob.frx":6B82
         Height          =   1395
         Left            =   -74880
         TabIndex        =   277
         Top             =   1980
         Width           =   19260
         _Version        =   196617
         RecordSelectors =   0   'False
         AllowUpdate     =   0   'False
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   14
         Columns(0).Width=   1482
         Columns(0).Caption=   "eventID"
         Columns(0).Name =   "eventID"
         Columns(0).DataField=   "eventID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   794
         Columns(1).Caption=   "Sr."
         Columns(1).Name =   "evenseries"
         Columns(1).DataField=   "eventseries"
         Columns(1).FieldLen=   256
         Columns(2).Width=   794
         Columns(2).Caption=   "Ep."
         Columns(2).Name =   "eventepisode"
         Columns(2).DataField=   "eventepisode"
         Columns(2).FieldLen=   256
         Columns(3).Width=   5292
         Columns(3).Caption=   "Title"
         Columns(3).Name =   "Title"
         Columns(3).DataField=   "eventtitle"
         Columns(3).FieldLen=   256
         Columns(4).Width=   5292
         Columns(4).Caption=   "Sub Title"
         Columns(4).Name =   "Sub Title"
         Columns(4).DataField=   "eventsubtitle"
         Columns(4).FieldLen=   256
         Columns(5).Width=   2117
         Columns(5).Caption=   "Format"
         Columns(5).Name =   "Format"
         Columns(5).DataField=   "clipformat"
         Columns(5).FieldLen=   256
         Columns(6).Width=   873
         Columns(6).Caption=   "Horiz"
         Columns(6).Name =   "Horiz."
         Columns(6).Alignment=   2
         Columns(6).DataField=   "cliphorizontalpixels"
         Columns(6).FieldLen=   256
         Columns(7).Width=   873
         Columns(7).Caption=   "Vert"
         Columns(7).Name =   "Vert."
         Columns(7).Alignment=   2
         Columns(7).DataField=   "clipverticalpixels"
         Columns(7).FieldLen=   256
         Columns(8).Width=   1640
         Columns(8).Caption=   "Bit Rate"
         Columns(8).Name =   "Bit Rate"
         Columns(8).DataField=   "clipbitrate"
         Columns(8).FieldLen=   256
         Columns(9).Width=   5292
         Columns(9).Caption=   "File Name"
         Columns(9).Name =   "File Name"
         Columns(9).DataField=   "clipfilename"
         Columns(9).FieldLen=   256
         Columns(10).Width=   2646
         Columns(10).Caption=   "Stored On"
         Columns(10).Name=   "clipstore"
         Columns(10).FieldLen=   256
         Columns(11).Width=   1402
         Columns(11).Caption=   "Size (GB)"
         Columns(11).Name=   "filesize"
         Columns(11).Alignment=   2
         Columns(11).FieldLen=   256
         Columns(12).Width=   4419
         Columns(12).Caption=   "Owned By"
         Columns(12).Name=   "Owned By"
         Columns(12).DataField=   "companyname"
         Columns(12).FieldLen=   256
         Columns(13).Width=   3200
         Columns(13).Visible=   0   'False
         Columns(13).Caption=   "libraryID"
         Columns(13).Name=   "libraryID"
         Columns(13).DataField=   "libraryID"
         Columns(13).FieldLen=   256
         _ExtentX        =   33972
         _ExtentY        =   2461
         _StockProps     =   79
         Caption         =   "Media Files Created by this Job"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdBillProgressItem 
         Caption         =   "<<"
         Height          =   495
         Left            =   -63420
         TabIndex        =   275
         Top             =   1680
         Width           =   555
      End
      Begin VB.CommandButton cmdAssignUnbilledItem 
         Caption         =   "<<"
         Height          =   495
         Left            =   -63420
         TabIndex        =   272
         Top             =   480
         Width           =   555
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdExtendedJobDetail 
         Bindings        =   "frmJob.frx":6B9C
         Height          =   3975
         Left            =   -74820
         TabIndex        =   269
         Top             =   1560
         Width           =   28155
         _Version        =   196617
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   25
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "extendedjobdetailid"
         Columns(0).Name =   "extendedjobdetailid"
         Columns(0).DataField=   "extendedjobdetailid"
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Name =   "jobdetailid"
         Columns(1).DataField=   "jobdetailid"
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   503
         Columns(2).Name =   "copytype"
         Columns(2).DataField=   "copytype"
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   7408
         Columns(3).Caption=   "Description"
         Columns(3).Name =   "description"
         Columns(3).DataField=   "description"
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   1720
         Columns(4).Caption=   "Format"
         Columns(4).Name =   "format"
         Columns(4).DataField=   "format"
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   3069
         Columns(5).Caption=   "BBC Customer Name"
         Columns(5).Name =   "bbccustomername"
         Columns(5).DataField=   "bbccustomername"
         Columns(5).FieldLen=   256
         Columns(6).Width=   2540
         Columns(6).Caption=   "BBC Charge Code"
         Columns(6).Name =   "bbcchargecode"
         Columns(6).DataField=   "bbcchargecode"
         Columns(6).FieldLen=   256
         Columns(7).Width=   3334
         Columns(7).Caption=   "BBC Business Area Code"
         Columns(7).Name =   "bbcbusinessareacode"
         Columns(7).DataField=   "bbcbusinessareacode"
         Columns(7).FieldLen=   256
         Columns(8).Width=   2540
         Columns(8).Caption=   "BBC Nominal Code"
         Columns(8).Name =   "bbcnominalcode"
         Columns(8).DataField=   "bbcnominalcode"
         Columns(8).FieldLen=   256
         Columns(9).Width=   2593
         Columns(9).Caption=   "BBC Country Code"
         Columns(9).Name =   "bbccountrycode"
         Columns(9).DataField=   "bbccountrycode"
         Columns(9).FieldLen=   256
         Columns(10).Width=   3757
         Columns(10).Caption=   "BBC Booking Number"
         Columns(10).Name=   "bbcbookingnumber"
         Columns(10).DataField=   "bbcbookingnumber"
         Columns(10).FieldLen=   256
         Columns(11).Width=   2011
         Columns(11).Caption=   "BBC Media ID"
         Columns(11).Name=   "bbcmediaID"
         Columns(11).DataField=   "bbcmediaID"
         Columns(11).FieldLen=   256
         Columns(12).Width=   2646
         Columns(12).Caption=   "BBC Programme ID"
         Columns(12).Name=   "bbcprogrammeID"
         Columns(12).DataField=   "bbcprogrammeID"
         Columns(12).FieldLen=   256
         Columns(13).Width=   2593
         Columns(13).Caption=   "BBC Business Area"
         Columns(13).Name=   "bbcbusinessarea"
         Columns(13).DataField=   "bbcbusinessarea"
         Columns(13).FieldLen=   256
         Columns(14).Width=   2672
         Columns(14).Caption=   "BBC Source Format"
         Columns(14).Name=   "bbcsourceformat"
         Columns(14).DataField=   "bbcsourceformat"
         Columns(14).FieldLen=   256
         Columns(15).Width=   2884
         Columns(15).Caption=   "BBC Record Format"
         Columns(15).Name=   "bbcrecordformat"
         Columns(15).DataField=   "bbcrecordformat"
         Columns(15).FieldLen=   256
         Columns(16).Width=   2646
         Columns(16).Caption=   "DespatchMethod"
         Columns(16).Name=   "DespatchMethod"
         Columns(16).DataField=   "DespatchMethod"
         Columns(16).FieldLen=   256
         Columns(17).Width=   2646
         Columns(17).Caption=   "CourierAccountNumber"
         Columns(17).Name=   "CourierAccountNumber"
         Columns(17).DataField=   "CourierAccountNumber"
         Columns(17).FieldLen=   256
         Columns(18).Width=   2117
         Columns(18).Caption=   "DespatchCostCentre"
         Columns(18).Name=   "DespatchCostCentre"
         Columns(18).DataField=   "DespatchCostCentre"
         Columns(18).FieldLen=   256
         Columns(19).Width=   2117
         Columns(19).Caption=   "DespatchNominalCode"
         Columns(19).Name=   "DespatchNominalCode"
         Columns(19).DataField=   "DespatchNominalCode"
         Columns(19).FieldLen=   256
         Columns(20).Width=   3200
         Columns(20).Caption=   "DespatchContactName"
         Columns(20).Name=   "DespatchContactName"
         Columns(20).DataField=   "DespatchContactName"
         Columns(20).FieldLen=   256
         Columns(21).Width=   3200
         Columns(21).Caption=   "DespatchTelephone"
         Columns(21).Name=   "DespatchTelephone"
         Columns(21).DataField=   "DespatchTelephone"
         Columns(21).FieldLen=   256
         Columns(22).Width=   3200
         Columns(22).Caption=   "MasterEmail"
         Columns(22).Name=   "MasterEmail"
         Columns(22).DataField=   "MasterEmail"
         Columns(22).FieldLen=   256
         Columns(23).Width=   3200
         Columns(23).Caption=   "DespatchAddress"
         Columns(23).Name=   "DespatchAddress"
         Columns(23).DataField=   "DespatchAddress"
         Columns(23).FieldLen=   256
         Columns(24).Width=   3200
         Columns(24).Caption=   "WaybillNumber"
         Columns(24).Name=   "WaybillNumber"
         Columns(24).DataField=   "WaybillNumber"
         Columns(24).FieldLen=   256
         _ExtentX        =   49662
         _ExtentY        =   7011
         _StockProps     =   79
         Caption         =   "BBC Extended Information"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox txtBBCBusinessArea 
         Height          =   285
         Left            =   -63480
         TabIndex        =   264
         ToolTipText     =   "The clients purchase order reference number"
         Top             =   780
         Width           =   1575
      End
      Begin VB.TextBox txtBBCBookingNumber 
         Height          =   285
         Left            =   -63480
         TabIndex        =   263
         ToolTipText     =   "The clients purchase order reference number"
         Top             =   420
         Width           =   1575
      End
      Begin VB.TextBox txtBBCCountryCode 
         Height          =   285
         Left            =   -67980
         TabIndex        =   262
         ToolTipText     =   "The clients purchase order reference number"
         Top             =   780
         Width           =   1575
      End
      Begin VB.TextBox txtBBCNominalCode 
         Height          =   285
         Left            =   -67980
         TabIndex        =   261
         ToolTipText     =   "The clients purchase order reference number"
         Top             =   420
         Width           =   1575
      End
      Begin VB.TextBox txtBBCBusinessAreaCode 
         Height          =   285
         Left            =   -73020
         TabIndex        =   259
         ToolTipText     =   "The clients purchase order reference number"
         Top             =   1140
         Width           =   1575
      End
      Begin VB.TextBox txtBBCChargeCode 
         Height          =   285
         Left            =   -73020
         TabIndex        =   257
         ToolTipText     =   "The clients purchase order reference number"
         Top             =   780
         Width           =   1575
      End
      Begin VB.TextBox txtBBCCustomername 
         Height          =   285
         Left            =   -73020
         TabIndex        =   255
         ToolTipText     =   "The clients purchase order reference number"
         Top             =   420
         Width           =   2895
      End
      Begin VB.CommandButton cmdOperations 
         Caption         =   "Done?"
         Height          =   315
         Left            =   -61620
         TabIndex        =   254
         ToolTipText     =   "Move the selected row down"
         Top             =   420
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.PictureBox picCostingTotals 
         BorderStyle     =   0  'None
         Height          =   1275
         Left            =   -69360
         ScaleHeight     =   1275
         ScaleWidth      =   8715
         TabIndex        =   240
         Top             =   3360
         Visible         =   0   'False
         Width           =   8715
         Begin VB.TextBox txtAdditionalDiscount 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFC0C0&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   288
            Left            =   3300
            Locked          =   -1  'True
            TabIndex        =   320
            Top             =   300
            Width           =   1695
         End
         Begin VB.TextBox txtTotalIncVAT 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0FF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   288
            Left            =   6900
            Locked          =   -1  'True
            TabIndex        =   247
            Top             =   300
            Width           =   1695
         End
         Begin VB.TextBox txtTotal 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFC0C0&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   288
            Left            =   5100
            Locked          =   -1  'True
            TabIndex        =   246
            Top             =   300
            Width           =   1695
         End
         Begin VB.CommandButton cmdCollect 
            Caption         =   "Refresh Sundry Costs"
            Height          =   315
            Left            =   0
            TabIndex        =   245
            Top             =   420
            Width           =   1995
         End
         Begin VB.PictureBox picNewSundries 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   0
            ScaleHeight     =   255
            ScaleWidth      =   1995
            TabIndex        =   243
            Top             =   660
            Visible         =   0   'False
            Width           =   1995
            Begin VB.Label lblCaption 
               Alignment       =   2  'Center
               Caption         =   "Check Sundries!"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000C0&
               Height          =   255
               Index           =   67
               Left            =   240
               TabIndex        =   244
               Top             =   60
               Width           =   1515
            End
         End
         Begin VB.TextBox txtRateCardTotal 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00808080&
            Height          =   288
            Left            =   5100
            Locked          =   -1  'True
            TabIndex        =   242
            ToolTipText     =   "Double click for the approx gross (inc VAT) amount"
            Top             =   600
            Width           =   1695
         End
         Begin VB.TextBox txtTotalDiscountPercent 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00808080&
            Height          =   288
            Left            =   6900
            Locked          =   -1  'True
            TabIndex        =   241
            Top             =   600
            Width           =   1695
         End
         Begin MSComctlLib.ProgressBar ProgressBar1 
            Height          =   315
            Left            =   60
            TabIndex        =   313
            Top             =   960
            Visible         =   0   'False
            Width           =   8535
            _ExtentX        =   15055
            _ExtentY        =   556
            _Version        =   393216
            Appearance      =   1
         End
         Begin VB.Label lblCaption 
            Caption         =   "Total Additional Discount"
            Height          =   255
            Index           =   45
            Left            =   3240
            TabIndex        =   319
            Top             =   60
            Width           =   1875
         End
         Begin VB.Label lblCaption 
            Caption         =   "Grand Total Inc VAT"
            Height          =   255
            Index           =   18
            Left            =   6960
            TabIndex        =   251
            Top             =   60
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "Grand Total Net"
            Height          =   255
            Index           =   17
            Left            =   5160
            TabIndex        =   250
            Top             =   60
            Width           =   1575
         End
         Begin VB.Label lblCaption 
            Caption         =   "Credit Limit:"
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   93
            Left            =   60
            TabIndex        =   249
            Top             =   60
            Width           =   2355
         End
         Begin VB.Label lblCaption 
            Caption         =   "Rate Card:"
            Height          =   255
            Index           =   99
            Left            =   4200
            TabIndex        =   248
            Top             =   660
            Width           =   795
         End
      End
      Begin VB.PictureBox picCostingFooter 
         BorderStyle     =   0  'None
         Height          =   855
         Left            =   -69600
         ScaleHeight     =   855
         ScaleWidth      =   8955
         TabIndex        =   227
         Top             =   4680
         Visible         =   0   'False
         Width           =   8955
         Begin VB.CheckBox chkNoVAT 
            Caption         =   "Do not charge VAT"
            Height          =   375
            Left            =   240
            TabIndex        =   252
            Top             =   0
            Width           =   1155
         End
         Begin VB.CommandButton cmdMinimumCharge 
            Caption         =   "Min Charge"
            Enabled         =   0   'False
            Height          =   315
            Left            =   2760
            TabIndex        =   238
            Top             =   480
            Visible         =   0   'False
            Width           =   1155
         End
         Begin VB.CommandButton cmdReCost 
            Caption         =   "Re-Cost"
            Height          =   315
            Left            =   4020
            TabIndex        =   237
            ToolTipText     =   "Re-build the costs for this job from ratecard"
            Top             =   480
            Width           =   1155
         End
         Begin VB.CommandButton cmdInvoice 
            Caption         =   "Finalise"
            Height          =   315
            Left            =   7800
            TabIndex        =   236
            Top             =   480
            Width           =   1155
         End
         Begin VB.CommandButton cmdReinstate 
            Caption         =   "Re-instate"
            Height          =   315
            Left            =   1500
            TabIndex        =   235
            Top             =   480
            Width           =   1155
         End
         Begin VB.CommandButton cmdCreditNote 
            Caption         =   "Credit Job"
            Height          =   315
            Left            =   5280
            TabIndex        =   234
            Top             =   480
            Width           =   1155
         End
         Begin VB.CommandButton cmdCostingPrint 
            Caption         =   "Print Draft"
            Height          =   315
            Left            =   6540
            TabIndex        =   233
            Top             =   480
            Width           =   1155
         End
         Begin VB.TextBox txtCostingSheet 
            Alignment       =   2  'Center
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   3300
            TabIndex        =   232
            ToolTipText     =   "If this job is attached to a costing sheet, this is the ID"
            Top             =   0
            Width           =   1395
         End
         Begin VB.CommandButton cmdRemoveFromCosting 
            Caption         =   "Unattach"
            Height          =   315
            Left            =   5280
            TabIndex        =   231
            Top             =   0
            Width           =   1155
         End
         Begin VB.CommandButton cmdNoCharge 
            Caption         =   "No Charge"
            Height          =   315
            Left            =   6540
            TabIndex        =   230
            Top             =   0
            Width           =   1155
         End
         Begin VB.CommandButton cmdCopyCostsFrom 
            Caption         =   "Copy Costs..."
            Height          =   315
            Left            =   7800
            TabIndex        =   229
            ToolTipText     =   "Copy the costs from another job (requires job ID)"
            Top             =   0
            Width           =   1155
         End
         Begin VB.CommandButton cmdLockCosts 
            Caption         =   "Lock Costs"
            Height          =   315
            Left            =   240
            TabIndex        =   228
            Top             =   480
            Width           =   1155
         End
         Begin VB.Label lblCaption 
            Caption         =   "Costing Sheet Number"
            Height          =   255
            Index           =   0
            Left            =   1500
            TabIndex        =   239
            Top             =   0
            Width           =   1635
         End
         Begin VB.Line Line1 
            X1              =   240
            X2              =   8940
            Y1              =   405
            Y2              =   405
         End
      End
      Begin VB.CheckBox chkRealMinutes 
         Caption         =   "Real Minutes"
         Height          =   435
         Left            =   -66600
         TabIndex        =   225
         Top             =   360
         Width           =   855
      End
      Begin VB.TextBox txtMediaRequiredBarcode 
         BackColor       =   &H0080FFFF&
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   -66600
         TabIndex        =   223
         ToolTipText     =   "Scan a tape barcode to link it to this job number"
         Top             =   480
         Width           =   1695
      End
      Begin VB.PictureBox picChargeDetails 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   -69960
         ScaleHeight     =   375
         ScaleWidth      =   3735
         TabIndex        =   212
         Top             =   480
         Visible         =   0   'False
         Width           =   3735
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdHistory 
         Bindings        =   "frmJob.frx":6BBF
         Height          =   4335
         Left            =   120
         TabIndex        =   208
         TabStop         =   0   'False
         Top             =   1140
         Width           =   13995
         _Version        =   196617
         stylesets.count =   2
         stylesets(0).Name=   "HIRE"
         stylesets(0).BackColor=   8629747
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmJob.frx":6BD8
         stylesets(1).Name=   "FREELANCER"
         stylesets(1).BackColor=   14744811
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmJob.frx":6BF4
         AllowUpdate     =   0   'False
         RowSelectionStyle=   1
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         BackColorOdd    =   12648447
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Caption=   "Date"
         Columns(0).Name =   "cdate"
         Columns(0).DataField=   "cdate"
         Columns(0).FieldLen=   256
         Columns(1).Width=   1085
         Columns(1).Caption=   "User"
         Columns(1).Name =   "cuser"
         Columns(1).DataField=   "cuser"
         Columns(1).FieldLen=   256
         Columns(2).Width=   17648
         Columns(2).Caption=   "Description"
         Columns(2).Name =   "description"
         Columns(2).DataField=   "description"
         Columns(2).FieldLen=   256
         _ExtentX        =   24686
         _ExtentY        =   7646
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame Frame1 
         Caption         =   "New Technical Log Entry"
         Height          =   4995
         Left            =   120
         TabIndex        =   99
         Top             =   480
         Visible         =   0   'False
         Width           =   4275
         Begin VB.CommandButton Command2 
            Caption         =   "Clear"
            Height          =   315
            Left            =   1740
            TabIndex        =   112
            ToolTipText     =   "Print this job"
            Top             =   4560
            Width           =   1155
         End
         Begin VB.CommandButton Command1 
            Caption         =   "Add"
            Height          =   315
            Left            =   3000
            TabIndex        =   111
            ToolTipText     =   "Print this job"
            Top             =   4560
            Width           =   1155
         End
         Begin VB.CheckBox chkView 
            Caption         =   "Client"
            Height          =   255
            Index           =   3
            Left            =   1440
            TabIndex        =   106
            Top             =   2940
            Width           =   1155
         End
         Begin VB.CheckBox chkView 
            Caption         =   "Project"
            Height          =   255
            Index           =   2
            Left            =   1440
            TabIndex        =   104
            Top             =   2580
            Width           =   1155
         End
         Begin VB.CheckBox chkView 
            Caption         =   "Job"
            Height          =   255
            Index           =   1
            Left            =   1440
            TabIndex        =   103
            Top             =   2220
            Width           =   1155
         End
         Begin VB.CheckBox chkView 
            Caption         =   "Resource"
            Height          =   255
            Index           =   0
            Left            =   1440
            TabIndex        =   102
            Top             =   1440
            Width           =   1155
         End
         Begin VB.TextBox txtNewTechnicalLog 
            BorderStyle     =   0  'None
            Height          =   855
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   101
            Top             =   3600
            Width           =   3975
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo SSOleDBCombo1 
            Height          =   255
            Left            =   1440
            TabIndex        =   100
            Top             =   1800
            Width           =   2715
            BevelType       =   0
            _Version        =   196617
            DataMode        =   2
            BorderStyle     =   0
            RowHeight       =   423
            Columns(0).Width=   3200
            _ExtentX        =   4789
            _ExtentY        =   450
            _StockProps     =   93
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            Enabled         =   0   'False
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbLogEntryType 
            Height          =   255
            Left            =   1500
            TabIndex        =   113
            ToolTipText     =   "Our contact on this job"
            Top             =   660
            Width           =   2715
            BevelWidth      =   0
            DataFieldList   =   "Column 0"
            BevelType       =   0
            _Version        =   196617
            DataMode        =   2
            BorderStyle     =   0
            ColumnHeaders   =   0   'False
            BeveColorScheme =   1
            BevelColorFrame =   -2147483644
            CheckBox3D      =   0   'False
            BackColorOdd    =   16761024
            RowHeight       =   423
            Columns(0).Width=   4868
            Columns(0).Caption=   "Description"
            Columns(0).Name =   "description"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   4789
            _ExtentY        =   450
            _StockProps     =   93
            BackColor       =   16761024
            DataFieldToDisplay=   "Column 0"
         End
         Begin VB.Label lblCaption 
            Caption         =   "Please select:"
            Height          =   255
            Index           =   51
            Left            =   180
            TabIndex        =   110
            Top             =   660
            Width           =   1095
         End
         Begin VB.Label lblCaption 
            Caption         =   "Please select a resource:"
            Enabled         =   0   'False
            Height          =   435
            Index           =   48
            Left            =   180
            TabIndex        =   108
            Top             =   1740
            Width           =   1095
         End
         Begin VB.Label lblCaption 
            Caption         =   "Please enter a new descrption for this log entry"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   36
            Left            =   120
            TabIndex        =   107
            Top             =   3300
            Width           =   3915
         End
         Begin VB.Label lblCaption 
            Caption         =   "To what does this new log relate? (Tick all that apply)"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   15
            Left            =   120
            TabIndex        =   105
            Top             =   1020
            Width           =   3975
         End
         Begin VB.Label lblCaption 
            Caption         =   "What kind of log entry is this?"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   50
            Left            =   120
            TabIndex        =   109
            Top             =   300
            Width           =   3855
         End
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid SSOleDBGrid1 
         Height          =   4935
         Left            =   120
         TabIndex        =   203
         Top             =   480
         Visible         =   0   'False
         Width           =   2715
         _Version        =   196617
         BackColorOdd    =   16744576
         RowHeight       =   423
         Columns(0).Width=   3200
         _ExtentX        =   4789
         _ExtentY        =   8705
         _StockProps     =   79
         Caption         =   "Technical Log Details"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnUnit 
         Height          =   1755
         Left            =   -62820
         TabIndex        =   200
         Top             =   900
         Width           =   1695
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   3200
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   2990
         _ExtentY        =   3096
         _StockProps     =   77
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.CommandButton cmdPasteDubbingClocks 
         Caption         =   "Insert Clocks"
         Height          =   810
         Left            =   -74880
         MaskColor       =   &H00FFFFFF&
         Picture         =   "frmJob.frx":6C10
         Style           =   1  'Graphical
         TabIndex        =   193
         ToolTipText     =   "9 to 5 day"
         Top             =   1020
         UseMaskColor    =   -1  'True
         Width           =   630
      End
      Begin VB.CommandButton cmdAddMaster 
         Caption         =   "+ Master"
         Height          =   315
         Left            =   -70800
         TabIndex        =   191
         ToolTipText     =   "Add Masters"
         Top             =   420
         Width           =   795
      End
      Begin VB.CommandButton cmdResetInvoiceNumber 
         Caption         =   "!!"
         Height          =   315
         Left            =   -63840
         TabIndex        =   190
         ToolTipText     =   "Change the invoice number on this job"
         Top             =   480
         Width           =   315
      End
      Begin VB.CommandButton cmdCopyToCollection 
         Caption         =   "Copy to collection >>"
         Height          =   315
         Left            =   -69360
         TabIndex        =   189
         Top             =   4980
         Width           =   1755
      End
      Begin MSAdodcLib.Adodc adoDelAddress 
         Height          =   330
         Left            =   -62160
         Top             =   900
         Visible         =   0   'False
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "Adodc1"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.CheckBox chkUseCompany 
         Caption         =   "Use the company address book, rather than delivery addresses"
         Height          =   195
         Left            =   -70020
         TabIndex        =   184
         Top             =   420
         Width           =   4875
      End
      Begin VB.Frame Frame3 
         Caption         =   "Collection"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4275
         Left            =   -67380
         TabIndex        =   171
         Top             =   660
         Width           =   4755
         Begin VB.CommandButton cmdLaunch 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   8
            Left            =   2520
            MaskColor       =   &H00FFFFFF&
            Picture         =   "frmJob.frx":7054
            Style           =   1  'Graphical
            TabIndex        =   222
            ToolTipText     =   "Show on-line map (using multimap)"
            Top             =   3000
            UseMaskColor    =   -1  'True
            Width           =   300
         End
         Begin VB.TextBox txtCollectionDNoteNumber 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFC0&
            BorderStyle     =   0  'None
            Height          =   315
            Left            =   3300
            Locked          =   -1  'True
            MaxLength       =   50
            TabIndex        =   197
            Top             =   3840
            Width           =   1275
         End
         Begin VB.ComboBox cmbIncomingMethod 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1260
            TabIndex        =   187
            ToolTipText     =   "Transportation Method"
            Top             =   360
            Width           =   3315
         End
         Begin VB.TextBox txtCollectionCountry 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   315
            Left            =   3600
            MaxLength       =   50
            TabIndex        =   175
            ToolTipText     =   "Country of Despatch"
            Top             =   3000
            Width           =   975
         End
         Begin VB.TextBox txtCollectionPostCode 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   315
            Left            =   1260
            MaxLength       =   10
            TabIndex        =   174
            ToolTipText     =   "Postcode of Despatch"
            Top             =   3000
            Width           =   1155
         End
         Begin VB.TextBox txtCollectionAddress 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   975
            Left            =   1260
            MaxLength       =   255
            MultiLine       =   -1  'True
            TabIndex        =   173
            ToolTipText     =   "Address of Despatch"
            Top             =   1920
            Width           =   3315
         End
         Begin VB.TextBox txtCollectionTelephone 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   315
            Left            =   1260
            MaxLength       =   255
            MultiLine       =   -1  'True
            TabIndex        =   172
            ToolTipText     =   "Telephone number of Despatch"
            Top             =   3420
            Width           =   3315
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCollectionCompany 
            Bindings        =   "frmJob.frx":7466
            Height          =   315
            Left            =   1260
            TabIndex        =   176
            ToolTipText     =   "Company Name for Despatch"
            Top             =   1080
            Width           =   3315
            DataFieldList   =   "name"
            ListAutoValidate=   0   'False
            BevelType       =   0
            _Version        =   196617
            BorderStyle     =   0
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   7
            Columns(0).Width=   5609
            Columns(0).Caption=   "Company Name"
            Columns(0).Name =   "name"
            Columns(0).DataField=   "name"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "address"
            Columns(1).Name =   "address"
            Columns(1).DataField=   "address"
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Caption=   "postcode"
            Columns(2).Name =   "postcode"
            Columns(2).DataField=   "postcode"
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Caption=   "country"
            Columns(3).Name =   "country"
            Columns(3).DataField=   "country"
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Caption=   "attentionof"
            Columns(4).Name =   "attentionof"
            Columns(4).DataField=   "attentionof"
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Caption=   "telephone"
            Columns(5).Name =   "telephone"
            Columns(5).DataField=   "telephone"
            Columns(5).FieldLen=   256
            Columns(6).Width=   3201
            Columns(6).Caption=   "theID"
            Columns(6).Name =   "theID"
            Columns(6).DataField=   "theID"
            Columns(6).FieldLen=   256
            _ExtentX        =   5847
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "name"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCollectionContact 
            Bindings        =   "frmJob.frx":7482
            Height          =   315
            Left            =   1260
            TabIndex        =   177
            ToolTipText     =   "For the Attention of?"
            Top             =   1500
            Width           =   3315
            DataFieldList   =   "name"
            BevelType       =   0
            _Version        =   196617
            BorderStyle     =   0
            ColumnHeaders   =   0   'False
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   5609
            Columns(0).Caption=   "Company Name"
            Columns(0).Name =   "name"
            Columns(0).DataField=   "name"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "companyID"
            Columns(1).Name =   "companyID"
            Columns(1).DataField=   "companyID"
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "contactID"
            Columns(2).Name =   "contactID"
            Columns(2).DataField=   "contactID"
            Columns(2).FieldLen=   256
            _ExtentX        =   5847
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "name"
         End
         Begin VB.Label lblCaption 
            Caption         =   "DNote #"
            Height          =   315
            Index           =   87
            Left            =   2580
            TabIndex        =   196
            Top             =   3900
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Method"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   25
            Left            =   180
            TabIndex        =   188
            Top             =   360
            Width           =   915
         End
         Begin VB.Label lblCaption 
            Caption         =   "Country"
            Height          =   315
            Index           =   83
            Left            =   2880
            TabIndex        =   183
            Top             =   3000
            Width           =   615
         End
         Begin VB.Label lblCaption 
            Caption         =   "Post Code"
            Height          =   315
            Index           =   82
            Left            =   180
            TabIndex        =   182
            Top             =   3000
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Address"
            Height          =   315
            Index           =   81
            Left            =   180
            TabIndex        =   181
            Top             =   1920
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Send To"
            Height          =   315
            Index           =   80
            Left            =   180
            TabIndex        =   180
            Top             =   1080
            Width           =   1155
         End
         Begin VB.Label lblCaption 
            Caption         =   "Contact"
            Height          =   315
            Index           =   79
            Left            =   180
            TabIndex        =   179
            Top             =   1500
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Telephone"
            Height          =   315
            Index           =   78
            Left            =   180
            TabIndex        =   178
            Top             =   3420
            Width           =   1035
         End
      End
      Begin VB.Frame fraDeliveryAddress 
         Caption         =   "Delivery"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4275
         Left            =   -72360
         TabIndex        =   158
         Top             =   660
         Width           =   4755
         Begin VB.CommandButton cmdLaunch 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   7
            Left            =   2520
            MaskColor       =   &H00FFFFFF&
            Picture         =   "frmJob.frx":749E
            Style           =   1  'Graphical
            TabIndex        =   221
            ToolTipText     =   "Show on-line map (using multimap)"
            Top             =   3000
            UseMaskColor    =   -1  'True
            Width           =   300
         End
         Begin VB.TextBox txtDeliveryDNoteNumber 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFC0&
            BorderStyle     =   0  'None
            Height          =   315
            Left            =   3300
            Locked          =   -1  'True
            MaxLength       =   50
            TabIndex        =   195
            Top             =   3840
            Width           =   1275
         End
         Begin VB.ComboBox cmbOutgoingMethod 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1200
            TabIndex        =   185
            ToolTipText     =   "Transportation Method"
            Top             =   360
            Width           =   3375
         End
         Begin VB.TextBox txtDeliveryTelephone 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   315
            Left            =   1260
            MaxLength       =   255
            MultiLine       =   -1  'True
            TabIndex        =   162
            ToolTipText     =   "Telephone number of Despatch"
            Top             =   3420
            Width           =   3315
         End
         Begin VB.TextBox txtDeliveryAddress 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   975
            Left            =   1260
            MaxLength       =   255
            MultiLine       =   -1  'True
            TabIndex        =   161
            ToolTipText     =   "Address of Despatch"
            Top             =   1920
            Width           =   3315
         End
         Begin VB.TextBox txtDeliveryPostCode 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   315
            Left            =   1260
            MaxLength       =   10
            TabIndex        =   160
            ToolTipText     =   "Postcode of Despatch"
            Top             =   3000
            Width           =   1155
         End
         Begin VB.TextBox txtDeliveryCountry 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   315
            Left            =   3600
            MaxLength       =   50
            TabIndex        =   159
            ToolTipText     =   "Country of Despatch"
            Top             =   3000
            Width           =   975
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbDeliveryCompany 
            Bindings        =   "frmJob.frx":78B0
            Height          =   315
            Left            =   1260
            TabIndex        =   163
            ToolTipText     =   "Company Name for Despatch"
            Top             =   1080
            Width           =   3315
            DataFieldList   =   "name"
            ListAutoValidate=   0   'False
            BevelType       =   0
            _Version        =   196617
            BorderStyle     =   0
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   7
            Columns(0).Width=   5609
            Columns(0).Caption=   "Company Name"
            Columns(0).Name =   "name"
            Columns(0).DataField=   "name"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "address"
            Columns(1).Name =   "address"
            Columns(1).DataField=   "address"
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Caption=   "postcode"
            Columns(2).Name =   "postcode"
            Columns(2).DataField=   "postcode"
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Caption=   "country"
            Columns(3).Name =   "country"
            Columns(3).DataField=   "country"
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Caption=   "attentionof"
            Columns(4).Name =   "attentionof"
            Columns(4).DataField=   "attentionof"
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Caption=   "telephone"
            Columns(5).Name =   "telephone"
            Columns(5).DataField=   "telephone"
            Columns(5).FieldLen=   256
            Columns(6).Width=   3201
            Columns(6).Caption=   "theID"
            Columns(6).Name =   "theID"
            Columns(6).DataField=   "theID"
            Columns(6).FieldLen=   256
            _ExtentX        =   5847
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "name"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbDeliveryContact 
            Bindings        =   "frmJob.frx":78CC
            Height          =   315
            Left            =   1260
            TabIndex        =   164
            ToolTipText     =   "For the Attention of?"
            Top             =   1500
            Width           =   3315
            DataFieldList   =   "name"
            BevelType       =   0
            _Version        =   196617
            BorderStyle     =   0
            ColumnHeaders   =   0   'False
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   5609
            Columns(0).Caption=   "Company Name"
            Columns(0).Name =   "name"
            Columns(0).DataField=   "name"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "companyID"
            Columns(1).Name =   "companyID"
            Columns(1).DataField=   "companyID"
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "contactID"
            Columns(2).Name =   "contactID"
            Columns(2).DataField=   "contactID"
            Columns(2).FieldLen=   256
            _ExtentX        =   5847
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "name"
         End
         Begin VB.Label lblCaption 
            Caption         =   "DNote #"
            Height          =   315
            Index           =   86
            Left            =   2580
            TabIndex        =   194
            Top             =   3900
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Method"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   24
            Left            =   120
            TabIndex        =   186
            Top             =   360
            Width           =   975
         End
         Begin VB.Label lblCaption 
            Caption         =   "Telephone"
            Height          =   315
            Index           =   77
            Left            =   120
            TabIndex        =   170
            Top             =   3420
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Contact"
            Height          =   315
            Index           =   76
            Left            =   120
            TabIndex        =   169
            Top             =   1500
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Send To"
            Height          =   315
            Index           =   75
            Left            =   120
            TabIndex        =   168
            Top             =   1080
            Width           =   1155
         End
         Begin VB.Label lblCaption 
            Caption         =   "Address"
            Height          =   315
            Index           =   74
            Left            =   120
            TabIndex        =   167
            Top             =   1920
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Post Code"
            Height          =   315
            Index           =   28
            Left            =   120
            TabIndex        =   166
            Top             =   3000
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Country"
            Height          =   315
            Index           =   26
            Left            =   2880
            TabIndex        =   165
            Top             =   3000
            Width           =   615
         End
      End
      Begin VB.CommandButton cmdPreviewInvoice 
         Caption         =   "Preview >>"
         Height          =   315
         Left            =   -70440
         TabIndex        =   153
         ToolTipText     =   "Print this job"
         Top             =   5220
         Width           =   1275
      End
      Begin VB.TextBox txtInsertInvoiceNotes 
         Appearance      =   0  'Flat
         Height          =   855
         Left            =   -74160
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   150
         Top             =   2040
         Width           =   4995
      End
      Begin VB.CommandButton cmdInsertInvoiceNotes 
         Caption         =   "vv Insert"
         Height          =   255
         Left            =   -70260
         TabIndex        =   149
         Top             =   3000
         Width           =   1035
      End
      Begin VB.TextBox txtBarcode 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   -62460
         TabIndex        =   147
         ToolTipText     =   "Scan a tape barcode to link it to this job number"
         Top             =   480
         Width           =   1695
      End
      Begin VB.TextBox txtInvoiceNotes 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         Height          =   1755
         IMEMode         =   3  'DISABLE
         Left            =   -74160
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   145
         Top             =   3360
         Width           =   4995
      End
      Begin VB.TextBox txtInvoiceText 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         Height          =   1335
         IMEMode         =   3  'DISABLE
         Left            =   -74160
         MultiLine       =   -1  'True
         PasswordChar    =   "*"
         ScrollBars      =   2  'Vertical
         TabIndex        =   143
         Top             =   480
         Width           =   4995
      End
      Begin VB.CommandButton cmdApprove 
         Caption         =   "Approve"
         Height          =   315
         Left            =   -62040
         TabIndex        =   139
         ToolTipText     =   "Clear the form"
         Top             =   480
         Width           =   1335
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnDigital 
         Height          =   1755
         Left            =   -73080
         TabIndex        =   138
         Top             =   2520
         Width           =   6915
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         DefColWidth     =   8819
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   9710
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Column 0"
         Columns(0).FieldLen=   256
         Columns(1).Width=   8819
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "format"
         Columns(1).Name =   "format"
         Columns(1).DataField=   "Column 1"
         Columns(1).FieldLen=   256
         _ExtentX        =   12197
         _ExtentY        =   3096
         _StockProps     =   77
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdSundryCosts 
         Bindings        =   "frmJob.frx":78E8
         Height          =   4455
         Left            =   -74880
         TabIndex        =   137
         Top             =   900
         Width           =   14175
         _Version        =   196617
         stylesets.count =   2
         stylesets(0).Name=   "authorised"
         stylesets(0).BackColor=   8454016
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmJob.frx":7900
         stylesets(1).Name=   "approved"
         stylesets(1).BackColor=   9549311
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmJob.frx":791C
         AllowUpdate     =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   212
         Columns.Count   =   27
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "sundrycostID"
         Columns(0).Name =   "sundrycostID"
         Columns(0).Alignment=   1
         Columns(0).CaptionAlignment=   1
         Columns(0).DataField=   "sundrycostID"
         Columns(0).DataType=   20
         Columns(0).FieldLen=   256
         Columns(1).Width=   1746
         Columns(1).Caption=   "Job ID"
         Columns(1).Name =   "jobID"
         Columns(1).Alignment=   1
         Columns(1).CaptionAlignment=   1
         Columns(1).DataField=   "jobID"
         Columns(1).DataType=   20
         Columns(1).FieldLen=   256
         Columns(2).Width=   1720
         Columns(2).Caption=   "Project #"
         Columns(2).Name =   "projectnumber"
         Columns(2).Alignment=   1
         Columns(2).CaptionAlignment=   1
         Columns(2).DataField=   "projectnumber"
         Columns(2).DataType=   20
         Columns(2).FieldLen=   256
         Columns(3).Width=   1244
         Columns(3).Caption=   "Type"
         Columns(3).Name =   "sundrytype"
         Columns(3).CaptionAlignment=   0
         Columns(3).DataField=   "sundrytype"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3175
         Columns(4).Caption=   "Date Ordered"
         Columns(4).Name =   "dateordered"
         Columns(4).Alignment=   1
         Columns(4).CaptionAlignment=   1
         Columns(4).DataField=   "dateordered"
         Columns(4).DataType=   135
         Columns(4).FieldLen=   256
         Columns(5).Width=   1720
         Columns(5).Caption=   "Amount"
         Columns(5).Name =   "amount"
         Columns(5).Alignment=   1
         Columns(5).CaptionAlignment=   1
         Columns(5).DataField=   "amount"
         Columns(5).DataType=   6
         Columns(5).NumberFormat=   "�0.00"
         Columns(5).FieldLen=   256
         Columns(6).Width=   2196
         Columns(6).Caption=   "Entered By"
         Columns(6).Name =   "enteredby"
         Columns(6).CaptionAlignment=   0
         Columns(6).DataField=   "enteredby"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Caption=   "Date Required"
         Columns(7).Name =   "daterequired"
         Columns(7).Alignment=   1
         Columns(7).CaptionAlignment=   1
         Columns(7).DataField=   "daterequired"
         Columns(7).DataType=   135
         Columns(7).FieldLen=   256
         Columns(8).Width=   2090
         Columns(8).Caption=   "Requested By"
         Columns(8).Name =   "requestedby"
         Columns(8).CaptionAlignment=   0
         Columns(8).DataField=   "requestedby"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   2963
         Columns(9).Caption=   "Comments"
         Columns(9).Name =   "comments"
         Columns(9).CaptionAlignment=   0
         Columns(9).DataField=   "comments"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   2143
         Columns(10).Caption=   "Passengers"
         Columns(10).Name=   "passengers"
         Columns(10).CaptionAlignment=   0
         Columns(10).DataField=   "passengers"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(11).Width=   2461
         Columns(11).Caption=   "Location"
         Columns(11).Name=   "location"
         Columns(11).CaptionAlignment=   0
         Columns(11).DataField=   "location"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(12).Width=   2566
         Columns(12).Caption=   "Pickup Time"
         Columns(12).Name=   "pickuptime"
         Columns(12).CaptionAlignment=   0
         Columns(12).DataField=   "pickuptime"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(13).Width=   2381
         Columns(13).Caption=   "A/C Number"
         Columns(13).Name=   "accountnumber"
         Columns(13).CaptionAlignment=   0
         Columns(13).DataField=   "accountnumber"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(14).Width=   2170
         Columns(14).Caption=   "Supplier"
         Columns(14).Name=   "supplier"
         Columns(14).CaptionAlignment=   0
         Columns(14).DataField=   "supplier"
         Columns(14).DataType=   8
         Columns(14).FieldLen=   256
         Columns(15).Width=   3200
         Columns(15).Caption=   "Details"
         Columns(15).Name=   "details"
         Columns(15).CaptionAlignment=   0
         Columns(15).DataField=   "details"
         Columns(15).DataType=   8
         Columns(15).FieldLen=   256
         Columns(16).Width=   3200
         Columns(16).Caption=   "Auth Date"
         Columns(16).Name=   "authoriseddate"
         Columns(16).Alignment=   1
         Columns(16).CaptionAlignment=   1
         Columns(16).DataField=   "authoriseddate"
         Columns(16).DataType=   135
         Columns(16).FieldLen=   256
         Columns(17).Width=   3200
         Columns(17).Caption=   "Auth By"
         Columns(17).Name=   "authoriseduser"
         Columns(17).CaptionAlignment=   0
         Columns(17).DataField=   "authoriseduser"
         Columns(17).DataType=   8
         Columns(17).FieldLen=   256
         Columns(18).Width=   3200
         Columns(18).Caption=   "Approved Date"
         Columns(18).Name=   "approveddate"
         Columns(18).Alignment=   1
         Columns(18).CaptionAlignment=   1
         Columns(18).DataField=   "approveddate"
         Columns(18).DataType=   135
         Columns(18).FieldLen=   256
         Columns(19).Width=   3200
         Columns(19).Caption=   "Approved By"
         Columns(19).Name=   "approveduser"
         Columns(19).CaptionAlignment=   0
         Columns(19).DataField=   "approveduser"
         Columns(19).DataType=   8
         Columns(19).FieldLen=   256
         Columns(20).Width=   3200
         Columns(20).Caption=   "Cost Code"
         Columns(20).Name=   "costcode"
         Columns(20).CaptionAlignment=   0
         Columns(20).DataField=   "costcode"
         Columns(20).DataType=   8
         Columns(20).FieldLen=   256
         Columns(21).Width=   3200
         Columns(21).Visible=   0   'False
         Columns(21).Caption=   "cuser"
         Columns(21).Name=   "cuser"
         Columns(21).CaptionAlignment=   0
         Columns(21).DataField=   "cuser"
         Columns(21).DataType=   8
         Columns(21).FieldLen=   256
         Columns(22).Width=   3200
         Columns(22).Visible=   0   'False
         Columns(22).Caption=   "cdate"
         Columns(22).Name=   "cdate"
         Columns(22).Alignment=   1
         Columns(22).CaptionAlignment=   1
         Columns(22).DataField=   "cdate"
         Columns(22).DataType=   135
         Columns(22).FieldLen=   256
         Columns(23).Width=   3200
         Columns(23).Visible=   0   'False
         Columns(23).Caption=   "muser"
         Columns(23).Name=   "muser"
         Columns(23).CaptionAlignment=   0
         Columns(23).DataField=   "muser"
         Columns(23).DataType=   8
         Columns(23).FieldLen=   256
         Columns(24).Width=   3200
         Columns(24).Visible=   0   'False
         Columns(24).Caption=   "mdate"
         Columns(24).Name=   "mdate"
         Columns(24).Alignment=   1
         Columns(24).CaptionAlignment=   1
         Columns(24).DataField=   "mdate"
         Columns(24).DataType=   135
         Columns(24).FieldLen=   256
         Columns(25).Width=   2461
         Columns(25).Caption=   "Overhead Code"
         Columns(25).Name=   "overheadcode"
         Columns(25).DataField=   "overheadcode"
         Columns(25).FieldLen=   256
         Columns(26).Width=   1482
         Columns(26).Caption=   "Personal"
         Columns(26).Name=   "personal"
         Columns(26).DataField=   "personal"
         Columns(26).DataType=   2
         Columns(26).FieldLen=   256
         Columns(26).Style=   2
         _ExtentX        =   25003
         _ExtentY        =   7858
         _StockProps     =   79
         Caption         =   "Sundry Costs"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdAuthoriseSundry 
         Caption         =   "Authorise"
         Height          =   315
         Left            =   -63480
         TabIndex        =   136
         ToolTipText     =   "Clear the form"
         Top             =   480
         Width           =   1335
      End
      Begin VB.CommandButton cmdNewSundry 
         Caption         =   "New"
         Height          =   315
         Left            =   -74880
         TabIndex        =   135
         ToolTipText     =   "Clear the form"
         Top             =   480
         Width           =   1155
      End
      Begin VB.TextBox txtVTStartedBy 
         BackColor       =   &H8000000F&
         ForeColor       =   &H80000012&
         Height          =   315
         Left            =   -64440
         Locked          =   -1  'True
         TabIndex        =   124
         ToolTipText     =   "The job specific fax number"
         Top             =   420
         Width           =   2775
      End
      Begin VB.TextBox txtMasterJobID 
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   4380
         TabIndex        =   120
         ToolTipText     =   "The unique identifier for this job. In the previous version of CETA, this was known as the 'Booking Number'."
         Top             =   5520
         Visible         =   0   'False
         Width           =   975
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnPlayouts 
         Height          =   1755
         Left            =   -73740
         TabIndex        =   117
         Top             =   2340
         Width           =   6915
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         DefColWidth     =   8819
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   9710
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Column 0"
         Columns(0).FieldLen=   256
         Columns(1).Width=   8819
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "format"
         Columns(1).Name =   "format"
         Columns(1).DataField=   "Column 1"
         Columns(1).FieldLen=   256
         _ExtentX        =   12197
         _ExtentY        =   3096
         _StockProps     =   77
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnCopyList 
         Height          =   1755
         Left            =   -74460
         TabIndex        =   115
         Top             =   2220
         Width           =   6915
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         DefColWidth     =   8819
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   9710
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Column 0"
         Columns(0).FieldLen=   256
         Columns(1).Width=   8819
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "format"
         Columns(1).Name =   "format"
         Columns(1).DataField=   "Column 1"
         Columns(1).FieldLen=   256
         _ExtentX        =   12197
         _ExtentY        =   3096
         _StockProps     =   77
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.CommandButton cmdReplaceProducerNotes 
         Caption         =   "<< Replace"
         Height          =   255
         Left            =   -66000
         TabIndex        =   95
         Top             =   6240
         Width           =   1035
      End
      Begin VB.TextBox txtProducerNotes 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFC0FF&
         Height          =   1395
         Left            =   -73800
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   94
         Top             =   5940
         Width           =   7695
      End
      Begin VB.CommandButton cmdAddProducerNotes 
         Caption         =   "<< Insert"
         Height          =   255
         Left            =   -66000
         TabIndex        =   93
         Top             =   5940
         Width           =   1035
      End
      Begin VB.CommandButton cmdAddLibraryEvent 
         Caption         =   "Search Library"
         Height          =   315
         Index           =   1
         Left            =   -74880
         TabIndex        =   89
         Top             =   420
         Width           =   1515
      End
      Begin VB.CommandButton cmdAddLibraryEvent 
         Caption         =   "Search Library"
         Height          =   315
         Index           =   0
         Left            =   -69960
         TabIndex        =   88
         Top             =   420
         Width           =   1155
      End
      Begin VB.ComboBox cmbDeadlineTime 
         Height          =   315
         Left            =   -71940
         TabIndex        =   83
         ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
         Top             =   420
         Width           =   1035
      End
      Begin VB.PictureBox picScheduleFooter 
         BorderStyle     =   0  'None
         Height          =   1695
         Index           =   0
         Left            =   -74880
         ScaleHeight     =   1695
         ScaleWidth      =   14235
         TabIndex        =   80
         Top             =   3960
         Width           =   14235
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnTimeCode 
         Height          =   1755
         Left            =   -64440
         TabIndex        =   68
         Top             =   1560
         Width           =   1275
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   3200
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   2249
         _ExtentY        =   3096
         _StockProps     =   77
      End
      Begin VB.TextBox txtAccounts 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         Height          =   1755
         Left            =   -74880
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   67
         Top             =   3900
         Width           =   5175
      End
      Begin VB.CheckBox chkClearAfterAdd 
         Alignment       =   1  'Right Justify
         Caption         =   "Clear after insert"
         Height          =   195
         Left            =   -62220
         TabIndex        =   66
         Top             =   420
         Value           =   1  'Checked
         Width           =   1575
      End
      Begin VB.CommandButton cmdAddDespatchNotes 
         Caption         =   "<< Insert"
         Height          =   255
         Left            =   -66000
         TabIndex        =   65
         Top             =   3900
         Width           =   1035
      End
      Begin VB.CommandButton cmdAddOperatorNotes 
         Caption         =   "<< Insert"
         Height          =   255
         Left            =   -66000
         TabIndex        =   64
         Top             =   1740
         Width           =   1035
      End
      Begin VB.CommandButton cmdAddOffice 
         Caption         =   "<< Insert"
         Height          =   255
         Left            =   -66000
         TabIndex        =   63
         Top             =   420
         Width           =   1035
      End
      Begin VB.TextBox txtInsertNotes 
         Appearance      =   0  'Flat
         Height          =   3375
         Left            =   -64740
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   62
         Top             =   660
         Width           =   6975
      End
      Begin VB.TextBox txtDespatch 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         Height          =   1815
         Left            =   -73800
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   61
         Top             =   4020
         Width           =   7695
      End
      Begin VB.TextBox txtOfficeClient 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFC0C0&
         Height          =   1155
         Left            =   -73800
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   60
         Top             =   420
         Width           =   7695
      End
      Begin VB.TextBox txtOperator 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         Height          =   1875
         Left            =   -73800
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   59
         Top             =   1860
         Width           =   7695
      End
      Begin VB.CommandButton cmdReplaceOffice 
         Caption         =   "<< Replace"
         Height          =   255
         Left            =   -66000
         TabIndex        =   51
         Top             =   720
         Width           =   1035
      End
      Begin VB.CommandButton cmdReplaceOperator 
         Caption         =   "<< Replace"
         Height          =   255
         Left            =   -66000
         TabIndex        =   50
         Top             =   2040
         Width           =   1035
      End
      Begin VB.CommandButton cmdReplaceDespatch 
         Caption         =   "<< Replace"
         Height          =   255
         Left            =   -66000
         TabIndex        =   49
         Top             =   4200
         Width           =   1035
      End
      Begin VB.CommandButton cmdMoveDown 
         Caption         =   "�"
         BeginProperty Font 
            Name            =   "Wingdings"
            Size            =   8.25
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -68400
         TabIndex        =   48
         ToolTipText     =   "Move the selected row down"
         Top             =   420
         Width           =   315
      End
      Begin VB.CommandButton cmdMoveUp 
         Caption         =   "�"
         BeginProperty Font 
            Name            =   "Wingdings"
            Size            =   8.25
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -68760
         TabIndex        =   47
         ToolTipText     =   "Move the selected row up"
         Top             =   420
         Width           =   315
      End
      Begin VB.CommandButton cmdDuplicateRow 
         Caption         =   "Duplicate Row"
         Height          =   315
         Left            =   -68040
         TabIndex        =   46
         ToolTipText     =   "Move the selected row down"
         Top             =   420
         Width           =   1275
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnOthers 
         Height          =   1755
         Left            =   -58860
         TabIndex        =   52
         Top             =   1740
         Width           =   6675
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         DefColWidth     =   8819
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   9710
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Column 0"
         Columns(0).FieldLen=   256
         Columns(1).Width=   8819
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "format"
         Columns(1).Name =   "format"
         Columns(1).DataField=   "Column 1"
         Columns(1).FieldLen=   256
         _ExtentX        =   11774
         _ExtentY        =   3096
         _StockProps     =   77
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnRateCard 
         Bindings        =   "frmJob.frx":7938
         Height          =   1755
         Left            =   -74400
         TabIndex        =   53
         Top             =   1380
         Width           =   11055
         DataFieldList   =   "cetacode"
         ListAutoValidate=   0   'False
         _Version        =   196617
         BeveColorScheme =   1
         ForeColorOdd    =   14737632
         BackColorOdd    =   16744576
         RowHeight       =   423
         Columns.Count   =   9
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ratecardID"
         Columns(0).Name =   "ratecardID"
         Columns(0).Alignment=   1
         Columns(0).CaptionAlignment=   1
         Columns(0).DataField=   "ratecardID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   2831
         Columns(1).Caption=   "Code"
         Columns(1).Name =   "cetacode"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "cetacode"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3201
         Columns(2).Caption=   "category"
         Columns(2).Name =   "category"
         Columns(2).CaptionAlignment=   0
         Columns(2).DataField=   "category"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   2011
         Columns(3).Caption=   "Account Code"
         Columns(3).Name =   "nominalcode"
         Columns(3).CaptionAlignment=   0
         Columns(3).DataField=   "nominalcode"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   7726
         Columns(4).Caption=   "Description"
         Columns(4).Name =   "description"
         Columns(4).CaptionAlignment=   0
         Columns(4).DataField=   "description"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "minimumcharge"
         Columns(5).Name =   "minimumcharge"
         Columns(5).Alignment=   1
         Columns(5).CaptionAlignment=   1
         Columns(5).DataField=   "minimumcharge"
         Columns(5).DataType=   6
         Columns(5).NumberFormat=   "CURRENCY"
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "timeintervals"
         Columns(6).Name =   "timeintervals"
         Columns(6).Alignment=   1
         Columns(6).CaptionAlignment=   1
         Columns(6).DataField=   "timeintervals"
         Columns(6).DataType=   5
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "companyID"
         Columns(7).Name =   "companyID"
         Columns(7).Alignment=   1
         Columns(7).CaptionAlignment=   1
         Columns(7).DataField=   "companyID"
         Columns(7).DataType=   3
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Caption=   "Standard Rate (Price 0)"
         Columns(8).Name =   "rate0"
         Columns(8).Alignment=   1
         Columns(8).DataField=   "rate0"
         Columns(8).DataType=   6
         Columns(8).NumberFormat=   "�0.00"
         Columns(8).FieldLen=   256
         _ExtentX        =   19500
         _ExtentY        =   3096
         _StockProps     =   77
         DataFieldToDisplay=   "cetacode"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnStockType 
         Height          =   1755
         Left            =   -63000
         TabIndex        =   54
         Top             =   1560
         Width           =   1275
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   3200
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   2249
         _ExtentY        =   3096
         _StockProps     =   77
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnStandard 
         Height          =   1755
         Left            =   -69120
         TabIndex        =   55
         Top             =   1560
         Width           =   1035
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   3200
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1826
         _ExtentY        =   3096
         _StockProps     =   77
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnSoundChannel 
         Height          =   1755
         Left            =   -67920
         TabIndex        =   56
         Top             =   1560
         Width           =   1035
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   3200
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1826
         _ExtentY        =   3096
         _StockProps     =   77
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnRatio 
         Height          =   1755
         Left            =   -65820
         TabIndex        =   57
         Top             =   1560
         Width           =   1215
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   3200
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   2143
         _ExtentY        =   3096
         _StockProps     =   77
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnFormat 
         Height          =   1755
         Left            =   -70320
         TabIndex        =   58
         Top             =   1560
         Width           =   1035
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   3200
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1826
         _ExtentY        =   3096
         _StockProps     =   77
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdMedia 
         Bindings        =   "frmJob.frx":7952
         Height          =   1095
         Left            =   -74880
         TabIndex        =   69
         TabStop         =   0   'False
         ToolTipText     =   "Information about library media used on this job"
         Top             =   840
         Width           =   16515
         _Version        =   196617
         AllowUpdate     =   0   'False
         SelectTypeRow   =   1
         BackColorOdd    =   12648447
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   11
         Columns(0).Width=   3200
         Columns(0).Caption=   "Barcode"
         Columns(0).Name =   "barcode"
         Columns(0).DataField=   "barcode"
         Columns(0).FieldLen=   256
         Columns(1).Width=   4101
         Columns(1).Caption=   "Product"
         Columns(1).Name =   "productname"
         Columns(1).DataField=   "productname"
         Columns(1).FieldLen=   256
         Columns(2).Width=   5636
         Columns(2).Caption=   "Title"
         Columns(2).Name =   "title"
         Columns(2).DataField=   "title"
         Columns(2).FieldLen=   256
         Columns(3).Width=   4842
         Columns(3).Caption=   "subtitle"
         Columns(3).Name =   "subtitle"
         Columns(3).DataField=   "subtitle"
         Columns(3).FieldLen=   256
         Columns(4).Width=   979
         Columns(4).Caption=   "Series"
         Columns(4).Name =   "series"
         Columns(4).DataField=   "series"
         Columns(4).FieldLen=   256
         Columns(5).Width=   873
         Columns(5).Caption=   "Set"
         Columns(5).Name =   "seriesset"
         Columns(5).DataField=   "seriesset"
         Columns(5).FieldLen=   256
         Columns(6).Width=   873
         Columns(6).Caption=   "Ep"
         Columns(6).Name =   "episode"
         Columns(6).DataField=   "episode"
         Columns(6).FieldLen=   256
         Columns(7).Width=   2037
         Columns(7).Caption=   "Format"
         Columns(7).Name =   "format"
         Columns(7).DataField=   "format"
         Columns(7).FieldLen=   256
         Columns(8).Width=   2037
         Columns(8).Caption=   "Copy Type"
         Columns(8).Name =   "copytype"
         Columns(8).DataField=   "copytype"
         Columns(8).FieldLen=   256
         Columns(9).Width=   3360
         Columns(9).Caption=   "Created Date"
         Columns(9).Name =   "createddate"
         Columns(9).DataField=   "cdate"
         Columns(9).DataType=   7
         Columns(9).FieldLen=   256
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "libraryID"
         Columns(10).Name=   "libraryID"
         Columns(10).DataField=   "libraryID"
         Columns(10).FieldLen=   256
         _ExtentX        =   29131
         _ExtentY        =   1931
         _StockProps     =   79
         Caption         =   "Tapes Created by this job"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdCostingDetail 
         Bindings        =   "frmJob.frx":796C
         Height          =   2715
         Left            =   -74880
         TabIndex        =   70
         TabStop         =   0   'False
         Top             =   600
         Width           =   14235
         _Version        =   196617
         GroupHeaders    =   0   'False
         BeveColorScheme =   1
         AllowUpdate     =   0   'False
         AllowColumnSwapping=   0
         SelectTypeRow   =   3
         MaxSelectedRows =   0
         BackColorOdd    =   12632319
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   39
         Columns(0).Width=   953
         Columns(0).Caption=   "Order"
         Columns(0).Name =   "ratecardorderby"
         Columns(0).DataField=   "ratecardorderby"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3519
         Columns(1).Caption=   "Code"
         Columns(1).Name =   "costcode"
         Columns(1).DataField=   "costcode"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   2381
         Columns(2).Caption=   "A/C Code"
         Columns(2).Name =   "accountcode"
         Columns(2).DataField=   "accountcode"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   2381
         Columns(3).Caption=   "VDMS Code"
         Columns(3).Name =   "VDMS_Charge_Code"
         Columns(3).DataField=   "VDMS_Charge_Code"
         Columns(3).FieldLen=   256
         Columns(4).Width=   8811
         Columns(4).Caption=   "Description"
         Columns(4).Name =   "description"
         Columns(4).DataField=   "description"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   900
         Columns(5).Caption=   "Qty"
         Columns(5).Name =   "quantity"
         Columns(5).DataField=   "quantity"
         Columns(5).DataType=   5
         Columns(5).FieldLen=   256
         Columns(6).Width=   847
         Columns(6).Caption=   "Time"
         Columns(6).Name =   "fd_time"
         Columns(6).DataField=   "fd_time"
         Columns(6).DataType=   5
         Columns(6).FieldLen=   256
         Columns(7).Width=   1323
         Columns(7).Caption=   "Units"
         Columns(7).Name =   "unit"
         Columns(7).DataField=   "unit"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   1058
         Columns(8).Caption=   "Disc%"
         Columns(8).Name =   "discount"
         Columns(8).Alignment=   1
         Columns(8).DataField=   "discount"
         Columns(8).DataType=   4
         Columns(8).FieldLen=   256
         Columns(9).Width=   1693
         Columns(9).Caption=   "R. UnitCh"
         Columns(9).Name =   "unitcharge"
         Columns(9).Alignment=   1
         Columns(9).DataField=   "unitcharge"
         Columns(9).DataType=   5
         Columns(9).NumberFormat=   "�0.00#"
         Columns(9).FieldLen=   256
         Columns(10).Width=   1693
         Columns(10).Caption=   "Calc UnitCh"
         Columns(10).Name=   "effectiveunit"
         Columns(10).Alignment=   1
         Columns(10).DataField=   "Column 30"
         Columns(10).DataType=   5
         Columns(10).NumberFormat=   "�0.00#"
         Columns(10).FieldLen=   256
         Columns(11).Width=   1429
         Columns(11).Caption=   "Add. Disc"
         Columns(11).Name=   "additionaldiscount"
         Columns(11).Alignment=   1
         Columns(11).DataField=   "additionaldiscount"
         Columns(11).DataType=   5
         Columns(11).NumberFormat=   "�0.00#"
         Columns(11).FieldLen=   256
         Columns(12).Width=   1693
         Columns(12).Caption=   "Total"
         Columns(12).Name=   "total"
         Columns(12).Alignment=   1
         Columns(12).DataField=   "total"
         Columns(12).DataType=   5
         Columns(12).NumberFormat=   "�0.00#"
         Columns(12).FieldLen=   256
         Columns(12).Locked=   -1  'True
         Columns(13).Width=   3200
         Columns(13).Visible=   0   'False
         Columns(13).Caption=   "VAT"
         Columns(13).Name=   "vat"
         Columns(13).Alignment=   1
         Columns(13).DataField=   "vat"
         Columns(13).NumberFormat=   "�#.00"
         Columns(13).FieldLen=   256
         Columns(13).Locked=   -1  'True
         Columns(14).Width=   3200
         Columns(14).Visible=   0   'False
         Columns(14).Caption=   "Total Inc VAT"
         Columns(14).Name=   "totalincludingvat"
         Columns(14).Alignment=   1
         Columns(14).DataField=   "totalincludingvat"
         Columns(14).NumberFormat=   "�#.00"
         Columns(14).FieldLen=   256
         Columns(14).Locked=   -1  'True
         Columns(15).Width=   3200
         Columns(15).Visible=   0   'False
         Columns(15).Caption=   "R U Chge"
         Columns(15).Name=   "refundedunitcharge"
         Columns(15).Alignment=   1
         Columns(15).DataField=   "refundedunitcharge"
         Columns(15).NumberFormat=   "�#.00"
         Columns(15).FieldLen=   256
         Columns(15).Locked=   -1  'True
         Columns(16).Width=   3200
         Columns(16).Visible=   0   'False
         Columns(16).Caption=   "R Total"
         Columns(16).Name=   "refundedtotal"
         Columns(16).Alignment=   1
         Columns(16).DataField=   "refundedtotal"
         Columns(16).NumberFormat=   "�#.00"
         Columns(16).FieldLen=   256
         Columns(16).Locked=   -1  'True
         Columns(17).Width=   3200
         Columns(17).Visible=   0   'False
         Columns(17).Caption=   "R VAT"
         Columns(17).Name=   "refundedvat"
         Columns(17).Alignment=   1
         Columns(17).DataField=   "refundedvat"
         Columns(17).NumberFormat=   "�#.00"
         Columns(17).FieldLen=   256
         Columns(17).Locked=   -1  'True
         Columns(18).Width=   3200
         Columns(18).Visible=   0   'False
         Columns(18).Caption=   "R Tot Inc"
         Columns(18).Name=   "refundedtotalincludingvat"
         Columns(18).Alignment=   1
         Columns(18).DataField=   "refundedtotalincludingvat"
         Columns(18).NumberFormat=   "�#.00"
         Columns(18).FieldLen=   256
         Columns(18).Locked=   -1  'True
         Columns(19).Width=   3200
         Columns(19).Visible=   0   'False
         Columns(19).Caption=   "I U Chge"
         Columns(19).Name=   "inflatedunitcharge"
         Columns(19).Alignment=   1
         Columns(19).DataField=   "inflatedunitcharge"
         Columns(19).NumberFormat=   "�#.00"
         Columns(19).FieldLen=   256
         Columns(19).Locked=   -1  'True
         Columns(20).Width=   3200
         Columns(20).Visible=   0   'False
         Columns(20).Caption=   "I Total"
         Columns(20).Name=   "inflatedtotal"
         Columns(20).Alignment=   1
         Columns(20).DataField=   "inflatedtotal"
         Columns(20).NumberFormat=   "�#.00"
         Columns(20).FieldLen=   256
         Columns(20).Locked=   -1  'True
         Columns(21).Width=   3200
         Columns(21).Visible=   0   'False
         Columns(21).Caption=   "I VAT"
         Columns(21).Name=   "inflatedvat"
         Columns(21).Alignment=   1
         Columns(21).DataField=   "inflatedvat"
         Columns(21).NumberFormat=   "�#.00"
         Columns(21).FieldLen=   256
         Columns(21).Locked=   -1  'True
         Columns(22).Width=   3200
         Columns(22).Visible=   0   'False
         Columns(22).Caption=   "I Tot Inc"
         Columns(22).Name=   "inflatedtotalincludingvat"
         Columns(22).Alignment=   1
         Columns(22).DataField=   "inflatedtotalincludiingvat"
         Columns(22).NumberFormat=   "�#.00"
         Columns(22).FieldLen=   256
         Columns(22).Locked=   -1  'True
         Columns(23).Width=   3200
         Columns(23).Visible=   0   'False
         Columns(23).Caption=   "jobID"
         Columns(23).Name=   "jobID"
         Columns(23).DataField=   "jobID"
         Columns(23).FieldLen=   256
         Columns(24).Width=   3200
         Columns(24).Visible=   0   'False
         Columns(24).Caption=   "invoicenumber"
         Columns(24).Name=   "invoicenumber"
         Columns(24).DataField=   "invoicenumber"
         Columns(24).FieldLen=   256
         Columns(25).Width=   3200
         Columns(25).Visible=   0   'False
         Columns(25).Caption=   "AcntsTrID"
         Columns(25).Name=   "accountstransactionID"
         Columns(25).DataField=   "accountstransactionID"
         Columns(25).FieldLen=   256
         Columns(26).Width=   794
         Columns(26).Caption=   "Inc?"
         Columns(26).Name=   "includedindealprice"
         Columns(26).DataField=   "includedindealprice"
         Columns(26).FieldLen=   256
         Columns(26).Style=   4
         Columns(26).ButtonsAlways=   -1  'True
         Columns(27).Width=   1693
         Columns(27).Caption=   "Rate Card"
         Columns(27).Name=   "ratecardprice"
         Columns(27).Alignment=   1
         Columns(27).DataField=   "ratecardprice"
         Columns(27).DataType=   5
         Columns(27).NumberFormat=   "�0.00#"
         Columns(27).FieldLen=   256
         Columns(27).Locked=   -1  'True
         Columns(27).HasBackColor=   -1  'True
         Columns(27).BackColor=   -2147483633
         Columns(28).Width=   3200
         Columns(28).Visible=   0   'False
         Columns(28).Caption=   "costingID"
         Columns(28).Name=   "costingID"
         Columns(28).DataField=   "costingID"
         Columns(28).FieldLen=   256
         Columns(28).Locked=   -1  'True
         Columns(29).Width=   1693
         Columns(29).Caption=   "R/C Total"
         Columns(29).Name=   "ratecardtotal"
         Columns(29).Alignment=   1
         Columns(29).DataField=   "ratecardtotal"
         Columns(29).DataType=   5
         Columns(29).NumberFormat=   "�0.00#"
         Columns(29).FieldLen=   256
         Columns(29).HasBackColor=   -1  'True
         Columns(29).BackColor=   -2147483633
         Columns(30).Width=   2381
         Columns(30).Caption=   "Stock Code"
         Columns(30).Name=   "includedstocknominal"
         Columns(30).DataField=   "includedstocknominal"
         Columns(30).FieldLen=   256
         Columns(30).HasBackColor=   -1  'True
         Columns(30).BackColor=   -2147483633
         Columns(31).Width=   1693
         Columns(31).Caption=   "Stock Total"
         Columns(31).Name=   "includedstocktotal"
         Columns(31).DataField=   "includedstocktotal"
         Columns(31).FieldLen=   256
         Columns(31).HasBackColor=   -1  'True
         Columns(31).BackColor=   -2147483633
         Columns(32).Width=   3200
         Columns(32).Visible=   0   'False
         Columns(32).Caption=   "includedstockVAT"
         Columns(32).Name=   "includedstockVAT"
         Columns(32).DataField=   "includedstockvat"
         Columns(32).FieldLen=   256
         Columns(32).HasBackColor=   -1  'True
         Columns(32).BackColor=   -2147483633
         Columns(33).Width=   3200
         Columns(33).Visible=   0   'False
         Columns(33).Caption=   "includedstocktotalincvat"
         Columns(33).Name=   "includedstocktotalincvat"
         Columns(33).DataField=   "includedstocktotalincludingvat"
         Columns(33).FieldLen=   256
         Columns(33).HasBackColor=   -1  'True
         Columns(33).BackColor=   -2147483633
         Columns(34).Width=   3200
         Columns(34).Visible=   0   'False
         Columns(34).Caption=   "ratecardcategory"
         Columns(34).Name=   "ratecardcategory"
         Columns(34).DataField=   "ratecardcategory"
         Columns(34).FieldLen=   256
         Columns(35).Width=   3200
         Columns(35).Visible=   0   'False
         Columns(35).Caption=   "costingsheetID"
         Columns(35).Name=   "costingsheetID"
         Columns(35).DataField=   "costingsheetID"
         Columns(35).FieldLen=   256
         Columns(36).Width=   3200
         Columns(36).Visible=   0   'False
         Columns(36).Caption=   "minimumcharge"
         Columns(36).Name=   "minimumcharge"
         Columns(36).DataField=   "minimumcharge"
         Columns(36).FieldLen=   256
         Columns(37).Width=   3200
         Columns(37).Visible=   0   'False
         Columns(37).Caption=   "ratecardminimumcharge"
         Columns(37).Name=   "ratecardminimumcharge"
         Columns(37).DataField=   "ratecardminimumcharge"
         Columns(37).FieldLen=   256
         Columns(38).Width=   3200
         Columns(38).Visible=   0   'False
         Columns(38).Caption=   "Invoice Status"
         Columns(38).Name=   "accountstransactionstatus"
         Columns(38).FieldLen=   256
         UseDefaults     =   0   'False
         _ExtentX        =   25109
         _ExtentY        =   4789
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComCtl2.DTPicker datDeadlineDate 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Left            =   -73800
         TabIndex        =   84
         ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
         Top             =   420
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   182845441
         CurrentDate     =   37870
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdRequiredMedia 
         Bindings        =   "frmJob.frx":7985
         Height          =   1335
         Left            =   -74880
         TabIndex        =   98
         TabStop         =   0   'False
         ToolTipText     =   "Information about library media used on this job"
         Top             =   6180
         Width           =   14115
         _Version        =   196617
         AllowUpdate     =   0   'False
         SelectTypeRow   =   1
         BackColorOdd    =   8454143
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   12
         Columns(0).Width=   2646
         Columns(0).Caption=   "Barcode"
         Columns(0).Name =   "barcode"
         Columns(0).DataField=   "barcode"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "Product"
         Columns(1).Name =   "productname"
         Columns(1).DataField=   "productname"
         Columns(1).FieldLen=   256
         Columns(2).Width=   4419
         Columns(2).Caption=   "Title"
         Columns(2).Name =   "title"
         Columns(2).DataField=   "title"
         Columns(2).FieldLen=   256
         Columns(3).Width=   1244
         Columns(3).Caption=   "Episode"
         Columns(3).Name =   "Episode"
         Columns(3).DataField=   "Episode"
         Columns(3).FieldLen=   256
         Columns(4).Width=   3360
         Columns(4).Caption=   "Clock Number"
         Columns(4).Name =   "clocknumber"
         Columns(4).FieldLen=   256
         Columns(5).Width=   2037
         Columns(5).Caption=   "Format"
         Columns(5).Name =   "format"
         Columns(5).DataField=   "format"
         Columns(5).FieldLen=   256
         Columns(6).Width=   2328
         Columns(6).Caption=   "Type"
         Columns(6).Name =   "copytype"
         Columns(6).DataField=   "copytype"
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "libraryID"
         Columns(7).Name =   "libraryID"
         Columns(7).DataField=   "libraryID"
         Columns(7).FieldLen=   256
         Columns(8).Width=   2646
         Columns(8).Caption=   "Location"
         Columns(8).Name =   "location"
         Columns(8).DataField=   "location"
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Caption=   "Shelf"
         Columns(9).Name =   "shelf"
         Columns(9).DataField=   "shelf"
         Columns(9).FieldLen=   256
         Columns(10).Width=   3200
         Columns(10).Caption=   "Box"
         Columns(10).Name=   "box"
         Columns(10).DataField=   "box"
         Columns(10).FieldLen=   256
         Columns(11).Width=   2302
         Columns(11).Caption=   "requiredmediaid"
         Columns(11).Name=   "requiredmediaID"
         Columns(11).DataField=   "requiredmediaID"
         Columns(11).FieldLen=   256
         _ExtentX        =   24897
         _ExtentY        =   2355
         _StockProps     =   79
         Caption         =   "Tapes Required by this job"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin CrystalActiveXReportViewerLib10Ctl.CrystalActiveXReportViewer CRViewer91 
         Height          =   6135
         Left            =   -69060
         TabIndex        =   152
         Top             =   900
         Width           =   22335
         lastProp        =   600
         _cx             =   39396
         _cy             =   10821
         DisplayGroupTree=   0   'False
         DisplayToolbar  =   -1  'True
         EnableGroupTree =   0   'False
         EnableNavigationControls=   -1  'True
         EnableStopButton=   -1  'True
         EnablePrintButton=   -1  'True
         EnableZoomControl=   -1  'True
         EnableCloseButton=   0   'False
         EnableProgressControl=   -1  'True
         EnableSearchControl=   0   'False
         EnableRefreshButton=   -1  'True
         EnableDrillDown =   0   'False
         EnableAnimationControl=   0   'False
         EnableSelectExpertButton=   0   'False
         EnableToolbar   =   -1  'True
         DisplayBorder   =   -1  'True
         DisplayTabs     =   0   'False
         DisplayBackgroundEdge=   0   'False
         SelectionFormula=   ""
         EnablePopupMenu =   -1  'True
         EnableExportButton=   -1  'True
         EnableSearchExpertButton=   0   'False
         EnableHelpButton=   0   'False
         LaunchHTTPHyperlinksInNewBrowser=   -1  'True
         EnableLogonPrompts=   -1  'True
      End
      Begin MSComCtl2.DTPicker datInvoiceDate 
         Height          =   315
         Left            =   -67740
         TabIndex        =   154
         Tag             =   "NOCHECK"
         ToolTipText     =   "When checked you are only shown deliveries with their deadline on the specified date"
         Top             =   480
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   182845441
         CurrentDate     =   37870
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdTechReviewMedia 
         Bindings        =   "frmJob.frx":799F
         Height          =   1335
         Left            =   -74880
         TabIndex        =   226
         TabStop         =   0   'False
         ToolTipText     =   "Information about library media used on this job"
         Top             =   4740
         Width           =   19275
         _Version        =   196617
         AllowUpdate     =   0   'False
         SelectTypeRow   =   1
         BackColorOdd    =   12648447
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   11
         Columns(0).Width=   3200
         Columns(0).Caption=   "Barcode"
         Columns(0).Name =   "barcode"
         Columns(0).DataField=   "barcode"
         Columns(0).FieldLen=   256
         Columns(1).Width=   4101
         Columns(1).Caption=   "Product"
         Columns(1).Name =   "productname"
         Columns(1).DataField=   "productname"
         Columns(1).FieldLen=   256
         Columns(2).Width=   5636
         Columns(2).Caption=   "Title"
         Columns(2).Name =   "title"
         Columns(2).DataField=   "title"
         Columns(2).FieldLen=   256
         Columns(3).Width=   4842
         Columns(3).Caption=   "Subtitle"
         Columns(3).Name =   "subtitle"
         Columns(3).DataField=   "subtitle"
         Columns(3).FieldLen=   256
         Columns(4).Width=   979
         Columns(4).Caption=   "Series"
         Columns(4).Name =   "series"
         Columns(4).DataField=   "series"
         Columns(4).FieldLen=   256
         Columns(5).Width=   873
         Columns(5).Caption=   "Set"
         Columns(5).Name =   "seriesset"
         Columns(5).DataField=   "seriesset"
         Columns(5).FieldLen=   256
         Columns(6).Width=   873
         Columns(6).Caption=   "Ep"
         Columns(6).Name =   "episode"
         Columns(6).DataField=   "episode"
         Columns(6).FieldLen=   256
         Columns(7).Width=   2037
         Columns(7).Caption=   "Format"
         Columns(7).Name =   "format"
         Columns(7).DataField=   "format"
         Columns(7).FieldLen=   256
         Columns(8).Width=   2037
         Columns(8).Caption=   "Copy Type"
         Columns(8).Name =   "copytype"
         Columns(8).DataField=   "copytype"
         Columns(8).FieldLen=   256
         Columns(9).Width=   3360
         Columns(9).Caption=   "Created Date"
         Columns(9).Name =   "createddate"
         Columns(9).DataField=   "cdate"
         Columns(9).FieldLen=   256
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "libraryID"
         Columns(10).Name=   "libraryID"
         Columns(10).DataField=   "libraryID"
         Columns(10).FieldLen=   256
         _ExtentX        =   33999
         _ExtentY        =   2355
         _StockProps     =   79
         Caption         =   "Tech Reviewed Media for this job"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnInclusive 
         Bindings        =   "frmJob.frx":79B9
         Height          =   1755
         Left            =   -61920
         TabIndex        =   253
         Top             =   3720
         Width           =   7875
         DataFieldList   =   "description"
         ListAutoValidate=   0   'False
         MaxDropDownItems=   20
         _Version        =   196617
         DefColWidth     =   8819
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   2
         Columns(0).Width=   9710
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "description"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3519
         Columns(1).Caption=   "format"
         Columns(1).Name =   "format"
         Columns(1).DataField=   "format"
         Columns(1).FieldLen=   256
         _ExtentX        =   13891
         _ExtentY        =   3096
         _StockProps     =   77
         DataFieldToDisplay=   "description"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDetail 
         Bindings        =   "frmJob.frx":79D4
         Height          =   3375
         Left            =   -74940
         TabIndex        =   116
         TabStop         =   0   'False
         Top             =   840
         Width           =   28185
         _Version        =   196617
         stylesets.count =   2
         stylesets(0).Name=   "REJECTED"
         stylesets(0).BackColor=   8421631
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmJob.frx":79EF
         stylesets(1).Name=   "COMPLETED"
         stylesets(1).BackColor=   7011261
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmJob.frx":7A0B
         BeveColorScheme =   1
         AllowUpdate     =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeRow   =   3
         ForeColorEven   =   0
         BackColorOdd    =   16051436
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   47
         Columns(0).Width=   476
         Columns(0).Name =   "copytype"
         Columns(0).DataField=   "copytype"
         Columns(0).Case =   2
         Columns(0).FieldLen=   2
         Columns(1).Width=   6165
         Columns(1).Caption=   "Description"
         Columns(1).Name =   "description"
         Columns(1).DataField=   "description"
         Columns(1).FieldLen=   256
         Columns(2).Width=   5398
         Columns(2).Caption=   "Work to be Done"
         Columns(2).Name =   "workflowdescription"
         Columns(2).DataField=   "workflowdescription"
         Columns(2).FieldLen=   256
         Columns(3).Width=   714
         Columns(3).Caption=   "Qty"
         Columns(3).Name =   "quantity"
         Columns(3).DataField=   "quantity"
         Columns(3).FieldLen=   256
         Columns(4).Width=   2302
         Columns(4).Caption=   "Format"
         Columns(4).Name =   "format"
         Columns(4).DataField=   "format"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   2011
         Columns(5).Caption=   "Standard"
         Columns(5).Name =   "standard"
         Columns(5).DataField=   "videostandard"
         Columns(5).FieldLen=   256
         Columns(6).Width=   714
         Columns(6).Caption=   "HD?"
         Columns(6).Name =   "HD?"
         Columns(6).DataField=   "hdflag"
         Columns(6).FieldLen=   256
         Columns(6).Style=   2
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "MasterStore"
         Columns(7).Name =   "MasterStore"
         Columns(7).DataField=   "MasterStore"
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "CopyDeleteDate"
         Columns(8).Name =   "CopyDeleteDate"
         Columns(8).DataField=   "CopyDeleteDate"
         Columns(8).FieldLen=   256
         Columns(9).Width=   2117
         Columns(9).Caption=   "Snd Ch 1"
         Columns(9).Name =   "ch1"
         Columns(9).DataField=   "sound1"
         Columns(9).FieldLen=   256
         Columns(10).Width=   2117
         Columns(10).Caption=   "Snd Ch 2"
         Columns(10).Name=   "ch2"
         Columns(10).DataField=   "sound2"
         Columns(10).FieldLen=   256
         Columns(11).Width=   2117
         Columns(11).Caption=   "Snd Ch 3"
         Columns(11).Name=   "ch3"
         Columns(11).DataField=   "sound3"
         Columns(11).FieldLen=   256
         Columns(12).Width=   2117
         Columns(12).Caption=   "Snd Ch 4"
         Columns(12).Name=   "ch4"
         Columns(12).DataField=   "sound4"
         Columns(12).FieldLen=   256
         Columns(13).Width=   1614
         Columns(13).Caption=   "Ratio"
         Columns(13).Name=   "ratio"
         Columns(13).DataField=   "aspectratio"
         Columns(13).FieldLen=   256
         Columns(14).Width=   1508
         Columns(14).Caption=   "T/C"
         Columns(14).Name=   "timecode"
         Columns(14).DataField=   "timecode"
         Columns(14).FieldLen=   256
         Columns(15).Width=   979
         Columns(15).Caption=   "R/T"
         Columns(15).Name=   "runningtime"
         Columns(15).DataField=   "runningtime"
         Columns(15).FieldLen=   256
         Columns(16).Width=   1058
         Columns(16).Caption=   "Mins?"
         Columns(16).Name=   "chargemins"
         Columns(16).Alignment=   2
         Columns(16).DataField=   "chargerealminutes"
         Columns(16).FieldLen=   256
         Columns(16).Style=   2
         Columns(17).Width=   1508
         Columns(17).Caption=   "Stock 1"
         Columns(17).Name=   "stocktype1"
         Columns(17).DataField=   "stock1type"
         Columns(17).FieldLen=   256
         Columns(18).Width=   794
         Columns(18).Caption=   "Ours"
         Columns(18).Name=   "ourstock1"
         Columns(18).DataField=   "stock1ours"
         Columns(18).FieldLen=   256
         Columns(19).Width=   794
         Columns(19).Caption=   "Cl"
         Columns(19).Name=   "clientstock1"
         Columns(19).DataField=   "stock1clients"
         Columns(19).FieldLen=   256
         Columns(20).Width=   1508
         Columns(20).Caption=   "Stock 2"
         Columns(20).Name=   "stocktype2"
         Columns(20).DataField=   "stock2type"
         Columns(20).FieldLen=   256
         Columns(21).Width=   794
         Columns(21).Caption=   "Ours"
         Columns(21).Name=   "ourstock2"
         Columns(21).DataField=   "stock2ours"
         Columns(21).FieldLen=   256
         Columns(22).Width=   794
         Columns(22).Caption=   "Cl"
         Columns(22).Name=   "clientstock2"
         Columns(22).DataField=   "stock2clients"
         Columns(22).FieldLen=   256
         Columns(23).Width=   873
         Columns(23).Caption=   "Order"
         Columns(23).Name=   "fd_orderby"
         Columns(23).DataField=   "fd_orderby"
         Columns(23).FieldLen=   256
         Columns(23).HasBackColor=   -1  'True
         Columns(23).BackColor=   14671839
         Columns(24).Width=   3200
         Columns(24).Visible=   0   'False
         Columns(24).Caption=   "jobID"
         Columns(24).Name=   "jobID"
         Columns(24).DataField=   "jobID"
         Columns(24).FieldLen=   256
         Columns(25).Width=   1931
         Columns(25).Caption=   "jobdetailID"
         Columns(25).Name=   "jobdetailID"
         Columns(25).DataField=   "jobdetailID"
         Columns(25).FieldLen=   256
         Columns(25).Locked=   -1  'True
         Columns(26).Width=   3200
         Columns(26).Caption=   "WaybillNumber"
         Columns(26).Name=   "WaybillNumber"
         Columns(26).DataField=   "WaybillNumber"
         Columns(26).FieldLen=   256
         Columns(27).Width=   2461
         Columns(27).Caption=   "Completed Date"
         Columns(27).Name=   "completeddate"
         Columns(27).DataField=   "completeddate"
         Columns(27).FieldLen=   256
         Columns(27).Style=   1
         Columns(28).Width=   609
         Columns(28).Caption=   "By"
         Columns(28).Name=   "completeduser"
         Columns(28).DataField=   "completeduser"
         Columns(28).FieldLen=   256
         Columns(29).Width=   2461
         Columns(29).Caption=   "Pending"
         Columns(29).Name=   "rejecteddate"
         Columns(29).DataField=   "rejecteddate"
         Columns(29).FieldLen=   256
         Columns(29).Style=   1
         Columns(30).Width=   609
         Columns(30).Caption=   "By"
         Columns(30).Name=   "rejecteduser"
         Columns(30).DataField=   "rejecteduser"
         Columns(30).FieldLen=   256
         Columns(31).Width=   2646
         Columns(31).Caption=   "Clip ID"
         Columns(31).Name=   "clipID"
         Columns(31).DataField=   "clipID"
         Columns(31).FieldLen=   256
         Columns(31).Style=   1
         Columns(31).HasBackColor=   -1  'True
         Columns(31).BackColor=   14671839
         Columns(32).Width=   3200
         Columns(32).Caption=   "Email To"
         Columns(32).Name=   "emailto"
         Columns(32).DataField=   "emailto"
         Columns(32).FieldLen=   256
         Columns(32).Style=   1
         Columns(32).HasBackColor=   -1  'True
         Columns(32).BackColor=   13294580
         Columns(33).Width=   3200
         Columns(33).Visible=   0   'False
         Columns(33).Caption=   "Invoice"
         Columns(33).Name=   "Invoice"
         Columns(33).DataField=   "chargethisitem"
         Columns(33).FieldLen=   256
         Columns(33).Style=   2
         Columns(34).Width=   1349
         Columns(34).Caption=   "portaluserID"
         Columns(34).Name=   "portaluserID"
         Columns(34).DataField=   "portaluserID"
         Columns(34).FieldLen=   256
         Columns(35).Width=   1482
         Columns(35).Caption=   "skibblycustomerID"
         Columns(35).Name=   "skibblycustomerID"
         Columns(35).DataField=   "skibblycustomerID"
         Columns(35).FieldLen=   256
         Columns(36).Width=   3200
         Columns(36).Visible=   0   'False
         Columns(36).Caption=   "DADCPeojectNumber"
         Columns(36).Name=   "DADCProjectNumber"
         Columns(36).DataField=   "DADCProjectNumber"
         Columns(36).FieldLen=   256
         Columns(37).Width=   3200
         Columns(37).Visible=   0   'False
         Columns(37).Caption=   "DADCProjectManager"
         Columns(37).Name=   "DADCProjectManager"
         Columns(37).DataField=   "DADCProjectManager"
         Columns(37).FieldLen=   256
         Columns(38).Width=   3200
         Columns(38).Visible=   0   'False
         Columns(38).Caption=   "DADCSeries"
         Columns(38).Name=   "DADCSeries"
         Columns(38).DataField=   "DADCSeries"
         Columns(38).FieldLen=   256
         Columns(39).Width=   3200
         Columns(39).Visible=   0   'False
         Columns(39).Caption=   "DADCEpisodeNumber"
         Columns(39).Name=   "DADCEpisodeNumber"
         Columns(39).DataField=   "DADCEpisodeNumber"
         Columns(39).FieldLen=   256
         Columns(40).Width=   3200
         Columns(40).Visible=   0   'False
         Columns(40).Caption=   "DADCEpisodeTitle"
         Columns(40).Name=   "DADCEpisodeTitle"
         Columns(40).DataField=   "DADCEpisodeTitle"
         Columns(40).FieldLen=   256
         Columns(41).Width=   3200
         Columns(41).Visible=   0   'False
         Columns(41).Caption=   "DADCRightsOwner"
         Columns(41).Name=   "DADCRightsOwner"
         Columns(41).DataField=   "DADCRightsOwner"
         Columns(41).FieldLen=   256
         Columns(42).Width=   3200
         Columns(42).Visible=   0   'False
         Columns(42).Caption=   "DADCJobTItle"
         Columns(42).Name=   "DADCJobTItle"
         Columns(42).DataField=   "DADCJobTItle"
         Columns(42).FieldLen=   256
         Columns(43).Width=   3200
         Columns(43).Visible=   0   'False
         Columns(43).Caption=   "FirstRejectedDate"
         Columns(43).Name=   "FirstRejectedDate"
         Columns(43).DataField=   "FirstRejectedDate"
         Columns(43).FieldLen=   256
         Columns(44).Width=   3200
         Columns(44).Visible=   0   'False
         Columns(44).Caption=   "DaysOnRejected"
         Columns(44).Name=   "DaysOnRejected"
         Columns(44).DataField=   "DaysOnRejected"
         Columns(44).FieldLen=   256
         Columns(45).Width=   1058
         Columns(45).Caption=   "Faulty"
         Columns(45).Name=   "complainedabout"
         Columns(45).DataField=   "complainedabout"
         Columns(45).FieldLen=   256
         Columns(45).Style=   2
         Columns(46).Width=   3200
         Columns(46).Visible=   0   'False
         Columns(46).Caption=   "DADCLegacyOracTVAENumber"
         Columns(46).Name=   "DADCLegacyOracTVAENumber"
         Columns(46).DataField=   "DADCLegacyOracTVAENumber"
         Columns(46).FieldLen=   256
         _ExtentX        =   49715
         _ExtentY        =   5953
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDetailCopy 
         Bindings        =   "frmJob.frx":7A27
         Height          =   3375
         Left            =   -74880
         TabIndex        =   270
         TabStop         =   0   'False
         Top             =   480
         Width           =   11310
         _Version        =   196617
         stylesets.count =   1
         stylesets(0).Name=   "COMPLETED"
         stylesets(0).BackColor=   7011261
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmJob.frx":7A42
         BeveColorScheme =   1
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16051436
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   10
         Columns(0).Width=   370
         Columns(0).Name =   "copytype"
         Columns(0).DataField=   "copytype"
         Columns(0).Case =   2
         Columns(0).FieldLen=   1
         Columns(1).Width=   12594
         Columns(1).Caption=   "Description / Work to be done"
         Columns(1).Name =   "description"
         Columns(1).DataField=   "description"
         Columns(1).FieldLen=   256
         Columns(2).Width=   714
         Columns(2).Caption=   "Qty"
         Columns(2).Name =   "quantity"
         Columns(2).DataField=   "quantity"
         Columns(2).FieldLen=   256
         Columns(3).Width=   2646
         Columns(3).Caption=   "Format"
         Columns(3).Name =   "format"
         Columns(3).DataField=   "format"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   979
         Columns(4).Caption=   "R/T"
         Columns(4).Name =   "runningtime"
         Columns(4).DataField=   "runningtime"
         Columns(4).FieldLen=   256
         Columns(5).Width=   1482
         Columns(5).Caption=   "real mins?"
         Columns(5).Name =   "chargemins"
         Columns(5).DataField=   "chargerealminutes"
         Columns(5).FieldLen=   256
         Columns(5).Style=   2
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "jobID"
         Columns(6).Name =   "jobID"
         Columns(6).DataField=   "jobID"
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "jobdetailID"
         Columns(7).Name =   "jobdetailID"
         Columns(7).DataField=   "jobdetailID"
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "Completed Date"
         Columns(8).Name =   "completeddate"
         Columns(8).DataField=   "completeddate"
         Columns(8).DataType=   7
         Columns(8).FieldLen=   256
         Columns(8).Style=   1
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "By"
         Columns(9).Name =   "completeduser"
         Columns(9).DataField=   "completeduser"
         Columns(9).FieldLen=   256
         _ExtentX        =   19950
         _ExtentY        =   5953
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdUnbilled 
         Bindings        =   "frmJob.frx":7A5E
         Height          =   3375
         Left            =   -62685
         TabIndex        =   271
         TabStop         =   0   'False
         Top             =   480
         Width           =   14310
         _Version        =   196617
         stylesets.count =   1
         stylesets(0).Name=   "COMPLETED"
         stylesets(0).BackColor=   7011261
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmJob.frx":7A78
         BeveColorScheme =   1
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeRow   =   3
         ForeColorEven   =   0
         BackColorOdd    =   16051436
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   14
         Columns(0).Width=   370
         Columns(0).Name =   "copytype"
         Columns(0).DataField=   "copytype"
         Columns(0).Case =   2
         Columns(0).FieldLen=   1
         Columns(1).Width=   9181
         Columns(1).Caption=   "Description / Work to be done"
         Columns(1).Name =   "description"
         Columns(1).DataField=   "description"
         Columns(1).FieldLen=   256
         Columns(2).Width=   714
         Columns(2).Caption=   "Qty"
         Columns(2).Name =   "quantity"
         Columns(2).DataField=   "quantity"
         Columns(2).FieldLen=   256
         Columns(3).Width=   2646
         Columns(3).Caption=   "Format"
         Columns(3).Name =   "format"
         Columns(3).DataField=   "format"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   979
         Columns(4).Caption=   "R/T"
         Columns(4).Name =   "runningtime"
         Columns(4).DataField=   "runningtime"
         Columns(4).FieldLen=   256
         Columns(5).Width=   3360
         Columns(5).Caption=   "Completed Date"
         Columns(5).Name =   "completeddate"
         Columns(5).DataField=   "completeddate"
         Columns(5).DataType=   7
         Columns(5).FieldLen=   256
         Columns(5).Style=   1
         Columns(6).Width=   1482
         Columns(6).Caption=   "Final"
         Columns(6).Name =   "finalstatus"
         Columns(6).DataField=   "finalstatus"
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "jobID"
         Columns(7).Name =   "companyID"
         Columns(7).DataField=   "companyID"
         Columns(7).DataType=   17
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "jobdetailID"
         Columns(8).Name =   "jobdetailID"
         Columns(8).DataField=   "jobdetailID"
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "By"
         Columns(9).Name =   "completeduser"
         Columns(9).DataField=   "completeduser"
         Columns(9).FieldLen=   256
         Columns(10).Width=   1931
         Columns(10).Caption=   "portaluserID"
         Columns(10).Name=   "portaluserID"
         Columns(10).DataField=   "portaluserID"
         Columns(10).FieldLen=   256
         Columns(11).Width=   2619
         Columns(11).Caption=   "skibblycustomerID"
         Columns(11).Name=   "skibblycustomerID"
         Columns(11).DataField=   "skibblycustomerID"
         Columns(11).FieldLen=   256
         Columns(12).Width=   3200
         Columns(12).Visible=   0   'False
         Columns(12).Caption=   "clipID"
         Columns(12).Name=   "clipID"
         Columns(12).DataField=   "clipID"
         Columns(12).FieldLen=   256
         Columns(13).Width=   609
         Columns(13).Caption=   "Bill"
         Columns(13).Name=   "SelectedForBilling"
         Columns(13).DataField=   "SelectedForBilling"
         Columns(13).FieldLen=   256
         Columns(13).Style=   2
         _ExtentX        =   25241
         _ExtentY        =   5953
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDetailAnotherCopy 
         Bindings        =   "frmJob.frx":7A94
         Height          =   2595
         Left            =   -74880
         TabIndex        =   273
         TabStop         =   0   'False
         Top             =   480
         Width           =   11310
         _Version        =   196617
         stylesets.count =   1
         stylesets(0).Name=   "COMPLETED"
         stylesets(0).BackColor=   7011261
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmJob.frx":7AAF
         BeveColorScheme =   1
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16051436
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   10
         Columns(0).Width=   370
         Columns(0).Name =   "copytype"
         Columns(0).DataField=   "copytype"
         Columns(0).Case =   2
         Columns(0).FieldLen=   1
         Columns(1).Width=   12594
         Columns(1).Caption=   "Description / Work to be done"
         Columns(1).Name =   "description"
         Columns(1).DataField=   "description"
         Columns(1).FieldLen=   256
         Columns(2).Width=   714
         Columns(2).Caption=   "Qty"
         Columns(2).Name =   "quantity"
         Columns(2).DataField=   "quantity"
         Columns(2).FieldLen=   256
         Columns(3).Width=   2646
         Columns(3).Caption=   "Format"
         Columns(3).Name =   "format"
         Columns(3).DataField=   "format"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   979
         Columns(4).Caption=   "R/T"
         Columns(4).Name =   "runningtime"
         Columns(4).DataField=   "runningtime"
         Columns(4).FieldLen=   256
         Columns(5).Width=   1482
         Columns(5).Caption=   "real mins?"
         Columns(5).Name =   "chargemins"
         Columns(5).DataField=   "chargerealminutes"
         Columns(5).FieldLen=   256
         Columns(5).Style=   2
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "jobID"
         Columns(6).Name =   "jobID"
         Columns(6).DataField=   "jobID"
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "jobdetailID"
         Columns(7).Name =   "jobdetailID"
         Columns(7).DataField=   "jobdetailID"
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "Completed Date"
         Columns(8).Name =   "completeddate"
         Columns(8).DataField=   "completeddate"
         Columns(8).DataType=   7
         Columns(8).FieldLen=   256
         Columns(8).Style=   1
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "By"
         Columns(9).Name =   "completeduser"
         Columns(9).DataField=   "completeduser"
         Columns(9).FieldLen=   256
         _ExtentX        =   19950
         _ExtentY        =   4577
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdProgressItems 
         Bindings        =   "frmJob.frx":7ACB
         Height          =   2595
         Left            =   -62700
         TabIndex        =   274
         TabStop         =   0   'False
         Top             =   480
         Width           =   11310
         _Version        =   196617
         stylesets.count =   1
         stylesets(0).Name=   "COMPLETED"
         stylesets(0).BackColor=   7011261
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmJob.frx":7AE5
         BeveColorScheme =   1
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeRow   =   3
         ForeColorEven   =   0
         BackColorOdd    =   16051436
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   11
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "companyID"
         Columns(0).Name =   "companyID"
         Columns(0).DataField=   "companyID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   5292
         Columns(1).Caption=   "Title"
         Columns(1).Name =   "title"
         Columns(1).DataField=   "title"
         Columns(1).FieldLen=   256
         Columns(2).Width=   5292
         Columns(2).Caption=   "Reference"
         Columns(2).Name =   "ISAN"
         Columns(2).DataField=   "ISAN"
         Columns(2).FieldLen=   256
         Columns(3).Width=   979
         Columns(3).Caption=   "R/T"
         Columns(3).Name =   "chargedrunningtime"
         Columns(3).DataField=   "chargedrunningtime"
         Columns(3).FieldLen=   256
         Columns(4).Width=   1773
         Columns(4).Caption=   "Format"
         Columns(4).Name =   "format"
         Columns(4).DataField=   "format"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   1058
         Columns(5).Caption=   "HD"
         Columns(5).Name =   "hd"
         Columns(5).CaptionAlignment=   2
         Columns(5).DataField=   "hd"
         Columns(5).FieldLen=   256
         Columns(5).Style=   2
         Columns(6).Width=   1058
         Columns(6).Caption=   "F Tr"
         Columns(6).Name =   "filmtreatment"
         Columns(6).CaptionAlignment=   2
         Columns(6).DataField=   "filmtreatment"
         Columns(6).FieldLen=   256
         Columns(6).Style=   2
         Columns(7).Width=   1058
         Columns(7).Caption=   "F Wa"
         Columns(7).Name =   "filmwash"
         Columns(7).CaptionAlignment=   2
         Columns(7).DataField=   "filmwash"
         Columns(7).FieldLen=   256
         Columns(7).Style=   2
         Columns(8).Width=   1058
         Columns(8).Caption=   "A. Pr"
         Columns(8).Name =   "audioprocess"
         Columns(8).CaptionAlignment=   2
         Columns(8).DataField=   "audioprocess"
         Columns(8).FieldLen=   256
         Columns(8).Style=   2
         Columns(9).Width=   1058
         Columns(9).Caption=   "V. Rst"
         Columns(9).Name =   "videorestore"
         Columns(9).CaptionAlignment=   2
         Columns(9).DataField=   "videorestore"
         Columns(9).FieldLen=   256
         Columns(9).Style=   2
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "billed"
         Columns(10).Name=   "billed"
         Columns(10).CaptionAlignment=   2
         Columns(10).DataField=   "billed"
         Columns(10).FieldLen=   256
         Columns(10).Style=   2
         _ExtentX        =   19950
         _ExtentY        =   4577
         _StockProps     =   79
         Caption         =   "Wellcome Tracker"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdRequiredFiles 
         Bindings        =   "frmJob.frx":7B01
         Height          =   1335
         Left            =   -60600
         TabIndex        =   290
         Top             =   6180
         Width           =   10035
         _Version        =   196617
         AllowUpdate     =   0   'False
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   15
         Columns(0).Width=   1482
         Columns(0).Caption=   "eventID"
         Columns(0).Name =   "eventID"
         Columns(0).DataField=   "eventID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   1402
         Columns(1).Caption=   "Series"
         Columns(1).Name =   "evenseries"
         Columns(1).DataField=   "eventseries"
         Columns(1).FieldLen=   256
         Columns(2).Width=   1402
         Columns(2).Caption=   "Episode"
         Columns(2).Name =   "eventepisode"
         Columns(2).DataField=   "eventepisode"
         Columns(2).FieldLen=   256
         Columns(3).Width=   5292
         Columns(3).Caption=   "Title"
         Columns(3).Name =   "Title"
         Columns(3).DataField=   "eventtitle"
         Columns(3).FieldLen=   256
         Columns(4).Width=   5292
         Columns(4).Caption=   "Sub Title"
         Columns(4).Name =   "Sub Title"
         Columns(4).DataField=   "eventsubtitle"
         Columns(4).FieldLen=   256
         Columns(5).Width=   2646
         Columns(5).Caption=   "Format"
         Columns(5).Name =   "Format"
         Columns(5).DataField=   "clipformat"
         Columns(5).FieldLen=   256
         Columns(6).Width=   1402
         Columns(6).Caption=   "Horiz."
         Columns(6).Name =   "Horiz."
         Columns(6).Alignment=   2
         Columns(6).DataField=   "cliphorizontalpixels"
         Columns(6).FieldLen=   256
         Columns(7).Width=   1402
         Columns(7).Caption=   "Vert."
         Columns(7).Name =   "Vert."
         Columns(7).Alignment=   2
         Columns(7).DataField=   "clipverticalpixels"
         Columns(7).FieldLen=   256
         Columns(8).Width=   1640
         Columns(8).Caption=   "Bit Rate"
         Columns(8).Name =   "Bit Rate"
         Columns(8).DataField=   "clipbitrate"
         Columns(8).FieldLen=   256
         Columns(9).Width=   5292
         Columns(9).Caption=   "File Name"
         Columns(9).Name =   "File Name"
         Columns(9).DataField=   "clipfilename"
         Columns(9).FieldLen=   256
         Columns(10).Width=   2646
         Columns(10).Caption=   "Stored On"
         Columns(10).Name=   "clipstore"
         Columns(10).FieldLen=   256
         Columns(11).Width=   1879
         Columns(11).Caption=   "File Size (GB)"
         Columns(11).Name=   "filesize"
         Columns(11).Alignment=   2
         Columns(11).FieldLen=   256
         Columns(12).Width=   4419
         Columns(12).Caption=   "Owned By"
         Columns(12).Name=   "Owned By"
         Columns(12).DataField=   "companyname"
         Columns(12).FieldLen=   256
         Columns(13).Width=   3200
         Columns(13).Visible=   0   'False
         Columns(13).Caption=   "libraryID"
         Columns(13).Name=   "libraryID"
         Columns(13).DataField=   "libraryID"
         Columns(13).FieldLen=   256
         Columns(14).Width=   3200
         Columns(14).Visible=   0   'False
         Columns(14).Caption=   "requiredmediaID"
         Columns(14).Name=   "requiredmediaID"
         Columns(14).DataField=   "requiredmediaID"
         Columns(14).FieldLen=   256
         _ExtentX        =   17701
         _ExtentY        =   2355
         _StockProps     =   79
         Caption         =   "Media Files Required by this Job"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdTechReviewMediaFiles 
         Bindings        =   "frmJob.frx":7B1B
         Height          =   1215
         Left            =   -74880
         TabIndex        =   294
         Top             =   3420
         Width           =   19260
         _Version        =   196617
         RecordSelectors =   0   'False
         AllowUpdate     =   0   'False
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   14
         Columns(0).Width=   1482
         Columns(0).Caption=   "eventID"
         Columns(0).Name =   "eventID"
         Columns(0).DataField=   "eventID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   794
         Columns(1).Caption=   "Sr."
         Columns(1).Name =   "evenseries"
         Columns(1).DataField=   "eventseries"
         Columns(1).FieldLen=   256
         Columns(2).Width=   794
         Columns(2).Caption=   "Ep."
         Columns(2).Name =   "eventepisode"
         Columns(2).DataField=   "eventepisode"
         Columns(2).FieldLen=   256
         Columns(3).Width=   5292
         Columns(3).Caption=   "Title"
         Columns(3).Name =   "Title"
         Columns(3).DataField=   "eventtitle"
         Columns(3).FieldLen=   256
         Columns(4).Width=   5292
         Columns(4).Caption=   "Sub Title"
         Columns(4).Name =   "Sub Title"
         Columns(4).DataField=   "eventsubtitle"
         Columns(4).FieldLen=   256
         Columns(5).Width=   2117
         Columns(5).Caption=   "Format"
         Columns(5).Name =   "Format"
         Columns(5).DataField=   "clipformat"
         Columns(5).FieldLen=   256
         Columns(6).Width=   873
         Columns(6).Caption=   "Horiz"
         Columns(6).Name =   "Horiz."
         Columns(6).Alignment=   2
         Columns(6).DataField=   "cliphorizontalpixels"
         Columns(6).FieldLen=   256
         Columns(7).Width=   873
         Columns(7).Caption=   "Vert"
         Columns(7).Name =   "Vert."
         Columns(7).Alignment=   2
         Columns(7).DataField=   "clipverticalpixels"
         Columns(7).FieldLen=   256
         Columns(8).Width=   1640
         Columns(8).Caption=   "Bit Rate"
         Columns(8).Name =   "Bit Rate"
         Columns(8).DataField=   "clipbitrate"
         Columns(8).FieldLen=   256
         Columns(9).Width=   5292
         Columns(9).Caption=   "File Name"
         Columns(9).Name =   "File Name"
         Columns(9).DataField=   "clipfilename"
         Columns(9).FieldLen=   256
         Columns(10).Width=   2646
         Columns(10).Caption=   "Stored On"
         Columns(10).Name=   "clipstore"
         Columns(10).FieldLen=   256
         Columns(11).Width=   1402
         Columns(11).Caption=   "Size (GB)"
         Columns(11).Name=   "filesize"
         Columns(11).Alignment=   2
         Columns(11).FieldLen=   256
         Columns(12).Width=   4419
         Columns(12).Caption=   "Owned By"
         Columns(12).Name=   "Owned By"
         Columns(12).DataField=   "companyname"
         Columns(12).FieldLen=   256
         Columns(13).Width=   3200
         Columns(13).Visible=   0   'False
         Columns(13).Caption=   "libraryID"
         Columns(13).Name=   "libraryID"
         Columns(13).DataField=   "libraryID"
         Columns(13).FieldLen=   256
         _ExtentX        =   33972
         _ExtentY        =   2143
         _StockProps     =   79
         Caption         =   "Media Files Reviewed by this Job"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdJobContacts 
         Bindings        =   "frmJob.frx":7B35
         Height          =   7095
         Left            =   -74820
         TabIndex        =   301
         Top             =   420
         Width           =   10800
         _Version        =   196617
         BeveColorScheme =   1
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16772351
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   7
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "companycontactID"
         Columns(0).Name =   "companycontactID"
         Columns(0).DataField=   "companycontactID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   6959
         Columns(1).Caption=   "Full Name"
         Columns(1).Name =   "name"
         Columns(1).DataField=   "name"
         Columns(1).FieldLen=   256
         Columns(1).Style=   1
         Columns(2).Width=   7938
         Columns(2).Caption=   "Email"
         Columns(2).Name =   "email"
         Columns(2).DataField=   "email"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "companyID"
         Columns(3).Name =   "companyID"
         Columns(3).DataField=   "companyID"
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "jobID"
         Columns(4).Name =   "jobID"
         Columns(4).DataField=   "jobID"
         Columns(4).FieldLen=   256
         Columns(5).Width=   714
         Columns(5).Caption=   "CC"
         Columns(5).Name =   "CCnotification"
         Columns(5).DataField=   "CCnotification"
         Columns(5).FieldLen=   256
         Columns(5).Style=   2
         Columns(6).Width=   2117
         Columns(6).Caption=   "portaluserID"
         Columns(6).Name =   "portaluserID"
         Columns(6).DataField=   "portaluserID"
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         _ExtentX        =   19050
         _ExtentY        =   12515
         _StockProps     =   79
         Caption         =   "Customer Contact Information (for delivery notifications etc.)"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSAdodcLib.Adodc adoJobContact 
         Height          =   330
         Left            =   -53760
         Top             =   5100
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoJobContact"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoHistory 
         Height          =   330
         Left            =   14340
         Top             =   1200
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoHistory"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoJobDetail 
         Height          =   330
         Left            =   -54540
         Top             =   420
         Width           =   2505
         _ExtentX        =   4419
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.Label lblCaption 
         Caption         =   "Notification Email Message Body"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   60
         Left            =   -63600
         TabIndex        =   324
         Top             =   1320
         Width           =   2655
      End
      Begin VB.Label lblCaption 
         Caption         =   "Notification Email Subject Line"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   55
         Left            =   -63600
         TabIndex        =   322
         Top             =   1020
         Width           =   2655
      End
      Begin VB.Label lblCaption 
         Caption         =   "Total Filesize of Job Media (GB)"
         Height          =   255
         Index           =   29
         Left            =   -72660
         TabIndex        =   279
         Top             =   480
         Width           =   2415
      End
      Begin VB.Label lblTotalMediaSize 
         Appearance      =   0  'Flat
         BackColor       =   &H0000FFFF&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -70200
         TabIndex        =   278
         Tag             =   "CLEARFIELDS"
         Top             =   480
         Width           =   675
      End
      Begin VB.Label lblCaption 
         Caption         =   "BBC Buisness Area"
         Height          =   255
         Index           =   106
         Left            =   -65220
         TabIndex        =   268
         Top             =   780
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "BBC Booking Number"
         Height          =   255
         Index           =   105
         Left            =   -65220
         TabIndex        =   267
         Top             =   480
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "BBC Country Code"
         Height          =   255
         Index           =   104
         Left            =   -69420
         TabIndex        =   266
         Top             =   780
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "BBC Nominal Code"
         Height          =   255
         Index           =   103
         Left            =   -69420
         TabIndex        =   265
         Top             =   480
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "BBC Business Area Code"
         Height          =   255
         Index           =   102
         Left            =   -74880
         TabIndex        =   260
         Top             =   1200
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "BBC Charge Code"
         Height          =   255
         Index           =   101
         Left            =   -74880
         TabIndex        =   258
         Top             =   840
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "BBC Customer Name"
         Height          =   255
         Index           =   100
         Left            =   -74880
         TabIndex        =   256
         Top             =   480
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Add as media required"
         Height          =   255
         Index           =   98
         Left            =   -68340
         TabIndex        =   224
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label lblCaption 
         Caption         =   "Sent To Accounts Date:"
         Height          =   255
         Index           =   97
         Left            =   -64680
         TabIndex        =   220
         Top             =   360
         Width           =   1755
      End
      Begin VB.Label lblSentToAccountsDate 
         Height          =   195
         Index           =   1
         Left            =   -62820
         TabIndex        =   219
         Top             =   360
         Width           =   1995
      End
      Begin VB.Label lblInvoicedDate 
         Height          =   195
         Left            =   -67260
         TabIndex        =   218
         Top             =   360
         Width           =   1995
      End
      Begin VB.Label lblCostedDate 
         Height          =   195
         Left            =   -70560
         TabIndex        =   217
         Top             =   360
         Width           =   1995
      End
      Begin VB.Label lblHoldCostDate 
         Height          =   195
         Left            =   -73680
         TabIndex        =   216
         Top             =   360
         Width           =   1995
      End
      Begin VB.Label lblCaption 
         Caption         =   "Finalised Date:"
         Height          =   255
         Index           =   96
         Left            =   -68400
         TabIndex        =   215
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Costed Date:"
         Height          =   255
         Index           =   95
         Left            =   -71580
         TabIndex        =   214
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Hold Cost Date:"
         Height          =   255
         Index           =   85
         Left            =   -74880
         TabIndex        =   213
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label lblCreditNoteNumber 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -61980
         TabIndex        =   202
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "The initials of the user who sent this job to Accounts"
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Credit Note #"
         Height          =   255
         Index           =   91
         Left            =   -63120
         TabIndex        =   201
         Top             =   480
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "PLEASE MAKE SURE YOU CLICK SAVE AFTER MAKING CHANGES TO DELIVERY ADDRESSES!!!"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   89
         Left            =   -70800
         TabIndex        =   199
         Top             =   5340
         Width           =   7395
      End
      Begin VB.Label lblCaption 
         Caption         =   "Before using operator notes, please make sure your job details are as descriptive as possible - especially for dubbings!"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   88
         Left            =   -73800
         TabIndex        =   198
         Top             =   1680
         Width           =   7695
      End
      Begin VB.Label lblCaption 
         Caption         =   "Before using despatch notes, be sure to create despatch records using the 'Delivery Notes' button at the bottom of this form."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   84
         Left            =   -73800
         TabIndex        =   192
         Top             =   3840
         Width           =   7695
      End
      Begin VB.Label lblCaption 
         Caption         =   "Invoice #"
         Height          =   255
         Index           =   27
         Left            =   -66000
         TabIndex        =   157
         Top             =   480
         Width           =   795
      End
      Begin VB.Label lblInvoiceNumber 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -65160
         TabIndex        =   156
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "The initials of the user who sent this job to Accounts"
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Invoiced Date (Finalised Date)"
         Height          =   375
         Index           =   72
         Left            =   -69000
         TabIndex        =   155
         Top             =   480
         Width           =   1155
      End
      Begin VB.Line Line6 
         X1              =   -74880
         X2              =   -69180
         Y1              =   1920
         Y2              =   1920
      End
      Begin VB.Label lblCaption 
         Caption         =   "Notes To Insert"
         Height          =   495
         Index           =   71
         Left            =   -74880
         TabIndex        =   151
         Top             =   2040
         Width           =   675
      End
      Begin VB.Label lblCaption 
         Caption         =   "Associate library barcode:"
         Height          =   255
         Index           =   70
         Left            =   -64380
         TabIndex        =   148
         Top             =   480
         Width           =   1875
      End
      Begin VB.Label lblCaption 
         Caption         =   "Invoice Notes"
         Height          =   435
         Index           =   69
         Left            =   -74880
         TabIndex        =   146
         Top             =   3360
         Width           =   615
      End
      Begin VB.Label lblCaption 
         Caption         =   "Invoice Text"
         Height          =   435
         Index           =   68
         Left            =   -74820
         TabIndex        =   144
         Top             =   480
         Width           =   735
      End
      Begin VB.Label lblCaption 
         Caption         =   "New"
         Height          =   255
         Index           =   66
         Left            =   -72600
         TabIndex        =   142
         Top             =   540
         Width           =   375
      End
      Begin VB.Label lblCaption 
         Caption         =   "Authorised"
         Height          =   255
         Index           =   65
         Left            =   -70200
         TabIndex        =   141
         Top             =   540
         Width           =   795
      End
      Begin VB.Label lblCaption 
         Caption         =   "Approved"
         Height          =   255
         Index           =   64
         Left            =   -71400
         TabIndex        =   140
         Top             =   540
         Width           =   735
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   1  'Opaque
         Height          =   255
         Left            =   -72960
         Top             =   540
         Width           =   255
      End
      Begin VB.Shape Shape2 
         BackColor       =   &H0080FF80&
         BackStyle       =   1  'Opaque
         Height          =   255
         Left            =   -70560
         Top             =   540
         Width           =   255
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H0080C0FF&
         BackStyle       =   1  'Opaque
         Height          =   255
         Left            =   -71760
         Top             =   540
         Width           =   255
      End
      Begin VB.Label lblCaption 
         Caption         =   "Sent To Accounts"
         Height          =   255
         Index           =   63
         Left            =   10200
         TabIndex        =   134
         Top             =   480
         Width           =   1335
      End
      Begin VB.Label lblSentToAccountsDate 
         Alignment       =   2  'Center
         BackColor       =   &H80000005&
         Height          =   255
         Index           =   0
         Left            =   11700
         TabIndex        =   133
         Tag             =   "CLEARFIELDS"
         Top             =   480
         Width           =   975
      End
      Begin VB.Label lblCaption 
         Caption         =   "Batch Number"
         Height          =   255
         Index           =   62
         Left            =   10200
         TabIndex        =   132
         Top             =   780
         Width           =   1035
      End
      Begin VB.Label lblSentToAccountsBatchNumber 
         Alignment       =   2  'Center
         BackColor       =   &H80000005&
         Height          =   255
         Left            =   11700
         TabIndex        =   131
         Tag             =   "CLEARFIELDS"
         Top             =   780
         Width           =   975
      End
      Begin VB.Label lblPrintedBy 
         Alignment       =   2  'Center
         BackColor       =   &H80000005&
         Height          =   255
         Left            =   8880
         TabIndex        =   130
         Tag             =   "CLEARFIELDS"
         Top             =   780
         Width           =   975
      End
      Begin VB.Label lblCaption 
         Caption         =   "Last Printed By"
         Height          =   255
         Index           =   61
         Left            =   7380
         TabIndex        =   129
         Top             =   780
         Width           =   1275
      End
      Begin VB.Label lblTotalPrints 
         Alignment       =   2  'Center
         BackColor       =   &H80000005&
         Caption         =   "0"
         Height          =   255
         Left            =   8880
         TabIndex        =   128
         Tag             =   "CLEARFIELDS"
         Top             =   480
         Width           =   975
      End
      Begin VB.Label lblCaption 
         Caption         =   "Total J/S Prints"
         Height          =   255
         Index           =   58
         Left            =   7380
         TabIndex        =   127
         Top             =   480
         Width           =   1455
      End
      Begin VB.Label lblCaption 
         Caption         =   "VT Started / By"
         Height          =   255
         Index           =   59
         Left            =   -65700
         TabIndex        =   125
         Top             =   480
         Width           =   1155
      End
      Begin VB.Line Line5 
         BorderColor     =   &H80000010&
         X1              =   -64860
         X2              =   -64860
         Y1              =   420
         Y2              =   7020
      End
      Begin VB.Label lblCaption 
         Caption         =   "Master Job ID"
         Height          =   255
         Index           =   53
         Left            =   3240
         TabIndex        =   121
         Top             =   5520
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Producer Notes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Index           =   11
         Left            =   -74880
         TabIndex        =   96
         Top             =   5940
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Time"
         Height          =   255
         Index           =   7
         Left            =   -72300
         TabIndex        =   86
         Top             =   420
         Width           =   375
      End
      Begin VB.Label Des 
         Caption         =   "VT Deadline for Despatch"
         Height          =   375
         Index           =   6
         Left            =   -74880
         TabIndex        =   85
         Top             =   360
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Accounts Notes"
         Height          =   255
         Index           =   22
         Left            =   -74880
         TabIndex        =   75
         Top             =   3660
         Width           =   1995
      End
      Begin VB.Label lblCaption 
         Caption         =   "Notes To Insert"
         Height          =   255
         Index           =   23
         Left            =   -64740
         TabIndex        =   74
         Top             =   420
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Despatch Notes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   21
         Left            =   -74880
         TabIndex        =   73
         Top             =   3840
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Invoice Notes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   20
         Left            =   -74880
         TabIndex        =   72
         Top             =   420
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Operator / Technician Notes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Index           =   19
         Left            =   -74880
         TabIndex        =   71
         Top             =   1680
         Width           =   1035
      End
   End
   Begin VB.CommandButton cmdLaunch 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   330
      Index           =   5
      Left            =   14400
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmJob.frx":7B51
      Style           =   1  'Graphical
      TabIndex        =   210
      ToolTipText     =   "Change this jobs status"
      Top             =   2220
      UseMaskColor    =   -1  'True
      Width           =   345
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   11400
      Locked          =   -1  'True
      TabIndex        =   209
      ToolTipText     =   "Current status of selected job"
      Top             =   2220
      Width           =   2895
   End
   Begin VB.Frame fraJobOptions 
      Caption         =   "Additional Job Options"
      Height          =   1215
      Left            =   9660
      TabIndex        =   91
      Top             =   420
      Visible         =   0   'False
      Width           =   6315
      Begin VB.CheckBox chkNoChargeJob 
         Caption         =   "No-Charge Job"
         Height          =   255
         Left            =   180
         TabIndex        =   326
         ToolTipText     =   "Is Job on Hold?"
         Top             =   840
         Width           =   1635
      End
      Begin VB.CheckBox chkDoWholeSeries 
         Caption         =   "Do Whole Series"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   4260
         TabIndex        =   307
         ToolTipText     =   "Is Job on Urgent?"
         Top             =   840
         Width           =   1935
      End
      Begin VB.CheckBox chkDoNotDeliver 
         Caption         =   "Do Not Deliver Yet"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   4260
         TabIndex        =   303
         ToolTipText     =   "Is Job on Urgent?"
         Top             =   540
         Width           =   1935
      End
      Begin VB.CheckBox chkUrgent 
         Caption         =   "Urgent"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   4260
         TabIndex        =   27
         ToolTipText     =   "Is Job on Urgent?"
         Top             =   240
         Width           =   975
      End
      Begin VB.CheckBox chkJobDetailOnInvoice 
         Caption         =   "Job Detail on Invoice"
         Height          =   255
         Left            =   180
         TabIndex        =   23
         ToolTipText     =   "Use the job detail descriptions rather than the rate card descriptions"
         Top             =   240
         Width           =   1935
      End
      Begin VB.CheckBox chkSendEmailOnCompletion 
         Caption         =   "Email on complete"
         Height          =   255
         Left            =   180
         TabIndex        =   25
         ToolTipText     =   "Create an email in Microsoft Outlook when this job has been completed"
         Top             =   540
         Width           =   1635
      End
      Begin VB.CheckBox chkQuote 
         Caption         =   "Quotation"
         Height          =   255
         Left            =   2460
         TabIndex        =   24
         ToolTipText     =   "If this is a Quotation"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CheckBox chkOnHold 
         Caption         =   "On Hold"
         Height          =   255
         Left            =   2460
         TabIndex        =   26
         ToolTipText     =   "Is Job on Hold?"
         Top             =   540
         Width           =   1095
      End
   End
   Begin VB.CommandButton cmdAddOrderReference 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9180
      Picture         =   "frmJob.frx":7F48
      Style           =   1  'Graphical
      TabIndex        =   15
      ToolTipText     =   "Apply a new order reference"
      Top             =   1860
      Width           =   330
   End
   Begin VB.TextBox txtCreated 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000012&
      Height          =   255
      Left            =   11040
      Locked          =   -1  'True
      TabIndex        =   118
      ToolTipText     =   "The job specific fax number"
      Top             =   60
      Width           =   2895
   End
   Begin VB.TextBox txtWorkOrderNumber 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000011&
      Height          =   255
      Left            =   3300
      TabIndex        =   1
      ToolTipText     =   "The unique identifier for this job. In the previous version of CETA, this was known as the 'Booking Number'."
      Top             =   420
      Width           =   915
   End
   Begin VB.CommandButton cmdLaunch 
      Appearance      =   0  'Flat
      Height          =   330
      Index           =   0
      Left            =   9180
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmJob.frx":E79A
      Style           =   1  'Graphical
      TabIndex        =   11
      ToolTipText     =   "Send email to this contact"
      Top             =   780
      UseMaskColor    =   -1  'True
      Width           =   330
   End
   Begin VB.TextBox txtClientStatus 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   8340
      Locked          =   -1  'True
      TabIndex        =   22
      ToolTipText     =   "Current client's status (On hold, cash, ok etc)"
      Top             =   60
      Width           =   1155
   End
   Begin VB.CommandButton cmdLaunch 
      Appearance      =   0  'Flat
      Height          =   330
      Index           =   1
      Left            =   9180
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmJob.frx":EBD2
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Open this company's details in the company editor"
      Top             =   420
      UseMaskColor    =   -1  'True
      Width           =   330
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbJobType 
      Height          =   285
      Left            =   1380
      TabIndex        =   21
      ToolTipText     =   "Select what the main type of work is going to happen on this job"
      Top             =   60
      Width           =   2895
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BevelColorFrame =   -2147483644
      CheckBox3D      =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   4868
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.TextBox txtFax 
      Height          =   285
      Left            =   6180
      TabIndex        =   13
      ToolTipText     =   "The job specific fax number"
      Top             =   1500
      Width           =   2895
   End
   Begin VB.PictureBox picFooter 
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   300
      ScaleHeight     =   315
      ScaleWidth      =   12495
      TabIndex        =   43
      Top             =   15180
      Width           =   12495
      Begin VB.CommandButton cmdTransferLinesFromOtherJob 
         Caption         =   "Transfer Lines from Other Job"
         Height          =   315
         Left            =   60
         TabIndex        =   325
         ToolTipText     =   "Show delivery notes for this job"
         Top             =   0
         Width           =   2595
      End
      Begin VB.CommandButton cmdDuplicateJob 
         Caption         =   "Duplicate"
         Height          =   315
         Left            =   4080
         TabIndex        =   276
         ToolTipText     =   "Clear the form"
         Top             =   0
         Width           =   1395
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         Height          =   315
         Left            =   9960
         TabIndex        =   32
         ToolTipText     =   "Save changes to this job"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   11280
         TabIndex        =   33
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   2820
         TabIndex        =   28
         ToolTipText     =   "Clear the form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   7440
         TabIndex        =   30
         ToolTipText     =   "Print this job"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdCancelJob 
         Caption         =   "Cancel Job"
         Height          =   315
         Left            =   8700
         TabIndex        =   31
         ToolTipText     =   "Cancel this job"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdJobDespatches 
         Caption         =   "Delivery Notes"
         Height          =   315
         Left            =   5640
         TabIndex        =   29
         ToolTipText     =   "Show delivery notes for this job"
         Top             =   0
         Width           =   1695
      End
   End
   Begin VB.TextBox txtSubTitle 
      Height          =   285
      Left            =   1380
      TabIndex        =   6
      ToolTipText     =   "The sub title (if known)"
      Top             =   1500
      Width           =   3315
   End
   Begin VB.TextBox txtTitle 
      Height          =   285
      Left            =   1380
      TabIndex        =   5
      ToolTipText     =   "The title for the job"
      Top             =   1140
      Width           =   3315
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   285
      Left            =   6180
      TabIndex        =   8
      ToolTipText     =   "The company this job is for"
      Top             =   420
      Width           =   2895
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   8
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "accountcode"
      Columns(2).Name =   "accountcode"
      Columns(2).DataField=   "accountcode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "telephone"
      Columns(3).Name =   "telephone"
      Columns(3).DataField=   "telephone"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "cetaclientcode"
      Columns(4).Name =   "cetaclientcode"
      Columns(4).DataField=   "cetaclientcode"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "accountstatus"
      Columns(5).Name =   "accountstatus"
      Columns(5).DataField=   "accountstatus"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "fax"
      Columns(6).Name =   "fax"
      Columns(6).DataField=   "fax"
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Caption=   "email"
      Columns(7).Name =   "email"
      Columns(7).DataField=   "email"
      Columns(7).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin VB.TextBox txtOrderReference 
      Height          =   285
      Left            =   6180
      TabIndex        =   14
      ToolTipText     =   "The clients purchase order reference number"
      Top             =   1860
      Width           =   2895
   End
   Begin VB.TextBox txtTelephone 
      Height          =   285
      Left            =   6180
      TabIndex        =   12
      ToolTipText     =   "The job specific telephone number"
      Top             =   1140
      Width           =   2895
   End
   Begin VB.TextBox txtAccountCode 
      BackColor       =   &H00FFC0FF&
      Height          =   285
      Left            =   16860
      TabIndex        =   7
      ToolTipText     =   "The company account code"
      Top             =   14100
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox txtJobID 
      BackColor       =   &H80000004&
      Height          =   285
      Left            =   1380
      TabIndex        =   0
      ToolTipText     =   "The current job ID. You can load an existing job by typing it's ID in to this box then pressing return"
      Top             =   420
      Width           =   1035
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbContact 
      Height          =   285
      Left            =   6180
      TabIndex        =   10
      ToolTipText     =   "The contact for this job"
      Top             =   780
      Width           =   2895
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   5424
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "telephone"
      Columns(1).Name =   "telephone"
      Columns(1).DataField=   "telephone"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "contactID"
      Columns(2).Name =   "contactID"
      Columns(2).DataField=   "contactID"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "email"
      Columns(3).Name =   "email"
      Columns(3).DataField=   "email"
      Columns(3).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin MSAdodcLib.Adodc adoCosting 
      Height          =   330
      Left            =   13140
      Top             =   12600
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   2
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCosting"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoRateCard 
      Height          =   330
      Left            =   13140
      Top             =   13440
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   8
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoRateCard"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoCreditNote 
      Height          =   330
      Left            =   15480
      Top             =   13440
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCreditNote"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoResourceSchedule 
      Height          =   330
      Left            =   15480
      Top             =   12600
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoResourceSchedule"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbProduct 
      Height          =   285
      Left            =   25200
      TabIndex        =   4
      ToolTipText     =   "The product for this job. Normally associated with a project."
      Top             =   12360
      Visible         =   0   'False
      Width           =   2895
      DataFieldList   =   "name"
      _Version        =   196617
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      CheckBox3D      =   0   'False
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   8438015
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "productID"
      Columns(0).Name =   "productID"
      Columns(0).DataField=   "productID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   5212
      Columns(1).Caption=   "Name"
      Columns(1).Name =   "name"
      Columns(1).DataField=   "name"
      Columns(1).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   8438015
      DataFieldToDisplay=   "name"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbAllocation 
      Height          =   285
      Left            =   25200
      TabIndex        =   2
      ToolTipText     =   "Select the job allocation. ie. Is this part of a project, or a regular (one off) job?"
      Top             =   12660
      Width           =   2895
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BackColorEven   =   -2147483643
      BackColorOdd    =   -2147483643
      RowHeight       =   423
      Columns(0).Width=   4868
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbOurContact 
      Height          =   285
      Left            =   11040
      TabIndex        =   16
      ToolTipText     =   "Our contact on this job"
      Top             =   420
      Width           =   2895
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BevelColorFrame =   -2147483644
      CheckBox3D      =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   4868
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16761024
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbProject 
      Height          =   285
      Left            =   25200
      TabIndex        =   3
      ToolTipText     =   "The referencce number of this project. In the previous version of CETA, this was known as the 'Special Job Number'"
      Top             =   12960
      Visible         =   0   'False
      Width           =   2895
      DataFieldList   =   "projectnumber"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      CheckBox3D      =   0   'False
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   12648447
      RowHeight       =   423
      Columns.Count   =   11
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "projectID"
      Columns(0).Name =   "projectID"
      Columns(0).DataField=   "projectID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   2566
      Columns(1).Caption=   "Project Number"
      Columns(1).Name =   "name"
      Columns(1).DataField=   "projectnumber"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3519
      Columns(2).Caption=   "Product"
      Columns(2).Name =   "productname"
      Columns(2).DataField=   "productname"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "Title"
      Columns(3).Name =   "Title"
      Columns(3).DataField=   "title"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3757
      Columns(4).Caption=   "Company"
      Columns(4).Name =   "companyname"
      Columns(4).DataField=   "companyname"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "Contact"
      Columns(5).Name =   "contactname"
      Columns(5).DataField=   "contactname"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "Our Contact"
      Columns(6).Name =   "ourcontact"
      Columns(6).DataField=   "ourcontact"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Caption=   "Status"
      Columns(7).Name =   "status"
      Columns(7).DataField=   "fd_status"
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "productID"
      Columns(8).Name =   "productID"
      Columns(8).DataField=   "productID"
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "companyID"
      Columns(9).Name =   "companyID"
      Columns(9).DataField=   "companyID"
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "contactID"
      Columns(10).Name=   "contactID"
      Columns(10).DataField=   "contactID"
      Columns(10).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   12648447
      DataFieldToDisplay=   "projectnumber"
   End
   Begin MSAdodcLib.Adodc adoSundry 
      Height          =   330
      Left            =   15480
      Top             =   13020
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasql"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoSundry"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoResource 
      Height          =   330
      Left            =   17820
      Top             =   12600
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoResource"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbVideoStandard 
      Height          =   255
      Left            =   11040
      TabIndex        =   17
      ToolTipText     =   "Current job status"
      Top             =   780
      Width           =   2895
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   4868
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.PictureBox picMovable1 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   17640
      ScaleHeight     =   555
      ScaleWidth      =   4995
      TabIndex        =   204
      Top             =   480
      Visible         =   0   'False
      Width           =   4995
      Begin VB.PictureBox pnlDeal 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   60
         ScaleHeight     =   375
         ScaleWidth      =   4875
         TabIndex        =   205
         Top             =   60
         Width           =   4875
         Begin VB.TextBox txtDealPrice 
            Alignment       =   1  'Right Justify
            BackColor       =   &H008080FF&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2640
            TabIndex        =   19
            ToolTipText     =   "The company account code"
            Top             =   60
            Width           =   795
         End
         Begin VB.CommandButton cmdApplyDeal 
            Caption         =   "Apply"
            Height          =   270
            Left            =   3540
            TabIndex        =   20
            ToolTipText     =   "Apply this deal price to the costings"
            Top             =   60
            Width           =   705
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbDealType 
            Height          =   285
            Left            =   1320
            TabIndex        =   18
            ToolTipText     =   "The contact for this job"
            Top             =   60
            Width           =   1095
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            Cols            =   1
            BorderStyle     =   0
            ColumnHeaders   =   0   'False
            ForeColorEven   =   -2147483640
            ForeColorOdd    =   -2147483640
            BackColorEven   =   -2147483643
            BackColorOdd    =   8421631
            RowHeight       =   423
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            _ExtentX        =   1931
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   8421631
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 0"
         End
         Begin VB.Label lblCaption 
            Caption         =   "Deal Type"
            Height          =   255
            Index           =   56
            Left            =   0
            TabIndex        =   207
            Top             =   60
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "�"
            Height          =   255
            Index           =   57
            Left            =   2460
            TabIndex        =   206
            Top             =   60
            Width           =   135
         End
      End
      Begin VB.Line Line7 
         BorderColor     =   &H80000010&
         Index           =   0
         X1              =   60
         X2              =   4260
         Y1              =   480
         Y2              =   480
      End
      Begin VB.Line Line7 
         BorderColor     =   &H80000010&
         Index           =   1
         X1              =   60
         X2              =   4260
         Y1              =   0
         Y2              =   0
      End
   End
   Begin MSAdodcLib.Adodc adoMedia 
      Height          =   330
      Index           =   0
      Left            =   21300
      Top             =   12060
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoMedia"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoMedia 
      Height          =   330
      Index           =   1
      Left            =   21300
      Top             =   12480
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoMedia"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoMedia 
      Height          =   330
      Index           =   2
      Left            =   21300
      Top             =   12900
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoMedia"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoExtendedJobDetail 
      Height          =   330
      Left            =   17880
      Top             =   12120
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   2
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoExtendedJobDetail"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoUnbilled 
      Height          =   330
      Left            =   17820
      Top             =   13020
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoUnbilled"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoProgress 
      Height          =   330
      Left            =   17820
      Top             =   13440
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoProgress"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoMedia 
      Height          =   330
      Index           =   3
      Left            =   21300
      Top             =   13320
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoMedia"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoInclusive 
      Height          =   330
      Left            =   21300
      Top             =   11640
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoInclusive"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoMedia 
      Height          =   330
      Index           =   4
      Left            =   21300
      Top             =   13740
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoMedia"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoMedia 
      Height          =   330
      Index           =   5
      Left            =   21300
      Top             =   14160
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoMedia"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label lblCaption 
      Caption         =   "DADC AE Number"
      Height          =   255
      Index           =   38
      Left            =   17700
      TabIndex        =   330
      Top             =   1860
      Visible         =   0   'False
      Width           =   1755
   End
   Begin VB.Label lblCaption 
      Caption         =   "DADC Series ID"
      Height          =   255
      Index           =   37
      Left            =   17700
      TabIndex        =   328
      Top             =   1500
      Visible         =   0   'False
      Width           =   1755
   End
   Begin VB.Label lblRedo 
      Caption         =   "This is a Redo Job"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   14100
      TabIndex        =   318
      Top             =   60
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label lblCaption 
      Caption         =   "Customer Contract #"
      Height          =   255
      Index           =   4
      Left            =   9780
      TabIndex        =   309
      Top             =   1920
      Width           =   1755
   End
   Begin VB.Label lblCaption 
      Caption         =   "Series ID"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   32
      Left            =   120
      TabIndex        =   300
      Top             =   840
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Supplier / Source"
      Height          =   255
      Index           =   31
      Left            =   4920
      TabIndex        =   296
      Top             =   2280
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Est Time Req."
      Height          =   195
      Index           =   30
      Left            =   120
      TabIndex        =   288
      Top             =   1920
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job Status"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   47
      Left            =   9720
      TabIndex        =   211
      Top             =   2280
      Width           =   1035
   End
   Begin VB.Line Line7 
      BorderColor     =   &H80000010&
      Index           =   2
      X1              =   4860
      X2              =   9420
      Y1              =   1800
      Y2              =   1800
   End
   Begin VB.Label lblCaption 
      Caption         =   "Client P/O #"
      Height          =   255
      Index           =   54
      Left            =   4920
      TabIndex        =   126
      Top             =   1920
      Width           =   1035
   End
   Begin VB.Label lblJobID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   20100
      TabIndex        =   123
      Tag             =   "CLEARFIELDS"
      Top             =   12840
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Video Standard"
      Height          =   255
      Index           =   44
      Left            =   9720
      TabIndex        =   122
      Top             =   780
      Width           =   1155
   End
   Begin VB.Line Line4 
      BorderColor     =   &H80000010&
      X1              =   9600
      X2              =   9600
      Y1              =   0
      Y2              =   2460
   End
   Begin VB.Line Line3 
      BorderColor     =   &H80000010&
      X1              =   4800
      X2              =   4800
      Y1              =   60
      Y2              =   2520
   End
   Begin VB.Label lblCaption 
      Caption         =   "Created / By"
      Height          =   255
      Index           =   6
      Left            =   9720
      TabIndex        =   119
      Top             =   60
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Old J/N"
      ForeColor       =   &H80000011&
      Height          =   255
      Index           =   52
      Left            =   2520
      TabIndex        =   114
      Top             =   420
      Width           =   615
   End
   Begin VB.Label lblCaption 
      Caption         =   "Our Contact"
      Height          =   255
      Index           =   12
      Left            =   9720
      TabIndex        =   97
      Top             =   420
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Client Status"
      Height          =   255
      Index           =   10
      Left            =   7260
      TabIndex        =   92
      Top             =   120
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Project Type"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   49
      Left            =   23940
      TabIndex        =   90
      Top             =   12720
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Project #"
      Height          =   255
      Index           =   33
      Left            =   23940
      TabIndex        =   82
      Top             =   13020
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Caption         =   "Product"
      Height          =   255
      Index           =   46
      Left            =   23940
      TabIndex        =   81
      Top             =   12420
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Label lblProjectID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   15060
      TabIndex        =   79
      Tag             =   "CLEARFIELDS"
      Top             =   14460
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Fax"
      Height          =   195
      Index           =   35
      Left            =   4920
      TabIndex        =   78
      Top             =   1560
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job Type"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   34
      Left            =   120
      TabIndex        =   77
      Top             =   120
      Width           =   1035
   End
   Begin VB.Label lblProductID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   15120
      TabIndex        =   76
      Tag             =   "CLEARFIELDS"
      Top             =   14100
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblTwoTier 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   13200
      TabIndex        =   44
      Tag             =   "CLEARFIELDS"
      Top             =   12120
      Width           =   4515
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job ID"
      Height          =   255
      Index           =   16
      Left            =   120
      TabIndex        =   42
      Top             =   480
      Width           =   1035
   End
   Begin VB.Label lblContactID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   14040
      TabIndex        =   41
      Tag             =   "CLEARFIELDS"
      Top             =   14100
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCompanyID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   6180
      TabIndex        =   40
      Tag             =   "CLEARFIELDS"
      Top             =   60
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Sub Title"
      Height          =   195
      Index           =   9
      Left            =   120
      TabIndex        =   39
      Top             =   1560
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      Height          =   195
      Index           =   8
      Left            =   120
      TabIndex        =   38
      Top             =   1200
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Contact"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   4920
      TabIndex        =   37
      Top             =   840
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Telephone"
      Height          =   255
      Index           =   3
      Left            =   4920
      TabIndex        =   36
      Top             =   1200
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Client"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   4920
      TabIndex        =   35
      Top             =   480
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Client A/C #"
      Height          =   255
      Index           =   1
      Left            =   4920
      TabIndex        =   34
      Top             =   120
      Width           =   1035
   End
End
Attribute VB_Name = "frmJob"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const m_intTabJob = 0
Const m_intTabCosting = 1
Const m_intTabLibrary = 2
Const m_intTabNotes = 3
Const m_intTabInvoice = 4
Const m_intTabBBCWW = 5
Const m_intTabDADCSvensk = 6
Const m_intTabSchedule = 7
Const m_intTabQuote = 8
Const m_intTabSundryCosts = 9
Const m_intTabDespatch = 10
Const m_intTabTechnical = 11
Const m_intUnBilled = 12
Const m_intProgress = 13
Dim m_intClearJunk As Integer
Dim m_blnDelete As Boolean

Public Sub FinaliseBulkCreditNote()

Dim l_blnResult As Boolean, TempInt As Integer

If Val(txtCostingSheet.Text) <> 0 Then
    MsgBox "This job is part of a costing sheet and so can not be finalised!", vbExclamation
    Exit Sub
End If

If cmdInvoice.Caption = "Invoice" Or cmdInvoice.Caption = "Finalise" Then

    If Val(txtJobID.Text) = 0 Then
        NoJobSelectedMessage
        Exit Sub
    End If
    
    If GetData("job", "flagquote", "jobID", Val(txtJobID.Text)) <> 0 Then
        MsgBox "Cannot Finalise Job", vbCritical, "Job is still flagged as a Quotation"
        Exit Sub
    End If
    
'    Dim i As Integer
'    i = MsgBox("Do you really want to FINALISE this job?", vbYesNo + vbQuestion)
'    If i = vbNo Then Exit Sub
'
    TempInt = g_optDontPromptForInvoiceAddress
    g_optDontPromptForInvoiceAddress = 1
    'call the invoice job routine and create the invoice header if required
    If UCase(Left(cmbJobType.Text, 6)) = "CREDIT" Then
        l_blnResult = InvoiceJob(Val(txtJobID.Text), "CREDIT")
    Else
        l_blnResult = InvoiceJob(Val(txtJobID.Text), "INVOICE")
    End If
    g_optDontPromptForInvoiceAddress = TempInt

Else

    'Caption must be re-print, so hard set the result caption to have been sucessful
    l_blnResult = True
    
End If

If l_blnResult = True Then
    
    If g_optDontPromptForNumberOfInvoiceCopies Then
        If g_intDefaultCopiesToPrintOfInvoice > 0 Then PrintCostingPage Val(txtJobID.Text), g_intDefaultCopiesToPrintOfInvoice
    Else
    
'        frmCopiesToPrint.txtCopies.Text = CStr(g_intDefaultCopiesToPrintOfInvoice)
'        frmCopiesToPrint.Show vbModal
'
'        If frmCopiesToPrint.Tag <> "CANCELLED" Then
'            PrintCostingPage Val(txtJobID.Text), Val(frmCopiesToPrint.txtCopies.Text)
'        Else
            PrintCostingPage Val(txtJobID.Text), 0
'        End If
        
'        Unload frmCopiesToPrint
'        Set frmCopiesToPrint = Nothing
        
        tab1.Tab = m_intTabCosting
    End If
    
    ShowJob Val(txtJobID.Text), 0, False
'    ShowJob Val(txtJobID.Text), 0, False
    
End If

End Sub

Public Sub MakeBulkCreditNote()

'Dim l_blnResult As Boolean''

If Not CheckAccess("/makecreditnote") Then
    Exit Sub
End If

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

'Dim l_intMsg As Integer
'l_intMsg = MsgBox("Do you really want to CREDIT this job?", vbYesNo + vbQuestion)
'If l_intMsg = vbNo Then Exit Sub

Dim l_lngJobID As Long
Dim l_lngOriginalJobID As Long

'this is only allowed for jobs which are already posted in accounts
If txtStatus.Text <> "Sent To Accounts" Then
    MsgBox "You can only credit jobs which have already been sent to accounts", vbExclamation
    Exit Sub
End If

'get the original job ID
l_lngOriginalJobID = Val(txtJobID.Text)

'generate a copy of this job, and pick up the jobID
l_lngJobID = DuplicateJob(l_lngOriginalJobID, "CREDIT")

'if the duplicate job function worked
If l_lngJobID > 0 Then

    'update the new job record, so that it doesnt have the notes etc from the previous job.
    'also - set the new jobs' jobtype to be "credit" and set the status to "Hold Cost"
    Dim l_strSQL As String
    l_strSQL = "UPDATE job SET fd_status = 'Hold Cost', notes1 = NULL, notes2 = NULL, notes3 = NULL, notes4 = NULL, notes5 = NULL, jobtype = 'Credit', " & _
        "senttoaccountsbatchnumber = NULL, senttoaccountsdate = NULL, senttoaccountsuser = NULL, " & _
        "creditnotenumber = NULL, creditnotebatchnumber = NULL, creditnotedate = NULL, creditsenttoaccountsdate = NULL, creditsenttoaccountsuser = NULL WHERE jobID = '" & l_lngJobID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'copy all the costing lines from the original job, but make them negetive
    CopyCostingLines l_lngOriginalJobID, l_lngJobID, "CREDIT"
    
    'update the job history of the original job
    AddJobHistory l_lngOriginalJobID, "CREDITED JOB - " & l_lngJobID
    
    'add job history for the new job
    AddJobHistory l_lngJobID, "MADE CREDIT NOTE"
    
    ShowJob l_lngJobID, 1, False
    
    DoEvents
    
End If

End Sub

Private Sub adoCosting_RecordChangeComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal cRecords As Long, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

Dim l_dblTotal As Double
Dim l_dblRateCardTotal As Double

l_dblTotal = RoundToCurrency(CDbl(GetTotalCostsForJob(Val(txtJobID.Text), "total", "INVOICE")))
l_dblRateCardTotal = RoundToCurrency(CDbl(GetTotalCostsForJob(Val(txtJobID.Text), "ratecardtotal", "INVOICE")))

frmJob.txtTotal.Text = g_strCurrency & Format(l_dblTotal, "0.00#")
frmJob.txtTotalIncVAT.Text = g_strCurrency & Format(RoundToCurrency(CDbl(GetTotalCostsForJob(Val(txtJobID.Text), "totalincludingvat", "INVOICE"))), "0.00#")
frmJob.txtRateCardTotal.Text = g_strCurrency & Format(RoundToCurrency(l_dblRateCardTotal), "0.00#")
frmJob.txtTotalDiscountPercent.Text = GetPercentageDiscount(l_dblTotal, l_dblRateCardTotal)


End Sub

Private Sub chkEnforceContacts_Click()

If chkEnforceContacts.Value <> 0 Then
    grdJobContacts.Columns("email").DropDownHwnd = ddnCompanyContact.hWnd
Else
    grdJobContacts.Columns("email").DropDownHwnd = 0
End If

End Sub

Private Sub chkNoVAT_Click()

Dim l_sngVATRate  As Single
Dim l_lngJobID As Long
Dim l_strSQL As String

Me.MousePointer = vbHourglass

l_lngJobID = Val(txtJobID.Text)
If l_lngJobID = 0 Then
    'NoJobSelectedMessage
    Exit Sub
End If

If chkNoVAT.Value = vbChecked Then
    l_sngVATRate = 0
Else
    l_sngVATRate = GetVATRate(Format(GetData("company", "vatcode", "companyID", lblCompanyID.Caption)))
End If

SetData "job", "donotchargevat", "jobID", l_lngJobID, Val(chkNoVAT.Value)

l_strSQL = "UPDATE costing SET vat = ROUND((total * " & (l_sngVATRate / 100) & ")," & g_optRoundCurrencyDecimals & ") WHERE (jobID = '" & l_lngJobID & "');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_strSQL = "UPDATE costing SET totalincludingvat = vat + total WHERE (jobID = '" & l_lngJobID & "');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

adoCosting.Refresh

txtTotalIncVAT.Text = g_strCurrency & Format(GetTotalCostsForJob(l_lngJobID, "totalincludingvat", "invoice"), "###,###,###,0.00")

Me.MousePointer = vbDefault

End Sub

Private Sub chkQuote_Click()

If chkQuote.Value = 1 Then
    txtJobID.BackColor = vbLightGreen
    If Val(txtJobID.Text) <> 0 Then
        SendSMTPMail g_strQuotesEmailAddress, g_strQuotesEmailName, "Job No: " & txtJobID.Text & " has been made into a Quote", "", "Client: " & cmbCompany.Text & vbCrLf & _
        "Title: " & txtTitle.Text & vbCrLf, True, "", ""
    End If
Else
    If cmdSave.Enabled = True Then
        txtJobID.BackColor = vbLightBlue
    Else
        txtJobID.BackColor = vbLightRed
    End If
End If

End Sub

Public Sub cmbAllocation_Click()

If LCase(GetData("allocation", "allocationtype", "name", cmbAllocation.Text)) = "automatic" Then
    cmbProject.Enabled = False
    pnlDeal.Visible = True
Else
    cmbProject.Enabled = True
    pnlDeal.Visible = False
End If

End Sub

Private Sub cmbAllocation_DropDown()
PopulateCombo "allocation", cmbAllocation
End Sub

Private Sub cmbCollectionCompany_Click()

txtCollectionAddress.Text = cmbCollectionCompany.Columns("address").Text
txtCollectionPostCode.Text = cmbCollectionCompany.Columns("postcode").Text
txtCollectionCountry.Text = cmbCollectionCompany.Columns("country").Text
txtCollectionTelephone.Text = cmbCollectionCompany.Columns("telephone").Text
If chkUseCompany.Value = False Then
    cmbCollectionContact.Text = cmbCollectionCompany.Columns("attentionof").Text
End If

End Sub

Private Sub cmbCollectionCompany_DropDown()

Dim l_strSQL As String

If chkUseCompany.Value = False Then
    l_strSQL = "SELECT deliveryaddressID as theID, deliverycompanyname as name, address, postcode, country, attentionof, telephone FROM deliveryaddress ORDER BY deliverycompanyname"
Else
    l_strSQL = "SELECT companyID as theID, name, address, postcode, country, '' as attentionof, telephone FROM company ORDER BY name"
End If

adoDelAddress.ConnectionString = g_strConnection
adoDelAddress.RecordSource = l_strSQL
adoDelAddress.Refresh

cmbCollectionCompany.ReBind

End Sub

Private Sub cmbCollectionContact_DropDown()

Dim l_strSQL As String
l_strSQL = "SELECT contact.name FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((company.name) LIKE '" & cmbCollectionCompany.Text & "%')) ORDER BY contact.name;"
adoDelAddress.ConnectionString = g_strConnection
adoDelAddress.RecordSource = l_strSQL
adoDelAddress.Refresh

End Sub

Private Sub cmbCompany_CloseUp()

txtAccountCode.Text = cmbCompany.Columns("accountcode").Text
txtTelephone.Text = cmbCompany.Columns("telephone").Text
txtFax.Text = cmbCompany.Columns("fax").Text
lblCompanyID.Caption = cmbCompany.Columns("companyID").Text
txtClientStatus.Text = Trim(" " & cmbCompany.Columns("accountstatus").Text)

'get the credit limit
lblCaption(93).Caption = "Credit Limit " & g_strCurrency & Format(GetData("company", "creditlimit", "companyID", lblCompanyID.Caption), "#,###,##0.00")

DoCompanyMessages

'check for insurance details
If LCase(cmbJobType.Text) = "hire" Then IsInsuranceValidMessage Val(lblCompanyID.Caption)

If Val(txtJobID.Text) <> 0 Then

    'HIRE ISSUES!!! These things should only be done for HIRE jobs, but METRO need to do it for HIRE jobs too
    If (UCase(cmbJobType.Text) = "HIRE" Or UCase(cmbJobType.Text) = "EDIT") Then
    
        Dim l_intMsg As Integer
        l_intMsg = MsgBox("Do you want to replace the delivery/collection address details with the details from this company?" & vbCrLf & vbCrLf & "Please remember to update the contact name manually!!", vbQuestion + vbYesNo)
        
        If l_intMsg = vbYes Then
               
            cmbDeliveryCompany.Text = cmbCompany.Text
            cmbDeliveryContact.Text = cmbContact.Text
            txtDeliveryAddress.Text = GetData("company", "address", "companyID", lblCompanyID.Caption)
            txtDeliveryPostCode.Text = GetData("company", "postcode", "companyID", lblCompanyID.Caption)
            txtDeliveryCountry.Text = GetData("company", "country", "companyID", lblCompanyID.Caption)
            txtDeliveryTelephone.Text = GetData("company", "telephone", "companyID", lblCompanyID.Caption)
            cmdCopyToCollection_Click
        Else
            AddJobHistory Val(txtJobID.Text), "User declined to use company address as default del/col address"
        End If
            
    End If
    
    'Select and refresh the unbilled items grid
    adoUnbilled.ConnectionString = g_strConnection
    adoUnbilled.RecordSource = "SELECT * FROM jobdetailunbilled WHERE companyID = " & Val(lblCompanyID.Caption) & " ORDER BY jobdetailunbilledID;"
    adoUnbilled.Refresh

End If

Dim l_strGroupCode As String, l_strSQL As String
l_strGroupCode = GetData("company", "source", "companyID", Val(lblCompanyID.Caption))
If l_strGroupCode <> "" Then
    l_strSQL = "SELECT description, format FROM xref WHERE category = 'inclusive' AND descriptionalias = '" & l_strGroupCode & "' ORDER BY forder, format"
    adoInclusive.ConnectionString = g_strConnection
    adoInclusive.RecordSource = l_strSQL
    adoInclusive.Refresh
    
    ddnOthers.RemoveAll
    
    PopulateCombo "other", ddnOthers, " AND descriptionalias = '" & l_strGroupCode & "'"
    
    If l_strGroupCode = "DADC" Then
        txtDADCSeriesID.Visible = True
        txtDADCLegacyOracTVAENumber.Visible = True
        lblCaption(37).Visible = True
        lblCaption(38).Visible = True
    Else
        txtDADCSeriesID.Visible = False
        txtDADCLegacyOracTVAENumber.Visible = False
        lblCaption(37).Visible = False
        lblCaption(38).Visible = False
    End If
Else
    l_strSQL = "SELECT description, format FROM xref WHERE category = 'inclusive' AND (descriptionalias IS NULL OR descriptionalias = '" & l_strGroupCode & "') ORDER BY forder, format"
    adoInclusive.ConnectionString = g_strConnection
    adoInclusive.RecordSource = l_strSQL
    adoInclusive.Refresh
    
    ddnOthers.RemoveAll
    
    PopulateCombo "other", ddnOthers, " AND (descriptionalias IS NULL OR descriptionalias = '" & l_strGroupCode & "')"

    txtDADCSeriesID.Visible = False
    txtDADCLegacyOracTVAENumber.Visible = False
    lblCaption(37).Visible = False
    lblCaption(38).Visible = False
End If
    
If txtNotificationSubjectLine.Text = "" Then txtNotificationSubjectLine.Text = cmbCompany.Text & " Content Delivery"
    
End Sub

Private Sub DoCompanyMessages()

If UCase(txtClientStatus.Text) = "HOLD" Or UCase(txtClientStatus.Text) = "CASH" Then
    txtClientStatus.BackColor = vbRed
    MsgBox "This account is currently set to " & txtClientStatus.Text & ". You will not be able to confirm this job.", vbExclamation
Else
    txtClientStatus.BackColor = vbWindowBackground
End If

Dim l_strBookingsMessage As String
l_strBookingsMessage = GetData("company", "messagebookings", "companyID", lblCompanyID.Caption)

If LCase(l_strBookingsMessage) = "false" Then l_strBookingsMessage = ""

If l_strBookingsMessage <> "" Then MsgBox l_strBookingsMessage, vbInformation, "Bookings Alert"

l_strBookingsMessage = GetData("company", "messagecostings", "companyID", lblCompanyID.Caption)
If LCase(l_strBookingsMessage) = "false" Then l_strBookingsMessage = ""
If l_strBookingsMessage <> "" Then MsgBox l_strBookingsMessage, vbInformation, "Costings Alert"


End Sub

Private Sub cmbCompany_DropDown()

Dim l_strSQL As String
l_strSQL = "SELECT name, accountcode, telephone, companyID, fax, accountstatus, email FROM company WHERE name LIKE '" & QuoteSanitise(cmbCompany.Text) & "%' AND (iscustomer = 1 OR isprospective = 1) AND system_active = 1 ORDER BY name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmbContact_Click()

If cmbContact.Columns("telephone").Text <> "" Then txtTelephone.Text = cmbContact.Columns("telephone").Text

lblContactID.Caption = cmbContact.Columns("contactID").Text

End Sub

Private Sub cmbContact_DropDown()

If lblCompanyID.Caption = "" Then
    NoCompanySelectedMessage
    Exit Sub
End If

Dim l_strSQL As String

l_strSQL = "SELECT company.companyID, contact.contactID, contact.name, contact.telephone, employee.jobtitle, contact.email FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((company.companyID)=" & lblCompanyID.Caption & ")) AND contact.system_active = 1 ORDER BY contact.name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbContact.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmbDeadlineTime_DropDown()
PopulateCombo "times", cmbDeadlineTime
End Sub

Private Sub cmbDealType_Click()
If cmbDealType.Text = "Deal" Then
    txtDealPrice.Enabled = True
Else
    txtDealPrice.Enabled = False
    txtDealPrice.Text = ""
End If
End Sub

Private Sub cmbDealType_DropDown()
PopulateCombo "dealtype", cmbDealType
End Sub


Private Sub cmbDeliveryCompany_Click()

txtDeliveryAddress.Text = cmbDeliveryCompany.Columns("address").Text
txtDeliveryPostCode.Text = cmbDeliveryCompany.Columns("postcode").Text
txtDeliveryCountry.Text = cmbDeliveryCompany.Columns("country").Text
txtDeliveryTelephone.Text = cmbDeliveryCompany.Columns("telephone").Text

If chkUseCompany.Value = False Then
    cmbDeliveryContact.Text = cmbDeliveryCompany.Columns("attentionof").Text
End If


End Sub

Private Sub cmbDeliveryCompany_DropDown()

Dim l_strSQL As String

If chkUseCompany.Value = False Then
    l_strSQL = "SELECT deliveryaddressID as theID, deliverycompanyname as name, address, postcode, country, attentionof, telephone FROM deliveryaddress ORDER BY deliverycompanyname"
Else
    l_strSQL = "SELECT companyID as theID, name, address, postcode, country, '' as attentionof, telephone FROM company ORDER BY name"
End If

adoDelAddress.ConnectionString = g_strConnection
adoDelAddress.RecordSource = l_strSQL
adoDelAddress.Refresh

cmbDeliveryCompany.ReBind


End Sub

Private Sub cmbDeliveryContact_DropDown()
Dim l_strSQL As String
l_strSQL = "SELECT contact.name FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((company.name) LIKE '" & cmbDeliveryCompany.Text & "%')) ORDER BY contact.name;"

adoDelAddress.ConnectionString = g_strConnection
adoDelAddress.RecordSource = l_strSQL
adoDelAddress.Refresh

End Sub

Private Sub cmbIncomingMethod_DropDown()
PopulateCombo "despatchmethod", cmbIncomingMethod
End Sub

Private Sub cmbJobType_DropDown()
PopulateCombo "jobtype", cmbJobType
End Sub

Private Sub cmbLogEntryType_DropDown()
PopulateCombo "technicallogentrytype", cmbLogEntryType
End Sub

Private Sub cmbOutgoingMethod_DropDown()
PopulateCombo "despatchmethod", cmbOutgoingMethod
End Sub

Private Sub cmbProject_Click()

If cmbProject.Rows = 0 Then Exit Sub

lblProjectID.Caption = cmbProject.Columns("projectID").Text
txtTitle.Text = GetData("project", "subtitle", "projectID", Val(lblProjectID.Caption))


cmbCompany.Text = cmbProject.Columns("companyname").Text
cmbContact.Text = cmbProject.Columns("contactname").Text
cmbProduct.Text = cmbProject.Columns("productname").Text
lblCompanyID.Caption = cmbProject.Columns("companyID").Text
lblProductID.Caption = cmbProject.Columns("productID").Text
lblContactID.Caption = cmbProject.Columns("contactID").Text

txtAccountCode.Text = GetData("company", "accountcode", "companyID", lblCompanyID.Caption)
txtClientStatus.Text = GetData("company", "accountstatus", "companyID", lblCompanyID.Caption)

txtTelephone.Text = GetData("project", "telephone", "projectID", lblProjectID.Caption)
txtFax.Text = GetData("project", "fax", "projectID", lblProjectID.Caption)
    
cmbOurContact.Text = GetData("project", "ourcontact", "projectID", lblProjectID.Caption)
cmbVideoStandard.Text = GetData("project", "videostandard", "projectID", lblProjectID.Caption)

cmbAllocation.Text = GetData("project", "allocation", "projectID", lblProjectID.Caption)

DoCompanyMessages


End Sub

Private Sub cmbProject_DropDown()

If cmbProject.Tag = "STOP" Then Exit Sub

Dim l_strSQL As String
l_strSQL = "SELECT project.projectnumber, project.title, project.companyname, project.contactname, project.ourcontact, project.fd_status, product.name as productname, product.productID, project.companyID, project.contactID, project.projectID  FROM project LEFT JOIN product ON project.productID = product.productID WHERE (allocation = '" & cmbAllocation.Text & "') AND (projectnumber LIKE '" & QuoteSanitise(cmbProject.Text) & "%') AND (project.fd_status IS NULL OR (project.fd_status <> 'COMPLETED' AND project.fd_status <> 'CANCELLED' AND project.fd_status <> 'CANCEL' AND project.fd_status <> 'INVOICED' AND project.fd_status <> 'Sent To Accounts')) ORDER BY projectnumber;"
Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbProject.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Public Sub cmbJobType_Click()

On Error GoTo cmbJobType_Error

If cmbJobType.Text = "" Then cmbJobType.Text = g_strDefaultJobType

If Left(LCase(cmbJobType.Text), 3) = "dub" Then
    tab1.Tab = m_intTabJob
    tabReplacementTabs.Tab = GetReplacementTab(m_intTabJob)
    tabReplacementTabs.TabVisible(GetReplacementTab(m_intTabLibrary)) = True
    tabReplacementTabs.TabVisible(GetReplacementTab(m_intTabDespatch)) = False
ElseIf LCase(cmbJobType.Text) = "hire" Then
    tabReplacementTabs.TabVisible(GetReplacementTab(m_intTabJob)) = False
    tabReplacementTabs.TabVisible(GetReplacementTab(m_intTabLibrary)) = False
    tabReplacementTabs.TabVisible(GetReplacementTab(m_intTabDespatch)) = True
End If

Exit Sub

cmbJobType_Error:

If Err.Number = 380 Then
    tabReplacementTabs.TabVisible(GetReplacementTab(m_intTabJob)) = True
    
    Resume
End If

'    tab1.Tab = m_intTabSchedule
'    tabReplacementTabs.Tab = GetReplacementTab(m_intTabSchedule)
'End If
End Sub

Private Sub cmbProduct_Click()

lblProductID.Caption = cmbProduct.Columns("productID").Text

'check if projects are live for this product
If Val(lblProjectID.Caption) = 0 And IsProjectLiveForProduct(cmbProduct.Text) = True Then
    Dim l_intMsg As Integer
    l_intMsg = MsgBox("The product you have selected is in use on one or more live projects. Do you want to list them now?", vbQuestion + vbYesNo)
    If l_intMsg = vbYes Then
        
        cmbProduct.DroppedDown = False
            
        Dim l_strSQL As String
        l_strSQL = "SELECT project.projectnumber, project.title, project.companyname, project.contactname, project.fd_status, product.name as productname, project.ourcontact, product.productID, project.companyID, project.contactID, project.projectID  FROM project LEFT JOIN product ON project.productID = product.productID WHERE product.name LIKE '" & QuoteSanitise(cmbProduct.Text) & "%' AND project.fd_status <> 'COMPLETED' AND project.fd_status <> 'CANCEL' AND project.fd_status <> 'CANCELLED' AND project.fd_status <> 'INVOICED' AND project.fd_status <> 'SENT TO ACCOUNTS' ORDER BY title;"
        Dim l_conSearch As ADODB.Connection
        Dim l_rstSearch As ADODB.Recordset
        
        Set l_conSearch = New ADODB.Connection
        Set l_rstSearch = New ADODB.Recordset
        
        l_conSearch.ConnectionString = g_strConnection
        l_conSearch.Open
        
        With l_rstSearch
             .CursorLocation = adUseClient
             .LockType = adLockBatchOptimistic
             .CursorType = adOpenDynamic
             .Open l_strSQL, l_conSearch, adOpenDynamic
        End With
        
        l_rstSearch.ActiveConnection = Nothing
        
        Set cmbProject.DataSourceList = l_rstSearch
        
        cmbProject.Enabled = True

        l_conSearch.Close
        Set l_conSearch = Nothing
        
        'open the drop down
        'first stop it from populating with its own details
        cmbProject.Tag = "STOP"
        cmbProject.DroppedDown = True
        cmbProject.Tag = ""
    End If
End If

End Sub

Private Sub cmbProduct_DropDown()

Dim l_strSQL As String
l_strSQL = "SELECT name, productID FROM product WHERE name LIKE '" & QuoteSanitise(cmbProduct.Text) & "%' AND fd_status <> 'Cancelled' AND fd_status <> 'Hidden' AND fd_status <> 'Completed' ORDER BY name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbProduct.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing
End Sub

Private Sub cmbVideoStandard_DropDown()
PopulateCombo "videostandard", cmbVideoStandard
End Sub

Private Sub cmdAddDespatchNotes_Click()
If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If
InsertNote Val(txtJobID.Text), "notes3", txtInsertNotes.Text
txtDespatch.Text = GetData("job", "notes3", "jobID", Val(txtJobID.Text))
CheckClear
End Sub

Private Sub cmdAddLibraryEvent_Click(Index As Integer)
Select Case Index
Case 0
    
Case 1
    
End Select
Select Case g_optSearchLibraryDefaultField
Case "Product"
    frmSearchLibrary.cmbProduct.Text = cmbProduct.Text
Case "Title"
    frmSearchLibrary.txtTitle.Text = txtTitle.Text
Case "Client"
    frmSearchLibrary.cmbCompany.Text = cmbCompany.Text
Case Else
    
End Select

frmSearchLibrary.cmdSearch.Value = True
frmSearchLibrary.WindowState = vbMaximized


End Sub

Private Sub cmdAddMaster_Click()

frmAddMasters.cmbProduct.Text = cmbProduct.Text
frmAddMasters.txtTitle.Text = Left(txtTitle.Text, 3)
frmAddMasters.cmdRefresh.Value = True

frmAddMasters.Show vbModal
Unload frmAddMasters
Set frmAddMasters = Nothing

adoJobDetail.Refresh


End Sub

Private Sub cmdAddOrderReference_Click()
If Not CheckAccess("/updateorderreference") Then Exit Sub

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

Dim l_strInputOrder As String

l_strInputOrder = InputBox("Please enter the new order referenece", "Update Order Reference", txtOrderReference.Text)

If l_strInputOrder <> txtOrderReference.Text And l_strInputOrder <> "" Then
    SetData "job", "orderreference", "jobID", txtJobID.Text, UCase(l_strInputOrder)
    AddJobHistory txtJobID.Text, "Updated Order Reference from " & txtOrderReference.Text & " to " & UCase(l_strInputOrder)
    txtOrderReference.Text = UCase(l_strInputOrder)
    
    RefreshJobHistory txtJobID.Text
End If

End Sub

Private Sub cmdAddProducerNotes_Click()

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

InsertNote Val(txtJobID.Text), "notes5", txtInsertNotes.Text
txtProducerNotes.Text = GetData("job", "notes5", "jobID", Val(txtJobID.Text))
CheckClear

End Sub


Private Sub cmdApplyDeal_Click()

If Not CheckAccess("/applydealpricetocostings") Then Exit Sub

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

If UCase(cmbDealType.Text) <> "DEAL" Then
    MsgBox "This function only works for jobs with a deal price", vbExclamation
    Exit Sub
End If

SetData "job", "dealprice", "jobID", txtJobID.Text, txtDealPrice.Text
SetData "job", "dealtype", "jobID", txtJobID.Text, cmbDealType.Text

ApplyDealPriceToCostings Val(txtJobID.Text)

'ERROR RESUME NEXT!!!
On Error Resume Next

If adoCosting.RecordSource <> "" Then adoCosting.Refresh

DoEvents

Dim l_dblTotal As Double
Dim l_dblRateCardTotal As Double

l_dblTotal = RoundToCurrency(CDbl(GetTotalCostsForJob(Val(txtJobID.Text), "total", "INVOICE")))
l_dblRateCardTotal = RoundToCurrency(CDbl(GetTotalCostsForJob(Val(txtJobID.Text), "ratecardtotal", "INVOICE")))

frmJob.txtTotal.Text = g_strCurrency & Format(l_dblTotal, "0.00#")
frmJob.txtTotalIncVAT.Text = g_strCurrency & Format(RoundToCurrency(CDbl(GetTotalCostsForJob(Val(txtJobID.Text), "totalincludingvat", "INVOICE"))), "0.00#")
frmJob.txtRateCardTotal.Text = g_strCurrency & Format(RoundToCurrency(l_dblRateCardTotal), "0.00#")
frmJob.txtTotalDiscountPercent.Text = GetPercentageDiscount(l_dblRateCardTotal, l_dblTotal)

tab1.Tab = m_intTabCosting
tabReplacementTabs.Tab = GetReplacementTab(tab1.Tab)

End Sub

Private Sub cmdApprove_Click()

If grdSundryCosts.Rows = 0 Then Exit Sub

If CheckAccess("/approvesundry") Then
    ApproveSundry grdSundryCosts.Columns("sundrycostID").Text
    adoSundry.Refresh
End If

End Sub

Private Sub cmdAssignSelectedUnbilledItems_Click()

Dim l_lngRunningTime As Long, l_lngQuantity As Long, l_lngChargeRealMinutes As Long

adoUnbilled.Recordset.MoveFirst

Do While Not adoUnbilled.Recordset.EOF
    
    If adoUnbilled.Recordset("SelectedForBilling") <> 0 Then
        If Not IsNull(adoUnbilled.Recordset.Fields("runningtime").Value) Then
            l_lngRunningTime = adoUnbilled.Recordset.Fields("runningtime").Value
        Else
            l_lngRunningTime = 0
        End If
            
        l_lngQuantity = Val(adoUnbilled.Recordset.Fields("quantity").Value)
        l_lngChargeRealMinutes = 0
        
        MakeJobDetailLine Val(txtJobID.Text), adoUnbilled.Recordset.Fields("copytype").Value, adoUnbilled.Recordset.Fields("description").Value, _
            l_lngQuantity, adoUnbilled.Recordset.Fields("format").Value, l_lngRunningTime, _
            adoUnbilled.Recordset.Fields("completeddate").Value, adoUnbilled.Recordset.Fields("completeduser").Value, adoJobDetail.Recordset.RecordCount, _
            l_lngChargeRealMinutes, "", 0, "", 0, "", adoUnbilled.Recordset("portaluserID"), adoUnbilled.Recordset("skibblycustomerID"), adoUnbilled.Recordset("clipID")
    End If
    adoUnbilled.Recordset.MoveNext
Loop

adoUnbilled.Recordset.MoveFirst

Do While Not adoUnbilled.Recordset.EOF

    If adoUnbilled.Recordset("SelectedForBilling") <> 0 Then adoUnbilled.Recordset.Delete
    On Error GoTo LAST_ITEM
    adoUnbilled.Recordset.MoveNext
    
Loop

LAST_ITEM:

On Error GoTo 0

adoJobDetail.Refresh
adoUnbilled.Refresh

End Sub

Private Sub cmdAssignUnbilledItem_Click()

Dim l_lngRunningTime As Long, l_lngQuantity As Long, l_lngChargeRealMinutes As Long

If Not adoUnbilled.Recordset.EOF Then

    If Not IsNull(adoUnbilled.Recordset.Fields("runningtime").Value) Then
        l_lngRunningTime = adoUnbilled.Recordset.Fields("runningtime").Value
    Else
        l_lngRunningTime = 0
    End If
        
    l_lngQuantity = Val(adoUnbilled.Recordset.Fields("quantity").Value)
    l_lngChargeRealMinutes = 0
    
    MakeJobDetailLine Val(txtJobID.Text), adoUnbilled.Recordset.Fields("copytype").Value, adoUnbilled.Recordset.Fields("description").Value, _
        l_lngQuantity, adoUnbilled.Recordset.Fields("format").Value, l_lngRunningTime, _
        adoUnbilled.Recordset.Fields("completeddate").Value, adoUnbilled.Recordset.Fields("completeduser").Value, adoJobDetail.Recordset.RecordCount, _
        l_lngChargeRealMinutes, "", 0, "", 0, "", adoUnbilled.Recordset("portaluserID"), adoUnbilled.Recordset("skibblycustomerID"), adoUnbilled.Recordset("clipID")
        
    adoUnbilled.Recordset.Delete
    adoJobDetail.Refresh
    adoUnbilled.Refresh

End If

End Sub

Private Sub cmdAuthoriseSundry_Click()
If grdSundryCosts.Rows = 0 Then Exit Sub

If CheckAccess("/authorisesundry") Then
    AuthoriseSundry grdSundryCosts.Columns("sundrycostID").Text
    adoSundry.Refresh
End If
End Sub

Private Sub cmdBillProgressItem_Click()

'To Bill a progress item, the various optional costs need to be confirmed, and then job detail lines added using'
'MakeJobDetailLine for each cost element. Finally the progressitem needs to have its billed flag set to -1

Select Case Val(lblCompanyID.Caption)

Case 0
    'Nothing
    
Case 833
    'Wellcome
    'If the original was film: Make row for the transfer to tape, and any cleaning/treatment processes
    If grdProgressItems.Columns("format").Text = "FILM" Then
        If grdProgressItems.Columns("hd").Text = "-1" Then
            MakeJobDetailLine Val(txtJobID.Text), "M", QuoteSanitise(grdProgressItems.Columns("title").Text) & " - Film to HDCAM-SR", 1, "WEFILMSR", _
                grdProgressItems.Columns("chargedrunningtime").Text, Now, "", adoJobDetail.Recordset.RecordCount, 0
            adoJobDetail.Refresh
        Else
            MakeJobDetailLine Val(txtJobID.Text), "M", QuoteSanitise(grdProgressItems.Columns("title").Text) & " - Film to DigiBeta", 1, "WEFILMDB", _
                grdProgressItems.Columns("chargedrunningtime").Text, Now, "", adoJobDetail.Recordset.RecordCount, 0
            adoJobDetail.Refresh
        End If
        If grdProgressItems.Columns("filmtreatment").Text = "-1" Then
            MakeJobDetailLine Val(txtJobID.Text), "A", QuoteSanitise(grdProgressItems.Columns("title").Text) & " - Film Repairs", 1, "WEFILMRE", _
                grdProgressItems.Columns("chargedrunningtime").Text, Now, "", adoJobDetail.Recordset.RecordCount, 0
            adoJobDetail.Refresh
        End If
        If grdProgressItems.Columns("filmwash").Text = "-1" Then
            MakeJobDetailLine Val(txtJobID.Text), "A", QuoteSanitise(grdProgressItems.Columns("title").Text) & " - Film Rewash", 1, "WEFILMWA", _
                grdProgressItems.Columns("chargedrunningtime").Text, Now, "", adoJobDetail.Recordset.RecordCount, 0
            adoJobDetail.Refresh
        End If
    'Otherwise, if it wasn't a DIGI, then one needs making
    ElseIf grdProgressItems.Columns("format").Text <> "DBETA" Then
        MakeJobDetailLine Val(txtJobID.Text), "M", QuoteSanitise(grdProgressItems.Columns("title").Text) & " - VT to DigiBeta", 1, "WEVTDB", _
            grdProgressItems.Columns("chargedrunningtime").Text, Now, "", adoJobDetail.Recordset.RecordCount, 0
        adoJobDetail.Refresh
    End If
    
    'Do the Encode line
    
    MakeJobDetailLine Val(txtJobID.Text), "M", QuoteSanitise(grdProgressItems.Columns("title").Text) & " - Initial Ingest", 1, "WEMEDHR", _
        grdProgressItems.Columns("chargedrunningtime").Text, Now, "", adoJobDetail.Recordset.RecordCount, 0
    adoJobDetail.Refresh
    
    'If there was audio restotation, or video restoration then make the lines
    If grdProgressItems.Columns("audioprocess").Text = "-1" Then
        MakeJobDetailLine Val(txtJobID.Text), "A", QuoteSanitise(grdProgressItems.Columns("title").Text) & " - Audio Restoration", 1, "WEAUD", _
            grdProgressItems.Columns("chargedrunningtime").Text, Now, "", adoJobDetail.Recordset.RecordCount, 0
        adoJobDetail.Refresh
    End If
    If grdProgressItems.Columns("videorestore").Text = "-1" Then
        MakeJobDetailLine Val(txtJobID.Text), "A", QuoteSanitise(grdProgressItems.Columns("title").Text) & " - Video Rstoration", 1, "WEREST", _
            grdProgressItems.Columns("chargedrunningtime").Text, Now, "", adoJobDetail.Recordset.RecordCount, 0
        adoJobDetail.Refresh
    End If

    'Do the Transcode/Deliver line
    MakeJobDetailLine Val(txtJobID.Text), "A", QuoteSanitise(grdProgressItems.Columns("title").Text) & " - Segmentation & Transcode", 1, "WETRAN-B", _
        grdProgressItems.Columns("chargedrunningtime").Text, Now, "", adoJobDetail.Recordset.RecordCount, 0
    adoJobDetail.Refresh
    grdProgressItems.Columns("billed").Value = -1
    grdProgressItems.Update
    adoProgress.Refresh

Case Else
    'Nothing Yet

End Select

End Sub

Private Sub cmdChargeAllItems_Click()

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

If cmdChargeAllItems.Caption = "Invoice All" Then
    SetData "jobdetail", "chargethisitem", "jobID", Val(txtJobID.Text), -1
    cmdChargeAllItems.Caption = "Invoice None"
    adoJobDetail.Refresh
Else
    SetData "jobdetail", "chargethisitem", "jobID", Val(txtJobID.Text), 0
    cmdChargeAllItems.Caption = "Invoice All"
    adoJobDetail.Refresh
End If

End Sub

Private Sub cmdCollect_Click()

UpdateSundryCostings Val(txtJobID.Text), lblCompanyID.Caption
adoCosting.Refresh
picNewSundries.Visible = False

End Sub

Private Sub cmdCompleteAllRows_Click()

Dim l_lngJobID As Long

l_lngJobID = Val(lblJobID.Caption)

If l_lngJobID = 0 Then Exit Sub

If MsgBox("Are you sure", vbYesNo, "Completing all Job Lines") = vbYes Then

    adoJobDetail.Recordset.MoveFirst
    Do While Not adoJobDetail.Recordset.EOF
        If Trim(" " & adoJobDetail.Recordset("completeddate")) = "" And Trim(" " & adoJobDetail.Recordset("rejecteddate")) = "" Then
            adoJobDetail.Recordset("completeddate") = Now
            adoJobDetail.Recordset("completeduser") = g_strUserInitials
        End If
        adoJobDetail.Recordset.MoveNext
    Loop
    
    ShowJob l_lngJobID, 1, False
'    ShowJob l_lngJobID, 1, False

End If

End Sub

Private Sub cmdConfirmWebOrder_Click()

If Val(txtJobID.Text) = 0 Then Exit Sub
If txtStatus.Text <> "Submitted" Then
    MsgBox "You can only confirm web orders that are currently in the 'Submitted' status", vbCritical, "Error"
    Exit Sub
End If

UpdateJobStatus Val(txtJobID.Text), "Confirmed"
ShowJob Val(txtJobID.Text), 1, False
'ShowJob Val(txtJobID.Text), 1, False

If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/makeorders") > 0 Then
    Dim email_message As String, lib As ADODB.Recordset, clipstore As String, l_strSQL As String, client_email As String
    
    email_message = "<!DOCTYPE HTML PUBLIC""-//IETF//DTD HTML//EN"">" & vbCrLf
    email_message = email_message & "<html>" & vbCrLf
    email_message = email_message & "<head>" & vbCrLf
    email_message = email_message & "<title>Web Order Confirmed Job No:" & txtJobID.Text & " Title: " & txtTitle.Text & "</title>" & vbCrLf
    email_message = email_message & "</head>" & vbCrLf
    email_message = email_message & "<body bgcolor=""FFFFFF"">" & vbCrLf
    email_message = email_message & "<p>This order has been accepted by RR Media and will proceed for work</p>" & vbCrLf
    email_message = email_message & "<p>Contact: " & cmbContact.Text & ", Company: " & cmbCompany.Text & "</p>" & vbCrLf
    email_message = email_message & "<p>Job Number: " & txtJobID.Text & " Title: " & txtTitle.Text & "</p>" & vbCrLf
    email_message = email_message & "<p>Deadline: " & Format(datDeadlineDate.Value, "DD mmm YY") & "</p>" & vbCrLf
    If chkDoNotDeliver.Value <> 0 Then email_message = email_message & "<p>Delivery: Not to be Delivered yet</p>" & vbCrLf
    email_message = email_message & "<p>Title: " & txtTitle.Text & "</p>" & vbCrLf
    email_message = email_message & "<p>Requirements: " & txtOperator.Text & "</p>" & vbCrLf
    email_message = email_message & "<p>Delivery Instructions: " & txtDespatch.Text & "</p>" & vbCrLf
    
    If adoJobDetail.Recordset.RecordCount > 0 Then
    
        adoJobDetail.Recordset.MoveFirst
        email_message = email_message & "<p>Workflow Details:</p><p><table cellspacing=""1"" cellpadding=""5"" border=""1"">" & vbCrLf
        email_message = email_message & "<tr>" & vbCrLf
        email_message = email_message & "<th>Type</th>" & vbCrLf
        email_message = email_message & "<th>Title</th>" & vbCrLf
        email_message = email_message & "<th>Qty</th>" & vbCrLf
        email_message = email_message & "<th>Format</th>" & vbCrLf
        email_message = email_message & "<th>Standard</th>" & vbCrLf
        email_message = email_message & "<th>Aspect</th>" & vbCrLf
        email_message = email_message & "<th>R/T</th>" & vbCrLf
        email_message = email_message & "</tr>" & vbCrLf
        
        Do While Not adoJobDetail.Recordset.EOF
        
            email_message = email_message & "<tr>" & vbCrLf
            email_message = email_message & "<td>" & Trim(" " & adoJobDetail.Recordset("copytype")) & "<br></td>" & vbCrLf
            email_message = email_message & "<td>"
            If Trim(" " & adoJobDetail.Recordset("description")) <> "" Then email_message = email_message & adoJobDetail.Recordset("description")
            If Trim(" " & adoJobDetail.Recordset("workflowdescription")) <> "" Then email_message = email_message & " - " & adoJobDetail.Recordset("workflowdescription")
            email_message = email_message & "<br></td>" & vbCrLf
            email_message = email_message & "<td>" & Trim(" " & adoJobDetail.Recordset("quantity")) & "<br></td>" & vbCrLf
            email_message = email_message & "<td>" & Trim(" " & adoJobDetail.Recordset("format")) & "<br></td>" & vbCrLf
            email_message = email_message & "<td>" & Trim(" " & adoJobDetail.Recordset("videostandard")) & "<br></td>" & vbCrLf
            email_message = email_message & "<td>" & Trim(" " & adoJobDetail.Recordset("aspectratio")) & "<br></td>" & vbCrLf
            email_message = email_message & "<td>" & Trim(" " & adoJobDetail.Recordset("runningtime")) & "<br></td>" & vbCrLf
            email_message = email_message & "</tr>" & vbCrLf
            adoJobDetail.Recordset.MoveNext
        Loop
        
        email_message = email_message & "</table></p>" & vbCrLf
        
    End If
    
    'display the required tapes
    l_strSQL = "SELECT library.*, requiredmedia.requiredmediaID FROM library INNER JOIN requiredmedia ON library.libraryID = requiredmedia.libraryID WHERE requiredmedia.jobid = " & txtJobID.Text & ";"
    adoMedia(1).ConnectionString = g_strConnection
    adoMedia(1).RecordSource = l_strSQL
    adoMedia(1).Refresh
    
    'display the required files
    l_strSQL = "SELECT requiredmedia.eventID, events.* FROM requiredmedia INNER JOIN events ON requiredmedia.eventID = events.eventID WHERE requiredmedia.jobid = " & txtJobID.Text & ";"
    adoMedia(4).ConnectionString = g_strConnection
    adoMedia(4).RecordSource = l_strSQL
    adoMedia(4).Refresh
    
    If adoMedia(1).Recordset.RecordCount > 0 Then
        adoMedia(1).Recordset.MoveFirst
        email_message = email_message & "<p>Required Tape Master Material:</p><p><table cellspacing=""1"" cellpadding=""5"" border=""1"">" & vbCrLf
        email_message = email_message & "<tr>" & vbCrLf
        email_message = email_message & "<th>Type</th>" & vbCrLf
        email_message = email_message & "<th>Barcode / FileID</th>" & vbCrLf
        email_message = email_message & "<th>Title</th>" & vbCrLf
        email_message = email_message & "<th>Format</th>" & vbCrLf
        email_message = email_message & "<th>Standard</th>" & vbCrLf
        email_message = email_message & "<th>Aspect</th>" & vbCrLf
        email_message = email_message & "<th>R/T</th>" & vbCrLf
        email_message = email_message & "<th>Location</th>" & vbCrLf
        email_message = email_message & "</tr>" & vbCrLf
        
        Do While Not adoMedia(1).Recordset.EOF
            email_message = email_message & "<tr>" & vbCrLf
            email_message = email_message & "<td>" & vbCrLf
            email_message = email_message & "Tape"
            email_message = email_message & "<br></td>" & vbCrLf
            
            If Not IsNull(adoMedia(1).Recordset("libraryID")) Then
            
                Set lib = ExecuteSQL("SELECT * FROM library WHERE libraryID=" & adoMedia(1).Recordset("libraryID") & ";", g_strExecuteError)
                CheckForSQLError
                If lib.RecordCount > 0 Then
                    lib.MoveFirst
                    email_message = email_message & "<td>" & lib("barcode") & "</td>" & vbCrLf
                    email_message = email_message & "<td>"
                    If Trim(" " & lib("title")) <> "" Then email_message = email_message & lib("title") & " "
                    If Trim(" " & lib("series")) <> "" Then email_message = email_message & "Sr. " & lib("series") & " "
                    If Trim(" " & lib("episode")) <> "" Then
                        email_message = email_message & "Ep. " & lib("episode")
                        If Trim(" " & lib("endepisode")) <> "" Then
                            email_message = email_message & "-" & lib("endepisode") & " "
                        Else
                            email_message = email_message & " "
                        End If
                    End If
                    If Trim(" " & lib("subtitle")) <> "" Then email_message = email_message & ", " & lib("subtitle") & " "
                    email_message = email_message & "<br></td>" & vbCrLf
                    email_message = email_message & "<td>" & Trim(" " & lib("format")) & "<br></td>" & vbCrLf
                    email_message = email_message & "<td>" & Trim(" " & lib("videostandard")) & "<br></td>" & vbCrLf
                    email_message = email_message & "<td>" & Trim(" " & lib("aspectratio") & " " & lib("geometriclinearity")) & "<br></td>" & vbCrLf
                    email_message = email_message & "<td>" & Trim(" " & lib("totalduration")) & "<br></td>" & vbCrLf
                    email_message = email_message & "<td>" & Trim(" " & lib("location")) & "<br></td>" & vbCrLf
                End If
                lib.Close
                Set lib = Nothing
            
            End If
            
            email_message = email_message & "</tr>" & vbCrLf
            adoMedia(1).Recordset.MoveNext
        Loop
        email_message = email_message & "</table></p>" & vbCrLf
        
    End If
    
    If adoMedia(4).Recordset.RecordCount > 0 Then
        adoMedia(4).Recordset.MoveFirst
        email_message = email_message & "<p>Required File Master Material:</p><p><table cellspacing=""1"" cellpadding=""5"" border=""1"">" & vbCrLf
        email_message = email_message & "<tr>" & vbCrLf
        email_message = email_message & "<th>Type</th>" & vbCrLf
        email_message = email_message & "<th>Barcode / FileID</th>" & vbCrLf
        email_message = email_message & "<th>Title</th>" & vbCrLf
        email_message = email_message & "<th>Format</th>" & vbCrLf
        email_message = email_message & "<th>Standard</th>" & vbCrLf
        email_message = email_message & "<th>Aspect</th>" & vbCrLf
        email_message = email_message & "<th>R/T</th>" & vbCrLf
        email_message = email_message & "<th>Location</th>" & vbCrLf
        email_message = email_message & "</tr>" & vbCrLf
        
        Do While Not adoMedia(4).Recordset.EOF
            email_message = email_message & "<tr>" & vbCrLf
            email_message = email_message & "<td>" & vbCrLf
            email_message = email_message & "File"
            email_message = email_message & "<br></td>" & vbCrLf
            
            If Not IsNull(adoMedia(4).Recordset("eventID")) Then
            
                Set lib = ExecuteSQL("SELECT * FROM events WHERE eventID=" & adoMedia(4).Recordset("eventID") & ";", g_strExecuteError)
                CheckForSQLError
                If lib.RecordCount > 0 Then
                    lib.MoveFirst
                    email_message = email_message & "<td>" & lib("eventID") & "</td>" & vbCrLf
                    email_message = email_message & "<td>"
                    If Trim(" " & lib("eventtitle")) <> "" Then email_message = email_message & lib("eventtitle") & " "
                    If Trim(" " & lib("eventseries")) <> "" Then email_message = email_message & "Sr. " & lib("eventseries") & " "
                    If Trim(" " & lib("eventepisode")) <> "" Then email_message = email_message & "Ep. " & lib("eventepisode")
                    If Trim(" " & lib("eventsubtitle")) <> "" Then email_message = email_message & ", " & lib("eventsubtitle") & " "
                    email_message = email_message & "<br></td>" & vbCrLf
                    email_message = email_message & "<td>"
                    If lib("clipformat") = "MOV ProRes" Then
                        email_message = email_message & "MOV "
                    Else
                        email_message = email_message & Trim(" " & lib("clipformat")) & " "
                    End If
                    email_message = email_message & Trim(" " & lib("clipcodec"))
                    email_message = email_message & "<br></td>" & vbCrLf
                    email_message = email_message & "<td>" & Trim(" " & lib("clipframerate")) & " fps<br></td>" & vbCrLf
                    email_message = email_message & "<td>" & Trim(" " & lib("aspectratio") & " " & lib("geometriclinearity")) & "<br></td>" & vbCrLf
                    email_message = email_message & "<td>" & Trim(" " & lib("fd_length")) & "<br></td>" & vbCrLf
                    clipstore = GetDataSQL("SELECT TOP 1 barcode from library where libraryID = " & lib("libraryID") & ";")
                    email_message = email_message & "<td>" & clipstore & "<br></td>" & vbCrLf
                End If
                lib.Close
                Set lib = Nothing
            
            End If
    
            email_message = email_message & "</tr>" & vbCrLf
            adoMedia(4).Recordset.MoveNext
        Loop
        email_message = email_message & "</table></p>" & vbCrLf
        
    End If
    
    email_message = email_message & "</body>" & vbCrLf
    email_message = email_message & "</html>" & vbCrLf
    
    client_email = Trim(" " & GetData("company", "email", "companyID", lblCompanyID.Caption))
    SendOutlookEmail GetData("contact", "email", "contactID", lblContactID.Caption), "Web Order Confirmed Job No:" & txtJobID.Text & " Title: " & txtTitle.Text, email_message, "", client_email, g_strAdministratorEmailAddress, False

End If

End Sub

Private Sub cmdCopyCostsFrom_Click()

If Not CheckAccess("/copycostinglines") Then Exit Sub

Dim l_lngJobID As Long
l_lngJobID = Val(InputBox("Please enter the job ID of the costings you want to copy"))
If l_lngJobID = 0 Then Exit Sub

If RecordExists("job", "jobID", l_lngJobID) Then
    Dim l_intMsg As Integer
    l_intMsg = MsgBox("Are you sure you want to copy the costs from job ID " & l_lngJobID & " to this job?", vbQuestion + vbYesNo)
    If l_intMsg = vbYes Then
        CopyCostingLines l_lngJobID, Val(txtJobID.Text), ""
        adoCosting.Refresh
    End If
Else
    MsgBox "The job ID entered does not exist", vbExclamation
End If


End Sub

Private Sub cmdCopyToCollection_Click()

'Dim l_intMsg As Integer
'l_intMsg = MsgBox("Are you sure you want to copy the delivery address to the collection address?", vbQuestion + vbYesNo)

'If l_intMsg = vbNo Then Exit Sub

cmbCollectionCompany.Text = cmbDeliveryCompany.Text
cmbCollectionContact.Text = cmbDeliveryContact.Text
txtCollectionAddress.Text = txtDeliveryAddress.Text
txtCollectionPostCode.Text = txtDeliveryPostCode.Text
txtCollectionCountry.Text = txtDeliveryCountry.Text
txtCollectionTelephone.Text = txtDeliveryTelephone.Text



End Sub

Private Sub cmdDuplicateJob_Click()

Dim l_lngOldJobID As Long, l_rstOldDetails As ADODB.Recordset, l_rstNewDetails As ADODB.Recordset, l_lngCount As Long

If Val(txtJobID.Text) <> 0 Then
    
    l_lngOldJobID = Val(txtJobID.Text)
    
    txtJobID.Text = ""
    SaveJob
    
    SetData "job", "invoiceddate", "jobID", Val(txtJobID.Text), Null
    SetData "job", "invoicenumber", "jobID", Val(txtJobID.Text), Null
    SetData "job", "invoiceduser", "jobID", Val(txtJobID.Text), Null
    SetData "job", "invoiceduserID", "jobID", Val(txtJobID.Text), Null
    SetData "job", "senttoaccountsbatchnumber", "jobID", Val(txtJobID.Text), Null
    SetData "job", "senttoaccountsdate", "jobID", Val(txtJobID.Text), Null
    SetData "job", "senttoaccountsuser", "jobID", Val(txtJobID.Text), Null
    SetData "job", "senttoaccountsuserID", "jobID", Val(txtJobID.Text), Null
    SetData "job", "creditnotenumber", "jobID", Val(txtJobID.Text), Null
    SetData "job", "creditnotebatchnumber", "jobID", Val(txtJobID.Text), Null
    SetData "job", "creditnotedate", "jobID", Val(txtJobID.Text), Null
    SetData "job", "creditnoteuser", "jobID", Val(txtJobID.Text), Null
    SetData "job", "creditnoteuserID", "jobID", Val(txtJobID.Text), Null
    SetData "job", "creditsenttoaccountsdate", "jobID", Val(txtJobID.Text), Null
    SetData "job", "creditsenttoaccountsuser", "jobID", Val(txtJobID.Text), Null
    SetData "job", "refundedcreditnotenumber", "jobID", Val(txtJobID.Text), Null
    SetData "job", "senttoBBCdate", "jobID", Val(txtJobID.Text), Null
    SetData "job", "senttoBBCbatchnumber", "jobID", Val(txtJobID.Text), Null
    SetData "job", "creditsenttoBBCdate", "jobID", Val(txtJobID.Text), Null
    SetData "job", "creditsenttoBBCbatchnumber", "jobID", Val(txtJobID.Text), Null
    SetData "job", "DeadlineDate", "jobID", Val(txtJobID), FormatSQLDate(Now)
    SetData "job", "EmailNotificationSubjectLine", "jobID", Val(txtJobID.Text), Null
    SetData "job", "EmailNotificationMessageBody", "jobID", Val(txtJobID.Text), Null
    
    SetData "job", "notes1", "jobID", Val(txtJobID.Text), txtOfficeClient.Text
    SetData "job", "notes2", "jobID", Val(txtJobID.Text), txtOperator.Text
    SetData "job", "notes3", "jobID", Val(txtJobID.Text), txtDespatch.Text
    SetData "job", "notes5", "jobID", Val(txtJobID.Text), txtProducerNotes.Text
                
    Set l_rstOldDetails = ExecuteSQL("SELECT * from jobdetail WHERE jobid = " & l_lngOldJobID, g_strExecuteError)
    CheckForSQLError
        
    If l_rstOldDetails.RecordCount > 0 Then
        Set l_rstNewDetails = ExecuteSQL("SELECT * FROM jobdetail WHERE jobid = -1", g_strExecuteError)
        CheckForSQLError
        
        l_rstOldDetails.MoveFirst
        Do While Not l_rstOldDetails.EOF
            l_rstNewDetails.AddNew
            For l_lngCount = 0 To l_rstOldDetails.Fields.Count - 1
            
                Select Case l_rstNewDetails.Fields(l_lngCount).Name
                
                    Case "jobID"
                        l_rstNewDetails.Fields(l_lngCount).Value = Val(txtJobID.Text)
                        
                    Case "jobdetailID", "completeddate", "completeduser", "rejecteddate", "rejecteduser", "CopyLibraryID", "CopyBarcode", "CopyEventID", "CopyClipfilename", "DateMasterArrived", _
                        "DateCopyMade", "DateDelivered", "TargetDate", "TargetDateCountdown", "FirstCompleteable", "OffRejectedDate", "DecisionTree", "DecisionTreeDate", "OffDecisionTreeDate", _
                        "FirstRejectedDate", "DaysOnDecisionTree", "MostRecentDecisionTreeDate", "DateTapeSentBack", "DaysOnRejected", "CopyMadeBy", "WaybillNumber", "complainedabout", "VTStartDate"
                        'Nothing
                    
                    Case Else
                        l_rstNewDetails.Fields(l_lngCount).Value = l_rstOldDetails.Fields(l_lngCount).Value
                        
                End Select
                
            Next
            l_rstNewDetails.Update
            l_rstOldDetails.MoveNext
        Loop
        l_rstNewDetails.Close
        Set l_rstNewDetails = Nothing
    End If
        
    l_rstOldDetails.Close
    
    If MsgBox("Also duplicate the costings lines" & vbCrLf & "(Answer no to this unless you are aware of the implications)?", vbYesNo + vbDefaultButton2) = vbYes Then
        Set l_rstOldDetails = ExecuteSQL("SELECT * from costing WHERE jobid = " & l_lngOldJobID, g_strExecuteError)
        CheckForSQLError
            
        If l_rstOldDetails.RecordCount > 0 Then
            Set l_rstNewDetails = ExecuteSQL("SELECT * FROM costing WHERE jobid = -1", g_strExecuteError)
            CheckForSQLError
            
            l_rstOldDetails.MoveFirst
            Do While Not l_rstOldDetails.EOF
                l_rstNewDetails.AddNew
                For l_lngCount = 0 To l_rstOldDetails.Fields.Count - 1
                
                    Select Case l_rstNewDetails.Fields(l_lngCount).Name
                    
                        Case "jobID"
                            l_rstNewDetails.Fields(l_lngCount).Value = Val(txtJobID.Text)
                            
                        Case "jobdetailID", "costingID"
                            'Nothing
                        
                        Case Else
                            l_rstNewDetails.Fields(l_lngCount).Value = l_rstOldDetails.Fields(l_lngCount).Value
                            
                    End Select
                    
                Next
                l_rstNewDetails.Update
                l_rstOldDetails.MoveNext
            Loop
            l_rstNewDetails.Close
            Set l_rstNewDetails = Nothing
        End If
    
    End If
    Set l_rstOldDetails = Nothing
    
    If MsgBox("Also duplicate the Delivery Contact lines", vbYesNo + vbDefaultButton2) = vbYes Then
        Set l_rstOldDetails = ExecuteSQL("SELECT * from jobcontact WHERE jobid = " & l_lngOldJobID, g_strExecuteError)
        CheckForSQLError
            
        If l_rstOldDetails.RecordCount > 0 Then
            Set l_rstNewDetails = ExecuteSQL("SELECT * FROM jobcontact WHERE jobid = -1", g_strExecuteError)
            CheckForSQLError
            
            l_rstOldDetails.MoveFirst
            Do While Not l_rstOldDetails.EOF
                l_rstNewDetails.AddNew
                For l_lngCount = 0 To l_rstOldDetails.Fields.Count - 1
                
                    Select Case l_rstNewDetails.Fields(l_lngCount).Name
                    
                        Case "jobID"
                            l_rstNewDetails.Fields(l_lngCount).Value = Val(txtJobID.Text)
                            
                        Case "jobcontactID"
                            'Nothing
                        
                        Case "cdate"
                            l_rstNewDetails.Fields(l_lngCount).Value = Now
                        
                        Case Else
                            l_rstNewDetails.Fields(l_lngCount).Value = l_rstOldDetails.Fields(l_lngCount).Value
                            
                    End Select
                    
                Next
                l_rstNewDetails.Update
                l_rstOldDetails.MoveNext
            Loop
            l_rstNewDetails.Close
            Set l_rstNewDetails = Nothing
        End If
    
    End If
    Set l_rstOldDetails = Nothing
    
    ShowJob Val(txtJobID.Text), tab1.Tab, False
'    ShowJob Val(txtJobID.Text), tab1.Tab, False
End If

End Sub

Private Sub cmdDuplicateRow_Click()

Dim l_varBookmark As Variant

If grdDetail.Columns("jobdetailID").Text = "" Then Exit Sub
l_varBookmark = adoJobDetail.Recordset.Bookmark
DuplicateJobDetailRow grdDetail.Columns("jobdetailID").Text, Val(txtJobID.Text)
adoJobDetail.Refresh
adoJobDetail.Recordset.Bookmark = l_varBookmark

End Sub


Private Sub cmdInsertInvoiceNotes_Click()

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If
InsertNote Val(txtJobID.Text), "invoicenotes", txtInsertInvoiceNotes.Text
txtInvoiceNotes.Text = GetData("job", "invoicenotes", "jobID", Val(txtJobID.Text))
txtInsertInvoiceNotes.Text = ""

End Sub

Private Sub cmdLaunch_Click(Index As Integer)

Dim l_strSearch As String

Select Case Index
Case 0 ' send email to contact
    
    If Val(lblContactID.Caption) <> 0 Then EmailContact lblContactID.Caption
    
Case 1 ' view company website
    'Dim l_strWebsite As String
    'l_strWebsite = GetData("company", "website", "companyID", lblCompanyID.Caption)
    'OpenBrowser l_strWebsite
    
    ShowCompany Val(lblCompanyID.Caption)
    
Case 5 ' change status
    
    If Val(txtJobID.Text) = 0 Then
        NoJobSelectedMessage
        Exit Sub
    End If
    
    'save the job first
    cmdSave.Value = True
    
    txtStatus.Text = ChangeJobStatus(Val(txtJobID.Text))
    ShowJob Val(txtJobID.Text), g_intDefaultTab, False
'    ShowJob Val(txtJobID.Text), g_intDefaultTab, False
Case 7


    l_strSearch = "http://www.multimap.com/map/places.cgi?db=pc&place=" & Replace(txtDeliveryPostCode.Text, " ", "")
    OpenBrowser l_strSearch

Case 8


    l_strSearch = "http://www.multimap.com/map/places.cgi?db=pc&place=" & Replace(txtCollectionPostCode.Text, " ", "")
    OpenBrowser l_strSearch

End Select

End Sub

Private Sub cmdLockCosts_Click()

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

LockCostingsOnJob Val(txtJobID.Text)

CostJob Val(txtJobID.Text), CBool(g_optCostDubbing), CBool(g_optCostSchedule)

End Sub

Private Sub cmdMakeSMVJobs_Click()

Dim l_lngNumberOfJobs As Long, l_lngCount As Long, l_rsJobs As ADODB.Recordset, l_lngJobID As Long

If Not CheckAccess("/MakeSMVJobBatch") Then Exit Sub

l_lngNumberOfJobs = Val(InputBox("How Many Job numbers to create", 0))
If l_lngNumberOfJobs > 0 Then
    Set l_rsJobs = ExecuteSQL("SELECT * FROM job WHERE jobID = -1;", g_strExecuteError)
    CheckForSQLError
    For l_lngCount = 1 To l_lngNumberOfJobs
        'Create a pencil job, not a quote, with title of "SMV Job Placeholder"
        l_rsJobs.AddNew
        l_rsJobs("createduserID") = g_lngUserID
        l_rsJobs("createduser") = g_strUserInitials
        l_rsJobs("createddate") = Now
        l_rsJobs("modifieddate") = Now
        l_rsJobs("modifieduserID") = g_lngUserID
        l_rsJobs("modifieduser") = g_strUserInitials
        l_rsJobs("flagemail") = 0
        l_rsJobs("fd_status") = "Pencil"
        l_rsJobs("jobtype") = "Dubbing"
        l_rsJobs("joballocation") = "Regular"
        l_rsJobs("deadlinedate") = Now
        l_rsJobs("despatchdate") = Now
        l_rsJobs("projectID") = 0
        l_rsJobs("productID") = 0
        l_rsJobs("companyID") = 1313
        l_rsJobs("companyname") = "Endemol Shine International Limited (SMV)"
        l_rsJobs("contactname") = "Vicky Albon"
        l_rsJobs("contactID") = 2454
        l_rsJobs("title1") = "SMV Order Placeholder"
        l_rsJobs.Update
        
        l_rsJobs.Bookmark = l_rsJobs.Bookmark
        l_lngJobID = l_rsJobs("JobID")
        SetData "job", "orderreference", "jobID", l_lngJobID, l_lngJobID
    Next
    l_rsJobs.Close
    Set l_rsJobs = Nothing
End If

MsgBox "Done.", vbInformation

End Sub

Private Sub cmdMarkFaulty_Click()

Dim l_strSQL As String, l_lngComplaintID As Long

If lblCompanyID.Caption <> "570" Then
    l_strSQL = "INSERT INTO complaint (complainttype, cdate, cuser, mdate, muser) VALUES (0, getdate(), '" & g_strFullUserName & "', getdate(), '" & g_strFullUserName & "');"
Else
    l_strSQL = "INSERT INTO complaint (complainttype, cdate, cuser, mdate, muser) VALUES (5, getdate(), '" & g_strFullUserName & "', getdate(), '" & g_strFullUserName & "');"
End If

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError
l_lngComplaintID = g_lngLastID

SetData "complaint", "originaljobID", "complaintID", l_lngComplaintID, txtJobID.Text
SetData "complaint", "companyID", "complaintID", l_lngComplaintID, lblCompanyID.Caption
SetData "complaint", "ordercontactID", "complaintID", l_lngComplaintID, lblContactID.Caption
SetData "complaint", "title", "complaintID", l_lngComplaintID, txtTitle.Text & IIf(txtSubtitle.Text <> "", ", " & txtSubtitle.Text, "") & " - " & grdDetail.Columns("description").Text
SetData "complaint", "orderreference", "complaintID", l_lngComplaintID, txtOrderReference.Text
SetData "complaint", "originaloperator", "complaintID", l_lngComplaintID, grdDetail.Columns("completeduser").Text
SetData "complaint", "jobcompleteddate", "complaintID", l_lngComplaintID, FormatSQLDate(grdDetail.Columns("completeddate").Text)
SetData "jobdetail", "complainedabout", "jobdetailID", grdDetail.Columns("jobdetailID").Text, 1
SetData "complaint", "originaljobdetailID", "complaintID", l_lngComplaintID, grdDetail.Columns("jobdetailID").Text

MsgBox "Issues Tracker item made.", vbInformation
ShowJob txtJobID.Text, 0, True

End Sub

Private Sub cmdMinimumCharge_Click()

Dim l_rstCosting As ADODB.Recordset, l_strSQL As String
Dim l_strCETACostCode As String, l_dblUnitCharge As Double, l_dblTotal As Double, l_dblVAT As Double, l_dblTotalIncVAT As Double
Dim l_intQuantity As Integer, l_intTime As Integer, l_sngVATRate As Single, l_intRatecard As Integer

Dim l_dblInflatedUnitCharge As Double
Dim l_dblInflatedTotal As Double
Dim l_dblInflatedVAT As Double
Dim l_dblInflatedTotalIncludingVAT As Double

Dim l_dblRefundedUnitCharge As Double
Dim l_dblRefundedTotal As Double
Dim l_dblRefundedVAT As Double
Dim l_dblRefundedTotalIncludingVAT As Double



l_strSQL = "SELECT * from costing WHERE jobID = " & txtJobID.Text & " AND (creditnotenumber IS NULL OR creditnotenumber = 0)"
Set l_rstCosting = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If l_rstCosting.EOF = False Then
    
    'Set the first entry line to be the MC cost line
    l_strCETACostCode = "MC"
    l_intRatecard = Val(GetData("company", "ratecard", "companyID", lblCompanyID.Caption))
    l_dblUnitCharge = GetUnitCharge(l_intRatecard, "MC", lblCompanyID.Caption)
    
    l_rstCosting.MoveFirst
    l_rstCosting("accountcode") = "1011001"
    l_rstCosting("copytype") = ""
    l_rstCosting("costcode") = "MC"
    l_rstCosting("description") = "Special Inclusive Price For Job"
    l_rstCosting("quantity") = 1
    l_rstCosting("fd_time") = 0
    l_rstCosting("unit") = "Each"
    l_rstCosting("discount") = 0
    l_rstCosting("unitcharge") = l_dblUnitCharge
    l_rstCosting("ratecardprice") = l_dblUnitCharge
    l_rstCosting("ratecardtotal") = l_dblUnitCharge

    l_sngVATRate = GetVATRate(Format(GetData("company", "vatcode", "companyID", lblCompanyID.Caption)))

    l_dblTotal = l_dblUnitCharge
    'l_dblTotal = Int((l_dblTotal * 100) + 0.5) / 100
    l_dblTotal = RoundToCurrency(l_dblTotal)

    l_dblVAT = l_dblTotal * ((l_sngVATRate / 100))
    'l_dblVAT = Int((l_dblVAT * 100) + 0.5) / 100
    l_dblVAT = RoundToCurrency(l_dblVAT)


    l_dblTotalIncVAT = l_dblTotal + l_dblVAT

    l_rstCosting("total") = l_dblTotal
    l_rstCosting("vat") = l_dblVAT
    l_rstCosting("totalincludingvat") = l_dblTotalIncVAT

    If lblTwoTier.Caption <> "" Then
    
        'inflated prices
        l_dblInflatedUnitCharge = GetUnitCharge(g_intBaseRateCard, l_strCETACostCode, lblCompanyID.Caption)
        
        l_dblInflatedTotal = l_dblInflatedUnitCharge
        'l_dblInflatedTotal = Int((l_dblInflatedTotal * 100) + 0.5) / 100
        l_dblInflatedTotal = RoundToCurrency(l_dblInflatedTotal)
           
        l_dblInflatedVAT = l_dblInflatedTotal * ((l_sngVATRate / 100))
        'l_dblInflatedVAT = Int((l_dblInflatedVAT * 100) + 0.5) / 100
        l_dblInflatedVAT = RoundToCurrency(l_dblInflatedVAT)
        
        
        l_dblInflatedTotalIncludingVAT = l_dblInflatedTotal + l_dblInflatedVAT
    
        'refunded prices
        l_dblRefundedUnitCharge = l_dblInflatedUnitCharge - l_dblUnitCharge
        l_dblRefundedTotal = l_dblInflatedTotal - l_dblTotal
        l_dblRefundedVAT = l_dblInflatedVAT - l_dblVAT
        l_dblRefundedTotalIncludingVAT = l_dblInflatedTotalIncludingVAT - l_dblTotalIncVAT
        
        l_rstCosting("inflatedunitcharge") = l_dblInflatedUnitCharge
        l_rstCosting("inflatedtotal") = l_dblInflatedTotal
        l_rstCosting("inflatedvat") = l_dblInflatedVAT
        l_rstCosting("inflatedtotalincludingvat") = l_dblInflatedTotalIncludingVAT
    
        l_rstCosting("refundedunitcharge") = l_dblRefundedUnitCharge
        l_rstCosting("refundedtotal") = l_dblRefundedTotal
        l_rstCosting("refundedvat") = l_dblRefundedVAT
        l_rstCosting("refundedtotalincludingvat") = l_dblRefundedTotalIncludingVAT
    
    End If
    l_rstCosting.Update
    'Then delete all the other entries and refresh the grid
    
    
    
l_strSQL = "DELETE FROM costing WHERE jobID = " & txtJobID.Text & " AND (creditnotenumber IS NULL OR creditnotenumber = 0) AND costcode <> 'MC';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

txtTotal.Text = FormatCurrency(GetTotalCostsForJob(Val(txtJobID.Text), "total", "INVOICE"), 2, vbFalse, vbFalse, vbTrue)
txtTotalIncVAT.Text = FormatCurrency(GetTotalCostsForJob(Val(txtJobID.Text), "totalincludingVAT", "INVOICE"), 2, vbFalse, vbFalse, vbTrue)
frmJob.txtRateCardTotal.Text = g_strCurrency & Format(RoundToCurrency(GetTotalCostsForJob(Val(txtJobID.Text), "ratecardtotal", "INVOICE")), "0.00#")
    
End If
l_rstCosting.Close
Set l_rstCosting = Nothing
adoCosting.Refresh

End Sub

Private Sub cmdMoveDown_Click()

On Error GoTo cmdMoveDown_Error

If grdDetail.Row <> grdDetail.Rows Then
    grdDetail.Columns("fd_orderby").Text = Val(grdDetail.Columns("fd_orderby").Text) + 1
    grdDetail.Update
    grdDetail.Row = grdDetail.Row + 1
    grdDetail.Columns("fd_orderby").Text = Val(grdDetail.Columns("fd_orderby").Text) - 1
    grdDetail.Update
    
    Dim l_lngBookmark As Long
    l_lngBookmark = grdDetail.Bookmark
    
    adoJobDetail.Refresh
    
    grdDetail.Bookmark = l_lngBookmark
End If

Exit Sub

cmdMoveDown_Error:

MsgBox Err.Description, vbExclamation
Exit Sub

End Sub

Private Sub cmdMoveUp_Click()
If grdDetail.Row <> 0 And grdDetail.Row <> grdDetail.Rows Then
    grdDetail.Columns("fd_orderby").Text = Val(grdDetail.Columns("fd_orderby").Text) - 1
    grdDetail.Update
    grdDetail.Row = grdDetail.Row - 1
    grdDetail.Columns("fd_orderby").Text = Val(grdDetail.Columns("fd_orderby").Text) + 1
    grdDetail.Update
    
    Dim l_lngBookmark As Long
    l_lngBookmark = grdDetail.Bookmark
    
    adoJobDetail.Refresh
    
    grdDetail.Bookmark = l_lngBookmark
    
End If
End Sub

Private Sub cmdNewOrderEntry_Click()

'Positions for the Frame
'Top = 4440
'Left = 180

'frmDisneyOrderEntry.fraDADC.Visible = False
frmDisneyOrderEntry.fraNormal.Top = 4440
frmDisneyOrderEntry.fraNormal.Left = 180
frmDisneyOrderEntry.fraNormal.Visible = True
frmDisneyOrderEntry.cmbCompany.Columns("companyID").Text = ""
frmDisneyOrderEntry.cmbCompany.Columns("name").Text = ""
frmDisneyOrderEntry.lblCompanyID.Caption = ""
frmDisneyOrderEntry.txtClientStatus.Text = ""
frmDisneyOrderEntry.cmbCompany.Columns("accountstatus").Text = ""
frmDisneyOrderEntry.cmbCompany.Visible = True
frmDisneyOrderEntry.lblCompanyID.Visible = True
frmDisneyOrderEntry.txtClientStatus.Visible = True
frmDisneyOrderEntry.lblCaption(16).Visible = True
frmDisneyOrderEntry.optOrderType(1).Value = True
frmDisneyOrderEntry.cmdClear.Value = True
frmDisneyOrderEntry.Show vbModal

ShowJob Val(frmDisneyOrderEntry.lblJobID.Caption), g_intDefaultTab, False
'ShowJob Val(frmDisneyOrderEntry.lblJobID.Caption), g_intDefaultTab, False
Unload frmDisneyOrderEntry

End Sub

Private Sub cmdNewSundry_Click()

If Not CheckAccess("/newsundry") Then Exit Sub

If GetStatusNumber(txtStatus.Text) >= 2 And GetStatusNumber(txtStatus.Text) <= 4 Then
      
    frmSundry.cmbJobID.Text = txtJobID.Text
    frmSundry.cmbProject.Text = cmbProject.Text

    frmSundry.cmbJobID.Enabled = False
    frmSundry.cmbProject.Enabled = False
         
    frmSundry.datSundryDateOrdered.Value = Date
    frmSundry.cmbSundryTimeOrdered.Text = Format(Time, "HH:MM")
    
    frmSundry.datSundryDateWanted.Value = Date
    frmSundry.cmbSundryTimeWanted.Text = Format(Time, "HH:MM")
    
    frmSundry.Show vbModal
    
    If frmSundry.Tag <> "CANCELLED" Then
    
        Dim l_strSundryType As String
        l_strSundryType = IIf(frmSundry.tabSundry.Caption = "Taxi", "Taxi", "Food")
        
        With frmSundry
        
            SaveSundry Val(.lblSundryID.Caption), l_strSundryType, Val(.cmbJobID.Text), Val(.cmbProject.Text), .datSundryDateOrdered.Value & " " & .cmbSundryTimeOrdered.Text, .datSundryDateWanted & " " & .cmbSundryTimeWanted, .txtSundryPickupTime.Text, .txtSundryAccountNumber.Text, .txtSundryPassenger.Text, _
                        .txtSundryLocation.Text, .txtSundryDestination.Text, .txtSundryComments.Text, .cmbRequestedBy.Text, .ddnSundryEnteredBy.Text, .txtSundryDetails.Text, .txtSundrySupplier.Text, Val(.txtSundryAmount.Text), .chkPersonal.Value, .cmbOverheads.Text
        End With
        
    
        
    End If
    
    Unload frmSundry
    Set frmSundry = Nothing

    adoSundry.Refresh
    
Else
    MsgBox "You can not add sundry costs for jobs of this status (" & txtStatus.Text & ")", vbExclamation
End If

End Sub

Private Sub cmdNoCharge_Click()

If cmdNoCharge.FontBold = False Then
    Dim l_intMsg As Integer
    l_intMsg = MsgBox("Are you sure you want to 'No Charge' this job?", vbQuestion + vbYesNo)
    If l_intMsg = vbNo Then Exit Sub
End If

If lblCompanyID.Caption = 782 Then
    NoChargeJob Val(txtJobID.Text), True
Else
    NoChargeJob Val(txtJobID.Text), False
End If

ShowJob Val(txtJobID.Text), tab1.Tab, False
'ShowJob Val(txtJobID.Text), tab1.Tab, False

End Sub

Private Sub cmdOperations_Click()

Dim l_lngJobID As Long
Dim l_strSQL As String
Dim l_strUserInitials As String
Dim l_intMsgbox As Integer
Dim l_strEmailTo As String, l_strEmailBody As String, l_lngBookedBy As Long

If cmdOperations.Caption = "Done" Then
    UpdateJobStatus Val(lblJobID.Caption), "VT Done"
    ShowJob Val(lblJobID.Caption), 0, False
'    ShowJob Val(lblJobID.Caption), 0, False
ElseIf cmdOperations.Caption = "Start" Then
    
    l_lngJobID = Val(lblJobID.Caption)
    
    If l_lngJobID = 0 Then Exit Sub
    
    'get the initials - if there are any
    l_strUserInitials = GetData("job", "vtstartuser", "jobID", l_lngJobID)
    
    If l_strUserInitials <> "" Then
        l_intMsgbox = NewMsgBox("Job already started", "This job has already been started. What do you want to do?", "Remove", "Re-start", "Cancel")
        Select Case l_intMsgbox
        Case 0
            
            l_strSQL = "UPDATE job SET vtstartdate = Null, vtstartuser = Null WHERE jobID = '" & l_lngJobID & "';"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
    
            GoTo Proc_Refresh
            
        Case 1
            'do nothing, carry on as usual
        Case 2
            Exit Sub
        End Select
    End If
        
Proc_Start:
    
    l_strUserInitials = g_strUserInitials

    l_strSQL = "UPDATE job SET vtstartdate = '" & FormatSQLDate(Now) & "', vtstartuser = '" & l_strUserInitials & "', fd_status = 'In Progress' WHERE jobID = '" & l_lngJobID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'allocate the clip ID numbers to any "D" lines on the job (digital)
    If g_intNoAutomaticClipNumbers = 0 Then
        AllocateClipIDNumbers l_lngJobID
    End If
    
    l_lngBookedBy = GetData("job", "createduserID", "jobID", Val(lblJobID.Caption))
    l_strEmailTo = GetData("cetauser", "email", "cetauserID", l_lngBookedBy)
    l_strEmailBody = "Job: " & Val(lblJobID.Caption) & " has been marked In Progress by " & g_strFullUserName
    'SendSMTPMail l_strEmailTo, "", l_strEmailBody, "", l_strEmailBody, True, "", "", g_strAdministratorEmailAddress, False
    
    If frmStartVTJob.Tag = "Print" Then PrintJobSheet Val(txtJobID.Text), "jobsheetdub.rpt"
    
    
    AddJobHistory Val(txtJobID.Text), "Started VT Job"
Proc_Refresh:
    ShowJob l_lngJobID, 1, False
'    ShowJob l_lngJobID, 1, False

End If

End Sub

'
Private Sub cmdPasteDubbingClocks_Click()
Dim l_strSQL As String
Dim l_lngJobID As Long

l_lngJobID = Val(txtJobID.Text)

l_strSQL = "SELECT clocknumber FROM jobdetail WHERE jobID = '" & l_lngJobID & "' AND clocknumber IS NOT NULL ORDER BY fd_orderby, jobdetailID;"
Dim l_rstClocks As New ADODB.Recordset

Dim l_conClocks As New ADODB.Connection
l_conClocks.Open g_strConnection

l_rstClocks.Open l_strSQL, l_conClocks, adOpenForwardOnly, adLockReadOnly

If Not l_rstClocks.EOF Then
    l_rstClocks.MoveFirst
Else
    MsgBox "This job has no clocks in its dubbings requirements", vbExclamation
End If

Do While Not l_rstClocks.EOF
    txtInvoiceText.Text = l_rstClocks("clocknumber") & vbCrLf & txtInvoiceText.Text
    l_rstClocks.MoveNext
    
Loop

l_rstClocks.Close
Set l_rstClocks = Nothing

l_conClocks.Close
Set l_conClocks = Nothing



End Sub

Private Sub cmdPreviewInvoice_Click()

On Error GoTo cmdPreview_Error


Dim l_lngJobID As Long
l_lngJobID = Val(txtJobID.Text)
If l_lngJobID = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

cmdPreviewInvoice.Enabled = False
Screen.MousePointer = vbHourglass

Dim l_lngInvoiceNumber  As Long
'get the invoice number
l_lngInvoiceNumber = Val(GetData("job", "invoicenumber", "jobID", l_lngJobID))

Dim l_strRPTFileName As String
Dim l_lngCostingSheetID As Long
Dim l_strSelectionFormula As String

l_lngCostingSheetID = GetData("job", "costingsheetID", "jobID", l_lngJobID)

If l_lngCostingSheetID = 0 Then
    l_strSelectionFormula = "{job.jobID} = " & l_lngJobID
    
    If Left(UCase(frmJob.cmbJobType.Text), 6) = "CREDIT" Then
        l_strRPTFileName = g_strLocationOfCrystalReportFiles & "creditnote.rpt"
    ElseIf l_lngInvoiceNumber = 0 Then
        l_strRPTFileName = g_strLocationOfCrystalReportFiles & "jobcosting.rpt"
    Else
        If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/invoicewellcome") > 0 Then
            l_strRPTFileName = g_strLocationOfCrystalReportFiles & "invoicewithsummary.rpt"
        Else
            If optInvoiceType(1).Value = True Then
                l_strRPTFileName = g_strLocationOfCrystalReportFiles & "invoice_vdms_temp.rpt"
            Else
                l_strRPTFileName = g_strLocationOfCrystalReportFiles & "invoice.rpt"
            End If
        End If
    End If

Else
    
    l_strSelectionFormula = "{costingsheet.costingsheetID} = " & l_lngCostingSheetID
    l_strRPTFileName = g_strLocationOfCrystalReportFiles & "groupcosting01.rpt"
End If

Dim crxApplication As New CRAXDRT.Application
Dim crxReport As New CRAXDRT.Report

Set crxReport = crxApplication.OpenReport(l_strRPTFileName)

crxReport.RecordSelectionFormula = l_strSelectionFormula

'Check whether a number of copies was sent, and if not set to 1
        
crxReport.DiscardSavedData
CRViewer91.ReportSource = crxReport
CRViewer91.Width = tab1.Width - CRViewer91.Left - 120
CRViewer91.Height = tab1.Height - CRViewer91.Top - 120
CRViewer91.ViewReport
CRViewer91.Zoom 75

Set crxReport = Nothing



Screen.MousePointer = vbDefault
cmdPreviewInvoice.Enabled = True


Exit Sub:
cmdPreview_Error:
MsgBox "There was an error while previewing." & vbCrLf & vbCrLf & Err.Description, vbExclamation

Screen.MousePointer = vbDefault
cmdPreviewInvoice.Enabled = True


Exit Sub


End Sub

Private Sub cmdRemoveFromCosting_Click()

If Not CheckAccess("/unattachfromcostingsheet") Then Exit Sub

If GetStatusNumber(txtStatus.Text) > 5 Then
    MsgBox "You can not remove this job as it has already been invoiced!", vbExclamation
End If

Dim l_intRemove As Integer

l_intRemove = MsgBox("Are you sure you want to unattach this job from costing sheet '" & txtCostingSheet.Text & "'?", vbYesNo + vbQuestion)

If l_intRemove = vbNo Then Exit Sub

SetData "job", "costingsheetID", "jobID", txtJobID.Text, Null
AddJobHistory Val(txtJobID.Text), "Removed from MCS " & txtCostingSheet.Text

ShowJob txtJobID.Text, m_intTabCosting, False
'ShowJob txtJobID.Text, m_intTabCosting, False

End Sub

Private Sub cmdReplaceDespatch_Click()

Dim l_intQuestion As Integer
l_intQuestion = MsgBox("Are you sure you want to replace these notes?", vbQuestion + vbYesNo)
If l_intQuestion = vbNo Then Exit Sub
SetData "job", "notes3", "jobID", Val(txtJobID.Text), ""
cmdAddDespatchNotes.Value = True
AddJobHistory Val(txtJobID.Text), "Replaced Despatch Notes"
RefreshJobHistory Val(txtJobID.Text)
End Sub
Sub CheckClear()
txtInsertNotes.SetFocus
If chkClearAfterAdd.Value = 1 Then txtInsertNotes.Text = ""

    
End Sub
Private Sub cmdAddOffice_Click()

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If
InsertNote Val(txtJobID.Text), "notes1", txtInsertNotes.Text
txtOfficeClient.Text = GetData("job", "notes1", "jobID", Val(txtJobID.Text))
CheckClear

End Sub
Private Sub cmdReplaceOffice_Click()
Dim l_intQuestion As Integer
l_intQuestion = MsgBox("Are you sure you want to replace these notes?", vbQuestion + vbYesNo)
If l_intQuestion = vbNo Then Exit Sub
SetData "job", "notes1", "jobID", Val(txtJobID.Text), ""
cmdAddOffice.Value = True
AddJobHistory Val(txtJobID.Text), "Replaced Office Notes"
RefreshJobHistory Val(txtJobID.Text)
End Sub

Private Sub cmdAddOperatorNotes_Click()
If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If
InsertNote Val(txtJobID.Text), "notes2", txtInsertNotes.Text
If g_optAlsoInsertOpNotesToAccounts Then InsertNote Val(txtJobID.Text), "notes4", "OP: " & txtInsertNotes.Text
txtOperator.Text = GetData("job", "notes2", "jobID", Val(txtJobID.Text))
CheckClear

End Sub
Private Sub cmdReplaceOperator_Click()
Dim l_intQuestion As Integer
l_intQuestion = MsgBox("Are you sure you want to replace these notes?", vbQuestion + vbYesNo)
If l_intQuestion = vbNo Then Exit Sub
SetData "job", "notes2", "jobID", Val(txtJobID.Text), ""
cmdAddOperatorNotes.Value = True
AddJobHistory Val(txtJobID.Text), "Replaced Operator Notes"
RefreshJobHistory Val(txtJobID.Text)
End Sub

Private Sub cmdCancelJob_Click()
If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

CancelJob Val(txtJobID.Text)

End Sub

Private Sub cmdClear_Click()
tab1.Tab = m_intTabJob
ShowJob -1, tab1.Tab, True
'ShowJob -1, tab1.Tab, True
cmbJobType.Text = g_strDefaultJobType
cmbAllocation.Text = g_strDefaultJobAllocation
End Sub

Private Sub cmdClose_Click()
Me.Hide
DoEvents
ShowJob -2, tab1.Tab, True
'ShowJob -2, tab1.Tab, True
Me.Hide
End Sub

Private Sub cmdCostingPrint_Click()

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

If Val(txtCostingSheet.Text) <> 0 Then
    MsgBox "You can not print a job costing for jobs attached to a costing sheet", vbExclamation
    Exit Sub
End If

PrintJobCostingPage Val(txtJobID.Text)

End Sub

Private Sub cmdCreditNote_Click()

'Dim l_blnResult As Boolean''

If Not CheckAccess("/makecreditnote") Then
    Exit Sub
End If

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

Dim l_intMsg As Integer
l_intMsg = MsgBox("Do you really want to CREDIT this job?", vbYesNo + vbQuestion)
If l_intMsg = vbNo Then Exit Sub

'call the invoice job routine and create the invoice header if required
'l_blnResult = InvoiceJob(Val(txtJobID.Text), "CREDIT")'

'If l_blnResult = True Then
'    tab1.Tab = m_intTabCreditNote
'
'    i = MsgBox("Do you want to print a credit note?", vbYesNo + vbQuestion)
'    If i = vbNo Then Exit Sub
'
'    PrintCreditNote Val(txtJobID.Text)
'End If'


Dim l_lngJobID As Long
Dim l_lngOriginalJobID As Long

'this is only allowed for jobs which are already posted in accounts
If txtStatus.Text <> "Sent To Accounts" Then
    MsgBox "You can only credit jobs which have already been sent to accounts", vbExclamation
    Exit Sub
End If

'get the original job ID
l_lngOriginalJobID = Val(txtJobID.Text)

'generate a copy of this job, and pick up the jobID
l_lngJobID = DuplicateJob(l_lngOriginalJobID, "CREDIT")

'if the duplicate job function worked
If l_lngJobID > 0 Then

    'update the new job record, so that it doesnt have the notes etc from the previous job.
    'also - set the new jobs' jobtype to be "credit" and set the status to "Hold Cost"
    Dim l_strSQL As String
    l_strSQL = "UPDATE job SET fd_status = 'Hold Cost', notes1 = NULL, notes2 = NULL, notes3 = NULL, notes4 = NULL, notes5 = NULL, jobtype = 'Credit', " & _
        "senttoaccountsbatchnumber = NULL, senttoaccountsdate = NULL, senttoaccountsuser = NULL, " & _
        "creditnotenumber = NULL, creditnotebatchnumber = NULL, creditnotedate = NULL, creditsenttoaccountsdate = NULL, creditsenttoaccountsuser = NULL WHERE jobID = '" & l_lngJobID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'copy all the costing lines from the original job, but make them negetive
    CopyCostingLines l_lngOriginalJobID, l_lngJobID, "CREDIT"
    
    'update the job history of the original job
    AddJobHistory l_lngOriginalJobID, "CREDITED JOB - " & l_lngJobID
    
    'add job history for the new job
    AddJobHistory l_lngJobID, "MADE CREDIT NOTE"
    
    'does the user want to show this job now?
    l_intMsg = MsgBox("New credit job created with ID: " & l_lngJobID & vbCrLf & vbCrLf & "Do you want to view this job now?", vbYesNo + vbQuestion)
    If l_intMsg = vbYes Then
        ShowJob l_lngJobID, 1, False
'        ShowJob l_lngJobID, 1, False
    End If
    
    DoEvents
    
    'ask the user if they want to create a positive copy of the original job. This can be used to add costs
    'which didnt need to be credited.

'    l_intMsg = MsgBox("Do you want to create a new invoice only copy of this job, in addition to the credit note?", vbYesNo + vbQuestion)
'
'    If l_intMsg = vbYes Then
'
'        'create annother copy of the original job
'        l_lngJobID = DuplicateJob(l_lngOriginalJobID)
'
'        'set the new job's notes to be null again, and set its type to be invoice only
'        l_strSQL = "UPDATE job SET fd_status = 'Hold Cost', notes1 = NULL, notes2 = NULL, notes3 = NULL, notes4 = NULL, notes5 = NULL, jobtype = 'Invoice Only' WHERE jobID = '" & l_lngJobID & "';"
'        ExecuteSQL l_strSQL, g_strExecuteError
'        CheckForSQLError
'
'        'copy the costing lines - this time, keep the exactly the same as the original
'        CopyCostingLines l_lngOriginalJobID, l_lngJobID, ""
'
'        'add job history
'        AddJobHistory l_lngOriginalJobID, "INV. ONLY - " & l_lngJobID
'        AddJobHistory l_lngJobID, "MADE INVOICE ONLY JOB"
'
'        'does the user want to show this job now?
'        l_intMsg = MsgBox("New job created with ID: " & l_lngJobID & vbCrLf & vbCrLf & "Do you want to view this job now?", vbYesNo + vbQuestion)
'
'        If l_intMsg = vbYes Then
'            ShowJob l_lngJobID, 1, True
'        End If
'
'    End If


End If

End Sub

Private Sub cmdInvoice_Click()

Dim l_blnResult As Boolean

If Val(txtCostingSheet.Text) <> 0 Then
    MsgBox "This job is part of a costing sheet and so can not be finalised!", vbExclamation
    Exit Sub
End If

If cmdInvoice.Caption = "Invoice" Or cmdInvoice.Caption = "Finalise" Then

    If Val(txtJobID.Text) = 0 Then
        NoJobSelectedMessage
        Exit Sub
    End If
    
    If GetData("job", "flagquote", "jobID", Val(txtJobID.Text)) <> 0 Then
        MsgBox "Cannot Finalise Job", vbCritical, "Job is still flagged as a Quotation"
        Exit Sub
    End If
    
    Dim i As Integer
    i = MsgBox("Do you really want to FINALISE this job?", vbYesNo + vbQuestion)
    If i = vbNo Then Exit Sub
    
    'call the invoice job routine and create the invoice header if required
    If UCase(Left(cmbJobType.Text, 6)) = "CREDIT" Then
        l_blnResult = InvoiceJob(Val(txtJobID.Text), "CREDIT")
    Else
        l_blnResult = InvoiceJob(Val(txtJobID.Text), "INVOICE")
    End If

ElseIf cmdInvoice.Caption = "Send To MP" Then

    'Routine to invoke the exporting of a .csv with all the info for MediaPulse.
    Dim l_strOutputLocation As String
    Dim l_strTransitLocation As String
    Dim FSO As Scripting.FileSystemObject
    Dim l_lngFilenumber As Long
    Dim l_curFileSize As Currency
    Dim l_lngCSV_FileID  As Long
    Dim l_strSQL As String
    
    'check to see if there are any problems with this jobs costing records
    Select Case JobSafeForPosting(Val(txtJobID.Text), True)
    Case vbuJobSafe
        l_strOutputLocation = GetData("library", "subtitle", "libraryID", GetData("setting", "value", "name", "InvoicesForMPLibraryID"))
        l_strOutputLocation = l_strOutputLocation & "\" & GetData("setting", "value", "name", "InvoicesForMPBuildLocation")
        On Error GoTo OPENERROR
        'Open text file for ouput if not batching to accounts.
        Open l_strOutputLocation & "\" & txtJobID.Text & ".csv" For Output As 1
        On Error GoTo 0
        SendJobToCustomer Val(txtJobID.Text), "CETAtoMP", 0
        Close 1
        Set FSO = New Scripting.FileSystemObject
        If FSO.FileExists(l_strOutputLocation & "\" & txtJobID.Text & ".csv") Then
            API_OpenFile l_strOutputLocation & "\" & txtJobID.Text & ".csv", l_lngFilenumber, l_curFileSize
            API_CloseFile l_lngFilenumber
            If l_curFileSize > 0 Then
                l_lngCSV_FileID = CreateClip
                SetData "events", "libraryID", "eventID", l_lngCSV_FileID, GetData("setting", "value", "name", "InvoicesForMPLibraryID")
                SetData "events", "companyID", "eventID", l_lngCSV_FileID, 501
                SetData "events", "companyname", "eventID", l_lngCSV_FileID, "MX1 Ltd."
                SetData "events", "clipfilename", "eventID", l_lngCSV_FileID, txtJobID.Text & ".csv"
                SetData "events", "altlocation", "eventID", l_lngCSV_FileID, GetData("setting", "value", "name", "InvoicesForMPBuildLocation")
                SetData "events", "clipreference", "eventID", l_lngCSV_FileID, txtJobID.Text
                SetData "events", "clipformat", "eventID", l_lngCSV_FileID, "Excel File"
                SetData "events", "bigfilesize", "eventID", l_lngCSV_FileID, l_curFileSize
                l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, bigfilesize, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
                l_strSQL = l_strSQL & l_lngCSV_FileID & ", "
                l_strSQL = l_strSQL & GetData("setting", "value", "name", "InvoicesForMPLibraryID") & ", "
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
                l_strSQL = l_strSQL & GetData("setting", "value", "name", "InvoicesForMPTransferLibraryID") & ", "
                l_strSQL = l_strSQL & "'" & GetData("setting", "value", "name", "InvoicesForMPTransferAltLocation") & "', "
                l_strSQL = l_strSQL & l_curFileSize & ", "
                l_strSQL = l_strSQL & "1, '" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                If GetData("job", "completeddate", "jobID", Val(txtJobID.Text)) = "" Then
                    SetData "job", "completeddate", "jobID", Val(txtJobID.Text), Format(Now, "YYYY-MM-DD HH:NN:SS")
                End If
                UpdateJobStatus Val(txtJobID.Text), "Sent To Accounts"
                SetData "job", "senttoaccountsdate", "jobID", Val(txtJobID.Text), Format(Now, "YYYY-MM-DD HH:NN:SS")
            End If
        End If
    Case vbJobNotSafeMediaPulseProfiles
        MsgBox "Job was not ready for finalising - please check Media Pulse Profiles on costing lines and then re-try"
        Exit Sub
    Case Else
        MsgBox "Job was not ready for finalising - please check costing lines and then re-try"
    End Select
    
    'Then move that file to the Transfer Location
    
    'Then tell them it's gone
    MsgBox "This job has been successfully sent to Media Pulse."
    ShowJob txtJobID.Text, 0, False
    Exit Sub

OPENERROR:
MsgBox "Failure to open Output file. Please verify that the folder" & vbCrLf & g_strUploadInvoiceLocation & " exists.", vbCritical, "Error opening Batch Output"
Exit Sub

Else

    'Caption must be re-print, so hard set the result caption to have been sucessful
    l_blnResult = True
    
End If

If l_blnResult = True Then
    
    If g_optDontPromptForNumberOfInvoiceCopies Then
        If g_intDefaultCopiesToPrintOfInvoice > 0 Then PrintCostingPage Val(txtJobID.Text), g_intDefaultCopiesToPrintOfInvoice
    Else
    
        frmCopiesToPrint.txtCopies.Text = CStr(g_intDefaultCopiesToPrintOfInvoice)
        frmCopiesToPrint.Show vbModal
        
        If frmCopiesToPrint.Tag <> "CANCELLED" Then
            PrintCostingPage Val(txtJobID.Text), Val(frmCopiesToPrint.txtCopies.Text)
        Else
            PrintCostingPage Val(txtJobID.Text), 0
        End If
        
        Unload frmCopiesToPrint
        Set frmCopiesToPrint = Nothing
        
        tab1.Tab = m_intTabCosting
    End If
    
    ShowJob Val(txtJobID.Text), 0, False
'    ShowJob Val(txtJobID.Text), 0, False
    
End If

End Sub

Private Sub cmdJobDespatches_Click()

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

ShowDespatchForJob Val(txtJobID.Text)

End Sub

Private Sub cmdPrint_Click()

If Val(txtJobID.Text) > 0 Then
    
    Dim l_strReportToPrint As String
    
    If chkQuote.Value = 1 And Val(lblCompanyID.Caption) <> 1244 Then
        If Not CheckAccess("/printquote") Then Exit Sub
        CostJob Val(txtJobID.Text), True, True
        l_strReportToPrint = g_strLocationOfCrystalReportFiles & "quote.rpt"
    ElseIf tab1.Caption = "Costing" Then
        If Not CheckAccess("/printjobcosting") Then Exit Sub
        If UCase(Left(cmbJobType.Text, 6)) = "CREDIT" Then
            l_strReportToPrint = g_strLocationOfCrystalReportFiles & "credit.rpt"
        Else
            If txtStatus.Text = "No Charge" Then
                l_strReportToPrint = g_strLocationOfCrystalReportFiles & "quote.rpt"
            Else
                l_strReportToPrint = g_strLocationOfCrystalReportFiles & "invoice.rpt"
            End If
        End If
    ElseIf Val(lblCompanyID.Caption) = 1244 Then
        If Not CheckAccess("/printquote") Then Exit Sub
        CostJob Val(txtJobID.Text), True, True
        l_strReportToPrint = g_strLocationOfCrystalReportFiles & "BFI-Report.rpt"
    Else
        If Left(UCase(cmbJobType.Text), 3) = "DUB" Then
'            If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/assetsearch") > 0 Then
'                PrintJobSheet Val(txtJobID.Text), "jobsheetdubasset.rpt"
'            Else
                PrintJobSheet Val(txtJobID.Text), "jobsheetdub.rpt"
'            End If
            If g_blnPreviewReport = False And g_intPrintCopyJobSheets <> 0 Then PrintJobSheet Val(txtJobID.Text), "jobsheetdubcopy.rpt"
        Else
            PrintJobSheet Val(txtJobID.Text)
        End If
        
        If g_blnPreviewReport = False Then
            ShowJob txtJobID.Text, tab1.Tab, False
'            ShowJob txtJobID.Text, tab1.Tab, False
        End If

        Exit Sub
    End If
    
    PrintCrystalReport l_strReportToPrint, "{job.jobID} = " & txtJobID.Text, True
    
Else
    NoJobSelectedMessage
End If

End Sub

Private Sub cmdReCost_Click()

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

If chkNoChargeJob.Value <> 0 Then
    Exit Sub
End If

Dim i As Integer

i = MsgBox("Re-costing a job deletes all unbilled costs, and then runs the automatic costing routines again." & vbCrLf & vbCrLf & "You will lose any manually added costing records or changes which were done manually." & vbCrLf & vbCrLf & "Are you sure you want to re-cost this job?", vbYesNo + vbQuestion)
If i = vbNo Then Exit Sub


If cmdNoCharge.FontBold = True Then
    i = MsgBox("This job has been 'No Charged'. Re-Costing it will remove the 'no charge' information." & vbCrLf & vbCrLf & "Are you sure you want to re-cost this job?", vbExclamation + vbYesNo)
    If i = vbNo Then Exit Sub

    Dim l_strSQL As String
    l_strSQL = "UPDATE job SET fd_status = 'Hold Cost', nochargerequestedby = Null, nochargerequestedreason = Null, nochargerequestedcode = Null WHERE jobID = '" & txtJobID.Text & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    AddJobHistory txtJobID.Text, "Removed No Charge"
    
    cmdNoCharge.FontBold = False

End If

AddJobHistory txtJobID.Text, "Re-Costed"

DeleteCostings Val(txtJobID.Text), "INVOICE"

CostJob Val(txtJobID.Text), 0 - GetFlag(g_optCostDubbing), 0 - GetFlag(g_optCostSchedule)

UpdateSundryCostings Val(txtJobID.Text), Val(lblCompanyID.Caption)

CostJob Val(txtJobID.Text), Val(g_optCostDubbing), Val(g_optCostSchedule)

'ShowJob Val(txtJobID.Text), tab1.Tab, False

tab1.Tab = m_intTabCosting
tabReplacementTabs.Tab = GetReplacementTab(tab1.Tab)

End Sub

Private Sub cmdReinstate_Click()

If Not CheckAccess("/reinstatejob") Then
    Exit Sub
End If

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

ReInstateJob Val(txtJobID.Text)

grdCostingDetail.AllowUpdate = True
grdCostingDetail.AllowAddNew = True
grdCostingDetail.AllowDelete = True

datInvoiceDate.Value = Null

CostJob Val(txtJobID.Text), CBool(g_optCostDubbing), CBool(g_optCostSchedule)

ShowJob Val(txtJobID.Text), g_intDefaultTab, False
'ShowJob Val(txtJobID.Text), g_intDefaultTab, False
cmdCancelJob.Enabled = True

End Sub

Private Sub cmdReplaceProducerNotes_Click()

Dim l_intQuestion As Integer
l_intQuestion = MsgBox("Are you sure you want to replace these notes?", vbQuestion + vbYesNo)
If l_intQuestion = vbNo Then Exit Sub
SetData "job", "notes5", "jobID", Val(txtJobID.Text), ""
cmdAddProducerNotes.Value = True
AddJobHistory Val(txtJobID.Text), "Replaced Producer Notes"
RefreshJobHistory Val(txtJobID.Text)
End Sub

Private Sub cmdResetInvoiceNumber_Click()

If Not CheckAccess("/updateinvoicenumber") Then Exit Sub

If UCase(txtStatus.Text) <> "SENT TO ACCOUNTS" Then
    MsgBox "You can only do this for jobs which have been sent to accounts", vbExclamation
    Exit Sub
End If

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

Dim l_strInputOrder As String

l_strInputOrder = InputBox("Please enter the new invoice number", "Update Invoice Number", lblInvoiceNumber.Caption)

If l_strInputOrder <> lblInvoiceNumber.Caption And l_strInputOrder <> "" Then
    
    SetData "job", "invoicenumber", "jobID", txtJobID.Text, Val(l_strInputOrder)
    AddJobHistory txtJobID.Text, "Updated Invoice Number From " & lblInvoiceNumber.Caption & " to " & UCase(l_strInputOrder)
    
    lblInvoiceNumber.Caption = UCase(l_strInputOrder)
    RefreshJobHistory txtJobID.Text
    
End If

End Sub

Private Sub cmdResetRowNumbering_Click()

Dim l_lngCount As Long
If adoJobDetail.Recordset.RecordCount > 0 Then
    l_lngCount = 0
    adoJobDetail.Recordset.MoveFirst
    Do While Not adoJobDetail.Recordset.EOF
        adoJobDetail.Recordset("fd_orderby") = l_lngCount
        adoJobDetail.Recordset.Update
        l_lngCount = l_lngCount + 1
        adoJobDetail.Recordset.MoveNext
    Loop
End If

End Sub

Private Sub cmdSave_Click()

If Val(lblCompanyID.Caption) = 0 Then
    NoCompanySelectedMessage
    cmbCompany.SetFocus
    Exit Sub
End If

If lblJobID.Caption <> txtJobID.Text Then
    txtJobID.Text = lblJobID.Caption
    MsgBox "You have tried to change this job's 'Job ID'. You can not do this!" & vbCrLf & vbCrLf & "The job you are viewing should be job ID " & lblJobID.Caption & " so the jobID textbox has now be reverted back to it. You can then either re-save if required, or load the new job ID by pressing return while in the job ID box.", vbExclamation
    Exit Sub
End If

Dim l_intMsg As Integer

'check if a title exists
If txtTitle.Text = "" Then
    l_intMsg = MsgBox("You have not entered a title." & vbCrLf & vbCrLf & "Are you sure you want to save without one?", vbExclamation + vbYesNo)
    If l_intMsg = vbYes Then
        txtTitle.Text = "TBA"
    Else
        Exit Sub
    End If
End If

If cmbContact.Text <> GetData("contact", "name", "contactID", Val(lblContactID.Caption)) Then
    MsgBox "Contact name is wrong. Please select a contact from the dropdown, and do not free type a name.", vbCritical, "Job Not Saved"
    Exit Sub
End If

'HIRE ISSUES!!! These things should only be done for HIRE jobs, but METRO need to do it for HIRE jobs too
If (UCase(cmbJobType.Text) = "HIRE" Or UCase(cmbJobType.Text) = "EDIT") Then

    If cmbDeliveryCompany.Text = "" Then
        'new check for delivery address  - make sure before using...
        l_intMsg = MsgBox("There is no specified default delivery address. Do you want to copy the company address?", vbExclamation + vbYesNo)
        If l_intMsg = vbYes Then
            cmbDeliveryCompany.Text = cmbCompany.Text
            cmbDeliveryContact.Text = cmbContact.Text
            txtDeliveryAddress.Text = GetData("company", "address", "companyID", lblCompanyID.Caption)
            txtDeliveryPostCode.Text = GetData("company", "postcode", "companyID", lblCompanyID.Caption)
            txtDeliveryCountry.Text = GetData("company", "country", "companyID", lblCompanyID.Caption)
            txtDeliveryTelephone.Text = GetData("company", "telephone", "companyID", lblCompanyID.Caption)
            cmdCopyToCollection_Click
        Else
            AddJobHistory Val(txtJobID.Text), "User declined to use company address as default del/col address"
        End If
    End If
End If

If g_optRequireOurContactBeforeSaving = 1 And cmbOurContact.Text = "" Then
    MsgBox "You must specify 'Our Contact' before saving.", vbExclamation
    Exit Sub
End If

If g_intDisableProjects = 1 Then cmbAllocation.Text = "Regular"

If cmbAllocation.Text = "" And g_intDisableProjects <> 1 Then
    MsgBox "You need to specify this job's project type. Please correct and try again", vbExclamation
    Exit Sub
End If

If Val(cmbProject.Text) <> 0 Then
    
    If UCase(GetData("allocation", "allocationtype", "name", cmbAllocation)) <> "AUTOMATIC" Then
        
        If Val(lblProjectID.Caption) <> Val(GetData("project", "projectID", "projectnumber", cmbProject.Text)) Then
            MsgBox "You must select a project number from the drop down provided. This ensures that the project you are using is still active.", vbExclamation
            Exit Sub
        End If
        

    
    End If
    
ElseIf Val(cmbProject.Text) = 0 Then

    If UCase(GetData("allocation", "allocationtype", "name", cmbAllocation.Text)) <> "AUTOMATIC" Then
        MsgBox "You need to select a project before saving a pre-defined job type", vbExclamation
        Exit Sub
    End If

End If

If UCase(GetData("allocation", "allocationtype", "name", cmbAllocation.Text)) = "AUTOMATIC" And g_optForceDealPrice <> 0 Then
    Select Case cmbDealType.Text
    Case ""
        MsgBox "You must select a deal type before saving a regular job", vbExclamation
        cmbDealType.SetFocus
        Exit Sub
    Case "Deal"
        If txtDealPrice.Text = "" Or Val(txtDealPrice.Text) = 0 Then
            MsgBox "You must enter a deal price before saving a regular job", vbExclamation
            txtDealPrice.SetFocus
            Exit Sub
        End If
        
    Case "F.O.C."
        txtDealPrice.Text = 0
        
    
    Case "Rate Card"
    
    End Select
End If

If Val(lblContactID.Caption) = 0 Then

    If cmbContact.Text <> "" Then
        frmAddSelectContact.txtContactName(0).Text = cmbContact.Text
        If InStr(cmbContact.Text, " ") <> 0 Then
            frmAddSelectContact.txtContactName(1).Text = Left(cmbContact.Text, InStr(cmbContact.Text, " ") - 1)
            frmAddSelectContact.txtContactName(2).Text = Right(cmbContact.Text, Len(cmbContact.Text) - InStr(cmbContact.Text, " "))
        End If
        frmAddSelectContact.txtContactName(3).Text = txtTelephone.Text
    End If
    
    frmAddSelectContact.lblCompanyID.Caption = lblCompanyID.Caption
    frmAddSelectContact.cmdLoadContacts.Value = True
    frmAddSelectContact.Show vbModal
    
    Dim l_lngContactID As Long
    l_lngContactID = frmAddSelectContact.Tag
    
    'if the user has not selected a valid contact...
    If l_lngContactID = "0" Then
        NoContactSelectedMessage
        cmbContact.SetFocus
        Exit Sub
    Else
        lblContactID.Caption = l_lngContactID
        cmbContact.Text = GetData("contact", "name", "contactID", l_lngContactID)
    End If
    
End If

'should there be a valid product selected?
If g_optRequireProductBeforeSaving <> 0 And Val(lblProductID.Caption) = 0 Then
    NoProductSelectedMessage
    cmbProduct.SetFocus
    Exit Sub
End If

If Left(LCase(cmbJobType.Text), 3) = "dub" Or grdDetail.Rows > 0 Then

    If g_optDeadlineTimeNotCompulsory = 0 Then
        If IsNull(datDeadlineDate.Value) Or cmbDeadlineTime.Text = "" Then
            MsgBox "You must enter a valid deadline for dubbing jobs", vbExclamation
            If datDeadlineDate.Enabled = True Then datDeadlineDate.SetFocus
            Exit Sub
        End If
    End If
End If

If cmbJobType.Text = "" Then
    MsgBox "You must select a job type before saving", vbExclamation
    cmbJobType.SetFocus
    Exit Sub
End If


If cmbAllocation.Text = "" Then
    MsgBox "You must select a project type before saving", vbExclamation
    cmbAllocation.SetFocus
    Exit Sub
End If

'dont prompt to hold cost for hire jobs
If UCase(cmbJobType.Text) <> "HIRE" Then
    If tabReplacementTabs.Caption = "Costing" Then
        If GetStatusNumber(txtStatus.Text) < 5 Then
            
            l_intMsg = MsgBox("Do you want to set this job to HOLD COST?", vbQuestion + vbYesNo)
            If l_intMsg = vbYes Then UpdateJobStatus txtJobID.Text, "Hold Cost", , True
        End If
    End If
End If

SaveJob

If txtClientStatus.Text <> "" And UCase(txtClientStatus.Text) <> "OK" Then
    MsgBox "WARNING: There is a problem with this customers account." & vbCrLf & vbCrLf & "Please check with your accounts department to find out why this customers account status is set to " & txtClientStatus.Text & "." & vbCrLf & vbCrLf & "The system has logged that you have read this message.", vbOKCancel + vbCritical
    AddJobHistory Val(txtJobID.Text), "Account Problem (" & txtClientStatus.Text & ")"
End If

If cmbDealType.Text = "F.O.C." Then

    If txtStatus.Text = "" Then txtStatus.Text = "Pencil"
    
    If GetData("job", "nochargerequestedreason", "jobID", Val(txtJobID.Text)) = "" Then
        NoChargeJob Val(txtJobID.Text)
    End If
    
    UpdateJobStatus Val(txtJobID.Text), txtStatus.Text, , True
    
    AddJobHistory Val(txtJobID.Text), "Reset status after no charge"
    
End If

ShowJob Val(txtJobID.Text), tab1.Tab, False
'ShowJob Val(txtJobID.Text), tab1.Tab, False

If grdDetail.Enabled = True And tab1.Tab = m_intTabJob Then
    grdDetail.SetFocus
End If
   

End Sub

Private Sub cmdAssignAllUnbilledItems_Click()

Dim l_lngRunningTime As Long, l_lngQuantity As Long, l_lngChargeRealMinutes As Long

adoUnbilled.Recordset.MoveFirst

Do While Not adoUnbilled.Recordset.EOF
    
    If Not IsNull(adoUnbilled.Recordset.Fields("runningtime").Value) Then
        l_lngRunningTime = adoUnbilled.Recordset.Fields("runningtime").Value
    Else
        l_lngRunningTime = 0
    End If
        
    l_lngQuantity = Val(adoUnbilled.Recordset.Fields("quantity").Value)
    l_lngChargeRealMinutes = 0
    
    MakeJobDetailLine Val(txtJobID.Text), adoUnbilled.Recordset.Fields("copytype").Value, adoUnbilled.Recordset.Fields("description").Value, _
        l_lngQuantity, adoUnbilled.Recordset.Fields("format").Value, l_lngRunningTime, _
        adoUnbilled.Recordset.Fields("completeddate").Value, adoUnbilled.Recordset.Fields("completeduser").Value, adoJobDetail.Recordset.RecordCount, _
        l_lngChargeRealMinutes, "", 0, "", 0, "", adoUnbilled.Recordset("portaluserID"), adoUnbilled.Recordset("skibblycustomerID"), adoUnbilled.Recordset("clipID")
    adoUnbilled.Recordset.MoveNext
Loop

adoUnbilled.Recordset.MoveFirst

Do While Not adoUnbilled.Recordset.EOF

    adoUnbilled.Recordset.Delete
    On Error GoTo LAST_ITEM
    adoUnbilled.Recordset.MoveNext
    
Loop

LAST_ITEM:

On Error GoTo 0

adoJobDetail.Refresh
adoUnbilled.Refresh

End Sub

Private Sub cmdClearJunk_Click()

Dim l_strSQL As String, Count As Long

l_strSQL = adoUnbilled.RecordSource

Count = InStr(l_strSQL, "WHERE")

If Count > 0 Then l_strSQL = Mid(l_strSQL, Count)

Count = InStr(l_strSQL, "ORDER")
If Count > 0 Then l_strSQL = Left(l_strSQL, Count - 1)

l_strSQL = "DELETE FROM jobdetailunbilled " & l_strSQL & " AND (finalstatus = 'error' OR finalstatus = 'cancelled');"
Debug.Print l_strSQL
ExecuteSQL l_strSQL, g_strExecuteError

adoUnbilled.Refresh

End Sub

Private Sub cmdSelectAllUnbilled_Click()

Dim l_strSQL As String, Count As Long

l_strSQL = adoUnbilled.RecordSource

Count = InStr(l_strSQL, "WHERE")

If Count > 0 Then l_strSQL = Mid(l_strSQL, Count)

Count = InStr(l_strSQL, "ORDER")
If Count > 0 Then l_strSQL = Left(l_strSQL, Count - 1)

If cmdSelectAllUnbilled.Caption = "Sel All" Then
    l_strSQL = "Update jobdetailunbilled SET SelectedForBilling = 1 " & l_strSQL
    cmdSelectAllUnbilled.Caption = "Clear All"
Else
    l_strSQL = "Update jobdetailunbilled SET SelectedForBilling = 0 " & l_strSQL
    cmdSelectAllUnbilled.Caption = "Sel All"
End If
Debug.Print l_strSQL
ExecuteSQL l_strSQL, g_strExecuteError

adoUnbilled.Refresh

End Sub

Private Sub cmdTransferLinesFromOtherJob_Click()

If Not CheckAccess("/transferlinesfromotherjob") Then
    Exit Sub
End If

If Val(txtJobID.Text) = 0 Then
    MsgBox "No Job Selected"
    Exit Sub
End If

Dim l_lngOtherJobID As Long
Dim l_lngCountThisJobLines As Long
Dim l_lngCountOtherJobLines As Long
Dim l_lngCountOtherCostingLines As Long
Dim l_strSQL As String
Dim l_rsJobLines As ADODB.Recordset

l_lngOtherJobID = Val(InputBox("Please enter the other Job Number", "Transfer Detail Lines from Other Job", 0))
If l_lngOtherJobID <> 0 Then
    If Val(Trim(" " & GetData("job", "companyID", "jobID", l_lngOtherJobID))) = Val(lblCompanyID.Caption) Then
        If GetStatusNumber(txtStatus.Text) <= 8 Then
            If GetStatusNumber(GetData("job", "fd_status", "jobID", l_lngOtherJobID)) <= 8 Then
                l_lngCountOtherJobLines = GetCount("SELECT Count(jobdetailID) FROM jobdetail WHERE jobID = " & l_lngOtherJobID)
                If l_lngCountOtherJobLines <> 0 Then
                    If MsgBox(l_lngCountOtherJobLines & " job detail lines found." & vbCrLf & "Proceed to transfer these lines to this job?", vbYesNo + vbDefaultButton2) = vbYes Then
                        l_lngCountThisJobLines = adoJobDetail.Recordset.RecordCount
                        l_strSQL = "DELETE from costing WHERE jobID = " & l_lngOtherJobID
                        Debug.Print l_strSQL
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                        l_strSQL = "SELECT * FROM jobdetail WHERE jobID = " & l_lngOtherJobID
                        Debug.Print l_strSQL
                        Set l_rsJobLines = ExecuteSQL(l_strSQL, g_strExecuteError)
                        If l_rsJobLines.RecordCount > 0 Then
                            l_lngCountOtherJobLines = l_lngCountThisJobLines
                            l_rsJobLines.MoveFirst
                            Do While Not l_rsJobLines.EOF
                                l_rsJobLines("jobID") = Val(txtJobID.Text)
                                l_rsJobLines("fd_OrderBy") = l_lngCountOtherJobLines
                                l_rsJobLines.Update
                                l_rsJobLines.MoveNext
                                l_lngCountOtherJobLines = l_lngCountOtherJobLines + 1
                            Loop
                        End If
                        l_rsJobLines.Close
                        Set l_rsJobLines = Nothing
                        SetData "job", "fd_status", "jobID", l_lngOtherJobID, "Cancelled"
                        SetData "job", "cancelleddate", "jobID", l_lngOtherJobID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                        SetData "job", "cancelleduser", "jobID", l_lngOtherJobID, g_strUserName
                        MsgBox "Done"
                        ShowJob txtJobID.Text, 0, False
                    Else
                        MsgBox "Transfer of Detail Lines Aborted"
                    End If
                Else
                    MsgBox "No lines found on that job." & vbCrLf & "Transfer of Detail Lines Aborted"
                End If
            Else
                MsgBox "Status of othere job must less than 'Hold Cost'" & vbCrLf & "Transfer of Detail Lines Aborted"
            End If
        Else
            MsgBox "Status of this job must less than 'Hold Cost'" & vbCrLf & "Transfer of Detail Lines Aborted"
        End If
    Else
        MsgBox "The Jobs must belong to the same company" & vbCrLf & "Transfer of Detail Lines Aborted"
    End If
Else
    MsgBox "Transfer of Detail Lines Aborted"
End If

End Sub

Private Sub ddnCompanyContact_CloseUp()

grdJobContacts.Columns("companycontactID").Text = ddnCompanyContact.Columns("companycontactID").Text
grdJobContacts.Columns("name").Text = ddnCompanyContact.Columns("name").Text
grdJobContacts.Columns("email").Text = ddnCompanyContact.Columns("email").Text
grdJobContacts.Columns("companyID").Text = ddnCompanyContact.Columns("CompanyID").Text

End Sub

Private Sub ddnCompanyContact_DropDown()

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

If lblCompanyID.Caption = "" Then
    NoCompanySelectedMessage
    Exit Sub
End If

Dim l_strSQL As String

l_strSQL = "SELECT * FROM companycontact WHERE companyID = " & Val(lblCompanyID.Caption) & " ORDER BY name;"
adoCompanyContact.RecordSource = l_strSQL
adoCompanyContact.ConnectionString = g_strConnection
adoCompanyContact.Refresh

End Sub

Private Sub ddnDigital_ValidateList(Text As String, RtnPassed As Integer)
    If Text = "" Then Exit Sub

    Dim l_strSQL As String
    l_strSQL = "SELECT description, format FROM xref WHERE category = 'digitaloptions' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"
    
    Dim l_rstXRefRatio As ADODB.Recordset
    Set l_rstXRefRatio = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError

    If l_rstXRefRatio.EOF Then
        'Code note found
        MsgBox "Cannot find this description", 64
        RtnPassed = False
    Else
        l_rstXRefRatio.MoveFirst
        RtnPassed = True
        Text = l_rstXRefRatio("description")
        grdDetail.Columns(grdDetail.Col).Text = Text
        grdDetail.Columns("format").Text = Format(l_rstXRefRatio("format"))
    End If
    
    l_rstXRefRatio.Close
    Set l_rstXRefRatio = Nothing
    
    If grdDetail.Columns("quantity").Text = "" Then grdDetail.Columns("quantity").Text = "1"
    
End Sub

Private Sub ddnFormat_ValidateList(Text As String, RtnPassed As Integer)
    If Text = "" Then Exit Sub
    Dim l_strSQL As String
    
      
    If g_optUseSpecialDubbingComboOptions = 1 Then
        l_strSQL = "SELECT description, information FROM xref WHERE category = 'dformat' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"
    Else
        l_strSQL = "SELECT description, information FROM xref WHERE category = 'format' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"
    End If
   
    
    Dim l_rstXRefFormat As ADODB.Recordset
    Set l_rstXRefFormat = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError

    If l_rstXRefFormat.EOF Then
        'Code note found
        MsgBox "Cannot find this description", 64
        RtnPassed = False
    Else
        l_rstXRefFormat.MoveFirst
        RtnPassed = True
        Text = l_rstXRefFormat("description")
        grdDetail.Columns(grdDetail.Col).Text = Text
        Text = Trim(" " & l_rstXRefFormat("information"))
        grdDetail.Columns("workflowdescription").Text = Text
    End If
    
    l_rstXRefFormat.Close
    Set l_rstXRefFormat = Nothing
    

End Sub

Private Sub ddnInclusive_CloseUp()

grdDetail.Columns("format").Text = ddnInclusive.Columns("format").Text
grdDetail.Columns("workflowdescription").Text = ddnInclusive.Columns("description").Text

End Sub

Private Sub ddnOthers_ValidateList(Text As String, RtnPassed As Integer)

    If Text = "" Then Exit Sub

    Dim l_strSQL As String
    l_strSQL = "SELECT description, format FROM xref WHERE category = 'other' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"
    
    Dim l_rstXRefRatio As ADODB.Recordset
    Set l_rstXRefRatio = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError

    If l_rstXRefRatio.EOF Then
        'Code note found
        MsgBox "Cannot find this description", 64
        RtnPassed = False
    Else
        l_rstXRefRatio.MoveFirst
        RtnPassed = True
        Text = l_rstXRefRatio("description")
        grdDetail.Columns("workflowdescription").Text = Text
        grdDetail.Columns("format").Text = Trim(" " & l_rstXRefRatio("format"))
    End If
    
    l_rstXRefRatio.Close
    Set l_rstXRefRatio = Nothing
    
    If grdDetail.Columns("quantity").Text = "" Then grdDetail.Columns("quantity").Text = "1"
End Sub

Private Sub ddnPlayouts_ValidateList(Text As String, RtnPassed As Integer)
    If Text = "" Then Exit Sub

    Dim l_strSQL As String
    l_strSQL = "SELECT description, format FROM xref WHERE category = 'playoutoptions' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"
    
    Dim l_rstXRefRatio As ADODB.Recordset
    Set l_rstXRefRatio = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError

    If l_rstXRefRatio.EOF Then
        'Code note found
        MsgBox "Cannot find this description", 64
        RtnPassed = False
    Else
        l_rstXRefRatio.MoveFirst
        RtnPassed = True
        Text = l_rstXRefRatio("description")
        grdDetail.Columns(grdDetail.Col).Text = Text
        grdDetail.Columns("format").Text = Format(l_rstXRefRatio("format"))
    End If
    
    l_rstXRefRatio.Close
    Set l_rstXRefRatio = Nothing
    
    If grdDetail.Columns("quantity").Text = "" Then grdDetail.Columns("quantity").Text = "1"
    
End Sub

Private Sub ddnRateCard_Click()

'AddLineToCosting ddnRateCard.Columns("cetacode").Text

    Dim l_lngCompanyID As Long
    Dim l_dblUnitCharge As Double
    Dim l_sngVATRate As Single, l_intRatecard As Integer, l_intTime As Integer, l_dblVAT As Double, l_dblTotal As Double, l_dblTotalIncVAT As Double
    Dim l_dblRateCardPrice As Double, l_dblMinimumCharge As Double, l_dblRateCardMinimumCharge As Double, l_dblDiscount As Double
    
    l_lngCompanyID = GetData("job", "companyID", "jobID", txtJobID.Text)
    l_sngVATRate = GetVATRate(Format(GetData("company", "vatcode", "companyID", l_lngCompanyID)))
    l_intRatecard = Val(GetData("company", "ratecard", "companyID", l_lngCompanyID))
    
    Dim l_intDoNotChargeVAT As Integer
    l_intDoNotChargeVAT = GetData("job", "donotchargevat", "jobID", txtJobID.Text)
    
    If l_intDoNotChargeVAT <> 0 Then
        l_sngVATRate = 0
    End If

    If l_intRatecard > 9 Or l_intRatecard < 1 Then l_intRatecard = 1
    
    Dim l_strCETACostCode As String, l_strPriorityCode As String
    l_strCETACostCode = grdCostingDetail.Columns("code").Text
    
    'l_dblUnitCharge = GetUnitCharge(l_intRatecard, l_strCETACostCode, l_lngCompanyID)
    
    l_dblUnitCharge = GetPrice(l_strCETACostCode, l_intRatecard, l_lngCompanyID, "", Val(lblProductID.Caption))
    l_dblMinimumCharge = GetMinimumCharge(l_strCETACostCode, Val(lblCompanyID.Caption))
    l_dblRateCardMinimumCharge = GetMinimumCharge(l_strCETACostCode, 0)
    l_dblDiscount = GetCompanyDiscount(Val(lblCompanyID.Caption), l_strCETACostCode)
    
    l_dblRateCardPrice = GetUnitCharge(g_intBaseRateCard, l_strCETACostCode, l_lngCompanyID)

    'grdCostingDetail.Columns("code").Text = l_strCETACostCode
    grdCostingDetail.Columns("unitcharge").Text = l_dblUnitCharge
    grdCostingDetail.Columns("minimumcharge").Text = l_dblMinimumCharge
    grdCostingDetail.Columns("ratecardminimumcharge").Text = l_dblRateCardMinimumCharge
    
    If grdCostingDetail.Columns("quantity").Value = "" Then grdCostingDetail.Columns("quantity").Text = 1
    If grdCostingDetail.Columns("time").Value = "" Then grdCostingDetail.Columns("time").Text = g_intDefaultTimeWhenZero
    
    l_dblTotal = Val(l_dblUnitCharge * Val(grdCostingDetail.Columns("quantity").Text) * Val(grdCostingDetail.Columns("time").Text))
    If l_dblTotal < l_dblMinimumCharge Then l_dblTotal = l_dblMinimumCharge
    If l_dblDiscount <> 0 Then l_dblTotal = l_dblTotal * (100 - l_dblDiscount) / 100
    l_dblTotal = RoundToCurrency(l_dblTotal)
    grdCostingDetail.Columns("total").Text = l_dblTotal
    
    grdCostingDetail.Columns("vat").Text = Val(l_dblUnitCharge * Val(grdCostingDetail.Columns("quantity").Text) * Val(grdCostingDetail.Columns("time").Text)) * (l_sngVATRate / 100)
    grdCostingDetail.Columns("totalincludingvat").Text = Val(grdCostingDetail.Columns("total").Text) + Val(grdCostingDetail.Columns("vat").Text)
    
    grdCostingDetail.Columns("ratecardprice").Text = l_dblRateCardPrice
    grdCostingDetail.Columns("ratecardtotal").Text = Val(l_dblRateCardPrice * Val(grdCostingDetail.Columns("quantity").Text) * Val(grdCostingDetail.Columns("time").Text))
    grdCostingDetail.Columns("ratecardcategory").Text = GetData("ratecard", "category", "cetacode", l_strCETACostCode)
    grdCostingDetail.Columns("ratecardorderby").Text = Val(GetData("ratecard", "fd_orderby", "cetacode", l_strCETACostCode))

    grdCostingDetail.Columns("accountcode").Text = GetData("ratecard", "nominalcode", "cetacode", l_strCETACostCode)
    If UCase(Left(l_strCETACostCode, 3)) <> "DEL" Or grdCostingDetail.Columns("description").Text = "" Then grdCostingDetail.Columns("description").Text = GetData("ratecard", "description", "cetacode", l_strCETACostCode)
    grdCostingDetail.Columns("code").Text = UCase(grdCostingDetail.Columns("code").Text)
    
    grdCostingDetail.Columns("unit").Text = GetData("ratecard", "unit", "cetacode", l_strCETACostCode)
    l_strPriorityCode = GetDataSQL("SELECT TOP 1 VDMS_Charge_Code from ratecard where cetacode = '" & l_strCETACostCode & "' AND companyID = " & GetData("job", "companyID", "jobID", Val(txtJobID.Text)))
    If l_strPriorityCode = "" Then l_strPriorityCode = GetData("ratecard", "VDMS_Charge_Code", "cetacode", l_strCETACostCode)
    grdCostingDetail.Columns("VDMS_Charge_Code").Text = l_strPriorityCode

End Sub



Private Sub ddnRateCard_ValidateList(Text As String, RtnPassed As Integer)

 If Text = "" Then Exit Sub
    Dim l_strSQL As String
    
    l_strSQL = "SELECT cetacode FROM ratecard WHERE cetacode = '" & Text & "';"
    
    Dim l_rstXRefFormat As ADODB.Recordset
    Set l_rstXRefFormat = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError

    If l_rstXRefFormat.EOF Then
        'Code note found
        MsgBox "Cannot find this code", 64
        RtnPassed = False
    Else
        l_rstXRefFormat.MoveFirst
        RtnPassed = True
        Text = UCase(l_rstXRefFormat("cetacode"))
        grdCostingDetail.Columns(grdCostingDetail.Col).Text = Text
        
        ddnRateCard_Click
    End If
    
    l_rstXRefFormat.Close
    Set l_rstXRefFormat = Nothing
    

End Sub

Private Sub ddnRatio_ValidateList(Text As String, RtnPassed As Integer)
    If Text = "" Then Exit Sub
    Dim l_strSQL As String
    
    If g_optUseSpecialDubbingComboOptions = 1 Then
        l_strSQL = "SELECT description FROM xref WHERE category = 'daspectratio' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"
    Else
        l_strSQL = "SELECT description FROM xref WHERE category = 'aspectratio' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"
    End If
    
    Dim l_rstXRefRatio As ADODB.Recordset
    Set l_rstXRefRatio = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError

    If l_rstXRefRatio.EOF Then
        'Code note found
        MsgBox "Cannot find this description", 64
        RtnPassed = False
    Else
        l_rstXRefRatio.MoveFirst
        RtnPassed = True
        Text = l_rstXRefRatio("description")
        grdDetail.Columns(grdDetail.Col).Text = Text
    End If
    
    l_rstXRefRatio.Close
    Set l_rstXRefRatio = Nothing
End Sub




Private Sub ddnSoundChannel_ValidateList(Text As String, RtnPassed As Integer)
    If Text = "" Then Exit Sub
    Dim l_strSQL As String
    
    If g_optUseSpecialDubbingComboOptions = 1 Then
        l_strSQL = "SELECT description FROM xref WHERE category = 'dsoundchannel' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"
    Else
        l_strSQL = "SELECT description FROM xref WHERE category = 'soundchannel' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"
    End If
    
    
    Dim l_rstXRefSoundChannel As ADODB.Recordset
    Set l_rstXRefSoundChannel = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError

    If l_rstXRefSoundChannel.EOF Then
        'Code note found
        MsgBox "Cannot find this description", 64
        RtnPassed = False
    Else
        l_rstXRefSoundChannel.MoveFirst
        RtnPassed = True
        Text = l_rstXRefSoundChannel("description")
        grdDetail.Columns(grdDetail.Col).Text = Text
    End If
    
    l_rstXRefSoundChannel.Close
    Set l_rstXRefSoundChannel = Nothing
End Sub

Private Sub ddnStandard_ValidateList(Text As String, RtnPassed As Integer)

If Text = "" Then Exit Sub

Dim l_strSQL As String

If g_optUseSpecialDubbingComboOptions = 1 Then
    l_strSQL = "SELECT description FROM xref WHERE category = 'dvideostandard' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"

Else
    l_strSQL = "SELECT description FROM xref WHERE category = 'videostandard' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"

End If




Dim l_rstXRefStandard As ADODB.Recordset
Set l_rstXRefStandard = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If l_rstXRefStandard.EOF Then
    'Code note found
    MsgBox "Cannot find this description", 64
    RtnPassed = False
Else
    l_rstXRefStandard.MoveFirst
    RtnPassed = True
    Text = l_rstXRefStandard("description")
    grdDetail.Columns(grdDetail.Col).Text = Text
End If

l_rstXRefStandard.Close
Set l_rstXRefStandard = Nothing

End Sub

Private Sub ddnStockType_ValidateList(Text As String, RtnPassed As Integer)
    If Text = "" Then Exit Sub

    Dim l_strSQL As String
    l_strSQL = "SELECT description FROM xref WHERE category = 'stockcode' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"
    
    Dim l_rstXRefStockType As ADODB.Recordset
    Set l_rstXRefStockType = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError

    If l_rstXRefStockType.EOF Then
        'Code note found
        MsgBox "Cannot find this description", 64
        RtnPassed = False
    Else
        l_rstXRefStockType.MoveFirst
        RtnPassed = True
        Text = l_rstXRefStockType("description")
        grdDetail.Columns(grdDetail.Col).Text = Text
    End If
    
    l_rstXRefStockType.Close
    Set l_rstXRefStockType = Nothing
End Sub








Private Sub ddnTimeCode_ValidateList(Text As String, RtnPassed As Integer)
    If Text = "" Then
        Exit Sub
    End If
    
    Dim l_strSQL As String

    If g_optUseSpecialDubbingComboOptions = 1 Then
        l_strSQL = "SELECT description FROM xref WHERE category = 'dtimecode' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"
    Else
        l_strSQL = "SELECT description FROM xref WHERE category = 'timecode' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"
    End If
     
     
    Dim l_rstXRefStockType As ADODB.Recordset
    Set l_rstXRefStockType = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError

    If l_rstXRefStockType.EOF Then
        'Code note found
        MsgBox "Cannot find this description", 64
        RtnPassed = False
    Else
        l_rstXRefStockType.MoveFirst
        RtnPassed = True
        Text = l_rstXRefStockType("description")
        grdDetail.Columns(grdDetail.Col).Text = Text
    End If
    
    l_rstXRefStockType.Close
    Set l_rstXRefStockType = Nothing
End Sub

Private Sub Form_Activate()

'if there is a loaded job then refresh that jobs details
If Val(txtJobID.Text) <> 0 And frmJob.Tag <> "LOADING" Then ''''

    'keep the tab we are on
    Dim l_intCurrentTab As Integer
    
    'store it
    l_intCurrentTab = tab1.Tab
    
    'show the job
    ShowJob CLng(txtJobID.Text), l_intCurrentTab, False
'    ShowJob CLng(txtJobID.Text), l_intCurrentTab, False
    
    'adoCosting.ConnectionString = g_strConnection
    'adoCosting.RecordSource = "SELECT * FROM costing WHERE jobID = -1"
    'adoCosting.Refresh

End If

If Not CheckAccess("/showproducernotes", True) Then
    txtProducerNotes.Visible = False
    lblCaption(11).Visible = False
    
Else
    txtProducerNotes.Visible = True
    lblCaption(11).Visible = True
End If

If Not CheckAccess("/transferlinesfromotherjob", True) Then
    cmdTransferLinesFromOtherJob.Visible = False
Else
    cmdTransferLinesFromOtherJob.Visible = True
End If

DoEvents

On Error Resume Next

txtJobID.SetFocus

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

m_intClearJunk = 0

tabReplacementTabs.Top = 2580
tabReplacementTabs.Left = 60

'populate all the other combos, including ones bound to the grid

'these are on the grids
If g_optUseSpecialDubbingComboOptions = 1 Then
    PopulateCombo "dformat", ddnFormat
    PopulateCombo "dvideostandard", ddnStandard
    PopulateCombo "dsoundchannel", ddnSoundChannel
    PopulateCombo "daspectratio", ddnRatio
    PopulateCombo "dtimecode", ddnTimeCode
Else
    PopulateCombo "format", ddnFormat
    PopulateCombo "videostandard", ddnStandard
    PopulateCombo "soundchannel", ddnSoundChannel
    PopulateCombo "aspectratio", ddnRatio
    PopulateCombo "timecode", ddnTimeCode
End If

PopulateCombo "copytypeoptions", ddnCopyList
PopulateCombo "digitaloptions", ddnDigital
PopulateCombo "other", ddnOthers
PopulateCombo "playoutoptions", ddnPlayouts
PopulateCombo "unit", ddnUnit
PopulateCombo "ourcontact", cmbOurContact
PopulateCombo "stockcode", ddnStockType
PopulateCombo "discstores", ddnDiscStores
grdDetail.Columns("MasterStore").DropDownHwnd = ddnDiscStores.hWnd

l_strSQL = "SELECT description, format FROM xref WHERE category = 'inclusive' ORDER BY forder, format"
adoInclusive.ConnectionString = g_strConnection
adoInclusive.RecordSource = l_strSQL
adoInclusive.Refresh

If g_optHideOrderByColumnOnCostingGrid = 1 Then
    lblCaption(85).Visible = False
    grdCostingDetail.Top = 420
End If


If g_optHideVATFieldsInCostings = 1 Then
    lblCaption(18).Visible = False
    txtTotalIncVAT.Visible = False
    grdCostingDetail.Columns("vat").Visible = False
    grdCostingDetail.Columns("totalincludingvat").Visible = False
End If

'lblCaption(58).Caption = g_strCurrency

If g_optHideHireDespatchTab <> 0 Then tabReplacementTabs.TabVisible(8) = False
If g_optRequireProductBeforeSaving <> 0 Then lblCaption(46).FontBold = True
If g_optRequireOurContactBeforeSaving <> 0 Then lblCaption(12).FontBold = True

If g_intHideVideoStandardOnJobForm <> 0 Then
    lblCaption(44).Visible = False
    cmbVideoStandard.Visible = False
End If

If chkEnforceContacts.Value <> 0 Then grdJobContacts.Columns("email").DropDownHwnd = ddnCompanyContact.hWnd

grdDetail.Columns("clipID").Caption = g_strClipIDCaption

l_strSQL = "SELECT name, accountcode, telephone, companyID, fax, accountstatus, email FROM company WHERE (issupplier = 1) AND system_active = 1 ORDER BY name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

l_conSearch.Close
Set l_conSearch = Nothing

cmbAllocation_Click

'center the form
CenterForm Me

End Sub

Private Sub Form_Resize()

On Error Resume Next

If Me.WindowState = vbMinimized Then Exit Sub

If Me.Height < 9315 Then
    Me.Height = 9315
End If

If Me.Width < 14760 Then
    Me.Width = 14760
End If

picFooter.Top = Me.ScaleHeight - picFooter.Height - 120
picFooter.Left = Me.ScaleWidth - picFooter.Width - 120

tab1.Height = Me.ScaleHeight - picFooter.Height - 120 - 120 - tab1.Top
tab1.Width = Me.ScaleWidth - 120
tabReplacementTabs.Width = tab1.Width

grdDetail.Width = tab1.Width - 120 - 120
grdDetail.Height = tab1.Height - 120 - grdDetail.Top
grdDetailCopy.Height = tab1.Height - 120 - grdDetailCopy.Top
grdUnbilled.Height = tab1.Height - 120 - grdDetailCopy.Top
grdDetailAnotherCopy.Height = tab1.Height - 120 - grdDetailCopy.Top
grdProgressItems.Height = (tab1.Height - grdDetailCopy.Top) - 120

grdJobContacts.Height = tab1.Height - 120 - grdDetail.Top

grdExtendedJobDetail.Width = tab1.Width - 120 - 120 - 120
grdExtendedJobDetail.Height = tab1.Height - 120 - grdExtendedJobDetail.Top

picCostingFooter.Top = tab1.Height - picCostingFooter.Height - 120
picCostingFooter.Left = tab1.Width - picCostingFooter.Width - 120

picCostingTotals.Top = picCostingFooter.Top - picCostingTotals.Height - 120
picCostingTotals.Left = tab1.Width - picCostingTotals.Width - 120

'picTotalCredits.Top = picCreditFooter.Top - picTotalCredits.Height - 120
'picTotalCredits.Left = tab1.Width - picTotalCredits.Width - 120

txtAccounts.Top = picCostingTotals.Top + txtTotal.Top
txtAccounts.Width = tab1.Width - picCostingFooter.Width - 120 - 120 - 120

lblCaption(22).Top = picCostingTotals.Top + lblCaption(17).Top

grdCostingDetail.Width = tab1.Width - 120 - 120
grdCostingDetail.Height = picCostingTotals.Top - 120 - grdCostingDetail.Top

'grdCreditNote.Width = tab1.Width - 120 - 120
'grdCreditNote.Height = picTotalCredits.Top - 120 - grdCreditNote.Top

grdMedia.Width = (tab1.Width - 360) / 2
grdMedia.Height = (tab1.Height - grdMedia.Top - 360) / 3

grdTechReviewMedia.Visible = False
grdTechReviewMedia.Top = grdMedia.Top
grdTechReviewMedia.Left = grdMedia.Left + grdMedia.Width + 120
grdTechReviewMedia.Height = grdMedia.Height
grdTechReviewMedia.Width = grdMedia.Width
grdTechReviewMedia.Visible = True

grdMediaFiles.Visible = False
grdMediaFiles.Top = grdMedia.Top + grdMedia.Height + 120
grdMediaFiles.Width = grdMedia.Width
grdMediaFiles.Height = grdMedia.Height
grdMediaFiles.Left = grdMedia.Left
grdMediaFiles.Visible = True

grdTechReviewMediaFiles.Visible = False
grdTechReviewMediaFiles.Top = grdMedia.Top + grdMedia.Height + 120
grdTechReviewMediaFiles.Left = grdMedia.Left + grdMedia.Width + 120
grdTechReviewMediaFiles.Width = grdMedia.Width
grdTechReviewMediaFiles.Height = grdMedia.Height
grdTechReviewMediaFiles.Visible = True

grdRequiredMedia.Visible = False
grdRequiredMedia.Top = grdMediaFiles.Top + grdMediaFiles.Height + 120
grdRequiredMedia.Width = grdMedia.Width
grdRequiredMedia.Height = grdMedia.Height
grdRequiredMedia.Visible = True

grdRequiredFiles.Visible = False
grdRequiredFiles.Width = grdMedia.Width
grdRequiredFiles.Height = grdMedia.Height
grdRequiredFiles.Top = grdRequiredMedia.Top
grdRequiredFiles.Left = grdRequiredMedia.Left + grdRequiredMedia.Width + 120
grdRequiredFiles.Visible = True

'grdSundryCosts.Width = tab1.Width - 120 - 120
grdSundryCosts.Height = tab1.Height - 120 - grdSundryCosts.Top

grdHistory.Width = tab1.Width - 120 - 120
grdHistory.Height = tab1.Height - 120 - grdHistory.Top

'grdQuoteOverview.Height = SSTab1.Height - 240

If g_intDisableJobOperatorCommentsOnDubbingTab = 0 Then
    grdDetail.Height = tab1.Height - 120 - grdDetail.Top - picScheduleFooter(1).Height - 120
    picScheduleFooter(1).Top = tab1.Height - picScheduleFooter(1).Height - 120
End If

'CRViewer91.Width = tab1.Width - 60 - 120 - CRViewer91.Left
'CRViewer91.Height = tab1.Height - 120 - CRViewer91.Top

lblTwoTier.Top = picFooter.Top
lblTwoTier.Left = picFooter.Left - lblTwoTier.Width - 120

DoEvents

End Sub

Private Sub Form_Unload(Cancel As Integer)

'Me.Hide
'DoEvents

'Cancel = True
cmdClose_Click

End Sub

Private Sub GetTotals()

txtTotal.Text = g_strCurrency & Format(GetTotalCostsForJob(Val(txtJobID.Text), "total", "INVOICE"), "CURRENCY")
txtTotalIncVAT.Text = g_strCurrency & Format(GetTotalCostsForJob(Val(txtJobID.Text), "totalincludingvat", "INVOICE"), "CURRENCY")
frmJob.txtRateCardTotal.Text = g_strCurrency & Format(RoundToCurrency(GetTotalCostsForJob(Val(txtJobID.Text), "ratecardtotal", "INVOICE")), "0.00#")

End Sub

Private Sub grdCostingDetail_AfterColUpdate(ByVal ColIndex As Integer)

Dim l_strCETACostCode       As String
Dim l_dblUnitCharge         As Double
Dim l_dblTotal              As Double
Dim l_dblQuantity           As Double
Dim l_dblTime               As Double
Dim l_dblDiscount           As Double
Dim l_dblRateCardPrice      As Double
Dim l_dblRateCardTotal      As Double
Dim l_dblNewTotal           As Double
Dim l_dblMinimumCharge      As Double
Dim l_dblDiscountAmount     As Double
Dim l_dblRateCardMinimumCharge As Double
Dim l_strRateCardCategory   As String
Dim l_intRateCardOrderBy    As Integer
Dim l_strAccountCode        As String
Dim l_strSQL                As String
Dim l_strUnit               As String
Dim l_strDescription        As String
Dim l_intRatecard           As Integer
Dim l_dblAdditionalDiscount As Double

'get the basics from the grid
l_strCETACostCode = grdCostingDetail.Columns("costcode").Text

If l_strCETACostCode <> "" Then
    
    'pick up the ratecard defaults
    l_strAccountCode = GetData("ratecard", "nominalcode", "cetacode", l_strCETACostCode)
    l_strRateCardCategory = GetData("ratecard", "category", "cetacode", l_strCETACostCode)
    l_intRateCardOrderBy = GetData("ratecard", "fd_orderby", "cetacode", l_strCETACostCode)
    l_dblRateCardPrice = CDbl(GetData("ratecard", CStr("price" & g_intBaseRateCard), "cetacode", l_strCETACostCode))
    l_intRateCardOrderBy = GetData("ratecard", "fd_orderby", "cetacode", l_strCETACostCode)
    l_strUnit = GetData("ratecard", "unit", "cetacode", l_strCETACostCode)
    l_strDescription = GetData("ratecard", "description", "cetacode", l_strCETACostCode)
    l_dblDiscount = GetCompanyDiscount(Val(lblCompanyID.Caption), l_strCETACostCode)
    
End If

Dim l_intDoNotChargeVAT As Integer
l_intDoNotChargeVAT = GetData("job", "donotchargevat", "jobID", Val(txtJobID.Text))

Dim l_sngVATRate As Single
If l_intDoNotChargeVAT = 1 Then
    l_sngVATRate = 0
Else
    l_sngVATRate = GetVATRate(Format(GetData("company", "vatcode", "companyID", Val(lblCompanyID.Caption))))
End If
    
l_dblQuantity = Val(grdCostingDetail.Columns("quantity").Text)
l_dblTime = Val(grdCostingDetail.Columns("fd_time").Text)
If l_dblTime = 0 Then l_dblTime = g_intDefaultTimeWhenZero
l_strUnit = grdCostingDetail.Columns("unit").Text
l_dblUnitCharge = Val(grdCostingDetail.Columns("unitcharge").Text)
l_dblMinimumCharge = Val(grdCostingDetail.Columns("minimumcharge").Text)
l_dblRateCardMinimumCharge = Val(grdCostingDetail.Columns("ratecardminimumcharge").Text)
l_dblAdditionalDiscount = Val(grdCostingDetail.Columns("AdditionalDiscount").Text)

Select Case grdCostingDetail.Columns(ColIndex).Name

Case "costcode", "cetacostcode", "unit"

    'If Val(grdCostingDetail.Columns("quantity").Text) = 0 Then grdCostingDetail.Columns("quantity").Text = 1
    
    'find out which rate card they use
    l_intRatecard = Val(GetData("company", "ratecard", "companyID", Val(lblCompanyID.Caption)))
    
    'make sure we use at least the bas rate card
    If l_intRatecard > 9 Or l_intRatecard < 1 Then
        l_intRatecard = g_intBaseRateCard
    End If

    l_dblUnitCharge = GetUnitCharge(l_intRatecard, l_strCETACostCode, Val(lblCompanyID.Caption))
    l_dblMinimumCharge = GetMinimumCharge(l_strCETACostCode, Val(lblCompanyID.Caption))
    l_dblRateCardPrice = CDbl(GetData("ratecard", CStr("price" & g_intBaseRateCard), "cetacode", l_strCETACostCode))
    l_dblRateCardMinimumCharge = GetMinimumCharge(l_strCETACostCode, 0)

    l_strDescription = grdCostingDetail.Columns("description").Text
    
    l_intRateCardOrderBy = grdCostingDetail.Columns("ratecardorderby").Text
    
    If l_dblQuantity = 0 Then l_dblQuantity = 1
    
    If l_strUnit = "M" Then
        l_dblTotal = l_dblQuantity * l_dblTime * l_dblUnitCharge
    ElseIf g_optDontMakeMachineTimeADecimal = 1 Then
        l_dblTotal = l_dblQuantity * l_dblTime * l_dblUnitCharge / 60
    Else
        l_dblTotal = l_dblQuantity * l_dblTime * l_dblUnitCharge
    End If
    
    If l_dblTotal >= 0 Then If l_dblTotal < l_dblMinimumCharge Then l_dblTotal = l_dblMinimumCharge
    
    If l_dblDiscount <> 0 Then l_dblTotal = l_dblTotal * (100 - l_dblDiscount) / 100
    l_dblTotal = RoundToCurrency(l_dblTotal)
    l_dblTotal = RoundToCurrency(l_dblTotal - l_dblAdditionalDiscount)

    'update the text accordingly
    grdCostingDetail.Columns("unitcharge").Text = RoundToCurrency(l_dblUnitCharge)
    grdCostingDetail.Columns("minimumcharge").Text = l_dblMinimumCharge
    grdCostingDetail.Columns("ratecardminimumcharge").Text = l_dblRateCardMinimumCharge
    grdCostingDetail.Columns("total").Text = RoundToCurrency(l_dblTotal)
    grdCostingDetail.Columns("VAT").Text = RoundToCurrency(l_dblTotal * (l_sngVATRate / 100))
    grdCostingDetail.Columns("totalincludingvat").Text = l_dblTotal + RoundToCurrency(l_dblTotal * (l_sngVATRate / 100))
    grdCostingDetail.Columns("ratecardprice").Text = l_dblRateCardPrice
    grdCostingDetail.Columns("discount").Text = l_dblDiscount
    If l_dblDiscount <> 0 Then
        grdCostingDetail.Columns("effectiveunit").Text = l_dblUnitCharge * (1 - (l_dblDiscount / 100))
    Else
        grdCostingDetail.Columns("effectiveunit").Text = l_dblUnitCharge
    End If

    If l_strUnit = "M" Then
        l_dblRateCardTotal = l_dblQuantity * l_dblTime * l_dblRateCardPrice
    ElseIf g_optDontMakeMachineTimeADecimal = 1 Then
        l_dblRateCardTotal = (l_dblRateCardPrice * l_dblTime * l_dblQuantity) / 60
    Else
        l_dblRateCardTotal = l_dblRateCardPrice * l_dblTime * l_dblQuantity
    End If
    
    If l_dblRateCardTotal < l_dblRateCardMinimumCharge Then l_dblRateCardTotal = l_dblRateCardMinimumCharge
    
    grdCostingDetail.Columns("ratecardtotal").Text = l_dblRateCardTotal
    
    grdCostingDetail.Columns("ratecardorderby").Text = l_intRateCardOrderBy
    grdCostingDetail.Columns("ratecardcategory").Text = l_strRateCardCategory
    
Case "quantity", "fd_time"

    l_dblUnitCharge = Val(grdCostingDetail.Columns("unitcharge").Text)
    l_dblTotal = Val(l_dblUnitCharge) * Val(l_dblTime) * Val(l_dblQuantity)
    l_dblDiscount = Val(grdCostingDetail.Columns("discount").Text)
    
    If l_strUnit = "M" Then
        l_dblTotal = l_dblQuantity * l_dblTime * l_dblUnitCharge
    ElseIf g_optDontMakeMachineTimeADecimal = 1 Then
        l_dblTotal = l_dblQuantity * l_dblTime * l_dblUnitCharge / 60
    Else
        l_dblTotal = l_dblQuantity * l_dblTime * l_dblUnitCharge
    End If
    
    If l_dblTotal >= 0 Then If l_dblTotal < l_dblMinimumCharge Then l_dblTotal = l_dblMinimumCharge
    
    l_dblDiscountAmount = (l_dblTotal / 100) * l_dblDiscount
    
    'calculate new values
    'l_dblDiscountAmount = ((l_dblUnitCharge / 100) * l_dblDiscount) * l_dblQuantity * l_dblTime
        
    l_dblTotal = RoundToCurrency(l_dblTotal - l_dblDiscountAmount)
    l_dblTotal = RoundToCurrency(l_dblTotal - l_dblAdditionalDiscount)
    
    grdCostingDetail.Columns("total").Text = RoundToCurrency(l_dblTotal)
    
    If l_strUnit = "M" Then
        l_dblRateCardTotal = l_dblQuantity * l_dblTime * l_dblRateCardPrice
    ElseIf g_optDontMakeMachineTimeADecimal = 1 Then
        l_dblRateCardTotal = (l_dblRateCardPrice * l_dblTime * l_dblQuantity) / 60
    Else
        l_dblRateCardTotal = l_dblRateCardPrice * l_dblTime * l_dblQuantity
    End If
    
    If l_dblRateCardTotal < l_dblRateCardMinimumCharge Then l_dblRateCardTotal = l_dblRateCardMinimumCharge
    
    grdCostingDetail.Columns("ratecardtotal").Text = l_dblRateCardTotal
    
    grdCostingDetail.Columns("VAT").Text = RoundToCurrency(l_dblTotal * (l_sngVATRate / 100))
    grdCostingDetail.Columns("totalincludingvat").Text = l_dblTotal + RoundToCurrency(l_dblTotal * (l_sngVATRate / 100))

    'this will update the ratecard values - if this code is a virtual code
    If IsVirtualCode(grdCostingDetail.Columns("costcode").Text) Then
        grdCostingDetail.Columns("ratecardprice").Text = l_dblUnitCharge
        grdCostingDetail.Columns("ratecardtotal").Text = l_dblTotal
        
        'l_dblRateCardPrice = l_dblUnitCharge
    End If

Case "unitcharge"
    
    'pick up whats known
    l_dblUnitCharge = Val(grdCostingDetail.Columns("unitcharge").Text)
    
    If l_strUnit = "M" Then
        l_dblTotal = l_dblQuantity * l_dblTime * l_dblUnitCharge
    ElseIf g_optDontMakeMachineTimeADecimal = 1 Then
        l_dblTotal = l_dblQuantity * l_dblTime * l_dblUnitCharge / 60
    Else
        l_dblTotal = l_dblQuantity * l_dblTime * l_dblUnitCharge
    End If
        
    If l_dblTotal >= 0 Then If l_dblTotal < l_dblMinimumCharge Then l_dblTotal = l_dblMinimumCharge
    
    'calculate new values
    'l_dblDiscount = RoundToCurrency(GetPercentageDiscount(l_dblRateCardPrice, l_dblUnitCharge))
    l_dblDiscount = Val(grdCostingDetail.Columns("discount").Text)
    
    l_dblDiscountAmount = (l_dblTotal / 100) * l_dblDiscount
    
    'calculate new values
    'l_dblDiscountAmount = ((l_dblUnitCharge / 100) * l_dblDiscount) * l_dblQuantity * l_dblTime
        
    l_dblTotal = RoundToCurrency(l_dblTotal - l_dblDiscountAmount)
    l_dblTotal = RoundToCurrency(l_dblTotal - l_dblAdditionalDiscount)
    
    grdCostingDetail.Columns("discount").Text = 0
    grdCostingDetail.Columns("total").Value = l_dblTotal
    grdCostingDetail.Columns("VAT").Value = RoundToCurrency(l_dblTotal * (l_sngVATRate / 100))
    grdCostingDetail.Columns("totalincludingvat").Value = l_dblTotal + RoundToCurrency(l_dblTotal * (l_sngVATRate / 100))
    
    'this will update the ratecard values - if this code is a virtual code
    If IsVirtualCode(grdCostingDetail.Columns("costcode").Text) Then
        grdCostingDetail.Columns("ratecardprice").Value = l_dblUnitCharge
        grdCostingDetail.Columns("ratecardtotal").Value = l_dblTotal
        
        l_dblRateCardPrice = l_dblUnitCharge
    End If
    
Case "discount"

    'pick up whats known
    l_dblDiscount = Val(grdCostingDetail.Columns("discount").Text)
    l_dblUnitCharge = Val(grdCostingDetail.Columns("unitcharge").Text) 'RoundToCurrency(l_dblRateCardPrice - l_dblDiscountAmount)
    l_dblAdditionalDiscount = Val(grdCostingDetail.Columns("additionaldiscount").Text)
    
    If l_strUnit = "M" Then
        l_dblTotal = l_dblQuantity * l_dblTime * l_dblUnitCharge
    ElseIf g_optDontMakeMachineTimeADecimal = 1 Then
        l_dblTotal = l_dblQuantity * l_dblTime * l_dblUnitCharge / 60
    Else
        l_dblTotal = l_dblQuantity * l_dblTime * l_dblUnitCharge
    End If
    
    If l_dblTotal >= 0 Then If l_dblTotal < l_dblMinimumCharge Then l_dblTotal = l_dblMinimumCharge
        
    'calculate new values
    'l_dblRateCardPrice = Val(grdCostingDetail.Columns("ratecardprice").Value)
    l_dblDiscountAmount = ((l_dblTotal / 100) * l_dblDiscount)
    l_dblTotal = RoundToCurrency(l_dblTotal - l_dblDiscountAmount)
    l_dblTotal = RoundToCurrency(l_dblTotal - l_dblAdditionalDiscount)

    'set the values in the grid
    grdCostingDetail.Columns("unitcharge").Text = l_dblUnitCharge
    grdCostingDetail.Columns("total").Text = l_dblTotal
    grdCostingDetail.Columns("VAT").Text = RoundToCurrency(l_dblTotal * (l_sngVATRate / 100))
    grdCostingDetail.Columns("totalincludingvat").Text = l_dblTotal + RoundToCurrency(l_dblTotal * (l_sngVATRate / 100))
 
     'this will update the ratecard values - if this code is a virtual code
    If IsVirtualCode(grdCostingDetail.Columns("costcode").Text) Then
        grdCostingDetail.Columns("ratecardprice").Text = l_dblUnitCharge
        grdCostingDetail.Columns("ratecardtotal").Text = l_dblTotal
        l_dblRateCardPrice = l_dblUnitCharge
    End If
 
Case "additionaldiscount"

    'pick up whats known
    l_dblDiscount = Val(grdCostingDetail.Columns("discount").Text)
    l_dblUnitCharge = Val(grdCostingDetail.Columns("unitcharge").Text) 'RoundToCurrency(l_dblRateCardPrice - l_dblDiscountAmount)
    l_dblAdditionalDiscount = Val(grdCostingDetail.Columns("additionaldiscount").Text)
    
    If l_strUnit = "M" Then
        l_dblTotal = l_dblQuantity * l_dblTime * l_dblUnitCharge
    ElseIf g_optDontMakeMachineTimeADecimal = 1 Then
        l_dblTotal = l_dblQuantity * l_dblTime * l_dblUnitCharge / 60
    Else
        l_dblTotal = l_dblQuantity * l_dblTime * l_dblUnitCharge
    End If
    
    If l_dblTotal >= 0 Then If l_dblTotal < l_dblMinimumCharge Then l_dblTotal = l_dblMinimumCharge
        
    'calculate new values
    'l_dblRateCardPrice = Val(grdCostingDetail.Columns("ratecardprice").Value)
    l_dblDiscountAmount = ((l_dblTotal / 100) * l_dblDiscount)
    l_dblTotal = RoundToCurrency(l_dblTotal - l_dblDiscountAmount)
    l_dblTotal = RoundToCurrency(l_dblTotal - l_dblAdditionalDiscount)

    'set the values in the grid
    grdCostingDetail.Columns("unitcharge").Text = l_dblUnitCharge
    grdCostingDetail.Columns("total").Text = l_dblTotal
    grdCostingDetail.Columns("VAT").Text = RoundToCurrency(l_dblTotal * (l_sngVATRate / 100))
    grdCostingDetail.Columns("totalincludingvat").Text = l_dblTotal + RoundToCurrency(l_dblTotal * (l_sngVATRate / 100))
 
     'this will update the ratecard values - if this code is a virtual code
    If IsVirtualCode(grdCostingDetail.Columns("costcode").Text) Then
        grdCostingDetail.Columns("ratecardprice").Text = l_dblUnitCharge
        grdCostingDetail.Columns("ratecardtotal").Text = l_dblTotal
        l_dblRateCardPrice = l_dblUnitCharge
    End If
     
End Select


Dim l_dblInflatedUnitCharge As Double
Dim l_dblInflatedTotal As Double
Dim l_dblInflatedVAT As Double
Dim l_dblInflatedTotalIncludingVAT As Double

Dim l_dblRefundedUnitCharge As Double
Dim l_dblRefundedTotal As Double
Dim l_dblRefundedVAT As Double
Dim l_dblRefundedTotalIncludingVAT As Double

If lblTwoTier.Caption <> "" Then


    l_dblTotal = grdCostingDetail.Columns("total").Value
    l_dblUnitCharge = Val(grdCostingDetail.Columns("unitcharge").Text)
    
    Dim l_dblVAT As Double, l_dblTotalIncludingVAT As Double
    l_dblVAT = RoundToCurrency(l_dblTotal * (l_sngVATRate / 100))
    l_dblTotalIncludingVAT = l_dblTotal + l_dblVAT
                
    'inflated prices
    Dim l_lngCompanyID As Long
    l_lngCompanyID = GetData("job", "companyID", "jobID", Val(txtJobID.Text))
    Dim l_strVATCode As String
    l_strVATCode = GetData("company", "vatcode", "companyID", l_lngCompanyID)
    
    l_dblInflatedUnitCharge = GetUnitCharge(g_intBaseRateCard, l_strCETACostCode, l_lngCompanyID)
        
    If l_strUnit = "M" Then
        l_dblInflatedTotal = l_dblQuantity * l_dblTime * l_dblInflatedUnitCharge
    ElseIf g_optDontMakeMachineTimeADecimal = 1 Then
        l_dblInflatedTotal = (l_dblQuantity * l_dblInflatedUnitCharge * (l_dblTime / 60))
    Else
        l_dblInflatedTotal = (l_dblQuantity * l_dblInflatedUnitCharge * l_dblTime)
    End If
    
    If l_dblInflatedTotal < l_dblMinimumCharge Then l_dblInflatedTotal = l_dblMinimumCharge
    
    l_dblInflatedTotal = RoundToCurrency(l_dblInflatedTotal)
       
    l_dblInflatedVAT = RoundToCurrency(l_dblInflatedTotal * ((GetVATRate(l_strVATCode) / 100)))
    
    l_dblInflatedTotalIncludingVAT = l_dblInflatedTotal + l_dblInflatedVAT
    
    'refunded prices
    l_dblRefundedUnitCharge = l_dblInflatedUnitCharge - l_dblUnitCharge
    l_dblRefundedTotal = l_dblInflatedTotal - l_dblTotal
    l_dblRefundedVAT = l_dblInflatedVAT - l_dblVAT
    l_dblRefundedTotalIncludingVAT = l_dblInflatedTotalIncludingVAT - l_dblTotalIncludingVAT

    
    grdCostingDetail.Columns("inflatedunitcharge").Text = l_dblInflatedUnitCharge
    grdCostingDetail.Columns("inflatedtotal").Text = l_dblInflatedTotal
    grdCostingDetail.Columns("inflatedvat").Text = l_dblInflatedVAT
    grdCostingDetail.Columns("inflatedtotalincludingvat").Text = l_dblInflatedTotalIncludingVAT

    grdCostingDetail.Columns("refundedunitcharge").Text = l_dblRefundedUnitCharge
    grdCostingDetail.Columns("refundedtotal").Text = l_dblRefundedTotal
    grdCostingDetail.Columns("refundedvat").Text = l_dblRefundedVAT
    grdCostingDetail.Columns("refundedtotalincludingvat").Text = l_dblRefundedTotalIncludingVAT

End If


End Sub



Private Sub grdCostingDetail_AfterUpdate(RtnDispErrMsg As Integer)

Dim l_dblJobTotal As Double, l_dblTotalIncludingVAT As Double, l_dblRateCardTotal As Double, l_dblAdditionalDiscountTotal As Double

l_dblJobTotal = GetTotalCostsForJob(Val(txtJobID.Text), "total", "INVOICE")
l_dblTotalIncludingVAT = GetTotalCostsForJob(Val(txtJobID.Text), "totalincludingvat", "INVOICE")
l_dblRateCardTotal = GetTotalCostsForJob(Val(txtJobID.Text), "ratecardtotal", "INVOICE")
l_dblAdditionalDiscountTotal = GetTotalCostsForJob(Val(txtJobID.Text), "additionaldiscount", "INVOICE")

frmJob.txtTotal.Text = g_strCurrency & Format(RoundToCurrency(l_dblJobTotal), "0.00#")
frmJob.txtTotalIncVAT.Text = g_strCurrency & Format(RoundToCurrency(l_dblTotalIncludingVAT), "0.00#")
frmJob.txtRateCardTotal.Text = g_strCurrency & Format(RoundToCurrency(l_dblRateCardTotal), "0.00#")
frmJob.txtTotalDiscountPercent.Text = GetPercentageDiscount(l_dblRateCardTotal, l_dblJobTotal)
frmJob.txtAdditionalDiscount.Text = g_strCurrency & Format(RoundToCurrency(l_dblAdditionalDiscountTotal), "0.00#")

End Sub

Private Sub grdCostingDetail_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

'variable
Dim l_intBookmark As Integer, l_intTotalBookmarks As Integer, l_varCurrentBookmark As Variant
Dim l_lngCostingID As Long, l_strInsertSQL As String

'store the bookmark count
l_intTotalBookmarks = grdCostingDetail.SelBookmarks.Count

Dim l_intMsg As Integer
l_intMsg = MsgBox("Are you sure you want to delete these " & l_intTotalBookmarks & " costing lines?", vbQuestion + vbYesNo)
If l_intMsg = vbNo Then Exit Sub

'loop through the grid bookmarks
For l_intBookmark = 0 To l_intTotalBookmarks - 1

    'get the bookmark
    l_varCurrentBookmark = grdCostingDetail.SelBookmarks(l_intBookmark)
    
    'pick up the costing ID
    l_lngCostingID = grdCostingDetail.Columns("costingID").CellText(l_varCurrentBookmark)
        
    'build it (its only easy, even a monkey could do it)
    l_strInsertSQL = "DELETE FROM costing WHERE costingID = '" & l_lngCostingID & "';"
        
    'then we need to run it
    ExecuteSQL l_strInsertSQL, g_strExecuteError
    
    CheckForSQLError

Next


Cancel = True

adoCosting.Refresh
grdCostingDetail.Refresh

End Sub

Private Sub grdCostingDetail_BeforeUpdate(Cancel As Integer)

'If g_intWebInvoicing = 1 And grdCostingDetail.Columns("accountstransactionID").Text <> 0 Then
'    MsgBox "Cannot update a row that has been billed", vbCritical, "Error"
'    Cancel = True
'    Exit Sub
'End If

grdCostingDetail.Columns("jobID").Text = Val(txtJobID.Text)
grdCostingDetail.Columns("costingsheetID").Text = Val(txtCostingSheet.Text)
grdCostingDetail.Columns("invoicenumber").Text = Val(lblInvoiceNumber.Caption)

End Sub

Private Sub grdCostingDetail_DblClick()

'dont allow this if the grid is disabled
If grdCostingDetail.AllowUpdate = False Then Exit Sub

If g_optDisableCostingPopUp = 1 Then Exit Sub

Dim l_lngCostingID As Long
l_lngCostingID = Val(grdCostingDetail.Columns("costingID").Text)

Dim l_strSQL As String

If l_lngCostingID = 0 Then
    l_strSQL = "INSERT INTO costing (jobID) VALUES ('" & Val(txtJobID.Text) & "');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    l_lngCostingID = g_lngLastID
End If

Dim l_blnUpdate As Boolean
l_blnUpdate = UpdateCostingRow(l_lngCostingID, Val(lblCompanyID.Caption))
If l_blnUpdate = True Then
    frmJob.txtTotal.Text = g_strCurrency & Format(RoundToCurrency(GetTotalCostsForJob(Val(txtJobID.Text), "total", "INVOICE")), "0.00#")
    frmJob.txtTotalIncVAT.Text = g_strCurrency & Format(RoundToCurrency(GetTotalCostsForJob(Val(txtJobID.Text), "totalincludingvat", "INVOICE")), "0.00#")
    frmJob.txtRateCardTotal.Text = g_strCurrency & Format(RoundToCurrency(GetTotalCostsForJob(Val(txtJobID.Text), "ratecardtotal", "INVOICE")), "0.00#")
End If

adoCosting.Refresh

End Sub

Private Sub grdCostingDetail_InitColumnProps()

    'set up the column drop downs
    
    If g_optHideTimeColumn = 1 Then
        grdCostingDetail.Columns("time").Visible = False
    Else
        grdCostingDetail.Columns("time").Visible = True
    End If
    
    If g_intLockDiscountColumnInCostings <> 0 Then grdCostingDetail.Columns("discount").Locked = True

    grdCostingDetail.Columns("includedindealprice").Visible = pnlDeal.Visible
    
    If lblTwoTier.Caption = "Two-Tier Costing Enabled" Then
    
        grdCostingDetail.Columns("refundedunitcharge").Visible = True
        grdCostingDetail.Columns("refundedtotal").Visible = True
        grdCostingDetail.Columns("refundedvat").Visible = True
        grdCostingDetail.Columns("refundedtotalincludingvat").Visible = True
        grdCostingDetail.Columns("refundedunitcharge").Width = 945
        grdCostingDetail.Columns("refundedtotal").Width = 945
        grdCostingDetail.Columns("refundedvat").Width = 945
        grdCostingDetail.Columns("refundedtotalincludingvat").Width = 945
        
    Else
        grdCostingDetail.Columns("refundedunitcharge").Visible = False
        grdCostingDetail.Columns("refundedtotal").Visible = False
        grdCostingDetail.Columns("refundedvat").Visible = False
        grdCostingDetail.Columns("refundedtotalincludingvat").Visible = False
    End If
    
    If g_optHideOrderByColumnOnCostingGrid = 1 Then
        grdCostingDetail.Columns("Order").Visible = False
    End If
    
    grdCostingDetail.Columns("costcode").DropDownHwnd = ddnRateCard.hWnd
    grdCostingDetail.Columns("unit").DropDownHwnd = ddnUnit.hWnd
    
   grdCostingDetail.Columns("total").NumberFormat = g_strCurrency & "###,###,##0.00"
   grdCostingDetail.Columns("unitcharge").NumberFormat = g_strCurrency & "###,###,##0.00"
   grdCostingDetail.Columns("ratecardprice").NumberFormat = g_strCurrency & "###,###,##0.00"
   grdCostingDetail.Columns("ratecardtotal").NumberFormat = g_strCurrency & "###,###,##0.00"
   grdCostingDetail.Columns("effectiveunit").NumberFormat = g_strCurrency & "###,###,##0.00"
   
End Sub
Private Sub grdCostingDetail_BtnClick()

Dim l_strSQL As String

Select Case Trim(grdCostingDetail.Columns("includedindealprice").Text)
Case "No", ""
    'grdCostingDetail.Columns("includedindealprice").Text = "Yes"
    l_strSQL = "UPDATE costing SET includedindealprice = 'Yes' WHERE costingID= '" & grdCostingDetail.Columns("costingID").Text & "';"
    
Case Else
    'grdCostingDetail.Columns("includedindealprice").Value = " "
    l_strSQL = "UPDATE costing SET includedindealprice = Null WHERE costingID= '" & grdCostingDetail.Columns("costingID").Text & "';"
End Select

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError


Dim l_lngBookmark As Long
l_lngBookmark = grdCostingDetail.Bookmark

adoCosting.Refresh

grdCostingDetail.Bookmark = l_lngBookmark

End Sub

Private Sub grdCostingDetail_KeyPress(KeyAscii As Integer)

If KeyAscii = vbKeyReturn Then
    KeyAscii = 0
    grdCostingDetail.Update
End If

End Sub

Private Sub grdCostingDetail_LostFocus()
'grdCostingDetail.Update
End Sub

Private Sub grdCostingDetail_RowLoaded(ByVal Bookmark As Variant)

Dim l_dblDiscount As Double, l_dblUnitCharge As Double, l_strStatus As String

l_dblDiscount = Val(grdCostingDetail.Columns("discount").Text)
l_dblUnitCharge = Val(grdCostingDetail.Columns("unitcharge").Text)

If l_dblDiscount <> 0 Then
    grdCostingDetail.Columns("effectiveunit").Text = l_dblUnitCharge * (1 - (l_dblDiscount / 100))
Else
    grdCostingDetail.Columns("effectiveunit").Text = l_dblUnitCharge
End If

If grdCostingDetail.Columns("accountstransactionID").Text <> "" Then
    If GetData("accountstransaction", "started", "accountstransactionID", Val(grdCostingDetail.Columns("accountstransactionID").Text)) <> 0 Then l_strStatus = "Started"
    If GetData("accountstransaction", "reviewed", "accountstransactionID", Val(grdCostingDetail.Columns("accountstransactionID").Text)) <> 0 Then l_strStatus = "Pro-Forma"
    If GetData("accountstransaction", "queued", "accountstransactionID", Val(grdCostingDetail.Columns("accountstransactionID").Text)) <> 0 Then l_strStatus = "Finalised"
    If GetData("accountstransaction", "exported", "accountstransactionID", Val(grdCostingDetail.Columns("accountstransactionID").Text)) <> 0 Then l_strStatus = "Sent To Accounts"
End If

grdCostingDetail.Columns("accountstransactionstatus").Text = l_strStatus

End Sub

Private Sub grdDetail_AfterDelete(RtnDispErrMsg As Integer)

m_blnDelete = False

End Sub

Private Sub grdDetail_AfterUpdate(RtnDispErrMsg As Integer)
adoJobDetail.Caption = adoJobDetail.Recordset.RecordCount & " job Lines"
End Sub

Private Sub grdDetail_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

m_blnDelete = True

End Sub

Private Sub grdDetail_BeforeUpdate(Cancel As Integer)

On Error GoTo grdDetail_BeforeUpdate_Error

If m_blnDelete = True Then Exit Sub

grdDetail.Columns("jobID").Text = txtJobID.Text

If lblCompanyID.Caption = "1292" Then
    If txtDADCSeriesID.Text = "" Then
        MsgBox "DADC Fixes Jobs need the DADC Series ID to be entered", vbExclamation
        Cancel = True
        txtDADCSeriesID.SetFocus
        Exit Sub
    End If
    grdDetail.Columns("DADCSeries").Text = txtDADCSeriesID.Text
    If txtDADCLegacyOracTVAENumber.Text = "" Then
        MsgBox "DADC Fixes Jobs need the DADC AE Number to be entered", vbExclamation
        Cancel = True
        txtDADCLegacyOracTVAENumber.SetFocus
        Exit Sub
    End If
    grdDetail.Columns("DADCLegacyOracTVAENumber").Text = txtDADCLegacyOracTVAENumber.Text
End If

'If g_intWebInvoicing = 1 And (Val(grdDetail.Columns("jobdetailID").Text) And GetData("costing", "accountstransactionID", "jobdetailID", Val(grdDetail.Columns("jobdetailID").Text)) <> 0) Then
'    MsgBox "Cannot update a line that has already been billed", vbCritical, "Error"
'    Cancel = True
'    grdDetail.SetFocus
'    Exit Sub
'End If

If grdDetail.Columns("copytype").Text = "" Then
    MsgBox "You must specify a copy type (first column M, C, O etc)", vbExclamation
    Cancel = True
    grdDetail.SetFocus
    Exit Sub
End If

grdDetail.Columns("copytype").Text = UCase(grdDetail.Columns("copytype").Text)

'for master and copy lines
If (UCase(grdDetail.Columns("copytype").Text) = "C" Or UCase(grdDetail.Columns("copytype").Text) = "M") Then
    
    If UCase(grdDetail.Columns("copytype").Text) = "C" And grdDetail.Columns("format").Text = "" Then
        MsgBox "You must specify a format for ALL COPY lines!", vbExclamation
        Cancel = True
        grdDetail.SetFocus
        Exit Sub
    End If
    
    If g_optHarshCheckingOnDubbingEntry = 1 Then
        
        If UCase(grdDetail.Columns("copytype").Text) = "C" And grdDetail.Columns("ratio").Text = "" Then
            MsgBox "You must specify an aspect ratio for ALL COPY lines!", vbExclamation
            Cancel = True
            grdDetail.SetFocus
            Exit Sub
        End If
        
        If grdDetail.Columns("standard").Text = "" Then
            MsgBox "You must specify a video standard for ALL lines!", vbExclamation
            Cancel = True
            grdDetail.SetFocus
            Exit Sub
        End If
        
        If Val(grdDetail.Columns("quantity").Text) = 0 Then
            MsgBox "You must specify a quantity for ALL lines!", vbExclamation
            Cancel = True
            grdDetail.SetFocus
            Exit Sub
        End If
        
    End If
    
End If

If grdDetail.Columns("fd_orderby").Text = "" Then

    Dim l_lngOrderby As Long
    l_lngOrderby = GetCount("SELECT COUNT(jobdetailID) FROM jobdetail WHERE jobID = '" & txtJobID.Text & "';")
        
    grdDetail.Columns("fd_orderby").Text = l_lngOrderby
End If

'If grdDetail.Columns("invoice").Text = 0 Then
'    If MsgBox("This item is set to not be charged." & vbCrLf & "Fix This?", vbYesNo, "Invoice Query") = vbYes Then
'        grdDetail.Columns("invoice").Text = -1
'    End If
'End If
'

If InStr(grdDetail.Columns("description").Text, vbTab) > 0 Then grdDetail.Columns("description").Text = Replace(grdDetail.Columns("description").Text, vbTab, " ")

'If (InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/svensktracker") > 0 Or InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/compilationtracker") > 0) _
'And chkSvenskTrackerInvoice.Value = 0 Then
'    'DADC Column Updates
'    If Trim(" " & GetData("job", "DADCProjectManager", "jobID", Val(txtJobID.Text))) <> "" Then
'        grdDetail.Columns("DADCProjectManager").Text = GetData("job", "DADCProjectManager", "jobID", Val(txtJobID.Text))
'    Else
'        MsgBox "Essential Svensk Info is not completed on the DADC Svensk tab. Job line not updated.", vbCritical, "Error..."
'        Cancel = 1
'    End If
'
'    If Trim(" " & GetData("job", "DADCProjectNumber", "jobID", Val(txtJobID.Text))) <> "" Then
'        grdDetail.Columns("DADCProjectNumber").Text = GetData("job", "DADCProjectNumber", "jobID", Val(txtJobID.Text))
'    Else
'        MsgBox "Essential Svensk Info is not completed on the DADC Svensk tab. Job line not updated.", vbCritical, "Error..."
'        Cancel = 1
'    End If
'
'    If Trim(" " & GetData("job", "DADCSeries", "jobID", Val(txtJobID.Text))) <> "" Then
'        grdDetail.Columns("DADCSeries").Text = GetData("job", "DADCSeries", "jobID", Val(txtJobID.Text))
'    ElseIf InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/svensktracker") > 0 Then
'        MsgBox "Essential Svensk Info is not completed on the DADC Svensk tab. Job line not updated.", vbCritical, "Error..."
'        Cancel = 1
'    End If
'
'    If Trim(" " & GetData("job", "DADCEpisodeNumber", "jobID", Val(txtJobID.Text))) <> "" Then
'        grdDetail.Columns("DADCEpisodeNumber").Text = GetData("job", "DADCEpisodeNumber", "jobID", Val(txtJobID.Text))
'    ElseIf InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/svensktracker") > 0 Then
'        MsgBox "Essential Svensk Info is not completed on the DADC Svensk tab. Job line not updated.", vbCritical, "Error..."
'        Cancel = 1
'    End If
'
'    If Trim(" " & GetData("job", "DADCEpisodeTitle", "jobID", Val(txtJobID.Text))) <> "" Then
'        grdDetail.Columns("DADCEpisodeTitle").Text = GetData("job", "DADCEpisodeTitle", "jobID", Val(txtJobID.Text))
'    ElseIf InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/svensktracker") > 0 Then
'        MsgBox "Essential Svensk Info is not completed on the DADC Svensk tab. Job line not updated.", vbCritical, "Error..."
'        Cancel = 1
'    End If
'
'    If Trim(" " & GetData("job", "DADCRightsOwner", "jobID", Val(txtJobID.Text))) <> "" Then
'        grdDetail.Columns("DADCRightsOwner").Text = GetData("job", "DADCRightsOwner", "jobID", Val(txtJobID.Text))
'    ElseIf InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/svensktracker") > 0 Then
'        MsgBox "Essential Svensk Info is not completed on the DADC Svensk tab. Job line not updated.", vbCritical, "Error..."
'        Cancel = 1
'    End If
'
'    If Trim(" " & GetData("job", "DADCJobTitle", "jobID", Val(txtJobID.Text))) <> "" Then
'        grdDetail.Columns("DADCJobTitle").Text = GetData("job", "DADCJobTitle", "jobID", Val(txtJobID.Text))
'    Else
'        MsgBox "Essential Svensk Info is not completed on the DADC Svensk tab. Job line not updated.", vbCritical, "Error..."
'        Cancel = 1
'    End If
'End If
'
Exit Sub

grdDetail_BeforeUpdate_Error:

MsgBox Err.Number & " " & Err.Description, vbExclamation
Exit Sub


End Sub

Private Sub grdDetail_BtnClick()

Dim l_varBookmark As Variant

Select Case grdDetail.Columns(grdDetail.Col).Name

    Case "completeddate"
        
        If grdDetail.Columns("rejecteddate").Text <> "" Then Exit Sub
        
        'complete the row - set its details to be completed
        If grdDetail.Columns("completeddate").Text = "" Then
            If g_intUseTerseJobDetailCompletion = 1 Then
                TerseCompleteJobDetailItem grdDetail.Columns("jobdetailID").Text
            Else
                CompleteJobDetailItem grdDetail.Columns("jobdetailID").Text
            End If
        Else
            grdDetail.Columns("completeddate").Text = ""
            grdDetail.Columns("completeduser").Text = ""
            grdDetail.Update
        End If
        
        'refresh the grid
        l_varBookmark = grdDetail.Bookmark
        adoJobDetail.Refresh
        grdDetail.Bookmark = l_varBookmark
        
    Case "rejecteddate"
        
        'reject the row - set its details to rejected
        If grdDetail.Columns("rejecteddate").Text = "" Then
            TerseRejectJobDetailItem grdDetail.Columns("jobdetailID").Text, False
            txtStatus.Text = "Pending"
            txtStatus.BackColor = CLng(GetResourceBookingColour(CStr(txtStatus.Text)))
            l_varBookmark = grdDetail.Bookmark
            adoJobDetail.Refresh
            UpdateJobStatus Val(txtJobID.Text), "Pending", 0, True
            ShowJob Val(txtJobID.Text), 1, False
'            ShowJob Val(txtJobID.Text), 1, False
            grdDetail.Bookmark = l_varBookmark
        Else
            TerseRejectJobDetailItem grdDetail.Columns("jobdetailID").Text, True
'            txtStatus.Text = "In Progress"
'            txtStatus.BackColor = CLng(GetResourceBookingColour(CStr(txtStatus.Text)))
'            UpdateJobStatus Val(txtJobID.Text), "In Progress", 0, True
'            l_varBookmark = grdDetail.bookmark
'            ShowJob Val(txtJobID.Text), 1, False
'            grdDetail.bookmark = l_varBookmark
            'refresh the grid
            l_varBookmark = grdDetail.Bookmark
            adoJobDetail.Refresh
            grdDetail.Bookmark = l_varBookmark
            
        End If
        
    Case "emailto"
        
        'open the text editor so people can add email addresses
        frmTextEdit.txtTextToEdit.Text = grdDetail.Columns(grdDetail.Col).Text
        frmTextEdit.Show vbModal
        
        'was the cancel button pressed
        If frmTextEdit.Tag <> "CANCELLED" Then
            grdDetail.Columns(grdDetail.Col).Text = frmTextEdit.txtTextToEdit.Text
        End If
        
        'unload the text edit form
        Unload frmTextEdit
        Set frmTextEdit = Nothing
End Select

End Sub

Private Sub grdDetail_GotFocus()

'let the user know they cant do this without a job selected
If Not IsNumeric(txtJobID.Text) Then
    NoJobSelectedMessage
    txtJobID.SetFocus
End If

Dim l_intMsg As Integer

If Not IsDate(GetData("job", "deadlinedate", "jobID", txtJobID.Text)) Then
    'l_intMsg = MsgBox("You must enter and save a valid deadline for dubbing jobs." & vbCrLf & vbCrLf & "Do you want to save now?", vbQuestion + vbOK)
    
    MsgBox "You must enter and SAVE a valid deadline for dubbing jobs", vbExclamation
    
    'If l_intMsg = vbCancel Then
        If datDeadlineDate.Enabled = True Then datDeadlineDate.SetFocus
        Exit Sub
    'Else
    '    cmdSave.Value = True
    'End If
    
End If

End Sub

Private Sub grdDetail_LostFocus()
'grdDetail.Update
End Sub

Private Sub grdDetail_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

If g_optUpperCaseDubbingDetails = 1 Then
    grdDetail.Columns("copytype").Text = UCase(grdDetail.Columns("copytype").Text)
    grdDetail.Columns("description").Text = UCase(grdDetail.Columns("description").Text)
End If

Dim l_blnLockStock As Boolean, l_blnLockOthers As Boolean

'only show drop downs for relevant copy types
grdDetail.Columns("description").DropDownHwnd = 0
grdDetail.Columns("stocktype1").DropDownHwnd = 0
grdDetail.Columns("stocktype2").DropDownHwnd = 0

grdDetail.Columns("format").DropDownHwnd = 0
grdDetail.Columns("standard").DropDownHwnd = 0
grdDetail.Columns("ch1").DropDownHwnd = 0
grdDetail.Columns("ch2").DropDownHwnd = 0
grdDetail.Columns("ch3").DropDownHwnd = 0
grdDetail.Columns("ch4").DropDownHwnd = 0
grdDetail.Columns("ratio").DropDownHwnd = 0
grdDetail.Columns("timecode").DropDownHwnd = 0

Dim l_strTitle As String

'what copy type (master copy etc)
Select Case UCase(grdDetail.Columns("copytype").Text)
Case "M"
    
    l_strTitle = txtTitle.Text
    
    If g_optUpperCaseDubbingDetails = 1 Then l_strTitle = UCase(l_strTitle)
    
    If LastCol = 0 And g_optUseTitleInMaster <> 0 Then
        If grdDetail.Columns("description").Text = "" Then grdDetail.Columns("description").Text = l_strTitle
    End If
    
    l_blnLockOthers = False
    l_blnLockStock = True
    
    grdDetail.Columns("format").DropDownHwnd = ddnFormat.hWnd
    grdDetail.Columns("standard").DropDownHwnd = ddnStandard.hWnd
    grdDetail.Columns("ch1").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch2").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch3").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch4").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ratio").DropDownHwnd = ddnRatio.hWnd
    grdDetail.Columns("timecode").DropDownHwnd = ddnTimeCode.hWnd

Case "A"
    l_blnLockOthers = False
    l_blnLockStock = True
    'lock the stock columns and prevent entry
    
    grdDetail.Columns("format").DropDownHwnd = ddnFormat.hWnd
    grdDetail.Columns("standard").DropDownHwnd = ddnStandard.hWnd
    grdDetail.Columns("ch1").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch2").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch3").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch4").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ratio").DropDownHwnd = ddnRatio.hWnd
    grdDetail.Columns("timecode").DropDownHwnd = ddnTimeCode.hWnd

Case "C"
    
    
    l_strTitle = txtTitle.Text
    
    If g_optUpperCaseDubbingDetails = 1 Then l_strTitle = UCase(l_strTitle)
    
   
    If LastCol = 0 Then
        If g_optUseTitleInCopy <> 0 Then
            If grdDetail.Columns("description").Text = "" Then grdDetail.Columns("description").Text = l_strTitle
        Else
            If grdDetail.Columns("description").Text = "" Then grdDetail.Columns("description").Text = "Please make copies"
        End If
    End If
    
    l_blnLockOthers = False
    l_blnLockStock = False
    grdDetail.Columns("stocktype1").DropDownHwnd = ddnStockType.hWnd
    grdDetail.Columns("stocktype2").DropDownHwnd = ddnStockType.hWnd
    
    grdDetail.Columns("format").DropDownHwnd = ddnFormat.hWnd
    grdDetail.Columns("standard").DropDownHwnd = ddnStandard.hWnd
    grdDetail.Columns("ch1").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch2").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch3").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch4").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ratio").DropDownHwnd = ddnRatio.hWnd
    grdDetail.Columns("timecode").DropDownHwnd = ddnTimeCode.hWnd
    grdDetail.Columns("description").DropDownHwnd = ddnCopyList.hWnd

Case "O"
    l_blnLockOthers = True
    l_blnLockStock = True
    grdDetail.Columns("format").DropDownHwnd = ddnOthers.hWnd

Case "I", "IS"
    l_blnLockOthers = False
    l_blnLockStock = True
    'lock the stock columns and prevent entry
    
    grdDetail.Columns("format").DropDownHwnd = ddnInclusive.hWnd
    grdDetail.Columns("standard").DropDownHwnd = ddnStandard.hWnd
    grdDetail.Columns("ch1").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch2").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch3").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch4").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ratio").DropDownHwnd = ddnRatio.hWnd
    grdDetail.Columns("timecode").DropDownHwnd = ddnTimeCode.hWnd

Case "P"
    l_blnLockOthers = True
    l_blnLockStock = True
    grdDetail.Columns("description").DropDownHwnd = ddnPlayouts.hWnd
    grdDetail.Columns("format").DropDownHwnd = ddnFormat.hWnd
    grdDetail.Columns("standard").DropDownHwnd = ddnStandard.hWnd
    grdDetail.Columns("ch1").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch2").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch3").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch4").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ratio").DropDownHwnd = ddnRatio.hWnd
    grdDetail.Columns("timecode").DropDownHwnd = ddnTimeCode.hWnd
    
Case "S"
    If LastCol = 0 Then
        If grdDetail.Columns("description").Text = "" Then grdDetail.Columns("description").Text = "SOURCE SUPPLIED BY CLIENT"
    End If
    
    l_blnLockOthers = False
    l_blnLockStock = True
    
    grdDetail.Columns("format").DropDownHwnd = ddnFormat.hWnd
    grdDetail.Columns("standard").DropDownHwnd = ddnStandard.hWnd
    grdDetail.Columns("ch1").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch2").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch3").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch4").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ratio").DropDownHwnd = ddnRatio.hWnd
    grdDetail.Columns("timecode").DropDownHwnd = ddnTimeCode.hWnd


Case "D"
    
    l_strTitle = txtTitle.Text
    
    If g_optUpperCaseDubbingDetails = 1 Then l_strTitle = UCase(l_strTitle)
    
    
    If LastCol = 0 And g_optUseTitleInMaster <> 0 Then
        If grdDetail.Columns("description").Text = "" Then grdDetail.Columns("description").Text = l_strTitle
    End If
    
    l_blnLockOthers = False
    l_blnLockStock = False
    
    grdDetail.Columns("format").DropDownHwnd = ddnFormat.hWnd
    grdDetail.Columns("standard").DropDownHwnd = ddnStandard.hWnd
    grdDetail.Columns("ch1").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch2").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch3").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ch4").DropDownHwnd = ddnSoundChannel.hWnd
    grdDetail.Columns("ratio").DropDownHwnd = ddnRatio.hWnd
    grdDetail.Columns("timecode").DropDownHwnd = ddnTimeCode.hWnd

    grdDetail.Columns("description").DropDownHwnd = ddnDigital.hWnd

End Select

If LastCol = 10 Then
    If grdDetail.Columns("ch2").Text = "" Then grdDetail.Columns("ch2").Text = grdDetail.Columns("ch1").Text
End If

If LastCol = 12 Then
    If grdDetail.Columns("ch4").Text = "" Then grdDetail.Columns("ch4").Text = grdDetail.Columns("ch3").Text
End If

'lock the stock columns and prevent entry
With grdDetail
    .Columns("stocktype1").Locked = l_blnLockStock
    .Columns("ourstock1").Locked = l_blnLockStock
    .Columns("clientstock1").Locked = l_blnLockStock
    .Columns("stocktype2").Locked = l_blnLockStock
    .Columns("ourstock2").Locked = l_blnLockStock
    .Columns("clientstock2").Locked = l_blnLockStock

    Debug.Print l_blnLockOthers
    
    '.Columns("format").Locked = l_blnLockOthers
    .Columns("standard").Locked = l_blnLockOthers
    .Columns("ch1").Locked = l_blnLockOthers
    .Columns("ch2").Locked = l_blnLockOthers
    .Columns("ch3").Locked = l_blnLockOthers
    .Columns("ch4").Locked = l_blnLockOthers
    .Columns("ratio").Locked = l_blnLockOthers
    .Columns("timecode").Locked = l_blnLockOthers
    .Columns("runningtime").Locked = l_blnLockOthers
    .Columns("chargemins").Locked = l_blnLockOthers
End With


End Sub

Private Sub grdDetail_RowLoaded(ByVal Bookmark As Variant)

If grdDetail.Columns("completeddate").Text <> "" Then
    grdDetail.Columns("description").CellStyleSet "COMPLETED"
ElseIf grdDetail.Columns("rejecteddate").Text <> "" Then
    grdDetail.Columns("description").CellStyleSet "REJECTED"
Else
    grdDetail.Columns("description").CellStyleSet ""
End If

End Sub

Private Sub grdJobContacts_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdJobContacts_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdJobContacts_BeforeUpdate(Cancel As Integer)

'Dont do any of this if the update is to delete lines.
If m_blnDelete = True Then Exit Sub

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Cancel = 1
    Exit Sub
End If

If Val(lblCompanyID.Caption) = 0 Then
    NoCompanySelectedMessage
    Cancel = 1
    Exit Sub
End If

If grdJobContacts.Columns("email").Text <> "" Then
    If ValidateEmail(grdJobContacts.Columns("email").Text) = False Then
        MsgBox "Invalid Email address provided"
        Cancel = 1
        Exit Sub
    End If
Else
    MsgBox "No Email address provided"
    Cancel = 1
    Exit Sub
End If

If grdJobContacts.Columns("name").Text = "" Then
    MsgBox "No name provided. Name is required for Automated Deliveries."
    Cancel = 1
    Exit Sub
End If

grdJobContacts.Columns("jobID").Text = txtJobID.Text

Dim l_strSQL As String, l_lngPortalUserID As Long, l_strPortalUserName As String, Count As Long, l_strPWD As String, l_blnPort33001 As Boolean

If Val(grdJobContacts.Columns("companycontactID").Text) = 0 Then
    If MsgBox("This contact was not selected from the dropdown list" & vbCrLf & "Do you wish to store this contact in the list", vbYesNo, "Unknown Notification Contact") = vbYes Then
        l_strSQL = "INSERT INTO companycontact (companyID, name, email) VALUES ("
        l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdJobContacts.Columns("name").Text) & "', "
        l_strSQL = l_strSQL & "'" & grdJobContacts.Columns("email").Text & "');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        grdJobContacts.Columns("companycontactID").Text = g_lngLastID
        adoCompanyContact.Refresh
    End If
End If

If grdJobContacts.Columns("CCnotification").Text = "" Then
    'If not just a CC notification, then there needs to be a portaluser made or activated for this person, because we might be delivering stuff to them
    If Val(Trim(" " & GetDataSQL("SELECT TOP 1 portaluserID FROM portaluser WHERE email = '" & grdJobContacts.Columns("email").Text & "' AND companyID = " & lblCompanyID.Caption & ";"))) <> 0 Then
        l_lngPortalUserID = GetDataSQL("SELECT TOP 1 portaluserID FROM portaluser WHERE email = '" & grdJobContacts.Columns("email").Text & "' AND companyID = " & lblCompanyID.Caption & ";")
        If Val(Trim(" " & GetData("portaluser", "activeuser", "portaluserID", l_lngPortalUserID))) = 0 Then
            SetData "portaluser", "activeuser", "portaluserID", l_lngPortalUserID, 1
            SetData "portaluser", "dateactivated", "portaluserID", l_lngPortalUserID, Format(Now, "YYYY-MM-DD HH:NN:SS")
        End If
    Else
        'Create a new portal user for this person.
        l_strPortalUserName = grdJobContacts.Columns("email").Text
        If Trim(" " & GetData("portaluser", "portaluserID", "username", grdJobContacts.Columns("email").Text)) <> "" Then
            Count = 1
            Do While Trim(" " & GetData("portaluser", "portaluserID", "username", l_strPortalUserName & "_" & Count)) <> ""
                Count = Count + 1
            Loop
            l_strPortalUserName = l_strPortalUserName & "_" & Count
        End If
        
        l_blnPort33001 = True
        l_strSQL = "INSERT INTO portaluser(companyID, fullname, fullname_UTF8, username, password, email, forceport33001, datecreated, dateactivated, portalheaderpage, portalfooterpage, portalmainpage, administratoremail, activeuser, lockpassword) VALUES ("
        l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdJobContacts.Columns("name").Text) & "', "
        l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(grdJobContacts.Columns("name").Text)) & "', "
        l_strSQL = l_strSQL & "'" & l_strPortalUserName & "', "
        l_strSQL = l_strSQL & "'" & RndPassword(8) & "', "
        l_strSQL = l_strSQL & "'" & grdJobContacts.Columns("email").Text & "', "
        l_strSQL = l_strSQL & IIf(l_blnPort33001 = True, "1, ", "Null, ")
        l_strSQL = l_strSQL & "convert(nvarchar, GETDATE(), 20), "
        l_strSQL = l_strSQL & "convert(nvarchar, GETDATE(), 20), "
        l_strSQL = l_strSQL & "'" & GetData("company", "portalheaderpage", "companyID", lblCompanyID.Caption) & "', "
        l_strSQL = l_strSQL & "'" & GetData("company", "portalfooterpage", "companyID", lblCompanyID.Caption) & "', "
        l_strSQL = l_strSQL & "'Portal_Search_Results_Initial.asp', "
        l_strSQL = l_strSQL & "'" & g_strUserEmailAddress & "', "
        l_strSQL = l_strSQL & "1, 0);"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_lngPortalUserID = g_lngLastID
    End If
    
    grdJobContacts.Columns("portaluserID").Text = l_lngPortalUserID
    
End If

End Sub

Private Sub grdJobContacts_BtnClick()

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

If Val(lblCompanyID.Caption) = 0 Then
    NoCompanySelectedMessage
    Exit Sub
End If

Dim l_strName As String, l_strEmail As String, l_strSQL As String

l_strEmail = ""
l_strName = ""

l_strName = InputBox("Please give the full name of the new notification contact", "Adding new Notification contact")
If l_strName = "" Then Exit Sub

Do While ValidateEmail(l_strEmail) = False
    l_strEmail = InputBox("Please give the email address of the new notification contact", "Adding new Notification contact")
    If l_strEmail = "" Then Exit Sub
Loop

l_strSQL = "INSERT INTO companycontact (companyID, name, email) VALUES (" & Val(lblCompanyID.Caption) & ", '" & QuoteSanitise(l_strName) & "', '" & l_strEmail & "');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_strSQL = "INSERT INTO jobcontact (companyID, companycontactID, jobID, name, email) VALUES (" & Val(lblCompanyID.Caption) & ", " & g_lngLastID & ", " & Val(txtJobID.Text) & ", '" & QuoteSanitise(l_strName) & "', '" & l_strEmail & "');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

adoCompanyContact.Refresh
adoJobContact.Refresh


End Sub

Private Sub grdMedia_DblClick()

If grdMedia.Rows > 0 Then ShowLibrary grdMedia.Columns("libraryID").Text

End Sub

Private Sub grdMedia_InitColumnProps()
If g_intDisableProducts = 1 Then
    grdMedia.Columns("productname").Visible = False
End If

End Sub

Private Sub grdMediaFiles_DblClick()
If grdMediaFiles.Rows > 0 Then ShowClipControl grdMediaFiles.Columns("eventID").Text
End Sub

Private Sub grdMediaFiles_RowLoaded(ByVal Bookmark As Variant)

Dim l_curTotalSize As Currency, l_lngLibraryID As Long

l_lngLibraryID = Val(grdMediaFiles.Columns("LibraryID").Text)

l_curTotalSize = GetData("events", "bigfilesize", "eventID", grdMediaFiles.Columns("eventID").Text)
Debug.Print l_curTotalSize

grdMediaFiles.Columns("filesize").Text = Format(Int((l_curTotalSize / 1024 / 1024 / 1024) + 0.99999999), "#,##0")

grdMediaFiles.Columns("clipstore").Text = GetData("library", "barcode", "libraryID", l_lngLibraryID)

End Sub

Private Sub grdRequiredFiles_KeyDown(KeyCode As Integer, Shift As Integer)

If grdRequiredFiles.Rows = 0 Then Exit Sub
If Val(grdRequiredFiles.Columns("requiredmediaid").Text) = 0 Then Exit Sub

If KeyCode = vbKeyDelete Then
    KeyCode = 0
    
    Dim l_intDelete As Integer, l_strDeleteSQL As String
    l_intDelete = MsgBox("Are you sure you want to remove this required media?", vbQuestion + vbYesNo, "Delete?")
    If l_intDelete = vbNo Then Exit Sub
        
    l_strDeleteSQL = "DELETE FROM requiredmedia WHERE requiredmediaID = '" & grdRequiredFiles.Columns("requiredmediaid").Text & "';"
    ExecuteSQL l_strDeleteSQL, g_strExecuteError
    
    If CheckForSQLError() = False Then
        tab1_Click tab1.Tab
    End If
    
End If

End Sub

Private Sub grdRequiredFiles_RowLoaded(ByVal Bookmark As Variant)

Dim l_curTotalSize As Currency, l_lngLibraryID As Long

l_lngLibraryID = Val(grdRequiredFiles.Columns("LibraryID").Text)

l_curTotalSize = GetData("events", "bigfilesize", "eventID", grdRequiredFiles.Columns("eventID").Text)
Debug.Print l_curTotalSize

grdRequiredFiles.Columns("filesize").Text = Format(Int((l_curTotalSize / 1024 / 1024 / 1024) + 0.99999999), "#,##0")

grdRequiredFiles.Columns("clipstore").Text = GetData("library", "barcode", "libraryID", l_lngLibraryID)

End Sub

Private Sub grdRequiredMedia_DblClick()
If grdRequiredMedia.Rows <= 0 Then Exit Sub
ShowLibrary grdRequiredMedia.Columns("libraryID").Text
End Sub

Private Sub grdRequiredMedia_KeyDown(KeyCode As Integer, Shift As Integer)

If grdRequiredMedia.Rows = 0 Then Exit Sub
If Val(grdRequiredMedia.Columns("requiredmediaid").Text) = 0 Then Exit Sub

If KeyCode = vbKeyDelete Then
    KeyCode = 0
    
    Dim l_intDelete As Integer, l_strDeleteSQL As String
    l_intDelete = MsgBox("Are you sure you want to remove this required media?", vbQuestion + vbYesNo, "Delete?")
    If l_intDelete = vbNo Then Exit Sub
    
'    Dim l_strDeleteSQL As String, l_lngLibraryID As Long, l_strBarcode As String
'    l_lngLibraryID = grdRequiredMedia.Columns("libraryID").Text
'    l_strBarcode = grdRequiredMedia.Columns("barcode").Text
'    If l_lngLibraryID <> 0 Then
'        If MsgBox("Do you wish to delete the Tape Record also?", vbYesNo, "Deleting Required Media") = vbYes Then
'            If MsgBox("Are you sure?", vbYesNo, "Deleting Tape " & l_strBarcode) = vbYes Then
'                DeleteLibrary (l_lngLibraryID)
'            End If
'        End If
'    End If
    
    l_strDeleteSQL = "DELETE FROM requiredmedia WHERE requiredmediaID = '" & grdRequiredMedia.Columns("requiredmediaid").Text & "';"
    ExecuteSQL l_strDeleteSQL, g_strExecuteError
    
    If CheckForSQLError() = False Then
        tab1_Click tab1.Tab
    End If
    
End If

End Sub

Private Sub grdSundryCosts_DblClick()

If grdSundryCosts.Rows = 0 Then Exit Sub
ShowSundryCost Val(grdSundryCosts.Columns("sundrycostID").Text)
adoSundry.Refresh


End Sub

Private Sub grdSundryCosts_RowLoaded(ByVal Bookmark As Variant)

Dim l_intLoop As Integer

If grdSundryCosts.Columns("authoriseddate").Text <> "" Then
    
    For l_intLoop = 0 To grdSundryCosts.Columns.Count - 1
        grdSundryCosts.Columns(l_intLoop).CellStyleSet "authorised"
    Next
    
Else
   For l_intLoop = 0 To grdSundryCosts.Columns.Count - 1
        grdSundryCosts.Columns(l_intLoop).CellStyleSet ""
    Next
End If

If grdSundryCosts.Columns("approveddate").Text <> "" Then
    For l_intLoop = 0 To grdSundryCosts.Columns.Count - 1
        grdSundryCosts.Columns(l_intLoop).CellStyleSet "approved"
    Next
End If

End Sub

Private Sub grdTechReviewMedia_DblClick()
If grdTechReviewMedia.Rows <= 0 Then Exit Sub
ShowLibrary grdTechReviewMedia.Columns("libraryID").Text
End Sub

Private Sub grdTechReviewMedia_InitColumnProps()
If g_intDisableProducts = 1 Then
    grdTechReviewMedia.Columns("productname").Visible = False
End If
End Sub

Private Sub grdTechReviewMediaFiles_DblClick()
If grdTechReviewMediaFiles.Rows > 0 Then ShowClipControl grdTechReviewMediaFiles.Columns("eventID").Text
End Sub

Private Sub grdUnbilled_BeforeUpdate(Cancel As Integer)

If m_intClearJunk = 0 Then
    
    If grdUnbilled.Columns("companyID").Text = "" Then
        grdUnbilled.Columns("companyID").Text = lblCompanyID.Caption
        grdUnbilled.Columns("completeddate").Text = Now
        grdUnbilled.Columns("completeduser").Text = g_strUserName
    End If

Else
    
    Cancel = 1

End If

End Sub

Public Sub tab1_Click(PreviousTab As Integer)

Dim l_lngJobID As Long
l_lngJobID = Val(txtJobID.Text)

Dim l_strSQL As String

If l_lngJobID = 0 Then
'    NoJobSelectedMessage
'    txtJobID.SetFocus
    Exit Sub
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

picCostingFooter.Visible = False
picCostingTotals.Visible = False


Select Case tab1.Tab
    Case m_intTabDADCSvensk
    
    Case m_intTabJob
    
    Case m_intTabCosting
        
        If l_lngJobID = 0 Then
            NoJobSelectedMessage
            txtJobID.SetFocus
            Exit Sub
        End If
       
        If Not CheckAccess("/costjob") Then
            tabReplacementTabs.Tab = GetReplacementTab(PreviousTab)
            Exit Sub
        End If
        
        picCostingFooter.Top = tab1.Height - picCostingFooter.Height - 120
        picCostingFooter.Left = tab1.Width - picCostingFooter.Width - 120
        
        picCostingTotals.Top = picCostingFooter.Top - picCostingTotals.Height - 120
        picCostingTotals.Left = tab1.Width - picCostingTotals.Width - 120
        
        'save the job
        'cmdSave.Value = True
            
        Dim l_strCostingUnit As String
        Dim l_sngCostingQty As Integer
        
        If LCase(cmbJobType.Text) = "hire" And g_optDontForceUnitsChargedOnHireJobs = 0 Then
            l_strCostingUnit = GetData("job", "chargeunits", "jobID", l_lngJobID)
            l_sngCostingQty = Val(GetData("job", "chargequantity", "jobID", l_lngJobID))
            
            If l_strCostingUnit = "" Or l_sngCostingQty = 0 Then
                MsgBox "Please complete the 'Charge For' details before trying to cost this hire job.", vbExclamation
                frmJob.tab1.Tab = PreviousTab
                frmJob.tabReplacementTabs.Tab = GetReplacementTab(PreviousTab)
                Exit Sub
            End If
        End If
        
        
        If GetStatusNumber(txtStatus.Text) < 2 And (UCase(cmbJobType.Text) <> "HIRE" And UCase(cmbJobType.Text) <> "EDIT") And g_optAllowCostingEntryAtAnyStatus <> 1 Then
            tabReplacementTabs.Tab = GetReplacementTab(PreviousTab)
            MsgBox "You can not cost 'Pencil' jobs." & vbCrLf & vbCrLf & "You must go through the process of 'Confirming' then 'Completing' this job (enter the actual times or complete any dubbing requests) before trying to cost it.", vbExclamation
            Exit Sub
        End If
        
        If g_optRequireCompletedDubsBeforeCosting = GetFlag(True) Then
            If CheckJobDubbingsAreCompleted(l_lngJobID) = False Then
                tabReplacementTabs.Tab = GetReplacementTab(PreviousTab)
                MsgBox "This job can not be costed as it has outstanding dubbing instructions.", vbExclamation
                Exit Sub
            End If
        End If
        
        'get the credit limit
        lblCaption(93).Caption = "Credit Limit " & g_strCurrency & Format(GetData("company", "creditlimit", "companyID", lblCompanyID.Caption), "#,###,##0.00")
        
        txtAccounts.Text = GetData("job", "notes4", "jobID", txtJobID.Text)
        If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/showadditionaldiscount") <= 0 Then
            grdCostingDetail.Columns("additionaldiscount").Visible = False
        Else
            grdCostingDetail.Columns("additionaldiscount").Visible = True
        End If
        
        If chkNoChargeJob.Value = 0 Then
            CostJob l_lngJobID, 0 - GetFlag(g_optCostDubbing), 0 - GetFlag(g_optCostSchedule)
            cmdReCost.Enabled = True
        Else
            ProgressBar1.Visible = False
            cmdReCost.Enabled = False
            cmdInvoice.Enabled = False
        End If
        
        picNewSundries.Visible = GetData("job", "flagnewsundries", "jobID", Val(txtJobID.Text))
        
        picCostingFooter.Visible = True
        picCostingTotals.Visible = True
    
        Dim l_strCostingsMessage As String
        l_strCostingsMessage = GetData("company", "messagecostings", "companyID", lblCompanyID.Caption)
        If l_strCostingsMessage = "False" Then l_strCostingsMessage = ""
        If l_strCostingsMessage <> "" And LCase(GetData("allocation", "allocationtype", "name", cmbAllocation.Text)) = "automatic" Then MsgBox l_strCostingsMessage, vbInformation
        
    Case m_intTabLibrary
    
        If Not CheckAccess("/viewjobmedia") Then
            tabReplacementTabs.Tab = GetReplacementTab(PreviousTab)
            Exit Sub
        End If
        
        'show the relevant associated media
        l_strSQL = "SELECT libraryID, barcode, productname, title, subtitle, series, seriesset, episode, format, copytype, cdate FROM library WHERE jobID = " & l_lngJobID & " AND system_deleted = 0 ORDER BY barcode"
        adoMedia(0).ConnectionString = g_strConnection
        adoMedia(0).RecordSource = l_strSQL
        adoMedia(0).Refresh
            
        'Show the relevant Clip Media for this job
        Dim l_curTotalSize As Currency
        l_strSQL = "SELECT eventID, eventseries, eventepisode, eventtitle, eventsubtitle, clipformat, cliphorizontalpixels, clipverticalpixels, clipbitrate, clipfilename, companyname, libraryID, bigfilesize " _
        & "FROM events WHERE 1=1 AND jobID = " & l_lngJobID & " AND system_deleted = 0ORDER BY clipfilename"
        adoMedia(3).ConnectionString = g_strConnection
        adoMedia(3).RecordSource = l_strSQL
        adoMedia(3).Refresh
        l_curTotalSize = 0
        If adoMedia(3).Recordset.RecordCount > 0 Then
            adoMedia(3).Recordset.MoveFirst
            Do While Not adoMedia(3).Recordset.EOF
                If Not IsNull(adoMedia(3).Recordset("bigfilesize")) Then l_curTotalSize = l_curTotalSize + adoMedia(3).Recordset("bigfilesize")
                adoMedia(3).Recordset.MoveNext
            Loop
        End If
        lblTotalMediaSize.Caption = Format(Int((l_curTotalSize / 1024 / 1024 / 1024) + 0.99999999), "#,##0")
        
        
        'Show the Tech Review Files for the job
        l_strSQL = "SELECT events.eventID, events.eventseries, events.eventepisode, events.eventtitle, events.eventsubtitle, events.clipformat, events.cliphorizontalpixels, events.clipverticalpixels, " _
        & "events.clipbitrate, events.clipfilename, events.companyname, events.libraryID, events.bigfilesize " _
        & "FROM events INNER JOIN techrev ON events.eventID = techrev.eventID WHERE techrev.jobID = " & l_lngJobID & " AND events.system_deleted = 0 ORDER BY events.clipfilename;"
        adoMedia(5).ConnectionString = g_strConnection
        adoMedia(5).RecordSource = l_strSQL
        adoMedia(5).Refresh
        
        'display the required tapes
        l_strSQL = "SELECT library.barcode, library.productname, library.title, library.episode, library.format, library.copytype, library.libraryID, library.location, library.shelf, library.box, requiredmedia.requiredmediaID " _
        & "FROM library INNER JOIN requiredmedia ON library.libraryID = requiredmedia.libraryID WHERE requiredmedia.jobid = " & l_lngJobID & " AND library.system_deleted = 0;"
        adoMedia(1).ConnectionString = g_strConnection
        adoMedia(1).RecordSource = l_strSQL
        adoMedia(1).Refresh
        
        'display the required files
        l_strSQL = "SELECT requiredmedia.requiredmediaiD, requiredmedia.eventID, events.eventID, events.eventseries, events.eventepisode, events.eventtitle, events.eventsubtitle, events.cliphorizontalpixels, events.clipverticalpixels, events.clipbitrate, " _
        & "events.clipfilename, events.libraryID, events.bigfilesize, events.companyname " _
        & "FROM requiredmedia INNER JOIN events ON requiredmedia.eventID = events.eventID WHERE requiredmedia.jobid = " & l_lngJobID & " AND events.system_deleted = 0;"
        adoMedia(4).ConnectionString = g_strConnection
        adoMedia(4).RecordSource = l_strSQL
        adoMedia(4).Refresh
        
        'tech review Tapes
        l_strSQL = "SELECT library.barcode, library.productname, library.title, library.subtitle, library.series, library.seriesset, library.episode, library.format, library.copytype, library.cdate, library.libraryID, techrev.jobID " _
        & "FROM library INNER JOIN techrev ON library.libraryID = techrev.libraryID WHERE techrev.jobID = " & l_lngJobID & " AND library.system_deleted = 0;"
        adoMedia(2).ConnectionString = g_strConnection
        adoMedia(2).RecordSource = l_strSQL
        adoMedia(2).Refresh
        
    
    Case m_intTabNotes
    Case m_intTabInvoice
        
        If Not CheckAccess("/viewinvoicetab") Then
            tabReplacementTabs.Tab = GetReplacementTab(PreviousTab)
            Exit Sub
        End If
        
        
        If Not CheckAccess("/editinvoicetext", True) Then txtInvoiceText.Locked = True
        If Not CheckAccess("/editinvoicenotes", True) Then txtInsertInvoiceNotes.Locked = True
        
        
    'Case m_intTabCreditNote'
'
'        Dim l_lngCreditNoteNumber As Variant
'        l_lngCreditNoteNumber = GetData("job", "creditnotenumber", "jobID", l_lngJobID)
'
'        If l_lngCreditNoteNumber = 0 Or l_lngCreditNoteNumber = "" Then
'            MsgBox "No credit note currently exists for this job", vbExclamation
'            Exit Sub
'        End If
'
'        lblCreditNoteNumber.Caption = l_lngCreditNoteNumber
'        adoCreditNote.ConnectionString = g_strConnection
'        adoCreditNote.RecordSource = "SELECT * FROM costing WHERE jobID = '" & l_lngJobID & "' AND (creditnotenumber = " & l_lngCreditNoteNumber & ") ORDER BY originalcostingID"
'        adoCreditNote.Refresh
'
'        txtTotalCredits.Text = Format(GetTotalCostsForJob(l_lngJobID, "total", "CREDIT"), "CURRENCY")
'        txtTotalCreditsIncVAT.Text = Format(GetTotalCostsForJob(l_lngJobID, "totalincludingvat", "CREDIT"), "CURRENCY")
'        picCreditFooter.Visible = True
'        picTotalCredits.Visible = True
        
'     Case m_intTabQuote
'
'
'        If Not CheckAccess("/viewjobprojecttab") Then
'            tabReplacementTabs.Tab = GetReplacementTab(PreviousTab)
'            Exit Sub
'        End If
'
'        Dim l_lngProjectID As Long
'        l_lngProjectID = GetData("job", "projectID", "jobID", Val(txtJobID.Text))
'
'        If l_lngProjectID = 0 Then
'            NoProjectSelectedMessage
'            Exit Sub
'        End If
'
'        Dim l_lngQuoteID As Long
'        l_lngQuoteID = Val(GetData("project", "quoteID", "projectID", l_lngProjectID))
'
'        l_strSQL = ""
'
'        'l_strSQL = l_strSQL & "SELECT costing.costcode, ratecard.description, ratecard.price1, costing.quantity AS actualhoursused "
'        'l_strSQL = l_strSQL & "FROM (job INNER JOIN costing ON job.jobID = costing.jobID) "
'        'l_strSQL = l_strSQL & "INNER JOIN ratecard ON costing.costcode = ratecard.cetacode "
'        'l_strSQL = l_strSQL & "WHERE (job.projectID) = '" & l_lngProjectID & "' "
'        'l_strSQL = l_strSQL & "GROUP BY costing.costcode,ratecard.description;"
'
'        l_strSQL = "SELECT costing.costcode as 'costcode', sum(costing.quantity) as TotalQuantity, sum(costing.total) AS TotalCost, '' AS averageunitcost FROM costing INNER JOIN job ON job.jobID = costing.jobID WHERE job.projectID = '" & l_lngProjectID & "' GROUP BY costing.costcode;"
'
'        Me.MousePointer = vbHourglass
'
'        Set l_conSearch = New ADODB.Connection
'        Set l_rstSearch = New ADODB.Recordset
'
'        l_conSearch.ConnectionString = g_strConnection
'        l_conSearch.Open
'
'        With l_rstSearch
'            .CursorLocation = adUseClient
'            .LockType = adLockBatchOptimistic
'            .CursorType = adOpenDynamic
'            .Open l_strSQL, l_conSearch, adOpenDynamic
'        End With
'
'        l_rstSearch.ActiveConnection = Nothing
'
'        Set grdQuoteOverview.DataSource = l_rstSearch
'
'        l_conSearch.Close
'        Set l_conSearch = Nothing
'
'        Dim l_dblQuoteTotal As Double, l_dblActualTotal As Double, l_dblChargedTotal As Double
'
'        Dim l_intLoop As Integer
'
'            'l_dblQuoteTotal = l_dblQuoteTotal + Val(grdQuoteOverview.Columns("quotedtotal").Text)
'            l_dblActualTotal = GetCount("SELECT sum(total) FROM costing INNER JOIN job ON costing.jobID = job.jobID WHERE job.projectID = '" & l_lngProjectID & "';")
'
'
'
'        grdQuoteOverview.Row = 0
'
'        'set up the total boxes at the bottom of the tab
'        txtQuotedTotal.Text = Format(GetTotalCostsForQuote(l_lngQuoteID, "quotedtotal"), "CURRENCY")
'        txtActualTotal.Text = Format(l_dblActualTotal, "CURRENCY")
'        txtChargedTotal.Text = Format(l_dblChargedTotal, "CURRENCY")
'
'
'        'set up the team grid
'        RefreshProjectTeam grdProjectTeam, Val(lblProjectID.Caption), Val(txtJobID.Text)
'
'        Me.MousePointer = vbDefault
        
    Case m_intTabSundryCosts
    
        If Not CheckAccess("/viewsundry") Then
            tab1.Tab = m_intTabSchedule
            tabReplacementTabs.Tab = GetReplacementTab(m_intTabSchedule)
            Exit Sub
        End If
        
        
        l_strSQL = "SELECT * FROM sundrycost WHERE jobID = '" & l_lngJobID & "' ORDER BY sundrycostID;"
        frmJob.adoSundry.ConnectionString = g_strConnection
        frmJob.adoSundry.RecordSource = l_strSQL
        frmJob.adoSundry.Refresh
        
        'allow new sundries?
        If GetStatusNumber(GetData("job", "fd_status", "jobID", l_lngJobID)) > 4 Then
            frmJob.cmdNewSundry.Enabled = False
        Else
            frmJob.cmdNewSundry.Enabled = True
        End If
        
        On Error Resume Next
        
    Case m_intTabDespatch
        
        Dim l_rstJob As New ADODB.Recordset
        Dim l_conJob As New ADODB.Connection
        
        l_conJob.Open g_strConnection
        l_rstJob.Open "SELECT deliverycompanyname, deliverycontactname, deliveryaddress, deliverypostcode, deliverycountry, deliverytelephone, collectioncompanyname, collectioncontactname, collectionaddress, collectionpostcode, collectioncountry, collectiontelephone FROM job WHERE jobID = '" & Val(txtJobID.Text) & "';", l_conJob, adOpenStatic, adLockReadOnly
        
        With l_rstJob
        
            cmbDeliveryCompany.Text = Format(.Fields("deliverycompanyname"))
            cmbDeliveryContact.Text = Format(.Fields("deliverycontactname"))
            txtDeliveryAddress.Text = Format(.Fields("deliveryaddress"))
            txtDeliveryPostCode.Text = Format(.Fields("deliverypostcode"))
            txtDeliveryCountry.Text = Format(.Fields("deliverycountry"))
            txtDeliveryTelephone.Text = Format(.Fields("deliverytelephone"))
        
            cmbCollectionCompany.Text = Format(.Fields("collectioncompanyname"))
            cmbCollectionContact.Text = Format(.Fields("collectioncontactname"))
            txtCollectionAddress.Text = Format(.Fields("collectionaddress"))
            txtCollectionPostCode.Text = Format(.Fields("collectionpostcode"))
            txtCollectionCountry.Text = Format(.Fields("collectioncountry"))
            txtCollectionTelephone.Text = Format(.Fields("collectiontelephone"))
                    
        End With
        
        l_rstJob.Close
        Set l_rstJob = Nothing
        
    Case m_intTabTechnical
        RefreshJobHistory l_lngJobID
        
    Case m_intUnBilled
        grdUnbilled.Width = tab1.Width - grdUnbilled.Left - 120

End Select

End Sub


Private Sub tabReplacementTabs_Click(PreviousTab As Integer)

If PreviousTab = m_intUnBilled And txtJobID.Text <> "" Then
    With adoJobDetail.Recordset
        If .RecordCount > 0 Then
            Dim l_intOrderBy As Integer
            .MoveFirst
            If .Fields("fd_orderby") <> "0" Then
                Do While Not .EOF
                    .Fields("fd_orderby") = l_intOrderBy
                    .Update
                    .MoveNext
                    l_intOrderBy = l_intOrderBy + 1
                Loop
            End If
        End If
    End With
End If

Select Case Trim(UCase(tabReplacementTabs.TabCaption(tabReplacementTabs.Tab)))
Case "SCHEDULE"
    tab1.Tab = m_intTabSchedule
Case "DUBBING"
    tab1.Tab = m_intTabJob
Case "MEDIA"
    tab1.Tab = m_intTabLibrary
Case "NOTES", "NOTES*"
    tab1.Tab = m_intTabNotes
Case "DADC SVENSK"
    tab1.Tab = m_intTabDADCSvensk
Case "COSTING"
    tab1.Tab = m_intTabCosting
Case "INVOICE"
    tab1.Tab = m_intTabInvoice
Case "SUNDRY"
    tab1.Tab = m_intTabSundryCosts
Case "DELIVERY CONTACTS"
    tab1.Tab = m_intTabQuote
Case "DESPATCH", "TO/FROM", "HIRE", "DEL/COL"
    tab1.Tab = m_intTabDespatch
Case "HISTORY"
    tab1.Tab = m_intTabTechnical
Case "BBC WW"
    If Val(lblCompanyID.Caption) = 570 Then
        tab1.Tab = m_intTabBBCWW
    Else
        tabReplacementTabs.Tab = PreviousTab
    End If
Case "PORTAL BILLING"
    tab1.Tab = m_intUnBilled
Case "WELLCOME"
    tab1.Tab = m_intProgress
    
End Select

End Sub



Private Sub txtAccountCode_GotFocus()
HighLite txtAccountCode
End Sub

Private Sub txtAccountCode_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    
    Dim l_strCompanyName As String
    l_strCompanyName = GetData("company", "name", "accountcode", txtAccountCode.Text)
    If l_strCompanyName <> "" Then
       
        cmbCompany.Text = l_strCompanyName
       
        Dim l_strSQL As String
        Dim l_lngCompanyID As Long
        
        l_lngCompanyID = GetData("company", "companyID", "accountcode", txtAccountCode.Text)
        lblCompanyID.Caption = l_lngCompanyID
        txtClientStatus.Text = GetData("company", "accountstatus", "companyID", l_lngCompanyID)
        
        If UCase(txtClientStatus.Text) = "HOLD" Or UCase(txtClientStatus.Text) = "CASH" Then
            txtClientStatus.BackColor = vbRed
        Else
            txtClientStatus.BackColor = vbWindowBackground
        End If
     
    End If
    
End If

End Sub

Private Sub txtBarcode_GotFocus()
    HighLite txtBarcode
    
End Sub

Private Sub txtBarcode_KeyPress(KeyAscii As Integer)

If KeyAscii = vbKeyReturn Then
    
    Dim l_intMsg As Integer
    l_intMsg = MsgBox("Are you sure you want to attach this tape to this job?", vbQuestion + vbYesNo)
    If l_intMsg = vbNo Then Exit Sub
    
    Dim l_lngJobID As Long
    Dim l_lngProjectNumber As Long
    Dim l_lngLibraryID As Long
    
    l_lngLibraryID = GetData("library", "libraryID", "barcode", txtBarcode.Text)
    l_lngJobID = Val(txtJobID.Text)
    
    l_lngProjectNumber = Val(cmbProject.Text)
    
    If l_lngJobID = 0 And l_lngProjectNumber = 0 Then Exit Sub

    KeyAscii = 0

    SaveJobDetailsToLibraryRecord l_lngLibraryID, l_lngJobID, l_lngProjectNumber
    
    tab1_Click m_intTabLibrary
    
    HighLite txtBarcode
    
End If

End Sub

Private Sub txtCostingSheet_GotFocus()

HighLite txtCostingSheet

End Sub

Private Sub txtCostingSheet_KeyPress(KeyAscii As Integer)

'if they don't have access
If Not CheckAccess("\attachjobtocostingsheet") Then Exit Sub

'if the enter key is pressed
If KeyAscii = vbKeyReturn Then

    'stop that little annoying beep
    KeyAscii = 0
    Dim l_lngCostingSheetID As Long
    
    'see if this job is already on a costing sheet
    l_lngCostingSheetID = Val(GetData("job", "costingsheetID", "jobID", txtJobID.Text))
    
    'if it is
    If l_lngCostingSheetID <> 0 Then
    
        'tell the user and exit
        MsgBox "This job is already attached to a costing sheet (" & l_lngCostingSheetID & ")", vbOKOnly + vbExclamation:        Exit Sub
        
    End If
    
    'make sure there is a costing sheet by that number
    If RecordExists("costingsheet", "costingsheetID", txtCostingSheet.Text) = False Then
    
        'no record
        MsgBox "No costing sheet exists for that ID", vbExclamation, "Missing costing": Exit Sub
    
    End If
    
    'otherwise ask them if they want to attach it
    Dim l_intAttach As Integer
    l_intAttach = MsgBox("Are you sure you want to attach this job to costing sheet number '" & txtCostingSheet.Text & "'?", vbYesNo + vbQuestion, "Attach sheet")
    
    'if no then exit
    If l_intAttach = vbNo Then Exit Sub
    
    'if we are here then the costing sheet is available, the job is cool and the user has the nuts
    'attach the job
    SetData "job", "costingsheetID", "jobID", txtJobID.Text, txtCostingSheet.Text
    
    'see if the operation was successful
    If GetData("job", "costingsheetID", "jobID", txtJobID.Text) = txtCostingSheet.Text Then
    
        'tell them
        MsgBox "Job was attached to costing sheet '" & txtCostingSheet.Text & "' successfuly", vbInformation
        
        'refresh the job
        ShowJob txtJobID.Text, m_intTabCosting, False
'        ShowJob txtJobID.Text, m_intTabCosting, False
        
    Else
    
        'tell them there was a problem
        MsgBox "There was a problem attaching the job to the costing sheet" & vbCrLf & vbCrLf & "Please try again in a moment", vbExclamation
        
    End If
    
End If

End Sub


Private Sub txtDespatch_GotFocus()
HighLite txtDespatch
End Sub

Private Sub txtInsertNotes_GotFocus()
HighLite txtInsertNotes
End Sub


Private Sub txtJobID_GotFocus()
HighLite txtJobID
End Sub

Private Sub txtJobID_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    If IsNumeric(txtJobID.Text) Then
        ShowJob Val(txtJobID.Text), g_intDefaultTab, True
'        ShowJob Val(txtJobID.Text), g_intDefaultTab, True
    End If
    HighLite txtJobID
End If

End Sub


Private Sub txtMediaRequiredBarcode_GotFocus()
HighLite txtMediaRequiredBarcode
End Sub

Private Sub txtMediaRequiredBarcode_KeyPress(KeyAscii As Integer)
If KeyAscii = vbKeyReturn Then

    KeyAscii = 0
    
    If txtMediaRequiredBarcode.Text = "" Then Exit Sub

    Dim l_lngLibraryID As Long
    Dim l_lngJobID As Long
    Dim l_strInsertSQL As String

    'store that sucker
    l_lngLibraryID = GetData("library", "libraryID", "barcode", txtMediaRequiredBarcode.Text)
    If l_lngLibraryID = 0 Then
        MsgBox "Sorry - No record exists for that tape.", vbCritical, "Error"
        Exit Sub
    End If
    
    l_lngJobID = Val(txtJobID.Text)
    
    'if there ain't one guv
    If l_lngJobID < 1 Then
        NoJobSelectedMessage
        Exit Sub
    End If

    'build it (its only easy, even a monkey could do it)
    l_strInsertSQL = "INSERT INTO requiredmedia (libraryid, barcode, eventID, jobid, cdate, cuser) VALUES ('" & l_lngLibraryID & "','" & UCase(txtMediaRequiredBarcode.Text) & "','" & 0 & "','" & l_lngJobID & "','" & FormatSQLDate(Now) & "','" & g_strUserName & "');"
        
    'then we need to run it
    ExecuteSQL l_strInsertSQL, g_strExecuteError
    
    'check for any errors
    If CheckForSQLError() = False Then MsgBox "Tape was added to required media list", vbInformation, "Added"
    
    tab1_Click m_intTabLibrary
    
    HighLite txtMediaRequiredBarcode

End If
End Sub

Private Sub txtOfficeClient_GotFocus()
HighLite txtOfficeClient
End Sub


Private Sub txtOperator_GotFocus()
HighLite txtOperator
End Sub

Private Sub txtOrderReference_GotFocus()
HighLite txtOrderReference
End Sub



Private Sub txtRateCardTotal_DblClick()

Dim l_intDoNotChargeVAT As Integer
l_intDoNotChargeVAT = GetData("job", "donotchargevat", "jobID", Val(txtJobID.Text))

Dim l_sngVATRate As Single
If l_intDoNotChargeVAT <> 0 Then
    l_sngVATRate = 0
End If

MsgBox "The approximate total including VAT for this job is " & g_strCurrency & Round(Val(txtRateCardTotal.Text * (l_sngVATRate / 100)) + txtRateCardTotal.Text, 2) & vbCrLf & vbCrLf & "The VAT amount (" & g_strCurrency & Round(Val(txtRateCardTotal.Text * (l_sngVATRate / 100)), 2) & ") is not calculated per row (as it should be) - it is just calculated on the rate card total.", vbInformation
End Sub

Private Sub txtSubTitle_GotFocus()
HighLite txtSubtitle
End Sub


Private Sub txtTelephone_GotFocus()
HighLite txtTelephone
End Sub


Private Sub txtTitle_GotFocus()
HighLite txtTitle
End Sub


Private Sub txtWorkOrderNumber_GotFocus()
HighLite txtWorkOrderNumber
End Sub

Private Sub txtWorkOrderNumber_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    If IsNumeric(txtWorkOrderNumber.Text) Then
        ShowJob GetData("job", "jobID", "workordernumber", txtWorkOrderNumber.Text), g_intDefaultTab, True
'        ShowJob GetData("job", "jobID", "workordernumber", txtWorkOrderNumber.Text), g_intDefaultTab, True
    End If
    HighLite txtWorkOrderNumber
End If

End Sub
