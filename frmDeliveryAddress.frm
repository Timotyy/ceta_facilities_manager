VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmDeliveryAddress 
   Caption         =   "Delivery Address Administration"
   ClientHeight    =   9480
   ClientLeft      =   2565
   ClientTop       =   2685
   ClientWidth     =   14820
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDeliveryAddress.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9480
   ScaleWidth      =   14820
   WindowState     =   2  'Maximized
   Begin TabDlg.SSTab SSTab1 
      Height          =   8535
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   14415
      _ExtentX        =   25426
      _ExtentY        =   15055
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Delivery Addresses"
      TabPicture(0)   =   "frmDeliveryAddress.frx":08CA
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "grdDelivery"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "adoDelivery"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Skibbly Customers"
      TabPicture(1)   =   "frmDeliveryAddress.frx":08E6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "adoSkibblyCustomers"
      Tab(1).Control(1)=   "grdSkibblyCustomers"
      Tab(1).ControlCount=   2
      Begin MSAdodcLib.Adodc adoSkibblyCustomers 
         Height          =   285
         Left            =   -74880
         Top             =   660
         Width           =   3000
         _ExtentX        =   5292
         _ExtentY        =   503
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   0
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "Browse"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoDelivery 
         Height          =   285
         Left            =   120
         Top             =   660
         Width           =   3000
         _ExtentX        =   5292
         _ExtentY        =   503
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   0
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "Browse"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDelivery 
         Bindings        =   "frmDeliveryAddress.frx":0902
         Height          =   7695
         Left            =   120
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   660
         Width           =   14145
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         AllowRowSizing  =   0   'False
         BackColorOdd    =   12648384
         RowHeight       =   423
         ExtraHeight     =   318
         Columns.Count   =   8
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "deliveryaddressID"
         Columns(0).Name =   "deliveryaddressID"
         Columns(0).DataField=   "deliveryaddressID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   1905
         Columns(1).Caption=   "CompanyID"
         Columns(1).Name =   "companyID"
         Columns(1).DataField=   "companyID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   4286
         Columns(2).Caption=   "Delivery Company Name"
         Columns(2).Name =   "deliverycompanyname"
         Columns(2).DataField=   "deliverycompanyname"
         Columns(2).FieldLen=   256
         Columns(3).Width=   5689
         Columns(3).Caption=   "Address"
         Columns(3).Name =   "address"
         Columns(3).DataField=   "address"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   2381
         Columns(4).Caption=   "Postcode"
         Columns(4).Name =   "postcode"
         Columns(4).DataField=   "postcode"
         Columns(4).FieldLen=   256
         Columns(5).Width=   3784
         Columns(5).Caption=   "Country"
         Columns(5).Name =   "country"
         Columns(5).DataField=   "country"
         Columns(5).FieldLen=   256
         Columns(6).Width=   2778
         Columns(6).Caption=   "Attention of"
         Columns(6).Name =   "attentionof"
         Columns(6).DataField=   "attentionof"
         Columns(6).FieldLen=   256
         Columns(7).Width=   2858
         Columns(7).Caption=   "Telephone"
         Columns(7).Name =   "telephone"
         Columns(7).DataField=   "telephone"
         Columns(7).FieldLen=   256
         _ExtentX        =   24950
         _ExtentY        =   13573
         _StockProps     =   79
         Caption         =   "Despatch Information"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdSkibblyCustomers 
         Bindings        =   "frmDeliveryAddress.frx":091C
         Height          =   7695
         Left            =   -74880
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   660
         Width           =   14145
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         AllowRowSizing  =   0   'False
         BackColorOdd    =   12648384
         RowHeight       =   423
         ExtraHeight     =   661
         Columns.Count   =   9
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "deliveryaddressID"
         Columns(0).Name =   "skibblycustomerID"
         Columns(0).DataField=   "skibblycustomerID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   4286
         Columns(1).Caption=   "Customer Name"
         Columns(1).Name =   "customername"
         Columns(1).DataField=   "customername"
         Columns(1).FieldLen=   256
         Columns(2).Width=   4286
         Columns(2).Caption=   "Company Name"
         Columns(2).Name =   "companyname"
         Columns(2).DataField=   "companyname"
         Columns(2).FieldLen=   256
         Columns(3).Width=   5689
         Columns(3).Caption=   "Address"
         Columns(3).Name =   "address"
         Columns(3).DataField=   "address"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   2381
         Columns(4).Caption=   "Postcode"
         Columns(4).Name =   "postcode"
         Columns(4).DataField=   "postcode"
         Columns(4).FieldLen=   256
         Columns(5).Width=   3784
         Columns(5).Caption=   "Country"
         Columns(5).Name =   "country"
         Columns(5).DataField=   "country"
         Columns(5).FieldLen=   256
         Columns(6).Width=   2778
         Columns(6).Caption=   "Attention of"
         Columns(6).Name =   "attentionof"
         Columns(6).DataField=   "attentionof"
         Columns(6).FieldLen=   256
         Columns(7).Width=   2858
         Columns(7).Caption=   "Telephone"
         Columns(7).Name =   "telephone"
         Columns(7).DataField=   "telephone"
         Columns(7).FieldLen=   256
         Columns(8).Width=   1588
         Columns(8).Caption=   "List Order"
         Columns(8).Name =   "orderby"
         Columns(8).DataField=   "orderby"
         Columns(8).FieldLen=   256
         _ExtentX        =   24950
         _ExtentY        =   13573
         _StockProps     =   79
         Caption         =   "Skibbly Customers"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   13620
      TabIndex        =   0
      ToolTipText     =   "Close this form"
      Top             =   9120
      Width           =   1155
   End
End
Attribute VB_Name = "frmDeliveryAddress"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdClose_Click()
Me.Hide
End Sub

Private Sub Form_Load()

adoDelivery.RecordSource = "SELECT * FROM deliveryaddress ORDER BY deliverycompanyname"
adoDelivery.ConnectionString = g_strConnection
adoDelivery.Refresh

adoSkibblyCustomers.RecordSource = "SELECT * FROM skibblycustomer ORDER BY customername"
adoSkibblyCustomers.ConnectionString = g_strConnection
adoSkibblyCustomers.Refresh

End Sub

Private Sub Form_Resize()

SSTab1.Height = Me.ScaleHeight - SSTab1.Top - cmdClose.Height - 120 - 120
SSTab1.Width = Me.ScaleWidth - SSTab1.Left - 120

grdDelivery.Height = SSTab1.Height - grdDelivery.Top - 120
grdDelivery.Width = SSTab1.Width - 240

grdSkibblyCustomers.Height = SSTab1.Height - grdSkibblyCustomers.Top - 120
grdSkibblyCustomers.Width = SSTab1.Width - 240

cmdClose.Top = Me.ScaleHeight - cmdClose.Height - 120
cmdClose.Left = Me.ScaleWidth - cmdClose.Width - 120

End Sub

Private Sub grdSkibblyCustomers_BeforeUpdate(Cancel As Integer)

If grdSkibblyCustomers.Columns("orderby").Text = "" Then

    Dim l_lngOrderby As Long
    l_lngOrderby = GetCount("SELECT COUNT(skibblycustomerid) FROM skibblycustomer;")
        
    grdSkibblyCustomers.Columns("orderby").Text = l_lngOrderby
End If

End Sub
