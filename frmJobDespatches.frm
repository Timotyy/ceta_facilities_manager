VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmJobDespatch 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Delivery Notes"
   ClientHeight    =   9060
   ClientLeft      =   1965
   ClientTop       =   3405
   ClientWidth     =   15195
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmJobDespatches.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9060
   ScaleWidth      =   15195
   Begin VB.ComboBox cmbLocation 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1260
      TabIndex        =   95
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The New Location?"
      Top             =   10440
      Visible         =   0   'False
      Width           =   2235
   End
   Begin VB.CommandButton cmdBarcodeKeypressReturn 
      Caption         =   "Command1"
      Height          =   555
      Left            =   13860
      TabIndex        =   94
      Top             =   1140
      Visible         =   0   'False
      Width           =   915
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbBillToCompany 
      Bindings        =   "frmJobDespatches.frx":08CA
      Height          =   315
      Left            =   11400
      TabIndex        =   90
      ToolTipText     =   "Which Client is this Despatch For?"
      Top             =   2160
      Visible         =   0   'False
      Width           =   3495
      DataFieldList   =   "name"
      _Version        =   196617
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5609
      Columns(0).Caption=   "Company Name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   6165
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin VB.TextBox txtProduct 
      BackColor       =   &H00C0E0FF&
      Height          =   285
      Left            =   7860
      Locked          =   -1  'True
      TabIndex        =   9
      ToolTipText     =   "The couriers reference for this delivery"
      Top             =   540
      Width           =   2055
   End
   Begin VB.CommandButton cmdShowPurchaseOrder 
      Caption         =   "..."
      Height          =   315
      Left            =   9600
      TabIndex        =   4
      ToolTipText     =   "Make a PDF of this Despatch"
      Top             =   120
      Width           =   315
   End
   Begin VB.TextBox txtCourierReference 
      BackColor       =   &H00C0FFC0&
      Height          =   285
      Left            =   11400
      MaxLength       =   10
      TabIndex        =   27
      ToolTipText     =   "The couriers reference for this delivery"
      Top             =   1800
      Width           =   2235
   End
   Begin VB.ComboBox cmbDeliveredBy 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   11400
      TabIndex        =   26
      ToolTipText     =   "Who is this despatch for or delivered by"
      Top             =   1380
      Width           =   2235
   End
   Begin VB.CheckBox chkUseCompany 
      Caption         =   "Display company address list / Save Address"
      ForeColor       =   &H00FF0000&
      Height          =   195
      Left            =   5520
      TabIndex        =   13
      ToolTipText     =   "Should the Client List Addresses be used?"
      Top             =   1860
      Width           =   3495
   End
   Begin MSAdodcLib.Adodc adoResourceSchedule 
      Height          =   330
      Left            =   2460
      Top             =   3840
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoResourceSchedule"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CheckBox chkExpand 
      Alignment       =   1  'Right Justify
      Caption         =   "Grow"
      Height          =   255
      Left            =   13800
      TabIndex        =   72
      Top             =   4260
      Width           =   675
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDespatchedItems 
      Bindings        =   "frmJobDespatches.frx":08E9
      Height          =   3255
      Left            =   4380
      TabIndex        =   52
      ToolTipText     =   "Information about the items in the Despatch"
      Top             =   5100
      Width           =   10515
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   1
      BackColorOdd    =   12648384
      RowHeight       =   423
      Columns.Count   =   13
      Columns(0).Width=   741
      Columns(0).Caption=   "Box"
      Columns(0).Name =   "packagenumber"
      Columns(0).DataField=   "packagenumber"
      Columns(0).FieldLen=   256
      Columns(1).Width=   1640
      Columns(1).Caption=   "Project #"
      Columns(1).Name =   "projectnumber"
      Columns(1).DataField=   "projectnumber"
      Columns(1).FieldLen=   256
      Columns(2).Width=   1720
      Columns(2).Caption=   "Job ID"
      Columns(2).Name =   "Job No"
      Columns(2).DataField=   "jobID"
      Columns(2).FieldLen=   256
      Columns(3).Width=   1561
      Columns(3).Caption=   "Type"
      Columns(3).Name =   "copytype"
      Columns(3).DataField=   "copytype"
      Columns(3).FieldLen=   256
      Columns(4).Width=   714
      Columns(4).Caption=   "Qty"
      Columns(4).Name =   "quantity"
      Columns(4).DataField=   "quantity"
      Columns(4).FieldLen=   256
      Columns(5).Width=   1614
      Columns(5).Caption=   "Format"
      Columns(5).Name =   "format"
      Columns(5).DataField=   "format"
      Columns(5).FieldLen=   256
      Columns(6).Width=   1535
      Columns(6).Caption=   "Standard"
      Columns(6).Name =   "videostandard"
      Columns(6).DataField=   "videostandard"
      Columns(6).FieldLen=   256
      Columns(7).Width=   2249
      Columns(7).Caption=   "Barcode"
      Columns(7).Name =   "barcode"
      Columns(7).DataField=   "barcode"
      Columns(7).FieldLen=   256
      Columns(8).Width=   4260
      Columns(8).Caption=   "Item Description"
      Columns(8).Name =   "title"
      Columns(8).DataField=   "title"
      Columns(8).FieldLen=   256
      Columns(9).Width=   4180
      Columns(9).Caption=   "Sub Description"
      Columns(9).Name =   "subtitle"
      Columns(9).DataField=   "subtitle"
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "despatchID"
      Columns(10).Name=   "despatchID"
      Columns(10).DataField=   "despatchID"
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "despatchdetailID"
      Columns(11).Name=   "despatchdetailID"
      Columns(11).DataField=   "despatchdetailID"
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "jobdetailID"
      Columns(12).Name=   "jobdetailID"
      Columns(12).DataField=   "jobdetailID"
      Columns(12).FieldLen=   256
      _ExtentX        =   18547
      _ExtentY        =   5741
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdComplete 
      Caption         =   "Received"
      Height          =   315
      Left            =   14100
      TabIndex        =   7
      ToolTipText     =   "Clear the form"
      Top             =   120
      Width           =   855
   End
   Begin VB.ComboBox cmbDeliveryFor 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   11400
      TabIndex        =   23
      ToolTipText     =   "Transportation Method"
      Top             =   540
      Width           =   2235
   End
   Begin VB.OptionButton optWhatToScan 
      Caption         =   "Resources"
      Height          =   255
      Index           =   1
      Left            =   11400
      TabIndex        =   32
      ToolTipText     =   "Scan equipment records from the resource database"
      Top             =   3960
      Width           =   1155
   End
   Begin VB.OptionButton optWhatToScan 
      Caption         =   "Media"
      Height          =   255
      Index           =   0
      Left            =   10140
      TabIndex        =   31
      Tag             =   "NOCLEAR"
      ToolTipText     =   "Scan media records from the library database"
      Top             =   3960
      Value           =   -1  'True
      Width           =   1155
   End
   Begin VB.TextBox txtDespatchNumber 
      BackColor       =   &H0080FF80&
      ForeColor       =   &H80000012&
      Height          =   285
      Left            =   5520
      TabIndex        =   2
      ToolTipText     =   "The Despatch Identifier"
      Top             =   120
      Width           =   1275
   End
   Begin VB.CommandButton cmdNewJobDespatch 
      Caption         =   "New Project Despatch"
      Height          =   315
      Index           =   1
      Left            =   1020
      TabIndex        =   45
      ToolTipText     =   "Make a PDF of this Despatch"
      Top             =   9420
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox txtProjectNumber 
      BackColor       =   &H00C0FFFF&
      Height          =   285
      Left            =   840
      TabIndex        =   44
      ToolTipText     =   "The uniqoe Job Identifier"
      Top             =   120
      Width           =   1035
   End
   Begin VB.CommandButton cmdNewJobDespatch 
      Caption         =   "New..."
      Height          =   285
      Index           =   0
      Left            =   3420
      TabIndex        =   1
      Top             =   480
      Width           =   855
   End
   Begin VB.TextBox txtPurchaseAuthorisationNumber 
      BackColor       =   &H0080C0FF&
      Height          =   285
      Left            =   7860
      TabIndex        =   3
      ToolTipText     =   "The unique identifier for this job"
      Top             =   120
      Width           =   1635
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   3180
      ScaleHeight     =   315
      ScaleWidth      =   11715
      TabIndex        =   67
      Top             =   8580
      Width           =   11715
      Begin VB.CommandButton cmdShowJob 
         Caption         =   "Show Job"
         Height          =   315
         Left            =   0
         TabIndex        =   35
         ToolTipText     =   "Make a PDF of this Despatch"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrintLabel 
         Caption         =   "Print Label"
         Height          =   315
         Left            =   2760
         TabIndex        =   37
         ToolTipText     =   "Print Label for this Despatch "
         Top             =   0
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   6780
         TabIndex        =   40
         ToolTipText     =   "Clear the form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         Height          =   315
         Left            =   9300
         TabIndex        =   42
         ToolTipText     =   "Save changes to this despatch"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   10560
         TabIndex        =   43
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "Delete"
         Height          =   315
         Left            =   8040
         TabIndex        =   41
         ToolTipText     =   "Delete this Despatch"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   5520
         TabIndex        =   39
         ToolTipText     =   "Print this Despatch"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrintPDF 
         Caption         =   "Make PDF"
         Height          =   315
         Left            =   1500
         TabIndex        =   36
         ToolTipText     =   "Make a PDF of this Despatch"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrintWWDespatch 
         Caption         =   "Print WW Desp"
         Height          =   315
         Left            =   4020
         TabIndex        =   38
         ToolTipText     =   "Print this Despatch on a W/W Despatch Sheet"
         Top             =   0
         Width           =   1395
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnStandard 
      Height          =   1755
      Left            =   8460
      TabIndex        =   66
      Top             =   6000
      Width           =   1035
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      BackColorOdd    =   8454016
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   3200
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "Description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   1826
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnCopyType 
      Height          =   1755
      Left            =   5940
      TabIndex        =   65
      Top             =   6000
      Width           =   1035
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      BackColorOdd    =   8454016
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   3200
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "Description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   1826
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnFormat 
      Height          =   1755
      Left            =   7200
      TabIndex        =   64
      Top             =   6000
      Width           =   1035
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      BackColorOdd    =   8454016
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   3200
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "Description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   1826
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.TextBox txtNoOfCopies 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   210
      Left            =   2580
      MaxLength       =   50
      TabIndex        =   34
      Text            =   "2"
      ToolTipText     =   "Number of Copies of the Despatch Note to Print"
      Top             =   8580
      Width           =   435
   End
   Begin VB.ComboBox cmbMethod 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   11400
      TabIndex        =   25
      ToolTipText     =   "Transportation Method"
      Top             =   960
      Width           =   2235
   End
   Begin VB.ComboBox cmbDirection 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1320
      TabIndex        =   8
      Text            =   "OUT"
      ToolTipText     =   "Incoming or Outgoing?"
      Top             =   9960
      Visible         =   0   'False
      Width           =   1275
   End
   Begin MSAdodcLib.Adodc adoDespatchDetail 
      Height          =   330
      Left            =   9900
      Top             =   9240
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoDelivery"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox txtDescription 
      BackColor       =   &H00C0FFC0&
      Height          =   285
      Left            =   5520
      MaxLength       =   50
      TabIndex        =   10
      ToolTipText     =   "A Heading for the Despatch"
      Top             =   960
      Width           =   4395
   End
   Begin VB.TextBox txtBarcode 
      BackColor       =   &H0080FFFF&
      Height          =   285
      Left            =   10080
      TabIndex        =   33
      ToolTipText     =   "Scan the Barcodes of the items on the Despatch"
      Top             =   4260
      Width           =   3255
   End
   Begin MSAdodcLib.Adodc adoDelivery 
      Height          =   330
      Left            =   1860
      Top             =   900
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoDelivery"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoJobDetail 
      Height          =   330
      Left            =   1860
      Top             =   4320
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoJobDetail"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox txtDespatchedUser 
      BackColor       =   &H00C0FFC0&
      Height          =   285
      Left            =   13200
      Locked          =   -1  'True
      TabIndex        =   6
      ToolTipText     =   "Who made this Despatch?"
      Top             =   120
      Width           =   795
   End
   Begin VB.TextBox txtDespatchedOn 
      BackColor       =   &H00C0FFC0&
      Height          =   285
      Left            =   11040
      Locked          =   -1  'True
      TabIndex        =   5
      ToolTipText     =   "Despatch Date and Time"
      Top             =   120
      Width           =   1875
   End
   Begin VB.CommandButton cmdAddItem 
      Caption         =   "Add >>"
      Height          =   315
      Left            =   120
      TabIndex        =   47
      ToolTipText     =   "Add an Item to the Depstach"
      Top             =   8580
      Width           =   1155
   End
   Begin VB.ComboBox cmbDeadlineTime 
      Height          =   315
      Left            =   7020
      TabIndex        =   12
      ToolTipText     =   "The deadline time"
      Top             =   1380
      Width           =   975
   End
   Begin VB.CheckBox chkUrgent 
      Alignment       =   1  'Right Justify
      Caption         =   "Urgent"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   285
      Left            =   8940
      TabIndex        =   46
      ToolTipText     =   "Is this Despatch Urgent?"
      Top             =   10020
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox txtDespatchID 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000011&
      Height          =   315
      Left            =   7200
      TabIndex        =   48
      ToolTipText     =   "The Despatch Identifier"
      Top             =   9660
      Width           =   975
   End
   Begin MSComCtl2.DTPicker datDeadlineDate 
      Height          =   315
      Left            =   5520
      TabIndex        =   11
      ToolTipText     =   "The deadline date"
      Top             =   1380
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      Format          =   166854657
      CurrentDate     =   37870
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDetail 
      Bindings        =   "frmJobDespatches.frx":0909
      Height          =   5175
      Left            =   120
      TabIndex        =   55
      TabStop         =   0   'False
      ToolTipText     =   "Information about the type of duplication on this job"
      Top             =   3180
      Width           =   4155
      _Version        =   196617
      AllowUpdate     =   0   'False
      SelectTypeRow   =   1
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   7
      Columns(0).Width=   370
      Columns(0).Name =   "copytype"
      Columns(0).DataField=   "copytype"
      Columns(0).FieldLen=   256
      Columns(1).Width=   1773
      Columns(1).Caption=   "Description"
      Columns(1).Name =   "description"
      Columns(1).DataField=   "description"
      Columns(1).FieldLen=   256
      Columns(2).Width=   714
      Columns(2).Caption=   "Qty"
      Columns(2).Name =   "quantity"
      Columns(2).DataField=   "quantity"
      Columns(2).FieldLen=   256
      Columns(3).Width=   1508
      Columns(3).Caption=   "Format"
      Columns(3).Name =   "format"
      Columns(3).DataField=   "format"
      Columns(3).FieldLen=   256
      Columns(4).Width=   2037
      Columns(4).Caption=   "Standard"
      Columns(4).Name =   "standard"
      Columns(4).DataField=   "standard"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "jobID"
      Columns(5).Name =   "jobID"
      Columns(5).DataField=   "jobID"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "jobdetailID"
      Columns(6).Name =   "jobdetailID"
      Columns(6).DataField=   "jobdetailID"
      Columns(6).FieldLen=   256
      _ExtentX        =   7329
      _ExtentY        =   9128
      _StockProps     =   79
      Caption         =   "Dubbing Instructions"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoContact 
      Height          =   330
      Left            =   7560
      Top             =   9180
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoContact"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoCompany 
      Height          =   330
      Left            =   5040
      Top             =   9240
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCompany"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoBillToCompany 
      Height          =   330
      Left            =   12300
      Top             =   9240
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoBillToCompany"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.PictureBox picContainer 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   2595
      Left            =   4380
      ScaleHeight     =   2595
      ScaleWidth      =   10515
      TabIndex        =   73
      Top             =   2460
      Width           =   10515
      Begin VB.ComboBox cmbCostCode 
         Height          =   315
         Left            =   8040
         TabIndex        =   30
         ToolTipText     =   "The deadline time"
         Top             =   1140
         Width           =   2355
      End
      Begin VB.TextBox txtCountry 
         Height          =   285
         Left            =   3480
         MaxLength       =   50
         TabIndex        =   19
         ToolTipText     =   "Country of Despatch"
         Top             =   1500
         Width           =   975
      End
      Begin VB.TextBox txtPostCode 
         Height          =   285
         Left            =   1140
         MaxLength       =   10
         TabIndex        =   17
         ToolTipText     =   "Postcode of Despatch"
         Top             =   1500
         Width           =   1155
      End
      Begin VB.TextBox txtAddress 
         Height          =   915
         Left            =   1140
         MaxLength       =   255
         MultiLine       =   -1  'True
         TabIndex        =   16
         ToolTipText     =   "Address of Despatch"
         Top             =   480
         Width           =   3315
      End
      Begin VB.TextBox txtNotes 
         Height          =   915
         Left            =   5700
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   28
         ToolTipText     =   "Notes for the Despatch"
         Top             =   120
         Width           =   4755
      End
      Begin VB.TextBox txtTelephone 
         Height          =   285
         Left            =   1140
         MaxLength       =   255
         MultiLine       =   -1  'True
         TabIndex        =   20
         ToolTipText     =   "Telephone number of Despatch"
         Top             =   1860
         Width           =   3315
      End
      Begin VB.CommandButton cmdLaunch 
         Appearance      =   0  'Flat
         Height          =   315
         Index           =   2
         Left            =   2400
         MaskColor       =   &H00FFFFFF&
         Picture         =   "frmJobDespatches.frx":0924
         Style           =   1  'Graphical
         TabIndex        =   18
         ToolTipText     =   "Show on-line map (using multimap)"
         Top             =   1500
         UseMaskColor    =   -1  'True
         Width           =   300
      End
      Begin VB.TextBox txtCharge 
         BackColor       =   &H00C0C0FF&
         Height          =   285
         Left            =   5700
         MaxLength       =   255
         MultiLine       =   -1  'True
         TabIndex        =   29
         ToolTipText     =   "Telephone number of Despatch"
         Top             =   1140
         Width           =   1215
      End
      Begin VB.TextBox txtEmail 
         Height          =   285
         Left            =   1140
         MaxLength       =   255
         MultiLine       =   -1  'True
         TabIndex        =   21
         ToolTipText     =   "Telephone number of Despatch"
         Top             =   2220
         Width           =   2895
      End
      Begin VB.CommandButton cmdLaunch 
         Appearance      =   0  'Flat
         Height          =   330
         Index           =   0
         Left            =   4140
         MaskColor       =   &H00FFFFFF&
         Picture         =   "frmJobDespatches.frx":0D36
         Style           =   1  'Graphical
         TabIndex        =   22
         ToolTipText     =   "Send email"
         Top             =   2220
         UseMaskColor    =   -1  'True
         Width           =   330
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbContact 
         Bindings        =   "frmJobDespatches.frx":116E
         Height          =   315
         Left            =   1140
         TabIndex        =   15
         ToolTipText     =   "For the Attention of?"
         Top             =   60
         Width           =   3315
         DataFieldList   =   "name"
         BevelType       =   0
         _Version        =   196617
         BorderStyle     =   0
         ColumnHeaders   =   0   'False
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   5609
         Columns(0).Caption=   "Company Name"
         Columns(0).Name =   "name"
         Columns(0).DataField=   "name"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "companyID"
         Columns(1).Name =   "companyID"
         Columns(1).DataField=   "companyID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "contactID"
         Columns(2).Name =   "contactID"
         Columns(2).DataField=   "contactID"
         Columns(2).FieldLen=   256
         _ExtentX        =   5847
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "name"
      End
      Begin VB.Label lblCaption 
         Caption         =   "Cost Code"
         Height          =   315
         Index           =   29
         Left            =   7080
         TabIndex        =   84
         Top             =   1140
         Width           =   795
      End
      Begin VB.Label lblCaption 
         Caption         =   "Barcode"
         Height          =   315
         Index           =   11
         Left            =   4560
         TabIndex        =   82
         Top             =   1800
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Country"
         Height          =   315
         Index           =   4
         Left            =   2760
         TabIndex        =   81
         Top             =   1500
         Width           =   615
      End
      Begin VB.Label lblCaption 
         Caption         =   "Post Code"
         Height          =   315
         Index           =   3
         Left            =   0
         TabIndex        =   80
         Top             =   1500
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Address"
         Height          =   315
         Index           =   1
         Left            =   0
         TabIndex        =   79
         Top             =   480
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Contact"
         Height          =   315
         Index           =   6
         Left            =   0
         TabIndex        =   78
         Top             =   60
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Notes"
         Height          =   315
         Index           =   7
         Left            =   4560
         TabIndex        =   77
         Top             =   120
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Telephone"
         Height          =   315
         Index           =   20
         Left            =   0
         TabIndex        =   76
         Top             =   1860
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Charge (�)"
         Height          =   315
         Index           =   22
         Left            =   4560
         TabIndex        =   75
         Top             =   1140
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Email"
         Height          =   315
         Index           =   24
         Left            =   0
         TabIndex        =   74
         Top             =   2220
         Width           =   1035
      End
   End
   Begin MSAdodcLib.Adodc adoOutgoingDNotes 
      Height          =   330
      Left            =   2340
      Top             =   9060
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoOutgoingDNotes"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbJobID 
      Height          =   285
      Left            =   2520
      TabIndex        =   0
      Top             =   120
      Width           =   1755
      DataFieldList   =   "job id"
      BevelType       =   0
      _Version        =   196617
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   3096
      _ExtentY        =   503
      _StockProps     =   93
      ForeColor       =   -2147483640
      BackColor       =   16761024
      DataFieldToDisplay=   "job id"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDelivery 
      Bindings        =   "frmJobDespatches.frx":1187
      Height          =   2595
      Left            =   120
      TabIndex        =   86
      TabStop         =   0   'False
      Top             =   480
      Width           =   4155
      _Version        =   196617
      BeveColorScheme =   1
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   0
      BackColorOdd    =   16773364
      RowHeight       =   423
      Columns.Count   =   17
      Columns(0).Width=   1296
      Columns(0).Caption=   "DNote"
      Columns(0).Name =   "despatchnumber"
      Columns(0).DataField=   "despatchnumber"
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   10744793
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "Job ID"
      Columns(1).Name =   "jobID"
      Columns(1).DataField=   "jobID"
      Columns(1).DataType=   17
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16239283
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "Project"
      Columns(2).Name =   "projectnumber"
      Columns(2).DataField=   "projectnumber"
      Columns(2).FieldLen=   256
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   9040629
      Columns(3).Width=   1296
      Columns(3).Caption=   "P/O"
      Columns(3).Name =   "purchaseauthorisationnumber"
      Columns(3).DataField=   "purchaseauthorisationnumber"
      Columns(3).FieldLen=   256
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   15389688
      Columns(4).Width=   900
      Columns(4).Caption=   "Dir..."
      Columns(4).Name =   "direction"
      Columns(4).DataField=   "direction"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   3016
      Columns(5).Caption=   "Description"
      Columns(5).Name =   "description"
      Columns(5).DataField=   "description"
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   3387
      Columns(6).Caption=   "Company"
      Columns(6).Name =   "company"
      Columns(6).DataField=   "companyname"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   1614
      Columns(7).Caption=   "Post Code"
      Columns(7).Name =   "postcode"
      Columns(7).DataField=   "postcode"
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "Date Created"
      Columns(8).Name =   "createddate"
      Columns(8).DataField=   "createddate"
      Columns(8).DataType=   7
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(9).Width=   794
      Columns(9).Caption=   "User"
      Columns(9).Name =   "creadtedby"
      Columns(9).DataField=   "createduser"
      Columns(9).FieldLen=   256
      Columns(9).Locked=   -1  'True
      Columns(10).Width=   3069
      Columns(10).Caption=   "Deadline"
      Columns(10).Name=   "deadlinedate"
      Columns(10).DataField=   "deadlinedate"
      Columns(10).DataType=   7
      Columns(10).FieldLen=   256
      Columns(10).Locked=   -1  'True
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "despatcheduser"
      Columns(11).Name=   "despatcheduser"
      Columns(11).DataField=   "despatcheduser"
      Columns(11).FieldLen=   256
      Columns(12).Width=   3069
      Columns(12).Caption=   "Despatched"
      Columns(12).Name=   "despatcheddate"
      Columns(12).DataField=   "despatcheddate"
      Columns(12).DataType=   7
      Columns(12).FieldLen=   256
      Columns(12).Style=   1
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "flagurgent"
      Columns(13).Name=   "flagurgent"
      Columns(13).DataField=   "flagurgent"
      Columns(13).FieldLen=   256
      Columns(13).Locked=   -1  'True
      Columns(14).Width=   2646
      Columns(14).Caption=   "Method"
      Columns(14).Name=   "deliverymethod"
      Columns(14).DataField=   "deliverymethod"
      Columns(14).FieldLen=   256
      Columns(15).Width=   2143
      Columns(15).Caption=   "Delivered By"
      Columns(15).Name=   "deliveredby"
      Columns(15).DataField=   "deliveredby"
      Columns(15).FieldLen=   256
      Columns(16).Width=   3200
      Columns(16).Caption=   "typeofjob"
      Columns(16).Name=   "typeofjob"
      Columns(16).DataField=   "typeofjob"
      Columns(16).FieldLen=   256
      _ExtentX        =   7329
      _ExtentY        =   4577
      _StockProps     =   79
      Caption         =   "Outgoing Despatches"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Bindings        =   "frmJobDespatches.frx":11A1
      Height          =   315
      Left            =   5520
      TabIndex        =   14
      ToolTipText     =   "Company Name for Despatch"
      Top             =   2160
      Width           =   3315
      DataFieldList   =   "name"
      ListAutoValidate=   0   'False
      BevelType       =   0
      _Version        =   196617
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   5609
      Columns(0).Caption=   "Company Name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "address"
      Columns(1).Name =   "address"
      Columns(1).DataField=   "address"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "postcode"
      Columns(2).Name =   "postcode"
      Columns(2).DataField=   "postcode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "country"
      Columns(3).Name =   "country"
      Columns(3).DataField=   "country"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "attentionof"
      Columns(4).Name =   "attentionof"
      Columns(4).DataField=   "attentionof"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "telephone"
      Columns(5).Name =   "telephone"
      Columns(5).DataField=   "telephone"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3201
      Columns(6).Caption=   "theID"
      Columns(6).Name =   "theID"
      Columns(6).DataField=   "theID"
      Columns(6).FieldLen=   256
      _ExtentX        =   5847
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "name"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdResourceSchedule 
      Bindings        =   "frmJobDespatches.frx":11BA
      Height          =   5175
      Left            =   120
      TabIndex        =   85
      Top             =   3180
      Width           =   4155
      _Version        =   196617
      AllowUpdate     =   0   'False
      BackColorOdd    =   12648447
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   7329
      _ExtentY        =   9128
      _StockProps     =   79
   End
   Begin VB.Label lblCaption 
      Caption         =   "Location"
      Height          =   315
      Index           =   18
      Left            =   120
      TabIndex        =   96
      Top             =   10440
      Width           =   1035
   End
   Begin VB.Label lblSilentSave 
      BackColor       =   &H00C0FFFF&
      Height          =   255
      Left            =   8280
      TabIndex        =   93
      Top             =   1440
      Width           =   1575
   End
   Begin VB.Label lblCaption 
      Caption         =   "Product"
      Height          =   315
      Index           =   30
      Left            =   6900
      TabIndex        =   92
      Top             =   540
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Bill To"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   13
      Left            =   10260
      TabIndex        =   91
      Top             =   2160
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Courier Ref."
      Height          =   315
      Index           =   25
      Left            =   10260
      TabIndex        =   89
      Top             =   1800
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Delivered By"
      Height          =   315
      Index           =   21
      Left            =   10260
      TabIndex        =   88
      Top             =   1380
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Send To"
      Height          =   315
      Index           =   2
      Left            =   4380
      TabIndex        =   87
      Top             =   2160
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Barcode"
      Height          =   315
      Index           =   28
      Left            =   4380
      TabIndex        =   83
      Top             =   7920
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Delivery For"
      Height          =   315
      Index           =   12
      Left            =   10260
      TabIndex        =   71
      Top             =   540
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Despatch #"
      Height          =   315
      Index           =   27
      Left            =   4380
      TabIndex        =   70
      Top             =   120
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Project #"
      Height          =   315
      Index           =   26
      Left            =   120
      TabIndex        =   69
      Top             =   120
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "P/O  Auth #"
      Height          =   315
      Index           =   23
      Left            =   6900
      TabIndex        =   68
      Top             =   120
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Copies to print:"
      Height          =   255
      Index           =   19
      Left            =   1380
      TabIndex        =   63
      Top             =   8580
      Width           =   1155
   End
   Begin VB.Label lblItemCount 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "0"
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   14460
      TabIndex        =   24
      ToolTipText     =   "Number of Items in the Depstach"
      Top             =   600
      Width           =   435
   End
   Begin VB.Label lblCaption 
      Caption         =   "Items:"
      Height          =   315
      Index           =   17
      Left            =   13800
      TabIndex        =   62
      Top             =   600
      Width           =   555
   End
   Begin VB.Label lblCaption 
      Caption         =   "Method"
      Height          =   315
      Index           =   16
      Left            =   10260
      TabIndex        =   61
      Top             =   960
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Description"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   15
      Left            =   4380
      TabIndex        =   60
      Top             =   960
      Width           =   1035
   End
   Begin VB.Label lblContactID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   12060
      TabIndex        =   59
      Tag             =   "CLEARFIELDS"
      Top             =   10260
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblBillToCompanyID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   12060
      TabIndex        =   58
      Tag             =   "CLEARFIELDS"
      Top             =   10680
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCompanyID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   12060
      TabIndex        =   57
      Tag             =   "CLEARFIELDS"
      Top             =   9840
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job ID"
      Height          =   255
      Index           =   14
      Left            =   1920
      TabIndex        =   56
      Top             =   120
      Width           =   495
   End
   Begin VB.Label lblCaption 
      Caption         =   "By"
      Height          =   315
      Index           =   10
      Left            =   12960
      TabIndex        =   54
      Top             =   120
      Width           =   255
   End
   Begin VB.Label lblCaption 
      Caption         =   "Despatched"
      Height          =   315
      Index           =   9
      Left            =   10020
      TabIndex        =   53
      Top             =   120
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Deadline / E.T.A."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Index           =   8
      Left            =   4380
      TabIndex        =   51
      Top             =   1380
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Direction"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   5
      Left            =   180
      TabIndex        =   50
      Top             =   9960
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Despatch ID"
      ForeColor       =   &H80000011&
      Height          =   315
      Index           =   0
      Left            =   6060
      TabIndex        =   49
      Top             =   9660
      Width           =   1035
   End
End
Attribute VB_Name = "frmJobDespatch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub chkExpand_Click()

If chkExpand.Value = 0 Then
    optWhatToScan(0).Top = 3960
    optWhatToScan(1).Top = 3960
    grdDespatchedItems.Top = 5100
    grdDespatchedItems.Height = 3255
    chkExpand.Top = 4260
    txtBarcode.Top = 4260
    txtBarcode.Left = 10080
    picContainer.Enabled = True
Else
    optWhatToScan(0).Top = 7920
    optWhatToScan(1).Top = 7920
    grdDespatchedItems.Top = 2520
    grdDespatchedItems.Height = 5295
    chkExpand.Top = 7920
    txtBarcode.Top = 7920
    txtBarcode.Left = 5520
    picContainer.Enabled = False
End If

End Sub

Private Sub cmbBillToCompany_Click()
lblBillToCompanyID.Caption = cmbBillToCompany.Columns("companyID").Text
End Sub

Private Sub cmbBillToCompany_DropDown()

'populate the company drop down (data bound)
Dim l_strSQL As String

l_strSQL = "SELECT companyID, name FROM company WHERE system_active = 1 ORDER BY name"

adoBillToCompany.ConnectionString = g_strConnection
adoBillToCompany.RecordSource = l_strSQL
adoBillToCompany.Refresh

End Sub

Private Sub cmbCompany_Click()
lblCompanyID.Caption = cmbCompany.Columns("theID").Text
txtAddress.Text = cmbCompany.Columns("address").Text
txtPostCode.Text = cmbCompany.Columns("postcode").Text
txtCountry.Text = cmbCompany.Columns("country").Text
txtTelephone.Text = cmbCompany.Columns("telephone").Text
If chkUseCompany.Value = False Then
    cmbContact.Text = cmbCompany.Columns("attentionof").Text
End If

Dim l_strContactName As String
l_strContactName = cmbContact.Text
txtEmail.Text = GetData("contact", "email", "name", l_strContactName)
    
End Sub

Private Sub cmbCompany_DropDown()
'populate the company drop down (data bound)
Dim l_strSQL As String

If chkUseCompany.Value = False Then
    l_strSQL = "SELECT deliveryaddressID as theID, deliverycompanyname as name, address, postcode, country, attentionof, telephone FROM deliveryaddress ORDER BY deliverycompanyname"
Else
    l_strSQL = "SELECT companyID as theID, name, address, postcode, country, '' as attentionof, telephone FROM company ORDER BY name"
End If

adoCompany.ConnectionString = g_strConnection
adoCompany.RecordSource = l_strSQL
adoCompany.Refresh
End Sub

Private Sub cmbCompany_GotFocus()
If cmbCompany.Text = "Please enter company..." Then
    cmbCompany.Text = ""
End If
End Sub

Private Sub cmbCompany_KeyPress(KeyAscii As Integer)

If chkUseCompany.Value = 0 Then Exit Sub

If KeyAscii = vbKeyReturn Then

    Dim l_strSQL As String
    l_strSQL = "SELECT name FROM company WHERE name LIKE '" & cmbCompany.Text & "%' ORDER BY name"
    
    Dim l_rstCompany As ADODB.Recordset
    Set l_rstCompany = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError

    If l_rstCompany.EOF Then
        'Code note found
        MsgBox "Cannot find this company", 64
    Else
        l_rstCompany.MoveFirst
        cmbCompany.Text = l_rstCompany("name")
    End If
    
    l_rstCompany.Close
    Set l_rstCompany = Nothing

End If
End Sub

Private Sub cmbCompany_LostFocus()
If cmbCompany.Text = "" Then
    cmbCompany.Text = "Please enter company..."
End If
End Sub

Private Sub cmbContact_Click()
lblContactID.Caption = cmbContact.Columns("contactID").Text

txtEmail.Text = GetData("contact", "email", "contactID", lblContactID.Caption)
  
End Sub

Private Sub cmbContact_DropDown()

If lblCompanyID.Caption = "" Then
    MsgBox "Please select a company before trying to select a contact", vbInformation
    cmbCompany.SetFocus
    Exit Sub
End If

Dim l_strSQL As String

l_strSQL = "SELECT company.companyID, contact.contactID, contact.name, contact.telephone, employee.jobtitle FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((company.name)='" & QuoteSanitise(cmbCompany.Text) & "')) ORDER BY contact.name;"

adoContact.ConnectionString = g_strConnection
adoContact.RecordSource = l_strSQL
adoContact.Refresh


End Sub

Private Sub cmbDeliveredBy_Click()

Dim l_strCostCode As String
l_strCostCode = GetDataSQL("SELECT TOP 1 videostandard FROM xref WHERE category = 'despatchmethod' AND description = '" & cmbDeliveredBy.Text & "';")

If l_strCostCode <> "" Then cmbCostCode.Text = l_strCostCode

End Sub

Private Sub cmbDirection_Click()
Select Case UCase(cmbDirection.Text)
Case "IN"
    lblCaption(2).Caption = "Received From"
    'cmbLocation.Text = "OPERATIONS"
Case "OUT"
    lblCaption(2).Caption = "Send To"
    cmbLocation.Text = ""
End Select
End Sub

Private Sub cmbJobID_DropDown()

If cmbJobID.Tag = "STOP" Then Exit Sub

Dim l_strSQL As String
l_strSQL = "SELECT jobID AS 'Job ID', jobtype AS 'Job Type', companyname AS 'Company', startdate AS 'Start Date', productname as 'Product', title1 AS 'Title', fd_status AS 'Status' FROM job "
l_strSQL = l_strSQL & " WHERE fd_status <> 'Cancelled' "
l_strSQL = l_strSQL & " AND fd_status <> 'Quickie' "
l_strSQL = l_strSQL & " AND fd_status <> 'Meeting' "
l_strSQL = l_strSQL & " AND fd_status <> '2nd Pencil' "
l_strSQL = l_strSQL & " AND fd_status <> 'Costed' "
l_strSQL = l_strSQL & " AND fd_status <> 'Cancel' "
l_strSQL = l_strSQL & " AND fd_status <> 'Sent To Accounts' "
l_strSQL = l_strSQL & " AND fd_status <> 'Unavail' "
'l_strSQL = l_strSQL & " AND fd_status <> 'Hold Cost' "
l_strSQL = l_strSQL & " AND fd_status <> 'Unavailable' "
l_strSQL = l_strSQL & " AND fd_status <> 'Quick' "
l_strSQL = l_strSQL & " AND fd_status <> 'aMeeting' "
If Val(txtProjectNumber.Text) <> 0 Then l_strSQL = l_strSQL & " AND projectnumber = '" & txtProjectNumber.Text & "' "
l_strSQL = l_strSQL & " ORDER BY startdate;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbJobID.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

cmbJobID.Columns(0).Width = 800
cmbJobID.Columns(1).Width = 800
cmbJobID.Columns(2).Width = 1800
cmbJobID.Columns(3).Width = 1000
cmbJobID.Columns(4).Width = 2000
cmbJobID.Columns(5).Width = 2000
cmbJobID.Columns(6).Width = 1000

End Sub

Private Sub cmbJobID_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        KeyAscii = 0
        ShowDespatchForJob Val(cmbJobID.Text)
    
    End If

End Sub


Private Sub cmbMethod_Click()

'If cmbCostCode.Text <> "" Then Exit Sub

Dim l_strCostCode As String
l_strCostCode = GetDataSQL("SELECT TOP 1 videostandard FROM xref WHERE category = 'despatchmethod' AND description = '" & cmbMethod.Text & "';")

If l_strCostCode <> "" Then cmbCostCode.Text = l_strCostCode

End Sub

Private Sub cmdAddItem_Click()

If grdDetail.Rows = 0 Then Exit Sub

If Val(txtDespatchID.Text) = 0 Then
    NoDespatchSelectedMessage
    Exit Sub
End If

DespatchJobDetailItem Val(txtDespatchID.Text), grdDetail.Columns("jobdetailID").Text
frmJobDespatch.lblItemCount.Caption = GetItemCountForDespatch(Val(txtDespatchID.Text))


adoDespatchDetail.Refresh
'ShowDespatchDetail Val(txtDespatchID.Text)

End Sub

Private Sub cmdClear_Click()

Dim l_lngJobID As Long
l_lngJobID = Val(cmbJobID.Text)
ClearFields Me
ShowDespatchDetail -1
txtNoOfCopies.Text = g_intNumberOfDespatchNoteCopies
If l_lngJobID <> 0 Then cmbJobID.Text = l_lngJobID

End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdComplete_Click()

'stop the despatch boys breaking the system without saving - ctd
If Val(txtDespatchID.Text) = 0 Then MsgBox "You need to save this despatch note before completing it.", vbExclamation: Exit Sub

CompleteDespatch Val(txtDespatchID.Text)

'refresh
ShowDespatchDetail txtDespatchID.Text

End Sub

Private Sub cmdDelete_Click()

If Not CheckAccess("/deletedespatch") Then Exit Sub

Dim l_intMsg As Integer
l_intMsg = MsgBox("Are you sure you want to delete this despatch?", vbYesNo + vbQuestion)
If l_intMsg = vbNo Then Exit Sub

Dim l_strSQL As String
l_strSQL = "DELETE FROM despatch WHERE despatchID = '" & txtDespatchID.Text & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_strSQL = "DELETE FROM despatchdetail WHERE despatchID = '" & txtDespatchID.Text & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError


If adoDelivery.ConnectionString <> "" Then adoDelivery.Refresh


End Sub


Private Sub cmdLaunch_Click(Index As Integer)
    
Select Case Index
Case 0
    OpenBrowser "mailto:" & txtEmail.Text
Case 2
    Dim l_strSearch  As String
    l_strSearch = "http://www.multimap.com/map/places.cgi?db=pc&place=" & Replace(txtPostCode.Text, " ", "")
    OpenBrowser l_strSearch
End Select

End Sub

Private Sub cmdNewJobDespatch_Click(Index As Integer)

Select Case Index
Case 0 'job

    If Val(cmbJobID.Text) = 0 Then
        NoJobSelectedMessage
        Exit Sub
    End If
    
    Dim l_intMsg As Integer
    l_intMsg = MsgBox("Create a despatch entry for this job?", vbOKCancel + vbQuestion)
    
    If l_intMsg = vbOK Then
    
        'check the project status
        Dim l_lngProjectNumber As Long
        Dim l_strProjectStatus  As String
        
        l_lngProjectNumber = Val(txtProjectNumber.Text)
        
        If l_lngProjectNumber > 0 Then
            l_strProjectStatus = GetData("project", "fd_status", "projectnumber", l_lngProjectNumber)
            
            If l_strProjectStatus <> "" And l_strProjectStatus <> "Current" And l_strProjectStatus <> "New" And l_strProjectStatus <> "Confirmed" Then
                MsgBox "The project status is set to '" & l_strProjectStatus & "' so you can not create a new delivery note for it!", vbExclamation
                Exit Sub
            End If
        End If
        
        Dim l_lngJobID As Long
        l_lngJobID = Val(cmbJobID.Text)
        
        If l_lngJobID > 0 Then
            l_strProjectStatus = GetData("job", "fd_status", "jobID", l_lngJobID)
            
            If l_strProjectStatus = "Sent To Accounts" Or l_strProjectStatus = "Cancelled" Or l_strProjectStatus = "Costed" Or l_strProjectStatus = "2nd Pencil" Or l_strProjectStatus = "Quickie" Or l_strProjectStatus = "Unavailable" Then
                MsgBox "The job status is set to '" & l_strProjectStatus & "' so you can not create a new delivery note for it!", vbExclamation
                Exit Sub
            End If
        End If
        
            
    
        Dim l_lngDespatchID As Long
        Dim l_strSQL As String
        l_strSQL = "SELECT * FROM job WHERE jobID = '" & cmbJobID.Text & "';"
        
        Dim l_rstJob As New ADODB.Recordset
        Set l_rstJob = ExecuteSQL(l_strSQL, g_strExecuteError)
        CheckForSQLError
        
        If Not l_rstJob.EOF Then
            Dim l_strDescription As String
            
            If GetFlag(g_optRequireProductBeforeSaving) = 1 Then
                l_strDescription = l_rstJob("productname")
            Else
                l_strDescription = l_rstJob("title1")
            End If
            
            If g_optCreateDeliveryNotesForJobsWithNoAddress = 1 Then
                l_lngDespatchID = SaveDespatch(0, Trim(" " & l_rstJob("jobtype")), Val(txtProjectNumber.Text), Val(cmbJobID.Text), "OUT", "", 0, "Please enter company...", "", 0, "", "", "", "", l_rstJob("startdate"), l_strDescription, l_rstJob("deadlinedate"), 0, "", "", "", 0, "", 0, 0, "", "", "", "")
            Else
                l_lngDespatchID = SaveDespatch(0, Trim(" " & l_rstJob("jobtype")), Val(txtProjectNumber.Text), Val(cmbJobID.Text), "OUT", "", l_rstJob("companyID"), l_rstJob("companyname"), l_rstJob("contactname"), l_rstJob("contactID"), GetData("company", "address", "companyID", l_rstJob("companyID")), GetData("company", "postcode", "companyID", l_rstJob("companyID")), l_rstJob("telephone"), GetData("company", "country", "companyID", l_rstJob("companyID")), l_rstJob("startdate"), l_strDescription, l_rstJob("deadlinedate"), 0, "", "", "", 0, GetData("contact", "email", "contactID", l_rstJob("contactID")), 0, 0, "", "", "", "")
            End If
            
            If l_lngDespatchID <> 0 Then ShowDespatchDetail l_lngDespatchID
            
            If adoDelivery.RecordSource = "" Then
                ShowDespatchForJob cmbJobID.Text
            End If
            
            adoDelivery.Refresh
        End If
        
        l_rstJob.Close
        Set l_rstJob = Nothing
    
    End If

Case 1 ' project
    
    If Val(txtProjectNumber.Text) = 0 Then
        NoJobSelectedMessage
        Exit Sub
    End If

    l_intMsg = MsgBox("Do you want to create a despatch entry for this project, using the project client details?" & vbCrLf & vbCrLf & "PLEASE NOTE: This despatch record will not be attached to a particular session", vbOKCancel + vbQuestion)
    
    If l_intMsg = vbOK Then
   
        l_strSQL = "SELECT * FROM project WHERE projectnumber = '" & txtProjectNumber.Text & "';"
        
        Set l_rstJob = ExecuteSQL(l_strSQL, g_strExecuteError)
        CheckForSQLError
        
        If Not l_rstJob.EOF Then
        
            If LCase(l_rstJob("allocation")) = "automatic" Then
                MsgBox "You can not create a despatch record for an automatically created project number. Please create one for the job number instead.", vbExclamation
                GoTo PROC_Close
            End If
            If GetFlag(g_optRequireProductBeforeSaving) = 1 Then
                l_strDescription = GetData("product", "name", "productID", l_rstJob("productID"))
            Else
                l_strDescription = l_rstJob("title")
            End If
            l_lngDespatchID = SaveDespatch(0, l_rstJob("jobtype"), Val(txtProjectNumber.Text), Val(cmbJobID.Text), "OUT", "", l_rstJob("companyID"), l_rstJob("companyname"), l_rstJob("contactname"), l_rstJob("contactID"), GetData("company", "address", "companyID", l_rstJob("companyID")), GetData("company", "postcode", "companyID", l_rstJob("companyID")), l_rstJob("telephone"), GetData("company", "country", "companyID", l_rstJob("companyID")), l_rstJob("startdate"), l_strDescription, l_rstJob("enddate"), 0, "", "", "", 0, GetData("contact", "email", "contactID", l_rstJob("contactID")), 0, 0, "", "", "", "")
            
            If l_lngDespatchID <> 0 Then ShowDespatchDetail l_lngDespatchID
            
        End If
        
PROC_Close:

        l_rstJob.Close
        Set l_rstJob = Nothing
    
    End If

End Select

'adoDelivery.Refresh

End Sub

Private Sub cmdPrint_Click()

Dim l_lngCopies As Long
Dim l_strSQL As String
Dim l_rstCount As New ADODB.Recordset
Dim l_strReportToPrint As String
Dim l_strJobStatus As String
Dim l_strJobType As String
Dim l_intCount As Integer
Dim l_lngJobID  As Long
Dim l_intMsg As Integer

'save changes first


If Val(txtDespatchID.Text) <> 0 Then
    
    If Not CheckAccess("/printdespatchnote") Then Exit Sub

    'checks to see that the job has been completed
    If IsNumeric(cmbJobID.Text) Then
    
        l_lngJobID = Val(cmbJobID.Text)
        
        'get the job type
        l_strJobType = GetData("job", "jobtype", "jobID", l_lngJobID)
        
        'check the job status
        l_strJobStatus = GetData("job", "fd_status", "jobID", l_lngJobID)
        
        'is this a dub job?
        'do jobs have to be completed before sending out despatch?
        If Left(LCase(l_strJobType), 3) = "dub" And g_optCheckJobStatusBeforePrintingDespatch = 1 And GetStatusNumber(l_strJobStatus) < 6 Then
            MsgBox "You can not print this delivery note yet, as the job it is part of has not been completed.", vbExclamation
            Exit Sub
        End If
    
        'if this is a hire job...
        If LCase(l_strJobType) = "hire" And LCase(l_strJobStatus) = "confirmed" Then
                
            l_strSQL = "SELECT COUNT(resourceID) FROM resourceschedule WHERE fd_status <> 'Prepared' AND jobID = '" & l_lngJobID & "';"
            l_intCount = GetCount(l_strSQL)
            
            If l_intCount > 0 Then
                l_intMsg = MsgBox("There are still " & l_intCount & " items which are not prepared." & vbCrLf & vbCrLf & "You can print, but it is not advised without preparing every item on the job." & vbCrLf & vbCrLf & "Do you want to print even though some items are not prepared?", vbExclamation + vbYesNo)
                If l_intMsg = vbNo Then
                    Exit Sub
                End If
            End If
                
            'Update the job status to be prepared
            UpdateJobStatus l_lngJobID, "Prepared", , True
            
        End If
        
    End If
    
    'how many copies should print?
    l_lngCopies = Val(txtNoOfCopies.Text)
    If l_lngCopies <= 0 Then l_lngCopies = 1
    
    'check the number of items on the dnote
    'l_strSQL = "SELECT count(despatchdetailID) from despatchdetail WHERE despatchID = '" & frmJobDespatch.txtDespatchID.Text & "';"
    l_intCount = grdDespatchedItems.Rows ' GetCount(l_strSQL)
    
    If l_intCount > 0 Then
        l_strReportToPrint = GetData("xref", "format", "description", CurrentMachineName)
        If l_strReportToPrint = "" Then
            l_strReportToPrint = g_strLocationOfCrystalReportFiles & "despatch.rpt"
        Else
            l_strReportToPrint = g_strLocationOfCrystalReportFiles & l_strReportToPrint
        End If
            
        PrintCrystalReport l_strReportToPrint, "{despatch.despatchID} = " & frmJobDespatch.txtDespatchID.Text, g_blnPreviewReport, l_lngCopies
    Else
        MsgBox "You need to specify what you are despatching before you can print. Add some detail lines and try again.", vbInformation
        Exit Sub
    End If
Else
    NoDespatchSelectedMessage
End If

End Sub

Private Sub cmdRemoveItem_Click()

Dim l_intMessage As Integer
l_intMessage = MsgBox("Do you really want to REMOVE the selected row?", vbYesNo + vbQuestion)
If l_intMessage = vbNo Then Exit Sub

Dim l_lngDespatchDetailID As Long
l_lngDespatchDetailID = grdDespatchedItems.Columns("despatchdetailID").Text

Dim l_strSQL As String
l_strSQL = "DELETE FROM despatchdetail WHERE despatchdetailID = " & l_lngDespatchDetailID

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

adoDespatchDetail.Refresh

End Sub

Private Sub cmdPrintLabel_Click()

If Val(txtDespatchID.Text) > 0 Then
    Dim l_strReportToPrint As String
    Dim l_intMsg As Integer
    Dim l_intLoop  As Integer
    If g_optJCADeliverylabels = 1 Then
        
        Dim l_lngCopies As Long
        l_lngCopies = Val(frmJobDespatch.txtNoOfCopies.Text)
        If l_lngCopies <= 0 Then l_lngCopies = 1
        PrintCrystalReport g_strLocationOfCrystalReportFiles & "deliverylabel.rpt", "{despatch.despatchnumber} = " & txtDespatchNumber.Text, False, l_lngCopies
        
    Else
    
        l_intMsg = Val(InputBox("How many do you want to print?", "Print Labels"))
        
        If l_intMsg = 0 Then Exit Sub
        
        For l_intLoop = 1 To l_intMsg
            PrintDespatchLabel Val(txtDespatchID.Text), l_intLoop, l_intMsg
        Next
    
    End If
    
End If

End Sub

Private Sub cmdPrintWWDespatch_Click()
Dim l_lngCopies As Long

If Val(txtDespatchID.Text) > 0 Then
    
    Dim l_strReportToPrint As String
    
    l_lngCopies = Val(txtNoOfCopies.Text)
    If l_lngCopies <= 0 Then l_lngCopies = 1
    
    If MsgBox("Print to screen?", vbYesNo, "WW Despatch Print") = vbYes Then
        PrintCrystalReport g_strLocationOfCrystalReportFiles & "wwdesp.rpt", "{despatch.despatchID} = " & frmJobDespatch.txtDespatchID.Text, True, l_lngCopies
    Else
        PrintCrystalReport g_strLocationOfCrystalReportFiles & "wwdesp.rpt", "{despatch.despatchID} = " & frmJobDespatch.txtDespatchID.Text, g_blnPreviewReport, l_lngCopies
    End If
    
Else
    NoDespatchSelectedMessage
End If
End Sub

Private Sub cmdPrintPDF_Click()

If Val(txtDespatchID.Text) > 0 Then
    
    Dim l_strSelectionFormula As String
    Dim l_strReportFile As String
    Dim l_strReportFileName As String
    Dim l_intMsg As Integer
    
    l_strSelectionFormula = "{despatch.despatchID} = " & txtDespatchID.Text
    
    If g_intUseMakePDFasExportButton = 1 Then
        l_intMsg = NewMsgBox("Export", "Which file would you like to export?", "PDF", "Excel", "Cancel")
        
        Select Case l_intMsg
        Case 0
            l_strReportFile = g_strLocationOfCrystalReportFiles & "despatch.rpt"
            l_strReportFileName = g_strPDFLocation & "\despatch\" & txtDespatchNumber.Text & ".pdf"
            ReportToPDF l_strReportFile, l_strSelectionFormula, l_strReportFileName
        Case 1
            l_strReportFile = g_strLocationOfCrystalReportFiles & "DespatchforXLS.rpt"
            l_strReportFileName = g_strPDFLocation & "\despatch\" & txtDespatchNumber.Text & ".xls"
            ReportToXLS l_strReportFile, l_strSelectionFormula, l_strReportFileName
        Case 2
            Exit Sub
        End Select
    
    Else
        
        l_intMsg = NewMsgBox("Create PDF", "Which report do you want to create a PDF file of?", "Despatch", "WW Despatch", "Cancel")
    
        Select Case l_intMsg
        Case 0
            l_strReportFile = g_strLocationOfCrystalReportFiles & "despatch.rpt"
        Case 1
            l_strReportFile = g_strLocationOfCrystalReportFiles & "wwdesp.rpt"
        Case 2
            Exit Sub
        End Select
                
        ReportToPDF l_strReportFile, l_strSelectionFormula, ""
    
    End If
Else
    NoDespatchSelectedMessage
End If

End Sub

Private Sub cmdSave_Click()

If cmbCompany.Text = "Please enter company..." Then
    MsgBox "Please select a valid delivery company before saving", vbExclamation
    Exit Sub
End If

'Check for direction, and if IN then check a location has been set.

If Val(cmbJobID.Text) <> 0 Then

    Dim l_strChanged As String
    If cmbCompany.Tag <> cmbCompany.Text Then l_strChanged = l_strChanged & vbCrLf & " * Company from '" & cmbCompany.Tag & "' to '" & cmbCompany.Text & "'."
    If cmbContact.Tag <> cmbContact.Text Then l_strChanged = l_strChanged & vbCrLf & " * Contact from '" & cmbContact.Tag & "' to '" & cmbContact.Text & "'."
    If txtAddress.Tag <> txtAddress.Text Then l_strChanged = l_strChanged & vbCrLf & " * Address from '" & txtAddress.Tag & "' to '" & txtAddress.Text & "'."
    If txtPostCode.Tag <> txtPostCode.Text Then l_strChanged = l_strChanged & vbCrLf & " * Postcode from '" & txtPostCode.Tag & "' to '" & txtPostCode.Text & "'."
    If txtTelephone.Tag <> txtTelephone.Text Then l_strChanged = l_strChanged & vbCrLf & " * Telephone from '" & txtTelephone.Tag & "' to '" & txtTelephone.Text & "'."
    If txtCountry.Tag <> txtCountry.Text Then l_strChanged = l_strChanged & vbCrLf & " * Country from '" & txtCountry.Tag & "' to '" & txtCountry.Text & "'."
    
    If l_strChanged <> "" Then
        AddJobHistory Val(cmbJobID.Text), "D-Note address changed " & txtDespatchID.Text & ": " & l_strChanged
    End If

End If

If g_optDescDeadNotCompulsory = 0 Then
    If txtDescription.Text = "" Then
        Dim l_intQuestion As Integer
        If Val(cmbJobID.Text) <> 0 Then
            l_intQuestion = MsgBox("Each despatch record must have a description before you can save. Do you want to use the job information?", vbExclamation + vbOKCancel)
            If l_intQuestion = vbOK Then
                If g_optRequireProductBeforeSaving = 1 Then
                    txtDescription.Text = GetData("job", "productname", "jobID", cmbJobID.Text)
                Else
                    txtDescription.Text = GetData("job", "title1", "jobID", cmbJobID.Text)
                End If
                GoTo PROC_Continue
            End If
        Else
            MsgBox "Please enter a description before saving.", vbExclamation
                txtDescription.SetFocus
        End If
        Exit Sub
    End If
End If

PROC_Continue:

If cmbBillToCompany.Text = "" Then
    MsgBox "Outgoing Despatches must have a 'Bill TO' Company before saving.", vbExclamation
    cmbBillToCompany.SetFocus
    Exit Sub
Else
    Dim l_intResult As Integer
    If GetData("company", "accountstatus", "name", cmbBillToCompany.Text) = "HOLD" _
    Or GetData("company", "accountstatus", "name", cmbCompany.Text) = "HOLD" Then
        l_intResult = MsgBox("Bill to Company or Destination company are on Hold - Do you wish to continue despatching?", vbYesNo)
        If l_intResult = vbNo Then Exit Sub
    End If
End If

'Check whether Bike Refs are compulsory, and insist there be one if they are and its a bike.
If g_intCompulsoryBikeRefs <> 0 Then
    If cmbDeliveredBy.Text = "BIKE" Then
        If txtCourierReference.Text = "" Then
            MsgBox "Bikes must have A Courier Reference before saving.", vbExclamation
            txtCourierReference.SetFocus
            Exit Sub
        End If
    End If
End If

If g_optDescDeadNotCompulsory = 0 Then
    If IsNull(datDeadlineDate.Value) Then
        MsgBox "You need to select a deadline / E.T.A. before saving", vbExclamation
        Exit Sub
    End If
End If

If g_optDescDeadNotCompulsory = 0 Then
    If cmbDeadlineTime.Text = "" Then
        MsgBox "You need to select a deadline / E.T.A. before saving", vbExclamation
        Exit Sub
    End If
End If

If cmbDirection.Text <> "OUT" Then
    cmbDirection.Text = "OUT"
End If

If lblSilentSave.Caption = "" Then
    If Val(cmbJobID.Text) = 0 Then
        Dim l_intMessage As Integer
        l_intMessage = MsgBox("You have not selected a job ID. Do you really want to save?", vbYesNo + vbQuestion)
        If l_intMessage = vbNo Then
            cmbJobID.SetFocus
            Exit Sub
        End If
    End If
End If

Dim l_strDeadlineDate As Date
If IsNull(datDeadlineDate.Value) Then
    l_strDeadlineDate = Now
Else
    l_strDeadlineDate = datDeadlineDate.Value & " " & cmbDeadlineTime.Text
End If

If lblSilentSave.Caption = "" Then
    If Val(lblCompanyID.Caption) = 0 Or chkUseCompany.Value <> False Then
        Dim l_intResponse As Integer
        l_intResponse = MsgBox("Do you wish to store this delivery address?", vbYesNo + vbQuestion, "Save Delivery Address")
        If l_intResponse = vbYes Then
            SaveDeliveryAddress cmbCompany.Text, cmbContact.Text, txtAddress.Text, txtPostCode.Text, txtTelephone.Text, txtCountry.Text, "NEW"
        End If
    Else
        SaveDeliveryAddress cmbCompany.Text, cmbContact.Text, txtAddress.Text, txtPostCode.Text, txtTelephone.Text, txtCountry.Text, lblCompanyID.Caption
    End If
Else
    SaveDeliveryAddress cmbCompany.Text, cmbContact.Text, txtAddress.Text, txtPostCode.Text, txtTelephone.Text, txtCountry.Text, "NEW"
End If

If (txtCharge.Text <> "") Then
    If Not IsNumeric(txtCharge.Text) Then
        MsgBox "Please specify a valid charge amount.", vbExclamation
        txtCharge.SetFocus
        Exit Sub
    End If
    
End If

If (txtPurchaseAuthorisationNumber.Text <> "") And txtPurchaseAuthorisationNumber.Text <> "0" Then

    If Not IsNumeric(txtPurchaseAuthorisationNumber.Text) Then
PROC_InvalidPurchase:
        MsgBox "Please specify a valid purchase authorisation number.", vbExclamation
        txtPurchaseAuthorisationNumber.SetFocus
        Exit Sub
    Else
        If txtPurchaseAuthorisationNumber.Text <> "" Then
            Dim l_lngPurchaseHeaderID  As Long
            l_lngPurchaseHeaderID = GetData("purchaseheader", "purchaseheaderID", "authorisationnumber", txtPurchaseAuthorisationNumber.Text)
            If l_lngPurchaseHeaderID = 0 Then
                GoTo PROC_InvalidPurchase
            End If
        End If
    End If
End If

SaveDespatch Val(txtDespatchID.Text), "", Val(txtProjectNumber.Text), Val(cmbJobID.Text), cmbDirection.Text, cmbMethod.Text, Val(lblCompanyID.Caption), cmbCompany.Text, cmbContact.Text, Val(lblContactID.Caption), txtAddress.Text, txtPostCode.Text, txtTelephone.Text, txtCountry.Text, Null, txtDescription.Text, l_strDeadlineDate, Val(chkUrgent.Value), txtNotes.Text, cmbLocation.Text, cmbBillToCompany.Text, Val(lblBillToCompanyID.Caption), txtEmail.Text, l_lngPurchaseHeaderID, Val(txtCharge.Text), cmbDeliveredBy.Text, txtCourierReference.Text, cmbDeliveryFor.Text, cmbCostCode.Text

If Val(txtDespatchID.Text) = 0 Then
    txtDespatchID.Text = g_lngLastID
'    ShowDespatchForJob Val(cmbJobID.Text)
End If

ShowDespatchDetail Val(txtDespatchID.Text)

On Error Resume Next
adoDelivery.Refresh

End Sub

Private Sub cmdBarcodeKeypressReturn_Click()

txtBarcode_KeyPress 13

End Sub

Private Sub cmdShowJob_Click()

If Val(cmbJobID.Text) > 0 Then
    ShowJob Val(cmbJobID.Text), g_intDefaultTab, True
'    ShowJob Val(cmbJobID.Text), g_intDefaultTab, True
Else
    NoJobSelectedMessage
End If

End Sub

Private Sub cmdShowPurchaseOrder_Click()

If Val(txtPurchaseAuthorisationNumber.Text) > 0 Then
    ShowPurchaseOrder GetData("purchaseheader", "purchaseheaderID", "authorisationnumber", txtPurchaseAuthorisationNumber.Text)
Else
    NoPurchaseOrderSelectedMessage
End If

End Sub

Private Sub ddnCopyType_ValidateList(Text As String, RtnPassed As Integer)

    If Text = "" Then Exit Sub
    Dim l_strSQL As String
    l_strSQL = "SELECT description FROM xref WHERE category = 'copytype' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"
    
    Dim l_rstXRefFormat As ADODB.Recordset
    Set l_rstXRefFormat = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError

    If l_rstXRefFormat.EOF Then
        'Code note found
        MsgBox "Cannot find this description", 64
        RtnPassed = False
    Else
        l_rstXRefFormat.MoveFirst
        RtnPassed = True
        Text = l_rstXRefFormat("description")
        grdDespatchedItems.Columns(grdDespatchedItems.Col).Text = Text
    End If
    
    l_rstXRefFormat.Close
    Set l_rstXRefFormat = Nothing
End Sub

Private Sub ddnFormat_ValidateList(Text As String, RtnPassed As Integer)
    If Text = "" Then Exit Sub
    Dim l_strSQL As String
    l_strSQL = "SELECT description FROM xref WHERE category = 'format' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"
    
    Dim l_rstXRefFormat As ADODB.Recordset
    Set l_rstXRefFormat = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError

    If l_rstXRefFormat.EOF Then
        'Code note found
        MsgBox "Cannot find this description", 64
        RtnPassed = False
    Else
        l_rstXRefFormat.MoveFirst
        RtnPassed = True
        Text = l_rstXRefFormat("description")
        grdDespatchedItems.Columns(grdDespatchedItems.Col).Text = Text
    End If
    
    l_rstXRefFormat.Close
    Set l_rstXRefFormat = Nothing
End Sub

Private Sub ddnStandard_ValidateList(Text As String, RtnPassed As Integer)
    If Text = "" Then Exit Sub
    Dim l_strSQL As String
    l_strSQL = "SELECT description FROM xref WHERE category = 'videostandard' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"
    
    Dim l_rstXRefFormat As ADODB.Recordset
    Set l_rstXRefFormat = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError

    If l_rstXRefFormat.EOF Then
        'Code note found
        MsgBox "Cannot find this description", 64
        RtnPassed = False
    Else
        l_rstXRefFormat.MoveFirst
        RtnPassed = True
        Text = l_rstXRefFormat("description")
        grdDespatchedItems.Columns(grdDespatchedItems.Col).Text = Text
    End If
    
    l_rstXRefFormat.Close
    Set l_rstXRefFormat = Nothing
End Sub

Private Sub Form_Activate()
txtNoOfCopies.Text = g_intNumberOfDespatchNoteCopies
End Sub

Private Sub Form_Load()

If Not CheckAccess("/despatchresource", True) Then optWhatToScan(1).Enabled = False

If g_intUseMakePDFasExportButton = 1 Then cmdPrintPDF.Caption = "Export"
datDeadlineDate.Value = Date
datDeadlineDate.Value = Null

'MakeLookLikeOffice Me

PopulateCombo "format", ddnFormat
PopulateCombo "videostandard", ddnStandard
PopulateCombo "copytype", ddnCopyType

PopulateCombo "despatchmethod", cmbMethod
PopulateCombo "deliveredby", cmbDeliveredBy
PopulateCombo "times", cmbDeadlineTime
PopulateCombo "location", cmbLocation
PopulateCombo "deliverycostcodes", cmbCostCode

PopulateCombo "users", cmbDeliveryFor

If g_optShowBillToInformationOnDespatch = 1 Then
    cmbBillToCompany.Visible = True
    lblCaption(13).Visible = True
End If

grdDelivery.StyleSets.Add "Hire"
grdDelivery.StyleSets("Hire").BackColor = &HFFC0C0

grdDelivery.StyleSets.Add "Purchase"
grdDelivery.StyleSets("Purchase").BackColor = &H80FF&

grdDelivery.StyleSets.Add "Edit"
grdDelivery.StyleSets("Edit").BackColor = &HFFC0FF

grdDelivery.StyleSets.Add "VT"
grdDelivery.StyleSets("VT").BackColor = &HC0FFC0

grdDelivery.StyleSets.Add "Dubbing"
grdDelivery.StyleSets("Dubbing").BackColor = &HC0FFC0


If g_intDisableProjects = 1 Then
    txtProjectNumber.Visible = False
    lblCaption(26).Visible = False
    DoEvents
    grdDespatchedItems.Columns("projectnumber").Visible = False
    DoEvents
End If

If g_intDisableResourceList = 1 Then
    optWhatToScan(0).Visible = False
    optWhatToScan(1).Visible = False
End If

CenterForm Me

End Sub

Private Sub Form_Resize()
On Error Resume Next
grdDespatchedItems.Width = Me.ScaleWidth - 120 - 120 - 120 - grdDetail.Width
End Sub

Private Sub grdDelivery_Click()

txtDespatchID.Text = GetData("despatch", "despatchID", "despatchnumber", grdDelivery.Columns("despatchnumber").Text)

ShowDespatchDetail Val(txtDespatchID.Text)


End Sub

Private Sub grdDelivery_RowLoaded(ByVal Bookmark As Variant)

grdDelivery.Columns("description").CellStyleSet grdDelivery.Columns("typeofjob").Text
grdDelivery.Columns("direction").CellStyleSet grdDelivery.Columns("typeofjob").Text



End Sub


Private Sub grdDespatchedItems_AfterDelete(RtnDispErrMsg As Integer)
'ShowDespatchDetail Val(txtDespatchID.Text)
frmJobDespatch.lblItemCount.Caption = GetItemCountForDespatch(Val(txtDespatchID.Text))
End Sub

Private Sub grdDespatchedItems_AfterInsert(RtnDispErrMsg As Integer)
frmJobDespatch.lblItemCount.Caption = GetItemCountForDespatch(Val(txtDespatchID.Text))

End Sub

Private Sub grdDespatchedItems_AfterUpdate(RtnDispErrMsg As Integer)
frmJobDespatch.lblItemCount.Caption = GetItemCountForDespatch(Val(txtDespatchID.Text))
End Sub

Private Sub grdDespatchedItems_BeforeUpdate(Cancel As Integer)
'ensure that the current dnote number is saved with the item
grdDespatchedItems.Columns("despatchID").Text = txtDespatchID.Text

End Sub

Private Sub grdDespatchedItems_InitColumnProps()
grdDespatchedItems.Columns("format").DropDownHwnd = ddnFormat.hWnd
grdDespatchedItems.Columns("standard").DropDownHwnd = ddnStandard.hWnd
grdDespatchedItems.Columns("copytype").DropDownHwnd = ddnCopyType.hWnd
End Sub

Private Sub grdResourceSchedule_DblClick()

Dim l_lngResourceScheduleID As Long
Dim l_strInput As String
Dim l_intMsg As Integer

    
    If UCase(cmbDirection.Text) = "OUT" Then
        If grdResourceSchedule.Columns(6).Text <> "" Then
            l_lngResourceScheduleID = Val(grdResourceSchedule.Columns(7).Text)
            
            l_strInput = InputBox("Please enter the hire item's serial number", "Enter Serial Number", grdResourceSchedule.Columns(1).Text)
            
            If l_strInput <> grdResourceSchedule.Columns(3).Text Then
                SetData "resourceschedule", "serialnumber", "resourcescheduleID", l_lngResourceScheduleID, l_strInput
                adoResourceSchedule.Refresh
            End If
        End If
    Else
    
        l_lngResourceScheduleID = GetDataSQL("SELECT TOP 1 itemID FROM despatchdetail WHERE despatchdetailID = '" & grdResourceSchedule.Columns("despatchdetailID").Text & "';")
    
    End If
    
    l_intMsg = MsgBox("Do you want to add this item (" & grdResourceSchedule.Columns("Resource").Text & ") to your delivery note?", vbQuestion + vbYesNo)
    If l_intMsg = vbNo Then Exit Sub
    
    DespatchHireResource l_lngResourceScheduleID, Val(txtDespatchID.Text)
    
    adoDespatchDetail.Refresh
    


End Sub

Private Sub grdResourceSchedule_RowLoaded(ByVal Bookmark As Variant)

On Error Resume Next
'
'If grdResourceSchedule.Caption = "Items Booked On Job" Then
    'grdResourceSchedule.Refresh
    'DoEvents
    'If grdResourceSchedule.Columns(6).Text <> "" Then
    '    grdResourceSchedule.Columns(0).Text = grdResourceSchedule.Columns(6).Text
    'ElseIf grdResourceSchedule.Columns(5).Text <> "0" Then
    '    grdResourceSchedule.Columns(0).Text = GetData("contact", "name", "contactID", grdResourceSchedule.Columns(5).Text)
    'Else
    '    grdResourceSchedule.Columns(0).Text = GetData("resource", "name", "resourceID", grdResourceSchedule.Columns(4).Text)
 '   End If
    
    'If grdResourceSchedule.Columns(4).Text <> "0" Then
        'get resource barcode
    '    grdResourceSchedule.Columns(3).Text = GetData("resource", "barcode", "resourceID", grdResourceSchedule.Columns(4).Text)
    'End If
'End If

End Sub

Private Sub optWhatToScan_Click(Index As Integer)

Select Case Index

Case 0
    grdResourceSchedule.Visible = False
    grdDetail.Visible = True
    cmdAddItem.Visible = True

Case 1
    grdResourceSchedule.Visible = True
    grdDetail.Visible = False
    cmdAddItem.Visible = False

End Select

End Sub

Private Sub Picture2_Click()

End Sub

Private Sub txtBarcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then
    KeyAscii = 0
    
    Dim l_strSQL As String, l_rst As ADODB.Recordset, l_strStatus As String, l_lngID As Long, l_datDate As Date
    
    If cmbCompany.Text = "Please enter company..." Then
        MsgBox "Please select a valid delivery company before adding items to the delivery note", vbExclamation
        Exit Sub
    End If
    
    If Val(txtDespatchID.Text) = 0 Then
        NoDespatchSelectedMessage
        Exit Sub
    End If
    
    'Check whether Rigorous tape Checking is in force, and if it is check the tape.
    If InStr(UCase(GetData("company", "cetaclientcode", "companyID", lblBillToCompanyID.Caption)), "/CHECKTAPES") <> 0 Then
        If GetData("library", "checkedlabels", "barcode", txtBarcode.Text) = 0 Then
            MsgBox "Cannot Despatch - Tape Labels Not Checked", vbExclamation, "Tape Checking"
            Exit Sub
        ElseIf GetData("library", "checkedrecreport", "barcode", txtBarcode.Text) = 0 Then
            MsgBox "Cannot Despatch - Record Report Not Checked", vbExclamation, "Tape Checking"
            Exit Sub
        ElseIf GetData("library", "checkedtapespec", "barcode", txtBarcode.Text) = 0 Then
            MsgBox "Cannot Despatch - Tape Specification Not Checked", vbExclamation, "Tape Checking"
            Exit Sub
        End If
    End If
    
    'Check if the item is a BBC Master that is currently neded for other jobs or 7 day rule.
    'First BBCMG
    l_strStatus = Trim(" " & GetDataSQL("SELECT TOP 1 Order_Status FROM vw_BBCMG_TapeArrived WHERE Tape_Number = '" & txtBarcode.Text & "'"))
    If l_strStatus <> "" And l_strStatus <> "Preview Order Complete" And l_strStatus <> "Preview Portal Ready" And l_strStatus <> "Order Cancelled" Then
        If MsgBox("Cannot Despatch - This tape is needed for a BBCMG Preview Order which is in state '" & l_strStatus & "'" & vbCrLf & "Send Out Anyway?", vbYesNo, "Error Despatching") = vbNo Then
            Exit Sub
        End If
    ElseIf l_strStatus = "Preview Order Complete" Or l_strStatus = "Preview Portal Ready" Then
        'Check the 7 day rule from the point the tape was ingested.
        l_lngID = GetData("vw_BBCMG_TapeArrived", "Preview_Order_ID", "Tape_Number", txtBarcode.Text)
        If l_lngID <> 0 Then
            l_datDate = GetData("BBCMG_Preview_Tracker", "DatePreviewReady", "Preview_Order_ID", l_lngID)
            If l_datDate <> 0 Then
                l_datDate = DateAdd("d", 7, l_datDate)
                If l_datDate > Now Then
                    If MsgBox("Cannot Despatch - This tape was used for a BBCMG Preview Order and 7 days have not elapsed yet '" & l_strStatus & "'" & vbCrLf & "Send Out Anyway?", vbYesNo, "Error Despatching") = vbNo Then
                        Exit Sub
                    End If
                End If
            End If
        End If
    End If
    'Then check BBCWW
    l_lngID = Val(Trim(" " & GetDataSQL("SELECT TOP 1 jobID FROM vw_Required_Tapes_BBCWW_Here WHERE MasterBarcode = '" & txtBarcode.Text & "' AND completeddate IS NULL")))
    If l_lngID <> 0 Then
        If MsgBox("Cannot Despatch - This tape is needed for a BBCWW Job " & l_lngID & vbCrLf & "Send Out Anyway?", vbYesNo, "Error Despatching") = vbNo Then
            Exit Sub
        End If
    Else
        l_datDate = GetDataSQL("SELECT TOP 1 completeddate FROM vw_Required_Tapes_BBCWW_Here WHERE MasterBarcode = '" & txtBarcode.Text & "' AND completeddate IS NOT NULL")
        If l_datDate <> 0 Then
            l_datDate = DateAdd("d", 7, l_datDate)
            If l_datDate > Now Then
                If MsgBox("Cannot Despatch - This tape was used for a BBCWW job and 7 days have not elapsed yet '" & l_strStatus & "'" & vbCrLf & "Send Out Anyway?", vbYesNo, "Error Despatching") = vbNo Then
                    Exit Sub
                End If
            End If
        End If
    End If
    'Then check DADC
    l_lngID = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM vw_Required_Tapes_DADCBBC_Here WHERE barcode='" & txtBarcode.Text & "' AND stagefield15 IS NULL")
    If l_lngID <> 0 Then
        If MsgBox("Cannot Despatch - This tape is needed for a DADC Job " & GetData("tracker_dadc_item", "itemreference", "tracker_dadc_itemID", l_lngID) & vbCrLf & "Send Out Anyway?", vbYesNo, "Error Despatching") = vbNo Then
            Exit Sub
        End If
    Else
        l_datDate = GetDataSQL("SELECT TOP 1 stagefield15 FROM vw_Required_Tapes_DADCBBC_Here WHERE barcode='" & txtBarcode.Text & "' AND stagefield15 IS NOT NULL")
        If l_datDate <> 0 Then
            l_datDate = DateAdd("d", 7, l_datDate)
            If l_datDate > Now Then
                If MsgBox("Cannot Despatch - This tape was used for a DADC job and 7 days have not elapsed yet '" & l_strStatus & "'" & vbCrLf & "Send Out Anyway?", vbYesNo, "Error Despatching") = vbNo Then
                    Exit Sub
                End If
            End If
        End If
    End If
    
    'Add the item to the despatch note
    If Not IsBarcodeOnDespatch(Val(txtDespatchID.Text), txtBarcode.Text) Then
        'is this library or resource?
        If optWhatToScan(0).Value = True Then
            DespatchLibraryItem Val(txtDespatchID.Text), txtBarcode.Text
        Else
            DespatchResource Val(txtDespatchID.Text), GetData("resource", "resourceID", "barcode", txtBarcode.Text)
            adoResourceSchedule.Refresh
        End If
        
        adoDespatchDetail.Refresh
        frmJobDespatch.lblItemCount.Caption = GetItemCountForDespatch(Val(txtDespatchID.Text))
        
    Else
        MsgBox "This item already exists on this despatch note", vbInformation
    End If
    
    'Check if item is a BBCWW product and if so, mark the BBCWW tracker line as despatched.
    If GetData("library", "jobID", "barcode", txtBarcode.Text) <> "" Then
        If GetData("job", "companyID", "jobID", GetData("library", "jobID", "barcode", txtBarcode.Text)) = 570 Then
            If GetData("library", "jobdetailID", "barcode", txtBarcode.Text) <> "" Then
                If GetData("jobdetail", "jobdetailID", "jobdetailID", GetData("library", "jobdetailID", "barcode", txtBarcode.Text)) <> 0 Then
                    SetData "jobdetail", "DateDelivered", "jobdetailID", GetData("library", "jobdetailID", "barcode", txtBarcode.Text), FormatSQLDate(Now)
                    SetData "jobdetail", "completeddate", "jobdetailID", GetData("library", "jobdetailID", "barcode", txtBarcode.Text), FormatSQLDate(Now)
                    If Trim(" " & GetData("jobdetail", "datecopymade", "jobdetailID", GetData("library", "jobdetailID", "barcode", txtBarcode.Text))) = "" Then
                        SetData "jobdetail", "datecopymade", "jobdetailID", GetData("library", "jobdetailID", "barcode", txtBarcode.Text), Format(GetData("library", "cdate", "barcode", txtBarcode.Text), "YYYY-MM-DD hh:nn:ss")
                    End If
                    If GetCount("SELECT COUNT(jobdetailID) FROM jobdetail WHERE jobID = " & GetData("library", "jobID", "barcode", txtBarcode.Text) & " AND completeddate IS NULL") = 0 Then
                        SetData "job", "fd_status", "jobID", GetData("library", "jobID", "barcode", txtBarcode.Text), "VT Done"
                    End If
                End If
            End If
        End If
    End If
    
    HighLite txtBarcode
End If

End Sub

Private Sub txtDespatchID_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    ShowDespatchDetail Val(txtDespatchID.Text)
End If

End Sub

Private Sub txtDespatchNumber_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then
    KeyAscii = 0
    If IsNumeric(txtDespatchNumber.Text) Then
    
        Dim l_lngDespatchID As Long
        l_lngDespatchID = GetData("despatch", "despatchID", "despatchnumber", txtDespatchNumber.Text)
        
        txtDespatchID.Text = l_lngDespatchID
        txtDespatchID_KeyPress vbKeyReturn
    
    End If
    
End If
    
End Sub

Private Sub txtProjectNumber_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    ShowDespatchForProject Val(txtProjectNumber.Text)

End If

End Sub

