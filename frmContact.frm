VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmContact 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Contact Information"
   ClientHeight    =   8235
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   18750
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmContact.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8235
   ScaleWidth      =   18750
   WindowState     =   2  'Maximized
   Begin VB.CheckBox chkForcePort33001 
      Alignment       =   1  'Right Justify
      Caption         =   "Port 33001"
      Height          =   195
      Left            =   14460
      TabIndex        =   81
      ToolTipText     =   "Has this contact been a Mailshot?"
      Top             =   3180
      Width           =   1275
   End
   Begin VB.CheckBox chkInactive 
      Alignment       =   1  'Right Justify
      Caption         =   "Active ?"
      Enabled         =   0   'False
      Height          =   255
      Left            =   17220
      TabIndex        =   80
      Top             =   4020
      Width           =   1335
   End
   Begin VB.TextBox txtFailedLogins 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   17460
      TabIndex        =   77
      ToolTipText     =   "Password for the JCA Website"
      Top             =   2820
      Width           =   1095
   End
   Begin VB.CheckBox chkSecurityLock 
      Alignment       =   1  'Right Justify
      Caption         =   "Security Lock"
      Height          =   195
      Left            =   14460
      TabIndex        =   76
      ToolTipText     =   "Has this contact been a Mailshot?"
      Top             =   2880
      Width           =   1275
   End
   Begin VB.TextBox txtAdminLink 
      DataField       =   "accountcode"
      Height          =   285
      Index           =   1
      Left            =   11880
      TabIndex        =   66
      Top             =   4020
      Width           =   4935
   End
   Begin VB.TextBox txtAdminTitle 
      DataField       =   "accountcode"
      Height          =   285
      Index           =   1
      Left            =   11880
      TabIndex        =   65
      Top             =   4320
      Width           =   4935
   End
   Begin VB.TextBox txtAdminText 
      DataField       =   "accountcode"
      Height          =   585
      Index           =   1
      Left            =   11880
      MultiLine       =   -1  'True
      TabIndex        =   64
      Top             =   4620
      Width           =   6675
   End
   Begin VB.TextBox txtAdminLink 
      DataField       =   "accountcode"
      Height          =   285
      Index           =   2
      Left            =   11880
      TabIndex        =   63
      Top             =   5220
      Width           =   4935
   End
   Begin VB.TextBox txtAdminTitle 
      DataField       =   "accountcode"
      Height          =   285
      Index           =   2
      Left            =   11880
      TabIndex        =   62
      Top             =   5520
      Width           =   4935
   End
   Begin VB.TextBox txtAdminText 
      DataField       =   "accountcode"
      Height          =   585
      Index           =   2
      Left            =   11880
      MultiLine       =   -1  'True
      TabIndex        =   61
      Top             =   5820
      Width           =   6615
   End
   Begin VB.TextBox txtAdminLink 
      DataField       =   "accountcode"
      Height          =   285
      Index           =   3
      Left            =   11880
      TabIndex        =   60
      Top             =   6420
      Width           =   4935
   End
   Begin VB.TextBox txtAdminTitle 
      DataField       =   "accountcode"
      Height          =   285
      Index           =   3
      Left            =   11880
      TabIndex        =   59
      Top             =   6720
      Width           =   4935
   End
   Begin VB.TextBox txtAdminText 
      DataField       =   "accountcode"
      Height          =   585
      Index           =   3
      Left            =   11880
      MultiLine       =   -1  'True
      TabIndex        =   58
      Top             =   7020
      Width           =   6615
   End
   Begin VB.TextBox txtJobTitle 
      Height          =   315
      Left            =   15540
      TabIndex        =   48
      ToolTipText     =   "Job title at the contact's Company"
      Top             =   1020
      Width           =   3015
   End
   Begin VB.CommandButton cmdRemove 
      Caption         =   "Remove"
      Height          =   315
      Left            =   16620
      TabIndex        =   47
      ToolTipText     =   "Remove this contact"
      Top             =   3240
      Width           =   1935
   End
   Begin VB.TextBox txtUserName 
      Height          =   315
      Left            =   15540
      TabIndex        =   46
      ToolTipText     =   "User Name for the JCA Website"
      Top             =   1380
      Width           =   3015
   End
   Begin VB.TextBox txtPassword 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   15540
      TabIndex        =   45
      ToolTipText     =   "Password for the JCA Website"
      Top             =   1740
      Width           =   1875
   End
   Begin VB.CommandButton cmdRandomPassword 
      Caption         =   "Make Pwd"
      Height          =   315
      Left            =   17460
      TabIndex        =   44
      Top             =   1740
      Width           =   1095
   End
   Begin VB.CommandButton cmdReportOnline 
      Caption         =   "Notify Online Web"
      Height          =   315
      Left            =   14460
      TabIndex        =   43
      Top             =   3600
      Width           =   1935
   End
   Begin VB.CommandButton cmdReportAgency 
      Caption         =   "Notify Agency Web"
      Height          =   315
      Left            =   16620
      TabIndex        =   42
      Top             =   3600
      Width           =   1935
   End
   Begin VB.TextBox txtCetaCode 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   15540
      TabIndex        =   41
      ToolTipText     =   "Password for the JCA Website"
      Top             =   2100
      Width           =   3015
   End
   Begin VB.TextBox txtUserSetting 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   15540
      TabIndex        =   40
      ToolTipText     =   "Password for the JCA Website"
      Top             =   2460
      Width           =   3015
   End
   Begin VB.TextBox txtVocation 
      Height          =   315
      Left            =   10560
      TabIndex        =   37
      ToolTipText     =   "Contact's Occupation"
      Top             =   600
      Width           =   2955
   End
   Begin VB.TextBox txtComments 
      Height          =   1695
      Left            =   4860
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   35
      ToolTipText     =   "Notes about the contact"
      Top             =   5880
      Width           =   4455
   End
   Begin MSAdodcLib.Adodc adoContactList 
      Height          =   330
      Left            =   2040
      Top             =   1440
      Visible         =   0   'False
      Width           =   1845
      _ExtentX        =   3254
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoContactList"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Deactivate"
      Height          =   315
      Left            =   13560
      TabIndex        =   16
      ToolTipText     =   "Delete this Contact "
      Top             =   7740
      Width           =   1155
   End
   Begin VB.CommandButton cmdLoadContactsForCompany 
      Caption         =   "Load Contacts For Company"
      Height          =   315
      Left            =   4860
      TabIndex        =   33
      ToolTipText     =   "Clear the form"
      Top             =   7800
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.CheckBox chkFreelancer 
      Caption         =   "Freelancer"
      Height          =   195
      Left            =   9480
      TabIndex        =   14
      ToolTipText     =   "Is this contact a freelancer?"
      Top             =   240
      Width           =   1155
   End
   Begin VB.CheckBox chkMailshot 
      Caption         =   "Mailshot"
      Height          =   195
      Left            =   10860
      TabIndex        =   15
      ToolTipText     =   "Has this contact been a Mailshot?"
      Top             =   240
      Width           =   1155
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   17340
      TabIndex        =   19
      ToolTipText     =   "Close this form"
      Top             =   7740
      Width           =   1155
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save"
      Height          =   315
      Left            =   16080
      TabIndex        =   18
      ToolTipText     =   "Save changes to this contact"
      Top             =   7740
      Width           =   1155
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear"
      Height          =   315
      Left            =   14820
      TabIndex        =   17
      ToolTipText     =   "Clear the form"
      Top             =   7740
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adoContact 
      Height          =   330
      Left            =   7500
      Top             =   180
      Visible         =   0   'False
      Width           =   1845
      _ExtentX        =   3254
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoContact"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox txtMobile 
      Height          =   315
      Left            =   6000
      TabIndex        =   8
      ToolTipText     =   "Mobile Number for the contact"
      Top             =   2340
      Width           =   3315
   End
   Begin VB.ComboBox cmbTitle 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   7980
      TabIndex        =   6
      ToolTipText     =   "Title for the contact"
      Top             =   1620
      Width           =   1335
   End
   Begin VB.TextBox txtInitials 
      Height          =   315
      Left            =   6000
      TabIndex        =   5
      ToolTipText     =   "Initials"
      Top             =   1620
      Width           =   975
   End
   Begin VB.TextBox txtLastName 
      Height          =   315
      Left            =   6000
      TabIndex        =   4
      ToolTipText     =   "Last Name"
      Top             =   1260
      Width           =   3315
   End
   Begin VB.TextBox txtFirstName 
      Height          =   315
      Left            =   6000
      TabIndex        =   3
      ToolTipText     =   "First Name"
      Top             =   900
      Width           =   3315
   End
   Begin VB.TextBox txtContactID 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0FF&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   6000
      TabIndex        =   1
      ToolTipText     =   "The unique identifier for this contact"
      Top             =   180
      Width           =   1395
   End
   Begin VB.TextBox txtAddress 
      Height          =   1335
      Left            =   6000
      MultiLine       =   -1  'True
      TabIndex        =   11
      ToolTipText     =   "Address for the contact"
      Top             =   3420
      Width           =   3315
   End
   Begin VB.TextBox txtPostCode 
      Height          =   315
      Left            =   6000
      TabIndex        =   12
      ToolTipText     =   "Postcode for the contact"
      Top             =   4800
      Width           =   3315
   End
   Begin VB.TextBox txtCountry 
      Height          =   315
      Left            =   6000
      TabIndex        =   13
      ToolTipText     =   "Country for the contact"
      Top             =   5160
      Width           =   3315
   End
   Begin VB.TextBox txtTelephone 
      Height          =   315
      Left            =   6000
      TabIndex        =   7
      ToolTipText     =   "The normal telephone number for the contact"
      Top             =   1980
      Width           =   3315
   End
   Begin VB.TextBox txtFax 
      Height          =   315
      Left            =   6000
      TabIndex        =   9
      ToolTipText     =   "Fax number for the contact"
      Top             =   2700
      Width           =   3315
   End
   Begin VB.TextBox txtEmail 
      Height          =   315
      Left            =   6000
      TabIndex        =   10
      ToolTipText     =   "email address for the contact"
      Top             =   3060
      Width           =   3315
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbContact 
      Bindings        =   "frmContact.frx":08CA
      Height          =   315
      Left            =   6000
      TabIndex        =   2
      ToolTipText     =   "Normal name for this contact"
      Top             =   540
      Width           =   3315
      DataFieldList   =   "name"
      BevelType       =   0
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "contactID"
      Columns(0).Name =   "contactID"
      Columns(0).DataField=   "contactID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3519
      Columns(1).Caption=   "Name"
      Columns(1).Name =   "name"
      Columns(1).DataField=   "name"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "Email"
      Columns(2).Name =   "email"
      Columns(2).DataField=   "email"
      Columns(2).FieldLen=   256
      _ExtentX        =   5847
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "name"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdCompanyList 
      Bindings        =   "frmContact.frx":08E3
      Height          =   8055
      Left            =   480
      TabIndex        =   0
      Top             =   60
      Width           =   4275
      _Version        =   196617
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   -2147483630
      ForeColorOdd    =   -2147483630
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "contactID"
      Columns(0).Name =   "contactID"
      Columns(0).DataField=   "contactID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3889
      Columns(1).Caption=   "name"
      Columns(1).Name =   "name"
      Columns(1).DataField=   "name"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3175
      Columns(2).Caption=   "Company"
      Columns(2).Name =   "companyname"
      Columns(2).DataField=   "companyname"
      Columns(2).FieldLen=   256
      _ExtentX        =   7541
      _ExtentY        =   14208
      _StockProps     =   79
      BackColor       =   -2147483643
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.TabStrip tabContactSearch 
      Height          =   7935
      Left            =   60
      TabIndex        =   34
      Top             =   60
      Width           =   435
      _ExtentX        =   767
      _ExtentY        =   13996
      MultiRow        =   -1  'True
      Placement       =   2
      TabMinWidth     =   353
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   28
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "A"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "B"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "C"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab4 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "D"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab5 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "E"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab6 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "F"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab7 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "G"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab8 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "H"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab9 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "I"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab10 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "J"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab11 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "K"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab12 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "L"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab13 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "M"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab14 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "N"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab15 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "O"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab16 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "P"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab17 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Q"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab18 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "R"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab19 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "S"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab20 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "T"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab21 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "U"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab22 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "V"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab23 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "W"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab24 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "X"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab25 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Y"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab26 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Z"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab27 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "#"
            Object.ToolTipText     =   "Show items which begin with numbers"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab28 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "*"
            Object.ToolTipText     =   "Show all items"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoCompanyAttach 
      Height          =   330
      Left            =   9600
      Top             =   1380
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCompanyAttach"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdCompanies 
      Bindings        =   "frmContact.frx":0900
      Height          =   2895
      Left            =   9540
      TabIndex        =   39
      Top             =   1020
      Width           =   4815
      _Version        =   196617
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowColumnSizing=   0   'False
      AllowColumnShrinking=   0   'False
      BackColorOdd    =   16761087
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   6
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "companyID"
      Columns(0).Name =   "companyID"
      Columns(0).DataField=   "companyID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   4233
      Columns(1).Caption=   "Company"
      Columns(1).Name =   "name"
      Columns(1).DataField=   "name"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3175
      Columns(2).Caption=   "Job Title"
      Columns(2).Name =   "jobtitle"
      Columns(2).DataField=   "jobtitle"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "username"
      Columns(3).Name =   "username"
      Columns(3).DataField=   "username"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "password"
      Columns(4).Name =   "password"
      Columns(4).DataField=   "password"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "employeeID"
      Columns(5).Name =   "employeeID"
      Columns(5).DataField=   "employeeID"
      Columns(5).FieldLen=   256
      _ExtentX        =   8493
      _ExtentY        =   5106
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoCompany 
      Height          =   330
      Left            =   15540
      Top             =   540
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCompany"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Bindings        =   "frmContact.frx":091F
      Height          =   315
      Left            =   15540
      TabIndex        =   49
      ToolTipText     =   "Company Name"
      Top             =   660
      Width           =   3015
      DataFieldList   =   "name"
      BevelType       =   0
      _Version        =   196617
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5609
      Columns(0).Caption=   "Company Name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   5318
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "name"
   End
   Begin VB.Label lblPasswordLastChanged 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   13800
      TabIndex        =   79
      Top             =   120
      Visible         =   0   'False
      Width           =   1755
   End
   Begin VB.Label lblCaption 
      Caption         =   "Failed Logins"
      Height          =   255
      Index           =   9
      Left            =   16380
      TabIndex        =   78
      Top             =   2880
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Media Window Admin Link 1"
      Height          =   255
      Index           =   70
      Left            =   9480
      TabIndex        =   75
      Top             =   4080
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Media Window Admin Title 1"
      Height          =   255
      Index           =   71
      Left            =   9480
      TabIndex        =   74
      Top             =   4380
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Media Window Admin Text 1"
      Height          =   255
      Index           =   72
      Left            =   9480
      TabIndex        =   73
      Top             =   4680
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Media Window Admin Link 2"
      Height          =   255
      Index           =   73
      Left            =   9480
      TabIndex        =   72
      Top             =   5280
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Media Window Admin Title 2"
      Height          =   255
      Index           =   74
      Left            =   9480
      TabIndex        =   71
      Top             =   5580
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Media Window Admin Text 2"
      Height          =   255
      Index           =   75
      Left            =   9480
      TabIndex        =   70
      Top             =   5880
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Media Window Admin Link 3"
      Height          =   255
      Index           =   76
      Left            =   9480
      TabIndex        =   69
      Top             =   6480
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Media Window Admin Title 3"
      Height          =   255
      Index           =   77
      Left            =   9480
      TabIndex        =   68
      Top             =   6780
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Media Window Admin Text 3"
      Height          =   255
      Index           =   78
      Left            =   9480
      TabIndex        =   67
      Top             =   7080
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      Height          =   255
      Index           =   8
      Left            =   14460
      TabIndex        =   57
      Top             =   720
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job Title"
      Height          =   255
      Index           =   11
      Left            =   14460
      TabIndex        =   56
      Top             =   1080
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "User Name"
      Height          =   255
      Index           =   17
      Left            =   14460
      TabIndex        =   55
      Top             =   1440
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Password"
      Height          =   255
      Index           =   18
      Left            =   14460
      TabIndex        =   54
      Top             =   1800
      Width           =   855
   End
   Begin VB.Label lblCompanyID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   12480
      TabIndex        =   53
      Top             =   360
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblEmployeeID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   12480
      TabIndex        =   52
      Top             =   120
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Ceta Codes"
      Height          =   255
      Index           =   20
      Left            =   14460
      TabIndex        =   51
      Top             =   2160
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "User Setting"
      Height          =   255
      Index           =   21
      Left            =   14460
      TabIndex        =   50
      Top             =   2520
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Vocation"
      Height          =   255
      Index           =   19
      Left            =   9540
      TabIndex        =   38
      Top             =   660
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Comments"
      Height          =   255
      Index           =   12
      Left            =   4860
      TabIndex        =   36
      Top             =   5580
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Mobile"
      Height          =   255
      Index           =   10
      Left            =   4860
      TabIndex        =   32
      Top             =   2340
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      Height          =   255
      Index           =   16
      Left            =   7080
      TabIndex        =   31
      Top             =   1680
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Initials"
      Height          =   255
      Index           =   15
      Left            =   4860
      TabIndex        =   30
      Top             =   1680
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Last Name"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   14
      Left            =   4860
      TabIndex        =   29
      Top             =   1320
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "First Name"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   13
      Left            =   4860
      TabIndex        =   28
      Top             =   960
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Contact ID"
      Height          =   255
      Index           =   0
      Left            =   4860
      TabIndex        =   27
      Top             =   240
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Known As"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   4860
      TabIndex        =   26
      Top             =   600
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Address"
      Height          =   255
      Index           =   1
      Left            =   4860
      TabIndex        =   25
      Top             =   3420
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Post Code"
      Height          =   255
      Index           =   3
      Left            =   4860
      TabIndex        =   24
      Top             =   4860
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Country"
      Height          =   255
      Index           =   4
      Left            =   4860
      TabIndex        =   23
      Top             =   5220
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Telephone"
      Height          =   255
      Index           =   5
      Left            =   4860
      TabIndex        =   22
      Top             =   2040
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Fax"
      Height          =   255
      Index           =   6
      Left            =   4860
      TabIndex        =   21
      Top             =   2700
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Email"
      Height          =   255
      Index           =   7
      Left            =   4860
      TabIndex        =   20
      Top             =   3060
      Width           =   1035
   End
End
Attribute VB_Name = "frmContact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub adoCompanyAttach_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)
If adoCompanyAttach.Recordset.EOF = True Then Exit Sub

If grdCompanies.Rows > 0 Then
    cmbCompany.Text = adoCompanyAttach.Recordset("name")
    lblCompanyID.Caption = adoCompanyAttach.Recordset("companyID")
    txtJobTitle.Text = Trim(" " & adoCompanyAttach.Recordset("jobtitle"))
    txtUserName.Text = Trim(" " & adoCompanyAttach.Recordset("username"))
    txtPassword.Text = Trim(" " & adoCompanyAttach.Recordset("password"))
    txtCetaCode.Text = Trim(" " & adoCompanyAttach.Recordset("cetacode"))
    txtUserSetting.Text = Trim(" " & adoCompanyAttach.Recordset("usersettings"))
    txtAdminLink(1).Text = Trim(" " & adoCompanyAttach.Recordset("portaladminlink1"))
    txtAdminLink(2).Text = Trim(" " & adoCompanyAttach.Recordset("portaladminlink2"))
    txtAdminLink(3).Text = Trim(" " & adoCompanyAttach.Recordset("portaladminlink3"))
    txtAdminTitle(1).Text = Trim(" " & adoCompanyAttach.Recordset("portaladmintitle1"))
    txtAdminTitle(2).Text = Trim(" " & adoCompanyAttach.Recordset("portaladmintitle2"))
    txtAdminTitle(3).Text = Trim(" " & adoCompanyAttach.Recordset("portaladmintitle3"))
    txtAdminText(1).Text = Trim(" " & adoCompanyAttach.Recordset("portaladmintext1"))
    txtAdminText(2).Text = Trim(" " & adoCompanyAttach.Recordset("portaladmintext2"))
    txtAdminText(3).Text = Trim(" " & adoCompanyAttach.Recordset("portaladmintext3"))
    lblEmployeeID.Caption = adoCompanyAttach.Recordset("employeeID")
    chkSecurityLock.Value = adoCompanyAttach.Recordset("securitylock")
    txtFailedLogins.Text = Val(adoCompanyAttach.Recordset("failedlogins"))
    chkForcePort33001.Value = GetFlag(adoCompanyAttach.Recordset("forceport33001"))
    lblPasswordLastChanged.Caption = Format(adoCompanyAttach.Recordset("passwordlastreset"), "dd MMM yy hh:nn:ss")
End If

End Sub

Private Sub cmbCompany_Click()
lblCompanyID.Caption = cmbCompany.Columns("companyID").Text
End Sub

Private Sub cmbContact_CloseUp()
If cmbContact.Columns("contactID").Text = "" Then Exit Sub
ShowContact Val(cmbContact.Columns("contactID").Text)
End Sub

Private Sub cmbContact_DropDown()

adoContact.ConnectionString = g_strConnection
adoContact.RecordSource = "SELECT name, email, contactID FROM contact WHERE name LIKE '" & cmbContact.Text & "%' AND system_active = 1 ORDER BY name;"
adoContact.Refresh

End Sub

Private Sub cmdClear_Click()
ClearFields Me

Dim l_strSQL  As String

l_strSQL = "SELECT employee.employeeID, company.companyID, company.name, employee.jobtitle, employee.username, employee.password, employee.cetacode, employee.usersettings, employee.portaladminlink1, employee.portaladminlink2, employee.portaladminlink3, employee.portaladmintitle1, employee.portaladmintitle2, employee.portaladmintitle3, employee.portaladmintext1, employee.portaladmintext2, employee.portaladmintext3, employee.securitylock, employee.failedlogins, employee.passwordlastreset, employee.forceport33001 FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((contact.contactID)=-1)) ORDER BY company.name;"

adoCompanyAttach.ConnectionString = g_strConnection
adoCompanyAttach.RecordSource = l_strSQL
adoCompanyAttach.Refresh

End Sub

Private Sub cmdClose_Click()
frmContact.Hide
Unload frmContact
End Sub

Private Sub cmdDelete_Click()
DeleteContact
End Sub

Private Sub cmdLoadContactsForCompany_Click()

If Val(lblCompanyID.Caption) = 0 Then
    Exit Sub
End If

Dim l_strSQL As String
l_strSQL = "SELECT company.companyID, contact.name, contact.email, contact.contactID, contact.telephone, employee.jobtitle FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((company.companyID)=" & lblCompanyID.Caption & "));"

adoContact.ConnectionString = g_strConnection
adoContact.RecordSource = l_strSQL
adoContact.Refresh

cmbCompany.Text = GetData("company", "name", "companyID", lblCompanyID.Caption)

End Sub

Private Sub cmdRandomPassword_Click()

txtPassword.Text = RndPassword(6)

End Sub

Private Sub cmdRemove_Click()

If Val(lblEmployeeID.Caption) <> 0 Then
    
    RemoveEmployee lblEmployeeID.Caption
    
    adoCompanyAttach.Refresh
End If

End Sub

Private Sub cmdReportAgency_Click()

Dim l_strSelectionFormula As String, l_strReportFile As String
l_strSelectionFormula = "{employee.employeeID} = " & lblEmployeeID.Caption

l_strReportFile = g_strLocationOfCrystalReportFiles & "reportagency.rpt"

PrintCrystalReport l_strReportFile, l_strSelectionFormula, True

End Sub

Private Sub cmdReportOnline_Click()

Dim l_strSelectionFormula As String, l_strReportFile As String
l_strSelectionFormula = "{employee.employeeID} = " & lblEmployeeID.Caption

l_strReportFile = g_strLocationOfCrystalReportFiles & "reportonline.rpt"

PrintCrystalReport l_strReportFile, l_strSelectionFormula, True

End Sub

Private Sub cmdSave_Click()

'If txtContactID.Text = "" Then cmdSave_Click

If cmbContact.Text = "" Then
    MsgBox "Please enter a valid 'known as' name for this contact", vbExclamation
    Exit Sub
End If

If txtFirstName.Text = "" Then
    MsgBox "Please enter a valid 'First Name' for this contact", vbExclamation
    Exit Sub
End If
    
If txtLastName.Text = "" Then
    MsgBox "Please enter a valid 'Last Name' for this contact", vbExclamation
    Exit Sub
End If

If SaveContact = True Then

    SaveEmployeeRecord Val(lblCompanyID.Caption), txtContactID.Text, txtJobTitle.Text, txtUserName.Text, txtPassword.Text, txtCetaCode.Text, txtUserSetting.Text, txtAdminLink(1).Text, txtAdminLink(2).Text, txtAdminLink(3).Text, txtAdminTitle(1).Text, txtAdminTitle(2).Text, txtAdminTitle(3).Text, txtAdminText(1).Text, txtAdminText(2).Text, txtAdminText(3).Text, Val(txtFailedLogins.Text), chkSecurityLock.Value, IIf(lblPasswordLastChanged.Caption <> "", lblPasswordLastChanged.Caption, Null), chkForcePort33001.Value
    ShowContact Val(txtContactID.Text)

End If

End Sub

Private Sub Form_Activate()

If CheckAccess("/reactivatecompany", True) Then
    chkInactive.Enabled = True
Else
    chkInactive.Enabled = False
End If
'Me.WindowState = vbMaximized

End Sub

Private Sub Form_Load()

MakeLookLikeOffice Me

Dim l_strSQL As String
l_strSQL = "SELECT name, companyID, system_active FROM company ORDER BY name"

adoCompany.ConnectionString = g_strConnection
adoCompany.RecordSource = l_strSQL
adoCompany.Refresh

PopulateCombo "title", cmbTitle

'CenterForm Me

End Sub

Private Sub grdCompanies_Click()
If grdCompanies.Rows = 0 Then Exit Sub
adoCompanyAttach.Recordset.Find "employeeID = " & grdCompanies.Columns("employeeID").Text
End Sub

Private Sub grdCompanies_DblClick()
If grdCompanies.Rows < 1 Then Exit Sub
ShowCompany Val(grdCompanies.Columns("companyID").Text)
End Sub

Private Sub grdCompanyList_Click()
ShowContact Val(grdCompanyList.Columns("contactID").Text)
End Sub

Private Sub tabContactSearch_Click()
Dim l_strSearch As String
If adoContactList.ConnectionString = "" Then
    adoContactList.ConnectionString = g_strConnection
    adoContactList.RecordSource = "SELECT * FROM contact WHERE system_active = 1 ORDER BY name"
    adoContactList.Refresh
End If
If tabContactSearch.SelectedItem.Caption = "*" Then
    adoContactList.Recordset.Filter = "system_active = 1"
ElseIf tabContactSearch.SelectedItem.Caption = "#" Then
    adoContactList.Recordset.Filter = "name LIKE '0%' OR name LIKE '1%' OR name LIKE '2%'  OR name LIKE '3%' OR name LIKE '4%' OR name LIKE '5%' OR name LIKE '6%' OR name LIKE '7%' OR name LIKE '8%' OR name LIKE '9%'"
Else
    l_strSearch = tabContactSearch.SelectedItem.Caption
    adoContactList.Recordset.Filter = "system_active = 1 AND name LIKE '" & l_strSearch & "%'"
End If
End Sub

Private Sub txtContactID_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    If IsNumeric(txtContactID.Text) Then
        ShowContact Val(txtContactID.Text)
    End If
    HighLite txtContactID
End If
End Sub

Private Sub txtFirstName_GotFocus()
On Error Resume Next
If txtFirstName.Text = "" Then
    txtFirstName.Text = Left(cmbContact.Text, InStr(cmbContact.Text, " ") - 1)
Else
    HighLite txtFirstName
End If
End Sub

Private Sub txtLastName_GotFocus()
On Error Resume Next
If txtLastName.Text = "" Then
    txtLastName.Text = Right(cmbContact.Text, Len(cmbContact.Text) - InStr(cmbContact.Text, " "))
Else
    HighLite txtLastName
End If
End Sub
