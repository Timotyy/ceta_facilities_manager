VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmiTunesPackage 
   AutoRedraw      =   -1  'True
   Caption         =   "Media File Details"
   ClientHeight    =   15615
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   25215
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   15615
   ScaleWidth      =   25215
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtSearchVendorID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   14460
      TabIndex        =   565
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   1500
      Width           =   1935
   End
   Begin VB.OptionButton optFilter 
      Caption         =   "Xbox"
      Height          =   315
      Index           =   8
      Left            =   14520
      TabIndex        =   557
      Top             =   1080
      Width           =   1155
   End
   Begin VB.OptionButton optFilter 
      Caption         =   "Unused Xbox"
      Height          =   315
      Index           =   7
      Left            =   12600
      TabIndex        =   556
      Top             =   1080
      Width           =   1755
   End
   Begin VB.OptionButton optFilter 
      Caption         =   "iTunes Film"
      Height          =   315
      Index           =   6
      Left            =   14400
      TabIndex        =   548
      Top             =   2460
      Visible         =   0   'False
      Width           =   1755
   End
   Begin VB.OptionButton optFilter 
      Caption         =   "Unused Amazon"
      Height          =   315
      Index           =   5
      Left            =   12600
      TabIndex        =   536
      Top             =   720
      Width           =   1755
   End
   Begin VB.OptionButton optFilter 
      Caption         =   "Amazon"
      Height          =   315
      Index           =   3
      Left            =   14520
      TabIndex        =   37
      Top             =   720
      Width           =   1155
   End
   Begin VB.OptionButton optFilter 
      Caption         =   "Unused iTunes TV"
      Height          =   315
      Index           =   4
      Left            =   12600
      TabIndex        =   36
      Top             =   360
      Value           =   -1  'True
      Width           =   1755
   End
   Begin VB.CommandButton cmdUpdateCrewUtf8 
      Caption         =   "Update Crew UTF8"
      Height          =   315
      Left            =   21720
      TabIndex        =   35
      Top             =   15420
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.CommandButton cmdUpdateCastCharacters 
      Caption         =   "Update Cast Characters UTF8"
      Height          =   315
      Left            =   21720
      TabIndex        =   34
      Top             =   15060
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.CommandButton cmdUpdatecastUTF8 
      Caption         =   "Update Cast UTF8"
      Height          =   315
      Left            =   21720
      TabIndex        =   32
      Top             =   14700
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.CommandButton cmdUpdateUTF8 
      Caption         =   "Update UTF8"
      Height          =   315
      Left            =   21720
      TabIndex        =   31
      Top             =   14340
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.TextBox txtiTunesVendorID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   18660
      TabIndex        =   29
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   1080
      Width           =   3855
   End
   Begin VB.OptionButton optFilter 
      Caption         =   "iTunes TV"
      Height          =   315
      Index           =   2
      Left            =   14520
      TabIndex        =   28
      Top             =   360
      Width           =   1155
   End
   Begin VB.OptionButton optFilter 
      Caption         =   "Unused iTunes Film"
      Height          =   315
      Index           =   1
      Left            =   12480
      TabIndex        =   27
      Top             =   2460
      Visible         =   0   'False
      Width           =   1755
   End
   Begin VB.OptionButton optFilter 
      Caption         =   "All Items"
      Height          =   315
      Index           =   0
      Left            =   12600
      TabIndex        =   26
      Top             =   0
      Width           =   1755
   End
   Begin VB.ComboBox cmbCaptionsReason 
      Height          =   315
      Left            =   18660
      TabIndex        =   23
      ToolTipText     =   "The Version for the Tape"
      Top             =   2880
      Width           =   3855
   End
   Begin VB.CheckBox chkCaptionsAvailability 
      Caption         =   "Check1"
      Height          =   255
      Left            =   18690
      TabIndex        =   22
      Top             =   2580
      Width           =   255
   End
   Begin MSAdodcLib.Adodc adoSpokenLocale 
      Height          =   330
      Left            =   3120
      Top             =   14820
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoSpokenLocale"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox txtiTunesVendorOfferCode 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   18660
      TabIndex        =   17
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   1440
      Width           =   3855
   End
   Begin VB.CommandButton cmdAddPackage 
      Caption         =   "Add New"
      Height          =   330
      Left            =   21300
      TabIndex        =   15
      Top             =   720
      Width           =   1215
   End
   Begin VB.TextBox txtiTunesISAN 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   18660
      TabIndex        =   12
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   1800
      Width           =   3855
   End
   Begin VB.CommandButton cmdClearPackageID 
      Caption         =   "C"
      Height          =   315
      Left            =   20940
      TabIndex        =   10
      Top             =   720
      Width           =   315
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   18780
      TabIndex        =   6
      Top             =   13680
      Width           =   5355
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search"
         Height          =   315
         Left            =   0
         TabIndex        =   14
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "New Package"
         Height          =   315
         Left            =   2760
         TabIndex        =   9
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   4140
         TabIndex        =   8
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         Height          =   315
         Left            =   1380
         TabIndex        =   7
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1215
      End
   End
   Begin VB.ComboBox cmbiTunesType 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   18660
      TabIndex        =   4
      ToolTipText     =   "The Version for the Tape"
      Top             =   360
      Width           =   3855
   End
   Begin MSAdodcLib.Adodc adoiTunesPackage 
      Height          =   330
      Left            =   180
      Tag             =   "NOCLEAR"
      Top             =   0
      Width           =   2745
      _ExtentX        =   4842
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoiTunesPackage"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdItunesPackage 
      Bindings        =   "frmiTunesPackage.frx":0000
      Height          =   3435
      Left            =   180
      TabIndex        =   0
      Tag             =   "NOCLEAR"
      Top             =   0
      Width           =   12195
      _Version        =   196617
      AllowDelete     =   -1  'True
      AllowUpdate     =   0   'False
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   12
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "itunes_packageID"
      Columns(0).Name =   "itunes_packageID"
      Columns(0).DataField=   "itunes_packageID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3810
      Columns(2).Caption=   "Company"
      Columns(2).Name =   "Company"
      Columns(2).DataField=   "Column 4"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   3200
      Columns(3).Caption=   "SeriesName"
      Columns(3).Name =   "AmazonSeriesName"
      Columns(3).DataField=   "AmazonSeriesName"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "SeasonNumber"
      Columns(4).Name =   "AmazonSeasonNumber"
      Columns(4).DataField=   "AmazonSeasonNumber"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "video_containerID"
      Columns(5).Name =   "video_containerID"
      Columns(5).DataField=   "video_containerID"
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   3200
      Columns(6).Caption=   "video_title"
      Columns(6).Name =   "video_title"
      Columns(6).DataField=   "video_title"
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   5292
      Columns(7).Caption=   "video_vendorID"
      Columns(7).Name =   "video_vendorID"
      Columns(7).DataField=   "video_vendorID"
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Caption=   "video_ep_prod_no"
      Columns(8).Name =   "video_ep_prod_no"
      Columns(8).DataField=   "video_ep_prod_no"
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "Package Type"
      Columns(9).Name =   "video_type"
      Columns(9).DataField=   "video_type"
      Columns(9).FieldLen=   256
      Columns(9).Locked=   -1  'True
      Columns(10).Width=   3200
      Columns(10).Caption=   "video_containerposition"
      Columns(10).Name=   "video_containerposition"
      Columns(10).DataField=   "video_containerposition"
      Columns(10).FieldLen=   256
      Columns(11).Width=   1032
      Columns(11).Caption=   "Used"
      Columns(11).Name=   "Used"
      Columns(11).DataField=   "packageused"
      Columns(11).FieldLen=   256
      Columns(11).Style=   2
      _ExtentX        =   21511
      _ExtentY        =   6059
      _StockProps     =   79
      Caption         =   "iTunes Packages"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoCompany 
      Height          =   330
      Left            =   3120
      Top             =   14100
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCompany"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Bindings        =   "frmiTunesPackage.frx":001F
      Height          =   315
      Left            =   18660
      TabIndex        =   1
      ToolTipText     =   "The company this tape belongs to"
      Top             =   0
      Width           =   3855
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   6800
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   12632319
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "name"
   End
   Begin MSAdodcLib.Adodc adoMetadataLanguage 
      Height          =   330
      Left            =   3120
      Top             =   14460
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoMetadataLanguage"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo txtOriginalSpokenLocale 
      Bindings        =   "frmiTunesPackage.frx":0038
      Height          =   315
      Left            =   18660
      TabIndex        =   20
      ToolTipText     =   "The company this tape belongs to"
      Top             =   2160
      Width           =   3855
      DataFieldList   =   "description"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "description"
      Columns(0).FieldLen=   256
      Columns(1).Width=   8043
      Columns(1).Caption=   "information"
      Columns(1).Name =   "information"
      Columns(1).DataField=   "information"
      Columns(1).FieldLen=   256
      _ExtentX        =   6800
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "description"
   End
   Begin MSAdodcLib.Adodc adoPreviewLocale 
      Height          =   330
      Left            =   3120
      Top             =   15180
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoPreviewLocale"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoVideoLanguage 
      Height          =   330
      Left            =   3120
      Top             =   15540
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoVideoLanguage"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoAdvisory 
      Height          =   330
      Left            =   9000
      Top             =   14820
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoAdvisory"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoGenres 
      Height          =   330
      Left            =   6060
      Top             =   14100
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoGenres"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoProduct 
      Height          =   330
      Left            =   6060
      Top             =   14460
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoProduct"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoISO2A 
      Height          =   330
      Left            =   6060
      Top             =   14820
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoISO2A"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoCast 
      Height          =   330
      Left            =   6060
      Top             =   15180
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCast"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoCrew 
      Height          =   330
      Left            =   6060
      Top             =   15900
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCrew"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoArtist1 
      Height          =   330
      Left            =   9000
      Top             =   14100
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoArtist1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoRoles1 
      Height          =   330
      Left            =   180
      Top             =   14100
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoRoles1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoCueSheet 
      Height          =   330
      Left            =   180
      Top             =   14460
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCueSheet"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoArtist2 
      Height          =   330
      Left            =   180
      Top             =   14820
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoArtist2"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoRoles2 
      Height          =   330
      Left            =   180
      Top             =   15180
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoRoles2"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoChapters 
      Height          =   330
      Left            =   180
      Top             =   15540
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoChapters"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoRoles 
      Height          =   330
      Left            =   17820
      Top             =   15180
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoRoles"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoGenreCodes 
      Height          =   330
      Left            =   9000
      Top             =   14460
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoGenreCodes"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoFullLocale 
      Height          =   330
      Left            =   9000
      Top             =   15180
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoFullLocale"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoISO3166 
      Height          =   330
      Left            =   14880
      Top             =   14820
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoISO3166"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoCastCharacter 
      Height          =   330
      Left            =   6060
      Top             =   15540
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCastCharacter"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   315
      Left            =   12600
      TabIndex        =   33
      Top             =   3060
      Visible         =   0   'False
      Width           =   3675
      _ExtentX        =   6482
      _ExtentY        =   556
      _Version        =   393216
      Appearance      =   1
   End
   Begin MSAdodcLib.Adodc adoPosterLocale 
      Height          =   330
      Left            =   3120
      Top             =   15900
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoPosterLocale"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoDubArtistLocale 
      Height          =   330
      Left            =   17820
      Top             =   15540
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoDubArtistLocale"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoAmazonTerritory 
      Height          =   330
      Left            =   9000
      Top             =   15540
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoAmazonTerritory"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin TabDlg.SSTab tabType 
      Height          =   10035
      Left            =   180
      TabIndex        =   38
      Top             =   3480
      Width           =   23955
      _ExtentX        =   42254
      _ExtentY        =   17701
      _Version        =   393216
      TabHeight       =   520
      TabCaption(0)   =   "TV Package (iTunes 5.2.8)"
      TabPicture(0)   =   "frmiTunesPackage.frx":0056
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblCaption(239)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblCaption(167)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblCaption(34)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblCaption(73)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblCaption(1)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblCaption(96)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "lblCaption(95)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "lblCaption(94)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "lblCaption(93)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "lblCaption(92)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "lblCaption(40)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "lblCaption(80)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "lblCaption(76)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "lblCaption(70)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "lblCaption(69)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "lblCaption(68)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "lblCaption(66)"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "lblCaption(85)"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "lblCaption(83)"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "lblCaption(79)"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "lblCaption(78)"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "lblCaption(72)"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "lblCaption(71)"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "lblCaption(67)"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "lblCaption(221)"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "txtMetaDataLanguage"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "txtVideoLanguage"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "grdiTunesAdvisory"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).Control(28)=   "ddnAdvisorySystem"
      Tab(0).Control(28).Enabled=   0   'False
      Tab(0).Control(29)=   "ddnAdvisory"
      Tab(0).Control(29).Enabled=   0   'False
      Tab(0).Control(30)=   "grdTVProduct"
      Tab(0).Control(30).Enabled=   0   'False
      Tab(0).Control(31)=   "ddnISO2A"
      Tab(0).Control(31).Enabled=   0   'False
      Tab(0).Control(32)=   "datiTunesReleaseDate"
      Tab(0).Control(32).Enabled=   0   'False
      Tab(0).Control(33)=   "txtStudio_Release_Title"
      Tab(0).Control(33).Enabled=   0   'False
      Tab(0).Control(34)=   "cmdReadExistingiTunesTVXML"
      Tab(0).Control(34).Enabled=   0   'False
      Tab(0).Control(35)=   "cmbiTunesProvider(1)"
      Tab(0).Control(35).Enabled=   0   'False
      Tab(0).Control(36)=   "txtiTunesCopyrightLine"
      Tab(0).Control(36).Enabled=   0   'False
      Tab(0).Control(37)=   "txtTitle"
      Tab(0).Control(37).Enabled=   0   'False
      Tab(0).Control(38)=   "txtiTunesTechComments"
      Tab(0).Control(38).Enabled=   0   'False
      Tab(0).Control(39)=   "txtCropRight"
      Tab(0).Control(39).Enabled=   0   'False
      Tab(0).Control(40)=   "txtCropLeft"
      Tab(0).Control(40).Enabled=   0   'False
      Tab(0).Control(41)=   "txtCropBottom"
      Tab(0).Control(41).Enabled=   0   'False
      Tab(0).Control(42)=   "txtCropTop"
      Tab(0).Control(42).Enabled=   0   'False
      Tab(0).Control(43)=   "chkPackageUsed(0)"
      Tab(0).Control(43).Enabled=   0   'False
      Tab(0).Control(44)=   "txtiTunesPreviewStartTime"
      Tab(0).Control(44).Enabled=   0   'False
      Tab(0).Control(45)=   "txtiTunesLongDescription"
      Tab(0).Control(45).Enabled=   0   'False
      Tab(0).Control(46)=   "txtiTunesContainerPosition"
      Tab(0).Control(46).Enabled=   0   'False
      Tab(0).Control(47)=   "txtiTunesContainerID"
      Tab(0).Control(47).Enabled=   0   'False
      Tab(0).Control(48)=   "txtiTunesEpProdNo"
      Tab(0).Control(48).Enabled=   0   'False
      Tab(0).Control(49)=   "cmbJP"
      Tab(0).Control(49).Enabled=   0   'False
      Tab(0).Control(50)=   "cmbUS"
      Tab(0).Control(50).Enabled=   0   'False
      Tab(0).Control(51)=   "cmbUK"
      Tab(0).Control(51).Enabled=   0   'False
      Tab(0).Control(52)=   "cmbFR"
      Tab(0).Control(52).Enabled=   0   'False
      Tab(0).Control(53)=   "cmbDE"
      Tab(0).Control(53).Enabled=   0   'False
      Tab(0).Control(54)=   "cmbCA"
      Tab(0).Control(54).Enabled=   0   'False
      Tab(0).Control(55)=   "cmbAU"
      Tab(0).Control(55).Enabled=   0   'False
      Tab(0).Control(56)=   "cmbSubType(1)"
      Tab(0).Control(56).Enabled=   0   'False
      Tab(0).ControlCount=   57
      TabCaption(1)   =   "Film Package (iTunes 5.2.4)"
      TabPicture(1)   =   "frmiTunesPackage.frx":0072
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "chkPackageUsed(2)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "cmbSubType(0)"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "cmdMakeXMLFiles"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "txtProductionCompany"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "txtUPC"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "txtAMGVideoID"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "cmdMakeMetadataOnlyXML"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "cmdMakeFullAssetOnlyXML"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "cmdMakePreviewAssetOnlyUpdateXML"
      Tab(1).Control(8).Enabled=   0   'False
      Tab(1).Control(9)=   "chkMultiLanguage"
      Tab(1).Control(9).Enabled=   0   'False
      Tab(1).Control(10)=   "cmdMakeFeatureAssetOnlyUpdateXML"
      Tab(1).Control(10).Enabled=   0   'False
      Tab(1).Control(11)=   "cmbiTunesProvider(0)"
      Tab(1).Control(11).Enabled=   0   'False
      Tab(1).Control(12)=   "txtStudioReleaseTitle"
      Tab(1).Control(12).Enabled=   0   'False
      Tab(1).Control(13)=   "txtComments"
      Tab(1).Control(13).Enabled=   0   'False
      Tab(1).Control(14)=   "cmdMakeAssetlessPreorderXML"
      Tab(1).Control(14).Enabled=   0   'False
      Tab(1).Control(15)=   "cmdMakeAssetWithChapters"
      Tab(1).Control(15).Enabled=   0   'False
      Tab(1).Control(16)=   "SSTab1"
      Tab(1).Control(16).Enabled=   0   'False
      Tab(1).Control(17)=   "qb"
      Tab(1).Control(17).Enabled=   0   'False
      Tab(1).Control(18)=   "ddnVodType"
      Tab(1).Control(18).Enabled=   0   'False
      Tab(1).Control(19)=   "ddnTrueFalse"
      Tab(1).Control(19).Enabled=   0   'False
      Tab(1).Control(20)=   "tabFilm"
      Tab(1).Control(20).Enabled=   0   'False
      Tab(1).Control(21)=   "ddnGenre"
      Tab(1).Control(21).Enabled=   0   'False
      Tab(1).Control(22)=   "cmbCountry"
      Tab(1).Control(22).Enabled=   0   'False
      Tab(1).Control(23)=   "grdGenre"
      Tab(1).Control(23).Enabled=   0   'False
      Tab(1).Control(24)=   "grdFilmProduct"
      Tab(1).Control(24).Enabled=   0   'False
      Tab(1).Control(25)=   "lblCaption(19)"
      Tab(1).Control(25).Enabled=   0   'False
      Tab(1).Control(26)=   "lblCaption(22)"
      Tab(1).Control(26).Enabled=   0   'False
      Tab(1).Control(27)=   "lblCaption(35)"
      Tab(1).Control(27).Enabled=   0   'False
      Tab(1).Control(28)=   "lblCaption(36)"
      Tab(1).Control(28).Enabled=   0   'False
      Tab(1).Control(29)=   "lblCaption(37)"
      Tab(1).Control(29).Enabled=   0   'False
      Tab(1).Control(30)=   "lblCaption(124)"
      Tab(1).Control(30).Enabled=   0   'False
      Tab(1).Control(31)=   "lblCaption(18)"
      Tab(1).Control(31).Enabled=   0   'False
      Tab(1).Control(32)=   "lblCaption(195)"
      Tab(1).Control(32).Enabled=   0   'False
      Tab(1).Control(33)=   "lblCaption(204)"
      Tab(1).Control(33).Enabled=   0   'False
      Tab(1).ControlCount=   34
      TabCaption(2)   =   "Amazon and Xbox"
      TabPicture(2)   =   "frmiTunesPackage.frx":008E
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "txtXboxLicensor"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "cmdXboxSeriesXML"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "cmdXboxSeasonXML"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "cmdXboxEpisodeXMLs"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).Control(4)=   "chkPackageUsed(1)"
      Tab(2).Control(4).Enabled=   0   'False
      Tab(2).Control(5)=   "txtAmazonSeasonProductionCompany"
      Tab(2).Control(5).Enabled=   0   'False
      Tab(2).Control(6)=   "txtAmazonSeasonNumber"
      Tab(2).Control(6).Enabled=   0   'False
      Tab(2).Control(7)=   "txtAmazonAudioClipID"
      Tab(2).Control(7).Enabled=   0   'False
      Tab(2).Control(8)=   "txtAmazonPosterClipID"
      Tab(2).Control(8).Enabled=   0   'False
      Tab(2).Control(9)=   "txtAmazonFullClipID"
      Tab(2).Control(9).Enabled=   0   'False
      Tab(2).Control(10)=   "txtAmazonRating"
      Tab(2).Control(10).Enabled=   0   'False
      Tab(2).Control(11)=   "txtAmazonRatingType"
      Tab(2).Control(11).Enabled=   0   'False
      Tab(2).Control(12)=   "txtAmazonGenre"
      Tab(2).Control(12).Enabled=   0   'False
      Tab(2).Control(13)=   "txtAmazonSequence"
      Tab(2).Control(13).Enabled=   0   'False
      Tab(2).Control(14)=   "cmdAmazonXML"
      Tab(2).Control(14).Enabled=   0   'False
      Tab(2).Control(15)=   "txtAmazonEpisodeLongSynopsis"
      Tab(2).Control(15).Enabled=   0   'False
      Tab(2).Control(16)=   "txtAmazonEpisodeShortSynopsis"
      Tab(2).Control(16).Enabled=   0   'False
      Tab(2).Control(17)=   "txtAmazonSeasonLongSynopsis"
      Tab(2).Control(17).Enabled=   0   'False
      Tab(2).Control(18)=   "txtAmazonSeasonShortSynopsis"
      Tab(2).Control(18).Enabled=   0   'False
      Tab(2).Control(19)=   "txtAmazonSeriesLongSynopsis"
      Tab(2).Control(19).Enabled=   0   'False
      Tab(2).Control(20)=   "txtAmazonSeriesShortSynopsis"
      Tab(2).Control(20).Enabled=   0   'False
      Tab(2).Control(21)=   "txtAmazonEpisodeTitle"
      Tab(2).Control(21).Enabled=   0   'False
      Tab(2).Control(22)=   "txtAmazonSeasonTitle"
      Tab(2).Control(22).Enabled=   0   'False
      Tab(2).Control(23)=   "txtAmazonSeriesName"
      Tab(2).Control(23).Enabled=   0   'False
      Tab(2).Control(24)=   "txtAmazonCopyrightYear"
      Tab(2).Control(24).Enabled=   0   'False
      Tab(2).Control(25)=   "txtAmazonCopyrightHolder"
      Tab(2).Control(25).Enabled=   0   'False
      Tab(2).Control(26)=   "txtAmazonSeasonID"
      Tab(2).Control(26).Enabled=   0   'False
      Tab(2).Control(27)=   "txtAmazonSeriesID"
      Tab(2).Control(27).Enabled=   0   'False
      Tab(2).Control(28)=   "datOriginalAirDate"
      Tab(2).Control(28).Enabled=   0   'False
      Tab(2).Control(29)=   "datSDAmazonOfferStart"
      Tab(2).Control(29).Enabled=   0   'False
      Tab(2).Control(30)=   "txtAmazonLanguage"
      Tab(2).Control(30).Enabled=   0   'False
      Tab(2).Control(31)=   "txtAmazonTerritory"
      Tab(2).Control(31).Enabled=   0   'False
      Tab(2).Control(32)=   "datHDVODAmazonOfferStart"
      Tab(2).Control(32).Enabled=   0   'False
      Tab(2).Control(33)=   "datSDVODAmazonOfferStart"
      Tab(2).Control(33).Enabled=   0   'False
      Tab(2).Control(34)=   "datHDAmazonOfferStart"
      Tab(2).Control(34).Enabled=   0   'False
      Tab(2).Control(35)=   "lblCaption(217)"
      Tab(2).Control(35).Enabled=   0   'False
      Tab(2).Control(36)=   "lblCaption(186)"
      Tab(2).Control(36).Enabled=   0   'False
      Tab(2).Control(37)=   "lblCaption(185)"
      Tab(2).Control(37).Enabled=   0   'False
      Tab(2).Control(38)=   "lblCaption(183)"
      Tab(2).Control(38).Enabled=   0   'False
      Tab(2).Control(39)=   "lblCaption(177)"
      Tab(2).Control(39).Enabled=   0   'False
      Tab(2).Control(40)=   "lblCaption(176)"
      Tab(2).Control(40).Enabled=   0   'False
      Tab(2).Control(41)=   "lblCaption(175)"
      Tab(2).Control(41).Enabled=   0   'False
      Tab(2).Control(42)=   "lblCaption(174)"
      Tab(2).Control(42).Enabled=   0   'False
      Tab(2).Control(43)=   "lblCaption(173)"
      Tab(2).Control(43).Enabled=   0   'False
      Tab(2).Control(44)=   "lblCaption(172)"
      Tab(2).Control(44).Enabled=   0   'False
      Tab(2).Control(45)=   "lblCaption(171)"
      Tab(2).Control(45).Enabled=   0   'False
      Tab(2).Control(46)=   "lblCaption(170)"
      Tab(2).Control(46).Enabled=   0   'False
      Tab(2).Control(47)=   "lblCaption(169)"
      Tab(2).Control(47).Enabled=   0   'False
      Tab(2).Control(48)=   "lblCaption(168)"
      Tab(2).Control(48).Enabled=   0   'False
      Tab(2).Control(49)=   "lblCaption(166)"
      Tab(2).Control(49).Enabled=   0   'False
      Tab(2).Control(50)=   "lblCaption(165)"
      Tab(2).Control(50).Enabled=   0   'False
      Tab(2).Control(51)=   "lblCaption(164)"
      Tab(2).Control(51).Enabled=   0   'False
      Tab(2).Control(52)=   "lblCaption(163)"
      Tab(2).Control(52).Enabled=   0   'False
      Tab(2).Control(53)=   "lblCaption(162)"
      Tab(2).Control(53).Enabled=   0   'False
      Tab(2).Control(54)=   "lblCaption(161)"
      Tab(2).Control(54).Enabled=   0   'False
      Tab(2).Control(55)=   "lblCaption(160)"
      Tab(2).Control(55).Enabled=   0   'False
      Tab(2).Control(56)=   "lblCaption(159)"
      Tab(2).Control(56).Enabled=   0   'False
      Tab(2).Control(57)=   "lblCaption(158)"
      Tab(2).Control(57).Enabled=   0   'False
      Tab(2).Control(58)=   "lblCaption(157)"
      Tab(2).Control(58).Enabled=   0   'False
      Tab(2).Control(59)=   "lblCaption(155)"
      Tab(2).Control(59).Enabled=   0   'False
      Tab(2).Control(60)=   "lblCaption(154)"
      Tab(2).Control(60).Enabled=   0   'False
      Tab(2).Control(61)=   "lblCaption(153)"
      Tab(2).Control(61).Enabled=   0   'False
      Tab(2).Control(62)=   "lblCaption(152)"
      Tab(2).Control(62).Enabled=   0   'False
      Tab(2).Control(63)=   "lblCaption(151)"
      Tab(2).Control(63).Enabled=   0   'False
      Tab(2).Control(64)=   "lblCaption(240)"
      Tab(2).Control(64).Enabled=   0   'False
      Tab(2).ControlCount=   65
      Begin VB.ComboBox cmbSubType 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         ItemData        =   "frmiTunesPackage.frx":00AA
         Left            =   7020
         List            =   "frmiTunesPackage.frx":00AC
         TabIndex        =   563
         ToolTipText     =   "The Version for the Tape"
         Top             =   900
         Width           =   1515
      End
      Begin VB.TextBox txtXboxLicensor 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72840
         TabIndex        =   561
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   1260
         Width           =   3855
      End
      Begin VB.CommandButton cmdXboxSeriesXML 
         Caption         =   "Make Xbox Series XML"
         Height          =   375
         Left            =   -55920
         TabIndex        =   560
         Top             =   1440
         Width           =   2115
      End
      Begin VB.CommandButton cmdXboxSeasonXML 
         Caption         =   "Make Xbox Season XML"
         Height          =   375
         Left            =   -55920
         TabIndex        =   559
         Top             =   1860
         Width           =   2115
      End
      Begin VB.CommandButton cmdXboxEpisodeXMLs 
         Caption         =   "Make Xbox Episode XMLs"
         Height          =   375
         Left            =   -55920
         TabIndex        =   558
         Top             =   2280
         Width           =   2115
      End
      Begin VB.CheckBox chkPackageUsed 
         Alignment       =   1  'Right Justify
         Caption         =   "Package Used"
         Height          =   255
         Index           =   2
         Left            =   -74760
         TabIndex        =   549
         Top             =   9300
         Width           =   1755
      End
      Begin VB.CheckBox chkPackageUsed 
         Alignment       =   1  'Right Justify
         Caption         =   "Package Used"
         Height          =   255
         Index           =   1
         Left            =   -55920
         TabIndex        =   537
         Top             =   660
         Width           =   1755
      End
      Begin VB.TextBox txtAmazonSeasonProductionCompany 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72840
         TabIndex        =   534
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   3060
         Width           =   3855
      End
      Begin VB.TextBox txtAmazonSeasonNumber 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72840
         TabIndex        =   529
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   4140
         Width           =   3855
      End
      Begin VB.TextBox txtAmazonAudioClipID 
         Height          =   315
         Left            =   -54540
         TabIndex        =   527
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   3540
         Width           =   855
      End
      Begin VB.TextBox txtAmazonPosterClipID 
         Height          =   315
         Left            =   -54540
         TabIndex        =   524
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   3180
         Width           =   855
      End
      Begin VB.TextBox txtAmazonFullClipID 
         Height          =   315
         Left            =   -54540
         TabIndex        =   522
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   2820
         Width           =   855
      End
      Begin VB.TextBox txtAmazonRating 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72840
         TabIndex        =   514
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   5940
         Width           =   3855
      End
      Begin VB.TextBox txtAmazonRatingType 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72840
         TabIndex        =   513
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   5580
         Width           =   3855
      End
      Begin VB.TextBox txtAmazonGenre 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72840
         TabIndex        =   512
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   5220
         Width           =   3855
      End
      Begin VB.TextBox txtAmazonSequence 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72840
         TabIndex        =   510
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   4860
         Width           =   3855
      End
      Begin VB.CommandButton cmdAmazonXML 
         Caption         =   "Make Amazon XML"
         Height          =   375
         Left            =   -55920
         TabIndex        =   509
         Top             =   1020
         Width           =   2115
      End
      Begin VB.TextBox txtAmazonEpisodeLongSynopsis 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   -65280
         MultiLine       =   -1  'True
         TabIndex        =   503
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   7440
         Width           =   7635
      End
      Begin VB.TextBox txtAmazonEpisodeShortSynopsis 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   -65280
         MultiLine       =   -1  'True
         TabIndex        =   502
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   6060
         Width           =   7635
      End
      Begin VB.TextBox txtAmazonSeasonLongSynopsis 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   -65280
         MultiLine       =   -1  'True
         TabIndex        =   501
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   4680
         Width           =   7635
      End
      Begin VB.TextBox txtAmazonSeasonShortSynopsis 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   -65280
         MultiLine       =   -1  'True
         TabIndex        =   500
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   3300
         Width           =   7635
      End
      Begin VB.TextBox txtAmazonSeriesLongSynopsis 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   -65280
         MultiLine       =   -1  'True
         TabIndex        =   499
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   1920
         Width           =   7635
      End
      Begin VB.TextBox txtAmazonSeriesShortSynopsis 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   -65280
         MultiLine       =   -1  'True
         TabIndex        =   497
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   540
         Width           =   7635
      End
      Begin VB.TextBox txtAmazonEpisodeTitle 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72840
         TabIndex        =   493
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   4500
         Width           =   3855
      End
      Begin VB.TextBox txtAmazonSeasonTitle 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72840
         TabIndex        =   492
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   3780
         Width           =   3855
      End
      Begin VB.TextBox txtAmazonSeriesName 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72840
         TabIndex        =   491
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   3420
         Width           =   3855
      End
      Begin VB.TextBox txtAmazonCopyrightYear 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72840
         TabIndex        =   488
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   2700
         Width           =   3855
      End
      Begin VB.TextBox txtAmazonCopyrightHolder 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72840
         TabIndex        =   487
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   2340
         Width           =   3855
      End
      Begin VB.TextBox txtAmazonSeasonID 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72840
         TabIndex        =   485
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   1980
         Width           =   3855
      End
      Begin VB.TextBox txtAmazonSeriesID 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -72840
         TabIndex        =   483
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   1620
         Width           =   3855
      End
      Begin VB.ComboBox cmbAU 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "frmiTunesPackage.frx":00AE
         Left            =   2280
         List            =   "frmiTunesPackage.frx":00B0
         TabIndex        =   437
         ToolTipText     =   "The Version for the Tape"
         Top             =   6900
         Width           =   1515
      End
      Begin VB.ComboBox cmbCA 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "frmiTunesPackage.frx":00B2
         Left            =   2280
         List            =   "frmiTunesPackage.frx":00B4
         TabIndex        =   436
         ToolTipText     =   "The Version for the Tape"
         Top             =   7260
         Width           =   1515
      End
      Begin VB.ComboBox cmbDE 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "frmiTunesPackage.frx":00B6
         Left            =   2280
         List            =   "frmiTunesPackage.frx":00B8
         TabIndex        =   435
         ToolTipText     =   "The Version for the Tape"
         Top             =   7620
         Width           =   1515
      End
      Begin VB.ComboBox cmbFR 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "frmiTunesPackage.frx":00BA
         Left            =   2280
         List            =   "frmiTunesPackage.frx":00BC
         TabIndex        =   434
         ToolTipText     =   "The Version for the Tape"
         Top             =   7980
         Width           =   1515
      End
      Begin VB.ComboBox cmbUK 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "frmiTunesPackage.frx":00BE
         Left            =   6060
         List            =   "frmiTunesPackage.frx":00C0
         TabIndex        =   433
         ToolTipText     =   "The Version for the Tape"
         Top             =   6900
         Width           =   1515
      End
      Begin VB.ComboBox cmbUS 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "frmiTunesPackage.frx":00C2
         Left            =   6060
         List            =   "frmiTunesPackage.frx":00C4
         TabIndex        =   432
         ToolTipText     =   "The Version for the Tape"
         Top             =   7260
         Width           =   1515
      End
      Begin VB.ComboBox cmbJP 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "frmiTunesPackage.frx":00C6
         Left            =   6060
         List            =   "frmiTunesPackage.frx":00C8
         TabIndex        =   431
         ToolTipText     =   "The Version for the Tape"
         Top             =   7620
         Width           =   1515
      End
      Begin VB.TextBox txtiTunesEpProdNo 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   430
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   1980
         Width           =   3855
      End
      Begin VB.TextBox txtiTunesContainerID 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   429
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   2340
         Width           =   3855
      End
      Begin VB.TextBox txtiTunesContainerPosition 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   428
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   2700
         Width           =   3855
      End
      Begin VB.TextBox txtiTunesLongDescription 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   2280
         MultiLine       =   -1  'True
         TabIndex        =   427
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   3780
         Width           =   7635
      End
      Begin VB.TextBox txtiTunesPreviewStartTime 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   426
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   6540
         Width           =   3855
      End
      Begin VB.ComboBox cmbSubType 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         ItemData        =   "frmiTunesPackage.frx":00CA
         Left            =   -73200
         List            =   "frmiTunesPackage.frx":00CC
         TabIndex        =   425
         ToolTipText     =   "The Version for the Tape"
         Top             =   480
         Width           =   1515
      End
      Begin VB.CommandButton cmdMakeXMLFiles 
         Caption         =   "Make Full Metadata.xml File"
         Height          =   315
         Left            =   -66420
         TabIndex        =   215
         Top             =   420
         Width           =   3315
      End
      Begin VB.TextBox txtProductionCompany 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73200
         TabIndex        =   214
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   1200
         Width           =   3255
      End
      Begin VB.TextBox txtUPC 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73200
         TabIndex        =   213
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   1560
         Width           =   3255
      End
      Begin VB.TextBox txtAMGVideoID 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73200
         TabIndex        =   212
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   1920
         Width           =   3255
      End
      Begin VB.CommandButton cmdMakeMetadataOnlyXML 
         Caption         =   "Make Metadata Only XML File"
         Height          =   315
         Left            =   -66420
         TabIndex        =   210
         Top             =   780
         Width           =   3315
      End
      Begin VB.CommandButton cmdMakeFullAssetOnlyXML 
         Caption         =   "Make Full Asset Only New Delivery XML File"
         Height          =   315
         Left            =   -66420
         TabIndex        =   209
         Top             =   1140
         Width           =   3315
      End
      Begin VB.CommandButton cmdMakePreviewAssetOnlyUpdateXML 
         Caption         =   "Make Preview Asset Only Update XML File"
         Height          =   315
         Left            =   -66420
         TabIndex        =   208
         Top             =   1860
         Width           =   3315
      End
      Begin VB.CheckBox chkPackageUsed 
         Alignment       =   1  'Right Justify
         Caption         =   "Package Used"
         Height          =   255
         Index           =   0
         Left            =   6300
         TabIndex        =   206
         Top             =   1320
         Width           =   1815
      End
      Begin VB.TextBox txtCropTop 
         Height          =   315
         Left            =   2280
         TabIndex        =   108
         Top             =   8340
         Width           =   1275
      End
      Begin VB.TextBox txtCropBottom 
         Height          =   315
         Left            =   2280
         TabIndex        =   107
         Top             =   8700
         Width           =   1275
      End
      Begin VB.TextBox txtCropLeft 
         Height          =   315
         Left            =   2280
         TabIndex        =   106
         Top             =   9060
         Width           =   1275
      End
      Begin VB.TextBox txtCropRight 
         Height          =   315
         Left            =   2280
         TabIndex        =   105
         Top             =   9420
         Width           =   1275
      End
      Begin VB.TextBox txtiTunesTechComments 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   2280
         MultiLine       =   -1  'True
         TabIndex        =   104
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   5160
         Width           =   7635
      End
      Begin VB.TextBox txtTitle 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   103
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   900
         Width           =   3855
      End
      Begin VB.TextBox txtiTunesCopyrightLine 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   102
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   3420
         Width           =   3855
      End
      Begin VB.CheckBox chkMultiLanguage 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -67410
         TabIndex        =   48
         Top             =   2040
         Width           =   255
      End
      Begin VB.CommandButton cmdMakeFeatureAssetOnlyUpdateXML 
         Caption         =   "Make Feature Asset Only Update XML File"
         Height          =   315
         Left            =   -66420
         TabIndex        =   47
         Top             =   1500
         Width           =   3315
      End
      Begin VB.ComboBox cmbiTunesProvider 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   -70740
         TabIndex        =   46
         ToolTipText     =   "The Version for the Tape"
         Top             =   480
         Width           =   3855
      End
      Begin VB.ComboBox cmbiTunesProvider 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   7020
         TabIndex        =   45
         ToolTipText     =   "The Version for the Tape"
         Top             =   540
         Width           =   3855
      End
      Begin VB.TextBox txtStudioReleaseTitle 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   -73200
         TabIndex        =   44
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   840
         Width           =   3255
      End
      Begin VB.TextBox txtComments 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Left            =   -73200
         MultiLine       =   -1  'True
         TabIndex        =   43
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   11400
         Width           =   21915
      End
      Begin VB.CommandButton cmdMakeAssetlessPreorderXML 
         Caption         =   "Make Assetless Preorder XML File"
         Height          =   315
         Left            =   -69780
         TabIndex        =   42
         Top             =   1140
         Width           =   3315
      End
      Begin VB.CommandButton cmdMakeAssetWithChapters 
         Caption         =   "Make Feature with Chapters XML File"
         Height          =   315
         Left            =   -69780
         TabIndex        =   41
         Top             =   1500
         Width           =   3315
      End
      Begin VB.CommandButton cmdReadExistingiTunesTVXML 
         Caption         =   "Read Existing iTunes TV XML (assumes Disney if cannot identify Provider)"
         Height          =   675
         Left            =   14880
         TabIndex        =   40
         Top             =   960
         Width           =   3255
      End
      Begin VB.TextBox txtStudio_Release_Title 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   39
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   1260
         Width           =   3855
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   2895
         Left            =   -74820
         TabIndex        =   49
         Top             =   3300
         Width           =   11775
         _ExtentX        =   20770
         _ExtentY        =   5106
         _Version        =   393216
         Tabs            =   4
         Tab             =   3
         TabsPerRow      =   4
         TabHeight       =   520
         TabCaption(0)   =   "Main Language"
         TabPicture(0)   =   "frmiTunesPackage.frx":00CE
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "txtSynopsis(0)"
         Tab(0).Control(1)=   "txtFilmTitle(0)"
         Tab(0).Control(2)=   "txtFilmCopyrightLine(0)"
         Tab(0).Control(3)=   "cmdUTF8Decode(0)"
         Tab(0).Control(4)=   "datTheatricalReleaseDate(0)"
         Tab(0).Control(5)=   "cmbFilmMetadataLanguage(0)"
         Tab(0).Control(6)=   "cmbFilmTerritory(0)"
         Tab(0).Control(7)=   "lblCaption(21)"
         Tab(0).Control(8)=   "lblCaption(20)"
         Tab(0).Control(9)=   "lblCaption(125)"
         Tab(0).Control(10)=   "lblCaption(126)"
         Tab(0).Control(11)=   "lblCaption(127)"
         Tab(0).Control(12)=   "lblCaption(128)"
         Tab(0).ControlCount=   13
         TabCaption(1)   =   "Language 2"
         TabPicture(1)   =   "frmiTunesPackage.frx":00EA
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lblCaption(129)"
         Tab(1).Control(1)=   "lblCaption(130)"
         Tab(1).Control(2)=   "lblCaption(131)"
         Tab(1).Control(3)=   "lblCaption(132)"
         Tab(1).Control(4)=   "lblCaption(133)"
         Tab(1).Control(5)=   "lblCaption(134)"
         Tab(1).Control(6)=   "cmbFilmTerritory(1)"
         Tab(1).Control(7)=   "cmbFilmMetadataLanguage(1)"
         Tab(1).Control(8)=   "datTheatricalReleaseDate(1)"
         Tab(1).Control(9)=   "txtSynopsis(1)"
         Tab(1).Control(10)=   "txtFilmTitle(1)"
         Tab(1).Control(11)=   "txtFilmCopyrightLine(1)"
         Tab(1).Control(12)=   "cmdUTF8Decode(1)"
         Tab(1).ControlCount=   13
         TabCaption(2)   =   "Language 3"
         TabPicture(2)   =   "frmiTunesPackage.frx":0106
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "cmdUTF8Decode(2)"
         Tab(2).Control(1)=   "txtFilmCopyrightLine(2)"
         Tab(2).Control(2)=   "txtFilmTitle(2)"
         Tab(2).Control(3)=   "txtSynopsis(2)"
         Tab(2).Control(4)=   "datTheatricalReleaseDate(2)"
         Tab(2).Control(5)=   "cmbFilmMetadataLanguage(2)"
         Tab(2).Control(6)=   "cmbFilmTerritory(2)"
         Tab(2).Control(7)=   "lblCaption(140)"
         Tab(2).Control(8)=   "lblCaption(139)"
         Tab(2).Control(9)=   "lblCaption(138)"
         Tab(2).Control(10)=   "lblCaption(137)"
         Tab(2).Control(11)=   "lblCaption(136)"
         Tab(2).Control(12)=   "lblCaption(135)"
         Tab(2).ControlCount=   13
         TabCaption(3)   =   "Language 4"
         TabPicture(3)   =   "frmiTunesPackage.frx":0122
         Tab(3).ControlEnabled=   -1  'True
         Tab(3).Control(0)=   "lblCaption(146)"
         Tab(3).Control(0).Enabled=   0   'False
         Tab(3).Control(1)=   "lblCaption(145)"
         Tab(3).Control(1).Enabled=   0   'False
         Tab(3).Control(2)=   "lblCaption(144)"
         Tab(3).Control(2).Enabled=   0   'False
         Tab(3).Control(3)=   "lblCaption(143)"
         Tab(3).Control(3).Enabled=   0   'False
         Tab(3).Control(4)=   "lblCaption(142)"
         Tab(3).Control(4).Enabled=   0   'False
         Tab(3).Control(5)=   "lblCaption(141)"
         Tab(3).Control(5).Enabled=   0   'False
         Tab(3).Control(6)=   "cmbFilmTerritory(3)"
         Tab(3).Control(6).Enabled=   0   'False
         Tab(3).Control(7)=   "cmbFilmMetadataLanguage(3)"
         Tab(3).Control(7).Enabled=   0   'False
         Tab(3).Control(8)=   "datTheatricalReleaseDate(3)"
         Tab(3).Control(8).Enabled=   0   'False
         Tab(3).Control(9)=   "cmdUTF8Decode(3)"
         Tab(3).Control(9).Enabled=   0   'False
         Tab(3).Control(10)=   "txtFilmCopyrightLine(3)"
         Tab(3).Control(10).Enabled=   0   'False
         Tab(3).Control(11)=   "txtFilmTitle(3)"
         Tab(3).Control(11).Enabled=   0   'False
         Tab(3).Control(12)=   "txtSynopsis(3)"
         Tab(3).Control(12).Enabled=   0   'False
         Tab(3).ControlCount=   13
         Begin VB.TextBox txtSynopsis 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   975
            Index           =   0
            Left            =   -73260
            MultiLine       =   -1  'True
            TabIndex        =   65
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   1380
            Width           =   9915
         End
         Begin VB.TextBox txtFilmTitle 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   -73260
            TabIndex        =   64
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   660
            Width           =   3855
         End
         Begin VB.TextBox txtFilmCopyrightLine 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   -73260
            TabIndex        =   63
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   2400
            Width           =   3855
         End
         Begin VB.TextBox txtSynopsis 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   975
            Index           =   1
            Left            =   -73260
            MultiLine       =   -1  'True
            TabIndex        =   62
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   1380
            Width           =   9915
         End
         Begin VB.TextBox txtFilmTitle 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   -73260
            TabIndex        =   61
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   660
            Width           =   3855
         End
         Begin VB.TextBox txtFilmCopyrightLine 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   -73260
            TabIndex        =   60
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   2400
            Width           =   3855
         End
         Begin VB.TextBox txtSynopsis 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   975
            Index           =   2
            Left            =   -73260
            MultiLine       =   -1  'True
            TabIndex        =   59
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   1380
            Width           =   9855
         End
         Begin VB.TextBox txtFilmTitle 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   -73260
            TabIndex        =   58
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   660
            Width           =   3855
         End
         Begin VB.TextBox txtFilmCopyrightLine 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   -73260
            TabIndex        =   57
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   2400
            Width           =   3855
         End
         Begin VB.TextBox txtSynopsis 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   975
            Index           =   3
            Left            =   1740
            MultiLine       =   -1  'True
            TabIndex        =   56
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   1380
            Width           =   9915
         End
         Begin VB.TextBox txtFilmTitle 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   1740
            TabIndex        =   55
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   660
            Width           =   3855
         End
         Begin VB.TextBox txtFilmCopyrightLine 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   1740
            TabIndex        =   54
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   2400
            Width           =   3855
         End
         Begin VB.CommandButton cmdUTF8Decode 
            Caption         =   "Decode Foreign Text"
            Height          =   555
            Index           =   0
            Left            =   -74820
            TabIndex        =   53
            Top             =   1740
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.CommandButton cmdUTF8Decode 
            Caption         =   "Decode Foreign Text"
            Height          =   555
            Index           =   1
            Left            =   -74820
            TabIndex        =   52
            Top             =   1740
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.CommandButton cmdUTF8Decode 
            Caption         =   "Decode Foreign Text"
            Height          =   555
            Index           =   2
            Left            =   -74820
            TabIndex        =   51
            Top             =   1740
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.CommandButton cmdUTF8Decode 
            Caption         =   "Decode Foreign Text"
            Height          =   555
            Index           =   3
            Left            =   180
            TabIndex        =   50
            Top             =   1740
            Visible         =   0   'False
            Width           =   1455
         End
         Begin MSComCtl2.DTPicker datTheatricalReleaseDate 
            Height          =   315
            Index           =   0
            Left            =   -73260
            TabIndex        =   66
            Tag             =   "NOCLEAR"
            ToolTipText     =   "The date the tape was created"
            Top             =   1020
            Width           =   1515
            _ExtentX        =   2672
            _ExtentY        =   556
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   111935489
            CurrentDate     =   37870
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbFilmMetadataLanguage 
            Bindings        =   "frmiTunesPackage.frx":013E
            Height          =   315
            Index           =   0
            Left            =   -67200
            TabIndex        =   67
            ToolTipText     =   "The company this tape belongs to"
            Top             =   660
            Width           =   3855
            DataFieldList   =   "description"
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "description"
            Columns(0).Name =   "description"
            Columns(0).DataField=   "description"
            Columns(0).FieldLen=   256
            Columns(1).Width=   8043
            Columns(1).Caption=   "information"
            Columns(1).Name =   "information"
            Columns(1).DataField=   "information"
            Columns(1).FieldLen=   256
            _ExtentX        =   6800
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "description"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbFilmTerritory 
            Bindings        =   "frmiTunesPackage.frx":0160
            Height          =   315
            Index           =   0
            Left            =   -67200
            TabIndex        =   68
            ToolTipText     =   "The company this tape belongs to"
            Top             =   1020
            Width           =   675
            DataFieldList   =   "iso2acode"
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4048
            Columns(0).Caption=   "country"
            Columns(0).Name =   "country"
            Columns(0).DataField=   "country"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "iso2acode"
            Columns(1).Name =   "iso2acode"
            Columns(1).DataField=   "iso2acode"
            Columns(1).FieldLen=   256
            _ExtentX        =   1191
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "iso2acode"
         End
         Begin MSComCtl2.DTPicker datTheatricalReleaseDate 
            Height          =   315
            Index           =   1
            Left            =   -73260
            TabIndex        =   69
            Tag             =   "NOCLEAR"
            ToolTipText     =   "The date the tape was created"
            Top             =   1020
            Width           =   1515
            _ExtentX        =   2672
            _ExtentY        =   556
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   111935489
            CurrentDate     =   37870
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbFilmMetadataLanguage 
            Bindings        =   "frmiTunesPackage.frx":0177
            Height          =   315
            Index           =   1
            Left            =   -67200
            TabIndex        =   70
            ToolTipText     =   "The company this tape belongs to"
            Top             =   660
            Width           =   3855
            DataFieldList   =   "description"
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "description"
            Columns(0).Name =   "description"
            Columns(0).DataField=   "description"
            Columns(0).FieldLen=   256
            Columns(1).Width=   8043
            Columns(1).Caption=   "information"
            Columns(1).Name =   "information"
            Columns(1).DataField=   "information"
            Columns(1).FieldLen=   256
            _ExtentX        =   6800
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "description"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbFilmTerritory 
            Bindings        =   "frmiTunesPackage.frx":0199
            Height          =   315
            Index           =   1
            Left            =   -67200
            TabIndex        =   71
            ToolTipText     =   "The company this tape belongs to"
            Top             =   1020
            Width           =   675
            DataFieldList   =   "iso2acode"
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4048
            Columns(0).Caption=   "country"
            Columns(0).Name =   "country"
            Columns(0).DataField=   "country"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "iso2acode"
            Columns(1).Name =   "iso2acode"
            Columns(1).DataField=   "iso2acode"
            Columns(1).FieldLen=   256
            _ExtentX        =   1191
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "iso2acode"
         End
         Begin MSComCtl2.DTPicker datTheatricalReleaseDate 
            Height          =   315
            Index           =   2
            Left            =   -73260
            TabIndex        =   72
            Tag             =   "NOCLEAR"
            ToolTipText     =   "The date the tape was created"
            Top             =   1020
            Width           =   1515
            _ExtentX        =   2672
            _ExtentY        =   556
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   111935489
            CurrentDate     =   37870
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbFilmMetadataLanguage 
            Bindings        =   "frmiTunesPackage.frx":01B0
            Height          =   315
            Index           =   2
            Left            =   -67200
            TabIndex        =   73
            ToolTipText     =   "The company this tape belongs to"
            Top             =   660
            Width           =   3855
            DataFieldList   =   "description"
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "description"
            Columns(0).Name =   "description"
            Columns(0).DataField=   "description"
            Columns(0).FieldLen=   256
            Columns(1).Width=   8043
            Columns(1).Caption=   "information"
            Columns(1).Name =   "information"
            Columns(1).DataField=   "information"
            Columns(1).FieldLen=   256
            _ExtentX        =   6800
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "description"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbFilmTerritory 
            Bindings        =   "frmiTunesPackage.frx":01D2
            Height          =   315
            Index           =   2
            Left            =   -67200
            TabIndex        =   74
            ToolTipText     =   "The company this tape belongs to"
            Top             =   1020
            Width           =   675
            DataFieldList   =   "iso2acode"
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4048
            Columns(0).Caption=   "country"
            Columns(0).Name =   "country"
            Columns(0).DataField=   "country"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "iso2acode"
            Columns(1).Name =   "iso2acode"
            Columns(1).DataField=   "iso2acode"
            Columns(1).FieldLen=   256
            _ExtentX        =   1191
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "iso2acode"
         End
         Begin MSComCtl2.DTPicker datTheatricalReleaseDate 
            Height          =   315
            Index           =   3
            Left            =   1740
            TabIndex        =   75
            Tag             =   "NOCLEAR"
            ToolTipText     =   "The date the tape was created"
            Top             =   1020
            Width           =   1515
            _ExtentX        =   2672
            _ExtentY        =   556
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   111935489
            CurrentDate     =   37870
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbFilmMetadataLanguage 
            Bindings        =   "frmiTunesPackage.frx":01E9
            Height          =   315
            Index           =   3
            Left            =   7800
            TabIndex        =   76
            ToolTipText     =   "The company this tape belongs to"
            Top             =   660
            Width           =   3855
            DataFieldList   =   "description"
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "description"
            Columns(0).Name =   "description"
            Columns(0).DataField=   "description"
            Columns(0).FieldLen=   256
            Columns(1).Width=   8043
            Columns(1).Caption=   "information"
            Columns(1).Name =   "information"
            Columns(1).DataField=   "information"
            Columns(1).FieldLen=   256
            _ExtentX        =   6800
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "description"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbFilmTerritory 
            Bindings        =   "frmiTunesPackage.frx":020B
            Height          =   315
            Index           =   3
            Left            =   7800
            TabIndex        =   77
            ToolTipText     =   "The company this tape belongs to"
            Top             =   1020
            Width           =   675
            DataFieldList   =   "iso2acode"
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4048
            Columns(0).Caption=   "country"
            Columns(0).Name =   "country"
            Columns(0).DataField=   "country"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "iso2acode"
            Columns(1).Name =   "iso2acode"
            Columns(1).DataField=   "iso2acode"
            Columns(1).FieldLen=   256
            _ExtentX        =   1191
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "iso2acode"
         End
         Begin VB.Label lblCaption 
            Caption         =   "Synopsis"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   21
            Left            =   -74820
            TabIndex        =   101
            Top             =   1440
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Release Date"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   20
            Left            =   -74820
            TabIndex        =   100
            Top             =   1080
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Film Title"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   125
            Left            =   -74820
            TabIndex        =   99
            Top             =   720
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Copyright Line"
            Height          =   255
            Index           =   126
            Left            =   -74820
            TabIndex        =   98
            Top             =   2460
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Metadata Language"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   127
            Left            =   -68940
            TabIndex        =   97
            Top             =   720
            Width           =   1575
         End
         Begin VB.Label lblCaption 
            Caption         =   "Territory"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   128
            Left            =   -68940
            TabIndex        =   96
            Top             =   1080
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Synopsis"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   129
            Left            =   -74820
            TabIndex        =   95
            Top             =   1440
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Release Date"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   130
            Left            =   -74820
            TabIndex        =   94
            Top             =   1080
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Film Title"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   131
            Left            =   -74820
            TabIndex        =   93
            Top             =   720
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Copyright Line"
            Height          =   255
            Index           =   132
            Left            =   -74820
            TabIndex        =   92
            Top             =   2460
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Metadata Language"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   133
            Left            =   -68940
            TabIndex        =   91
            Top             =   720
            Width           =   1575
         End
         Begin VB.Label lblCaption 
            Caption         =   "Territory"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   134
            Left            =   -68940
            TabIndex        =   90
            Top             =   1080
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Synopsis"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   135
            Left            =   -74820
            TabIndex        =   89
            Top             =   1440
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Release Date"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   136
            Left            =   -74820
            TabIndex        =   88
            Top             =   1080
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Film Title"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   137
            Left            =   -74820
            TabIndex        =   87
            Top             =   720
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Copyright Line"
            Height          =   255
            Index           =   138
            Left            =   -74820
            TabIndex        =   86
            Top             =   2460
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Metadata Language"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   139
            Left            =   -68940
            TabIndex        =   85
            Top             =   720
            Width           =   1575
         End
         Begin VB.Label lblCaption 
            Caption         =   "Territory"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   140
            Left            =   -68940
            TabIndex        =   84
            Top             =   1080
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Synopsis"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   141
            Left            =   180
            TabIndex        =   83
            Top             =   1440
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Release Date"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   142
            Left            =   180
            TabIndex        =   82
            Top             =   1080
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Film Title"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   143
            Left            =   180
            TabIndex        =   81
            Top             =   720
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Copyright Line"
            Height          =   255
            Index           =   144
            Left            =   180
            TabIndex        =   80
            Top             =   2460
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Metadata Language"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   145
            Left            =   6060
            TabIndex        =   79
            Top             =   720
            Width           =   1575
         End
         Begin VB.Label lblCaption 
            Caption         =   "Territory"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   146
            Left            =   6060
            TabIndex        =   78
            Top             =   1080
            Width           =   675
         End
      End
      Begin TabDlg.SSTab qb 
         Height          =   2835
         Left            =   -74820
         TabIndex        =   109
         Top             =   6300
         Width           =   8325
         _ExtentX        =   14684
         _ExtentY        =   5001
         _Version        =   393216
         Tabs            =   4
         Tab             =   3
         TabsPerRow      =   4
         TabHeight       =   520
         TabCaption(0)   =   "Ratings Page 1"
         TabPicture(0)   =   "frmiTunesPackage.frx":0222
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "lblCaption(15)"
         Tab(0).Control(1)=   "lblCaption(3)"
         Tab(0).Control(2)=   "lblCaption(46)"
         Tab(0).Control(3)=   "lblCaption(48)"
         Tab(0).Control(4)=   "lblCaption(49)"
         Tab(0).Control(5)=   "lblCaption(50)"
         Tab(0).Control(6)=   "lblCaption(59)"
         Tab(0).Control(7)=   "lblCaption(60)"
         Tab(0).Control(8)=   "lblCaption(82)"
         Tab(0).Control(9)=   "lblCaption(86)"
         Tab(0).Control(10)=   "lblCaption(14)"
         Tab(0).Control(11)=   "lblCaption(4)"
         Tab(0).Control(12)=   "txtUSreason"
         Tab(0).Control(13)=   "cmbUSfilm"
         Tab(0).Control(14)=   "txtDENreason"
         Tab(0).Control(15)=   "cmbDENfilm"
         Tab(0).Control(16)=   "txtFIreason"
         Tab(0).Control(17)=   "cmbFIfilm"
         Tab(0).Control(18)=   "txtNOreason"
         Tab(0).Control(19)=   "cmbNOfilm"
         Tab(0).Control(20)=   "txtSEreason"
         Tab(0).Control(21)=   "cmbSEfilm"
         Tab(0).Control(22)=   "txtUKreason"
         Tab(0).Control(23)=   "cmbUKfilm"
         Tab(0).ControlCount=   24
         TabCaption(1)   =   "Ratings Page 2"
         TabPicture(1)   =   "frmiTunesPackage.frx":023E
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lblCaption(56)"
         Tab(1).Control(1)=   "lblCaption(55)"
         Tab(1).Control(2)=   "lblCaption(54)"
         Tab(1).Control(3)=   "lblCaption(53)"
         Tab(1).Control(4)=   "lblCaption(52)"
         Tab(1).Control(5)=   "lblCaption(51)"
         Tab(1).Control(6)=   "lblCaption(5)"
         Tab(1).Control(7)=   "lblCaption(13)"
         Tab(1).Control(8)=   "lblCaption(6)"
         Tab(1).Control(9)=   "lblCaption(12)"
         Tab(1).Control(10)=   "lblCaption(2)"
         Tab(1).Control(11)=   "lblCaption(16)"
         Tab(1).Control(12)=   "cmbITfilm"
         Tab(1).Control(13)=   "txtITreason"
         Tab(1).Control(14)=   "cmbIEfilm"
         Tab(1).Control(15)=   "txtIEreason"
         Tab(1).Control(16)=   "cmbGRfilm"
         Tab(1).Control(17)=   "txtGRreason"
         Tab(1).Control(18)=   "cmbFRfilm"
         Tab(1).Control(19)=   "txtFRreason"
         Tab(1).Control(20)=   "cmbDEfilm"
         Tab(1).Control(21)=   "txtDEreason"
         Tab(1).Control(22)=   "cmbJPfilm"
         Tab(1).Control(23)=   "txtJPreason"
         Tab(1).ControlCount=   24
         TabCaption(2)   =   "Ratings Page 3"
         TabPicture(2)   =   "frmiTunesPackage.frx":025A
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "lblCaption(43)"
         Tab(2).Control(1)=   "lblCaption(45)"
         Tab(2).Control(2)=   "lblCaption(41)"
         Tab(2).Control(3)=   "lblCaption(42)"
         Tab(2).Control(4)=   "lblCaption(63)"
         Tab(2).Control(5)=   "lblCaption(64)"
         Tab(2).Control(6)=   "lblCaption(75)"
         Tab(2).Control(7)=   "lblCaption(74)"
         Tab(2).Control(8)=   "lblCaption(62)"
         Tab(2).Control(9)=   "lblCaption(61)"
         Tab(2).Control(10)=   "lblCaption(9)"
         Tab(2).Control(11)=   "lblCaption(17)"
         Tab(2).Control(12)=   "txtAUSreason"
         Tab(2).Control(13)=   "cmbAUSfilm"
         Tab(2).Control(14)=   "txtBEreason"
         Tab(2).Control(15)=   "cmbBEfilm"
         Tab(2).Control(16)=   "txtMXreason"
         Tab(2).Control(17)=   "cmbMXfilm"
         Tab(2).Control(18)=   "cmbLUfilm"
         Tab(2).Control(19)=   "txtLUreason"
         Tab(2).Control(20)=   "cmbNLfilm"
         Tab(2).Control(21)=   "txtNLreason"
         Tab(2).Control(22)=   "cmbNZfilm"
         Tab(2).Control(23)=   "txtNZreason"
         Tab(2).ControlCount=   24
         TabCaption(3)   =   "Ratings Page 4"
         TabPicture(3)   =   "frmiTunesPackage.frx":0276
         Tab(3).ControlEnabled=   -1  'True
         Tab(3).Control(0)=   "lblCaption(10)"
         Tab(3).Control(0).Enabled=   0   'False
         Tab(3).Control(1)=   "lblCaption(8)"
         Tab(3).Control(1).Enabled=   0   'False
         Tab(3).Control(2)=   "lblCaption(11)"
         Tab(3).Control(2).Enabled=   0   'False
         Tab(3).Control(3)=   "lblCaption(7)"
         Tab(3).Control(3).Enabled=   0   'False
         Tab(3).Control(4)=   "lblCaption(57)"
         Tab(3).Control(4).Enabled=   0   'False
         Tab(3).Control(5)=   "lblCaption(58)"
         Tab(3).Control(5).Enabled=   0   'False
         Tab(3).Control(6)=   "lblCaption(90)"
         Tab(3).Control(6).Enabled=   0   'False
         Tab(3).Control(7)=   "lblCaption(89)"
         Tab(3).Control(7).Enabled=   0   'False
         Tab(3).Control(8)=   "lblCaption(88)"
         Tab(3).Control(8).Enabled=   0   'False
         Tab(3).Control(9)=   "lblCaption(87)"
         Tab(3).Control(9).Enabled=   0   'False
         Tab(3).Control(10)=   "lblCaption(81)"
         Tab(3).Control(10).Enabled=   0   'False
         Tab(3).Control(11)=   "lblCaption(77)"
         Tab(3).Control(11).Enabled=   0   'False
         Tab(3).Control(12)=   "txtAUreason"
         Tab(3).Control(12).Enabled=   0   'False
         Tab(3).Control(13)=   "cmbAUfilm"
         Tab(3).Control(13).Enabled=   0   'False
         Tab(3).Control(14)=   "txtCAreason"
         Tab(3).Control(14).Enabled=   0   'False
         Tab(3).Control(15)=   "cmbCAfilm"
         Tab(3).Control(15).Enabled=   0   'False
         Tab(3).Control(16)=   "txtPTreason"
         Tab(3).Control(16).Enabled=   0   'False
         Tab(3).Control(17)=   "cmbPTfilm"
         Tab(3).Control(17).Enabled=   0   'False
         Tab(3).Control(18)=   "cmbQBfilm"
         Tab(3).Control(18).Enabled=   0   'False
         Tab(3).Control(19)=   "txtQBreason"
         Tab(3).Control(19).Enabled=   0   'False
         Tab(3).Control(20)=   "cmbESfilm"
         Tab(3).Control(20).Enabled=   0   'False
         Tab(3).Control(21)=   "txtESreason"
         Tab(3).Control(21).Enabled=   0   'False
         Tab(3).Control(22)=   "cmbCHfilm"
         Tab(3).Control(22).Enabled=   0   'False
         Tab(3).Control(23)=   "txtCHreason"
         Tab(3).Control(23).Enabled=   0   'False
         Tab(3).ControlCount=   24
         Begin VB.TextBox txtJPreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   157
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   2400
            Width           =   3855
         End
         Begin VB.ComboBox cmbJPfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":0292
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":0294
            TabIndex        =   156
            ToolTipText     =   "The Version for the Tape"
            Top             =   2400
            Width           =   1515
         End
         Begin VB.TextBox txtDEreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   155
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   960
            Width           =   3855
         End
         Begin VB.ComboBox cmbDEfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":0296
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":0298
            TabIndex        =   154
            ToolTipText     =   "The Version for the Tape"
            Top             =   960
            Width           =   1515
         End
         Begin VB.TextBox txtFRreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   153
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   600
            Width           =   3855
         End
         Begin VB.ComboBox cmbFRfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":029A
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":029C
            TabIndex        =   152
            ToolTipText     =   "The Version for the Tape"
            Top             =   600
            Width           =   1515
         End
         Begin VB.TextBox txtNZreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   151
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   1680
            Width           =   3855
         End
         Begin VB.ComboBox cmbNZfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":029E
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":02A0
            TabIndex        =   150
            ToolTipText     =   "The Version for the Tape"
            Top             =   1680
            Width           =   1515
         End
         Begin VB.TextBox txtGRreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   149
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   1320
            Width           =   3855
         End
         Begin VB.ComboBox cmbGRfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02A2
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":02A4
            TabIndex        =   148
            ToolTipText     =   "The Version for the Tape"
            Top             =   1320
            Width           =   1515
         End
         Begin VB.TextBox txtIEreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   147
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   1680
            Width           =   3855
         End
         Begin VB.ComboBox cmbIEfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02A6
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":02A8
            TabIndex        =   146
            ToolTipText     =   "The Version for the Tape"
            Top             =   1680
            Width           =   1515
         End
         Begin VB.TextBox txtITreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   145
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   2040
            Width           =   3855
         End
         Begin VB.ComboBox cmbITfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02AA
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":02AC
            TabIndex        =   144
            ToolTipText     =   "The Version for the Tape"
            Top             =   2040
            Width           =   1515
         End
         Begin VB.TextBox txtNLreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   143
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   1320
            Width           =   3855
         End
         Begin VB.ComboBox cmbNLfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02AE
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":02B0
            TabIndex        =   142
            ToolTipText     =   "The Version for the Tape"
            Top             =   1320
            Width           =   1515
         End
         Begin VB.TextBox txtLUreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   141
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   600
            Width           =   3855
         End
         Begin VB.ComboBox cmbLUfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02B2
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":02B4
            TabIndex        =   140
            ToolTipText     =   "The Version for the Tape"
            Top             =   600
            Width           =   1515
         End
         Begin VB.TextBox txtCHreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4260
            TabIndex        =   139
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   2040
            Width           =   3855
         End
         Begin VB.ComboBox cmbCHfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02B6
            Left            =   1680
            List            =   "frmiTunesPackage.frx":02B8
            TabIndex        =   138
            ToolTipText     =   "The Version for the Tape"
            Top             =   2040
            Width           =   1515
         End
         Begin VB.TextBox txtESreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4260
            TabIndex        =   137
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   1680
            Width           =   3855
         End
         Begin VB.ComboBox cmbESfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02BA
            Left            =   1680
            List            =   "frmiTunesPackage.frx":02BC
            TabIndex        =   136
            ToolTipText     =   "The Version for the Tape"
            Top             =   1680
            Width           =   1515
         End
         Begin VB.TextBox txtQBreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4260
            TabIndex        =   135
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   1320
            Width           =   3855
         End
         Begin VB.ComboBox cmbQBfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02BE
            Left            =   1680
            List            =   "frmiTunesPackage.frx":02C0
            TabIndex        =   134
            ToolTipText     =   "The Version for the Tape"
            Top             =   1320
            Width           =   1515
         End
         Begin VB.ComboBox cmbPTfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02C2
            Left            =   1680
            List            =   "frmiTunesPackage.frx":02C4
            TabIndex        =   133
            ToolTipText     =   "The Version for the Tape"
            Top             =   960
            Width           =   1515
         End
         Begin VB.TextBox txtPTreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4260
            TabIndex        =   132
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   960
            Width           =   3855
         End
         Begin VB.ComboBox cmbMXfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02C6
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":02C8
            TabIndex        =   131
            ToolTipText     =   "The Version for the Tape"
            Top             =   960
            Width           =   1515
         End
         Begin VB.TextBox txtMXreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   130
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   960
            Width           =   3855
         End
         Begin VB.ComboBox cmbBEfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02CA
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":02CC
            TabIndex        =   129
            ToolTipText     =   "The Version for the Tape"
            Top             =   2040
            Width           =   1515
         End
         Begin VB.TextBox txtBEreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   128
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   2040
            Width           =   3855
         End
         Begin VB.ComboBox cmbAUSfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02CE
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":02D0
            TabIndex        =   127
            ToolTipText     =   "The Version for the Tape"
            Top             =   2400
            Width           =   1515
         End
         Begin VB.TextBox txtAUSreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   126
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   2400
            Width           =   3855
         End
         Begin VB.ComboBox cmbCAfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02D2
            Left            =   1680
            List            =   "frmiTunesPackage.frx":02D4
            TabIndex        =   125
            ToolTipText     =   "The Version for the Tape"
            Top             =   600
            Width           =   1515
         End
         Begin VB.TextBox txtCAreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4260
            TabIndex        =   124
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   600
            Width           =   3855
         End
         Begin VB.ComboBox cmbUKfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02D6
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":02D8
            TabIndex        =   123
            ToolTipText     =   "The Version for the Tape"
            Top             =   600
            Width           =   1515
         End
         Begin VB.TextBox txtUKreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   122
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   600
            Width           =   3855
         End
         Begin VB.ComboBox cmbSEfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02DA
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":02DC
            TabIndex        =   121
            ToolTipText     =   "The Version for the Tape"
            Top             =   1320
            Width           =   1515
         End
         Begin VB.TextBox txtSEreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   120
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   1320
            Width           =   3855
         End
         Begin VB.ComboBox cmbNOfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02DE
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":02E0
            TabIndex        =   119
            ToolTipText     =   "The Version for the Tape"
            Top             =   960
            Width           =   1515
         End
         Begin VB.TextBox txtNOreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   118
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   960
            Width           =   3855
         End
         Begin VB.ComboBox cmbFIfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02E2
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":02E4
            TabIndex        =   117
            ToolTipText     =   "The Version for the Tape"
            Top             =   2040
            Width           =   1515
         End
         Begin VB.TextBox txtFIreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   116
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   2040
            Width           =   3855
         End
         Begin VB.ComboBox cmbDENfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02E6
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":02E8
            TabIndex        =   115
            ToolTipText     =   "The Version for the Tape"
            Top             =   1680
            Width           =   1515
         End
         Begin VB.TextBox txtDENreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   114
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   1680
            Width           =   3855
         End
         Begin VB.ComboBox cmbAUfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02EA
            Left            =   1680
            List            =   "frmiTunesPackage.frx":02EC
            TabIndex        =   113
            ToolTipText     =   "The Version for the Tape"
            Top             =   2400
            Width           =   1515
         End
         Begin VB.TextBox txtAUreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4260
            TabIndex        =   112
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   2400
            Width           =   3855
         End
         Begin VB.ComboBox cmbUSfilm 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmiTunesPackage.frx":02EE
            Left            =   -73320
            List            =   "frmiTunesPackage.frx":02F0
            TabIndex        =   111
            ToolTipText     =   "The Version for the Tape"
            Top             =   2400
            Width           =   1515
         End
         Begin VB.TextBox txtUSreason 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -70740
            TabIndex        =   110
            ToolTipText     =   "Job number for which the clip was made"
            Top             =   2400
            Width           =   3855
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   16
            Left            =   -71580
            TabIndex        =   205
            Top             =   2460
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Japan rating"
            Height          =   255
            Index           =   2
            Left            =   -74880
            TabIndex        =   204
            Top             =   2460
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   12
            Left            =   -71580
            TabIndex        =   203
            Top             =   1020
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Germany rating"
            Height          =   255
            Index           =   6
            Left            =   -74880
            TabIndex        =   202
            Top             =   1020
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   13
            Left            =   -71580
            TabIndex        =   201
            Top             =   660
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "France rating"
            Height          =   255
            Index           =   5
            Left            =   -74880
            TabIndex        =   200
            Top             =   660
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   17
            Left            =   -71580
            TabIndex        =   199
            Top             =   1740
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "New Zealand rating"
            Height          =   255
            Index           =   9
            Left            =   -74880
            TabIndex        =   198
            Top             =   1740
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   51
            Left            =   -71580
            TabIndex        =   197
            Top             =   1380
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Greece rating"
            Height          =   255
            Index           =   52
            Left            =   -74880
            TabIndex        =   196
            Top             =   1380
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   53
            Left            =   -71580
            TabIndex        =   195
            Top             =   1740
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Ireland rating"
            Height          =   255
            Index           =   54
            Left            =   -74880
            TabIndex        =   194
            Top             =   1740
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   55
            Left            =   -71580
            TabIndex        =   193
            Top             =   2100
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Italy rating"
            Height          =   255
            Index           =   56
            Left            =   -74880
            TabIndex        =   192
            Top             =   2100
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   61
            Left            =   -71580
            TabIndex        =   191
            Top             =   1380
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Netherlands rating"
            Height          =   255
            Index           =   62
            Left            =   -74880
            TabIndex        =   190
            Top             =   1380
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   74
            Left            =   -71580
            TabIndex        =   189
            Top             =   660
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Luxembourg rating"
            Height          =   255
            Index           =   75
            Left            =   -74880
            TabIndex        =   188
            Top             =   660
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   77
            Left            =   3420
            TabIndex        =   187
            Top             =   2100
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Switzerland rating"
            Height          =   255
            Index           =   81
            Left            =   120
            TabIndex        =   186
            Top             =   2100
            Width           =   1575
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   87
            Left            =   3420
            TabIndex        =   185
            Top             =   1740
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Spain rating"
            Height          =   255
            Index           =   88
            Left            =   120
            TabIndex        =   184
            Top             =   1740
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   89
            Left            =   3420
            TabIndex        =   183
            Top             =   1380
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Quebec rating"
            Height          =   255
            Index           =   90
            Left            =   120
            TabIndex        =   182
            Top             =   1380
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Portugal rating"
            Height          =   255
            Index           =   58
            Left            =   120
            TabIndex        =   181
            Top             =   1020
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   57
            Left            =   3420
            TabIndex        =   180
            Top             =   1020
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Mexico rating"
            Height          =   255
            Index           =   64
            Left            =   -74880
            TabIndex        =   179
            Top             =   1020
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   63
            Left            =   -71580
            TabIndex        =   178
            Top             =   1020
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Belgium rating"
            Height          =   255
            Index           =   42
            Left            =   -74880
            TabIndex        =   177
            Top             =   2100
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   41
            Left            =   -71580
            TabIndex        =   176
            Top             =   2100
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Austria rating"
            Height          =   255
            Index           =   45
            Left            =   -74880
            TabIndex        =   175
            Top             =   2460
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   43
            Left            =   -71580
            TabIndex        =   174
            Top             =   2460
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Canada rating"
            Height          =   255
            Index           =   7
            Left            =   120
            TabIndex        =   173
            Top             =   660
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   11
            Left            =   3420
            TabIndex        =   172
            Top             =   660
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "UK rating"
            Height          =   255
            Index           =   4
            Left            =   -74880
            TabIndex        =   171
            Top             =   660
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   14
            Left            =   -71580
            TabIndex        =   170
            Top             =   660
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Sweden rating"
            Height          =   255
            Index           =   86
            Left            =   -74880
            TabIndex        =   169
            Top             =   1380
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   82
            Left            =   -71580
            TabIndex        =   168
            Top             =   1380
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Norway rating"
            Height          =   255
            Index           =   60
            Left            =   -74880
            TabIndex        =   167
            Top             =   1020
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   59
            Left            =   -71580
            TabIndex        =   166
            Top             =   1020
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Finland rating"
            Height          =   255
            Index           =   50
            Left            =   -74880
            TabIndex        =   165
            Top             =   2100
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   49
            Left            =   -71580
            TabIndex        =   164
            Top             =   2100
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Denmark rating"
            Height          =   255
            Index           =   48
            Left            =   -74880
            TabIndex        =   163
            Top             =   1740
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   46
            Left            =   -71580
            TabIndex        =   162
            Top             =   1740
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Australia rating"
            Height          =   255
            Index           =   8
            Left            =   120
            TabIndex        =   161
            Top             =   2460
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   10
            Left            =   3420
            TabIndex        =   160
            Top             =   2460
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "USA rating"
            Height          =   255
            Index           =   3
            Left            =   -74880
            TabIndex        =   159
            Top             =   2460
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "reason"
            Height          =   255
            Index           =   15
            Left            =   -71580
            TabIndex        =   158
            Top             =   2460
            Width           =   675
         End
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnVodType 
         Height          =   855
         Left            =   -69960
         TabIndex        =   207
         Top             =   10020
         Width           =   2295
         DataFieldList   =   "column 0"
         ScrollBars      =   0
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         BackColorOdd    =   16761024
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).Caption=   "value"
         Columns(0).Name =   "value"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   4048
         _ExtentY        =   1508
         _StockProps     =   77
         DataFieldToDisplay=   "column 0"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnTrueFalse 
         Height          =   855
         Left            =   -74220
         TabIndex        =   211
         Top             =   9900
         Width           =   2295
         DataFieldList   =   "column 0"
         ScrollBars      =   0
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         BackColorOdd    =   16761024
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).Caption=   "value"
         Columns(0).Name =   "value"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   4048
         _ExtentY        =   1508
         _StockProps     =   77
         DataFieldToDisplay=   "column 0"
      End
      Begin TabDlg.SSTab tabFilm 
         Height          =   9195
         Left            =   -62880
         TabIndex        =   216
         Top             =   420
         Width           =   11715
         _ExtentX        =   20664
         _ExtentY        =   16219
         _Version        =   393216
         Tabs            =   5
         TabsPerRow      =   5
         TabHeight       =   520
         TabCaption(0)   =   "Cast && Crew"
         TabPicture(0)   =   "frmiTunesPackage.frx":02F2
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "grdCrewRole"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "grdCrew"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "grdCast"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "ddnBilling"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "ddnFilmRoles"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "grdCastCharacters"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "ddnDubArtistRoles"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "ddnDubArtistLocale"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).ControlCount=   8
         TabCaption(1)   =   "Assets"
         TabPicture(1)   =   "frmiTunesPackage.frx":030E
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Frame1"
         Tab(1).Control(1)=   "Frame3"
         Tab(1).Control(2)=   "Frame4"
         Tab(1).Control(3)=   "Frame5"
         Tab(1).Control(4)=   "Frame6"
         Tab(1).Control(5)=   "Frame8"
         Tab(1).Control(6)=   "Frame9"
         Tab(1).Control(7)=   "Frame10"
         Tab(1).ControlCount=   8
         TabCaption(2)   =   "Chapters"
         TabPicture(2)   =   "frmiTunesPackage.frx":032A
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "Frame2"
         Tab(2).Control(1)=   "chkCustomChapterTitles"
         Tab(2).ControlCount=   2
         TabCaption(3)   =   "Artists"
         TabPicture(3)   =   "frmiTunesPackage.frx":0346
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "ddnArtistRoles"
         Tab(3).Control(1)=   "grdArtistRoles1"
         Tab(3).Control(2)=   "grdArtist1"
         Tab(3).ControlCount=   3
         TabCaption(4)   =   "Cue Sheet"
         TabPicture(4)   =   "frmiTunesPackage.frx":0362
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "ddnUsage"
         Tab(4).Control(1)=   "grdCueSheet"
         Tab(4).Control(2)=   "grdArtist2"
         Tab(4).Control(3)=   "grdArtistRoles2"
         Tab(4).ControlCount=   4
         Begin VB.Frame Frame1 
            Caption         =   "Full"
            Height          =   1155
            Left            =   -74820
            TabIndex        =   393
            Top             =   360
            Width           =   11295
            Begin VB.TextBox txtFullClipID 
               Height          =   315
               Left            =   1380
               TabIndex        =   400
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   300
               Width           =   855
            End
            Begin VB.TextBox txtFullCropTop 
               Height          =   315
               Left            =   6600
               TabIndex        =   399
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   300
               Width           =   615
            End
            Begin VB.TextBox txtFullCropBottom 
               Height          =   315
               Left            =   9240
               TabIndex        =   398
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   300
               Width           =   555
            End
            Begin VB.TextBox txtFullCropLeft 
               Height          =   315
               Left            =   7860
               TabIndex        =   397
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   300
               Width           =   615
            End
            Begin VB.TextBox txtFullCropRight 
               Height          =   315
               Left            =   10380
               TabIndex        =   396
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   300
               Width           =   615
            End
            Begin VB.TextBox txtCaptionsClipID 
               Height          =   315
               Left            =   1380
               TabIndex        =   395
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   660
               Width           =   855
            End
            Begin VB.CheckBox chkFeatureTextless 
               Caption         =   "Check1"
               Height          =   255
               Left            =   5580
               TabIndex        =   394
               Top             =   720
               Width           =   255
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbFullLocale 
               Bindings        =   "frmiTunesPackage.frx":037E
               Height          =   315
               Index           =   0
               Left            =   3060
               TabIndex        =   401
               ToolTipText     =   "The company this tape belongs to"
               Top             =   300
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbForcedNarrativeLocale 
               Bindings        =   "frmiTunesPackage.frx":039A
               Height          =   315
               Index           =   0
               Left            =   10020
               TabIndex        =   402
               ToolTipText     =   "The company this tape belongs to"
               Top             =   660
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbFullLocale 
               Bindings        =   "frmiTunesPackage.frx":03B6
               Height          =   315
               Index           =   1
               Left            =   3060
               TabIndex        =   403
               ToolTipText     =   "The company this tape belongs to"
               Top             =   660
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbForcedNarrativeLocale 
               Bindings        =   "frmiTunesPackage.frx":03D2
               Height          =   315
               Index           =   5
               Left            =   7140
               TabIndex        =   542
               ToolTipText     =   "The company this tape belongs to"
               Top             =   660
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin VB.Label lblCaption 
               Caption         =   "BIS Locale"
               ForeColor       =   &H000000FF&
               Height          =   255
               Index           =   182
               Left            =   6180
               TabIndex        =   543
               Top             =   720
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID"
               ForeColor       =   &H000000FF&
               Height          =   255
               Index           =   23
               Left            =   120
               TabIndex        =   413
               Top             =   360
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Right"
               Height          =   255
               Index           =   24
               Left            =   9960
               TabIndex        =   412
               Top             =   360
               Width           =   435
            End
            Begin VB.Label lblCaption 
               Caption         =   "Left"
               Height          =   255
               Index           =   25
               Left            =   7500
               TabIndex        =   411
               Top             =   360
               Width           =   375
            End
            Begin VB.Label lblCaption 
               Caption         =   "Bottom"
               Height          =   255
               Index           =   26
               Left            =   8640
               TabIndex        =   410
               Top             =   360
               Width           =   555
            End
            Begin VB.Label lblCaption 
               Caption         =   "Top"
               Height          =   255
               Index           =   27
               Left            =   6240
               TabIndex        =   409
               Top             =   360
               Width           =   375
            End
            Begin VB.Label lblCaption 
               Caption         =   "Captions Clip ID"
               Height          =   255
               Index           =   100
               Left            =   120
               TabIndex        =   408
               Top             =   720
               Width           =   1155
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               ForeColor       =   &H000000FF&
               Height          =   255
               Index           =   147
               Left            =   2400
               TabIndex        =   407
               Top             =   360
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Flag as textless"
               ForeColor       =   &H000000FF&
               Height          =   255
               Index           =   148
               Left            =   4260
               TabIndex        =   406
               Top             =   720
               Width           =   1155
            End
            Begin VB.Label lblCaption 
               Caption         =   "Forced Narrative Locale"
               ForeColor       =   &H000000FF&
               Height          =   255
               Index           =   149
               Left            =   8220
               TabIndex        =   405
               Top             =   720
               Width           =   1755
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   150
               Left            =   2400
               TabIndex        =   404
               Top             =   720
               Width           =   675
            End
         End
         Begin VB.Frame Frame3 
            Caption         =   "Preview"
            Height          =   1815
            Left            =   -74820
            TabIndex        =   354
            Top             =   1560
            Width           =   11295
            Begin VB.TextBox txtPreviewClipID 
               Height          =   315
               Index           =   0
               Left            =   780
               TabIndex        =   362
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   300
               Width           =   855
            End
            Begin VB.TextBox txtPreviewCropTop 
               Height          =   315
               Left            =   10500
               TabIndex        =   361
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   1020
               Width           =   555
            End
            Begin VB.TextBox txtPreviewCropBottom 
               Height          =   315
               Left            =   10500
               TabIndex        =   360
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   1380
               Width           =   555
            End
            Begin VB.TextBox txtPreviewCropLeft 
               Height          =   315
               Left            =   10500
               TabIndex        =   359
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   300
               Width           =   555
            End
            Begin VB.TextBox txtPreviewCropRight 
               Height          =   315
               Left            =   10500
               TabIndex        =   358
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   660
               Width           =   555
            End
            Begin VB.TextBox txtPreviewClipID 
               Height          =   315
               Index           =   1
               Left            =   780
               TabIndex        =   357
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   660
               Width           =   855
            End
            Begin VB.TextBox txtPreviewClipID 
               Height          =   315
               Index           =   2
               Left            =   780
               TabIndex        =   356
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   1020
               Width           =   855
            End
            Begin VB.TextBox txtPreviewClipID 
               Height          =   315
               Index           =   3
               Left            =   780
               TabIndex        =   355
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   1380
               Width           =   855
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPreviewLocale 
               Bindings        =   "frmiTunesPackage.frx":03EE
               Height          =   315
               Index           =   0
               Left            =   3780
               TabIndex        =   363
               ToolTipText     =   "The company this tape belongs to"
               Top             =   300
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPreviewTerritory 
               Bindings        =   "frmiTunesPackage.frx":040D
               Height          =   315
               Index           =   1
               Left            =   2400
               TabIndex        =   364
               ToolTipText     =   "The company this tape belongs to"
               Top             =   660
               Width           =   675
               DataFieldList   =   "iso2acode"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   4048
               Columns(0).Caption=   "country"
               Columns(0).Name =   "country"
               Columns(0).DataField=   "country"
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "iso2acode"
               Columns(1).Name =   "iso2acode"
               Columns(1).DataField=   "iso2acode"
               Columns(1).FieldLen=   256
               _ExtentX        =   1191
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "iso2acode"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPreviewTerritory 
               Bindings        =   "frmiTunesPackage.frx":0424
               Height          =   315
               Index           =   2
               Left            =   2400
               TabIndex        =   365
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1020
               Width           =   675
               DataFieldList   =   "iso2acode"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   4048
               Columns(0).Caption=   "country"
               Columns(0).Name =   "country"
               Columns(0).DataField=   "country"
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "iso2acode"
               Columns(1).Name =   "iso2acode"
               Columns(1).DataField=   "iso2acode"
               Columns(1).FieldLen=   256
               _ExtentX        =   1191
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "iso2acode"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPreviewTerritory 
               Bindings        =   "frmiTunesPackage.frx":043B
               Height          =   315
               Index           =   3
               Left            =   2400
               TabIndex        =   366
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1380
               Width           =   675
               DataFieldList   =   "iso2acode"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   4048
               Columns(0).Caption=   "country"
               Columns(0).Name =   "country"
               Columns(0).DataField=   "country"
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "iso2acode"
               Columns(1).Name =   "iso2acode"
               Columns(1).DataField=   "iso2acode"
               Columns(1).FieldLen=   256
               _ExtentX        =   1191
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "iso2acode"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPreviewLocale 
               Bindings        =   "frmiTunesPackage.frx":0452
               Height          =   315
               Index           =   1
               Left            =   3780
               TabIndex        =   367
               ToolTipText     =   "The company this tape belongs to"
               Top             =   660
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPreviewLocale 
               Bindings        =   "frmiTunesPackage.frx":0471
               Height          =   315
               Index           =   2
               Left            =   3780
               TabIndex        =   368
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1020
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPreviewLocale 
               Bindings        =   "frmiTunesPackage.frx":0490
               Height          =   315
               Index           =   3
               Left            =   3780
               TabIndex        =   369
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1380
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbForcedNarrativeLocale 
               Bindings        =   "frmiTunesPackage.frx":04AF
               Height          =   315
               Index           =   1
               Left            =   8820
               TabIndex        =   370
               ToolTipText     =   "The company this tape belongs to"
               Top             =   300
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbForcedNarrativeLocale 
               Bindings        =   "frmiTunesPackage.frx":04CB
               Height          =   315
               Index           =   2
               Left            =   8820
               TabIndex        =   371
               ToolTipText     =   "The company this tape belongs to"
               Top             =   660
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbForcedNarrativeLocale 
               Bindings        =   "frmiTunesPackage.frx":04E7
               Height          =   315
               Index           =   3
               Left            =   8820
               TabIndex        =   372
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1020
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbForcedNarrativeLocale 
               Bindings        =   "frmiTunesPackage.frx":0503
               Height          =   315
               Index           =   4
               Left            =   8820
               TabIndex        =   373
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1380
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbForcedNarrativeLocale 
               Bindings        =   "frmiTunesPackage.frx":051F
               Height          =   315
               Index           =   6
               Left            =   5880
               TabIndex        =   544
               ToolTipText     =   "The company this tape belongs to"
               Top             =   300
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbForcedNarrativeLocale 
               Bindings        =   "frmiTunesPackage.frx":053B
               Height          =   315
               Index           =   7
               Left            =   5880
               TabIndex        =   545
               ToolTipText     =   "The company this tape belongs to"
               Top             =   660
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbForcedNarrativeLocale 
               Bindings        =   "frmiTunesPackage.frx":0557
               Height          =   315
               Index           =   8
               Left            =   5880
               TabIndex        =   546
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1020
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbForcedNarrativeLocale 
               Bindings        =   "frmiTunesPackage.frx":0573
               Height          =   315
               Index           =   9
               Left            =   5880
               TabIndex        =   547
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1380
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin VB.Label lblCaption 
               Caption         =   "BIS Locale"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   181
               Left            =   4920
               TabIndex        =   541
               Top             =   1440
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "BIS Locale"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   180
               Left            =   4920
               TabIndex        =   540
               Top             =   1080
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "BIS Locale"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   179
               Left            =   4920
               TabIndex        =   539
               Top             =   720
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "BIS Locale"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   178
               Left            =   4920
               TabIndex        =   538
               Top             =   360
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 1"
               ForeColor       =   &H000000FF&
               Height          =   255
               Index           =   28
               Left            =   120
               TabIndex        =   392
               Top             =   360
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Right"
               Height          =   255
               Index           =   29
               Left            =   10080
               TabIndex        =   391
               Top             =   720
               Width           =   435
            End
            Begin VB.Label lblCaption 
               Caption         =   "Left"
               Height          =   255
               Index           =   30
               Left            =   10140
               TabIndex        =   390
               Top             =   360
               Width           =   315
            End
            Begin VB.Label lblCaption 
               Caption         =   "Bottom"
               Height          =   255
               Index           =   31
               Left            =   9960
               TabIndex        =   389
               Top             =   1440
               Width           =   615
            End
            Begin VB.Label lblCaption 
               Caption         =   "Top"
               Height          =   255
               Index           =   32
               Left            =   10140
               TabIndex        =   388
               Top             =   1080
               Width           =   375
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 2"
               Height          =   255
               Index           =   101
               Left            =   120
               TabIndex        =   387
               Top             =   720
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 3"
               Height          =   255
               Index           =   102
               Left            =   120
               TabIndex        =   386
               Top             =   1080
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 4"
               Height          =   255
               Index           =   103
               Left            =   120
               TabIndex        =   385
               Top             =   1440
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               ForeColor       =   &H000000FF&
               Height          =   255
               Index           =   111
               Left            =   3240
               TabIndex        =   384
               Top             =   360
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Territory"
               Height          =   255
               Index           =   112
               Left            =   1800
               TabIndex        =   383
               Top             =   720
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Territory"
               Height          =   255
               Index           =   113
               Left            =   1800
               TabIndex        =   382
               Top             =   1080
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Territory"
               Height          =   255
               Index           =   114
               Left            =   1800
               TabIndex        =   381
               Top             =   1440
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   115
               Left            =   3240
               TabIndex        =   380
               Top             =   720
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   116
               Left            =   3240
               TabIndex        =   379
               Top             =   1080
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   117
               Left            =   3240
               TabIndex        =   378
               Top             =   1440
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Forced Narrative Locale"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   200
               Left            =   7020
               TabIndex        =   377
               Top             =   360
               Width           =   1755
            End
            Begin VB.Label lblCaption 
               Caption         =   "Forced Narrative Locale"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   201
               Left            =   7020
               TabIndex        =   376
               Top             =   720
               Width           =   1755
            End
            Begin VB.Label lblCaption 
               Caption         =   "Forced Narrative Locale"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   202
               Left            =   7020
               TabIndex        =   375
               Top             =   1080
               Width           =   1755
            End
            Begin VB.Label lblCaption 
               Caption         =   "Forced Narrative Locale"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   203
               Left            =   7020
               TabIndex        =   374
               Top             =   1440
               Width           =   1755
            End
         End
         Begin VB.Frame Frame4 
            Caption         =   "Poster Image"
            Height          =   1815
            Left            =   -74820
            TabIndex        =   323
            Top             =   6300
            Width           =   7575
            Begin VB.TextBox txtPosterClipID 
               Height          =   315
               Index           =   0
               Left            =   840
               TabIndex        =   327
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   300
               Width           =   855
            End
            Begin VB.TextBox txtPosterClipID 
               Height          =   315
               Index           =   1
               Left            =   840
               TabIndex        =   326
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   660
               Width           =   855
            End
            Begin VB.TextBox txtPosterClipID 
               Height          =   315
               Index           =   2
               Left            =   840
               TabIndex        =   325
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   1020
               Width           =   855
            End
            Begin VB.TextBox txtPosterClipID 
               Height          =   315
               Index           =   3
               Left            =   840
               TabIndex        =   324
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   1380
               Width           =   855
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPosterTerritory 
               Bindings        =   "frmiTunesPackage.frx":058F
               Height          =   315
               Index           =   1
               Left            =   2460
               TabIndex        =   328
               ToolTipText     =   "The company this tape belongs to"
               Top             =   660
               Width           =   675
               DataFieldList   =   "iso2acode"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   4048
               Columns(0).Caption=   "country"
               Columns(0).Name =   "country"
               Columns(0).DataField=   "country"
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "iso2acode"
               Columns(1).Name =   "iso2acode"
               Columns(1).DataField=   "iso2acode"
               Columns(1).FieldLen=   256
               _ExtentX        =   1191
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "iso2acode"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPosterTerritory 
               Bindings        =   "frmiTunesPackage.frx":05A6
               Height          =   315
               Index           =   2
               Left            =   2460
               TabIndex        =   329
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1020
               Width           =   675
               DataFieldList   =   "iso2acode"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   4048
               Columns(0).Caption=   "country"
               Columns(0).Name =   "country"
               Columns(0).DataField=   "country"
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "iso2acode"
               Columns(1).Name =   "iso2acode"
               Columns(1).DataField=   "iso2acode"
               Columns(1).FieldLen=   256
               _ExtentX        =   1191
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "iso2acode"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPosterTerritory 
               Bindings        =   "frmiTunesPackage.frx":05BD
               Height          =   315
               Index           =   3
               Left            =   2460
               TabIndex        =   330
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1380
               Width           =   675
               DataFieldList   =   "iso2acode"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   4048
               Columns(0).Caption=   "country"
               Columns(0).Name =   "country"
               Columns(0).DataField=   "country"
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "iso2acode"
               Columns(1).Name =   "iso2acode"
               Columns(1).DataField=   "iso2acode"
               Columns(1).FieldLen=   256
               _ExtentX        =   1191
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "iso2acode"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPosterLocale 
               Bindings        =   "frmiTunesPackage.frx":05D4
               Height          =   315
               Index           =   4
               Left            =   6360
               TabIndex        =   331
               ToolTipText     =   "The company this tape belongs to"
               Top             =   300
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPosterLocale 
               Bindings        =   "frmiTunesPackage.frx":05F2
               Height          =   315
               Index           =   5
               Left            =   6360
               TabIndex        =   332
               ToolTipText     =   "The company this tape belongs to"
               Top             =   660
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPosterLocale 
               Bindings        =   "frmiTunesPackage.frx":0610
               Height          =   315
               Index           =   6
               Left            =   6360
               TabIndex        =   333
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1020
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPosterLocale 
               Bindings        =   "frmiTunesPackage.frx":062E
               Height          =   315
               Index           =   7
               Left            =   6360
               TabIndex        =   334
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1380
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPosterLocale 
               Bindings        =   "frmiTunesPackage.frx":064C
               Height          =   315
               Index           =   0
               Left            =   4380
               TabIndex        =   335
               ToolTipText     =   "The company this tape belongs to"
               Top             =   300
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPosterLocale 
               Bindings        =   "frmiTunesPackage.frx":066A
               Height          =   315
               Index           =   1
               Left            =   4380
               TabIndex        =   336
               ToolTipText     =   "The company this tape belongs to"
               Top             =   660
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPosterLocale 
               Bindings        =   "frmiTunesPackage.frx":0688
               Height          =   315
               Index           =   2
               Left            =   4380
               TabIndex        =   337
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1020
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPosterLocale 
               Bindings        =   "frmiTunesPackage.frx":06A6
               Height          =   315
               Index           =   3
               Left            =   4380
               TabIndex        =   338
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1380
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 1"
               ForeColor       =   &H000000FF&
               Height          =   255
               Index           =   33
               Left            =   120
               TabIndex        =   353
               Top             =   360
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 2"
               Height          =   255
               Index           =   104
               Left            =   120
               TabIndex        =   352
               Top             =   720
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 3"
               Height          =   255
               Index           =   105
               Left            =   120
               TabIndex        =   351
               Top             =   1080
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 4"
               Height          =   255
               Index           =   106
               Left            =   120
               TabIndex        =   350
               Top             =   1440
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Territory"
               Height          =   255
               Index           =   110
               Left            =   1800
               TabIndex        =   349
               Top             =   720
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Territory"
               Height          =   255
               Index           =   118
               Left            =   1800
               TabIndex        =   348
               Top             =   1080
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Territory"
               Height          =   255
               Index           =   119
               Left            =   1800
               TabIndex        =   347
               Top             =   1440
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale 1"
               Height          =   255
               Index           =   231
               Left            =   3420
               TabIndex        =   346
               Top             =   1440
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale 1"
               Height          =   255
               Index           =   232
               Left            =   3420
               TabIndex        =   345
               Top             =   1080
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale 1"
               Height          =   255
               Index           =   233
               Left            =   3420
               TabIndex        =   344
               Top             =   720
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale 1"
               ForeColor       =   &H000000FF&
               Height          =   255
               Index           =   234
               Left            =   3420
               TabIndex        =   343
               Top             =   360
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale 2"
               Height          =   255
               Index           =   235
               Left            =   5580
               TabIndex        =   342
               Top             =   1440
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale 2"
               Height          =   255
               Index           =   236
               Left            =   5580
               TabIndex        =   341
               Top             =   1080
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale 2"
               Height          =   255
               Index           =   237
               Left            =   5580
               TabIndex        =   340
               Top             =   720
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale 2"
               Height          =   255
               Index           =   238
               Left            =   5580
               TabIndex        =   339
               Top             =   360
               Width           =   675
            End
         End
         Begin VB.Frame Frame5 
            Caption         =   "Subtitles"
            Height          =   1815
            Left            =   -67140
            TabIndex        =   306
            Top             =   3420
            Width           =   3615
            Begin VB.TextBox txtSubtitlesClipID 
               Height          =   315
               Index           =   0
               Left            =   840
               TabIndex        =   310
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   300
               Width           =   855
            End
            Begin VB.TextBox txtSubtitlesClipID 
               Height          =   315
               Index           =   1
               Left            =   840
               TabIndex        =   309
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   660
               Width           =   855
            End
            Begin VB.TextBox txtSubtitlesClipID 
               Height          =   315
               Index           =   2
               Left            =   840
               TabIndex        =   308
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   1020
               Width           =   855
            End
            Begin VB.TextBox txtSubtitlesClipID 
               Height          =   315
               Index           =   3
               Left            =   840
               TabIndex        =   307
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   1380
               Width           =   855
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSubtitlesLocale 
               Bindings        =   "frmiTunesPackage.frx":06C4
               Height          =   315
               Index           =   0
               Left            =   2460
               TabIndex        =   311
               ToolTipText     =   "The company this tape belongs to"
               Top             =   300
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSubtitlesLocale 
               Bindings        =   "frmiTunesPackage.frx":06E3
               Height          =   315
               Index           =   1
               Left            =   2460
               TabIndex        =   312
               ToolTipText     =   "The company this tape belongs to"
               Top             =   660
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSubtitlesLocale 
               Bindings        =   "frmiTunesPackage.frx":0702
               Height          =   315
               Index           =   2
               Left            =   2460
               TabIndex        =   313
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1020
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSubtitlesLocale 
               Bindings        =   "frmiTunesPackage.frx":0721
               Height          =   315
               Index           =   3
               Left            =   2460
               TabIndex        =   314
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1380
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 1"
               Height          =   255
               Index           =   38
               Left            =   120
               TabIndex        =   322
               Top             =   360
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 2"
               Height          =   255
               Index           =   107
               Left            =   120
               TabIndex        =   321
               Top             =   720
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 3"
               Height          =   255
               Index           =   108
               Left            =   120
               TabIndex        =   320
               Top             =   1080
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 4"
               Height          =   255
               Index           =   109
               Left            =   120
               TabIndex        =   319
               Top             =   1440
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   120
               Left            =   1860
               TabIndex        =   318
               Top             =   360
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   121
               Left            =   1860
               TabIndex        =   317
               Top             =   720
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   122
               Left            =   1860
               TabIndex        =   316
               Top             =   1080
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   123
               Left            =   1860
               TabIndex        =   315
               Top             =   1440
               Width           =   675
            End
         End
         Begin VB.Frame Frame2 
            Caption         =   "Chapters"
            Height          =   7515
            Left            =   -74820
            TabIndex        =   296
            Top             =   540
            Width           =   11235
            Begin VB.OptionButton chkTimecode 
               Caption         =   "HH:MM:SS"
               Height          =   195
               Index           =   4
               Left            =   5820
               TabIndex        =   302
               Top             =   300
               Width           =   1335
            End
            Begin VB.OptionButton chkTimecode 
               Caption         =   "29.97 Drop"
               Height          =   195
               Index           =   3
               Left            =   4260
               TabIndex        =   301
               Top             =   300
               Width           =   1335
            End
            Begin VB.OptionButton chkTimecode 
               Caption         =   "29.97 Non Drop"
               Height          =   195
               Index           =   2
               Left            =   2340
               TabIndex        =   300
               Top             =   300
               Width           =   1515
            End
            Begin VB.TextBox txtWebScreenerClipID 
               Height          =   315
               Left            =   1800
               TabIndex        =   299
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   600
               Width           =   2115
            End
            Begin VB.OptionButton chkTimecode 
               Caption         =   "25"
               Height          =   195
               Index           =   1
               Left            =   1380
               TabIndex        =   298
               Top             =   300
               Value           =   -1  'True
               Width           =   735
            End
            Begin VB.OptionButton chkTimecode 
               Caption         =   "23.976"
               Height          =   195
               Index           =   0
               Left            =   180
               TabIndex        =   297
               Top             =   300
               Width           =   855
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdChapters 
               Bindings        =   "frmiTunesPackage.frx":0740
               Height          =   6375
               Left            =   180
               TabIndex        =   303
               Top             =   1020
               Width           =   10935
               _Version        =   196617
               AllowAddNew     =   -1  'True
               AllowDelete     =   -1  'True
               SelectTypeRow   =   3
               RowHeight       =   423
               ExtraHeight     =   53
               Columns.Count   =   8
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "iTunes_ChapterID"
               Columns(0).Name =   "iTunes_ChapterID"
               Columns(0).DataField=   "iTunes_ChapterID"
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "iTunes_PackageID"
               Columns(1).Name =   "iTunes_PackageID"
               Columns(1).DataField=   "iTunes_PackageID"
               Columns(1).FieldLen=   256
               Columns(2).Width=   2302
               Columns(2).Caption=   "Start Time"
               Columns(2).Name =   "starttime"
               Columns(2).DataField=   "starttime"
               Columns(2).FieldLen=   256
               Columns(3).Width=   2646
               Columns(3).Caption=   "Image Grab ClipID"
               Columns(3).Name =   "pictureClipID"
               Columns(3).DataField=   "pictureClipID"
               Columns(3).FieldLen=   256
               Columns(4).Width=   3200
               Columns(4).Visible=   0   'False
               Columns(4).Caption=   "Title 1"
               Columns(4).Name =   "Title1"
               Columns(4).DataField=   "Title1"
               Columns(4).FieldLen=   256
               Columns(5).Width=   3200
               Columns(5).Visible=   0   'False
               Columns(5).Caption=   "Title 2"
               Columns(5).Name =   "Title2"
               Columns(5).DataField=   "Title2"
               Columns(5).FieldLen=   256
               Columns(6).Width=   3200
               Columns(6).Visible=   0   'False
               Columns(6).Caption=   "Title 3"
               Columns(6).Name =   "Title3"
               Columns(6).DataField=   "Title3"
               Columns(6).FieldLen=   256
               Columns(7).Width=   3200
               Columns(7).Visible=   0   'False
               Columns(7).Caption=   "Title 4"
               Columns(7).Name =   "Title4"
               Columns(7).DataField=   "Title4"
               Columns(7).FieldLen=   256
               _ExtentX        =   19288
               _ExtentY        =   11245
               _StockProps     =   79
               Caption         =   "Chapter Information"
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblCaption 
               Caption         =   "Web Screener ClipID"
               Height          =   255
               Index           =   91
               Left            =   180
               TabIndex        =   304
               Top             =   660
               Width           =   1575
            End
         End
         Begin VB.CheckBox chkCustomChapterTitles 
            Alignment       =   1  'Right Justify
            Caption         =   "Custom Chapter Titles"
            Height          =   255
            Left            =   -70500
            TabIndex        =   295
            Top             =   1170
            Width           =   1995
         End
         Begin VB.Frame Frame6 
            Caption         =   "QC  Notes"
            Height          =   1815
            Left            =   -71160
            TabIndex        =   283
            Top             =   3420
            Width           =   3915
            Begin VB.TextBox txtQCClipID 
               Height          =   315
               Index           =   0
               Left            =   960
               TabIndex        =   288
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   300
               Width           =   855
            End
            Begin VB.TextBox txtQCClipID 
               Height          =   315
               Index           =   1
               Left            =   2940
               TabIndex        =   287
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   240
               Width           =   855
            End
            Begin VB.TextBox txtQCClipID 
               Height          =   315
               Index           =   2
               Left            =   2940
               TabIndex        =   286
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   600
               Width           =   855
            End
            Begin VB.TextBox txtQCClipID 
               Height          =   315
               Index           =   3
               Left            =   2940
               TabIndex        =   285
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   960
               Width           =   855
            End
            Begin VB.TextBox txtQCClipID 
               Height          =   315
               Index           =   4
               Left            =   2940
               TabIndex        =   284
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   1320
               Width           =   855
            End
            Begin VB.Label lblCaption 
               Caption         =   "Feat Clip ID"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   156
               Left            =   120
               TabIndex        =   293
               Top             =   360
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Trail 1 Clip ID"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   196
               Left            =   1920
               TabIndex        =   292
               Top             =   300
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Trail 2 Clip ID"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   197
               Left            =   1920
               TabIndex        =   291
               Top             =   660
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Trail 3 Clip ID"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   198
               Left            =   1920
               TabIndex        =   290
               Top             =   1020
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Trail 4 Clip ID"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   199
               Left            =   1920
               TabIndex        =   289
               Top             =   1380
               Width           =   1095
            End
         End
         Begin VB.Frame Frame8 
            Caption         =   "Subtitles Hearing Impaired"
            Height          =   1815
            Left            =   -67140
            TabIndex        =   265
            Top             =   7200
            Width           =   3615
            Begin VB.TextBox txtSubtitlesClipID 
               Height          =   315
               Index           =   11
               Left            =   840
               TabIndex        =   269
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   1380
               Width           =   855
            End
            Begin VB.TextBox txtSubtitlesClipID 
               Height          =   315
               Index           =   10
               Left            =   840
               TabIndex        =   268
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   1020
               Width           =   855
            End
            Begin VB.TextBox txtSubtitlesClipID 
               Height          =   315
               Index           =   9
               Left            =   840
               TabIndex        =   267
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   660
               Width           =   855
            End
            Begin VB.TextBox txtSubtitlesClipID 
               Height          =   315
               Index           =   8
               Left            =   840
               TabIndex        =   266
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   300
               Width           =   855
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSubtitlesLocale 
               Bindings        =   "frmiTunesPackage.frx":075A
               Height          =   315
               Index           =   7
               Left            =   2460
               TabIndex        =   270
               ToolTipText     =   "The company this tape belongs to"
               Top             =   300
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSubtitlesLocale 
               Bindings        =   "frmiTunesPackage.frx":0779
               Height          =   315
               Index           =   8
               Left            =   2460
               TabIndex        =   271
               ToolTipText     =   "The company this tape belongs to"
               Top             =   660
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSubtitlesLocale 
               Bindings        =   "frmiTunesPackage.frx":0798
               Height          =   315
               Index           =   9
               Left            =   2460
               TabIndex        =   272
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1020
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSubtitlesLocale 
               Bindings        =   "frmiTunesPackage.frx":07B7
               Height          =   315
               Index           =   10
               Left            =   2460
               TabIndex        =   273
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1380
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   187
               Left            =   1860
               TabIndex        =   281
               Top             =   1440
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   188
               Left            =   1860
               TabIndex        =   280
               Top             =   1080
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   189
               Left            =   1860
               TabIndex        =   279
               Top             =   720
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   190
               Left            =   1860
               TabIndex        =   278
               Top             =   360
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 4"
               Height          =   255
               Index           =   191
               Left            =   120
               TabIndex        =   277
               Top             =   1440
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 3"
               Height          =   255
               Index           =   192
               Left            =   120
               TabIndex        =   276
               Top             =   1080
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 2"
               Height          =   255
               Index           =   193
               Left            =   120
               TabIndex        =   275
               Top             =   720
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 1"
               Height          =   255
               Index           =   194
               Left            =   120
               TabIndex        =   274
               Top             =   360
               Width           =   1095
            End
         End
         Begin VB.Frame Frame9 
            Caption         =   "Subtitles Forced Narratives"
            Height          =   1875
            Left            =   -67140
            TabIndex        =   248
            Top             =   5280
            Width           =   3615
            Begin VB.TextBox txtSubtitlesClipID 
               Height          =   315
               Index           =   12
               Left            =   840
               TabIndex        =   252
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   300
               Width           =   855
            End
            Begin VB.TextBox txtSubtitlesClipID 
               Height          =   315
               Index           =   13
               Left            =   840
               TabIndex        =   251
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   660
               Width           =   855
            End
            Begin VB.TextBox txtSubtitlesClipID 
               Height          =   315
               Index           =   14
               Left            =   840
               TabIndex        =   250
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   1020
               Width           =   855
            End
            Begin VB.TextBox txtSubtitlesClipID 
               Height          =   315
               Index           =   15
               Left            =   840
               TabIndex        =   249
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   1380
               Width           =   855
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSubtitlesLocale 
               Bindings        =   "frmiTunesPackage.frx":07D6
               Height          =   315
               Index           =   11
               Left            =   2460
               TabIndex        =   253
               ToolTipText     =   "The company this tape belongs to"
               Top             =   300
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSubtitlesLocale 
               Bindings        =   "frmiTunesPackage.frx":07F5
               Height          =   315
               Index           =   12
               Left            =   2460
               TabIndex        =   254
               ToolTipText     =   "The company this tape belongs to"
               Top             =   660
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSubtitlesLocale 
               Bindings        =   "frmiTunesPackage.frx":0814
               Height          =   315
               Index           =   13
               Left            =   2460
               TabIndex        =   255
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1020
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSubtitlesLocale 
               Bindings        =   "frmiTunesPackage.frx":0833
               Height          =   315
               Index           =   14
               Left            =   2460
               TabIndex        =   256
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1380
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 1"
               Height          =   255
               Index           =   205
               Left            =   120
               TabIndex        =   264
               Top             =   360
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 2"
               Height          =   255
               Index           =   206
               Left            =   120
               TabIndex        =   263
               Top             =   720
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 3"
               Height          =   255
               Index           =   207
               Left            =   120
               TabIndex        =   262
               Top             =   1080
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Clip ID 4"
               Height          =   255
               Index           =   208
               Left            =   120
               TabIndex        =   261
               Top             =   1440
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   209
               Left            =   1860
               TabIndex        =   260
               Top             =   360
               Width           =   735
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   210
               Left            =   1860
               TabIndex        =   259
               Top             =   720
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   211
               Left            =   1860
               TabIndex        =   258
               Top             =   1080
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   212
               Left            =   1860
               TabIndex        =   257
               Top             =   1440
               Width           =   675
            End
         End
         Begin VB.Frame Frame10 
            Caption         =   "Additional Feature Audio Files"
            Height          =   2835
            Left            =   -74820
            TabIndex        =   219
            Top             =   3420
            Width           =   3555
            Begin VB.TextBox txtAudioClipID 
               Height          =   315
               Index           =   2
               Left            =   840
               TabIndex        =   226
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   1020
               Width           =   855
            End
            Begin VB.TextBox txtAudioClipID 
               Height          =   315
               Index           =   1
               Left            =   840
               TabIndex        =   225
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   660
               Width           =   855
            End
            Begin VB.TextBox txtAudioClipID 
               Height          =   315
               Index           =   0
               Left            =   840
               TabIndex        =   224
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   300
               Width           =   855
            End
            Begin VB.TextBox txtAudioClipID 
               Height          =   315
               Index           =   3
               Left            =   840
               TabIndex        =   223
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   1380
               Width           =   855
            End
            Begin VB.TextBox txtAudioClipID 
               Height          =   315
               Index           =   4
               Left            =   840
               TabIndex        =   222
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   1740
               Width           =   855
            End
            Begin VB.TextBox txtAudioClipID 
               Height          =   315
               Index           =   5
               Left            =   840
               TabIndex        =   221
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   2100
               Width           =   855
            End
            Begin VB.TextBox txtAudioClipID 
               Height          =   315
               Index           =   6
               Left            =   840
               TabIndex        =   220
               ToolTipText     =   "Job number for which the clip was made"
               Top             =   2460
               Width           =   855
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbAudioLocale 
               Bindings        =   "frmiTunesPackage.frx":0852
               Height          =   315
               Index           =   0
               Left            =   2400
               TabIndex        =   227
               ToolTipText     =   "The company this tape belongs to"
               Top             =   300
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbAudioLocale 
               Bindings        =   "frmiTunesPackage.frx":0871
               Height          =   315
               Index           =   1
               Left            =   2400
               TabIndex        =   228
               ToolTipText     =   "The company this tape belongs to"
               Top             =   660
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbAudioLocale 
               Bindings        =   "frmiTunesPackage.frx":0890
               Height          =   315
               Index           =   2
               Left            =   2400
               TabIndex        =   229
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1020
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbAudioLocale 
               Bindings        =   "frmiTunesPackage.frx":08AF
               Height          =   315
               Index           =   3
               Left            =   2400
               TabIndex        =   230
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1380
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbAudioLocale 
               Bindings        =   "frmiTunesPackage.frx":08CE
               Height          =   315
               Index           =   4
               Left            =   2400
               TabIndex        =   231
               ToolTipText     =   "The company this tape belongs to"
               Top             =   1740
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbAudioLocale 
               Bindings        =   "frmiTunesPackage.frx":08ED
               Height          =   315
               Index           =   5
               Left            =   2400
               TabIndex        =   232
               ToolTipText     =   "The company this tape belongs to"
               Top             =   2100
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbAudioLocale 
               Bindings        =   "frmiTunesPackage.frx":090C
               Height          =   315
               Index           =   6
               Left            =   2400
               TabIndex        =   233
               ToolTipText     =   "The company this tape belongs to"
               Top             =   2460
               Width           =   1035
               DataFieldList   =   "description"
               _Version        =   196617
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BackColorOdd    =   16761087
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "description"
               Columns(0).Name =   "description"
               Columns(0).DataField=   "description"
               Columns(0).FieldLen=   256
               Columns(1).Width=   8043
               Columns(1).Caption=   "information"
               Columns(1).Name =   "information"
               Columns(1).DataField=   "information"
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   556
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "description"
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   214
               Left            =   1800
               TabIndex        =   247
               Top             =   1080
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   215
               Left            =   1800
               TabIndex        =   246
               Top             =   720
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   216
               Left            =   1800
               TabIndex        =   245
               Top             =   360
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Audio ID3"
               Height          =   255
               Index           =   218
               Left            =   60
               TabIndex        =   244
               Top             =   1080
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Audio ID2"
               Height          =   255
               Index           =   219
               Left            =   60
               TabIndex        =   243
               Top             =   720
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Audio ID1"
               Height          =   255
               Index           =   220
               Left            =   60
               TabIndex        =   242
               Top             =   360
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "A.Dis  ID1"
               Height          =   255
               Index           =   184
               Left            =   60
               TabIndex        =   241
               Top             =   1440
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "A.Dis  ID2"
               Height          =   255
               Index           =   224
               Left            =   60
               TabIndex        =   240
               Top             =   1800
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "A.Dis  ID3"
               Height          =   255
               Index           =   225
               Left            =   60
               TabIndex        =   239
               Top             =   2160
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "A.Dis  ID4"
               Height          =   255
               Index           =   226
               Left            =   60
               TabIndex        =   238
               Top             =   2520
               Width           =   1095
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   227
               Left            =   1800
               TabIndex        =   237
               Top             =   1440
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   228
               Left            =   1800
               TabIndex        =   236
               Top             =   1800
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   229
               Left            =   1800
               TabIndex        =   235
               Top             =   2160
               Width           =   675
            End
            Begin VB.Label lblCaption 
               Caption         =   "Locale"
               Height          =   255
               Index           =   230
               Left            =   1800
               TabIndex        =   234
               Top             =   2520
               Width           =   675
            End
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnDubArtistLocale 
            Bindings        =   "frmiTunesPackage.frx":092B
            Height          =   855
            Left            =   7440
            TabIndex        =   217
            Top             =   7920
            Width           =   3015
            DataFieldList   =   "description"
            _Version        =   196617
            ColumnHeaders   =   0   'False
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1588
            Columns(0).Caption=   "description"
            Columns(0).Name =   "description"
            Columns(0).DataField=   "description"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "information"
            Columns(1).Name =   "information"
            Columns(1).DataField=   "information"
            Columns(1).FieldLen=   256
            _ExtentX        =   5318
            _ExtentY        =   1508
            _StockProps     =   77
            DataFieldToDisplay=   "description"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnDubArtistRoles 
            Bindings        =   "frmiTunesPackage.frx":094C
            Height          =   795
            Left            =   4860
            TabIndex        =   218
            Tag             =   "CLEAR"
            Top             =   7980
            Width           =   2175
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16761024
            RowHeight       =   423
            ExtraHeight     =   26
            Columns(0).Width=   3200
            Columns(0).Caption=   "advisorysystem"
            Columns(0).Name =   "advisorysystem"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   3836
            _ExtentY        =   1402
            _StockProps     =   77
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdCastCharacters 
            Bindings        =   "frmiTunesPackage.frx":0968
            Height          =   1815
            Left            =   60
            TabIndex        =   282
            Top             =   2700
            Width           =   11475
            _Version        =   196617
            AllowAddNew     =   -1  'True
            AllowDelete     =   -1  'True
            SelectTypeRow   =   3
            RowHeight       =   423
            ExtraHeight     =   53
            Columns.Count   =   12
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "iTunes_Cast_CharacterID"
            Columns(0).Name =   "iTunes_Cast_CharacterID"
            Columns(0).DataField=   "iTunes_Cast_CharacterID"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "itunes_packageID"
            Columns(1).Name =   "iTunes_CastID"
            Columns(1).DataField=   "iTunes_CastID"
            Columns(1).FieldLen=   256
            Columns(2).Width=   2805
            Columns(2).Caption=   "Reference ID"
            Columns(2).Name =   "reference_ID"
            Columns(2).DataField=   "reference_ID"
            Columns(2).FieldLen=   256
            Columns(3).Width=   4128
            Columns(3).Caption=   "Character Name (as Displayed)"
            Columns(3).Name =   "Character_name_decoded"
            Columns(3).DataField=   "character_name"
            Columns(3).FieldLen=   256
            Columns(4).Width=   4128
            Columns(4).Caption=   "Character Note"
            Columns(4).Name =   "character_note_decoded"
            Columns(4).DataField=   "character_note"
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Caption=   "Character Image ClipID"
            Columns(5).Name =   "image_clipID"
            Columns(5).DataField=   "image_clipID"
            Columns(5).FieldLen=   256
            Columns(6).Width=   4128
            Columns(6).Caption=   "Character Name (Lang 2)"
            Columns(6).Name =   "character_name_Decoded2"
            Columns(6).DataField=   "character_name2"
            Columns(6).FieldLen=   256
            Columns(7).Width=   4128
            Columns(7).Caption=   "Character Note (Lang 2)"
            Columns(7).Name =   "character_note_decoded2"
            Columns(7).DataField=   "character_note2"
            Columns(7).FieldLen=   256
            Columns(8).Width=   4128
            Columns(8).Caption=   "Character Name (Lang 3)"
            Columns(8).Name =   "character_name_Decoded3"
            Columns(8).DataField=   "character_name3"
            Columns(8).FieldLen=   256
            Columns(9).Width=   4128
            Columns(9).Caption=   "Character Note (Lang 3)"
            Columns(9).Name =   "character_note_decoded3"
            Columns(9).DataField=   "character_note3"
            Columns(9).FieldLen=   256
            Columns(10).Width=   4128
            Columns(10).Caption=   "Character Name (Lang 4)"
            Columns(10).Name=   "character_name_Decoded4"
            Columns(10).DataField=   "character_name4"
            Columns(10).FieldLen=   256
            Columns(11).Width=   4128
            Columns(11).Caption=   "Character Note (Lang 4)"
            Columns(11).Name=   "character_note_decoded4"
            Columns(11).DataField=   "character_note4"
            Columns(11).FieldLen=   256
            _ExtentX        =   20241
            _ExtentY        =   3201
            _StockProps     =   79
            Caption         =   "Characters"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnFilmRoles 
            Height          =   795
            Left            =   720
            TabIndex        =   294
            Tag             =   "CLEAR"
            Top             =   5100
            Width           =   2175
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16761024
            RowHeight       =   423
            ExtraHeight     =   26
            Columns(0).Width=   3200
            Columns(0).Caption=   "advisorysystem"
            Columns(0).Name =   "advisorysystem"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   3836
            _ExtentY        =   1402
            _StockProps     =   77
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnBilling 
            Height          =   795
            Left            =   1380
            TabIndex        =   305
            Tag             =   "CLEAR"
            Top             =   1500
            Width           =   2175
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16761024
            RowHeight       =   423
            ExtraHeight     =   26
            Columns(0).Width=   3200
            Columns(0).Caption=   "advisorysystem"
            Columns(0).Name =   "advisorysystem"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   3836
            _ExtentY        =   1402
            _StockProps     =   77
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdCast 
            Bindings        =   "frmiTunesPackage.frx":0987
            Height          =   2235
            Left            =   60
            TabIndex        =   414
            Top             =   420
            Width           =   11475
            _Version        =   196617
            AllowAddNew     =   -1  'True
            AllowDelete     =   -1  'True
            SelectTypeRow   =   3
            RowHeight       =   423
            ExtraHeight     =   53
            Columns.Count   =   19
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "iTunes_CastID"
            Columns(0).Name =   "iTunes_CastID"
            Columns(0).DataField=   "itunes_castID"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "itunes_packageID"
            Columns(1).Name =   "itunes_packageID"
            Columns(1).DataField=   "iTunes_PackageID"
            Columns(1).FieldLen=   256
            Columns(2).Width=   2805
            Columns(2).Caption=   "Name (as Displayed)"
            Columns(2).Name =   "Decoded_name"
            Columns(2).DataField=   "display_name"
            Columns(2).FieldLen=   256
            Columns(3).Width=   1402
            Columns(3).Caption=   "Apple ID"
            Columns(3).Name =   "apple_ID"
            Columns(3).DataField=   "apple_ID"
            Columns(3).FieldLen=   256
            Columns(4).Width=   1058
            Columns(4).Caption=   "Billing"
            Columns(4).Name =   "billing"
            Columns(4).DataField=   "billing"
            Columns(4).FieldLen=   256
            Columns(5).Width=   3201
            Columns(5).Caption=   "Description"
            Columns(5).Name =   "description"
            Columns(5).DataField=   "description"
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "Description"
            Columns(6).Name =   "description_decoded"
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "Name (Language 2)"
            Columns(7).Name =   "Decoded_name2"
            Columns(7).DataField=   "display_name2"
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "Apple ID 2"
            Columns(8).Name =   "apple_ID2"
            Columns(8).DataField=   "apple_ID2"
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "description2"
            Columns(9).Name =   "description2"
            Columns(9).DataField=   "description2"
            Columns(9).FieldLen=   256
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "Descripiton (Lang 2)"
            Columns(10).Name=   "description_decoded2"
            Columns(10).DataField=   "Column 17"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(11).Width=   3200
            Columns(11).Visible=   0   'False
            Columns(11).Caption=   "Name (Language 3)"
            Columns(11).Name=   "Decoded_name3"
            Columns(11).DataField=   "display_name3"
            Columns(11).FieldLen=   256
            Columns(12).Width=   3200
            Columns(12).Visible=   0   'False
            Columns(12).Caption=   "Apple ID 3"
            Columns(12).Name=   "apple_ID3"
            Columns(12).DataField=   "apple_ID3"
            Columns(12).FieldLen=   256
            Columns(13).Width=   3200
            Columns(13).Visible=   0   'False
            Columns(13).Caption=   "description3"
            Columns(13).Name=   "description3"
            Columns(13).DataField=   "description3"
            Columns(13).FieldLen=   256
            Columns(14).Width=   3200
            Columns(14).Visible=   0   'False
            Columns(14).Caption=   "Description (Lang 3)"
            Columns(14).Name=   "description_decoded3"
            Columns(14).DataField=   "Column 18"
            Columns(14).DataType=   8
            Columns(14).FieldLen=   256
            Columns(15).Width=   3200
            Columns(15).Visible=   0   'False
            Columns(15).Caption=   "Name (Language 4)"
            Columns(15).Name=   "Decoded_name4"
            Columns(15).DataField=   "display_name4"
            Columns(15).FieldLen=   256
            Columns(16).Width=   3200
            Columns(16).Visible=   0   'False
            Columns(16).Caption=   "Apple ID 4"
            Columns(16).Name=   "apple_ID4"
            Columns(16).DataField=   "apple_ID4"
            Columns(16).FieldLen=   256
            Columns(17).Width=   3200
            Columns(17).Visible=   0   'False
            Columns(17).Caption=   "description4"
            Columns(17).Name=   "description4"
            Columns(17).DataField=   "description4"
            Columns(17).FieldLen=   256
            Columns(18).Width=   3200
            Columns(18).Visible=   0   'False
            Columns(18).Caption=   "Description (Lang 4)"
            Columns(18).Name=   "description_decoded4"
            Columns(18).DataField=   "Column 19"
            Columns(18).DataType=   8
            Columns(18).FieldLen=   256
            _ExtentX        =   20241
            _ExtentY        =   3942
            _StockProps     =   79
            Caption         =   "Cast"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnUsage 
            Height          =   795
            Left            =   -71340
            TabIndex        =   415
            Tag             =   "CLEAR"
            Top             =   1260
            Width           =   2175
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16761024
            RowHeight       =   423
            ExtraHeight     =   26
            Columns(0).Width=   3200
            Columns(0).Caption=   "advisorysystem"
            Columns(0).Name =   "advisorysystem"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   3836
            _ExtentY        =   1402
            _StockProps     =   77
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdCueSheet 
            Bindings        =   "frmiTunesPackage.frx":099D
            Height          =   2535
            Left            =   -74820
            TabIndex        =   416
            Top             =   540
            Width           =   7755
            _Version        =   196617
            AllowAddNew     =   -1  'True
            AllowDelete     =   -1  'True
            SelectTypeRow   =   3
            RowHeight       =   423
            ExtraHeight     =   53
            Columns.Count   =   11
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "iTunes_cuesheetID"
            Columns(0).Name =   "iTunes_cuesheetID"
            Columns(0).DataField=   "iTunes_cuesheetID"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "iTunes_PackageID"
            Columns(1).Name =   "iTunes_PackageID"
            Columns(1).DataField=   "iTunes_PackageID"
            Columns(1).FieldLen=   256
            Columns(2).Width=   2090
            Columns(2).Caption=   "Sequence No."
            Columns(2).Name =   "sequence_number"
            Columns(2).DataField=   "sequence_number"
            Columns(2).FieldLen=   256
            Columns(3).Width=   2275
            Columns(3).Caption=   "Start Timecode"
            Columns(3).Name =   "start_timecode"
            Columns(3).DataField=   "start_timecode"
            Columns(3).FieldLen=   256
            Columns(4).Width=   1931
            Columns(4).Caption=   "Duration"
            Columns(4).Name =   "duration"
            Columns(4).DataField=   "duration"
            Columns(4).FieldLen=   256
            Columns(5).Width=   4366
            Columns(5).Caption=   "Title"
            Columns(5).Name =   "title"
            Columns(5).DataField=   "title"
            Columns(5).FieldLen=   256
            Columns(6).Width=   2223
            Columns(6).Caption=   "ISRC"
            Columns(6).Name =   "isrc"
            Columns(6).DataField=   "isrc"
            Columns(6).FieldLen=   256
            Columns(7).Width=   2090
            Columns(7).Caption=   "ISWC"
            Columns(7).Name =   "iswc"
            Columns(7).DataField=   "iswc"
            Columns(7).FieldLen=   256
            Columns(8).Width=   2566
            Columns(8).Caption=   "Copyright C Line"
            Columns(8).Name =   "copyright_cline"
            Columns(8).DataField=   "copyright_cline"
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Caption=   "Copyright P Line"
            Columns(9).Name =   "copyright_pline"
            Columns(9).DataField=   "copyright_pline"
            Columns(9).FieldLen=   256
            Columns(10).Width=   3200
            Columns(10).Caption=   "Usage"
            Columns(10).Name=   "usage"
            Columns(10).DataField=   "usage"
            Columns(10).FieldLen=   256
            _ExtentX        =   13679
            _ExtentY        =   4471
            _StockProps     =   79
            Caption         =   "Cue Sheet"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdArtist2 
            Bindings        =   "frmiTunesPackage.frx":09B7
            Height          =   2175
            Left            =   -74820
            TabIndex        =   417
            Top             =   3120
            Width           =   7755
            _Version        =   196617
            AllowAddNew     =   -1  'True
            AllowDelete     =   -1  'True
            SelectTypeRow   =   3
            RowHeight       =   423
            ExtraHeight     =   53
            Columns.Count   =   4
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "iTunes_artistID"
            Columns(0).Name =   "iTunes_artistID"
            Columns(0).DataField=   "iTunes_artistID"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "iTunesPackageID"
            Columns(1).Name =   "iTunesPackageID"
            Columns(1).DataField=   "iTunes_PackageID"
            Columns(1).FieldLen=   256
            Columns(2).Width=   9260
            Columns(2).Caption=   "Name"
            Columns(2).Name =   "name"
            Columns(2).DataField=   "name"
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "iTunes_cuesheetID"
            Columns(3).Name =   "iTunes_cuesheetID"
            Columns(3).DataField=   "iTunes_cuesheetID"
            Columns(3).FieldLen=   256
            _ExtentX        =   13679
            _ExtentY        =   3836
            _StockProps     =   79
            Caption         =   "Package Artists"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdArtistRoles2 
            Bindings        =   "frmiTunesPackage.frx":09D0
            Height          =   2055
            Left            =   -74820
            TabIndex        =   418
            Top             =   5340
            Width           =   7755
            _Version        =   196617
            AllowAddNew     =   -1  'True
            AllowDelete     =   -1  'True
            SelectTypeRow   =   3
            RowHeight       =   423
            ExtraHeight     =   53
            Columns.Count   =   3
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "iTunes_artist_roleID"
            Columns(0).Name =   "iTunes_artist_roleID"
            Columns(0).DataField=   "iTunes_artist_roleID"
            Columns(0).FieldLen=   256
            Columns(1).Width=   6615
            Columns(1).Caption=   "Role"
            Columns(1).Name =   "iTunes_artist_role"
            Columns(1).DataField=   "iTunes_artist_role"
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "iTunes_artistID"
            Columns(2).Name =   "iTunes_artistID"
            Columns(2).DataField=   "iTunes_artistID"
            Columns(2).FieldLen=   256
            _ExtentX        =   13679
            _ExtentY        =   3625
            _StockProps     =   79
            Caption         =   "Cue Sheet Artist Roles"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnArtistRoles 
            Height          =   795
            Left            =   -70440
            TabIndex        =   419
            Tag             =   "CLEAR"
            Top             =   4740
            Width           =   2175
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16761024
            RowHeight       =   423
            ExtraHeight     =   26
            Columns(0).Width=   3200
            Columns(0).Caption=   "advisorysystem"
            Columns(0).Name =   "advisorysystem"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   3836
            _ExtentY        =   1402
            _StockProps     =   77
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdArtistRoles1 
            Bindings        =   "frmiTunesPackage.frx":09E8
            Height          =   2835
            Left            =   -74820
            TabIndex        =   420
            Top             =   3720
            Width           =   7755
            _Version        =   196617
            AllowAddNew     =   -1  'True
            AllowDelete     =   -1  'True
            SelectTypeRow   =   3
            RowHeight       =   423
            ExtraHeight     =   53
            Columns.Count   =   3
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "iTunes_artist_roleID"
            Columns(0).Name =   "iTunes_artist_roleID"
            Columns(0).DataField=   "iTunes_artist_roleID"
            Columns(0).FieldLen=   256
            Columns(1).Width=   6615
            Columns(1).Caption=   "Role"
            Columns(1).Name =   "iTunes_artist_role"
            Columns(1).DataField=   "iTunes_artist_role"
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "iTunes_artistID"
            Columns(2).Name =   "iTunes_artistID"
            Columns(2).DataField=   "iTunes_artistID"
            Columns(2).FieldLen=   256
            _ExtentX        =   13679
            _ExtentY        =   5001
            _StockProps     =   79
            Caption         =   "Package Artist Roles"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdArtist1 
            Bindings        =   "frmiTunesPackage.frx":0A00
            Height          =   3135
            Left            =   -74820
            TabIndex        =   421
            Top             =   480
            Width           =   7755
            _Version        =   196617
            AllowAddNew     =   -1  'True
            AllowDelete     =   -1  'True
            SelectTypeRow   =   3
            RowHeight       =   423
            ExtraHeight     =   53
            Columns.Count   =   5
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "iTunes_artistID"
            Columns(0).Name =   "iTunes_artistID"
            Columns(0).DataField=   "iTunes_artistID"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "iTunesPackageID"
            Columns(1).Name =   "iTunesPackageID"
            Columns(1).DataField=   "iTunes_PackageID"
            Columns(1).FieldLen=   256
            Columns(2).Width=   9260
            Columns(2).Caption=   "Name"
            Columns(2).Name =   "name"
            Columns(2).DataField=   "name"
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Caption=   "Primary Artist"
            Columns(3).Name =   "primaryartist"
            Columns(3).DataField=   "primaryartist"
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "iTunes_cuesheetID"
            Columns(4).Name =   "iTunes_cuesheetID"
            Columns(4).DataField=   "iTunes_cuesheetID"
            Columns(4).FieldLen=   256
            _ExtentX        =   13679
            _ExtentY        =   5530
            _StockProps     =   79
            Caption         =   "Package Artists"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdCrew 
            Bindings        =   "frmiTunesPackage.frx":0A19
            Height          =   2895
            Left            =   60
            TabIndex        =   422
            Top             =   4560
            Width           =   11475
            _Version        =   196617
            AllowAddNew     =   -1  'True
            AllowDelete     =   -1  'True
            SelectTypeRow   =   3
            RowHeight       =   423
            ExtraHeight     =   53
            Columns.Count   =   12
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "iTunes_CastID"
            Columns(0).Name =   "iTunes_CrewID"
            Columns(0).DataField=   "itunes_crewID"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "itunes_packageID"
            Columns(1).Name =   "itunes_packageID"
            Columns(1).DataField=   "iTunes_PackageID"
            Columns(1).FieldLen=   256
            Columns(2).Width=   4419
            Columns(2).Caption=   "Name (as Displayed)"
            Columns(2).Name =   "Decoded_name"
            Columns(2).DataField=   "display_name"
            Columns(2).FieldLen=   256
            Columns(3).Width=   2275
            Columns(3).Caption=   "Apple ID"
            Columns(3).Name =   "apple_ID"
            Columns(3).DataField=   "apple_ID"
            Columns(3).FieldLen=   256
            Columns(4).Width=   1164
            Columns(4).Caption=   "Billing"
            Columns(4).Name =   "billing"
            Columns(4).DataField=   "billing"
            Columns(4).FieldLen=   256
            Columns(5).Width=   4419
            Columns(5).Caption=   "Role"
            Columns(5).Name =   "role"
            Columns(5).DataField=   "role"
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Caption=   "Apple ID 2"
            Columns(6).Name =   "apple_ID2"
            Columns(6).DataField=   "apple_ID2"
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Caption=   "Name (Language 2)"
            Columns(7).Name =   "Decoded_name2"
            Columns(7).DataField=   "display_name2"
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Caption=   "Apple ID 3"
            Columns(8).Name =   "apple_ID3"
            Columns(8).DataField=   "apple_ID3"
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Caption=   "Name (Language 3)"
            Columns(9).Name =   "Decoded_name3"
            Columns(9).DataField=   "display_name3"
            Columns(9).FieldLen=   256
            Columns(10).Width=   3200
            Columns(10).Caption=   "Apple ID 4"
            Columns(10).Name=   "apple_ID4"
            Columns(10).DataField=   "apple_ID4"
            Columns(10).FieldLen=   256
            Columns(11).Width=   3200
            Columns(11).Caption=   "Name (Language 4)"
            Columns(11).Name=   "Decoded_name4"
            Columns(11).DataField=   "display_name4"
            Columns(11).FieldLen=   256
            _ExtentX        =   20241
            _ExtentY        =   5106
            _StockProps     =   79
            Caption         =   "Crew"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdCrewRole 
            Bindings        =   "frmiTunesPackage.frx":0A2F
            Height          =   1635
            Left            =   60
            TabIndex        =   423
            Top             =   7500
            Width           =   11475
            _Version        =   196617
            AllowAddNew     =   -1  'True
            AllowDelete     =   -1  'True
            SelectTypeRow   =   3
            RowHeight       =   423
            ExtraHeight     =   53
            Columns.Count   =   4
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "iTunes_Crew_RoleID"
            Columns(0).Name =   "iTunes_Crew_RoleID"
            Columns(0).DataField=   "iTunes_Crew_RoleID"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "iTunes_CrewID"
            Columns(1).Name =   "iTunes_CrewID"
            Columns(1).DataField=   "iTunes_CrewID"
            Columns(1).FieldLen=   256
            Columns(2).Width=   3519
            Columns(2).Caption=   "Reference ID"
            Columns(2).Name =   "reference_ID"
            Columns(2).DataField=   "reference_ID"
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Caption=   "Locale"
            Columns(3).Name =   "locale"
            Columns(3).DataField=   "locale"
            Columns(3).FieldLen=   256
            _ExtentX        =   20241
            _ExtentY        =   2884
            _StockProps     =   79
            Caption         =   "Dub Artist Roles"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnGenre 
         Bindings        =   "frmiTunesPackage.frx":0A46
         Height          =   795
         Left            =   -66240
         TabIndex        =   424
         Tag             =   "CLEAR"
         Top             =   6360
         Width           =   2895
         DataFieldList   =   "genre"
         ListAutoValidate=   0   'False
         _Version        =   196617
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Caption=   "genre"
         Columns(0).Name =   "genre"
         Columns(0).DataField=   "genre"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "Code"
         Columns(1).Name =   "GenreCode"
         Columns(1).DataField=   "GenreCode"
         Columns(1).FieldLen=   256
         _ExtentX        =   5106
         _ExtentY        =   1402
         _StockProps     =   77
         DataFieldToDisplay=   "genre"
      End
      Begin MSComCtl2.DTPicker datiTunesReleaseDate 
         Height          =   315
         Left            =   2280
         TabIndex        =   438
         Tag             =   "NOCLEAR"
         ToolTipText     =   "The date the tape was created"
         Top             =   3060
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   111935489
         CurrentDate     =   37870
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnISO2A 
         Bindings        =   "frmiTunesPackage.frx":0A62
         Height          =   1155
         Left            =   10680
         TabIndex        =   439
         Tag             =   "CLEAR"
         Top             =   4920
         Width           =   4335
         DataFieldList   =   "country"
         ListAutoValidate=   0   'False
         _Version        =   196617
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   2
         Columns(0).Width=   6350
         Columns(0).Caption=   "Country"
         Columns(0).Name =   "Country"
         Columns(0).DataField=   "country"
         Columns(0).FieldLen=   256
         Columns(1).Width=   1244
         Columns(1).Caption=   "Code"
         Columns(1).Name =   "Code"
         Columns(1).DataField=   "iso2acode"
         Columns(1).FieldLen=   256
         _ExtentX        =   7646
         _ExtentY        =   2037
         _StockProps     =   77
         DataFieldToDisplay=   "country"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdTVProduct 
         Bindings        =   "frmiTunesPackage.frx":0A79
         Height          =   3135
         Left            =   10320
         TabIndex        =   440
         Top             =   3960
         Width           =   7125
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         SelectTypeRow   =   3
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   8
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "itunes_productID"
         Columns(0).Name =   "itunes_productID"
         Columns(0).DataField=   "itunes_productID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "eventID"
         Columns(1).Name =   "eventID"
         Columns(1).DataField=   "eventID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   4710
         Columns(2).Caption=   "Territory"
         Columns(2).Name =   "territory"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "territorycode"
         Columns(3).Name =   "territorycode"
         Columns(3).DataField=   "territory"
         Columns(3).FieldLen=   256
         Columns(4).Width=   2408
         Columns(4).Caption=   "Sales Start Date"
         Columns(4).Name =   "salesstartdate"
         Columns(4).DataField=   "salesstartdate"
         Columns(4).DataType=   7
         Columns(4).FieldLen=   256
         Columns(4).Style=   1
         Columns(5).Width=   2381
         Columns(5).Caption=   "Cleared For Sale"
         Columns(5).Name =   "clearedforsale"
         Columns(5).DataField=   "clearedforsale"
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "packageID"
         Columns(6).Name =   "packageID"
         Columns(6).DataField=   "itunes_packageID"
         Columns(6).FieldLen=   256
         Columns(7).Width=   1958
         Columns(7).Caption=   "Bundle Only"
         Columns(7).Name =   "bundleonly"
         Columns(7).DataField=   "bundleonly"
         Columns(7).FieldLen=   256
         _ExtentX        =   12568
         _ExtentY        =   5530
         _StockProps     =   79
         Caption         =   "iTunes Product Details"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCountry 
         Bindings        =   "frmiTunesPackage.frx":0A92
         Height          =   315
         Left            =   -73200
         TabIndex        =   441
         ToolTipText     =   "The company this tape belongs to"
         Top             =   2280
         Width           =   3255
         DataFieldList   =   "country"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   5424
         Columns(0).Caption=   "country"
         Columns(0).Name =   "country"
         Columns(0).DataField=   "country"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "iso2code"
         Columns(1).Name =   "iso2acode"
         Columns(1).DataField=   "iso2acode"
         Columns(1).FieldLen=   256
         _ExtentX        =   5741
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "country"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdGenre 
         Bindings        =   "frmiTunesPackage.frx":0AA9
         Height          =   2835
         Left            =   -66360
         TabIndex        =   442
         Top             =   6300
         Width           =   3315
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         SelectTypeRow   =   3
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "iTunes_GenreID"
         Columns(0).Name =   "iTunes_GenreID"
         Columns(0).DataField=   "iTunes_GenreID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "iTunes_PackageID"
         Columns(1).Name =   "iTunes_PackageID"
         Columns(1).DataField=   "iTunes_PackageID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   4498
         Columns(2).Caption=   "Genre"
         Columns(2).Name =   "Genre"
         Columns(2).DataField=   "genre"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "GenreCode"
         Columns(3).Name =   "GenreCode"
         Columns(3).DataField=   "GenreCode"
         Columns(3).FieldLen=   256
         _ExtentX        =   5847
         _ExtentY        =   5001
         _StockProps     =   79
         Caption         =   "Genres"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnAdvisory 
         Height          =   795
         Left            =   10560
         TabIndex        =   443
         Tag             =   "CLEAR"
         Top             =   1860
         Width           =   2235
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         MaxDropDownItems=   16
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   3200
         Columns(0).Caption=   "advisory"
         Columns(0).Name =   "advisory"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   3942
         _ExtentY        =   1402
         _StockProps     =   77
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnAdvisorySystem 
         Height          =   795
         Left            =   10500
         TabIndex        =   444
         Tag             =   "CLEAR"
         Top             =   1020
         Width           =   2175
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   3200
         Columns(0).Caption=   "advisorysystem"
         Columns(0).Name =   "advisorysystem"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   3836
         _ExtentY        =   1402
         _StockProps     =   77
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdiTunesAdvisory 
         Bindings        =   "frmiTunesPackage.frx":0AC1
         Height          =   2895
         Left            =   10320
         TabIndex        =   445
         Top             =   960
         Width           =   4335
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         SelectTypeRow   =   3
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "iTunes_Clip_AdvisoryID"
         Columns(0).Name =   "iTunes_Clip_AdvisoryID"
         Columns(0).DataField=   "iTunes_Clip_AdvisoryID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "eventID"
         Columns(1).Name =   "eventID"
         Columns(1).DataField=   "eventID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Advisory System"
         Columns(2).Name =   "advisorysystem"
         Columns(2).DataField=   "advisorysystem"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "Advisory Item"
         Columns(3).Name =   "advisory"
         Columns(3).DataField=   "advisory"
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "itunes_PackageID"
         Columns(4).Name =   "itunes_PackageID"
         Columns(4).DataField=   "itunes_packageID"
         Columns(4).FieldLen=   256
         _ExtentX        =   7646
         _ExtentY        =   5106
         _StockProps     =   79
         Caption         =   "iTunes Content Advisory"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo txtVideoLanguage 
         Bindings        =   "frmiTunesPackage.frx":0ADB
         Height          =   315
         Left            =   2280
         TabIndex        =   446
         ToolTipText     =   "The company this tape belongs to"
         Top             =   1620
         Width           =   3855
         DataFieldList   =   "description"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "description"
         Columns(0).Name =   "description"
         Columns(0).DataField=   "description"
         Columns(0).FieldLen=   256
         Columns(1).Width=   8043
         Columns(1).Caption=   "information"
         Columns(1).Name =   "information"
         Columns(1).DataField=   "information"
         Columns(1).FieldLen=   256
         _ExtentX        =   6800
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "description"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdFilmProduct 
         Bindings        =   "frmiTunesPackage.frx":0AFA
         Height          =   1575
         Left            =   -74820
         TabIndex        =   447
         Top             =   9720
         Width           =   23535
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         SelectTypeRow   =   3
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   18
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "itunes_productID"
         Columns(0).Name =   "itunes_productID"
         Columns(0).DataField=   "itunes_productID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "eventID"
         Columns(1).Name =   "eventID"
         Columns(1).DataField=   "eventID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "PackageID"
         Columns(2).Name =   "PackageID"
         Columns(2).DataField=   "itunes_PackageID"
         Columns(2).FieldLen=   256
         Columns(3).Width=   4233
         Columns(3).Caption=   "Territory"
         Columns(3).Name =   "territory"
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "territorycode"
         Columns(4).Name =   "territorycode"
         Columns(4).DataField=   "territory"
         Columns(4).FieldLen=   256
         Columns(5).Width=   2302
         Columns(5).Caption=   "Sales Start Date"
         Columns(5).Name =   "salesstartdate"
         Columns(5).DataField=   "salesstartdate"
         Columns(5).DataType=   17
         Columns(5).FieldLen=   256
         Columns(5).Style=   1
         Columns(6).Width=   2381
         Columns(6).Caption=   "Cleared For Sale"
         Columns(6).Name =   "clearedforsale"
         Columns(6).DataField=   "clearedforsale"
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Caption=   "Cleared For HD Sale"
         Columns(7).Name =   "clearedforhdsale"
         Columns(7).DataField=   "clearedforhdsale"
         Columns(7).FieldLen=   256
         Columns(8).Width=   2223
         Columns(8).Caption=   "Sales End Date"
         Columns(8).Name =   "salesenddate"
         Columns(8).DataField=   "salesenddate"
         Columns(8).FieldLen=   256
         Columns(8).Style=   1
         Columns(9).Width=   2858
         Columns(9).Caption=   "Wholesale Price Tier"
         Columns(9).Name =   "wholesalepricetier"
         Columns(9).DataField=   "wholesalepricetier"
         Columns(9).FieldLen=   256
         Columns(10).Width=   3360
         Columns(10).Caption=   "HD Wholesale Price Tier"
         Columns(10).Name=   "hdwholesalepricetier"
         Columns(10).DataField=   "hdwholesalepricetier"
         Columns(10).FieldLen=   256
         Columns(11).Width=   2990
         Columns(11).Caption=   "Pre-Order Sales Start Date"
         Columns(11).Name=   "preordersalesstartdate"
         Columns(11).DataField=   "preordersalesstartdate"
         Columns(11).FieldLen=   256
         Columns(11).Style=   1
         Columns(12).Width=   2302
         Columns(12).Caption=   "Cleared for VOD"
         Columns(12).Name=   "clearedforvod"
         Columns(12).DataField=   "clearedforvod"
         Columns(12).FieldLen=   256
         Columns(13).Width=   1773
         Columns(13).Caption=   "VOD Type"
         Columns(13).Name=   "vodtype"
         Columns(13).DataField=   "vodtype"
         Columns(13).FieldLen=   256
         Columns(14).Width=   3200
         Columns(14).Caption=   "Available for VOD Date"
         Columns(14).Name=   "availableforvoddate"
         Columns(14).DataField=   "availableforvoddate"
         Columns(14).FieldLen=   256
         Columns(14).Style=   1
         Columns(15).Width=   2910
         Columns(15).Caption=   "Cleared For HD Vod"
         Columns(15).Name=   "clearedforhdvod"
         Columns(15).DataField=   "clearedforhdvod"
         Columns(15).FieldLen=   256
         Columns(16).Width=   3519
         Columns(16).Caption=   "Unavailable for VOD Date"
         Columns(16).Name=   "unavailableforvoddate"
         Columns(16).DataField=   "unavailableforvoddate"
         Columns(16).FieldLen=   256
         Columns(16).Style=   1
         Columns(17).Width=   3122
         Columns(17).Caption=   "Physical Release Date"
         Columns(17).Name=   "physicalreleasedate"
         Columns(17).DataField=   "physicalreleasedate"
         Columns(17).FieldLen=   256
         Columns(17).Style=   1
         _ExtentX        =   41513
         _ExtentY        =   2778
         _StockProps     =   79
         Caption         =   "iTunes Product Details"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo txtMetaDataLanguage 
         Bindings        =   "frmiTunesPackage.frx":0B13
         Height          =   315
         Left            =   2280
         TabIndex        =   448
         ToolTipText     =   "The company this tape belongs to"
         Top             =   540
         Width           =   3855
         DataFieldList   =   "description"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "description"
         Columns(0).Name =   "description"
         Columns(0).DataField=   "description"
         Columns(0).FieldLen=   256
         Columns(1).Width=   8043
         Columns(1).Caption=   "information"
         Columns(1).Name =   "information"
         Columns(1).DataField=   "information"
         Columns(1).FieldLen=   256
         _ExtentX        =   6800
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "description"
      End
      Begin MSComCtl2.DTPicker datOriginalAirDate 
         Height          =   315
         Left            =   -72840
         TabIndex        =   518
         Tag             =   "NOCLEAR"
         ToolTipText     =   "The date the tape was created"
         Top             =   6300
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   111935489
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datSDAmazonOfferStart 
         Height          =   315
         Left            =   -72840
         TabIndex        =   520
         Tag             =   "NOCLEAR"
         ToolTipText     =   "The date the tape was created"
         Top             =   6660
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   111935489
         CurrentDate     =   37870
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo txtAmazonLanguage 
         Bindings        =   "frmiTunesPackage.frx":0B35
         Height          =   315
         Left            =   -72840
         TabIndex        =   531
         ToolTipText     =   "The company this tape belongs to"
         Top             =   900
         Width           =   3855
         DataFieldList   =   "description"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "description"
         Columns(0).Name =   "description"
         Columns(0).DataField=   "description"
         Columns(0).FieldLen=   256
         Columns(1).Width=   8043
         Columns(1).Caption=   "information"
         Columns(1).Name =   "information"
         Columns(1).DataField=   "information"
         Columns(1).FieldLen=   256
         _ExtentX        =   6800
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "description"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo txtAmazonTerritory 
         Bindings        =   "frmiTunesPackage.frx":0B53
         Height          =   315
         Left            =   -72840
         TabIndex        =   533
         ToolTipText     =   "The company this tape belongs to"
         Top             =   540
         Width           =   3855
         DataFieldList   =   "iso2acode"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4048
         Columns(0).Caption=   "country"
         Columns(0).Name =   "country"
         Columns(0).DataField=   "country"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "iso2acode"
         Columns(1).Name =   "iso2acode"
         Columns(1).DataField=   "iso2acode"
         Columns(1).FieldLen=   256
         _ExtentX        =   6800
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "iso2acode"
      End
      Begin MSComCtl2.DTPicker datHDVODAmazonOfferStart 
         Height          =   315
         Left            =   -72840
         TabIndex        =   550
         Tag             =   "NOCLEAR"
         ToolTipText     =   "The date the tape was created"
         Top             =   7740
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   111935489
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datSDVODAmazonOfferStart 
         Height          =   315
         Left            =   -72840
         TabIndex        =   551
         Tag             =   "NOCLEAR"
         ToolTipText     =   "The date the tape was created"
         Top             =   7380
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   111935489
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datHDAmazonOfferStart 
         Height          =   315
         Left            =   -72840
         TabIndex        =   552
         Tag             =   "NOCLEAR"
         ToolTipText     =   "The date the tape was created"
         Top             =   7020
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   111935489
         CurrentDate     =   37870
      End
      Begin VB.Label lblCaption 
         Caption         =   "Subtype"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   221
         Left            =   6300
         TabIndex        =   564
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lblCaption 
         Caption         =   "Xbox Licensor"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   217
         Left            =   -74700
         TabIndex        =   562
         Top             =   1320
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Offer HD VOD Start Date"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   186
         Left            =   -74700
         TabIndex        =   555
         Top             =   7800
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Offer SD VOD Start Date"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   185
         Left            =   -74700
         TabIndex        =   554
         Top             =   7440
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Offer HD Start Date"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   183
         Left            =   -74700
         TabIndex        =   553
         Top             =   7080
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Studio"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   177
         Left            =   -74700
         TabIndex        =   535
         Top             =   3120
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Language"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   176
         Left            =   -74700
         TabIndex        =   532
         Top             =   960
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Season Number"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   175
         Left            =   -74700
         TabIndex        =   530
         Top             =   4200
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Audio Clip ID"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   174
         Left            =   -56100
         TabIndex        =   526
         Top             =   3600
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Poster Clip ID"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   173
         Left            =   -56100
         TabIndex        =   525
         Top             =   3240
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Video Clip ID"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   172
         Left            =   -56100
         TabIndex        =   523
         Top             =   2880
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Offer SD Start Date"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   171
         Left            =   -74700
         TabIndex        =   521
         Top             =   6720
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Original Release Date"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   170
         Left            =   -74700
         TabIndex        =   519
         Top             =   6360
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Rating"
         ForeColor       =   &H00000040&
         Height          =   255
         Index           =   169
         Left            =   -74700
         TabIndex        =   517
         Top             =   6000
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Rating Type"
         ForeColor       =   &H00000040&
         Height          =   255
         Index           =   168
         Left            =   -74700
         TabIndex        =   516
         Top             =   5640
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Genre"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   166
         Left            =   -74700
         TabIndex        =   515
         Top             =   5280
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Running Order Position"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   165
         Left            =   -74700
         TabIndex        =   511
         Top             =   4920
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Episode Long Synopsis"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   164
         Left            =   -67860
         TabIndex        =   508
         Top             =   7560
         Width           =   2235
      End
      Begin VB.Label lblCaption 
         Caption         =   "Episode Short Synopsis"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   163
         Left            =   -67860
         TabIndex        =   507
         Top             =   6120
         Width           =   2235
      End
      Begin VB.Label lblCaption 
         Caption         =   "Season Long Synopsis"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   162
         Left            =   -67860
         TabIndex        =   506
         Top             =   4740
         Width           =   2235
      End
      Begin VB.Label lblCaption 
         Caption         =   "Season Short Synopsis"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   161
         Left            =   -67860
         TabIndex        =   505
         Top             =   3420
         Width           =   2235
      End
      Begin VB.Label lblCaption 
         Caption         =   "Series Long Synopsis"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   160
         Left            =   -67920
         TabIndex        =   504
         Top             =   2040
         Width           =   2235
      End
      Begin VB.Label lblCaption 
         Caption         =   "Series Short Synopsis"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   159
         Left            =   -67920
         TabIndex        =   498
         Top             =   600
         Width           =   2235
      End
      Begin VB.Label lblCaption 
         Caption         =   "Episode Title"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   158
         Left            =   -74700
         TabIndex        =   496
         Top             =   4560
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Season Title"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   157
         Left            =   -74700
         TabIndex        =   495
         Top             =   3840
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Series Name"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   155
         Left            =   -74700
         TabIndex        =   494
         Top             =   3480
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Copyright Year"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   154
         Left            =   -74700
         TabIndex        =   490
         Top             =   2760
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Copyright Holder"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   153
         Left            =   -74700
         TabIndex        =   489
         Top             =   2400
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Season Unique ID"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   152
         Left            =   -74700
         TabIndex        =   486
         Top             =   2040
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Series Unique ID"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   151
         Left            =   -74700
         TabIndex        =   484
         Top             =   1680
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Territory"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   240
         Left            =   -74700
         TabIndex        =   482
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "au-tv rating"
         Height          =   255
         Index           =   67
         Left            =   180
         TabIndex        =   481
         Top             =   6960
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "ca-tv rating"
         Height          =   255
         Index           =   71
         Left            =   180
         TabIndex        =   480
         Top             =   7320
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "de-tv rating"
         Height          =   255
         Index           =   72
         Left            =   180
         TabIndex        =   479
         Top             =   7680
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "fr-tv rating"
         Height          =   255
         Index           =   78
         Left            =   180
         TabIndex        =   478
         Top             =   8040
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "uk-tv rating"
         Height          =   255
         Index           =   79
         Left            =   3960
         TabIndex        =   477
         Top             =   6960
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "us-tv rating"
         Height          =   255
         Index           =   83
         Left            =   3960
         TabIndex        =   476
         Top             =   7320
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "jp-tv rating"
         Height          =   255
         Index           =   85
         Left            =   3960
         TabIndex        =   475
         Top             =   7680
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Episode Production Number"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   66
         Left            =   180
         TabIndex        =   474
         Top             =   2040
         Width           =   2235
      End
      Begin VB.Label lblCaption 
         Caption         =   "Container ID"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   68
         Left            =   180
         TabIndex        =   473
         Top             =   2400
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Container Position"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   69
         Left            =   180
         TabIndex        =   472
         Top             =   2760
         Width           =   1935
      End
      Begin VB.Label lblCaption 
         Caption         =   "Release Date"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   70
         Left            =   180
         TabIndex        =   471
         Top             =   3120
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Description"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   76
         Left            =   180
         TabIndex        =   470
         Top             =   3840
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Preview Start Time"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   80
         Left            =   180
         TabIndex        =   469
         Top             =   6600
         Width           =   1935
      End
      Begin VB.Label lblCaption 
         Caption         =   "Film Subtype"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   19
         Left            =   -74760
         TabIndex        =   468
         Top             =   540
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Country of Origin"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   22
         Left            =   -74760
         TabIndex        =   467
         Top             =   2340
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Production Company"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   35
         Left            =   -74760
         TabIndex        =   466
         Top             =   1260
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "UPC"
         Height          =   255
         Index           =   36
         Left            =   -74760
         TabIndex        =   465
         Top             =   1620
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "AMP Video ID"
         Height          =   255
         Index           =   37
         Left            =   -74760
         TabIndex        =   464
         Top             =   1980
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Video Language"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   40
         Left            =   180
         TabIndex        =   463
         Top             =   1680
         Width           =   2235
      End
      Begin VB.Label lblCaption 
         Caption         =   "Crop Left"
         Height          =   195
         Index           =   92
         Left            =   180
         TabIndex        =   462
         Top             =   9120
         Width           =   975
      End
      Begin VB.Label lblCaption 
         Caption         =   "Crop Top"
         Height          =   195
         Index           =   93
         Left            =   180
         TabIndex        =   461
         Top             =   8400
         Width           =   975
      End
      Begin VB.Label lblCaption 
         Caption         =   "Crop Right"
         Height          =   195
         Index           =   94
         Left            =   180
         TabIndex        =   460
         Top             =   9480
         Width           =   975
      End
      Begin VB.Label lblCaption 
         Caption         =   "Crop Bottom"
         Height          =   195
         Index           =   95
         Left            =   180
         TabIndex        =   459
         Top             =   8760
         Width           =   975
      End
      Begin VB.Label lblCaption 
         Caption         =   "Technical Comments"
         Height          =   255
         Index           =   96
         Left            =   180
         TabIndex        =   458
         Top             =   5280
         Width           =   1935
      End
      Begin VB.Label lblCaption 
         Caption         =   "Video Title"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   1
         Left            =   180
         TabIndex        =   457
         Top             =   960
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Copyright Line"
         Height          =   255
         Index           =   73
         Left            =   180
         TabIndex        =   456
         Top             =   3480
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Metadata Language"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   34
         Left            =   180
         TabIndex        =   455
         Top             =   600
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Multi-Language Package?"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   124
         Left            =   -69480
         TabIndex        =   454
         Top             =   2340
         Width           =   1935
      End
      Begin VB.Label lblCaption 
         Caption         =   "Provider"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   18
         Left            =   -71460
         TabIndex        =   453
         Top             =   540
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Provider"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   167
         Left            =   6300
         TabIndex        =   452
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Studio Release Title"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   195
         Left            =   -74760
         TabIndex        =   451
         Top             =   900
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Technical Comments"
         Height          =   255
         Index           =   204
         Left            =   -74760
         TabIndex        =   450
         Top             =   11460
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Studio Release Title"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   239
         Left            =   180
         TabIndex        =   449
         Top             =   1320
         Width           =   1995
      End
   End
   Begin MSAdodcLib.Adodc adoAmazonLocale 
      Height          =   330
      Left            =   9000
      Top             =   15900
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoAmazonLocale"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label lblCaption 
      Caption         =   "Search Vendor ID"
      ForeColor       =   &H00FF0000&
      Height          =   255
      Index           =   222
      Left            =   12660
      TabIndex        =   566
      Top             =   1560
      Width           =   1575
   End
   Begin VB.Label lblCaption 
      Caption         =   "Only Compulsory for iTunes. Use for Locale of MLF sep audio on Amazon"
      ForeColor       =   &H000000FF&
      Height          =   675
      Index           =   213
      Left            =   22620
      TabIndex        =   528
      Top             =   2220
      Width           =   2295
   End
   Begin VB.Label lblCaption 
      Caption         =   "Vendor's Package ID"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   65
      Left            =   16620
      TabIndex        =   30
      Top             =   1140
      Width           =   1515
   End
   Begin VB.Label lblCaption 
      Caption         =   "Captions Non-Avail Reason"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   97
      Left            =   16620
      TabIndex        =   25
      Top             =   2940
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Captions Available (USA)"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   98
      Left            =   16620
      TabIndex        =   24
      Top             =   2580
      Width           =   1935
   End
   Begin VB.Label lblCaption 
      Caption         =   "Original Spoken Locale"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   39
      Left            =   16620
      TabIndex        =   21
      Top             =   2220
      Width           =   1695
   End
   Begin VB.Label lblPackageID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   18660
      TabIndex        =   19
      Tag             =   "CLEARFIELDS"
      Top             =   720
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Vendor Offer Code"
      Height          =   255
      Index           =   99
      Left            =   16620
      TabIndex        =   18
      Top             =   1500
      Width           =   1575
   End
   Begin VB.Label lblSearchPackage 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   19920
      TabIndex        =   16
      Tag             =   "CLEARFIELDS"
      Top             =   720
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "ISAN Number"
      Height          =   255
      Index           =   84
      Left            =   16620
      TabIndex        =   13
      Top             =   1860
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "iTunes Package ID"
      Height          =   255
      Index           =   0
      Left            =   16620
      TabIndex        =   11
      Top             =   780
      Width           =   1515
   End
   Begin VB.Label lblCaption 
      Caption         =   "Package Type"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   44
      Left            =   16620
      TabIndex        =   5
      Top             =   420
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      Height          =   255
      Index           =   47
      Left            =   16620
      TabIndex        =   3
      Top             =   60
      Width           =   1095
   End
   Begin VB.Label lblCompanyID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   22680
      TabIndex        =   2
      Tag             =   "CLEARFIELDS"
      Top             =   0
      Width           =   915
   End
End
Attribute VB_Name = "frmiTunesPackage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strFilter As String
Dim m_strSearch As String
Dim m_blnNoRefresh As Boolean
Dim m_iTunes As iTunes_Structure
Dim m_blnCastNoRefresh As Boolean

Private Sub adoArtist1_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

If Not adoArtist1.Recordset.EOF Then
    adoRoles1.RecordSource = "SELECT * FROM itunes_artist_role WHERE iTunes_artistID = " & adoArtist1.Recordset("itunes_artistID") & " ORDER BY itunes_artist_role;"
    adoRoles1.ConnectionString = g_strConnection
    adoRoles1.Refresh
Else
    adoRoles1.RecordSource = "SELECT * FROM itunes_artist_role WHERE iTunes_artistID = -1 ORDER BY itunes_artist_role;"
    adoRoles1.ConnectionString = g_strConnection
    adoRoles1.Refresh
End If

End Sub

Private Sub adoArtist2_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

If Not adoArtist2.Recordset.EOF Then
    adoRoles2.RecordSource = "SELECT * FROM iTunes_artist_role WHERE iTunes_artistID = " & Val(adoArtist2.Recordset("iTunes_artistID")) & ";"
    adoRoles2.ConnectionString = g_strConnection
    adoRoles2.Refresh
Else
    adoRoles2.RecordSource = "SELECT * FROM iTunes_artist_role WHERE iTunes_artistID = -1"
    adoRoles2.ConnectionString = g_strConnection
    adoRoles2.Refresh
End If

End Sub

Private Sub adoCast_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

If Not adoCast.Recordset.EOF And m_blnNoRefresh = False Then
    
    If Val(Trim(" " & adoCast.Recordset("itunes_castID"))) <> 0 Then
        adoCastCharacter.RecordSource = "SELECT * FROM itunes_cast_character WHERE itunes_castID = " & adoCast.Recordset("itunes_castID") & ";"
        adoCastCharacter.ConnectionString = g_strConnection
        adoCastCharacter.Refresh
    Else
        adoCastCharacter.RecordSource = "SELECT * FROM itunes_cast_character WHERE itunes_castID = 0;"
        adoCastCharacter.ConnectionString = g_strConnection
        adoCastCharacter.Refresh
    End If
    
Else

    adoCastCharacter.RecordSource = "SELECT * FROM itunes_cast_character WHERE itunes_castID = 0;"
    adoCastCharacter.ConnectionString = g_strConnection
    adoCastCharacter.Refresh

End If

End Sub

Private Sub adoCrew_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

If Not adoCrew.Recordset.EOF And m_blnNoRefresh = False Then
    
    If Val(Trim(" " & adoCrew.Recordset("itunes_crewID"))) <> 0 And adoCrew.Recordset("Role") = "Dub Artist" Then
        adoRoles.RecordSource = "SELECT * FROM itunes_crew_role WHERE itunes_crewID = " & adoCrew.Recordset("itunes_crewID") & ";"
        adoRoles.ConnectionString = g_strConnection
        adoRoles.Refresh
        grdCrewRole.AllowAddNew = True
        PopulateCombo "iTunesPackageCharacterRoles", ddnDubArtistRoles, Val(lblPackageID.Caption)
        grdCrewRole.Columns("reference_ID").DropDownHwnd = ddnDubArtistRoles.hWnd
        grdCrewRole.Columns("Locale").DropDownHwnd = ddnDubArtistLocale.hWnd
    Else
        adoRoles.RecordSource = "SELECT * FROM itunes_crew_role WHERE itunes_crewID = 0;"
        adoRoles.ConnectionString = g_strConnection
        adoRoles.Refresh
        grdCrewRole.AllowAddNew = False
    End If
    
Else

    adoRoles.RecordSource = "SELECT * FROM itunes_crew_role WHERE itunes_crewID = 0;"
    adoRoles.ConnectionString = g_strConnection
    adoRoles.Refresh
    grdCrewRole.AllowAddNew = False

End If

End Sub

Private Sub adoCueSheet_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

If Not adoCueSheet.Recordset.EOF Then
    adoArtist2.RecordSource = "SELECT * FROM iTunes_artist WHERE iTunes_cuesheetID = " & Val(adoCueSheet.Recordset("iTunes_cuesheetID")) & ";"
    adoArtist2.ConnectionString = g_strConnection
    adoArtist2.Refresh
Else
    adoArtist2.RecordSource = "SELECT * FROM iTunes_artist WHERE iTunes_cuesheetID = -1;"
    adoArtist2.ConnectionString = g_strConnection
    adoArtist2.Refresh
End If

End Sub

Private Sub adoiTunesPackage_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

If Not adoiTunesPackage.Recordset.EOF Then

    lblPackageID.Caption = adoiTunesPackage.Recordset("itunes_packageID")

    lblCompanyID.Caption = adoiTunesPackage.Recordset("companyID")
    cmbCompany.Text = GetData("company", "name", "CompanyID", Val(lblCompanyID.Caption))
    cmbiTunesType.Text = Trim(" " & adoiTunesPackage.Recordset("video_type"))
    txtiTunesISAN.Text = Trim(" " & adoiTunesPackage.Recordset("video_ISAN"))
    txtiTunesVendorID.Text = Trim(" " & adoiTunesPackage.Recordset("video_vendorID"))
    txtiTunesVendorOfferCode.Text = Trim(" " & adoiTunesPackage.Recordset("vendor_offer_code"))
    txtOriginalSpokenLocale.Text = Trim(" " & adoiTunesPackage.Recordset("originalspokenlocale"))
    chkCaptionsAvailability.Value = GetFlag(adoiTunesPackage.Recordset("captionsavailable"))
    cmbCaptionsReason.Text = Trim(" " & adoiTunesPackage.Recordset("captionsreason"))
    
    Select Case cmbiTunesType.Text
    
        Case "tv"
            tabType.Tab = 0
            chkPackageUsed(0).Value = GetFlag(adoiTunesPackage.Recordset("packageused"))
            cmbiTunesProvider(1).Text = Trim(" " & adoiTunesPackage.Recordset("provider"))
            cmbSubType(1).Text = Trim(" " & adoiTunesPackage.Recordset("video_subtype"))
            txtMetaDataLanguage.Text = Trim(" " & adoiTunesPackage.Recordset("metadata_language"))
            txtTitle.Text = Trim(" " & adoiTunesPackage.Recordset("video_title"))
            txtStudio_Release_Title.Text = Trim(" " & adoiTunesPackage.Recordset("studio_release_title"))
            txtiTunesCopyrightLine.Text = Trim(" " & adoiTunesPackage.Recordset("video_copyright_cline"))
            txtiTunesLongDescription.Text = Trim(" " & adoiTunesPackage.Recordset("video_longdescription"))
            txtiTunesEpProdNo.Text = Trim(" " & adoiTunesPackage.Recordset("video_ep_prod_no"))
            txtiTunesContainerID.Text = Trim(" " & adoiTunesPackage.Recordset("video_containerID"))
            txtiTunesContainerPosition.Text = Trim(" " & adoiTunesPackage.Recordset("video_containerposition"))
            datiTunesReleaseDate.Value = Trim(" " & adoiTunesPackage.Recordset("video_releasedate"))
            txtiTunesTechComments.Text = Trim(" " & adoiTunesPackage.Recordset("itunestechcomment"))
            txtVideoLanguage.Text = Trim(" " & adoiTunesPackage.Recordset("video_language"))
            txtiTunesPreviewStartTime.Text = Trim(" " & adoiTunesPackage.Recordset("video_previewstarttime"))
            cmbAU.Text = Trim(" " & adoiTunesPackage.Recordset("rating_au"))
            cmbCA.Text = Trim(" " & adoiTunesPackage.Recordset("rating_ca"))
            cmbDE.Text = Trim(" " & adoiTunesPackage.Recordset("rating_de"))
            cmbFR.Text = Trim(" " & adoiTunesPackage.Recordset("rating_fr"))
            cmbUK.Text = Trim(" " & adoiTunesPackage.Recordset("rating_uk"))
            cmbUS.Text = Trim(" " & adoiTunesPackage.Recordset("rating_us"))
            cmbJP.Text = Trim(" " & adoiTunesPackage.Recordset("rating_jp"))
            txtCropTop.Text = Trim(" " & adoiTunesPackage.Recordset("fullcroptop"))
            txtCropBottom.Text = Trim(" " & adoiTunesPackage.Recordset("fullcropbottom"))
            txtCropLeft.Text = Trim(" " & adoiTunesPackage.Recordset("fullcropleft"))
            txtCropRight.Text = Trim(" " & adoiTunesPackage.Recordset("fullcropright"))
        
        Case "film"
            tabType.Tab = 1
            chkMultiLanguage.Value = adoiTunesPackage.Recordset("film_multilanguage")
            cmbiTunesProvider(0).Text = Trim(" " & adoiTunesPackage.Recordset("provider"))
            chkPackageUsed(2).Value = GetFlag(adoiTunesPackage.Recordset("packageused"))
            If chkMultiLanguage.Value <> 0 Then
                grdCast.Columns("Decoded_name2").Visible = True
                grdCast.Columns("Decoded_name3").Visible = True
                grdCast.Columns("Decoded_name4").Visible = True
'                grdCast.Columns("apple_ID2").Visible = True
'                grdCast.Columns("apple_ID3").Visible = True
'                grdCast.Columns("apple_ID4").Visible = True
                grdCast.Columns("description_Decoded2").Visible = True
                grdCast.Columns("description_Decoded3").Visible = True
                grdCast.Columns("description_Decoded4").Visible = True
                grdCrew.Columns("Decoded_name2").Visible = True
                grdCrew.Columns("Decoded_name3").Visible = True
                grdCrew.Columns("Decoded_name4").Visible = True
'                grdCrew.Columns("apple_ID2").Visible = True
'                grdCrew.Columns("apple_ID3").Visible = True
'                grdCrew.Columns("apple_ID4").Visible = True
                grdCastCharacters.Columns("Character_name_Decoded2").Visible = True
                grdCastCharacters.Columns("Character_name_Decoded3").Visible = True
                grdCastCharacters.Columns("Character_name_Decoded4").Visible = True
                grdCastCharacters.Columns("Character_note_Decoded2").Visible = True
                grdCastCharacters.Columns("Character_note_Decoded3").Visible = True
                grdCastCharacters.Columns("Character_note_Decoded4").Visible = True
            Else
                grdCast.Columns("Decoded_name2").Visible = False
                grdCast.Columns("Decoded_name3").Visible = False
                grdCast.Columns("Decoded_name4").Visible = False
                grdCast.Columns("apple_ID2").Visible = False
                grdCast.Columns("apple_ID3").Visible = False
                grdCast.Columns("apple_ID4").Visible = False
                grdCast.Columns("description_Decoded2").Visible = False
                grdCast.Columns("description_Decoded3").Visible = False
                grdCast.Columns("description_Decoded4").Visible = False
                grdCrew.Columns("Decoded_name2").Visible = False
                grdCrew.Columns("Decoded_name3").Visible = False
                grdCrew.Columns("Decoded_name4").Visible = False
                grdCrew.Columns("apple_ID2").Visible = False
                grdCrew.Columns("apple_ID3").Visible = False
                grdCrew.Columns("apple_ID4").Visible = False
                grdCastCharacters.Columns("Character_name_Decoded2").Visible = False
                grdCastCharacters.Columns("Character_name_Decoded3").Visible = False
                grdCastCharacters.Columns("Character_name_Decoded4").Visible = False
                grdCastCharacters.Columns("Character_note_Decoded2").Visible = False
                grdCastCharacters.Columns("Character_note_Decoded3").Visible = False
                grdCastCharacters.Columns("Character_note_Decoded4").Visible = False
            End If
            txtComments.Text = Trim(" " & adoiTunesPackage.Recordset("iTunesComments"))
            txtStudioReleaseTitle.Text = Trim(" " & adoiTunesPackage.Recordset("StudioReleaseTitle"))
            chkCustomChapterTitles.Value = adoiTunesPackage.Recordset("CustomChapterTitles")
            cmbFilmMetadataLanguage(0).Text = Trim(" " & adoiTunesPackage.Recordset("film_locale1"))
            cmbFilmTerritory(0).Text = Trim(" " & adoiTunesPackage.Recordset("film_territory1"))
            txtFilmTitle(0).Text = Trim(" " & adoiTunesPackage.Recordset("film_title1"))
            txtSynopsis(0).Text = Trim(" " & adoiTunesPackage.Recordset("film_synopsis1"))
            txtFilmCopyrightLine(0).Text = Trim(" " & adoiTunesPackage.Recordset("film_copyright_cline1"))
            datTheatricalReleaseDate(0).Value = Trim(" " & adoiTunesPackage.Recordset("film_releasedate1"))
            cmbFilmMetadataLanguage(1).Text = Trim(" " & adoiTunesPackage.Recordset("film_locale2"))
            cmbFilmTerritory(1).Text = Trim(" " & adoiTunesPackage.Recordset("film_territory2"))
            txtFilmTitle(1).Text = Trim(" " & adoiTunesPackage.Recordset("film_title2"))
            txtSynopsis(1).Text = Trim(" " & adoiTunesPackage.Recordset("film_synopsis2"))
            txtFilmCopyrightLine(1).Text = Trim(" " & adoiTunesPackage.Recordset("film_copyright_cline2"))
            datTheatricalReleaseDate(1).Value = Trim(" " & adoiTunesPackage.Recordset("film_releasedate2"))
            cmbFilmMetadataLanguage(2).Text = Trim(" " & adoiTunesPackage.Recordset("film_locale3"))
            cmbFilmTerritory(2).Text = Trim(" " & adoiTunesPackage.Recordset("film_territory3"))
            txtFilmTitle(2).Text = Trim(" " & adoiTunesPackage.Recordset("film_title3"))
            txtSynopsis(2).Text = Trim(" " & adoiTunesPackage.Recordset("film_synopsis3"))
            txtFilmCopyrightLine(2).Text = Trim(" " & adoiTunesPackage.Recordset("film_copyright_cline3"))
            datTheatricalReleaseDate(2).Value = Trim(" " & adoiTunesPackage.Recordset("film_releasedate3"))
            cmbFilmMetadataLanguage(3).Text = Trim(" " & adoiTunesPackage.Recordset("film_locale4"))
            cmbFilmTerritory(3).Text = Trim(" " & adoiTunesPackage.Recordset("film_territory4"))
            txtFilmTitle(3).Text = Trim(" " & adoiTunesPackage.Recordset("film_title4"))
            txtSynopsis(3).Text = Trim(" " & adoiTunesPackage.Recordset("film_synopsis4"))
            txtFilmCopyrightLine(3).Text = Trim(" " & adoiTunesPackage.Recordset("film_copyright_cline4"))
            datTheatricalReleaseDate(3).Value = Trim(" " & adoiTunesPackage.Recordset("film_releasedate4"))
            cmbSubType(0).Text = Trim(" " & adoiTunesPackage.Recordset("video_subtype"))
            cmbCountry.Columns("iso2acode").Text = Trim(" " & adoiTunesPackage.Recordset("country"))
            cmbCountry.Text = GetData("iso2a", "country", "iso2acode", adoiTunesPackage.Recordset("country"))
            txtFullClipID.Text = Trim(" " & adoiTunesPackage.Recordset("fullclipID"))
            cmbFullLocale(0).Text = Trim(" " & adoiTunesPackage.Recordset("mainassetlocale"))
            txtCaptionsClipID.Text = Trim(" " & adoiTunesPackage.Recordset("captionsclipID"))
            cmbFullLocale(1).Text = Trim(" " & adoiTunesPackage.Recordset("captionslocale"))
            txtFullCropTop.Text = Trim(" " & adoiTunesPackage.Recordset("fullcroptop"))
            txtFullCropBottom.Text = Trim(" " & adoiTunesPackage.Recordset("fullcropbottom"))
            txtFullCropLeft.Text = Trim(" " & adoiTunesPackage.Recordset("fullcropleft"))
            txtFullCropRight.Text = Trim(" " & adoiTunesPackage.Recordset("fullcropright"))
            txtWebScreenerClipID.Text = Trim(" " & adoiTunesPackage.Recordset("webscreenerclipID"))
            txtPreviewClipID(0).Text = Trim(" " & adoiTunesPackage.Recordset("previewclipid"))
            cmbPreviewLocale(0).Text = Trim(" " & adoiTunesPackage.Recordset("previewlocale"))
            txtPreviewClipID(1).Text = Trim(" " & adoiTunesPackage.Recordset("previewclipid2"))
            cmbPreviewTerritory(1).Text = Trim(" " & adoiTunesPackage.Recordset("previewterritory2"))
            cmbPreviewLocale(1).Text = Trim(" " & adoiTunesPackage.Recordset("previewlocale2"))
            txtPreviewClipID(2).Text = Trim(" " & adoiTunesPackage.Recordset("previewclipid3"))
            cmbPreviewTerritory(2).Text = Trim(" " & adoiTunesPackage.Recordset("previewterritory3"))
            cmbPreviewLocale(2).Text = Trim(" " & adoiTunesPackage.Recordset("previewlocale3"))
            txtPreviewClipID(3).Text = Trim(" " & adoiTunesPackage.Recordset("previewclipid4"))
            cmbPreviewTerritory(3).Text = Trim(" " & adoiTunesPackage.Recordset("previewterritory4"))
            cmbPreviewLocale(3).Text = Trim(" " & adoiTunesPackage.Recordset("previewlocale4"))
            txtPreviewCropTop.Text = Trim(" " & adoiTunesPackage.Recordset("previewcroptop"))
            txtPreviewCropBottom.Text = Trim(" " & adoiTunesPackage.Recordset("previewcropbottom"))
            txtPreviewCropLeft.Text = Trim(" " & adoiTunesPackage.Recordset("previewcropleft"))
            txtPreviewCropRight.Text = Trim(" " & adoiTunesPackage.Recordset("previewcropright"))
            txtPosterClipID(0).Text = Trim(" " & adoiTunesPackage.Recordset("posterclipid"))
            txtPosterClipID(1).Text = Trim(" " & adoiTunesPackage.Recordset("posterclipid2"))
            cmbPosterTerritory(1).Text = Trim(" " & adoiTunesPackage.Recordset("posterterritory2"))
            txtPosterClipID(2).Text = Trim(" " & adoiTunesPackage.Recordset("posterclipid3"))
            cmbPosterTerritory(2).Text = Trim(" " & adoiTunesPackage.Recordset("posterterritory3"))
            txtPosterClipID(3).Text = Trim(" " & adoiTunesPackage.Recordset("posterclipid4"))
            cmbPosterTerritory(3).Text = Trim(" " & adoiTunesPackage.Recordset("posterterritory4"))
            txtSubtitlesClipID(0).Text = Trim(" " & adoiTunesPackage.Recordset("subtitlesclipID"))
            cmbSubtitlesLocale(0).Text = Trim(" " & adoiTunesPackage.Recordset("subtitleslocale"))
            txtSubtitlesClipID(1).Text = Trim(" " & adoiTunesPackage.Recordset("subtitlesclipID2"))
            cmbSubtitlesLocale(1).Text = Trim(" " & adoiTunesPackage.Recordset("subtitleslocale2"))
            txtSubtitlesClipID(2).Text = Trim(" " & adoiTunesPackage.Recordset("subtitlesclipID3"))
            cmbSubtitlesLocale(2).Text = Trim(" " & adoiTunesPackage.Recordset("subtitleslocale3"))
            txtSubtitlesClipID(3).Text = Trim(" " & adoiTunesPackage.Recordset("subtitlesclipID4"))
            cmbSubtitlesLocale(3).Text = Trim(" " & adoiTunesPackage.Recordset("subtitleslocale4"))
            txtSubtitlesClipID(8).Text = Trim(" " & adoiTunesPackage.Recordset("subtitlesclipID5"))
            cmbSubtitlesLocale(7).Text = Trim(" " & adoiTunesPackage.Recordset("subtitleslocale5"))
            txtSubtitlesClipID(9).Text = Trim(" " & adoiTunesPackage.Recordset("subtitlesclipID6"))
            cmbSubtitlesLocale(8).Text = Trim(" " & adoiTunesPackage.Recordset("subtitleslocale6"))
            txtSubtitlesClipID(10).Text = Trim(" " & adoiTunesPackage.Recordset("subtitlesclipID7"))
            cmbSubtitlesLocale(9).Text = Trim(" " & adoiTunesPackage.Recordset("subtitleslocale7"))
            txtSubtitlesClipID(11).Text = Trim(" " & adoiTunesPackage.Recordset("subtitlesclipID8"))
            cmbSubtitlesLocale(10).Text = Trim(" " & adoiTunesPackage.Recordset("subtitleslocale8"))
            txtSubtitlesClipID(12).Text = Trim(" " & adoiTunesPackage.Recordset("subtitlesclipID9"))
            cmbSubtitlesLocale(11).Text = Trim(" " & adoiTunesPackage.Recordset("subtitleslocale9"))
            txtSubtitlesClipID(13).Text = Trim(" " & adoiTunesPackage.Recordset("subtitlesclipID10"))
            cmbSubtitlesLocale(12).Text = Trim(" " & adoiTunesPackage.Recordset("subtitleslocale10"))
            txtSubtitlesClipID(14).Text = Trim(" " & adoiTunesPackage.Recordset("subtitlesclipID11"))
            cmbSubtitlesLocale(13).Text = Trim(" " & adoiTunesPackage.Recordset("subtitleslocale11"))
            txtSubtitlesClipID(15).Text = Trim(" " & adoiTunesPackage.Recordset("subtitlesclipID12"))
            cmbSubtitlesLocale(14).Text = Trim(" " & adoiTunesPackage.Recordset("subtitleslocale12"))
            txtAudioClipID(0).Text = Trim(" " & adoiTunesPackage.Recordset("audioclipID1"))
            cmbAudioLocale(0).Text = Trim(" " & adoiTunesPackage.Recordset("audiolocale1"))
            txtAudioClipID(1).Text = Trim(" " & adoiTunesPackage.Recordset("audioclipID2"))
            cmbAudioLocale(1).Text = Trim(" " & adoiTunesPackage.Recordset("audiolocale2"))
            txtAudioClipID(2).Text = Trim(" " & adoiTunesPackage.Recordset("audioclipID3"))
            cmbAudioLocale(2).Text = Trim(" " & adoiTunesPackage.Recordset("audiolocale3"))
            txtAudioClipID(3).Text = Trim(" " & adoiTunesPackage.Recordset("audiodisclipID1"))
            cmbAudioLocale(3).Text = Trim(" " & adoiTunesPackage.Recordset("audiodislocale1"))
            txtAudioClipID(4).Text = Trim(" " & adoiTunesPackage.Recordset("audiodisclipID2"))
            cmbAudioLocale(4).Text = Trim(" " & adoiTunesPackage.Recordset("audiodislocale2"))
            txtAudioClipID(5).Text = Trim(" " & adoiTunesPackage.Recordset("audiodisclipID3"))
            cmbAudioLocale(5).Text = Trim(" " & adoiTunesPackage.Recordset("audiodislocale3"))
            txtAudioClipID(6).Text = Trim(" " & adoiTunesPackage.Recordset("audiodisclipID4"))
            cmbAudioLocale(6).Text = Trim(" " & adoiTunesPackage.Recordset("audiodislocale4"))
            cmbPosterLocale(0).Text = Trim(" " & adoiTunesPackage.Recordset("iTunesPosterLocale1_1"))
            cmbPosterLocale(1).Text = Trim(" " & adoiTunesPackage.Recordset("iTunesPosterLocale2_1"))
            cmbPosterLocale(2).Text = Trim(" " & adoiTunesPackage.Recordset("iTunesPosterLocale3_1"))
            cmbPosterLocale(3).Text = Trim(" " & adoiTunesPackage.Recordset("iTunesPosterLocale4_1"))
            cmbPosterLocale(4).Text = Trim(" " & adoiTunesPackage.Recordset("iTunesPosterLocale1_2"))
            cmbPosterLocale(5).Text = Trim(" " & adoiTunesPackage.Recordset("iTunesPosterLocale2_2"))
            cmbPosterLocale(6).Text = Trim(" " & adoiTunesPackage.Recordset("iTunesPosterLocale3_2"))
            cmbPosterLocale(7).Text = Trim(" " & adoiTunesPackage.Recordset("iTunesPosterLocale4_2"))
            cmbAUfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_au"))
            cmbAUSfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_aus"))
            cmbBEfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_be"))
            cmbCAfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_ca"))
            cmbDENfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_den"))
            cmbFIfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_fi"))
            cmbFRfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_fr"))
            cmbDEfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_de"))
            cmbGRfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_gr"))
            cmbIEfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_ie"))
            cmbITfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_it"))
            cmbJPfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_jp"))
            cmbLUfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_lu"))
            cmbMXfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_mx"))
            cmbNLfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_nl"))
            cmbNZfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_nz"))
            cmbNOfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_no"))
            cmbPTfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_pt"))
            cmbQBfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_qb"))
            cmbESfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_es"))
            cmbSEfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_se"))
            cmbCHfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_ch"))
            cmbUKfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_UK"))
            cmbUSfilm.Text = Trim(" " & adoiTunesPackage.Recordset("rating_US"))
            txtAUreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_au_reason"))
            txtAUSreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_aus_reason"))
            txtBEreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_be_reason"))
            txtCAreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_ca_reason"))
            txtDENreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_den_reason"))
            txtFIreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_fi_reason"))
            txtFRreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_fr_reason"))
            txtDEreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_de_reason"))
            txtGRreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_gr_reason"))
            txtIEreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_ie_reason"))
            txtITreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_it_reason"))
            txtJPreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_jp_reason"))
            txtLUreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_lu_reason"))
            txtMXreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_mx_reason"))
            txtNLreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_nl_reason"))
            txtNZreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_nz_reason"))
            txtNOreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_no_reason"))
            txtPTreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_pt_reason"))
            txtQBreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_qb_reason"))
            txtESreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_es_reason"))
            txtSEreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_se_reason"))
            txtCHreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_ch_reason"))
            txtUKreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_uk_reason"))
            txtUSreason.Text = Trim(" " & adoiTunesPackage.Recordset("rating_us_reason"))
            txtProductionCompany.Text = Trim(" " & adoiTunesPackage.Recordset("video_networkname"))
            txtUPC.Text = Trim(" " & adoiTunesPackage.Recordset("upc"))
            txtAMGVideoID.Text = Trim(" " & adoiTunesPackage.Recordset("amg_video_id"))
            chkFeatureTextless.Value = GetFlag(adoiTunesPackage.Recordset("FeatureTextless"))
            cmbForcedNarrativeLocale(0).Text = Trim(" " & adoiTunesPackage.Recordset("FeatureForcedNarrativeLocale"))
            cmbForcedNarrativeLocale(5).Text = Trim(" " & adoiTunesPackage.Recordset("FeatureBurnedInSubsLocale"))
            cmbForcedNarrativeLocale(1).Text = Trim(" " & adoiTunesPackage.Recordset("Trailer1ForcedNarrativeLocale"))
            cmbForcedNarrativeLocale(2).Text = Trim(" " & adoiTunesPackage.Recordset("Trailer2ForcedNarrativeLocale"))
            cmbForcedNarrativeLocale(3).Text = Trim(" " & adoiTunesPackage.Recordset("Trailer3ForcedNarrativeLocale"))
            cmbForcedNarrativeLocale(4).Text = Trim(" " & adoiTunesPackage.Recordset("Trailer4ForcedNarrativeLocale"))
            cmbForcedNarrativeLocale(6).Text = Trim(" " & adoiTunesPackage.Recordset("Trailer1BurnedInSubsLocale"))
            cmbForcedNarrativeLocale(7).Text = Trim(" " & adoiTunesPackage.Recordset("Trailer2BurnedInSubsLocale"))
            cmbForcedNarrativeLocale(8).Text = Trim(" " & adoiTunesPackage.Recordset("Trailer3BurnedInSubsLocale"))
            cmbForcedNarrativeLocale(9).Text = Trim(" " & adoiTunesPackage.Recordset("Trailer4BurnedInSubsLocale"))
            txtQCClipID(0).Text = Trim(" " & adoiTunesPackage.Recordset("QCnotesClipID"))
            txtQCClipID(1).Text = Trim(" " & adoiTunesPackage.Recordset("QCnotestrail1ClipID"))
            txtQCClipID(2).Text = Trim(" " & adoiTunesPackage.Recordset("QCnotestrail2ClipID"))
            txtQCClipID(3).Text = Trim(" " & adoiTunesPackage.Recordset("QCnotestrail3ClipID"))
            txtQCClipID(4).Text = Trim(" " & adoiTunesPackage.Recordset("QCnotestrail4ClipID"))
            Select Case adoiTunesPackage.Recordset("chaptertimecodeformat")
                Case 8
                    'TC_23
                    chkTimecode(0).Value = True
                    chkTimecode(1).Value = False
                    chkTimecode(2).Value = False
                    chkTimecode(3).Value = False
                    chkTimecode(4).Value = False
                Case 1
                    'TC_25
                    chkTimecode(0).Value = False
                    chkTimecode(1).Value = True
                    chkTimecode(2).Value = False
                    chkTimecode(3).Value = False
                    chkTimecode(4).Value = False
                Case 3
                    'TC_30
                    chkTimecode(0).Value = False
                    chkTimecode(1).Value = False
                    chkTimecode(2).Value = True
                    chkTimecode(3).Value = False
                    chkTimecode(4).Value = False
                Case 2
                    'TC_29
                    chkTimecode(0).Value = False
                    chkTimecode(1).Value = False
                    chkTimecode(2).Value = False
                    chkTimecode(3).Value = True
                    chkTimecode(4).Value = False
                Case 9
                    'TC_QT
                    chkTimecode(0).Value = False
                    chkTimecode(1).Value = False
                    chkTimecode(2).Value = False
                    chkTimecode(3).Value = False
                    chkTimecode(4).Value = True
                Case Else
                    'TC_UN
                    chkTimecode(0).Value = False
                    chkTimecode(1).Value = False
                    chkTimecode(2).Value = False
                    chkTimecode(3).Value = False
                    chkTimecode(4).Value = False
            End Select
            
        Case "Amazon", "Xbox"
            tabType.Tab = 2
            chkPackageUsed(1).Value = GetFlag(adoiTunesPackage.Recordset("packageused"))
            txtAmazonTerritory = Trim(" " & adoiTunesPackage.Recordset("country"))
            txtAmazonLanguage = Trim(" " & adoiTunesPackage.Recordset("video_language"))
            txtAmazonSeriesID = Trim(" " & adoiTunesPackage.Recordset("AmazonSeriesUniqueID"))
            txtAmazonSeasonID = Trim(" " & adoiTunesPackage.Recordset("AmazonSeasonUniqueID"))
            txtAmazonCopyrightHolder = Trim(" " & adoiTunesPackage.Recordset("AmazonCopyrightHolder"))
            txtAmazonCopyrightYear = Trim(" " & adoiTunesPackage.Recordset("AmazonCopyrightYear"))
            txtAmazonSeriesName = Trim(" " & adoiTunesPackage.Recordset("AmazonSeriesName"))
            txtAmazonSeasonTitle = Trim(" " & adoiTunesPackage.Recordset("AmazonSeasonTitle"))
            txtAmazonSeasonProductionCompany = Trim(" " & adoiTunesPackage.Recordset("AmazonSeasonProductionCompany"))
            txtAmazonSeasonNumber = Trim(" " & adoiTunesPackage.Recordset("AmazonSeasonNumber"))
            txtAmazonEpisodeTitle = Trim(" " & adoiTunesPackage.Recordset("video_title"))
            txtAmazonSequence = Trim(" " & adoiTunesPackage.Recordset("AmazonSequence"))
            txtAmazonSeriesShortSynopsis = Trim(" " & adoiTunesPackage.Recordset("AmazonSeriesShortSynopsis"))
            txtAmazonSeriesLongSynopsis = Trim(" " & adoiTunesPackage.Recordset("AmazonSeriesLongSynopsis"))
            txtAmazonSeasonShortSynopsis = Trim(" " & adoiTunesPackage.Recordset("AmazonSeasonShortSynopsis"))
            txtAmazonSeasonLongSynopsis = Trim(" " & adoiTunesPackage.Recordset("AmazonSeasonLongSynopsis"))
            txtAmazonEpisodeShortSynopsis = Trim(" " & adoiTunesPackage.Recordset("AmazonEpisodeShortSynopsis"))
            txtAmazonEpisodeLongSynopsis = Trim(" " & adoiTunesPackage.Recordset("AmazonEpisodeLongSynopsis"))
            txtAmazonGenre = Trim(" " & adoiTunesPackage.Recordset("AmazonGenre"))
            txtAmazonRatingType = Trim(" " & adoiTunesPackage.Recordset("AmazonRatingType"))
            txtAmazonRating = Trim(" " & adoiTunesPackage.Recordset("AmazonRating"))
            datOriginalAirDate.Value = Trim(" " & adoiTunesPackage.Recordset("AmazonOriginalAirDate"))
            datSDAmazonOfferStart.Value = Trim(" " & adoiTunesPackage.Recordset("AmazonSDOfferStart"))
            datHDAmazonOfferStart.Value = Trim(" " & adoiTunesPackage.Recordset("AmazonHDOfferStart"))
            datSDVODAmazonOfferStart.Value = Trim(" " & adoiTunesPackage.Recordset("AmazonSDVODOfferStart"))
            datHDVODAmazonOfferStart.Value = Trim(" " & adoiTunesPackage.Recordset("AmazonHDVODOfferStart"))
            txtAmazonFullClipID.Text = Trim(" " & adoiTunesPackage.Recordset("fullclipid"))
            txtAmazonPosterClipID.Text = Trim(" " & adoiTunesPackage.Recordset("posterclipid"))
            txtAmazonAudioClipID.Text = Trim(" " & adoiTunesPackage.Recordset("AudioClipID1"))
            txtXboxLicensor.Text = Trim(" " & adoiTunesPackage.Recordset("XboxLicensor"))
            
        Case Else
            tabType.Tab = 0
            txtiTunesEpProdNo.Text = ""
            txtiTunesContainerID.Text = ""
            txtiTunesContainerPosition.Text = ""
            datiTunesReleaseDate.Value = ""
            txtiTunesLongDescription.Text = ""
            txtiTunesPreviewStartTime.Text = ""
            cmbAU.Text = ""
            cmbCA.Text = ""
            cmbDE.Text = ""
            cmbFR.Text = ""
            cmbUK.Text = ""
            cmbUS.Text = ""
            cmbJP.Text = ""
            datTheatricalReleaseDate(0).Value = Null
            cmbSubType(0).Text = ""
            cmbSubType(1).Text = ""
            cmbCountry.Columns("iso2acode").Text = ""
            cmbCountry.Text = ""
            txtSynopsis(0).Text = ""
            txtFullClipID.Text = ""
            txtFullCropTop.Text = ""
            txtFullCropBottom.Text = ""
            txtFullCropLeft.Text = ""
            txtFullCropRight.Text = ""
            txtPreviewClipID(0).Text = ""
            txtPreviewClipID(1).Text = ""
            txtPreviewClipID(2).Text = ""
            txtPreviewClipID(3).Text = ""
            cmbPreviewTerritory(1).Text = ""
            cmbPreviewTerritory(2).Text = ""
            cmbPreviewTerritory(3).Text = ""
            cmbPreviewLocale(0).Text = ""
            cmbPreviewLocale(1).Text = ""
            cmbPreviewLocale(2).Text = ""
            cmbPreviewLocale(3).Text = ""
            txtPreviewCropTop.Text = ""
            txtPreviewCropBottom.Text = ""
            txtPreviewCropLeft.Text = ""
            txtPreviewCropRight.Text = ""
            txtPosterClipID(0).Text = ""
            txtPosterClipID(1).Text = ""
            txtPosterClipID(2).Text = ""
            txtPosterClipID(3).Text = ""
            txtSubtitlesClipID(0).Text = ""
            txtSubtitlesClipID(1).Text = ""
            txtSubtitlesClipID(2).Text = ""
            txtSubtitlesClipID(3).Text = ""
            txtWebScreenerClipID.Text = ""
            cmbAUfilm.Text = ""
            cmbAUSfilm.Text = ""
            cmbBEfilm.Text = ""
            cmbCAfilm.Text = ""
            cmbDENfilm.Text = ""
            cmbFIfilm.Text = ""
            cmbFRfilm.Text = ""
            cmbDEfilm.Text = ""
            cmbGRfilm.Text = ""
            cmbIEfilm.Text = ""
            cmbITfilm.Text = ""
            cmbJPfilm.Text = ""
            cmbLUfilm.Text = ""
            cmbMXfilm.Text = ""
            cmbNLfilm.Text = ""
            cmbNZfilm.Text = ""
            cmbNOfilm.Text = ""
            cmbPTfilm.Text = ""
            cmbFRfilm.Text = ""
            cmbQBfilm.Text = ""
            cmbESfilm.Text = ""
            cmbSEfilm.Text = ""
            cmbCHfilm.Text = ""
            cmbUKfilm.Text = ""
            cmbUSfilm.Text = ""
            txtAUreason.Text = ""
            txtAUSreason.Text = ""
            txtBEreason.Text = ""
            txtDENreason.Text = ""
            txtFIreason.Text = ""
            txtFRreason.Text = ""
            txtDEreason.Text = ""
            txtGRreason.Text = ""
            txtIEreason.Text = ""
            txtITreason.Text = ""
            txtJPreason.Text = ""
            txtLUreason.Text = ""
            txtMXreason.Text = ""
            txtNLreason.Text = ""
            txtNZreason.Text = ""
            txtNZreason.Text = ""
            txtNOreason.Text = ""
            txtPTreason.Text = ""
            txtQBreason.Text = ""
            txtESreason.Text = ""
            txtSEreason.Text = ""
            txtCHreason.Text = ""
            txtUKreason.Text = ""
            txtUSreason.Text = ""
            txtProductionCompany.Text = ""
            txtUPC.Text = ""
            txtAMGVideoID.Text = ""
            txtAmazonFullClipID.Text = ""
            txtAmazonPosterClipID.Text = ""
            txtAmazonAudioClipID.Text = ""
            txtXboxLicensor.Text = ""
            
    End Select

    adoAdvisory.RecordSource = "SELECT * FROM itunes_clip_advisory WHERE itunes_packageID = " & adoiTunesPackage.Recordset("itunes_packageID") & ";"
    adoAdvisory.ConnectionString = g_strConnection
    adoAdvisory.Refresh
    
    adoProduct.ConnectionString = g_strConnection
    adoProduct.RecordSource = "SELECT * FROM iTunes_product WHERE itunes_packageID = " & adoiTunesPackage.Recordset("itunes_packageID") & ";"
    adoProduct.Refresh
    
    adoGenres.RecordSource = "SELECT * FROM itunes_genre WHERE itunes_packageID = " & adoiTunesPackage.Recordset("itunes_packageID") & ";"
    adoGenres.ConnectionString = g_strConnection
    adoGenres.Refresh
    
    adoCast.RecordSource = "SELECT * FROM itunes_cast WHERE itunes_packageID = " & adoiTunesPackage.Recordset("itunes_packageID") & ";"
    adoCast.ConnectionString = g_strConnection
    adoCast.Refresh
    
    adoCrew.RecordSource = "SELECT * FROM itunes_crew WHERE itunes_packageID = " & adoiTunesPackage.Recordset("itunes_packageID") & " ORDER BY display_name;"
    adoCrew.ConnectionString = g_strConnection
    adoCrew.Refresh
    
    adoArtist1.RecordSource = "SELECT * FROM itunes_artist WHERE itunes_packageID = " & adoiTunesPackage.Recordset("itunes_packageID") & ";"
    adoArtist1.ConnectionString = g_strConnection
    adoArtist1.Refresh
    
    adoCueSheet.RecordSource = "SELECT * FROM itunes_cuesheet WHERE itunes_packageID = " & adoiTunesPackage.Recordset("itunes_packageID") & ";"
    adoCueSheet.ConnectionString = g_strConnection
    adoCueSheet.Refresh
    
    adoChapters.RecordSource = "SELECT * FROM itunes_chapter WHERE itunes_packageID = " & adoiTunesPackage.Recordset("itunes_packageID") & " ORDER BY starttime;"
    adoChapters.ConnectionString = g_strConnection
    adoChapters.Refresh
    
End If

End Sub

Private Sub chkCustomChapterTitles_Click()

If chkCustomChapterTitles.Value <> 0 Then
    grdChapters.Columns("Title1").Visible = True
    grdChapters.Columns("Title2").Visible = True
    grdChapters.Columns("Title3").Visible = True
    grdChapters.Columns("Title4").Visible = True
Else
    grdChapters.Columns("Title1").Visible = False
    grdChapters.Columns("Title2").Visible = False
    grdChapters.Columns("Title3").Visible = False
    grdChapters.Columns("Title4").Visible = False
End If

End Sub

Private Sub chkMultiLanguage_Click()

If chkMultiLanguage.Value <> 0 Then
    grdCast.Columns("Decoded_name2").Visible = True
    grdCast.Columns("Decoded_name3").Visible = True
    grdCast.Columns("Decoded_name4").Visible = True
'    grdCast.Columns("apple_ID2").Visible = True
'    grdCast.Columns("apple_ID3").Visible = True
'    grdCast.Columns("apple_ID4").Visible = True
    grdCast.Columns("description_Decoded2").Visible = True
    grdCast.Columns("description_Decoded3").Visible = True
    grdCast.Columns("description_Decoded4").Visible = True
    grdCrew.Columns("Decoded_name2").Visible = True
    grdCrew.Columns("Decoded_name3").Visible = True
    grdCrew.Columns("Decoded_name4").Visible = True
'    grdCrew.Columns("apple_ID2").Visible = True
'    grdCrew.Columns("apple_ID3").Visible = True
'    grdCrew.Columns("apple_ID4").Visible = True
    grdCastCharacters.Columns("Character_name_Decoded2").Visible = True
    grdCastCharacters.Columns("Character_name_Decoded3").Visible = True
    grdCastCharacters.Columns("Character_name_Decoded4").Visible = True
    grdCastCharacters.Columns("Character_note_Decoded2").Visible = True
    grdCastCharacters.Columns("Character_note_Decoded3").Visible = True
    grdCastCharacters.Columns("Character_note_Decoded4").Visible = True
Else
    grdCast.Columns("Decoded_name2").Visible = False
    grdCast.Columns("Decoded_name3").Visible = False
    grdCast.Columns("Decoded_name4").Visible = False
    grdCast.Columns("apple_ID2").Visible = False
    grdCast.Columns("apple_ID3").Visible = False
    grdCast.Columns("apple_ID4").Visible = False
    grdCast.Columns("description_Decoded2").Visible = False
    grdCast.Columns("description_Decoded3").Visible = False
    grdCast.Columns("description_Decoded4").Visible = False
    grdCrew.Columns("Decoded_name2").Visible = False
    grdCrew.Columns("Decoded_name3").Visible = False
    grdCrew.Columns("Decoded_name4").Visible = False
    grdCrew.Columns("apple_ID2").Visible = False
    grdCrew.Columns("apple_ID3").Visible = False
    grdCrew.Columns("apple_ID4").Visible = False
    grdCastCharacters.Columns("Character_name_Decoded2").Visible = False
    grdCastCharacters.Columns("Character_name_Decoded3").Visible = False
    grdCastCharacters.Columns("Character_name_Decoded4").Visible = False
    grdCastCharacters.Columns("Character_note_Decoded2").Visible = False
    grdCastCharacters.Columns("Character_note_Decoded3").Visible = False
    grdCastCharacters.Columns("Character_note_Decoded4").Visible = False
End If

End Sub

Private Sub cmbAU_GotFocus()

PopulateCombo "iTunesRating", cmbAU, " AND format = 'au-tv'"
HighLite cmbAU

End Sub

Private Sub cmbAUfilm_GotFocus()

PopulateCombo "iTunesRating", cmbAUfilm, " AND format = 'au-oflc'"
HighLite cmbAUfilm

End Sub

Private Sub cmbAUSfilm_DropDown()

PopulateCombo "iTunesRating", cmbAUSfilm, " AND format = 'fsk'"
HighLite cmbAUSfilm

End Sub

Private Sub cmbBEfilm_GotFocus()

PopulateCombo "iTunesRating", cmbBEfilm, " AND format = 'be-movies'"
HighLite cmbBEfilm

End Sub

Private Sub cmbCA_GotFocus()

PopulateCombo "iTunesRating", cmbCA, " AND format = 'ca-tv'"
HighLite cmbCA

End Sub

Private Sub cmbCAfilm_GotFocus()

PopulateCombo "iTunesRating", cmbCAfilm, "AND format = 'ca-chvs'"
HighLite cmbCAfilm

End Sub

Private Sub cmbCHfilm_GotFocus()

PopulateCombo "iTunesRating", cmbCHfilm, " AND format = 'ch-movies'"
HighLite cmbCHfilm

End Sub

Private Sub cmbDE_GotFocus()

PopulateCombo "iTunesRating", cmbDE, " AND format = 'de-tv'"
HighLite cmbDE

End Sub

Private Sub cmbDEfilm_GotFocus()

PopulateCombo "iTunesRating", cmbDEfilm, " AND format = 'de-fsk'"
HighLite cmbDEfilm

End Sub

Private Sub cmbDENfilm_GotFocus()

PopulateCombo "iTunesRating", cmbDENfilm, " AND format = 'dk-movies'"
HighLite cmbDENfilm

End Sub

Private Sub cmbESfilm_GotFocus()

PopulateCombo "iTunesRating", cmbESfilm, " AND format = 'es-movies'"
HighLite cmbESfilm

End Sub

Private Sub cmbFIfilm_GotFocus()

PopulateCombo "iTunesRating", cmbFIfilm, " AND format = 'fi-movies'"
HighLite cmbFIfilm

End Sub


Private Sub cmbFR_GotFocus()

PopulateCombo "iTunesRating", cmbFR, " AND format = 'fr-tv'"
HighLite cmbFR

End Sub

Private Sub cmbFRfilm_GotFocus()

PopulateCombo "iTunesRating", cmbFRfilm, " AND format = 'fr-cnc'"
HighLite cmbFRfilm

End Sub

Private Sub cmbGRfilm_GotFocus()

PopulateCombo "iTunesRating", cmbGRfilm, " AND format = 'gr-movies'"
HighLite cmbGRfilm

End Sub

Private Sub cmbIEfilm_GotFocus()

PopulateCombo "iTunesRating", cmbIEfilm, " AND format = 'ie-ifco'"
HighLite cmbIEfilm

End Sub

Private Sub cmbITfilm_GotFocus()

PopulateCombo "iTunesRating", cmbITfilm, " AND format = 'it-movies'"
HighLite cmbITfilm

End Sub

Private Sub cmbiTunesType_Click()

Select Case cmbiTunesType.Text

    Case "tv"
        tabType.Tab = 0
        
    Case "film"
        tabType.Tab = 1
    
    Case "Amazon"
        tabType.Tab = 2
    
    Case "Xbox"
        tabType.Tab = 3
        
    Case Else
        'Nothing

End Select

End Sub

Private Sub cmbJPfilm_GotFocus()

PopulateCombo "iTunesRating", cmbJPfilm, " AND format = 'jp-eirin'"
HighLite cmbJPfilm

End Sub

Private Sub cmbLUfilm_GotFocus()

PopulateCombo "iTunesRating", cmbLUfilm, " AND format = 'lu-movies'"
HighLite cmbLUfilm

End Sub

Private Sub cmbMXfilm_GotFocus()

PopulateCombo "iTunesRating", cmbMXfilm, " AND format = 'mx-movies'"
HighLite cmbMXfilm

End Sub

Private Sub cmbNLfilm_GotFocus()

PopulateCombo "iTunesRating", cmbNLfilm, " AND format = 'nl-movies'"
HighLite cmbNLfilm

End Sub

Private Sub cmbNOfilm_GotFocus()

PopulateCombo "iTunesRating", cmbNOfilm, " AND format = 'no-movies'"
HighLite cmbNOfilm

End Sub

Private Sub cmbNZfilm_GotFocus()

PopulateCombo "iTunesRating", cmbNZfilm, " AND format = 'nz-oflc'"
HighLite cmbNZfilm

End Sub

Private Sub cmbPTfilm_GotFocus()

PopulateCombo "iTunesRating", cmbPTfilm, " AND format = 'pt-movies'"
HighLite cmbPTfilm

End Sub

Private Sub cmbQBfilm_GotFocus()

PopulateCombo "iTunesRating", cmbQBfilm, " AND format = 'frca-movies'"
HighLite cmbQBfilm

End Sub

Private Sub cmbSEfilm_GotFocus()

PopulateCombo "iTunesRating", cmbSEfilm, " AND format = 'se-movies'"
HighLite cmbSEfilm

End Sub

Private Sub cmbUK_GotFocus()

PopulateCombo "iTunesRating", cmbUK, " AND format = 'uk-tv'"
HighLite cmbUK

End Sub

Private Sub cmbUKfilm_GotFocus()

PopulateCombo "iTunesRating", cmbUKfilm, " AND format = 'bbfc'"
HighLite cmbUKfilm

End Sub

Private Sub cmbUS_GotFocus()

PopulateCombo "iTunesRating", cmbUS, " AND format = 'us-tv'"
HighLite cmbUS

End Sub
Private Sub cmbJP_GotFocus()

PopulateCombo "iTunesRating", cmbJP, " AND format = 'jp-tv'"
HighLite cmbJP

End Sub

Private Sub cmbCompany_Click()

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

'adoiTunesPackage.RecordSource = "SELECT * FROM itunes_package WHERE companyID = " & Val(lblCompanyID.Caption) & ";"
'adoiTunesPackage.Refresh
'
'adoiTunesPackage.Caption = adoiTunesPackage.Recordset.RecordCount & " Packages"

End Sub

Private Sub cmbiTunesType_GotFocus()

PopulateCombo "iTunesType", cmbiTunesType
HighLite cmbiTunesType

End Sub

Private Sub cmbUSfilm_GotFocus()

PopulateCombo "iTunesRating", cmbUSfilm, " AND format = 'mpaa'"
HighLite cmbUSfilm

End Sub

Private Sub cmdAddPackage_Click()

Dim l_strSQL As String

l_strSQL = "INSERT INTO itunes_package (companyID, video_type) VALUES (" & Val(lblCompanyID.Caption) & ", '" & cmbiTunesType.Text & "');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError
lblPackageID.Caption = g_lngLastID

adoiTunesPackage.Refresh

End Sub

Private Sub cmdAmazonXML_Click()

Dim l_strPathToFullFile As String, l_strAltFolder As String, l_strOriginalFilename As String, l_strNewFilename As String, l_strNewFileReference As String, l_strFileNameToSave As String
Dim l_strTemp As String, l_lngTemp As Long, FSO As Scripting.FileSystemObject, l_strMasterFilename As String, l_strJPGFilename As String, l_strAudioFilename As String
Dim l_strDuration As String, l_lngRunningTime As Long, l_lngChannelCount As Long, l_strMasterReference As String

'Check Package Output for Amazon Packages only.
If cmbiTunesType.Text <> "Amazon" Then
    MsgBox "This XML Output is for 'Amazon' packages only" & vbCrLf & "For 'tv' - load the package information into into the appropriate Media Clip" & vbCrLf _
    & "and make XML there.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

cmdSave.Value = True
Set FSO = New Scripting.FileSystemObject

'Check the compulsory info in the normal controls
If txtiTunesVendorID.Text = "" Or txtAmazonTerritory.Text = "" Or txtAmazonLanguage.Text = "" Or txtAmazonSeriesID.Text = "" Or txtAmazonSeasonID.Text = "" _
Or txtAmazonCopyrightHolder.Text = "" Or txtAmazonCopyrightYear.Text = "" Or txtAmazonSeriesName.Text = "" Or txtAmazonSeasonTitle.Text = "" _
Or txtAmazonSeasonNumber.Text = "" Or txtAmazonEpisodeTitle.Text = "" Or txtAmazonSequence.Text = "" Or txtAmazonGenre.Text = "" _
Or IsNull(datOriginalAirDate.Value) Or IsNull(datSDAmazonOfferStart.Value) Or IsNull(datSDAmazonOfferStart.Value) _
Or txtAmazonSeriesShortSynopsis.Text = "" Or txtAmazonSeriesLongSynopsis.Text = "" Or txtAmazonEpisodeShortSynopsis.Text = "" Or txtAmazonEpisodeLongSynopsis.Text = "" _
Or Val(txtAmazonFullClipID.Text) = 0 Or Val(txtAmazonPosterClipID.Text) = 0 Then
    MsgBox "Compulsory Information is has not yet been completed." & vbCrLf & "All red fields must be filled in," & vbCrLf _
    & "There must be at least a full clip and a poster item.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

'Check the poster is the right dimensions
If Not (GetData("events", "cliphorizontalpixels", "eventID", txtAmazonPosterClipID.Text) = 2560 And GetData("events", "clipverticalpixels", "eventID", txtAmazonPosterClipID.Text) = 1920) Then
    MsgBox "The poster clipID is not pointing to something that is 2560x1920.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

'Get Poster Clip and do the rename of the file, and update the file record.
l_lngTemp = GetData("events", "libraryID", "eventID", txtAmazonPosterClipID.Text)
l_strPathToFullFile = GetData("library", "subtitle", "libraryID", l_lngTemp)
l_strAltFolder = GetData("events", "altlocation", "eventID", txtAmazonPosterClipID.Text)
If l_strAltFolder <> "" Then
    l_strPathToFullFile = l_strPathToFullFile & "\" & l_strAltFolder
End If
l_strOriginalFilename = GetData("events", "clipfilename", "eventID", txtAmazonPosterClipID.Text)
l_strNewFileReference = GetData("company", "marketcode", "companyID", Val(lblCompanyID.Caption)) & "-"
l_strNewFileReference = l_strNewFileReference & txtAmazonSeasonID.Text & "-Full-Image-"
l_strNewFileReference = l_strNewFileReference & txtAmazonLanguage.Text
l_strNewFilename = l_strNewFileReference & ".jpg"
If l_strOriginalFilename <> l_strNewFilename Then
    If InStr(l_strNewFilename, ":") > 0 Then
        MsgBox "THere is a colon somewhere in the Season ID. This is not allowed"
        Exit Sub
    End If
    If FSO.FileExists(l_strPathToFullFile & "\" & l_strOriginalFilename) Then
        FSO.MoveFile l_strPathToFullFile & "\" & l_strOriginalFilename, l_strPathToFullFile & "\" & l_strNewFilename
        SetData "events", "clipreference", "eventID", txtAmazonPosterClipID.Text, l_strNewFileReference
        SetData "events", "clipfilename", "eventID", txtAmazonPosterClipID.Text, l_strNewFilename
    Else
        MsgBox "Could not rename the poster Art - XML Package aborted"
        Exit Sub
    End If
End If
l_strJPGFilename = l_strNewFilename

'Get Master Clip and do the rename of the file, and update the file record.
l_lngTemp = GetData("events", "libraryID", "eventID", txtAmazonFullClipID.Text)
l_strPathToFullFile = GetData("library", "subtitle", "libraryID", l_lngTemp)
l_strAltFolder = GetData("events", "altlocation", "eventID", txtAmazonFullClipID.Text)
If l_strAltFolder <> "" Then
    l_strPathToFullFile = l_strPathToFullFile & "\" & l_strAltFolder
End If
l_strOriginalFilename = GetData("events", "clipfilename", "eventID", txtAmazonFullClipID.Text)
l_strNewFileReference = GetData("company", "marketcode", "companyID", Val(lblCompanyID.Caption)) & "-"
l_strNewFileReference = l_strNewFileReference & txtiTunesVendorID.Text & "-Full-Mezz_HD-"
l_strNewFileReference = l_strNewFileReference & txtAmazonLanguage.Text
l_strNewFilename = l_strNewFileReference & Right(l_strOriginalFilename, 4)
If l_strOriginalFilename <> l_strNewFilename Then
    If FSO.FileExists(l_strPathToFullFile & "\" & l_strOriginalFilename) Then
        FSO.MoveFile l_strPathToFullFile & "\" & l_strOriginalFilename, l_strPathToFullFile & "\" & l_strNewFilename
        SetData "events", "clipreference", "eventID", txtAmazonFullClipID.Text, l_strNewFileReference
        SetData "events", "clipfilename", "eventID", txtAmazonFullClipID.Text, l_strNewFilename
    Else
        MsgBox "Could not rename the main clip - XML Package aborted"
        Exit Sub
    End If
End If
l_strMasterFilename = l_strNewFilename
l_strMasterReference = l_strNewFileReference

If Val(txtAmazonAudioClipID.Text) <> 0 Then
    'Get Master Clip and do the rename of the file, and update the file record.
    l_lngTemp = GetData("events", "libraryID", "eventID", txtAmazonAudioClipID.Text)
    l_strPathToFullFile = GetData("library", "subtitle", "libraryID", l_lngTemp)
    l_strAltFolder = GetData("events", "altlocation", "eventID", txtAmazonAudioClipID.Text)
    If l_strAltFolder <> "" Then
        l_strPathToFullFile = l_strPathToFullFile & "\" & l_strAltFolder
    End If
    l_strOriginalFilename = GetData("events", "clipfilename", "eventID", txtAmazonAudioClipID.Text)
    l_strNewFileReference = GetData("company", "marketcode", "companyID", Val(lblCompanyID.Caption)) & "-"
    l_strNewFileReference = l_strNewFileReference & txtiTunesVendorID.Text & "-Full-Audio-"
    l_strNewFileReference = l_strNewFileReference & IIf(txtOriginalSpokenLocale.Text <> "", txtOriginalSpokenLocale.Text, "en-US")
    l_strNewFilename = l_strNewFileReference & Right(l_strOriginalFilename, 4)
    If l_strOriginalFilename <> l_strNewFilename Then
        If FSO.FileExists(l_strPathToFullFile & "\" & l_strOriginalFilename) Then
            FSO.MoveFile l_strPathToFullFile & "\" & l_strOriginalFilename, l_strPathToFullFile & "\" & l_strNewFilename
            SetData "events", "clipreference", "eventID", txtAmazonAudioClipID.Text, l_strNewFileReference
            SetData "events", "clipfilename", "eventID", txtAmazonAudioClipID.Text, l_strNewFilename
        Else
            MsgBox "Could not rename the audio clip - XML Package aborted"
            Exit Sub
        End If
    End If
    l_strAudioFilename = l_strNewFilename
End If

l_strDuration = GetData("events", "fd_length", "eventID", Val(txtAmazonFullClipID.Text))
If l_strDuration <> "" Then
    l_lngRunningTime = 60 * Val(Mid(l_strDuration, 1, 2))
    l_lngRunningTime = l_lngRunningTime + Val(Mid(l_strDuration, 4, 2))
    If Val(Mid(l_strDuration, 7, 2)) > 30 Then
        l_lngRunningTime = l_lngRunningTime + 1
    End If
End If


MDIForm1.dlgMain.InitDir = l_strPathToFullFile
MDIForm1.dlgMain.Filter = "XML files|*.xml"
MDIForm1.dlgMain.Filename = l_strMasterReference & ".xml"

MDIForm1.dlgMain.ShowSave

l_strFileNameToSave = MDIForm1.dlgMain.Filename

If l_strFileNameToSave <> "" Then

    Open l_strFileNameToSave For Output As 1
    Print #1, "<?xml version=""1.0"" encoding=""UTF-8""?>"
    Print #1, "<Metadata xmlns=""http://www.amazon.com/UnboxMetadata/v1"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" ";
    Print #1, "xmlns:str=""http://exslt.org/strings"" xmlns:xalan=""http://xml.apache.org/xslt"" version=""INTL-1.2"" country=""" & txtAmazonTerritory.Text & """>"
    Print #1, Chr(9) & "<TV>"
    Print #1, Chr(9) & Chr(9) & "<Series>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<PartnerName>" & StrConv(GetData("Company", "source", "companyID", lblCompanyID.Caption), vbProperCase) & "</PartnerName>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<UniqueID>" & txtAmazonSeriesID.Text & "</UniqueID>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<CopyrightHolder>" & txtAmazonCopyrightHolder.Text & "</CopyrightHolder>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<CopyrightYear>" & txtAmazonCopyrightYear.Text & "</CopyrightYear>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<Language>" & Left(txtAmazonLanguage.Text, 2) & "</Language>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<TitleInfo>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Title locale=""" & txtAmazonLanguage.Text & """>" & UTF8_Encode(XMLSanitise(txtAmazonSeriesName.Text)) & "</Title>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Synopses locale=""" & txtAmazonLanguage.Text & """>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Short>" & UTF8_Encode(XMLSanitise(txtAmazonSeriesShortSynopsis.Text)) & "</Short>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Long>" & UTF8_Encode(XMLSanitise(txtAmazonSeriesLongSynopsis.Text)) & "</Long>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</Synopses>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Studio>" & txtAmazonSeasonProductionCompany.Text & "</Studio>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<StartYear>" & txtAmazonCopyrightYear.Text & "</StartYear>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</TitleInfo>"
    Print #1, Chr(9) & Chr(9) & "</Series>"
    Print #1, Chr(9) & Chr(9) & "<Season>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<PartnerName>" & StrConv(GetData("Company", "source", "companyID", lblCompanyID.Caption), vbProperCase) & "</PartnerName>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<UniqueID>" & txtAmazonSeasonID.Text & "</UniqueID>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<CopyrightHolder>" & txtAmazonCopyrightHolder.Text & "</CopyrightHolder>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<CopyrightYear>" & txtAmazonCopyrightYear.Text & "</CopyrightYear>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<Language>" & Left(txtAmazonLanguage.Text, 2) & "</Language>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<SeriesID>" & txtAmazonSeriesID.Text & "</SeriesID>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<TitleInfo>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Sequence>" & txtAmazonSeasonNumber.Text & "</Sequence>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Title locale=""" & txtAmazonLanguage.Text & """>" & UTF8_Encode(XMLSanitise(txtAmazonSeasonTitle.Text)) & "</Title>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Synopses locale=""" & txtAmazonLanguage.Text & """>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Short>" & UTF8_Encode(XMLSanitise(IIf(txtAmazonSeasonShortSynopsis.Text <> "", txtAmazonSeasonShortSynopsis.Text, txtAmazonSeriesShortSynopsis.Text))) & "</Short>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Long>" & UTF8_Encode(XMLSanitise(IIf(txtAmazonSeasonLongSynopsis.Text <> "", txtAmazonSeasonLongSynopsis.Text, txtAmazonSeriesLongSynopsis.Text))) & "</Long>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</Synopses>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Studio>" & txtAmazonSeasonProductionCompany.Text & "</Studio>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</TitleInfo>"
    Print #1, Chr(9) & Chr(9) & "</Season>"
    Print #1, Chr(9) & Chr(9) & "<Episode>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<PartnerName>" & StrConv(GetData("Company", "source", "companyID", lblCompanyID.Caption), vbProperCase) & "</PartnerName>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<UniqueID>" & txtiTunesVendorID.Text & "</UniqueID>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<CopyrightHolder>" & txtAmazonCopyrightHolder.Text & "</CopyrightHolder>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<CopyrightYear>" & txtAmazonCopyrightYear.Text & "</CopyrightYear>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<Language>" & Left(txtAmazonLanguage.Text, 2) & "</Language>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<RuntimeInMinutes>" & l_lngRunningTime & "</RuntimeInMinutes>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<SeriesID>" & txtAmazonSeriesID.Text & "</SeriesID>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<SeasonID>" & txtAmazonSeasonID.Text & "</SeasonID>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<TitleInfo>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Sequence>" & txtAmazonSequence.Text & "</Sequence>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Title locale=""" & txtAmazonLanguage.Text & """>" & UTF8_Encode(XMLSanitise(txtAmazonEpisodeTitle.Text)) & "</Title>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Synopses locale=""" & txtAmazonLanguage.Text & """>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Short>" & UTF8_Encode(XMLSanitise(txtAmazonEpisodeShortSynopsis.Text)) & "</Short>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Long>" & UTF8_Encode(XMLSanitise(txtAmazonEpisodeLongSynopsis.Text)) & "</Long>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</Synopses>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Studio>" & txtAmazonSeasonProductionCompany.Text & "</Studio>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<ItemType>downloadable-television-shows</ItemType>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Genre>" & UTF8_Encode(XMLSanitise(txtAmazonGenre.Text)) & "</Genre>"
    If txtAmazonRating.Text <> "" Then Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Rating scheme=""" & LCase(txtAmazonRatingType.Text) & """>" & txtAmazonRating.Text & "</Rating>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<OriginalAirDate>" & Format(datOriginalAirDate.Value, "YYYY-MM-DD") & "T00:00:00</OriginalAirDate>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</TitleInfo>"
    Print #1, Chr(9) & Chr(9) & "</Episode>"
    Print #1, Chr(9) & "</TV>"
    Print #1, Chr(9) & "<Offer>"
    Print #1, Chr(9) & Chr(9) & "<PartnerName>" & StrConv(GetData("Company", "source", "companyID", lblCompanyID.Caption), vbProperCase) & "</PartnerName>"
    Print #1, Chr(9) & Chr(9) & "<UniqueID>" & txtiTunesVendorID.Text & "</UniqueID>"
    Print #1, Chr(9) & Chr(9) & "<Type>EST</Type>"
    Print #1, Chr(9) & Chr(9) & "<Quality>HD</Quality>"
    Print #1, Chr(9) & Chr(9) & "<WindowStart>" & Format(datHDAmazonOfferStart.Value, "YYYY-MM-DD") & "T00:00:00</WindowStart>"
    Print #1, Chr(9) & "</Offer>"
    Print #1, Chr(9) & "<Offer>"
    Print #1, Chr(9) & Chr(9) & "<PartnerName>" & StrConv(GetData("Company", "source", "companyID", lblCompanyID.Caption), vbProperCase) & "</PartnerName>"
    Print #1, Chr(9) & Chr(9) & "<UniqueID>" & txtiTunesVendorID.Text & "</UniqueID>"
    Print #1, Chr(9) & Chr(9) & "<Type>EST</Type>"
    Print #1, Chr(9) & Chr(9) & "<Quality>SD</Quality>"
    Print #1, Chr(9) & Chr(9) & "<WindowStart>" & Format(datSDAmazonOfferStart.Value, "YYYY-MM-DD") & "T00:00:00</WindowStart>"
    Print #1, Chr(9) & "</Offer>"
    If Not IsNull(datSDVODAmazonOfferStart.Value) Then
        Print #1, Chr(9) & "<Offer>"
        Print #1, Chr(9) & Chr(9) & "<PartnerName>" & StrConv(GetData("Company", "source", "companyID", lblCompanyID.Caption), vbProperCase) & "</PartnerName>"
        Print #1, Chr(9) & Chr(9) & "<UniqueID>" & txtiTunesVendorID.Text & "</UniqueID>"
        Print #1, Chr(9) & Chr(9) & "<Type>VOD</Type>"
        Print #1, Chr(9) & Chr(9) & "<Quality>SD</Quality>"
        Print #1, Chr(9) & Chr(9) & "<WindowStart>" & Format(datSDVODAmazonOfferStart.Value, "YYYY-MM-DD") & "T00:00:00</WindowStart>"
        Print #1, Chr(9) & "</Offer>"
    End If
    If Not IsNull(datHDVODAmazonOfferStart.Value) Then
        Print #1, Chr(9) & "<Offer>"
        Print #1, Chr(9) & Chr(9) & "<PartnerName>" & StrConv(GetData("Company", "source", "companyID", lblCompanyID.Caption), vbProperCase) & "</PartnerName>"
        Print #1, Chr(9) & Chr(9) & "<UniqueID>" & txtiTunesVendorID.Text & "</UniqueID>"
        Print #1, Chr(9) & Chr(9) & "<Type>VOD</Type>"
        Print #1, Chr(9) & Chr(9) & "<Quality>HD</Quality>"
        Print #1, Chr(9) & Chr(9) & "<WindowStart>" & Format(datHDVODAmazonOfferStart.Value, "YYYY-MM-DD") & "T00:00:00</WindowStart>"
        Print #1, Chr(9) & "</Offer>"
    End If
    Print #1, Chr(9) & "<AssetManifest>"
    Print #1, Chr(9) & Chr(9) & "<PartnerName>" & StrConv(GetData("Company", "source", "companyID", lblCompanyID.Caption), vbProperCase) & "</PartnerName>"
    Print #1, Chr(9) & Chr(9) & "<UniqueID>" & txtiTunesVendorID.Text & "</UniqueID>"
    Print #1, Chr(9) & Chr(9) & "<Content type=""full"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<Asset type=""artwork"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Filename>" & l_strJPGFilename & "</Filename>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Attribute name=""image.width"">" & GetData("events", "cliphorizontalpixels", "eventID", Val(txtAmazonPosterClipID.Text)) & "</Attribute>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Attribute name=""image.height"">" & GetData("events", "clipverticalpixels", "eventID", Val(txtAmazonPosterClipID.Text)) & "</Attribute>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</Asset>"
    Print #1, Chr(9) & Chr(9) & "</Content>"
    Print #1, Chr(9) & Chr(9) & "<Content type=""full"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<Asset type=""source"" locale=""" & txtAmazonLanguage.Text & """>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Filename>" & l_strMasterFilename & "</Filename>"
'    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<ChannelMapping>"
'    l_lngChannelCount = 0
'    If GetData("events", "audiotypegroup1", "eventID", Val(txtAmazonFullClipID.Text)) <> "" Then
'        Select Case GetData("events", "audiotypegroup1", "eventID", Val(txtAmazonFullClipID.Text))
'            Case "5.1"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 1 & """>left</Channel>"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 2 & """>right</Channel>"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 3 & """>center</Channel>"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 4 & """>lfe</Channel>"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 5 & """>left surround</Channel>"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 6 & """>right surround</Channel>"
'                l_lngChannelCount = l_lngChannelCount + 6
'            Case "LT/RT"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 1 & """>left stereo total</Channel>"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 2 & """>right stereo total</Channel>"
'                l_lngChannelCount = l_lngChannelCount + 2
'            Case "Standard Stereo"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 1 & """>left</Channel>"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 2 & """>right</Channel>"
'                l_lngChannelCount = l_lngChannelCount + 2
'            Case Else
'                MsgBox "Unhandled Audio Type - xml aborted"
'                Close 1
'                Exit Sub
'        End Select
'    End If
'    If GetData("events", "audiotypegroup2", "eventID", Val(txtAmazonFullClipID.Text)) <> "" Then
'        Select Case GetData("events", "audiotypegroup2", "eventID", Val(txtAmazonFullClipID.Text))
'            Case "5.1"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 1 & """>left</Channel>"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 2 & """>right</Channel>"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 3 & """>center</Channel>"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 4 & """>lfe</Channel>"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 5 & """>left surround</Channel>"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 6 & """>right surround</Channel>"
'                l_lngChannelCount = l_lngChannelCount + 6
'            Case "LT/RT"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 1 & """>left stereo total</Channel>"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 2 & """>right stereo total</Channel>"
'                l_lngChannelCount = l_lngChannelCount + 2
'            Case "Standard Stereo"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 1 & """>left</Channel>"
'                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Channel index=""" & l_lngChannelCount + 2 & """>right</Channel>"
'                l_lngChannelCount = l_lngChannelCount + 2
'            Case Else
'                MsgBox "Unhandled Audio Type - xml aborted"
'                Close 1
'                Exit Sub
'        End Select
'    End If
'    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</ChannelMapping>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</Asset>"
    If Val(txtAmazonAudioClipID.Text) <> 0 Then
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<AudioAsset locale=""" & IIf(txtOriginalSpokenLocale.Text <> "", txtOriginalSpokenLocale.Text, "en-US") & """>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Filename>" & l_strAudioFilename & "</Filename>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<RegionalEdit>INTL</RegionalEdit>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<Type>Audio</Type>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</AudioAsset>"
    End If
    Print #1, Chr(9) & Chr(9) & "</Content>"
    Print #1, Chr(9) & "</AssetManifest>"
    Print #1, "</Metadata>"
    Close 1
    
End If

chkPackageUsed(1).Value = 1
cmdSave.Value = True
MsgBox "Done"

End Sub

Private Sub cmdClear_Click()

ClearFields Me
lblPackageID.Caption = ""

'adoiTunesPackage.ConnectionString = g_strConnection
'adoiTunesPackage.RecordSource = "SELECT * FROM itunes_package;"
'adoiTunesPackage.Refresh
'
'adoiTunesPackage.Caption = adoiTunesPackage.Recordset.RecordCount & " Packages"
'
End Sub

Private Sub cmdClearPackageID_Click()

lblPackageID.Caption = ""
lblSearchPackage.Caption = ""
adoiTunesPackage.RecordSource = "SELECT * FROM itunes_package;"
adoiTunesPackage.Refresh

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdMakeAssetlessPreorderXML_Click()

Dim l_lngChaptersClipID As Long, l_strPathToFullFile As String, l_strAltFolder As String, l_strFileNameToSave As String
Dim l_strTemp As String, l_lngTemp As Long, l_blnUSProduct As Boolean, l_lngChapterCount As Long

'Check Package Output for Film Packages only.
If cmbiTunesType.Text <> "film" Then
    MsgBox "XML Output is for 'film' packages only" & vbCrLf & "For 'tv' - load the package information into into the appropriate Media Clip" & vbCrLf _
    & "and make XML there.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

cmdSave.Value = True

'Check the compulsory info in the normal controls
If cmbiTunesProvider(0).Text = "" Or txtiTunesVendorID.Text = "" Or txtStudioReleaseTitle.Text = "" Or txtFilmTitle(0).Text = "" Or txtSynopsis(0).Text = "" Or cmbCountry.Text = "" _
Or adoGenres.Recordset.RecordCount <= 0 Or adoProduct.Recordset.RecordCount <= 0 Or Val(txtPreviewClipID(0).Text) = 0 _
Or Val(txtPosterClipID(0).Text) = 0 Or IsNull(datTheatricalReleaseDate(0).Value) _
Or cmbFilmMetadataLanguage(0).Text = "" Or txtOriginalSpokenLocale.Text = "" Or cmbPreviewLocale(0).Text = "" Then
    MsgBox "Compulsory Information is has not yet been completed." & vbCrLf & "All red fields must be filled in," & vbCrLf _
    & "There must be at least one chapter and one Product Item.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

If cmbSubType(0).Text <> "Concert" Then
    If adoCast.Recordset.RecordCount <= 0 Or adoCrew.Recordset.RecordCount <= 0 Then
        MsgBox "Compulsory Information is has not yet been completed." & vbCrLf & "All red fields must be filled in," & vbCrLf _
        & "There must be at least one chapter and one Product Item.", vbCritical, "Cannot Make XML"
        Exit Sub
    End If
Else
    If adoCueSheet.Recordset.RecordCount <= 0 Or adoArtist1.Recordset.RecordCount <= 0 Then
        MsgBox "Compulsory Information is has not yet been completed." & vbCrLf & "All red fields must be filled in," & vbCrLf _
        & "There must be at least one Artist and one Cuesheet Item.", vbCritical, "Cannot Make XML"
        Exit Sub
    End If
End If

If cmbSubType(0).Text <> "concert" Then
    'check the cast info
    adoCast.Refresh
    adoCast.Recordset.MoveFirst
    Do While Not adoCast.Recordset.EOF
        If EmptyCheck(adoCast.Recordset("display_name")) = True Then
            MsgBox "Compulsory Cast Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Exit Sub
        End If
        adoCast.Recordset.MoveNext
    Loop

    'check the crew info
    adoCrew.Refresh
    adoCrew.Recordset.MoveFirst
    Do While Not adoCrew.Recordset.EOF
        If EmptyCheck(adoCrew.Recordset("display_name")) = True Or EmptyCheck(adoCrew.Recordset("role")) Then
            MsgBox "Compulsory Crew Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Exit Sub
        End If
        adoCrew.Recordset.MoveNext
    Loop
Else
    'check the Package Artist info
    adoArtist1.Refresh
    adoArtist1.Recordset.MoveFirst
    Do While Not adoArtist1.Recordset.EOF
        If EmptyCheck(adoArtist1.Recordset("name")) = True Then
            MsgBox "Compulsory Artist Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Exit Sub
        End If
        adoCast.Recordset.MoveNext
    Loop
    'check the cue sheet info
    adoCueSheet.Refresh
    adoCueSheet.Recordset.MoveFirst
    Do While Not adoCueSheet.Recordset.EOF
        If EmptyCheck(adoCueSheet.Recordset("sequence_number")) = True Then
            MsgBox "Compulsory Cue Sheet Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Exit Sub
        End If
        adoCast.Recordset.MoveNext
    Loop
    adoCueSheet.Recordset.MoveFirst
    'check the Package Artist info
    adoArtist2.Refresh
    adoArtist2.Recordset.MoveFirst
    Do While Not adoArtist2.Recordset.EOF
        If EmptyCheck(adoArtist2.Recordset("name")) = True Then
            MsgBox "Compulsory Cue Sheet Artist Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Exit Sub
        End If
        adoCast.Recordset.MoveNext
    Loop
End If

'Check the Product Info
adoProduct.Refresh
adoProduct.Recordset.MoveFirst
Do While Not adoProduct.Recordset.EOF
    If EmptyCheck(adoProduct.Recordset("territory")) = True Or EmptyCheck(adoProduct.Recordset("clearedforsale")) = True Then
        MsgBox "Compulsory Product Information is has not yet been completed." & vbCrLf & "Product Information is not valid.", vbCritical, "Cannot Make XML"
        Exit Sub
    End If
    If adoProduct.Recordset("clearedforvod") = "true" Then
        If EmptyCheck(adoProduct.Recordset("vodtype")) = True Then
            MsgBox "Compulsory Product Information is has not yet been completed." & vbCrLf & "Product Informatino is not valid.", vbCritical, "Cannot Make XML"
            Exit Sub
        Else
            If adoProduct.Recordset("vodtype") = "New Release" Then
                If EmptyCheck(adoProduct.Recordset("availableforvoddate")) = True Or EmptyCheck(adoProduct.Recordset("physicalreleasedate")) = True Then
                    MsgBox "Compulsory Product Information is has not yet been completed." & vbCrLf & "Product Information is not valid.", vbCritical, "Cannot Make XML"
                    Exit Sub
                End If
            End If
        End If
    End If
    adoProduct.Recordset.MoveNext
Loop

l_lngTemp = GetData("events", "libraryID", "eventID", txtFullClipID.Text)
l_strPathToFullFile = GetData("library", "subtitle", "libraryID", l_lngTemp)
l_strAltFolder = GetData("events", "altlocation", "eventID", txtFullClipID.Text)
If l_strAltFolder <> "" Then
    l_strPathToFullFile = l_strPathToFullFile & "\" & l_strAltFolder
End If
MDIForm1.dlgMain.InitDir = l_strPathToFullFile
MDIForm1.dlgMain.Filter = "XML files|*.xml"
MDIForm1.dlgMain.Filename = "metadata.xml"

MDIForm1.dlgMain.ShowSave

l_strFileNameToSave = MDIForm1.dlgMain.Filename

If l_strFileNameToSave <> "" Then

    Open l_strFileNameToSave For Output As 1
    Print #1, "<?xml version=""1.0"" encoding=""UTF-8""?>"
    Print #1, "<package xmlns=""http://apple.com/itunes/importer"" version=""film5.2"">"
    If txtComments.Text <> "" Then Print #1, Chr(9) & "<comments>" & XMLSanitise(txtComments.Text) & "</comments>"
    Print #1, Chr(9) & "<provider>" & XMLSanitise(cmbiTunesProvider(0).Text) & "</provider>"
    Print #1, Chr(9) & "<language>" & cmbFilmMetadataLanguage(0).Text & "</language>"
    Print #1, Chr(9) & "<video>"
    
    'Basic Information
    Print #1, Chr(9) & Chr(9) & "<type>" & cmbiTunesType.Text & "</type>"
    Print #1, Chr(9) & Chr(9) & "<subtype>" & cmbSubType(0).Text & "</subtype>"
    Print #1, Chr(9) & Chr(9) & "<production_company>" & UTF8_Encode(XMLSanitise(txtProductionCompany.Text)) & "</production_company>"
    Print #1, Chr(9) & Chr(9) & "<vendor_id>" & XMLSanitise(txtiTunesVendorID.Text) & "</vendor_id>"
    If txtiTunesVendorOfferCode.Text <> "" Then Print #1, Chr(9) & Chr(9) & "<vendor_offer_code>" & XMLSanitise(txtiTunesVendorOfferCode.Text) & "</vendor_offer_code>"
    If txtiTunesISAN.Text <> "" Then Print #1, Chr(9) & Chr(9) & "<isan>" & txtiTunesISAN.Text & "</isan>"
    If txtUPC.Text <> "" Then Print #1, Chr(9) & Chr(9) & "<upc>" & txtUPC.Text & "</upc>"
    If txtAMGVideoID.Text <> "" Then Print #1, Chr(9) & Chr(9) & "<amg_video_id>" & txtAMGVideoID.Text & "</amg_video_id>"
    Print #1, Chr(9) & Chr(9) & "<country>" & cmbCountry.Columns("iso2acode").Text & "</country>"
    Print #1, Chr(9) & Chr(9) & "<original_spoken_locale>" & txtOriginalSpokenLocale.Text & "</original_spoken_locale>"
    Print #1, Chr(9) & Chr(9) & "<title>" & UTF8_Encode(XMLSanitise(txtFilmTitle(0).Text)) & "</title>"
    Print #1, Chr(9) & Chr(9) & "<studio_release_title>" & UTF8_Encode(XMLSanitise(txtStudioReleaseTitle.Text)) & "</studio_release_title>"
    Print #1, Chr(9) & Chr(9) & "<synopsis>" & UTF8_Encode(XMLSanitise(txtSynopsis(0).Text)) & "</synopsis>"
    Print #1, Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(0).Text)) & "</copyright_cline>"
    Print #1, Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(0).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
    If chkMultiLanguage.Value <> 0 Then
        Print #1, Chr(9) & Chr(9) & "<locales>"
        If cmbFilmMetadataLanguage(1).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(1).Text & """>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<title>" & UTF8_Encode(XMLSanitise(txtFilmTitle(1).Text)) & "</title>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<synopsis>" & UTF8_Encode(XMLSanitise(txtSynopsis(1).Text)) & "</synopsis>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</locale>"
        End If
        If cmbFilmMetadataLanguage(2).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(2).Text & """>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<title>" & UTF8_Encode(XMLSanitise(txtFilmTitle(2).Text)) & "</title>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<synopsis>" & UTF8_Encode(XMLSanitise(txtSynopsis(2).Text)) & "</synopsis>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</locale>"
        End If
        If cmbFilmMetadataLanguage(3).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(3).Text & """>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<title>" & UTF8_Encode(XMLSanitise(txtFilmTitle(3).Text)) & "</title>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<synopsis>" & UTF8_Encode(XMLSanitise(txtSynopsis(3).Text)) & "</synopsis>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</locale>"
        End If
        Print #1, Chr(9) & Chr(9) & "</locales>"
    End If
    If chkMultiLanguage.Value <> 0 Then
        Print #1, Chr(9) & Chr(9) & "<regions>"
        If cmbFilmTerritory(0).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<region>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbFilmTerritory(0).Text & "</territory>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(0).Text)) & "</copyright_cline>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(0).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</region>"
        End If
        If cmbFilmTerritory(1).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<region>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbFilmTerritory(1).Text & "</territory>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(1).Text)) & "</copyright_cline>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(1).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</region>"
        End If
        If cmbFilmTerritory(2).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<region>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbFilmTerritory(2).Text & "</territory>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(2).Text)) & "</copyright_cline>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(2).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</region>"
        End If
        If cmbFilmTerritory(3).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<region>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbFilmTerritory(3).Text & "</territory>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(3).Text)) & "</copyright_cline>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(3).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</region>"
        End If
        Print #1, Chr(9) & Chr(9) & "</regions>"
    Else
        Print #1, Chr(9) & Chr(9) & "<regions>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<region>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbFilmTerritory(0).Text & "</territory>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(0).Text)) & "</copyright_cline>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(0).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</region>"
        Print #1, Chr(9) & Chr(9) & "</regions>"
    End If
    
    'Genre information
    Print #1, Chr(9) & Chr(9) & "<genres>"
    adoGenres.Recordset.MoveFirst
    Do While Not adoGenres.Recordset.EOF
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<genre code=""" & XMLSanitise(adoGenres.Recordset("genrecode")) & """/>"
        adoGenres.Recordset.MoveNext
    Loop
    Print #1, Chr(9) & Chr(9) & "</genres>"
    
    'Ratings information
    WriteiTunesFilmRatings
    
    'Cast and Crew
    If cmbSubType(0) <> "concert" Then
        If WriteCastandCrew = False Then
            MsgBox "Compulsory Cast and Crew Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Close #1
            Exit Sub
        End If
    Else
        If WriteArtistAndCuesheet = False Then
            MsgBox "Compulsory Artist and Cuesheet Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Close #1
            Exit Sub
        End If
    End If
    
    'Asset Information
    Print #1, Chr(9) & Chr(9) & "<assets>"
    
    'Preview Assets
    If WriteFilmPreviewAssets = False Then Exit Sub
    
    'Artwork
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<asset type=""artwork"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territories>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>WW</territory>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</territories>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<data_file>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locales>"
    If cmbPosterLocale(0).Text <> "" And cmbPosterLocale(4).Text <> "" Then
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(0).Text & """/>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(4).Text & """/>"
    Else
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(0).Text & """/>"
    End If
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locales>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", txtPosterClipID(0).Text)) & "</file_name>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<size>" & GetData("events", "bigfilesize", "eventID", txtPosterClipID(0).Text) & "</size>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", txtPosterClipID(0).Text) & "</checksum>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</data_file>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</asset>"
    If Val(txtPosterClipID(1).Text) <> 0 Then
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<asset type=""artwork"">"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbPosterTerritory(1).Text & "</territory>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<data_file>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locales>"
        If cmbPosterLocale(1).Text <> "" And cmbPosterLocale(5).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(1).Text & """/>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(5).Text & """/>"
        ElseIf cmbPosterLocale(1).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(1).Text & """/>"
        Else
            MsgBox "Compulsory Locale Information is has not yet been completed on Artwork Poster 2." & vbCrLf, vbCritical, "Cannot Make XML"
            Close #1
            Exit Sub
        End If
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locales>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", txtPosterClipID(1).Text)) & "</file_name>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<size>" & GetData("events", "bigfilesize", "eventID", txtPosterClipID(1).Text) & "</size>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", txtPosterClipID(1).Text) & "</checksum>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</data_file>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</asset>"
    End If
    If Val(txtPosterClipID(2).Text) <> 0 Then
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<asset type=""artwork"">"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbPosterTerritory(2).Text & "</territory>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<data_file>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locales>"
        If cmbPosterLocale(2).Text <> "" And cmbPosterLocale(6).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(2).Text & """/>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(6).Text & """/>"
        ElseIf cmbPosterLocale(2).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(2).Text & """/>"
        Else
            MsgBox "Compulsory Locale Information is has not yet been completed on Artwork Poster 3." & vbCrLf, vbCritical, "Cannot Make XML"
            Close #1
            Exit Sub
        End If
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locales>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", txtPosterClipID(2).Text)) & "</file_name>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<size>" & GetData("events", "bigfilesize", "eventID", txtPosterClipID(2).Text) & "</size>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", txtPosterClipID(2).Text) & "</checksum>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</data_file>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</asset>"
    End If
    If Val(txtPosterClipID(3).Text) <> 0 Then
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<asset type=""artwork"">"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbPosterTerritory(3).Text & "</territory>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<data_file>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locales>"
        If cmbPosterLocale(3).Text <> "" And cmbPosterLocale(7).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(3).Text & """/>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(7).Text & """/>"
        ElseIf cmbPosterLocale(3).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(3).Text & """/>"
        Else
            MsgBox "Compulsory Locale Information is has not yet been completed on Artwork Poster 2." & vbCrLf, vbCritical, "Cannot Make XML"
            Close #1
            Exit Sub
        End If
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locales>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", txtPosterClipID(3).Text)) & "</file_name>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<size>" & GetData("events", "bigfilesize", "eventID", txtPosterClipID(3).Text) & "</size>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", txtPosterClipID(3).Text) & "</checksum>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</data_file>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</asset>"
    End If
    Print #1, Chr(9) & Chr(9) & "</assets>"
    
    'Products Information
    adoProduct.Recordset.MoveFirst
    Print #1, Chr(9) & Chr(9) & "<products>"
    Do While Not adoProduct.Recordset.EOF
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<product>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & adoProduct.Recordset("territory") & "</territory>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<cleared_for_sale>" & adoProduct.Recordset("clearedforsale") & "</cleared_for_sale>"
        If EmptyCheck(adoProduct.Recordset("clearedforhdsale")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<cleared_for_hd_sale>" & adoProduct.Recordset("clearedforhdsale") & "</cleared_for_hd_sale>"
        End If
        If EmptyCheck(adoProduct.Recordset("wholesalepricetier")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<wholesale_price_tier>" & adoProduct.Recordset("wholesalepricetier") & "</wholesale_price_tier>"
        End If
        If EmptyCheck(adoProduct.Recordset("hdwholesalepricetier")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<hd_wholesale_price_tier>" & adoProduct.Recordset("hdwholesalepricetier") & "</hd_wholesale_price_tier>"
        End If
        If EmptyCheck(adoProduct.Recordset("preordersalesstartdate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<preorder_sales_start_date>" & Format(adoProduct.Recordset("preordersalesstartdate"), "yyyy-mm-dd") & "</preorder_sales_start_date>"
        End If
        If EmptyCheck(adoProduct.Recordset("salesstartdate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<sales_start_date>" & Format(adoProduct.Recordset("salesstartdate"), "yyyy-mm-dd") & "</sales_start_date>"
        End If
        If EmptyCheck(adoProduct.Recordset("salesenddate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<sales_end_date>" & Format(adoProduct.Recordset("salesenddate"), "yyyy-mm-dd") & "</sales_end_date>"
        End If
        If EmptyCheck(adoProduct.Recordset("clearedforvod")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<cleared_for_vod>" & adoProduct.Recordset("clearedforvod") & "</cleared_for_vod>"
        End If
        If EmptyCheck(adoProduct.Recordset("clearedforhdvod")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<cleared_for_hd_vod>" & adoProduct.Recordset("clearedforhdvod") & "</cleared_for_hd_vod>"
        End If
        If EmptyCheck(adoProduct.Recordset("vodtype")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<vod_type>" & adoProduct.Recordset("vodtype") & "</vod_type>"
        End If
        If EmptyCheck(adoProduct.Recordset("availableforvoddate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<available_for_vod_date>" & Format(adoProduct.Recordset("availableforvoddate"), "yyyy-mm-dd") & "</available_for_vod_date>"
        End If
        If EmptyCheck(adoProduct.Recordset("unavailableforvoddate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<unavailable_for_vod_date>" & Format(adoProduct.Recordset("unavailableforvoddate"), "yyyy-mm-dd") & "</unavailable_for_vod_date>"
        End If
        If EmptyCheck(adoProduct.Recordset("physicalreleasedate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<physical_release_date>" & Format(adoProduct.Recordset("physicalreleasedate"), "yyyy-mm-dd") & "</physical_release_date>"
        End If
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</product>"
        adoProduct.Recordset.MoveNext
    Loop
    Print #1, Chr(9) & Chr(9) & "</products>"
    
    Print #1, Chr(9) & "</video>"
    Print #1, "</package>"
    
    Close 1
    
End If

End Sub

Private Sub cmdMakeAssetWithChapters_Click()

Dim l_lngChaptersClipID As Long, l_strPathToFullFile As String, l_strAltFolder As String, l_strFileNameToSave As String
Dim l_strTemp As String, l_lngTemp As Long, l_blnUSProduct As Boolean, l_lngChapterCount As Long

'Check Package Output for Film Packages only.
If cmbiTunesType.Text <> "film" Then
    MsgBox "XML Output is for 'film' packages only" & vbCrLf & "For 'tv' - load the package information into into the appropriate Media Clip" & vbCrLf _
    & "and make XML there.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

cmdSave.Value = True

'Check the compulsory info in the normal controls
If cmbiTunesProvider(0).Text = "" Or txtiTunesVendorID.Text = "" _
Or Val(txtFullClipID.Text) = 0 _
Or cmbFullLocale(0).Text = "" Then
    MsgBox "Compulsory Information is has not yet been completed." & vbCrLf & "All red fields must be filled in," & vbCrLf _
    & "There must be at least one chapter and one Product Item.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

l_lngTemp = GetData("events", "libraryID", "eventID", txtFullClipID.Text)
l_strPathToFullFile = GetData("library", "subtitle", "libraryID", l_lngTemp)
l_strAltFolder = GetData("events", "altlocation", "eventID", txtFullClipID.Text)
If l_strAltFolder <> "" Then
    l_strPathToFullFile = l_strPathToFullFile & "\" & l_strAltFolder
End If
MDIForm1.dlgMain.InitDir = l_strPathToFullFile
MDIForm1.dlgMain.Filter = "XML files|*.xml"
MDIForm1.dlgMain.Filename = "metadata.xml"

MDIForm1.dlgMain.ShowSave

l_strFileNameToSave = MDIForm1.dlgMain.Filename

If l_strFileNameToSave <> "" Then

    Open l_strFileNameToSave For Output As 1
    Print #1, "<?xml version=""1.0"" encoding=""UTF-8""?>"
    Print #1, "<package xmlns=""http://apple.com/itunes/importer"" version=""film5.2"">"
    Print #1, Chr(9) & "<provider>" & XMLSanitise(cmbiTunesProvider(0).Text) & "</provider>"
    
    'Basic Information
    'Asset Information
    Print #1, Chr(9) & "<assets media_type=""video"" vendor_id=""" & XMLSanitise(txtiTunesVendorID.Text) & """>"
    
    'Main Asset
    WriteFilmMainAsset True, True
        
    Print #1, Chr(9) & "</assets>"
    
    Print #1, "</package>"
    
    Close 1
    
End If

End Sub

Private Sub cmdMakeFeatureAssetOnlyUpdateXML_Click()

Dim l_lngChaptersClipID As Long, l_strPathToFullFile As String, l_strAltFolder As String, l_strFileNameToSave As String
Dim l_strTemp As String, l_lngTemp As Long, l_blnUSProduct As Boolean, l_lngChapterCount As Long

'Check Package Output for Film Packages only.
If cmbiTunesType.Text <> "film" Then
    MsgBox "XML Output is for 'film' packages only" & vbCrLf & "For 'tv' - load the package information into into the appropriate Media Clip" & vbCrLf _
    & "and make XML there.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

cmdSave.Value = True

'Check the compulsory info in the normal controls
If cmbiTunesProvider(0).Text = "" Or txtiTunesVendorID.Text = "" _
Or Val(txtFullClipID.Text) = 0 _
Or cmbFullLocale(0).Text = "" Then
    MsgBox "Compulsory Information is has not yet been completed." & vbCrLf & "All red fields must be filled in," & vbCrLf _
    & "There must be at least one chapter and one Product Item.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

l_lngTemp = GetData("events", "libraryID", "eventID", txtFullClipID.Text)
l_strPathToFullFile = GetData("library", "subtitle", "libraryID", l_lngTemp)
l_strAltFolder = GetData("events", "altlocation", "eventID", txtFullClipID.Text)
If l_strAltFolder <> "" Then
    l_strPathToFullFile = l_strPathToFullFile & "\" & l_strAltFolder
End If
MDIForm1.dlgMain.InitDir = l_strPathToFullFile
MDIForm1.dlgMain.Filter = "XML files|*.xml"
MDIForm1.dlgMain.Filename = "metadata.xml"

MDIForm1.dlgMain.ShowSave

l_strFileNameToSave = MDIForm1.dlgMain.Filename

If l_strFileNameToSave <> "" Then

    Open l_strFileNameToSave For Output As 1
    Print #1, "<?xml version=""1.0"" encoding=""UTF-8""?>"
    Print #1, "<package xmlns=""http://apple.com/itunes/importer"" version=""film5.2"">"
    Print #1, Chr(9) & "<provider>" & XMLSanitise(cmbiTunesProvider(0).Text) & "</provider>"
    
    'Basic Information
    'Asset Information
    Print #1, Chr(9) & "<assets media_type=""video"" vendor_id=""" & XMLSanitise(txtiTunesVendorID.Text) & """>"
    
    'Main Asset
    WriteFilmMainAsset True
        
    Print #1, Chr(9) & "</assets>"
    
    Print #1, "</package>"
    
    Close 1
    
End If

End Sub

Private Sub cmdMakeFullAssetOnlyXML_Click()

Dim l_lngChaptersClipID As Long, l_strPathToFullFile As String, l_strAltFolder As String, l_strFileNameToSave As String
Dim l_strTemp As String, l_lngTemp As Long, l_blnUSProduct As Boolean, l_lngChapterCount As Long

'Check Package Output for Film Packages only.
If cmbiTunesType.Text <> "film" Then
    MsgBox "XML Output is for 'film' packages only" & vbCrLf & "For 'tv' - load the package information into into the appropriate Media Clip" & vbCrLf _
    & "and make XML there.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

cmdSave.Value = True

'Check the compulsory info in the normal controls
If cmbiTunesProvider(0).Text = "" Or txtiTunesVendorID.Text = "" _
Or Val(txtFullClipID.Text) = 0 Or Val(txtPreviewClipID(0).Text) = 0 _
Or Val(txtPosterClipID(0).Text) = 0 _
Or cmbPreviewLocale(0).Text = "" Or cmbFullLocale(0).Text = "" Then
    MsgBox "Compulsory Information is has not yet been completed." & vbCrLf & "All red fields must be filled in," & vbCrLf _
    & "There must be at least one chapter and one Product Item.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

l_lngTemp = GetData("events", "libraryID", "eventID", txtFullClipID.Text)
l_strPathToFullFile = GetData("library", "subtitle", "libraryID", l_lngTemp)
l_strAltFolder = GetData("events", "altlocation", "eventID", txtFullClipID.Text)
If l_strAltFolder <> "" Then
    l_strPathToFullFile = l_strPathToFullFile & "\" & l_strAltFolder
End If
MDIForm1.dlgMain.InitDir = l_strPathToFullFile
MDIForm1.dlgMain.Filter = "XML files|*.xml"
MDIForm1.dlgMain.Filename = "metadata.xml"

MDIForm1.dlgMain.ShowSave

l_strFileNameToSave = MDIForm1.dlgMain.Filename

If l_strFileNameToSave <> "" Then

    Open l_strFileNameToSave For Output As 1
    Print #1, "<?xml version=""1.0"" encoding=""UTF-8""?>"
    Print #1, "<package xmlns=""http://apple.com/itunes/importer"" version=""film5.2"">"
    Print #1, Chr(9) & "<provider>" & XMLSanitise(cmbiTunesProvider(0).Text) & "</provider>"
    
    'Basic Information
    'Asset Information
    Print #1, Chr(9) & "<assets media_type=""video"" vendor_id=""" & XMLSanitise(txtiTunesVendorID.Text) & """>"
    
    'Main Asset
    If WriteFilmMainAsset(True) = False Then Exit Sub
    
    'Preview Assets
    If WriteFilmPreviewAssets(True) = False Then Exit Sub
    
    'Artwork
    Print #1, Chr(9) & Chr(9) & "<asset type=""artwork"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<territories>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>WW</territory>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</territories>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<data_file>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locales>"
    If cmbPosterLocale(0).Text <> "" And cmbPosterLocale(4).Text <> "" Then
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(0).Text & """/>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(4).Text & """/>"
    Else
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(0).Text & """/>"
    End If
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locales>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", txtPosterClipID(0).Text)) & "</file_name>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<size>" & GetData("events", "bigfilesize", "eventID", txtPosterClipID(0).Text) & "</size>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", txtPosterClipID(0).Text) & "</checksum>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</data_file>"
    Print #1, Chr(9) & Chr(9) & "</asset>"
    If Val(txtPosterClipID(1).Text) <> 0 Then
        Print #1, Chr(9) & Chr(9) & "<asset type=""artwork"">"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbPosterTerritory(1).Text & "</territory>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<data_file>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locales>"
        If cmbPosterLocale(1).Text <> "" And cmbPosterLocale(5).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(1).Text & """/>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(5).Text & """/>"
        ElseIf cmbPosterLocale(1).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(1).Text & """/>"
        Else
            MsgBox "Compulsory Locale Information is has not yet been completed on Artwork Poster 2." & vbCrLf, vbCritical, "Cannot Make XML"
            Close #1
            Exit Sub
        End If
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locales>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", txtPosterClipID(1).Text)) & "</file_name>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<size>" & GetData("events", "bigfilesize", "eventID", txtPosterClipID(1).Text) & "</size>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", txtPosterClipID(1).Text) & "</checksum>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</data_file>"
        Print #1, Chr(9) & Chr(9) & "</asset>"
    End If
    If Val(txtPosterClipID(2).Text) <> 0 Then
        Print #1, Chr(9) & Chr(9) & "<asset type=""artwork"">"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbPosterTerritory(2).Text & "</territory>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<data_file>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locales>"
        If cmbPosterLocale(2).Text <> "" And cmbPosterLocale(6).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(2).Text & """/>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(6).Text & """/>"
        ElseIf cmbPosterLocale(2).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(2).Text & """/>"
        Else
            MsgBox "Compulsory Locale Information is has not yet been completed on Artwork Poster 3." & vbCrLf, vbCritical, "Cannot Make XML"
            Close #1
            Exit Sub
        End If
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locales>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", txtPosterClipID(2).Text)) & "</file_name>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<size>" & GetData("events", "bigfilesize", "eventID", txtPosterClipID(2).Text) & "</size>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", txtPosterClipID(2).Text) & "</checksum>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</data_file>"
        Print #1, Chr(9) & Chr(9) & "</asset>"
    End If
    If Val(txtPosterClipID(3).Text) <> 0 Then
        Print #1, Chr(9) & Chr(9) & "<asset type=""artwork"">"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbPosterTerritory(3).Text & "</territory>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<data_file>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locales>"
        If cmbPosterLocale(3).Text <> "" And cmbPosterLocale(7).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(3).Text & """/>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(7).Text & """/>"
        ElseIf cmbPosterLocale(3).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(3).Text & """/>"
        Else
            MsgBox "Compulsory Locale Information is has not yet been completed on Artwork Poster 2." & vbCrLf, vbCritical, "Cannot Make XML"
            Close #1
            Exit Sub
        End If
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locales>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", txtPosterClipID(3).Text)) & "</file_name>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<size>" & GetData("events", "bigfilesize", "eventID", txtPosterClipID(3).Text) & "</size>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", txtPosterClipID(3).Text) & "</checksum>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</data_file>"
        Print #1, Chr(9) & Chr(9) & "</asset>"
    End If
    Print #1, Chr(9) & "</assets>"
    
    Print #1, "</package>"
    
    Close 1
    
End If

End Sub

Private Sub cmdMakeMetadataOnlyXML_Click()

Dim l_lngChaptersClipID As Long, l_strPathToFullFile As String, l_strAltFolder As String, l_strFileNameToSave As String
Dim l_strTemp As String, l_lngTemp As Long, l_blnUSProduct As Boolean, l_lngChapterCount As Long

'Check Package Output for Film Packages only.
If cmbiTunesType.Text <> "film" Then
    MsgBox "XML Output is for 'film' packages only" & vbCrLf & "For 'tv' - load the package information into into the appropriate Media Clip" & vbCrLf _
    & "and make XML there.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

cmdSave.Value = True

'Check the compulsory info in the normal controls
If cmbiTunesProvider(0).Text = "" Or txtiTunesVendorID.Text = "" Or txtTitle.Text = "" Or txtSynopsis(0).Text = "" Or cmbCountry.Text = "" _
Or adoGenres.Recordset.RecordCount <= 0 Or adoProduct.Recordset.RecordCount <= 0 _
Or adoChapters.Recordset.RecordCount <= 0 Or IsNull(datTheatricalReleaseDate(0).Value) _
Or cmbFilmMetadataLanguage(0).Text = "" Or txtOriginalSpokenLocale.Text = "" Then
    MsgBox "Compulsory Information is has not yet been completed." & vbCrLf & "All red fields must be filled in except for Asset items," & vbCrLf _
    & "There must be at least one chapter and one Product Item.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

If cmbSubType(0).Text <> "Concert" Then
    If adoCast.Recordset.RecordCount <= 0 Or adoCrew.Recordset.RecordCount <= 0 Then
        MsgBox "Compulsory Information is has not yet been completed." & vbCrLf & "All red fields must be filled in," & vbCrLf _
        & "There must be at least one chapter and one Product Item.", vbCritical, "Cannot Make XML"
        Exit Sub
    End If
Else
    If adoCueSheet.Recordset.RecordCount <= 0 Or adoArtist1.Recordset.RecordCount <= 0 Then
        MsgBox "Compulsory Information is has not yet been completed." & vbCrLf & "All red fields must be filled in," & vbCrLf _
        & "There must be at least one Artist and one Cuesheet Item.", vbCritical, "Cannot Make XML"
        Exit Sub
    End If
End If

If cmbSubType(0).Text <> "concert" Then
    'check the cast info
    adoCast.Refresh
    adoCast.Recordset.MoveFirst
    Do While Not adoCast.Recordset.EOF
        If EmptyCheck(adoCast.Recordset("display_name")) = True Then
            MsgBox "Compulsory Cast Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Exit Sub
        End If
        adoCast.Recordset.MoveNext
    Loop

    'check the crew info
    adoCrew.Refresh
    adoCrew.Recordset.MoveFirst
    Do While Not adoCrew.Recordset.EOF
        If EmptyCheck(adoCrew.Recordset("display_name")) = True Or EmptyCheck(adoCrew.Recordset("role")) Then
            MsgBox "Compulsory Crew Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Exit Sub
        End If
        adoCrew.Recordset.MoveNext
    Loop
Else
    'check the Package Artist info
    adoArtist1.Refresh
    adoArtist1.Recordset.MoveFirst
    Do While Not adoArtist1.Recordset.EOF
        If EmptyCheck(adoArtist1.Recordset("name")) = True Then
            MsgBox "Compulsory Artist Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Exit Sub
        End If
        adoCast.Recordset.MoveNext
    Loop
    'check the cue sheet info
    adoCueSheet.Refresh
    adoCueSheet.Recordset.MoveFirst
    Do While Not adoCueSheet.Recordset.EOF
        If EmptyCheck(adoCueSheet.Recordset("sequence_number")) = True Then
            MsgBox "Compulsory Cue Sheet Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Exit Sub
        End If
        adoCast.Recordset.MoveNext
    Loop
    adoCueSheet.Recordset.MoveFirst
    'check the Package Artist info
    adoArtist2.Refresh
    adoArtist2.Recordset.MoveFirst
    Do While Not adoArtist2.Recordset.EOF
        If EmptyCheck(adoArtist2.Recordset("name")) = True Then
            MsgBox "Compulsory Cue Sheet Artist Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Exit Sub
        End If
        adoCast.Recordset.MoveNext
    Loop
End If

'check the chapter info
adoChapters.Refresh
adoChapters.Recordset.MoveFirst
Do While Not adoChapters.Recordset.EOF
    If EmptyCheck(adoChapters.Recordset("starttime")) = True Or (EmptyCheck(adoChapters.Recordset("title1")) = True And chkCustomChapterTitles.Value <> 0) Or Val(Trim(" " & adoChapters.Recordset("pictureclipid"))) = 0 Then
        MsgBox "Compulsory Chapter Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
        Exit Sub
    End If
    adoChapters.Recordset.MoveNext
Loop
adoChapters.Recordset.MoveFirst
If adoChapters.Recordset("starttime") <> "00:00:00" Then
    MsgBox "Compulsory Chapter Information is has not yet been completed." & vbCrLf & "First Chapter must start at 00:00:00", vbCritical, "Cannot Make XML"
    Exit Sub
End If

'Check the Product Info
adoProduct.Refresh
adoProduct.Recordset.MoveFirst
Do While Not adoProduct.Recordset.EOF
    If EmptyCheck(adoProduct.Recordset("territory")) = True Or EmptyCheck(adoProduct.Recordset("clearedforsale")) = True Then
        MsgBox "Compulsory Chapter Information is has not yet been completed." & vbCrLf & "Product Information is not valid.", vbCritical, "Cannot Make XML"
        Exit Sub
    End If
    If adoProduct.Recordset("clearedforvod") = "true" Then
        If EmptyCheck(adoProduct.Recordset("vodtype")) = True Then
            MsgBox "Compulsory Chapter Information is has not yet been completed." & vbCrLf & "Product Information is not valid.", vbCritical, "Cannot Make XML"
            Exit Sub
        Else
            If adoProduct.Recordset("vodtype") = "New Release" Then
                If EmptyCheck(adoProduct.Recordset("availableforvoddate")) = True Or EmptyCheck(adoProduct.Recordset("physicalreleasedate")) = True Then
                    MsgBox "Compulsory Product Information is has not yet been completed." & vbCrLf & "Product Information is not valid.", vbCritical, "Cannot Make XML"
                    Exit Sub
                End If
            End If
        End If
    End If
    adoProduct.Recordset.MoveNext
Loop

l_lngTemp = GetData("events", "libraryID", "eventID", txtFullClipID.Text)
If l_lngTemp <> 0 Then
    l_strPathToFullFile = GetData("library", "subtitle", "libraryID", l_lngTemp)
    l_strAltFolder = GetData("events", "altlocation", "eventID", txtFullClipID.Text)
    If l_strAltFolder <> "" Then
        l_strPathToFullFile = l_strPathToFullFile & "\" & l_strAltFolder
    End If
    MDIForm1.dlgMain.InitDir = l_strPathToFullFile
End If
MDIForm1.dlgMain.Filter = "XML files|*.xml"
MDIForm1.dlgMain.Filename = "metadata.xml"

MDIForm1.dlgMain.ShowSave

l_strFileNameToSave = MDIForm1.dlgMain.Filename

If l_strFileNameToSave <> "" Then

    Open l_strFileNameToSave For Output As 1
    Print #1, "<?xml version=""1.0"" encoding=""UTF-8""?>"
    Print #1, "<package xmlns=""http://apple.com/itunes/importer"" version=""film5.2"">"
    If txtComments.Text <> "" Then Print #1, Chr(9) & "<comments>" & XMLSanitise(txtComments.Text) & "</comments>"
    Print #1, Chr(9) & "<provider>" & XMLSanitise(cmbiTunesProvider(0).Text) & "</provider>"
    Print #1, Chr(9) & "<language>" & cmbFilmMetadataLanguage(0).Text & "</language>"
    Print #1, Chr(9) & "<video>"
    
    'Basic Information
    Print #1, Chr(9) & Chr(9) & "<type>" & cmbiTunesType.Text & "</type>"
    Print #1, Chr(9) & Chr(9) & "<subtype>" & cmbSubType(0).Text & "</subtype>"
    Print #1, Chr(9) & Chr(9) & "<production_company>" & UTF8_Encode(XMLSanitise(txtProductionCompany.Text)) & "</production_company>"
    Print #1, Chr(9) & Chr(9) & "<vendor_id>" & XMLSanitise(txtiTunesVendorID.Text) & "</vendor_id>"
    If txtiTunesISAN.Text <> "" Then Print #1, Chr(9) & Chr(9) & "<isan>" & txtiTunesISAN.Text & "</isan>"
    If txtUPC.Text <> "" Then Print #1, Chr(9) & Chr(9) & "<upc>" & txtUPC.Text & "</upc>"
    If txtiTunesVendorOfferCode.Text <> "" Then Print #1, Chr(9) & Chr(9) & "<vendor_offer_code>" & XMLSanitise(txtiTunesVendorOfferCode.Text) & "</vendor_offer_code>"
    If txtAMGVideoID.Text <> "" Then Print #1, Chr(9) & Chr(9) & "<amg_video_id>" & txtAMGVideoID.Text & "</amg_video_id>"
    Print #1, Chr(9) & Chr(9) & "<country>" & cmbCountry.Columns("iso2acode").Text & "</country>"
    Print #1, Chr(9) & Chr(9) & "<original_spoken_locale>" & txtOriginalSpokenLocale.Text & "</original_spoken_locale>"
    Print #1, Chr(9) & Chr(9) & "<title>" & UTF8_Encode(XMLSanitise(txtFilmTitle(0).Text)) & "</title>"
    Print #1, Chr(9) & Chr(9) & "<studio_release_title>" & UTF8_Encode(XMLSanitise(txtStudioReleaseTitle.Text)) & "</studio_release_title>"
    Print #1, Chr(9) & Chr(9) & "<synopsis>" & UTF8_Encode(XMLSanitise(txtSynopsis(0).Text)) & "</synopsis>"
    Print #1, Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(0).Text)) & "</copyright_cline>"
    Print #1, Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(0).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
    If chkMultiLanguage.Value <> 0 Then
        Print #1, Chr(9) & Chr(9) & "<locales>"
        If cmbFilmMetadataLanguage(1).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(1).Text & """>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<title>" & UTF8_Encode(XMLSanitise(txtFilmTitle(1).Text)) & "</title>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<synopsis>" & UTF8_Encode(XMLSanitise(txtSynopsis(1).Text)) & "</synopsis>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</locale>"
        End If
        If cmbFilmMetadataLanguage(2).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(2).Text & """>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<title>" & UTF8_Encode(XMLSanitise(txtFilmTitle(2).Text)) & "</title>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<synopsis>" & UTF8_Encode(XMLSanitise(txtSynopsis(2).Text)) & "</synopsis>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</locale>"
        End If
        If cmbFilmMetadataLanguage(3).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(3).Text & """>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<title>" & UTF8_Encode(XMLSanitise(txtFilmTitle(3).Text)) & "</title>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<synopsis>" & UTF8_Encode(XMLSanitise(txtSynopsis(3).Text)) & "</synopsis>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</locale>"
        End If
        Print #1, Chr(9) & Chr(9) & "</locales>"
    End If
    If chkMultiLanguage.Value <> 0 Then
        Print #1, Chr(9) & Chr(9) & "<regions>"
        If cmbFilmTerritory(0).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<region>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbFilmTerritory(0).Text & "</territory>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(0).Text)) & "</copyright_cline>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(0).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</region>"
        End If
        If cmbFilmTerritory(1).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<region>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbFilmTerritory(1).Text & "</territory>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(1).Text)) & "</copyright_cline>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(1).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</region>"
        End If
        If cmbFilmTerritory(2).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<region>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbFilmTerritory(2).Text & "</territory>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(2).Text)) & "</copyright_cline>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(2).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</region>"
        End If
        If cmbFilmTerritory(3).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<region>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbFilmTerritory(3).Text & "</territory>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(3).Text)) & "</copyright_cline>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(3).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</region>"
        End If
        Print #1, Chr(9) & Chr(9) & "</regions>"
    Else
        Print #1, Chr(9) & Chr(9) & "<regions>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<region>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbFilmTerritory(0).Text & "</territory>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(0).Text)) & "</copyright_cline>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(0).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</region>"
        Print #1, Chr(9) & Chr(9) & "</regions>"
    End If
    
    'Genre information
    Print #1, Chr(9) & Chr(9) & "<genres>"
    adoGenres.Recordset.MoveFirst
    Do While Not adoGenres.Recordset.EOF
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<genre code=""" & XMLSanitise(adoGenres.Recordset("genrecode")) & """/>"
        adoGenres.Recordset.MoveNext
    Loop
    Print #1, Chr(9) & Chr(9) & "</genres>"
    
    'Ratings information
    WriteiTunesFilmRatings
    
    'Cast and Crew
    If cmbSubType(0) <> "concert" Then
        If WriteCastandCrew = False Then
            MsgBox "Compulsory Cast and Crew Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Close #1
            Exit Sub
        End If
    Else
        If WriteArtistAndCuesheet = False Then
            MsgBox "Compulsory Artist and Cuesheet Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Close #1
            Exit Sub
        End If
    End If
    
    'Chapters
    WriteFilmChapterInfo
    
    'Accessibility Information (Closed Captions)
    l_blnUSProduct = False
    If adoProduct.Recordset.RecordCount > 0 Then
        adoProduct.Recordset.MoveFirst
        Do While Not adoProduct.Recordset.EOF
            If adoProduct.Recordset("territory") = "US" Then
                l_blnUSProduct = True
                Exit Do
            End If
            adoProduct.Recordset.MoveNext
        Loop
    End If
    If l_blnUSProduct = True Or chkCaptionsAvailability.Value <> 0 Then
        Print #1, Chr(9) & Chr(9) & "<accessibility_info>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<accessibility role=""captions"" ";
        If chkCaptionsAvailability.Value <> 0 Then
            Print #1, "available=""true""/>"
        Else
            Print #1, "available=""false"" reason_code=""" & cmbCaptionsReason.Text & """/>"
        End If
        Print #1, Chr(9) & Chr(9) & "</accessibility_info>"
    End If
    
    'Products Information
    adoProduct.Recordset.MoveFirst
    Print #1, Chr(9) & Chr(9) & "<products>"
    Do While Not adoProduct.Recordset.EOF
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<product>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & adoProduct.Recordset("territory") & "</territory>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<cleared_for_sale>" & adoProduct.Recordset("clearedforsale") & "</cleared_for_sale>"
        If EmptyCheck(adoProduct.Recordset("clearedforhdsale")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<cleared_for_hd_sale>" & adoProduct.Recordset("clearedforhdsale") & "</cleared_for_hd_sale>"
        End If
        If EmptyCheck(adoProduct.Recordset("wholesalepricetier")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<wholesale_price_tier>" & adoProduct.Recordset("wholesalepricetier") & "</wholesale_price_tier>"
        End If
        If EmptyCheck(adoProduct.Recordset("hdwholesalepricetier")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<hd_wholesale_price_tier>" & adoProduct.Recordset("hdwholesalepricetier") & "</hd_wholesale_price_tier>"
        End If
        If EmptyCheck(adoProduct.Recordset("preordersalesstartdate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<preorder_sales_start_date>" & Format(adoProduct.Recordset("preordersalesstartdate"), "yyyy-mm-dd") & "</preorder_sales_start_date>"
        End If
        If EmptyCheck(adoProduct.Recordset("salesstartdate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<sales_start_date>" & Format(adoProduct.Recordset("salesstartdate"), "yyyy-mm-dd") & "</sales_start_date>"
        End If
        If EmptyCheck(adoProduct.Recordset("salesenddate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<sales_end_date>" & Format(adoProduct.Recordset("salesenddate"), "yyyy-mm-dd") & "</sales_end_date>"
        End If
        If EmptyCheck(adoProduct.Recordset("clearedforvod")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<cleared_for_vod>" & adoProduct.Recordset("clearedforvod") & "</cleared_for_vod>"
        End If
        If EmptyCheck(adoProduct.Recordset("clearedforhdvod")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<cleared_for_hd_vod>" & adoProduct.Recordset("clearedforhdvod") & "</cleared_for_hd_vod>"
        End If
        If EmptyCheck(adoProduct.Recordset("vodtype")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<vod_type>" & adoProduct.Recordset("vodtype") & "</vod_type>"
        End If
        If EmptyCheck(adoProduct.Recordset("availableforvoddate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<available_for_vod_date>" & Format(adoProduct.Recordset("availableforvoddate"), "yyyy-mm-dd") & "</available_for_vod_date>"
        End If
        If EmptyCheck(adoProduct.Recordset("unavailableforvoddate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<unavailable_for_vod_date>" & Format(adoProduct.Recordset("unavailableforvoddate"), "yyyy-mm-dd") & "</unavailable_for_vod_date>"
        End If
        If EmptyCheck(adoProduct.Recordset("physicalreleasedate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<physical_release_date>" & Format(adoProduct.Recordset("physicalreleasedate"), "yyyy-mm-dd") & "</physical_release_date>"
        End If
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</product>"
        adoProduct.Recordset.MoveNext
    Loop
    Print #1, Chr(9) & Chr(9) & "</products>"
    
    Print #1, Chr(9) & "</video>"
    Print #1, "</package>"
    
    Close 1
    
End If

End Sub

Private Sub cmdMakePreviewAssetOnlyUpdateXML_Click()

Dim l_lngChaptersClipID As Long, l_strPathToFullFile As String, l_strAltFolder As String, l_strFileNameToSave As String
Dim l_strTemp As String, l_lngTemp As Long, l_blnUSProduct As Boolean, l_lngChapterCount As Long

'Check Package Output for Film Packages only.
If cmbiTunesType.Text <> "film" Then
    MsgBox "XML Output is for 'film' packages only" & vbCrLf & "For 'tv' - load the package information into into the appropriate Media Clip" & vbCrLf _
    & "and make XML there.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

cmdSave.Value = True

'Check the compulsory info in the normal controls
If cmbiTunesProvider(0).Text = "" Or txtiTunesVendorID.Text = "" _
Or Val(txtPreviewClipID(0).Text) = 0 _
Or cmbPreviewLocale(0).Text = "" Then
    MsgBox "Compulsory Information is has not yet been completed." & vbCrLf & "All red fields must be filled in," & vbCrLf _
    & "There must be at least one chapter and one Product Item.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

l_lngTemp = GetData("events", "libraryID", "eventID", txtPreviewClipID(0).Text)
l_strPathToFullFile = GetData("library", "subtitle", "libraryID", l_lngTemp)
l_strAltFolder = GetData("events", "altlocation", "eventID", txtFullClipID.Text)
If l_strAltFolder <> "" Then
    l_strPathToFullFile = l_strPathToFullFile & "\" & l_strAltFolder
End If
MDIForm1.dlgMain.InitDir = l_strPathToFullFile
MDIForm1.dlgMain.Filter = "XML files|*.xml"
MDIForm1.dlgMain.Filename = "metadata.xml"

MDIForm1.dlgMain.ShowSave

l_strFileNameToSave = MDIForm1.dlgMain.Filename

If l_strFileNameToSave <> "" Then

    Open l_strFileNameToSave For Output As 1
    Print #1, "<?xml version=""1.0"" encoding=""UTF-8""?>"
    Print #1, "<package xmlns=""http://apple.com/itunes/importer"" version=""film5.2"">"
    Print #1, Chr(9) & "<provider>" & XMLSanitise(cmbiTunesProvider(0).Text) & "</provider>"
    
    'Basic Information
    'Asset Information
    Print #1, Chr(9) & "<assets media_type=""video"" vendor_id=""" & XMLSanitise(txtiTunesVendorID.Text) & """>"
    
    'Preview Assets
    WriteFilmPreviewAssets True
    
    Print #1, Chr(9) & "</assets>"
    
    Print #1, "</package>"
    
    Close 1
    
End If

End Sub

Private Sub cmdMakeXMLFiles_Click()

Dim l_lngChaptersClipID As Long, l_strPathToFullFile As String, l_strAltFolder As String, l_strFileNameToSave As String
Dim l_strTemp As String, l_lngTemp As Long, l_blnUSProduct As Boolean, l_lngChapterCount As Long

'Check Package Output for Film Packages only.
If cmbiTunesType.Text <> "film" Then
    MsgBox "XML Output is for 'film' packages only" & vbCrLf & "For 'tv' - load the package information into into the appropriate Media Clip" & vbCrLf _
    & "and make XML there.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

cmdSave.Value = True

'Check the compulsory info in the normal controls
If cmbiTunesProvider(0).Text = "" Or txtiTunesVendorID.Text = "" Or txtStudioReleaseTitle.Text = "" Or txtFilmTitle(0).Text = "" Or txtSynopsis(0).Text = "" Or cmbCountry.Text = "" _
Or adoGenres.Recordset.RecordCount <= 0 Or adoProduct.Recordset.RecordCount <= 0 Or Val(txtFullClipID.Text) = 0 Or Val(txtPreviewClipID(0).Text) = 0 _
Or adoChapters.Recordset.RecordCount <= 0 Or Val(txtPosterClipID(0).Text) = 0 Or IsNull(datTheatricalReleaseDate(0).Value) _
Or cmbFilmMetadataLanguage(0).Text = "" Or cmbFilmTerritory(0).Text = "" Or txtOriginalSpokenLocale.Text = "" Or cmbPreviewLocale(0).Text = "" Or cmbFullLocale(0).Text = "" _
Or cmbPosterLocale(0).Text = "" Then
    MsgBox "Compulsory Information is has not yet been completed." & vbCrLf & "All red fields must be filled in," & vbCrLf _
    & "There must be at least one chapter and one Product Item.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

If cmbSubType(0).Text <> "Concert" Then
    If adoCast.Recordset.RecordCount <= 0 Or adoCrew.Recordset.RecordCount <= 0 Then
        MsgBox "Compulsory Information is has not yet been completed." & vbCrLf & "All red fields must be filled in," & vbCrLf _
        & "There must be at least one chapter and one Product Item.", vbCritical, "Cannot Make XML"
        Exit Sub
    End If
Else
    If adoCueSheet.Recordset.RecordCount <= 0 Or adoArtist1.Recordset.RecordCount <= 0 Then
        MsgBox "Compulsory Information is has not yet been completed." & vbCrLf & "All red fields must be filled in," & vbCrLf _
        & "There must be at least one Artist and one Cuesheet Item.", vbCritical, "Cannot Make XML"
        Exit Sub
    End If
End If

If cmbSubType(0).Text <> "concert" Then
    'check the cast info
    adoCast.Refresh
    adoCast.Recordset.MoveFirst
    Do While Not adoCast.Recordset.EOF
        If EmptyCheck(adoCast.Recordset("display_name")) = True Then
            MsgBox "Compulsory Cast Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Exit Sub
        End If
        adoCast.Recordset.MoveNext
    Loop

    'check the crew info
    adoCrew.Refresh
    adoCrew.Recordset.MoveFirst
    Do While Not adoCrew.Recordset.EOF
        If EmptyCheck(adoCrew.Recordset("display_name")) = True Or EmptyCheck(adoCrew.Recordset("role")) Then
            MsgBox "Compulsory Crew Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Exit Sub
        End If
        adoCrew.Recordset.MoveNext
    Loop
Else
    'check the Package Artist info
    adoArtist1.Refresh
    adoArtist1.Recordset.MoveFirst
    Do While Not adoArtist1.Recordset.EOF
        If EmptyCheck(adoArtist1.Recordset("name")) = True Then
            MsgBox "Compulsory Artist Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Exit Sub
        End If
        adoCast.Recordset.MoveNext
    Loop
    'check the cue sheet info
    adoCueSheet.Refresh
    adoCueSheet.Recordset.MoveFirst
    Do While Not adoCueSheet.Recordset.EOF
        If EmptyCheck(adoCueSheet.Recordset("sequence_number")) = True Then
            MsgBox "Compulsory Cue Sheet Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Exit Sub
        End If
        adoCast.Recordset.MoveNext
    Loop
    adoCueSheet.Recordset.MoveFirst
    'check the Package Artist info
    adoArtist2.Refresh
    adoArtist2.Recordset.MoveFirst
    Do While Not adoArtist2.Recordset.EOF
        If EmptyCheck(adoArtist2.Recordset("name")) = True Then
            MsgBox "Compulsory Cue Sheet Artist Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Exit Sub
        End If
        adoCast.Recordset.MoveNext
    Loop
End If

'check the chapter info
adoChapters.Refresh
adoChapters.Recordset.MoveFirst
Do While Not adoChapters.Recordset.EOF
    If EmptyCheck(adoChapters.Recordset("starttime")) = True Or (EmptyCheck(adoChapters.Recordset("title1")) = True And chkCustomChapterTitles.Value <> 0) Or Val(Trim(" " & adoChapters.Recordset("pictureclipid"))) = 0 Then
        MsgBox "Compulsory Chapter Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
        Exit Sub
    End If
    adoChapters.Recordset.MoveNext
Loop
adoChapters.Recordset.MoveFirst
If adoChapters.Recordset("starttime") <> "00:00:00" And adoChapters.Recordset("starttime") <> "00:00:00:00" And adoChapters.Recordset("starttime") <> "00:00:00;00" Then
    MsgBox "Compulsory Chapter Information is has not yet been completed." & vbCrLf & "First Chapter must start at zero.", vbCritical, "Cannot Make XML"
    Exit Sub
End If

'Check the Product Info
adoProduct.Refresh
adoProduct.Recordset.MoveFirst
Do While Not adoProduct.Recordset.EOF
    If EmptyCheck(adoProduct.Recordset("territory")) = True Or EmptyCheck(adoProduct.Recordset("clearedforsale")) = True Then
        MsgBox "Compulsory Product Information is has not yet been completed." & vbCrLf & "Product Information is not valid.", vbCritical, "Cannot Make XML"
        Exit Sub
    End If
    If adoProduct.Recordset("clearedforvod") = "true" Then
        If EmptyCheck(adoProduct.Recordset("vodtype")) = True Then
            MsgBox "Compulsory Product Information is has not yet been completed." & vbCrLf & "Product Informatino is not valid.", vbCritical, "Cannot Make XML"
            Exit Sub
        Else
            If adoProduct.Recordset("vodtype") = "New Release" Then
                If EmptyCheck(adoProduct.Recordset("availableforvoddate")) = True Or EmptyCheck(adoProduct.Recordset("physicalreleasedate")) = True Then
                    MsgBox "Compulsory Product Information is has not yet been completed." & vbCrLf & "Product Information is not valid.", vbCritical, "Cannot Make XML"
                    Exit Sub
                End If
            End If
        End If
    End If
    adoProduct.Recordset.MoveNext
Loop

l_lngTemp = GetData("events", "libraryID", "eventID", txtFullClipID.Text)
l_strPathToFullFile = GetData("library", "subtitle", "libraryID", l_lngTemp)
l_strAltFolder = GetData("events", "altlocation", "eventID", txtFullClipID.Text)
If l_strAltFolder <> "" Then
    l_strPathToFullFile = l_strPathToFullFile & "\" & l_strAltFolder
End If
MDIForm1.dlgMain.InitDir = l_strPathToFullFile
MDIForm1.dlgMain.Filter = "XML files|*.xml"
MDIForm1.dlgMain.Filename = "metadata.xml"

MDIForm1.dlgMain.ShowSave

l_strFileNameToSave = MDIForm1.dlgMain.Filename

If l_strFileNameToSave <> "" Then

    Open l_strFileNameToSave For Output As 1
    Print #1, "<?xml version=""1.0"" encoding=""UTF-8""?>"
    Print #1, "<package xmlns=""http://apple.com/itunes/importer"" version=""film5.2"">"
    If txtComments.Text <> "" Then Print #1, Chr(9) & "<comments>" & XMLSanitise(txtComments.Text) & "</comments>"
    Print #1, Chr(9) & "<provider>" & XMLSanitise(cmbiTunesProvider(0).Text) & "</provider>"
    Print #1, Chr(9) & "<language>" & cmbFilmMetadataLanguage(0).Text & "</language>"
    Print #1, Chr(9) & "<video>"
    
    'Basic Information
    Print #1, Chr(9) & Chr(9) & "<type>" & cmbiTunesType.Text & "</type>"
    Print #1, Chr(9) & Chr(9) & "<subtype>" & cmbSubType(0).Text & "</subtype>"
    Print #1, Chr(9) & Chr(9) & "<production_company>" & UTF8_Encode(XMLSanitise(txtProductionCompany.Text)) & "</production_company>"
    Print #1, Chr(9) & Chr(9) & "<vendor_id>" & XMLSanitise(txtiTunesVendorID.Text) & "</vendor_id>"
    If txtiTunesVendorOfferCode.Text <> "" Then Print #1, Chr(9) & Chr(9) & "<vendor_offer_code>" & XMLSanitise(txtiTunesVendorOfferCode.Text) & "</vendor_offer_code>"
    If txtiTunesISAN.Text <> "" Then Print #1, Chr(9) & Chr(9) & "<isan>" & txtiTunesISAN.Text & "</isan>"
    If txtUPC.Text <> "" Then Print #1, Chr(9) & Chr(9) & "<upc>" & txtUPC.Text & "</upc>"
    If txtAMGVideoID.Text <> "" Then Print #1, Chr(9) & Chr(9) & "<amg_video_id>" & txtAMGVideoID.Text & "</amg_video_id>"
    Print #1, Chr(9) & Chr(9) & "<country>" & cmbCountry.Columns("iso2acode").Text & "</country>"
    Print #1, Chr(9) & Chr(9) & "<original_spoken_locale>" & txtOriginalSpokenLocale.Text & "</original_spoken_locale>"
    Print #1, Chr(9) & Chr(9) & "<title>" & UTF8_Encode(XMLSanitise(txtFilmTitle(0).Text)) & "</title>"
    Print #1, Chr(9) & Chr(9) & "<studio_release_title>" & UTF8_Encode(XMLSanitise(txtStudioReleaseTitle.Text)) & "</studio_release_title>"
    Print #1, Chr(9) & Chr(9) & "<synopsis>" & UTF8_Encode(XMLSanitise(txtSynopsis(0).Text)) & "</synopsis>"
    Print #1, Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(0).Text)) & "</copyright_cline>"
    Print #1, Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(0).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
    If chkMultiLanguage.Value <> 0 Then
        Print #1, Chr(9) & Chr(9) & "<locales>"
        If cmbFilmMetadataLanguage(1).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(1).Text & """>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<title>" & UTF8_Encode(XMLSanitise(txtFilmTitle(1).Text)) & "</title>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<synopsis>" & UTF8_Encode(XMLSanitise(txtSynopsis(1).Text)) & "</synopsis>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</locale>"
        End If
        If cmbFilmMetadataLanguage(2).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(2).Text & """>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<title>" & UTF8_Encode(XMLSanitise(txtFilmTitle(2).Text)) & "</title>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<synopsis>" & UTF8_Encode(XMLSanitise(txtSynopsis(2).Text)) & "</synopsis>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</locale>"
        End If
        If cmbFilmMetadataLanguage(3).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(3).Text & """>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<title>" & UTF8_Encode(XMLSanitise(txtFilmTitle(3).Text)) & "</title>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<synopsis>" & UTF8_Encode(XMLSanitise(txtSynopsis(3).Text)) & "</synopsis>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</locale>"
        End If
        Print #1, Chr(9) & Chr(9) & "</locales>"
    End If
    If chkMultiLanguage.Value <> 0 Then
        Print #1, Chr(9) & Chr(9) & "<regions>"
        If cmbFilmTerritory(0).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<region>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbFilmTerritory(0).Text & "</territory>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(0).Text)) & "</copyright_cline>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(0).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</region>"
        End If
        If cmbFilmTerritory(1).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<region>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbFilmTerritory(1).Text & "</territory>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(1).Text)) & "</copyright_cline>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(1).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</region>"
        End If
        If cmbFilmTerritory(2).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<region>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbFilmTerritory(2).Text & "</territory>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(2).Text)) & "</copyright_cline>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(2).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</region>"
        End If
        If cmbFilmTerritory(3).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<region>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbFilmTerritory(3).Text & "</territory>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(3).Text)) & "</copyright_cline>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(3).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</region>"
        End If
        Print #1, Chr(9) & Chr(9) & "</regions>"
    Else
        Print #1, Chr(9) & Chr(9) & "<regions>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<region>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbFilmTerritory(0).Text & "</territory>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<copyright_cline>" & UTF8_Encode(XMLSanitise(txtFilmCopyrightLine(0).Text)) & "</copyright_cline>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<theatrical_release_date>" & Format(datTheatricalReleaseDate(0).Value, "YYYY-MM-DD") & "</theatrical_release_date>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</region>"
        Print #1, Chr(9) & Chr(9) & "</regions>"
    End If
    
    'Genre information
    Print #1, Chr(9) & Chr(9) & "<genres>"
    adoGenres.Recordset.MoveFirst
    Do While Not adoGenres.Recordset.EOF
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<genre code=""" & XMLSanitise(adoGenres.Recordset("genrecode")) & """/>"
        adoGenres.Recordset.MoveNext
    Loop
    Print #1, Chr(9) & Chr(9) & "</genres>"
    
    'Ratings information
    WriteiTunesFilmRatings
    
    'Cast and Crew
    If cmbSubType(0) <> "concert" Then
        If WriteCastandCrew = False Then
            MsgBox "Compulsory Cast and Crew Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Close #1
            Exit Sub
        End If
    Else
        If WriteArtistAndCuesheet = False Then
            MsgBox "Compulsory Artist and Cuesheet Information is has not yet been completed." & vbCrLf, vbCritical, "Cannot Make XML"
            Close #1
            Exit Sub
        End If
    End If
    
    'Chapters
    WriteFilmChapterInfo
    
    'Accessibility Information (Closed Captions)
    l_blnUSProduct = False
    If adoProduct.Recordset.RecordCount > 0 Then
        adoProduct.Recordset.MoveFirst
        Do While Not adoProduct.Recordset.EOF
            If adoProduct.Recordset("territory") = "US" Then
                l_blnUSProduct = True
                Exit Do
            End If
            adoProduct.Recordset.MoveNext
        Loop
    End If
    If l_blnUSProduct = True Or chkCaptionsAvailability.Value <> 0 Then
        Print #1, Chr(9) & Chr(9) & "<accessibility_info>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<accessibility role=""captions"" ";
        If chkCaptionsAvailability.Value <> 0 Then
            Print #1, "available=""true""/>"
        Else
            Print #1, "available=""false"" reason_code=""" & cmbCaptionsReason.Text & """/>"
        End If
        Print #1, Chr(9) & Chr(9) & "</accessibility_info>"
    End If
    
    'Asset Information
    Print #1, Chr(9) & Chr(9) & "<assets>"
    
    'Main Asset
    If WriteFilmMainAsset = False Then Exit Sub
    
    'Preview Assets
    If WriteFilmPreviewAssets = False Then Exit Sub
    
    'Artwork
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<asset type=""artwork"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territories>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>WW</territory>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</territories>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<data_file>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locales>"
    If cmbPosterLocale(0).Text <> "" And cmbPosterLocale(4).Text <> "" Then
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(0).Text & """/>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(4).Text & """/>"
    Else
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(0).Text & """/>"
    End If
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locales>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", txtPosterClipID(0).Text)) & "</file_name>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<size>" & GetData("events", "bigfilesize", "eventID", txtPosterClipID(0).Text) & "</size>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", txtPosterClipID(0).Text) & "</checksum>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</data_file>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</asset>"
    If Val(txtPosterClipID(1).Text) <> 0 Then
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<asset type=""artwork"">"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbPosterTerritory(1).Text & "</territory>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<data_file>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locales>"
        If cmbPosterLocale(1).Text <> "" And cmbPosterLocale(5).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(1).Text & """/>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(5).Text & """/>"
        ElseIf cmbPosterLocale(1).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(1).Text & """/>"
        Else
            MsgBox "Compulsory Locale Information is has not yet been completed on Artwork Poster 2." & vbCrLf, vbCritical, "Cannot Make XML"
            Close #1
            Exit Sub
        End If
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locales>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", txtPosterClipID(1).Text)) & "</file_name>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<size>" & GetData("events", "bigfilesize", "eventID", txtPosterClipID(1).Text) & "</size>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", txtPosterClipID(1).Text) & "</checksum>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</data_file>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</asset>"
    End If
    If Val(txtPosterClipID(2).Text) <> 0 Then
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<asset type=""artwork"">"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbPosterTerritory(2).Text & "</territory>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<data_file>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locales>"
        If cmbPosterLocale(2).Text <> "" And cmbPosterLocale(6).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(2).Text & """/>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(6).Text & """/>"
        ElseIf cmbPosterLocale(2).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(2).Text & """/>"
        Else
            MsgBox "Compulsory Locale Information is has not yet been completed on Artwork Poster 3." & vbCrLf, vbCritical, "Cannot Make XML"
            Close #1
            Exit Sub
        End If
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locales>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", txtPosterClipID(2).Text)) & "</file_name>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<size>" & GetData("events", "bigfilesize", "eventID", txtPosterClipID(2).Text) & "</size>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", txtPosterClipID(2).Text) & "</checksum>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</data_file>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</asset>"
    End If
    If Val(txtPosterClipID(3).Text) <> 0 Then
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<asset type=""artwork"">"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & cmbPosterTerritory(3).Text & "</territory>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</territories>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<data_file>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locales>"
        If cmbPosterLocale(3).Text <> "" And cmbPosterLocale(7).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(3).Text & """/>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(7).Text & """/>"
        ElseIf cmbPosterLocale(3).Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbPosterLocale(3).Text & """/>"
        Else
            MsgBox "Compulsory Locale Information is has not yet been completed on Artwork Poster 2." & vbCrLf, vbCritical, "Cannot Make XML"
            Close #1
            Exit Sub
        End If
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locales>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", txtPosterClipID(3).Text)) & "</file_name>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<size>" & GetData("events", "bigfilesize", "eventID", txtPosterClipID(3).Text) & "</size>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", txtPosterClipID(3).Text) & "</checksum>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</data_file>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</asset>"
    End If
    Print #1, Chr(9) & Chr(9) & "</assets>"
    
    'Products Information
    adoProduct.Recordset.MoveFirst
    Print #1, Chr(9) & Chr(9) & "<products>"
    Do While Not adoProduct.Recordset.EOF
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<product>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<territory>" & adoProduct.Recordset("territory") & "</territory>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<cleared_for_sale>" & adoProduct.Recordset("clearedforsale") & "</cleared_for_sale>"
        If EmptyCheck(adoProduct.Recordset("clearedforhdsale")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<cleared_for_hd_sale>" & adoProduct.Recordset("clearedforhdsale") & "</cleared_for_hd_sale>"
        End If
        If EmptyCheck(adoProduct.Recordset("wholesalepricetier")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<wholesale_price_tier>" & adoProduct.Recordset("wholesalepricetier") & "</wholesale_price_tier>"
        End If
        If EmptyCheck(adoProduct.Recordset("hdwholesalepricetier")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<hd_wholesale_price_tier>" & adoProduct.Recordset("hdwholesalepricetier") & "</hd_wholesale_price_tier>"
        End If
        If EmptyCheck(adoProduct.Recordset("preordersalesstartdate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<preorder_sales_start_date>" & Format(adoProduct.Recordset("preordersalesstartdate"), "yyyy-mm-dd") & "</preorder_sales_start_date>"
        End If
        If EmptyCheck(adoProduct.Recordset("salesstartdate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<sales_start_date>" & Format(adoProduct.Recordset("salesstartdate"), "yyyy-mm-dd") & "</sales_start_date>"
        End If
        If EmptyCheck(adoProduct.Recordset("salesenddate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<sales_end_date>" & Format(adoProduct.Recordset("salesenddate"), "yyyy-mm-dd") & "</sales_end_date>"
        End If
        If EmptyCheck(adoProduct.Recordset("clearedforvod")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<cleared_for_vod>" & adoProduct.Recordset("clearedforvod") & "</cleared_for_vod>"
        End If
        If EmptyCheck(adoProduct.Recordset("clearedforhdvod")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<cleared_for_hd_vod>" & adoProduct.Recordset("clearedforhdvod") & "</cleared_for_hd_vod>"
        End If
        If EmptyCheck(adoProduct.Recordset("vodtype")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<vod_type>" & adoProduct.Recordset("vodtype") & "</vod_type>"
        End If
        If EmptyCheck(adoProduct.Recordset("availableforvoddate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<available_for_vod_date>" & Format(adoProduct.Recordset("availableforvoddate"), "yyyy-mm-dd") & "</available_for_vod_date>"
        End If
        If EmptyCheck(adoProduct.Recordset("unavailableforvoddate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<unavailable_for_vod_date>" & Format(adoProduct.Recordset("unavailableforvoddate"), "yyyy-mm-dd") & "</unavailable_for_vod_date>"
        End If
        If EmptyCheck(adoProduct.Recordset("physicalreleasedate")) = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<physical_release_date>" & Format(adoProduct.Recordset("physicalreleasedate"), "yyyy-mm-dd") & "</physical_release_date>"
        End If
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</product>"
        adoProduct.Recordset.MoveNext
    Loop
    Print #1, Chr(9) & Chr(9) & "</products>"
    
    Print #1, Chr(9) & "</video>"
    Print #1, "</package>"
    
    Close 1
    MsgBox "Done"
End If

End Sub

Private Sub cmdReadExistingiTunesTVXML_Click()

Dim l_xmlDOC As Object
Dim l_strSQL As String, l_lngPackageID As Long

Set l_xmlDOC = CreateObject("MSXML2.DOMDocument")

l_xmlDOC.async = False
MDIForm1.dlgMain.InitDir = g_strExcelOrderLocation
MDIForm1.dlgMain.Filter = "All Files|*.*"
MDIForm1.dlgMain.ShowOpen

If MDIForm1.dlgMain.Filename = "" Then
    Beep
    Exit Sub
End If

If l_xmlDOC.Load(MDIForm1.dlgMain.Filename) Then
    ParseThroughNodes l_xmlDOC.childNodes
    l_strSQL = "INSERT INTO iTunes_package (companyID, video_type, fullcroptop, fullcropbottom, fullcropleft, fullcropright) VALUES ("
    If Val(GetDataSQL("SELECT TOP 1 information FROM xref WHERE category = 'iTunesProvider' AND description = '" & m_iTunes.Provider & "'")) <> 0 Then
        l_strSQL = l_strSQL & Val(GetDataSQL("SELECT TOP 1 information FROM xref WHERE category = 'iTunesProvider' AND description = '" & m_iTunes.Provider & "'")) & ", "
    Else
        l_strSQL = l_strSQL & "1163, "
    End If
    l_strSQL = l_strSQL & "'tv', 4, 4, 4, 4);"
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    l_lngPackageID = g_lngLastID
    'Update String comprising
    SetData "iTunes_Package", "provider", "iTunes_packageID", l_lngPackageID, m_iTunes.Provider
    SetData "iTunes_Package", "video_subtype", "iTunes_PackageID", l_lngPackageID, m_iTunes.Subtype
    SetData "iTunes_Package", "video_language", "iTunes_packageID", l_lngPackageID, m_iTunes.Language
    SetData "iTunes_Package", "metadata_language", "iTunes_packageID", l_lngPackageID, m_iTunes.Language
    SetData "iTunes_Package", "video_containerID", "iTunes_packageID", l_lngPackageID, m_iTunes.ContainerID
    SetData "iTunes_Package", "video_containerposition", "iTunes_packageID", l_lngPackageID, m_iTunes.ContainerPosition
    SetData "iTunes_Package", "video_vendorID", "iTunes_packageID", l_lngPackageID, m_iTunes.VendorID
    SetData "iTunes_Package", "video_ep_prod_no", "iTunes_packageID", l_lngPackageID, m_iTunes.EpisodeProductionNumber
    SetData "iTunes_Package", "originalspokenlocale", "iTunes_packageID", l_lngPackageID, m_iTunes.OriginalLocale
    SetData "iTunes_Package", "video_title", "iTunes_packageID", l_lngPackageID, QuoteSanitise(m_iTunes.Title)
    SetData "iTunes_Package", "video_title_utf8", "iTunes_packageID", l_lngPackageID, UTF8_Encode(QuoteSanitise(m_iTunes.Title))
    SetData "iTunes_Package", "studio_release_title", "iTunes_packageID", l_lngPackageID, QuoteSanitise(m_iTunes.Studio_Release_Title)
    SetData "iTunes_Package", "studio_release_title_utf8", "iTunes_packageID", l_lngPackageID, UTF8_Encode(QuoteSanitise(m_iTunes.Studio_Release_Title))
    SetData "iTunes_Package", "video_longdescription", "iTunes_packageID", l_lngPackageID, QuoteSanitise(m_iTunes.Synopsis)
    SetData "iTunes_Package", "video_longdescription_utf8", "iTunes_packageID", l_lngPackageID, UTF8_Encode(QuoteSanitise(m_iTunes.Synopsis))
    SetData "iTunes_Package", "video_releasedate", "iTunes_packageID", l_lngPackageID, Format(m_iTunes.ReleaseDate, "YYYY-MM-DD")
    SetData "iTunes_Package", "video_copyright_cline", "iTunes_packageID", l_lngPackageID, QuoteSanitise(m_iTunes.Copyright)
    SetData "iTunes_Package", "video_copyright_cline_utf8", "iTunes_packageID", l_lngPackageID, UTF8_Encode(QuoteSanitise(m_iTunes.Copyright))
    SetData "iTunes_Package", "video_previewstarttime", "iTunes_packageID", l_lngPackageID, IIf(Val(m_iTunes.PreviewStartTime) <> 0, Val(m_iTunes.PreviewStartTime), 360)
    Select Case m_iTunes.Territory
        Case "FR"
            If m_iTunes.Rating = "TP" Then m_iTunes.Rating = "Tout Public"
            SetData "iTunes_Package", "rating_fr", "iTunes_packageID", l_lngPackageID, m_iTunes.Rating
        Case "DE"
            SetData "iTunes_Package", "rating_de", "iTunes_packageID", l_lngPackageID, m_iTunes.Rating
        Case "GB"
            SetData "iTunes_Package", "rating_uk", "iTunes_packageID", l_lngPackageID, m_iTunes.Rating
        Case Else
            MsgBox "Unhandled Territory - please get CETA updated. Row will be wrong"
    End Select
    l_strSQL = "INSERT INTO iTunes_Product (iTunes_packageID, territory, salesstartdate, clearedforsale) VALUES ("
    l_strSQL = l_strSQL & l_lngPackageID & ", "
    l_strSQL = l_strSQL & "'" & m_iTunes.Territory & "', "
    l_strSQL = l_strSQL & "'" & Format(m_iTunes.SalesStartDate, "YYYY-MM-DD") & "', "
    l_strSQL = l_strSQL & "'" & m_iTunes.ClearedForSale & "');"
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
Else
    MsgBox "not loaded"
End If

Set l_xmlDOC = Nothing
MsgBox "Done"

End Sub

Private Sub ParseThroughNodes(ByRef Nodes As MSXML2.IXMLDOMNodeList)

Dim xNode As MSXML2.IXMLDOMNode

For Each xNode In Nodes
    If xNode.nodeType = NODE_TEXT Then
        Select Case xNode.parentNode.nodeName
            Case "provider"
                m_iTunes.Provider = xNode.nodeValue
            Case "subtype"
                m_iTunes.Subtype = xNode.nodeValue
            Case "language"
                m_iTunes.Language = xNode.nodeValue
            Case "container_id"
                m_iTunes.ContainerID = xNode.nodeValue
            Case "container_position"
                m_iTunes.ContainerPosition = xNode.nodeValue
            Case "vendor_id"
                m_iTunes.VendorID = xNode.nodeValue
            Case "episode_production_number"
                m_iTunes.EpisodeProductionNumber = xNode.nodeValue
            Case "original_spoken_locale"
                m_iTunes.OriginalLocale = xNode.nodeValue
            Case "title"
                m_iTunes.Title = xNode.nodeValue
            Case "studio_release_title"
                m_iTunes.Studio_Release_Title = xNode.nodeValue
            Case "description"
                m_iTunes.Synopsis = xNode.nodeValue
            Case "release_date"
                m_iTunes.ReleaseDate = xNode.nodeValue
            Case "copyright_cline"
                m_iTunes.Copyright = xNode.nodeValue
            Case "rating"
                m_iTunes.Rating = xNode.nodeValue
            Case "territory"
                m_iTunes.Territory = xNode.nodeValue
            Case "sales_start_date"
                m_iTunes.SalesStartDate = xNode.nodeValue
            Case "cleared_for_sale"
                m_iTunes.ClearedForSale = xNode.nodeValue
            Case "preview"
                m_iTunes.PreviewStartTime = xNode.nodeValue
        End Select
    End If
    
    If xNode.hasChildNodes Then
        ParseThroughNodes xNode.childNodes
    End If
Next xNode

End Sub

Private Sub cmdSave_Click()

Dim l_strSQL As String

If lblCompanyID.Caption = "" Then
    MsgBox "Cannot Save iTunes Packages unless a Company has been chosen.", vbCritical, "Error while Saving Package..."
    Exit Sub
End If

If Val(lblPackageID.Caption) = 0 Then
    l_strSQL = "INSERT INTO itunes_package (companyID) VALUES (" & Val(lblCompanyID.Caption) & ");"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    lblPackageID.Caption = g_lngLastID
End If

'This next block all relates to all iTunes content
l_strSQL = "UPDATE itunes_package SET "
l_strSQL = l_strSQL & "companyID = " & lblCompanyID.Caption & ", "
l_strSQL = l_strSQL & "video_type = '" & QuoteSanitise(cmbiTunesType.Text) & "', "
l_strSQL = l_strSQL & "video_vendorID = '" & QuoteSanitise(txtiTunesVendorID.Text) & "', "
l_strSQL = l_strSQL & "vendor_offer_code = '" & txtiTunesVendorOfferCode.Text & "', "
l_strSQL = l_strSQL & "video_ISAN = '" & txtiTunesISAN.Text & "', "
l_strSQL = l_strSQL & "originalspokenlocale = '" & txtOriginalSpokenLocale.Text & "', "
l_strSQL = l_strSQL & "captionsavailable = " & chkCaptionsAvailability.Value & ", "
If cmbCaptionsReason.Text <> "" Then
    l_strSQL = l_strSQL & "captionsreason = '" & cmbCaptionsReason.Text & "', "
Else
    l_strSQL = l_strSQL & "captionsreason = NULL, "
End If

Select Case tabType.Tab

    Case 0 '"tv"
        l_strSQL = l_strSQL & "provider = '" & QuoteSanitise(cmbiTunesProvider(1).Text) & "', "
        l_strSQL = l_strSQL & "video_subtype = '" & QuoteSanitise(cmbSubType(1).Text) & "', "
        l_strSQL = l_strSQL & "packageused = " & chkPackageUsed(0).Value & ", "
        l_strSQL = l_strSQL & "metadata_language = '" & txtMetaDataLanguage.Text & "', "
        l_strSQL = l_strSQL & "video_copyright_cline = '" & QuoteSanitise(txtiTunesCopyrightLine.Text) & "', "
        l_strSQL = l_strSQL & "video_copyright_cline_utf8 = '" & UTF8_Encode(QuoteSanitise(txtiTunesCopyrightLine.Text)) & "', "
        l_strSQL = l_strSQL & "video_title = '" & QuoteSanitise(txtTitle.Text) & "', "
        l_strSQL = l_strSQL & "studio_release_title = '" & QuoteSanitise(txtStudio_Release_Title.Text) & "', "
        l_strSQL = l_strSQL & "video_title_utf8 = '" & UTF8_Encode(QuoteSanitise(txtTitle.Text)) & "', "
        l_strSQL = l_strSQL & "studio_release_title_utf8 = '" & UTF8_Encode(QuoteSanitise(txtStudio_Release_Title.Text)) & "', "
        l_strSQL = l_strSQL & "video_ep_prod_no = '" & QuoteSanitise(txtiTunesEpProdNo.Text) & "', "
        l_strSQL = l_strSQL & "video_containerID = '" & QuoteSanitise(txtiTunesContainerID.Text) & "', "
        l_strSQL = l_strSQL & "video_containerposition = '" & QuoteSanitise(txtiTunesContainerPosition.Text) & "', "
        If Not IsNull(datiTunesReleaseDate.Value) Then
            l_strSQL = l_strSQL & "video_releasedate = '" & FormatSQLDate(datiTunesReleaseDate.Value) & "', "
        Else
            l_strSQL = l_strSQL & "video_releasedate = NULL, "
        End If
        l_strSQL = l_strSQL & "video_longdescription = '" & QuoteSanitise(txtiTunesLongDescription.Text) & "', "
        l_strSQL = l_strSQL & "video_longdescription_utf8 = '" & UTF8_Encode(QuoteSanitise(txtiTunesLongDescription.Text)) & "', "
        l_strSQL = l_strSQL & "itunestechcomment = '" & QuoteSanitise(txtiTunesTechComments.Text) & "', "
        l_strSQL = l_strSQL & "itunestechcomment_utf8 = '" & UTF8_Encode(QuoteSanitise(txtiTunesTechComments.Text)) & "', "
        l_strSQL = l_strSQL & "video_language = '" & txtVideoLanguage.Text & "', "
        l_strSQL = l_strSQL & "video_previewstarttime = " & Val(txtiTunesPreviewStartTime.Text) & ", "
        l_strSQL = l_strSQL & "rating_au = '" & cmbAU.Text & "', "
        l_strSQL = l_strSQL & "rating_ca = '" & cmbCA.Text & "', "
        l_strSQL = l_strSQL & "rating_de = '" & cmbDE.Text & "', "
        l_strSQL = l_strSQL & "rating_fr = '" & cmbFR.Text & "', "
        l_strSQL = l_strSQL & "rating_uk = '" & cmbUK.Text & "', "
        l_strSQL = l_strSQL & "rating_us = '" & cmbUS.Text & "', "
        l_strSQL = l_strSQL & "rating_jp = '" & cmbJP.Text & "', "
        l_strSQL = l_strSQL & "fullcroptop = '" & txtCropTop.Text & "', "
        l_strSQL = l_strSQL & "fullcropbottom = '" & txtCropBottom.Text & "', "
        l_strSQL = l_strSQL & "fullcropleft = '" & txtCropLeft.Text & "', "
        l_strSQL = l_strSQL & "fullcropright = '" & txtCropRight.Text & "' "

    Case 1 '"film"
        
        l_strSQL = l_strSQL & "provider = '" & QuoteSanitise(cmbiTunesProvider(0).Text) & "', "
        l_strSQL = l_strSQL & "packageused = " & chkPackageUsed(2).Value & ", "
        If chkTimecode(0).Value = True Then
            l_strSQL = l_strSQL & "chaptertimecodeformat = 8, "
        ElseIf chkTimecode(1).Value = True Then
            l_strSQL = l_strSQL & "chaptertimecodeformat = 1, "
        ElseIf chkTimecode(2).Value = True Then
            l_strSQL = l_strSQL & "chaptertimecodeformat = 3, "
        ElseIf chkTimecode(3).Value = True Then
            l_strSQL = l_strSQL & "chaptertimecodeformat = 2, "
        ElseIf chkTimecode(4).Value = True Then
            l_strSQL = l_strSQL & "chaptertimecodeformat = 9, "
        Else
            l_strSQL = l_strSQL & "chaptertimecodeformat = 0, "
        End If
        l_strSQL = l_strSQL & "iTunesComments = '" & QuoteSanitise(txtComments.Text) & "', "
        l_strSQL = l_strSQL & "video_subtype = '" & cmbSubType(0).Text & "', "
        l_strSQL = l_strSQL & "film_multilanguage = " & chkMultiLanguage.Value & ", "
        l_strSQL = l_strSQL & "CustomChapterTitles = " & chkCustomChapterTitles.Value & ", "
        l_strSQL = l_strSQL & "film_locale1 = '" & cmbFilmMetadataLanguage(0).Text & "', "
        l_strSQL = l_strSQL & "film_territory1 = '" & cmbFilmTerritory(0).Text & "', "
        l_strSQL = l_strSQL & "StudioReleaseTitle = '" & QuoteSanitise(txtStudioReleaseTitle.Text) & "', "
        l_strSQL = l_strSQL & "StudioReleaseTitle_utf8 = '" & UTF8_Encode(QuoteSanitise(txtStudioReleaseTitle.Text)) & "', "
        l_strSQL = l_strSQL & "film_title1 = '" & QuoteSanitise(txtFilmTitle(0).Text) & "', "
        l_strSQL = l_strSQL & "film_title1_utf8 = '" & UTF8_Encode(QuoteSanitise(txtFilmTitle(0).Text)) & "', "
        l_strSQL = l_strSQL & "video_title = '" & QuoteSanitise(txtFilmTitle(0).Text) & "', "
        l_strSQL = l_strSQL & "video_title_utf8 = '" & UTF8_Encode(QuoteSanitise(txtFilmTitle(0).Text)) & "', "
        l_strSQL = l_strSQL & "film_synopsis1 = '" & QuoteSanitise(txtSynopsis(0).Text) & "', "
        l_strSQL = l_strSQL & "film_synopsis1_utf8 = '" & UTF8_Encode(QuoteSanitise(txtSynopsis(0).Text)) & "', "
        l_strSQL = l_strSQL & "film_copyright_cline1 = '" & QuoteSanitise(txtFilmCopyrightLine(0).Text) & "', "
        l_strSQL = l_strSQL & "film_copyright_cline1_utf8 = '" & UTF8_Encode(QuoteSanitise(txtFilmCopyrightLine(0).Text)) & "', "
        If Not IsNull(datTheatricalReleaseDate(0).Value) Then
            l_strSQL = l_strSQL & "film_releasedate1 = '" & FormatSQLDate(datTheatricalReleaseDate(0).Value) & "', "
        Else
            l_strSQL = l_strSQL & "film_releasedate1 = NULL, "
        End If
        l_strSQL = l_strSQL & "film_locale2 = '" & cmbFilmMetadataLanguage(1).Text & "', "
        l_strSQL = l_strSQL & "film_territory2 = '" & cmbFilmTerritory(1).Text & "', "
        l_strSQL = l_strSQL & "film_title2 = '" & QuoteSanitise(txtFilmTitle(1).Text) & "', "
        l_strSQL = l_strSQL & "film_title2_utf8 = '" & UTF8_Encode(QuoteSanitise(txtFilmTitle(1).Text)) & "', "
        l_strSQL = l_strSQL & "film_synopsis2 = '" & QuoteSanitise(txtSynopsis(1).Text) & "', "
        l_strSQL = l_strSQL & "film_synopsis2_utf8 = '" & UTF8_Encode(QuoteSanitise(txtSynopsis(1).Text)) & "', "
        l_strSQL = l_strSQL & "film_copyright_cline2 = '" & QuoteSanitise(txtFilmCopyrightLine(1).Text) & "', "
        l_strSQL = l_strSQL & "film_copyright_cline2_utf8 = '" & UTF8_Encode(QuoteSanitise(txtFilmCopyrightLine(1).Text)) & "', "
        If Not IsNull(datTheatricalReleaseDate(1).Value) Then
            l_strSQL = l_strSQL & "film_releasedate2 = '" & FormatSQLDate(datTheatricalReleaseDate(1).Value) & "', "
        Else
            l_strSQL = l_strSQL & "film_releasedate2 = NULL, "
        End If
        l_strSQL = l_strSQL & "film_locale3 = '" & cmbFilmMetadataLanguage(2).Text & "', "
        l_strSQL = l_strSQL & "film_territory3 = '" & cmbFilmTerritory(2).Text & "', "
        l_strSQL = l_strSQL & "film_title3 = '" & QuoteSanitise(txtFilmTitle(2).Text) & "', "
        l_strSQL = l_strSQL & "film_title3_utf8 = '" & UTF8_Encode(QuoteSanitise(txtFilmTitle(2).Text)) & "', "
        l_strSQL = l_strSQL & "film_synopsis3 = '" & QuoteSanitise(txtSynopsis(2).Text) & "', "
        l_strSQL = l_strSQL & "film_synopsis3_utf8 = '" & UTF8_Encode(QuoteSanitise(txtSynopsis(2).Text)) & "', "
        l_strSQL = l_strSQL & "film_copyright_cline3 = '" & QuoteSanitise(txtFilmCopyrightLine(2).Text) & "', "
        l_strSQL = l_strSQL & "film_copyright_cline3_utf8 = '" & UTF8_Encode(QuoteSanitise(txtFilmCopyrightLine(2).Text)) & "', "
        If Not IsNull(datTheatricalReleaseDate(2).Value) Then
            l_strSQL = l_strSQL & "film_releasedate3 = '" & FormatSQLDate(datTheatricalReleaseDate(2).Value) & "', "
        Else
            l_strSQL = l_strSQL & "film_releasedate3 = NULL, "
        End If
        l_strSQL = l_strSQL & "film_locale4 = '" & cmbFilmMetadataLanguage(3).Text & "', "
        l_strSQL = l_strSQL & "film_territory4 = '" & cmbFilmTerritory(3).Text & "', "
        l_strSQL = l_strSQL & "film_title4 = '" & QuoteSanitise(txtFilmTitle(3).Text) & "', "
        l_strSQL = l_strSQL & "film_title4_utf8 = '" & UTF8_Encode(QuoteSanitise(txtFilmTitle(3).Text)) & "', "
        l_strSQL = l_strSQL & "film_synopsis4 = '" & QuoteSanitise(txtSynopsis(3).Text) & "', "
        l_strSQL = l_strSQL & "film_synopsis4_utf8 = '" & UTF8_Encode(QuoteSanitise(txtSynopsis(3).Text)) & "', "
        l_strSQL = l_strSQL & "film_copyright_cline4 = '" & QuoteSanitise(txtFilmCopyrightLine(3).Text) & "', "
        l_strSQL = l_strSQL & "film_copyright_cline4_utf8 = '" & UTF8_Encode(QuoteSanitise(txtFilmCopyrightLine(3).Text)) & "', "
        If Not IsNull(datTheatricalReleaseDate(3).Value) Then
            l_strSQL = l_strSQL & "film_releasedate4 = '" & FormatSQLDate(datTheatricalReleaseDate(3).Value) & "', "
        Else
            l_strSQL = l_strSQL & "film_releasedate4 = NULL, "
        End If
        If cmbCountry.Text <> "" Then
            l_strSQL = l_strSQL & "country = '" & cmbCountry.Columns("iso2acode").Text & "', "
        End If
        l_strSQL = l_strSQL & "fullclipID = '" & txtFullClipID.Text & "', "
        l_strSQL = l_strSQL & "mainassetlocale = '" & cmbFullLocale(0).Text & "', "
        l_strSQL = l_strSQL & "captionsclipID = '" & txtCaptionsClipID.Text & "', "
        l_strSQL = l_strSQL & "captionslocale = '" & cmbFullLocale(1).Text & "', "
        l_strSQL = l_strSQL & "fullcroptop = '" & txtFullCropTop.Text & "', "
        l_strSQL = l_strSQL & "fullcropbottom = '" & txtFullCropBottom.Text & "', "
        l_strSQL = l_strSQL & "fullcropleft = '" & txtFullCropLeft.Text & "', "
        l_strSQL = l_strSQL & "fullcropright = '" & txtFullCropRight.Text & "', "
        l_strSQL = l_strSQL & "webscreenerclipID = '" & txtWebScreenerClipID.Text & "', "
        l_strSQL = l_strSQL & "previewclipID = '" & txtPreviewClipID(0).Text & "', "
        l_strSQL = l_strSQL & "previewlocale = '" & cmbPreviewLocale(0).Text & "', "
        l_strSQL = l_strSQL & "previewclipID2 = '" & txtPreviewClipID(1).Text & "', "
        l_strSQL = l_strSQL & "previewterritory2 = '" & cmbPreviewTerritory(1).Text & "', "
        l_strSQL = l_strSQL & "previewlocale2 = '" & cmbPreviewLocale(1).Text & "', "
        l_strSQL = l_strSQL & "previewclipID3 = '" & txtPreviewClipID(2).Text & "', "
        l_strSQL = l_strSQL & "previewterritory3 = '" & cmbPreviewTerritory(2).Text & "', "
        l_strSQL = l_strSQL & "previewlocale3 = '" & cmbPreviewLocale(2).Text & "', "
        l_strSQL = l_strSQL & "previewclipID4 = '" & txtPreviewClipID(3).Text & "', "
        l_strSQL = l_strSQL & "previewterritory4 = '" & cmbPreviewTerritory(3).Text & "', "
        l_strSQL = l_strSQL & "previewlocale4 = '" & cmbPreviewLocale(3).Text & "', "
        l_strSQL = l_strSQL & "previewcroptop = '" & txtPreviewCropTop.Text & "', "
        l_strSQL = l_strSQL & "previewcropbottom = '" & txtPreviewCropBottom.Text & "', "
        l_strSQL = l_strSQL & "previewcropleft = '" & txtPreviewCropLeft.Text & "', "
        l_strSQL = l_strSQL & "previewcropright = '" & txtPreviewCropRight.Text & "', "
        l_strSQL = l_strSQL & "posterclipID = '" & txtPosterClipID(0).Text & "', "
        l_strSQL = l_strSQL & "posterclipID2 = '" & txtPosterClipID(1).Text & "', "
        l_strSQL = l_strSQL & "posterterritory2 = '" & cmbPosterTerritory(1).Text & "', "
        l_strSQL = l_strSQL & "posterclipID3 = '" & txtPosterClipID(2).Text & "', "
        l_strSQL = l_strSQL & "posterterritory3 = '" & cmbPosterTerritory(2).Text & "', "
        l_strSQL = l_strSQL & "posterclipID4 = '" & txtPosterClipID(3).Text & "', "
        l_strSQL = l_strSQL & "posterterritory4 = '" & cmbPosterTerritory(3).Text & "', "
        l_strSQL = l_strSQL & "subtitlesclipID = '" & txtSubtitlesClipID(0).Text & "', "
        l_strSQL = l_strSQL & "subtitleslocale = '" & cmbSubtitlesLocale(0).Text & "', "
        l_strSQL = l_strSQL & "subtitlesclipID2 = '" & txtSubtitlesClipID(1).Text & "', "
        l_strSQL = l_strSQL & "subtitleslocale2 = '" & cmbSubtitlesLocale(1).Text & "', "
        l_strSQL = l_strSQL & "subtitlesclipID3 = '" & txtSubtitlesClipID(2).Text & "', "
        l_strSQL = l_strSQL & "subtitleslocale3 = '" & cmbSubtitlesLocale(2).Text & "', "
        l_strSQL = l_strSQL & "subtitlesclipID4 = '" & txtSubtitlesClipID(3).Text & "', "
        l_strSQL = l_strSQL & "subtitleslocale4 = '" & cmbSubtitlesLocale(3).Text & "', "
        l_strSQL = l_strSQL & "subtitlesclipID5 = '" & txtSubtitlesClipID(8).Text & "', "
        l_strSQL = l_strSQL & "subtitleslocale5 = '" & cmbSubtitlesLocale(7).Text & "', "
        l_strSQL = l_strSQL & "subtitlesclipID6 = '" & txtSubtitlesClipID(9).Text & "', "
        l_strSQL = l_strSQL & "subtitleslocale6 = '" & cmbSubtitlesLocale(8).Text & "', "
        l_strSQL = l_strSQL & "subtitlesclipID7 = '" & txtSubtitlesClipID(10).Text & "', "
        l_strSQL = l_strSQL & "subtitleslocale7 = '" & cmbSubtitlesLocale(9).Text & "', "
        l_strSQL = l_strSQL & "subtitlesclipID8 = '" & txtSubtitlesClipID(11).Text & "', "
        l_strSQL = l_strSQL & "subtitleslocale8 = '" & cmbSubtitlesLocale(10).Text & "', "
        l_strSQL = l_strSQL & "subtitlesclipID9 = '" & txtSubtitlesClipID(12).Text & "', "
        l_strSQL = l_strSQL & "subtitleslocale9 = '" & cmbSubtitlesLocale(11).Text & "', "
        l_strSQL = l_strSQL & "subtitlesclipID10 = '" & txtSubtitlesClipID(13).Text & "', "
        l_strSQL = l_strSQL & "subtitleslocale10 = '" & cmbSubtitlesLocale(12).Text & "', "
        l_strSQL = l_strSQL & "subtitlesclipID11 = '" & txtSubtitlesClipID(14).Text & "', "
        l_strSQL = l_strSQL & "subtitleslocale11 = '" & cmbSubtitlesLocale(13).Text & "', "
        l_strSQL = l_strSQL & "subtitlesclipID12 = '" & txtSubtitlesClipID(15).Text & "', "
        l_strSQL = l_strSQL & "subtitleslocale12 = '" & cmbSubtitlesLocale(14).Text & "', "
        l_strSQL = l_strSQL & "AudioClipID1 = '" & txtAudioClipID(0).Text & "', "
        l_strSQL = l_strSQL & "Audiolocale1 = '" & cmbAudioLocale(0).Text & "', "
        l_strSQL = l_strSQL & "AudioClipID2 = '" & txtAudioClipID(1).Text & "', "
        l_strSQL = l_strSQL & "Audiolocale2 = '" & cmbAudioLocale(1).Text & "', "
        l_strSQL = l_strSQL & "AudioClipID3 = '" & txtAudioClipID(2).Text & "', "
        l_strSQL = l_strSQL & "Audiolocale3 = '" & cmbAudioLocale(2).Text & "', "
        l_strSQL = l_strSQL & "AudioDisClipID1 = '" & txtAudioClipID(3).Text & "', "
        l_strSQL = l_strSQL & "AudioDislocale1 = '" & cmbAudioLocale(3).Text & "', "
        l_strSQL = l_strSQL & "AudioDisClipID2 = '" & txtAudioClipID(4).Text & "', "
        l_strSQL = l_strSQL & "AudioDislocale2 = '" & cmbAudioLocale(4).Text & "', "
        l_strSQL = l_strSQL & "AudioDisClipID3 = '" & txtAudioClipID(5).Text & "', "
        l_strSQL = l_strSQL & "AudioDislocale3 = '" & cmbAudioLocale(5).Text & "', "
        l_strSQL = l_strSQL & "AudioDisClipID4 = '" & txtAudioClipID(6).Text & "', "
        l_strSQL = l_strSQL & "AudioDislocale4 = '" & cmbAudioLocale(6).Text & "', "
        l_strSQL = l_strSQL & "iTunesPosterLocale1_1 = '" & cmbPosterLocale(0).Text & "', "
        l_strSQL = l_strSQL & "iTunesPosterLocale2_1 = '" & cmbPosterLocale(1).Text & "', "
        l_strSQL = l_strSQL & "iTunesPosterLocale3_1 = '" & cmbPosterLocale(2).Text & "', "
        l_strSQL = l_strSQL & "iTunesPosterLocale4_1 = '" & cmbPosterLocale(3).Text & "', "
        l_strSQL = l_strSQL & "iTunesPosterLocale1_2 = '" & cmbPosterLocale(4).Text & "', "
        l_strSQL = l_strSQL & "iTunesPosterLocale2_2 = '" & cmbPosterLocale(5).Text & "', "
        l_strSQL = l_strSQL & "iTunesPosterLocale3_2 = '" & cmbPosterLocale(6).Text & "', "
        l_strSQL = l_strSQL & "iTunesPosterLocale4_2 = '" & cmbPosterLocale(7).Text & "', "
        l_strSQL = l_strSQL & "video_networkname = '" & QuoteSanitise(txtProductionCompany.Text) & "', "
        l_strSQL = l_strSQL & "video_networkname_utf8 = '" & UTF8_Encode(QuoteSanitise(txtProductionCompany.Text)) & "', "
        l_strSQL = l_strSQL & "upc = '" & txtUPC.Text & "', "
        l_strSQL = l_strSQL & "amg_video_id = '" & txtAMGVideoID.Text & "', "
        l_strSQL = l_strSQL & "FeatureTextless = " & chkFeatureTextless.Value & ", "
        l_strSQL = l_strSQL & "FeatureForcedNarrativeLocale = '" & cmbForcedNarrativeLocale(0).Text & "', "
        l_strSQL = l_strSQL & "FeatureBurnedInSubsLocale = '" & cmbForcedNarrativeLocale(5).Text & "', "
        l_strSQL = l_strSQL & "Trailer1ForcedNarrativeLocale = '" & cmbForcedNarrativeLocale(1).Text & "', "
        l_strSQL = l_strSQL & "Trailer1BurnedInSubsLocale = '" & cmbForcedNarrativeLocale(6).Text & "', "
        l_strSQL = l_strSQL & "Trailer2ForcedNarrativeLocale = '" & cmbForcedNarrativeLocale(2).Text & "', "
        l_strSQL = l_strSQL & "Trailer2BurnedInSubsLocale = '" & cmbForcedNarrativeLocale(7).Text & "', "
        l_strSQL = l_strSQL & "Trailer3ForcedNarrativeLocale = '" & cmbForcedNarrativeLocale(3).Text & "', "
        l_strSQL = l_strSQL & "Trailer3BurnedInSubsLocale = '" & cmbForcedNarrativeLocale(8).Text & "', "
        l_strSQL = l_strSQL & "Trailer4ForcedNarrativeLocale = '" & cmbForcedNarrativeLocale(4).Text & "', "
        l_strSQL = l_strSQL & "Trailer4BurnedInSubsLocale = '" & cmbForcedNarrativeLocale(9).Text & "', "
        l_strSQL = l_strSQL & "QCnotesClipID = '" & txtQCClipID(0).Text & "', "
        l_strSQL = l_strSQL & "QCnotestrail1ClipID = '" & txtQCClipID(1).Text & "', "
        l_strSQL = l_strSQL & "QCnotestrail2ClipID = '" & txtQCClipID(2).Text & "', "
        l_strSQL = l_strSQL & "QCnotestrail3ClipID = '" & txtQCClipID(3).Text & "', "
        l_strSQL = l_strSQL & "QCnotestrail4ClipID = '" & txtQCClipID(4).Text & "', "
        l_strSQL = l_strSQL & "rating_au = '" & cmbAUfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_aus = '" & cmbAUSfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_be = '" & cmbBEfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_ca = '" & cmbCAfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_den = '" & cmbDENfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_fi = '" & cmbFIfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_fr = '" & cmbFRfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_de = '" & cmbDEfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_gr = '" & cmbGRfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_ie = '" & cmbIEfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_it = '" & cmbITfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_jp = '" & cmbJPfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_lu = '" & cmbLUfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_mx = '" & cmbMXfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_nl = '" & cmbNLfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_nz = '" & cmbNZfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_no = '" & cmbNOfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_pt = '" & cmbPTfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_qb = '" & cmbQBfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_es = '" & cmbESfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_se = '" & cmbSEfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_ch = '" & cmbCHfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_uk = '" & cmbUKfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_us = '" & cmbUSfilm.Text & "', "
        l_strSQL = l_strSQL & "rating_au_reason = '" & txtAUreason.Text & "', "
        l_strSQL = l_strSQL & "rating_aus_reason = '" & txtAUSreason.Text & "', "
        l_strSQL = l_strSQL & "rating_be_reason = '" & txtBEreason.Text & "', "
        l_strSQL = l_strSQL & "rating_ca_reason = '" & txtCAreason.Text & "', "
        l_strSQL = l_strSQL & "rating_den_reason = '" & txtDENreason.Text & "', "
        l_strSQL = l_strSQL & "rating_fi_reason = '" & txtFIreason.Text & "', "
        l_strSQL = l_strSQL & "rating_fr_reason = '" & txtFRreason.Text & "', "
        l_strSQL = l_strSQL & "rating_de_reason = '" & txtDEreason.Text & "', "
        l_strSQL = l_strSQL & "rating_gr_reason = '" & txtGRreason.Text & "', "
        l_strSQL = l_strSQL & "rating_ie_reason = '" & txtIEreason.Text & "', "
        l_strSQL = l_strSQL & "rating_it_reason = '" & txtITreason.Text & "', "
        l_strSQL = l_strSQL & "rating_jp_reason = '" & txtJPreason.Text & "', "
        l_strSQL = l_strSQL & "rating_lu_reason = '" & txtLUreason.Text & "', "
        l_strSQL = l_strSQL & "rating_mx_reason = '" & txtMXreason.Text & "', "
        l_strSQL = l_strSQL & "rating_nl_reason = '" & txtNLreason.Text & "', "
        l_strSQL = l_strSQL & "rating_nz_reason = '" & txtNZreason.Text & "', "
        l_strSQL = l_strSQL & "rating_no_reason = '" & txtNOreason.Text & "', "
        l_strSQL = l_strSQL & "rating_pt_reason = '" & txtPTreason.Text & "', "
        l_strSQL = l_strSQL & "rating_qb_reason = '" & txtQBreason.Text & "', "
        l_strSQL = l_strSQL & "rating_es_reason = '" & txtESreason.Text & "', "
        l_strSQL = l_strSQL & "rating_se_reason = '" & txtSEreason.Text & "', "
        l_strSQL = l_strSQL & "rating_ch_reason = '" & txtCHreason.Text & "', "
        l_strSQL = l_strSQL & "rating_uk_reason = '" & txtUKreason.Text & "', "
        l_strSQL = l_strSQL & "rating_us_reason = '" & txtUSreason.Text & "' "

    Case 2, 3 '"Amazon" nad "Xbox"
    
        l_strSQL = l_strSQL & "packageused = " & chkPackageUsed(1).Value & ", "
        l_strSQL = l_strSQL & "country = '" & txtAmazonTerritory.Text & "', "
        l_strSQL = l_strSQL & "video_language = '" & txtAmazonLanguage.Text & "', "
        l_strSQL = l_strSQL & "AmazonSeriesUniqueID = '" & txtAmazonSeriesID.Text & "', "
        l_strSQL = l_strSQL & "AmazonSeasonUniqueID = '" & txtAmazonSeasonID.Text & "', "
        l_strSQL = l_strSQL & "AmazonCopyrightHolder = '" & QuoteSanitise(txtAmazonCopyrightHolder.Text) & "', "
        l_strSQL = l_strSQL & "AmazonCopyrightYear = '" & txtAmazonCopyrightYear.Text & "', "
        l_strSQL = l_strSQL & "AmazonSeriesName = '" & QuoteSanitise(txtAmazonSeriesName.Text) & "', "
        l_strSQL = l_strSQL & "AmazonSeasonTitle = '" & QuoteSanitise(txtAmazonSeasonTitle.Text) & "', "
        l_strSQL = l_strSQL & "AmazonSeasonNumber = '" & txtAmazonSeasonNumber.Text & "', "
        l_strSQL = l_strSQL & "video_title = '" & QuoteSanitise(txtAmazonEpisodeTitle.Text) & "', "
        l_strSQL = l_strSQL & "AmazonSequence = '" & txtAmazonSequence.Text & "', "
        l_strSQL = l_strSQL & "AmazonSeriesShortSynopsis = '" & QuoteSanitise(txtAmazonSeriesShortSynopsis.Text) & "', "
        l_strSQL = l_strSQL & "AmazonSeriesLongSynopsis = '" & QuoteSanitise(txtAmazonSeriesLongSynopsis.Text) & "', "
        l_strSQL = l_strSQL & "AmazonSeasonShortSynopsis = '" & QuoteSanitise(txtAmazonSeasonShortSynopsis.Text) & "', "
        l_strSQL = l_strSQL & "AmazonSeasonLongSynopsis = '" & QuoteSanitise(txtAmazonSeasonLongSynopsis.Text) & "', "
        l_strSQL = l_strSQL & "AmazonEpisodeShortSynopsis = '" & QuoteSanitise(txtAmazonEpisodeShortSynopsis.Text) & "', "
        l_strSQL = l_strSQL & "AmazonEpisodeLongSynopsis = '" & QuoteSanitise(txtAmazonEpisodeLongSynopsis.Text) & "', "
        l_strSQL = l_strSQL & "AmazonGenre = '" & txtAmazonGenre.Text & "', "
        l_strSQL = l_strSQL & "AmazonRatingType = '" & txtAmazonRatingType.Text & "', "
        l_strSQL = l_strSQL & "AmazonRating = '" & txtAmazonRating.Text & "', "
        l_strSQL = l_strSQL & "AmazonOriginalAirDate = " & IIf(Not IsNull(datOriginalAirDate.Value), "'" & Format(datOriginalAirDate.Value, "YYYY-MM-DD") & "'", "Null") & ", "
        l_strSQL = l_strSQL & "AmazonSDOfferStart = " & IIf(Not IsNull(datSDAmazonOfferStart.Value), "'" & Format(datSDAmazonOfferStart.Value, "YYYY-MM-DD") & "'", "Null") & ", "
        l_strSQL = l_strSQL & "AmazonHDOfferStart = " & IIf(Not IsNull(datHDAmazonOfferStart.Value), "'" & Format(datHDAmazonOfferStart.Value, "YYYY-MM-DD") & "'", "Null") & ", "
        l_strSQL = l_strSQL & "AmazonSDVODOfferStart = " & IIf(Not IsNull(datSDVODAmazonOfferStart.Value), "'" & Format(datSDVODAmazonOfferStart.Value, "YYYY-MM-DD") & "'", "Null") & ", "
        l_strSQL = l_strSQL & "AmazonHDVODOfferStart = " & IIf(Not IsNull(datHDVODAmazonOfferStart.Value), "'" & Format(datHDVODAmazonOfferStart.Value, "YYYY-MM-DD") & "'", "Null") & ", "
        l_strSQL = l_strSQL & "fullclipid = '" & txtAmazonFullClipID.Text & "', "
        l_strSQL = l_strSQL & "posterclipid = '" & txtAmazonPosterClipID.Text & "', "
        l_strSQL = l_strSQL & "AudioClipid1 = '" & txtAmazonAudioClipID.Text & "', "
        l_strSQL = l_strSQL & "XboxLicensor = '" & QuoteSanitise(txtXboxLicensor.Text) & "' "
        
End Select

l_strSQL = l_strSQL & " WHERE itunes_packageID = " & Val(lblPackageID.Caption) & ";"
Debug.Print l_strSQL
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

Dim PackageBookmark As Variant
If Not adoiTunesPackage.Recordset.BOF And Not adoiTunesPackage.Recordset.EOF Then
    PackageBookmark = adoiTunesPackage.Recordset.Bookmark
    If m_blnNoRefresh = False Then
        adoiTunesPackage.Refresh
        adoiTunesPackage.Caption = adoiTunesPackage.Recordset.RecordCount & " Packages"
    End If
    If PackageBookmark <= adoiTunesPackage.Recordset.RecordCount Then
        adoiTunesPackage.Recordset.Bookmark = PackageBookmark
    End If
End If

End Sub

'Private Sub cmdMakeChaptersXML_Click()
'
'Dim l_lngChaptersClipID As Long, l_lngTemp As Long, l_strPathToFullFile As String, l_strAltFolder As String, l_strFileNameToSave As String
'
'adoChapters.Refresh
'
''Check the compulsory info in the Chapters controls
'adoChapters.Recordset.MoveFirst
'
'Do While Not adoChapters.Recordset.EOF
'    If Trim(" " & adoChapters.Recordset("starttime")) = "" Or Trim(" " & adoChapters.Recordset("title")) = "" Or Val(Trim(" " & adoChapters.Recordset("pictureclipID"))) = 0 Then
'        MsgBox "Compulsory Chapter Information is not in order." & vbCrLf & "All chapters must have a start time, a title and the clipID of the chapter graphic.", vbCritical, "Cannot Make XML"
'        Exit Sub
'    End If
'    adoChapters.Recordset.MoveNext
'Loop
'
'l_lngTemp = GetData("events", "libraryID", "eventID", txtFullClipID.Text)
'l_strPathToFullFile = GetData("library", "subtitle", "libraryID", l_lngTemp)
'l_strAltFolder = GetData("events", "altlocation", "eventID", txtFullClipID.Text)
'If l_strAltFolder <> "" Then
'    l_strPathToFullFile = l_strPathToFullFile & "\" & l_strAltFolder
'End If
'MDIForm1.dlgMain.InitDir = l_strPathToFullFile
'MDIForm1.dlgMain.Filter = "XML files|*.xml"
'MDIForm1.dlgMain.FileName = "chapter.xml"
'
'MDIForm1.dlgMain.ShowSave
'
'l_strFileNameToSave = MDIForm1.dlgMain.FileName
'
'If l_strFileNameToSave <> "" Then
'
'    l_lngChaptersClipID = MakeClipInAdvance(141, "iTunes Package " & lblPackageID.caption & " chapter.xml file", "", 0, "", "", GetNextSequence("internalreference"), GetData("library", "barcode", "libraryID", l_lngTemp), lblCompanyID.Caption, "", "chapter.xml", l_strAltFolder)
'    SetData "events", "hidefromweb", "eventID", l_lngChaptersClipID, 1
'
'    Open l_strFileNameToSave For Output As 1
'    Print #1, "<?xml version=""1.0"" encoding=""UTF-8""?>"
'    Print #1, "<chapters version=""1"">"
'    adoChapters.Recordset.MoveFirst
'    Do While Not adoChapters.Recordset.EOF
'        Print #1, Chr(9) & "<chapter starttime=""" & adoChapters.Recordset("starttime") & """>"
'        Print #1, Chr(9) & Chr(9) & "<title>" & XMLSanitise(adoChapters.Recordset("title")) & "</title>"
'        Print #1, Chr(9) & Chr(9) & "<picture>" & GetData("events", "clipfilename", "eventID", adoChapters.Recordset("pictureclipid")) & "</picture>"
'        Print #1, Chr(9) & "</chapter>"
'        adoChapters.Recordset.MoveNext
'    Loop
'    Print #1, "</chapters>"
'    Close 1
'
'End If
'
'ShowClipControl l_lngChaptersClipID
'DoEvents
'
'frmClipControl.cmdMD5Checksum.Value = True
'frmClipControl.cmdPlay.Value = True
'frmClipControl.cmdSave.Value = True
'Unload frmClipControl
'
'txtChaptersClipID.Text = l_lngChaptersClipID
'
'End Sub

Private Sub cmdSearch_Click()

Dim l_varBookmark As Variant, SQL As String

On Error GoTo PROCEND
If optFilter(1).Value = True Then
    m_strFilter = " AND video_type = 'film' AND packageused = 0"
ElseIf optFilter(2).Value = True Then
    m_strFilter = " AND video_type = 'tv'"
ElseIf optFilter(3).Value = True Then
    m_strFilter = " AND video_type = 'Amazon'"
ElseIf optFilter(4).Value = True Then
    m_strFilter = " AND video_type = 'tv' AND packageused = 0"
ElseIf optFilter(5).Value = True Then
    m_strFilter = " AND video_type = 'Amazon' AND packageused = 0"
ElseIf optFilter(6).Value = True Then
    m_strFilter = " AND video_type = 'film'"
ElseIf optFilter(7).Value = True Then
    m_strFilter = " AND video_type = 'Xbox' AND packageused = 0"
ElseIf optFilter(8).Value = True Then
    m_strFilter = " AND video_type = 'Xbox'"
Else
    m_strFilter = ""
End If

SQL = m_strSearch
If txtSearchVendorID.Text <> "" Then
    SQL = SQL & " AND video_VendorID LIKE '" & txtSearchVendorID.Text & "%'"
End If
adoiTunesPackage.RecordSource = SQL & m_strFilter & ";"
adoiTunesPackage.ConnectionString = g_strConnection

If adoiTunesPackage.Recordset.BOF = False And adoiTunesPackage.Recordset.EOF = False Then
    l_varBookmark = adoiTunesPackage.Recordset.Bookmark
    adoiTunesPackage.Refresh
    adoiTunesPackage.Recordset.Bookmark = l_varBookmark
Else
    adoiTunesPackage.Refresh
End If

PROCEND:
adoiTunesPackage.Refresh
adoiTunesPackage.Caption = adoiTunesPackage.Recordset.RecordCount & " Packages"

If optFilter(3).Value = True Or optFilter(5).Value Or optFilter(7).Value Or optFilter(8).Value = True Then
    grdItunesPackage.Columns("video_containerID").Visible = False
    grdItunesPackage.Columns("AmazonSeriesName").Visible = True
    grdItunesPackage.Columns("AmazonSeasonNumber").Visible = True
    grdItunesPackage.Columns("video_vendorID").Visible = True
    grdItunesPackage.Columns("video_ep_prod_no").Visible = False
    grdItunesPackage.Columns("video_containerposition").Visible = False
Else
    grdItunesPackage.Columns("video_containerID").Visible = False
    grdItunesPackage.Columns("AmazonSeriesName").Visible = False
    grdItunesPackage.Columns("AmazonSeasonNumber").Visible = False
    grdItunesPackage.Columns("video_vendorID").Visible = True
    grdItunesPackage.Columns("video_ep_prod_no").Visible = True
    grdItunesPackage.Columns("video_containerposition").Visible = True
End If

End Sub

Private Sub cmdUpdateCastCharacters_Click()

Dim l_rst As ADODB.Recordset, Count As Integer

Set l_rst = ExecuteSQL("SELECT * FROM iTunes_cast_character;", g_strExecuteError)
If l_rst.RecordCount > 0 Then

    l_rst.MoveFirst
    ProgressBar1.Visible = True
    ProgressBar1.Max = l_rst.RecordCount
    Count = 0
    Do While Not l_rst.EOF
        ProgressBar1.Value = Count
        DoEvents
        l_rst("character_name") = UTF8_Decode(Trim(" " & l_rst("character_name_utf8")))
        l_rst("character_name2") = UTF8_Decode(Trim(" " & l_rst("character_name2_utf8")))
        l_rst("character_name3") = UTF8_Decode(Trim(" " & l_rst("character_name3_utf8")))
        l_rst("character_name4") = UTF8_Decode(Trim(" " & l_rst("character_name4_utf8")))
        l_rst("character_note") = UTF8_Decode(Trim(" " & l_rst("character_note_utf8")))
        l_rst("character_note2") = UTF8_Decode(Trim(" " & l_rst("character_note2_utf8")))
        l_rst("character_note3") = UTF8_Decode(Trim(" " & l_rst("character_note3_utf8")))
        l_rst("character_note4") = UTF8_Decode(Trim(" " & l_rst("character_note4_utf8")))
        l_rst.Update
        l_rst.MoveNext
        Count = Count + 1
    Loop
End If
l_rst.Close
Set l_rst = Nothing
ProgressBar1.Visible = False

End Sub

Private Sub cmdUpdatecastUTF8_Click()

Dim l_rst As ADODB.Recordset, Count As Integer

Set l_rst = ExecuteSQL("SELECT * FROM iTunes_cast;", g_strExecuteError)
If l_rst.RecordCount > 0 Then

    l_rst.MoveFirst
    ProgressBar1.Visible = True
    ProgressBar1.Max = l_rst.RecordCount
    Count = 0
    Do While Not l_rst.EOF
        ProgressBar1.Value = Count
        DoEvents
        l_rst("display_name") = UTF8_Decode(Trim(" " & l_rst("display_name_utf8")))
        l_rst("display_name2") = UTF8_Decode(Trim(" " & l_rst("display_name2_utf8")))
        l_rst("display_name3") = UTF8_Decode(Trim(" " & l_rst("display_name3_utf8")))
        l_rst("display_name4") = UTF8_Decode(Trim(" " & l_rst("display_name4_utf8")))
        l_rst("description") = UTF8_Decode(Trim(" " & l_rst("description_utf8")))
        l_rst("description2") = UTF8_Decode(Trim(" " & l_rst("description2_utf8")))
        l_rst("description3") = UTF8_Decode(Trim(" " & l_rst("description3_utf8")))
        l_rst("description4") = UTF8_Decode(Trim(" " & l_rst("description4_utf8")))
        l_rst.Update
        l_rst.MoveNext
        Count = Count + 1
    Loop
End If
l_rst.Close
Set l_rst = Nothing
ProgressBar1.Visible = False

End Sub

Private Sub cmdUpdateCrewUtf8_Click()

Dim l_rst As ADODB.Recordset, Count As Integer

Set l_rst = ExecuteSQL("SELECT * FROM iTunes_crew;", g_strExecuteError)
If l_rst.RecordCount > 0 Then

    l_rst.MoveFirst
    ProgressBar1.Visible = True
    ProgressBar1.Max = l_rst.RecordCount
    Count = 0
    Do While Not l_rst.EOF
        ProgressBar1.Value = Count
        DoEvents
        l_rst("display_name") = UTF8_Decode(Trim(" " & l_rst("display_name_utf8")))
        l_rst("display_name2") = UTF8_Decode(Trim(" " & l_rst("display_name2_utf8")))
        l_rst("display_name3") = UTF8_Decode(Trim(" " & l_rst("display_name3_utf8")))
        l_rst("display_name4") = UTF8_Decode(Trim(" " & l_rst("display_name4_utf8")))
        l_rst.Update
        l_rst.MoveNext
        Count = Count + 1
    Loop
End If
l_rst.Close
Set l_rst = Nothing
ProgressBar1.Visible = False

End Sub

Private Sub cmdUpdateUTF8_Click()

Dim l_rst As ADODB.Recordset, Count As Integer, l_strSQL

Set l_rst = ExecuteSQL("SELECT * FROM iTunes_package;", g_strExecuteError)
If l_rst.RecordCount > 0 Then

    l_rst.MoveFirst
    ProgressBar1.Visible = True
    ProgressBar1.Max = l_rst.RecordCount
    Count = 0
    Do While Not l_rst.EOF
        ProgressBar1.Value = Count
        DoEvents
        l_rst("video_networkname") = UTF8_Decode(Trim(" " & l_rst("video_networkname_utf8")))
        l_rst("video_title") = UTF8_Decode(Trim(" " & l_rst("video_title_utf8")))
        l_rst("video_copyright_cline") = UTF8_Decode(Trim(" " & l_rst("video_copyright_cline_utf8")))
        l_rst("video_longdescription") = UTF8_Decode(Trim(" " & l_rst("video_longdescription_utf8")))
        l_rst("itunestechcomment") = UTF8_Decode(Trim(" " & l_rst("itunestechcomment_utf8")))
        l_rst("film_title1") = UTF8_Decode(Trim(" " & l_rst("film_title1_utf8")))
        l_rst("film_synopsis1") = UTF8_Decode(Trim(" " & l_rst("film_synopsis1_utf8")))
        l_rst("film_copyright_cline1") = UTF8_Decode(Trim(" " & l_rst("film_copyright_cline1_utf8")))
        l_rst("film_title2") = UTF8_Decode(Trim(" " & l_rst("film_title2_utf8")))
        l_rst("film_synopsis2") = UTF8_Decode(Trim(" " & l_rst("film_synopsis2_utf8")))
        l_rst("film_copyright_cline2") = UTF8_Decode(Trim(" " & l_rst("film_copyright_cline2_utf8")))
        l_rst("film_title3") = UTF8_Decode(Trim(" " & l_rst("film_title3_utf8")))
        l_rst("film_synopsis3") = UTF8_Decode(Trim(" " & l_rst("film_synopsis3_utf8")))
        l_rst("film_copyright_cline3") = UTF8_Decode(Trim(" " & l_rst("film_copyright_cline3_utf8")))
        l_rst("film_title4") = UTF8_Decode(Trim(" " & l_rst("film_title4_utf8")))
        l_rst("film_synopsis4") = UTF8_Decode(Trim(" " & l_rst("film_synopsis4_utf8")))
        l_rst("film_copyright_cline4") = UTF8_Decode(Trim(" " & l_rst("film_copyright_cline4_utf8")))
        l_rst("Maxdome_Title_German") = UTF8_Decode(Trim(" " & l_rst("Maxdome_Title_German_utf8")))
        l_rst("Maxdome_Title_Original") = UTF8_Decode(Trim(" " & l_rst("Maxdome_Title_Original_utf8")))
        l_rst("Maxdome_Season_Title_German") = UTF8_Decode(Trim(" " & l_rst("Maxdome_Season_Title_German_utf8")))
        l_rst("Maxdome_Season_Title_Original") = UTF8_Decode(Trim(" " & l_rst("Maxdome_Season_Title_Original_utf8")))
        l_rst("Maxdome_Episode_Title_German") = UTF8_Decode(Trim(" " & l_rst("Maxdome_Episode_Title_German_utf8")))
        l_rst("Maxdome_Episode_Title_Original") = UTF8_Decode(Trim(" " & l_rst("Maxdome_Episode_Title_Original_utf8")))
        l_rst("Maxdome_Long_Description_German") = UTF8_Decode(Trim(" " & l_rst("Maxdome_Long_Description_German_utf8")))
        l_rst("Maxdome_Long_Description_Original") = UTF8_Decode(Trim(" " & l_rst("Maxdome_Long_Description_Original_utf8")))
        l_rst("Maxdome_Short_Description_German") = UTF8_Decode(Trim(" " & l_rst("Maxdome_Short_Description_German_utf8")))
        l_rst("Maxdome_Short_Description_Original") = UTF8_Decode(Trim(" " & l_rst("Maxdome_Short_Description_Original_utf8")))
        l_rst.Update
        l_rst.MoveNext
        Count = Count + 1
    Loop
End If
l_rst.Close

Set l_rst = ExecuteSQL("SELECT eventID FROM events WHERE system_deleted = 0 AND video_title_utf8 IS NOT NULL and video_title_utf8 <> '';", g_strExecuteError)
If l_rst.RecordCount > 0 Then

    l_rst.MoveFirst
    ProgressBar1.Visible = True
    ProgressBar1.Max = l_rst.RecordCount
    Count = 0
    Do While Not l_rst.EOF
        ProgressBar1.Value = Count
        DoEvents
        l_strSQL = "UPDATE events SET "
        l_strSQL = l_strSQL & "video_title = '" & UTF8_Decode(QuoteSanitise(GetData("events", "video_title_utf8", "eventID", l_rst("eventID")))) & "', "
        l_strSQL = l_strSQL & "video_copyright_cline = '" & UTF8_Decode(QuoteSanitise(GetData("events", "video_copyright_cline_utf8", "eventID", l_rst("eventID")))) & "', "
        l_strSQL = l_strSQL & "video_longdescription = '" & UTF8_Decode(QuoteSanitise(GetData("events", "video_longdescription_utf8", "eventID", l_rst("eventID")))) & "', "
        l_strSQL = l_strSQL & "itunestechcomment = '" & UTF8_Decode(QuoteSanitise(GetData("events", "itunestechcomment_utf8", "eventID", l_rst("eventID")))) & "' "
        l_strSQL = l_strSQL & "WHERE eventID = " & l_rst("eventID") & ";"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_rst.MoveNext
        Count = Count + 1
    Loop
End If
l_rst.Close

Set l_rst = Nothing
ProgressBar1.Visible = False

End Sub

Private Sub cmdUTF8Decode_Click(Index As Integer)

txtFilmTitle(Index).Text = UTF8_Decode(txtFilmTitle(Index).Text)
txtSynopsis(Index).Text = UTF8_Decode(txtSynopsis(Index).Text)
txtFilmCopyrightLine(Index).Text = UTF8_Decode(txtFilmCopyrightLine(Index).Text)

End Sub

Private Sub cmdXboxEpisodeXMLs_Click()

Dim l_strPathToFullFile As String, l_strAltFolder As String, l_strOriginalFilename As String, l_strNewFilename As String, l_strNewFileReference As String, l_strFileNameToSave As String
Dim l_strTemp As String, l_lngTemp As Long, FSO As Scripting.FileSystemObject, l_strMasterFilename As String, l_strJPGFilename As String, l_strAudioFilename As String
Dim l_strDuration As String, l_strRunningTime As String, l_lngChannelCount As Long, l_strMasterReference As String

'Check Package Output for Xbox Packages only.
If cmbiTunesType.Text <> "Xbox" Then
    MsgBox "This XML Output is for 'Xbox' packages only", vbCritical, "Cannot Make XML"
    Exit Sub
End If

If txtAmazonLanguage.Text = "" Then
    txtAmazonLanguage.Text = GetDataSQL("SELECT TOP 1 LanguageCultureName FROM isoLanguageCulture WHERE LanguageCultureName LIKE '%" & txtAmazonTerritory.Text & "'")
End If

cmdSave.Value = True
Set FSO = New Scripting.FileSystemObject

'Check the compulsory info in the normal controls
If txtiTunesVendorID.Text = "" Or txtAmazonTerritory.Text = "" Or txtAmazonLanguage.Text = "" Or txtAmazonSeriesID.Text = "" Or txtAmazonSeasonID.Text = "" _
Or txtAmazonCopyrightHolder.Text = "" Or txtAmazonEpisodeTitle.Text = "" Or txtAmazonGenre.Text = "" Or IsNull(datOriginalAirDate.Value) _
Or txtAmazonEpisodeShortSynopsis.Text = "" Or txtAmazonEpisodeLongSynopsis.Text = "" _
Or txtAmazonFullClipID.Text = "" Or txtAmazonPosterClipID.Text = "" _
Then
    MsgBox "Compulsory Information is has not yet been completed." & vbCrLf & "All red fields must be filled in", vbCritical, "Cannot Make XML"
    Exit Sub
End If

'Get the Duration String
l_strDuration = GetData("events", "fd_length", "eventID", txtAmazonFullClipID.Text)
If l_strDuration <> "" Then
    l_strRunningTime = "PT" & Val(Left(l_strDuration, 2)) & "H" & Val(Mid(l_strDuration, 4, 2)) & "M" & Val(Mid(l_strDuration, 7, 2)) & Format(Val(Mid(l_strDuration, 10, 2)) / Val(GetData("events", "clipframerate", "eventID", txtAmazonFullClipID.Text)), ".00") & "S"
End If

'Get Master Clip and do the rename of the file, and update the file record.
l_lngTemp = GetData("events", "libraryID", "eventID", txtAmazonFullClipID.Text)
l_strPathToFullFile = GetData("library", "subtitle", "libraryID", l_lngTemp)
l_strAltFolder = GetData("events", "altlocation", "eventID", txtAmazonFullClipID.Text)
If l_strAltFolder <> "" Then
    l_strPathToFullFile = l_strPathToFullFile & "\" & l_strAltFolder
End If

MDIForm1.dlgMain.InitDir = l_strPathToFullFile
MDIForm1.dlgMain.Filter = "XML files|*.xml"
MDIForm1.dlgMain.Filename = txtiTunesVendorID.Text & "_episode.xml"

MDIForm1.dlgMain.ShowSave

l_strFileNameToSave = MDIForm1.dlgMain.Filename

If l_strFileNameToSave <> "" Then

    Open l_strFileNameToSave For Output As 1
    Print #1, "<?xml version=""1.0"" encoding=""UTF-8"" standalone=""no""?>"
    Print #1, "<mdmec:CoreMetadata xmlns:mdmec=""http://www.movielabs.com/schema/mdmec/v2.4"" xmlns:md=""http://www.movielabs.com/schema/md/v2.4/md""";
    Print #1, " xmlns:xs=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">"
    Print #1, Chr(9) & "<mdmec:Basic ContentID=""md:cid:" & txtiTunesVendorID.Text & ":episode"">"
    Print #1, Chr(9) & Chr(9) & "<md:LocalizedInfo language=""" & Left(txtAmazonLanguage.Text, 2) & """ default=""true"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:TitleDisplayUnlimited>" & XMLSanitise(txtAmazonEpisodeTitle.Text) & "</md:TitleDisplayUnlimited>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:TitleSort></md:TitleSort>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:ArtReference resolution=""800x800"">md:cid:" & txtAmazonSeasonID.Text & ".image." & Left(txtAmazonLanguage.Text, 2) & "</md:ArtReference>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Summary190>" & XMLSanitise(txtAmazonEpisodeShortSynopsis.Text) & "</md:Summary190>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Summary4000>" & XMLSanitise(txtAmazonEpisodeLongSynopsis.Text) & "</md:Summary4000>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:OriginalTitle>" & XMLSanitise(txtAmazonEpisodeTitle.Text) & "</md:OriginalTitle>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Genre>" & txtAmazonGenre.Text & "</md:Genre>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:CopyrightLine>" & txtAmazonCopyrightHolder.Text & "</md:CopyrightLine>"
    Print #1, Chr(9) & Chr(9) & "</md:LocalizedInfo>"
    Print #1, Chr(9) & Chr(9) & "<md:RunLength>" & l_strRunningTime & "</md:RunLength>"
    Print #1, Chr(9) & Chr(9) & "<md:ReleaseYear>" & Format(datOriginalAirDate.Value, "YYYY") & "</md:ReleaseYear>"
    Print #1, Chr(9) & Chr(9) & "<md:ReleaseDate>" & Format(datOriginalAirDate.Value, "YYYY-MM-DD") & "</md:ReleaseDate>"
    Print #1, Chr(9) & Chr(9) & "<md:ReleaseHistory>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:ReleaseType>original</md:ReleaseType>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:DistrTerritory>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:country>" & txtAmazonTerritory.Text & "</md:country>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:DistrTerritory>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Date>" & Format(datOriginalAirDate.Value, "YYYY-MM-DD") & "</md:Date>"
    Print #1, Chr(9) & Chr(9) & "</md:ReleaseHistory>"
    Print #1, Chr(9) & Chr(9) & "<md:WorkType>TVSeason</md:WorkType>"
    Print #1, Chr(9) & Chr(9) & "<md:RatingSet>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Rating>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:Region>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:country>" & txtAmazonTerritory.Text & "</md:country>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</md:Region>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:System>" & txtAmazonRatingType.Text & "</md:System>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:Value>" & txtAmazonRating.Text & "</md:Value>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:Description/>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Rating>"
    Print #1, Chr(9) & Chr(9) & "</md:RatingSet>"
    Print #1, Chr(9) & Chr(9) & "<md:People>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobFunction>Actor</md:JobFunction>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobDisplay>Actor</md:JobDisplay>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:BillingBlockOrder>1</md:BillingBlockOrder>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:Character>" & "Character Name" & "</md:Character>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Name>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:DisplayName>" & "Actor Name" & "</md:DisplayName>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Name>"
    Print #1, Chr(9) & Chr(9) & "</md:People>"
    Print #1, Chr(9) & Chr(9) & "<md:People>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobFunction>Writer</md:JobFunction>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobDisplay>Writer</md:JobDisplay>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:BillingBlockOrder>1</md:BillingBlockOrder>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Name>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:DisplayName>" & "Writer Name" & "</md:DisplayName>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Name>"
    Print #1, Chr(9) & Chr(9) & "</md:People>"
    Print #1, Chr(9) & Chr(9) & "<md:People>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobFunction>Director</md:JobFunction>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobDisplay>Director</md:JobDisplay>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:BillingBlockOrder>1</md:BillingBlockOrder>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Name>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:DisplayName>" & "Directot Name" & "</md:DisplayName>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Name>"
    Print #1, Chr(9) & Chr(9) & "</md:People>"
    Print #1, Chr(9) & Chr(9) & "<md:People>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobFunction>Producer</md:JobFunction>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobDisplay>Producer</md:JobDisplay>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:BillingBlockOrder>1</md:BillingBlockOrder>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Name>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:DisplayName>" & "Producer Name" & "</md:DisplayName>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Name>"
    Print #1, Chr(9) & Chr(9) & "</md:People>"
    Print #1, Chr(9) & Chr(9) & "<md:CountryOfOrigin>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:country>" & txtAmazonTerritory.Text & "</md:country>"
    Print #1, Chr(9) & Chr(9) & "</md:CountryOfOrigin>"
    Print #1, Chr(9) & Chr(9) & "<md:PrimarySpokenLanguage>" & Left(txtAmazonLanguage.Text, 2) & "</md:PrimarySpokenLanguage>"
    Print #1, Chr(9) & Chr(9) & "<md:OriginalLanguage>" & Left(txtAmazonLanguage.Text, 2) & "</md:OriginalLanguage>"
    Print #1, Chr(9) & Chr(9) & "<md:AssociatedOrg role=""Studio"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:DisplayName>" & XMLSanitise(txtXboxLicensor.Text) & "</md:DisplayName>"
    Print #1, Chr(9) & Chr(9) & "</md:AssociatedOrg>"
    Print #1, Chr(9) & Chr(9) & "<md:SequenceInfo>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Number>" & txtAmazonSequence.Text & "</md:Number>"
    Print #1, Chr(9) & Chr(9) & "</md:SequenceInfo>"
    Print #1, Chr(9) & Chr(9) & "<md:Parent relationshipType=""isepisodeof"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:ParentContentID>md:cid:" & txtAmazonSeasonID.Text & ":season</md:ParentContentID>"
    Print #1, Chr(9) & Chr(9) & "</md:Parent>"
    Print #1, Chr(9) & "</mdmec:Basic>"
    Print #1, "</mdmec:CoreMetadata>"
    Close 1
    
End If

MDIForm1.dlgMain.InitDir = l_strPathToFullFile
MDIForm1.dlgMain.Filter = "XML files|*.xml"
MDIForm1.dlgMain.Filename = txtiTunesVendorID.Text & ".xml"

MDIForm1.dlgMain.ShowSave

l_strFileNameToSave = MDIForm1.dlgMain.Filename

If l_strFileNameToSave <> "" Then

    Open l_strFileNameToSave For Output As 1
    Print #1, "<?xml version=""1.0"" encoding=""UTF-8"" standalone=""no""?>"
 
    Print #1, "<manifest:MediaManifest xmlns:manifest=""http://www.movielabs.com/schema/manifest/v1.5/manifest"" xmlns:md=""http://www.movielabs.com/schema/md/v2.4/md"" ";
    Print #1, "xmlns:xs=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" ManifestID=""MSFT_123454321"">"
    Print #1, Chr(9) & "<manifest:Compatibility>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<manifest:SpecVersion>1.6</manifest:SpecVersion>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<manifest:Profile>IP-0</manifest:Profile>"
    Print #1, Chr(9) & "</manifest:Compatibility>"
    Print #1, Chr(9) & "<manifest:Inventory>"
    Print #1, Chr(9) & Chr(9) & "<manifest:Audio AudioTrackID=""md:audtrackid:org:bbc.com:" & Left(txtAmazonLanguage.Text, 2) & "." & txtiTunesVendorID.Text & ".TH-HDsource"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Type>primary</md:Type>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Encoding>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:Codec>PCM</md:Codec>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Encoding>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Language dubbed=""false"">" & Left(txtAmazonLanguage.Text, 2) & "</md:Language>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<manifest:ContainerReference>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<manifest:ContainerLocation>./resources/" & GetData("events", "clipfilename", "eventID", txtAmazonFullClipID.Text) & "</manifest:ContainerLocation>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</manifest:ContainerReference>"
    Print #1, Chr(9) & Chr(9) & "</manifest:Audio>"
    Print #1, Chr(9) & Chr(9) & "<manifest:Video VideoTrackID=""md:vidtrackid:org:bbc.com:" & Left(txtAmazonLanguage.Text, 2) & "." & txtiTunesVendorID.Text & ".TH-HDsource"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Type>primary</md:Type>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Encoding>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:Codec>ProRes</md:Codec>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Encoding>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Picture>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:AspectRatio>" & GetData("events", "aspectratio", "eventID", txtAmazonFullClipID.Text) & "</md:AspectRatio>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:WidthPixels>" & GetData("events", "cliphorizontalpixels", "eventID", txtAmazonFullClipID.Text) & "</md:WidthPixels>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:HeightPixels>" & GetData("events", "clipverticalpixels", "eventID", txtAmazonFullClipID.Text) & "</md:HeightPixels>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Picture>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<manifest:ContainerReference>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<manifest:ContainerLocation>./resources/" & GetData("events", "clipfilename", "eventID", txtAmazonFullClipID.Text) & "</manifest:ContainerLocation>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</manifest:ContainerReference>"
    Print #1, Chr(9) & Chr(9) & "</manifest:Video>"
    Print #1, Chr(9) & Chr(9) & "<manifest:Video VideoTrackID=""md:vidtrackid:org:bbc.com:" & Left(txtAmazonLanguage.Text, 2) & "." & txtiTunesVendorID.Text & ".TH-SDsource"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Type>primary</md:Type>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Encoding>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:Codec>ProRes</md:Codec>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Encoding>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<manifest:ContainerReference>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<manifest:ContainerLocation>./resources/" & GetData("events", "clipfilename", "eventID", txtAmazonFullClipID.Text) & "</manifest:ContainerLocation>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</manifest:ContainerReference>"
    Print #1, Chr(9) & Chr(9) & "</manifest:Video>"
    Print #1, Chr(9) & Chr(9) & "<manifest:Image ImageID=""md:imageid:org:bbc.com:" & Left(txtAmazonLanguage.Text, 2) & "." & txtiTunesVendorID.Text & ".TH-WW-artwork"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Width>800</md:Width>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Height>800</md:Height>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Encoding>JPEG</md:Encoding>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Language>" & txtAmazonLanguage.Text & "</md:Language>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<manifest:ContainerReference>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<manifest:ContainerLocation>./resources/" & GetData("events", "clipfilename", "eventID", txtAmazonPosterClipID.Text) & "</manifest:ContainerLocation>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</manifest:ContainerReference>"
    Print #1, Chr(9) & Chr(9) & "</manifest:Image>"
    Print #1, Chr(9) & Chr(9) & "<manifest:Metadata ContentID=""md:cid:org:bbc.com:" & txtiTunesVendorID.Text & ":episode"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<manifest:ContainerReference>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<manifest:ContainerLocation>./resources/" & txtiTunesVendorID.Text & ".xml</manifest:ContainerLocation>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</manifest:ContainerReference>"
    Print #1, Chr(9) & Chr(9) & "</manifest:Metadata>"
    Print #1, Chr(9) & "</manifest:Inventory>"
    Print #1, Chr(9) & "<manifest:Presentations>"
    Print #1, Chr(9) & Chr(9) & "<manifest:Presentation PresentationID=""md:presentationid:org:bbc.com:" & txtiTunesVendorID.Text & ":episode"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<manifest:TrackMetadata>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<manifest:TrackSelectionNumber>0</manifest:TrackSelectionNumber>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<manifest:VideoTrackReference>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<manifest:VideoTrackID>md:vidtrackid:org:bbc.com:" & Left(txtAmazonLanguage.Text, 2) & "." & txtiTunesVendorID.Text & ".TH-HDsource</manifest:VideoTrackID>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</manifest:VideoTrackReference>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<manifest:VideoTrackReference>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<manifest:VideoTrackID>md:vidtrackid:org:bbc.com:" & Left(txtAmazonLanguage.Text, 2) & "." & txtiTunesVendorID.Text & ".TH-SDsource</manifest:VideoTrackID>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</manifest:VideoTrackReference>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<manifest:AudioTrackReference>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<manifest:AudioTrackID>md:audtrackid:org:bbc.com:" & Left(txtAmazonLanguage.Text, 2) & "." & txtiTunesVendorID.Text & ".TH-HDsource</manifest:AudioTrackID>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</manifest:AudioTrackReference>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</manifest:TrackMetadata>"
    Print #1, Chr(9) & Chr(9) & "</manifest:Presentation>"
    Print #1, Chr(9) & "</manifest:Presentations>"
    Print #1, Chr(9) & "<manifest:Experiences>"
    Print #1, Chr(9) & Chr(9) & "<manifest:Experience ExperienceID=""md:experienceid:org:bbc.com:" & txtiTunesVendorID.Text & ":episode"" version=""1.0"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<manifest:ContentID>md:cid:" & txtiTunesVendorID.Text & ":episode</manifest:ContentID>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<manifest:Audiovisual ContentID=""md:cid:" & txtiTunesVendorID.Text & ":episode"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<manifest:Type>Main</manifest:Type>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<manifest:SubType>TVEpisode</manifest:SubType>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<manifest:PresentationID>md:presentationid:org:bbc.com:" & txtiTunesVendorID.Text & ":episode</manifest:PresentationID>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</manifest:Audiovisual>"
    Print #1, Chr(9) & Chr(9) & "</manifest:Experience>"
    Print #1, Chr(9) & "</manifest:Experiences>"
    Print #1, Chr(9) & "<manifest:ALIDExperienceMaps>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<manifest:ALIDExperienceMap>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<manifest:ALID>" & txtiTunesVendorID.Text & "</manifest:ALID>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<manifest:ExperienceID condition=""For-sale"">md:experienceid:org:bbc.com:" & txtiTunesVendorID.Text & ":episode</manifest:ExperienceID>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</manifest:ALIDExperienceMap>"
    Print #1, Chr(9) & "</manifest:ALIDExperienceMaps>"
    Print #1, "</manifest:MediaManifest>"
    Close 1
    
End If

'chkPackageUsed(1).Value = 1
'cmdSave.Value = True
MsgBox "Done"

End Sub

Private Sub cmdXboxSeasonXML_Click()

Dim l_strPathToFullFile As String, l_strAltFolder As String, l_strOriginalFilename As String, l_strNewFilename As String, l_strNewFileReference As String, l_strFileNameToSave As String
Dim l_strTemp As String, l_lngTemp As Long, FSO As Scripting.FileSystemObject, l_strMasterFilename As String, l_strJPGFilename As String, l_strAudioFilename As String
Dim l_strDuration As String, l_lngRunningTime As Long, l_lngChannelCount As Long, l_strMasterReference As String


'Check Package Output for Xbox Packages only.
If cmbiTunesType.Text <> "Xbox" Then
    MsgBox "This XML Output is for 'Xbox' packages only", vbCritical, "Cannot Make XML"
    Exit Sub
End If

If txtAmazonLanguage.Text = "" Then
    txtAmazonLanguage.Text = GetDataSQL("SELECT TOP 1 LanguageCultureName FROM isoLanguageCulture WHERE LanguageCultureName LIKE '%" & txtAmazonTerritory.Text & "'")
End If

cmdSave.Value = True
Set FSO = New Scripting.FileSystemObject

'Check the compulsory info in the normal controls
If txtiTunesVendorID.Text = "" Or txtAmazonTerritory.Text = "" Or txtAmazonLanguage.Text = "" Or txtAmazonSeriesID.Text = "" Or txtAmazonSeasonID.Text = "" _
Or txtAmazonCopyrightHolder.Text = "" Or txtAmazonSeasonTitle.Text = "" Or txtAmazonGenre.Text = "" Or IsNull(datOriginalAirDate.Value) _
Or txtAmazonSeasonShortSynopsis.Text = "" Or txtAmazonSeasonLongSynopsis.Text = "" _
Then
    MsgBox "Compulsory Information is has not yet been completed." & vbCrLf & "All red fields must be filled in", vbCritical, "Cannot Make XML"
    Exit Sub
End If

'Get Master Clip and do the rename of the file, and update the file record.
l_lngTemp = GetData("events", "libraryID", "eventID", txtAmazonFullClipID.Text)
l_strPathToFullFile = GetData("library", "subtitle", "libraryID", l_lngTemp)
l_strAltFolder = GetData("events", "altlocation", "eventID", txtAmazonFullClipID.Text)
If l_strAltFolder <> "" Then
    l_strPathToFullFile = l_strPathToFullFile & "\" & l_strAltFolder
End If

MDIForm1.dlgMain.InitDir = l_strPathToFullFile
MDIForm1.dlgMain.Filter = "XML files|*.xml"
MDIForm1.dlgMain.Filename = txtAmazonSeasonID.Text & "_season.xml"

MDIForm1.dlgMain.ShowSave

l_strFileNameToSave = MDIForm1.dlgMain.Filename


If l_strFileNameToSave <> "" Then

    Open l_strFileNameToSave For Output As 1
    Print #1, "<?xml version=""1.0"" encoding=""UTF-8"" standalone=""no""?>"
    Print #1, "<mdmec:CoreMetadata xmlns:mdmec=""http://www.movielabs.com/schema/mdmec/v2.4"" xmlns:md=""http://www.movielabs.com/schema/md/v2.4/md""";
    Print #1, " xmlns:xs=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">"
    Print #1, Chr(9) & "<mdmec:Basic ContentID=""md:cid:" & txtAmazonSeasonID.Text & ":season"">"
    Print #1, Chr(9) & Chr(9) & "<md:LocalizedInfo language=""" & Left(txtAmazonLanguage.Text, 2) & """ default=""true"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:TitleDisplayUnlimited>" & XMLSanitise(txtAmazonSeasonTitle.Text) & "</md:TitleDisplayUnlimited>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:TitleSort></md:TitleSort>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:ArtReference resolution=""800x800"">md:cid:" & txtAmazonSeasonID.Text & ".image." & Left(txtAmazonLanguage.Text, 2) & "</md:ArtReference>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Summary190>" & XMLSanitise(txtAmazonSeasonShortSynopsis.Text) & "</md:Summary190>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Summary4000>" & XMLSanitise(txtAmazonSeasonLongSynopsis.Text) & "</md:Summary4000>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:OriginalTitle>" & XMLSanitise(txtAmazonSeasonTitle.Text) & "</md:OriginalTitle>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Genre>" & txtAmazonGenre.Text & "</md:Genre>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:CopyrightLine>" & txtAmazonCopyrightHolder.Text & "</md:CopyrightLine>"
    Print #1, Chr(9) & Chr(9) & "</md:LocalizedInfo>"
    Print #1, Chr(9) & Chr(9) & "<md:ReleaseYear>" & Format(datOriginalAirDate.Value, "YYYY") & "</md:ReleaseYear>"
    Print #1, Chr(9) & Chr(9) & "<md:ReleaseDate>" & Format(datOriginalAirDate.Value, "YYYY-MM-DD") & "</md:ReleaseDate>"
    Print #1, Chr(9) & Chr(9) & "<md:ReleaseHistory>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:ReleaseType>original</md:ReleaseType>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:DistrTerritory>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:country>" & txtAmazonTerritory.Text & "</md:country>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:DistrTerritory>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Date>" & Format(datOriginalAirDate.Value, "YYYY-MM-DD") & "</md:Date>"
    Print #1, Chr(9) & Chr(9) & "</md:ReleaseHistory>"
    Print #1, Chr(9) & Chr(9) & "<md:WorkType>TVSeason</md:WorkType>"
    Print #1, Chr(9) & Chr(9) & "<md:RatingSet>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Rating>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:Region>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:country>" & txtAmazonTerritory.Text & "</md:country>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</md:Region>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:System>" & txtAmazonRatingType.Text & "</md:System>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:Value>" & txtAmazonRating.Text & "</md:Value>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:Description/>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Rating>"
    Print #1, Chr(9) & Chr(9) & "</md:RatingSet>"
    Print #1, Chr(9) & Chr(9) & "<md:People>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobFunction>Actor</md:JobFunction>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobDisplay>Actor</md:JobDisplay>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:BillingBlockOrder>1</md:BillingBlockOrder>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:Character>" & "Character Name" & "</md:Character>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Name>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:DisplayName>" & "Actor Name" & "</md:DisplayName>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Name>"
    Print #1, Chr(9) & Chr(9) & "</md:People>"
    Print #1, Chr(9) & Chr(9) & "<md:People>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobFunction>Writer</md:JobFunction>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobDisplay>Writer</md:JobDisplay>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:BillingBlockOrder>1</md:BillingBlockOrder>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Name>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:DisplayName>" & "Writer Name" & "</md:DisplayName>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Name>"
    Print #1, Chr(9) & Chr(9) & "</md:People>"
    Print #1, Chr(9) & Chr(9) & "<md:People>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobFunction>Director</md:JobFunction>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobDisplay>Director</md:JobDisplay>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:BillingBlockOrder>1</md:BillingBlockOrder>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Name>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:DisplayName>" & "Directot Name" & "</md:DisplayName>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Name>"
    Print #1, Chr(9) & Chr(9) & "</md:People>"
    Print #1, Chr(9) & Chr(9) & "<md:People>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobFunction>Producer</md:JobFunction>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobDisplay>Producer</md:JobDisplay>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:BillingBlockOrder>1</md:BillingBlockOrder>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Name>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:DisplayName>" & "Producer Name" & "</md:DisplayName>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Name>"
    Print #1, Chr(9) & Chr(9) & "</md:People>"
    Print #1, Chr(9) & Chr(9) & "<md:CountryOfOrigin>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:country>" & txtAmazonTerritory.Text & "</md:country>"
    Print #1, Chr(9) & Chr(9) & "</md:CountryOfOrigin>"
    Print #1, Chr(9) & Chr(9) & "<md:PrimarySpokenLanguage>" & Left(txtAmazonLanguage.Text, 2) & "</md:PrimarySpokenLanguage>"
    Print #1, Chr(9) & Chr(9) & "<md:OriginalLanguage>" & Left(txtAmazonLanguage.Text, 2) & "</md:OriginalLanguage>"
    Print #1, Chr(9) & Chr(9) & "<md:AssociatedOrg role=""Studio"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:DisplayName>" & XMLSanitise(txtXboxLicensor.Text) & "</md:DisplayName>"
    Print #1, Chr(9) & Chr(9) & "</md:AssociatedOrg>"
    Print #1, Chr(9) & Chr(9) & "<md:SequenceInfo>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Number>" & txtAmazonSeasonNumber.Text & "</md:Number>"
    Print #1, Chr(9) & Chr(9) & "</md:SequenceInfo>"
    Print #1, Chr(9) & Chr(9) & "<md:Parent relationshipType=""isseasonof"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:ParentContentID>md:cid:" & txtAmazonSeriesID.Text & ":series</md:ParentContentID>"
    Print #1, Chr(9) & Chr(9) & "</md:Parent>"
    Print #1, Chr(9) & "</mdmec:Basic>"
    Print #1, "</mdmec:CoreMetadata>"
    Close 1
    
End If

'chkPackageUsed(1).Value = 1
'cmdSave.Value = True
MsgBox "Done"

End Sub

Private Sub cmdXboxSeriesXML_Click()

Dim l_strPathToFullFile As String, l_strAltFolder As String, l_strOriginalFilename As String, l_strNewFilename As String, l_strNewFileReference As String, l_strFileNameToSave As String
Dim l_strTemp As String, l_lngTemp As Long, FSO As Scripting.FileSystemObject, l_strMasterFilename As String, l_strJPGFilename As String, l_strAudioFilename As String
Dim l_strDuration As String, l_lngRunningTime As Long, l_lngChannelCount As Long, l_strMasterReference As String


'Check Package Output for Xbox Packages only.
If cmbiTunesType.Text <> "Xbox" Then
    MsgBox "This XML Output is for 'Xbox' packages only", vbCritical, "Cannot Make XML"
    Exit Sub
End If

If txtAmazonLanguage.Text = "" Then
    txtAmazonLanguage.Text = GetDataSQL("SELECT TOP 1 LanguageCultureName FROM isoLanguageCulture WHERE LanguageCultureName LIKE '%" & txtAmazonTerritory.Text & "'")
End If

cmdSave.Value = True
Set FSO = New Scripting.FileSystemObject

'Check the compulsory info in the normal controls
If txtiTunesVendorID.Text = "" Or txtAmazonTerritory.Text = "" Or txtAmazonLanguage.Text = "" Or txtAmazonSeriesID.Text = "" _
Or txtAmazonCopyrightHolder.Text = "" Or txtAmazonSeriesName.Text = "" _
Or txtAmazonGenre.Text = "" _
Or IsNull(datOriginalAirDate.Value) _
Or txtAmazonSeriesShortSynopsis.Text = "" Or txtAmazonSeriesLongSynopsis.Text = "" _
Then
    MsgBox "Compulsory Information is has not yet been completed." & vbCrLf & "All red fields must be filled in", vbCritical, "Cannot Make XML"
    Exit Sub
End If

'Get Master Clip and do the rename of the file, and update the file record.
l_lngTemp = GetData("events", "libraryID", "eventID", txtAmazonFullClipID.Text)
l_strPathToFullFile = GetData("library", "subtitle", "libraryID", l_lngTemp)
l_strAltFolder = GetData("events", "altlocation", "eventID", txtAmazonFullClipID.Text)
If l_strAltFolder <> "" Then
    l_strPathToFullFile = l_strPathToFullFile & "\" & l_strAltFolder
End If

MDIForm1.dlgMain.InitDir = l_strPathToFullFile
MDIForm1.dlgMain.Filter = "XML files|*.xml"
MDIForm1.dlgMain.Filename = txtAmazonSeriesID.Text & "_series.xml"

MDIForm1.dlgMain.ShowSave

l_strFileNameToSave = MDIForm1.dlgMain.Filename


If l_strFileNameToSave <> "" Then

    Open l_strFileNameToSave For Output As 1
    Print #1, "<?xml version=""1.0"" encoding=""UTF-8"" standalone=""no""?>"
    Print #1, "<mdmec:CoreMetadata xmlns:mdmec=""http://www.movielabs.com/schema/mdmec/v2.4"" xmlns:md=""http://www.movielabs.com/schema/md/v2.4/md""";
    Print #1, " xmlns:xs=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">"
    Print #1, Chr(9) & "<mdmec:Basic ContentID=""md:cid:" & txtAmazonSeriesID.Text & ":series"">"
    Print #1, Chr(9) & Chr(9) & "<md:LocalizedInfo language=""" & Left(txtAmazonLanguage.Text, 2) & """ default=""true"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:TitleDisplayUnlimited>" & XMLSanitise(txtAmazonSeriesName.Text) & "</md:TitleDisplayUnlimited>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:TitleSort>" & XMLSanitise(txtAmazonSeriesName.Text) & "</md:TitleSort>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:ArtReference resolution=""800x800"">md:cid:" & txtAmazonSeriesID.Text & ".image." & Left(txtAmazonLanguage.Text, 2) & "</md:ArtReference>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Summary190>" & XMLSanitise(txtAmazonSeriesShortSynopsis.Text) & "</md:Summary190>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Summary4000>" & XMLSanitise(txtAmazonSeriesLongSynopsis.Text) & "</md:Summary4000>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:OriginalTitle>" & XMLSanitise(txtAmazonSeriesName.Text) & "</md:OriginalTitle>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Genre>" & txtAmazonGenre.Text & "</md:Genre>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:CopyrightLine>" & txtAmazonCopyrightHolder.Text & "</md:CopyrightLine>"
    Print #1, Chr(9) & Chr(9) & "</md:LocalizedInfo>"
    Print #1, Chr(9) & Chr(9) & "<md:ReleaseYear>" & Format(datOriginalAirDate.Value, "YYYY") & "</md:ReleaseYear>"
    Print #1, Chr(9) & Chr(9) & "<md:ReleaseDate>" & Format(datOriginalAirDate.Value, "YYYY-MM-DD") & "</md:ReleaseDate>"
    Print #1, Chr(9) & Chr(9) & "<md:ReleaseHistory>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:ReleaseType>original</md:ReleaseType>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:DistrTerritory>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:country>" & txtAmazonTerritory.Text & "</md:country>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:DistrTerritory>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Date>" & Format(datOriginalAirDate.Value, "YYYY-MM-DD") & "</md:Date>"
    Print #1, Chr(9) & Chr(9) & "</md:ReleaseHistory>"
    Print #1, Chr(9) & Chr(9) & "<md:WorkType>TVSeries</md:WorkType>"
    Print #1, Chr(9) & Chr(9) & "<md:RatingSet>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Rating>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:Region>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:country>" & txtAmazonTerritory.Text & "</md:country>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</md:Region>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:System>" & txtAmazonRatingType.Text & "</md:System>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:Value>" & txtAmazonRating.Text & "</md:Value>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:Description/>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Rating>"
    Print #1, Chr(9) & Chr(9) & "</md:RatingSet>"
    Print #1, Chr(9) & Chr(9) & "<md:People>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobFunction>Actor</md:JobFunction>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobDisplay>Actor</md:JobDisplay>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:BillingBlockOrder>1</md:BillingBlockOrder>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:Character>" & "Character Name" & "</md:Character>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Name>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:DisplayName>" & "Actor Name" & "</md:DisplayName>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Name>"
    Print #1, Chr(9) & Chr(9) & "</md:People>"
    Print #1, Chr(9) & Chr(9) & "<md:People>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobFunction>Writer</md:JobFunction>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobDisplay>Writer</md:JobDisplay>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:BillingBlockOrder>1</md:BillingBlockOrder>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Name>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:DisplayName>" & "Writer Name" & "</md:DisplayName>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Name>"
    Print #1, Chr(9) & Chr(9) & "</md:People>"
    Print #1, Chr(9) & Chr(9) & "<md:People>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobFunction>Director</md:JobFunction>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobDisplay>Director</md:JobDisplay>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:BillingBlockOrder>1</md:BillingBlockOrder>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Name>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:DisplayName>" & "Directot Name" & "</md:DisplayName>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Name>"
    Print #1, Chr(9) & Chr(9) & "</md:People>"
    Print #1, Chr(9) & Chr(9) & "<md:People>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobFunction>Producer</md:JobFunction>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:JobDisplay>Producer</md:JobDisplay>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:BillingBlockOrder>1</md:BillingBlockOrder>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Job>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:Name>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<md:DisplayName>" & "Producer Name" & "</md:DisplayName>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</md:Name>"
    Print #1, Chr(9) & Chr(9) & "</md:People>"
    Print #1, Chr(9) & Chr(9) & "<md:CountryOfOrigin>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:country>" & txtAmazonTerritory.Text & "</md:country>"
    Print #1, Chr(9) & Chr(9) & "</md:CountryOfOrigin>"
    Print #1, Chr(9) & Chr(9) & "<md:PrimarySpokenLanguage>" & Left(txtAmazonLanguage.Text, 2) & "</md:PrimarySpokenLanguage>"
    Print #1, Chr(9) & Chr(9) & "<md:OriginalLanguage>" & Left(txtAmazonLanguage.Text, 2) & "</md:OriginalLanguage>"
    Print #1, Chr(9) & Chr(9) & "<md:AssociatedOrg role=""Studio"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<md:DisplayName>" & XMLSanitise(txtXboxLicensor.Text) & "</md:DisplayName>"
    Print #1, Chr(9) & Chr(9) & "</md:AssociatedOrg>"
    Print #1, Chr(9) & "</mdmec:Basic>"
    Print #1, "</mdmec:CoreMetadata>"
    Close 1
    
End If

'chkPackageUsed(1).Value = 1
'cmdSave.Value = True
MsgBox "Done"

End Sub

Private Sub ddnAdvisorySystem_CloseUp()

If ddnAdvisorySystem.Columns(0).Text <> "" Then
    ddnAdvisory.RemoveAll
    PopulateCombo "iTunesAdvisory", ddnAdvisory, " AND format = '" & ddnAdvisorySystem.Columns(0).Text & "'"
    grdiTunesAdvisory.Columns("advisory").DropDownHwnd = ddnAdvisory.hWnd
End If

End Sub

Private Sub ddnGenre_CloseUp()

grdGenre.Columns("genrecode").Text = ddnGenre.Columns("genrecode").Text

End Sub

Private Sub ddnISO2A_CloseUp()

grdTVProduct.Columns("territory").Text = ddnISO2A.Columns("country").Text
grdTVProduct.Columns("territorycode").Text = ddnISO2A.Columns("code").Text

End Sub

Private Sub Form_Activate()

If Val(lblSearchPackage.Caption) <> 0 Then
    m_strSearch = "SELECT * FROM itunes_package WHERE iTUnes_packageID = " & Val(lblSearchPackage.Caption)
    adoiTunesPackage.RecordSource = m_strSearch & m_strFilter & ";"
    adoiTunesPackage.Refresh
End If

End Sub

Private Sub Form_GotFocus()

If Val(lblSearchPackage.Caption) <> 0 Then
    m_strSearch = "SELECT * FROM itunes_package WHERE iTunes_packageID = " & Val(lblSearchPackage.Caption)
    adoiTunesPackage.RecordSource = m_strSearch & m_strFilter & ";"
    adoiTunesPackage.Refresh
End If

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

l_strSQL = "SELECT name, companyID FROM company ORDER BY name;"

adoCompany.ConnectionString = g_strConnection
adoCompany.RecordSource = l_strSQL
adoCompany.Refresh

m_strSearch = "SELECT * FROM itunes_package Where 1 = 1"
'If optFilter(1).Value = True Then
'    m_strFilter = " AND video_type = 'film'"
'ElseIf optFilter(2).Value = True Then
'    m_strFilter = " AND video_type = 'tv'"
'ElseIf optFilter(3).Value = True Then
'    m_strFilter = " AND video_type = 'Maxdome Non-Episodic'"
'ElseIf optFilter(4).Value = True Then
'    m_strFilter = " AND video_type = 'tv' AND packageused = 0"
'ElseIf optFilter(5).Value = True Then
'    m_strFilter = " AND video_type = 'film' AND packageused = 0"
'Else
'    m_strFilter = ""
'End If
'l_strSQL = m_strSearch & m_strFilter & ";"
'
'adoiTunesPackage.ConnectionString = g_strConnection
'adoiTunesPackage.RecordSource = l_strSQL
'adoiTunesPackage.Refresh
'
l_strSQL = "SELECT description, information FROM xref WHERE category = 'iTunesLocale' and format = 'Y' ORDER BY information;"
adoMetadataLanguage.ConnectionString = g_strConnection
adoMetadataLanguage.RecordSource = l_strSQL
adoMetadataLanguage.Refresh

adoAmazonLocale.ConnectionString = g_strConnection
adoAmazonLocale.RecordSource = l_strSQL
adoAmazonLocale.Refresh

adoSpokenLocale.ConnectionString = g_strConnection
adoSpokenLocale.RecordSource = l_strSQL
adoSpokenLocale.Refresh

adoPreviewLocale.ConnectionString = g_strConnection
adoPreviewLocale.RecordSource = l_strSQL
adoPreviewLocale.Refresh

adoFullLocale.ConnectionString = g_strConnection
adoFullLocale.RecordSource = l_strSQL
adoFullLocale.Refresh

adoPosterLocale.ConnectionString = g_strConnection
adoPosterLocale.RecordSource = l_strSQL
adoPosterLocale.Refresh

adoDubArtistLocale.ConnectionString = g_strConnection
adoDubArtistLocale.RecordSource = l_strSQL
adoDubArtistLocale.Refresh

l_strSQL = "SELECT description, information FROM xref WHERE category = 'iTunesLocale' ORDER BY information;"
adoVideoLanguage.ConnectionString = g_strConnection
adoVideoLanguage.RecordSource = l_strSQL
adoVideoLanguage.Refresh

CenterForm Me

PopulateCombo "iTunesAdvisorySystem", ddnAdvisorySystem
PopulateCombo "iTunesAvailabilityReason", cmbCaptionsReason
PopulateCombo "iTunesProvider", cmbiTunesProvider(0)
PopulateCombo "iTunesProvider", cmbiTunesProvider(1)
PopulateCombo "iTunesSubType", cmbSubType(0), "film"
PopulateCombo "iTunesSubType", cmbSubType(1), "tv"

grdiTunesAdvisory.Columns("advisorysystem").DropDownHwnd = ddnAdvisorySystem.hWnd

adoISO2A.ConnectionString = g_strConnection
adoISO2A.RecordSource = "SELECT country, iso2acode FROM iso2a;"
adoISO2A.Refresh
grdTVProduct.Columns("territory").DropDownHwnd = ddnISO2A.hWnd
grdFilmProduct.Columns("territory").DropDownHwnd = ddnISO2A.hWnd

adoAmazonTerritory.ConnectionString = g_strConnection
adoAmazonTerritory.RecordSource = "SELECT country, iso2acode FROM iso2a;"
adoAmazonTerritory.Refresh

adoISO3166.ConnectionString = g_strConnection
adoISO3166.RecordSource = "SELECT country, iso3166code FROM iso2a;"
adoISO3166.Refresh

l_strSQL = "SELECT description AS genre, information as genrecode FROM xref WHERE category = 'itunesgenre' ORDER BY description;"
adoGenreCodes.RecordSource = l_strSQL
adoGenreCodes.ConnectionString = g_strConnection
adoGenreCodes.Refresh
grdGenre.Columns("genre").DropDownHwnd = ddnGenre.hWnd

PopulateCombo "itunesbilling", ddnBilling
grdCast.Columns("billing").DropDownHwnd = ddnBilling.hWnd
grdCrew.Columns("billing").DropDownHwnd = ddnBilling.hWnd

PopulateCombo "iTunesRole", ddnArtistRoles
grdArtistRoles1.Columns("iTunes_artist_role").DropDownHwnd = ddnArtistRoles.hWnd
grdArtistRoles2.Columns("iTunes_artist_role").DropDownHwnd = ddnArtistRoles.hWnd

PopulateCombo "iTunesFilmRole", ddnFilmRoles
grdCrew.Columns("role").DropDownHwnd = ddnFilmRoles.hWnd

PopulateCombo "iTunesCueUsage", ddnUsage
grdCueSheet.Columns("usage").DropDownHwnd = ddnUsage.hWnd

PopulateCombo "true-false", ddnTrueFalse
grdFilmProduct.Columns("clearedforsale").DropDownHwnd = ddnTrueFalse.hWnd
grdFilmProduct.Columns("clearedforhdsale").DropDownHwnd = ddnTrueFalse.hWnd
grdFilmProduct.Columns("clearedforvod").DropDownHwnd = ddnTrueFalse.hWnd
grdFilmProduct.Columns("clearedforhdvod").DropDownHwnd = ddnTrueFalse.hWnd
grdArtist1.Columns("primaryartist").DropDownHwnd = ddnTrueFalse.hWnd

PopulateCombo "iTunesvodtypes", ddnVodType
grdFilmProduct.Columns("vodtype").DropDownHwnd = ddnVodType.hWnd

cmdSearch.Value = True

End Sub

Private Sub Form_Resize()

On Error Resume Next

frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120

'tabType.Top = frmButtons.Top - tabType.Height - 120
grdItunesPackage.Height = tabType.Top - grdItunesPackage.Top - 120

End Sub

Private Sub grdArtist1_BeforeUpdate(Cancel As Integer)

If Val(lblPackageID.Caption) <> 0 Then
    grdArtist1.Columns("itunes_packageID").Text = lblPackageID.Caption
Else
    Cancel = True
End If

End Sub

Private Sub grdArtist2_BeforeUpdate(Cancel As Integer)

If Not adoCueSheet.Recordset.EOF Then
    grdArtist2.Columns("iTUnes_cuesheetID").Text = adoCueSheet.Recordset("iTunes_cuesheetID")
Else
    Cancel = True
End If

End Sub

Private Sub grdArtistRoles2_BeforeUpdate(Cancel As Integer)

If Not adoArtist2.Recordset.EOF Then
    grdArtistRoles2.Columns("iTunes_artistID").Text = adoArtist2.Recordset("iTUnes_artistID")
Else
    Cancel = True
End If

End Sub

Private Sub grdCast_AfterInsert(RtnDispErrMsg As Integer)

m_blnNoRefresh = False

End Sub

Private Sub grdCast_BeforeInsert(Cancel As Integer)

m_blnNoRefresh = True

End Sub

Private Sub grdCast_BeforeUpdate(Cancel As Integer)

If Val(lblPackageID.Caption) <> 0 Then
    grdCast.Columns("itunes_packageID").Text = lblPackageID.Caption
'    grdCast.Columns("display_name").Text = UTF8_Encode(grdCast.Columns("Decoded_name").Text)
'    grdCast.Columns("display_name2").Text = UTF8_Encode(grdCast.Columns("Decoded_name2").Text)
'    grdCast.Columns("display_name3").Text = UTF8_Encode(grdCast.Columns("Decoded_name3").Text)
'    grdCast.Columns("display_name4").Text = UTF8_Encode(grdCast.Columns("Decoded_name4").Text)
'    grdCast.Columns("description").Text = UTF8_Encode(grdCast.Columns("description_decoded").Text)
'    grdCast.Columns("description2").Text = UTF8_Encode(grdCast.Columns("description_decoded2").Text)
'    grdCast.Columns("description3").Text = UTF8_Encode(grdCast.Columns("description_decoded3").Text)
'    grdCast.Columns("description4").Text = UTF8_Encode(grdCast.Columns("description_decoded4").Text)
Else
    Cancel = True
End If

End Sub

Private Sub grdCast_RowLoaded(ByVal Bookmark As Variant)

'grdCast.Columns("decoded_name").Text = UTF8_Decode(grdCast.Columns("display_name").Text)
'grdCast.Columns("decoded_name2").Text = UTF8_Decode(grdCast.Columns("display_name2").Text)
'grdCast.Columns("decoded_name3").Text = UTF8_Decode(grdCast.Columns("display_name3").Text)
'grdCast.Columns("decoded_name4").Text = UTF8_Decode(grdCast.Columns("display_name4").Text)
'grdCast.Columns("description_decoded").Text = UTF8_Decode(grdCast.Columns("description").Text)
'grdCast.Columns("description_decoded2").Text = UTF8_Decode(grdCast.Columns("description2").Text)
'grdCast.Columns("description_decoded3").Text = UTF8_Decode(grdCast.Columns("description3").Text)
'grdCast.Columns("description_decoded4").Text = UTF8_Decode(grdCast.Columns("description4").Text)

End Sub

Private Sub grdCastCharacters_BeforeUpdate(Cancel As Integer)

If Not adoCast.Recordset.EOF Then
    grdCastCharacters.Columns("itunes_castID").Text = adoCast.Recordset("itunes_castID")
'    grdCastCharacters.Columns("character_name").Text = UTF8_Encode(grdCastCharacters.Columns("Character_name_Decoded").Text)
'    grdCastCharacters.Columns("character_name2").Text = UTF8_Encode(grdCastCharacters.Columns("Character_name_Decoded2").Text)
'    grdCastCharacters.Columns("character_name3").Text = UTF8_Encode(grdCastCharacters.Columns("Character_name_Decoded3").Text)
'    grdCastCharacters.Columns("character_name4").Text = UTF8_Encode(grdCastCharacters.Columns("Character_name_Decoded4").Text)
'    grdCastCharacters.Columns("character_note").Text = UTF8_Encode(grdCastCharacters.Columns("character_note_decoded").Text)
'    grdCastCharacters.Columns("character_note2").Text = UTF8_Encode(grdCastCharacters.Columns("character_note_decoded2").Text)
'    grdCastCharacters.Columns("character_note3").Text = UTF8_Encode(grdCastCharacters.Columns("character_note_decoded3").Text)
'    grdCastCharacters.Columns("character_note4").Text = UTF8_Encode(grdCastCharacters.Columns("character_note_decoded4").Text)
Else
    Cancel = True
End If

End Sub

Private Sub grdCastCharacters_RowLoaded(ByVal Bookmark As Variant)

'grdCastCharacters.Columns("Character_name_Decoded").Text = UTF8_Decode(grdCastCharacters.Columns("Character_name").Text)
'grdCastCharacters.Columns("Character_name_Decoded2").Text = UTF8_Decode(grdCastCharacters.Columns("Character_name2").Text)
'grdCastCharacters.Columns("Character_name_Decoded3").Text = UTF8_Decode(grdCastCharacters.Columns("Character_name3").Text)
'grdCastCharacters.Columns("Character_name_Decoded4").Text = UTF8_Decode(grdCastCharacters.Columns("Character_name4").Text)
'grdCastCharacters.Columns("character_note_decoded").Text = UTF8_Decode(grdCastCharacters.Columns("character_note").Text)
'grdCastCharacters.Columns("character_note_decoded2").Text = UTF8_Decode(grdCastCharacters.Columns("character_note2").Text)
'grdCastCharacters.Columns("character_note_decoded3").Text = UTF8_Decode(grdCastCharacters.Columns("character_note3").Text)
'grdCastCharacters.Columns("character_note_decoded4").Text = UTF8_Decode(grdCastCharacters.Columns("character_note4").Text)

End Sub

Private Sub grdChapters_BeforeUpdate(Cancel As Integer)

If Val(lblPackageID.Caption) <> 0 Then
    grdChapters.Columns("itunes_packageID").Text = lblPackageID.Caption
Else
    Cancel = True
End If

End Sub

Private Sub grdChapters_KeyPress(KeyAscii As Integer)

If grdChapters.Rows < 1 Then Exit Sub
If grdChapters.Col < 1 Then Exit Sub

If g_optUseFormattedTimeCodesInEvents = 1 Then
    If grdChapters.Columns(grdChapters.Col).Name = "starttime" Then
        If Len(grdChapters.ActiveCell.Text) < 2 Then
            If chkTimecode(3).Value = True Then
                grdChapters.ActiveCell.Text = "00:00:00;00"
            ElseIf chkTimecode(4).Value = True Then
                grdChapters.ActiveCell.Text = "00:00:00"
            Else
                grdChapters.ActiveCell.Text = "00:00:00:00"
            End If
            grdChapters.ActiveCell.SelStart = 0
            grdChapters.ActiveCell.SelLength = 1
        Else
            If chkTimecode(0).Value = True Then
                Timecode_Check_Grid grdChapters, KeyAscii, TC_24
            ElseIf chkTimecode(1).Value = True Then
                Timecode_Check_Grid grdChapters, KeyAscii, TC_25
            ElseIf chkTimecode(2).Value = True Then
                Timecode_Check_Grid grdChapters, KeyAscii, TC_30
            ElseIf chkTimecode(3).Value = True Then
                Timecode_Check_Grid grdChapters, KeyAscii, TC_29
            ElseIf chkTimecode(4).Value = True Then
                Timecode_Check_Grid grdChapters, KeyAscii, TC_QT
            End If
        End If
    End If
End If

End Sub

Private Sub grdCrew_BeforeUpdate(Cancel As Integer)

If Val(lblPackageID.Caption) <> 0 Then
    grdCrew.Columns("itunes_packageID").Text = lblPackageID.Caption
'    grdCrew.Columns("display_name").Text = UTF8_Encode(grdCrew.Columns("decoded_name").Text)
'    grdCrew.Columns("display_name2").Text = UTF8_Encode(grdCrew.Columns("decoded_name2").Text)
'    grdCrew.Columns("display_name3").Text = UTF8_Encode(grdCrew.Columns("decoded_name3").Text)
'    grdCrew.Columns("display_name4").Text = UTF8_Encode(grdCrew.Columns("decoded_name4").Text)
Else
    Cancel = True
End If

End Sub

Private Sub grdCrew_RowLoaded(ByVal Bookmark As Variant)

'grdCrew.Columns("Decoded_name").Text = UTF8_Decode(grdCrew.Columns("display_name").Text)
'grdCrew.Columns("Decoded_name2").Text = UTF8_Decode(grdCrew.Columns("display_name2").Text)
'grdCrew.Columns("Decoded_name3").Text = UTF8_Decode(grdCrew.Columns("display_name3").Text)
'grdCrew.Columns("Decoded_name4").Text = UTF8_Decode(grdCrew.Columns("display_name4").Text)

End Sub

Private Sub grdCrewRole_BeforeUpdate(Cancel As Integer)

grdCrewRole.Columns("itunes_crewID").Text = grdCrew.Columns("itunes_crewID").Text

End Sub

Private Sub grdCueSheet_BeforeUpdate(Cancel As Integer)

grdCueSheet.Columns("iTunes_PackageID").Text = Val(lblPackageID.Caption)

End Sub

Private Sub grdFilmProduct_BeforeUpdate(Cancel As Integer)

grdFilmProduct.Columns("packageID").Text = Val(lblPackageID.Caption)

End Sub

Private Sub grdFilmProduct_BtnClick()

If grdFilmProduct.Columns(grdFilmProduct.Col).Name = "salesstartdate" Or grdFilmProduct.Columns(grdFilmProduct.Col).Name = "salesenddate" _
Or grdFilmProduct.Columns(grdFilmProduct.Col).Name = "physicalreleasedate" Or grdFilmProduct.Columns(grdFilmProduct.Col).Name = "preordersalesstartdate" _
Or grdFilmProduct.Columns(grdFilmProduct.Col).Name = "availableforvoddate" Or grdFilmProduct.Columns(grdFilmProduct.Col).Name = "unavailableforvoddate" Then
    If IsDate(grdFilmProduct.Columns(grdFilmProduct.Col).Text) Then
        grdFilmProduct.Columns(grdFilmProduct.Col).Text = Format(GetDate(grdFilmProduct.Columns(grdFilmProduct.Col).Text), "dd mmm yyyy")
    Else
        grdFilmProduct.Columns(grdFilmProduct.Col).Text = Format(GetDate(Now), "dd mmm yyyy")
    End If
End If

End Sub

Private Sub grdFilmProduct_RowLoaded(ByVal Bookmark As Variant)

grdFilmProduct.Columns("territory").Text = GetData("iso2a", "country", "iso2acode", grdFilmProduct.Columns("territorycode").Text)

End Sub

Private Sub grdGenre_BeforeUpdate(Cancel As Integer)

If Val(lblPackageID.Caption) <> 0 Then
    grdGenre.Columns("itunes_packageID").Text = lblPackageID.Caption
Else
    Cancel = True
End If

End Sub

Private Sub grdiTunesAdvisory_BeforeUpdate(Cancel As Integer)

If lblPackageID.Caption <> "" Then
    grdiTunesAdvisory.Columns("itunes_packageID").Text = lblPackageID.Caption
Else
    Cancel = True
End If

End Sub

Private Sub grdItunesPackage_AfterDelete(RtnDispErrMsg As Integer)

ClearFields Me
cmdSearch.Value = True

End Sub

Private Sub grdItunesPackage_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

Dim l_int As Integer
l_int = MsgBox("Are you sure you want to delete the selected rows?", vbQuestion + vbOKCancel)

If l_int = vbCancel Then
    Cancel = True
    Exit Sub
End If

Dim nTotalSelRows   As Integer
Dim i               As Integer
Dim bkmrk           As Variant ' Bookmarks are always defined as variants
Dim l_lngPackageID  As Long
Dim l_strSQL        As String


'get the total count of selected rows
nTotalSelRows = grdItunesPackage.SelBookmarks.Count - 1

'loop through each selected row
For i = 0 To nTotalSelRows
   
    'get the current row's bookmark
    bkmrk = grdItunesPackage.SelBookmarks(i)
    
    'get the job ID of the current selected row
    l_lngPackageID = grdItunesPackage.Columns("itunes_packageID").CellText(bkmrk)
    
    If l_lngPackageID <> 0 Then
        l_strSQL = "DELETE FROM itunes_cast WHERE itunes_packageID = " & l_lngPackageID
        ExecuteSQL l_strSQL, g_strExecuteError
        l_strSQL = "DELETE FROM itunes_chapter WHERE itunes_packageID = " & l_lngPackageID
        ExecuteSQL l_strSQL, g_strExecuteError
        l_strSQL = "DELETE FROM itunes_clip_advisory WHERE itunes_packageID = " & l_lngPackageID
        ExecuteSQL l_strSQL, g_strExecuteError
        l_strSQL = "DELETE FROM itunes_crew WHERE itunes_packageID = " & l_lngPackageID
        ExecuteSQL l_strSQL, g_strExecuteError
        l_strSQL = "DELETE FROM itunes_genre WHERE itunes_packageID = " & l_lngPackageID
        ExecuteSQL l_strSQL, g_strExecuteError
        l_strSQL = "DELETE FROM itunes_product WHERE itunes_packageID = " & l_lngPackageID
        ExecuteSQL l_strSQL, g_strExecuteError
    End If
    'add something to the job history to say it was applied from group edit
    l_strSQL = "DELETE FROM itunes_package WHERE itunes_packageID = '" & l_lngPackageID & "';"
    
    ExecuteSQL CStr(l_strSQL), g_strExecuteError
    CheckForSQLError
    

    DoEvents
    
Next i

adoiTunesPackage.Refresh

Cancel = True

End Sub

Private Sub grdItunesPackage_BeforeRowColChange(Cancel As Integer)

If CheckIfDataChanged = True Then
    If MsgBox("Data appears to have changed - Save the existing record?", vbYesNo, "Have you Saved...") = vbYes Then
        cmdSave.Value = True
    End If
End If

End Sub

Private Sub grdItunesPackage_RowLoaded(ByVal Bookmark As Variant)

grdItunesPackage.Columns("Company").Text = GetData("company", "name", "companyID", Val(grdItunesPackage.Columns("companyID").Text))
'grdItunesPackage.Columns("video_title").Text = UTF8_Decode(grdItunesPackage.Columns("video_title").Text)

End Sub

Private Sub grdTVProduct_BeforeUpdate(Cancel As Integer)

grdTVProduct.Columns("packageID").Text = Val(lblPackageID.Caption)

End Sub

Private Sub grdTVProduct_BtnClick()

If grdTVProduct.Columns(grdTVProduct.Col).Name = "salesstartdate" Then
    If IsDate(grdTVProduct.Columns("salesstartdate").Text) Then
        grdTVProduct.Columns("salesstartdate").Text = Format(GetDate(grdTVProduct.Columns("salesstartdate").Text), "dd mmm yyyy")
    Else
        grdTVProduct.Columns("salesstartdate").Text = Format(GetDate(Now), "dd mmm yyyy")
    End If
End If


End Sub

Private Sub grdTVProduct_RowLoaded(ByVal Bookmark As Variant)

grdTVProduct.Columns("territory").Text = GetData("iso2a", "country", "iso2acode", grdTVProduct.Columns("territorycode").Text)

End Sub

Private Sub grdArtistRoles1_BeforeUpdate(Cancel As Integer)

If Not adoArtist1.Recordset.EOF Then
    grdArtistRoles1.Columns("itunes_artistID").Text = Val(adoArtist1.Recordset("iTunes_artistID"))
Else
    Cancel = True
End If

End Sub

Private Sub tabFilm_Click(PreviousTab As Integer)

Select Case tabFilm.Tab

    Case 2
        If chkCustomChapterTitles.Value <> 0 Then
            grdChapters.Columns("Title1").Visible = True
            grdChapters.Columns("Title2").Visible = True
            grdChapters.Columns("Title3").Visible = True
            grdChapters.Columns("Title4").Visible = True
        Else
            grdChapters.Columns("Title1").Visible = False
            grdChapters.Columns("Title2").Visible = False
            grdChapters.Columns("Title3").Visible = False
            grdChapters.Columns("Title4").Visible = False
        End If
    
    Case 3, 4
        tabFilm.Tab = PreviousTab
    
    Case Else
        'do nothing
        
End Select

End Sub

Private Sub txtAudioClipID_DblClick(Index As Integer)
If Val(txtAudioClipID(Index).Text) <> 0 Then ShowClipControl Val(txtAudioClipID(Index).Text)
End Sub

Private Sub txtFullClipID_DblClick()
If Val(txtFullClipID.Text) <> 0 Then ShowClipControl Val(txtFullClipID.Text)
End Sub

Private Sub txtMetaDataLanguage_GotFocus()
HighLite txtMetaDataLanguage
End Sub

Private Sub txtOriginalSpokenLocale_GotFocus()
HighLite txtOriginalSpokenLocale
End Sub

Private Sub txtPosterClipID_DblClick(Index As Integer)
If Val(txtPosterClipID(Index).Text) <> 0 Then ShowClipControl Val(txtPosterClipID(Index).Text)
End Sub

Private Sub txtPreviewClipID_DblClick(Index As Integer)
If Val(txtPreviewClipID(Index).Text) <> 0 Then ShowClipControl Val(txtPreviewClipID(Index).Text)
End Sub

Private Sub txtQCClipID_DblClick(Index As Integer)
If Val(txtQCClipID(Index).Text) <> 0 Then ShowClipControl Val(txtQCClipID(Index).Text)
End Sub

Private Sub txtSubtitlesClipID_DblClick(Index As Integer)
If Val(txtSubtitlesClipID(Index).Text) <> 0 Then ShowClipControl Val(txtSubtitlesClipID(Index).Text)
End Sub

Private Sub txtVideoLanguage_GotFocus()
HighLite txtVideoLanguage
End Sub

Sub WriteiTunesFilmRatings()

    'Ratings Information
    If cmbUKfilm.Text <> "" Or cmbNOfilm.Text <> "" Or cmbSEfilm.Text <> "" Or cmbDENfilm.Text <> "" Or cmbFIfilm.Text <> "" Or cmbUSfilm.Text <> "" _
    Or cmbFRfilm.Text <> "" Or cmbDEfilm.Text <> "" Or cmbGRfilm.Text <> "" Or cmbIEfilm.Text <> "" Or cmbITfilm.Text <> "" Or cmbJPfilm.Text <> "" _
    Or cmbLUfilm.Text <> "" Or cmbMXfilm.Text <> "" Or cmbNLfilm.Text <> "" Or cmbNZfilm.Text <> "" Or cmbBEfilm.Text <> "" Or cmbAUfilm.Text <> "" _
    Or cmbCAfilm.Text <> "" Or cmbPTfilm.Text <> "" Or cmbQBfilm.Text <> "" Or cmbESfilm.Text <> "" Or cmbCHfilm.Text <> "" Or cmbAUSfilm.Text <> "" _
    Then
        Print #1, Chr(9) & Chr(9) & "<ratings>"
        If cmbAUfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""au-oflc""";
            If txtAUreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtAUreason.Text) & """";
            Print #1, " code=""" & cmbAUfilm.Text & """/>"
        End If
        If cmbAUSfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""fsk""";
            If txtAUSreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtAUSreason.Text) & """";
            Print #1, " code=""" & cmbAUSfilm.Text & """/>"
        End If
        If cmbBEfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""be-movies""";
            If txtBEreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtBEreason.Text) & """";
            Print #1, " code=""" & cmbBEfilm.Text & """/>"
        End If
        If cmbCAfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""ca-chvs""";
            If txtCAreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtCAreason.Text) & """";
            Print #1, " code=""" & cmbCAfilm.Text & """/>"
        End If
        If cmbDENfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""dk-movies""";
            If txtDENreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtDENreason.Text) & """";
            Print #1, " code=""" & cmbDENfilm.Text & """/>"
        End If
        If cmbFIfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""fi-movies""";
            If txtFIreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtFIreason.Text) & """";
            Print #1, " code=""" & cmbFIfilm.Text & """/>"
        End If
        If cmbFRfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""fr-cnc""";
            If txtFRreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtFRreason.Text) & """";
            Print #1, " code=""" & cmbFRfilm.Text & """/>"
        End If
        If cmbDEfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""de-fsk""";
            If txtDEreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtDEreason.Text) & """";
            Print #1, " code=""" & cmbDEfilm.Text & """/>"
        End If
        If cmbGRfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""gr-movies""";
            If txtGRreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtGRreason.Text) & """";
            Print #1, " code=""" & cmbGRfilm.Text & """/>"
        End If
        If cmbIEfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""ie-ifco""";
            If txtIEreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtIEreason.Text) & """";
            Print #1, " code=""" & cmbIEfilm.Text & """/>"
        End If
        If cmbITfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""it-movies""";
            If txtITreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtITreason.Text) & """";
            Print #1, " code=""" & cmbITfilm.Text & """/>"
        End If
        If cmbJPfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""jp-eirin""";
            If txtJPreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtJPreason.Text) & """";
            Print #1, " code=""" & cmbJPfilm.Text & """/>"
        End If
        If cmbLUfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""lu-movies""";
            If txtLUreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtLUreason.Text) & """";
            Print #1, " code=""" & cmbLUfilm.Text & """/>"
        End If
        If cmbMXfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""mx-movies""";
            If txtMXreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtMXreason.Text) & """";
            Print #1, " code=""" & cmbMXfilm.Text & """/>"
        End If
        If cmbNLfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""nl-movies""";
            If txtNLreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtNLreason.Text) & """";
            Print #1, " code=""" & cmbNLfilm.Text & """/>"
        End If
        If cmbNZfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""nz-oflc""";
            If txtNZreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtNZreason.Text) & """";
            Print #1, " code=""" & cmbNZfilm.Text & """/>"
        End If
        If cmbNOfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""no-movies""";
            If txtNOreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtNOreason.Text) & """";
            Print #1, " code=""" & cmbNOfilm.Text & """/>"
        End If
        If cmbPTfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""pt-movies""";
            If txtPTreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtPTreason.Text) & """";
            Print #1, " code=""" & cmbPTfilm.Text & """/>"
        End If
        If cmbQBfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""frca-movies""";
            If txtQBreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtQBreason.Text) & """";
            Print #1, " code=""" & cmbQBfilm.Text & """/>"
        End If
        If cmbESfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""es-movies""";
            If txtESreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtESreason.Text) & """";
            Print #1, " code=""" & cmbESfilm.Text & """/>"
        End If
        If cmbSEfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""se-movies""";
            If txtSEreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtSEreason.Text) & """";
            Print #1, " code=""" & cmbSEfilm.Text & """/>"
        End If
        If cmbCHfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""ch-movies""";
            If txtCHreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtCHreason.Text) & """";
            Print #1, " code=""" & cmbCHfilm.Text & """/>"
        End If
        If cmbUKfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""bbfc""";
            If txtUKreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtUKreason.Text) & """";
            Print #1, " code=""" & cmbUKfilm.Text & """/>"
        End If
        If cmbUSfilm.Text <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<rating system=""mpaa""";
            If txtUSreason.Text <> "" Then Print #1, " reason=""" & XMLSanitise(txtUSreason.Text) & """";
            Print #1, " code=""" & cmbUSfilm.Text & """/>"
        End If
        Print #1, Chr(9) & Chr(9) & "</ratings>"
    End If
    
End Sub

Function WriteFilmMainAsset(Optional lp_blnAssetOnly As Boolean, Optional lp_blnFeatureWithChapters As Boolean) As Boolean

    Dim TabVariable As Integer
    
    WriteFilmMainAsset = True
    
    If lp_blnAssetOnly = True Then
        TabVariable = -1
    Else
        TabVariable = 0
    End If
'<attribute name="image.burned_subtitles.locale">
    'Main feature
    WriteTabs TabVariable + 3
    Print #1, "<asset type=""full"">"
    WriteTabs TabVariable + 4
    Print #1, "<data_file role=""source"">"
    WriteTabs TabVariable + 5
    Print #1, "<locale name=""" & cmbFullLocale(0).Text & """/>"
    WriteTabs TabVariable + 5
    Print #1, "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", txtFullClipID.Text)) & "</file_name>"
    WriteTabs TabVariable + 5
    Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", txtFullClipID.Text) & "</size>"
    WriteTabs TabVariable + 5
    Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", txtFullClipID.Text) & "</checksum>"
    If Not EmptyCheck(txtFullCropTop.Text) Then
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""crop.top"">" & txtFullCropTop.Text & "</attribute>"
    Else
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""crop.top"">0</attribute>"
    End If
    If Not EmptyCheck(txtFullCropBottom.Text) Then
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""crop.bottom"">" & txtFullCropBottom.Text & "</attribute>"
    Else
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""crop.bottom"">0</attribute>"
    End If
    If Not EmptyCheck(txtFullCropLeft.Text) Then
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""crop.left"">" & txtFullCropLeft.Text & "</attribute>"
    Else
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""crop.left"">0</attribute>"
    End If
    If Not EmptyCheck(txtFullCropRight.Text) Then
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""crop.right"">" & txtFullCropRight.Text & "</attribute>"
    Else
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""crop.right"">0</attribute>"
    End If
    If chkFeatureTextless.Value = 0 Then
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""image.textless_master"">false</attribute>"
    Else
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""image.textless_master"">true</attribute>"
    End If
    If cmbForcedNarrativeLocale(0).Text <> "" Then
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""image.burned_forced_narrative.locale"">" & cmbForcedNarrativeLocale(0).Text & "</attribute>"
    End If
    If cmbForcedNarrativeLocale(5).Text <> "" Then
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""image.burned_subtitles.locale"">" & cmbForcedNarrativeLocale(5).Text & "</attribute>"
    End If
    WriteTabs TabVariable + 4
    Print #1, "</data_file>"
    If chkCaptionsAvailability.Value <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""captions"">"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtCaptionsClipID.Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtCaptionsClipID.Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtCaptionsClipID.Text)) & "</checksum>"
        If cmbFullLocale(1).Text <> "" Then
            WriteTabs TabVariable + 5
            Print #1, "<locale name=""" & cmbFullLocale(1).Text & """/>"
        Else
            MsgBox "Locale is compulsory for the captions file if there are captions present.", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    
    If lp_blnAssetOnly = True And lp_blnFeatureWithChapters = True Then
        WriteFilmChapterInfo True, True
    End If
        
    'Additional Audio Files
    If Val(txtAudioClipID(0).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""audio"">"
        If cmbAudioLocale(0).Text = "" Then
            MsgBox "Compulsory Information not complete - Audio Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbAudioLocale(0).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtAudioClipID(0).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtAudioClipID(0).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtAudioClipID(0).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    If Val(txtAudioClipID(1).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""audio"">"
        If cmbAudioLocale(1).Text = "" Then
            MsgBox "Compulsory Information not complete - Audio Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbAudioLocale(1).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtAudioClipID(1).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtAudioClipID(1).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtAudioClipID(1).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    If Val(txtAudioClipID(2).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""audio"">"
        If cmbAudioLocale(2).Text = "" Then
            MsgBox "Compulsory Information not complete - Audio Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbAudioLocale(2).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtAudioClipID(2).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtAudioClipID(2).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtAudioClipID(2).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    If Val(txtAudioClipID(3).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""audio.visually_impaired"">"
        If cmbAudioLocale(3).Text = "" Then
            MsgBox "Compulsory Information not complete - Audio Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbAudioLocale(3).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtAudioClipID(3).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtAudioClipID(3).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtAudioClipID(3).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    If Val(txtAudioClipID(4).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""audio.visually_impaired"">"
        If cmbAudioLocale(4).Text = "" Then
            MsgBox "Compulsory Information not complete - Audio Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbAudioLocale(4).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtAudioClipID(4).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtAudioClipID(4).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtAudioClipID(4).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    If Val(txtAudioClipID(5).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""audio.visually_impaired"">"
        If cmbAudioLocale(5).Text = "" Then
            MsgBox "Compulsory Information not complete - Audio Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbAudioLocale(5).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtAudioClipID(5).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtAudioClipID(5).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtAudioClipID(5).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    If Val(txtAudioClipID(6).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""audio.visually_impaired"">"
        If cmbAudioLocale(6).Text = "" Then
            MsgBox "Compulsory Information not complete - Audio Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbAudioLocale(6).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtAudioClipID(6).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtAudioClipID(6).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtAudioClipID(6).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    
    'Subtitles
    If Val(txtSubtitlesClipID(0).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""subtitles"">"
        If cmbSubtitlesLocale(0).Text = "" Then
            MsgBox "Compulsory Information not complete - Subtitle Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbSubtitlesLocale(0).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtSubtitlesClipID(0).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtSubtitlesClipID(0).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtSubtitlesClipID(0).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    If Val(txtSubtitlesClipID(1).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""subtitles"">"
        If cmbSubtitlesLocale(1).Text = "" Then
            MsgBox "Compulsory Information not complete - Subtitle Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbSubtitlesLocale(1).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtSubtitlesClipID(1).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtSubtitlesClipID(1).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtSubtitlesClipID(1).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    If Val(txtSubtitlesClipID(2).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""subtitles"">"
        If cmbSubtitlesLocale(2).Text = "" Then
            MsgBox "Compulsory Information not complete - Subtitle Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbSubtitlesLocale(2).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtSubtitlesClipID(2).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtSubtitlesClipID(2).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtSubtitlesClipID(2).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    If Val(txtSubtitlesClipID(3).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""subtitles"">"
        If cmbSubtitlesLocale(3).Text = "" Then
            MsgBox "Compulsory Information not complete - Subtitle Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbSubtitlesLocale(3).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtSubtitlesClipID(3).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtSubtitlesClipID(3).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtSubtitlesClipID(3).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    
    'Subtitles Hard of Hearing
    If Val(txtSubtitlesClipID(8).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""subtitles.hearing_impaired"">"
        If cmbSubtitlesLocale(7).Text = "" Then
            MsgBox "Compulsory Information not complete - Subtitle Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbSubtitlesLocale(7).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtSubtitlesClipID(8).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtSubtitlesClipID(8).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtSubtitlesClipID(8).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    If Val(txtSubtitlesClipID(9).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<data_file role=""subtitles.hearing_impaired"">"
        If cmbSubtitlesLocale(8).Text = "" Then
            MsgBox "Compulsory Information not complete - Subtitle Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbSubtitlesLocale(8).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtSubtitlesClipID(9).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtSubtitlesClipID(9).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtSubtitlesClipID(9).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    If Val(txtSubtitlesClipID(10).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""subtitles.hearing_impaired"">"
        If cmbSubtitlesLocale(9).Text = "" Then
            MsgBox "Compulsory Information not complete - Subtitle Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbSubtitlesLocale(9).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtSubtitlesClipID(10).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtSubtitlesClipID(10).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtSubtitlesClipID(10).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    If Val(txtSubtitlesClipID(11).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""subtitles.hearing_impaired"">"
        If cmbSubtitlesLocale(10).Text = "" Then
            MsgBox "Compulsory Information not complete - Subtitle Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbSubtitlesLocale(10).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtSubtitlesClipID(11).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtSubtitlesClipID(11).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtSubtitlesClipID(11).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    
    'Subtitles Forced Narratives
    If Val(txtSubtitlesClipID(12).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""forced_subtitles"">"
        If cmbSubtitlesLocale(11).Text = "" Then
            MsgBox "Compulsory Information not complete - Subtitle Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbSubtitlesLocale(11).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtSubtitlesClipID(12).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtSubtitlesClipID(12).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtSubtitlesClipID(12).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    If Val(txtSubtitlesClipID(13).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""forced_subtitles"">"
        If cmbSubtitlesLocale(12).Text = "" Then
            MsgBox "Compulsory Information not complete - Subtitle Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbSubtitlesLocale(12).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtSubtitlesClipID(13).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtSubtitlesClipID(13).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtSubtitlesClipID(13).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    If Val(txtSubtitlesClipID(14).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""forced_subtitles"">"
        If cmbSubtitlesLocale(13).Text = "" Then
            MsgBox "Compulsory Information not complete - Subtitle Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbSubtitlesLocale(13).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtSubtitlesClipID(14).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtSubtitlesClipID(14).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtSubtitlesClipID(14).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    If Val(txtSubtitlesClipID(15).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<data_file role=""forced_subtitles"">"
        If cmbSubtitlesLocale(14).Text = "" Then
            MsgBox "Compulsory Information not complete - Subtitle Locales", vbCritical, "Error..."
            WriteFilmMainAsset = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbSubtitlesLocale(14).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtSubtitlesClipID(15).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtSubtitlesClipID(15).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtSubtitlesClipID(15).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    
    'QC Report
    If Val(txtQCClipID(0).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""notes"">"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtQCClipID(0).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtQCClipID(0).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtQCClipID(0).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    WriteTabs TabVariable + 3
    Print #1, "</asset>"
    
End Function

Function WriteFilmPreviewAssets(Optional lp_blnAssetOnly As Boolean) As Boolean

    Dim TabVariable As Integer
    
    WriteFilmPreviewAssets = True
    
    If lp_blnAssetOnly = True Then
        TabVariable = -1
    Else
        TabVariable = 0
    End If
    
    'Preview Clip - first one is compulsory - up to 4 of them
    WriteTabs TabVariable + 3
    Print #1, "<asset type=""preview"">"
    WriteTabs TabVariable + 4
    Print #1, "<territories>"
    WriteTabs TabVariable + 5
    Print #1, "<territory>WW</territory>"
    WriteTabs TabVariable + 4
    Print #1, "</territories>"
    WriteTabs TabVariable + 4
    Print #1, "<data_file role=""source"">"
    WriteTabs TabVariable + 5
    Print #1, "<locale name=""" & cmbPreviewLocale(0).Text & """/>"
    WriteTabs TabVariable + 5
    Print #1, "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", txtPreviewClipID(0).Text)) & "</file_name>"
    WriteTabs TabVariable + 5
    Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", txtPreviewClipID(0).Text) & "</size>"
    WriteTabs TabVariable + 5
    Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", txtPreviewClipID(0).Text) & "</checksum>"
    If Not EmptyCheck(txtPreviewCropTop.Text) Then
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""crop.top"">" & txtPreviewCropTop.Text & "</attribute>"
    Else
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""crop.top"">0</attribute>"
    End If
    If Not EmptyCheck(txtPreviewCropBottom.Text) Then
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""crop.bottom"">" & txtPreviewCropBottom.Text & "</attribute>"
    Else
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""crop.bottom"">0</attribute>"
    End If
    If Not EmptyCheck(txtPreviewCropLeft.Text) Then
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""crop.left"">" & txtPreviewCropLeft.Text & "</attribute>"
    Else
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""crop.left"">0</attribute>"
    End If
    If Not EmptyCheck(txtPreviewCropRight.Text) Then
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""crop.right"">" & txtPreviewCropRight.Text & "</attribute>"
    Else
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""crop.right"">0</attribute>"
    End If
    If cmbForcedNarrativeLocale(1).Text <> "" Then
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""image.burned_forced_narrative.locale"">" & cmbForcedNarrativeLocale(1).Text & "</attribute>"
    End If
    If cmbForcedNarrativeLocale(6).Text <> "" Then
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""image.burned_subtitles.locale"">" & cmbForcedNarrativeLocale(6).Text & "</attribute>"
    End If
    If cmbForcedNarrativeLocale(1).Text <> "" And cmbForcedNarrativeLocale(6).Text <> "" Then
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""image.textless_master"">true</attribute>"
    Else
        WriteTabs TabVariable + 5
        Print #1, "<attribute name=""image.textless_master"">false</attribute>"
    End If
    WriteTabs TabVariable + 4
    Print #1, "</data_file>"
    
    'QC Report
    If Val(txtQCClipID(1).Text) <> 0 Then
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""notes"">"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtQCClipID(1).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtQCClipID(1).Text)) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtQCClipID(1).Text)) & "</checksum>"
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
    End If
    
    WriteTabs TabVariable + 3
    Print #1, "</asset>"
    If Val(txtPreviewClipID(1).Text) <> 0 Then
        WriteTabs TabVariable + 3
        Print #1, "<asset type=""preview"">"
        If cmbPreviewTerritory(1).Text = "" Then
            MsgBox "Compulsory Information not complete - Preview Territories", vbCritical, "Error..."
            WriteFilmPreviewAssets = False
            Exit Function
        End If
        WriteTabs TabVariable + 4
        Print #1, "<territories>"
        WriteTabs TabVariable + 5
        Print #1, "<territory>" & cmbPreviewTerritory(1).Text & "</territory>"
        WriteTabs TabVariable + 4
        Print #1, "</territories>"
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""source"">"
        If cmbPreviewLocale(1).Text = "" Then
            MsgBox "Compulsory Information not complete - Preview Locales", vbCritical, "Error..."
            WriteFilmPreviewAssets = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbPreviewLocale(1).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", txtPreviewClipID(1).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", txtPreviewClipID(1).Text) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", txtPreviewClipID(1).Text) & "</checksum>"
        If Not EmptyCheck(txtPreviewCropTop.Text) Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.top"">" & txtPreviewCropTop.Text & "</attribute>"
        Else
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.top"">0</attribute>"
        End If
        If Not EmptyCheck(txtPreviewCropBottom.Text) Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.bottom"">" & txtPreviewCropBottom.Text & "</attribute>"
        Else
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.bottom"">0</attribute>"
        End If
        If Not EmptyCheck(txtPreviewCropLeft.Text) Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.left"">" & txtPreviewCropLeft.Text & "</attribute>"
        Else
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.left"">0</attribute>"
        End If
        If Not EmptyCheck(txtPreviewCropRight.Text) Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.right"">" & txtPreviewCropRight.Text & "</attribute>"
        Else
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.right"">0</attribute>"
        End If
        If cmbForcedNarrativeLocale(2).Text <> "" Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""image.burned_forced_narrative.locale"">" & cmbForcedNarrativeLocale(2).Text & "</attribute>"
        End If
        If cmbForcedNarrativeLocale(7).Text <> "" Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""image.burned_subtitles.locale"">" & cmbForcedNarrativeLocale(7).Text & "</attribute>"
        End If
        If cmbForcedNarrativeLocale(2).Text <> "" And cmbForcedNarrativeLocale(7).Text <> "" Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""image.textless_master"">true</attribute>"
        Else
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""image.textless_master"">false</attribute>"
        End If
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
        'QC Report
        If Val(txtQCClipID(2).Text) <> 0 Then
            WriteTabs TabVariable + 4
            Print #1, "<data_file role=""notes"">"
            WriteTabs TabVariable + 5
            Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtQCClipID(2).Text)) & "</file_name>"
            WriteTabs TabVariable + 5
            Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtQCClipID(2).Text)) & "</size>"
            WriteTabs TabVariable + 5
            Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtQCClipID(2).Text)) & "</checksum>"
            WriteTabs TabVariable + 4
            Print #1, "</data_file>"
        End If
        WriteTabs TabVariable + 3
        Print #1, "</asset>"
    End If
    If Val(txtPreviewClipID(2).Text) <> 0 Then
        WriteTabs TabVariable + 3
        Print #1, "<asset type=""preview"">"
        If cmbPreviewTerritory(2).Text = "" Then
            MsgBox "Compulsory Information not complete - Preview Territories", vbCritical, "Error..."
            WriteFilmPreviewAssets = False
            Exit Function
        End If
        WriteTabs TabVariable + 4
        Print #1, "<territories>"
        WriteTabs TabVariable + 5
        Print #1, "<territory>" & cmbPreviewTerritory(2).Text & "</territory>"
        WriteTabs TabVariable + 4
        Print #1, "</territories>"
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""source"">"
        If cmbPreviewLocale(2).Text = "" Then
            MsgBox "Compulsory Information not complete - Preview Locales", vbCritical, "Error..."
            WriteFilmPreviewAssets = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbPreviewLocale(2).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", txtPreviewClipID(2).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", txtPreviewClipID(2).Text) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", txtPreviewClipID(2).Text) & "</checksum>"
        If Not EmptyCheck(txtPreviewCropTop.Text) Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.top"">" & txtPreviewCropTop.Text & "</attribute>"
        Else
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.top"">0</attribute>"
        End If
        If Not EmptyCheck(txtPreviewCropBottom.Text) Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.bottom"">" & txtPreviewCropBottom.Text & "</attribute>"
        Else
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.bottom"">0</attribute>"
        End If
        If Not EmptyCheck(txtPreviewCropLeft.Text) Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.left"">" & txtPreviewCropLeft.Text & "</attribute>"
        Else
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.left"">0</attribute>"
        End If
        If Not EmptyCheck(txtPreviewCropRight.Text) Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.right"">" & txtPreviewCropRight.Text & "</attribute>"
        Else
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.right"">0</attribute>"
        End If
        If cmbForcedNarrativeLocale(3).Text <> "" Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""image.burned_forced_narrative.locale"">" & cmbForcedNarrativeLocale(3).Text & "</attribute>"
        End If
        If cmbForcedNarrativeLocale(8).Text <> "" Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""image.burned_subtitles.locale"">" & cmbForcedNarrativeLocale(8).Text & "</attribute>"
        End If
        If cmbForcedNarrativeLocale(3).Text <> "" And cmbForcedNarrativeLocale(8).Text <> "" Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""image.textless_master"">true</attribute>"
        Else
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""image.textless_master"">false</attribute>"
        End If
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
        'QC Report
        If Val(txtQCClipID(3).Text) <> 0 Then
            WriteTabs TabVariable + 4
            Print #1, "<data_file role=""notes"">"
            WriteTabs TabVariable + 5
            Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtQCClipID(3).Text)) & "</file_name>"
            WriteTabs TabVariable + 5
            Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtQCClipID(3).Text)) & "</size>"
            WriteTabs TabVariable + 5
            Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtQCClipID(3).Text)) & "</checksum>"
            WriteTabs TabVariable + 4
            Print #1, "</data_file>"
        End If
        WriteTabs TabVariable + 3
        Print #1, "</asset>"
    End If
    If Val(txtPreviewClipID(3).Text) <> 0 Then
        WriteTabs TabVariable + 3
        Print #1, "<asset type=""preview"">"
        If cmbPreviewTerritory(3).Text = "" Then
            MsgBox "Compulsory Information not complete - Preview Territories", vbCritical, "Error..."
            WriteFilmPreviewAssets = False
            Exit Function
        End If
        WriteTabs TabVariable + 4
        Print #1, "<territories>"
        WriteTabs TabVariable + 5
        Print #1, "<territory>" & cmbPreviewTerritory(3).Text & "</territory>"
        WriteTabs TabVariable + 4
        Print #1, "</territories>"
        WriteTabs TabVariable + 4
        Print #1, "<data_file role=""source"">"
        If cmbPreviewLocale(3).Text = "" Then
            MsgBox "Compulsory Information not complete - Preview Locales", vbCritical, "Error..."
            WriteFilmPreviewAssets = False
            Exit Function
        End If
        WriteTabs TabVariable + 5
        Print #1, "<locale name=""" & cmbPreviewLocale(3).Text & """/>"
        WriteTabs TabVariable + 5
        Print #1, "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", txtPreviewClipID(3).Text)) & "</file_name>"
        WriteTabs TabVariable + 5
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", txtPreviewClipID(3).Text) & "</size>"
        WriteTabs TabVariable + 5
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", txtPreviewClipID(3).Text) & "</checksum>"
        If Not EmptyCheck(txtPreviewCropTop.Text) Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.top"">" & txtPreviewCropTop.Text & "</attribute>"
        Else
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.top"">0</attribute>"
        End If
        If Not EmptyCheck(txtPreviewCropBottom.Text) Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.bottom"">" & txtPreviewCropBottom.Text & "</attribute>"
        Else
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.bottom"">0</attribute>"
        End If
        If Not EmptyCheck(txtPreviewCropLeft.Text) Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.left"">" & txtPreviewCropLeft.Text & "</attribute>"
        Else
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.left"">0</attribute>"
        End If
        If Not EmptyCheck(txtPreviewCropRight.Text) Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.right"">" & txtPreviewCropRight.Text & "</attribute>"
        Else
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""crop.right"">0</attribute>"
        End If
        If cmbForcedNarrativeLocale(4).Text <> "" Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""image.burned_forced_narrative.locale"">" & cmbForcedNarrativeLocale(4).Text & "</attribute>"
        End If
        If cmbForcedNarrativeLocale(9).Text <> "" Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""image.burned_subtitles.locale"">" & cmbForcedNarrativeLocale(9).Text & "</attribute>"
        End If
        If cmbForcedNarrativeLocale(4).Text <> "" And cmbForcedNarrativeLocale(9).Text <> "" Then
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""image.textless_master"">true</attribute>"
        Else
            WriteTabs TabVariable + 5
            Print #1, "<attribute name=""image.textless_master"">false</attribute>"
        End If
        WriteTabs TabVariable + 4
        Print #1, "</data_file>"
        'QC Report
        If Val(txtQCClipID(4).Text) <> 0 Then
            WriteTabs TabVariable + 4
            Print #1, "<data_file role=""notes"">"
            WriteTabs TabVariable + 5
            Print #1, "<file_name>" & GetData("events", "clipfilename", "eventID", Val(txtQCClipID(4).Text)) & "</file_name>"
            WriteTabs TabVariable + 5
            Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", Val(txtQCClipID(4).Text)) & "</size>"
            WriteTabs TabVariable + 5
            Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", Val(txtQCClipID(4).Text)) & "</checksum>"
            WriteTabs TabVariable + 4
            Print #1, "</data_file>"
        End If
        WriteTabs TabVariable + 3
        Print #1, "</asset>"
    End If
    
End Function

Sub WriteFilmChapterInfo(Optional lp_blnAssetOnly As Boolean, Optional lp_blnFeatureOnly As Boolean)

    'Chapter Information
    
    Dim l_lngChapterCount As Long, TabVariable As Integer
    
    If lp_blnFeatureOnly = True Then
        TabVariable = 2
    Else
        TabVariable = 0
    End If
    
    If lp_blnAssetOnly = True Then
        TabVariable = TabVariable - 1
    End If
    
    WriteTabs (TabVariable + 2)
    Print #1, "<chapters>"
    If chkTimecode(0).Value = True Then
            WriteTabs (TabVariable + 3)
            Print #1, "<timecode_format>24/999 1000/nonDrop</timecode_format>"
    ElseIf chkTimecode(1).Value = True Then
            WriteTabs (TabVariable + 3)
            Print #1, "<timecode_format>25/1 1/nonDrop</timecode_format>"
    ElseIf chkTimecode(2).Value = True Then
            WriteTabs (TabVariable + 3)
            Print #1, "<timecode_format>30/999 1000/nonDrop</timecode_format>"
    ElseIf chkTimecode(3).Value = True Then
            WriteTabs (TabVariable + 3)
            Print #1, "<timecode_format>30/999 1000/dropNTSC</timecode_format>"
    ElseIf chkTimecode(4).Value = True Then
            WriteTabs (TabVariable + 3)
            Print #1, "<timecode_format>qt_text</timecode_format>"
    End If
    adoChapters.Recordset.MoveFirst
    l_lngChapterCount = 1
    Do While Not adoChapters.Recordset.EOF
        WriteTabs (TabVariable + 3)
        Print #1, "<chapter>"
        WriteTabs (TabVariable + 4)
        Print #1, "<start_time>" & adoChapters.Recordset("starttime") & "</start_time>"
        If chkMultiLanguage.Value <> 0 Then
            WriteTabs (TabVariable + 4)
            Print #1, "<titles>"
            If chkCustomChapterTitles.Value <> 0 Then
                WriteTabs (TabVariable + 5)
                Print #1, "<title locale=""" & cmbFilmMetadataLanguage(0).Text & """>" & UTF8_Encode(XMLSanitise(adoChapters.Recordset("Title1"))) & "</title>"
            ElseIf Trim(" " & GetDataSQL("SELECT TOP 1 videostandard FROM xref WHERE category='iTunesLocale' AND description = '" & cmbFilmMetadataLanguage(0).Text & "';")) <> "" Then
                WriteTabs (TabVariable + 5)
                Print #1, "<title locale=""" & cmbFilmMetadataLanguage(0).Text & """>" & UTF8_Encode(XMLSanitise(GetDataSQL("SELECT TOP 1 videostandard FROM xref WHERE category='iTunesLocale' AND description = '" & cmbFilmMetadataLanguage(0).Text & "';"))) & " " & l_lngChapterCount & "</title>"
            Else
                WriteTabs (TabVariable + 5)
                Print #1, "<title locale=""en"">Chapter " & l_lngChapterCount & "</title>"
            End If
            If cmbFilmMetadataLanguage(1).Text <> "" Then
                If chkCustomChapterTitles.Value <> 0 Then
                    WriteTabs (TabVariable + 5)
                    Print #1, "<title locale=""" & cmbFilmMetadataLanguage(1).Text & """>" & UTF8_Encode(XMLSanitise(adoChapters.Recordset("Title2"))) & "</title>"
                ElseIf Trim(" " & GetDataSQL("SELECT TOP 1 videostandard FROM xref WHERE category='iTunesLocale' AND description = '" & cmbFilmMetadataLanguage(1).Text & "';")) <> "" Then
                    WriteTabs (TabVariable + 5)
                    Print #1, "<title locale=""" & cmbFilmMetadataLanguage(1).Text & """>" & UTF8_Encode(XMLSanitise(GetDataSQL("SELECT TOP 1 videostandard FROM xref WHERE category='iTunesLocale' AND description = '" & cmbFilmMetadataLanguage(1).Text & "';"))) & " " & l_lngChapterCount & "</title>"
                Else
                    WriteTabs (TabVariable + 5)
                    Print #1, "<title locale=""en"">Chapter " & l_lngChapterCount & "</title>"
                End If
            End If
            If cmbFilmMetadataLanguage(2).Text <> "" Then
                If chkCustomChapterTitles.Value <> 0 Then
                    WriteTabs (TabVariable + 5)
                    Print #1, "<title locale=""" & cmbFilmMetadataLanguage(1).Text & """>" & UTF8_Encode(XMLSanitise(adoChapters.Recordset("Title3"))) & "</title>"
                ElseIf Trim(" " & GetDataSQL("SELECT TOP 1 videostandard FROM xref WHERE category='iTunesLocale' AND description = '" & cmbFilmMetadataLanguage(2).Text & "';")) <> "" Then
                    WriteTabs (TabVariable + 5)
                    Print #1, "<title locale=""" & cmbFilmMetadataLanguage(2).Text & """>" & UTF8_Encode(XMLSanitise(GetDataSQL("SELECT TOP 1 videostandard FROM xref WHERE category='iTunesLocale' AND description = '" & cmbFilmMetadataLanguage(2).Text & "';"))) & " " & l_lngChapterCount & "</title>"
                Else
                    WriteTabs (TabVariable + 5)
                    Print #1, "<title locale=""en"">Chapter " & l_lngChapterCount & "</title>"
                End If
            End If
            If cmbFilmMetadataLanguage(3).Text <> "" Then
                If chkCustomChapterTitles.Value <> 0 Then
                    WriteTabs (TabVariable + 5)
                    Print #1, "<title locale=""" & cmbFilmMetadataLanguage(1).Text & """>" & UTF8_Encode(XMLSanitise(adoChapters.Recordset("Title4"))) & "</title>"
                ElseIf Trim(" " & GetDataSQL("SELECT TOP 1 videostandard FROM xref WHERE category='iTunesLocale' AND description = '" & cmbFilmMetadataLanguage(3).Text & "';")) <> "" Then
                    WriteTabs (TabVariable + 5)
                    Print #1, "<title locale=""" & cmbFilmMetadataLanguage(3).Text & """>" & UTF8_Encode(XMLSanitise(GetDataSQL("SELECT TOP 1 videostandard FROM xref WHERE category='iTunesLocale' AND description = '" & cmbFilmMetadataLanguage(3).Text & "';"))) & " " & l_lngChapterCount & "</title>"
                Else
                    WriteTabs (TabVariable + 5)
                    Print #1, "<title locale=""en"">Chapter " & l_lngChapterCount & "</title>"
                End If
            End If
            WriteTabs (TabVariable + 4)
            Print #1, "</titles>"
        Else
            If chkCustomChapterTitles.Value <> 0 Then
                WriteTabs (TabVariable + 5)
                Print #1, "<title locale=""" & cmbFilmMetadataLanguage(0).Text & """>" & UTF8_Encode(XMLSanitise(adoChapters.Recordset("Title1"))) & "</title>"
            ElseIf Trim(" " & GetDataSQL("SELECT TOP 1 videostandard FROM xref WHERE category='iTunesLocale' AND description = '" & cmbFilmMetadataLanguage(0).Text & "';")) <> "" Then
                WriteTabs (TabVariable + 5)
                Print #1, "<title locale=""" & cmbFilmMetadataLanguage(0).Text & """>" & UTF8_Encode(XMLSanitise(GetDataSQL("SELECT TOP 1 videostandard FROM xref WHERE category='iTunesLocale' AND description = '" & cmbFilmMetadataLanguage(0).Text & "';"))) & " " & l_lngChapterCount & "</title>"
            Else
                WriteTabs (TabVariable + 5)
                Print #1, "<title locale=""en"">Chapter " & l_lngChapterCount & "</title>"
            End If
        End If
        WriteTabs (TabVariable + 4)
        Print #1, "<artwork_file>"
        WriteTabs (TabVariable + 5)
        Print #1, "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", adoChapters.Recordset("pictureclipID"))) & "</file_name>"
        WriteTabs (TabVariable + 5)
        Print #1, "<size>" & GetData("events", "bigfilesize", "eventID", adoChapters.Recordset("pictureclipID")) & "</size>"
        WriteTabs (TabVariable + 5)
        Print #1, "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", adoChapters.Recordset("pictureclipID")) & "</checksum>"
        WriteTabs (TabVariable + 4)
        Print #1, "</artwork_file>"
        WriteTabs (TabVariable + 3)
        Print #1, "</chapter>"
        l_lngChapterCount = l_lngChapterCount + 1
        adoChapters.Recordset.MoveNext
    Loop
    WriteTabs (TabVariable + 2)
    Print #1, "</chapters>"
    
End Sub

Function CheckIfDataChanged() As Boolean

CheckIfDataChanged = False
Exit Function

If Val(lblPackageID.Caption) <> 0 Then
    CheckIfDataChanged = False

    If GetData("itunes_package", "companyID", "itunes_packageID", Val(lblPackageID.Caption)) <> lblCompanyID.Caption Then CheckIfDataChanged = True: Exit Function
    If GetData("itunes_package", "video_type", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbiTunesType.Text Then CheckIfDataChanged = True: Exit Function
    If GetData("itunes_package", "video_vendorID", "itunes_packageID", Val(lblPackageID.Caption)) <> txtiTunesVendorID.Text Then CheckIfDataChanged = True: Exit Function
    If GetData("itunes_package", "vendor_offer_code", "itunes_packageID", Val(lblPackageID.Caption)) <> txtiTunesVendorOfferCode.Text Then CheckIfDataChanged = True: Exit Function
    If GetData("itunes_package", "video_ISAN", "itunes_packageID", Val(lblPackageID.Caption)) <> txtiTunesISAN.Text Then CheckIfDataChanged = True: Exit Function
    If GetData("itunes_package", "originalspokenlocale", "itunes_packageID", Val(lblPackageID.Caption)) <> txtOriginalSpokenLocale.Text Then CheckIfDataChanged = True: Exit Function
    If GetData("itunes_package", "captionsavailable", "itunes_packageID", Val(lblPackageID.Caption)) <> chkCaptionsAvailability.Value Then CheckIfDataChanged = True: Exit Function
    If GetData("itunes_package", "captionsreason", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbCaptionsReason.Text Then CheckIfDataChanged = True: Exit Function

    Select Case tabType.Tab
    
        Case 0 '"tv"
            If GetData("itunes_package", "provider", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbiTunesProvider(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "video_subtype", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbSubType(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "packageused", "itunes_packageID", Val(lblPackageID.Caption)) <> chkPackageUsed(0).Value Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "metadata_language", "itunes_packageID", Val(lblPackageID.Caption)) <> txtMetaDataLanguage.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "video_copyright_cline", "itunes_packageID", Val(lblPackageID.Caption)) <> txtiTunesCopyrightLine.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "video_title", "itunes_packageID", Val(lblPackageID.Caption)) <> txtTitle.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "video_ep_prod_no", "itunes_packageID", Val(lblPackageID.Caption)) <> txtiTunesEpProdNo.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "video_containerID", "itunes_packageID", Val(lblPackageID.Caption)) <> txtiTunesContainerID.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "video_containerposition", "itunes_packageID", Val(lblPackageID.Caption)) <> txtiTunesContainerPosition.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "video_releasedate", "itunes_packageID", Val(lblPackageID.Caption)) <> datiTunesReleaseDate.Value Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "video_longdescription", "itunes_packageID", Val(lblPackageID.Caption)) <> txtiTunesLongDescription.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "itunestechcomment", "itunes_packageID", Val(lblPackageID.Caption)) <> txtiTunesTechComments.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "video_language", "itunes_packageID", Val(lblPackageID.Caption)) <> txtVideoLanguage.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "video_previewstarttime", "itunes_packageID", Val(lblPackageID.Caption)) <> txtiTunesPreviewStartTime.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_au", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbAU.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_ca", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbCA.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_de", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbDE.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_fr", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbFR.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_uk", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbUK.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_us", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbUS.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_jp", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbJP.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "fullcroptop", "itunes_packageID", Val(lblPackageID.Caption)) <> txtCropTop.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "fullcropbottom", "itunes_packageID", Val(lblPackageID.Caption)) <> txtCropBottom.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "fullcropleft", "itunes_packageID", Val(lblPackageID.Caption)) <> txtCropLeft.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "fullcropright", "itunes_packageID", Val(lblPackageID.Caption)) <> txtCropRight.Text Then CheckIfDataChanged = True: Exit Function
    
        Case 1 '"film"
            If GetData("itunes_package", "iTunesComments", "itunes_packageID", Val(lblPackageID.Caption)) <> txtComments.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "provider", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbiTunesProvider(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "video_subtype", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbSubType(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_multilanguage", "itunes_packageID", Val(lblPackageID.Caption)) <> chkMultiLanguage.Value Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "CustomChapterTitles", "itunes_packageID", Val(lblPackageID.Caption)) <> chkCustomChapterTitles.Value Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_locale1", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbFilmMetadataLanguage(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_territory1", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbFilmTerritory(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_title1", "itunes_packageID", Val(lblPackageID.Caption)) <> txtFilmTitle(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "video_title", "itunes_packageID", Val(lblPackageID.Caption)) <> txtFilmTitle(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_synopsis1", "itunes_packageID", Val(lblPackageID.Caption)) <> txtSynopsis(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_copyright_cline1", "itunes_packageID", Val(lblPackageID.Caption)) <> txtFilmCopyrightLine(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_releasedate1", "itunes_packageID", Val(lblPackageID.Caption)) <> datTheatricalReleaseDate(0).Value Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_locale2", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbFilmMetadataLanguage(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_territory2", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbFilmTerritory(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_title2", "itunes_packageID", Val(lblPackageID.Caption)) <> txtFilmTitle(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_synopsis2", "itunes_packageID", Val(lblPackageID.Caption)) <> txtSynopsis(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_copyright_cline2", "itunes_packageID", Val(lblPackageID.Caption)) <> txtFilmCopyrightLine(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_releasedate2", "itunes_packageID", Val(lblPackageID.Caption)) <> datTheatricalReleaseDate(1).Value Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_locale3", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbFilmMetadataLanguage(2).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_territory3", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbFilmTerritory(2).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_title3", "itunes_packageID", Val(lblPackageID.Caption)) <> txtFilmTitle(2).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_synopsis3", "itunes_packageID", Val(lblPackageID.Caption)) <> txtSynopsis(2).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_copyright_cline3", "itunes_packageID", Val(lblPackageID.Caption)) <> txtFilmCopyrightLine(2).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_releasedate3", "itunes_packageID", Val(lblPackageID.Caption)) <> datTheatricalReleaseDate(2).Value Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_locale4", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbFilmMetadataLanguage(3).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_territory4", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbFilmTerritory(3).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_title4", "itunes_packageID", Val(lblPackageID.Caption)) <> txtFilmTitle(3).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_synopsis4", "itunes_packageID", Val(lblPackageID.Caption)) <> txtSynopsis(3).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_copyright_cline4", "itunes_packageID", Val(lblPackageID.Caption)) <> txtFilmCopyrightLine(3).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "film_releasedate4", "itunes_packageID", Val(lblPackageID.Caption)) <> datTheatricalReleaseDate(3).Value Then CheckIfDataChanged = True: Exit Function
            If cmbCountry.Text <> "" Then
                If GetData("itunes_package", "country", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbCountry.Columns("iso2acode").Text Then CheckIfDataChanged = True: Exit Function
            End If
            If GetData("itunes_package", "fullclipID", "itunes_packageID", Val(lblPackageID.Caption)) <> txtFullClipID.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "mainassetlocale", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbFullLocale(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "captionsclipID", "itunes_packageID", Val(lblPackageID.Caption)) <> txtCaptionsClipID.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "fullcroptop", "itunes_packageID", Val(lblPackageID.Caption)) <> txtFullCropTop.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "fullcropbottom", "itunes_packageID", Val(lblPackageID.Caption)) <> txtFullCropBottom.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "fullcropleft", "itunes_packageID", Val(lblPackageID.Caption)) <> txtFullCropLeft.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "fullcropright", "itunes_packageID", Val(lblPackageID.Caption)) <> txtFullCropRight.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "webscreenerclipID", "itunes_packageID", Val(lblPackageID.Caption)) <> txtWebScreenerClipID.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "previewclipID", "itunes_packageID", Val(lblPackageID.Caption)) <> txtPreviewClipID(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "previewlocale", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbPreviewLocale(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "previewclipID2", "itunes_packageID", Val(lblPackageID.Caption)) <> txtPreviewClipID(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "previewterritory2", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbPreviewTerritory(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "previewlocale2", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbPreviewLocale(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "previewclipID3", "itunes_packageID", Val(lblPackageID.Caption)) <> txtPreviewClipID(2).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "previewterritory3", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbPreviewTerritory(2).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "previewlocale3", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbPreviewLocale(2).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "previewclipID4", "itunes_packageID", Val(lblPackageID.Caption)) <> txtPreviewClipID(3).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "previewterritory4", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbPreviewTerritory(3).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "previewlocale4", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbPreviewLocale(3).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "previewcroptop", "itunes_packageID", Val(lblPackageID.Caption)) <> txtPreviewCropTop.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "previewcropbottom", "itunes_packageID", Val(lblPackageID.Caption)) <> txtPreviewCropBottom.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "previewcropleft", "itunes_packageID", Val(lblPackageID.Caption)) <> txtPreviewCropLeft.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "previewcropright", "itunes_packageID", Val(lblPackageID.Caption)) <> txtPreviewCropRight.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "posterclipID", "itunes_packageID", Val(lblPackageID.Caption)) <> txtPosterClipID(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "posterclipID2", "itunes_packageID", Val(lblPackageID.Caption)) <> txtPosterClipID(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "posterterritory2", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbPosterTerritory(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "posterclipID3", "itunes_packageID", Val(lblPackageID.Caption)) <> txtPosterClipID(2).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "posterterritory3", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbPosterTerritory(2).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "posterclipID4", "itunes_packageID", Val(lblPackageID.Caption)) <> txtPosterClipID(3).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "posterterritory4", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbPosterTerritory(3).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitlesclipID", "itunes_packageID", Val(lblPackageID.Caption)) <> txtSubtitlesClipID(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitleslocale", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbSubtitlesLocale(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitlesclipID2", "itunes_packageID", Val(lblPackageID.Caption)) <> txtSubtitlesClipID(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitleslocale2", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbSubtitlesLocale(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitlesclipID3", "itunes_packageID", Val(lblPackageID.Caption)) <> txtSubtitlesClipID(2).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitleslocale3", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbSubtitlesLocale(2).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitlesclipID4", "itunes_packageID", Val(lblPackageID.Caption)) <> txtSubtitlesClipID(3).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitleslocale4", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbSubtitlesLocale(3).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitlesclipID5", "itunes_packageID", Val(lblPackageID.Caption)) <> txtSubtitlesClipID(8).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitleslocale5", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbSubtitlesLocale(7).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitlesclipID6", "itunes_packageID", Val(lblPackageID.Caption)) <> txtSubtitlesClipID(9).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitleslocale6", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbSubtitlesLocale(8).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitlesclipID7", "itunes_packageID", Val(lblPackageID.Caption)) <> txtSubtitlesClipID(10).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitleslocale7", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbSubtitlesLocale(9).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitlesclipID8", "itunes_packageID", Val(lblPackageID.Caption)) <> txtSubtitlesClipID(11).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitleslocale8", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbSubtitlesLocale(10).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitlesclipID9", "itunes_packageID", Val(lblPackageID.Caption)) <> txtSubtitlesClipID(12).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitleslocale9", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbSubtitlesLocale(11).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitlesclipID10", "itunes_packageID", Val(lblPackageID.Caption)) <> txtSubtitlesClipID(13).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitleslocale10", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbSubtitlesLocale(12).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitlesclipID11", "itunes_packageID", Val(lblPackageID.Caption)) <> txtSubtitlesClipID(14).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitleslocale11", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbSubtitlesLocale(13).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitlesclipID12", "itunes_packageID", Val(lblPackageID.Caption)) <> txtSubtitlesClipID(15).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "subtitleslocale12", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbSubtitlesLocale(14).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "audioclipID1", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAudioClipID(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "audiolocale1", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbAudioLocale(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "audioclipID2", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAudioClipID(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "audiolocale2", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbAudioLocale(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "audioclipID3", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAudioClipID(2).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "audiolocale3", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbAudioLocale(2).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "video_networkname", "itunes_packageID", Val(lblPackageID.Caption)) <> (txtProductionCompany.Text) Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "upc", "itunes_packageID", Val(lblPackageID.Caption)) <> txtUPC.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "amg_video_id", "itunes_packageID", Val(lblPackageID.Caption)) <> txtUPC.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "FeatureTextless", "itunes_packageID", Val(lblPackageID.Caption)) <> chkFeatureTextless.Value Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "FeatureForcedNarrativeLocale", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbForcedNarrativeLocale(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "Trailer1ForcedNarrativeLocale", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbForcedNarrativeLocale(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "Trailer2ForcedNarrativeLocale", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbForcedNarrativeLocale(2).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "Trailer3ForcedNarrativeLocale", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbForcedNarrativeLocale(3).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "Trailer4ForcedNarrativeLocale", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbForcedNarrativeLocale(4).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "QCnotesClipID", "itunes_packageID", Val(lblPackageID.Caption)) <> txtQCClipID(0).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "QCnotestrail1ClipID", "itunes_packageID", Val(lblPackageID.Caption)) <> txtQCClipID(1).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "QCnotestrail2ClipID", "itunes_packageID", Val(lblPackageID.Caption)) <> txtQCClipID(2).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "QCnotestrail3ClipID", "itunes_packageID", Val(lblPackageID.Caption)) <> txtQCClipID(3).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "QCnotestrail4ClipID", "itunes_packageID", Val(lblPackageID.Caption)) <> txtQCClipID(4).Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_au", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbAUfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_aus", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbAUSfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_be", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbBEfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_ca", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbCAfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_den", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbDENfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_fi", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbFIfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_fr", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbFRfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_de", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbDEfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_gr", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbGRfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_ie", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbIEfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_it", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbITfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_jp", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbJPfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_lu", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbLUfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_mx", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbMXfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_nl", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbNLfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_nz", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbNZfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_no", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbNOfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_pt", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbPTfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_qb", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbQBfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_es", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbESfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_se", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbSEfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_ch", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbCHfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_uk", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbUKfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_us", "itunes_packageID", Val(lblPackageID.Caption)) <> cmbUSfilm.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_au_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAUreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_aus_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAUSreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_be_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtBEreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_ca_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtCAreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_den_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtDENreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_fi_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtFIreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_fr_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtFRreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_de_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtDEreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_gr_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtGRreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_ie_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtIEreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_it_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtITreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_jp_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtJPreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_lu_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtLUreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_mx_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtMXreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_nl_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtNLreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_nz_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtNZreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_no_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtNOreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_pt_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtPTreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_qb_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtQBreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_es_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtESreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_se_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtSEreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_ch_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtCHreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_uk_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtUKreason.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "rating_us_reason", "itunes_packageID", Val(lblPackageID.Caption)) <> txtUSreason.Text Then CheckIfDataChanged = True: Exit Function
            
        Case 2, 3 'Amazon and Xbox
    
            If GetData("itunes_package", "packageused", "itunes_packageID", Val(lblPackageID.Caption)) <> chkPackageUsed(1).Value Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "country", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonTerritory.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "video_language", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonLanguage.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "AmazonSeriesUniqueID", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonSeriesID.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "AmazonSeasonUniqueID", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonSeasonID.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "AmazonCopyrightHolder", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonCopyrightHolder.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "AmazonCopyrightYear", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonCopyrightYear.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "AmazonSeriesName", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonSeriesName.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "AmazonSeasonTitle", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonSeasonTitle.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "AmazonSeasonNumber", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonSeasonNumber.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "video_title", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonEpisodeTitle.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "AmazonSequence", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonSequence.Text Then CheckIfDataChanged = True: Exit Function
'            If GetData("itunes_package", "AmazonSeriesShortSynopsis", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonSeriesShortSynopsis.Text Then CheckIfDataChanged = True: Exit Function
'            If GetData("itunes_package", "AmazonSeriesLongSynopsis", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonSeriesLongSynopsis.Text Then CheckIfDataChanged = True: Exit Function
'            If GetData("itunes_package", "AmazonSeasonShortSynopsis", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonSeasonShortSynopsis.Text Then CheckIfDataChanged = True: Exit Function
'            If GetData("itunes_package", "AmazonSeasonLongSynopsis", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonSeasonLongSynopsis.Text Then CheckIfDataChanged = True: Exit Function
'            If GetData("itunes_package", "AmazonEpisodeShortSynopsis", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonEpisodeShortSynopsis.Text Then CheckIfDataChanged = True: Exit Function
'            If GetData("itunes_package", "AmazonEpisodeLongSynopsis", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonEpisodeLongSynopsis.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "AmazonGenre", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonGenre.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "AmazonRatingType", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonRatingType.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "AmazonRating", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonRating.Text Then CheckIfDataChanged = True: Exit Function
            If Format(GetData("itunes_package", "AmazonOriginalAirDate", "itunes_packageID", Val(lblPackageID.Caption)), "YYYY-MM-DD") <> Format(datOriginalAirDate.Value, "YYYY-MM-DD") Then CheckIfDataChanged = True: Exit Function
            If Format(GetData("itunes_package", "AmazonSDOfferStart", "itunes_packageID", Val(lblPackageID.Caption)), "YYYY-MM-DD") <> Format(datSDAmazonOfferStart.Value, "YYYY-MM-DD") Then CheckIfDataChanged = True: Exit Function
            If Format(GetData("itunes_package", "AmazonHDOfferStart", "itunes_packageID", Val(lblPackageID.Caption)), "YYYY-MM-DD") <> Format(datHDAmazonOfferStart.Value, "YYYY-MM-DD") Then CheckIfDataChanged = True: Exit Function
            If Format(GetData("itunes_package", "AmazonSDVODOfferStart", "itunes_packageID", Val(lblPackageID.Caption)), "YYYY-MM-DD") <> Format(datSDVODAmazonOfferStart.Value, "YYYY-MM-DD") Then CheckIfDataChanged = True: Exit Function
            If Format(GetData("itunes_package", "AmazonHDVODOfferStart", "itunes_packageID", Val(lblPackageID.Caption)), "YYYY-MM-DD") <> Format(datHDVODAmazonOfferStart.Value, "YYYY-MM-DD") Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "fullclipid", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonFullClipID.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "posterclipid", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonPosterClipID.Text Then CheckIfDataChanged = True: Exit Function
            If GetData("itunes_package", "AudioClipid1", "itunes_packageID", Val(lblPackageID.Caption)) <> txtAmazonAudioClipID.Text Then CheckIfDataChanged = True: Exit Function
                
    End Select

End If

End Function

Function WriteCastandCrew() As Boolean

WriteCastandCrew = True

'Cast Information
adoCast.Recordset.MoveFirst
Print #1, Chr(9) & Chr(9) & "<cast>"
Do While Not adoCast.Recordset.EOF
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<cast_member";
    If adoCast.Recordset("billing") = "top" Then
        Print #1, " billing=""top""";
    End If
    Print #1, ">"
    If Val(Trim(" " & adoCast.Recordset("apple_ID"))) = 0 Then
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<display_name>" & UTF8_Encode(XMLSanitise(adoCast.Recordset("display_name"))) & "</display_name>"
    Else
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<apple_id>" & adoCast.Recordset("apple_ID") & "</apple_id>"
    End If
    If Trim(" " & adoCast.Recordset("description")) <> "" Then Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<description format=""plain"">" & UTF8_Encode(XMLSanitise(adoCast.Recordset("description"))) & "</description>"
    If chkMultiLanguage.Value <> 0 And (Trim(" " & adoCast.Recordset("display_name2")) <> "" Or Trim(" " & adoCast.Recordset("display_name3")) <> "" Or Trim(" " & adoCast.Recordset("display_name4")) <> "") Then
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locales>"
        If Trim(" " & adoCast.Recordset("display_name2")) <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(1).Text & """>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<display_name>" & UTF8_Encode(XMLSanitise(adoCast.Recordset("display_name2"))) & "</display_name>"
            If Trim(" " & adoCast.Recordset("description2")) <> "" Then Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<description format=""plain"">" & UTF8_Encode(XMLSanitise(adoCast.Recordset("description2"))) & "</description>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locale>"
        End If
        If Trim(" " & adoCast.Recordset("display_name3")) <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(2).Text & """>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<display_name>" & UTF8_Encode(XMLSanitise(adoCast.Recordset("display_name3"))) & "</display_name>"
            If Trim(" " & adoCast.Recordset("description3")) <> "" Then Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<description format=""plain"">" & UTF8_Encode(XMLSanitise(adoCast.Recordset("description3"))) & "</description>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locale>"
        End If
        If Trim(" " & adoCast.Recordset("display_name4")) <> "" Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(3).Text & """>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<display_name>" & UTF8_Encode(XMLSanitise(adoCast.Recordset("display_name4"))) & "</display_name>"
            If Trim(" " & adoCast.Recordset("description4")) <> "" Then Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<description format=""plain"">" & UTF8_Encode(XMLSanitise(adoCast.Recordset("description4"))) & "</description>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locale>"
        End If
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locales>"
    End If
    If Not adoCastCharacter.Recordset.EOF Then
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<characters>"
        adoCastCharacter.Recordset.MoveFirst
        Do While Not adoCastCharacter.Recordset.EOF
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<character>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<reference_id>";
            If adoCastCharacter.Recordset("reference_id") <> "" And ValidateEnglishCharacters(adoCastCharacter.Recordset("reference_id")) Then
                Print #1, adoCastCharacter.Recordset("reference_id") & "</reference_id>"
            Else
                WriteCastandCrew = False
                Exit Function
            End If
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<character_name>" & UTF8_Encode(XMLSanitise(adoCastCharacter.Recordset("character_name"))) & "</character_name>"
            If Trim(" " & adoCastCharacter.Recordset("character_note")) <> "" Then Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<character_note>" & UTF8_Encode(XMLSanitise(adoCastCharacter.Recordset("character_note"))) & "</character_note>"
            If Val(Trim(" " & adoCastCharacter.Recordset("image_clipID"))) <> 0 Then
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<character_image>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<data_file>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<file_name>" & XMLSanitise(GetData("events", "clipfilename", "eventID", adoCastCharacter.Recordset("image_clipID"))) & "</file_name>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<size>" & GetData("events", "bigfilesize", "eventID", adoCastCharacter.Recordset("image_clipID")) & "</size>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<checksum type=""md5"">" & GetData("events", "md5checksum", "eventID", adoCastCharacter.Recordset("image_clipID")) & "</checksum>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</data_file>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</character_image>"
            End If
            If chkMultiLanguage.Value <> 0 And (Trim(" " & adoCastCharacter.Recordset("character_name2")) <> "" Or Trim(" " & adoCastCharacter.Recordset("character_name3")) <> "" Or Trim(" " & adoCastCharacter.Recordset("character_name4")) <> "") Then
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locales>"
                If Trim(" " & adoCastCharacter.Recordset("character_name2")) <> "" Then
                    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(1).Text & """>"
                    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<character_name>" & UTF8_Encode(XMLSanitise(adoCastCharacter.Recordset("character_name2"))) & "</character_name>"
                    If Trim(" " & adoCastCharacter.Recordset("character_note2")) <> "" Then Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<character_note>" & UTF8_Encode(XMLSanitise(adoCastCharacter.Recordset("character_note2"))) & "</character_note>"
                    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locale>"
                End If
                If Trim(" " & adoCastCharacter.Recordset("character_name3")) <> "" Then
                    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(2).Text & """>"
                    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<character_name>" & UTF8_Encode(XMLSanitise(adoCastCharacter.Recordset("character_name3"))) & "</character_name>"
                    If Trim(" " & adoCastCharacter.Recordset("character_note3")) <> "" Then Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<character_note>" & UTF8_Encode(XMLSanitise(adoCastCharacter.Recordset("character_note3"))) & "</character_note>"
                    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locale>"
                End If
                If Trim(" " & adoCastCharacter.Recordset("character_name4")) <> "" Then
                    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(3).Text & """>"
                    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<character_name>" & UTF8_Encode(XMLSanitise(adoCastCharacter.Recordset("character_name4"))) & "</character_name>"
                    If Trim(" " & adoCastCharacter.Recordset("character_note4")) <> "" Then Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<character_note>" & UTF8_Encode(XMLSanitise(adoCastCharacter.Recordset("character_note4"))) & "</character_note>"
                    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locale>"
                End If
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locales>"
            End If
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</character>"
            adoCastCharacter.Recordset.MoveNext
        Loop
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</characters>"
    
    Else
        WriteCastandCrew = False
        Exit Function
    End If
    
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</cast_member>"
    adoCast.Recordset.MoveNext
Loop
Print #1, Chr(9) & Chr(9) & "</cast>"

'Crew Information
Dim l_strLastCrewMember As String, l_strLastAppleID As String, l_blnNewCrew As Boolean, l_blnFirstCrew As Boolean

adoCrew.Recordset.MoveFirst
Print #1, Chr(9) & Chr(9) & "<crew>"
l_strLastCrewMember = ""
l_strLastAppleID = ""
l_blnNewCrew = False
l_blnFirstCrew = True
Do While Not adoCrew.Recordset.EOF
    If Val(Trim(" " & adoCrew.Recordset("apple_id"))) <> 0 Then
        If adoCrew.Recordset("apple_id") <> l_strLastAppleID Then
            l_blnNewCrew = True
            l_strLastAppleID = adoCrew.Recordset("apple_ID")
        End If
    ElseIf adoCrew.Recordset("display_name") <> "" Then
        If adoCrew.Recordset("display_name") <> l_strLastCrewMember Then
            l_blnNewCrew = True
            l_strLastCrewMember = adoCrew.Recordset("display_name")
        End If
    End If
    If l_blnNewCrew = True Then
        l_blnNewCrew = False
        If l_blnFirstCrew = False Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</roles>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</crew_member>"
        Else
            l_blnFirstCrew = False
        End If
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<crew_member";
        If adoCrew.Recordset("billing") = "top" Then
            Print #1, " billing=""top""";
        End If
        Print #1, ">"
        If Val(Trim(" " & adoCrew.Recordset("apple_ID"))) = 0 Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<display_name>" & UTF8_Encode(XMLSanitise(adoCrew.Recordset("display_name"))) & "</display_name>"
        Else
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<apple_id>" & adoCrew.Recordset("apple_ID") & "</apple_id>"
        End If
        If chkMultiLanguage.Value <> 0 And (Trim(" " & adoCrew.Recordset("display_name2")) <> "" Or Trim(" " & adoCrew.Recordset("display_name3")) <> "" Or Trim(" " & adoCrew.Recordset("display_name4")) <> "") Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locales>"
            If Trim(" " & adoCrew.Recordset("display_name2")) <> "" Then
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(1).Text & """>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<display_name>" & UTF8_Encode(XMLSanitise(adoCrew.Recordset("display_name2"))) & "</display_name>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<apple_id>" & adoCrew.Recordset("apple_ID2") & "</apple_id>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locale>"
            End If
            If Trim(" " & adoCrew.Recordset("display_name3")) <> "" Then
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(2).Text & """>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<display_name>" & UTF8_Encode(XMLSanitise(adoCrew.Recordset("display_name3"))) & "</display_name>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<apple_id>" & adoCrew.Recordset("apple_ID3") & "</apple_id>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locale>"
            End If
            If Trim(" " & adoCrew.Recordset("display_name4")) <> "" Then
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<locale name=""" & cmbFilmMetadataLanguage(3).Text & """>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<display_name>" & UTF8_Encode(XMLSanitise(adoCrew.Recordset("display_name4"))) & "</display_name>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<apple_id>" & adoCrew.Recordset("apple_ID4") & "</apple_id>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locale>"
            End If
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</locales>"
        End If
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<roles>"
    End If
    If adoCrew.Recordset("role") = "Dub Artist" Then
        If adoRoles.Recordset.RecordCount > 0 Then
            adoRoles.Recordset.MoveFirst
            Do While Not adoRoles.Recordset.EOF
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<role locale=""" & adoRoles.Recordset("locale") & """ character_reference=""" & adoRoles.Recordset("reference_ID") & """>Dub Artist</role>"
                adoRoles.Recordset.MoveNext
            Loop
        Else
            MsgBox "Need Role information for Rub Artist Crew Members", vbCritical
            WriteCastandCrew = False
            Exit Function
        End If
    Else
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<role>" & XMLSanitise(adoCrew.Recordset("role")) & "</role>"
    End If
    adoCrew.Recordset.MoveNext
Loop
Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</roles>"
Print #1, Chr(9) & Chr(9) & Chr(9) & "</crew_member>"
Print #1, Chr(9) & Chr(9) & "</crew>"
    
End Function

Function WriteArtistAndCuesheet() As Boolean

WriteArtistAndCuesheet = True

'Package Artist Information
adoArtist1.Recordset.MoveFirst
Print #1, Chr(9) & Chr(9) & "<artists>"
Do While Not adoArtist1.Recordset.EOF
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<artist>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<name>" & XMLSanitise(adoArtist1.Recordset("name")) & "</name>"
    adoRoles1.Recordset.MoveFirst
    If Not adoRoles1.Recordset.EOF Then
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<roles>"
        Do While Not adoRoles1.Recordset.EOF
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<role>" & adoRoles1.Recordset("role") & "</role>"
            adoRoles1.Recordset.MoveNext
        Loop
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</roles>"
    End If
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<primary>" & adoArtist1.Recordset("primaryartist") & "</primary>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</artist>"
    adoArtist1.Recordset.MoveNext
Loop
Print #1, Chr(9) & Chr(9) & "</artists>"

'Cue Sheet information
adoCueSheet.Recordset.MoveFirst
Print #1, Chr(9) & Chr(9) & "<cue_sheet>"
Do While Not adoCueSheet.Recordset.EOF
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<cue>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<sequence_number>" & adoCueSheet.Recordset("sequence_number") & "</sequence_number>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<time>"
    If Trim(" " & adoCueSheet.Recordset("start_timecode")) <> "" Then Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<start_timecode>" & adoCueSheet.Recordset("start_timecode") & "</start_timecode>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<duration>" & adoCueSheet.Recordset("duration") & "</duration>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</time>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<title>" & XMLSanitise(adoCueSheet.Recordset("title")) & "</title>"
    adoArtist2.Recordset.MoveFirst
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<artists>"
    Do While Not adoArtist2.Recordset.EOF
        Print #1, Chr(9) & Chr(9) & Chr(9) & "<artist>"
        Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<name>" & XMLSanitise(adoArtist2.Recordset("name")) & "</name>"
        adoRoles2.Recordset.MoveFirst
        If Not adoRoles2.Recordset.EOF Then
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<roles>"
            Do While Not adoRoles2.Recordset.EOF
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<role>" & adoRoles2.Recordset("role") & "</role>"
                adoRoles1.Recordset.MoveNext
            Loop
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</roles>"
        End If
        Print #1, Chr(9) & Chr(9) & Chr(9) & "</artist>"
        adoArtist2.Recordset.MoveNext
    Loop
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</artists>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<isrc>" & adoCueSheet.Recordset("isrc") & "</isrc>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<iswc>" & adoCueSheet.Recordset("iswc") & "</iswc>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<copyright_cline>" & XMLSanitise(adoCueSheet.Recordset("copyright_cline")) & "</copyright_cline>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<copyright_pline>" & XMLSanitise(adoCueSheet.Recordset("copyright_pline")) & "</copyright_pline>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<usage>" & adoCueSheet.Recordset("usage") & "</usage>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</cue>"
    adoCueSheet.Recordset.MoveNext
Loop
Print #1, Chr(9) & Chr(9) & "</cue_sheet>"
    
End Function

Private Sub WriteTabs(lp_intTabCount As Integer)

Dim Count As Integer

For Count = 1 To lp_intTabCount

    Print #1, Chr(9);

Next

End Sub


