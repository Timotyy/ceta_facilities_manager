VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmClipPortalUsers 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Media Portal Users"
   ClientHeight    =   13545
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   27975
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmClipPortalUsers.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   13545
   ScaleWidth      =   27975
   WindowState     =   2  'Maximized
   Begin VB.OptionButton Option1 
      Caption         =   "Sort by ID"
      Height          =   255
      Index           =   1
      Left            =   7860
      TabIndex        =   30
      Top             =   180
      Width           =   1275
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Sort by name"
      Height          =   255
      Index           =   0
      Left            =   6360
      TabIndex        =   29
      Top             =   180
      Value           =   -1  'True
      Width           =   1395
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   315
      Left            =   22560
      TabIndex        =   28
      Top             =   11160
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CheckBox chkEnforceCompanyContacts 
      Caption         =   "Enforce Company Contacts"
      Height          =   255
      Left            =   17040
      TabIndex        =   27
      Top             =   180
      Width           =   2415
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnPortalProfileGroups 
      Height          =   915
      Left            =   24840
      TabIndex        =   26
      Top             =   2220
      Width           =   2355
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   8819
      Columns(0).Caption=   "Supplier"
      Columns(0).Name =   "headerpage"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4154
      _ExtentY        =   1614
      _StockProps     =   77
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnCompanyContact 
      Bindings        =   "frmClipPortalUsers.frx":08CA
      Height          =   1575
      Left            =   16380
      TabIndex        =   25
      Top             =   2580
      Width           =   6855
      DataFieldList   =   "email"
      MaxDropDownItems=   12
      _Version        =   196617
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   4
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "companycontactID"
      Columns(0).Name =   "companycontactID"
      Columns(0).DataField=   "companycontactID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   5847
      Columns(2).Caption=   "name"
      Columns(2).Name =   "name"
      Columns(2).DataField=   "name"
      Columns(2).FieldLen=   256
      Columns(3).Width=   5662
      Columns(3).Caption=   "email"
      Columns(3).Name =   "email"
      Columns(3).DataField=   "email"
      Columns(3).FieldLen=   256
      _ExtentX        =   12091
      _ExtentY        =   2778
      _StockProps     =   77
      DataFieldToDisplay=   "email"
   End
   Begin MSAdodcLib.Adodc adoCompanyContact 
      Height          =   330
      Left            =   21480
      Top             =   4800
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCompanyContact"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton cmdNotifyAllUsers 
      Caption         =   "Email All Content Delivery Users"
      Height          =   315
      Left            =   25320
      TabIndex        =   24
      Top             =   120
      Width           =   2475
   End
   Begin VB.CommandButton cmdNotifyAllAdministrators 
      Caption         =   "Email All Content Delivery Admins"
      Height          =   315
      Left            =   22440
      TabIndex        =   23
      Top             =   120
      Width           =   2715
   End
   Begin VB.CommandButton cmdGoToCompany 
      Caption         =   "Go To"
      Height          =   315
      Left            =   5520
      TabIndex        =   22
      Top             =   120
      Width           =   675
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "Search"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   18960
      TabIndex        =   21
      ToolTipText     =   "Close this form"
      Top             =   13080
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adoUserClipsExpired 
      Height          =   330
      Left            =   120
      Top             =   6540
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoUserClipsExpired"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton cmdNotifyUser 
      Caption         =   "Notify User of Login and Password"
      Height          =   315
      Left            =   19560
      TabIndex        =   19
      Top             =   120
      Width           =   2715
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   17700
      TabIndex        =   16
      ToolTipText     =   "Close this form"
      Top             =   13080
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adoAdminDeliveries 
      Height          =   330
      Left            =   10980
      Top             =   10140
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoAdminDeliveries"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoAdminLogins 
      Height          =   330
      Left            =   10980
      Top             =   8160
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoAdminLogins"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoLogins 
      Height          =   330
      Left            =   120
      Top             =   8160
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoLogins"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdLogins 
      Bindings        =   "frmClipPortalUsers.frx":08EA
      Height          =   1935
      Left            =   120
      TabIndex        =   9
      Top             =   8160
      Width           =   10755
      _Version        =   196617
      AllowDelete     =   -1  'True
      AllowUpdate     =   0   'False
      SelectTypeCol   =   3
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   8
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "webportalloginID"
      Columns(0).Name =   "webportalloginID"
      Columns(0).DataField=   "webportalloginID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "Login Date"
      Columns(1).Name =   "logindate"
      Columns(1).DataField=   "logindate"
      Columns(1).DataType=   7
      Columns(1).FieldLen=   256
      Columns(2).Width=   3175
      Columns(2).Caption=   "Full Name"
      Columns(2).Name =   "fullname"
      Columns(2).DataField=   "fullname"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2117
      Columns(3).Caption=   "Username"
      Columns(3).Name =   "username"
      Columns(3).DataField=   "username"
      Columns(3).FieldLen=   256
      Columns(4).Width=   2117
      Columns(4).Caption=   "Password"
      Columns(4).Name =   "password"
      Columns(4).DataField=   "password"
      Columns(4).FieldLen=   256
      Columns(5).Width=   2461
      Columns(5).Caption=   "IP Address"
      Columns(5).Name =   "ipaddress"
      Columns(5).DataField=   "ipaddress"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "Result"
      Columns(6).Name =   "result"
      Columns(6).DataField=   "result"
      Columns(6).FieldLen=   256
      Columns(7).Width=   4419
      Columns(7).Caption=   "Additional Info"
      Columns(7).Name =   "additionalinfo"
      Columns(7).DataField=   "additionalinfo"
      Columns(7).FieldLen=   256
      _ExtentX        =   18971
      _ExtentY        =   3413
      _StockProps     =   79
      Caption         =   "Portal Logins"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoDeliveries 
      Height          =   330
      Left            =   120
      Top             =   10140
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoDeliveries"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoUserClips 
      Height          =   330
      Left            =   120
      Top             =   4800
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoUserClips"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDeliveries 
      Bindings        =   "frmClipPortalUsers.frx":0902
      Height          =   2715
      Left            =   120
      TabIndex        =   8
      Top             =   10140
      Width           =   10785
      _Version        =   196617
      AllowDelete     =   -1  'True
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   12
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "webdeliveryportalID"
      Columns(0).Name =   "webdeliveryportalID"
      Columns(0).DataField=   "webdeliveryportalID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "portaluserID"
      Columns(1).Name =   "portaluserID"
      Columns(1).DataField=   "portaluserID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3625
      Columns(2).Caption=   "Delivered To"
      Columns(2).Name =   "fullname"
      Columns(2).DataField=   "fullname"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2990
      Columns(3).Caption=   "timewhen"
      Columns(3).Name =   "timewhen"
      Columns(3).DataField=   "timewhen"
      Columns(3).DataType=   7
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "clipID"
      Columns(4).Name =   "clipID"
      Columns(4).DataField=   "clipID"
      Columns(4).FieldLen=   256
      Columns(5).Width=   8811
      Columns(5).Caption=   "Clip Details"
      Columns(5).Name =   "clipdetails"
      Columns(5).DataField=   "clipdetails"
      Columns(5).FieldLen=   256
      Columns(6).Width=   2963
      Columns(6).Caption=   "Access Type"
      Columns(6).Name =   "accesstype"
      Columns(6).DataField=   "accesstype"
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Caption=   "Final Status"
      Columns(7).Name =   "finalstatus"
      Columns(7).DataField=   "finalstatus"
      Columns(7).FieldLen=   256
      Columns(8).Width=   4419
      Columns(8).Caption=   "Additional Info"
      Columns(8).Name =   "additionalinfo"
      Columns(8).DataField=   "additionalinfo"
      Columns(8).FieldLen=   256
      Columns(9).Width=   2990
      Columns(9).Caption=   "Ack Good"
      Columns(9).Name =   "receivedgood"
      Columns(9).DataField=   "receivedgood"
      Columns(9).FieldLen=   256
      Columns(10).Width=   2990
      Columns(10).Caption=   "Ack Bad"
      Columns(10).Name=   "receivedbad"
      Columns(10).DataField=   "receivedbad"
      Columns(10).FieldLen=   256
      Columns(11).Width=   2990
      Columns(11).Caption=   "Ack Comments"
      Columns(11).Name=   "receivedcomments"
      Columns(11).DataField=   "receivedcomments"
      Columns(11).FieldLen=   256
      _ExtentX        =   19024
      _ExtentY        =   4789
      _StockProps     =   79
      Caption         =   "User Deliveries"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdUserClips 
      Bindings        =   "frmClipPortalUsers.frx":091E
      Height          =   1695
      Left            =   120
      TabIndex        =   7
      Top             =   4800
      Width           =   21270
      _Version        =   196617
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   13
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "portalpermissionID"
      Columns(0).Name =   "portalpermissionID"
      Columns(0).DataField=   "portalpermissionID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   1773
      Columns(1).Caption=   "eventID"
      Columns(1).Name =   "eventID"
      Columns(1).DataField=   "eventID"
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "portaluserID"
      Columns(2).Name =   "portaluserID"
      Columns(2).DataField=   "portaluserID"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2117
      Columns(3).Caption=   "Clip Store"
      Columns(3).Name =   "barcode"
      Columns(3).DataField=   "barcode"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   5292
      Columns(4).Caption=   "File Name"
      Columns(4).Name =   "clipfilename"
      Columns(4).DataField=   "clipfilename"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   2646
      Columns(5).Caption=   "Folder"
      Columns(5).Name =   "altlocation"
      Columns(5).DataField=   "altlocation"
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   2514
      Columns(6).Caption=   "Format"
      Columns(6).Name =   "clipformat"
      Columns(6).DataField=   "clipformat"
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   2037
      Columns(7).Caption=   "Bitrate (kbps)"
      Columns(7).Name =   "clipbitrate"
      Columns(7).DataField=   "clipbitrate"
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   2117
      Columns(8).Caption=   "Approval"
      Columns(8).Name =   "approval"
      Columns(8).DataField=   "approval"
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(9).Width=   3200
      Columns(9).Caption=   "Date Assigned"
      Columns(9).Name =   "dateassigned"
      Columns(9).DataField=   "dateassigned"
      Columns(9).DataType=   7
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Caption=   "Assigned By"
      Columns(10).Name=   "assignedby"
      Columns(10).DataField=   "assignedby"
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Caption=   "Permission Start"
      Columns(11).Name=   "permissionstart"
      Columns(11).DataField=   "permissionstart"
      Columns(11).DataType=   7
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Caption=   "Permission End"
      Columns(12).Name=   "permissionend"
      Columns(12).DataField=   "permissionend"
      Columns(12).DataType=   7
      Columns(12).FieldLen=   256
      _ExtentX        =   37518
      _ExtentY        =   2990
      _StockProps     =   79
      Caption         =   "User Clip Assignments"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnMainPages 
      Bindings        =   "frmClipPortalUsers.frx":0939
      Height          =   915
      Left            =   13200
      TabIndex        =   6
      Top             =   2100
      Width           =   2355
      DataFieldList   =   "description"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   2
      Columns(0).Width=   8819
      Columns(0).Caption=   "Main Page Name"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "description"
      Columns(0).FieldLen=   256
      Columns(1).Width=   8819
      Columns(1).Caption=   "Main Page Description"
      Columns(1).Name =   "information"
      Columns(1).DataField=   "information"
      Columns(1).FieldLen=   256
      _ExtentX        =   4154
      _ExtentY        =   1614
      _StockProps     =   77
      DataFieldToDisplay=   "description"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnHeaderPages 
      Height          =   915
      Left            =   10320
      TabIndex        =   5
      Top             =   1320
      Width           =   2475
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   8819
      Columns(0).Caption=   "Supplier"
      Columns(0).Name =   "Supplier"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4366
      _ExtentY        =   1614
      _StockProps     =   77
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   20220
      TabIndex        =   3
      ToolTipText     =   "Close this form"
      Top             =   13080
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adoPortalUsers 
      Height          =   330
      Left            =   120
      Top             =   600
      Width           =   2835
      _ExtentX        =   5001
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoPortalUsers"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdPortalUsers 
      Bindings        =   "frmClipPortalUsers.frx":095A
      Height          =   4095
      Left            =   120
      TabIndex        =   2
      ToolTipText     =   "People who receive content from this company"
      Top             =   600
      Width           =   27675
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeveColorScheme =   1
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16772351
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   32
      Columns(0).Width=   1773
      Columns(0).Caption=   "portaluserID"
      Columns(0).Name =   "portaluserID"
      Columns(0).DataField=   "portaluserID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "Full Name"
      Columns(1).Name =   "fullname"
      Columns(1).DataField=   "fullname"
      Columns(1).FieldLen=   256
      Columns(2).Width=   4419
      Columns(2).Caption=   "Full Name"
      Columns(2).Name =   "fullname-UTF8"
      Columns(2).DataField=   "fullname_UTF8"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2117
      Columns(3).Caption=   "User name"
      Columns(3).Name =   "username"
      Columns(3).DataField=   "username"
      Columns(3).DataType=   10
      Columns(3).FieldLen=   256
      Columns(4).Width=   2117
      Columns(4).Caption=   "Password"
      Columns(4).Name =   "password"
      Columns(4).DataField=   "password"
      Columns(4).DataType=   10
      Columns(4).FieldLen=   256
      Columns(4).Style=   1
      Columns(5).Width=   5292
      Columns(5).Caption=   "Email"
      Columns(5).Name =   "email"
      Columns(5).DataField=   "email"
      Columns(5).DataType=   10
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "companyID"
      Columns(6).Name =   "companyID"
      Columns(6).DataField=   "companyID"
      Columns(6).FieldLen=   256
      Columns(7).Width=   5292
      Columns(7).Caption=   "Administrator Email"
      Columns(7).Name =   "administratoremail"
      Columns(7).DataField=   "administratoremail"
      Columns(7).FieldLen=   256
      Columns(8).Width=   1905
      Columns(8).Caption=   "Date Created"
      Columns(8).Name =   "datecreated"
      Columns(8).DataField=   "datecreated"
      Columns(8).DataType=   7
      Columns(8).FieldLen=   256
      Columns(9).Width=   1905
      Columns(9).Caption=   "Date Active"
      Columns(9).Name =   "dateactivated"
      Columns(9).DataField=   "dateactivated"
      Columns(9).DataType=   7
      Columns(9).FieldLen=   256
      Columns(10).Width=   3651
      Columns(10).Caption=   "Portal Header Page"
      Columns(10).Name=   "portalheaderpage"
      Columns(10).DataField=   "portalheaderpage"
      Columns(10).FieldLen=   256
      Columns(11).Width=   3651
      Columns(11).Caption=   "Portal Footer Page"
      Columns(11).Name=   "portalfooterpage"
      Columns(11).DataField=   "portalfooterpage"
      Columns(11).FieldLen=   256
      Columns(12).Width=   5636
      Columns(12).Caption=   "Portal Main Page"
      Columns(12).Name=   "portalmainpage"
      Columns(12).DataField=   "portalmainpage"
      Columns(12).FieldLen=   256
      Columns(13).Width=   1508
      Columns(13).Caption=   "Pwd Lock"
      Columns(13).Name=   "lockpassword"
      Columns(13).DataField=   "lockpassword"
      Columns(13).FieldLen=   256
      Columns(13).Style=   2
      Columns(14).Width=   1058
      Columns(14).Caption=   "A Del"
      Columns(14).Name=   "acknowledgedeliveries"
      Columns(14).DataField=   "acknowledgedeliveries"
      Columns(14).DataType=   11
      Columns(14).FieldLen=   256
      Columns(14).Style=   2
      Columns(15).Width=   1058
      Columns(15).Caption=   "Exp"
      Columns(15).Name=   "donotexpire"
      Columns(15).Alignment=   2
      Columns(15).DataField=   "donotexpire"
      Columns(15).DataType=   11
      Columns(15).FieldLen=   256
      Columns(16).Width=   1058
      Columns(16).Caption=   "Cl St"
      Columns(16).Name=   "cleanstream"
      Columns(16).DataField=   "cleanstream"
      Columns(16).FieldLen=   256
      Columns(16).Style=   2
      Columns(17).Width=   2223
      Columns(17).Caption=   "Expiry Date"
      Columns(17).Name=   "expirydate"
      Columns(17).DataField=   "expirydate"
      Columns(17).FieldLen=   256
      Columns(17).Style=   1
      Columns(18).Width=   1191
      Columns(18).Caption=   "All Clips"
      Columns(18).Name=   "allclips"
      Columns(18).Alignment=   2
      Columns(18).DataField=   "allclips"
      Columns(18).FieldLen=   256
      Columns(19).Width=   1164
      Columns(19).Caption=   "Active"
      Columns(19).Name=   "activeuser"
      Columns(19).DataField=   "activeuser"
      Columns(19).DataType=   17
      Columns(19).FieldLen=   256
      Columns(19).Style=   2
      Columns(20).Width=   1244
      Columns(20).Caption=   "Single"
      Columns(20).Name=   "singleplay"
      Columns(20).DataField=   "singleplay"
      Columns(20).FieldLen=   256
      Columns(20).Style=   2
      Columns(21).Width=   1614
      Columns(21).Caption=   "Port 33001"
      Columns(21).Name=   "forceport33001"
      Columns(21).DataField=   "forceport33001"
      Columns(21).DataType=   17
      Columns(21).FieldLen=   256
      Columns(21).Style=   2
      Columns(22).Width=   1191
      Columns(22).Caption=   "Setting"
      Columns(22).Name=   "usersettings"
      Columns(22).DataField=   "usersettings"
      Columns(22).FieldLen=   256
      Columns(23).Width=   3200
      Columns(23).Caption=   "Category"
      Columns(23).Name=   "categoryvalue"
      Columns(23).DataField=   "categoryvalue"
      Columns(23).FieldLen=   256
      Columns(24).Width=   1773
      Columns(24).Caption=   "Cat. Fld."
      Columns(24).Name=   "categoryfield"
      Columns(24).DataField=   "categoryfield"
      Columns(24).FieldLen=   256
      Columns(25).Width=   3200
      Columns(25).Caption=   "portaldownloadocde"
      Columns(25).Name=   "portaldownloadcode"
      Columns(25).DataField=   "portaldownloadcode"
      Columns(25).FieldLen=   256
      Columns(26).Width=   3200
      Columns(26).Visible=   0   'False
      Columns(26).Caption=   "portalpurchaseprofilespecID"
      Columns(26).Name=   "portalpurchaseprofilespecID"
      Columns(26).DataField=   "portalpurchaseprofilespecID"
      Columns(26).FieldLen=   256
      Columns(27).Width=   4419
      Columns(27).Caption=   "Profile Group"
      Columns(27).Name=   "Profile Group"
      Columns(27).FieldLen=   256
      Columns(28).Width=   3201
      Columns(28).Caption=   "CopyAfterUpload"
      Columns(28).Name=   "CopyFilesAfterUpload"
      Columns(28).DataField=   "CopyFilesAfterUpload"
      Columns(28).FieldLen=   256
      Columns(28).Style=   2
      Columns(29).Width=   3201
      Columns(29).Caption=   "CopyLibraryID"
      Columns(29).Name=   "CopyFilesToLibraryID"
      Columns(29).DataField=   "CopyFilesToLibraryID"
      Columns(29).FieldLen=   256
      Columns(30).Width=   3201
      Columns(30).Caption=   "CopyFolder"
      Columns(30).Name=   "CopyFilesToFolder"
      Columns(30).DataField=   "CopyFilesToFolder"
      Columns(30).FieldLen=   256
      Columns(31).Width=   3201
      Columns(31).Caption=   "CopyFilesIfExtender"
      Columns(31).Name=   "CopyFilesIfExtender"
      Columns(31).DataField=   "CopyFilesIfExtender"
      Columns(31).FieldLen=   256
      _ExtentX        =   48816
      _ExtentY        =   7223
      _StockProps     =   79
      Caption         =   "Portal Users defined for this JCA Customer"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   300
      Left            =   1560
      TabIndex        =   4
      ToolTipText     =   "The company name"
      Top             =   120
      Width           =   3255
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "Name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   5741
      _ExtentY        =   529
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin MSComCtl2.DTPicker datFrom 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   12480
      TabIndex        =   10
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   120
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   95289345
      CurrentDate     =   39580
   End
   Begin MSComCtl2.DTPicker datTo 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   15480
      TabIndex        =   11
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   120
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   95289345
      CurrentDate     =   39580
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdAdminLogins 
      Bindings        =   "frmClipPortalUsers.frx":0977
      Height          =   1935
      Left            =   10980
      TabIndex        =   14
      Top             =   8160
      Width           =   10395
      _Version        =   196617
      AllowDelete     =   -1  'True
      AllowUpdate     =   0   'False
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   7
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "webportalloginID"
      Columns(0).Name =   "webportalloginID"
      Columns(0).DataField=   "webportalloginID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "Login Date"
      Columns(1).Name =   "logindate"
      Columns(1).DataField=   "logindate"
      Columns(1).DataType=   7
      Columns(1).FieldLen=   256
      Columns(2).Width=   3175
      Columns(2).Caption=   "Full Name"
      Columns(2).Name =   "fullname"
      Columns(2).DataField=   "contactname"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2117
      Columns(3).Caption=   "Username"
      Columns(3).Name =   "username"
      Columns(3).DataField=   "username"
      Columns(3).FieldLen=   256
      Columns(4).Width=   2117
      Columns(4).Caption=   "Password"
      Columns(4).Name =   "password"
      Columns(4).DataField=   "password"
      Columns(4).FieldLen=   256
      Columns(5).Width=   2461
      Columns(5).Caption=   "IP Address"
      Columns(5).Name =   "ipaddress"
      Columns(5).DataField=   "ipaddress"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "Result"
      Columns(6).Name =   "result"
      Columns(6).DataField=   "result"
      Columns(6).FieldLen=   256
      _ExtentX        =   18336
      _ExtentY        =   3413
      _StockProps     =   79
      Caption         =   "Admin Logins"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdAdminDeliveries 
      Bindings        =   "frmClipPortalUsers.frx":0994
      Height          =   2715
      Left            =   10980
      TabIndex        =   15
      Top             =   10140
      Width           =   10425
      _Version        =   196617
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   8
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "webdeliveryportalID"
      Columns(0).Name =   "webdeliveryID"
      Columns(0).DataField=   "webdeliveryID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "portaluserID"
      Columns(1).Name =   "contactID"
      Columns(1).DataField=   "contactID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   2831
      Columns(2).Caption=   "Delivered To"
      Columns(2).Name =   "fullname"
      Columns(2).DataField=   "fullname"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2990
      Columns(3).Caption=   "timewhen"
      Columns(3).Name =   "timewhen"
      Columns(3).DataField=   "timewhen"
      Columns(3).DataType=   7
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "clipID"
      Columns(4).Name =   "clipID"
      Columns(4).DataField=   "clipID"
      Columns(4).FieldLen=   256
      Columns(5).Width=   8811
      Columns(5).Caption=   "Clip Details"
      Columns(5).Name =   "clipdetails"
      Columns(5).DataField=   "clipdetails"
      Columns(5).FieldLen=   256
      Columns(6).Width=   2963
      Columns(6).Caption=   "Access Type"
      Columns(6).Name =   "accesstype"
      Columns(6).DataField=   "accesstype"
      Columns(6).FieldLen=   256
      Columns(7).Width=   2646
      Columns(7).Caption=   "Final Status"
      Columns(7).Name =   "finalstatus"
      Columns(7).DataField=   "finalstatus"
      Columns(7).FieldLen=   256
      _ExtentX        =   18389
      _ExtentY        =   4789
      _StockProps     =   79
      Caption         =   "Admin Deliveries"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdUserClipsExpired 
      Bindings        =   "frmClipPortalUsers.frx":09B5
      Height          =   1575
      Left            =   120
      TabIndex        =   20
      Top             =   6540
      Width           =   22590
      _Version        =   196617
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   16
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "portalpermissionID"
      Columns(0).Name =   "portalpermissionID"
      Columns(0).DataField=   "portalpermissionID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   1773
      Columns(1).Caption=   "eventID"
      Columns(1).Name =   "eventID"
      Columns(1).DataField=   "eventID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "portaluserID"
      Columns(2).Name =   "portaluserID"
      Columns(2).DataField=   "portaluserID"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2117
      Columns(3).Caption=   "Clip Store"
      Columns(3).Name =   "barcode"
      Columns(3).DataField=   "barcode"
      Columns(3).FieldLen=   256
      Columns(4).Width=   5292
      Columns(4).Caption=   "File Name"
      Columns(4).Name =   "clipfilename"
      Columns(4).DataField=   "clipfilename"
      Columns(4).FieldLen=   256
      Columns(5).Width=   2646
      Columns(5).Caption=   "Folder"
      Columns(5).Name =   "altlocation"
      Columns(5).DataField=   "altlocation"
      Columns(5).FieldLen=   256
      Columns(6).Width=   2514
      Columns(6).Caption=   "Format"
      Columns(6).Name =   "clipformat"
      Columns(6).DataField=   "clipformat"
      Columns(6).FieldLen=   256
      Columns(7).Width=   2037
      Columns(7).Caption=   "Bitrate (kbps)"
      Columns(7).Name =   "clipbitrate"
      Columns(7).DataField=   "clipbitrate"
      Columns(7).FieldLen=   256
      Columns(8).Width=   2117
      Columns(8).Caption=   "Approval"
      Columns(8).Name =   "approval"
      Columns(8).DataField=   "approval"
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Caption=   "Date Assigned"
      Columns(9).Name =   "dateassigned"
      Columns(9).DataField=   "dateassigned"
      Columns(9).DataType=   7
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Caption=   "Assigned By"
      Columns(10).Name=   "assignedby"
      Columns(10).DataField=   "assignedby"
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Caption=   "Permission Start"
      Columns(11).Name=   "permissionstart"
      Columns(11).DataField=   "permissionstart"
      Columns(11).DataType=   7
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Caption=   "Permission End"
      Columns(12).Name=   "permissionend"
      Columns(12).DataField=   "permissionend"
      Columns(12).DataType=   7
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Caption=   "Date Expired"
      Columns(13).Name=   "dateexpired"
      Columns(13).DataField=   "dateexpired"
      Columns(13).DataType=   7
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "portalpermissionexpiredID"
      Columns(14).Name=   "portalpermissionexpiredID"
      Columns(14).DataField=   "portalpermissionexpiredID"
      Columns(14).FieldLen=   256
      Columns(15).Width=   1164
      Columns(15).Caption=   "Clip Del"
      Columns(15).Name=   "system_deleted"
      Columns(15).DataField=   "system_deleted"
      Columns(15).FieldLen=   256
      Columns(15).Style=   2
      _ExtentX        =   39846
      _ExtentY        =   2778
      _StockProps     =   79
      Caption         =   "Expired User Clip Assignments"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoPortalMainPages 
      Height          =   330
      Left            =   21480
      Top             =   5160
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoPortalMainPages"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label lblCaption 
      Caption         =   "User Expiry Days"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   3
      Left            =   9180
      TabIndex        =   18
      Top             =   180
      Width           =   1215
   End
   Begin VB.Label lblCompanyExpiry 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   10440
      TabIndex        =   17
      Top             =   120
      Width           =   615
   End
   Begin VB.Label lblCaption 
      Caption         =   "Date Range To"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   2
      Left            =   14280
      TabIndex        =   13
      Top             =   180
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Date Range From"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   11160
      TabIndex        =   12
      Top             =   180
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "JCA Customer"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   180
      Width           =   1275
   End
   Begin VB.Label lblCompanyID 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   4860
      TabIndex        =   0
      Top             =   120
      Width           =   555
   End
End
Attribute VB_Name = "frmClipPortalUsers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_blnNoExistenceCheck As Boolean, m_blnNewUser As Boolean, m_blnReactivateUser As Boolean

Private Sub adoPortalUsers_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

Dim l_strSQL As String

If lblCompanyID.Caption = "" Then Exit Sub

If adoPortalUsers.Recordset.EOF Then
    Exit Sub
Else
    If adoPortalUsers.Recordset("portaluserID") = "" Then
        Exit Sub
    End If
End If

l_strSQL = "SELECT portalpermission.*, events.*, library.barcode FROM portalpermission INNER JOIN (events INNER JOIN library ON events.libraryID = library.libraryID) ON portalpermission.eventID = events.eventID WHERE portaluserID = " & Val(adoPortalUsers.Recordset("portaluserID")) & " and events.system_deleted = 0 ORDER BY dateassigned DESC;"

adoUserClips.RecordSource = l_strSQL
adoUserClips.ConnectionString = g_strConnection
adoUserClips.Refresh
adoUserClips.Caption = adoUserClips.Recordset.RecordCount & " Clips"

l_strSQL = "SELECT portalpermissionexpiredID, portalpermissionexpired.eventID, events.clipfilename, events.altlocation, events.clipformat, events.clipbitrate, portalpermissionexpired.approval, portalpermissionexpired.dateassigned, " _
    & "portalpermissionexpired.dateexpired, events.system_deleted, library.barcode FROM portalpermissionexpired LEFT JOIN (events INNER JOIN library ON events.libraryID = library.libraryID) " _
    & "ON portalpermissionexpired.eventID = events.eventID WHERE portaluserID = " & Val(adoPortalUsers.Recordset("portaluserID")) & " ORDER BY dateassigned DESC" & ";"

adoUserClipsExpired.RecordSource = l_strSQL
adoUserClipsExpired.ConnectionString = g_strConnection
adoUserClipsExpired.Refresh
adoUserClipsExpired.Caption = adoUserClipsExpired.Recordset.RecordCount & " Clips"

l_strSQL = "SELECT * FROM webdeliveryportal WHERE companyID = " & Val(lblCompanyID.Caption)
If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
l_strSQL = l_strSQL & " ORDER BY timewhen DESC;"

adoDeliveries.RecordSource = l_strSQL
adoDeliveries.ConnectionString = g_strConnection
adoDeliveries.Refresh
adoDeliveries.Caption = adoDeliveries.Recordset.RecordCount & " Del."

End Sub

Private Sub chkEnforceCompanyContacts_Click()

Refresh_All_Grids

End Sub

Private Sub cmbCompany_CloseUp()

Dim l_strTemp As String

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text
l_strTemp = GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption)
If InStr(l_strTemp, "/EXP=") > 0 Then
    lblCompanyExpiry.Caption = Val(Mid(l_strTemp, CInt(InStr(l_strTemp, "/EXP=") + 5)))
ElseIf InStr(l_strTemp, "/donotexpire") > 0 Then
    lblCompanyExpiry.Caption = "Never"
Else
    lblCompanyExpiry.Caption = "30"
End If

If InStr(l_strTemp, "/allclips") > 0 Then
    grdPortalUsers.Columns("allclips").Visible = True
Else
    grdPortalUsers.Columns("allclips").Visible = False
End If

If InStr(l_strTemp, "/mediasales") > 0 Then
    grdPortalUsers.Columns("Profile Group").Visible = True
Else
    grdPortalUsers.Columns("Profile Group").Visible = False
End If

Refresh_All_Grids

End Sub

Private Sub cmdClear_Click()

Dim l_strSQL As String

cmbCompany.Text = ""
lblCompanyID.Caption = ""

adoUserClips.ConnectionString = g_strConnection
adoUserClips.RecordSource = "SELECT portalpermission.*, events.*, library.barcode FROM portalpermission INNER JOIN (events INNER JOIN library ON events.libraryID = library.libraryID) ON portalpermission.eventID = events.eventID WHERE portaluserID = -1;"
adoUserClips.Refresh

adoLogins.ConnectionString = g_strConnection
adoLogins.RecordSource = "SELECT * from webportallogin WHERE companyID = -1"
adoLogins.Refresh

adoDeliveries.ConnectionString = g_strConnection
adoDeliveries.RecordSource = "SELECT * from webdeliveryportal WHERE portaluserID = -1"
adoDeliveries.Refresh

adoAdminLogins.ConnectionString = g_strConnection
adoAdminLogins.RecordSource = "SELECT * from weblogin WHERE companyID = -1"
adoAdminLogins.Refresh

l_strSQL = "SELECT webdeliveryid, webdelivery.contactID, webdelivery.clipID, webdelivery.accesstype, timewhen, contact.name as fullname, "
l_strSQL = l_strSQL & "events.clipfilename + ' ' + events.clipformat + ' ' + events.clipbitrate + 'kbps' AS clipdetails "
l_strSQL = l_strSQL & "FROM (webdelivery INNER JOIN events ON webdelivery.clipID = events.eventID) INNER JOIN contact ON webdelivery.contactID = contact.contactID "
l_strSQL = l_strSQL & "WHERE webdelivery.companyID = -1;"

adoAdminDeliveries.ConnectionString = g_strConnection
adoAdminDeliveries.RecordSource = l_strSQL
adoAdminDeliveries.Refresh

Refresh_All_Grids

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub


Private Sub cmdGoToCompany_Click()

If lblCompanyID.Caption <> "" Then ShowCompany Val(lblCompanyID.Caption)
frmCompany.tabDetails.Tab = 8

End Sub

Private Sub cmdNotifyAllAdministrators_Click()

Dim l_strEmailBody As String, CountLines As Long
Dim temp As String, Count As Long
Dim l_strEmailSubject As String
Dim l_rstPeople As ADODB.Recordset, l_strSQL As String, l_strEmailAddress As String, l_strEmailName As String

frmTextEdit.Caption = "Please type the body of the message to be sent"
frmTextEdit.txtTextToEdit.Text = ""
frmTextEdit.Show vbModal
l_strEmailBody = frmTextEdit.txtTextToEdit.Text
Unload frmTextEdit

frmTextEdit.Caption = "Please type the subject of the message to be sent"
frmTextEdit.txtTextToEdit.Text = ""
frmTextEdit.Show vbModal
l_strEmailSubject = frmTextEdit.txtTextToEdit.Text
Unload frmTextEdit

If l_strEmailBody <> "" Then
    
    If MsgBox("You sending an e-mail message to all Content Delivery Administrative Users", vbYesNo, "Are you sure") = vbYes Then
        If MsgBox("You sending an e-mail message to all Content Delivery Administrative Users", vbYesNo, "Are you REALLY sure") = vbYes Then
            l_strEmailBody = WordWrap(l_strEmailBody, 80, CountLines)
            l_strEmailBody = l_strEmailBody & vbCrLf & vbCrLf & "--------------------------------" & vbCrLf & "MX1 Content Delivery Administration" & vbCrLf
            l_strSQL = "SELECT * FROM employee WHERE username IS NOT NULL AND username <> '';"
            Set l_rstPeople = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            
            If l_rstPeople.RecordCount > 0 Then
                l_rstPeople.MoveFirst
                Count = 1
                Do While Not l_rstPeople.EOF
                    l_strEmailName = GetData("contact", "name", "contactID", l_rstPeople("contactID"))
                    l_strEmailAddress = GetData("contact", "email", "contactID", l_rstPeople("contactID"))
                    If l_strEmailAddress <> "" Then
                        cmdNotifyAllAdministrators.Caption = Count & " of " & l_rstPeople.RecordCount & " " & l_strEmailAddress
                        DoEvents
                        SendSMTPMail l_strEmailAddress, l_strEmailName, l_strEmailSubject, "", l_strEmailBody, False, "", "", "", True
                    End If
                    l_rstPeople.MoveNext
                    Count = Count + 1
                Loop
            End If
            l_rstPeople.Close
            Set l_rstPeople = Nothing
        End If
    End If
End If

cmdNotifyAllAdministrators.Caption = "Email All Content Delivery Admins"
DoEvents
Beep

End Sub

Private Sub cmdNotifyAllUsers_Click()

Dim l_strEmailBody As String, CountLines As Long
Dim temp As String, Count As Long
Dim l_strEmailSubject As String, l_strEmailSubjectPrefix As String, l_strSentSubject As String
Dim l_rstPeople As ADODB.Recordset, l_strSQL As String, l_strEmailAddress As String, l_strEmailName As String, l_strEmailFrom As String

frmTextEdit.Caption = "Please type the body of the message to be sent"
frmTextEdit.txtTextToEdit.Text = ""
frmTextEdit.Show vbModal
l_strEmailBody = frmTextEdit.txtTextToEdit.Text
Unload frmTextEdit

frmTextEdit.Caption = "Please type the subject of the message to be sent"
frmTextEdit.txtTextToEdit.Text = ""
frmTextEdit.Show vbModal
l_strEmailSubject = frmTextEdit.txtTextToEdit.Text
Unload frmTextEdit

If l_strEmailBody <> "" Then
    
    If MsgBox("You sending an e-mail message to all Content Delivery Users", vbYesNo, "Are you sure") = vbYes Then
        If MsgBox("You sending an e-mail message to all Content Delivery Users", vbYesNo, "Are you REALLY sure") = vbYes Then
            l_strEmailBody = WordWrap(l_strEmailBody, 80, CountLines)
            l_strEmailBody = l_strEmailBody & vbCrLf & vbCrLf & "--------------------------------" & vbCrLf & "Content Delivery Administration" & vbCrLf
            l_strSQL = "SELECT * FROM portaluser WHERE username IS NOT NULL AND username <> '' AND activeuser > 0;"
            Set l_rstPeople = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            
            If l_rstPeople.RecordCount > 0 Then
                l_rstPeople.MoveFirst
                Count = 1
                Do While Not l_rstPeople.EOF
                    l_strEmailName = Trim(" " & l_rstPeople("fullname"))
                    l_strEmailAddress = Trim(" " & l_rstPeople("email"))
                    l_strEmailFrom = Trim(" " & l_rstPeople("administratoremail"))
                    l_strEmailSubjectPrefix = GetData("company", "portalemailsubject", "companyID", l_rstPeople("companyID"))
                    If l_strEmailSubjectPrefix <> "" Then l_strSentSubject = l_strEmailSubjectPrefix & " - " & l_strEmailSubject
                    If l_strEmailAddress <> "" Then
                        cmdNotifyAllUsers.Caption = l_strEmailAddress
                        DoEvents
                        SendSMTPMail l_strEmailAddress, l_strEmailName, l_strSentSubject, "", l_strEmailBody, False, "", "", "", True ', l_strEmailFrom
                    End If
                    l_rstPeople.MoveNext
                    Count = Count + 1
                Loop
            End If
            l_rstPeople.Close
            Set l_rstPeople = Nothing
        End If
    End If
End If

cmdNotifyAllUsers.Caption = "Email All Content Deliver Users"
DoEvents
Beep

End Sub

Private Sub cmdNotifyUser_Click()

If Val(lblCompanyID.Caption) = 0 Then
    NoCompanySelectedMessage
    Exit Sub
End If

If ValidateEmail(grdPortalUsers.Columns("email").Text) = False Then
    InvalidEmailAddressMessage
    Exit Sub
End If

If MsgBox("Have you used https://cd-users-test.mx1.com to check that the material" & vbCrLf & "is correctly displayed", vbYesNo, "About to notify user of new material") = vbNo Then Exit Sub

Dim l_lngCompanyID As Long, l_strCetaClientCode As String, l_intHours As Integer, l_intCount As Integer
Dim l_strEmailTo As String, l_strEmailFrom As String, l_strEmailSubject As String, l_strEmailCC As String, l_strEmailBody As String, l_strWelcomeMessage As String
Dim l_lngJobID As Long, l_rstContacts As ADODB.Recordset
Dim l_rstClips As ADODB.Recordset, l_strSQL As String

l_strEmailCC = "client_service_uk-VIE@visualdatamedia.com"

l_lngJobID = Val(InputBox("If there is a Job with a notification list, scan the JobID, or Cancel if not", "Notify Users for Job"))
If l_lngJobID <> 0 Then
    Set l_rstContacts = ExecuteSQL("SELECT * FROM jobcontact WHERE jobID = " & l_lngJobID & " ORDER BY name;", g_strExecuteError)
    CheckForSQLError
    If l_rstContacts.RecordCount > 0 Then
        l_rstContacts.MoveFirst
        Do While Not l_rstContacts.EOF
            If l_strEmailCC <> "" Then l_strEmailCC = l_strEmailCC & ";"
            l_strEmailCC = l_strEmailCC & l_rstContacts("email")
            l_rstContacts.MoveNext
        Loop
    End If
    l_rstContacts.Close
    Set l_rstContacts = Nothing
End If
   
l_lngCompanyID = Val(lblCompanyID.Caption)
l_strCetaClientCode = GetData("Company", "cetaclientcode", "companyID", l_lngCompanyID)

If InStr(l_strCetaClientCode, "/portalemailadmin") <= 0 Then
    l_strEmailTo = grdPortalUsers.Columns("email").Text
Else
    l_strEmailTo = grdPortalUsers.Columns("administratoremail").Text
End If

l_strEmailFrom = GetData("company", "jcacontactemail", "companyID", l_lngCompanyID)
If l_strEmailFrom = "" Then l_strEmailFrom = g_strBookingsEmailAddress

If grdPortalUsers.Columns("administratoremail").Text <> "" And InStr(l_strEmailCC, grdPortalUsers.Columns("administratoremail").Text) <= 0 Then
    If l_strEmailCC <> "" Then l_strEmailCC = l_strEmailCC & ";"
    l_strEmailCC = l_strEmailCC & grdPortalUsers.Columns("administratoremail").Text
End If

l_strEmailSubject = GetData("company", "portalemailsubject", "companyID", l_lngCompanyID)
If l_strEmailSubject = "" Then
    l_strEmailSubject = GetData("company", "portalcompanyname", "companyID", l_lngCompanyID)
    If l_strEmailSubject = "" Then
        l_strEmailSubject = GetData("company", "name", "companyID", l_lngCompanyID) & " Content Delivery User Created"
    Else
        l_strEmailSubject = l_strEmailSubject & " Content Delivery User Created"
    End If
End If

l_strEmailBody = GetData("company", "portalemailbody", "companyID", l_lngCompanyID)
l_strWelcomeMessage = GetData("company", "portalwelcomemessage", "companyID", l_lngCompanyID)

l_strSQL = "SELECT events.clipfilename FROM events INNER JOIN portalpermission ON events.eventID = portalpermission.eventID " & _
"WHERE events.bigfilesize IS NOT Null AND events.system_deleted = 0 AND portalpermission.portaluserID = " & grdPortalUsers.Columns("portaluserID").Text & ";"
Set l_rstClips = ExecuteSQL(l_strSQL, g_strExecuteError)
If l_rstClips.RecordCount > 0 Then
    l_strEmailBody = l_strEmailBody & "The following files are available for access:" & vbCrLf
    l_rstClips.MoveFirst
    Do While Not l_rstClips.EOF
        l_strEmailBody = l_strEmailBody & vbCrLf & l_rstClips("clipfilename")
        l_rstClips.MoveNext
    Loop
End If
l_rstClips.Close
Set l_rstClips = Nothing

If l_strEmailBody = "" Then l_strEmailBody = "A UserID has been prepared for you on the Content Delivery system."

l_strEmailBody = l_strEmailBody & vbCrLf & vbCrLf & "Content Delivery User Full Name: " & grdPortalUsers.Columns("Fullname").Text & vbCrLf & _
"Login: " & grdPortalUsers.Columns("username").Text & vbCrLf & "Password: " & grdPortalUsers.Columns("password").Text & vbCrLf & _
"Email: " & grdPortalUsers.Columns("email").Text & vbCrLf & _
"Link to Content Delivery Site: " & GetData("company", "portalwebsiteurl", "companyID", l_lngCompanyID) & vbCrLf & vbCrLf & _
"This is an informational email. Please do not reply to this email." & vbCrLf & _
"If you require any further information, please e-mail client_service_uk-VIE@visualdatamedia.com"

If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/hours=") > 0 Then
    l_intCount = InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/hours=")
    l_intHours = Val(Mid(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), l_intCount + 7))
    If l_intHours > 0 Then
        l_strEmailBody = l_strEmailBody & vbCrLf & "Material will be available for download for " & l_intHours & " hours." & vbCrLf
    End If
End If

If l_strWelcomeMessage <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & vbCrLf & l_strWelcomeMessage

SendOutlookEmail l_strEmailTo, l_strEmailSubject, l_strEmailBody, "", l_strEmailCC, g_strAdministratorEmailAddress, True, l_strEmailFrom

End Sub

Private Sub cmdSearch_Click()

Refresh_All_Grids

End Sub

Private Sub Command1_Click()

Dim rs As ADODB.Recordset, SQL As String

SQL = "SELECT administratoremail FROM portaluser WHERE CHARINDEX('rrsat', administratoremail) <> 0;"
Set rs = ExecuteSQL(SQL, g_strExecuteError)
CheckForSQLError

If rs.RecordCount > 0 Then
    rs.MoveFirst
    Do While Not rs.EOF
        rs("administratoremail") = Replace(rs("administratoremail"), "rrsat", "rrmedia")
        rs.Update
        rs.MoveNext
    Loop
End If
rs.Close
Set rs = Nothing
Beep

End Sub

Private Sub datFrom_CloseUp()

Dim l_strSQL As String

If Val(lblCompanyID.Caption) <> 0 Then

    l_strSQL = "SELECT * FROM webdeliveryportal WHERE companyID = " & Val(lblCompanyID.Caption)
    If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
    If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
    l_strSQL = l_strSQL & " ORDER BY timewhen DESC;"
    
    adoDeliveries.RecordSource = l_strSQL
    adoDeliveries.ConnectionString = g_strConnection
    adoDeliveries.Refresh
    
    l_strSQL = "SELECT * from webportallogin WHERE companyID = '" & Val(lblCompanyID.Caption) & "'"
    If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND logindate >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
    If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND logindate <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
    l_strSQL = l_strSQL & " ORDER BY logindate DESC;"
    
    adoLogins.ConnectionString = g_strConnection
    adoLogins.RecordSource = l_strSQL
    adoLogins.Refresh
    
    l_strSQL = "SELECT * from weblogin WHERE companyID = '" & Val(lblCompanyID.Caption) & "'"
    If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND logindate >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
    If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND logindate <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
    l_strSQL = l_strSQL & " ORDER BY logindate DESC;"
    
    adoAdminLogins.ConnectionString = g_strConnection
    adoAdminLogins.RecordSource = l_strSQL
    adoAdminLogins.Refresh
    
    l_strSQL = "SELECT webdeliveryid, webdelivery.contactID, webdelivery.clipID, webdelivery.accesstype, timewhen, contact.name as fullname, "
    l_strSQL = l_strSQL & "events.clipfilename + ' ' + events.clipformat + ' ' + events.clipbitrate + 'kbps' AS clipdetails "
    l_strSQL = l_strSQL & "FROM (webdelivery INNER JOIN events ON webdelivery.clipID = events.eventID) INNER JOIN contact ON webdelivery.contactID = contact.contactID "
    l_strSQL = l_strSQL & "WHERE webdelivery.companyID = " & Val(lblCompanyID.Caption)
    If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
    If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
    l_strSQL = l_strSQL & " ORDER BY timewhen DESC;"
    
    adoAdminDeliveries.RecordSource = l_strSQL
    adoAdminDeliveries.ConnectionString = g_strConnection
    adoAdminDeliveries.Refresh

End If

End Sub

Private Sub datTo_CloseUp()

Dim l_strSQL As String

If Val(lblCompanyID.Caption) <> 0 Then
    
    l_strSQL = "SELECT * FROM webdeliveryportal WHERE companyID = " & Val(lblCompanyID.Caption)
    If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
    If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
    l_strSQL = l_strSQL & " ORDER BY timewhen DESC;"
    
    adoDeliveries.RecordSource = l_strSQL
    adoDeliveries.ConnectionString = g_strConnection
    adoDeliveries.Refresh
    
    l_strSQL = "SELECT * from webportallogin WHERE companyID = '" & Val(lblCompanyID.Caption) & "'"
    If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND logindate >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
    If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND logindate <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
    l_strSQL = l_strSQL & " ORDER BY logindate DESC;"
    
    adoLogins.ConnectionString = g_strConnection
    adoLogins.RecordSource = l_strSQL
    adoLogins.Refresh
    
    l_strSQL = "SELECT * from weblogin WHERE companyID = '" & Val(lblCompanyID.Caption) & "'"
    If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND logindate >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
    If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND logindate <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
    l_strSQL = l_strSQL & " ORDER BY logindate DESC;"
    
    adoAdminLogins.ConnectionString = g_strConnection
    adoAdminLogins.RecordSource = l_strSQL
    adoAdminLogins.Refresh

    l_strSQL = "SELECT webdeliveryid, webdelivery.contactID, webdelivery.clipID, webdelivery.accesstype, timewhen, contact.name as fullname, "
    l_strSQL = l_strSQL & "events.clipfilename + ' ' + events.clipformat + ' ' + events.clipbitrate + 'kbps' AS clipdetails "
    l_strSQL = l_strSQL & "FROM (webdelivery INNER JOIN events ON webdelivery.clipID = events.eventID) INNER JOIN contact ON webdelivery.contactID = contact.contactID "
    l_strSQL = l_strSQL & "WHERE webdelivery.companyID = " & Val(lblCompanyID.Caption)
    If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
    If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
    l_strSQL = l_strSQL & " ORDER BY timewhen DESC;"
    
    adoAdminDeliveries.RecordSource = l_strSQL
    adoAdminDeliveries.ConnectionString = g_strConnection
    adoAdminDeliveries.Refresh

End If

End Sub

Private Sub ddnCompanyContact_CloseUp()

grdPortalUsers.Columns("fullname").Text = ddnCompanyContact.Columns("name").Text
grdPortalUsers.Columns("Username").Text = ddnCompanyContact.Columns("email").Text

End Sub

Private Sub Form_Activate()

Dim l_strTemp As String
If Val(lblCompanyID.Caption) <> 0 Then
    l_strTemp = GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption)
    If InStr(l_strTemp, "/EXP=") > 0 Then
        lblCompanyExpiry.Caption = Val(Mid(l_strTemp, CInt(InStr(l_strTemp, "/EXP=") + 5)))
    ElseIf InStr(l_strTemp, "/donotexpire") > 0 Then
        lblCompanyExpiry.Caption = "Never"
    Else
        lblCompanyExpiry.Caption = "30"
    End If
    
    If InStr(l_strTemp, "/allclips") > 0 Then
        grdPortalUsers.Columns("allclips").Visible = True
    Else
        grdPortalUsers.Columns("allclips").Visible = False
    End If
    
    If InStr(l_strTemp, "/mediasales") > 0 Then
        grdPortalUsers.Columns("Profile Group").Visible = True
    Else
        grdPortalUsers.Columns("Profile Group").Visible = False
    End If
End If
Refresh_All_Grids

m_blnNoExistenceCheck = False
m_blnNewUser = False

End Sub

Private Sub Form_Load()

Dim l_strSQL As String
l_strSQL = "SELECT name, companyID FROM company WHERE (iscustomer = 1 OR isprospective = 1) AND system_active = 1 ORDER BY name"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

adoPortalMainPages.RecordSource = "SELECT information, description FROM xref WHERE category = 'portalmainpages' ORDER BY forder, information;"
adoPortalMainPages.ConnectionString = g_strConnection
adoPortalMainPages.Refresh

PopulateCombo "portalheaderpages", ddnHeaderPages
PopulateCombo "portalpurchaseprofilegroups", ddnPortalProfileGroups

grdPortalUsers.Columns("portalheaderpage").DropDownHwnd = ddnHeaderPages.hWnd
grdPortalUsers.Columns("portalmainpage").DropDownHwnd = ddnMainPages.hWnd
grdPortalUsers.Columns("Profile Group").DropDownHwnd = ddnPortalProfileGroups.hWnd

End Sub

Private Sub Form_Resize()

On Error Resume Next

If Me.WindowState = vbMinimized Then Exit Sub

cmdClose.Top = Me.ScaleHeight - cmdClose.Height - 120
cmdClose.Left = Me.ScaleWidth - cmdClose.Width - 120
cmdSearch.Top = cmdClose.Top
cmdSearch.Left = cmdClose.Left - cmdSearch.Width - 120
cmdClear.Top = cmdClose.Top
cmdClear.Left = cmdSearch.Left - cmdClear.Width - 120

grdPortalUsers.Height = (Me.ScaleHeight - grdPortalUsers.Top - cmdClose.Height - 120 - 480) / 3
grdPortalUsers.Width = Me.ScaleWidth - 240

grdDeliveries.Top = grdPortalUsers.Top + grdPortalUsers.Height + 120
grdDeliveries.Width = Me.ScaleWidth * 0.65 - 240
grdDeliveries.Height = grdPortalUsers.Height
grdDeliveries.Left = grdPortalUsers.Left
adoDeliveries.Left = grdDeliveries.Left
adoDeliveries.Top = grdDeliveries.Top

grdAdminDeliveries.Height = grdPortalUsers.Height
grdAdminDeliveries.Top = grdDeliveries.Top + grdDeliveries.Height + 120
grdAdminDeliveries.Width = Me.ScaleWidth * 0.42 - 180
grdAdminDeliveries.Left = grdPortalUsers.Left
adoAdminDeliveries.Left = grdAdminDeliveries.Left
adoAdminDeliveries.Top = grdAdminDeliveries.Top

grdAdminLogins.Height = grdDeliveries.Height / 2 - 60
grdAdminLogins.Top = grdDeliveries.Top
grdAdminLogins.Width = Me.ScaleWidth * 0.35 - 120
grdAdminLogins.Left = grdDeliveries.Left + grdDeliveries.Width + 120
adoAdminLogins.Left = grdAdminLogins.Left
adoAdminLogins.Top = grdAdminLogins.Top

grdLogins.Height = grdAdminLogins.Height
grdLogins.Top = grdAdminLogins.Top + grdAdminLogins.Height + 120
grdLogins.Width = grdAdminLogins.Width
grdLogins.Left = grdAdminLogins.Left
adoLogins.Left = grdLogins.Left
adoLogins.Top = grdLogins.Top

grdUserClips.Top = grdAdminDeliveries.Top
grdUserClips.Width = Me.ScaleWidth * 0.58 - 180
grdUserClips.Left = grdAdminDeliveries.Left + grdAdminDeliveries.Width + 120
grdUserClips.Height = grdPortalUsers.Height / 2 - 60
adoUserClips.Top = grdUserClips.Top
adoUserClips.Left = grdUserClips.Left

grdUserClipsExpired.Top = grdUserClips.Top + grdUserClips.Height + 120
grdUserClipsExpired.Height = grdUserClips.Height
grdUserClipsExpired.Width = grdUserClips.Width
grdUserClipsExpired.Left = grdUserClips.Left
adoUserClipsExpired.Left = grdUserClipsExpired.Left
adoUserClipsExpired.Top = grdUserClipsExpired.Top

End Sub

Private Sub grdDeliveries_DblClick()

If grdDeliveries.Columns("clipID").Text <> "" Then
    ShowClipControl Val(grdDeliveries.Columns("clipID").Text)
End If

End Sub

Private Sub grdPortalUsers_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

Dim l_strSQL As String, l_intResp As Integer

l_intResp = MsgBox("You are about to delete portal user: " & grdPortalUsers.Columns("fullname").Text & vbCrLf & "Are you sure?", vbYesNo, "Delete Portal User")
If l_intResp = vbYes Then
    l_strSQL = "DELETE FROM portalpermission WHERE portaluserid = " & grdPortalUsers.Columns("portaluserID").Text
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    DispPromptMsg = 0
    m_blnNoExistenceCheck = True
Else
    Cancel = 1
End If

End Sub

Private Sub grdPortalUsers_BeforeInsert(Cancel As Integer)

If lblCompanyID.Caption = "" Then
    Cancel = True
Else
    m_blnNewUser = True
End If

End Sub

Private Sub grdPortalUsers_BeforeUpdate(Cancel As Integer)

Dim l_strSQL As String, l_strCetaCode As String, l_lngDays As Long, Count As Long

grdPortalUsers.Columns("fullname").Text = UTF8_Encode(grdPortalUsers.Columns("fullname-utf8").Text)
'grdPortalUsers.Columns("fullname").Text = grdPortalUsers.Columns("fullname-utf8").Text

grdPortalUsers.Columns("companyID").Text = lblCompanyID.Caption

If InStr(LCase(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption))), "/upload") <= 0 Then
    If GetData("xref", "format", "description", grdPortalUsers.Columns("portalmainpage").Text) = "Upload" Then
        MsgBox "You cannot use this Portal Main Page" & vbCrLf & "This company has not yet been enabled for Uploading", vbCritical, "Error"
        Cancel = True
        Exit Sub
    End If
End If

If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/mediasales") <= 0 Then
    If GetData("xref", "format", "description", grdPortalUsers.Columns("portalmainpage").Text) = "Sales" Then
        MsgBox "You cannot use this Portal Main Page" & vbCrLf & "This company has not yet been enabled for Media Sales Functionality", vbCritical, "Error"
        Cancel = True
        Exit Sub
    End If
End If

If Trim(grdPortalUsers.Columns("portaldownloadocde").Text) = "" Then
    grdPortalUsers.Columns("portaldownloadcode").Text = GetData("company", "portaldownloadcode", "companyID", Val(lblCompanyID.Caption))
End If

If grdPortalUsers.Columns("portalheaderpage").Text = "" Then
    grdPortalUsers.Columns("portalheaderpage").Text = GetData("company", "portalheaderpage", "companyID", Val(lblCompanyID.Caption))
End If
If grdPortalUsers.Columns("portalmainpage").Text = "" Then
    grdPortalUsers.Columns("portalmainpage").Text = GetData("company", "portalmainpage", "companyID", Val(lblCompanyID.Caption))
End If
If grdPortalUsers.Columns("portalfooterpage").Text = "" Then
    grdPortalUsers.Columns("portalfooterpage").Text = GetData("company", "portalfooterpage", "companyID", Val(lblCompanyID.Caption))
End If

If grdPortalUsers.Columns("email").Text <> "" Then
    If ValidateEmail(grdPortalUsers.Columns("email").Text) = False Then
        MsgBox "Invalid Email address provided"
        Cancel = 1
    End If
Else
    MsgBox "No Email address provided"
    Cancel = 1
End If

If grdPortalUsers.Columns("administratoremail").Text = "" Then grdPortalUsers.Columns("administratoremail").Text = GetData("company", "jcacontactemail", "companyID", lblCompanyID.Caption)

If grdPortalUsers.Columns("administratoremail").Text <> "" Then
    If ValidateEmail(grdPortalUsers.Columns("administratoremail").Text) = False Then
        MsgBox "Invalid Administrator Email address provided"
        Cancel = 1
    End If
Else
    MsgBox "No Email address provided"
    Cancel = 1
End If

If grdPortalUsers.Columns("acknowledgedeliveries").Text = "" Then grdPortalUsers.Columns("acknowledgedeliveries").Text = "0"
If grdPortalUsers.Columns("acknowledgedeliveries").Text = "-1" Then grdPortalUsers.Columns("acknowledgedeliveries").Text = "1"

If grdPortalUsers.Columns("singleplay").Text = "" Then grdPortalUsers.Columns("singleplay").Text = "0"
If grdPortalUsers.Columns("singleplay").Text = "-1" Then
    grdPortalUsers.Columns("singleplay").Text = "1"
    grdPortalUsers.Columns("portalmainpage").Text = "portal_search_results_playonly.asp"
End If

If grdPortalUsers.Columns("donotexpire").Text = "" Then grdPortalUsers.Columns("donotexpire").Text = "0"

If grdPortalUsers.Columns("cleanstream").Text = "" Then grdPortalUsers.Columns("cleanstream").Text = "0"
If grdPortalUsers.Columns("cleanstream").Text = "-1" Then grdPortalUsers.Columns("cleanstream").Text = "1"

If grdPortalUsers.Columns("activeuser").Text = "" Then grdPortalUsers.Columns("activeuser").Text = "1"
If grdPortalUsers.Columns("activeuser").Text = "-1" Then grdPortalUsers.Columns("activeuser").Text = "1"

If grdPortalUsers.Columns("allclips").Text = "" Then grdPortalUsers.Columns("allclips").Text = "0"
If grdPortalUsers.Columns("allclips").Text = "-1" Then grdPortalUsers.Columns("allclips").Text = "1"

If grdPortalUsers.Columns("lockpassword").Text = "" Then grdPortalUsers.Columns("lockpassword").Text = "0"
If grdPortalUsers.Columns("lockpassword").Text = "-1" Then grdPortalUsers.Columns("lockpassword").Text = "1"

If grdPortalUsers.Columns("datecreated").Text = "" Then grdPortalUsers.Columns("datecreated").Text = Format(Now, "dd mmm yyyy")
If grdPortalUsers.Columns("dateactivated").Text = "" Then grdPortalUsers.Columns("dateactivated").Text = Format(Now, "dd mmm yyyy")

If m_blnNoExistenceCheck = False Then
    If GetData("portaluser", "portaluserID", "username", grdPortalUsers.Columns("username").Text) <> grdPortalUsers.Columns("portaluserID").Text And GetData("portaluser", "portaluserID", "username", grdPortalUsers.Columns("username").Text) <> 0 Then
        MsgBox "A user with that username already exists", vbCritical, "Cannot Save."
        Cancel = True
    End If
Else
    m_blnNoExistenceCheck = False
End If

On Error Resume Next

If Val(Trim(" " & adoPortalUsers.Recordset("activeuser"))) = 0 And grdPortalUsers.Columns("activeuser").Text = 1 Then
    m_blnReactivateUser = True
    grdPortalUsers.Columns("dateactivated").Text = Format(Now, "dd mmm yyyy")
End If

If grdPortalUsers.Columns("Profile Group").Text <> "" Then
    grdPortalUsers.Columns("portalpurchaseprofilespecID").Text = GetDataSQL("SELECT TOP 1 description FROM xref WHERE category = 'portalpurchaseprofilegroups' AND information = '" & grdPortalUsers.Columns("Profile Group").Text & "';")
End If

If (m_blnNewUser = True Or m_blnReactivateUser = True) And (InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/nochargeuser") <= 0) Then

    l_strCetaCode = GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption)
    If InStr(l_strCetaCode, "/donotexpire") > 0 Then
        l_lngDays = 0
    Else
        Count = InStr(l_strCetaCode, "/EXP=")
        If Count > 0 Then
            l_lngDays = Val(Mid(l_strCetaCode, Count + 5))
        Else
            l_lngDays = 30
        End If
    End If
    l_strSQL = "INSERT INTO jobdetailunbilled (companyID, copytype, description, quantity, format, runningtime, completeddate, completeduser) VALUES ("
    l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
    l_strSQL = l_strSQL & "'O', "
    If m_blnNewUser = True Then
        l_strSQL = l_strSQL & "'Media Window New User "
    ElseIf m_blnReactivateUser = True Then
        l_strSQL = l_strSQL & "'Media Window User Reactivation "
    Else
        l_strSQL = l_strSQL & "'Media Window "
    End If
    If l_lngDays > 0 Then
        l_strSQL = l_strSQL & l_lngDays & " day charge "
    End If
    l_strSQL = l_strSQL & "- " & QuoteSanitise(grdPortalUsers.Columns("fullname").Text) & "', "
    l_strSQL = l_strSQL & "1, 'MEDWINUSER', 0, "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
    l_strSQL = l_strSQL & "'" & g_strFullUserName & "');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    m_blnNewUser = False
    m_blnReactivateUser = False
    
End If
On Error GoTo 0
End Sub

Private Sub grdPortalUsers_BtnClick()

If grdPortalUsers.Columns(grdPortalUsers.Col).Name = "expirydate" Then
    If grdPortalUsers.Columns(grdPortalUsers.Col).Text <> "" Then
        grdPortalUsers.Columns(grdPortalUsers.Col).Text = ""
    Else
        grdPortalUsers.Columns(grdPortalUsers.Col).Text = Format(Now, "yyyy-mm-dd")
    End If
Else
    grdPortalUsers.Columns(grdPortalUsers.Col).Text = RndPassword(6)
End If

End Sub

Private Sub grdPortalUsers_RowLoaded(ByVal Bookmark As Variant)

grdPortalUsers.Columns("fullname-UTF8").Text = UTF8_Decode(grdPortalUsers.Columns("fullname").Text)
grdPortalUsers.Columns("Profile Group").Text = GetDataSQL("SELECT TOP 1 information FROM xref WHERE category = 'portalpurchaseprofilegroups' AND description = '" & grdPortalUsers.Columns("portalpurchaseprofilespecID").Text & "';")

End Sub

Private Sub grdUserClips_DblClick()

ShowClipControlFromPortalControl grdUserClips

End Sub

Sub Refresh_All_Grids()

Dim l_strSQL As String, l_lngCompanyID As Long, l_strTemp As String

l_lngCompanyID = Val(lblCompanyID.Caption)

If l_lngCompanyID <> 0 Then
    
    adoPortalUsers.ConnectionString = g_strConnection
    adoPortalUsers.RecordSource = "SELECT * from portaluser WHERE companyID = '" & Val(lblCompanyID.Caption) & "' "
    If datFrom.Value <> "" Then adoPortalUsers.RecordSource = adoPortalUsers.RecordSource & " AND dateactivated >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
    If datTo.Value <> "" Then adoPortalUsers.RecordSource = adoPortalUsers.RecordSource & " AND dateactivated <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
    If Option1(0).Value <> 0 Then
        adoPortalUsers.RecordSource = adoPortalUsers.RecordSource & " ORDER BY fullname;"
    Else
        adoPortalUsers.RecordSource = adoPortalUsers.RecordSource & " ORDER BY portaluserID;"
    End If
    adoPortalUsers.Refresh
    adoPortalUsers.Caption = adoPortalUsers.Recordset.RecordCount & " Users"
    
    l_strSQL = "SELECT * from webportallogin WHERE companyID = '" & Val(lblCompanyID.Caption) & "'"
    If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND logindate >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
    If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND logindate <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
    l_strSQL = l_strSQL & " ORDER BY logindate DESC;"
    
    adoLogins.ConnectionString = g_strConnection
    adoLogins.RecordSource = l_strSQL
    adoLogins.Refresh
    adoLogins.Caption = adoLogins.Recordset.RecordCount & " Logins"
    
    l_strSQL = "SELECT * from weblogin WHERE companyID = '" & Val(lblCompanyID.Caption) & "'"
    If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND logindate >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
    If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND logindate <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
    l_strSQL = l_strSQL & " ORDER BY logindate DESC;"
    
    adoAdminLogins.ConnectionString = g_strConnection
    adoAdminLogins.RecordSource = l_strSQL
    adoAdminLogins.Refresh
    adoAdminLogins.Caption = adoAdminLogins.Recordset.RecordCount & " Logins"
    
    l_strSQL = "SELECT webdeliveryid, webdelivery.contactID, webdelivery.clipID, webdelivery.accesstype, timewhen, finalstatus, contact.name as fullname, "
    l_strSQL = l_strSQL & "events.clipfilename + ' ' + events.clipformat AS clipdetails "
    l_strSQL = l_strSQL & "FROM (webdelivery INNER JOIN events ON webdelivery.clipID = events.eventID) INNER JOIN contact ON webdelivery.contactID = contact.contactID "
    l_strSQL = l_strSQL & "WHERE webdelivery.companyID = " & Val(lblCompanyID.Caption)
    If datFrom.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen >= '" & FormatSQLDate(Format(datFrom.Value, vbShortDateFormat) & " 00:00") & "'"
    If datTo.Value <> "" Then l_strSQL = l_strSQL & " AND timewhen <= '" & FormatSQLDate(Format(datTo.Value, vbShortDateFormat) & " 23:59") & "'"
    l_strSQL = l_strSQL & " ORDER BY timewhen DESC;"
    
    adoAdminDeliveries.RecordSource = l_strSQL
    adoAdminDeliveries.ConnectionString = g_strConnection
    adoAdminDeliveries.Refresh
    adoAdminDeliveries.Caption = adoAdminDeliveries.Recordset.RecordCount & " Del."

    l_strTemp = GetData("company", "cetaclientcode", "companyID", l_lngCompanyID)
    
    If InStr(l_strTemp, "/allclips") > 0 Then
        grdPortalUsers.Columns("allclips").Visible = True
    Else
        grdPortalUsers.Columns("allclips").Visible = False
    End If
    
    If GetData("companycontact", "companycontactID", "companyID", l_lngCompanyID) <> 0 And chkEnforceCompanyContacts.Value <> 0 Then
        adoCompanyContact.RecordSource = "SELECT * FROM companycontact WHERE companyID = " & l_lngCompanyID & "ORDER BY name;"
        adoCompanyContact.ConnectionString = g_strConnection
        adoCompanyContact.Refresh
        grdPortalUsers.Columns("email").DropDownHwnd = ddnCompanyContact.hWnd
    Else
        grdPortalUsers.Columns("email").DropDownHwnd = 0
    End If

    DoEvents
    
Else
    
    adoPortalUsers.ConnectionString = g_strConnection
    adoPortalUsers.RecordSource = "SELECT * FROM portaluser;"
    adoPortalUsers.Refresh
    adoPortalUsers.Caption = adoPortalUsers.Recordset.RecordCount & " Users"
    
End If

End Sub

Private Sub grdUserClipsExpired_DblClick()

ShowClipControlFromPortalControl grdUserClipsExpired

End Sub

Private Sub grdUserClipsExpired_RowLoaded(ByVal Bookmark As Variant)

grdUserClipsExpired.Columns("assignedby").Text = GetData("portalpermissionexpired", "assignedby", "portalpermissionexpiredID", Val(grdUserClipsExpired.Columns("portalpermissionexpiredID").Text))

End Sub
