VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmResourceList 
   Caption         =   "Resource List"
   ClientHeight    =   7065
   ClientLeft      =   3495
   ClientTop       =   4440
   ClientWidth     =   13155
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7065
   ScaleWidth      =   13155
   WindowState     =   2  'Maximized
   Begin VB.PictureBox picHeader 
      Align           =   1  'Align Top
      Height          =   1335
      Left            =   0
      ScaleHeight     =   1275
      ScaleWidth      =   13095
      TabIndex        =   6
      Top             =   0
      Width           =   13155
      Begin VB.CheckBox chkShowDeletedItems 
         Caption         =   "Show Deleted Items"
         Height          =   255
         Left            =   7260
         TabIndex        =   29
         Top             =   960
         Width           =   1755
      End
      Begin VB.CheckBox chkShowSoldItems 
         Caption         =   "Show Sold Items"
         Height          =   255
         Left            =   7260
         TabIndex        =   28
         Top             =   660
         Width           =   1755
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   375
         Left            =   7260
         TabIndex        =   27
         ToolTipText     =   "Search resource list"
         Top             =   180
         Width           =   1755
      End
      Begin VB.TextBox txtSearch 
         DataField       =   "barcode"
         Height          =   285
         Index           =   9
         Left            =   5520
         TabIndex        =   25
         Top             =   900
         Width           =   1395
      End
      Begin VB.TextBox txtSearch 
         DataField       =   "serialnumber"
         Height          =   285
         Index           =   8
         Left            =   5520
         TabIndex        =   23
         Top             =   300
         Width           =   1395
      End
      Begin VB.TextBox txtSearch 
         DataField       =   "description"
         Height          =   285
         Index           =   7
         Left            =   4020
         TabIndex        =   21
         Top             =   900
         Width           =   1395
      End
      Begin VB.TextBox txtSearch 
         DataField       =   "name"
         Height          =   285
         Index           =   6
         Left            =   4020
         TabIndex        =   13
         Top             =   300
         Width           =   1395
      End
      Begin VB.TextBox txtSearch 
         DataField       =   "nominalcode"
         Height          =   285
         Index           =   5
         Left            =   2880
         TabIndex        =   12
         Top             =   900
         Width           =   1035
      End
      Begin VB.TextBox txtSearch 
         DataField       =   "stockcode"
         Height          =   285
         Index           =   4
         Left            =   2880
         TabIndex        =   11
         Top             =   300
         Width           =   1035
      End
      Begin VB.TextBox txtSearch 
         DataField       =   "productcode"
         Height          =   285
         Index           =   3
         Left            =   1560
         TabIndex        =   10
         Top             =   900
         Width           =   1095
      End
      Begin VB.TextBox txtSearch 
         DataField       =   "subcategory2"
         Height          =   285
         Index           =   2
         Left            =   1560
         TabIndex        =   9
         Top             =   300
         Width           =   1215
      End
      Begin VB.TextBox txtSearch 
         DataField       =   "subcategory1"
         Height          =   285
         Index           =   1
         Left            =   60
         TabIndex        =   8
         Top             =   900
         Width           =   1395
      End
      Begin VB.TextBox txtSearch 
         DataField       =   "category"
         Height          =   285
         Index           =   0
         Left            =   60
         TabIndex        =   7
         Top             =   300
         Width           =   1395
      End
      Begin VB.Label Label1 
         Caption         =   "Barcode"
         Height          =   195
         Index           =   9
         Left            =   5520
         TabIndex        =   26
         Top             =   660
         Width           =   1035
      End
      Begin VB.Label Label1 
         Caption         =   "Serial Number"
         Height          =   195
         Index           =   8
         Left            =   5520
         TabIndex        =   24
         Top             =   60
         Width           =   1035
      End
      Begin VB.Label Label1 
         Caption         =   "Description"
         Height          =   195
         Index           =   7
         Left            =   4020
         TabIndex        =   22
         Top             =   660
         Width           =   1035
      End
      Begin VB.Label Label1 
         Caption         =   "Item Name"
         Height          =   195
         Index           =   6
         Left            =   4020
         TabIndex        =   20
         Top             =   60
         Width           =   1035
      End
      Begin VB.Label Label1 
         Caption         =   "Cost Code"
         Height          =   195
         Index           =   5
         Left            =   2880
         TabIndex        =   19
         Top             =   660
         Width           =   1035
      End
      Begin VB.Label Label1 
         Caption         =   "Stock Code"
         Height          =   195
         Index           =   4
         Left            =   2880
         TabIndex        =   18
         Top             =   60
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Product"
         Height          =   195
         Index           =   3
         Left            =   1560
         TabIndex        =   17
         Top             =   660
         Width           =   1035
      End
      Begin VB.Label Label1 
         Caption         =   "Sub Cat. 2"
         Height          =   195
         Index           =   2
         Left            =   1560
         TabIndex        =   16
         Top             =   60
         Width           =   1035
      End
      Begin VB.Label Label1 
         Caption         =   "Sub Category 1"
         Height          =   195
         Index           =   1
         Left            =   60
         TabIndex        =   15
         Top             =   660
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Category"
         Height          =   195
         Index           =   0
         Left            =   60
         TabIndex        =   14
         Top             =   60
         Width           =   1035
      End
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print"
      Height          =   495
      Left            =   5940
      TabIndex        =   5
      ToolTipText     =   "Search resource list"
      Top             =   5760
      Visible         =   0   'False
      Width           =   1215
   End
   Begin MSAdodcLib.Adodc adoRateCard 
      Height          =   330
      Left            =   5160
      Top             =   6780
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnRateCard 
      Bindings        =   "frmResourceList.frx":0000
      Height          =   1755
      Left            =   420
      TabIndex        =   4
      Top             =   2580
      Width           =   9495
      DataFieldList   =   "cetacode"
      ListAutoValidate=   0   'False
      _Version        =   196617
      BeveColorScheme =   1
      ForeColorOdd    =   14737632
      BackColorOdd    =   16744576
      RowHeight       =   423
      Columns.Count   =   9
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ratecardID"
      Columns(0).Name =   "ratecardID"
      Columns(0).Alignment=   1
      Columns(0).CaptionAlignment=   1
      Columns(0).DataField=   "ratecardID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   2831
      Columns(1).Caption=   "Code"
      Columns(1).Name =   "cetacode"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "cetacode"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "category"
      Columns(2).Name =   "category"
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "category"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   2011
      Columns(3).Caption=   "Account Code"
      Columns(3).Name =   "nominalcode"
      Columns(3).CaptionAlignment=   0
      Columns(3).DataField=   "nominalcode"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   7726
      Columns(4).Caption=   "Description"
      Columns(4).Name =   "description"
      Columns(4).CaptionAlignment=   0
      Columns(4).DataField=   "description"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "minimumcharge"
      Columns(5).Name =   "minimumcharge"
      Columns(5).Alignment=   1
      Columns(5).CaptionAlignment=   1
      Columns(5).DataField=   "minimumcharge"
      Columns(5).DataType=   6
      Columns(5).NumberFormat=   "CURRENCY"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "timeintervals"
      Columns(6).Name =   "timeintervals"
      Columns(6).Alignment=   1
      Columns(6).CaptionAlignment=   1
      Columns(6).DataField=   "timeintervals"
      Columns(6).DataType=   5
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "companyID"
      Columns(7).Name =   "companyID"
      Columns(7).Alignment=   1
      Columns(7).CaptionAlignment=   1
      Columns(7).DataField=   "companyID"
      Columns(7).DataType=   3
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Caption=   "Standard Rate (Price 0)"
      Columns(8).Name =   "rate0"
      Columns(8).Alignment=   1
      Columns(8).DataField=   "rate0"
      Columns(8).DataType=   6
      Columns(8).NumberFormat=   "�0.00"
      Columns(8).FieldLen=   256
      _ExtentX        =   16748
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "cetacode"
   End
   Begin VB.ComboBox cmbOrderBy 
      Height          =   315
      Left            =   1260
      TabIndex        =   1
      Text            =   "cmbOrderBy"
      ToolTipText     =   "Order data by "
      Top             =   5160
      Visible         =   0   'False
      Width           =   2235
   End
   Begin VB.ComboBox cmbDirection 
      Height          =   315
      ItemData        =   "frmResourceList.frx":001A
      Left            =   3540
      List            =   "frmResourceList.frx":0024
      Style           =   2  'Dropdown List
      TabIndex        =   2
      ToolTipText     =   "Ascending/Descending"
      Top             =   5160
      Visible         =   0   'False
      Width           =   1095
   End
   Begin MSAdodcLib.Adodc adoResources 
      Height          =   330
      Left            =   1140
      Top             =   6120
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdResources 
      Align           =   1  'Align Top
      Bindings        =   "frmResourceList.frx":0033
      Height          =   4995
      Left            =   0
      TabIndex        =   0
      Top             =   1335
      Width           =   13155
      _Version        =   196617
      HeadLines       =   2
      stylesets.count =   1
      stylesets(0).Name=   "notinratecard"
      stylesets(0).BackColor=   255
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmResourceList.frx":004E
      BeveColorScheme =   1
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      BackColorOdd    =   14351580
      RowHeight       =   370
      Columns.Count   =   41
      Columns(0).Width=   1296
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "resourceID"
      Columns(0).Alignment=   1
      Columns(0).CaptionAlignment=   1
      Columns(0).DataField=   "resourceID"
      Columns(0).DataType=   20
      Columns(0).FieldLen=   256
      Columns(1).Width=   2699
      Columns(1).Caption=   "Category"
      Columns(1).Name =   "category"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "category"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3016
      Columns(2).Caption=   "Sub Category 1"
      Columns(2).Name =   "subcategory1"
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "subcategory1"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   2302
      Columns(3).Caption=   "Sub Category 2"
      Columns(3).Name =   "subcategory2"
      Columns(3).CaptionAlignment=   0
      Columns(3).DataField=   "subcategory2"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   2037
      Columns(4).Caption=   "Product"
      Columns(4).Name =   "productcode"
      Columns(4).CaptionAlignment=   0
      Columns(4).DataField=   "productcode"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   1270
      Columns(5).Caption=   "Stock Code"
      Columns(5).Name =   "stockcode"
      Columns(5).CaptionAlignment=   0
      Columns(5).DataField=   "stockcode"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   2064
      Columns(6).Caption=   "Cost Code"
      Columns(6).Name =   "nominalcode"
      Columns(6).CaptionAlignment=   0
      Columns(6).DataField=   "nominalcode"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   2514
      Columns(7).Caption=   "Item Name"
      Columns(7).Name =   "name"
      Columns(7).CaptionAlignment=   0
      Columns(7).DataField=   "name"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   4974
      Columns(8).Caption=   "Description"
      Columns(8).Name =   "description"
      Columns(8).CaptionAlignment=   0
      Columns(8).DataField=   "description"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   2566
      Columns(9).Caption=   "Category Code"
      Columns(9).Name =   "categorycode"
      Columns(9).CaptionAlignment=   0
      Columns(9).DataField=   "categorycode"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   2487
      Columns(10).Caption=   "Serial Number"
      Columns(10).Name=   "serialnumber"
      Columns(10).CaptionAlignment=   0
      Columns(10).DataField=   "serialnumber"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   2461
      Columns(11).Caption=   "Barcode"
      Columns(11).Name=   "barcode"
      Columns(11).CaptionAlignment=   0
      Columns(11).DataField=   "barcode"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(12).Width=   1217
      Columns(12).Caption=   "Generic"
      Columns(12).Name=   "generic"
      Columns(12).Alignment=   1
      Columns(12).CaptionAlignment=   1
      Columns(12).DataField=   "generic"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(12).Style=   4
      Columns(13).Width=   1879
      Columns(13).Caption=   "Status"
      Columns(13).Name=   "fd_status"
      Columns(13).DataField=   "fd_status"
      Columns(13).FieldLen=   256
      Columns(14).Width=   2302
      Columns(14).Caption=   "Department"
      Columns(14).Name=   "department"
      Columns(14).CaptionAlignment=   0
      Columns(14).DataField=   "department"
      Columns(14).DataType=   8
      Columns(14).FieldLen=   256
      Columns(15).Width=   2011
      Columns(15).Caption=   "Format"
      Columns(15).Name=   "format"
      Columns(15).CaptionAlignment=   0
      Columns(15).DataField=   "format"
      Columns(15).DataType=   8
      Columns(15).FieldLen=   256
      Columns(16).Width=   2037
      Columns(16).Caption=   "Standard"
      Columns(16).Name=   "standard"
      Columns(16).CaptionAlignment=   0
      Columns(16).DataField=   "standard"
      Columns(16).DataType=   8
      Columns(16).FieldLen=   256
      Columns(17).Width=   1614
      Columns(17).Caption=   "Width"
      Columns(17).Name=   "width"
      Columns(17).Alignment=   1
      Columns(17).CaptionAlignment=   1
      Columns(17).DataField=   "width"
      Columns(17).DataType=   5
      Columns(17).FieldLen=   256
      Columns(18).Width=   1455
      Columns(18).Caption=   "Height"
      Columns(18).Name=   "height"
      Columns(18).Alignment=   1
      Columns(18).CaptionAlignment=   1
      Columns(18).DataField=   "height"
      Columns(18).DataType=   5
      Columns(18).FieldLen=   256
      Columns(19).Width=   1429
      Columns(19).Caption=   "Length"
      Columns(19).Name=   "fd_length"
      Columns(19).Alignment=   1
      Columns(19).CaptionAlignment=   1
      Columns(19).DataField=   "fd_length"
      Columns(19).DataType=   5
      Columns(19).FieldLen=   256
      Columns(20).Width=   1376
      Columns(20).Caption=   "Weight"
      Columns(20).Name=   "weight"
      Columns(20).Alignment=   1
      Columns(20).CaptionAlignment=   1
      Columns(20).DataField=   "weight"
      Columns(20).DataType=   5
      Columns(20).FieldLen=   256
      Columns(21).Width=   3200
      Columns(21).Caption=   "Current Location"
      Columns(21).Name=   "location"
      Columns(21).CaptionAlignment=   0
      Columns(21).DataField=   "location"
      Columns(21).DataType=   8
      Columns(21).FieldLen=   256
      Columns(22).Width=   1720
      Columns(22).Caption=   "Job ID"
      Columns(22).Name=   "jobID"
      Columns(22).Alignment=   1
      Columns(22).DataField=   "jobID"
      Columns(22).DataType=   9
      Columns(22).FieldLen=   256
      Columns(23).Width=   3200
      Columns(23).Caption=   "Additional Information"
      Columns(23).Name=   "additionalinformation"
      Columns(23).CaptionAlignment=   0
      Columns(23).DataField=   "additionalinformation"
      Columns(23).DataType=   8
      Columns(23).FieldLen=   256
      Columns(24).Width=   1826
      Columns(24).Caption=   "PAT Test Date"
      Columns(24).Name=   "patdate"
      Columns(24).DataField=   "Column 40"
      Columns(24).DataType=   7
      Columns(24).FieldLen=   256
      Columns(25).Width=   1826
      Columns(25).Caption=   "Purchase Price"
      Columns(25).Name=   "purchaseprice"
      Columns(25).Alignment=   1
      Columns(25).CaptionAlignment=   1
      Columns(25).DataField=   "purchaseprice"
      Columns(25).DataType=   6
      Columns(25).NumberFormat=   "CURRENCY"
      Columns(25).FieldLen=   256
      Columns(26).Width=   1958
      Columns(26).Caption=   "Purchased Date"
      Columns(26).Name=   "purchasedate"
      Columns(26).Alignment=   1
      Columns(26).CaptionAlignment=   1
      Columns(26).DataField=   "purchasedate"
      Columns(26).DataType=   135
      Columns(26).FieldLen=   256
      Columns(27).Width=   1826
      Columns(27).Caption=   "Current Value"
      Columns(27).Name=   "currentvalue"
      Columns(27).Alignment=   1
      Columns(27).CaptionAlignment=   1
      Columns(27).DataField=   "currentvalue"
      Columns(27).DataType=   6
      Columns(27).NumberFormat=   "CURRENCY"
      Columns(27).FieldLen=   256
      Columns(28).Width=   1720
      Columns(28).Caption=   "Valued Date"
      Columns(28).Name=   "currentvaluedate"
      Columns(28).Alignment=   1
      Columns(28).CaptionAlignment=   1
      Columns(28).DataField=   "currentvaluedate"
      Columns(28).DataType=   135
      Columns(28).FieldLen=   256
      Columns(29).Width=   1852
      Columns(29).Caption=   "Insurance Value"
      Columns(29).Name=   "insurancevalue"
      Columns(29).Alignment=   1
      Columns(29).CaptionAlignment=   1
      Columns(29).DataField=   "insurancevalue"
      Columns(29).DataType=   6
      Columns(29).NumberFormat=   "CURRENCY"
      Columns(29).FieldLen=   256
      Columns(30).Width=   1879
      Columns(30).Caption=   "Insurance Valued Date"
      Columns(30).Name=   "insurancevaluedate"
      Columns(30).Alignment=   1
      Columns(30).CaptionAlignment=   1
      Columns(30).DataField=   "insurancevaluedate"
      Columns(30).DataType=   135
      Columns(30).FieldLen=   256
      Columns(31).Width=   1773
      Columns(31).Caption=   "Customs Value"
      Columns(31).Name=   "customsvalue"
      Columns(31).Alignment=   1
      Columns(31).CaptionAlignment=   1
      Columns(31).DataField=   "customsvalue"
      Columns(31).DataType=   6
      Columns(31).NumberFormat=   "CURRENCY"
      Columns(31).FieldLen=   256
      Columns(32).Width=   1905
      Columns(32).Caption=   "Customs Valued Date"
      Columns(32).Name=   "customesvaluedate"
      Columns(32).Alignment=   1
      Columns(32).CaptionAlignment=   1
      Columns(32).DataField=   "customesvaluedate"
      Columns(32).DataType=   135
      Columns(32).FieldLen=   256
      Columns(33).Width=   3572
      Columns(33).Caption=   "Alternative Products"
      Columns(33).Name=   "alternativeproductcodes"
      Columns(33).CaptionAlignment=   0
      Columns(33).DataField=   "alternativeproductcodes"
      Columns(33).FieldLen=   256
      Columns(34).Width=   4710
      Columns(34).Caption=   "Notes"
      Columns(34).Name=   "notes"
      Columns(34).CaptionAlignment=   0
      Columns(34).DataField=   "notes"
      Columns(34).DataType=   8
      Columns(34).FieldLen=   256
      Columns(35).Width=   2037
      Columns(35).Caption=   "Created Date"
      Columns(35).Name=   "cdate"
      Columns(35).Alignment=   1
      Columns(35).CaptionAlignment=   1
      Columns(35).DataField=   "cdate"
      Columns(35).DataType=   135
      Columns(35).FieldLen=   256
      Columns(36).Width=   1429
      Columns(36).Caption=   "Created By"
      Columns(36).Name=   "cuser"
      Columns(36).CaptionAlignment=   0
      Columns(36).DataField=   "cuser"
      Columns(36).DataType=   8
      Columns(36).FieldLen=   256
      Columns(37).Width=   2037
      Columns(37).Caption=   "Modified Date"
      Columns(37).Name=   "mdate"
      Columns(37).Alignment=   1
      Columns(37).CaptionAlignment=   1
      Columns(37).DataField=   "mdate"
      Columns(37).DataType=   7
      Columns(37).FieldLen=   256
      Columns(38).Width=   1482
      Columns(38).Caption=   "Modified By"
      Columns(38).Name=   "muser"
      Columns(38).CaptionAlignment=   0
      Columns(38).DataField=   "muser"
      Columns(38).DataType=   8
      Columns(38).FieldLen=   256
      Columns(39).Width=   1482
      Columns(39).Caption=   "Web Info ID"
      Columns(39).Name=   "webinfoID"
      Columns(39).Alignment=   1
      Columns(39).CaptionAlignment=   1
      Columns(39).DataField=   "webinfoID"
      Columns(39).DataType=   20
      Columns(39).FieldLen=   256
      Columns(40).Width=   3200
      Columns(40).Caption=   "Master Resource ID"
      Columns(40).Name=   "masterresourceID"
      Columns(40).DataField=   "masterresourceID"
      Columns(40).FieldLen=   256
      UseDefaults     =   0   'False
      _ExtentX        =   23204
      _ExtentY        =   8811
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order By"
      Height          =   255
      Index           =   20
      Left            =   120
      TabIndex        =   3
      Top             =   5160
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "frmResourceList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdPrint_Click()

If Not CheckAccess("/printresourcelist") Then Exit Sub

Dim l_strReportToPrint As String

l_strReportToPrint = g_strLocationOfCrystalReportFiles & "resourcelist.rpt"
PrintCrystalReport l_strReportToPrint, "", g_blnPreviewReport

End Sub

Private Sub cmdSearch_Click()


Dim l_strWhere As String
l_strWhere = " WHERE resourceID > 0 "

Dim l_intLoop As Integer

For l_intLoop = 0 To 9
    If txtSearch(l_intLoop).Text <> "" Then l_strWhere = l_strWhere & " AND " & txtSearch(l_intLoop).DataField & " LIKE '" & QuoteSanitise(txtSearch(l_intLoop).Text) & "%'"
Next

If chkShowSoldItems.Value = 0 Then l_strWhere = l_strWhere & " AND (fd_status IS NULL or fd_status <> 'Sold') "

If chkShowDeletedItems.Value = 0 Then l_strWhere = l_strWhere & " AND (fd_status IS NULL or fd_status <> 'DELETED') "



adoResources.ConnectionString = g_strConnection
adoResources.RecordSource = "SELECT * FROM resource " & l_strWhere & " ORDER BY " & cmbOrderBy.Text & " " & cmbDirection.Text & " " & IIf(LCase(cmbOrderBy.Text) <> "category", ",category", "") & IIf(LCase(cmbOrderBy.Text) <> "subcategory1", ",subcategory1", "") & IIf(LCase(cmbOrderBy.Text) <> "subcategory2", ",subcategory2", "") & IIf(LCase(cmbOrderBy.Text) <> "categorycode", ",categorycode", "") & IIf(LCase(cmbOrderBy.Text) <> "name", ",name", "")
adoResources.Refresh

End Sub

Private Sub Form_Load()
cmbOrderBy.Text = "Category"
cmbDirection.ListIndex = 1
cmdSearch_Click

l_strSQL = "SELECT * FROM ratecard ORDER BY nominalcode, cetacode"
adoRateCard.ConnectionString = g_strConnection
adoRateCard.RecordSource = l_strSQL
adoRateCard.Refresh

grdResources.Columns("purchaseprice").NumberFormat = g_strCurrency & "0.00"
grdResources.Columns("currentvalue").NumberFormat = g_strCurrency & "0.00"
grdResources.Columns("insurancevalue").NumberFormat = g_strCurrency & "0.00"
grdResources.Columns("customsvalue").NumberFormat = g_strCurrency & "0.00"

End Sub

Private Sub Form_Resize()
On Error Resume Next
grdResources.Height = Me.ScaleHeight - picHeader.Height

End Sub

Private Sub grdResources_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

If Not CheckAccess("/deleteresource") Then
    Cancel = True
    Exit Sub
End If

Dim l_intMsg As Integer
l_intMsg = MsgBox("Are you sure you want to mark this resource as deleted?", vbQuestion + vbOKCancel)
If l_intMsg = vbCancel Then Exit Sub

Dim l_lngResourceID As Long
l_lngResourceID = Val(grdResources.Columns("resourceID").Text)

If l_lngResourceID = 0 Then Exit Sub

Dim l_strSQL As String
l_strSQL = "UPDATE resource SET fd_status = 'DELETED' WHERE resourceID = '" & l_lngResourceID & "';"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

Cancel = True

On Error Resume Next
grdResources.Row = grdResources.Row - 1

Dim l_lngBookmark As Long
l_lngBookmark = grdResources.Bookmark

adoResources.Refresh

grdResources.Bookmark = l_lngBookmark



End Sub

Private Sub grdResources_BtnClick()
Select Case grdResources.Columns(grdResources.Col).Name
Case "generic"
    Select Case Trim(UCase(grdResources.Columns("generic").Text))
        Case ""
            grdResources.Columns("generic").Value = "Yes"
        Case "NO"
            grdResources.Columns("generic").Value = "Yes"
        Case "YES"
            grdResources.Columns("generic").Value = "No"
    End Select
    
    grdResources.Update
    
End Select

End Sub

Private Sub grdResources_HeadClick(ByVal ColIndex As Integer)
If cmbOrderBy.Text <> grdResources.Columns(ColIndex).DataField Then
    cmbOrderBy.Text = grdResources.Columns(ColIndex).DataField
Else
    cmbDirection.ListIndex = 1 - cmbDirection.ListIndex
End If

cmdSearch_Click
End Sub

Private Sub grdResources_InitColumnProps()
grdResources.Columns("nominalcode").DropDownHwnd = ddnRateCard.hWnd

End Sub

Private Sub grdResources_RowLoaded(ByVal Bookmark As Variant)
'check to see if the cost code is in the ratecard.

Dim l_strCostCode As String
l_strCostCode = grdResources.Columns("nominalcode").Text


If grdResources.Columns("fd_status").Text = "DELETED" Then
    grdResources.Columns("fd_status").CellStyleSet "notinratecard"
End If

If l_strCostCode = "" Or UCase(l_strCostCode) = "NO COST" Then Exit Sub

Dim l_lngCostCodeID As Long
l_lngCostCodeID = GetData("ratecard", "ratecardID", "cetacode", l_strCostCode)

If l_lngCostCodeID = 0 Then
    grdResources.Columns("nominalcode").CellStyleSet "notinratecard"
End If



End Sub


