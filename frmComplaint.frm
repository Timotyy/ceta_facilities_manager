VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmComplaint 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Issues Log"
   ClientHeight    =   14865
   ClientLeft      =   2370
   ClientTop       =   3240
   ClientWidth     =   28590
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmComplaint.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   14865
   ScaleWidth      =   28590
   WindowState     =   2  'Maximized
   Begin VB.ComboBox cmbSearchIssueType 
      BackColor       =   &H00FFC0FF&
      Height          =   315
      Left            =   25260
      TabIndex        =   368
      ToolTipText     =   "The machine the Tape was reviewed on"
      Top             =   4380
      Width           =   2835
   End
   Begin VB.PictureBox picDetails 
      Height          =   13095
      Left            =   600
      ScaleHeight     =   13035
      ScaleWidth      =   12315
      TabIndex        =   31
      Top             =   1020
      Visible         =   0   'False
      Width           =   12375
      Begin VB.CommandButton cmdHide 
         Caption         =   "Hide"
         Height          =   315
         Left            =   10980
         TabIndex        =   340
         ToolTipText     =   "Close this form"
         Top             =   12540
         Width           =   1155
      End
      Begin TabDlg.SSTab tabComplaintType 
         Height          =   12735
         Left            =   180
         TabIndex        =   32
         Top             =   120
         Width           =   10725
         _ExtentX        =   18918
         _ExtentY        =   22463
         _Version        =   393216
         Tabs            =   6
         Tab             =   5
         TabsPerRow      =   6
         TabHeight       =   520
         TabCaption(0)   =   "Normal Jobs"
         TabPicture(0)   =   "frmComplaint.frx":6852
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "lblCaption(20)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblCaption(15)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblCaption(8)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblCaption(6)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblCaption(3)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblCaption(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblCaption(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblCaption(2)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblCaption(5)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblCompanyID(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblContactID(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblCaption(18)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblCaption(54)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblCaption(58)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblCaption(90)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblRRContactID(0)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "lblCaption(93)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "lblRRContactEmail(0)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "lblCaption(112)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "lblCaption(4)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "lblCaption(126)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "cmbCetaUser(0)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "cmbContact(0)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "cmbCompany(0)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "datJobCompletedDate(0)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtConclusion(0)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtOrderRef(0)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtOriginalJobID(0)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtTitle(0)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "optFindings(0)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "optFindings(1)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "optFindings(2)"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).Control(32)=   "optFindings(3)"
         Tab(0).Control(32).Enabled=   0   'False
         Tab(0).Control(33)=   "optFindings(4)"
         Tab(0).Control(33).Enabled=   0   'False
         Tab(0).Control(34)=   "cmbOriginalOperator(0)"
         Tab(0).Control(34).Enabled=   0   'False
         Tab(0).Control(35)=   "cmbAssignedTo(0)"
         Tab(0).Control(35).Enabled=   0   'False
         Tab(0).Control(36)=   "txtTrackerFindings(0)"
         Tab(0).Control(36).Enabled=   0   'False
         Tab(0).Control(37)=   "txtRejectionIssue(0)"
         Tab(0).Control(37).Enabled=   0   'False
         Tab(0).Control(38)=   "cmbSignoff(0)"
         Tab(0).Control(38).Enabled=   0   'False
         Tab(0).Control(39)=   "txtOriginalJobdetailID(0)"
         Tab(0).Control(39).Enabled=   0   'False
         Tab(0).Control(40)=   "cmdCreateRedoJob(0)"
         Tab(0).Control(40).Enabled=   0   'False
         Tab(0).Control(41)=   "cmbInternalDepartment(0)"
         Tab(0).Control(41).Enabled=   0   'False
         Tab(0).Control(42)=   "optFindings(5)"
         Tab(0).Control(42).Enabled=   0   'False
         Tab(0).Control(43)=   "cmbInternalExternal(0)"
         Tab(0).Control(43).Enabled=   0   'False
         Tab(0).Control(44)=   "cmdEmailOriginalOperator(0)"
         Tab(0).Control(44).Enabled=   0   'False
         Tab(0).Control(45)=   "cmbIssueType(0)"
         Tab(0).Control(45).Enabled=   0   'False
         Tab(0).ControlCount=   46
         TabCaption(1)   =   "DADC Tracker"
         TabPicture(1)   =   "frmComplaint.frx":686E
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lblCaption(21)"
         Tab(1).Control(1)=   "lblOriginalAlphaCode"
         Tab(1).Control(2)=   "lblCaption(22)"
         Tab(1).Control(3)=   "lblCaption(23)"
         Tab(1).Control(4)=   "lblCaption(24)"
         Tab(1).Control(5)=   "lblCaption(25)"
         Tab(1).Control(6)=   "lblCaption(26)"
         Tab(1).Control(7)=   "lblCaption(27)"
         Tab(1).Control(8)=   "lblCaption(28)"
         Tab(1).Control(9)=   "lblCaption(29)"
         Tab(1).Control(10)=   "lblCaption(30)"
         Tab(1).Control(11)=   "lblCaption(31)"
         Tab(1).Control(12)=   "lblCaption(32)"
         Tab(1).Control(13)=   "lblCaption(33)"
         Tab(1).Control(14)=   "lblCaption(34)"
         Tab(1).Control(15)=   "lblCaption(35)"
         Tab(1).Control(16)=   "lblCaption(36)"
         Tab(1).Control(17)=   "lblOriginalXmlDeliveryDate(0)"
         Tab(1).Control(18)=   "lblBarcode"
         Tab(1).Control(19)=   "lblCaption(38)"
         Tab(1).Control(20)=   "lblMasterFormat"
         Tab(1).Control(21)=   "lblOriginalIngestDate(0)"
         Tab(1).Control(22)=   "lblOriginalEpisode(0)"
         Tab(1).Control(23)=   "lblFileStore(0)"
         Tab(1).Control(24)=   "lblDecisionTreeDate(0)"
         Tab(1).Control(25)=   "lblOffPendingDate"
         Tab(1).Control(26)=   "lblFixJobID(0)"
         Tab(1).Control(27)=   "lblOriginalSubtitle(0)"
         Tab(1).Control(28)=   "lblOriginalTitle(0)"
         Tab(1).Control(29)=   "lblReference(0)"
         Tab(1).Control(30)=   "lblCaption(37)"
         Tab(1).Control(31)=   "lblContentVersionCode"
         Tab(1).Control(32)=   "lblCaption(55)"
         Tab(1).Control(33)=   "lblCaption(72)"
         Tab(1).Control(34)=   "lblRRContactEmail(1)"
         Tab(1).Control(35)=   "lblCaption(94)"
         Tab(1).Control(36)=   "lblRRContactID(1)"
         Tab(1).Control(37)=   "lblCaption(105)"
         Tab(1).Control(38)=   "lblCaption(106)"
         Tab(1).Control(39)=   "lblCaption(107)"
         Tab(1).Control(40)=   "lblCaption(108)"
         Tab(1).Control(41)=   "lblCaption(109)"
         Tab(1).Control(42)=   "lblCaption(110)"
         Tab(1).Control(43)=   "lblCaption(111)"
         Tab(1).Control(44)=   "lblCaption(113)"
         Tab(1).Control(45)=   "lblCaption(80)"
         Tab(1).Control(46)=   "lblCaption(128)"
         Tab(1).Control(47)=   "datMasterArrived3(1)"
         Tab(1).Control(48)=   "datMasterArrived2(1)"
         Tab(1).Control(49)=   "datTargetDate(1)"
         Tab(1).Control(50)=   "datMasterArrived(1)"
         Tab(1).Control(51)=   "cmbCetaUser(1)"
         Tab(1).Control(52)=   "txtRejectionIssue(1)"
         Tab(1).Control(53)=   "cmbAssignedTo(1)"
         Tab(1).Control(54)=   "txtTrackerFindings(1)"
         Tab(1).Control(55)=   "optFindings1(0)"
         Tab(1).Control(56)=   "optFindings1(1)"
         Tab(1).Control(57)=   "optFindings1(2)"
         Tab(1).Control(58)=   "optFindings1(3)"
         Tab(1).Control(59)=   "optFindings1(4)"
         Tab(1).Control(60)=   "cmdCreateRedoTrackerTask(0)"
         Tab(1).Control(61)=   "optFindings1(5)"
         Tab(1).Control(62)=   "optFindings1(6)"
         Tab(1).Control(63)=   "cmbSignoff(1)"
         Tab(1).Control(64)=   "optFindings1(7)"
         Tab(1).Control(65)=   "optFindings1(8)"
         Tab(1).Control(66)=   "cmbOriginalOperator(1)"
         Tab(1).Control(67)=   "txtAffinityCaseNumber(1)"
         Tab(1).Control(68)=   "txtSourceID3(1)"
         Tab(1).Control(69)=   "txtSourceID2(1)"
         Tab(1).Control(70)=   "txtSourceID(1)"
         Tab(1).Control(71)=   "cmbInternalDepartment(1)"
         Tab(1).Control(72)=   "optFindings1(9)"
         Tab(1).Control(73)=   "cmbInternalExternal(1)"
         Tab(1).Control(74)=   "cmdEmailOriginalOperator(1)"
         Tab(1).Control(75)=   "cmbIssueType(1)"
         Tab(1).ControlCount=   76
         TabCaption(2)   =   "Svensk Tracker"
         TabPicture(2)   =   "frmComplaint.frx":688A
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "lblReference(1)"
         Tab(2).Control(0).Enabled=   0   'False
         Tab(2).Control(1)=   "lblOriginalTitle(1)"
         Tab(2).Control(1).Enabled=   0   'False
         Tab(2).Control(2)=   "lblOriginalSubtitle(1)"
         Tab(2).Control(2).Enabled=   0   'False
         Tab(2).Control(3)=   "lblFixJobID(1)"
         Tab(2).Control(3).Enabled=   0   'False
         Tab(2).Control(4)=   "lblDecisionTreeDate(1)"
         Tab(2).Control(4).Enabled=   0   'False
         Tab(2).Control(5)=   "lblOriginalEpisode(1)"
         Tab(2).Control(5).Enabled=   0   'False
         Tab(2).Control(6)=   "lblOriginalIngestDate(1)"
         Tab(2).Control(6).Enabled=   0   'False
         Tab(2).Control(7)=   "lblCaption(12)"
         Tab(2).Control(7).Enabled=   0   'False
         Tab(2).Control(8)=   "lblOriginalXmlDeliveryDate(1)"
         Tab(2).Control(8).Enabled=   0   'False
         Tab(2).Control(9)=   "lblCaption(16)"
         Tab(2).Control(9).Enabled=   0   'False
         Tab(2).Control(10)=   "lblCaption(17)"
         Tab(2).Control(10).Enabled=   0   'False
         Tab(2).Control(11)=   "lblCaption(19)"
         Tab(2).Control(11).Enabled=   0   'False
         Tab(2).Control(12)=   "lblCaption(39)"
         Tab(2).Control(12).Enabled=   0   'False
         Tab(2).Control(13)=   "lblCaption(40)"
         Tab(2).Control(13).Enabled=   0   'False
         Tab(2).Control(14)=   "lblCaption(41)"
         Tab(2).Control(14).Enabled=   0   'False
         Tab(2).Control(15)=   "lblCaption(42)"
         Tab(2).Control(15).Enabled=   0   'False
         Tab(2).Control(16)=   "lblCaption(43)"
         Tab(2).Control(16).Enabled=   0   'False
         Tab(2).Control(17)=   "lblCaption(44)"
         Tab(2).Control(17).Enabled=   0   'False
         Tab(2).Control(18)=   "lblCaption(45)"
         Tab(2).Control(18).Enabled=   0   'False
         Tab(2).Control(19)=   "lblCaption(46)"
         Tab(2).Control(19).Enabled=   0   'False
         Tab(2).Control(20)=   "lblCaption(50)"
         Tab(2).Control(20).Enabled=   0   'False
         Tab(2).Control(21)=   "lblOriginalSeries(1)"
         Tab(2).Control(21).Enabled=   0   'False
         Tab(2).Control(22)=   "lblCaption(56)"
         Tab(2).Control(22).Enabled=   0   'False
         Tab(2).Control(23)=   "lblRRContactEmail(2)"
         Tab(2).Control(23).Enabled=   0   'False
         Tab(2).Control(24)=   "lblCaption(95)"
         Tab(2).Control(24).Enabled=   0   'False
         Tab(2).Control(25)=   "lblRRContactID(2)"
         Tab(2).Control(25).Enabled=   0   'False
         Tab(2).Control(26)=   "lblCaption(114)"
         Tab(2).Control(26).Enabled=   0   'False
         Tab(2).Control(27)=   "lblCaption(120)"
         Tab(2).Control(27).Enabled=   0   'False
         Tab(2).Control(28)=   "lblCaption(129)"
         Tab(2).Control(28).Enabled=   0   'False
         Tab(2).Control(29)=   "cmbCetaUser(2)"
         Tab(2).Control(29).Enabled=   0   'False
         Tab(2).Control(30)=   "cmbSignoff(2)"
         Tab(2).Control(30).Enabled=   0   'False
         Tab(2).Control(31)=   "txtTrackerFindings(2)"
         Tab(2).Control(31).Enabled=   0   'False
         Tab(2).Control(32)=   "cmbAssignedTo(2)"
         Tab(2).Control(32).Enabled=   0   'False
         Tab(2).Control(33)=   "txtRejectionIssue(2)"
         Tab(2).Control(33).Enabled=   0   'False
         Tab(2).Control(34)=   "optFindings2(7)"
         Tab(2).Control(34).Enabled=   0   'False
         Tab(2).Control(35)=   "optFindings2(6)"
         Tab(2).Control(35).Enabled=   0   'False
         Tab(2).Control(36)=   "optFindings2(5)"
         Tab(2).Control(36).Enabled=   0   'False
         Tab(2).Control(37)=   "optFindings2(4)"
         Tab(2).Control(37).Enabled=   0   'False
         Tab(2).Control(38)=   "optFindings2(3)"
         Tab(2).Control(38).Enabled=   0   'False
         Tab(2).Control(39)=   "optFindings2(2)"
         Tab(2).Control(39).Enabled=   0   'False
         Tab(2).Control(40)=   "optFindings2(1)"
         Tab(2).Control(40).Enabled=   0   'False
         Tab(2).Control(41)=   "optFindings2(0)"
         Tab(2).Control(41).Enabled=   0   'False
         Tab(2).Control(42)=   "optFindings2(8)"
         Tab(2).Control(42).Enabled=   0   'False
         Tab(2).Control(43)=   "cmbOriginalOperator(2)"
         Tab(2).Control(43).Enabled=   0   'False
         Tab(2).Control(44)=   "cmdCreateRedoTrackerTask(1)"
         Tab(2).Control(44).Enabled=   0   'False
         Tab(2).Control(45)=   "cmbInternalDepartment(2)"
         Tab(2).Control(45).Enabled=   0   'False
         Tab(2).Control(46)=   "optFindings2(9)"
         Tab(2).Control(46).Enabled=   0   'False
         Tab(2).Control(47)=   "cmbInternalExternal(2)"
         Tab(2).Control(47).Enabled=   0   'False
         Tab(2).Control(48)=   "cmdEmailOriginalOperator(2)"
         Tab(2).Control(48).Enabled=   0   'False
         Tab(2).Control(49)=   "cmbIssueType(2)"
         Tab(2).Control(49).Enabled=   0   'False
         Tab(2).ControlCount=   50
         TabCaption(3)   =   "DSP Tracker"
         TabPicture(3)   =   "frmComplaint.frx":68A6
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "lblCaption(7)"
         Tab(3).Control(1)=   "lblCaption(9)"
         Tab(3).Control(2)=   "lblCaption(10)"
         Tab(3).Control(3)=   "lblCaption(11)"
         Tab(3).Control(4)=   "lblCaption(14)"
         Tab(3).Control(5)=   "lblCaption(47)"
         Tab(3).Control(6)=   "lblOriginalEpisode(2)"
         Tab(3).Control(7)=   "lblOriginalSubtitle(2)"
         Tab(3).Control(8)=   "lblOriginalTitle(2)"
         Tab(3).Control(9)=   "lblReference(2)"
         Tab(3).Control(10)=   "lblCaption(48)"
         Tab(3).Control(11)=   "lblOriginalSeries(2)"
         Tab(3).Control(12)=   "lblOriginalVolume(2)"
         Tab(3).Control(13)=   "lblCaption(49)"
         Tab(3).Control(14)=   "lblOriginalOrderNumber(2)"
         Tab(3).Control(15)=   "lblOriginaliTunesVendorID(2)"
         Tab(3).Control(16)=   "lblCaption(51)"
         Tab(3).Control(17)=   "lblCaption(52)"
         Tab(3).Control(18)=   "lblCaption(53)"
         Tab(3).Control(19)=   "lblCaption(57)"
         Tab(3).Control(20)=   "lblRRContactEmail(3)"
         Tab(3).Control(21)=   "lblCaption(96)"
         Tab(3).Control(22)=   "lblRRContactID(3)"
         Tab(3).Control(23)=   "lblCaption(115)"
         Tab(3).Control(24)=   "lblCaption(121)"
         Tab(3).Control(25)=   "lblCaption(130)"
         Tab(3).Control(26)=   "cmbCetaUser(3)"
         Tab(3).Control(27)=   "optFindings3(8)"
         Tab(3).Control(28)=   "optFindings3(0)"
         Tab(3).Control(29)=   "optFindings3(1)"
         Tab(3).Control(30)=   "optFindings3(2)"
         Tab(3).Control(31)=   "optFindings3(3)"
         Tab(3).Control(32)=   "optFindings3(4)"
         Tab(3).Control(33)=   "optFindings3(5)"
         Tab(3).Control(34)=   "optFindings3(6)"
         Tab(3).Control(35)=   "optFindings3(7)"
         Tab(3).Control(36)=   "txtRejectionIssue(3)"
         Tab(3).Control(37)=   "cmbAssignedTo(3)"
         Tab(3).Control(38)=   "txtTrackerFindings(3)"
         Tab(3).Control(39)=   "cmbSignoff(3)"
         Tab(3).Control(40)=   "cmbOriginalOperator(3)"
         Tab(3).Control(41)=   "cmdCreateRedoTrackerTask(2)"
         Tab(3).Control(42)=   "cmbInternalDepartment(3)"
         Tab(3).Control(43)=   "optFindings3(9)"
         Tab(3).Control(44)=   "cmbInternalExternal(3)"
         Tab(3).Control(45)=   "cmdEmailOriginalOperator(3)"
         Tab(3).Control(46)=   "cmbIssueType(3)"
         Tab(3).ControlCount=   47
         TabCaption(4)   =   "DADC (Non-MX1)"
         TabPicture(4)   =   "frmComplaint.frx":68C2
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "lblCaption(59)"
         Tab(4).Control(1)=   "lblCaption(60)"
         Tab(4).Control(2)=   "lblContactID(4)"
         Tab(4).Control(3)=   "lblCaption(61)"
         Tab(4).Control(4)=   "lblCompanyID(4)"
         Tab(4).Control(5)=   "lblCaption(62)"
         Tab(4).Control(6)=   "lblCaption(63)"
         Tab(4).Control(7)=   "lblCaption(64)"
         Tab(4).Control(8)=   "lblCaption(65)"
         Tab(4).Control(9)=   "lblCaption(66)"
         Tab(4).Control(10)=   "lblCaption(67)"
         Tab(4).Control(11)=   "lblCaption(68)"
         Tab(4).Control(12)=   "lblCaption(69)"
         Tab(4).Control(13)=   "lblCaption(70)"
         Tab(4).Control(14)=   "lblCaption(71)"
         Tab(4).Control(15)=   "lblRRContactEmail(4)"
         Tab(4).Control(16)=   "lblCaption(97)"
         Tab(4).Control(17)=   "lblRRContactID(4)"
         Tab(4).Control(18)=   "lblCaption(100)"
         Tab(4).Control(19)=   "lblCaption(101)"
         Tab(4).Control(20)=   "lblCaption(102)"
         Tab(4).Control(21)=   "lblCaption(103)"
         Tab(4).Control(22)=   "lblCaption(104)"
         Tab(4).Control(23)=   "datMasterArrived3(0)"
         Tab(4).Control(24)=   "datMasterArrived2(0)"
         Tab(4).Control(25)=   "datTargetDate(0)"
         Tab(4).Control(26)=   "cmbCetaUser(4)"
         Tab(4).Control(27)=   "datSignOff"
         Tab(4).Control(28)=   "datInvestigated"
         Tab(4).Control(29)=   "datMasterArrived(0)"
         Tab(4).Control(30)=   "datFileArrived"
         Tab(4).Control(31)=   "cmbContact(4)"
         Tab(4).Control(32)=   "txtRejectionIssue(4)"
         Tab(4).Control(33)=   "txtTitle(4)"
         Tab(4).Control(34)=   "cmbAssignedTo(4)"
         Tab(4).Control(35)=   "txtTrackerFindings(4)"
         Tab(4).Control(36)=   "optFindings(10)"
         Tab(4).Control(37)=   "optFindings(9)"
         Tab(4).Control(38)=   "optFindings(7)"
         Tab(4).Control(39)=   "optFindings(8)"
         Tab(4).Control(40)=   "cmbSignoff(4)"
         Tab(4).Control(41)=   "txtSourceID(0)"
         Tab(4).Control(42)=   "txtAffinityCaseNumber(4)"
         Tab(4).Control(43)=   "txtContentVersionCode"
         Tab(4).Control(44)=   "fraBilling"
         Tab(4).Control(45)=   "txtSourceID2(0)"
         Tab(4).Control(46)=   "txtSourceID3(0)"
         Tab(4).ControlCount=   47
         TabCaption(5)   =   "BBCWW Tracker"
         TabPicture(5)   =   "frmComplaint.frx":68DE
         Tab(5).ControlEnabled=   -1  'True
         Tab(5).Control(0)=   "lblContactID(1)"
         Tab(5).Control(0).Enabled=   0   'False
         Tab(5).Control(1)=   "lblCompanyID(1)"
         Tab(5).Control(1).Enabled=   0   'False
         Tab(5).Control(2)=   "lblCaption(73)"
         Tab(5).Control(2).Enabled=   0   'False
         Tab(5).Control(3)=   "lblCaption(74)"
         Tab(5).Control(3).Enabled=   0   'False
         Tab(5).Control(4)=   "lblCaption(75)"
         Tab(5).Control(4).Enabled=   0   'False
         Tab(5).Control(5)=   "lblCaption(76)"
         Tab(5).Control(5).Enabled=   0   'False
         Tab(5).Control(6)=   "lblCaption(77)"
         Tab(5).Control(6).Enabled=   0   'False
         Tab(5).Control(7)=   "lblCaption(78)"
         Tab(5).Control(7).Enabled=   0   'False
         Tab(5).Control(8)=   "lblCaption(79)"
         Tab(5).Control(8).Enabled=   0   'False
         Tab(5).Control(9)=   "lblCaption(81)"
         Tab(5).Control(9).Enabled=   0   'False
         Tab(5).Control(10)=   "lblCaption(82)"
         Tab(5).Control(10).Enabled=   0   'False
         Tab(5).Control(11)=   "lblCaption(83)"
         Tab(5).Control(11).Enabled=   0   'False
         Tab(5).Control(12)=   "lblCaption(84)"
         Tab(5).Control(12).Enabled=   0   'False
         Tab(5).Control(13)=   "lblCaption(85)"
         Tab(5).Control(13).Enabled=   0   'False
         Tab(5).Control(14)=   "lblCaption(86)"
         Tab(5).Control(14).Enabled=   0   'False
         Tab(5).Control(15)=   "lblCaption(87)"
         Tab(5).Control(15).Enabled=   0   'False
         Tab(5).Control(16)=   "lblCaption(88)"
         Tab(5).Control(16).Enabled=   0   'False
         Tab(5).Control(17)=   "lblCaption(89)"
         Tab(5).Control(17).Enabled=   0   'False
         Tab(5).Control(18)=   "lblCaption(91)"
         Tab(5).Control(18).Enabled=   0   'False
         Tab(5).Control(19)=   "lblRRContactEmail(5)"
         Tab(5).Control(19).Enabled=   0   'False
         Tab(5).Control(20)=   "lblCaption(98)"
         Tab(5).Control(20).Enabled=   0   'False
         Tab(5).Control(21)=   "lblRRContactID(5)"
         Tab(5).Control(21).Enabled=   0   'False
         Tab(5).Control(22)=   "lblCaption(116)"
         Tab(5).Control(22).Enabled=   0   'False
         Tab(5).Control(23)=   "lblCaption(122)"
         Tab(5).Control(23).Enabled=   0   'False
         Tab(5).Control(24)=   "lblCaption(131)"
         Tab(5).Control(24).Enabled=   0   'False
         Tab(5).Control(25)=   "cmbCetaUser(5)"
         Tab(5).Control(25).Enabled=   0   'False
         Tab(5).Control(26)=   "datJobCompletedDate(1)"
         Tab(5).Control(26).Enabled=   0   'False
         Tab(5).Control(27)=   "cmbContact(1)"
         Tab(5).Control(27).Enabled=   0   'False
         Tab(5).Control(28)=   "cmbCompany(1)"
         Tab(5).Control(28).Enabled=   0   'False
         Tab(5).Control(29)=   "txtRejectionIssue(5)"
         Tab(5).Control(29).Enabled=   0   'False
         Tab(5).Control(30)=   "cmbOriginalOperator(4)"
         Tab(5).Control(30).Enabled=   0   'False
         Tab(5).Control(31)=   "txtTitle(1)"
         Tab(5).Control(31).Enabled=   0   'False
         Tab(5).Control(32)=   "txtOriginalJobID(1)"
         Tab(5).Control(32).Enabled=   0   'False
         Tab(5).Control(33)=   "txtOrderRef(1)"
         Tab(5).Control(33).Enabled=   0   'False
         Tab(5).Control(34)=   "txtTrackerFindings(5)"
         Tab(5).Control(34).Enabled=   0   'False
         Tab(5).Control(35)=   "cmbAssignedTo(5)"
         Tab(5).Control(35).Enabled=   0   'False
         Tab(5).Control(36)=   "optFindings4(4)"
         Tab(5).Control(36).Enabled=   0   'False
         Tab(5).Control(37)=   "optFindings4(3)"
         Tab(5).Control(37).Enabled=   0   'False
         Tab(5).Control(38)=   "optFindings4(2)"
         Tab(5).Control(38).Enabled=   0   'False
         Tab(5).Control(39)=   "optFindings4(1)"
         Tab(5).Control(39).Enabled=   0   'False
         Tab(5).Control(40)=   "optFindings4(0)"
         Tab(5).Control(40).Enabled=   0   'False
         Tab(5).Control(41)=   "cmbSignoff(5)"
         Tab(5).Control(41).Enabled=   0   'False
         Tab(5).Control(42)=   "txtConclusion(1)"
         Tab(5).Control(42).Enabled=   0   'False
         Tab(5).Control(43)=   "cmbRejectionCategory"
         Tab(5).Control(43).Enabled=   0   'False
         Tab(5).Control(44)=   "cmbRejectionType"
         Tab(5).Control(44).Enabled=   0   'False
         Tab(5).Control(45)=   "cmbRejectionAccountability"
         Tab(5).Control(45).Enabled=   0   'False
         Tab(5).Control(46)=   "cmbRejectionReason"
         Tab(5).Control(46).Enabled=   0   'False
         Tab(5).Control(47)=   "txtOriginalJobdetailID(1)"
         Tab(5).Control(47).Enabled=   0   'False
         Tab(5).Control(48)=   "cmdCreateRedoJob(1)"
         Tab(5).Control(48).Enabled=   0   'False
         Tab(5).Control(49)=   "cmbInternalDepartment(4)"
         Tab(5).Control(49).Enabled=   0   'False
         Tab(5).Control(50)=   "optFindings4(5)"
         Tab(5).Control(50).Enabled=   0   'False
         Tab(5).Control(51)=   "cmbInternalExternal(4)"
         Tab(5).Control(51).Enabled=   0   'False
         Tab(5).Control(52)=   "cmdEmailOriginalOperator(4)"
         Tab(5).Control(52).Enabled=   0   'False
         Tab(5).Control(53)=   "cmbIssueType(4)"
         Tab(5).Control(53).Enabled=   0   'False
         Tab(5).ControlCount=   54
         Begin VB.ComboBox cmbIssueType 
            Height          =   315
            Index           =   4
            Left            =   5040
            TabIndex        =   376
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   8160
            Width           =   3255
         End
         Begin VB.ComboBox cmbIssueType 
            Height          =   315
            Index           =   3
            Left            =   -70020
            TabIndex        =   374
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   9900
            Width           =   3255
         End
         Begin VB.ComboBox cmbIssueType 
            Height          =   315
            Index           =   2
            Left            =   -70080
            TabIndex        =   372
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   9660
            Width           =   3255
         End
         Begin VB.ComboBox cmbIssueType 
            Height          =   315
            Index           =   1
            Left            =   -67800
            TabIndex        =   370
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   11040
            Width           =   2835
         End
         Begin VB.ComboBox cmbIssueType 
            Height          =   315
            Index           =   0
            Left            =   -69960
            TabIndex        =   366
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   9420
            Width           =   2835
         End
         Begin VB.CommandButton cmdEmailOriginalOperator 
            Caption         =   "Email Original Operator"
            Height          =   315
            Index           =   4
            Left            =   6240
            TabIndex        =   350
            Top             =   4980
            Width           =   2715
         End
         Begin VB.CommandButton cmdEmailOriginalOperator 
            Caption         =   "Email Original Operator"
            Height          =   315
            Index           =   3
            Left            =   -68880
            TabIndex        =   349
            Top             =   4080
            Width           =   2715
         End
         Begin VB.CommandButton cmdEmailOriginalOperator 
            Caption         =   "Email Original Operator"
            Height          =   315
            Index           =   2
            Left            =   -68820
            TabIndex        =   348
            Top             =   3720
            Width           =   2715
         End
         Begin VB.CommandButton cmdEmailOriginalOperator 
            Caption         =   "Email Original Operator"
            Height          =   315
            Index           =   1
            Left            =   -68880
            TabIndex        =   347
            Top             =   6300
            Width           =   2715
         End
         Begin VB.CommandButton cmdEmailOriginalOperator 
            Caption         =   "Email Original Operator"
            Height          =   315
            Index           =   0
            Left            =   -68760
            TabIndex        =   346
            Top             =   5040
            Width           =   2715
         End
         Begin VB.ComboBox cmbInternalExternal 
            Height          =   315
            Index           =   4
            Left            =   2040
            TabIndex        =   149
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   480
            Width           =   3075
         End
         Begin VB.ComboBox cmbInternalExternal 
            Height          =   315
            Index           =   3
            Left            =   -73320
            TabIndex        =   148
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   480
            Width           =   3075
         End
         Begin VB.ComboBox cmbInternalExternal 
            Height          =   315
            Index           =   2
            Left            =   -73320
            TabIndex        =   147
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   480
            Width           =   3075
         End
         Begin VB.ComboBox cmbInternalExternal 
            Height          =   315
            Index           =   1
            Left            =   -73320
            TabIndex        =   146
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   420
            Width           =   3075
         End
         Begin VB.ComboBox cmbInternalExternal 
            Height          =   315
            Index           =   0
            Left            =   -72960
            TabIndex        =   145
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   420
            Width           =   3075
         End
         Begin VB.OptionButton optFindings4 
            Alignment       =   1  'Right Justify
            Caption         =   "Issue In Dispute"
            Height          =   315
            Index           =   5
            Left            =   150
            TabIndex        =   144
            Top             =   8520
            Width           =   3345
         End
         Begin VB.OptionButton optFindings3 
            Alignment       =   1  'Right Justify
            Caption         =   "Issue In Dispute"
            Height          =   315
            Index           =   9
            Left            =   -74880
            TabIndex        =   143
            Top             =   11340
            Width           =   3165
         End
         Begin VB.OptionButton optFindings2 
            Alignment       =   1  'Right Justify
            Caption         =   "Issue In Dispute"
            Height          =   315
            Index           =   9
            Left            =   -74880
            TabIndex        =   142
            Top             =   11100
            Width           =   3165
         End
         Begin VB.OptionButton optFindings1 
            Alignment       =   1  'Right Justify
            Caption         =   "Issue in Dispute"
            Height          =   315
            Index           =   9
            Left            =   -74880
            TabIndex        =   141
            Top             =   11760
            Width           =   2565
         End
         Begin VB.OptionButton optFindings 
            Alignment       =   1  'Right Justify
            Caption         =   "Issue in dispute"
            Height          =   315
            Index           =   5
            Left            =   -74820
            TabIndex        =   140
            Top             =   9780
            Width           =   3345
         End
         Begin VB.ComboBox cmbInternalDepartment 
            Height          =   315
            Index           =   4
            Left            =   5040
            TabIndex        =   139
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   7800
            Width           =   3255
         End
         Begin VB.ComboBox cmbInternalDepartment 
            Height          =   315
            Index           =   3
            Left            =   -70020
            TabIndex        =   138
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   9540
            Width           =   3255
         End
         Begin VB.ComboBox cmbInternalDepartment 
            Height          =   315
            Index           =   2
            Left            =   -70080
            TabIndex        =   137
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   9300
            Width           =   3255
         End
         Begin VB.ComboBox cmbInternalDepartment 
            Height          =   315
            Index           =   1
            Left            =   -67800
            TabIndex        =   136
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   10680
            Width           =   2835
         End
         Begin VB.ComboBox cmbInternalDepartment 
            Height          =   315
            Index           =   0
            Left            =   -69960
            TabIndex        =   135
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   9060
            Width           =   2835
         End
         Begin VB.TextBox txtSourceID 
            Height          =   315
            Index           =   1
            Left            =   -73320
            TabIndex        =   134
            Top             =   4800
            Width           =   1755
         End
         Begin VB.TextBox txtSourceID2 
            Height          =   315
            Index           =   1
            Left            =   -73320
            TabIndex        =   133
            Top             =   5160
            Width           =   1755
         End
         Begin VB.TextBox txtSourceID3 
            Height          =   315
            Index           =   1
            Left            =   -73320
            TabIndex        =   132
            Top             =   5520
            Width           =   1755
         End
         Begin VB.TextBox txtSourceID3 
            Height          =   315
            Index           =   0
            Left            =   -72960
            TabIndex        =   131
            Top             =   5880
            Width           =   1755
         End
         Begin VB.TextBox txtSourceID2 
            Height          =   315
            Index           =   0
            Left            =   -72960
            TabIndex        =   130
            Top             =   5520
            Width           =   1755
         End
         Begin VB.CommandButton cmdCreateRedoJob 
            Caption         =   "Create Redo Job for this item"
            Height          =   555
            Index           =   1
            Left            =   4680
            TabIndex        =   129
            Top             =   7020
            Width           =   1575
         End
         Begin VB.CommandButton cmdCreateRedoJob 
            Caption         =   "Create Redo Job for this item"
            Height          =   555
            Index           =   0
            Left            =   -70380
            TabIndex        =   128
            Top             =   8220
            Width           =   1575
         End
         Begin VB.TextBox txtOriginalJobdetailID 
            Height          =   315
            Index           =   1
            Left            =   7080
            TabIndex        =   127
            Top             =   3240
            Width           =   1875
         End
         Begin VB.TextBox txtOriginalJobdetailID 
            Height          =   315
            Index           =   0
            Left            =   -67860
            TabIndex        =   126
            Top             =   3960
            Width           =   1875
         End
         Begin VB.ComboBox cmbRejectionReason 
            Height          =   315
            Left            =   2040
            TabIndex        =   125
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   10020
            Width           =   3255
         End
         Begin VB.ComboBox cmbRejectionAccountability 
            Height          =   315
            Left            =   2040
            TabIndex        =   124
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   9660
            Width           =   3255
         End
         Begin VB.ComboBox cmbRejectionType 
            Height          =   315
            Left            =   2040
            TabIndex        =   123
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   9300
            Width           =   3255
         End
         Begin VB.ComboBox cmbRejectionCategory 
            Height          =   315
            Left            =   2040
            TabIndex        =   122
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   8940
            Width           =   3255
         End
         Begin VB.TextBox txtConclusion 
            Height          =   1755
            Index           =   1
            Left            =   2040
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   121
            Top             =   10380
            Width           =   6855
         End
         Begin VB.ComboBox cmbSignoff 
            Height          =   315
            Index           =   5
            Left            =   2040
            TabIndex        =   120
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   12180
            Width           =   2955
         End
         Begin VB.OptionButton optFindings4 
            Alignment       =   1  'Right Justify
            Caption         =   "Not Checked Yet"
            Height          =   315
            Index           =   0
            Left            =   150
            TabIndex        =   119
            Top             =   6720
            Width           =   3345
         End
         Begin VB.OptionButton optFindings4 
            Alignment       =   1  'Right Justify
            Caption         =   "No Fault Found"
            Height          =   315
            Index           =   1
            Left            =   150
            TabIndex        =   118
            Top             =   7080
            Width           =   3345
         End
         Begin VB.OptionButton optFindings4 
            Alignment       =   1  'Right Justify
            Caption         =   "Fault on Original Master"
            Height          =   315
            Index           =   2
            Left            =   150
            TabIndex        =   117
            Top             =   7440
            Width           =   3345
         End
         Begin VB.OptionButton optFindings4 
            Alignment       =   1  'Right Justify
            Caption         =   "MX1 Issue - MX1 to Redo"
            Height          =   315
            Index           =   3
            Left            =   150
            TabIndex        =   116
            Top             =   7800
            Width           =   3345
         End
         Begin VB.OptionButton optFindings4 
            Alignment       =   1  'Right Justify
            Caption         =   "Fault not on Original  - MX1 to Redo"
            Height          =   315
            Index           =   4
            Left            =   150
            TabIndex        =   115
            Top             =   8160
            Width           =   3345
         End
         Begin VB.ComboBox cmbAssignedTo 
            Height          =   315
            Index           =   5
            Left            =   2040
            TabIndex        =   114
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   4980
            Width           =   3255
         End
         Begin VB.TextBox txtTrackerFindings 
            Height          =   1305
            Index           =   5
            Left            =   2040
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   113
            Top             =   5340
            Width           =   6915
         End
         Begin VB.TextBox txtOrderRef 
            Height          =   315
            Index           =   1
            Left            =   2040
            TabIndex        =   112
            Top             =   3960
            Width           =   3975
         End
         Begin VB.TextBox txtOriginalJobID 
            Height          =   315
            Index           =   1
            Left            =   2040
            TabIndex        =   111
            Top             =   3240
            Width           =   1875
         End
         Begin VB.TextBox txtTitle 
            Height          =   315
            Index           =   1
            Left            =   2040
            TabIndex        =   110
            Top             =   3600
            Width           =   3975
         End
         Begin VB.ComboBox cmbOriginalOperator 
            Height          =   315
            Index           =   4
            Left            =   2040
            TabIndex        =   109
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   4320
            Width           =   3975
         End
         Begin VB.TextBox txtRejectionIssue 
            Height          =   1185
            Index           =   5
            Left            =   2040
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   108
            Top             =   1980
            Width           =   6915
         End
         Begin VB.Frame fraBilling 
            Caption         =   "Billing"
            Height          =   1335
            Left            =   -69120
            TabIndex        =   105
            Top             =   9600
            Width           =   2955
            Begin VB.CommandButton cmdBillAllItems 
               Caption         =   "Bill All Items"
               Height          =   315
               Left            =   120
               TabIndex        =   107
               ToolTipText     =   "Save and Print"
               Top             =   780
               Width           =   2655
            End
            Begin VB.CommandButton cmdBillItem 
               Caption         =   "Bill Item"
               Height          =   315
               Left            =   120
               TabIndex        =   106
               ToolTipText     =   "Save and Print"
               Top             =   360
               Width           =   2655
            End
         End
         Begin VB.TextBox txtAffinityCaseNumber 
            Height          =   315
            Index           =   1
            Left            =   -73320
            TabIndex        =   104
            Top             =   1140
            Width           =   1935
         End
         Begin VB.TextBox txtContentVersionCode 
            Height          =   315
            Left            =   -72960
            TabIndex        =   103
            Top             =   2460
            Width           =   3975
         End
         Begin VB.TextBox txtAffinityCaseNumber 
            Height          =   315
            Index           =   4
            Left            =   -72960
            TabIndex        =   102
            Top             =   1740
            Width           =   3975
         End
         Begin VB.TextBox txtSourceID 
            Height          =   315
            Index           =   0
            Left            =   -72960
            TabIndex        =   101
            Top             =   5160
            Width           =   1755
         End
         Begin VB.ComboBox cmbSignoff 
            Height          =   315
            Index           =   4
            Left            =   -72990
            TabIndex        =   100
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   11040
            Width           =   2955
         End
         Begin VB.OptionButton optFindings 
            Alignment       =   1  'Right Justify
            Caption         =   "Not Checked Yet"
            Height          =   315
            Index           =   8
            Left            =   -74820
            TabIndex        =   99
            Top             =   9240
            Width           =   4485
         End
         Begin VB.OptionButton optFindings 
            Alignment       =   1  'Right Justify
            Caption         =   "No Fault Found"
            Height          =   315
            Index           =   7
            Left            =   -74820
            TabIndex        =   98
            Top             =   9600
            Width           =   4485
         End
         Begin VB.OptionButton optFindings 
            Alignment       =   1  'Right Justify
            Caption         =   "Fault Found on Original Master - Fixing Process"
            Height          =   315
            Index           =   9
            Left            =   -74820
            TabIndex        =   97
            Top             =   9960
            Width           =   4485
         End
         Begin VB.OptionButton optFindings 
            Alignment       =   1  'Right Justify
            Caption         =   "Fault not found on Original - IR to be requested for Redo"
            Height          =   315
            Index           =   10
            Left            =   -74820
            TabIndex        =   96
            Top             =   10320
            Width           =   4485
         End
         Begin VB.TextBox txtTrackerFindings 
            Height          =   1845
            Index           =   4
            Left            =   -72960
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   95
            Top             =   7320
            Width           =   6915
         End
         Begin VB.ComboBox cmbAssignedTo 
            Height          =   315
            Index           =   4
            Left            =   -72960
            TabIndex        =   94
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   6600
            Width           =   3255
         End
         Begin VB.TextBox txtTitle 
            Height          =   315
            Index           =   4
            Left            =   -72960
            TabIndex        =   93
            Top             =   2100
            Width           =   3975
         End
         Begin VB.TextBox txtRejectionIssue 
            Height          =   1965
            Index           =   4
            Left            =   -72960
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   92
            Top             =   2820
            Width           =   6915
         End
         Begin VB.ComboBox cmbSignoff 
            Height          =   315
            Index           =   0
            Left            =   -72960
            TabIndex        =   91
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   12060
            Width           =   2955
         End
         Begin VB.TextBox txtRejectionIssue 
            Height          =   1965
            Index           =   0
            Left            =   -72960
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   90
            Top             =   1920
            Width           =   6915
         End
         Begin VB.TextBox txtTrackerFindings 
            Height          =   1845
            Index           =   0
            Left            =   -72960
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   89
            Top             =   6120
            Width           =   6915
         End
         Begin VB.ComboBox cmbAssignedTo 
            Height          =   315
            Index           =   0
            Left            =   -72960
            TabIndex        =   88
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   5760
            Width           =   3255
         End
         Begin VB.CommandButton cmdCreateRedoTrackerTask 
            Caption         =   "Create Redo Tracker Entry"
            Height          =   555
            Index           =   2
            Left            =   -70980
            TabIndex        =   87
            Top             =   8700
            Width           =   1575
         End
         Begin VB.CommandButton cmdCreateRedoTrackerTask 
            Caption         =   "Create Redo Tracker Entry"
            Height          =   555
            Index           =   1
            Left            =   -71160
            TabIndex        =   86
            Top             =   8340
            Width           =   1575
         End
         Begin VB.ComboBox cmbOriginalOperator 
            Height          =   315
            Index           =   3
            Left            =   -73320
            TabIndex        =   85
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   4080
            Width           =   3255
         End
         Begin VB.ComboBox cmbOriginalOperator 
            Height          =   315
            Index           =   2
            Left            =   -73320
            TabIndex        =   84
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   3720
            Width           =   3255
         End
         Begin VB.ComboBox cmbOriginalOperator 
            Height          =   315
            Index           =   1
            Left            =   -73320
            TabIndex        =   83
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   6300
            Width           =   3255
         End
         Begin VB.ComboBox cmbOriginalOperator 
            Height          =   315
            Index           =   0
            Left            =   -72960
            TabIndex        =   82
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   5040
            Width           =   3975
         End
         Begin VB.ComboBox cmbSignoff 
            Height          =   315
            Index           =   3
            Left            =   -73320
            TabIndex        =   81
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   12120
            Width           =   2955
         End
         Begin VB.TextBox txtTrackerFindings 
            Height          =   1545
            Index           =   3
            Left            =   -73320
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   80
            Top             =   6420
            Width           =   7155
         End
         Begin VB.ComboBox cmbAssignedTo 
            Height          =   315
            Index           =   3
            Left            =   -73320
            TabIndex        =   79
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   6060
            Width           =   3255
         End
         Begin VB.TextBox txtRejectionIssue 
            Height          =   1545
            Index           =   3
            Left            =   -73320
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   78
            Top             =   4440
            Width           =   7155
         End
         Begin VB.OptionButton optFindings3 
            Alignment       =   1  'Right Justify
            Caption         =   "Original File Rejection on Drive"
            Height          =   315
            Index           =   7
            Left            =   -74880
            TabIndex        =   77
            Top             =   10620
            Width           =   3165
         End
         Begin VB.OptionButton optFindings3 
            Alignment       =   1  'Right Justify
            Caption         =   "Checking In Progress"
            Height          =   315
            Index           =   6
            Left            =   -74880
            TabIndex        =   76
            Top             =   8460
            Width           =   3165
         End
         Begin VB.OptionButton optFindings3 
            Alignment       =   1  'Right Justify
            Caption         =   "Fault not on Original - MX1 Redo"
            Height          =   315
            Index           =   5
            Left            =   -74880
            TabIndex        =   75
            Top             =   10260
            Width           =   3165
         End
         Begin VB.OptionButton optFindings3 
            Alignment       =   1  'Right Justify
            Caption         =   "Fault on Original Master"
            Height          =   315
            Index           =   4
            Left            =   -74880
            TabIndex        =   74
            Top             =   9900
            Width           =   3165
         End
         Begin VB.OptionButton optFindings3 
            Alignment       =   1  'Right Justify
            Caption         =   "MX1 Issue - MX1 to Redo"
            Height          =   315
            Index           =   3
            Left            =   -74880
            TabIndex        =   73
            Top             =   9540
            Width           =   3165
         End
         Begin VB.OptionButton optFindings3 
            Alignment       =   1  'Right Justify
            Caption         =   "File Issue at DADC"
            Height          =   315
            Index           =   2
            Left            =   -74880
            TabIndex        =   72
            Top             =   9180
            Width           =   3165
         End
         Begin VB.OptionButton optFindings3 
            Alignment       =   1  'Right Justify
            Caption         =   "No Fault Found"
            Height          =   315
            Index           =   1
            Left            =   -74880
            TabIndex        =   71
            Top             =   8820
            Width           =   3165
         End
         Begin VB.OptionButton optFindings3 
            Alignment       =   1  'Right Justify
            Caption         =   "Not Checked Yet"
            Height          =   315
            Index           =   0
            Left            =   -74880
            TabIndex        =   70
            Top             =   8100
            Width           =   3165
         End
         Begin VB.OptionButton optFindings3 
            Alignment       =   1  'Right Justify
            Caption         =   "Original File Rejection on Warp"
            Height          =   315
            Index           =   8
            Left            =   -74880
            TabIndex        =   69
            Top             =   10980
            Width           =   3165
         End
         Begin VB.OptionButton optFindings 
            Alignment       =   1  'Right Justify
            Caption         =   "Fault not on Original  - MX1 to Redo"
            Height          =   315
            Index           =   4
            Left            =   -74820
            TabIndex        =   68
            Top             =   9420
            Width           =   3345
         End
         Begin VB.OptionButton optFindings 
            Alignment       =   1  'Right Justify
            Caption         =   "MX1 Issue - MX1to Redo"
            Height          =   315
            Index           =   3
            Left            =   -74820
            TabIndex        =   67
            Top             =   9060
            Width           =   3345
         End
         Begin VB.OptionButton optFindings 
            Alignment       =   1  'Right Justify
            Caption         =   "Fault on Original Master"
            Height          =   315
            Index           =   2
            Left            =   -74820
            TabIndex        =   66
            Top             =   8700
            Width           =   3345
         End
         Begin VB.OptionButton optFindings 
            Alignment       =   1  'Right Justify
            Caption         =   "No Fault Found"
            Height          =   315
            Index           =   1
            Left            =   -74820
            TabIndex        =   65
            Top             =   8340
            Width           =   3345
         End
         Begin VB.OptionButton optFindings 
            Alignment       =   1  'Right Justify
            Caption         =   "Not Checked Yet"
            Height          =   315
            Index           =   0
            Left            =   -74820
            TabIndex        =   64
            Top             =   7980
            Value           =   -1  'True
            Width           =   3345
         End
         Begin VB.TextBox txtTitle 
            Height          =   315
            Index           =   0
            Left            =   -72960
            TabIndex        =   63
            Top             =   4320
            Width           =   3975
         End
         Begin VB.OptionButton optFindings2 
            Alignment       =   1  'Right Justify
            Caption         =   "Original File Rejection on Warp"
            Height          =   315
            Index           =   8
            Left            =   -74880
            TabIndex        =   62
            Top             =   10740
            Width           =   3165
         End
         Begin VB.OptionButton optFindings2 
            Alignment       =   1  'Right Justify
            Caption         =   "Not Checked Yet"
            Height          =   315
            Index           =   0
            Left            =   -74880
            TabIndex        =   61
            Top             =   7860
            Width           =   3165
         End
         Begin VB.OptionButton optFindings2 
            Alignment       =   1  'Right Justify
            Caption         =   "No Fault Found"
            Height          =   315
            Index           =   1
            Left            =   -74880
            TabIndex        =   60
            Top             =   8580
            Width           =   3165
         End
         Begin VB.OptionButton optFindings2 
            Alignment       =   1  'Right Justify
            Caption         =   "File Issue at DADC"
            Height          =   315
            Index           =   2
            Left            =   -74880
            TabIndex        =   59
            Top             =   8940
            Width           =   3165
         End
         Begin VB.OptionButton optFindings2 
            Alignment       =   1  'Right Justify
            Caption         =   "MX1 Issue - MX1 to Redo"
            Height          =   315
            Index           =   3
            Left            =   -74880
            TabIndex        =   58
            Top             =   9300
            Width           =   3165
         End
         Begin VB.OptionButton optFindings2 
            Alignment       =   1  'Right Justify
            Caption         =   "Fault on Original Master"
            Height          =   315
            Index           =   4
            Left            =   -74880
            TabIndex        =   57
            Top             =   9660
            Width           =   3165
         End
         Begin VB.OptionButton optFindings2 
            Alignment       =   1  'Right Justify
            Caption         =   "Fault not on Original - MX1 Redo"
            Height          =   315
            Index           =   5
            Left            =   -74880
            TabIndex        =   56
            Top             =   10020
            Width           =   3165
         End
         Begin VB.OptionButton optFindings2 
            Alignment       =   1  'Right Justify
            Caption         =   "Checking In Progress"
            Height          =   315
            Index           =   6
            Left            =   -74880
            TabIndex        =   55
            Top             =   8220
            Width           =   3165
         End
         Begin VB.OptionButton optFindings2 
            Alignment       =   1  'Right Justify
            Caption         =   "Original File Rejection on Drive"
            Height          =   315
            Index           =   7
            Left            =   -74880
            TabIndex        =   54
            Top             =   10380
            Width           =   3165
         End
         Begin VB.TextBox txtRejectionIssue 
            Height          =   1545
            Index           =   2
            Left            =   -73320
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   53
            Top             =   4080
            Width           =   7215
         End
         Begin VB.ComboBox cmbAssignedTo 
            Height          =   315
            Index           =   2
            Left            =   -73320
            TabIndex        =   52
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   5700
            Width           =   3255
         End
         Begin VB.TextBox txtTrackerFindings 
            Height          =   1545
            Index           =   2
            Left            =   -73320
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   51
            Top             =   6060
            Width           =   7215
         End
         Begin VB.ComboBox cmbSignoff 
            Height          =   315
            Index           =   2
            Left            =   -73320
            TabIndex        =   50
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   11700
            Width           =   2955
         End
         Begin VB.OptionButton optFindings1 
            Alignment       =   1  'Right Justify
            Caption         =   "Original File Rejection on Warp"
            Height          =   315
            Index           =   8
            Left            =   -71880
            TabIndex        =   49
            Top             =   11400
            Width           =   2685
         End
         Begin VB.OptionButton optFindings1 
            Alignment       =   1  'Right Justify
            Caption         =   "Original File Rejection on Drive"
            Height          =   315
            Index           =   7
            Left            =   -74880
            TabIndex        =   48
            Top             =   11400
            Width           =   2565
         End
         Begin VB.ComboBox cmbSignoff 
            Height          =   315
            Index           =   1
            Left            =   -73320
            TabIndex        =   47
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   12180
            Width           =   2955
         End
         Begin VB.OptionButton optFindings1 
            Alignment       =   1  'Right Justify
            Caption         =   "Checking In Progress"
            Height          =   315
            Index           =   6
            Left            =   -74880
            TabIndex        =   46
            Top             =   9960
            Width           =   2565
         End
         Begin VB.OptionButton optFindings1 
            Alignment       =   1  'Right Justify
            Caption         =   "Fault not on Original - MX1 Redo"
            Height          =   315
            Index           =   5
            Left            =   -71880
            TabIndex        =   45
            Top             =   11040
            Width           =   2685
         End
         Begin VB.CommandButton cmdCreateRedoTrackerTask 
            Caption         =   "Create Redo Tracker Entry"
            Height          =   555
            Index           =   0
            Left            =   -71040
            TabIndex        =   44
            Top             =   9780
            Width           =   1575
         End
         Begin VB.OptionButton optFindings1 
            Alignment       =   1  'Right Justify
            Caption         =   "Fault on Original Master"
            Height          =   315
            Index           =   4
            Left            =   -74880
            TabIndex        =   43
            Top             =   11040
            Width           =   2565
         End
         Begin VB.OptionButton optFindings1 
            Alignment       =   1  'Right Justify
            Caption         =   "MX1 Issue - MX1 to Redo"
            Height          =   315
            Index           =   3
            Left            =   -71880
            TabIndex        =   42
            Top             =   10680
            Width           =   2685
         End
         Begin VB.OptionButton optFindings1 
            Alignment       =   1  'Right Justify
            Caption         =   "File Issue at DADC"
            Height          =   315
            Index           =   2
            Left            =   -74880
            TabIndex        =   41
            Top             =   10680
            Width           =   2565
         End
         Begin VB.OptionButton optFindings1 
            Alignment       =   1  'Right Justify
            Caption         =   "No Fault Found"
            Height          =   315
            Index           =   1
            Left            =   -74880
            TabIndex        =   40
            Top             =   10320
            Width           =   2565
         End
         Begin VB.OptionButton optFindings1 
            Alignment       =   1  'Right Justify
            Caption         =   "Not Checked Yet"
            Height          =   315
            Index           =   0
            Left            =   -74880
            TabIndex        =   39
            Top             =   9600
            Width           =   2565
         End
         Begin VB.TextBox txtTrackerFindings 
            Height          =   1185
            Index           =   1
            Left            =   -73320
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   38
            Top             =   8280
            Width           =   7155
         End
         Begin VB.ComboBox cmbAssignedTo 
            Height          =   315
            Index           =   1
            Left            =   -73320
            TabIndex        =   37
            ToolTipText     =   "The machine the Tape was reviewed on"
            Top             =   7920
            Width           =   3255
         End
         Begin VB.TextBox txtRejectionIssue 
            Height          =   1185
            Index           =   1
            Left            =   -73320
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   36
            Top             =   6660
            Width           =   7155
         End
         Begin VB.TextBox txtOriginalJobID 
            Height          =   315
            Index           =   0
            Left            =   -72960
            TabIndex        =   35
            Top             =   3960
            Width           =   1875
         End
         Begin VB.TextBox txtOrderRef 
            Height          =   315
            Index           =   0
            Left            =   -72960
            TabIndex        =   34
            Top             =   4680
            Width           =   3975
         End
         Begin VB.TextBox txtConclusion 
            Height          =   1755
            Index           =   0
            Left            =   -72960
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   33
            Top             =   10260
            Width           =   6855
         End
         Begin MSComCtl2.DTPicker datJobCompletedDate 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "dd/MM/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   3
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   -72960
            TabIndex        =   150
            Tag             =   "NOCHECK"
            ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
            Top             =   5400
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   155582465
            CurrentDate     =   37870
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
            Bindings        =   "frmComplaint.frx":68FA
            Height          =   315
            Index           =   0
            Left            =   -72960
            TabIndex        =   151
            ToolTipText     =   "The company this quote is for"
            Top             =   780
            Width           =   3975
            BevelWidth      =   0
            DataFieldList   =   "name"
            BevelType       =   0
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BorderStyle     =   0
            BeveColorScheme =   1
            ForeColorEven   =   -2147483640
            ForeColorOdd    =   -2147483640
            BackColorEven   =   -2147483643
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6376
            Columns(0).Caption=   "name"
            Columns(0).Name =   "name"
            Columns(0).DataField=   "name"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "companyID"
            Columns(1).Name =   "companyID"
            Columns(1).DataField=   "companyID"
            Columns(1).FieldLen=   256
            _ExtentX        =   7011
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16761087
            DataFieldToDisplay=   "name"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbContact 
            Bindings        =   "frmComplaint.frx":6913
            Height          =   315
            Index           =   0
            Left            =   -72960
            TabIndex        =   152
            ToolTipText     =   "The contact for this quote"
            Top             =   1140
            Width           =   3975
            DataFieldList   =   "name"
            BevelType       =   0
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BorderStyle     =   0
            ForeColorEven   =   -2147483640
            ForeColorOdd    =   -2147483640
            BackColorEven   =   -2147483643
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5424
            Columns(0).Caption=   "name"
            Columns(0).Name =   "name"
            Columns(0).DataField=   "name"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "contactID"
            Columns(1).Name =   "contactID"
            Columns(1).DataField=   "contactID"
            Columns(1).FieldLen=   256
            _ExtentX        =   7011
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16761087
            DataFieldToDisplay=   "name"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbContact 
            Bindings        =   "frmComplaint.frx":692C
            Height          =   315
            Index           =   4
            Left            =   -72960
            TabIndex        =   153
            ToolTipText     =   "The contact for this quote"
            Top             =   1380
            Width           =   3075
            DataFieldList   =   "name"
            BevelType       =   0
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BorderStyle     =   0
            ForeColorEven   =   -2147483640
            ForeColorOdd    =   -2147483640
            BackColorEven   =   -2147483643
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5424
            Columns(0).Caption=   "name"
            Columns(0).Name =   "name"
            Columns(0).DataField=   "name"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "contactID"
            Columns(1).Name =   "contactID"
            Columns(1).DataField=   "contactID"
            Columns(1).FieldLen=   256
            _ExtentX        =   5424
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16761087
            DataFieldToDisplay=   "name"
         End
         Begin MSComCtl2.DTPicker datFileArrived 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "dd/MM/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   3
            EndProperty
            Height          =   315
            Left            =   -72960
            TabIndex        =   154
            Tag             =   "NOCHECK"
            ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
            Top             =   4800
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   155582465
            CurrentDate     =   37870
         End
         Begin MSComCtl2.DTPicker datMasterArrived 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "dd/MM/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   3
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   -69180
            TabIndex        =   155
            Tag             =   "NOCHECK"
            ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
            Top             =   5160
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   155582465
            CurrentDate     =   37870
         End
         Begin MSComCtl2.DTPicker datInvestigated 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "dd/MM/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   3
            EndProperty
            Height          =   315
            Left            =   -72960
            TabIndex        =   156
            Tag             =   "NOCHECK"
            ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
            Top             =   6960
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   155582465
            CurrentDate     =   37870
         End
         Begin MSComCtl2.DTPicker datSignOff 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "dd/MM/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   3
            EndProperty
            Height          =   315
            Left            =   -72960
            TabIndex        =   157
            Tag             =   "NOCHECK"
            ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
            Top             =   11400
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   155582465
            CurrentDate     =   37870
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
            Bindings        =   "frmComplaint.frx":6945
            Height          =   315
            Index           =   1
            Left            =   2040
            TabIndex        =   158
            ToolTipText     =   "The company this quote is for"
            Top             =   840
            Width           =   3075
            BevelWidth      =   0
            DataFieldList   =   "name"
            BevelType       =   0
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BorderStyle     =   0
            BeveColorScheme =   1
            ForeColorEven   =   -2147483640
            ForeColorOdd    =   -2147483640
            BackColorEven   =   -2147483643
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6376
            Columns(0).Caption=   "name"
            Columns(0).Name =   "name"
            Columns(0).DataField=   "name"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "companyID"
            Columns(1).Name =   "companyID"
            Columns(1).DataField=   "companyID"
            Columns(1).FieldLen=   256
            _ExtentX        =   5424
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16761087
            DataFieldToDisplay=   "name"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbContact 
            Bindings        =   "frmComplaint.frx":695E
            Height          =   315
            Index           =   1
            Left            =   2040
            TabIndex        =   159
            ToolTipText     =   "The contact for this quote"
            Top             =   1200
            Width           =   3075
            DataFieldList   =   "name"
            BevelType       =   0
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BorderStyle     =   0
            ForeColorEven   =   -2147483640
            ForeColorOdd    =   -2147483640
            BackColorEven   =   -2147483643
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5424
            Columns(0).Caption=   "name"
            Columns(0).Name =   "name"
            Columns(0).DataField=   "name"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "contactID"
            Columns(1).Name =   "contactID"
            Columns(1).DataField=   "contactID"
            Columns(1).FieldLen=   256
            _ExtentX        =   5424
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16761087
            DataFieldToDisplay=   "name"
         End
         Begin MSComCtl2.DTPicker datJobCompletedDate 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "dd/MM/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   3
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   2040
            TabIndex        =   160
            Tag             =   "NOCHECK"
            ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
            Top             =   4680
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   155582465
            CurrentDate     =   37870
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCetaUser 
            Bindings        =   "frmComplaint.frx":6977
            Height          =   315
            Index           =   0
            Left            =   -72960
            TabIndex        =   161
            ToolTipText     =   "The contact for this quote"
            Top             =   1500
            Width           =   3075
            DataFieldList   =   "fullname"
            BevelType       =   0
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BorderStyle     =   0
            ForeColorEven   =   -2147483640
            ForeColorOdd    =   -2147483640
            BackColorEven   =   -2147483643
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   5424
            Columns(0).Caption=   "Full Name"
            Columns(0).Name =   "fullname"
            Columns(0).DataField=   "fullname"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "cetauserID"
            Columns(1).Name =   "cetauserID"
            Columns(1).DataField=   "cetauserID"
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "email"
            Columns(2).Name =   "email"
            Columns(2).DataField=   "email"
            Columns(2).FieldLen=   256
            _ExtentX        =   5424
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16761087
            DataFieldToDisplay=   "fullname"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCetaUser 
            Bindings        =   "frmComplaint.frx":6991
            Height          =   315
            Index           =   1
            Left            =   -73320
            TabIndex        =   162
            ToolTipText     =   "The contact for this quote"
            Top             =   780
            Width           =   3075
            DataFieldList   =   "fullname"
            BevelType       =   0
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BorderStyle     =   0
            ForeColorEven   =   -2147483640
            ForeColorOdd    =   -2147483640
            BackColorEven   =   -2147483643
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   5424
            Columns(0).Caption=   "Full Name"
            Columns(0).Name =   "fullname"
            Columns(0).DataField=   "fullname"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "cetauserID"
            Columns(1).Name =   "cetauserID"
            Columns(1).DataField=   "cetauserID"
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "email"
            Columns(2).Name =   "email"
            Columns(2).DataField=   "email"
            Columns(2).FieldLen=   256
            _ExtentX        =   5424
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16761087
            DataFieldToDisplay=   "fullname"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCetaUser 
            Bindings        =   "frmComplaint.frx":69AB
            Height          =   315
            Index           =   3
            Left            =   -73320
            TabIndex        =   163
            ToolTipText     =   "The contact for this quote"
            Top             =   840
            Width           =   3075
            DataFieldList   =   "fullname"
            BevelType       =   0
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BorderStyle     =   0
            ForeColorEven   =   -2147483640
            ForeColorOdd    =   -2147483640
            BackColorEven   =   -2147483643
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   5424
            Columns(0).Caption=   "Full Name"
            Columns(0).Name =   "fullname"
            Columns(0).DataField=   "fullname"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "cetauserID"
            Columns(1).Name =   "cetauserID"
            Columns(1).DataField=   "cetauserID"
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "email"
            Columns(2).Name =   "email"
            Columns(2).DataField=   "email"
            Columns(2).FieldLen=   256
            _ExtentX        =   5424
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16761087
            DataFieldToDisplay=   "fullname"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCetaUser 
            Bindings        =   "frmComplaint.frx":69C5
            Height          =   315
            Index           =   2
            Left            =   -73320
            TabIndex        =   164
            ToolTipText     =   "The contact for this quote"
            Top             =   840
            Width           =   3075
            DataFieldList   =   "fullname"
            BevelType       =   0
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BorderStyle     =   0
            ForeColorEven   =   -2147483640
            ForeColorOdd    =   -2147483640
            BackColorEven   =   -2147483643
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   5424
            Columns(0).Caption=   "Full Name"
            Columns(0).Name =   "fullname"
            Columns(0).DataField=   "fullname"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "cetauserID"
            Columns(1).Name =   "cetauserID"
            Columns(1).DataField=   "cetauserID"
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "email"
            Columns(2).Name =   "email"
            Columns(2).DataField=   "email"
            Columns(2).FieldLen=   256
            _ExtentX        =   5424
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16761087
            DataFieldToDisplay=   "fullname"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCetaUser 
            Bindings        =   "frmComplaint.frx":69DF
            Height          =   315
            Index           =   4
            Left            =   -72960
            TabIndex        =   165
            ToolTipText     =   "The contact for this quote"
            Top             =   1020
            Width           =   3075
            DataFieldList   =   "fullname"
            BevelType       =   0
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BorderStyle     =   0
            ForeColorEven   =   -2147483640
            ForeColorOdd    =   -2147483640
            BackColorEven   =   -2147483643
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   5424
            Columns(0).Caption=   "Full Name"
            Columns(0).Name =   "fullname"
            Columns(0).DataField=   "fullname"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "cetauserID"
            Columns(1).Name =   "cetauserID"
            Columns(1).DataField=   "cetauserID"
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "email"
            Columns(2).Name =   "email"
            Columns(2).DataField=   "email"
            Columns(2).FieldLen=   256
            _ExtentX        =   5424
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16761087
            DataFieldToDisplay=   "fullname"
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCetaUser 
            Bindings        =   "frmComplaint.frx":69F9
            Height          =   315
            Index           =   5
            Left            =   2040
            TabIndex        =   166
            ToolTipText     =   "The contact for this quote"
            Top             =   1560
            Width           =   3075
            DataFieldList   =   "fullname"
            BevelType       =   0
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BorderStyle     =   0
            ForeColorEven   =   -2147483640
            ForeColorOdd    =   -2147483640
            BackColorEven   =   -2147483643
            BackColorOdd    =   16761087
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   5424
            Columns(0).Caption=   "Full Name"
            Columns(0).Name =   "fullname"
            Columns(0).DataField=   "fullname"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "cetauserID"
            Columns(1).Name =   "cetauserID"
            Columns(1).DataField=   "cetauserID"
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "email"
            Columns(2).Name =   "email"
            Columns(2).DataField=   "email"
            Columns(2).FieldLen=   256
            _ExtentX        =   5424
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16761087
            DataFieldToDisplay=   "fullname"
         End
         Begin MSComCtl2.DTPicker datTargetDate 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "dd/MM/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   3
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   -72960
            TabIndex        =   167
            Tag             =   "NOCHECK"
            ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
            Top             =   6240
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   136314881
            CurrentDate     =   37870
         End
         Begin MSComCtl2.DTPicker datMasterArrived2 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "dd/MM/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   3
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   -69180
            TabIndex        =   168
            Tag             =   "NOCHECK"
            ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
            Top             =   5520
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   136314881
            CurrentDate     =   37870
         End
         Begin MSComCtl2.DTPicker datMasterArrived3 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "dd/MM/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   3
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   -69180
            TabIndex        =   169
            Tag             =   "NOCHECK"
            ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
            Top             =   5880
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   136314881
            CurrentDate     =   37870
         End
         Begin MSComCtl2.DTPicker datMasterArrived 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "dd/MM/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   3
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   -69480
            TabIndex        =   170
            Tag             =   "NOCHECK"
            ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
            Top             =   4800
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   136314881
            CurrentDate     =   37870
         End
         Begin MSComCtl2.DTPicker datTargetDate 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "dd/MM/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   3
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   -73320
            TabIndex        =   171
            Tag             =   "NOCHECK"
            ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
            Top             =   5880
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   136314881
            CurrentDate     =   37870
         End
         Begin MSComCtl2.DTPicker datMasterArrived2 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "dd/MM/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   3
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   -69480
            TabIndex        =   172
            Tag             =   "NOCHECK"
            ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
            Top             =   5160
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   136314881
            CurrentDate     =   37870
         End
         Begin MSComCtl2.DTPicker datMasterArrived3 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "dd/MM/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   3
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   -69480
            TabIndex        =   173
            Tag             =   "NOCHECK"
            ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
            Top             =   5520
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   556
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   153092097
            CurrentDate     =   37870
         End
         Begin VB.Label lblCaption 
            Caption         =   "Issue Type"
            Height          =   255
            Index           =   131
            Left            =   3840
            TabIndex        =   377
            Top             =   8220
            Width           =   1155
         End
         Begin VB.Label lblCaption 
            Caption         =   "Issue Type"
            Height          =   255
            Index           =   130
            Left            =   -71220
            TabIndex        =   375
            Top             =   9960
            Width           =   1155
         End
         Begin VB.Label lblCaption 
            Caption         =   "Issue Type"
            Height          =   255
            Index           =   129
            Left            =   -71280
            TabIndex        =   373
            Top             =   9720
            Width           =   1155
         End
         Begin VB.Label lblCaption 
            Caption         =   "Issue Type"
            Height          =   255
            Index           =   128
            Left            =   -69000
            TabIndex        =   371
            Top             =   11100
            Width           =   1155
         End
         Begin VB.Label lblCaption 
            Caption         =   "Issue Type"
            Height          =   255
            Index           =   126
            Left            =   -71160
            TabIndex        =   367
            Top             =   9480
            Width           =   1155
         End
         Begin VB.Label lblCaption 
            Caption         =   "Internal / External"
            Height          =   255
            Index           =   122
            Left            =   180
            TabIndex        =   339
            Top             =   540
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Internal / External"
            Height          =   255
            Index           =   121
            Left            =   -74880
            TabIndex        =   338
            Top             =   540
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Internal / External"
            Height          =   255
            Index           =   120
            Left            =   -74880
            TabIndex        =   337
            Top             =   540
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Internal / External"
            Height          =   255
            Index           =   80
            Left            =   -74880
            TabIndex        =   336
            Top             =   480
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Internal / External"
            Height          =   255
            Index           =   4
            Left            =   -74820
            TabIndex        =   335
            Top             =   480
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "MX1 Dept"
            Height          =   255
            Index           =   116
            Left            =   3840
            TabIndex        =   334
            Top             =   7860
            Width           =   735
         End
         Begin VB.Label lblCaption 
            Caption         =   "MX1 Dept"
            Height          =   255
            Index           =   115
            Left            =   -71220
            TabIndex        =   333
            Top             =   9600
            Width           =   735
         End
         Begin VB.Label lblCaption 
            Caption         =   "MX1 Dept"
            Height          =   255
            Index           =   114
            Left            =   -71280
            TabIndex        =   332
            Top             =   9360
            Width           =   735
         End
         Begin VB.Label lblCaption 
            Caption         =   "MX1 Dept"
            Height          =   255
            Index           =   113
            Left            =   -69000
            TabIndex        =   331
            Top             =   10740
            Width           =   735
         End
         Begin VB.Label lblCaption 
            Caption         =   "MX1 Dept"
            Height          =   255
            Index           =   112
            Left            =   -71160
            TabIndex        =   330
            Top             =   9120
            Width           =   1155
         End
         Begin VB.Label lblCaption 
            Caption         =   "Date Master Arrived"
            Height          =   255
            Index           =   111
            Left            =   -71340
            TabIndex        =   329
            Top             =   4860
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Master Source ID 1"
            Height          =   255
            Index           =   110
            Left            =   -74880
            TabIndex        =   328
            Top             =   4860
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Master Source ID 2"
            Height          =   255
            Index           =   109
            Left            =   -74880
            TabIndex        =   327
            Top             =   5220
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Master Source ID 3"
            Height          =   255
            Index           =   108
            Left            =   -74880
            TabIndex        =   326
            Top             =   5580
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Target Date"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   107
            Left            =   -74880
            TabIndex        =   325
            Top             =   5940
            Width           =   1875
         End
         Begin VB.Label lblCaption 
            Caption         =   "Date Master Arrived"
            Height          =   255
            Index           =   106
            Left            =   -71340
            TabIndex        =   324
            Top             =   5220
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Date Master Arrived"
            Height          =   255
            Index           =   105
            Left            =   -71340
            TabIndex        =   323
            Top             =   5580
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Date Master Arrived"
            Height          =   255
            Index           =   104
            Left            =   -71040
            TabIndex        =   322
            Top             =   5940
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Date Master Arrived"
            Height          =   255
            Index           =   103
            Left            =   -71040
            TabIndex        =   321
            Top             =   5580
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Target Date"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   102
            Left            =   -74820
            TabIndex        =   320
            Top             =   6300
            Width           =   1875
         End
         Begin VB.Label lblCaption 
            Caption         =   "Master Source ID 3"
            Height          =   255
            Index           =   101
            Left            =   -74820
            TabIndex        =   319
            Top             =   5940
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Master Source ID 2"
            Height          =   255
            Index           =   100
            Left            =   -74820
            TabIndex        =   318
            Top             =   5580
            Width           =   1815
         End
         Begin VB.Label lblRRContactID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   5
            Left            =   5160
            TabIndex        =   317
            Tag             =   "CLEARFIELDS"
            Top             =   1560
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Account Handler"
            Height          =   255
            Index           =   98
            Left            =   180
            TabIndex        =   316
            Top             =   1620
            Width           =   1755
         End
         Begin VB.Label lblRRContactEmail 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   5
            Left            =   6060
            TabIndex        =   315
            Tag             =   "CLEARFIELDS"
            Top             =   1560
            Width           =   2835
         End
         Begin VB.Label lblRRContactID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   4
            Left            =   -69840
            TabIndex        =   314
            Tag             =   "CLEARFIELDS"
            Top             =   1020
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Account Handler"
            Height          =   255
            Index           =   97
            Left            =   -74820
            TabIndex        =   313
            Top             =   1080
            Width           =   1815
         End
         Begin VB.Label lblRRContactEmail 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   4
            Left            =   -68940
            TabIndex        =   312
            Tag             =   "CLEARFIELDS"
            Top             =   1020
            Width           =   2835
         End
         Begin VB.Label lblRRContactID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   2
            Left            =   -70200
            TabIndex        =   311
            Tag             =   "CLEARFIELDS"
            Top             =   840
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Account Handler"
            Height          =   255
            Index           =   95
            Left            =   -74880
            TabIndex        =   310
            Top             =   900
            Width           =   1575
         End
         Begin VB.Label lblRRContactEmail 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   2
            Left            =   -69300
            TabIndex        =   309
            Tag             =   "CLEARFIELDS"
            Top             =   840
            Width           =   2835
         End
         Begin VB.Label lblRRContactID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   3
            Left            =   -70200
            TabIndex        =   308
            Tag             =   "CLEARFIELDS"
            Top             =   840
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Account Handler"
            Height          =   255
            Index           =   96
            Left            =   -74880
            TabIndex        =   307
            Top             =   900
            Width           =   1515
         End
         Begin VB.Label lblRRContactEmail 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   3
            Left            =   -69300
            TabIndex        =   306
            Tag             =   "CLEARFIELDS"
            Top             =   840
            Width           =   2835
         End
         Begin VB.Label lblRRContactID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   -70200
            TabIndex        =   305
            Tag             =   "CLEARFIELDS"
            Top             =   780
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Account Handler"
            Height          =   255
            Index           =   94
            Left            =   -74880
            TabIndex        =   304
            Top             =   840
            Width           =   1515
         End
         Begin VB.Label lblRRContactEmail 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   -69300
            TabIndex        =   303
            Tag             =   "CLEARFIELDS"
            Top             =   780
            Width           =   2835
         End
         Begin VB.Label lblRRContactEmail 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   0
            Left            =   -68940
            TabIndex        =   302
            Tag             =   "CLEARFIELDS"
            Top             =   1500
            Width           =   2835
         End
         Begin VB.Label lblCaption 
            Caption         =   "Account Handler"
            Height          =   255
            Index           =   93
            Left            =   -74820
            TabIndex        =   301
            Top             =   1560
            Width           =   1755
         End
         Begin VB.Label lblRRContactID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   0
            Left            =   -69840
            TabIndex        =   300
            Tag             =   "CLEARFIELDS"
            Top             =   1500
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Original JobdetailID"
            Height          =   255
            Index           =   91
            Left            =   5460
            TabIndex        =   299
            Top             =   3300
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Original JobdetailID"
            Height          =   255
            Index           =   90
            Left            =   -69480
            TabIndex        =   298
            Top             =   4020
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Rejection Reason"
            Height          =   255
            Index           =   89
            Left            =   180
            TabIndex        =   297
            Top             =   10080
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Rejection Accountability"
            Height          =   255
            Index           =   88
            Left            =   180
            TabIndex        =   296
            Top             =   9720
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Rejection Type"
            Height          =   255
            Index           =   87
            Left            =   180
            TabIndex        =   295
            Top             =   9360
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Rejection Category"
            Height          =   255
            Index           =   86
            Left            =   180
            TabIndex        =   294
            Top             =   9000
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Recommendation"
            Height          =   255
            Index           =   85
            Left            =   180
            TabIndex        =   293
            Top             =   10440
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Final Signoff"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   84
            Left            =   180
            TabIndex        =   292
            Top             =   12240
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Findings"
            Height          =   255
            Index           =   83
            Left            =   180
            TabIndex        =   291
            Top             =   5400
            Width           =   1275
         End
         Begin VB.Label lblCaption 
            Caption         =   "Assigned To Check"
            Height          =   255
            Index           =   82
            Left            =   180
            TabIndex        =   290
            Top             =   5100
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Job Completed"
            Height          =   255
            Index           =   81
            Left            =   180
            TabIndex        =   289
            Top             =   4740
            Width           =   1275
         End
         Begin VB.Label lblCaption 
            Caption         =   "Order Ref"
            Height          =   255
            Index           =   79
            Left            =   180
            TabIndex        =   288
            Top             =   4020
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Original JobID"
            Height          =   255
            Index           =   78
            Left            =   180
            TabIndex        =   287
            Top             =   3300
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Original Title"
            Height          =   255
            Index           =   77
            Left            =   180
            TabIndex        =   286
            Top             =   3660
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Original Operator"
            Height          =   255
            Index           =   76
            Left            =   180
            TabIndex        =   285
            Top             =   4380
            Width           =   1275
         End
         Begin VB.Label lblCaption 
            Caption         =   "Complaint"
            Height          =   255
            Index           =   75
            Left            =   180
            TabIndex        =   284
            Top             =   2040
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Company"
            Height          =   255
            Index           =   74
            Left            =   180
            TabIndex        =   283
            Top             =   900
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Contact"
            Height          =   255
            Index           =   73
            Left            =   180
            TabIndex        =   282
            Top             =   1260
            Width           =   1035
         End
         Begin VB.Label lblCompanyID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   5160
            TabIndex        =   281
            Tag             =   "CLEARFIELDS"
            Top             =   840
            Width           =   855
         End
         Begin VB.Label lblContactID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   5160
            TabIndex        =   280
            Tag             =   "CLEARFIELDS"
            Top             =   1200
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Affinity Case #"
            Height          =   255
            Index           =   72
            Left            =   -74880
            TabIndex        =   279
            Top             =   1200
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "Content Version Code"
            Height          =   255
            Index           =   71
            Left            =   -74820
            TabIndex        =   278
            Top             =   2520
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Affinity Case Number"
            Height          =   255
            Index           =   70
            Left            =   -74820
            TabIndex        =   277
            Top             =   1800
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Date Signed Off"
            Height          =   255
            Index           =   69
            Left            =   -74820
            TabIndex        =   276
            Top             =   11460
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Date Checked"
            Height          =   255
            Index           =   68
            Left            =   -74820
            TabIndex        =   275
            Top             =   7020
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Master Source ID 1"
            Height          =   255
            Index           =   67
            Left            =   -74820
            TabIndex        =   274
            Top             =   5220
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Date Master Arrived"
            Height          =   255
            Index           =   66
            Left            =   -71040
            TabIndex        =   273
            Top             =   5220
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Date Arrived"
            Height          =   255
            Index           =   65
            Left            =   -74820
            TabIndex        =   272
            Top             =   4860
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Final Signoff"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   64
            Left            =   -74820
            TabIndex        =   271
            Top             =   11040
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Findings"
            Height          =   255
            Index           =   63
            Left            =   -74820
            TabIndex        =   270
            Top             =   7380
            Width           =   1275
         End
         Begin VB.Label lblCaption 
            Caption         =   "Assigned To Check"
            Height          =   255
            Index           =   62
            Left            =   -74820
            TabIndex        =   269
            Top             =   6660
            Width           =   1815
         End
         Begin VB.Label lblCompanyID 
            Appearance      =   0  'Flat
            BackColor       =   &H0080FFFF&
            ForeColor       =   &H80000008&
            Height          =   315
            Index           =   4
            Left            =   -64560
            TabIndex        =   268
            Tag             =   "CLEARFIELDS"
            Top             =   11340
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Contact"
            Height          =   255
            Index           =   61
            Left            =   -74820
            TabIndex        =   267
            Top             =   1440
            Width           =   1035
         End
         Begin VB.Label lblContactID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   4
            Left            =   -69840
            TabIndex        =   266
            Tag             =   "CLEARFIELDS"
            Top             =   1380
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Original Title"
            Height          =   255
            Index           =   60
            Left            =   -74820
            TabIndex        =   265
            Top             =   2160
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Complaint"
            Height          =   255
            Index           =   59
            Left            =   -74820
            TabIndex        =   264
            Top             =   2880
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Assigned To Check"
            Height          =   255
            Index           =   58
            Left            =   -74820
            TabIndex        =   263
            Top             =   5820
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Original Operator"
            Height          =   255
            Index           =   57
            Left            =   -74880
            TabIndex        =   262
            Top             =   4140
            Width           =   1275
         End
         Begin VB.Label lblCaption 
            Caption         =   "Original Operator"
            Height          =   255
            Index           =   56
            Left            =   -74880
            TabIndex        =   261
            Top             =   3780
            Width           =   1275
         End
         Begin VB.Label lblCaption 
            Caption         =   "Original Operator"
            Height          =   255
            Index           =   55
            Left            =   -74880
            TabIndex        =   260
            Top             =   6360
            Width           =   1275
         End
         Begin VB.Label lblCaption 
            Caption         =   "Original Operator"
            Height          =   255
            Index           =   54
            Left            =   -74820
            TabIndex        =   259
            Top             =   5100
            Width           =   1275
         End
         Begin VB.Label lblCaption 
            Caption         =   "Findings"
            Height          =   255
            Index           =   53
            Left            =   -74880
            TabIndex        =   258
            Top             =   6480
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "iTunes Vendor ID"
            Height          =   255
            Index           =   52
            Left            =   -74880
            TabIndex        =   257
            Top             =   3780
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Order Number"
            Height          =   255
            Index           =   51
            Left            =   -74880
            TabIndex        =   256
            Top             =   1620
            Width           =   1455
         End
         Begin VB.Label lblOriginaliTunesVendorID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   2
            Left            =   -73320
            TabIndex        =   255
            Top             =   3720
            Width           =   4275
         End
         Begin VB.Label lblOriginalOrderNumber 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   2
            Left            =   -73320
            TabIndex        =   254
            Top             =   1560
            Width           =   4275
         End
         Begin VB.Label lblOriginalSeries 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   -73320
            TabIndex        =   253
            Top             =   2280
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Series"
            Height          =   255
            Index           =   50
            Left            =   -74880
            TabIndex        =   252
            Top             =   2340
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Volume"
            Height          =   255
            Index           =   49
            Left            =   -74880
            TabIndex        =   251
            Top             =   3060
            Width           =   1515
         End
         Begin VB.Label lblOriginalVolume 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   2
            Left            =   -73320
            TabIndex        =   250
            Top             =   3000
            Width           =   4275
         End
         Begin VB.Label lblOriginalSeries 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   2
            Left            =   -73320
            TabIndex        =   249
            Top             =   2640
            Width           =   1875
         End
         Begin VB.Label lblCaption 
            Caption         =   "Series"
            Height          =   255
            Index           =   48
            Left            =   -74880
            TabIndex        =   248
            Top             =   2700
            Width           =   1515
         End
         Begin VB.Label lblReference 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   2
            Left            =   -73320
            TabIndex        =   247
            Top             =   1200
            Width           =   4275
         End
         Begin VB.Label lblOriginalTitle 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   2
            Left            =   -73320
            TabIndex        =   246
            Top             =   1920
            Width           =   4275
         End
         Begin VB.Label lblOriginalSubtitle 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   2
            Left            =   -73320
            TabIndex        =   245
            Top             =   1560
            Width           =   4275
         End
         Begin VB.Label lblOriginalEpisode 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   2
            Left            =   -73320
            TabIndex        =   244
            Top             =   3360
            Width           =   1875
         End
         Begin VB.Label lblCaption 
            Caption         =   "Reference"
            Height          =   255
            Index           =   47
            Left            =   -74880
            TabIndex        =   243
            Top             =   1260
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "Episode"
            Height          =   255
            Index           =   14
            Left            =   -74880
            TabIndex        =   242
            Top             =   3420
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Title"
            Height          =   255
            Index           =   11
            Left            =   -74880
            TabIndex        =   241
            Top             =   1980
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Final Signoff"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   10
            Left            =   -74910
            TabIndex        =   240
            Top             =   12180
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Assigned To Check"
            Height          =   255
            Index           =   9
            Left            =   -74880
            TabIndex        =   239
            Top             =   6120
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Rejection Issue"
            Height          =   255
            Index           =   7
            Left            =   -74880
            TabIndex        =   238
            Top             =   4500
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Original Title"
            Height          =   255
            Index           =   18
            Left            =   -74820
            TabIndex        =   237
            Top             =   4380
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Rejection Issue"
            Height          =   255
            Index           =   46
            Left            =   -74880
            TabIndex        =   236
            Top             =   4140
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Assigned To Check"
            Height          =   255
            Index           =   45
            Left            =   -74880
            TabIndex        =   235
            Top             =   5760
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Findings"
            Height          =   255
            Index           =   44
            Left            =   -74880
            TabIndex        =   234
            Top             =   5760
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Final Signoff"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   43
            Left            =   -74850
            TabIndex        =   233
            Top             =   11760
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Title"
            Height          =   255
            Index           =   42
            Left            =   -74880
            TabIndex        =   232
            Top             =   1620
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Episode Title"
            Height          =   255
            Index           =   41
            Left            =   -74880
            TabIndex        =   231
            Top             =   1980
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Episode"
            Height          =   255
            Index           =   40
            Left            =   -71520
            TabIndex        =   230
            Top             =   2340
            Width           =   675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Original Ingest Date"
            Height          =   255
            Index           =   39
            Left            =   -74880
            TabIndex        =   229
            Top             =   2700
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "XML Sent"
            Height          =   255
            Index           =   19
            Left            =   -71520
            TabIndex        =   228
            Top             =   2700
            Width           =   735
         End
         Begin VB.Label lblCaption 
            Caption         =   "Decision Tree Date"
            Height          =   255
            Index           =   17
            Left            =   -74880
            TabIndex        =   227
            Top             =   3060
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Fix Jobsheet #"
            Height          =   255
            Index           =   16
            Left            =   -74880
            TabIndex        =   226
            Top             =   3420
            Width           =   1515
         End
         Begin VB.Label lblOriginalXmlDeliveryDate 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   -73320
            TabIndex        =   225
            Top             =   2640
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Reference"
            Height          =   255
            Index           =   12
            Left            =   -74880
            TabIndex        =   224
            Top             =   1260
            Width           =   1455
         End
         Begin VB.Label lblOriginalIngestDate 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   -70800
            TabIndex        =   223
            Top             =   2640
            Width           =   1755
         End
         Begin VB.Label lblOriginalEpisode 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   -70800
            TabIndex        =   222
            Top             =   2280
            Width           =   1755
         End
         Begin VB.Label lblDecisionTreeDate 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   -73320
            TabIndex        =   221
            Top             =   3000
            Width           =   1755
         End
         Begin VB.Label lblFixJobID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   -73320
            TabIndex        =   220
            Top             =   3360
            Width           =   1755
         End
         Begin VB.Label lblOriginalSubtitle 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   -73320
            TabIndex        =   219
            Top             =   1920
            Width           =   4275
         End
         Begin VB.Label lblOriginalTitle 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   -73320
            TabIndex        =   218
            Top             =   1560
            Width           =   4275
         End
         Begin VB.Label lblReference 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   1
            Left            =   -73320
            TabIndex        =   217
            Top             =   1200
            Width           =   4275
         End
         Begin VB.Label lblContentVersionCode 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Left            =   -69660
            TabIndex        =   216
            Top             =   1500
            Width           =   1935
         End
         Begin VB.Label lblCaption 
            Caption         =   "Content Vers Code"
            Height          =   255
            Index           =   37
            Left            =   -71220
            TabIndex        =   215
            Top             =   1560
            Width           =   1515
         End
         Begin VB.Label lblReference 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   0
            Left            =   -73320
            TabIndex        =   214
            Top             =   1860
            Width           =   4275
         End
         Begin VB.Label lblOriginalTitle 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   0
            Left            =   -73320
            TabIndex        =   213
            Top             =   2220
            Width           =   4275
         End
         Begin VB.Label lblOriginalSubtitle 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   0
            Left            =   -73320
            TabIndex        =   212
            Top             =   2580
            Width           =   4275
         End
         Begin VB.Label lblFixJobID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   0
            Left            =   -69480
            TabIndex        =   211
            Top             =   4380
            Width           =   1755
         End
         Begin VB.Label lblOffPendingDate 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Left            =   -69480
            TabIndex        =   210
            Top             =   4020
            Width           =   1755
         End
         Begin VB.Label lblDecisionTreeDate 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   0
            Left            =   -73320
            TabIndex        =   209
            Top             =   4380
            Width           =   1755
         End
         Begin VB.Label lblFileStore 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   0
            Left            =   -73320
            TabIndex        =   208
            Top             =   4020
            Width           =   1755
         End
         Begin VB.Label lblOriginalEpisode 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   0
            Left            =   -73320
            TabIndex        =   207
            Top             =   2940
            Width           =   915
         End
         Begin VB.Label lblOriginalIngestDate 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   0
            Left            =   -73320
            TabIndex        =   206
            Top             =   3300
            Width           =   1755
         End
         Begin VB.Label lblMasterFormat 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Left            =   -73320
            TabIndex        =   205
            Top             =   3660
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Reference"
            Height          =   255
            Index           =   38
            Left            =   -74880
            TabIndex        =   204
            Top             =   1920
            Width           =   1515
         End
         Begin VB.Label lblBarcode 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Left            =   -69480
            TabIndex        =   203
            Top             =   3660
            Width           =   1755
         End
         Begin VB.Label lblOriginalXmlDeliveryDate 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   0
            Left            =   -69480
            TabIndex        =   202
            Top             =   3300
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Final Signoff"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   36
            Left            =   -74850
            TabIndex        =   201
            Top             =   12240
            Width           =   1215
         End
         Begin VB.Label lblCaption 
            Caption         =   "Findings"
            Height          =   255
            Index           =   35
            Left            =   -74880
            TabIndex        =   200
            Top             =   8340
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Assigned To Check"
            Height          =   255
            Index           =   34
            Left            =   -74880
            TabIndex        =   199
            Top             =   7980
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Rejection Issue"
            Height          =   255
            Index           =   33
            Left            =   -74880
            TabIndex        =   198
            Top             =   6720
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Fix Jobsheet #"
            Height          =   255
            Index           =   32
            Left            =   -71340
            TabIndex        =   197
            Top             =   4440
            Width           =   1395
         End
         Begin VB.Label lblCaption 
            Caption         =   "Off Pending Date"
            Height          =   255
            Index           =   31
            Left            =   -71340
            TabIndex        =   196
            Top             =   4080
            Width           =   1395
         End
         Begin VB.Label lblCaption 
            Caption         =   "Decision Tree Date"
            Height          =   255
            Index           =   30
            Left            =   -74880
            TabIndex        =   195
            Top             =   4440
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Stored On"
            Height          =   255
            Index           =   29
            Left            =   -74880
            TabIndex        =   194
            Top             =   4080
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Barcode"
            Height          =   255
            Index           =   28
            Left            =   -71340
            TabIndex        =   193
            Top             =   3720
            Width           =   1395
         End
         Begin VB.Label lblCaption 
            Caption         =   "Format"
            Height          =   255
            Index           =   27
            Left            =   -74880
            TabIndex        =   192
            Top             =   3720
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "XML Sent"
            Height          =   255
            Index           =   26
            Left            =   -71340
            TabIndex        =   191
            Top             =   3360
            Width           =   1395
         End
         Begin VB.Label lblCaption 
            Caption         =   "Original Ingest Date"
            Height          =   255
            Index           =   25
            Left            =   -74880
            TabIndex        =   190
            Top             =   3360
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Episode"
            Height          =   255
            Index           =   24
            Left            =   -74880
            TabIndex        =   189
            Top             =   3000
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Episode Title"
            Height          =   255
            Index           =   23
            Left            =   -74880
            TabIndex        =   188
            Top             =   2640
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Title"
            Height          =   255
            Index           =   22
            Left            =   -74880
            TabIndex        =   187
            Top             =   2280
            Width           =   1515
         End
         Begin VB.Label lblOriginalAlphaCode 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Left            =   -73320
            TabIndex        =   186
            Top             =   1500
            Width           =   1935
         End
         Begin VB.Label lblCaption 
            Caption         =   "Alpha Code"
            Height          =   255
            Index           =   21
            Left            =   -74880
            TabIndex        =   185
            Top             =   1560
            Width           =   1455
         End
         Begin VB.Label lblContactID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   0
            Left            =   -68940
            TabIndex        =   184
            Tag             =   "CLEARFIELDS"
            Top             =   1140
            Width           =   855
         End
         Begin VB.Label lblCompanyID 
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Index           =   0
            Left            =   -68940
            TabIndex        =   183
            Tag             =   "CLEARFIELDS"
            Top             =   780
            Width           =   855
         End
         Begin VB.Label lblCaption 
            Caption         =   "Contact"
            Height          =   255
            Index           =   5
            Left            =   -74820
            TabIndex        =   182
            Top             =   1200
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Company"
            Height          =   255
            Index           =   2
            Left            =   -74820
            TabIndex        =   181
            Top             =   840
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Complaint"
            Height          =   255
            Index           =   0
            Left            =   -74820
            TabIndex        =   180
            Top             =   1980
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Original JobID"
            Height          =   255
            Index           =   1
            Left            =   -74820
            TabIndex        =   179
            Top             =   4020
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Order Ref"
            Height          =   255
            Index           =   3
            Left            =   -74820
            TabIndex        =   178
            Top             =   4680
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Job Completed"
            Height          =   255
            Index           =   6
            Left            =   -74820
            TabIndex        =   177
            Top             =   5460
            Width           =   1275
         End
         Begin VB.Label lblCaption 
            Caption         =   "Findings"
            Height          =   255
            Index           =   8
            Left            =   -74820
            TabIndex        =   176
            Top             =   6180
            Width           =   1275
         End
         Begin VB.Label lblCaption 
            Caption         =   "Final Signoff"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   15
            Left            =   -74820
            TabIndex        =   175
            Top             =   12120
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Recommendation"
            Height          =   255
            Index           =   20
            Left            =   -74790
            TabIndex        =   174
            Top             =   10320
            Width           =   1755
         End
      End
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "BBCWW Unresolved"
      Height          =   315
      Index           =   14
      Left            =   25260
      TabIndex        =   365
      Tag             =   "NOCLEAR"
      Top             =   6960
      Value           =   1  'Checked
      Width           =   2895
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "DADC (Non-MX1) Un-resolved"
      Height          =   315
      Index           =   13
      Left            =   25260
      TabIndex        =   364
      Tag             =   "NOCLEAR"
      Top             =   6600
      Value           =   1  'Checked
      Width           =   2895
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "DADC DSP Un-resolved"
      Height          =   315
      Index           =   12
      Left            =   25260
      TabIndex        =   363
      Tag             =   "NOCLEAR"
      Top             =   6240
      Value           =   1  'Checked
      Width           =   2895
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "DADC Svensk Un-resolved"
      Height          =   315
      Index           =   11
      Left            =   25260
      TabIndex        =   362
      Tag             =   "NOCLEAR"
      Top             =   5880
      Value           =   1  'Checked
      Width           =   2895
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "DADC BBC Un-resolved"
      Height          =   315
      Index           =   10
      Left            =   25260
      TabIndex        =   361
      Tag             =   "NOCLEAR"
      Top             =   5520
      Value           =   1  'Checked
      Width           =   2895
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "Normal Un-resolved"
      Height          =   315
      Index           =   9
      Left            =   25260
      TabIndex        =   360
      Tag             =   "NOCLEAR"
      Top             =   5160
      Value           =   1  'Checked
      Width           =   2895
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "BBCWW Resolved"
      Height          =   315
      Index           =   8
      Left            =   25260
      TabIndex        =   359
      Tag             =   "NOCLEAR"
      Top             =   9480
      Width           =   2895
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "DADC (NonMX1) Resolved"
      Height          =   315
      Index           =   7
      Left            =   25260
      TabIndex        =   358
      Tag             =   "NOCLEAR"
      Top             =   9120
      Width           =   2895
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "DADC DSP Resolved"
      Height          =   315
      Index           =   6
      Left            =   25260
      TabIndex        =   357
      Tag             =   "NOCLEAR"
      Top             =   8760
      Width           =   2895
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "DADC Svensk Resolved"
      Height          =   315
      Index           =   5
      Left            =   25260
      TabIndex        =   356
      Tag             =   "NOCLEAR"
      Top             =   8400
      Width           =   2895
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "DADC BBC Resolved"
      Height          =   315
      Index           =   4
      Left            =   25260
      TabIndex        =   355
      Tag             =   "NOCLEAR"
      Top             =   8040
      Width           =   2895
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "Normal Resokved"
      Height          =   315
      Index           =   3
      Left            =   25260
      TabIndex        =   354
      Tag             =   "NOCLEAR"
      Top             =   7680
      Width           =   2775
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "All Resolved"
      Height          =   315
      Index           =   1
      Left            =   25260
      TabIndex        =   353
      Tag             =   "NOCLEAR"
      Top             =   7320
      Width           =   1455
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "All"
      Height          =   315
      Index           =   0
      Left            =   25260
      TabIndex        =   352
      Tag             =   "NOCLEAR"
      Top             =   9840
      Width           =   2895
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "All Unresolved"
      Height          =   315
      Index           =   2
      Left            =   25260
      TabIndex        =   351
      Tag             =   "NOCLEAR"
      Top             =   4800
      Value           =   1  'Checked
      Width           =   1455
   End
   Begin VB.TextBox txtSearchTitle 
      BackColor       =   &H00FFC0FF&
      Height          =   315
      Left            =   25260
      TabIndex        =   344
      Top             =   2580
      Width           =   2775
   End
   Begin VB.TextBox txtSearchJobID 
      BackColor       =   &H00FFC0FF&
      Height          =   315
      Left            =   25260
      TabIndex        =   342
      Top             =   2220
      Width           =   2775
   End
   Begin VB.ComboBox cmbSearchInternalExternal 
      BackColor       =   &H00FFC0FF&
      Height          =   315
      Left            =   25260
      TabIndex        =   29
      ToolTipText     =   "The machine the Tape was reviewed on"
      Top             =   4020
      Width           =   2835
   End
   Begin VB.ComboBox cmbFilterOriginalOperator 
      BackColor       =   &H00FFC0FF&
      Height          =   315
      Left            =   25260
      TabIndex        =   25
      ToolTipText     =   "The machine the Tape was reviewed on"
      Top             =   3300
      Width           =   2835
   End
   Begin VB.ComboBox cmbFilterInternalDepartment 
      BackColor       =   &H00FFC0FF&
      Height          =   315
      Left            =   25260
      TabIndex        =   23
      ToolTipText     =   "The machine the Tape was reviewed on"
      Top             =   2940
      Width           =   2835
   End
   Begin VB.Frame FraDateChoice 
      Caption         =   "Dates Filters Against..."
      Height          =   855
      Left            =   25260
      TabIndex        =   17
      Top             =   1260
      Width           =   3135
      Begin VB.OptionButton optDateFilter 
         Caption         =   "Date Issue Created"
         Height          =   255
         Index           =   0
         Left            =   180
         TabIndex        =   22
         Top             =   240
         Value           =   -1  'True
         Width           =   2655
      End
      Begin VB.OptionButton optDateFilter 
         Caption         =   "Date Issue Signed Off"
         Height          =   255
         Index           =   1
         Left            =   180
         TabIndex        =   21
         Top             =   540
         Width           =   2655
      End
   End
   Begin MSAdodcLib.Adodc adoComplaints 
      Height          =   330
      Left            =   60
      Top             =   120
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdComplaints 
      Bindings        =   "frmComplaint.frx":6A13
      Height          =   9735
      Left            =   60
      TabIndex        =   4
      Top             =   120
      Width           =   22995
      _Version        =   196617
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   24
      Columns(0).Width=   847
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "complaintID"
      Columns(0).DataField=   "complaintID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   4233
      Columns(1).Caption=   "Client"
      Columns(1).Name =   "Client"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "Contact"
      Columns(2).Name =   "Contact"
      Columns(2).FieldLen=   256
      Columns(3).Width=   7170
      Columns(3).Caption=   "Title"
      Columns(3).Name =   "Title"
      Columns(3).FieldLen=   256
      Columns(4).Width=   2011
      Columns(4).Caption=   "Original Job #"
      Columns(4).Name =   "originaljobID"
      Columns(4).DataField=   "originaljobID"
      Columns(4).FieldLen=   256
      Columns(5).Width=   2831
      Columns(5).Caption=   "Original Operator"
      Columns(5).Name =   "originaloperator"
      Columns(5).DataField=   "originaloperator"
      Columns(5).FieldLen=   256
      Columns(6).Width=   2831
      Columns(6).Caption=   "Account Handler"
      Columns(6).Name =   "RRContactName"
      Columns(6).DataField=   "RRContactName"
      Columns(6).FieldLen=   256
      Columns(7).Width=   1773
      Columns(7).Caption=   "Created"
      Columns(7).Name =   "cdate"
      Columns(7).DataField=   "cdate"
      Columns(7).FieldLen=   256
      Columns(8).Width=   2831
      Columns(8).Caption=   "Assigned To"
      Columns(8).Name =   "assignedto"
      Columns(8).DataField=   "assignedto"
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "Target Date"
      Columns(9).Name =   "TargetDate"
      Columns(9).DataField=   "TargetDate"
      Columns(9).FieldLen=   256
      Columns(10).Width=   1773
      Columns(10).Caption=   "Updated"
      Columns(10).Name=   "mdate"
      Columns(10).DataField=   "mdate"
      Columns(10).FieldLen=   256
      Columns(11).Width=   6853
      Columns(11).Caption=   "complaint"
      Columns(11).Name=   "complaint"
      Columns(11).DataField=   "complaint"
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "companyID"
      Columns(12).Name=   "companyID"
      Columns(12).DataField=   "companyID"
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "contactID"
      Columns(13).Name=   "contactID"
      Columns(13).DataField=   "contactID"
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "Signed Off"
      Columns(14).Name=   "signoffby"
      Columns(14).DataField=   "signoffby"
      Columns(14).FieldLen=   256
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "Tracker"
      Columns(15).Name=   "complainttype"
      Columns(15).DataField=   "complainttype"
      Columns(15).FieldLen=   256
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "originaldadctrackerID"
      Columns(16).Name=   "originaldadctrackerID"
      Columns(16).DataField=   "originaldadctrackerID"
      Columns(16).FieldLen=   256
      Columns(17).Width=   3889
      Columns(17).Caption=   "MX1 Department"
      Columns(17).Name=   "IssueInternalDepartment"
      Columns(17).DataField=   "IssueInternalDepartment"
      Columns(17).FieldLen=   256
      Columns(18).Width=   2381
      Columns(18).Caption=   "Type"
      Columns(18).Name=   "Type"
      Columns(18).DataField=   "Column 14"
      Columns(18).DataType=   8
      Columns(18).FieldLen=   256
      Columns(19).Width=   3200
      Columns(19).Visible=   0   'False
      Columns(19).Caption=   "originalsvensktrackerID"
      Columns(19).Name=   "originalsvensktrackerID"
      Columns(19).DataField=   "originalsvensktrackerID"
      Columns(19).FieldLen=   256
      Columns(20).Width=   3200
      Columns(20).Visible=   0   'False
      Columns(20).Caption=   "originalitunestrackerID"
      Columns(20).Name=   "originalitunestrackerID"
      Columns(20).DataField=   "originalitunestrackerID"
      Columns(20).FieldLen=   256
      Columns(21).Width=   3200
      Columns(21).Visible=   0   'False
      Columns(21).Caption=   "ReadyToBill"
      Columns(21).Name=   "ReadyToBill"
      Columns(21).DataField=   "ReadyToBill"
      Columns(21).FieldLen=   256
      Columns(22).Width=   3200
      Columns(22).Visible=   0   'False
      Columns(22).Caption=   "Billed"
      Columns(22).Name=   "Billed"
      Columns(22).DataField=   "Billed"
      Columns(22).FieldLen=   256
      Columns(23).Width=   3200
      Columns(23).Visible=   0   'False
      Columns(23).Caption=   "JobID"
      Columns(23).Name=   "JobID"
      Columns(23).DataField=   "JobID"
      Columns(23).FieldLen=   256
      _ExtentX        =   40561
      _ExtentY        =   17171
      _StockProps     =   79
      Caption         =   "Issues"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   120
      Top             =   11820
   End
   Begin VB.PictureBox picFooter 
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   7020
      ScaleHeight     =   315
      ScaleWidth      =   15195
      TabIndex        =   3
      Top             =   14460
      Width           =   15195
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         Height          =   315
         Left            =   12780
         TabIndex        =   30
         ToolTipText     =   "Save changes to this job (seconds since last save!)"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdSendEmail 
         Caption         =   "Send Email to MX1 Contact"
         Height          =   315
         Left            =   2820
         TabIndex        =   15
         ToolTipText     =   "Save and Print"
         Top             =   0
         Width           =   2175
      End
      Begin VB.CommandButton cmdOutputiTunesReport 
         Caption         =   "Output Report (As Filtered)"
         Height          =   315
         Left            =   5100
         TabIndex        =   11
         ToolTipText     =   "Save and Print"
         Top             =   0
         Width           =   2655
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "Delete"
         Height          =   315
         Left            =   9060
         TabIndex        =   7
         ToolTipText     =   "Save and Print"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Refresh"
         Height          =   315
         Left            =   10260
         TabIndex        =   6
         ToolTipText     =   "Save and Print"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   7860
         TabIndex        =   0
         ToolTipText     =   "Save and Print"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "New Item"
         Height          =   315
         Left            =   11580
         TabIndex        =   1
         ToolTipText     =   "Clear the form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   13980
         TabIndex        =   2
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1155
      End
   End
   Begin MSAdodcLib.Adodc adoContact 
      Height          =   330
      Left            =   120
      Top             =   11460
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoContact"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoCompany 
      Height          =   330
      Left            =   120
      Top             =   11100
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCompany"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoOriginalContact 
      Height          =   330
      Index           =   0
      Left            =   2460
      Top             =   11100
      Visible         =   0   'False
      Width           =   3165
      _ExtentX        =   5583
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoOriginalContact"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoOriginalContact 
      Height          =   330
      Index           =   1
      Left            =   2460
      Top             =   11460
      Visible         =   0   'False
      Width           =   3165
      _ExtentX        =   5583
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoOriginalContact"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSearchCompany 
      Bindings        =   "frmComplaint.frx":6A2F
      Height          =   315
      Left            =   25260
      TabIndex        =   12
      ToolTipText     =   "The company this quote is for"
      Top             =   60
      Width           =   3195
      BevelWidth      =   0
      DataFieldList   =   "name"
      BevelType       =   0
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   5636
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin MSAdodcLib.Adodc adoCetaUser 
      Height          =   330
      Left            =   5700
      Top             =   11100
      Visible         =   0   'False
      Width           =   3165
      _ExtentX        =   5583
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCetaUser"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker datStartDate 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   25260
      TabIndex        =   16
      Tag             =   "NOCHECK"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   480
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   154140673
      CurrentDate     =   37870
   End
   Begin MSComCtl2.DTPicker datEndDate 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   25260
      TabIndex        =   19
      Tag             =   "NOCHECK"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   840
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   154140673
      CurrentDate     =   37870
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbFilterCetaUser 
      Bindings        =   "frmComplaint.frx":6A48
      Height          =   315
      Left            =   25260
      TabIndex        =   27
      ToolTipText     =   "The contact for this quote"
      Top             =   3660
      Width           =   2835
      DataFieldList   =   "fullname"
      BevelType       =   0
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   5424
      Columns(0).Caption=   "Full Name"
      Columns(0).Name =   "fullname"
      Columns(0).DataField=   "fullname"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "cetauserID"
      Columns(1).Name =   "cetauserID"
      Columns(1).DataField=   "cetauserID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "email"
      Columns(2).Name =   "email"
      Columns(2).DataField=   "email"
      Columns(2).FieldLen=   256
      _ExtentX        =   5001
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "fullname"
   End
   Begin VB.Label lblCaption 
      Caption         =   "Filter Issue Type"
      Height          =   255
      Index           =   127
      Left            =   23220
      TabIndex        =   369
      Top             =   4440
      Width           =   1995
   End
   Begin VB.Label lblCaption 
      Caption         =   "Filter Title"
      Height          =   255
      Index           =   125
      Left            =   23220
      TabIndex        =   345
      Top             =   2640
      Width           =   1335
   End
   Begin VB.Label lblCaption 
      Caption         =   "Filter Original Job #"
      Height          =   255
      Index           =   124
      Left            =   23220
      TabIndex        =   343
      Top             =   2280
      Width           =   1995
   End
   Begin VB.Label lblCaption 
      Caption         =   "Filter Internal / External"
      Height          =   255
      Index           =   123
      Left            =   23220
      TabIndex        =   341
      Top             =   4080
      Width           =   1995
   End
   Begin VB.Label lblCaption 
      Caption         =   "Filter Account Handler"
      Height          =   255
      Index           =   119
      Left            =   23220
      TabIndex        =   28
      Top             =   3720
      Width           =   1695
   End
   Begin VB.Label lblCaption 
      Caption         =   "Filter Original Operator"
      Height          =   255
      Index           =   118
      Left            =   23220
      TabIndex        =   26
      Top             =   3360
      Width           =   1755
   End
   Begin VB.Label lblCaption 
      Caption         =   "Filter MX1 Dept"
      Height          =   255
      Index           =   117
      Left            =   23220
      TabIndex        =   24
      Top             =   3000
      Width           =   1335
   End
   Begin VB.Label lblCaption 
      Caption         =   "Filter by End Date"
      Height          =   255
      Index           =   99
      Left            =   23220
      TabIndex        =   20
      Top             =   900
      Width           =   1995
   End
   Begin VB.Label lblCaption 
      Caption         =   "Filter by Start Date"
      Height          =   255
      Index           =   13
      Left            =   23220
      TabIndex        =   18
      Top             =   540
      Width           =   1995
   End
   Begin VB.Label lblSearchCompanyID 
      Height          =   315
      Left            =   25260
      TabIndex        =   14
      Tag             =   "CLEARFIELDS"
      Top             =   10260
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Filter by Company"
      Height          =   255
      Index           =   92
      Left            =   23220
      TabIndex        =   13
      Top             =   120
      Width           =   1995
   End
   Begin VB.Label lblOriginaliTunesTrackerID 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   25260
      TabIndex        =   10
      Tag             =   "CLEARFIELDS"
      Top             =   11760
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblOriginalSvenskTrackerID 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   25260
      TabIndex        =   9
      Tag             =   "CLEARFIELDS"
      Top             =   11400
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblOriginalDADCTrackerID 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   25260
      TabIndex        =   8
      Tag             =   "CLEARFIELDS"
      Top             =   11040
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblcomplaintID 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   25260
      TabIndex        =   5
      Tag             =   "CLEARFIELDS"
      Top             =   10680
      Visible         =   0   'False
      Width           =   855
   End
End
Attribute VB_Name = "frmComplaint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim m_lngTimeSinceLastSave As Long
Dim m_strSearchFilter As String
Dim m_strCrystalSearchFilter As String
Dim m_blnSilent As Boolean
Dim m_blnBillAll As Boolean
Dim m_blnUpdating As Boolean

Private Sub chkFilter_Click(Index As Integer)

If m_blnUpdating = True Then Exit Sub

Dim counter As Integer

m_blnUpdating = True

If Index = 0 Then
    If chkFilter(Index).Value = 0 Then
        For counter = 1 To 14
            chkFilter(counter).Value = 0
        Next
    Else
        For counter = 1 To 14
            chkFilter(counter).Value = 1
        Next
    End If
ElseIf Index = 1 Then
    If chkFilter(Index).Value = 0 Then
        For counter = 3 To 8
            chkFilter(counter).Value = 0
        Next
    Else
        For counter = 3 To 8
            chkFilter(counter).Value = 1
        Next
    End If
ElseIf Index = 2 Then
    If chkFilter(Index).Value = 0 Then
        For counter = 9 To 14
            chkFilter(counter).Value = 0
        Next
    Else
        For counter = 9 To 14
            chkFilter(counter).Value = 1
        Next
    End If
End If

m_blnUpdating = False
cmdSearch.Value = True

End Sub

Private Sub cmbAssignedTo_DropDown(Index As Integer)

PopulateCombo "investigateissues", cmbAssignedTo(Index)

End Sub

Private Sub cmbCetaUser_Click(Index As Integer)

lblRRContactID(Index).Caption = cmbCetaUser(Index).Columns("cetauserID").Text
lblRRContactEmail(Index).Caption = cmbCetaUser(Index).Columns("email").Text

End Sub

Private Sub cmbInternalDepartment_DropDown(Index As Integer)

PopulateCombo "IssueInternalDepartment", cmbInternalDepartment(Index)

End Sub

Private Sub cmbInternalExternal_DropDown(Index As Integer)

PopulateCombo "internal/external", cmbInternalExternal(Index)

End Sub

Private Sub cmbIssueType_DropDown(Index As Integer)

PopulateCombo "IssueType", cmbIssueType(Index)

End Sub

Private Sub cmbOriginalOperator_DropDown(Index As Integer)

PopulateCombo "users", cmbOriginalOperator(Index)

End Sub

Private Sub cmbSearchCompany_Click()

lblSearchCompanyID.Caption = cmbSearchCompany.Columns("companyID").Text

End Sub

Private Sub cmbSearchInternalExternal_DropDown()

PopulateCombo "Internal/External", cmbSearchInternalExternal

End Sub

Private Sub cmbSearchIssueType_DropDown()

PopulateCombo "IssueType", cmbSearchIssueType

End Sub

Private Sub cmbSignoff_DropDown(Index As Integer)

PopulateCombo "signoffissues", cmbSignoff(Index)

End Sub

Private Sub cmdBillAllItems_Click()

Dim l_rstBilling As ADODB.Recordset, l_strSQL As String, l_strTempSearch

l_strTempSearch = m_strSearchFilter

m_strSearchFilter = "WHERE complainttype = 4 AND readytobill <> 0 and billed = 0 "
cmdSearch.Value = True

If adoComplaints.Recordset.RecordCount > 0 Then
    If MsgBox("The Grid is now showing the items to be billed." & vbCrLf & "Proceed?", vbYesNo, "Billing Affinity Investigations") = vbYes Then
        m_blnBillAll = True
        adoComplaints.Recordset.MoveFirst
        Do While Not adoComplaints.Recordset.EOF
            cmdBillItem.Value = True
            adoComplaints.Recordset.MoveNext
        Loop
        m_blnBillAll = False
    End If
End If

m_strSearchFilter = l_strTempSearch
cmdSearch.Value = True

End Sub

Private Sub cmdBillItem_Click()

Dim l_rstBilling As ADODB.Recordset, l_strDescription As String, l_lngJobID As Long, l_lngCount As Long

If Val(lblcomplaintID.Caption) = 0 Then Exit Sub
If GetData("complaint", "Complainttype", "ComplaintID", lblcomplaintID.Caption) <> 4 Then Exit Sub

Set l_rstBilling = ExecuteSQL("SELECT * FROM complaint WHERE complaintID = " & lblcomplaintID.Caption, g_strExecuteError)
If l_rstBilling.RecordCount <= 0 Then
    MsgBox "This item cannot be found.", vbCritical, "Error..."
    Exit Sub
End If

If l_rstBilling("readytobill") = 0 Or l_rstBilling("billed") <> 0 Then
    MsgBox "This item is not yet ready to be billed", vbCritical, "Error..."
    Exit Sub
End If

If l_rstBilling("affinitycasenumber") = "" Then
    MsgBox "This item is not in a state to be billed - no Affinity Case Number", vbCritical, "Error..."
    Exit Sub
End If

If l_rstBilling("readytobill") <> 0 And l_rstBilling("billed") = 0 Then
    If frmJob.txtJobID.Text <> "" Then
        'A job is loaded - now check that it isn't locked.
        l_lngJobID = Val(frmJob.txtJobID.Text)
        If frmJob.txtStatus.Text = "Confirmed" And frmJob.lblCompanyID.Caption = lblCompanyID(4).Caption Then
            l_strDescription = l_rstBilling("affinitycasenumber") & " - Investigation"
            MakeJobDetailLine l_lngJobID, "O", l_strDescription, 1, "DADCISSUECHECK", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0
            frmJob.adoJobDetail.Refresh
            l_rstBilling("billed") = 1
            l_rstBilling.Update
        Else
            MsgBox "Biliong Job is not corrct company or state", vbCritical, "Error..."
            Exit Sub
        End If
    Else
        MsgBox "No Billing Job loaded", vbCritical, "Error..."
        Exit Sub
    End If
End If

l_rstBilling.Close
If m_blnBillAll = False Then cmdSearch.Value = True

End Sub

Private Sub cmdCreateRedoJob_Click(Index As Integer)

Dim l_lngOldJobID As Long, l_lngOldJobdetailID As Long, l_lngNewJobID As Long, l_rsOld As ADODB.Recordset, l_rsNew As ADODB.Recordset, l_lngCount As Long

If tabComplaintType.Tab = 5 Then
    If MsgBox("Normally, BBCWW will issue a new Star Order." & vbCrLf & "Are You sure?", vbYesNo, "Making a new Redo Job for this item.") = vbNo Then
        Exit Sub
    End If
End If

l_lngOldJobID = Val(txtOriginalJobID(Index).Text)
l_lngOldJobdetailID = Val(txtOriginalJobdetailID(Index).Text)

If l_lngOldJobID = 0 Or l_lngOldJobdetailID = 0 Then
    MsgBox "Cannot autocreate a new Job unoless original JobID and original JobdetailID have values", vbCritical, "Error"
    Exit Sub
End If

ShowJob l_lngOldJobID, 1, True
frmJob.txtJobID.Text = ""
SaveJob
l_lngNewJobID = Val(frmJob.txtJobID.Text)
If l_lngNewJobID = 0 Then
    MsgBox "There was an error creating the new Job", vbCritical, "Error"
    Exit Sub
End If

SetData "job", "invoiceddate", "jobID", l_lngNewJobID, Null
SetData "job", "invoicenumber", "jobID", l_lngNewJobID, Null
SetData "job", "invoiceduser", "jobID", l_lngNewJobID, Null
SetData "job", "invoiceduserID", "jobID", l_lngNewJobID, Null
SetData "job", "senttoaccountsbatchnumber", "jobID", l_lngNewJobID, Null
SetData "job", "senttoaccountsdate", "jobID", l_lngNewJobID, Null
SetData "job", "senttoaccountsuser", "jobID", l_lngNewJobID, Null
SetData "job", "senttoaccountsuserID", "jobID", l_lngNewJobID, Null
SetData "job", "creditnotenumber", "jobID", l_lngNewJobID, Null
SetData "job", "creditnotebatchnumber", "jobID", l_lngNewJobID, Null
SetData "job", "creditnotedate", "jobID", l_lngNewJobID, Null
SetData "job", "creditnoteuser", "jobID", l_lngNewJobID, Null
SetData "job", "creditnoteuserID", "jobID", l_lngNewJobID, Null
SetData "job", "creditsenttoaccountsdate", "jobID", l_lngNewJobID, Null
SetData "job", "creditsenttoaccountsuser", "jobID", l_lngNewJobID, Null
SetData "job", "refundedcreditnotenumber", "jobID", l_lngNewJobID, Null
SetData "job", "senttoBBCdate", "jobID", l_lngNewJobID, Null
SetData "job", "senttoBBCbatchnumber", "jobID", l_lngNewJobID, Null
SetData "job", "creditsenttoBBCdate", "jobID", l_lngNewJobID, Null
SetData "job", "creditsenttoBBCbatchnumber", "jobID", l_lngNewJobID, Null
SetData "job", "DeadlineDate", "jobID", l_lngNewJobID, FormatSQLDate(Now)
SetData "job", "redojob", "jobID", l_lngNewJobID, 1
SetData "job", "orderreference", "jobID", l_lngNewJobID, frmJob.txtOrderReference.Text & "_Redo"

SetData "job", "notes1", "jobID", l_lngNewJobID, frmJob.txtOfficeClient.Text
SetData "job", "notes2", "jobID", l_lngNewJobID, frmJob.txtOperator.Text
SetData "job", "notes3", "jobID", l_lngNewJobID, frmJob.txtDespatch.Text
SetData "job", "notes5", "jobID", l_lngNewJobID, frmJob.txtProducerNotes.Text
            
Set l_rsOld = ExecuteSQL("SELECT * FROM jobdetail WHERE jobdetailID = " & l_lngOldJobdetailID, g_strExecuteError)
If l_rsOld.RecordCount = 1 Then
    Set l_rsNew = ExecuteSQL("SELECT * FROM jobdetail WHERE jobID = -1", g_strExecuteError)
    l_rsNew.AddNew
    For l_lngCount = 0 To l_rsNew.Fields.Count - 1
    
        Select Case l_rsNew.Fields(l_lngCount).Name
        
            Case "jobID"
                l_rsNew.Fields(l_lngCount).Value = l_lngNewJobID
                
            Case "jobdetailID", "DateMasterArrived", "DateCopyMade", "DateDelivered", "Completeable", "TargetDate", "FirstCompleteable", "rejecteddate", "rejecteduser", "offrejecteddate", "decisiontree", "decisiontreedate", "offdecisiontreedate", "firstreejecteddate", "daysondecisiontree", "MostRecentDecisionTreeDate", "DateTapeSentBack", "DaysOnRejected", "CopyMadeBy", "WyabillNumber", "MasterIsLateFlag", "complainedabout"
                'Nothing
            
            Case Else
                l_rsNew.Fields(l_lngCount).Value = l_rsOld.Fields(l_lngCount).Value
                
        End Select
        
    Next
    l_rsNew.Update
Else
    l_rsOld.Close
    Set l_rsOld = Nothing
    MsgBox "Problem locating the original job detail line to be duplicated.", vbCritical, "Error"
    Exit Sub
End If

l_rsNew.Close
l_rsOld.Close
Set l_rsNew = Nothing
Set l_rsOld = Nothing

ShowJob l_lngNewJobID, 1, True
'ShowJob l_lngNewJobID, 1, True

End Sub

Private Sub cmdCreateRedoTrackerTask_Click(Index As Integer)

Select Case Index

    Case 0
        CreateNewTrackerLine Val(lblOriginalDADCTrackerID.Caption)
    
    Case 1
        CreateNewSvenskTrackerLine Val(lblOriginalSvenskTrackerID.Caption)
    
    Case 2
        CreateNewiTunesTrackerLine Val(lblOriginaliTunesTrackerID.Caption)

End Select

End Sub

Private Sub cmdDelete_Click()

If Not CheckAccess("/deletecomplaints") Then
    Exit Sub
End If

If lblcomplaintID.Caption <> "" Then
    If MsgBox("About to Delete Complaint " & lblcomplaintID.Caption & vbCrLf & "Are you Sure?", vbYesNo, "Confirm Complaint Delete") = vbYes Then
        ExecuteSQL "DELETE FROM complaint WHERE complaintID = " & lblcomplaintID.Caption & ";", g_strExecuteError
        CheckForSQLError
        adoComplaints.Refresh
        If adoComplaints.Recordset.RecordCount <= 0 Then ClearFields frmComplaint
    End If
End If

End Sub

Private Sub cmdEmailOriginalOperator_Click(Index As Integer)

Dim l_strEmail As String, l_strEmailAddress As String

l_strEmail = "A Rejection issue has been raised for an item for which you were the original operator." & vbCrLf

Select Case Index

    Case 0 'Normal Jobs Tab
        l_strEmail = l_strEmail & "Job #: " & txtOriginalJobID(0).Text & " for " & cmbCompany(0).Text & vbCrLf
        l_strEmail = l_strEmail & "Issue Type: " & cmbIssueType(0).Text & vbCrLf
        l_strEmail = l_strEmail & "The issue raised was: " & txtRejectionIssue(0).Text & vbCrLf
        l_strEmail = l_strEmail & "The findings when investigated by " & cmbAssignedTo(0).Text & " were: " & IIf(txtTrackerFindings(0).Text <> "", txtTrackerFindings(0).Text, "<no findings>") & vbCrLf
        l_strEmail = l_strEmail & "Please contact " & cmbAssignedTo(0).Text & " for more details." & vbCrLf
        l_strEmailAddress = GetData("cetauser", "email", "fullname", cmbOriginalOperator(0).Text)
        If l_strEmailAddress <> "" Then
            SendSMTPMail l_strEmailAddress, cmbOriginalOperator(0).Text, "Issues Tracker Notification", "", l_strEmail, True, GetData("setting", "value", "name", "ManagerEmailAddress"), GetData("setting", "value", "name", "ManagerEmailName"), GetData("setting", "value", "name", "AdministratorEmailAddress")
            MsgBox "Mail Sent"
        Else
            MsgBox "Mail not sent - no Operator address could be determined"
        End If
    
    Case 1 'DADC Tracker Tab
        l_strEmail = l_strEmail & "Affinity Case #: " & txtAffinityCaseNumber(1).Text & " ref: " & lblReference(0).Caption & vbCrLf
        l_strEmail = l_strEmail & "Issue Type: " & cmbIssueType(1).Text & vbCrLf
        l_strEmail = l_strEmail & "The issue raised was: " & txtRejectionIssue(1).Text & vbCrLf
        l_strEmail = l_strEmail & "The findings when investigated by " & cmbAssignedTo(1).Text & " were: " & IIf(txtTrackerFindings(1).Text <> "", txtTrackerFindings(1).Text, "<no findings>") & vbCrLf
        l_strEmail = l_strEmail & "Please contact " & cmbAssignedTo(1).Text & " for more details." & vbCrLf
        l_strEmailAddress = GetData("cetauser", "email", "fullname", cmbOriginalOperator(1).Text)
        If l_strEmailAddress <> "" Then
            SendSMTPMail l_strEmailAddress, cmbOriginalOperator(1).Text, "Issues Tracker Notification", "", l_strEmail, True, GetData("setting", "value", "name", "ManagerEmailAddress"), GetData("setting", "value", "name", "ManagerEmailName"), GetData("setting", "value", "name", "AdministratorEmailAddress")
            MsgBox "Mail Sent"
        Else
            MsgBox "Mail not sent - no Operator address could be determined"
        End If
    
    Case 2 'Svensk Tracker Tab
        l_strEmail = l_strEmail & "Ref: " & lblReference(1).Caption & vbCrLf
        l_strEmail = l_strEmail & "Issue Type: " & cmbIssueType(2).Text & vbCrLf
        l_strEmail = l_strEmail & "The issue raised was: " & txtRejectionIssue(2).Text & vbCrLf
        l_strEmail = l_strEmail & "The findings when investigated by " & cmbAssignedTo(2).Text & " were: " & IIf(txtTrackerFindings(2).Text <> "", txtTrackerFindings(2).Text, "<no findings>") & vbCrLf
        l_strEmail = l_strEmail & "Please contact " & cmbAssignedTo(2).Text & " for more details." & vbCrLf
        l_strEmailAddress = GetData("cetauser", "email", "fullname", cmbOriginalOperator(2).Text)
        If l_strEmailAddress <> "" Then
            SendSMTPMail l_strEmailAddress, cmbOriginalOperator(2).Text, "Issues Tracker Notification", "", l_strEmail, True, GetData("setting", "value", "name", "ManagerEmailAddress"), GetData("setting", "value", "name", "ManagerEmailName"), GetData("setting", "value", "name", "AdministratorEmailAddress")
            MsgBox "Mail Sent"
        Else
            MsgBox "Mail not sent - no Operator address could be determined"
        End If
    
    Case 3 'DSP Tracker Tab
        l_strEmail = l_strEmail & "Ref: " & lblReference(2).Caption & vbCrLf
        l_strEmail = l_strEmail & "Issue Type: " & cmbIssueType(3).Text & vbCrLf
        l_strEmail = l_strEmail & "The issue raised was: " & txtRejectionIssue(3).Text & vbCrLf
        l_strEmail = l_strEmail & "The findings when investigated by " & cmbAssignedTo(3).Text & " were: " & IIf(txtTrackerFindings(3).Text <> "", txtTrackerFindings(3).Text, "<no findings>") & vbCrLf
        l_strEmail = l_strEmail & "Please contact " & cmbAssignedTo(3).Text & " for more details." & vbCrLf
        l_strEmailAddress = GetData("cetauser", "email", "fullname", cmbOriginalOperator(3).Text)
        If l_strEmailAddress <> "" Then
            SendSMTPMail l_strEmailAddress, cmbOriginalOperator(3).Text, "Issues Tracker Notification", "", l_strEmail, True, GetData("setting", "value", "name", "ManagerEmailAddress"), GetData("setting", "value", "name", "ManagerEmailName"), GetData("setting", "value", "name", "AdministratorEmailAddress")
            MsgBox "Mail Sent"
        Else
            MsgBox "Mail not sent - no Operator address could be determined"
        End If
    
    Case 4 'BBCWW Tracker Tab
        l_strEmail = l_strEmail & "Job #: " & txtOriginalJobID(1).Text & " for " & cmbCompany(1).Text & vbCrLf
        l_strEmail = l_strEmail & "Issue Type: " & cmbIssueType(4).Text & vbCrLf
        l_strEmail = l_strEmail & "The issue raised was: " & txtRejectionIssue(5).Text & vbCrLf
        l_strEmail = l_strEmail & "The findings when investigated by " & cmbAssignedTo(5).Text & " were: " & IIf(txtTrackerFindings(5).Text <> "", txtTrackerFindings(5).Text, "<no findings>") & vbCrLf
        l_strEmail = l_strEmail & "Please contact " & cmbAssignedTo(5).Text & " for more details." & vbCrLf
        l_strEmailAddress = GetData("cetauser", "email", "fullname", cmbOriginalOperator(4).Text)
        If l_strEmailAddress <> "" Then
            SendSMTPMail l_strEmailAddress, cmbOriginalOperator(4).Text, "Issues Tracker Notification", "", l_strEmail, True, GetData("setting", "value", "name", "ManagerEmailAddress"), GetData("setting", "value", "name", "ManagerEmailName"), GetData("setting", "value", "name", "AdministratorEmailAddress")
            MsgBox "Mail Sent"
        Else
            MsgBox "Mail not sent - no Operator address could be determined"
        End If
        
End Select

End Sub

Private Sub cmdHide_Click()
picDetails.Visible = False
End Sub

Private Sub cmdOutputiTunesReport_Click()

Dim l_strReportToPrint As String, l_strCriteria As String

'Select Case tabComplaintType.Tab
'    Case 0
        l_strReportToPrint = g_strLocationOfCrystalReportFiles & "complaint_job.rpt"
'    Case 1
'        l_strReportToPrint = g_strLocationOfCrystalReportFiles & "complaint_tracker.rpt"
'    Case 2
'        l_strReportToPrint = g_strLocationOfCrystalReportFiles & "complaint_svensk.rpt"
'    Case 3
'        l_strReportToPrint = g_strLocationOfCrystalReportFiles & "complaint_iTunes.rpt"
'    Case 4
'        l_strReportToPrint = g_strLocationOfCrystalReportFiles & "complaint_DADC_NonRR.rpt"
'    Case 5
'        l_strReportToPrint = g_strLocationOfCrystalReportFiles & "complaint_BBCWW.rpt"
'End Select

If m_strCrystalSearchFilter <> "" Then
    l_strCriteria = m_strCrystalSearchFilter
Else
    l_strCriteria = " 1=1 "
End If

If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strCriteria = l_strCriteria & " AND {complaint.companyID} = " & Val(lblSearchCompanyID.Caption)
End If

If cmbFilterInternalDepartment.Text <> "" Then
    l_strCriteria = l_strCriteria & " AND {complaint.IssueInternalDepartment} = '" & cmbFilterInternalDepartment.Text & "' "
End If

If cmbFilterOriginalOperator.Text <> "" Then
    l_strCriteria = l_strCriteria & " AND {complaint.originaloperator} = '" & cmbFilterOriginalOperator.Text & "' "
End If

If cmbFilterCetaUser.Text <> "" Then
    l_strCriteria = l_strCriteria & " AND {complaint.RRContactName} = '" & cmbFilterCetaUser.Text & "' "
End If

If Not IsNull(datStartDate.Value) Then
    If optDateFilter(0).Value = True Then
        l_strCriteria = l_strCriteria & " AND {complaint.cdate} >= #" & Format(datStartDate.Value, "YYYY-MM-DD") & "#"
    Else
        l_strCriteria = l_strCriteria & " AND {complaint.DateSignedOff} >= #" & Format(datStartDate.Value, "YYYY-MM-DD") & "#"
    End If
End If

If Not IsNull(datEndDate.Value) Then
    If optDateFilter(0).Value = True Then
        l_strCriteria = l_strCriteria & " AND {complaint.cdate} <= #" & Format(datEndDate.Value, "YYYY-MM-DD 23:59:59") & "#"
    Else
        l_strCriteria = l_strCriteria & " AND {complaint.DateSignedOff} <= #" & Format(datEndDate.Value, "YYYY-MM-DD 23:59:59") & "#"
    End If
End If

If cmbSearchInternalExternal.Text <> "" Then
    l_strCriteria = l_strCriteria & " AND {complaint.InternalExternal} = '" & cmbSearchInternalExternal.Text & "' "
End If

Debug.Print l_strCriteria
PrintCrystalReport l_strReportToPrint, l_strCriteria, True

End Sub

Private Sub cmdSave_Click()

If Not CheckAccess("/editcomplaints") Then
    Exit Sub
End If

Dim l_strSQL As String, l_varBookmark As Variant
Dim l_strInvestigator As String

If txtSourceID(0).Text <> "" Then
    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", txtSourceID(0).Text) <> 0 Then
        txtSourceID(0).Text = GetData("bbc_barcode_correction", "corrected_barcode", "original_barcode", txtSourceID(0).Text)
    End If
End If

If txtSourceID(1).Text <> "" Then
    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", txtSourceID(1).Text) <> 0 Then
        txtSourceID(1).Text = GetData("bbc_barcode_correction", "corrected_barcode", "original_barcode", txtSourceID(1).Text)
    End If
End If

If txtSourceID2(0).Text <> "" Then
    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", txtSourceID2(0).Text) <> 0 Then
        txtSourceID2(0).Text = GetData("bbc_barcode_correction", "corrected_barcode", "original_barcode", txtSourceID2(0).Text)
    End If
End If

If txtSourceID2(1).Text <> "" Then
    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", txtSourceID2(1).Text) <> 0 Then
        txtSourceID2(1).Text = GetData("bbc_barcode_correction", "corrected_barcode", "original_barcode", txtSourceID2(1).Text)
    End If
End If

If txtSourceID3(0).Text <> "" Then
    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", txtSourceID3(0).Text) <> 0 Then
        txtSourceID3(0).Text = GetData("bbc_barcode_correction", "corrected_barcode", "original_barcode", txtSourceID3(0).Text)
    End If
End If

If txtSourceID3(1).Text <> "" Then
    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", txtSourceID3(1).Text) <> 0 Then
        txtSourceID3(1).Text = GetData("bbc_barcode_correction", "corrected_barcode", "original_barcode", txtSourceID3(1).Text)
    End If
End If

If Val(lblcomplaintID.Caption) = 0 Then

    If tabComplaintType.Tab = 4 Then
        'Save a new complaint as a DADC Non-MX1
    '    l_varBookmark = adoComplaints.Recordset.Bookmark
        If lblContactID(4).Caption = "" Then
            MsgBox "Essential information has not yet been completed.", vbCritical, "Cannot Save"
            Exit Sub
        End If
        If optFindings(8).Value = 0 And cmbAssignedTo(4).Text = "" Then
            MsgBox "Cannot set the status unless someone has been asigned to invetstigate the issue."
            Exit Sub
        End If
        l_strSQL = "INSERT INTO complaint (cdate, cuser, mdate, muser, complainttype, companyID, contactID, AffinityCaseNumber, title, ContentVersionCode, DateFileArrived, DateMasterArrived, DateMasterArrived2, DateMasterArrived3, "
        l_strSQL = l_strSQL & "TargetDate, DateInvestigated, DateSignedOff, MasterSourceID, MasterSourceID2, MasterSourceID3, assignedto, findings, redorequired, signoffby, complaint, RRContactEmail, RRContactName, RRContactUserID) VALUES ("
        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "'" & g_strFullUserName & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "'" & g_strFullUserName & "', "
        l_strSQL = l_strSQL & "4, 1292, "
        l_strSQL = l_strSQL & Val(lblContactID(4).Caption) & ", "
        If txtAffinityCaseNumber(4).Text <> "" Then l_strSQL = l_strSQL & "'" & txtAffinityCaseNumber(4).Text & "', " Else l_strSQL = l_strSQL & "NULL, "
        If txtTitle(4).Text <> "" Then l_strSQL = l_strSQL & "'" & QuoteSanitise(txtTitle(4).Text) & "', " Else l_strSQL = l_strSQL & "NULL, "
        If txtContentVersionCode.Text <> "" Then l_strSQL = l_strSQL & "'" & txtContentVersionCode.Text & "', " Else l_strSQL = l_strSQL & "NULL, "
        If Not IsNull(datFileArrived.Value) Then
            l_strSQL = l_strSQL & "'" & FormatSQLDate(datFileArrived.Value) & "', "
        Else
            l_strSQL = l_strSQL & "NULL, "
        End If
        If Not IsNull(datMasterArrived(0).Value) Then
            l_strSQL = l_strSQL & "'" & FormatSQLDate(datMasterArrived(0).Value) & "', "
        Else
            l_strSQL = l_strSQL & "NULL, "
        End If
        If Not IsNull(datMasterArrived2(0).Value) Then
            l_strSQL = l_strSQL & "'" & FormatSQLDate(datMasterArrived2(0).Value) & "', "
        Else
            l_strSQL = l_strSQL & "NULL, "
        End If
        If Not IsNull(datMasterArrived3(0).Value) Then
            l_strSQL = l_strSQL & "'" & FormatSQLDate(datMasterArrived3(0).Value) & "', "
        Else
            l_strSQL = l_strSQL & "NULL, "
        End If
        If Not IsNull(datTargetDate(0).Value) Then
            l_strSQL = l_strSQL & "'" & FormatSQLDate(datTargetDate(0).Value) & "', "
        Else
            l_strSQL = l_strSQL & "NULL, "
        End If
        If Not IsNull(datInvestigated.Value) Then
            l_strSQL = l_strSQL & "'" & FormatSQLDate(datInvestigated.Value) & "', "
        Else
            l_strSQL = l_strSQL & "NULL, "
        End If
        If Not IsNull(datSignOff.Value) Then
            l_strSQL = l_strSQL & "'" & FormatSQLDate(datSignOff.Value) & "', "
        Else
            l_strSQL = l_strSQL & "NULL, "
        End If
        If txtSourceID(0).Text <> "" Then l_strSQL = l_strSQL & "'" & QuoteSanitise(txtSourceID(0).Text) & "', " Else l_strSQL = l_strSQL & "NULL, "
        If txtSourceID2(0).Text <> "" Then l_strSQL = l_strSQL & "'" & QuoteSanitise(txtSourceID2(0).Text) & "', " Else l_strSQL = l_strSQL & "NULL, "
        If txtSourceID3(0).Text <> "" Then l_strSQL = l_strSQL & "'" & QuoteSanitise(txtSourceID3(0).Text) & "', " Else l_strSQL = l_strSQL & "NULL, "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbAssignedTo(4).Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(txtTrackerFindings(4).Text) & "', "
        If optFindings(7).Value = True Then
            l_strSQL = l_strSQL & "7, NULL, "
        ElseIf optFindings(10).Value = True Then
            l_strSQL = l_strSQL & "10, NULL, "
        ElseIf optFindings(9).Value = True Then
            l_strSQL = l_strSQL & "9, NULL, "
        Else
            l_strSQL = l_strSQL & "8, NULL, "
        End If
        l_strSQL = l_strSQL & "'" & QuoteSanitise(txtRejectionIssue(4).Text) & "', "
        l_strSQL = l_strSQL & "'" & g_strUserEmailAddress & "', "
        l_strSQL = l_strSQL & "'" & g_strFullUserName & "', "
        l_strSQL = l_strSQL & g_lngUserID & ");"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        If cmbAssignedTo(4).Text <> "" Then
            SendSMTPMail GetDataSQL("SELECT TOP 1 email FROM cetauser WHERE fullname = '" & QuoteSanitise(cmbAssignedTo(4).Text) & "';"), cmbAssignedTo(4).Text, "CETA Issue Investigation", "", "You have been assigned to investigate Issue no: " & g_lngLastID, True, "", ""
        End If
        adoComplaints.Refresh
        adoComplaints.Recordset.Bookmark = adoComplaints.Recordset.Bookmark
    Else
        If lblCompanyID(0).Caption = "" Or lblContactID(0).Caption = "" Or txtOriginalJobID(0).Text = "" Or txtRejectionIssue(0).Text = "" Or cmbOriginalOperator(0).Text = "" Or cmbInternalExternal(0).Text = "" Then
            If lblCompanyID(0).Caption = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Select a Company", vbCritical, "Cannot Save"
            ElseIf lblContactID(0).Caption = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Select a Contact", vbCritical, "Cannot Save"
            ElseIf txtOriginalJobID(0).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Provide the JobID, or enter 0", vbCritical, "Cannot Save"
            ElseIf txtRejectionIssue(0).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "What is the Issue?", vbCritical, "Cannot Save"
            ElseIf cmbOriginalOperator(0).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Select the Original Operator", vbCritical, "Cannot Save"
            ElseIf cmbInternalExternal(0).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Is the issue External or Internal", vbCritical, "Cannot Save"
            End If
            Exit Sub
        End If
        If optFindings(0).Value = 0 And cmbAssignedTo(0).Text = "" Then
            MsgBox "Cannot set the status unless someone has been asigned to invetstigate the issue."
            Exit Sub
        End If
        'Save a new complaint
    '    l_varBookmark = adoComplaints.Recordset.Bookmark
        l_strSQL = "INSERT INTO complaint (cdate, cuser, mdate, muser, companyID, contactID, issuetype, originaljobID, originaljobdetailID, title, orderreference, jobcompleteddate, originaloperator, "
        l_strSQL = l_strSQL & "assignedto, findings, redorequired, signoffby, conclusion, complaint, InternalExternal, IssueInternalDepartment, RRContactEmail, RRContactName, RRContactUSerID) VALUES ("
        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "'" & g_strFullUserName & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "'" & g_strFullUserName & "', "
        l_strSQL = l_strSQL & Val(lblCompanyID(0).Caption) & ", "
        l_strSQL = l_strSQL & Val(lblContactID(0).Caption) & ", "
        l_strSQL = l_strSQL & "'" & cmbIssueType(0).Text & "', "
        If txtOriginalJobID(0).Text <> "" Then l_strSQL = l_strSQL & Val(txtOriginalJobID(0).Text) & ", " Else l_strSQL = l_strSQL & "NULL, "
        If txtOriginalJobdetailID(0).Text <> "" Then l_strSQL = l_strSQL & Val(txtOriginalJobdetailID(0).Text) & ", " Else l_strSQL = l_strSQL & "NULL, "
        If txtTitle(0).Text <> "" Then l_strSQL = l_strSQL & "'" & QuoteSanitise(txtTitle(0).Text) & "', " Else l_strSQL = l_strSQL & "NULL, "
        l_strSQL = l_strSQL & "'" & txtOrderRef(0).Text & "', "
        If Not IsNull(datJobCompletedDate(0).Value) Then
            l_strSQL = l_strSQL & "'" & FormatSQLDate(datJobCompletedDate(0).Value) & "', "
        Else
            l_strSQL = l_strSQL & "NULL, "
        End If
        l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbOriginalOperator(0).Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbAssignedTo(0).Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(txtTrackerFindings(0).Text) & "', "
        If optFindings(1).Value = True Then
            l_strSQL = l_strSQL & "1, NULL, "
        ElseIf optFindings(2).Value = True Then
            l_strSQL = l_strSQL & "2, NULL, "
        ElseIf optFindings(3).Value = True Then
            l_strSQL = l_strSQL & "3, NULL, "
        ElseIf optFindings(4).Value = True Then
            l_strSQL = l_strSQL & "4, NULL, "
        ElseIf optFindings(5).Value = True Then
            l_strSQL = l_strSQL & "5, NULL, "
        Else
            l_strSQL = l_strSQL & "0, NULL, "
        End If
        l_strSQL = l_strSQL & "'" & QuoteSanitise(txtConclusion(0).Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(txtRejectionIssue(0).Text) & "', "
        l_strSQL = l_strSQL & "'" & cmbInternalExternal(0).Text & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbInternalDepartment(0).Text) & "', "
        l_strSQL = l_strSQL & "'" & g_strUserEmailAddress & "', "
        l_strSQL = l_strSQL & "'" & g_strFullUserName & "', "
        l_strSQL = l_strSQL & g_lngUserID & ");"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        If cmbAssignedTo(0).Text <> "" Then
            SendSMTPMail GetDataSQL("SELECT TOP 1 email FROM cetauser WHERE fullname = '" & QuoteSanitise(cmbAssignedTo(0).Text) & "';"), cmbAssignedTo(0).Text, "CETA Issue Investigation", "", "You have been assigned to investigate Issue no: " & g_lngLastID, True, "", ""
        End If
        adoComplaints.Refresh
        adoComplaints.Recordset.Bookmark = adoComplaints.Recordset.Bookmark
    End If
Else

    'Update the current Complaint
    l_varBookmark = adoComplaints.Recordset.Bookmark
    l_strSQL = "UPDATE complaint SET "
    If grdComplaints.Columns("complainttype").Text = 1 Then
        'DADC BBC Tracker Issue
        If txtAffinityCaseNumber(1).Text = "" Or txtRejectionIssue(1).Text = "" Or cmbOriginalOperator(1).Text = "" Or cmbInternalExternal(1).Text = "" Then
            If txtAffinityCaseNumber(1).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Give the Affinity Case Number", vbCritical, "Cannot Save"
            ElseIf txtRejectionIssue(1).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "What is the Issue?", vbCritical, "Cannot Save"
            ElseIf cmbOriginalOperator(1).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Select the Original Operator", vbCritical, "Cannot Save"
            ElseIf cmbInternalExternal(1).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Is the issue External or Internal", vbCritical, "Cannot Save"
            End If
            Exit Sub
        End If
        If cmbAssignedTo(1).Text = "" And optFindings1(0).Value <> True Then
            MsgBox "Cannot set the status unless someone has been asigned to invetstigate the issue."
            Exit Sub
        End If
        If cmbCetaUser(grdComplaints.Columns("complainttype").Text).Text = "" Then
            l_strSQL = l_strSQL & "RRcontactUserID = " & g_lngUserID & ", "
            l_strSQL = l_strSQL & "RRcontactemail = '" & g_strUserEmailAddress & "', "
            l_strSQL = l_strSQL & "RRcontactname = '" & g_strFullUserName & "', "
        Else
            l_strSQL = l_strSQL & "RRcontactUserID = " & lblRRContactID(1).Caption & ", "
            l_strSQL = l_strSQL & "RRcontactemail = '" & lblRRContactEmail(1).Caption & "', "
            l_strSQL = l_strSQL & "RRcontactname = '" & cmbCetaUser(1).Text & "', "
        End If
        l_strSQL = l_strSQL & "mdate = '" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "muser = '" & g_strUserInitials & "', "
        l_strSQL = l_strSQL & "findings = '" & QuoteSanitise(txtTrackerFindings(1).Text) & "', "
        l_strSQL = l_strSQL & "assignedto = '" & cmbAssignedTo(1).Text & "', "
        l_strSQL = l_strSQL & "originaloperator = '" & QuoteSanitise(cmbOriginalOperator(1).Text) & "', "
        l_strSQL = l_strSQL & "affinityCaseNumber = " & IIf(txtAffinityCaseNumber(1).Text <> "", "'" & txtAffinityCaseNumber(1).Text & "', ", "NULL, ")
        l_strSQL = l_strSQL & "MasterSourceID = " & IIf(txtSourceID(1).Text <> "", "'" & QuoteSanitise(txtSourceID(1).Text) & "', ", "NULL, ")
        l_strSQL = l_strSQL & "MasterSourceID2 = " & IIf(txtSourceID2(1).Text <> "", "'" & QuoteSanitise(txtSourceID2(1).Text) & "', ", "NULL, ")
        l_strSQL = l_strSQL & "MasterSourceID3 = " & IIf(txtSourceID3(1).Text <> "", "'" & QuoteSanitise(txtSourceID3(1).Text) & "', ", "NULL, ")
        If Not IsNull(datMasterArrived(1).Value) Then l_strSQL = l_strSQL & "DateMasterArrived = '" & FormatSQLDate(datMasterArrived(1).Value) & "', " Else l_strSQL = l_strSQL & "DateMasterArrived = NULL, "
        If Not IsNull(datMasterArrived2(1).Value) Then l_strSQL = l_strSQL & "DateMasterArrived2 = '" & FormatSQLDate(datMasterArrived2(1).Value) & "', " Else l_strSQL = l_strSQL & "DateMasterArrived2 = NULL, "
        If Not IsNull(datMasterArrived3(1).Value) Then l_strSQL = l_strSQL & "DateMasterArrived3 = '" & FormatSQLDate(datMasterArrived3(1).Value) & "', " Else l_strSQL = l_strSQL & "DateMasterArrived3 = NULL, "
        If Not IsNull(datTargetDate(1).Value) Then l_strSQL = l_strSQL & "TargetDate = '" & FormatSQLDate(datTargetDate(1).Value) & "', " Else l_strSQL = l_strSQL & "TargetDate = NULL, "
        If optFindings1(0).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 0, "
        ElseIf optFindings1(1).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 1, "
        ElseIf optFindings1(2).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 2, "
        ElseIf optFindings1(3).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 3, "
        ElseIf optFindings1(4).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 4, "
        ElseIf optFindings1(5).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 5, "
        ElseIf optFindings1(6).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 6, "
        ElseIf optFindings1(7).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 7, "
        ElseIf optFindings1(8).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 8, "
        ElseIf optFindings1(9).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 9, "
        End If
        l_strSQL = l_strSQL & "complaint = '" & QuoteSanitise(txtRejectionIssue(1).Text) & "', "
        l_strSQL = l_strSQL & "InternalExternal = '" & cmbInternalExternal(1).Text & "', "
        l_strSQL = l_strSQL & "IssueInternalDepartment = '" & cmbInternalDepartment(1).Text & "', "
        l_strSQL = l_strSQL & "IssueType = '" & cmbIssueType(1).Text & "', "
        If cmbSignoff(1).Text <> cmbAssignedTo(1).Text And cmbSignoff(1).Text <> "" Then
            l_strSQL = l_strSQL & "signoffby = '" & QuoteSanitise(cmbSignoff(1).Text) & "' "
        Else
            l_strSQL = l_strSQL & "signoffby = Null "
        End If
        If grdComplaints.Columns("assignedto").Text <> cmbAssignedTo(4).Text Then
            l_strInvestigator = cmbAssignedTo(4).Text
        End If
    ElseIf grdComplaints.Columns("complainttype").Text = 2 Then
        'Svensk Tracker Issue
        If txtRejectionIssue(2).Text = "" Or cmbOriginalOperator(2).Text = "" Or cmbInternalExternal(2).Text = "" Then
            If txtRejectionIssue(2).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "What is the Issue?", vbCritical, "Cannot Save"
            ElseIf cmbOriginalOperator(2).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Select the Original Operator", vbCritical, "Cannot Save"
            ElseIf cmbInternalExternal(2).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Is the issue External or Internal", vbCritical, "Cannot Save"
            End If
            Exit Sub
        End If
        If cmbAssignedTo(2).Text = "" And optFindings2(0).Value <> True Then
            MsgBox "Cannot set the status unless someone has been asigned to invetstigate the issue."
            Exit Sub
        End If
        If cmbCetaUser(grdComplaints.Columns("complainttype").Text).Text = "" Then
            l_strSQL = l_strSQL & "RRcontactUserID = " & g_lngUserID & ", "
            l_strSQL = l_strSQL & "RRcontactemail = '" & g_strUserEmailAddress & "', "
            l_strSQL = l_strSQL & "RRcontactname = '" & g_strFullUserName & "', "
        Else
            l_strSQL = l_strSQL & "RRcontactUserID = " & lblRRContactID(2).Caption & ", "
            l_strSQL = l_strSQL & "RRcontactemail = '" & lblRRContactEmail(2).Caption & "', "
            l_strSQL = l_strSQL & "RRcontactname = '" & cmbCetaUser(2).Text & "', "
        End If
        l_strSQL = l_strSQL & "mdate = '" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "muser = '" & g_strUserInitials & "', "
        l_strSQL = l_strSQL & "findings = '" & QuoteSanitise(txtTrackerFindings(2).Text) & "', "
        l_strSQL = l_strSQL & "assignedto = '" & cmbAssignedTo(2).Text & "', "
        l_strSQL = l_strSQL & "originaloperator = '" & QuoteSanitise(cmbOriginalOperator(2).Text) & "', "
        If optFindings2(0).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 0, "
        ElseIf optFindings2(1).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 1, "
        ElseIf optFindings2(2).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 2, "
        ElseIf optFindings2(3).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 3, "
        ElseIf optFindings2(4).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 4, "
        ElseIf optFindings2(5).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 5, "
        ElseIf optFindings2(6).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 6, "
        ElseIf optFindings2(7).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 7, "
        ElseIf optFindings2(8).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 8, "
        ElseIf optFindings2(9).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 9, "
        End If
        l_strSQL = l_strSQL & "complaint = '" & QuoteSanitise(txtRejectionIssue(2).Text) & "', "
        l_strSQL = l_strSQL & "InternalExternal = '" & cmbInternalExternal(2).Text & "', "
        l_strSQL = l_strSQL & "IssueInternalDepartment = '" & cmbInternalDepartment(2).Text & "', "
        l_strSQL = l_strSQL & "IssueType = '" & cmbIssueType(2).Text & "', "
        If cmbSignoff(2).Text <> cmbAssignedTo(2).Text And cmbSignoff(2).Text <> "" Then
            l_strSQL = l_strSQL & "signoffby = '" & QuoteSanitise(cmbSignoff(2).Text) & "' "
        Else
            l_strSQL = l_strSQL & "signoffby = Null "
        End If
        If grdComplaints.Columns("assignedto").Text <> cmbAssignedTo(2).Text Then
            l_strInvestigator = cmbAssignedTo(2).Text
        End If
    ElseIf grdComplaints.Columns("complainttype").Text = 3 Then
        'iTunes Tracker Issue
        If txtRejectionIssue(3).Text = "" Or cmbOriginalOperator(3).Text = "" Or cmbInternalExternal(3).Text = "" Then
            If txtRejectionIssue(3).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "What is the Issue?", vbCritical, "Cannot Save"
            ElseIf cmbOriginalOperator(3).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Select the Original Operator", vbCritical, "Cannot Save"
            ElseIf cmbInternalExternal(3).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Is the issue External or Internal", vbCritical, "Cannot Save"
            End If
            Exit Sub
        End If
        If cmbAssignedTo(3).Text = "" And optFindings3(0).Value <> True Then
            MsgBox "Cannot set the status unless someone has been asigned to invetstigate the issue."
            Exit Sub
        End If
        If cmbCetaUser(grdComplaints.Columns("complainttype").Text).Text = "" Then
            l_strSQL = l_strSQL & "RRcontactUserID = " & g_lngUserID & ", "
            l_strSQL = l_strSQL & "RRcontactemail = '" & g_strUserEmailAddress & "', "
            l_strSQL = l_strSQL & "RRcontactname = '" & g_strFullUserName & "', "
        Else
            l_strSQL = l_strSQL & "RRcontactUserID = " & lblRRContactID(3).Caption & ", "
            l_strSQL = l_strSQL & "RRcontactemail = '" & lblRRContactEmail(3).Caption & "', "
            l_strSQL = l_strSQL & "RRcontactname = '" & cmbCetaUser(3).Text & "', "
        End If
        l_strSQL = l_strSQL & "mdate = '" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "muser = '" & g_strUserInitials & "', "
        l_strSQL = l_strSQL & "findings = '" & QuoteSanitise(txtTrackerFindings(3).Text) & "', "
        l_strSQL = l_strSQL & "assignedto = '" & cmbAssignedTo(3).Text & "', "
        l_strSQL = l_strSQL & "originaloperator = '" & QuoteSanitise(cmbOriginalOperator(3).Text) & "', "
        l_strSQL = l_strSQL & "IssueType = '" & cmbIssueType(3).Text & "', "
        If optFindings3(0).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 0, "
        ElseIf optFindings3(1).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 1, "
        ElseIf optFindings3(2).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 2, "
        ElseIf optFindings3(3).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 3, "
        ElseIf optFindings3(4).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 4, "
        ElseIf optFindings3(5).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 5, "
        ElseIf optFindings3(6).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 6, "
        ElseIf optFindings3(7).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 7, "
        ElseIf optFindings3(8).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 8, "
        ElseIf optFindings3(9).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 9, "
        End If
        l_strSQL = l_strSQL & "complaint = '" & QuoteSanitise(txtRejectionIssue(3).Text) & "', "
        l_strSQL = l_strSQL & "InternalExternal = '" & cmbInternalExternal(3).Text & "', "
        l_strSQL = l_strSQL & "IssueInternalDepartment = '" & cmbInternalDepartment(3).Text & "', "
        If cmbSignoff(3).Text <> cmbAssignedTo(3).Text And cmbSignoff(3).Text <> "" Then
            l_strSQL = l_strSQL & "signoffby = '" & QuoteSanitise(cmbSignoff(3).Text) & "' "
        Else
            l_strSQL = l_strSQL & "signoffby = Null "
        End If
        If grdComplaints.Columns("assignedto").Text <> cmbAssignedTo(3).Text Then
            l_strInvestigator = cmbAssignedTo(3).Text
        End If
    ElseIf grdComplaints.Columns("complainttype").Text = 4 Then
        'Non-RR DADC type complaints
        If txtAffinityCaseNumber(4).Text = "" Or txtRejectionIssue(4).Text = "" Then
            If txtAffinityCaseNumber(4).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Give the Affinity Case Number", vbCritical, "Cannot Save"
            ElseIf txtRejectionIssue(4).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "What is the Issue?", vbCritical, "Cannot Save"
            End If
            Exit Sub
        End If
        If cmbAssignedTo(4).Text = "" And optFindings(8).Value <> True Then
            MsgBox "Cannot set the status unless someone has been asigned to invetstigate the issue."
            Exit Sub
        End If
        If cmbCetaUser(grdComplaints.Columns("complainttype").Text).Text = "" Then
            l_strSQL = l_strSQL & "RRcontactUserID = " & g_lngUserID & ", "
            l_strSQL = l_strSQL & "RRcontactemail = '" & g_strUserEmailAddress & "', "
            l_strSQL = l_strSQL & "RRcontactname = '" & g_strFullUserName & "', "
        Else
            l_strSQL = l_strSQL & "RRcontactUserID = " & lblRRContactID(4).Caption & ", "
            l_strSQL = l_strSQL & "RRcontactemail = '" & lblRRContactEmail(4).Caption & "', "
            l_strSQL = l_strSQL & "RRcontactname = '" & cmbCetaUser(4).Text & "', "
        End If
        l_strSQL = l_strSQL & "mdate = '" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "muser = '" & g_strFullUserName & "', "
        l_strSQL = l_strSQL & "companyID = " & Val(lblCompanyID(4).Caption) & ", "
        l_strSQL = l_strSQL & "contactID = " & Val(lblContactID(4).Caption) & ", "
        l_strSQL = l_strSQL & "assignedto = '" & cmbAssignedTo(4).Text & "', "
        l_strSQL = l_strSQL & "title = " & IIf(txtTitle(4).Text <> "", "'" & QuoteSanitise(txtTitle(4).Text) & "', ", "NULL, ")
        l_strSQL = l_strSQL & "ContentVersionCode = " & IIf(txtContentVersionCode.Text <> "", "'" & txtContentVersionCode.Text & "', ", "NULL, ")
        l_strSQL = l_strSQL & "MasterSourceID = " & IIf(txtSourceID(0).Text <> "", "'" & QuoteSanitise(txtSourceID(0).Text) & "', ", "NULL, ")
        l_strSQL = l_strSQL & "MasterSourceID2 = " & IIf(txtSourceID2(0).Text <> "", "'" & QuoteSanitise(txtSourceID2(0).Text) & "', ", "NULL, ")
        l_strSQL = l_strSQL & "MasterSourceID3 = " & IIf(txtSourceID3(0).Text <> "", "'" & QuoteSanitise(txtSourceID3(0).Text) & "', ", "NULL, ")
        l_strSQL = l_strSQL & "affinityCaseNumber = " & IIf(txtAffinityCaseNumber(4).Text <> "", "'" & txtAffinityCaseNumber(4).Text & "', ", "NULL, ")
        l_strSQL = l_strSQL & "complaint = '" & QuoteSanitise(txtRejectionIssue(4).Text) & "', "
        l_strSQL = l_strSQL & "findings = '" & QuoteSanitise(txtTrackerFindings(4).Text) & "', "
        If Not IsNull(datFileArrived.Value) Then
            l_strSQL = l_strSQL & "DateFileArrived = '" & FormatSQLDate(datFileArrived.Value) & "', "
        Else
            l_strSQL = l_strSQL & "DateFileArrived = NULL, "
        End If
        If Not IsNull(datMasterArrived(0).Value) Then l_strSQL = l_strSQL & "DateMasterArrived = '" & FormatSQLDate(datMasterArrived(0).Value) & "', " Else l_strSQL = l_strSQL & "DateMasterArrived = NULL, "
        If Not IsNull(datMasterArrived2(0).Value) Then l_strSQL = l_strSQL & "DateMasterArrived2 = '" & FormatSQLDate(datMasterArrived2(0).Value) & "', " Else l_strSQL = l_strSQL & "DateMasterArrived2 = NULL, "
        If Not IsNull(datMasterArrived3(0).Value) Then l_strSQL = l_strSQL & "DateMasterArrived3 = '" & FormatSQLDate(datMasterArrived3(0).Value) & "', " Else l_strSQL = l_strSQL & "DateMasterArrived3 = NULL, "
        If Not IsNull(datTargetDate(0).Value) Then l_strSQL = l_strSQL & "TargetDate = '" & FormatSQLDate(datTargetDate(0).Value) & "', " Else l_strSQL = l_strSQL & "TargetDate = NULL, "
        If Not IsNull(datInvestigated.Value) Then
            l_strSQL = l_strSQL & "DateInvestigated = '" & FormatSQLDate(datInvestigated.Value) & "', "
        Else
            l_strSQL = l_strSQL & "DateInvestigated = NULL, "
        End If
        If optFindings(7).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 7, "
        ElseIf optFindings(9).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 9, "
        ElseIf optFindings(10).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 10, "
        Else
            l_strSQL = l_strSQL & "redorequired = 8, "
        End If
        If GetData("complaint", "billed", "complaintID", lblcomplaintID.Caption) = 0 Then
            If cmbSignoff(4).Text <> cmbAssignedTo(4).Text And cmbSignoff(4).Text <> "" Then
                l_strSQL = l_strSQL & "signoffby = '" & QuoteSanitise(cmbSignoff(4).Text) & "' "
                If Trim(" " & GetData("complaint", "signoffby", "complaintID", lblcomplaintID.Caption)) = "" Then
                    l_strSQL = l_strSQL & ", DateSignedOff = '" & FormatSQLDate(Now) & "' "
                    l_strSQL = l_strSQL & ", ReadyToBill = 1 "
                End If
            Else
                l_strSQL = l_strSQL & "signoffby = Null, "
                l_strSQL = l_strSQL & "DateSignedOff = Null, "
                l_strSQL = l_strSQL & "ReadyToBill = 0 "
            End If
        Else
            MsgBox "This item cannot have its signoff changed - it has already been billed", vbInformation, "Signoff Not Updated"
        End If
        If grdComplaints.Columns("assignedto").Text <> cmbAssignedTo(4).Text Then
            l_strInvestigator = cmbAssignedTo(4).Text
        End If
    ElseIf grdComplaints.Columns("complainttype").Text = 5 Then
        'BBCWW Tracker issues
        If lblCompanyID(1).Caption = "" Or lblContactID(1).Caption = "" Or txtOriginalJobID(1).Text = "" Or txtRejectionIssue(5).Text = "" Or cmbOriginalOperator(4).Text = "" Or cmbInternalExternal(4).Text = "" Then
            If lblCompanyID(1).Caption = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Select a Company", vbCritical, "Cannot Save"
            ElseIf lblContactID(1).Caption = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Select a Contact", vbCritical, "Cannot Save"
            ElseIf txtOriginalJobID(1).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Provide the JobID, or enter 0", vbCritical, "Cannot Save"
            ElseIf txtRejectionIssue(5).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "What is the Issue?", vbCritical, "Cannot Save"
            ElseIf cmbOriginalOperator(4).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Select the Original Operator", vbCritical, "Cannot Save"
            ElseIf cmbInternalExternal(4).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Is the issue External or Internal", vbCritical, "Cannot Save"
            End If
            Exit Sub
        End If
        If cmbAssignedTo(5).Text = "" And optFindings4(0).Value <> True Then
            MsgBox "Cannot set the status unless someone has been asigned to invetstigate the issue."
            Exit Sub
        End If
        If cmbCetaUser(grdComplaints.Columns("complainttype").Text).Text = "" Then
            l_strSQL = l_strSQL & "RRcontactUserID = " & g_lngUserID & ", "
            l_strSQL = l_strSQL & "RRcontactemail = '" & g_strUserEmailAddress & "', "
            l_strSQL = l_strSQL & "RRcontactname = '" & g_strFullUserName & "', "
        Else
            l_strSQL = l_strSQL & "RRcontactUserID = " & lblRRContactID(5).Caption & ", "
            l_strSQL = l_strSQL & "RRcontactemail = '" & lblRRContactEmail(5).Caption & "', "
            l_strSQL = l_strSQL & "RRcontactname = '" & cmbCetaUser(5).Text & "', "
        End If
        l_strSQL = l_strSQL & "mdate = '" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "muser = '" & g_strFullUserName & "', "
        l_strSQL = l_strSQL & "companyID = " & Val(lblCompanyID(1).Caption) & ", "
        l_strSQL = l_strSQL & "contactID = " & Val(lblContactID(1).Caption) & ", "
        l_strSQL = l_strSQL & "assignedto = '" & cmbAssignedTo(5).Text & "', "
        l_strSQL = l_strSQL & "originaljobID = " & IIf(txtOriginalJobID(1).Text <> "", Val(txtOriginalJobID(1).Text) & ", ", "NULL, ")
        l_strSQL = l_strSQL & "originaljobdetailID = " & IIf(txtOriginalJobdetailID(1).Text <> "", Val(txtOriginalJobdetailID(1).Text) & ", ", "NULL, ")
        l_strSQL = l_strSQL & "IssueType = '" & cmbIssueType(4).Text & "', "
        l_strSQL = l_strSQL & "title = " & IIf(txtTitle(1).Text <> "", "'" & QuoteSanitise(txtTitle(1).Text) & "', ", "NULL, ")
        l_strSQL = l_strSQL & "orderreference = '" & txtOrderRef(1).Text & "', "
        l_strSQL = l_strSQL & "originaloperator = '" & QuoteSanitise(cmbOriginalOperator(4).Text) & "', "
        l_strSQL = l_strSQL & "jobcompleteddate = " & IIf(Not IsNull(datJobCompletedDate(1).Value), "'" & FormatSQLDate(datJobCompletedDate(1).Value) & "', ", "NULL, ")
        l_strSQL = l_strSQL & "complaint = '" & QuoteSanitise(txtRejectionIssue(5).Text) & "', "
        l_strSQL = l_strSQL & "findings = '" & QuoteSanitise(txtTrackerFindings(5).Text) & "', "
        l_strSQL = l_strSQL & "RejectionCategory = '" & cmbRejectionCategory.Text & "', "
        l_strSQL = l_strSQL & "RejectionType = '" & cmbRejectionType.Text & "', "
        l_strSQL = l_strSQL & "RejectionAccountability = '" & cmbRejectionAccountability.Text & "', "
        l_strSQL = l_strSQL & "RejectionReason = '" & cmbRejectionReason.Text & "', "
        l_strSQL = l_strSQL & "InternalExternal = '" & cmbInternalExternal(4).Text & "', "
        l_strSQL = l_strSQL & "IssueInternalDepartment = '" & cmbInternalDepartment(4).Text & "', "
        If optFindings4(0).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 0, "
        ElseIf optFindings4(1).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 1, "
        ElseIf optFindings4(2).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 2, "
        ElseIf optFindings4(3).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 3, "
        ElseIf optFindings4(4).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 4, "
        ElseIf optFindings4(5).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 5, "
        End If
        l_strSQL = l_strSQL & "conclusion = '" & QuoteSanitise(txtConclusion(1).Text) & "', "
        If cmbSignoff(5).Text <> cmbAssignedTo(5).Text And cmbSignoff(5).Text <> "" Then
            l_strSQL = l_strSQL & "signoffby = '" & QuoteSanitise(cmbSignoff(5).Text) & "' "
        Else
            l_strSQL = l_strSQL & "signoffby = Null "
        End If
        If grdComplaints.Columns("assignedto").Text <> cmbAssignedTo(5).Text Then
            l_strInvestigator = cmbAssignedTo(5).Text
        End If
    Else
        'Job type complaints
        If lblCompanyID(0).Caption = "" Or lblContactID(0).Caption = "" Or txtOriginalJobID(0).Text = "" Or txtRejectionIssue(0).Text = "" Or cmbOriginalOperator(0).Text = "" Or cmbInternalExternal(0).Text = "" Then
            If lblCompanyID(0).Caption = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Select a Company", vbCritical, "Cannot Save"
            ElseIf lblContactID(0).Caption = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Select a Contact", vbCritical, "Cannot Save"
            ElseIf txtOriginalJobID(0).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Provide the JobID, or enter 0", vbCritical, "Cannot Save"
            ElseIf txtRejectionIssue(0).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "What is the Issue?", vbCritical, "Cannot Save"
            ElseIf cmbOriginalOperator(0).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Select the Original Operator", vbCritical, "Cannot Save"
            ElseIf cmbInternalExternal(0).Text = "" Then
                MsgBox "Essential information has not yet been completed." & vbCrLf & "Is the issue External or Internal", vbCritical, "Cannot Save"
            End If
            Exit Sub
        End If
        If cmbAssignedTo(0).Text = "" And optFindings(0).Value <> True Then
            MsgBox "Cannot set the status unless someone has been asigned to invetstigate the issue."
            Exit Sub
        End If
        If cmbCetaUser(grdComplaints.Columns("complainttype").Text).Text = "" Then
            l_strSQL = l_strSQL & "RRcontactUserID = " & g_lngUserID & ", "
            l_strSQL = l_strSQL & "RRcontactemail = '" & g_strUserEmailAddress & "', "
            l_strSQL = l_strSQL & "RRcontactname = '" & g_strFullUserName & "', "
        Else
            l_strSQL = l_strSQL & "RRcontactUserID = " & lblRRContactID(0).Caption & ", "
            l_strSQL = l_strSQL & "RRcontactemail = '" & lblRRContactEmail(0).Caption & "', "
            l_strSQL = l_strSQL & "RRcontactname = '" & cmbCetaUser(0).Text & "', "
        End If
        l_strSQL = l_strSQL & "mdate = '" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "muser = '" & g_strFullUserName & "', "
        l_strSQL = l_strSQL & "companyID = " & Val(lblCompanyID(0).Caption) & ", "
        l_strSQL = l_strSQL & "contactID = " & Val(lblContactID(0).Caption) & ", "
        l_strSQL = l_strSQL & "assignedto = '" & cmbAssignedTo(0).Text & "', "
        l_strSQL = l_strSQL & "originaljobID = " & IIf(txtOriginalJobID(0).Text <> "", Val(txtOriginalJobID(0).Text) & ", ", "NULL, ")
        l_strSQL = l_strSQL & "originaljobdetailID = " & IIf(txtOriginalJobdetailID(0).Text <> "", Val(txtOriginalJobdetailID(0).Text) & ", ", "NULL, ")
        l_strSQL = l_strSQL & "IssueType = '" & cmbIssueType(0).Text & "', "
        l_strSQL = l_strSQL & "title = " & IIf(txtTitle(0).Text <> "", "'" & QuoteSanitise(txtTitle(0).Text) & "', ", "NULL, ")
        l_strSQL = l_strSQL & "orderreference = '" & txtOrderRef(0).Text & "', "
        l_strSQL = l_strSQL & "originaloperator = '" & QuoteSanitise(cmbOriginalOperator(0).Text) & "', "
        l_strSQL = l_strSQL & "jobcompleteddate = " & IIf(Not IsNull(datJobCompletedDate(0).Value), "'" & FormatSQLDate(datJobCompletedDate(0).Value) & "', ", "NULL, ")
        l_strSQL = l_strSQL & "complaint = '" & QuoteSanitise(txtRejectionIssue(0).Text) & "', "
        l_strSQL = l_strSQL & "findings = '" & QuoteSanitise(txtTrackerFindings(0).Text) & "', "
        l_strSQL = l_strSQL & "InternalExternal = '" & cmbInternalExternal(0).Text & "', "
        l_strSQL = l_strSQL & "IssueInternalDepartment = '" & cmbInternalDepartment(0).Text & "', "
        If optFindings(0).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 0, "
        ElseIf optFindings(1).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 1, "
        ElseIf optFindings(2).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 2, "
        ElseIf optFindings(3).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 3, "
        ElseIf optFindings(4).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 4, "
        ElseIf optFindings(5).Value = True Then
            l_strSQL = l_strSQL & "redorequired = 5, "
        End If
        l_strSQL = l_strSQL & "conclusion = '" & QuoteSanitise(txtConclusion(0).Text) & "', "
        If cmbSignoff(0).Text <> cmbAssignedTo(0).Text And cmbSignoff(0).Text <> "" Then
            l_strSQL = l_strSQL & "signoffby = '" & QuoteSanitise(cmbSignoff(0).Text) & "' "
        Else
            l_strSQL = l_strSQL & "signoffby = Null "
        End If
        If grdComplaints.Columns("assignedto").Text <> cmbAssignedTo(0).Text Then
            l_strInvestigator = cmbAssignedTo(0).Text
        End If
    End If
    
    l_strSQL = l_strSQL & "WHERE complaintID = " & Val(lblcomplaintID.Caption) & ";"
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    If l_strInvestigator <> "" Then
        SendSMTPMail GetDataSQL("SELECT TOP 1 email FROM cetauser WHERE fullname = '" & QuoteSanitise(l_strInvestigator) & "';"), l_strInvestigator, "CETA Issue Investigation", "", "You have been assigned to investigate Issue no: " & lblcomplaintID.Caption, True, "", ""
    End If
    adoComplaints.Refresh
    On Error Resume Next
    adoComplaints.Recordset.Bookmark = l_varBookmark
    
End If

End Sub

Private Sub cmdSendEmail_Click()

Dim l_strEmail As String

If lblRRContactEmail(tabComplaintType.Tab).Caption <> "" Then
    l_strEmail = "Issue #: " & lblcomplaintID.Caption
    Select Case tabComplaintType.Tab
        Case 0
            l_strEmail = l_strEmail & ", Original Job #: " & txtOriginalJobID(0).Text & ", Title: " & txtTitle(0).Text & vbCrLf
            l_strEmail = l_strEmail & "Issue Type: " & cmbIssueType(0).Text & vbCrLf
            l_strEmail = l_strEmail & "Fault: " & txtRejectionIssue(0).Text & vbCrLf
            l_strEmail = l_strEmail & "Investigated by: " & cmbAssignedTo(0).Text & vbCrLf
            l_strEmail = l_strEmail & "Findings: " & txtTrackerFindings(0).Text & vbCrLf
            l_strEmail = l_strEmail & "Status: "
            If optFindings(0).Value = True Then
                l_strEmail = l_strEmail & "Not Checked Yet" & vbCrLf
            ElseIf optFindings(1).Value = True Then
                l_strEmail = l_strEmail & "No Fault Found" & vbCrLf
            ElseIf optFindings(2).Value = True Then
                l_strEmail = l_strEmail & "Fault on Original Master" & vbCrLf
            ElseIf optFindings(3).Value = True Then
                l_strEmail = l_strEmail & "MX1 issue - MX1 to Redo" & vbCrLf
            ElseIf optFindings(4).Value = True Then
                l_strEmail = l_strEmail & "Fault not on original - MX1 to Redo" & vbCrLf
            End If
            l_strEmail = l_strEmail & "Conclusion: " & txtConclusion(0).Text & vbCrLf
        Case 1
            l_strEmail = l_strEmail & ", Affinity Case #: " & txtAffinityCaseNumber(1).Text & ", Title: " & lblOriginalTitle(0).Caption & vbCrLf
            l_strEmail = l_strEmail & "Issue Type: " & cmbIssueType(1).Text & vbCrLf
            l_strEmail = l_strEmail & "Fault: " & txtRejectionIssue(1).Text & vbCrLf
            l_strEmail = l_strEmail & "Investigated by: " & cmbAssignedTo(1).Text & vbCrLf
            l_strEmail = l_strEmail & "Findings: " & txtTrackerFindings(1).Text & vbCrLf
            l_strEmail = l_strEmail & "Status: "
            If optFindings1(0).Value = True Then
                l_strEmail = l_strEmail & "Not Checked Yet" & vbCrLf
            ElseIf optFindings1(1).Value = True Then
                l_strEmail = l_strEmail & "No Fault Found" & vbCrLf
            ElseIf optFindings1(2).Value = True Then
                l_strEmail = l_strEmail & "File issue at DADC" & vbCrLf
            ElseIf optFindings1(3).Value = True Then
                l_strEmail = l_strEmail & "MX1 issue - MX1 to Redo" & vbCrLf
            ElseIf optFindings1(4).Value = True Then
                l_strEmail = l_strEmail & "Fault on original Master" & vbCrLf
            ElseIf optFindings1(5).Value = True Then
                l_strEmail = l_strEmail & "Fault not on original - RR to Redo" & vbCrLf
            ElseIf optFindings1(6).Value = True Then
                l_strEmail = l_strEmail & "Checking in Progress" & vbCrLf
            ElseIf optFindings1(7).Value = True Then
                l_strEmail = l_strEmail & "Original File Rejection on Drive" & vbCrLf
            ElseIf optFindings1(8).Value = True Then
                l_strEmail = l_strEmail & "Original File Rejection on Warp" & vbCrLf
            End If
        Case 2
            l_strEmail = l_strEmail & ", Reference: " & lblReference(1).Caption & ", Title: " & lblOriginalTitle(1).Caption & vbCrLf
            l_strEmail = l_strEmail & "Issue Type: " & cmbIssueType(2).Text & vbCrLf
            l_strEmail = l_strEmail & "Fault: " & txtRejectionIssue(2).Text & vbCrLf
            l_strEmail = l_strEmail & "Investigated by: " & cmbAssignedTo(2).Text & vbCrLf
            l_strEmail = l_strEmail & "Findings: " & txtTrackerFindings(2).Text & vbCrLf
            l_strEmail = l_strEmail & "Status: "
            If optFindings2(0).Value = True Then
                l_strEmail = l_strEmail & "Not Checked Yet" & vbCrLf
            ElseIf optFindings2(1).Value = True Then
                l_strEmail = l_strEmail & "No Fault Found" & vbCrLf
            ElseIf optFindings2(2).Value = True Then
                l_strEmail = l_strEmail & "File issue at DADC" & vbCrLf
            ElseIf optFindings2(3).Value = True Then
                l_strEmail = l_strEmail & "MX1 issue - MX1 to Redo" & vbCrLf
            ElseIf optFindings2(4).Value = True Then
                l_strEmail = l_strEmail & "Fault on original Master" & vbCrLf
            ElseIf optFindings2(5).Value = True Then
                l_strEmail = l_strEmail & "Fault not on original - RR to Redo" & vbCrLf
            ElseIf optFindings2(6).Value = True Then
                l_strEmail = l_strEmail & "Checking in Progress" & vbCrLf
            ElseIf optFindings2(7).Value = True Then
                l_strEmail = l_strEmail & "Original File Rejection on Drive" & vbCrLf
            ElseIf optFindings2(8).Value = True Then
                l_strEmail = l_strEmail & "Original File Rejection on Warp" & vbCrLf
            End If
        Case 3
            l_strEmail = l_strEmail & ", Reference: " & lblReference(2).Caption & ", Title: " & lblOriginalTitle(2).Caption & vbCrLf
            l_strEmail = l_strEmail & "Issue Type: " & cmbIssueType(3).Text & vbCrLf
            l_strEmail = l_strEmail & "Fault: " & txtRejectionIssue(3).Text & vbCrLf
            l_strEmail = l_strEmail & "Investigated by: " & cmbAssignedTo(3).Text & vbCrLf
            l_strEmail = l_strEmail & "Findings: " & txtTrackerFindings(3).Text & vbCrLf
            l_strEmail = l_strEmail & "Status: "
            If optFindings3(0).Value = True Then
                l_strEmail = l_strEmail & "Not Checked Yet" & vbCrLf
            ElseIf optFindings3(1).Value = True Then
                l_strEmail = l_strEmail & "No Fault Found" & vbCrLf
            ElseIf optFindings3(2).Value = True Then
                l_strEmail = l_strEmail & "File issue at DADC" & vbCrLf
            ElseIf optFindings3(3).Value = True Then
                l_strEmail = l_strEmail & "MX1 issue - MX1 to Redo" & vbCrLf
            ElseIf optFindings3(4).Value = True Then
                l_strEmail = l_strEmail & "Fault on original Master" & vbCrLf
            ElseIf optFindings3(5).Value = True Then
                l_strEmail = l_strEmail & "Fault not on original - MX1 to Redo" & vbCrLf
            ElseIf optFindings3(6).Value = True Then
                l_strEmail = l_strEmail & "Checking in Progress" & vbCrLf
            ElseIf optFindings3(7).Value = True Then
                l_strEmail = l_strEmail & "Original File Rejection on Drive" & vbCrLf
            ElseIf optFindings3(8).Value = True Then
                l_strEmail = l_strEmail & "Original File Rejection on Warp" & vbCrLf
            End If
        Case 4
            l_strEmail = l_strEmail & ", Affinity Case #: " & txtAffinityCaseNumber(4).Text & ", Content Version Code: " & txtContentVersionCode.Text & vbCrLf
            l_strEmail = l_strEmail & "Title: " & txtTitle(4).Text & vbCrLf
            l_strEmail = l_strEmail & "Fault: " & txtRejectionIssue(4).Text & vbCrLf
            l_strEmail = l_strEmail & "Investigated by: " & cmbAssignedTo(4).Text & vbCrLf
            l_strEmail = l_strEmail & "Findings: " & txtTrackerFindings(4).Text & vbCrLf
            l_strEmail = l_strEmail & "Status: "
            If optFindings(8).Value = True Then
                l_strEmail = l_strEmail & "Not Checked Yet" & vbCrLf
            ElseIf optFindings(7).Value = True Then
                l_strEmail = l_strEmail & "No Fault Found" & vbCrLf
            ElseIf optFindings(9).Value = True Then
                l_strEmail = l_strEmail & "Fault found on Original Master - Fixing in Progress" & vbCrLf
            ElseIf optFindings(10).Value = True Then
                l_strEmail = l_strEmail & "Fault not found on Original Master - IR to be requested for Redo" & vbCrLf
            End If
        Case 5
            l_strEmail = l_strEmail & ", Original Job #: " & txtOriginalJobID(1).Text & ", Title: " & txtTitle(1).Text & vbCrLf
            l_strEmail = l_strEmail & "Issue Type: " & cmbIssueType(4).Text & vbCrLf
            l_strEmail = l_strEmail & "Fault: " & txtRejectionIssue(5).Text & vbCrLf
            l_strEmail = l_strEmail & "Investigated by: " & cmbAssignedTo(5).Text & vbCrLf
            l_strEmail = l_strEmail & "Findings: " & txtTrackerFindings(5).Text & vbCrLf
            l_strEmail = l_strEmail & "Status: "
            If optFindings4(0).Value = True Then
                l_strEmail = l_strEmail & "Not Checked Yet" & vbCrLf
            ElseIf optFindings4(1).Value = True Then
                l_strEmail = l_strEmail & "No Fault Found" & vbCrLf
            ElseIf optFindings4(2).Value = True Then
                l_strEmail = l_strEmail & "Fault on Original Master" & vbCrLf
            ElseIf optFindings4(3).Value = True Then
                l_strEmail = l_strEmail & "MX1 issue - MX1 to Redo" & vbCrLf
            ElseIf optFindings4(4).Value = True Then
                l_strEmail = l_strEmail & "Fault not on original - MX1 to Redo" & vbCrLf
            End If
            l_strEmail = l_strEmail & "Rejection Category: " & cmbRejectionCategory.Text & vbCrLf
            l_strEmail = l_strEmail & "Rejection Type: " & cmbRejectionType.Text & vbCrLf
            l_strEmail = l_strEmail & "Rejection Accountability: " & cmbRejectionAccountability.Text & vbCrLf
            l_strEmail = l_strEmail & "Rejection Reason: " & cmbRejectionReason.Text & vbCrLf
            l_strEmail = l_strEmail & "Conclusion: " & txtConclusion(0).Text & vbCrLf
    End Select
    SendSMTPMail lblRRContactEmail(tabComplaintType.Tab).Caption, "", "Issue Tracker Updated", "", l_strEmail, False, g_strUserEmailAddress, g_strFullUserName, g_strAdministratorEmailAddress
Else
    MsgBox "Cannot send email when no RR Contact person has been chosen.", vbCritical, "Error"
End If

End Sub

Private Sub grdComplaints_DblClick()
picDetails.Visible = True
End Sub

Private Sub grdComplaints_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

If grdComplaints.Columns("complaintID").Text <> "" Then
    lblcomplaintID.Caption = grdComplaints.Columns("complaintID").Text
    If m_blnSilent = False Then
        If grdComplaints.Columns("complainttype").Text = 1 Then
            'DADC BBC Manual Ingest Tracker Issue
            tabComplaintType.Tab = 1
            lblOriginalDADCTrackerID.Caption = Trim(" " & GetData("complaint", "originaldadctrackerID", "complaintID", lblcomplaintID.Caption, True))
            lblOriginalAlphaCode.Caption = Trim(" " & GetData("tracker_dadc_item", "externalalphaID", "tracker_dadc_itemID", lblOriginalDADCTrackerID.Caption, True))
            lblContentVersionCode.Caption = Trim(" " & GetData("tracker_dadc_item", "contentversioncode", "tracker_dadc_itemID", lblOriginalDADCTrackerID.Caption, True))
            lblReference(0).Caption = Trim(" " & GetData("tracker_dadc_item", "itemreference", "tracker_dadc_itemID", lblOriginalDADCTrackerID.Caption, True))
            lblOriginalTitle(0).Caption = Trim(" " & GetData("tracker_dadc_item", "title", "tracker_dadc_itemID", lblOriginalDADCTrackerID.Caption, True))
            lblOriginalSubtitle(0).Caption = Trim(" " & GetData("tracker_dadc_item", "subtitle", "tracker_dadc_itemID", lblOriginalDADCTrackerID.Caption, True))
            lblOriginalEpisode(0).Caption = Trim(" " & GetData("tracker_dadc_item", "episode", "tracker_dadc_itemID", lblOriginalDADCTrackerID.Caption, True))
            lblOriginalIngestDate(0).Caption = Trim(" " & GetData("tracker_dadc_item", "stagefield2", "tracker_dadc_itemID", lblOriginalDADCTrackerID.Caption, True))
            lblOriginalXmlDeliveryDate(0).Caption = Trim(" " & GetData("tracker_dadc_item", "stagefield4", "tracker_dadc_itemID", lblOriginalDADCTrackerID.Caption, True))
            cmbOriginalOperator(1).Text = Trim(" " & GetData("complaint", "originaloperator", "complaintID", lblcomplaintID.Caption, True))
            lblMasterFormat.Caption = Trim(" " & GetData("tracker_dadc_item", "format", "tracker_dadc_itemID", lblOriginalDADCTrackerID.Caption, True))
            lblBarcode.Caption = Trim(" " & GetData("tracker_dadc_item", "newbarcode", "tracker_dadc_itemID", lblOriginalDADCTrackerID.Caption, True))
            lblFileStore(0).Caption = GetData("library", "barcode", "libraryID", GetDataSQL("SELECT TOP 1 events.libraryID FROM events INNER JOIN library ON events.libraryID = library.libraryID WHERE events.clipreference = '" & Trim(" " & GetData("tracker_dadc_item", "itemreference", "tracker_dadc_itemID", lblOriginalDADCTrackerID.Caption, True)) & "' AND events.clipformat = 'Quicktime' AND events.clipcodec = 'ProRes HQ' AND (library.format = 'DISCSTORE' OR library.format = 'LTO5' OR library.format = 'MOBILEDISC');"))
            lblDecisionTreeDate(0).Caption = Trim(" " & GetData("tracker_dadc_item", "decisiontreedate", "tracker_dadc_itemID", lblOriginalDADCTrackerID.Caption, True))
            lblOffPendingDate.Caption = Trim(" " & GetData("tracker_dadc_item", "offpendingdate", "tracker_dadc_itemID", lblOriginalDADCTrackerID.Caption, True))
            lblFixJobID(0).Caption = Trim(" " & GetData("tracker_dadc_item", "fixedjobID", "tracker_dadc_itemID", lblOriginalDADCTrackerID.Caption, True))
            txtRejectionIssue(1).Text = grdComplaints.Columns("Complaint").Text
            txtAffinityCaseNumber(1).Text = Trim(" " & GetData("complaint", "AffinityCaseNumber", "complaintID", lblcomplaintID.Caption, True))
            txtTrackerFindings(1).Text = Trim(" " & GetData("complaint", "findings", "complaintID", lblcomplaintID.Caption, True))
            cmbInternalExternal(1).Text = Trim(" " & GetData("complaint", "InternalExternal", "complaintID", lblcomplaintID.Caption, True))
            cmbInternalDepartment(1).Text = Trim(" " & GetData("complaint", "IssueInternalDepartment", "complaintID", lblcomplaintID.Caption, True))
            txtSourceID(1).Text = Trim(" " & GetData("complaint", "MasterSourceID", "complaintID", lblcomplaintID.Caption, True))
            txtSourceID2(1).Text = Trim(" " & GetData("complaint", "MasterSourceID2", "complaintID", lblcomplaintID.Caption, True))
            txtSourceID3(1).Text = Trim(" " & GetData("complaint", "MasterSourceID3", "complaintID", lblcomplaintID.Caption, True))
            datMasterArrived(1).Value = Trim(" " & GetData("complaint", "DateMasterArrived", "complaintID", lblcomplaintID.Caption, True))
            datMasterArrived2(1).Value = Trim(" " & GetData("complaint", "DateMasterArrived2", "complaintID", lblcomplaintID.Caption, True))
            datMasterArrived3(1).Value = Trim(" " & GetData("complaint", "DateMasterArrived3", "complaintID", lblcomplaintID.Caption, True))
            datTargetDate(1).Value = Trim(" " & GetData("complaint", "TargetDate", "complaintID", lblcomplaintID.Caption, True))
            cmbAssignedTo(1).Text = Trim(" " & GetData("complaint", "assignedto", "complaintID", lblcomplaintID.Caption, True))
            If Not CheckAccess("/signoffcomplaints", True) Then cmbSignoff(1).Enabled = False Else cmbSignoff(1).Enabled = True
            cmbSignoff(1).Text = Trim(" " & GetData("complaint", "signoffby", "complaintID", lblcomplaintID.Caption, True))
            optFindings1(Val(Trim(" " & GetData("complaint", "redorequired", "complaintID", lblcomplaintID.Caption)))).Value = True
            cmbSignoff(1).Text = Trim(" " & GetData("complaint", "signoffby", "complaintID", lblcomplaintID.Caption, True))
            cmbCetaUser(1).Text = Trim(" " & GetData("complaint", "RRContactName", "complaintID", lblcomplaintID.Caption, True))
            lblRRContactID(1).Caption = Trim(" " & GetData("complaint", "RRContactUserID", "complaintID", lblcomplaintID.Caption, True))
            lblRRContactEmail(1).Caption = Trim(" " & GetData("complaint", "RRContactEmail", "complaintID", lblcomplaintID.Caption, True))
            cmbIssueType(1).Text = Trim(" " & GetData("complaint", "issuetype", "complaintID", lblcomplaintID.Caption, True))
        ElseIf grdComplaints.Columns("complainttype").Text = 2 Then
            'Svensk Tracker Issue
            tabComplaintType.Tab = 2
            lblOriginalSvenskTrackerID.Caption = Trim(" " & GetData("complaint", "originalsvensktrackerID", "complaintID", lblcomplaintID.Caption, True))
            lblReference(1).Caption = Trim(" " & GetData("tracker_svensk_item", "itemreference", "tracker_svensk_itemID", lblOriginalSvenskTrackerID.Caption, True))
            lblOriginalTitle(1).Caption = Trim(" " & GetData("tracker_svensk_item", "title", "tracker_svensk_itemID", lblOriginalSvenskTrackerID.Caption, True))
            lblOriginalSubtitle(1).Caption = Trim(" " & GetData("tracker_svensk_item", "subtitle", "tracker_svensk_itemID", lblOriginalSvenskTrackerID.Caption, True))
            lblOriginalSeries(1).Caption = Trim(" " & GetData("tracker_svensk_item", "series", "tracker_svensk_itemID", lblOriginalSvenskTrackerID.Caption, True))
            lblOriginalEpisode(1).Caption = Trim(" " & GetData("tracker_svensk_item", "episode", "tracker_svensk_itemID", lblOriginalSvenskTrackerID.Caption, True))
            lblOriginalIngestDate(1).Caption = Trim(" " & GetData("tracker_svensk_item", "filemade", "tracker_svensk_itemID", lblOriginalSvenskTrackerID.Caption, True))
            lblOriginalXmlDeliveryDate(1).Caption = Trim(" " & GetData("tracker_svensk_item", "xmlsent", "tracker_svensk_itemID", lblOriginalSvenskTrackerID.Caption, True))
            cmbOriginalOperator(2).Text = Trim(" " & GetData("complaint", "originaloperator", "complaintID", lblcomplaintID.Caption, True))
            lblDecisionTreeDate(1).Caption = Trim(" " & GetData("tracker_svensk_item", "decisiontreedate", "tracker_svensk_itemID", lblOriginalSvenskTrackerID.Caption, True))
            lblFixJobID(1).Caption = Trim(" " & GetData("tracker_svensk_item", "fixedjobID", "tracker_svensk_itemID", lblOriginalSvenskTrackerID.Caption, True))
            txtRejectionIssue(2).Text = grdComplaints.Columns("Complaint").Text
            txtTrackerFindings(2).Text = Trim(" " & GetData("complaint", "findings", "complaintID", lblcomplaintID.Caption, True))
            cmbInternalExternal(2).Text = Trim(" " & GetData("complaint", "InternalExternal", "complaintID", lblcomplaintID.Caption, True))
            cmbInternalDepartment(2).Text = Trim(" " & GetData("complaint", "IssueInternalDepartment", "complaintID", lblcomplaintID.Caption, True))
            cmbAssignedTo(2).Text = Trim(" " & GetData("complaint", "assignedto", "complaintID", lblcomplaintID.Caption, True))
            If Not CheckAccess("/signoffcomplaints", True) Then cmbSignoff(2).Enabled = False Else cmbSignoff(2).Enabled = True
            cmbSignoff(2).Text = Trim(" " & GetData("complaint", "signoffby", "complaintID", lblcomplaintID.Caption, True))
            optFindings2(Val(Trim(" " & GetData("complaint", "redorequired", "complaintID", lblcomplaintID.Caption)))).Value = True
            cmbSignoff(2).Text = Trim(" " & GetData("complaint", "signoffby", "complaintID", lblcomplaintID.Caption, True))
            cmbCetaUser(2).Text = Trim(" " & GetData("complaint", "RRContactName", "complaintID", lblcomplaintID.Caption, True))
            lblRRContactID(2).Caption = Trim(" " & GetData("complaint", "RRContactUserID", "complaintID", lblcomplaintID.Caption, True))
            lblRRContactEmail(2).Caption = Trim(" " & GetData("complaint", "RRContactEmail", "complaintID", lblcomplaintID.Caption, True))
            cmbIssueType(2).Text = Trim(" " & GetData("complaint", "issuetype", "complaintID", lblcomplaintID.Caption, True))
        ElseIf grdComplaints.Columns("complainttype").Text = 3 Then
            'iTunes Tracker Issue
            tabComplaintType.Tab = 3
            lblOriginaliTunesTrackerID.Caption = Trim(" " & GetData("complaint", "originalitunestrackerID", "complaintID", lblcomplaintID.Caption, True))
            lblReference(2).Caption = Trim(" " & GetData("tracker_iTunes_item", "itemreference", "tracker_iTunes_itemID", lblOriginaliTunesTrackerID.Caption, True))
            lblOriginalTitle(2).Caption = Trim(" " & GetData("tracker_iTunes_item", "title", "tracker_iTunes_itemID", lblOriginaliTunesTrackerID.Caption, True))
            lblOriginalSubtitle(2).Caption = Trim(" " & GetData("tracker_iTunes_item", "EpisodeTitle", "tracker_iTunes_itemID", lblOriginaliTunesTrackerID.Caption, True))
            lblOriginalSeries(2).Caption = Trim(" " & GetData("tracker_iTunes_item", "series", "tracker_iTunes_itemID", lblOriginaliTunesTrackerID.Caption, True))
            lblOriginalVolume(2).Caption = Trim(" " & GetData("tracker_iTunes_item", "Volume", "tracker_iTunes_itemID", lblOriginaliTunesTrackerID.Caption, True))
            lblOriginalEpisode(2).Caption = Trim(" " & GetData("tracker_iTunes_item", "Episode", "tracker_iTunes_itemID", lblOriginaliTunesTrackerID.Caption, True))
            lblOriginaliTunesVendorID(2).Caption = Trim(" " & GetData("tracker_iTunes_item", "iTunesVendorID", "tracker_iTunes_itemID", lblOriginaliTunesTrackerID.Caption, True))
            cmbOriginalOperator(3).Text = Trim(" " & GetData("complaint", "originaloperator", "complaintID", lblcomplaintID.Caption, True))
    '        lblOriginalIngestDate(1).Caption = Trim(" " & GetData("tracker_svensk_item", "filemade", "tracker_svensk_itemID", lblOriginalSvenskTrackerID.Caption, True))
    '        lblOriginalXmlDeliveryDate(1).Caption = Trim(" " & GetData("tracker_svensk_item", "xmlsent", "tracker_svensk_itemID", lblOriginalSvenskTrackerID.Caption, True))
    '        lblDecisionTreeDate(1).Caption = Trim(" " & GetData("tracker_svensk_item", "decisiontreedate", "tracker_svensk_itemID", lblOriginalSvenskTrackerID.Caption, True))
    '        lblFixJobID(1).Caption = Trim(" " & GetData("tracker_svensk_item", "fixedjobID", "tracker_svensk_itemID", lblOriginalSvenskTrackerID.Caption, True))
            txtRejectionIssue(3).Text = grdComplaints.Columns("Complaint").Text
            txtTrackerFindings(3).Text = Trim(" " & GetData("complaint", "findings", "complaintID", lblcomplaintID.Caption, True))
            cmbInternalExternal(3).Text = Trim(" " & GetData("complaint", "InternalExternal", "complaintID", lblcomplaintID.Caption, True))
            cmbInternalDepartment(3).Text = Trim(" " & GetData("complaint", "IssueInternalDepartment", "complaintID", lblcomplaintID.Caption, True))
            cmbAssignedTo(3).Text = Trim(" " & GetData("complaint", "assignedto", "complaintID", lblcomplaintID.Caption, True))
            If Not CheckAccess("/signoffcomplaints", True) Then cmbSignoff(3).Enabled = False Else cmbSignoff(3).Enabled = True
            cmbSignoff(3).Text = Trim(" " & GetData("complaint", "signoffby", "complaintID", lblcomplaintID.Caption, True))
            optFindings3(Val(Trim(" " & GetData("complaint", "redorequired", "complaintID", lblcomplaintID.Caption)))).Value = True
            cmbSignoff(3).Text = Trim(" " & GetData("complaint", "signoffby", "complaintID", lblcomplaintID.Caption, True))
            cmbCetaUser(3).Text = Trim(" " & GetData("complaint", "RRContactName", "complaintID", lblcomplaintID.Caption, True))
            lblRRContactID(3).Caption = Trim(" " & GetData("complaint", "RRContactUserID", "complaintID", lblcomplaintID.Caption, True))
            lblRRContactEmail(3).Caption = Trim(" " & GetData("complaint", "RRContactEmail", "complaintID", lblcomplaintID.Caption, True))
            cmbIssueType(3).Text = Trim(" " & GetData("complaint", "issuetype", "complaintID", lblcomplaintID.Caption, True))
        ElseIf grdComplaints.Columns("complainttype").Text = 4 Then
            'Non RR DADC Issues
            tabComplaintType.Tab = 4
            lblCompanyID(4).Caption = grdComplaints.Columns("companyID").Text
            lblContactID(4).Caption = grdComplaints.Columns("ContactID").Text
'            cmbCompany.Text = grdComplaints.Columns("Client").Text
            cmbContact(4).Text = grdComplaints.Columns("Contact").Text
            txtRejectionIssue(4).Text = grdComplaints.Columns("Complaint").Text
            txtTitle(4).Text = grdComplaints.Columns("title").Text
            cmbAssignedTo(4).Text = Trim(" " & GetData("complaint", "assignedto", "complaintID", lblcomplaintID.Caption, True))
            txtTrackerFindings(4).Text = Trim(" " & GetData("complaint", "findings", "complaintID", lblcomplaintID.Caption, True))
            If Not CheckAccess("/signoffcomplaints", True) Then cmbSignoff(4).Enabled = False Else cmbSignoff(4).Enabled = True
            cmbSignoff(4).Text = Trim(" " & GetData("complaint", "signoffby", "complaintID", lblcomplaintID.Caption, True))
            optFindings(Val(Trim(" " & GetData("complaint", "redorequired", "complaintID", lblcomplaintID.Caption)))).Value = True
            lblOriginalDADCTrackerID.Caption = ""
            txtAffinityCaseNumber(4).Text = Trim(" " & GetData("complaint", "AffinityCaseNumber", "complaintID", lblcomplaintID.Caption, True))
            txtSourceID(0).Text = Trim(" " & GetData("complaint", "MasterSourceID", "complaintID", lblcomplaintID.Caption, True))
            txtSourceID2(0).Text = Trim(" " & GetData("complaint", "MasterSourceID2", "complaintID", lblcomplaintID.Caption, True))
            txtSourceID3(0).Text = Trim(" " & GetData("complaint", "MasterSourceID3", "complaintID", lblcomplaintID.Caption, True))
            datMasterArrived(0).Value = Trim(" " & GetData("complaint", "DateMasterArrived", "complaintID", lblcomplaintID.Caption, True))
            datMasterArrived2(0).Value = Trim(" " & GetData("complaint", "DateMasterArrived2", "complaintID", lblcomplaintID.Caption, True))
            datMasterArrived3(0).Value = Trim(" " & GetData("complaint", "DateMasterArrived3", "complaintID", lblcomplaintID.Caption, True))
            datTargetDate(0).Value = Trim(" " & GetData("complaint", "TargetDate", "complaintID", lblcomplaintID.Caption, True))
            txtContentVersionCode = Trim(" " & GetData("complaint", "ContentVersionCode", "complaintID", lblcomplaintID.Caption, True))
            datFileArrived.Value = Trim(" " & GetData("complaint", "DateFileArrived", "complaintID", lblcomplaintID.Caption, True))
            datInvestigated.Value = Trim(" " & GetData("complaint", "DateInvestigated", "complaintID", lblcomplaintID.Caption, True))
            datSignOff.Value = Trim(" " & GetData("complaint", "DateSignedOff", "complaintID", lblcomplaintID.Caption, True))
            If GetData("complaint", "readytobill", "complaintID", lblcomplaintID.Caption) = 1 And GetData("complaint", "billed", "complaintID", lblcomplaintID.Caption) = 0 Then
                fraBilling.Enabled = True
            Else
                fraBilling.Enabled = False
            End If
            cmbCetaUser(4).Text = Trim(" " & GetData("complaint", "RRContactName", "complaintID", lblcomplaintID.Caption, True))
            lblRRContactID(4).Caption = Trim(" " & GetData("complaint", "RRContactUserID", "complaintID", lblcomplaintID.Caption, True))
            lblRRContactEmail(4).Caption = Trim(" " & GetData("complaint", "RRContactEmail", "complaintID", lblcomplaintID.Caption, True))
        ElseIf grdComplaints.Columns("complainttype").Text = 5 Then
            'BBCWW Tracker Issue
            tabComplaintType.Tab = 5
            lblCompanyID(1).Caption = grdComplaints.Columns("companyID").Text
            lblContactID(1).Caption = grdComplaints.Columns("ContactID").Text
            cmbCompany(1).Text = grdComplaints.Columns("Client").Text
            cmbContact(1).Text = grdComplaints.Columns("Contact").Text
            txtRejectionIssue(5).Text = grdComplaints.Columns("Complaint").Text
            txtTitle(1).Text = grdComplaints.Columns("title").Text
            cmbOriginalOperator(4).Text = Trim(" " & GetData("complaint", "originaloperator", "complaintID", lblcomplaintID.Caption, True))
            txtOriginalJobID(1).Text = Trim(" " & GetData("complaint", "originaljobID", "complaintID", lblcomplaintID.Caption, True))
            txtOriginalJobdetailID(1).Text = Trim(" " & GetData("complaint", "originaljobdetailID", "complaintID", lblcomplaintID.Caption, True))
            txtOrderRef(1).Text = GetData("complaint", "orderreference", "complaintID", lblcomplaintID.Caption)
            datJobCompletedDate(1).Value = GetData("complaint", "jobcompleteddate", "complaintID", lblcomplaintID.Caption, True)
            cmbAssignedTo(5).Text = Trim(" " & GetData("complaint", "assignedto", "complaintID", lblcomplaintID.Caption, True))
            txtTrackerFindings(5).Text = Trim(" " & GetData("complaint", "findings", "complaintID", lblcomplaintID.Caption, True))
            cmbInternalExternal(4).Text = Trim(" " & GetData("complaint", "InternalExternal", "complaintID", lblcomplaintID.Caption, True))
            cmbInternalDepartment(4).Text = Trim(" " & GetData("complaint", "IssueInternalDepartment", "complaintID", lblcomplaintID.Caption, True))
            txtConclusion(1).Text = Trim(" " & GetData("complaint", "conclusion", "complaintID", lblcomplaintID.Caption, True))
            If Not CheckAccess("/signoffcomplaints", True) Then cmbSignoff(5).Enabled = False Else cmbSignoff(5).Enabled = True
            cmbSignoff(5).Text = Trim(" " & GetData("complaint", "signoffby", "complaintID", lblcomplaintID.Caption, True))
            optFindings4(Val(Trim(" " & GetData("complaint", "redorequired", "complaintID", lblcomplaintID.Caption)))).Value = True
            lblOriginalDADCTrackerID.Caption = ""
            cmbRejectionCategory.Text = GetData("complaint", "RejectionCategory", "complaintID", lblcomplaintID.Caption)
            cmbRejectionType.Text = GetData("complaint", "RejectionType", "complaintID", lblcomplaintID.Caption)
            cmbRejectionAccountability.Text = GetData("complaint", "RejectionAccountability", "complaintID", lblcomplaintID.Caption)
            cmbRejectionReason.Text = GetData("complaint", "RejectionReason", "complaintID", lblcomplaintID.Caption)
            cmbCetaUser(5).Text = Trim(" " & GetData("complaint", "RRContactName", "complaintID", lblcomplaintID.Caption, True))
            lblRRContactID(5).Caption = Trim(" " & GetData("complaint", "RRContactUserID", "complaintID", lblcomplaintID.Caption, True))
            lblRRContactEmail(5).Caption = Trim(" " & GetData("complaint", "RRContactEmail", "complaintID", lblcomplaintID.Caption, True))
            cmbIssueType(4).Text = Trim(" " & GetData("complaint", "issuetype", "complaintID", lblcomplaintID.Caption, True))
        Else
            'Normal Job Issue
            tabComplaintType.Tab = 0
            lblCompanyID(0).Caption = grdComplaints.Columns("companyID").Text
            lblContactID(0).Caption = grdComplaints.Columns("ContactID").Text
            cmbCompany(0).Text = grdComplaints.Columns("Client").Text
            cmbContact(0).Text = grdComplaints.Columns("Contact").Text
            txtRejectionIssue(0).Text = grdComplaints.Columns("Complaint").Text
            txtTitle(0).Text = grdComplaints.Columns("title").Text
            cmbOriginalOperator(0).Text = Trim(" " & GetData("complaint", "originaloperator", "complaintID", lblcomplaintID.Caption, True))
            txtOriginalJobID(0).Text = Trim(" " & GetData("complaint", "originaljobID", "complaintID", lblcomplaintID.Caption, True))
            txtOriginalJobdetailID(0).Text = Trim(" " & GetData("complaint", "originaljobdetailID", "complaintID", lblcomplaintID.Caption, True))
            txtOrderRef(0).Text = GetData("complaint", "orderreference", "complaintID", lblcomplaintID.Caption)
            datJobCompletedDate(0).Value = GetData("complaint", "jobcompleteddate", "complaintID", lblcomplaintID.Caption, True)
            cmbAssignedTo(0).Text = Trim(" " & GetData("complaint", "assignedto", "complaintID", lblcomplaintID.Caption, True))
            txtTrackerFindings(0).Text = Trim(" " & GetData("complaint", "findings", "complaintID", lblcomplaintID.Caption, True))
'            chkInternalIssue(0).Value = GetFlag(GetData("complaint", "InternalIssue", "complaintID", lblcomplaintID.Caption))
            cmbInternalExternal(0).Text = Trim(" " & GetData("complaint", "InternalExternal", "complaintID", lblcomplaintID.Caption, True))
            cmbInternalDepartment(0).Text = Trim(" " & GetData("complaint", "IssueInternalDepartment", "complaintID", lblcomplaintID.Caption, True))
            txtConclusion(0).Text = Trim(" " & GetData("complaint", "conclusion", "complaintID", lblcomplaintID.Caption, True))
            If Not CheckAccess("/signoffcomplaints", True) Then cmbSignoff(0).Enabled = False Else cmbSignoff(0).Enabled = True
            cmbSignoff(0).Text = Trim(" " & GetData("complaint", "signoffby", "complaintID", lblcomplaintID.Caption, True))
            optFindings(Val(Trim(" " & GetData("complaint", "redorequired", "complaintID", lblcomplaintID.Caption)))).Value = True
            lblOriginalDADCTrackerID.Caption = ""
            cmbCetaUser(0).Text = Trim(" " & GetData("complaint", "RRContactName", "complaintID", lblcomplaintID.Caption, True))
            lblRRContactID(0).Caption = Trim(" " & GetData("complaint", "RRContactUserID", "complaintID", lblcomplaintID.Caption, True))
            lblRRContactEmail(0).Caption = Trim(" " & GetData("complaint", "RRContactEmail", "complaintID", lblcomplaintID.Caption, True))
            cmbIssueType(0).Text = Trim(" " & GetData("complaint", "issuetype", "complaintID", lblcomplaintID.Caption, True))
        End If
    End If
Else

    ClearFields frmComplaint

End If

End Sub

Private Sub grdComplaints_RowLoaded(ByVal Bookmark As Variant)

If grdComplaints.Columns("complainttype").Text = 1 Then
    grdComplaints.Columns("Client").Text = GetData("company", "name", "companyID", grdComplaints.Columns("companyID").Text)
    grdComplaints.Columns("contact").Text = GetData("contact", "name", "contactID", grdComplaints.Columns("contactID").Text)
    grdComplaints.Columns("title").Text = GetData("tracker_dadc_item", "title", "tracker_dadc_itemID", grdComplaints.Columns("originaldadctrackerID").Text)
    grdComplaints.Columns("Type").Text = "DADC"
ElseIf grdComplaints.Columns("complainttype").Text = 2 Then
    grdComplaints.Columns("Client").Text = GetData("company", "name", "companyID", grdComplaints.Columns("companyID").Text)
    grdComplaints.Columns("contact").Text = GetData("contact", "name", "contactID", grdComplaints.Columns("contactID").Text)
    grdComplaints.Columns("title").Text = GetData("tracker_svensk_item", "title", "tracker_svensk_itemID", grdComplaints.Columns("originalsvensktrackerID").Text)
    grdComplaints.Columns("Type").Text = "Svensk"
ElseIf grdComplaints.Columns("complainttype").Text = 3 Then
    grdComplaints.Columns("Client").Text = GetData("company", "portalcompanyname", "companyID", grdComplaints.Columns("companyID").Text)
    grdComplaints.Columns("contact").Text = GetData("contact", "name", "contactID", grdComplaints.Columns("contactID").Text)
    grdComplaints.Columns("title").Text = GetData("tracker_iTunes_item", "title", "tracker_iTunes_itemID", grdComplaints.Columns("originaliTunestrackerID").Text)
    grdComplaints.Columns("Type").Text = "iTunes"
ElseIf grdComplaints.Columns("complainttype").Text = 4 Then
    grdComplaints.Columns("Client").Text = GetData("company", "portalcompanyname", "companyID", grdComplaints.Columns("companyID").Text)
    grdComplaints.Columns("contact").Text = GetData("contact", "name", "contactID", grdComplaints.Columns("contactID").Text)
    grdComplaints.Columns("title").Text = GetData("complaint", "title", "complaintID", grdComplaints.Columns("complaintID").Text)
    grdComplaints.Columns("Type").Text = "DADC (Non-MX1)"
ElseIf grdComplaints.Columns("complainttype").Text = 5 Then
    grdComplaints.Columns("Client").Text = GetData("company", "name", "companyID", grdComplaints.Columns("companyID").Text)
    grdComplaints.Columns("contact").Text = GetData("contact", "name", "contactID", grdComplaints.Columns("contactID").Text)
    grdComplaints.Columns("title").Text = GetData("complaint", "title", "complaintID", grdComplaints.Columns("complaintID").Text)
    grdComplaints.Columns("Type").Text = "BBCWW"
Else
    grdComplaints.Columns("Client").Text = GetData("company", "name", "companyID", grdComplaints.Columns("companyID").Text)
    grdComplaints.Columns("contact").Text = GetData("contact", "name", "contactID", grdComplaints.Columns("contactID").Text)
    grdComplaints.Columns("title").Text = GetData("complaint", "title", "complaintID", grdComplaints.Columns("complaintID").Text)
    grdComplaints.Columns("Type").Text = "Normal"
End If

End Sub

Private Sub cmbCompany_Click(Index As Integer)

lblCompanyID(Index).Caption = cmbCompany(Index).Columns("companyID").Text

End Sub

'Private Sub cmbCompany_DropDown(Index As Integer)
'adoCompany(Index).Refresh
'End Sub
'
Private Sub cmbContact_Click(Index As Integer)

lblContactID(Index).Caption = cmbContact(Index).Columns("contactID").Text

End Sub

Private Sub cmbContact_DropDown(Index As Integer)

If lblCompanyID(Index).Caption = "" Then
    NoCompanySelectedMessage
    Exit Sub
End If

Dim l_strSQL As String

l_strSQL = "SELECT company.companyID, contact.contactID, contact.name, contact.telephone, employee.jobtitle FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((company.companyID)=" & lblCompanyID(Index).Caption & ")) ORDER BY contact.name;"

adoContact.ConnectionString = g_strConnection
adoContact.RecordSource = l_strSQL
adoContact.Refresh

End Sub

Private Sub cmdClear_Click()
ClearFields Me
If MsgBox("Is this an MX1 Issue? (No means it is a DADC Special Issue)", vbYesNo, "Question...") = vbYes Then
    tabComplaintType.Tab = 0
    optFindings(0).Value = True
Else
    tabComplaintType.Tab = 4
    lblCompanyID(4).Caption = 1292
    optFindings(8).Value = True
End If
picDetails.Visible = True
End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdPrint_Click()

cmdSave_Click

Dim l_strReportToPrint As String

If grdComplaints.Columns("complainttype").Text = 0 Then
    l_strReportToPrint = g_strLocationOfCrystalReportFiles & "complaint.rpt"
    PrintCrystalReport l_strReportToPrint, "{complaint.complaintID} = " & lblcomplaintID.Caption, g_blnPreviewReport
Else
    l_strReportToPrint = g_strLocationOfCrystalReportFiles & "dadc_complaint.rpt"
    PrintCrystalReport l_strReportToPrint, "{complaint.complaintID} = " & lblcomplaintID.Caption, g_blnPreviewReport
End If

End Sub

Private Sub cmdSearch_Click()

Dim l_strSQL As String, l_blnFirstItem As Boolean

m_blnSilent = True

l_strSQL = "SELECT * FROM complaint "

m_strSearchFilter = "WHERE 1=1 "
m_strCrystalSearchFilter = "1=1 "
        
If chkFilter(0).Value <> 0 Then
    'Do nothing
Else
    If chkFilter(1).Value <> 0 Then
        m_strSearchFilter = m_strSearchFilter & "AND ("
        m_strCrystalSearchFilter = m_strCrystalSearchFilter & "AND ( "
        m_strSearchFilter = m_strSearchFilter & "(signoffby IS NOT NULL AND signoffby <> '') "
        m_strCrystalSearchFilter = m_strCrystalSearchFilter & "(not isnull({complaint.signoffby}) AND {complaint.signoffby} <> '') "
    ElseIf chkFilter(2).Value <> 0 Then
        m_strSearchFilter = m_strSearchFilter & "AND ("
        m_strCrystalSearchFilter = m_strCrystalSearchFilter & "AND ( "
        m_strSearchFilter = m_strSearchFilter & "(signoffby IS NULL or signoffby = '') "
        m_strCrystalSearchFilter = m_strCrystalSearchFilter & "(isnull({complaint.signoffby}) OR {complaint.signoffby} = '') "
    End If
    If chkFilter(2).Value <> 0 And chkFilter(1).Value <> 0 Then
        m_strSearchFilter = m_strSearchFilter & "OR (signoffby IS NULL or signoffby = '') "
        m_strCrystalSearchFilter = m_strCrystalSearchFilter & " OR ((isnull({complaint.signoffby}) OR {complaint.signoffby} = '')) "
    End If
    
    If chkFilter(0).Value = 0 And chkFilter(1).Value = 0 And chkFilter(2).Value = 0 Then
        l_blnFirstItem = True
        If chkFilter(3).Value <> 0 Then
            If l_blnFirstItem = False Then
                m_strSearchFilter = m_strSearchFilter & "OR "
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "OR "
                l_blnFirstItem = False
            Else
                m_strSearchFilter = m_strSearchFilter & "AND ("
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "AND ( "
                l_blnFirstItem = False
            End If
            m_strSearchFilter = m_strSearchFilter & "(signoffby IS NOT NULL AND signoffby <> '' AND complainttype = 0) "
            m_strCrystalSearchFilter = m_strCrystalSearchFilter & "((not isnull({complaint.signoffby}) AND {complaint.signoffby} <> '') AND {complaint.complainttype} = 0) "
            l_blnFirstItem = False
        End If
        If chkFilter(4).Value <> 0 Then
            If l_blnFirstItem = False Then
                m_strSearchFilter = m_strSearchFilter & "OR "
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "OR "
                l_blnFirstItem = False
            Else
                m_strSearchFilter = m_strSearchFilter & "AND ("
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "AND ( "
                l_blnFirstItem = False
            End If
            m_strSearchFilter = m_strSearchFilter & "(signoffby IS NOT NULL AND signoffby <> '' AND complainttype = 1) "
            m_strCrystalSearchFilter = m_strCrystalSearchFilter & "((not isnull({complaint.signoffby}) AND {complaint.signoffby} <> '') AND {complaint.complainttype} = 1) "
        End If
        If chkFilter(5).Value <> 0 Then
            If l_blnFirstItem = False Then
                m_strSearchFilter = m_strSearchFilter & "OR "
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "OR "
                l_blnFirstItem = False
            Else
                m_strSearchFilter = m_strSearchFilter & "AND ("
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "AND ( "
                l_blnFirstItem = False
            End If
            m_strSearchFilter = m_strSearchFilter & "(signoffby IS NOT NULL AND signoffby <> '' AND complainttype = 2) "
            m_strCrystalSearchFilter = m_strCrystalSearchFilter & "((not isnull({complaint.signoffby}) AND {complaint.signoffby} <> '') AND {complaint.complainttype} = 2) "
        End If
        If chkFilter(6).Value <> 0 Then
            If l_blnFirstItem = False Then
                m_strSearchFilter = m_strSearchFilter & "OR "
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "OR "
                l_blnFirstItem = False
            Else
                m_strSearchFilter = m_strSearchFilter & "AND ("
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "AND ( "
                l_blnFirstItem = False
            End If
            m_strSearchFilter = m_strSearchFilter & "(signoffby IS NOT NULL AND signoffby <> '' AND complainttype = 3) "
            m_strCrystalSearchFilter = m_strCrystalSearchFilter & "((not isnull({complaint.signoffby}) AND {complaint.signoffby} <> '') AND {complaint.complainttype} = 3) "
        End If
        If chkFilter(7).Value <> 0 Then
            If l_blnFirstItem = False Then
                m_strSearchFilter = m_strSearchFilter & "OR "
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "OR "
                l_blnFirstItem = False
            Else
                m_strSearchFilter = m_strSearchFilter & "AND ("
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "AND ( "
                l_blnFirstItem = False
            End If
            m_strSearchFilter = m_strSearchFilter & "(signoffby IS NOT NULL AND signoffby <> '' AND complainttype = 4)  "
            m_strCrystalSearchFilter = m_strCrystalSearchFilter & "(not isnull({complaint.signoffby}) AND {complaint.signoffby} <> '' AND {complaint.complainttype} = 4) "
        End If
        If chkFilter(8).Value <> 0 Then
            If l_blnFirstItem = False Then
                m_strSearchFilter = m_strSearchFilter & "OR "
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "OR "
                l_blnFirstItem = False
            Else
                m_strSearchFilter = m_strSearchFilter & "AND ("
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "AND ( "
                l_blnFirstItem = False
            End If
            m_strSearchFilter = m_strSearchFilter & "(signoffby IS NOT NULL AND signoffby <> '' AND complainttype = 5) "
            m_strCrystalSearchFilter = m_strCrystalSearchFilter & "((not isnull({complaint.signoffby}) AND {complaint.signoffby} <> '') AND {complaint.complainttype} = 5) "
        End If
        If chkFilter(9).Value <> 0 Then
            If l_blnFirstItem = False Then
                m_strSearchFilter = m_strSearchFilter & "OR "
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "OR "
                l_blnFirstItem = False
            Else
                m_strSearchFilter = m_strSearchFilter & "AND ("
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "AND ( "
                l_blnFirstItem = False
            End If
            m_strSearchFilter = m_strSearchFilter & "((signoffby IS NULL or signoffby = '') AND complainttype = 0) "
            m_strCrystalSearchFilter = m_strCrystalSearchFilter & "((isnull({complaint.signoffby}) OR {complaint.signoffby} = """") AND {complaint.complainttype} = 0) "
        End If
        If chkFilter(10).Value <> 0 Then
            If l_blnFirstItem = False Then
                m_strSearchFilter = m_strSearchFilter & "OR "
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "OR "
                l_blnFirstItem = False
            Else
                m_strSearchFilter = m_strSearchFilter & "AND ("
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "AND ( "
                l_blnFirstItem = False
            End If
            m_strSearchFilter = m_strSearchFilter & "((signoffby IS NULL or signoffby = '') AND complainttype = 1) "
            m_strCrystalSearchFilter = m_strCrystalSearchFilter & "((isnull({complaint.signoffby}) OR {complaint.signoffby} = '') AND {complaint.complainttype} = 1) "
        End If
        If chkFilter(11).Value <> 0 Then
            If l_blnFirstItem = False Then
                m_strSearchFilter = m_strSearchFilter & "OR "
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "OR "
                l_blnFirstItem = False
            Else
                m_strSearchFilter = m_strSearchFilter & "AND ("
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "AND ( "
                l_blnFirstItem = False
            End If
            m_strSearchFilter = m_strSearchFilter & "((signoffby IS NULL or signoffby = '') AND complainttype = 2) "
            m_strCrystalSearchFilter = m_strCrystalSearchFilter & "((isnull({complaint.signoffby}) OR {complaint.signoffby} = '') AND {complaint.complainttype} = 2) "
        End If
        If chkFilter(12).Value <> 0 Then
            If l_blnFirstItem = False Then
                m_strSearchFilter = m_strSearchFilter & "OR "
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "OR "
                l_blnFirstItem = False
            Else
                m_strSearchFilter = m_strSearchFilter & "AND ("
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "AND ( "
                l_blnFirstItem = False
            End If
            m_strSearchFilter = m_strSearchFilter & "((signoffby IS NULL or signoffby = '') AND complainttype = 3) "
            m_strCrystalSearchFilter = m_strCrystalSearchFilter & "((isnull({complaint.signoffby}) OR {complaint.signoffby} = '') AND {complaint.complainttype} = 3) "
        End If
        If chkFilter(13).Value <> 0 Then
            If l_blnFirstItem = False Then
                m_strSearchFilter = m_strSearchFilter & "OR "
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "OR "
                l_blnFirstItem = False
            Else
                m_strSearchFilter = m_strSearchFilter & "AND ("
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "AND ( "
                l_blnFirstItem = False
            End If
            m_strSearchFilter = m_strSearchFilter & "((signoffby IS NULL OR signoffby = '') AND complainttype = 4) "
            m_strCrystalSearchFilter = m_strCrystalSearchFilter & "((isnull({complaint.signoffby}) OR {complaint.signoffby} = '') AND {complaint.complainttype} = 4) "
        End If
        If chkFilter(14).Value <> 0 Then
            If l_blnFirstItem = False Then
                m_strSearchFilter = m_strSearchFilter & "OR "
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "OR "
                l_blnFirstItem = False
            Else
                m_strSearchFilter = m_strSearchFilter & "AND ("
                m_strCrystalSearchFilter = m_strCrystalSearchFilter & "AND ( "
                l_blnFirstItem = False
            End If
            m_strSearchFilter = m_strSearchFilter & "((signoffby IS NULL or signoffby = '') AND complainttype = 5) "
            m_strCrystalSearchFilter = m_strCrystalSearchFilter & "((isnull({complaint.signoffby}) OR {complaint.signoffby} = '') AND {complaint.complainttype} = 5) "
        End If
        If l_blnFirstItem = False Then
            m_strSearchFilter = m_strSearchFilter & ")"
            m_strCrystalSearchFilter = m_strCrystalSearchFilter & ")"
        End If
    Else
        m_strSearchFilter = m_strSearchFilter & ")"
        m_strCrystalSearchFilter = m_strCrystalSearchFilter & ")"
    End If
End If

l_strSQL = l_strSQL & m_strSearchFilter

If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strSQL = l_strSQL & " AND companyID = " & Val(lblSearchCompanyID.Caption)
End If

If cmbFilterInternalDepartment.Text <> "" Then
    l_strSQL = l_strSQL & " AND IssueInternalDepartment = '" & cmbFilterInternalDepartment.Text & "' "
End If

If cmbFilterOriginalOperator.Text <> "" Then
    l_strSQL = l_strSQL & " AND originaloperator = '" & cmbFilterOriginalOperator.Text & "' "
End If

If cmbFilterCetaUser.Text <> "" Then
    l_strSQL = l_strSQL & " AND RRContactName = '" & cmbFilterCetaUser.Text & "' "
End If

If txtSearchJobID.Text <> "" Then
    l_strSQL = l_strSQL & " AND originaljobid = '" & txtSearchJobID.Text & "' "
End If

If txtSearchTitle.Text <> "" Then
    l_strSQL = l_strSQL & " AND title = '" & txtSearchTitle.Text & "' "
End If

If cmbSearchIssueType.Text <> "" Then
    l_strSQL = l_strSQL & " AND IssueType = '" & cmbSearchIssueType.Text & "' "
End If

If Not IsNull(datStartDate.Value) Then
    If optDateFilter(0).Value = True Then
        l_strSQL = l_strSQL & " AND cdate >= '" & Format(datStartDate.Value, "YYYY-MM-DD") & "'"
    Else
        l_strSQL = l_strSQL & " AND DateSignedOff >= '" & Format(datStartDate.Value, "YYYY-MM-DD") & "'"
    End If
End If

If Not IsNull(datEndDate.Value) Then
    If optDateFilter(0).Value = True Then
        l_strSQL = l_strSQL & " AND cdate <= '" & Format(datEndDate.Value, "YYYY-MM-DD 23:59:59") & "'"
    Else
        l_strSQL = l_strSQL & " AND DateSignedOff <= '" & Format(datEndDate.Value, "YYYY-MM-DD 23:59:59") & "'"
    End If
End If

If cmbSearchInternalExternal.Text <> "" Then
    l_strSQL = l_strSQL & " AND InternalExternal = '" & cmbSearchInternalExternal.Text & "' "
End If

l_strSQL = l_strSQL & " ORDER BY cdate DESC;"

Debug.Print l_strSQL

adoComplaints.ConnectionString = g_strConnection
adoComplaints.RecordSource = l_strSQL
adoComplaints.Refresh

adoComplaints.Caption = adoComplaints.Recordset.RecordCount & " Item(s)"
m_blnSilent = False

If Not adoComplaints.Recordset.EOF Then
    adoComplaints.Recordset.MoveLast
    adoComplaints.Recordset.MoveFirst
End If

End Sub

Private Sub Form_Load()

'populate the company drop down (data bound)
Dim l_strSQL As String
l_strSQL = "SELECT name, accountcode, telephone, fax, accountstatus, companyID FROM company WHERE iscustomer = 1 ORDER BY name"

adoCompany.ConnectionString = g_strConnection
adoCompany.RecordSource = l_strSQL
adoCompany.Refresh

l_strSQL = "SELECT cetauser.fullname, cetauser.cetauserID, cetauser.email, cetauser.username, cetauser2.username as 'securityprofile' FROM cetauser LEFT JOIN cetauser AS cetauser2 ON cetauser.securitytemplate = cetauser2.cetauserID WHERE cetauser.securitytemplate <> 120 ORDER BY cetauser.username"

adoCetaUser.ConnectionString = g_strConnection
adoCetaUser.RecordSource = l_strSQL
adoCetaUser.Refresh

m_strSearchFilter = " WHERE (signoffby IS NULL or signoffby = '') "
m_blnSilent = False
m_blnBillAll = False

PopulateCombo "IssueRejectionCategory", cmbRejectionCategory
PopulateCombo "IssueRejectionType", cmbRejectionType
PopulateCombo "IssueRejectionAccountability", cmbRejectionAccountability
PopulateCombo "IssueRejectionReason", cmbRejectionReason
PopulateCombo "IssueInternalDEpartment", cmbFilterInternalDepartment
PopulateCombo "users", cmbFilterOriginalOperator

cmdSearch.Value = True

MakeLookLikeOffice Me

CenterForm Me

End Sub

Private Sub Form_Resize()

On Error Resume Next

picFooter.Top = Me.ScaleHeight - picFooter.Height - 120
picFooter.Left = Me.ScaleWidth - picFooter.Width - 120

grdComplaints.Height = picFooter.Top - 120 - grdComplaints.Top

picDetails.Left = (Me.ScaleWidth - picDetails.Width) / 2
picDetails.Top = (Me.ScaleHeight - picDetails.Height) / 2

End Sub

Private Sub tabComplaintType_Click(PreviousTab As Integer)

If tabComplaintType.Tab = 4 Then lblCompanyID(4).Caption = 1292

End Sub

Private Sub Timer1_Timer()

m_lngTimeSinceLastSave = m_lngTimeSinceLastSave + 1
cmdSave.Caption = "Save (" & m_lngTimeSinceLastSave & ")"

End Sub

Private Sub txtOriginalJobID_KeyPress(Index As Integer, KeyAscii As Integer)

If KeyAscii = 13 Then
    KeyAscii = 0
    If IsNumeric(txtOriginalJobID(Index).Text) Then
        lblCompanyID(Index).Caption = GetData("job", "companyID", "jobID", Val(txtOriginalJobID(Index).Text))
        cmbCompany(Index).Text = GetData("job", "companyname", "jobID", Val(txtOriginalJobID(Index).Text))
        txtOrderRef(Index).Text = GetData("job", "orderreference", "jobID", Val(txtOriginalJobID(Index).Text))
        txtTitle(Index).Text = GetData("job", "title1", "jobID", Val(txtOriginalJobID(Index).Text))
        datJobCompletedDate(Index).Value = GetData("job", "completeddate", "jobID", Val(txtOriginalJobID(Index).Text))
    End If
End If

End Sub

Private Sub txtSourceID_KeyPress(Index As Integer, KeyAscii As Integer)

If KeyAscii = 13 Then
    KeyAscii = 0
    txtSourceID(Index).Text = UCase(txtSourceID(Index).Text)
    If txtSourceID(Index).Text <> "" Then
        If txtSourceID(Index).Text <> GetData("complaint", "MasterSourceID", "complaintID", lblcomplaintID.Caption) Then
            If GetData("library", "libraryID", "barcode", txtSourceID(Index).Text) <> "" Then
                If IsItHere(GetData("library", "location", "barcode", txtSourceID(Index).Text)) = True Then
                    datMasterArrived(Index).Value = Now
                    If CheckAllComplaintMastersHere(txtSourceID(Index).Text, datMasterArrived(Index).Value, txtSourceID2(Index).Text, datMasterArrived2(Index).Value, txtSourceID3(Index).Text, datMasterArrived(Index).Value) = True Then
                        If IsNull(datTargetDate(Index).Value) Then
                            datTargetDate(Index).Value = GetNewTargetDate
                        End If
                    Else
                        datMasterArrived(Index).Value = Null
                        datTargetDate(Index).Value = Null
                    End If
                Else
                    datMasterArrived(Index).Value = Null
                    datTargetDate(Index).Value = Null
                End If
            End If
        End If
    Else
        datMasterArrived(Index).Value = Null
        If CheckAllComplaintMastersHere(txtSourceID(Index).Text, datMasterArrived(Index).Value, txtSourceID2(Index).Text, datMasterArrived2(Index).Value, txtSourceID3(Index).Text, datMasterArrived(Index).Value) = True Then
            If IsNull(datTargetDate(Index).Value) Then datTargetDate(Index).Value = GetNewTargetDate
        Else
            datTargetDate(Index).Value = Null
        End If
    End If
End If

End Sub

Private Sub txtSourceID2_KeyPress(Index As Integer, KeyAscii As Integer)

If KeyAscii = 13 Then
    KeyAscii = 0
    txtSourceID2(Index).Text = UCase(txtSourceID2(Index).Text)
    If txtSourceID2(Index).Text <> "" Then
        If txtSourceID2(Index).Text <> GetData("complaint", "MasterSourceID2", "complaintID", lblcomplaintID.Caption) Then
            If GetData("library", "libraryID", "barcode", txtSourceID2(Index).Text) <> "" Then
                If IsItHere(GetData("library", "location", "barcode", txtSourceID2(Index).Text)) = True Then
                    datMasterArrived2(Index).Value = Now
                    If CheckAllComplaintMastersHere(txtSourceID(Index).Text, datMasterArrived(Index).Value, txtSourceID2(Index).Text, datMasterArrived2(Index).Value, txtSourceID3(Index).Text, datMasterArrived(Index).Value) = True Then
                        If IsNull(datTargetDate(Index).Value) Then
                            datTargetDate(Index).Value = GetNewTargetDate
                        End If
                    Else
                        datMasterArrived2(Index).Value = Null
                        datTargetDate(Index).Value = Null
                    End If
                Else
                    datMasterArrived2(Index).Value = Null
                    datTargetDate(Index).Value = Null
                End If
            End If
        End If
    Else
        datMasterArrived2(Index).Value = Null
        If CheckAllComplaintMastersHere(txtSourceID(Index).Text, datMasterArrived(Index).Value, txtSourceID2(Index).Text, datMasterArrived2(Index).Value, txtSourceID3(Index).Text, datMasterArrived(Index).Value) = True Then
            If IsNull(datTargetDate(Index).Value) Then datTargetDate(Index).Value = GetNewTargetDate
        Else
            datTargetDate(Index).Value = Null
        End If
    End If
End If

End Sub

Private Sub txtSourceID3_KeyPress(Index As Integer, KeyAscii As Integer)

If KeyAscii = 13 Then
    KeyAscii = 0
    txtSourceID3(Index).Text = UCase(txtSourceID3(Index).Text)
    If txtSourceID3(Index).Text <> "" Then
        If txtSourceID3(Index).Text <> GetData("complaint", "MasterSourceID3", "complaintID", lblcomplaintID.Caption) Then
            If GetData("library", "libraryID", "barcode", txtSourceID3(Index).Text) <> "" Then
                If IsItHere(GetData("library", "location", "barcode", txtSourceID3(Index).Text)) = True Then
                    datMasterArrived3(Index).Value = Now
                    If CheckAllComplaintMastersHere(txtSourceID(Index).Text, datMasterArrived(Index).Value, txtSourceID2(Index).Text, datMasterArrived2(Index).Value, txtSourceID3(Index).Text, datMasterArrived3(Index).Value) = True Then
                        If IsNull(datTargetDate(Index).Value) Then
                            datTargetDate(Index).Value = GetNewTargetDate
                        End If
                    Else
                        datMasterArrived3(Index).Value = Null
                        datTargetDate(Index).Value = Null
                    End If
                Else
                    datMasterArrived3(Index).Value = Null
                    datTargetDate(Index).Value = Null
                End If
            End If
        End If
    Else
        datMasterArrived3(Index).Value = Null
        If CheckAllComplaintMastersHere(txtSourceID(Index).Text, datMasterArrived(Index).Value, txtSourceID2(Index).Text, datMasterArrived2(Index).Value, txtSourceID3(Index).Text, datMasterArrived(Index).Value) = True Then
            If IsNull(datTargetDate(Index).Value) Then datTargetDate(Index).Value = GetNewTargetDate
        Else
            datTargetDate(Index).Value = Null
        End If
    End If
End If

End Sub
