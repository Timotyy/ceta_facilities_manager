Attribute VB_Name = "modSettings"
Option Explicit

Sub SaveCETASetting(lp_strSettingName As String, lp_varSettingValue As Variant)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    l_strSQL = "DELETE FROM setting WHERE name = '" & lp_strSettingName & "'"
    ExecuteSQL l_strSQL, g_strExecuteError
    
    CheckForSQLError
    
    l_strSQL = "INSERT INTO setting (name, value) VALUES ('" & lp_strSettingName & "', '" & lp_varSettingValue & "')"
    ExecuteSQL l_strSQL, g_strExecuteError
    
    CheckForSQLError
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub GetSettingsFromDatabase()
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strTempCaption  As String
    
    Screen.MousePointer = vbHourglass
    
    DoEvents
    
    Dim l_strSQL As String
    l_strSQL = "SELECT value, name FROM setting ORDER BY name"
    
    Dim l_rstSettings As New ADODB.Recordset
    Dim l_conSettings As New ADODB.Connection
    
    l_conSettings.Open g_strConnection
    
    With l_rstSettings
        .CursorLocation = adUseClient
        .LockType = adLockBatchOptimistic
        .CursorType = adOpenDynamic
        .Open l_strSQL, l_conSettings, adOpenDynamic
    End With
    
    l_rstSettings.ActiveConnection = Nothing
    l_conSettings.Close
    
    l_rstSettings.Filter = "name = 'DoOfflineCFMRequests'"
    g_optDoOfflineCFMRequests = Format(l_rstSettings("value"))

    l_rstSettings.Filter = "name = 'VDMSEmailForInvoices'"
    g_strVDMSEmailForInvoices = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'UseAutoDeleteWhenFinalisingJobs'"
    g_optUseAutoDeleteWhenFinalisingJobs = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'UseDADCFlipFlop'"
    g_intUseDADCFlipFlop = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'SolaRRArchivePath'"
    g_strSolaRRArchivePath = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'SolaRRRestorePath'"
    g_strSolaRRRestorePath = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'UseSvenskAutoSend'"
    g_intUseSvenskAutoSend = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'SigniantSendPath'"
    g_strSigniantSendPath = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'AsperaClient1SendPath'"
    g_strAsperaClient1SendPath = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'AsperaClient360SendPath'"
    g_strAsperaClient360SendPath = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'TranscodeOverlayFolder'"
    g_strTranscodeOverlayFolder = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'AdministratorEmailAddress'"
    g_strAdministratorEmailAddress = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'AdministratorEmailName'"
    g_strAdministratorEmailName = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'OperationsEmailAddress'"
    g_strOperationsEmailAddress = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'OperationsEmailName'"
    g_strOperationsEmailName = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'BookingsEmailAddress'"
    g_strBookingsEmailAddress = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'BookingsEmailName'"
    g_strBookingsEmailName = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'QuotesEmailAddress'"
    g_strQuotesEmailAddress = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'QuotesEmailName'"
    g_strQuotesEmailName = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'ManagerEmailAddress'"
    g_strManagerEmailAddress = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'CETAAdministratorEmail'"
    g_strCETAAdministratorEmail = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'CetaMailFromEmailDefault'"
    g_strCetaMailFromEmailDefault = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'SprocketsPercentage'"
    g_lngSprocketsPercentage = Val(Format(l_rstSettings("value")))

    l_rstSettings.Filter = "name = 'JPEGextension'"
    g_strJPEGextension = Format(l_rstSettings("value"))

    l_rstSettings.Filter = "name = 'BBC16x9TranscodeWatchfolder'"
    g_strBBC16x9TranscodeWatchFolder = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'BBC4x3TranscodeWatchfolder'"
    g_strBBC4x3TranscodeWatchFolder = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'JCA16x9TranscodeWatchfolder'"
    g_strJCA16x9TranscodeWatchFolder = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'JCA4x3TranscodeWatchfolder'"
    g_strJCA4x3TranscodeWatchFolder = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'AsperaRootDirectory'"
    g_strAsperaRootDirectory = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DADC_XML_File_Requests'"
    g_optDADC_XML_File_Requests = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'VantageOnline'"
    g_optVantageOnline = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'AsperaOnline'"
    g_optAsperaOnline = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'BBCAkamaiFTPaddress'"
    g_strBBCAkamaiFTPaddress = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'BBCAkamaiFTPusername'"
    g_strBBCAkamaiFTPusername = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'BBCAkamaiFTPpassword'"
    g_strBBCAkamaiFTPpassword = Format(l_rstSettings("value"))

    l_rstSettings.Filter = "name = 'BBCAkamaiFTProotdirectory'"
    g_strBBCAkamaiFTProotdirectory = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'BBCMG_Clip_Archive'"
    g_strBBCMG_ClipArchive = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'BBCMG_Proxy_Archive'"
    g_strBBCMG_ProxyArchive = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'BBCMG_Archive'"
    g_strBBCMG_Archive = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'BBCMG_Aspera'"
    g_strBBCMG_Aspera = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'BBCMG_Flashstore'"
    g_strBBCMG_Flashstore = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'BBCMGRestoreLocationBarcode'"
    g_strBBCMG_Restore = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DefaultMainPage'"
    g_strDefaultMainPage = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DefaultHeaderPage'"
    g_strDefaultHeaderPage = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DefaultFooterPage'"
    g_strDefaultFooterPage = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DefaultPortalAddress'"
    g_strDefaultPortalAddress = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'MediaWindowSystemMessage'"
    g_strMediaWindowSystemMessage = Trim(" " & l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DefaultAdminLink1'"
    g_strDefaultAdminLink1 = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DefaultAdminLink2'"
    g_strDefaultAdminLink2 = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DefaultAdminLink3'"
    g_strDefaultAdminLink3 = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DefaultAdminTitle1'"
    g_strDefaultAdminTitle1 = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DefaultAdminTitle2'"
    g_strDefaultAdminTitle2 = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DefaultAdminTitle3'"
    g_strDefaultAdminTitle3 = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DefaultAdminText1'"
    g_strDefaultAdminText1 = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DefaultAdminText2'"
    g_strDefaultAdminText2 = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DefaultAdminText3'"
    g_strDefaultAdminText3 = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'EnforceRigorousLibraryChecks'"
    g_intEnforceRigorousLibraryChecks = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'PrintCopyJobSheets'"
    g_intPrintCopyJobSheets = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'CompulsoryBikeRefs'"
    g_intCompulsoryBikeRefs = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'LocationOfWatchfolders'"
    g_strLocationOfWatchfolders = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'CheckAllBBCJobs'"
    g_intCheckAllBBCJobs = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'LocationOfOmneonXLS'"
    g_strLocationOfOmneonXLS = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'ExcelOrderLocation'"
    g_strExcelOrderLocation = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'LocationOfRapidsLogs'"
    g_strLocationOfRapidsLogs = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'UseSingleCompletionScreenForDubbing'"
    g_optUseSingleCompletionScreenForDubbing = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'UseSingleCompletionScreenForDubbing'"
    g_optUseSingleCompletionScreenForDubbing = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'KeyframeStore'"
    g_strKeyframeStore = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'FlashThumbStore'"
    g_strFlashThumbStore = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'HideAudioChannels'"
    g_optHideAudioChannels = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DontPromptForConfirmationOnCompletionOfDespatches'"
    g_optDontPromptForConfirmationOnCompletionOfDespatches = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'RequireProductOnQuotes'"
    g_optRequireProductOnQuotes = Format(l_rstSettings("value"))
    
    'JCASystemVariables is used to select the global settings that MX1 use. Cannot be changed in the form - set it hard to 1 in the database for MX1.
    l_rstSettings.Filter = "name = 'JCASystemVariables'"
    g_optJCASystemVariables = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DescDeadNotCompulsory'"
    g_optDescDeadNotCompulsory = Format(l_rstSettings("value"))
        
    l_rstSettings.Filter = "name = 'AbortIfAZeroDuration'"
    g_optAbortIfAZeroDuration = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'CostDespatch'"
    g_optCostDespatch = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'JCADeliverylabels'"
    g_optJCADeliverylabels = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'ShowBillToInformationOnDespatch'"
    g_optShowBillToInformationOnDespatch = Format(l_rstSettings("value"))
    
    
    l_rstSettings.Filter = "name = 'DontClearShelfWhenMovingTapes'"
    g_optDontClearShelfWhenMovingTapes = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'OrderByNumbers'"
    g_optOrderByNumbers = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DeadlineTimeNotCompulsory'"
    g_optDeadlineTimeNotCompulsory = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'AllocateBarcodes'"
    g_optAllocateBarcodes = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'AddFakeJobIDToTapes'"
    g_optAddFakeJobIDToTapes = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'CompanyPrefix'"
    g_strCompanyPrefix = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'PrefixLibraryBarcodesWithCompanyPrefix'"
    g_optPrefixLibraryBarcodesWithCompanyPrefix = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DisableUpdateLibraryRecordWithJobDetails'"
    g_optDisableUpdateLibraryRecordWithJobDetails = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'UseFormattedTimeCodesInEvents'"
    g_optUseFormattedTimeCodesInEvents = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'AllowCostingEntryAtAnyStatus'"
    g_optAllowCostingEntryAtAnyStatus = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DisableCostingPopUp'"
    g_optDisableCostingPopUp = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'UseMinimumCharging'"
    g_optUseMinimumCharging = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'BaseRateCard'"
    g_intBaseRateCard = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DontMakeMachineTimeADecimal'"
    g_optDontMakeMachineTimeADecimal = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DontUseDynoBarcodeLabel'"
    g_optDontUseDynoBarcodeLabel = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'UseJCAPostingToAccounts'"
    g_optUseJCAPostingToAccounts = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'UseAudioStandardInLibraryToStoreLanguage'"
    g_optUseAudioStandardInLibraryToStoreLanguage = Format(l_rstSettings("value"))
    
    
    l_rstSettings.Filter = "name = 'LocationOfJellyroll1Folder'"
    g_strLocationOfJellyroll1Folder = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'LocationOfJellyroll2Folder'"
    g_strLocationOfJellyroll2Folder = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'Jellyroll1LibraryID'"
    g_lngJellyroll1LibraryID = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'Jellyroll2LibraryID'"
    g_lngJellyroll2LibraryID = Format(l_rstSettings("value"))
    
   
    l_rstSettings.Filter = "name = 'AsperaServer'"
    g_strAsperaServer = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'FlashServer'"
    g_strFlashServer = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'FlashstorePath'"
    g_strFlashstorePath = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'UseProductRateCards'"
    g_optUseProductRateCards = Format(l_rstSettings("value"))
    
    
    l_rstSettings.Filter = "name = 'AllowCostedJobsWhenNotComplete'"
    g_optAllowCostedJobsWhenNotComplete = Format(l_rstSettings("value"))
    
    
    l_rstSettings.Filter = "name = 'DontForceUnitsChargedOnHireJobs'"
    g_optDontForceUnitsChargedOnHireJobs = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'HideVideoStandardOnJobForm'"
    g_intHideVideoStandardOnJobForm = Format(l_rstSettings("value"))
       
    
    l_rstSettings.Filter = "name = 'PasswordChangeEvery'"
    g_intPasswordChangeEvery = Format(l_rstSettings("value"))
    
       l_rstSettings.Filter = "name = 'PasswordLength'"
    g_intPasswordLength = Format(l_rstSettings("value"))
    
       l_rstSettings.Filter = "name = 'PasswordNumbers'"
    g_intPasswordNumbers = Format(l_rstSettings("value"))
    
       l_rstSettings.Filter = "name = 'PasswordUpperCase'"
    g_intPasswordUpperCase = Format(l_rstSettings("value"))


   l_rstSettings.Filter = "name = 'LockDiscountColumnInCostings'"
    g_intLockDiscountColumnInCostings = Format(l_rstSettings("value"))
 
    
    l_rstSettings.Filter = "name = 'UserIsNotDefaultOurContact'"
    g_optUserIsNotDefaultOurContact = Format(l_rstSettings("value"))
    
    
    l_rstSettings.Filter = "name = 'DontCostScheduledResourcesIfJobTypeIsADub'"
    g_intDontCostScheduledResourcesIfJobTypeIsADub = Format(l_rstSettings("value"))
    
    
    l_rstSettings.Filter = "name = 'AdvancedPurchaseApproveEmail'"
    g_strAdvancedPurchaseApproveEmail = Format(l_rstSettings("value"))
    
    
    l_rstSettings.Filter = "name = 'RequireVideoStandardOnQuote'"
    g_optRequireVideoStandardOnQuote = Format(l_rstSettings("value"))
    
    
    l_rstSettings.Filter = "name = 'HideOverTime'"
    g_optHideOverTime = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'HideDownTime'"
    g_optHideDownTime = Format(l_rstSettings("value"))
    
    
    l_rstSettings.Filter = "name = 'HideStandardLabelsButton'"
    g_optHideStandardLabelsButton = Format(l_rstSettings("value"))
    
    
    l_rstSettings.Filter = "name = 'BookDubsAsConfirmedStatus'"
    g_optBookDubsAsConfirmedStatus = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DefaultInternalMovementLocation'"
    g_strDefaultInternalMovementLocation = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'SMTPServer'"
    g_strSMTPServer = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'SMTPUserName'"
    g_strSMTPUserName = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'SMTPPassword'"
    g_strSMTPPassword = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'SMTPServerAuthorised'"
    g_strSMTPServerAuthorised = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'SMTPUserNameAuthorised'"
    g_strSMTPUserNameAuthorised = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'SMTPPasswordAuthorised'"
    g_strSMTPPasswordAuthorised = Format(l_rstSettings("value"))
    
    
    l_rstSettings.Filter = "name = 'MaximumQuoteDiscount'"
    g_optMaximumQuoteDiscount = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'RequireBudgetCodesBeforeAuthorisation'"
    g_optRequireBudgetCodesBeforeAuthorisation = Format(l_rstSettings("value"))
    
    
    l_rstSettings.Filter = "name = 'ShowTimeColumnInPurchaseOrder'"
    g_optShowTimeColumnInPurchaseOrder = Format(l_rstSettings("value"))
    
    
    l_rstSettings.Filter = "name = 'HideOrderByColumnOnCostingGrid'"
    g_optHideOrderByColumnOnCostingGrid = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'RequireCompletedDubsBeforeCosting'"
    g_optRequireCompletedDubsBeforeCosting = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'HideSoundChannelsOnVTDetailGrid'"
    g_optHideSoundChannelsOnVTDetailGrid = Format(l_rstSettings("value"))
    
    
    'MPC TV online root play path
    l_rstSettings.Filter = "name = 'UseClipID'"
    g_optUseClipID = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'ClipIDCaption'"
    g_strClipIDCaption = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'ClipStorePlayURL'"
    g_strClipStorePlayURL = Format(l_rstSettings("value"))
        
    l_rstSettings.Filter = "name = 'DefaultCopiesToPrintOfInvoice'"
    g_intDefaultCopiesToPrintOfInvoice = Format(l_rstSettings("value"))
    
    
    l_rstSettings.Filter = "name = 'UpperCaseDubbingDetails'"
    g_optUpperCaseDubbingDetails = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'CreateDNotesForExternalDeliveryPurchaseOrders'"
    g_optCreateDNotesForExternalDeliveryPurchaseOrders = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'ApplyDealPriceToActualsRatherThanRateCard'"
    g_optApplyDealPriceToActualsRatherThanRateCard = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'RequireDeliveryMethodsOnHireAndEditJobs'"
    g_optRequireDeliveryMethodsOnHireAndEditJobs = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'DontPromptForNumberOfInvoiceCopies'"
    g_optDontPromptForNumberOfInvoiceCopies = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'NeedAccountNumberToConfirm'"
    g_optNeedAccountNumberToConfirm = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'AutoTickOurContactInJobLists'"
    g_optAutoTickOurContactInJobLists = Format(l_rstSettings("value"))

    l_rstSettings.Filter = "name = 'HideZeroRateCardItemsInManagementReports'"
    g_optHideZeroRateCardItemsInManagementReports = Format(l_rstSettings("value"))

    
    l_rstSettings.Filter = "name = 'AllowNewPurchaseOrdersOnCostedJobs'"
    g_optAllowNewPurchaseOrdersOnCostedJobs = Format(l_rstSettings("value"))

    
    l_rstSettings.Filter = "name = 'HarshCheckingOnDubbingEntry'"
    g_optHarshCheckingOnDubbingEntry = Format(l_rstSettings("value"))

    
    l_rstSettings.Filter = "name = 'DefaultCategoryToAddResource'"
    g_strDefaultCategoryToAddResource = Format(l_rstSettings("value"))

    
    l_rstSettings.Filter = "name = 'CreateDeliveryNotesForJobsWithNoAddress'"
    g_optCreateDeliveryNotesForJobsWithNoAddress = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'HideHireDespatchTab'"
    g_optHideHireDespatchTab = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'RequireOurContactBeforeSaving'"
    g_optRequireOurContactBeforeSaving = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'ShowHorizontalScrollBar'"
    g_optShowHorizontalScrollBar = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'AlwaysHideTimeColumn'"
    g_optHideTimeColumn = Format(l_rstSettings("value")) ' Val(GetData("setting", "value", "name", "DontPromptForInvoiceAddress"))
    
    l_rstSettings.Filter = "name = 'RoundCurrencyDecimals'"
    g_optRoundCurrencyDecimals = Format(l_rstSettings("value")) ' Val(GetData("setting", "value", "name", "DontPromptForInvoiceAddress"))
    
    l_rstSettings.Filter = "name = 'DontPromptForInvoiceAddress'"
    g_optDontPromptForInvoiceAddress = Format(l_rstSettings("value")) ' Val(GetData("setting", "value", "name", "DontPromptForInvoiceAddress"))
    
    l_rstSettings.Filter = "name = 'CurrentVersion'"
    g_strCurrentVersion = Format(l_rstSettings("value")) 'GetData("setting", "value", "name", "CurrentVersion")
    
    l_rstSettings.Filter = "name = 'UploadInvoiceLocation'"
    g_strUploadInvoiceLocation = Format(l_rstSettings("value"))
    'g_strUploadInvoiceLocation = GetData("setting", "value", "name", "UploadInvoiceLocation")
    
    l_rstSettings.Filter = "name = 'SundryMarkup'"
    g_intSundryMarkup = Format(l_rstSettings("value"))
    'g_intSundryMarkup = Val(GetData("setting", "value", "name", "SundryMarkup"))
    
    l_rstSettings.Filter = "name = 'SendToAccountsOnFinalise'"
    g_optSendToAccountsOnFinalise = Format(l_rstSettings("value"))
    
    l_rstSettings.Filter = "name = 'CompleteJobsWhenVTCompleted'"
    g_optCompleteJobsWhenVTCompleted = Format(l_rstSettings("value"))
    'g_optCompleteJobsWhenVTCompleted = Val(GetData("setting", "value", "name", "CompleteJobsWhenVTCompleted"))
    
    l_rstSettings.Filter = "name = 'AllowEditAuthorisedPurchaseOrders'"
    g_optAllowEditAuthorisedPurchaseOrders = Format(l_rstSettings("value"))
    'g_optAllowEditAuthorisedPurchaseOrders = Val(GetData("setting", "value", "name", "AllowEditAuthorisedPurchaseOrders"))
      
    l_rstSettings.Filter = "name = 'UseNewSearchLibraryFields'"
    g_optUseNewSearchLibraryFields = Format(l_rstSettings("value"))
    'g_optUseNewSearchLibraryFields = Val(GetData("setting", "value", "name", "UseNewSearchLibraryFields"))
    
    l_rstSettings.Filter = "name = 'ForceDealPrice'"
    g_optForceDealPrice = Format(l_rstSettings("value"))
    'g_optForceDealPrice = Val(GetData("setting", "value", "name", "ForceDealPrice"))
    
    
    frmLoading.lblStatus.Caption = frmLoading.lblStatus.Caption & "."
    DoEvents

    l_rstSettings.Filter = "name = 'MakeEventsWide'"
    g_optMakeEventsWide = Format(l_rstSettings("value"))
    'g_optMakeEventsWide = Val(GetData("setting", "value", "name", "MakeEventsWide"))
    
    l_rstSettings.Filter = "name = 'DefaultEventSearchInLibrary'"
    g_optDefaultEventSearchInLibrary = Format(l_rstSettings("value"))
    'g_optDefaultEventSearchInLibrary = Val(GetData("setting", "value", "name", "DefaultEventSearchInLibrary"))
    
    
    l_rstSettings.Filter = "name = 'HideVatFieldsInCostings'"
    g_optHideVATFieldsInCostings = Format(l_rstSettings("value"))
    'g_optHideVATFieldsInCostings = Val(GetData("setting", "value", "name", "HideVatFieldsInCostings"))
    
    l_rstSettings.Filter = "name = 'Connection1Name'"
    g_optConnection1Name = Format(l_rstSettings("value"))
    'g_optConnection1Name = GetData("setting", "value", "name", "Connection1Name")
    
    
    l_rstSettings.Filter = "name = 'Connection2Name'"
    g_optConnection2Name = Format(l_rstSettings("value"))
    'g_optConnection2Name = GetData("setting", "value", "name", "Connection2Name")
    
    l_rstSettings.Filter = "name = 'Connection3Name'"
    g_optConnection3Name = Format(l_rstSettings("value"))
    'g_optConnection3Name = GetData("setting", "value", "name", "Connection3Name")
    
    l_rstSettings.Filter = "name = 'Connection4Name'"
    g_optConnection4Name = Format(l_rstSettings("value"))
    'g_optConnection4Name = GetData("setting", "value", "name", "Connection4Name")
    
    frmLoading.lblStatus.Caption = frmLoading.lblStatus.Caption & "."
    DoEvents

    
    '******************************************
    
    l_rstSettings.Filter = "name = 'CostSchedule'"
    g_optCostSchedule = Format(l_rstSettings("value"))
    'g_optCostSchedule = GetFlag(GetData("setting", "value", "name", "CostSchedule"))
    
    l_rstSettings.Filter = "name = 'CostDubbing'"
    g_optCostDubbing = Format(l_rstSettings("value"))
    'g_optCostDubbing = GetFlag(GetData("setting", "value", "name", "CostDubbing"))
    
    l_rstSettings.Filter = "name = 'UseTitleInMaster'"
    g_optUseTitleInMaster = Format(l_rstSettings("value"))
    'g_optUseTitleInMaster = GetFlag(GetData("setting", "value", "name", "UseTitleInMaster"))
    
    l_rstSettings.Filter = "name = 'UseTitleInCopy'"
    g_optUseTitleInCopy = Format(l_rstSettings("value"))
    'g_optUseTitleInCopy = GetFlag(GetData("setting", "value", "name", "UseTitleInCopy"))
    
    frmLoading.lblStatus.Caption = frmLoading.lblStatus.Caption & "."
    DoEvents
    
    
    l_rstSettings.Filter = "name = 'UseSpecialDubbingComboOptions'"
    g_optUseSpecialDubbingComboOptions = Format(l_rstSettings("value"))
    'g_optUseSpecialDubbingComboOptions = GetFlag(0 - GetData("setting", "value", "name", "UseSpecialDubbingComboOptions"))
    
    l_rstSettings.Filter = "name = 'RequireProductBeforeSaving'"
    g_optRequireProductBeforeSaving = Format(l_rstSettings("value"))
    'g_optRequireProductBeforeSaving = GetFlag(GetData("setting", "value", "name", "RequireProductBeforeSaving"))
    
    frmLoading.lblStatus.Caption = frmLoading.lblStatus.Caption & "."
    DoEvents

    
    l_rstSettings.Filter = "name = 'AddDurationToRateCode'"
    g_optAddDurationToRateCode = Format(l_rstSettings("value"))
    'g_optAddDurationToRateCode = Val(GetData("setting", "value", "name", "AddDurationToRateCode"))
        
    l_rstSettings.Filter = "name = 'AlwaysUseRateCardDescriptions'"
    g_optAlwaysUseRateCardDescriptions = Format(l_rstSettings("value"))
    'g_optAlwaysUseRateCardDescriptions = Val(GetData("setting", "value", "name", "AlwaysUseRateCardDescriptions"))
    
    '******************************************
    
    frmLoading.lblStatus.Caption = frmLoading.lblStatus.Caption & "."
    DoEvents
    
    l_rstSettings.Filter = "name = 'SearchLibraryDefaultField'"
    g_optSearchLibraryDefaultField = Format(l_rstSettings("value"))
    'g_optSearchLibraryDefaultField = GetData("setting", "value", "name", "SearchLibraryDefaultField")
    
    l_rstSettings.Filter = "name = 'UseJobDetailsWhenAddingJobDetailLinesToDespatch'"
    g_optUseJobDetailsWhenAddingJobDetailLinesToDespatch = Format(l_rstSettings("value"))
    'g_optUseJobDetailsWhenAddingJobDetailLinesToDespatch = GetFlag(Val(GetData("setting", "value", "name", "UseJobDetailsWhenAddingJobDetailLinesToDespatch")))
    
    
    l_rstSettings.Filter = "name = 'PromptForLateDespatchReason'"
    g_optPromptForLateDespatchReason = Format(l_rstSettings("value"))
    'g_optPromptForLateDespatchReason = GetFlag(GetData("setting", "value", "name", "PromptForLateDespatchReason"))
    
    l_rstSettings.Filter = "name = 'ShowCompanyInsuranceOutDate'"
    g_optShowCompanyInsuranceOutDate = Format(l_rstSettings("value"))
    'g_optShowCompanyInsuranceOutDate = GetFlag(GetData("setting", "value", "name", "ShowCompanyInsuranceOutDate"))
    
    frmLoading.lblStatus.Caption = frmLoading.lblStatus.Caption & "."
    DoEvents

    l_rstSettings.Filter = "name = 'LockSystem'"
    g_optLockSystem = Format(l_rstSettings("value"))
    'g_optLockSystem = GetFlag(GetData("setting", "value", "name", "LockSystem"))

    l_rstSettings.Filter = "name = 'CloseSystemDown'"
    g_optCloseSystemDown = Format(l_rstSettings("value"))
    'g_optCloseSystemDown = GetFlag(GetData("setting", "value", "name", "CloseSystemDown"))
    
    l_rstSettings.Filter = "name = 'StopFFMPGServices'"
    g_optStopFFMPGServices = Format(l_rstSettings("value"))
    'g_optCloseSystemDown = GetFlag(GetData("setting", "value", "name", "CloseSystemDown"))
   
    l_rstSettings.Filter = "name = 'UseMasterDetailOnInvoice'"
    g_optUseMasterDetailOnInvoice = Format(l_rstSettings("value"))
    'g_optUseMasterDetailOnInvoice = GetFlag(GetData("setting", "value", "name", "UseMasterDetailOnInvoice"))
        
    frmLoading.lblStatus.Caption = frmLoading.lblStatus.Caption & "."
    DoEvents
    
    
    l_rstSettings.Filter = "name = 'UseAdditionalDescriptionsWhenCosting'"
    g_optUseAdditionalDescriptionsWhenCosting = Format(l_rstSettings("value"))
    'g_optUseAdditionalDescriptionsWhenCosting = GetFlag(GetData("setting", "value", "name", "UseAdditionalDescriptionsWhenCosting"))
    
    l_rstSettings.Filter = "name = 'DeductPreSetDiscountsFromUnitPrice'"
    g_optDeductPreSetDiscountsFromUnitPrice = Format(l_rstSettings("value"))
    'g_optDeductPreSetDiscountsFromUnitPrice = GetFlag(GetData("setting", "value", "name", "DeductPreSetDiscountsFromUnitPrice"))
    
    
    l_rstSettings.Filter = "name = 'PromptForDetailedCompletion'"
    g_optPromptForDetailedCompletion = Format(l_rstSettings("value"))
    'g_optPromptForDetailedCompletion = GetFlag(GetData("setting", "value", "name", "PromptForDetailedCompletion"))
    
    
    
    l_rstSettings.Filter = "name = 'AppendInitialsAndDateToNotes'"
    g_optAppendInitialsAndDateToNotes = Format(l_rstSettings("value"))
    'g_optAppendInitialsAndDateToNotes = GetFlag(GetData("setting", "value", "name", "AppendInitialsAndDateToNotes"))
    
    frmLoading.lblStatus.Caption = frmLoading.lblStatus.Caption & "."
    DoEvents

    
    l_rstSettings.Filter = "name = 'ShowAdditionalEventFields'"
    g_optShowAdditionalEventFields = Format(l_rstSettings("value"))
    'g_optShowAdditionalEventFields = GetFlag(GetData("setting", "value", "name", "ShowAdditionalEventFields"))
    
    l_rstSettings.Filter = "name = 'PDFLocation'"
    g_strPDFLocation = Format(l_rstSettings("value"))
    'g_strPDFLocation = GetData("setting", "value", "name", "PDFLocation")
    
    l_rstSettings.Filter = "name = 'FilePDFLocation'"
    g_strFilePDFLocation = Format(l_rstSettings("value"))
    'g_strPDFLocation = GetData("setting", "value", "name", "FilePDFLocation")
    
    l_rstSettings.Filter = "name = 'TapePDFLocation'"
    g_strTapePDFLocation = Format(l_rstSettings("value"))
    'g_strPDFLocation = GetData("setting", "value", "name", "FilePDFLocation")
    
    l_rstSettings.Filter = "name = 'CreatePDFFilesOfTechReviews'"
    g_optCreatePDFFilesOfTechReviews = Format(l_rstSettings("value"))
    'g_optCreatePDFFilesOfTechReviews = GetFlag(GetData("setting", "value", "name", "CreatePDFFilesOfTechReviews"))
    
    
    l_rstSettings.Filter = "name = 'NumberOfDespatchNoteCopies'"
    g_intNumberOfDespatchNoteCopies = Format(l_rstSettings("value"))
    'g_intNumberOfDespatchNoteCopies = Val(GetData("setting", "value", "name", "NumberOfDespatchNoteCopies"))
    
    frmLoading.lblStatus.Caption = frmLoading.lblStatus.Caption & "."
    DoEvents
    
    
    'fonts for the schedule form
    l_rstSettings.Filter = "name = 'ResourceBookingCaption1FontName'"
    g_strResourceBookingCaption1FontName = Format(l_rstSettings("value"))
    'g_strResourceBookingCaption1FontName = GetData("setting", "value", "name", "ResourceBookingCaption1FontName")
    
    l_rstSettings.Filter = "name = 'ResourceBookingCaption2FontName'"
    g_strResourceBookingCaption2FontName = Format(l_rstSettings("value"))
    'g_strResourceBookingCaption2FontName = GetData("setting", "value", "name", "ResourceBookingCaption2FontName")
    
    
    l_rstSettings.Filter = "name = 'ResourceBookingCaption3FontName'"
    g_strResourceBookingCaption3FontName = Format(l_rstSettings("value"))
    'g_strResourceBookingCaption3FontName = GetData("setting", "value", "name", "ResourceBookingCaption3FontName")
    
    
    l_rstSettings.Filter = "name = 'ResourceBookingCaption1FontSize'"
    g_intResourceBookingCaption1FontSize = Format(l_rstSettings("value"))
    'g_intResourceBookingCaption1FontSize = Val(GetData("setting", "value", "name", "ResourceBookingCaption1FontSize"))
    
    l_rstSettings.Filter = "name = 'ResourceBookingCaption2FontSize'"
    g_intResourceBookingCaption2FontSize = Format(l_rstSettings("value"))
    'g_intResourceBookingCaption2FontSize = Val(GetData("setting", "value", "name", "ResourceBookingCaption2FontSize"))
    
    frmLoading.lblStatus.Caption = frmLoading.lblStatus.Caption & "."
    DoEvents
    
    
    l_rstSettings.Filter = "name = 'ResourceBookingCaption3FontSize'"
    g_intResourceBookingCaption3FontSize = Format(l_rstSettings("value"))
    'g_intResourceBookingCaption3FontSize = Val(GetData("setting", "value", "name", "ResourceBookingCaption3FontSize"))
    

    l_rstSettings.Filter = "name = 'ResourceBookingCaption1ForeColour'"
    g_lngResourceBookingCaption1ForeColour = Format(l_rstSettings("value"))
    'g_lngResourceBookingCaption1ForeColour = Val(GetData("setting", "value", "name", "ResourceBookingCaption1ForeColour"))
    
    l_rstSettings.Filter = "name = 'ResourceBookingCaption2ForeColour'"
    g_lngResourceBookingCaption2ForeColour = Format(l_rstSettings("value"))
    'g_lngResourceBookingCaption2ForeColour = Val(GetData("setting", "value", "name", "ResourceBookingCaption2ForeColour"))
    
    l_rstSettings.Filter = "name = 'ResourceBookingCaption3ForeColour'"
    g_lngResourceBookingCaption3ForeColour = Format(l_rstSettings("value"))
    'g_lngResourceBookingCaption3ForeColour = Val(GetData("setting", "value", "name", "ResourceBookingCaption3ForeColour"))
    
    frmLoading.lblStatus.Caption = frmLoading.lblStatus.Caption & "."
    DoEvents

    
    l_rstSettings.Filter = "name = 'WebBrowserHomeDirectory'"
    g_strWebBrowserHomeDirectory = Format(l_rstSettings("value"))
    'g_strWebBrowserHomeDirectory = GetData("setting", "value", "name", "WebBrowserHomeDirectory")
    
    l_rstSettings.Filter = "name = 'WebBrowserResourceInfoPage'"
    g_strWebBrowserResourceInfoPage = Format(l_rstSettings("value"))
    'g_strWebBrowserResourceInfoPage = GetData("setting", "value", "name", "WebBrowserResourceInfoPage")
    
    l_rstSettings.Filter = "name = 'RequireActualsBeforeCosting'"
    g_optRequireActualsBeforeCosting = Format(l_rstSettings("value"))
    'g_optRequireActualsBeforeCosting = GetFlag(GetData("setting", "value", "name", "RequireActualsBeforeCosting"))
    
    frmLoading.lblStatus.Caption = frmLoading.lblStatus.Caption & "."
    DoEvents
    
    l_rstSettings.Filter = "name = 'DrawResourceBookingShadows'"
    g_optDrawResourceBookingShadows = Format(l_rstSettings("value"))
    'g_optDrawResourceBookingShadows = GetFlag(GetData("setting", "value", "name", "DrawResourceBookingShadows"))
    
   
    l_rstSettings.Filter = "name = 'AnimateSchedule'"
    g_optAnimateSchedule = Format(l_rstSettings("value"))
    'g_optAnimateSchedule = GetFlag(GetData("setting", "value", "name", "AnimateSchedule"))
    
    l_rstSettings.Filter = "name = 'AnimateLoop'"
    g_optAnimateLoop = Format(l_rstSettings("value"))
    'g_optAnimateLoop = Val(GetData("setting", "value", "name", "AnimateLoop"))
    
    
    l_rstSettings.Filter = "name = 'DefaultJobType'"
    g_strDefaultJobType = Format(l_rstSettings("value"))
    'g_strDefaultJobType = GetData("setting", "value", "name", "DefaultJobType")
    
    l_rstSettings.Filter = "name = 'DefaultJobAllocation'"
    g_strDefaultJobAllocation = Format(l_rstSettings("value"))
    'g_strDefaultJobAllocation = GetData("setting", "value", "name", "DefaultJobAllocation")
    
    frmLoading.lblStatus.Caption = frmLoading.lblStatus.Caption & "."
    DoEvents
    
    
    l_rstSettings.Filter = "name = 'DefaultTimeWhenZero'"
    g_intDefaultTimeWhenZero = Format(l_rstSettings("value"))
    'g_intDefaultTimeWhenZero = Val(GetData("setting", "value", "name", "DefaultTimeWhenZero"))
    
    l_rstSettings.Filter = "name = 'ShowResourceLocation'"
    g_optShowResourceLocation = Format(l_rstSettings("value"))
    'g_optShowResourceLocation = Val(GetData("setting", "value", "name", "ShowResourceLocation"))
    
    
    l_rstSettings.Filter = "name = 'WhenToAllocate'"
    g_strWhenToAllocate = Format(l_rstSettings("value"))
    'g_strWhenToAllocate = GetData("setting", "value", "name", "WhenToAllocate")
    
    l_rstSettings.Filter = "name = 'ShowSerialNumberInSchedule'"
    g_optShowSerialNumberInSchedule = Format(l_rstSettings("value"))
    'g_optShowSerialNumberInSchedule = Val(GetData("setting", "value", "name", "ShowSerialNumberInSchedule"))
        
    frmLoading.lblStatus.Caption = frmLoading.lblStatus.Caption & "."
    DoEvents
    
    
    l_rstSettings.Filter = "name = 'DefaultTabInJobDetails'"
    g_intDefaultTab = Format(l_rstSettings("value"))
    'g_intDefaultTab = Val(GetData("setting", "value", "name", "DefaultTabInJobDetails"))
    
    
    l_rstSettings.Close
    Set l_rstSettings = Nothing
    
    Set l_conSettings = Nothing
    
    'MDIForm1.Caption = l_strTempCaption
    Screen.MousePointer = vbDefault
    
    DoEvents
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:

    If Err.Number = 13 Then
        Resume Next
    
    End If

    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub SaveSettingsToDatabase()
    
    Dim i As Integer
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/savesettings") Then
        Exit Sub
    End If

    g_optDoOfflineCFMRequests = frmSettings.chkDoOfflineCFMRequests.Value
    SaveCETASetting "DoOfflineCFMRequests", g_optDoOfflineCFMRequests
    
    g_optUseAutoDeleteWhenFinalisingJobs = frmSettings.chkUseAutoDeleteWhenFinalisingJobs.Value
    SaveCETASetting "UseAutoDeleteWhenFinalisingJobs", g_optUseAutoDeleteWhenFinalisingJobs
    
    g_strVDMSEmailForInvoices = CStr(frmSettings.txtVDMSEmailForInvoice.Text)
    SaveCETASetting "VDMSEmailForInvoices", g_strVDMSEmailForInvoices
    
    g_strSigniantSendPath = CStr(frmSettings.txtSigniantSendPath.Text)
    SaveCETASetting "SigniantSendPath", g_strSigniantSendPath
    
    g_strTranscodeOverlayFolder = CStr(frmSettings.txtTranscodeOverlayFolder.Text)
    SaveCETASetting "TranscodeOverlayFolder", g_strTranscodeOverlayFolder
    
    g_strCETAAdministratorEmail = CStr(frmSettings.txtCETAAdministratorEmail.Text)
    SaveCETASetting "CETAAdministratorEmail", g_strCETAAdministratorEmail
    
    g_strCetaMailFromEmailDefault = CStr(frmSettings.txtCetaMailFromEmailDefault.Text)
    SaveCETASetting "CetaMailFromEmailDefault", g_strCetaMailFromEmailDefault
    
    g_strAdministratorEmailAddress = CStr(frmSettings.txtAdministratorEmailAddress.Text)
    SaveCETASetting "AdministratorEmailAddress", g_strAdministratorEmailAddress
    
    g_strAdministratorEmailName = CStr(frmSettings.txtAdministratorEmailName.Text)
    SaveCETASetting "AdministratorEmailName", g_strAdministratorEmailName
    
    g_strOperationsEmailAddress = CStr(frmSettings.txtOperationsEmailAddress.Text)
    SaveCETASetting "OperationsEmailAddress", g_strOperationsEmailAddress
    
    g_strOperationsEmailName = CStr(frmSettings.txtOperationsEmailName.Text)
    SaveCETASetting "OperationsEmailName", g_strOperationsEmailName
    
    g_strBookingsEmailAddress = CStr(frmSettings.txtBookingsEmailAddress.Text)
    SaveCETASetting "BookingsEmailAddress", g_strBookingsEmailAddress
    
    g_strBookingsEmailName = CStr(frmSettings.txtBookingsEmailName.Text)
    SaveCETASetting "BookingsEmailName", g_strBookingsEmailName
    
    g_strQuotesEmailAddress = CStr(frmSettings.txtQuotesEmailAddress.Text)
    SaveCETASetting "QuotesEmailAddress", g_strQuotesEmailAddress
    
    g_strQuotesEmailName = CStr(frmSettings.txtQuotesEmailName.Text)
    SaveCETASetting "QuotesEmailName", g_strQuotesEmailName
    
    g_strManagerEmailAddress = CStr(frmSettings.txtManagerEmailAddress.Text)
    SaveCETASetting "ManagerEmailAddress", g_strManagerEmailAddress
    
    g_lngSprocketsPercentage = Int(Val(frmSettings.txtSprocketsPercentage.Text))
    SaveCETASetting "SprocketsPercentage", g_lngSprocketsPercentage
    
    g_strJPEGextension = frmSettings.txtJPEGextension.Text
    SaveCETASetting "JPEGextension", g_strJPEGextension
    
    g_strBBC16x9TranscodeWatchFolder = frmSettings.txtBBC16x9TranscodeWatchfolder.Text
    SaveCETASetting "BBC16x9TranscodeWatchfolder", g_strBBC16x9TranscodeWatchFolder
    
    g_strBBC4x3TranscodeWatchFolder = frmSettings.txtBBC4x3TranscodeWatchfolder.Text
    SaveCETASetting "BBC4x3TranscodeWatchfolder", g_strBBC4x3TranscodeWatchFolder
    
    g_strJCA16x9TranscodeWatchFolder = frmSettings.txtJCA16x9TranscodeWatchfolder.Text
    SaveCETASetting "JCA16x9TranscodeWatchfolder", g_strJCA16x9TranscodeWatchFolder
    
    g_strJCA4x3TranscodeWatchFolder = frmSettings.txtJCA4x3TranscodeWatchfolder.Text
    SaveCETASetting "JCA4x3TranscodeWatchfolder", g_strJCA4x3TranscodeWatchFolder
    
    g_strAsperaRootDirectory = frmSettings.txtAsperaRootDirectory.Text
    SaveCETASetting "AsperaRootDirectory", g_strAsperaRootDirectory
    
    g_strBBCAkamaiFTPaddress = frmSettings.txtBBCAkamaiFTPaddress.Text
    SaveCETASetting "BBCAkamaiFTPaddress", g_strBBCAkamaiFTPaddress
    
    g_strBBCAkamaiFTPusername = frmSettings.txtBBCAkamaiFTPusername.Text
    SaveCETASetting "BBCAkamaiFTPusername", g_strBBCAkamaiFTPusername
    
    g_strBBCAkamaiFTPpassword = frmSettings.txtBBCAkamaiFTPpassword.Text
    SaveCETASetting "BBCAkamaiFTPpassword", g_strBBCAkamaiFTPpassword

    g_strBBCMG_Archive = frmSettings.txtBBCMG_Archive.Text
    SaveCETASetting "BBCMG_Archive", g_strBBCMG_Archive
    
    g_strBBCMG_Restore = frmSettings.txtBBCMG_Restore.Text
    SaveCETASetting "BBCMGRestoreLocationBarcode", g_strBBCMG_Restore
    
    g_strBBCMG_ClipArchive = frmSettings.txtBBCMG_ClipArchive.Text
    SaveCETASetting "BBCMG_Clip_Archive", g_strBBCMG_ClipArchive
    
    g_strBBCMG_ProxyArchive = frmSettings.txtBBCMG_ProxyArchive.Text
    SaveCETASetting "BBCMG_Proxy_Archive", g_strBBCMG_ProxyArchive
    
    g_strBBCMG_Aspera = frmSettings.txtBBCMG_Aspera.Text
    SaveCETASetting "BBCMG_Aspera", g_strBBCMG_Aspera
    
    g_strBBCMG_Flashstore = frmSettings.txtBBCMG_Flashstore.Text
    SaveCETASetting "BBCMG_Flashstore", g_strBBCMG_Flashstore
    
    g_strBBCAkamaiFTProotdirectory = frmSettings.txtBBCAkamaiFTProotdirectory.Text
    SaveCETASetting "BBCAkamaiFTProotdirectory", g_strBBCAkamaiFTProotdirectory
    
    g_strDefaultMainPage = frmSettings.txtDefaultMainPage.Text
    SaveCETASetting "DefaultMainPage", g_strDefaultMainPage
    
    g_strDefaultHeaderPage = frmSettings.txtDefaultHeaderPage.Text
    SaveCETASetting "DefaultHeaderPage", g_strDefaultHeaderPage
    
    g_strDefaultFooterPage = frmSettings.txtDefaultFooterPage.Text
    SaveCETASetting "DefaultFooterPage", g_strDefaultFooterPage
    
    g_strDefaultPortalAddress = frmSettings.txtDefaultPortalAddress.Text
    SaveCETASetting "DefaultPortalAddress", g_strDefaultPortalAddress
    
    g_strMediaWindowSystemMessage = frmSettings.txtMediaWindowSystemMessage.Text
    SaveCETASetting "MediaWindowSystemMessage", g_strMediaWindowSystemMessage
    
    g_strDefaultAdminLink1 = frmSettings.txtAdminLink(1).Text
    SaveCETASetting "DefaultAdminLink1", g_strDefaultAdminLink1
    
    g_strDefaultAdminLink2 = frmSettings.txtAdminLink(2).Text
    SaveCETASetting "DefaultAdminLink2", g_strDefaultAdminLink2
    
    g_strDefaultAdminLink3 = frmSettings.txtAdminLink(3).Text
    SaveCETASetting "DefaultAdminLink3", g_strDefaultAdminLink3
    
    g_strDefaultAdminTitle1 = frmSettings.txtAdminTitle(1).Text
    SaveCETASetting "DefaultAdminTitle1", g_strDefaultAdminTitle1
    
    g_strDefaultAdminTitle2 = frmSettings.txtAdminTitle(2).Text
    SaveCETASetting "DefaultAdminTitle2", g_strDefaultAdminTitle2
    
    g_strDefaultAdminTitle3 = frmSettings.txtAdminTitle(3).Text
    SaveCETASetting "DefaultAdminTitle3", g_strDefaultAdminTitle3
    
    g_strDefaultAdminText1 = frmSettings.txtAdminText(1).Text
    SaveCETASetting "DefaultAdminText1", g_strDefaultAdminText1
    
    g_strDefaultAdminText2 = frmSettings.txtAdminText(2).Text
    SaveCETASetting "DefaultAdminText2", g_strDefaultAdminText2
    
    g_strDefaultAdminText3 = frmSettings.txtAdminText(3).Text
    SaveCETASetting "DefaultAdmintext3", g_strDefaultAdminText3
    
    g_intEnforceRigorousLibraryChecks = frmSettings.chkEnforceRigorousLibraryChecks.Value
    SaveCETASetting "EnforceRigorousLibraryChecks", g_intEnforceRigorousLibraryChecks
    
    g_intPrintCopyJobSheets = frmSettings.chkPrintCopyJobSheets.Value
    SaveCETASetting "PrintCopyJobSheets", g_intPrintCopyJobSheets
    
    g_strLocationOfWatchfolders = frmSettings.txtLocationOfWatchfolders.Text
    SaveCETASetting "LocationOfWatchfolders", g_strLocationOfWatchfolders
    
    g_intCompulsoryBikeRefs = frmSettings.chkCompulsoryBikeRefs.Value
    SaveCETASetting "CompulsoryBikeRefs", g_intCompulsoryBikeRefs
    
    g_intCheckAllBBCJobs = frmSettings.chkCheckAllBBCJobs.Value
    SaveCETASetting "CheckAllBBCJobs", g_intCheckAllBBCJobs
    
    g_strLocationOfOmneonXLS = frmSettings.txtLocationOfOmneonXLS.Text
    SaveCETASetting "LocationOfOmneonXLS", g_strLocationOfOmneonXLS
       
    g_strExcelOrderLocation = frmSettings.txtExcelOrderLocation.Text
    SaveCETASetting "ExcelOrderLocation", g_strExcelOrderLocation
        
    g_optAbortIfAZeroDuration = frmSettings.chkAbortIfAZeroDuration.Value
    SaveCETASetting "AbortIfAZeroDuration", g_optAbortIfAZeroDuration

    g_strKeyframeStore = frmSettings.txtKeyframeStore.Text
    SaveCETASetting "KeyframeStore", g_strKeyframeStore
    
    g_strFlashThumbStore = frmSettings.txtFlashThumbStore.Text
    SaveCETASetting "FlashThumbStore", g_strFlashThumbStore
    
    g_strLocationOfRapidsLogs = frmSettings.txtLocationOfRapidsLogs.Text
    SaveCETASetting "LocationOfRapidsLogs", g_strLocationOfRapidsLogs
    
    g_optUseSingleCompletionScreenForDubbing = frmSettings.chkUseSingleCompletionScreenForDubbing.Value
    SaveCETASetting "UseSingleCompletionScreenForDubbing", g_optUseSingleCompletionScreenForDubbing
    
    
    g_strRateCardType = frmSettings.txtRateCardType.Text
    SaveCETASetting "RateCardType", g_strRateCardType
    
    
    g_optHideAudioChannels = frmSettings.chkHideAudioChannels.Value
    SaveCETASetting "HideAudioChannels", g_optHideAudioChannels
    
    g_optDontPromptForConfirmationOnCompletionOfDespatches = frmSettings.chkDontPromptForConfirmationOnCompletionOfDespatches.Value
    SaveCETASetting "DontPromptForConfirmationOnCompletionOfDespatches", g_optDontPromptForConfirmationOnCompletionOfDespatches
    
    g_optRequireProductOnQuotes = frmSettings.chkRequireProductOnQuotes.Value
    SaveCETASetting "RequireProductOnQuotes", g_optRequireProductOnQuotes
    
    
    g_optCostDespatch = frmSettings.chkCostDespatch.Value
    SaveCETASetting "CostDespatch", g_optCostDespatch
    
    g_optJCADeliverylabels = frmSettings.chkJCADeliverylabels.Value
    SaveCETASetting "JCADeliverylabels", g_optJCADeliverylabels

    g_optDescDeadNotCompulsory = frmSettings.chkDescDeadNotCompulsory.Value
    SaveCETASetting "DescDeadNotCompulsory", g_optDescDeadNotCompulsory

    g_optShowBillToInformationOnDespatch = frmSettings.chkShowBillToInformationOnDespatch.Value
    SaveCETASetting "ShowBillToInformationOnDespatch", g_optShowBillToInformationOnDespatch
    
    
    g_optDontClearShelfWhenMovingTapes = frmSettings.chkDontClearShelfWhenMovingTapes.Value
    SaveCETASetting "DontClearShelfWhenMovingTapes", g_optDontClearShelfWhenMovingTapes
    
    
    g_optOrderByNumbers = frmSettings.chkOrderByNumbers.Value
    SaveCETASetting "OrderByNumbers", g_optOrderByNumbers
    
    g_optDeadlineTimeNotCompulsory = frmSettings.chkDeadlineTimeNotCompulsory.Value
    SaveCETASetting "DeadlineTimeNotCompulsory", g_optDeadlineTimeNotCompulsory

    g_optAllocateBarcodes = frmSettings.chkAllocateBarcodes.Value
    SaveCETASetting "AllocateBarcodes", g_optAllocateBarcodes
    
    
    g_optAddFakeJobIDToTapes = frmSettings.chkAddFakeJobIDToTapes.Value
    SaveCETASetting "AddFakeJobIDToTapes", g_optAddFakeJobIDToTapes
    
    g_strCompanyPrefix = frmSettings.txtCompanyPrefix.Text
    SaveCETASetting "CompanyPrefix", g_strCompanyPrefix
    
    g_optPrefixLibraryBarcodesWithCompanyPrefix = frmSettings.chkPrefixLibraryBarcodesWithCompanyPrefix.Value
    SaveCETASetting "PrefixLibraryBarcodesWithCompanyPrefix", g_optPrefixLibraryBarcodesWithCompanyPrefix
    
    g_optDisableUpdateLibraryRecordWithJobDetails = frmSettings.chkDisableUpdateLibraryRecordWithJobDetails.Value
    SaveCETASetting "DisableUpdateLibraryRecordWithJobDetails", g_optDisableUpdateLibraryRecordWithJobDetails
    
    g_optUseFormattedTimeCodesInEvents = frmSettings.chkUseFormattedTimeCodesInEvents.Value
    SaveCETASetting "UseFormattedTimeCodesInEvents", g_optUseFormattedTimeCodesInEvents
    
    g_optAllowCostingEntryAtAnyStatus = frmSettings.chkAllowCostingEntryAtAnyStatus.Value
    SaveCETASetting "AllowCostingEntryAtAnyStatus", g_optAllowCostingEntryAtAnyStatus
    
    g_optDisableCostingPopUp = frmSettings.chkDisableCostingPopUp.Value
    SaveCETASetting "DisableCostingPopUp", g_optDisableCostingPopUp

    g_optUseMinimumCharging = frmSettings.chkUseMinimumCharging.Value
    SaveCETASetting "UseMinimumCharging", g_optUseMinimumCharging
    
    g_intBaseRateCard = Val(frmSettings.txtBaseRateCard.Text)
    If g_intBaseRateCard = 0 Then g_intBaseRateCard = 1
    SaveCETASetting "BaseRateCard", g_intBaseRateCard

    g_optDontMakeMachineTimeADecimal = frmSettings.chkDontMakeMachineTimeADecimal.Value
    SaveCETASetting "DontMakeMachineTimeADecimal", g_optDontMakeMachineTimeADecimal


    g_optDontUseDynoBarcodeLabel = frmSettings.chkDontUseDynoBarcodeLabel.Value
    SaveCETASetting "DontUseDynoBarcodeLabel", g_optDontUseDynoBarcodeLabel
    
    g_optUseJCAPostingToAccounts = frmSettings.chkUseJCAPostingToAccounts.Value
    SaveCETASetting "UseJCAPostingToAccounts", g_optUseJCAPostingToAccounts
    
    g_optUseAudioStandardInLibraryToStoreLanguage = frmSettings.chkUseAudioStandardInLibraryToStoreLanguage.Value
    SaveCETASetting "UseAudioStandardInLibraryToStoreLanguage", g_optUseAudioStandardInLibraryToStoreLanguage
    
    g_strScreenGrabStore = frmSettings.txtScreenGrabStore.Text
    SaveCETASetting "ScreenGrabStore", g_strScreenGrabStore
    
    g_strAsperaServer = frmSettings.txtAsperaServer.Text
    SaveCETASetting "AsperaServer", g_strAsperaServer
    
    g_optVantageOnline = frmSettings.chkVantageOnline.Value
    SaveCETASetting "VantageOnline", g_optVantageOnline
    
    g_optAsperaOnline = frmSettings.chkAsperaOnline.Value
    SaveCETASetting "AsperaOnline", g_optAsperaOnline
    
    g_strFlashstorePath = frmSettings.txtFlashstorePath.Text
    SaveCETASetting "FlashstorePath", g_strFlashstorePath
    
    g_optUseProductRateCards = Val(frmSettings.chkUseProductRateCards.Value)
    SaveCETASetting "UseProductRateCards", g_optUseProductRateCards
    
    
    g_optAllowCostedJobsWhenNotComplete = Val(frmSettings.chkAllowCostedJobsWhenNotComplete.Value)
    SaveCETASetting "AllowCostedJobsWhenNotComplete", g_optAllowCostedJobsWhenNotComplete
    
    g_optDontForceUnitsChargedOnHireJobs = Val(frmSettings.chkDontForceUnitsChargedOnHireJobs.Value)
    SaveCETASetting "DontForceUnitsChargedOnHireJobs", g_optDontForceUnitsChargedOnHireJobs
    
    g_intHideVideoStandardOnJobForm = Val(frmSettings.chkHideVideoStandardOnJobForm.Value)
    SaveCETASetting "HideVideoStandardOnJobForm", g_intHideVideoStandardOnJobForm
    
    g_intPasswordChangeEvery = CStr(frmSettings.txtPasswordChangeDays.Text)
    SaveCETASetting "PasswordChangeEvery", g_intPasswordChangeEvery
        
    g_intPasswordLength = CStr(frmSettings.txtPasswordLength.Text)
    SaveCETASetting "PasswordLength", g_intPasswordLength
        
    g_intPasswordNumbers = CStr(frmSettings.txtPasswordNumbers.Text)
    SaveCETASetting "PasswordNumbers", g_intPasswordNumbers
        
    g_intPasswordUpperCase = CStr(frmSettings.txtPasswordUppercase.Text)
    SaveCETASetting "PasswordUpperCase", g_intPasswordUpperCase
        
    g_intLockDiscountColumnInCostings = CStr(frmSettings.chkLockDiscountColumnInCostings.Value)
    SaveCETASetting "LockDiscountColumnInCostings", g_intLockDiscountColumnInCostings
    
    
    g_optUserIsNotDefaultOurContact = CStr(frmSettings.chkUserIsNotDefaultOurContact.Value)
    SaveCETASetting "UserIsNotDefaultOurContact", g_optUserIsNotDefaultOurContact
    
    
    g_intDontCostScheduledResourcesIfJobTypeIsADub = CStr(frmSettings.chkDontCostScheduledResourcesIfJobTypeIsADub.Value)
    SaveCETASetting "DontCostScheduledResourcesIfJobTypeIsADub", g_intDontCostScheduledResourcesIfJobTypeIsADub
    
    
    g_strAdvancedPurchaseApproveEmail = CStr(frmSettings.txtAdvancedPurchaseApproveEmail.Text)
    SaveCETASetting "AdvancedPurchaseApproveEmail", g_strAdvancedPurchaseApproveEmail
    
    
    
    g_optRequireVideoStandardOnQuote = Val(frmSettings.chkRequireVideoStandardOnQuote.Value)
    SaveCETASetting "RequireVideoStandardOnQuote", g_optRequireVideoStandardOnQuote

    
    
    g_optHideStandardLabelsButton = Val(frmSettings.chkHideStandardLabelsButton.Value)
    SaveCETASetting "HideStandardLabelsButton", g_optHideStandardLabelsButton

    
    g_optBookDubsAsConfirmedStatus = Val(frmSettings.chkBookDubsAsConfirmedStatus.Value)
    SaveCETASetting "BookDubsAsConfirmedStatus", g_optBookDubsAsConfirmedStatus
    
    
    g_strDefaultInternalMovementLocation = CStr(frmSettings.txtDefaultInternalMovementLocation.Text)
    SaveCETASetting "DefaultInternalMovementLocation", g_strDefaultInternalMovementLocation
    
    g_strSMTPServer = CStr(frmSettings.txtSMTPServer.Text)
    SaveCETASetting "SMTPServer", g_strSMTPServer
    
    g_strSMTPUserName = CStr(frmSettings.txtSMTPUserName.Text)
    SaveCETASetting "SMTPUserName", g_strSMTPUserName
    
    g_strSMTPPassword = CStr(frmSettings.txtSMTPPassword.Text)
    SaveCETASetting "SMTPPassword", g_strSMTPPassword
    
    g_strSMTPServerAuthorised = CStr(frmSettings.txtSMTPServerAuthorised.Text)
    SaveCETASetting "SMTPServerAuthorised", g_strSMTPServerAuthorised
    
    g_strSMTPUserNameAuthorised = CStr(frmSettings.txtSMTPUserNameAuthorised.Text)
    SaveCETASetting "SMTPUserNameAuthorised", g_strSMTPUserNameAuthorised
    
    g_strSMTPPasswordAuthorised = CStr(frmSettings.txtSMTPPasswordAUthorised)
    SaveCETASetting "SMTPPasswordAuthorised", g_strSMTPPasswordAuthorised
    
    
    g_optShowTimeColumnInPurchaseOrder = Int(Val(frmSettings.chkShowTimeColumnInPurchaseOrder.Value))
    SaveCETASetting "ShowTimeColumnInPurchaseOrder", g_optShowTimeColumnInPurchaseOrder
    
    g_optMaximumQuoteDiscount = Int(Val(frmSettings.txtMaximumQuoteDiscount.Text))
    SaveCETASetting "MaximumQuoteDiscount", g_optMaximumQuoteDiscount
    
    
    g_optRequireBudgetCodesBeforeAuthorisation = frmSettings.chkRequireBudgetCodesBeforeAuthorisation.Value
    SaveCETASetting "RequireBudgetCodesBeforeAuthorisation", g_optRequireBudgetCodesBeforeAuthorisation
    
    g_optHideOrderByColumnOnCostingGrid = frmSettings.chkHideOrderByColumnOnCostingGrid.Value
    SaveCETASetting "HideOrderByColumnOnCostingGrid", g_optHideOrderByColumnOnCostingGrid
    
    
    g_optRequireCompletedDubsBeforeCosting = frmSettings.chkRequireCompletedDubsBeforeCosting.Value
    SaveCETASetting "RequireCompletedDubsBeforeCosting", g_optRequireCompletedDubsBeforeCosting
    
    g_optHideSoundChannelsOnVTDetailGrid = frmSettings.chkHideSoundChannelsOnVTDetailGrid.Value
    SaveCETASetting "HideSoundChannelsOnVTDetailGrid", g_optHideSoundChannelsOnVTDetailGrid

    
    g_optUseClipID = frmSettings.chkUseClipID.Value
    SaveCETASetting "UseClipID", g_optUseClipID
    
    g_strClipIDCaption = frmSettings.txtClipIDCaption.Text
    SaveCETASetting "ClipIDCaption", g_strClipIDCaption
    
    g_optShowTimeColumnInPurchaseOrder = frmSettings.chkShowTimeColumnInPurchaseOrder.Value
    SaveCETASetting "ShowTimeColumnInPurchaseOrder", g_optShowTimeColumnInPurchaseOrder
    
    
    g_strClipStorePlayURL = frmSettings.txtWebInfo(2).Text
    SaveCETASetting "ClipStorePlayURL", g_strClipStorePlayURL
    
    
    g_intDefaultCopiesToPrintOfInvoice = frmSettings.txtDefaultCopiesToPrintOfInvoice.Text
    SaveCETASetting "DefaultCopiesToPrintOfInvoice", g_intDefaultCopiesToPrintOfInvoice
    
    g_optUpperCaseDubbingDetails = frmSettings.chkUpperCaseDubbingDetails.Value
    SaveCETASetting "UpperCaseDubbingDetails", g_optUpperCaseDubbingDetails
    
    
    g_optCreateDNotesForExternalDeliveryPurchaseOrders = frmSettings.chkCreateDNotesForExternalDeliveryPurchaseOrders.Value
    SaveCETASetting "CreateDNotesForExternalDeliveryPurchaseOrders", g_optCreateDNotesForExternalDeliveryPurchaseOrders
    
    g_optApplyDealPriceToActualsRatherThanRateCard = frmSettings.chkApplyDealPriceToActualsRatherThanRateCard.Value
    SaveCETASetting "ApplyDealPriceToActualsRatherThanRateCard", g_optApplyDealPriceToActualsRatherThanRateCard
    
    g_optRequireDeliveryMethodsOnHireAndEditJobs = frmSettings.chkRequireDeliveryMethodsOnHireAndEditJobs.Value
    SaveCETASetting "RequireDeliveryMethodsOnHireAndEditJobs", g_optRequireDeliveryMethodsOnHireAndEditJobs
    
    g_optNeedAccountNumberToConfirm = frmSettings.chkNeedAccountNumberToConfirm.Value
    SaveCETASetting "NeedAccountNumberToConfirm", g_optNeedAccountNumberToConfirm
    
    g_optDontPromptForNumberOfInvoiceCopies = frmSettings.chkDontPromptForNumberOfInvoiceCopies.Value
    SaveCETASetting "DontPromptForNumberOfInvoiceCopies", g_optDontPromptForNumberOfInvoiceCopies
    
    g_optAutoTickOurContactInJobLists = frmSettings.chkAutoTickOurContactInJobLists.Value
    SaveCETASetting "AutoTickOurContactInJobLists", g_optAutoTickOurContactInJobLists
    
    g_optHideZeroRateCardItemsInManagementReports = frmSettings.chkHideZeroRateCardItemsInManagementReports.Value
    SaveCETASetting "HideZeroRateCardItemsInManagementReports", g_optHideZeroRateCardItemsInManagementReports
    
    g_optAllowNewPurchaseOrdersOnCostedJobs = frmSettings.chkAllowNewPurchaseOrdersOnCostedJobs.Value
    SaveCETASetting "AllowNewPurchaseOrdersOnCostedJobs", g_optAllowNewPurchaseOrdersOnCostedJobs
    
    g_optHarshCheckingOnDubbingEntry = frmSettings.chkHarshCheckingOnDubbingEntry.Value
    SaveCETASetting "HarshCheckingOnDubbingEntry", g_optHarshCheckingOnDubbingEntry
    
    g_strDefaultCategoryToAddResource = frmSettings.txtDefaultCategoryToAddResource.Text
    SaveCETASetting "DefaultCategoryToAddResource", g_strDefaultCategoryToAddResource
    
    g_optCreateDeliveryNotesForJobsWithNoAddress = frmSettings.chkCreateDeliveryNotesForJobsWithNoAddress.Value
    SaveCETASetting "CreateDeliveryNotesForJobsWithNoAddress", g_optCreateDeliveryNotesForJobsWithNoAddress
    
    g_optHideHireDespatchTab = frmSettings.chkHideHireDespatchTab.Value
    SaveCETASetting "HideHireDespatchTab", g_optHideHireDespatchTab
    
    g_optRequireOurContactBeforeSaving = frmSettings.chkRequireOurContactBeforeSaving.Value
    SaveCETASetting "RequireOurContactBeforeSaving", g_optRequireOurContactBeforeSaving
    
    g_optHideTimeColumn = Val(frmSettings.chkHideTimeColumnInCostings.Value)
    SaveCETASetting "AlwaysHideTimeColumn", g_optHideTimeColumn
    
    g_optRoundCurrencyDecimals = Val(frmSettings.txtRoundCurrencyDecimals.Text)
    SaveCETASetting "RoundCurrencyDecimals", g_optRoundCurrencyDecimals
    
    g_optSendToAccountsOnFinalise = frmSettings.chkSendToAccountsOnFinalise.Value
    SaveCETASetting "SendToAccountsOnFinalise", g_optSendToAccountsOnFinalise
    
    g_optDontPromptForInvoiceAddress = frmSettings.chkDontPromptForInvoiceAddress.Value
    SaveCETASetting "DontPromptForInvoiceAddress", g_optDontPromptForInvoiceAddress

    g_strUploadInvoiceLocation = frmSettings.txtAccountsExportLocation.Text
    If g_dbtype = "mysql" Then g_strUploadInvoiceLocation = Replace(g_strUploadInvoiceLocation, "\", "\\")
    SaveCETASetting "UploadInvoiceLocation", g_strUploadInvoiceLocation

    g_intSundryMarkup = frmSettings.txtSundryMarkup.Text
    SaveCETASetting "SundryMarkup", g_intSundryMarkup

    g_optCompleteJobsWhenVTCompleted = frmSettings.chkCompleteJobsWhenVTCompleted.Value
    SaveCETASetting "CompleteJobsWhenVTCompleted", g_optCompleteJobsWhenVTCompleted

    g_optAllowEditAuthorisedPurchaseOrders = frmSettings.chkAllowEditAuthorisedPurchaseOrders.Value
    SaveCETASetting "AllowEditAuthorisedPurchaseOrders", g_optAllowEditAuthorisedPurchaseOrders

    g_optForceDealPrice = frmSettings.chkForceDealPrice.Value
    SaveCETASetting "ForceDealPrice", g_optForceDealPrice
    
    g_optMakeEventsWide = frmSettings.chkMakeEventsWide.Value
    SaveCETASetting "MakeEventsWide", g_optMakeEventsWide
    
    g_optDefaultEventSearchInLibrary = frmSettings.chkDefaultEventSearchInLibrary.Value
    SaveCETASetting "DefaultEventSearchInLibrary", g_optDefaultEventSearchInLibrary
    
    g_optHideVATFieldsInCostings = frmSettings.chkHideVATFieldsInCostings.Value
    SaveCETASetting "HideVATFieldsInCostings", g_optHideVATFieldsInCostings
    
    g_optConnection1Name = frmSettings.txtConnection1.Text
    SaveCETASetting "Connection1Name", g_optConnection1Name
    
    g_optConnection2Name = frmSettings.txtConnection2.Text
    SaveCETASetting "Connection2Name", g_optConnection2Name
    
    g_optConnection3Name = frmSettings.txtConnection3.Text
    SaveCETASetting "Connection3Name", g_optConnection3Name
    
    g_optConnection4Name = frmSettings.txtConnection4.Text
    SaveCETASetting "Connection4Name", g_optConnection4Name
    
    g_optUseDepartmentalPurchaseAuthorisationNumbers = frmSettings.chkUseDepartmentalAuthorisationNumbers.Value
    SaveCETASetting "UseDepartmentalPurchaseAuthorisationNumbers", g_optUseDepartmentalPurchaseAuthorisationNumbers
    
    g_optUseJobDetailsWhenAddingJobDetailLinesToDespatch = frmSettings.chkUseJobDetailsWhenAddingJobDetailLinesToDespatch.Value
    SaveCETASetting "UseJobDetailsWhenAddingJobDetailLinesToDespatch", g_optUseJobDetailsWhenAddingJobDetailLinesToDespatch
    
    g_optSearchLibraryDefaultField = frmSettings.cmbDefaultLibrarySearchField.Text
    SaveCETASetting "SearchLibraryDefaultField", g_optSearchLibraryDefaultField
    
    g_optAlwaysUseRateCardDescriptions = frmSettings.chkAlwaysUseRateCardDescriptions.Value
    SaveCETASetting "AlwaysUseRateCardDescriptions", g_optAlwaysUseRateCardDescriptions
    
    g_intDefaultTab = IIf(frmSettings.cmbDefaultTabInJobDetails.Text = "Schedule", 7, 0)
    SaveCETASetting "DefaultTabInJobDetails", g_intDefaultTab
    
    g_optAddDurationToRateCode = frmSettings.chkAddDurationToRateCode.Value
    SaveCETASetting "AddDurationToRateCode", g_optAddDurationToRateCode
    
    g_optPromptForLateDespatchReason = frmSettings.chkPromptForLateDespatchReason.Value
    SaveCETASetting "PromptForLateDespatchReason", g_optPromptForLateDespatchReason
    
    g_optShowCompanyInsuranceOutDate = frmSettings.chkShowCompanyInsuranceOutDate
    SaveCETASetting "ShowCompanyInsuranceOutDate", g_optShowCompanyInsuranceOutDate
    
    g_optLockSystem = 0
    If frmSettings.chkSystemCETALock.Value <> 0 Then g_optLockSystem = g_optLockSystem Or 1
    If frmSettings.chkSystemFileManagerLock.Value <> 0 Then g_optLockSystem = g_optLockSystem Or 2
    If frmSettings.chkSystemWebLock.Value <> 0 Then g_optLockSystem = g_optLockSystem Or 4
    If frmSettings.chkSystemInvoiceLock.Value <> 0 Then g_optLockSystem = g_optLockSystem Or 8
    If frmSettings.chkFileHandlersLock.Value <> 0 Then g_optLockSystem = g_optLockSystem Or 16
    If frmSettings.chkVantageAPILocked.Value <> 0 Then g_optLockSystem = g_optLockSystem Or 32
    If frmSettings.chkFFMPGExplicitLocked.Value <> 0 Then g_optLockSystem = g_optLockSystem Or 65536
    If frmSettings.chkFFMPGGenericLocked.Value <> 0 Then g_optLockSystem = g_optLockSystem Or 512
    If frmSettings.chkFFMPGAudioLocked.Value <> 0 Then g_optLockSystem = g_optLockSystem Or 1024
    If frmSettings.chkFFMPGSpeedChangeLocked.Value <> 0 Then g_optLockSystem = g_optLockSystem Or 2048
    If frmSettings.chkDADCXMLLock.Value <> 0 Then g_optLockSystem = g_optLockSystem Or 4096
    If frmSettings.chkChecksumHandlersLock.Value <> 0 Then g_optLockSystem = g_optLockSystem Or 8192
    If frmSettings.chkDeliveryMonitoringLock.Value <> 0 Then g_optLockSystem = g_optLockSystem Or 131072
    If frmSettings.chkMediaInfoLock.Value <> 0 Then g_optLockSystem = g_optLockSystem Or 262144
    SaveCETASetting "LockSystem", g_optLockSystem
    
    g_optCloseSystemDown = frmSettings.chkCloseSystemDown.Value
    SaveCETASetting "CloseSystemDown", g_optCloseSystemDown
    
    g_optStopFFMPGServices = frmSettings.chkStopFFMPGServices.Value
    SaveCETASetting "StopFFMPGServices", g_optStopFFMPGServices
    
    g_optUseAdditionalDescriptionsWhenCosting = frmSettings.chkUseAdditionalDescriptionsWhenCosting.Value
    SaveCETASetting "UseAdditionalDescriptionsWhenCosting", g_optUseAdditionalDescriptionsWhenCosting
    
    g_optDeductPreSetDiscountsFromUnitPrice = frmSettings.chkDeductPreSetDiscountsFromUnitPrice.Value
    SaveCETASetting "DeductPreSetDiscountsFromUnitPrice", g_optDeductPreSetDiscountsFromUnitPrice
    
    g_optUseMasterDetailOnInvoice = frmSettings.chkUseMasterDetailOnInvoice.Value
    SaveCETASetting "UseMasterDetailOnInvoice", g_optUseMasterDetailOnInvoice
    
    g_optPromptForDetailedCompletion = frmSettings.chkPromptForDetailedCompletion.Value
    SaveCETASetting "PromptForDetailedCompletion", g_optPromptForDetailedCompletion
    
    g_optAppendInitialsAndDateToNotes = frmSettings.chkAppendInitialsAndDateToNotes.Value
    SaveCETASetting "AppendInitialsAndDateToNotes", g_optAppendInitialsAndDateToNotes
    
    g_optShowAdditionalEventFields = frmSettings.chkShowAdditionalEventFields.Value
    SaveCETASetting "ShowAdditionalEventFields", g_optShowAdditionalEventFields
    
    g_optCreatePDFFilesOfTechReviews = frmSettings.chkCreatePDFFilesOfTechReviews.Value
    SaveCETASetting "CreatePDFFilesOfTechReviews", g_optCreatePDFFilesOfTechReviews
    
    g_strPDFLocation = frmSettings.txtPDFSaveLocation.Text
    If g_dbtype = "mysql" Then g_strPDFLocation = Replace(g_strPDFLocation, "\", "\\")
    SaveCETASetting "PDFLocation", g_strPDFLocation
    
    g_strTapePDFLocation = frmSettings.txtTapePDFSaveLocation.Text
    If g_dbtype = "mysql" Then g_strTapePDFLocation = Replace(g_strTapePDFLocation, "\", "\\")
    SaveCETASetting "TapePDFLocation", g_strTapePDFLocation
    
    g_strFilePDFLocation = frmSettings.txtFilePDFSaveLocation.Text
    If g_dbtype = "mysql" Then g_strFilePDFLocation = Replace(g_strFilePDFLocation, "\", "\\")
    SaveCETASetting "FilePDFLocation", g_strFilePDFLocation
    
    g_intNumberOfDespatchNoteCopies = Val(frmSettings.txtNoOfCopies.Text)
    SaveCETASetting "NumberOfDespatchNoteCopies", g_intNumberOfDespatchNoteCopies
    
    g_strWebBrowserHomeDirectory = frmSettings.txtWebInfo(0).Text
    SaveCETASetting "WebBrowserHomeDirectory", g_strWebBrowserHomeDirectory
    
    g_strWebBrowserResourceInfoPage = frmSettings.txtWebInfo(1).Text
    SaveCETASetting "WebBrowserResourceInfoPage", g_strWebBrowserResourceInfoPage
    
    'g_optRequireActualsBeforeCosting = GetData("setting", "value", "name", "RequireActualsBeforeCosting")
    g_optRequireActualsBeforeCosting = frmSettings.chkRequireActualsBeforeCosting.Value
    SaveCETASetting "RequireActualsBeforeCosting", g_optRequireActualsBeforeCosting
    
    g_strDefaultJobType = frmSettings.cmbJobType.Text
    SaveCETASetting "DefaultJobType", g_strDefaultJobType
    
    g_strDefaultJobAllocation = frmSettings.cmbJobAllocation.Text
    SaveCETASetting "DefaultJobAllocation", g_strDefaultJobAllocation
    
    g_strWhenToAllocate = frmSettings.cmbWhenToAllocate.Text
    SaveCETASetting "WhenToAllocate", g_strWhenToAllocate
    
    g_intDefaultTimeWhenZero = Val(frmSettings.txtDefaultTimeWhenZero.Text)
    SaveCETASetting "DefaultTimeWhenZero", g_intDefaultTimeWhenZero
    
    g_optCostSchedule = frmSettings.chkCostSchedule.Value
    SaveCETASetting "CostSchedule", g_optCostSchedule
    
    g_optCostDubbing = frmSettings.chkCostDubbing.Value
    SaveCETASetting "CostDubbing", g_optCostDubbing
    
    g_optUseTitleInMaster = frmSettings.chkUseTitleInMaster.Value
    SaveCETASetting "UseTitleInMaster", g_optUseTitleInMaster
    
    g_optUseTitleInCopy = frmSettings.chkUseTitleInCopy.Value
    SaveCETASetting "UseTitleInCopy", g_optUseTitleInCopy
    
    g_optUseSpecialDubbingComboOptions = frmSettings.chkUseSpecialDubbingComboOptions.Value
    SaveCETASetting "UseSpecialDubbingComboOptions", g_optUseSpecialDubbingComboOptions
    
    g_optRequireProductBeforeSaving = frmSettings.chkRequireProductBeforeSaving.Value
    SaveCETASetting "RequireProductBeforeSaving", g_optRequireProductBeforeSaving
    
    g_lngJellyroll1LibraryID = Val(frmSettings.txtJellyroll1LibraryID.Text)
    SaveCETASetting "Jellyroll1LibraryID", g_lngJellyroll1LibraryID
    
    g_lngJellyroll2LibraryID = Val(frmSettings.txtJellyroll2LibraryID.Text)
    SaveCETASetting "Jellyroll2LibraryID", g_lngJellyroll2LibraryID
    
    g_strLocationOfJellyroll1Folder = frmSettings.txtLocationOfJellyroll1Folder.Text
    SaveCETASetting "LocationOfJellyroll1Folder", g_strLocationOfJellyroll1Folder
    
    g_strLocationOfJellyroll2Folder = frmSettings.txtLocationOfJellyroll2Folder.Text
    SaveCETASetting "LocationOfJellyroll2Folder", g_strLocationOfJellyroll2Folder
    
    GetSettingsFromDatabase
    
    Unload frmLoading
    Set frmLoading = Nothing

    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub


