VERSION 5.00
Begin VB.Form frmSelectPrinter 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Select Printer"
   ClientHeight    =   1455
   ClientLeft      =   2535
   ClientTop       =   1665
   ClientWidth     =   4680
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1455
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   1620
      TabIndex        =   2
      Top             =   960
      Width           =   1635
   End
   Begin VB.ComboBox cmbSelectPrinter 
      Height          =   315
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   4455
   End
   Begin VB.Label Label1 
      Caption         =   "Which printer do you want to use to print your labels?"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   4455
   End
End
Attribute VB_Name = "frmSelectPrinter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOK_Click()

If cmbSelectPrinter.Text = "" Then
    MsgBox "You must select a valid printer", vbExclamation
    Exit Sub
End If
Me.Hide
End Sub

Private Sub Form_Load()

   Dim x As Printer
   
   cmbSelectPrinter.Clear
   'Load the combo with all available printers
   For Each x In Printers
      cmbSelectPrinter.AddItem x.DeviceName
      If Printer.DeviceName = x.DeviceName Then 'Current default
         cmbSelectPrinter.Text = x.DeviceName
      End If
   Next
   'Set the number of copies to 1
   cmbSelectPrinter.ListIndex = 0


Dim prt As Printer

'This code would probably go in the Form_Load of your printer selection form

'Load Printers
For Each prt In Printers
     cboSelectPrinter.AddItem prt.DeviceName
Next


'This code would go in the function that gets called
CenterForm Me

End Sub

Private Sub Label1_DblClick()
Me.Hide
End Sub
