VERSION 5.00
Begin VB.Form frmMediaInfo 
   Caption         =   "Media Info Query of File"
   ClientHeight    =   12615
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   23865
   LinkTopic       =   "Form1"
   ScaleHeight     =   12615
   ScaleWidth      =   23865
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtSynopsis 
      Height          =   315
      Left            =   1740
      TabIndex        =   97
      ToolTipText     =   "The source Clip ID"
      Top             =   9060
      Width           =   2355
   End
   Begin VB.TextBox txtEpisode 
      Height          =   315
      Left            =   1740
      TabIndex        =   96
      ToolTipText     =   "The source Clip ID"
      Top             =   7620
      Width           =   2355
   End
   Begin VB.TextBox txtTitle 
      Height          =   315
      Left            =   1740
      TabIndex        =   95
      ToolTipText     =   "The source Clip ID"
      Top             =   7260
      Width           =   2355
   End
   Begin VB.TextBox txtSeriesTitle 
      Height          =   315
      Left            =   1740
      TabIndex        =   94
      ToolTipText     =   "The source Clip ID"
      Top             =   6900
      Width           =   2355
   End
   Begin VB.TextBox txtFileName 
      Height          =   315
      Left            =   11280
      TabIndex        =   70
      ToolTipText     =   "The source Clip ID"
      Top             =   420
      Width           =   9495
   End
   Begin VB.TextBox txtFilePath 
      Height          =   315
      Left            =   1740
      TabIndex        =   69
      ToolTipText     =   "The source Clip ID"
      Top             =   420
      Width           =   9495
   End
   Begin VB.TextBox txtColourSpace 
      Height          =   315
      Left            =   1740
      TabIndex        =   65
      ToolTipText     =   "The source Clip ID"
      Top             =   1860
      Width           =   2355
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   10215
      Left            =   13740
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   63
      Top             =   2220
      Width           =   9975
   End
   Begin VB.TextBox txtTimecodeStop 
      Height          =   315
      Left            =   4140
      TabIndex        =   55
      ToolTipText     =   "The source Clip ID"
      Top             =   6540
      Width           =   2355
   End
   Begin VB.TextBox txtTrackCount 
      Height          =   315
      Left            =   1740
      TabIndex        =   53
      ToolTipText     =   "The source Clip ID"
      Top             =   4020
      Width           =   2355
   End
   Begin VB.TextBox txtStreamType 
      Height          =   315
      Left            =   1740
      TabIndex        =   49
      ToolTipText     =   "The source Clip ID"
      Top             =   1140
      Width           =   2355
   End
   Begin VB.CommandButton CmdClose 
      Caption         =   "Close Form"
      Height          =   375
      Left            =   60
      TabIndex        =   47
      Top             =   12120
      Width           =   2295
   End
   Begin VB.TextBox txtTimecodeStart 
      Height          =   315
      Left            =   1740
      TabIndex        =   16
      ToolTipText     =   "The source Clip ID"
      Top             =   6540
      Width           =   2355
   End
   Begin VB.TextBox txtGeometry 
      Height          =   315
      Left            =   1740
      TabIndex        =   15
      ToolTipText     =   "The source Clip ID"
      Top             =   6180
      Width           =   2355
   End
   Begin VB.TextBox txtDuration 
      Height          =   315
      Left            =   1740
      TabIndex        =   14
      ToolTipText     =   "The source Clip ID"
      Top             =   5820
      Width           =   2355
   End
   Begin VB.TextBox txtAspect 
      Height          =   315
      Left            =   1740
      TabIndex        =   13
      ToolTipText     =   "The source Clip ID"
      Top             =   5460
      Width           =   2355
   End
   Begin VB.TextBox txtInterlace 
      Height          =   315
      Left            =   1740
      TabIndex        =   12
      ToolTipText     =   "The source Clip ID"
      Top             =   5100
      Width           =   2355
   End
   Begin VB.TextBox txtHeight 
      Height          =   315
      Left            =   1740
      TabIndex        =   11
      ToolTipText     =   "The source Clip ID"
      Top             =   4740
      Width           =   2355
   End
   Begin VB.TextBox txtWidth 
      Height          =   315
      Left            =   1740
      TabIndex        =   10
      ToolTipText     =   "The source Clip ID"
      Top             =   4380
      Width           =   2355
   End
   Begin VB.TextBox txtAudioBitrate 
      Height          =   315
      Left            =   1740
      TabIndex        =   9
      ToolTipText     =   "The source Clip ID"
      Top             =   3660
      Width           =   2355
   End
   Begin VB.TextBox txtVideoBitrate 
      Height          =   315
      Left            =   1740
      TabIndex        =   8
      ToolTipText     =   "The source Clip ID"
      Top             =   3300
      Width           =   2355
   End
   Begin VB.TextBox txtVBRCBR 
      Height          =   315
      Left            =   1740
      TabIndex        =   7
      ToolTipText     =   "The source Clip ID"
      Top             =   2940
      Width           =   2355
   End
   Begin VB.TextBox txtFrameRate 
      Height          =   315
      Left            =   1740
      TabIndex        =   6
      ToolTipText     =   "The source Clip ID"
      Top             =   2580
      Width           =   2355
   End
   Begin VB.TextBox txtAudioCodec 
      Height          =   315
      Left            =   1740
      TabIndex        =   5
      ToolTipText     =   "The source Clip ID"
      Top             =   2220
      Width           =   2355
   End
   Begin VB.TextBox txtVideoCodec 
      Height          =   315
      Left            =   1740
      TabIndex        =   4
      ToolTipText     =   "The source Clip ID"
      Top             =   1500
      Width           =   2355
   End
   Begin VB.TextBox txtFormat 
      Height          =   315
      Left            =   1740
      TabIndex        =   2
      ToolTipText     =   "The source Clip ID"
      Top             =   780
      Width           =   2355
   End
   Begin VB.Label lblAudioProfile 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   8940
      TabIndex        =   103
      Tag             =   "CLEARFIELDS"
      Top             =   2220
      Width           =   2355
   End
   Begin VB.Label lblAudioVersion 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   6540
      TabIndex        =   102
      Tag             =   "CLEARFIELDS"
      Top             =   2220
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Primary Audio Lang"
      Height          =   255
      Index           =   28
      Left            =   240
      TabIndex        =   101
      Top             =   11280
      Width           =   1455
   End
   Begin VB.Label lblPrimaryAudioLanguage 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   100
      Tag             =   "CLEARFIELDS"
      Top             =   11220
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Audio Layout"
      Height          =   255
      Index           =   27
      Left            =   240
      TabIndex        =   99
      Top             =   10920
      Width           =   1455
   End
   Begin VB.Label lblAudioLayout 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   98
      Tag             =   "CLEARFIELDS"
      Top             =   10860
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "# of Parts and Info"
      Height          =   255
      Index           =   26
      Left            =   240
      TabIndex        =   93
      Top             =   9840
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Copyright Year"
      Height          =   255
      Index           =   25
      Left            =   240
      TabIndex        =   92
      Top             =   9480
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Synopsis"
      Height          =   255
      Index           =   24
      Left            =   240
      TabIndex        =   91
      Top             =   9120
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Total Prog Duration"
      Height          =   255
      Index           =   23
      Left            =   240
      TabIndex        =   90
      Top             =   8760
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Clock Start"
      Height          =   255
      Index           =   22
      Left            =   240
      TabIndex        =   89
      Top             =   8400
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Line Up Start"
      Height          =   255
      Index           =   21
      Left            =   240
      TabIndex        =   88
      Top             =   8040
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Episde Number"
      Height          =   255
      Index           =   20
      Left            =   240
      TabIndex        =   87
      Top             =   7680
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Programme Title"
      Height          =   255
      Index           =   19
      Left            =   240
      TabIndex        =   86
      Top             =   7320
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Series Title"
      Height          =   255
      Index           =   18
      Left            =   240
      TabIndex        =   85
      Top             =   6960
      Width           =   1455
   End
   Begin VB.Label lblEpisodeNumber 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   6540
      TabIndex        =   84
      Tag             =   "CLEARFIELDS"
      Top             =   7620
      Width           =   2355
   End
   Begin VB.Label lblSeriesNumber 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   6540
      TabIndex        =   83
      Tag             =   "CLEARFIELDS"
      Top             =   6900
      Width           =   2355
   End
   Begin VB.Label lblPart 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Index           =   2
      Left            =   6540
      TabIndex        =   82
      Tag             =   "CLEARFIELDS"
      Top             =   10500
      Width           =   4755
   End
   Begin VB.Label lblPart 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Index           =   1
      Left            =   6540
      TabIndex        =   81
      Tag             =   "CLEARFIELDS"
      Top             =   10140
      Width           =   4755
   End
   Begin VB.Label lblPart 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Index           =   0
      Left            =   6540
      TabIndex        =   80
      Tag             =   "CLEARFIELDS"
      Top             =   9780
      Width           =   4755
   End
   Begin VB.Label lblNumberOfParts 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   79
      Tag             =   "CLEARFIELDS"
      Top             =   9780
      Width           =   2355
   End
   Begin VB.Label lblCopyrightYear 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   78
      Tag             =   "CLEARFIELDS"
      Top             =   9420
      Width           =   2355
   End
   Begin VB.Label lblSynopsis 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   77
      Tag             =   "CLEARFIELDS"
      Top             =   9060
      Width           =   9555
   End
   Begin VB.Label lblTotalProgrammeDuration 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   76
      Tag             =   "CLEARFIELDS"
      Top             =   8700
      Width           =   2355
   End
   Begin VB.Label lblIdentClockStart 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   75
      Tag             =   "CLEARFIELDS"
      Top             =   8340
      Width           =   2355
   End
   Begin VB.Label lblLineUpStart 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   74
      Tag             =   "CLEARFIELDS"
      Top             =   7980
      Width           =   2355
   End
   Begin VB.Label lblEpisodeTitleNumber 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   73
      Tag             =   "CLEARFIELDS"
      Top             =   7620
      Width           =   2355
   End
   Begin VB.Label lblProgrammeTitle 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   72
      Tag             =   "CLEARFIELDS"
      Top             =   7260
      Width           =   2355
   End
   Begin VB.Label lblSeriesTitle 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   71
      Tag             =   "CLEARFIELDS"
      Top             =   6900
      Width           =   2355
   End
   Begin VB.Label lblMatroskaTimecode 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   11340
      TabIndex        =   68
      Tag             =   "CLEARFIELDS"
      Top             =   6540
      Width           =   2355
   End
   Begin VB.Label lblTransferCharacteristics 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   18540
      TabIndex        =   67
      Tag             =   "CLEARFIELDS"
      Top             =   1500
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Colour Space"
      Height          =   255
      Index           =   17
      Left            =   240
      TabIndex        =   66
      Top             =   1920
      Width           =   1155
   End
   Begin VB.Label lblColorPrimaries 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   16140
      TabIndex        =   64
      Tag             =   "CLEARFIELDS"
      Top             =   1500
      Width           =   2355
   End
   Begin VB.Label lblVideoChromaSubsampling 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   13740
      TabIndex        =   62
      Tag             =   "CLEARFIELDS"
      Top             =   1500
      Width           =   2355
   End
   Begin VB.Label lblVideoColorSpace 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   11340
      TabIndex        =   61
      Tag             =   "CLEARFIELDS"
      Top             =   1500
      Width           =   2355
   End
   Begin VB.Label lblSampleCount 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   6540
      TabIndex        =   60
      Tag             =   "CLEARFIELDS"
      Top             =   5820
      Width           =   2355
   End
   Begin VB.Label lblFileName 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   11280
      TabIndex        =   59
      Tag             =   "CLEARFIELDS"
      Top             =   60
      Visible         =   0   'False
      Width           =   9495
   End
   Begin VB.Label lblOverallBitrate 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   6540
      TabIndex        =   58
      Tag             =   "CLEARFIELDS"
      Top             =   3300
      Width           =   2355
   End
   Begin VB.Label lblVideoBitDepth 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   8940
      TabIndex        =   57
      Tag             =   "CLEARFIELDS"
      Top             =   1500
      Width           =   2355
   End
   Begin VB.Label lblSamplingRate 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   8940
      TabIndex        =   56
      Tag             =   "CLEARFIELDS"
      Top             =   3660
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "# of Tracks"
      Height          =   255
      Index           =   16
      Left            =   240
      TabIndex        =   54
      Top             =   4080
      Width           =   1155
   End
   Begin VB.Label lblNumberOfTracks 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   52
      Tag             =   "CLEARFIELDS"
      Top             =   4020
      Width           =   2355
   End
   Begin VB.Label lblCodecID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   6540
      TabIndex        =   51
      Tag             =   "CLEARFIELDS"
      Top             =   1500
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Stream"
      Height          =   255
      Index           =   15
      Left            =   240
      TabIndex        =   50
      Top             =   1200
      Width           =   1155
   End
   Begin VB.Label lblGeneralFormat 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   48
      Tag             =   "CLEARFIELDS"
      Top             =   1140
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Timecode"
      Height          =   255
      Index           =   14
      Left            =   240
      TabIndex        =   46
      Top             =   6600
      Width           =   1155
   End
   Begin VB.Label lblTimecode 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   8940
      TabIndex        =   45
      Tag             =   "CLEARFIELDS"
      Top             =   6540
      Width           =   2355
   End
   Begin VB.Label lblTimecodeType 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   6540
      TabIndex        =   44
      Tag             =   "CLEARFIELDS"
      Top             =   6540
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Geometry"
      Height          =   255
      Index           =   13
      Left            =   240
      TabIndex        =   43
      Top             =   6240
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Duration"
      Height          =   255
      Index           =   12
      Left            =   240
      TabIndex        =   42
      Top             =   5880
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Aspect"
      Height          =   255
      Index           =   11
      Left            =   240
      TabIndex        =   41
      Top             =   5520
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Interlace"
      Height          =   255
      Index           =   10
      Left            =   240
      TabIndex        =   40
      Top             =   5160
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Height"
      Height          =   255
      Index           =   9
      Left            =   240
      TabIndex        =   39
      Top             =   4800
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Width"
      Height          =   255
      Index           =   8
      Left            =   240
      TabIndex        =   38
      Top             =   4440
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Audio Bitrate"
      Height          =   255
      Index           =   7
      Left            =   240
      TabIndex        =   37
      Top             =   3720
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Video Bitrate"
      Height          =   255
      Index           =   6
      Left            =   240
      TabIndex        =   36
      Top             =   3360
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "CBR / VBR"
      Height          =   255
      Index           =   5
      Left            =   240
      TabIndex        =   35
      Top             =   3000
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Frame Rate"
      Height          =   255
      Index           =   4
      Left            =   240
      TabIndex        =   34
      Top             =   2640
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Audio Codec"
      Height          =   255
      Index           =   3
      Left            =   240
      TabIndex        =   33
      Top             =   2280
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Video Codec"
      Height          =   255
      Index           =   2
      Left            =   240
      TabIndex        =   32
      Top             =   1560
      Width           =   1155
   End
   Begin VB.Label lblAudioChannels 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   6540
      TabIndex        =   31
      Tag             =   "CLEARFIELDS"
      Top             =   4020
      Width           =   2355
   End
   Begin VB.Label lblBitDepth 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   6540
      TabIndex        =   30
      Tag             =   "CLEARFIELDS"
      Top             =   3660
      Width           =   2355
   End
   Begin VB.Label lblPixelRatio 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   29
      Tag             =   "CLEARFIELDS"
      Top             =   6180
      Width           =   2355
   End
   Begin VB.Label lblFrameCount 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   28
      Tag             =   "CLEARFIELDS"
      Top             =   5820
      Width           =   2355
   End
   Begin VB.Label lblDisplayAspect 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   27
      Tag             =   "CLEARFIELDS"
      Top             =   5460
      Width           =   2355
   End
   Begin VB.Label lblInterlace 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   26
      Tag             =   "CLEARFIELDS"
      Top             =   5100
      Width           =   2355
   End
   Begin VB.Label lblHeight 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   25
      Tag             =   "CLEARFIELDS"
      Top             =   4740
      Width           =   2355
   End
   Begin VB.Label lblWidth 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   24
      Tag             =   "CLEARFIELDS"
      Top             =   4380
      Width           =   2355
   End
   Begin VB.Label lblAudioBitrate 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   23
      Tag             =   "CLEARFIELDS"
      Top             =   3660
      Width           =   2355
   End
   Begin VB.Label lblVideoBitrate 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   22
      Tag             =   "CLEARFIELDS"
      Top             =   3300
      Width           =   2355
   End
   Begin VB.Label lblVBRCBR 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   21
      Tag             =   "CLEARFIELDS"
      Top             =   2940
      Width           =   2355
   End
   Begin VB.Label lblFrameRate 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   20
      Tag             =   "CLEARFIELDS"
      Top             =   2580
      Width           =   2355
   End
   Begin VB.Label lblAudioCodec 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   19
      Tag             =   "CLEARFIELDS"
      Top             =   2220
      Width           =   2355
   End
   Begin VB.Label lblVideoCodec 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   18
      Tag             =   "CLEARFIELDS"
      Top             =   1500
      Width           =   2355
   End
   Begin VB.Label lblFormat 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4140
      TabIndex        =   17
      Tag             =   "CLEARFIELDS"
      Top             =   780
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Format"
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   3
      Top             =   840
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "File Path"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   1
      Top             =   480
      Width           =   1155
   End
   Begin VB.Label lblFilePath 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   1740
      TabIndex        =   0
      Tag             =   "CLEARFIELDS"
      Top             =   60
      Visible         =   0   'False
      Width           =   9495
   End
End
Attribute VB_Name = "frmMediaInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdClose_Click()

Me.Hide

End Sub

Private Sub Form_Activate()

Dim m_Framerate As Integer
Dim Handle As Long, l_curAudioBitRate As Currency, Count As Long, TempStr As String, l_lngAudioChannelCount As Long

txtFilePath.Text = lblFilePath.Caption
txtFileName.Text = lblFileName.Caption

'Open the File with MediaInfo.dll and collect the available information
Handle = MediaInfo_New()
Call MediaInfo_Open(Handle, StrPtr(lblFilePath.Caption))
Text1.Text = bstr(MediaInfo_Inform(Handle, 0))
Screen.MousePointer = vbNormal

lblFormat.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("Format_Profile"), MediaInfo_Info_Text, MediaInfo_Info_Name))
If lblFormat.Caption = "" Then lblFormat.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("Format_Commercial"), MediaInfo_Info_Text, MediaInfo_Info_Name))
If lblFormat.Caption = "PDF" And UCase(Right(lblFileName.Caption, 3)) = ".AI" Then lblFormat.Caption = "Adobe AI File"
lblGeneralFormat.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("Format"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblVideoCodec.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("Video_Format_List"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblCodecID.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("CodecID"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblAudioCodec.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("Audio_Format_List"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblFrameRate.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("FrameRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblVBRCBR.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("BitRate_Mode"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblVideoBitrate.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("BitRate_Maximum"), MediaInfo_Info_Text, MediaInfo_Info_Name))
If lblVideoBitrate.Caption = "" Then lblVideoBitrate.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("BitRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblWidth.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("Width_Original"), MediaInfo_Info_Text, MediaInfo_Info_Name))
If lblWidth.Caption = "" Then lblWidth.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("Width"), MediaInfo_Info_Text, MediaInfo_Info_Name))
If lblWidth.Caption = "" Then lblWidth.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Image, 0, StrPtr("Width"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblHeight.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("Height_Original"), MediaInfo_Info_Text, MediaInfo_Info_Name))
If lblHeight.Caption = "" Then lblHeight.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("Height"), MediaInfo_Info_Text, MediaInfo_Info_Name))
If lblHeight.Caption = "" Then lblHeight.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Image, 0, StrPtr("Height"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblInterlace.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("ScanType"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblDisplayAspect.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("DisplayAspectRatio"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblFrameCount.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("FrameCount"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblPixelRatio.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("PixelAspectRatio_Original"), MediaInfo_Info_Text, MediaInfo_Info_Name))
If lblPixelRatio.Caption = "" Then lblPixelRatio.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("PixelAspectRatio"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblAudioCodec.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, 0, StrPtr("Format"), MediaInfo_Info_Text, MediaInfo_Info_Name))
If lblAudioCodec.Caption = "MPEG Audio" Then
    lblAudioProfile.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, 0, StrPtr("Format_Version"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblAudioVersion.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, 0, StrPtr("Format_Profile"), MediaInfo_Info_Text, MediaInfo_Info_Name))
End If
lblBitDepth.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, 0, StrPtr("BitDepth"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblVideoBitDepth.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("BitDepth"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblVideoColorSpace.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("ColorSpace"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblColorPrimaries.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("colour_primaries"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblTransferCharacteristics.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("transfer_characteristics"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblVideoChromaSubsampling.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("ChromaSubsampling"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblSamplingRate.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, 0, StrPtr("SamplingRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblSampleCount.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, 0, StrPtr("SamplingCount"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblNumberOfTracks.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("AudioCount"), MediaInfo_Info_Text, MediaInfo_Info_Name))
l_curAudioBitRate = 0
l_lngAudioChannelCount = 0
For Count = 0 To Val(lblNumberOfTracks.Caption) - 1
    If Val(bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, Count, StrPtr("BitRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))) <> 0 Then
        l_curAudioBitRate = l_curAudioBitRate + Val(bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, Count, StrPtr("BitRate"), MediaInfo_Info_Text, MediaInfo_Info_Name)))
    Else
        l_curAudioBitRate = l_curAudioBitRate + Val(bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, Count, StrPtr("BitRate_Maximum"), MediaInfo_Info_Text, MediaInfo_Info_Name)))
    End If
    l_lngAudioChannelCount = l_lngAudioChannelCount + Val(bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, Count, StrPtr("Channel(s)"), MediaInfo_Info_Text, MediaInfo_Info_Name)))
Next
lblAudioChannels.Caption = l_lngAudioChannelCount
If l_curAudioBitRate = 0 Then
    If lblVideoBitrate.Caption <> "" And l_lngAudioChannelCount <> 0 Then
        l_curAudioBitRate = Val(bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("OverallBitRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))) - Val(lblVideoBitrate.Caption)
    ElseIf lblVideoBitrate.Caption <> "" Then
        lblOverallBitrate.Caption = Val(lblVideoBitrate.Caption)
    Else
        lblOverallBitrate.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("OverallBitRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    End If
End If
lblAudioBitrate.Caption = l_curAudioBitRate
lblTimecodeType.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 0, StrPtr("Type"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblTimecode.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 0, StrPtr("TimeCode_FirstFrame"), MediaInfo_Info_Text, MediaInfo_Info_Name))
lblMatroskaTimecode.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("TimeCode_FirstFrame"), MediaInfo_Info_Text, MediaInfo_Info_Name))
If lblMatroskaTimecode.Caption = "" Then lblMatroskaTimecode.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("TimeCode_FirstFrame"), MediaInfo_Info_Text, MediaInfo_Info_Name))
If lblVideoCodec.Caption = "DV" Then
    lblVideoCodec.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("Format_Commercial"), MediaInfo_Info_Text, MediaInfo_Info_Name))
End If
If lblGeneralFormat.Caption = "QuickTime" Then
    lblFormat.Caption = "Quicktime"
    lblVideoCodec.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("Format_Commercial"), MediaInfo_Info_Text, MediaInfo_Info_Name))
ElseIf lblGeneralFormat.Caption = "MXF" Then
    lblFormat.Caption = "MXF " & lblFormat.Caption
    lblVideoCodec.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("Format_Commercial"), MediaInfo_Info_Text, MediaInfo_Info_Name))
ElseIf lblVideoCodec.Caption = "MPEG Video" And lblCodecID.Caption = "61" Then
    lblFormat.Caption = "MPEG 4"
    lblVideoCodec.Caption = "XDCAM EX 35"
ElseIf lblVideoCodec.Caption = "MPEG Video" And lblFormat.Caption <> "QuickTime" Then
    If lblCodecID.Caption = "xdv7" Then
        lblVideoCodec.Caption = "XDCAM EX 35"
    ElseIf lblVideoCodec.Caption = "xd5c" Then
        lblVideoCodec.Caption = "XDCAM HD422"
    ElseIf Left(bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("Format_Commercial"), MediaInfo_Info_Text, MediaInfo_Info_Name)), 6) = "MPEG-2" Then
        lblFormat.Caption = "MPEG 2"
    Else
        lblFormat.Caption = "MPEG"
    End If
End If
If lblFormat.Caption = "MPEG Audio" Then
    lblFormat.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, 0, StrPtr("Codec"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblAudioCodec.Caption = lblFormat.Caption & " " & lblAudioChannels.Caption
End If

Dim ParsingOut As RegExp
Dim Matches As MatchCollection
Set ParsingOut = New RegExp
ParsingOut.Pattern = "(\d+)"

If bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 2, StrPtr("Format"), MediaInfo_Info_Text, MediaInfo_Info_Name)) = "AS-11 Core" Then
    lblSeriesTitle.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 2, StrPtr("SeriesTitle"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    Set Matches = ParsingOut.Execute(lblSeriesTitle.Caption)
    If Matches.Count > 0 Then
        If Matches(0).SubMatches.Count > 0 Then
            lblSeriesNumber.Caption = Matches(0).SubMatches(0)
        Else
            lblSeriesNumber.Caption = ""
        End If
    Else
        lblSeriesNumber.Caption = ""
    End If
    lblProgrammeTitle.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 2, StrPtr("ProgrammeTitle"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblEpisodeTitleNumber.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 2, StrPtr("EpisodeTitleNumber"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    Set Matches = ParsingOut.Execute(lblEpisodeTitleNumber.Caption)
    If Matches.Count > 0 Then
        If Matches(0).SubMatches.Count > 0 Then
            lblEpisodeNumber.Caption = Matches(0).SubMatches(0)
        Else
            lblEpisodeNumber.Caption = ""
        End If
    Else
        lblEpisodeNumber.Caption = ""
    End If
    lblLineUpStart.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 3, StrPtr("LineUpStart"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblIdentClockStart.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 3, StrPtr("IdentClockStart"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblTotalProgrammeDuration.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 3, StrPtr("TotalProgrammeDuration"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblSynopsis.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 3, StrPtr("Synopsis"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblCopyrightYear.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 3, StrPtr("CopyrightYear"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblNumberOfParts.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 3, StrPtr("TotalNumberOfParts"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    For Count = 0 To Val(lblNumberOfParts.Caption)
        lblPart(Count).Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 4 + Count, StrPtr(Count + 1), MediaInfo_Info_Text, MediaInfo_Info_Name))
    Next
    lblPrimaryAudioLanguage.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 2, StrPtr("PrimaryAudioLanguage"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblAudioLayout.Caption = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 2, StrPtr("AudioTrackLayout"), MediaInfo_Info_Text, MediaInfo_Info_Name))
End If

'Close the MediaInfo
Call MediaInfo_Close(Handle)
Call MediaInfo_Delete(Handle)

'Translate the Information into MX1 speak

If lblFormat.Caption = "Wave" Then
    txtVideoCodec.Text = GetAlias(lblVideoCodec.Caption)
ElseIf lblCodecID.Caption = "AVdn" Then
    txtVideoCodec.Text = GetAlias(Trim(lblVideoCodec.Caption & " " & lblCodecID.Caption) & " " & lblVideoBitDepth.Caption)
ElseIf lblFormat.Caption <> "" Then
    txtVideoCodec.Text = GetAlias(Trim(lblVideoCodec.Caption & " " & lblCodecID.Caption))
End If
txtColourSpace.Text = lblVideoColorSpace.Caption
If lblTransferCharacteristics.Caption = "HLG" Then txtColourSpace.Text = IIf(txtColourSpace.Text <> "", txtColourSpace.Text & " ", "") & "HDR"
If lblAudioCodec.Caption = "PCM" Or lblAudioCodec.Caption = "MPA1L2" Then
    txtAudioCodec.Text = GetAlias(Trim(lblAudioCodec.Caption & " " & lblBitDepth.Caption & " " & lblSamplingRate & " " & lblAudioChannels.Caption))
Else
    txtAudioCodec.Text = GetAlias(lblAudioCodec.Caption)
End If
If Val(lblFrameRate.Caption) = Int(Val(lblFrameRate.Caption)) Then
    txtFrameRate.Text = Int(Val(lblFrameRate.Caption))
Else
    txtFrameRate.Text = Format(Val(lblFrameRate.Caption), "#.00")
End If
If txtFrameRate.Text = "0" Then txtFrameRate.Text = ""
Select Case txtFrameRate.Text

    Case "25"
        m_Framerate = TC_25
    Case "29.97"
        If lblTimecode <> "" Then
            If Mid(lblTimecode, 9, 1) = ";" Then
                m_Framerate = TC_29
            Else
                m_Framerate = TC_30
                txtFrameRate = "29.97 NDF"
            End If
        Else
            m_Framerate = TC_30
            txtFrameRate = "29.97 NDF"
        End If
    Case "30"
        m_Framerate = TC_30
    Case "24", "23.98"
        m_Framerate = TC_24
    Case "50"
        m_Framerate = TC_50
    Case "60"
        m_Framerate = TC_60
    Case "59.94"
        If lblTimecode <> "" Then
            If Mid(lblTimecode, 9, 1) = ";" Then
                m_Framerate = TC_59
            Else
                m_Framerate = TC_60
                txtFrameRate = "59.94 NDF"
            End If
        Else
            m_Framerate = TC_60
            txtFrameRate = "59.94 NDF"
        End If
    Case Else
        m_Framerate = TC_UN

End Select
txtVBRCBR.Text = lblVBRCBR.Caption
txtWidth.Text = lblWidth.Caption
txtHeight.Text = lblHeight.Caption
txtVideoBitrate.Text = Int(Val(lblVideoBitrate.Caption) / 1000)
If txtVideoBitrate.Text = 0 Then txtVideoBitrate.Text = ""
txtInterlace.Text = GetAlias(lblInterlace.Caption)
If lblFrameCount.Caption <> "" Then
    txtDuration.Text = Timecode_From_FrameNumber(Val(lblFrameCount.Caption), m_Framerate)
Else
    If Val(lblSamplingRate.Caption) <> 0 Then
        lblFrameCount.Caption = Val(lblSampleCount.Caption) / Val(lblSamplingRate.Caption) * 25
    Else
        lblFrameCount.Caption = ""
    End If
    txtDuration.Text = Timecode_From_FrameNumber(Val(lblFrameCount.Caption), m_Framerate)
End If
If Val(lblAudioBitrate.Caption) <> 0 Then txtAudioBitrate.Text = Int(Val(lblAudioBitrate.Caption) / 1000)
txtTrackCount = lblNumberOfTracks.Caption
If lblTimecodeType.Caption = "Time code" Then
    txtTimecodeStart.Text = lblTimecode.Caption
    txtTimecodeStop.Text = Timecode_Add(txtTimecodeStart.Text, txtDuration.Text, m_Framerate)
ElseIf lblMatroskaTimecode.Caption <> "" Then
    txtTimecodeStart.Text = lblMatroskaTimecode.Caption
    txtTimecodeStop.Text = Timecode_Add(txtTimecodeStart.Text, txtDuration.Text, m_Framerate)
Else
    Select Case m_Framerate
        Case TC_29, TC_59
            txtTimecodeStart.Text = "00:00:00;00"
        Case Else
            txtTimecodeStart.Text = "00:00:00:00"
    End Select
    txtTimecodeStop.Text = txtDuration.Text
End If
If lblFormat.Caption <> "MPA1L3" Then
    If CLng(Val(lblDisplayAspect.Caption) * 9) <= 14 Then
        txtAspect.Text = "4:3"
    ElseIf CLng(Val(lblDisplayAspect.Caption) * 9) <= 16 Then
        txtAspect.Text = Int(Val(lblDisplayAspect.Caption) * 9) & ":9"
    ElseIf Val(lblDisplayAspect.Caption) < 2 Then
        txtAspect.Text = Format(Val(lblDisplayAspect.Caption), "#.00") & ":1"
    Else
        txtAspect.Text = Format(Val(lblDisplayAspect.Caption), "#.0") & ":1"
    End If
    If lblPixelRatio.Caption = "1.000" And lblFormat.Caption <> "AVI" Then
        txtGeometry.Text = "SQUARE"
    ElseIf Val(lblPixelRatio.Caption) < 1.3 Then
        txtGeometry.Text = "NORMAL"
    Else
        txtGeometry.Text = "ANAMORPHIC"
    End If
End If
If lblFormat.Caption = "MPEG 2" Then
    txtFormat.Text = GetAlias(lblFormat.Caption & " " & txtAspect.Text)
    txtStreamType.Text = GetAlias(lblGeneralFormat.Caption)
    If Val(txtVideoBitrate.Text) > 45000 Then
        If txtAudioCodec.Text <> "" Then
            txtVideoCodec.Text = "50I VID + AUD"
        Else
            txtVideoCodec.Text = "50I VID Only"
        End If
    Else
        If txtAudioCodec.Text <> "" Then
            txtVideoCodec.Text = "VID + AUD"
        Else
            txtVideoCodec.Text = "VID Only"
        End If
    End If
ElseIf lblFormat.Caption = "Windows Media" Then
    txtFormat.Text = GetAlias(lblFormat.Caption & " " & txtAspect.Text)
    txtStreamType.Text = ""
    If txtAudioCodec.Text <> "" Then
        txtVideoCodec.Text = "VID + AUD"
    Else
        txtVideoCodec.Text = "VID Only"
    End If
ElseIf lblFormat.Caption = "MPEG" Then
    txtFormat.Text = GetAlias(lblFormat.Caption)
    txtStreamType.Text = GetAlias(lblGeneralFormat.Caption)
    If txtAudioCodec.Text <> "" Then
        txtVideoCodec.Text = "VID + AUD"
    Else
        txtVideoCodec.Text = "VID Only"
    End If
ElseIf lblFormat.Caption = "Wave" Then
    txtFormat.Text = GetAlias(lblFormat.Caption)
    txtGeometry.Text = ""
    txtAspect.Text = ""
Else
    txtFormat.Text = GetAlias(lblFormat.Caption)
End If

If lblSeriesTitle.Caption <> "" Then
    txtSeriesTitle.Text = lblSeriesTitle.Caption
    txtTitle.Text = lblProgrammeTitle.Caption
    txtEpisode.Text = lblEpisodeNumber.Caption
    txtSynopsis.Text = lblSynopsis.Caption
End If

End Sub
