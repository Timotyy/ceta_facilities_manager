VERSION 5.00
Begin VB.Form frmSelectGroup 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Select Project Group"
   ClientHeight    =   3585
   ClientLeft      =   45
   ClientTop       =   360
   ClientWidth     =   4005
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3585
   ScaleWidth      =   4005
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   315
      Left            =   2760
      TabIndex        =   2
      ToolTipText     =   "Close form"
      Top             =   3180
      Width           =   1155
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   315
      Left            =   1500
      TabIndex        =   1
      ToolTipText     =   "Save and continue"
      Top             =   3180
      Width           =   1155
   End
   Begin VB.OptionButton optGroup 
      Caption         =   "Project Group"
      Height          =   195
      Index           =   0
      Left            =   480
      TabIndex        =   0
      ToolTipText     =   "Project Group"
      Top             =   420
      Width           =   3015
   End
End
Attribute VB_Name = "frmSelectGroup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
