Attribute VB_Name = "modCosting"
Option Explicit

Sub PrintBasicInvoiceLineToFile(lp_lngFileNumber As Long, lp_strTheOrderNumber As String, lp_strTheCustomer As String, lp_strTheOrderDate As String, lp_strTheTitle As String, lp_strTheInvoiceNumber As String, lp_strTheInvoiceDate As String, lp_strTheItemCode As String, lp_strTheItemQuantity As String, lp_strTheDuration As String, lp_strTheUnitCharge As String, lp_strTotalNet As String, lp_strTotalVAT As String, lp_strTotalGross As String)

Print #lp_lngFileNumber, lp_strTheOrderNumber & Chr(9);
Print #lp_lngFileNumber, lp_strTheCustomer & Chr(9);
Print #lp_lngFileNumber, lp_strTheOrderDate & Chr(9);
Print #lp_lngFileNumber, lp_strTheTitle & Chr(9);
Print #lp_lngFileNumber, lp_strTheInvoiceNumber & Chr(9);
Print #lp_lngFileNumber, lp_strTheInvoiceDate & Chr(9);
Print #lp_lngFileNumber, lp_strTheItemCode & Chr(9);
Print #lp_lngFileNumber, lp_strTheItemQuantity & Chr(9);
Print #lp_lngFileNumber, lp_strTheDuration & Chr(9);
Print #lp_lngFileNumber, lp_strTheUnitCharge & Chr(9);
Print #lp_lngFileNumber, lp_strTotalNet & Chr(9);
Print #lp_lngFileNumber, lp_strTotalVAT & Chr(9);
Print #lp_lngFileNumber, lp_strTotalGross

End Sub

Sub LockCostingsOnJob(ByVal lp_lngJobID As Long)

If Not CheckAccess("/lockcostingonjob") Then Exit Sub

SetData "job", "costingslocked", "jobID", lp_lngJobID, 1

AddJobHistory lp_lngJobID, "Locked Costings"

End Sub

Sub UnlockCostingsOnJob(ByVal lp_lngJobID As Long)

If Not CheckAccess("/unlockcostingonjob") Then Exit Sub

SetData "job", "costingslocked", "jobID", lp_lngJobID, 0

AddJobHistory lp_lngJobID, "Unlocked Costings"

End Sub

Function InvoiceJob(lp_lngJobID As Long, lp_strInvoiceType As String) As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_rsInvoiceHeader As ADODB.Recordset
    Dim l_strSQL As String
    Dim l_lngInvoiceNumber As Long
    Dim l_strInvoiceCompany As String
    Dim l_strInvoiceAddress As String
    Dim l_strInvoicePostCode As String
    Dim l_strInvoiceCountry As String
    Dim l_strInvoiceNotes As String
    Dim l_strAccountStatus As String
    Dim l_lngCompanyID As Long
    Dim l_intResponse As Integer
    Dim l_rsCostings As ADODB.Recordset
    Dim l_curTotal As Currency
    Dim l_curVAT As Currency
    Dim l_curTotIncVAT As Currency
    Dim l_datInvoiceDate As Date
    Dim l_rstFiles As ADODB.Recordset
    
    'open the costings
    l_strSQL = frmJob.adoCosting.RecordSource
    Set l_rsCostings = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    'run through all the costing rows checking them for posting
    If Not l_rsCostings.EOF Then
        l_rsCostings.MoveFirst
        Do While Not l_rsCostings.EOF
            l_curTotal = l_rsCostings("total")
            l_curVAT = l_rsCostings("vat")
            l_curTotIncVAT = l_rsCostings("totalincludingvat")
            
            'if there is a problem
            If ((IsNull(l_rsCostings("accountcode")) Or l_rsCostings("accountcode") = "")) Or (l_curTotIncVAT <> l_curTotal + l_curVAT) Then
                MsgBox "Problem with costing detail lines - either an item has no account code, or an item has invalid totals. Please correct and re-invoice.", vbCritical, "Problem with Invoice Detail"
                l_rsCostings.Close
                Set l_rsCostings = Nothing
                InvoiceJob = False
                Exit Function
            End If
            
            'loop
            l_rsCostings.MoveNext
        Loop
    End If
    
    'close the costing recordset
    l_rsCostings.Close
    Set l_rsCostings = Nothing
    
    'get the company ID
    l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
    
    'check if there is already an invoice number for this job
    If lp_strInvoiceType = "INVOICE" Then
        l_lngInvoiceNumber = GetData("job", "invoicenumber", "jobID", lp_lngJobID)
    ElseIf lp_strInvoiceType = "CREDIT" Then
        If l_lngInvoiceNumber = 0 Then l_lngInvoiceNumber = GetData("job", "creditnotenumber", "jobID", lp_lngJobID)
    End If
    
    
    If l_lngInvoiceNumber <> 0 Then
        'this job has already been invoiced.
        Dim l_intMsg As Integer
        l_intMsg = MsgBox("This job has been invoiced / credited before. Do you want to re-invoice it now (You will NOT be issued a new invoice / credit note number)?", vbExclamation + vbOKCancel)
        If l_intMsg = vbOK Then
            're-invoice this job
        Else
            Exit Function
        End If
        
    End If
    
    'is this an invoice?
    If lp_strInvoiceType = "INVOICE" Then
        
        'check the account status
        l_strAccountStatus = GetData("company", "accountstatus", "companyID", l_lngCompanyID)
        If UCase(l_strAccountStatus) = "HOLD" Then
            l_intResponse = MsgBox("Company is on Hold - Do you wish to proceed and make the invoice?", vbYesNo, "Company on Hold")
            If l_intResponse = vbNo Then
                InvoiceJob = False
                Exit Function
            End If
        End If
        
    End If
    
    'see if there is already an invoice record for this jobID
    l_strSQL = "SELECT * FROM invoiceheader WHERE jobID = '" & lp_lngJobID & "' AND invoicetype = '" & lp_strInvoiceType & "'"
    Set l_rsInvoiceHeader = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    'if there is no invoice header record then CREATE a new invoice header
    If l_rsInvoiceHeader.EOF Then
        
        Select Case lp_strInvoiceType
        
            'this is an invoice
            Case "INVOICE"
                
                'pick up the invoice date
                l_datInvoiceDate = GetData("job", "invoiceddate", "jobID", lp_lngJobID)
                If l_datInvoiceDate = 0 Then
                    l_datInvoiceDate = Now()
                End If
                
                If l_lngInvoiceNumber = 0 Then
                    'pick up the next invoice number
                    l_lngInvoiceNumber = GetNextSequence("invoicenumber")
                    
                    'save the invoice details back to the job
                    SetData "job", "invoiceddate", "jobID", lp_lngJobID, FormatSQLDate(l_datInvoiceDate)
                    SetData "job", "invoicenumber", "jobID", lp_lngJobID, l_lngInvoiceNumber
                    SetData "job", "invoiceduser", "jobID", lp_lngJobID, g_strUserInitials
                End If
                
                'create the invoice header record
                l_strSQL = "INSERT INTO invoiceheader (invoicenumber, jobID, invoicetype, cdate, cuser, invoicedate) VALUES"
                l_strSQL = l_strSQL & " ('" & l_lngInvoiceNumber & "','" & lp_lngJobID & "','" & lp_strInvoiceType & "','" & FormatSQLDate(Now) & "','" & g_strUserInitials & "','" & FormatSQLDate(l_datInvoiceDate) & "');"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                        
                'update all the costing rows with the invoice number
                l_strSQL = "UPDATE costing SET invoicenumber = '" & l_lngInvoiceNumber & "' WHERE jobID = '" & lp_lngJobID & "'"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                
                'check for two tier pricing
                Dim l_intTwoTierPricing As Integer
                Dim l_lngRefundedCreditNoteNumber As Long
                l_intTwoTierPricing = InStr(GetData("company", "cetaclientcode", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID)), "/two-tier-pricing")
                
                If l_intTwoTierPricing <> 0 Then
                    'save the next credit note number to the refunded credit note number field
                    l_lngRefundedCreditNoteNumber = GetNextSequence("creditnotenumber")
                    SetData "job", "refundedcreditnotenumber", "jobID", lp_lngJobID, l_lngRefundedCreditNoteNumber
                End If
                
            'this is a credit note
            Case "CREDIT"
            
                If l_lngInvoiceNumber = 0 Then
                    'pick up the invoice number
                    l_lngInvoiceNumber = GetNextSequence("creditnotenumber")
                End If
                
                SetData "job", "creditnotenumber", "jobID", lp_lngJobID, l_lngInvoiceNumber
                SetData "job", "creditnotedate", "jobID", lp_lngJobID, FormatSQLDate(Now)
                SetData "job", "creditnoteuser", "jobID", lp_lngJobID, g_strUserInitials
                
                'create the invoice header record
                l_strSQL = "INSERT INTO invoiceheader (creditnotenumber, jobID, invoicetype, cdate, cuser, invoicedate) VALUES"
                l_strSQL = l_strSQL & " ('" & l_lngInvoiceNumber & "','" & lp_lngJobID & "','" & lp_strInvoiceType & "','" & FormatSQLDate(Now) & "','" & g_strUserInitials & "','" & FormatSQLDate(l_datInvoiceDate) & "');"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                
                'create a credit note
                'CreateCreditNote lp_lngJobID, l_lngInvoiceNumber
        
        End Select
        
        'pick up the invoice company name etc
        l_strInvoiceCompany = GetData("company", "invoicecompanyname", "companyID", l_lngCompanyID)
        
        'if there is a company name
        If l_strInvoiceCompany <> "" Then
            l_strInvoiceAddress = GetData("company", "invoiceaddress", "companyID", l_lngCompanyID)
            l_strInvoicePostCode = GetData("company", "invoicepostcode", "companyID", l_lngCompanyID)
            l_strInvoiceCountry = GetData("company", "invoicecountry", "companyID", l_lngCompanyID)
        Else
            l_strInvoiceCompany = GetData("company", "name", "companyID", l_lngCompanyID)
            l_strInvoiceAddress = GetData("company", "address", "companyID", l_lngCompanyID)
            l_strInvoicePostCode = GetData("company", "postcode", "companyID", l_lngCompanyID)
            l_strInvoiceCountry = GetData("company", "country", "companyID", l_lngCompanyID)
        End If
        
    Else
    
        'there is already an invoice record for this job
    
        'pick up the first record (there should only be one)
        l_rsInvoiceHeader.MoveFirst
        
        Select Case lp_strInvoiceType
            'this is an invoice
            Case "INVOICE"
            
                'pick up the invoice number from the invoice number table
                l_lngInvoiceNumber = l_rsInvoiceHeader("invoicenumber")
                
                'collect the invoice company address etc
                l_strInvoiceCompany = Trim(" " & l_rsInvoiceHeader("companyname"))
                l_strInvoiceAddress = Trim(" " & l_rsInvoiceHeader("address"))
                l_strInvoicePostCode = Trim(" " & l_rsInvoiceHeader("postcode"))
                l_strInvoiceCountry = Trim(" " & l_rsInvoiceHeader("country"))
                l_strInvoiceNotes = Trim(" " & l_rsInvoiceHeader("notes1"))
                
                'get the invoice date
                l_datInvoiceDate = GetData("job", "invoiceddate", "jobID", lp_lngJobID)
                
                'set the invoice date to be 0 if its invalid
                If Val(l_datInvoiceDate) = 0 Then l_datInvoiceDate = 0
                
                'if this is a 0 invoice number
                If l_lngInvoiceNumber = 0 Then
                    
                    'pick up a new invoice number
                    l_lngInvoiceNumber = GetNextSequence("invoicenumber")
                    
                    'save the details back to the job
                    SetData "job", "invoiceddate", "jobID", lp_lngJobID, FormatSQLDate(Now)
                    SetData "job", "invoiceduser", "jobID", lp_lngJobID, g_strUserInitials
                End If
                                  
                'set the invoice number back on to the job record
                SetData "job", "invoicenumber", "jobID", lp_lngJobID, l_lngInvoiceNumber
                              
                'SetData "job", "invoicenumber", "jobID", lp_lngJobID, l_lngInvoiceNumber
                If l_datInvoiceDate = 0 Then SetData "job", "invoiceddate", "jobID", lp_lngJobID, FormatSQLDate(l_rsInvoiceHeader("cdate"))
                SetData "job", "invoiceduser", "jobID", lp_lngJobID, g_strUserInitials
                
                'add job history
                AddJobHistory lp_lngJobID, "Re-Finalised"
                
                'update the invoice number to all the costing records
                l_strSQL = "UPDATE costing SET invoicenumber = '" & l_lngInvoiceNumber & "' WHERE jobID = '" & lp_lngJobID & "'"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                
            'this is a credit note
            Case "CREDIT"
            
                'get next credit note number
                l_lngInvoiceNumber = l_rsInvoiceHeader("creditnotenumber")
                
                'collect the address details
                l_strInvoiceCompany = Trim(" " & l_rsInvoiceHeader("companyname"))
                l_strInvoiceAddress = Trim(" " & l_rsInvoiceHeader("address"))
                l_strInvoicePostCode = Trim(" " & l_rsInvoiceHeader("postcode"))
                l_strInvoiceCountry = Trim(" " & l_rsInvoiceHeader("country"))
                l_strInvoiceNotes = Trim(" " & l_rsInvoiceHeader("notes1"))
                
                'save the credit note details back to the job
                SetData "job", "creditnotenumber", "jobID", lp_lngJobID, l_lngInvoiceNumber
                SetData "job", "creditnotedate", "jobID", lp_lngJobID, FormatSQLDate(l_rsInvoiceHeader("cdate"))
                SetData "job", "creditnoteuser", "jobID", lp_lngJobID, g_strUserInitials
                
                'raise the credit note
                'CreateCreditNote lp_lngJobID, l_lngInvoiceNumber
        End Select
        
    End If
    
    'close the invoice header table
    l_rsInvoiceHeader.Close
    Set l_rsInvoiceHeader = Nothing
    
    'if they dont want to prompt for an invoice address (setting)
    If g_optDontPromptForInvoiceAddress = 0 Then
        
        'populate the form with the existing values
        frmAdjustClient.txtCompanyName.Text = l_strInvoiceCompany
        frmAdjustClient.txtAddress.Text = l_strInvoiceAddress
        frmAdjustClient.txtPostCode.Text = l_strInvoicePostCode
        frmAdjustClient.txtCountry.Text = l_strInvoiceCountry
        frmAdjustClient.txtNotes.Text = l_strInvoiceNotes
        
        'pop up the form allowing the user to edit the invoice address
        frmAdjustClient.Show vbModal
        
        'pickup new values
        l_strInvoiceCompany = frmAdjustClient.txtCompanyName.Text
        l_strInvoiceAddress = frmAdjustClient.txtAddress.Text
        l_strInvoicePostCode = frmAdjustClient.txtPostCode.Text
        l_strInvoiceCountry = frmAdjustClient.txtCountry.Text
        l_strInvoiceNotes = frmAdjustClient.txtNotes.Text
        
        'unload the adjust form
        Unload frmAdjustClient
        Set frmAdjustClient = Nothing
    
    End If
    
    'update the invoice header record to include the company name etc
    l_strSQL = "UPDATE invoiceheader SET companyname = '" & QuoteSanitise(l_strInvoiceCompany) & "', address = '" & QuoteSanitise(l_strInvoiceAddress) & "', postcode = '" & QuoteSanitise(l_strInvoicePostCode) & "', country = '" & QuoteSanitise(l_strInvoiceCountry) & "', notes1 = '" & QuoteSanitise(l_strInvoiceNotes) & "' WHERE jobID = '" & lp_lngJobID & "' AND invoicetype = '" & lp_strInvoiceType & "'"
    Set l_rsInvoiceHeader = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    'save the completed dates to a job if there is none already
    If GetData("job", "completeduser", "jobID", lp_lngJobID) = "" Then
        SetData "job", "completeddate", "jobID", lp_lngJobID, FormatSQLDate(Now)
        SetData "job", "completeduser", "jobID", lp_lngJobID, g_strUserInitials
    End If
    
    CreateInvoiceHeader2 "JOB", lp_lngJobID, lp_strInvoiceType, GetData("job", "projectnumber", "jobID", lp_lngJobID), CStr(l_lngInvoiceNumber), l_datInvoiceDate, l_lngCompanyID, l_strInvoiceCompany, l_strInvoiceAddress, l_strInvoicePostCode, l_strInvoiceCountry
    
    'close the rs and database
    InvoiceJob = True
    
    Dim l_strJobType As String
    l_strJobType = Trim(GetData("job", "jobtype", "jobID", lp_lngJobID))
    
    
    'update the status to be costed - if its not a hire job of course!!
    If (g_optAllowCostedJobsWhenNotComplete <> 0) Or (UCase(l_strJobType) <> "HIRE" And UCase(l_strJobType) <> "EDIT") Or (UCase(GetData("job", "fd_status", "jobID", lp_lngJobID)) = "COMPLETED") Then
        UpdateJobStatus lp_lngJobID, "Costed", , True
    
        'send to accounts, if the setting is set
        If g_optSendToAccountsOnFinalise = 1 Then UpdateJobStatus lp_lngJobID, "Sent To Accounts"
    Else
        SetData "job", "costeddate", "jobID", lp_lngJobID, FormatSQLDate(Now)
        SetData "job", "costeduser", "jobID", lp_lngJobID, g_strUserInitials
        
    End If
    
    AddJobHistory lp_lngJobID, "Finalised job costings. Invoice number allocated = " & l_lngInvoiceNumber
    
    'Disney checks - Update the tracker to say that it is billed, and also delete source materials
    Dim l_lngAgileNumber As Long
    If InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/disney") > 0 Then
        SetData "disneytracker", "billed", "jobID", lp_lngJobID, 1
        SetData "disneytracker", "workdone", "jobID", lp_lngJobID, 1
        
'        l_lngAgileNumber = Val(GetData("disneytracker", "agilenumber", "jobID", lp_lngJobID))
'        If l_lngAgileNumber <> 0 Then
'            'Source materials were found
'            Dim l_lngSourceCount As Long, l_rst As ADODB.Recordset
'
'            Set l_rst = ExecuteSQL("SELECT count(agilenumber) FROM disneytracker WHERE jobID = " & lp_lngJobID & ";", g_strExecuteError)
'            CheckForSQLError
'            l_lngSourceCount = l_rst(0)
'            l_rst.Close
'            Set l_rst = Nothing
'            If MsgBox("Do you need to keep them?", vbYesNo, "There were " & l_lngSourceCount & " potential source files associated with this job.") = vbNo Then
'                MsgBox "When code is written to delete these files it will run here.", vbInformation, "Placeholder"
'            End If
'        End If
'
'        Set l_rst = ExecuteSQL("SELECT count(eventID) FROM vwEventsWithLibraryFormat WHERE system_deleted = 0 AND jobID = " & lp_lngJobID & " AND companyID = " & l_lngCompanyID & " AND online <> 0 AND format = 'DISCSTORE';", g_strExecuteError)
'        CheckForSQLError
'        If l_rst.RecordCount > 0 Then
'            l_lngSourceCount = l_rst(0)
'            If l_lngSourceCount <> 0 Then
'                If MsgBox("Do you need to keep them?", vbYesNo, "There were " & l_lngSourceCount & " product files associated with this job.") = vbNo Then
'                    MsgBox "When code is written to delete these files it will run here.", vbInformation, "Placeholder"
'                End If
'            End If
'        End If
'        l_rst.Close
'        Set l_rst = Nothing
    End If
    
    'Media Checks - Check whether any files were made for this job, and if so offer to Dump them to AutoDelete
    If GetCETASetting("UseAutoDeleteWhenFinalisingJobs") <> 0 Then
        If InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/NoAutoDelete") <= 0 Then
            l_strSQL = "SELECT count(eventID) FROM vwEventsWithLibraryFormat WHERE system_deleted = 0 AND format = 'DISCSTORE' AND jobID = " & lp_lngJobID & ";"
            If GetCount(l_strSQL) > 0 Then
                If MsgBox("There are " & GetCount(l_strSQL) & " files made for this job." & vbCrLf & "Should these files be moved to the AutoDelete process", vbYesNo) = vbYes Then
                    l_strSQL = "SELECT eventID, libraryID FROM vwEventsWithLibraryFormat WHERE system_deleted = 0 AND format = 'DISCSTORE' AND jobID = " & lp_lngJobID & ";"
                    Set l_rstFiles = ExecuteSQL(l_strSQL, g_strExecuteError)
                    If l_rstFiles.RecordCount > 0 Then
                        l_rstFiles.MoveFirst
                        Do While Not l_rstFiles.EOF
                            If Not CheckFileOnDeliveryStore(l_rstFiles("eventID")) Then
                                l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, jobID, RequestName, RequesterEmail) VALUES ("
                                l_strSQL = l_strSQL & l_rstFiles("eventID") & ", "
                                l_strSQL = l_strSQL & l_rstFiles("libraryID") & ", "
                                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move to AutoDelete") & ", "
                                l_strSQL = l_strSQL & lp_lngJobID & ", "
                                l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                                Debug.Print l_strSQL
                                ExecuteSQL l_strSQL, g_strExecuteError
                                CheckForSQLError
                            End If
                            l_rstFiles.MoveNext
                        Loop
                    End If
                    l_rstFiles.Close
                    Set l_rstFiles = Nothing
                Else
                    If MsgBox("Are you sure that you don't want to AutoDelete these files", vbYesNo + vbDefaultButton2) = vbNo Then
                        l_strSQL = "SELECT eventID, libraryID FROM vwEventsWithLibraryFormat WHERE system_deleted = 0 AND format = 'DISCSTORE' AND jobID = " & lp_lngJobID & ";"
                        Set l_rstFiles = ExecuteSQL(l_strSQL, g_strExecuteError)
                        If l_rstFiles.RecordCount > 0 Then
                            l_rstFiles.MoveFirst
                            Do While Not l_rstFiles.EOF
                                If Not CheckFileOnDeliveryStore(l_rstFiles("eventID")) Then
                                    l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, jobID, RequestName, RequesterEmail) VALUES ("
                                    l_strSQL = l_strSQL & l_rstFiles("eventID") & ", "
                                    l_strSQL = l_strSQL & l_rstFiles("libraryID") & ", "
                                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move to AutoDelete") & ", "
                                    l_strSQL = l_strSQL & lp_lngJobID & ", "
                                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                                    Debug.Print l_strSQL
                                    ExecuteSQL l_strSQL, g_strExecuteError
                                    CheckForSQLError
                                End If
                                l_rstFiles.MoveNext
                            Loop
                        End If
                        l_rstFiles.Close
                        Set l_rstFiles = Nothing
                    End If
                End If
            End If
            l_strSQL = "SELECT count(eventID) FROM requiredmedia WHERE eventID IN (SELECT eventID FROM vwEventsWithLibraryFormat WHERE system_deleted = 0 AND format = 'DISCSTORE') AND jobID = " & lp_lngJobID & ";"
            If GetCount(l_strSQL) > 0 Then
                If MsgBox("There are " & GetCount(l_strSQL) & " files used as masters by this job." & vbCrLf & "Should these files be moved to the AutoDelete process", vbYesNo) = vbYes Then
                    l_strSQL = "SELECT eventID FROM requiredmedia WHERE eventID IN (SELECT eventID FROM vwEventsWithLibraryFormat WHERE system_deleted = 0 AND format = 'DISCSTORE') AND jobID = " & lp_lngJobID & ";"
                    Set l_rstFiles = ExecuteSQL(l_strSQL, g_strExecuteError)
                    If l_rstFiles.RecordCount > 0 Then
                        l_rstFiles.MoveFirst
                        Do While Not l_rstFiles.EOF
                            If Trim(" " & GetDataSQL("SELECT requiredmediaID FROM requiredmedia WHERE eventID = " & l_rstFiles("eventID") & " AND jobID <> " & lp_lngJobID & ":")) = "" Then
                                l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, jobID, RequestName, RequesterEmail) VALUES ("
                                l_strSQL = l_strSQL & l_rstFiles("eventID") & ", "
                                l_strSQL = l_strSQL & GetData("events", "libraryID", "eventID", l_rstFiles("eventID")) & ", "
                                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move to AutoDelete") & ", "
                                l_strSQL = l_strSQL & lp_lngJobID & ", "
                                l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                                Debug.Print l_strSQL
                                ExecuteSQL l_strSQL, g_strExecuteError
                                CheckForSQLError
                            End If
                            l_rstFiles.MoveNext
                        Loop
                    End If
                    l_rstFiles.Close
                    Set l_rstFiles = Nothing
                Else
                    If MsgBox("Are you sure that you don't want to AutoDelete these files", vbYesNo + vbDefaultButton2) = vbNo Then
                    l_strSQL = "SELECT eventID FROM requiredmedia WHERE eventID IN (SELECT eventID FROM vwEventsWithLibraryFormat WHERE system_deleted = 0 AND format = 'DISCSTORE') AND jobID = " & lp_lngJobID & ";"
                        Set l_rstFiles = ExecuteSQL(l_strSQL, g_strExecuteError)
                        If l_rstFiles.RecordCount > 0 Then
                            l_rstFiles.MoveFirst
                            Do While Not l_rstFiles.EOF
                                If Trim(" " & GetDataSQL("SELECT requiredmediaID FROM requiredmedia WHERE eventID = " & l_rstFiles("eventID") & " AND jobID <> " & lp_lngJobID & ":")) = "" Then
                                    l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, jobID, RequestName, RequesterEmail) VALUES ("
                                    l_strSQL = l_strSQL & l_rstFiles("eventID") & ", "
                                    l_strSQL = l_strSQL & GetData("events", "libraryID", "eventID", l_rstFiles("eventID")) & ", "
                                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move to AutoDelete") & ", "
                                    l_strSQL = l_strSQL & lp_lngJobID & ", "
                                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                                    Debug.Print l_strSQL
                                    ExecuteSQL l_strSQL, g_strExecuteError
                                    CheckForSQLError
                                End If
                                l_rstFiles.MoveNext
                            Loop
                        End If
                        l_rstFiles.Close
                        Set l_rstFiles = Nothing
                    End If
                End If
            End If
        End If
    End If
    
    ShowJob lp_lngJobID, g_intDefaultTab, False
'    ShowJob lp_lngJobID, g_intDefaultTab, False
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function UpdateCostingRow(ByVal lp_lngCostingID As Long, ByVal lp_lngCompanyID As Long) As Boolean

Dim l_con As New ADODB.Connection
l_con.Open g_strConnection

Dim l_strSQL As String
l_strSQL = "SELECT * FROM costing WHERE costingID = '" & lp_lngCostingID & "';"

Dim l_rstCosting As New ADODB.Recordset
l_rstCosting.Open l_strSQL, l_con, adOpenDynamic, adLockReadOnly

If Not l_rstCosting.EOF Then

    'fill the form with the details
    With frmEditCostingRow
        .txtPosition.Text = Format(l_rstCosting("ratecardorderby"))
        .cmbCostCode.Text = Trim(" " & l_rstCosting("costcode"))
        
        'insert the companyID
        .lblCompanyID.Caption = lp_lngCompanyID
            
        'load the basic details - and the history
        .cmbCostCode_Click
        
        .txtAccountCode.Text = Trim(" " & l_rstCosting("accountcode"))
        .txtDescription.Text = Trim(" " & l_rstCosting("description"))
        .txtQuantity.Text = Trim(" " & l_rstCosting("quantity"))
        .txtTime.Text = Trim(" " & l_rstCosting("fd_time"))
        
        'check that there is a valid time
        If .txtTime.Text = "" Or .txtTime.Text = "0" Then
            .txtTime.Text = g_intDefaultTimeWhenZero
        End If
        
        .cmbUnit.Text = Trim(" " & l_rstCosting("unit"))
        .txtDiscount.Text = Trim(" " & l_rstCosting("discount"))
        .txtUnitCharge.Text = Trim(" " & l_rstCosting("unitcharge"))
        .txtRateCardPrice.Text = Trim(" " & (l_rstCosting("ratecardprice")))
    
        .lblJobID.Caption = Trim(" " & l_rstCosting("jobID"))
    
    End With

    'pick up the total costs
    frmEditCostingRow.cmdCalculateTotals.Value = True

    

    
    'show the form
    frmEditCostingRow.Show vbModal
    
    Dim l_strCancelled As String
    l_strCancelled = frmEditCostingRow.Tag
    
    Dim l_strRateCardCategory As String
    l_strRateCardCategory = GetData("ratecard", "category", "cetacode", frmEditCostingRow.cmbCostCode.Text)
    
    'check if the user cancelled the dialog
    If l_strCancelled = "CANCELLED" Then
        UpdateCostingRow = False
        GoTo PROC_Close
    End If
    
    With frmEditCostingRow
    
        'build the SQL
        l_strSQL = "UPDATE costing SET "
        l_strSQL = l_strSQL & " ratecardorderby = '" & .txtPosition.Text & "',"
        l_strSQL = l_strSQL & " ratecardcategory = '" & l_strRateCardCategory & "',"
        l_strSQL = l_strSQL & " costcode = '" & .cmbCostCode.Text & "',"
        l_strSQL = l_strSQL & " accountcode = '" & .txtAccountCode.Text & "',"
        l_strSQL = l_strSQL & " description = '" & .txtDescription.Text & "',"
        l_strSQL = l_strSQL & " quantity = '" & .txtQuantity.Text & "',"
        l_strSQL = l_strSQL & " fd_time = '" & .txtTime.Text & "',"
        l_strSQL = l_strSQL & " unit = '" & .cmbUnit.Text & "',"
        l_strSQL = l_strSQL & " discount = '" & .txtDiscount.Text & "',"
        l_strSQL = l_strSQL & " unitcharge = '" & .txtUnitCharge.Text & "',"
        l_strSQL = l_strSQL & " ratecardprice = '" & .txtRateCardPrice.Text & "',"
        l_strSQL = l_strSQL & " total = '" & Val(.txtNetTotal.Text) & "',"
        l_strSQL = l_strSQL & " vat= '" & Val(.txtVATTotal.Text) & "',"
        l_strSQL = l_strSQL & " totalincludingvat= '" & Val(.txtGrossTotal.Text) & "',"
        l_strSQL = l_strSQL & " ratecardtotal= '" & Val(.txtRateCardNet.Text) & "'"
        l_strSQL = l_strSQL & " WHERE costingID = '" & lp_lngCostingID & "';"
    End With
    l_con.Execute l_strSQL
    
    UpdateCostingRow = True
    
End If

PROC_Close:

'unload the form
Unload frmEditCostingRow
Set frmEditCostingRow = Nothing

l_rstCosting.Close
Set l_rstCosting = Nothing

l_con.Close
Set l_con = Nothing

End Function
Function GetCompanyDiscount(ByVal lp_lngCompanyID As Long, ByVal lp_strCETACode As String) As Single
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    l_strSQL = "SELECT discount FROM discount WHERE companyID = " & lp_lngCompanyID & " AND cetacode = '" & lp_strCETACode & "'"
    
    Dim l_rstDiscount As New ADODB.Recordset
    Set l_rstDiscount = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    CheckForSQLError
    
    If Not l_rstDiscount.EOF Then
        GetCompanyDiscount = l_rstDiscount("discount")
    Else
        'If no specific discount fourn then check if there is a general discount for this company in the company table.
        l_rstDiscount.Close
            
        l_strSQL = "SELECT masterdiscount FROM company WHERE companyID = " & lp_lngCompanyID
        
        Set l_rstDiscount = ExecuteSQL(l_strSQL, g_strExecuteError)
        CheckForSQLError
        
        If Not l_rstDiscount.EOF Then
            GetCompanyDiscount = Val(Trim(" " & l_rstDiscount("masterdiscount")))
        Else
            GetCompanyDiscount = 0
        End If
    End If
    
    l_rstDiscount.Close
    Set l_rstDiscount = Nothing
    
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function DeleteCostings(lp_lngJobID As Long, lp_strCostingType As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    l_strSQL = "DELETE FROM costing WHERE jobID = '" & lp_lngJobID & "'"
    
    Select Case lp_strCostingType
        Case "INVOICE"
            l_strSQL = l_strSQL & " AND (originalcostingID IS NULL or originalcostingID = 0)"
        Case "CREDIT"
            l_strSQL = l_strSQL & " AND originalcostingID IS NOT NULL"
    End Select
    
'    If g_intWebInvoicing = 1 Then
'        l_strSQL = l_strSQL & " AND accountstransactionID = 0"
'    End If
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetUnitCharge(lp_intRateCard As Integer, lp_strFormat As String, lp_lngCompanyID As Long) As Double
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_rsRatecards As New ADODB.Recordset
    Dim l_blnFoundPrice As Boolean
    
    l_blnFoundPrice = False
    
    
    
    GetUnitCharge = 0
    
    If lp_lngCompanyID = 0 Then
        l_strSQL = "Select * FROM ratecard WHERE cetacode = '" & QuoteSanitise(lp_strFormat) & "' and companyID = 0"
    Else
        l_strSQL = "Select * FROM ratecard WHERE cetacode = '" & QuoteSanitise(lp_strFormat) & "' and (companyID = 0 or companyID = '" & lp_lngCompanyID & "')"
    End If
    
    Set l_rsRatecards = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If l_rsRatecards.EOF <> True Then
        l_rsRatecards.MoveFirst
    End If
    
    Do While Not l_rsRatecards.EOF
        
        If l_rsRatecards("companyID") = 0 And l_blnFoundPrice = False Then
            GetUnitCharge = Val(Format(l_rsRatecards("Price" & lp_intRateCard)))
        End If
        If l_rsRatecards("companyID") = lp_lngCompanyID Then
            GetUnitCharge = Val(Format(l_rsRatecards("Price" & lp_intRateCard)))
            l_blnFoundPrice = True
            Exit Do
        End If
        
        l_rsRatecards.MoveNext
        
    Loop
    
    l_rsRatecards.Close
    Set l_rsRatecards = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetStockCharge(lp_strFormat As String, lp_lngCompanyID As Long) As Double
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_rsRatecards As New ADODB.Recordset
    Dim l_blnFoundPrice As Boolean
    
    l_blnFoundPrice = False
    
    GetStockCharge = 0
    
    If lp_lngCompanyID = 0 Then
        l_strSQL = "Select * FROM ratecard WHERE cetacode = '" & QuoteSanitise(lp_strFormat) & "' and companyID = 0"
    Else
        l_strSQL = "Select * FROM ratecard WHERE cetacode = '" & QuoteSanitise(lp_strFormat) & "' and (companyID = 0 or companyID = '" & lp_lngCompanyID & "')"
    End If
    
    Set l_rsRatecards = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If l_rsRatecards.EOF <> True Then
        l_rsRatecards.MoveFirst
    End If
    
    Do While Not l_rsRatecards.EOF
        
        If l_rsRatecards("companyID") = 0 And l_blnFoundPrice = False Then
            GetStockCharge = Val(Format(l_rsRatecards("includedstockprice")))
        End If
        If l_rsRatecards("companyID") = lp_lngCompanyID Then
            GetStockCharge = Val(Format(l_rsRatecards("includedstockprice")))
            l_blnFoundPrice = True
            Exit Do
        End If
        
        l_rsRatecards.MoveNext
        
    Loop
    
    l_rsRatecards.Close
    Set l_rsRatecards = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetStockNominal(lp_strFormat As String, lp_lngCompanyID As Long) As String
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_rsRatecards As New ADODB.Recordset
    Dim l_blnFoundPrice As Boolean
    
    l_blnFoundPrice = False
    
    GetStockNominal = ""
    
    If lp_lngCompanyID = 0 Then
        l_strSQL = "Select * FROM ratecard WHERE cetacode = '" & QuoteSanitise(lp_strFormat) & "' and companyID = 0"
    Else
        l_strSQL = "Select * FROM ratecard WHERE cetacode = '" & QuoteSanitise(lp_strFormat) & "' and (companyID = 0 or companyID = '" & lp_lngCompanyID & "')"
    End If
    
    Set l_rsRatecards = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If l_rsRatecards.EOF <> True Then
        l_rsRatecards.MoveFirst
    End If
    
    Do While Not l_rsRatecards.EOF
        
        If l_rsRatecards("companyID") = 0 And l_blnFoundPrice = False Then
            GetStockNominal = Trim(" " & l_rsRatecards("includedstocknominal"))
        End If
        If l_rsRatecards("companyID") = lp_lngCompanyID Then
            GetStockNominal = Trim(" " & l_rsRatecards("includedstocknominal"))
            l_blnFoundPrice = True
            Exit Do
        End If
        
        l_rsRatecards.MoveNext
        
    Loop
    
    l_rsRatecards.Close
    Set l_rsRatecards = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetVATRate(lp_strVATCode As String) As Single
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    
    l_strSQL = "SELECT vatrate FROM vatrate WHERE vatcode = '" & lp_strVATCode & "'"
    
    Dim l_rstGetData As New ADODB.Recordset
    Set l_rstGetData = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    CheckForSQLError
    
    If Not l_rstGetData.EOF Then
        GetVATRate = Format(l_rstGetData("vatrate"))
    Else
        'Vat code didn't exist - tell them and then set it to code 1
        'MsgBox "VAT Code '" & lp_strVATCode & "' does not exist - setting Vat rate to Base UK VAT", vbInformation, "VAT Rate"
        l_strSQL = "SELECT vatrate FROM vatrate WHERE vatcode = '1'"
        l_rstGetData.Close
        Set l_rstGetData = ExecuteSQL(l_strSQL, g_strExecuteError)
        If Not l_rstGetData.EOF Then
            GetVATRate = Format(l_rstGetData("vatrate"))
        Else
            MsgBox "Could not load base UK VAT Rate.", vbCritical, "VAT Rate"
            GetVATRate = 0
        End If
    End If
    
    l_rstGetData.Close
    Set l_rstGetData = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetTotalCostsForMCS(lp_lngMCSID As Long, lp_strField As String, Optional lp_strCategory As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_rstTotal As New ADODB.Recordset
    
    l_strSQL = "SELECT SUM(" & lp_strField & ") FROM costing INNER JOIN job ON job.jobID = costing.jobID WHERE job.costingsheetID = '" & lp_lngMCSID & "'"
    
    If lp_strCategory <> "" Then
        l_strSQL = l_strSQL & " AND ratecardcategory = '" & lp_strCategory & "'"
    End If
    
    Set l_rstTotal = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    CheckForSQLError
    
    GetTotalCostsForMCS = Format(l_rstTotal(0))
    
    l_rstTotal.Close
    Set l_rstTotal = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetTotalCostsForJob(lp_lngJobID As Long, lp_strField As String, lp_strCostingType As String) As Double
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_rstTotal As New ADODB.Recordset
    
    
    '((creditnotenumber IS NULL) OR (creditnotenumber = 0)) AND (jobID = '" & lp_lngJobID & "')
    
    Select Case UCase(lp_strCostingType)
        Case "INVOICE"
            l_strSQL = "SELECT SUM(" & lp_strField & ") FROM costing WHERE ((creditnotenumber IS NULL) OR (creditnotenumber = 0)) AND (jobID = '" & lp_lngJobID & "')"
        Case "CREDIT"
            Dim l_lngCreditNoteNumber As Long
            l_lngCreditNoteNumber = GetData("job", "creditnotenumber", "jobID", lp_lngJobID)
            
            l_strSQL = "SELECT SUM(" & lp_strField & ") FROM costing WHERE jobID = '" & lp_lngJobID & "' AND (creditnotenumber = " & l_lngCreditNoteNumber & ")"
    End Select
    
    
    Set l_rstTotal = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    
    CheckForSQLError
    
    If IsNull(l_rstTotal(0)) Then
        GetTotalCostsForJob = 0
    Else
        GetTotalCostsForJob = l_rstTotal(0)
    End If
    
    'If Val(GetTotalCostsForJob) = 0 Then GetTotalCostsForJob = 0
    
    l_rstTotal.Close
    Set l_rstTotal = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function CostingsExist(lp_lngJobID As Long) As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    l_strSQL = "SELECT count(costingID) FROM costing WHERE jobID = " & lp_lngJobID
    
'    If g_intWebInvoicing = 1 Then
'        l_strSQL = l_strSQL & " AND accountstransactionID = 0"
'    End If
    
    Dim l_rstCosting As New ADODB.Recordset
    Set l_rstCosting = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    If l_rstCosting(0) = 0 Then
        CostingsExist = False
    Else
        CostingsExist = True
    End If
    
    l_rstCosting.Close
    Set l_rstCosting = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function CreateCostingSheetFromJob(lp_lngJobID As Long) As Long

Dim l_strSQL As String

l_strSQL = "SELECT * FROM job WHERE jobID = '" & lp_lngJobID & "';"

Dim l_rstJob As New ADODB.Recordset
Set l_rstJob = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If l_rstJob.EOF Then
    MsgBox "Job not found. Could not create a costing sheet.", vbExclamation
    Exit Function
End If


l_strSQL = "INSERT INTO costingsheet (specifiedjobID, fd_status, clientordernumber, accountnumber, companyname, companyID, title, "
l_strSQL = l_strSQL & " telephone, projectnumber, projectID, createddate, createduser, productname, productID, fax, contactname, contactID) VALUES ("

Dim l_strOrderNumber As String
Dim l_strAccountNumber  As String
Dim l_strCompanyName As String
Dim l_lngCompanyID  As Long
Dim l_strTitle As String
Dim l_strTelephone As String
Dim l_lngProjectNumber As Long
Dim l_lngProjectID As Long
Dim l_strProductName As String
Dim l_lngProductID As Long
Dim l_strFax As String
Dim l_strContactName As String
Dim l_lngContactID As Long


'get the values
l_strOrderNumber = Format(l_rstJob("orderreference"))
l_strAccountNumber = GetData("company", "accountcode", "companyID", l_rstJob("companyID"))
l_strCompanyName = Format(l_rstJob("companyname"))
l_lngCompanyID = Format(l_rstJob("companyID"))
l_strTitle = Format(l_rstJob("title1"))
l_strTelephone = Format(l_rstJob("telephone"))
l_lngProjectNumber = Format(l_rstJob("projectnumber"))
l_lngProjectID = Format(l_rstJob("projectID"))
l_strProductName = Format(l_rstJob("productname"))
l_lngProductID = Format(l_rstJob("productID"))
l_strFax = Format(l_rstJob("fax"))
l_strContactName = Format(l_rstJob("contactname"))
l_lngContactID = Format(l_rstJob("contactID"))


l_strSQL = l_strSQL & "'" & lp_lngJobID & "', "
l_strSQL = l_strSQL & "'" & "Hold Cost" & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strOrderNumber) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strAccountNumber) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strCompanyName) & "', "
l_strSQL = l_strSQL & "'" & l_lngCompanyID & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTelephone) & "', "
l_strSQL = l_strSQL & "'" & l_lngProjectNumber & "', "
l_strSQL = l_strSQL & "'" & l_lngProjectID & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strProductName) & "', "
l_strSQL = l_strSQL & "'" & l_lngProductID & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strFax) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strContactName) & "', "
l_strSQL = l_strSQL & "'" & l_lngContactID & "');"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

CreateCostingSheetFromJob = g_lngLastID

End Function


Sub CreateCreditNote(lp_lngJobID As Long, lp_lngCreditNoteNumber As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    'we should check here to see if this has already been credited!
    
    Dim l_strNewSQL As String
    Dim l_rstOriginalCostings As New ADODB.Recordset
    Dim l_strSQL As String
    Dim l_rstNewCostings As New ADODB.Recordset
    Dim l_intFields As Integer
    
    'pick up all the costing records for this job
    l_strSQL = "SELECT * FROM costing WHERE jobID = '" & lp_lngJobID & "' AND (creditnotenumber IS NULL OR creditnotenumber = 0) ORDER BY costingID"
    Set l_rstOriginalCostings = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    'set this job's credit note number field to be the new credit note number
    SetData "job", "creditnotenumber", "jobID", lp_lngJobID, lp_lngCreditNoteNumber
    
    'get ready to create costings for this credit - check whether new lines already exist
    l_strNewSQL = "SELECT * FROM costing WHERE creditnotenumber = '" & lp_lngCreditNoteNumber & "'"
    Set l_rstNewCostings = ExecuteSQL(l_strNewSQL, g_strExecuteError)
    CheckForSQLError
    
    If l_rstNewCostings.RecordCount <= 0 Then
        'copy all the costing lines and insert the original costing ID info
        If Not l_rstOriginalCostings.EOF Then
            
            'move to the first original costing line
            l_rstOriginalCostings.MoveFirst
            
            'loop through all the original costing records
            Do While Not l_rstOriginalCostings.EOF
                
                'add a new costing record (copy the original one to the new recordset)
                l_rstNewCostings.AddNew
                l_rstNewCostings.Fields("creditnotenumber") = lp_lngCreditNoteNumber
    
                'loop through all the costing fields
                For l_intFields = 0 To l_rstOriginalCostings.Fields.Count - 1
                    Select Case l_rstOriginalCostings.Fields(l_intFields).Name
                        Case "costingID", "invoicenumber", "creditnotenumber"
                            'do nothing
    
                        Case "originalcostingID"
                            'insert the original costing ID
                            l_rstNewCostings.Fields(l_intFields).Value = l_rstOriginalCostings("costingID").Value
                            
                        Case "total", "quantity", "vat", "totalincludingvat", "inflatedtotal", "inflatedvat", "inflatedtotalincludingvat", "refundedtotal", "refundedvat", "refundedtotalincludingvat", "ratecardtotal"
                            l_rstNewCostings.Fields(l_intFields).Value = 0 - Val(l_rstOriginalCostings(l_intFields).Value)
                            
                        Case Else
                            l_rstNewCostings.Fields(l_intFields).Value = l_rstOriginalCostings(l_intFields).Value
                            
                    End Select
                Next
                'save the record
                l_rstNewCostings.Update
                
                'loop to the next item
                l_rstOriginalCostings.MoveNext
            Loop
            
            
        End If
    
    End If
    
    l_rstNewCostings.Close
    Set l_rstNewCostings = Nothing

    'close the recordset
    l_rstOriginalCostings.Close
    Set l_rstOriginalCostings = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Function CreateCostingLine(ByVal lp_lngItemID As Long, ByVal lp_lngJobID As Long, ByVal lp_strCopyType As String, ByVal lp_strDescription As String, _
                        ByVal lp_strCETACostCode As String, ByVal lp_dblQuantity As Double, ByVal lp_sngTime As Single, ByVal lp_dblUnitCharge As Double, _
                        ByVal lp_sngDiscount As Single, ByVal lp_sngVATRate As Single, ByVal lp_blnTwoTier As Integer, ByVal lp_dblRateCardPrice As Double, _
                        ByVal lp_dblMinimumCharge As Double, ByVal lp_dblRatecardMinimumCharge As Double, Optional ByVal lp_lngCostingDetailID As Long, _
                        Optional ByVal lp_strUnit As String) As Long

'TVCodeTools ErrorEnablerStart
On Error GoTo PROC_ERR
'TVCodeTools ErrorEnablerEnd
    
    Dim l_strDescription As String
    Dim l_strUnit As String
    
    l_strDescription = QuoteSanitise(lp_strDescription)
    
    If lp_strCopyType <> "S" Then
        
        'delete any existing line for this jobdetailID
        Dim l_strSQL As String
        l_strSQL = "DELETE FROM costing WHERE jobdetailID = " & lp_lngItemID & " AND copytype = '" & lp_strCopyType & "'"
        
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
    End If
    
    Dim l_dblRateCardTotal As Double
    
    Dim l_strAccountCode As String, l_strStockNominalCode As String, l_dblStockUnitCharge  As Double
    Dim l_strPriorityCode As String, l_strPriorityStockCode As String
    l_strAccountCode = Trim(" " & GetData("ratecard", "nominalcode", "cetacode", lp_strCETACostCode))
    l_strStockNominalCode = Trim(" " & GetData("ratecard", "includedstocknominalcode", "cetacode", lp_strCETACostCode))
    If InStr(GetData("company", "cetaclientcode", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID)), "/posttononrevenuebalancesheet") > 0 Then
        l_strPriorityCode = "1464002"
    Else
        l_strPriorityCode = GetDataSQL("SELECT TOP 1 VDMS_Charge_Code from ratecard where cetacode = '" & lp_strCETACostCode & "' AND companyID = " & GetData("job", "companyID", "jobID", lp_lngJobID))
        If l_strPriorityCode = "" Then l_strPriorityCode = GetData("ratecard", "VDMS_Charge_Code", "cetacode", lp_strCETACostCode)
    End If
    l_strPriorityStockCode = GetData("ratecard", "VDMS_Charge_Code", "nominalcode", l_strStockNominalCode)
    If l_strPriorityStockCode <> "" Then
        If InStr(GetData("company", "cetaclientcode", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID)), "/posttononrevenuebalancesheet") > 0 Then
            l_strPriorityStockCode = "1464002"
        End If
    End If
    l_dblStockUnitCharge = Val(Trim(" " & GetData("ratecard", "includedstockprice", "cetacode", lp_strCETACostCode)))
    
    'in here sort out quantity thing... if need be...
    If g_optHideTimeColumn = 1 Then
        lp_dblQuantity = lp_sngTime
        lp_sngTime = 1
    End If
    
    If lp_strCopyType = "O" Or lp_strCopyType = "T" Or lp_strCopyType = "P" Then
    
        If lp_strCopyType = "R" Then lp_dblQuantity = (lp_dblQuantity * lp_sngTime)
        
        If g_optDontMakeMachineTimeADecimal = 1 Then
            lp_sngTime = 60
        Else
            lp_sngTime = 1
        End If
        
    End If
    
    If lp_strCopyType = "I" Or lp_strCopyType = "IS" Then
        lp_sngTime = 0
    End If
    
    If lp_strUnit <> "" Then
        l_strUnit = lp_strUnit
    Else
        l_strUnit = GetData("ratecard", "unit", "cetacode", lp_strCETACostCode)
    End If
    
    'work out the base ratecard total
    If lp_strCopyType <> "I" And lp_strCopyType <> "IS" Then
        If g_optDontMakeMachineTimeADecimal = 1 Then
            If lp_strCopyType <> "S" Then
                If l_strUnit <> "M" Then
                    l_dblRateCardTotal = lp_dblRateCardPrice * lp_dblQuantity * (lp_sngTime / 60)
                Else
                    l_dblRateCardTotal = lp_dblRateCardPrice * lp_dblQuantity * lp_sngTime
                End If
            Else
                l_dblRateCardTotal = lp_dblRateCardPrice * lp_dblQuantity
            End If
        Else
            l_dblRateCardTotal = lp_dblRateCardPrice * lp_dblQuantity * lp_sngTime
        End If
    Else
        l_dblRateCardTotal = lp_dblRateCardPrice * lp_dblQuantity
    End If
    
    If l_dblRateCardTotal < lp_dblRatecardMinimumCharge Then l_dblRateCardTotal = lp_dblRatecardMinimumCharge
    
    If l_strDescription = "" Or g_optAlwaysUseRateCardDescriptions <> 0 Then
        If lp_strCopyType <> "X" And UCase(Left(lp_strCETACostCode, 3)) <> "DEL" Then l_strDescription = GetData("ratecard", "description", "cetacode", lp_strCETACostCode)
    End If
    
    
    'work out the actual total
    Dim l_dblTotal As Double
    Dim l_sngTime2 As Single
    
    If lp_sngTime = 0 Then
        l_dblTotal = lp_dblUnitCharge * lp_dblQuantity
        l_sngTime2 = g_intDefaultTimeWhenZero
    Else
        l_sngTime2 = lp_sngTime
        If l_strUnit = "M" Then
            l_dblTotal = (lp_dblUnitCharge * lp_dblQuantity * (lp_sngTime))
        ElseIf g_optDontMakeMachineTimeADecimal = 1 Then
            l_dblTotal = (lp_dblUnitCharge * lp_dblQuantity * (lp_sngTime / 60))
        Else
            l_dblTotal = (lp_dblUnitCharge * lp_dblQuantity * (lp_sngTime))
        End If
        
    End If
    
    If l_dblTotal < lp_dblMinimumCharge Then l_dblTotal = lp_dblMinimumCharge
    
    If lp_sngDiscount <> 0 Then
        l_dblTotal = (l_dblTotal * (100 - lp_sngDiscount)) / 100
    End If
    
    l_dblTotal = RoundToCurrency(l_dblTotal)
       
    'vat calculations
    
    Dim l_dblVAT As Double
    Dim l_dblTotalIncludingVAT As Double
    
    l_dblVAT = (l_dblTotal / 100) * lp_sngVATRate
    'l_dblVAT = Int((l_dblVAT * 100) + 0.5) / 100 'round
    
    l_dblVAT = RoundToCurrency(l_dblVAT)
    
    l_dblTotalIncludingVAT = l_dblTotal + l_dblVAT 'already rounded from above sums
    
    'Do the business with the included stock rates
    Dim l_dblStockTotal As Double, l_dblStockVAT As Double, l_dblStockTotalIncVAT As Double
    If l_strStockNominalCode <> "" Then
    
        l_dblStockTotal = l_dblStockUnitCharge * lp_dblQuantity
        l_dblStockTotal = (l_dblStockTotal * (100 - lp_sngDiscount)) / 100
        l_dblStockTotal = RoundToCurrency(l_dblStockTotal)
        
        l_dblStockVAT = (l_dblStockTotal / 100) * lp_sngVATRate
        l_dblStockVAT = RoundToCurrency(l_dblStockVAT)
        
        l_dblStockTotalIncVAT = l_dblStockTotal + l_dblStockVAT 'already rounded from above sums
    
    Else
    
        l_dblStockTotal = 0
        l_dblStockVAT = 0
        l_dblStockTotalIncVAT = 0
        
    End If
    
    Dim l_dblInflatedUnitCharge As Double
    Dim l_dblInflatedTotal As Double
    Dim l_dblInflatedVAT As Double
    Dim l_dblInflatedTotalIncludingVAT As Double
    
    Dim l_dblRefundedUnitCharge As Double
    Dim l_dblRefundedTotal As Double
    Dim l_dblRefundedVAT As Double
    Dim l_dblRefundedTotalIncludingVAT As Double
    
    'check for two tier pricing and calculate amounts
    
    If lp_blnTwoTier = True Then
        

        'inflated prices
        Dim l_lngCompanyID As Long
        l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
        
        l_dblInflatedUnitCharge = GetUnitCharge(g_intBaseRateCard, lp_strCETACostCode, l_lngCompanyID)
            
        If l_strUnit = "M" Then
            l_dblInflatedTotal = (l_dblInflatedUnitCharge * lp_dblQuantity * (l_sngTime2))
        ElseIf g_optDontMakeMachineTimeADecimal = 1 Then
            l_dblInflatedTotal = (lp_dblQuantity * l_dblInflatedUnitCharge / 60 * l_sngTime2)
        Else
            l_dblInflatedTotal = (lp_dblQuantity * l_dblInflatedUnitCharge * l_sngTime2)
        End If
        
        If l_dblInflatedTotal < lp_dblMinimumCharge Then l_dblInflatedTotal = lp_dblMinimumCharge
        
        l_dblInflatedTotal = RoundToCurrency(l_dblInflatedTotal)
           
        l_dblInflatedVAT = l_dblInflatedTotal * ((lp_sngVATRate / 100))
        l_dblInflatedVAT = RoundToCurrency(l_dblInflatedVAT)
        
        l_dblInflatedTotalIncludingVAT = l_dblInflatedTotal + l_dblInflatedVAT
    
        'refunded prices
        l_dblRefundedUnitCharge = l_dblInflatedUnitCharge - lp_dblUnitCharge
        l_dblRefundedTotal = l_dblInflatedTotal - l_dblTotal
        l_dblRefundedVAT = l_dblInflatedVAT - l_dblVAT
        l_dblRefundedTotalIncludingVAT = l_dblInflatedTotalIncludingVAT - l_dblTotalIncludingVAT

    End If
    
    Dim l_strIDFieldName As String
    
    If lp_strCopyType = "R" Then
        l_strIDFieldName = "resourcescheduleID"
    Else
        l_strIDFieldName = "jobdetailID"
    End If
    
    Dim l_strRateCardCategory As String
    l_strRateCardCategory = GetData("ratecard", "category", "cetacode", lp_strCETACostCode)
    Dim l_lngRateCardOrderBy As Long
    l_lngRateCardOrderBy = GetData("ratecard", "fd_orderby", "cetacode", lp_strCETACostCode)
    
    If lp_strCopyType = "O" Then lp_sngTime = 0
    
    If lp_lngCostingDetailID <> 0 Then
        'edit an existing line
        l_strSQL = "UPDATE costing SET "
        l_strSQL = l_strSQL & l_strIDFieldName & " = '" & lp_lngItemID & "', "
        l_strSQL = l_strSQL & "jobID = '" & lp_lngJobID & "', "
        l_strSQL = l_strSQL & "copytype = '" & lp_strCopyType & "', "
        l_strSQL = l_strSQL & "description = '" & l_strDescription & "', "
        l_strSQL = l_strSQL & "accountcode = '" & l_strAccountCode & "', "
        l_strSQL = l_strSQL & "costcode = '" & lp_strCETACostCode & "', "
        l_strSQL = l_strSQL & "quantity = '" & lp_dblQuantity & "', "
        l_strSQL = l_strSQL & "fd_time = '" & lp_sngTime & "', "
        l_strSQL = l_strSQL & "unit = '" & l_strUnit & "', "
        l_strSQL = l_strSQL & "unitcharge = '" & lp_dblUnitCharge & "', "
        l_strSQL = l_strSQL & "discount = '" & lp_sngDiscount & "', "
        l_strSQL = l_strSQL & "total = '" & l_dblTotal & "', "
        l_strSQL = l_strSQL & "vat = '" & l_dblVAT & "', "
        l_strSQL = l_strSQL & "totalincludingvat = '" & l_dblTotalIncludingVAT & "', "
        If lp_strCopyType <> "IS" Then
            l_strSQL = l_strSQL & "includedstocknominal = '" & l_strStockNominalCode & "', "
            l_strSQL = l_strSQL & "includedstocktotal = '" & l_dblStockTotal & "', "
            l_strSQL = l_strSQL & "includedstockvat = '" & l_dblStockVAT & "', "
            l_strSQL = l_strSQL & "includedstocktotalincludingvat = '" & l_dblStockTotalIncVAT & "', "
        Else
            l_strSQL = l_strSQL & "includedstocknominal = NULL, "
            l_strSQL = l_strSQL & "includedstocktotal = 0, "
            l_strSQL = l_strSQL & "includedstockvat = 0, "
            l_strSQL = l_strSQL & "includedstocktotalincludingvat = 0, "
        End If
        l_strSQL = l_strSQL & "inflatedunitcharge = '" & l_dblInflatedUnitCharge & "', "
        l_strSQL = l_strSQL & "inflatedtotal = '" & l_dblInflatedTotal & "', "
        l_strSQL = l_strSQL & "inflatedvat = '" & l_dblInflatedVAT & "', "
        l_strSQL = l_strSQL & "inflatedtotalincludingvat = '" & l_dblInflatedTotalIncludingVAT & "', "
        l_strSQL = l_strSQL & "refundedunitcharge = '" & l_dblRefundedUnitCharge & "', "
        l_strSQL = l_strSQL & "refundedtotal = '" & l_dblRefundedTotal & "', "
        l_strSQL = l_strSQL & "refundedvat = '" & l_dblRefundedVAT & "', "
        l_strSQL = l_strSQL & "refundedtotalincludingvat = '" & l_dblRefundedTotalIncludingVAT & "',"
        l_strSQL = l_strSQL & "ratecardprice = '" & lp_dblRateCardPrice & "',"
        l_strSQL = l_strSQL & "ratecardtotal = '" & l_dblRateCardTotal & "',"
        l_strSQL = l_strSQL & "ratecardcategory = '" & l_strRateCardCategory & "',"
        l_strSQL = l_strSQL & "ratecardorderby = '" & l_lngRateCardOrderBy & "',"
        l_strSQL = l_strSQL & "minimumcharge = '" & lp_dblMinimumCharge & "',"
        l_strSQL = l_strSQL & "ratecardminimumcharge = '" & lp_dblRatecardMinimumCharge & "'"
        l_strSQL = l_strSQL & " WHERE costingID = '" & lp_lngCostingDetailID & "';"
    
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
    ElseIf lp_strCopyType <> "IS" Then
        'add the new line
        
        l_strSQL = "INSERT INTO costing"
        l_strSQL = l_strSQL & " (jobID, jobdetailID, costingsheetID, copytype, accountcode, includedstocknominal, costcode, description, quantity, fd_time,"
        l_strSQL = l_strSQL & " VDMS_Charge_Code, includedstock_VDMS_Charge_Code,"
        l_strSQL = l_strSQL & " unitcharge, discount, total, unit, vat, totalincludingvat, includedstocktotal, includedstockvat, includedstocktotalincludingvat, inflatedunitcharge, inflatedtotal, inflatedvat,"
        l_strSQL = l_strSQL & " inflatedtotalincludingvat, refundedunitcharge, refundedtotal, refundedvat, refundedtotalincludingvat,"
        l_strSQL = l_strSQL & " creditnotenumber, originalcostingID, invoicenumber, page, agg, adjust, notes, category, qry, fd_order, resourcescheduleID,"
        l_strSQL = l_strSQL & " includedindealprice, ratecardprice, ratecardtotal, ratecardcategory, ratecardorderby, minimumcharge, ratecardminimumcharge)"
        l_strSQL = l_strSQL & " Values"
        l_strSQL = l_strSQL & " ('" & lp_lngJobID & "', '" & lp_lngItemID & "', '0','" & lp_strCopyType & "','" & l_strAccountCode & "','" & l_strStockNominalCode & "','" & lp_strCETACostCode & "','" & l_strDescription & "','" & lp_dblQuantity & "','" & lp_sngTime & "',"
        l_strSQL = l_strSQL & " '" & l_strPriorityCode & "', '" & l_strPriorityStockCode & "',"
        l_strSQL = l_strSQL & " '" & lp_dblUnitCharge & "','" & lp_sngDiscount & "','" & l_dblTotal & "', '" & l_strUnit & "','" & l_dblVAT & "','" & l_dblTotalIncludingVAT & "',"
        l_strSQL = l_strSQL & " '" & l_dblStockTotal & "', '" & l_dblStockVAT & "', '" & l_dblStockTotalIncVAT & "', '" & l_dblInflatedUnitCharge & "', '" & l_dblInflatedTotal & "','" & l_dblInflatedVAT & "',"
        l_strSQL = l_strSQL & " '" & l_dblInflatedTotalIncludingVAT & "', '" & l_dblRefundedUnitCharge & "', '" & l_dblRefundedTotal & "', '" & l_dblRefundedVAT & "', '" & l_dblRefundedTotalIncludingVAT & "',"
        l_strSQL = l_strSQL & " Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, '" & lp_lngItemID & "',"
        l_strSQL = l_strSQL & " Null, '" & lp_dblRateCardPrice & "','" & l_dblRateCardTotal & "','" & l_strRateCardCategory & "','" & l_lngRateCardOrderBy & "','" & lp_dblMinimumCharge & "','" & lp_dblRatecardMinimumCharge & "');"
            
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
    Else
    
        l_strSQL = "INSERT INTO costing"
        l_strSQL = l_strSQL & " (jobID, jobdetailID, costingsheetID, copytype, accountcode, costcode, description, quantity, fd_time,"
        l_strSQL = l_strSQL & " VDMS_Charge_Code, "
        l_strSQL = l_strSQL & " unitcharge, discount, total, unit, vat, totalincludingvat, inflatedunitcharge, inflatedtotal, inflatedvat,"
        l_strSQL = l_strSQL & " inflatedtotalincludingvat, refundedunitcharge, refundedtotal, refundedvat, refundedtotalincludingvat,"
        l_strSQL = l_strSQL & " creditnotenumber, originalcostingID, invoicenumber, page, agg, adjust, notes, category, qry, fd_order, resourcescheduleID,"
        l_strSQL = l_strSQL & " includedindealprice, ratecardprice, ratecardtotal, ratecardcategory, ratecardorderby, minimumcharge, ratecardminimumcharge)"
        l_strSQL = l_strSQL & " Values"
        l_strSQL = l_strSQL & " ('" & lp_lngJobID & "', '" & lp_lngItemID & "', '0','" & lp_strCopyType & "','" & l_strAccountCode & "','" & lp_strCETACostCode & "','" & l_strDescription & "','" & lp_dblQuantity & "','" & lp_sngTime & "',"
        l_strSQL = l_strSQL & " '" & l_strPriorityCode & "',"
        l_strSQL = l_strSQL & " '" & lp_dblUnitCharge & "','" & lp_sngDiscount & "','" & l_dblTotal & "', '" & l_strUnit & "','" & l_dblVAT & "','" & l_dblTotalIncludingVAT & "',"
        l_strSQL = l_strSQL & " '" & l_dblInflatedUnitCharge & "', '" & l_dblInflatedTotal & "','" & l_dblInflatedVAT & "',"
        l_strSQL = l_strSQL & " '" & l_dblInflatedTotalIncludingVAT & "', '" & l_dblRefundedUnitCharge & "', '" & l_dblRefundedTotal & "', '" & l_dblRefundedVAT & "', '" & l_dblRefundedTotalIncludingVAT & "',"
        l_strSQL = l_strSQL & " Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, '" & lp_lngItemID & "',"
        l_strSQL = l_strSQL & " Null, '" & lp_dblRateCardPrice & "','" & l_dblRateCardTotal & "','" & l_strRateCardCategory & "','" & l_lngRateCardOrderBy & "','" & lp_dblMinimumCharge & "','" & lp_dblRatecardMinimumCharge & "');"
        
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    
        l_strSQL = "INSERT INTO costing"
        l_strSQL = l_strSQL & " (jobID, jobdetailID, costingsheetID, copytype, accountcode, costcode, description, quantity, fd_time,"
        l_strSQL = l_strSQL & " VDMS_Charge_Code, "
        l_strSQL = l_strSQL & " unitcharge, discount, total, unit, vat, totalincludingvat, inflatedunitcharge, inflatedtotal, inflatedvat,"
        l_strSQL = l_strSQL & " inflatedtotalincludingvat, refundedunitcharge, refundedtotal, refundedvat, refundedtotalincludingvat,"
        l_strSQL = l_strSQL & " creditnotenumber, originalcostingID, invoicenumber, page, agg, adjust, notes, category, qry, fd_order, resourcescheduleID,"
        l_strSQL = l_strSQL & " includedindealprice, ratecardprice, ratecardtotal, ratecardcategory, ratecardorderby, minimumcharge, ratecardminimumcharge)"
        l_strSQL = l_strSQL & " Values"
        l_strSQL = l_strSQL & " ('" & lp_lngJobID & "', '" & lp_lngItemID & "', '0', 'S','" & l_strStockNominalCode & "','STOCK','" & l_strDescription & "','" & lp_dblQuantity & "','" & lp_sngTime & "',"
        l_strSQL = l_strSQL & " '" & l_strPriorityStockCode & "',"
        l_strSQL = l_strSQL & " '" & l_dblStockUnitCharge & "','" & lp_sngDiscount & "','" & l_dblStockTotal & "', '', '" & l_dblStockVAT & "','" & l_dblStockTotalIncVAT & "',"
        l_strSQL = l_strSQL & " '" & l_dblInflatedUnitCharge & "', '" & l_dblInflatedTotal & "','" & l_dblInflatedVAT & "',"
        l_strSQL = l_strSQL & " '" & l_dblInflatedTotalIncludingVAT & "', '" & l_dblRefundedUnitCharge & "', '" & l_dblRefundedTotal & "', '" & l_dblRefundedVAT & "', '" & l_dblRefundedTotalIncludingVAT & "',"
        l_strSQL = l_strSQL & " Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, '" & lp_lngItemID & "',"
        l_strSQL = l_strSQL & " Null, '" & lp_dblRateCardPrice & "','" & l_dblRateCardTotal & "','" & l_strRateCardCategory & "','" & l_lngRateCardOrderBy & "','" & lp_dblMinimumCharge & "','" & lp_dblRatecardMinimumCharge & "');"
                        
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
    End If
    
    'return the new costingID
    CreateCostingLine = g_lngLastID
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub CopyCostingLines(lp_lngSourceJobID As Long, lp_lngDestinationJobID As Long, lp_strType As String)

If Not CheckAccess("/copycostinglines") Then Exit Sub

Dim l_strSQL As String
Dim l_rstSourceDetail As New ADODB.Recordset

l_strSQL = "SELECT * FROM costing WHERE jobID = '" & lp_lngSourceJobID & "' AND (creditnotenumber = 0 OR creditnotenumber IS NULL);"

Set l_rstSourceDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
If l_rstSourceDetail.RecordCount > 0 Then
    
    Do While Not l_rstSourceDetail.EOF
        
        l_strSQL = "INSERT INTO costing ("
        l_strSQL = l_strSQL & "jobID, "
        l_strSQL = l_strSQL & "copytype, "
        l_strSQL = l_strSQL & "accountcode, "
        l_strSQL = l_strSQL & "costcode, "
        l_strSQL = l_strSQL & "VDMS_Charge_Code, "
        l_strSQL = l_strSQL & "description, "
        l_strSQL = l_strSQL & "quantity, "
        l_strSQL = l_strSQL & "fd_time, "
        l_strSQL = l_strSQL & "unitcharge, "
        l_strSQL = l_strSQL & "discount, "
        l_strSQL = l_strSQL & "total, "
        l_strSQL = l_strSQL & "unit, "
        l_strSQL = l_strSQL & "vat, "
        l_strSQL = l_strSQL & "totalincludingvat, "
        l_strSQL = l_strSQL & "category, "
        l_strSQL = l_strSQL & "fd_order, "
        l_strSQL = l_strSQL & "includedindealprice, "
        l_strSQL = l_strSQL & "ratecardprice, "
        l_strSQL = l_strSQL & "ratecardtotal, "
        l_strSQL = l_strSQL & "ratecardcategory, "
        l_strSQL = l_strSQL & "ratecardorderby) VALUES ("
    
        l_strSQL = l_strSQL & "'" & lp_lngDestinationJobID & "', "
        l_strSQL = l_strSQL & "'" & l_rstSourceDetail("copytype") & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstSourceDetail("accountcode")) & "', "
        l_strSQL = l_strSQL & "'" & l_rstSourceDetail("costcode") & "', "
        l_strSQL = l_strSQL & "'" & l_rstSourceDetail("VDMS_Charge_Code") & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstSourceDetail("description")) & "', "
        l_strSQL = l_strSQL & "'" & l_rstSourceDetail("quantity") & "', "
        l_strSQL = l_strSQL & "'" & l_rstSourceDetail("fd_time") & "', "
        l_strSQL = l_strSQL & "'" & IIf(lp_strType = "CREDIT", 0 - l_rstSourceDetail("unitcharge"), l_rstSourceDetail("unitcharge")) & "', "
        l_strSQL = l_strSQL & "'" & l_rstSourceDetail("discount") & "', "
        l_strSQL = l_strSQL & "'" & IIf(lp_strType = "CREDIT", 0 - l_rstSourceDetail("total"), l_rstSourceDetail("total")) & "', "
        l_strSQL = l_strSQL & "'" & l_rstSourceDetail("unit") & "', "
        l_strSQL = l_strSQL & "'" & IIf(lp_strType = "CREDIT", 0 - l_rstSourceDetail("vat"), l_rstSourceDetail("vat")) & "', "
        l_strSQL = l_strSQL & "'" & IIf(lp_strType = "CREDIT", 0 - l_rstSourceDetail("totalincludingvat"), l_rstSourceDetail("totalincludingvat")) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstSourceDetail("category")) & "', "
        l_strSQL = l_strSQL & "'" & l_rstSourceDetail("fd_order") & "', "
        l_strSQL = l_strSQL & "'" & l_rstSourceDetail("includedindealprice") & "', "
        l_strSQL = l_strSQL & "'" & IIf(lp_strType = "CREDIT", 0 - l_rstSourceDetail("ratecardprice"), l_rstSourceDetail("ratecardprice")) & "', "
        l_strSQL = l_strSQL & "'" & IIf(lp_strType = "CREDIT", 0 - l_rstSourceDetail("ratecardtotal"), l_rstSourceDetail("ratecardtotal")) & "', "
        l_strSQL = l_strSQL & "'" & l_rstSourceDetail("ratecardcategory") & "', "
        l_strSQL = l_strSQL & "'" & l_rstSourceDetail("ratecardorderby") & "');"
    
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        l_rstSourceDetail.MoveNext
        
    Loop

End If

l_rstSourceDetail.Close
Set l_rstSourceDetail = Nothing

End Sub

Sub ApplyDealPriceToCostings(lp_lngJobID As Long)

Dim l_dblTotal As Double
Dim l_dblDealPrice As Double
Dim l_sngDiscount As Single
Dim l_strSQL As String
Dim l_strFieldToApplyFrom As String
Dim l_strFieldToGetTotalFrom As String

If g_optApplyDealPriceToActualsRatherThanRateCard = 0 Then
    l_strFieldToApplyFrom = "ratecardprice"
    l_strFieldToGetTotalFrom = "ratecardtotal"
Else
    l_strFieldToApplyFrom = "unitcharge"
    l_strFieldToGetTotalFrom = "total"
End If

l_strSQL = "SELECT COUNT(costingID) FROM costing WHERE (jobID = '" & lp_lngJobID & "') AND (includedindealprice = 'Yes');"

Dim l_intRecords As Integer
l_intRecords = GetCount(l_strSQL)

If l_intRecords = 0 Then
    l_intRecords = MsgBox("There are no items on this job that are included in the deal price. Do you want to automatically include all costing lines?", vbQuestion + vbOKCancel)
    If l_intRecords = vbCancel Then Exit Sub
    
    l_strSQL = "UPDATE costing SET includedindealprice = 'Yes' WHERE jobID = '" & lp_lngJobID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
End If

DoEvents

Dim l_intDoNotChargeVAT As Integer
l_intDoNotChargeVAT = GetData("job", "donotchargevat", "jobID", lp_lngJobID)
Dim l_sngVATRate As Single

If l_intDoNotChargeVAT <> 0 Then
    l_sngVATRate = 0
Else
    l_sngVATRate = GetVATRate(Format(GetData("company", "vatcode", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID))))
End If

l_strSQL = "SELECT SUM(" & l_strFieldToGetTotalFrom & ") FROM costing WHERE (jobID = '" & lp_lngJobID & "') AND (includedindealprice = 'Yes');"

l_dblTotal = GetCount(l_strSQL)
l_dblDealPrice = GetData("job", "dealprice", "jobID", lp_lngJobID)
l_sngDiscount = GetPercentageDiscount(l_dblTotal, l_dblDealPrice)

l_strSQL = "UPDATE costing SET discount = 0, unitcharge = Round(((" & l_strFieldToApplyFrom & " / 100) * (100 - " & l_sngDiscount & "))," & g_optRoundCurrencyDecimals & ") WHERE (jobID = '" & lp_lngJobID & "') AND (includedindealprice = 'Yes');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_strSQL = "UPDATE costing SET total = ROUND((unitcharge * quantity * fd_time)," & g_optRoundCurrencyDecimals & ") WHERE (jobID = '" & lp_lngJobID & "') AND (includedindealprice = 'Yes');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_strSQL = "UPDATE costing SET vat = ROUND((total * " & (l_sngVATRate / 100) & ")," & g_optRoundCurrencyDecimals & ") WHERE (jobID = '" & lp_lngJobID & "') AND (includedindealprice = 'Yes');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError


l_strSQL = "UPDATE costing SET totalincludingvat = vat + total WHERE (jobID = '" & lp_lngJobID & "') AND (includedindealprice = 'Yes');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

AddJobHistory lp_lngJobID, "Applied a deal price to the job"


End Sub

Sub ReInstateJob(lp_lngJobID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/reinstatejob") Then
        Exit Sub
    End If
    
    If UCase(GetData("job", "fd_status", "jobID", lp_lngJobID)) = "SENT TO ACCOUNTS" Then
        If Not CheckAccess("/reinstatesenttoaccountsjobs") Then
            Exit Sub
        End If
    End If
    
    'remove the details and revert job back to normal status
    
    SetData "job", "invoicenumber", "jobID", lp_lngJobID, "Null"
    SetData "job", "invoiceduser", "jobID", lp_lngJobID, "Null"
    SetData "job", "invoiceddate", "jobID", lp_lngJobID, "Null"
    
    SetData "job", "costeddate", "jobID", lp_lngJobID, "Null"
    SetData "job", "costeduser", "jobID", lp_lngJobID, "Null"
    
    
    
    'unlock if it was locked
    UnlockCostingsOnJob lp_lngJobID
    
    'dont update the job status if this is a hire job
    If UCase(GetData("job", "jobtype", "jobID", lp_lngJobID)) <> "HIRE" Then UpdateJobStatus lp_lngJobID, "Hold Cost", , False
    
    Dim l_strSQL As String
    l_strSQL = "DELETE FROM invoiceheader2 WHERE cfmtype = 'JOB' AND cfmID = '" & lp_lngJobID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    
    'add a history line to the jobn
    AddJobHistory lp_lngJobID, "Reinstated Job"
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Function RoundToCurrency(ByVal lp_dblAmountToRound As Double) As Double

Dim l_dblAmount As Double

If g_optRoundCurrencyDecimals <= 2 Then
    l_dblAmount = Int((lp_dblAmountToRound * 100) + 0.5) / 100
Else
    l_dblAmount = lp_dblAmountToRound
End If

RoundToCurrency = Round(l_dblAmount, g_optRoundCurrencyDecimals)

End Function

Function CheckMCSForVAT(ByVal lp_lngCostingSheetID As Long) As Long

Dim l_strSQL As String
l_strSQL = "SELECT SUM(total) AS sumtotal, SUM(vat) AS sumvat, SUM(totalincludingvat) AS sumtotalincludingvat FROM costing INNER JOIN job ON job.jobID = costing.jobID WHERE job.costingsheetID = '" & lp_lngCostingSheetID & "';"

Dim l_rst As New ADODB.Recordset
Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

Dim l_dblTotal              As Double
Dim l_dblVAT                As Double
Dim l_dblTotalIncludingVAT  As Double

l_dblTotal = l_rst("sumtotal")
l_dblVAT = l_rst("sumvat")
l_dblTotalIncludingVAT = l_rst("sumtotalincludingvat")

l_rst.Close
Set l_rst = Nothing


If Val(l_dblTotal + l_dblVAT) <> Val(l_dblTotalIncludingVAT) Then
    CheckMCSForVAT = True
Else
    CheckMCSForVAT = False
End If


End Function

Sub ApplyNoChargeToJob(lp_lngJobID As Long, lp_strOurContact As String, lp_strReasonCode As String, lp_strReason As String)

Dim l_strSQL As String

l_strSQL = "UPDATE job SET fd_status = 'No Charge', nochargerequestedby = '" & QuoteSanitise(lp_strOurContact) & "', nochargedate = '" & FormatSQLDate(Now) & "', nochargerequestedreason = '" & QuoteSanitise(lp_strReason) & "', nochargerequestedcode = '" & QuoteSanitise(lp_strReasonCode) & "' WHERE jobID = '" & lp_lngJobID & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

If g_intKidsCoContractRoutines <> 1 Then
    l_strSQL = "UPDATE costing SET discount = '100', unitcharge = '0', total = '0', vat = '0', totalincludingvat = '0' WHERE jobID = '" & lp_lngJobID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
End If

l_strSQL = "UPDATE resourceschedule SET fd_status = 'No Charge' WHERE jobID = '" & lp_lngJobID & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

'DELETE ALL THE COSTINGS
l_strSQL = "UPDATE costing SET total = 0, vat = 0, totalincludingvat = 0, inflatedtotal = 0, inflatedvat = 0, inflatedtotalincludingvat = 0, refundedtotal = 0, refundedvat = 0, refundedtotalincludingvat = 0 WHERE jobID = '" & lp_lngJobID & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

AddJobHistory lp_lngJobID, "No Charged Job - " & QuoteSanitise(lp_strReasonCode)

End Sub


Public Function CheckMCSFornominals(ByVal lp_lngCostingSheetID As Long) As Long
Dim l_strSQL As String
l_strSQL = "SELECT Count(costingID) FROM costing INNER JOIN job ON job.jobID = costing.jobID WHERE job.costingsheetID = '" & lp_lngCostingSheetID & "' AND (costing.accountcode IS NULL OR costing.accountcode = '');"

Dim l_lngProblemCostings As Long
l_lngProblemCostings = GetCount(l_strSQL)

CheckMCSFornominals = l_lngProblemCostings

End Function


Function NewAccountsTransaction(ByVal lp_lngProjectNumber As Long) As Long

If Not CheckAccess("/newaccountstransaction") Then Exit Function

frmAccountsTransaction.datTransactionDate.Value = Date
frmAccountsTransaction.datStartDate.Value = Date
frmAccountsTransaction.lblProjectNumber.Caption = frmAccountsTransactions.lblProjectNumber.Caption
frmAccountsTransaction.Show vbModal
Unload frmAccountsTransaction
Set frmAccountsTransaction = Nothing

End Function


Sub AddAccountsTransaction(lp_lngProjectNumber As Long, lp_strTransactionType As String, lp_datTransactionDate As Variant, _
lp_strDescription As String, lp_strReference As String, lp_dblAmount As Double, lp_lngCompanyID As Long, lp_strClientOrderNumber As String, lp_strDetails As String, lp_lngAccountsTransactionID As Long, lp_lngCostingSheetID As Long, lp_datJobStartDate As Date)

Dim l_strSQL As String

If lp_lngAccountsTransactionID = 0 Then
    
    l_strSQL = "INSERT INTO accountstransaction (cdate, cuser)"
    l_strSQL = l_strSQL & " VALUES "
    l_strSQL = l_strSQL & " ('" & FormatSQLDate(Now()) & "', '" & g_strUserInitials & "');"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    lp_lngAccountsTransactionID = g_lngLastID

End If

l_strSQL = "UPDATE accountstransaction SET "
l_strSQL = l_strSQL & " projectnumber = '" & lp_lngProjectNumber & "', "
l_strSQL = l_strSQL & " transactiontype = '" & lp_strTransactionType & "', "
If Not IsDate(lp_datTransactionDate) Then
    l_strSQL = l_strSQL & " transactiondate = Null, "
Else
    l_strSQL = l_strSQL & " transactiondate = '" & FormatSQLDate(lp_datTransactionDate) & "', "
End If
l_strSQL = l_strSQL & " startdate = '" & FormatSQLDate(lp_datJobStartDate) & "', "
l_strSQL = l_strSQL & " description = '" & QuoteSanitise(lp_strDescription) & "', "
l_strSQL = l_strSQL & " reference = '" & lp_strReference & "', "
l_strSQL = l_strSQL & " total = '" & lp_dblAmount & "', "
l_strSQL = l_strSQL & " vat = '" & RoundToCurrency(lp_dblAmount * 0.175) & "', "
l_strSQL = l_strSQL & " totalincludingvat = '" & RoundToCurrency(lp_dblAmount * 1.175) & "', "
l_strSQL = l_strSQL & " companyID = '" & lp_lngCompanyID & "', "
l_strSQL = l_strSQL & " clientordernumber = '" & QuoteSanitise(lp_strClientOrderNumber) & "', "
l_strSQL = l_strSQL & " muser = '" & g_strUserInitials & "', "
l_strSQL = l_strSQL & " mdate = '" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & " muserID = '" & g_lngUserID & "', "
l_strSQL = l_strSQL & " costingsheetID = '" & lp_lngCostingSheetID & "', "
l_strSQL = l_strSQL & " details = '" & QuoteSanitise(lp_strDetails) & "' WHERE accountstransactionID = '" & lp_lngAccountsTransactionID & "';"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

'get the new total and attach it to the project

Dim l_dblTotal As Double
l_dblTotal = GetDataSQL("SELECT sum(total) FROM accountstransaction WHERE projectnumber = '" & lp_lngProjectNumber & "';")

l_strSQL = "UPDATE project SET invoicedtotal = '" & l_dblTotal & "' WHERE projectnumber = '" & lp_lngProjectNumber & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError


End Sub

Public Sub ShowAccountsTransaction(ByVal lp_lngAccountsTransactionID As Long)
If Not CheckAccess("/showaccountstransaction") Then Exit Sub

'load an existing accounts transaction

Dim l_strSQL As String
Dim l_con    As New ADODB.Connection
Dim l_rst    As New ADODB.Recordset

l_strSQL = "SELECT * FROM accountstransaction WHERE accountstransactionID = '" & lp_lngAccountsTransactionID & "';"
l_con.Open g_strConnection
l_rst.Open l_strSQL, l_con, adOpenDynamic, adLockReadOnly

If Not l_rst.EOF Then l_rst.MoveFirst

With frmAccountsTransaction
    .lblAccountsTransactionID.Caption = lp_lngAccountsTransactionID
    
    .txtInput(0).Text = l_rst("cdate")
    .txtInput(1).Text = l_rst("cuser")
    
    .cmbTransactionType.ListIndex = GetListIndex(.cmbTransactionType, l_rst("transactiontype"))
    .txtInput(3).Text = Format(l_rst("reference"))
    If Not IsNull(l_rst("transactiondate")) Then
        .datTransactionDate.Value = l_rst("transactiondate")
    Else
        .datTransactionDate.Value = ""
    End If
    .datStartDate.Value = l_rst("startdate")
    .txtInput(5).Text = Format(l_rst("clientordernumber"))
    .txtInput(2).Text = Format(l_rst("description"))
    .txtInput(6).Text = Trim(" " & l_rst("details"))
    .txtInput(4).Text = l_rst("total")
    .lblProjectNumber.Caption = l_rst("projectnumber")
    If Not IsNull(l_rst("costingsheetID")) Then
        If l_rst("costingsheetID") = 0 Then
            .ddnCostingSheets.Text = ""
        Else
            .ddnCostingSheets.Text = l_rst("costingsheetID")
        End If
    Else
        .ddnCostingSheets.Text = ""
    End If
End With

l_rst.Close
Set l_rst = Nothing

l_con.Close
Set l_con = Nothing

l_strSQL = "SELECT accountcode AS 'Nominal Code', SUM(total) AS 'Total', SUM(ratecardtotal) AS 'R/C Total' FROM costing WHERE costing.accountstransactionID = '" & lp_lngAccountsTransactionID & "' GROUP BY accountcode ORDER BY accountcode;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set frmAccountsTransaction.grdNominalTotals.DataSource = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

frmAccountsTransaction.grdNominalTotals.Columns(0).Width = 1300
frmAccountsTransaction.grdNominalTotals.Columns(1).Width = 1300
frmAccountsTransaction.grdNominalTotals.Columns(2).Width = 1300

frmAccountsTransaction.grdNominalTotals.Columns(0).Alignment = ssCaptionAlignmentLeft
frmAccountsTransaction.grdNominalTotals.Columns(1).Alignment = ssCaptionAlignmentRight
frmAccountsTransaction.grdNominalTotals.Columns(2).Alignment = ssCaptionAlignmentRight

frmAccountsTransaction.grdNominalTotals.Columns(0).DataType = vbString
frmAccountsTransaction.grdNominalTotals.Columns(1).DataType = vbCurrency
frmAccountsTransaction.grdNominalTotals.Columns(1).NumberFormat = g_strCurrency & " 0.00"

frmAccountsTransaction.grdNominalTotals.Columns(2).DataType = vbCurrency
frmAccountsTransaction.grdNominalTotals.Columns(2).NumberFormat = g_strCurrency & "0.00"

l_strSQL = "SELECT SUM(total) AS 'Total' FROM costing WHERE costing.accountstransactionID = '" & lp_lngAccountsTransactionID & "';"
frmAccountsTransaction.txtTotal.Text = Format(GetCount(l_strSQL), "###,###,0.00")

l_strSQL = "SELECT SUM(ratecardtotal) AS 'Total' FROM costing WHERE costing.accountstransactionID = '" & lp_lngAccountsTransactionID & "';"
frmAccountsTransaction.txtRateCardTotal.Text = Format(GetCount(l_strSQL), "###,###,0.00")
frmAccountsTransaction.txtDifferencePercent.Text = GetPercentageDiscount(frmAccountsTransaction.txtRateCardTotal.Text, frmAccountsTransaction.txtTotal.Text)

frmAccountsTransaction.txtBalance.Text = g_strCurrency & Format(Val(frmAccountsTransaction.txtInput(4).Text) - Val(Replace(frmAccountsTransaction.txtTotal.Text, ",", "")), "###,###,0.00")

'these are the totals by job status
l_strSQL = "SELECT SUM(total) AS 'Total' FROM costing INNER JOIN job ON job.jobID = costing.jobID WHERE costing.accountstransactionID = '" & lp_lngAccountsTransactionID & "' AND (job.fd_status = 'Pencil' or job.fd_status = '2nd Pencil');"
frmAccountsTransaction.lblCaption(20).Caption = Format(GetCount(l_strSQL), "###,###,0.00")

l_strSQL = "SELECT SUM(total) AS 'Total' FROM costing INNER JOIN job ON job.jobID = costing.jobID WHERE costing.accountstransactionID = '" & lp_lngAccountsTransactionID & "' AND (job.fd_status = 'Confirmed');"
frmAccountsTransaction.lblCaption(21).Caption = Format(GetCount(l_strSQL), "###,###,0.00")

l_strSQL = "SELECT SUM(total) AS 'Total' FROM costing INNER JOIN job ON job.jobID = costing.jobID WHERE costing.accountstransactionID = '" & lp_lngAccountsTransactionID & "' AND (job.fd_status = 'Completed');"
frmAccountsTransaction.lblCaption(22).Caption = Format(GetCount(l_strSQL), "###,###,0.00")

l_strSQL = "SELECT SUM(total) AS 'Total' FROM costing INNER JOIN job ON job.jobID = costing.jobID WHERE costing.accountstransactionID = '" & lp_lngAccountsTransactionID & "' AND (job.fd_status = 'Hold Cost');"
frmAccountsTransaction.lblCaption(23).Caption = Format(GetCount(l_strSQL), "###,###,0.00")

l_strSQL = "SELECT SUM(total) AS 'Total' FROM costing INNER JOIN job ON job.jobID = costing.jobID WHERE costing.accountstransactionID = '" & lp_lngAccountsTransactionID & "' AND (job.fd_status = 'Costed');"
frmAccountsTransaction.lblCaption(24).Caption = Format(GetCount(l_strSQL), "###,###,0.00")

l_strSQL = "SELECT SUM(total) AS 'Total' FROM costing INNER JOIN job ON job.jobID = costing.jobID WHERE costing.accountstransactionID = '" & lp_lngAccountsTransactionID & "' AND (job.fd_status = 'Sent To Accounts');"
frmAccountsTransaction.lblCaption(25).Caption = Format(GetCount(l_strSQL), "###,###,0.00")

On Error GoTo Error_Handler
frmAccountsTransaction.Show vbModal
Unload frmAccountsTransaction
Set frmAccountsTransaction = Nothing

Error_Handler:
Exit Sub

End Sub

Public Function CreateInvoiceHeader2(lp_strCFMType As String, lp_lngCFMID As Long, lp_strInvoiceType As String, lp_lngProjectNumber As Long, lp_strReference As String, lp_datInvoiceDate As Date, lp_lngCompanyID As Long, lp_strCompanyName As String, lp_strInvoiceAddress As String, lp_strInvoicePostCode As String, lp_strInvoiceCountry As String)

Dim l_strCustomerReference As String
Dim l_dblTotal As Double

Select Case lp_strCFMType
Case "MCS"
    l_dblTotal = GetDataSQL("SELECT Sum(total) FROM costing WHERE costingsheetID = '" & lp_lngCFMID & "'")
    l_strCustomerReference = GetData("costingsheet", "clientordernumber", "costingsheetID", lp_lngCFMID)
Case "JOB"
    l_dblTotal = GetDataSQL("SELECT Sum(total) FROM costing WHERE jobID = '" & lp_lngCFMID & "'")
    l_strCustomerReference = GetData("job", "orderreference", "jobID", lp_lngCFMID)
End Select

Dim l_strSQL As String

'first check and delete any existing rows for this invoice header
l_strSQL = "DELETE FROM invoiceheader2 WHERE cfmtype = '" & lp_strCFMType & "' AND cfmID = '" & lp_lngCFMID & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError


'create the new invoice header
l_strSQL = "INSERT INTO invoiceheader2 (cfmtype, cfmID, invoicetype, projectnumber, reference, invoicedate, companyID, companyname, address, postcode, country, servertime, cuser, total, customerreference) VALUES"

If g_dbtype = "mysql" Then
    l_strSQL = l_strSQL & " ('" & lp_strCFMType & "','" & lp_lngCFMID & "','" & lp_strInvoiceType & "','" & lp_lngProjectNumber & "','" & lp_strReference & "', '" & FormatSQLDate(lp_datInvoiceDate) & "','" & lp_lngCompanyID & "','" & QuoteSanitise(lp_strCompanyName) & "', '" & QuoteSanitise(lp_strInvoiceAddress) & "','" & QuoteSanitise(lp_strInvoicePostCode) & "','" & QuoteSanitise(lp_strInvoiceCountry) & "', Now(), '" & g_strUserInitials & "', '" & l_dblTotal & "','" & QuoteSanitise(l_strCustomerReference) & "')"
Else
    l_strSQL = l_strSQL & " ('" & lp_strCFMType & "','" & lp_lngCFMID & "','" & lp_strInvoiceType & "','" & lp_lngProjectNumber & "','" & lp_strReference & "', '" & FormatSQLDate(lp_datInvoiceDate) & "','" & lp_lngCompanyID & "','" & QuoteSanitise(lp_strCompanyName) & "', '" & QuoteSanitise(lp_strInvoiceAddress) & "','" & QuoteSanitise(lp_strInvoicePostCode) & "','" & QuoteSanitise(lp_strInvoiceCountry) & "', GETdate(), '" & g_strUserInitials & "', '" & l_dblTotal & "','" & QuoteSanitise(l_strCustomerReference) & "')"
End If
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Function

Function CostJob(lp_lngJobID As Long, lp_blnCostDub As Boolean, lp_blnCostSchedule As Boolean, Optional lp_blnSilent As Boolean)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_blnJobDetailOnInvoice As Boolean
    Dim l_blnCostingsExist As Boolean
    Dim l_strSQL As String
    Dim l_lngCompanyID As Long
    Dim l_intRatecard As Integer
    
    Dim l_strDescription As String
    Dim l_strFormat As String
    Dim l_dblQuantity As Double
    Dim l_dblUnitCharge As Double
    Dim l_sngDiscount As Single
    Dim l_sngMachineTime As Single
    Dim l_dblMinimumCharge As Double
    Dim l_dblRateCardMinimumCharge As Double
    
    Dim l_lngJobDetailID As Long
    Dim l_strCopyType As String
    Dim l_sngVATRate As Single
    
    Dim l_lngQuoteID As Long
    
    Dim l_intTwoTierPricing As Integer
    
    Dim l_lngMasterResourceScheduleID  As Long
    Dim l_intShowOnQuote  As Integer
    

    Dim l_lngResourceScheduleID As Long
    Dim l_rstResourceSchedule As ADODB.Recordset
    
    Dim l_dblRateCardPrice As Double
    Dim l_lngResourceID As Long
    
    Dim l_intLoop As Integer
    Dim l_lngCount As Long
    Dim l_rsChargeBands As ADODB.Recordset, l_strChargeBandName As String
    
    'check to see if product costing should be used
    
    Dim l_lngProductID As Long
    l_lngProductID = GetData("job", "productID", "jobID", lp_lngJobID)
    
    'if not, just keep the product ID as 0 and it will ignore the code
    If g_optUseProductRateCards = 0 Then l_lngProductID = 0
    
    'mousepointer
    Screen.MousePointer = vbHourglass
    
    'get the settings
    l_blnJobDetailOnInvoice = GetFlag(GetData("job", "flagdetailoninvoice", "jobID", lp_lngJobID))
    
    Dim l_strJobType  As String
    l_strJobType = GetData("job", "jobtype", "jobID", lp_lngJobID)
    
    'check if there are already costings
    l_blnCostingsExist = CostingsExist(lp_lngJobID)
    
    'pick up the company ID
    l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
    
    'find out which rate card they use
    l_intRatecard = Val(GetData("company", "ratecard", "companyID", l_lngCompanyID))

    'make sure we use at least the bas rate card
    If l_intRatecard > 9 Or l_intRatecard < 1 Then
        l_intRatecard = g_intBaseRateCard
    End If
    
    Dim l_intDoNotChargeVAT As Integer
    l_intDoNotChargeVAT = GetData("job", "donotchargevat", "jobID", lp_lngJobID)
    
'    'are they on two - tier pricing
'    l_intTwoTierPricing = InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/two-tier-pricing")
'
'    If l_intTwoTierPricing <> 0 Then
'        l_intTwoTierPricing = True
'    End If

    'get their vat rate
    l_sngVATRate = GetVATRate(Format(GetData("company", "vatcode", "companyID", l_lngCompanyID)))
    
    'check for the actual tick box on the form
    If l_intDoNotChargeVAT <> 0 Then
        l_sngVATRate = 0
    End If
    
    
    'is there a quote ID to use for this job
    l_lngQuoteID = GetData("project", "quoteID", "projectID", GetJobProjectID(lp_lngJobID))
    
    'if there are NO costings...
    If l_blnCostingsExist = False Then
        
        'if we want to cost schedule items
        If lp_blnCostSchedule <> False Or UCase(l_strJobType) = "HIRE" Or UCase(l_strJobType) = "EDIT" Then
            
            'should we skip the costing of resource schedules
            If UCase(Left(l_strJobType, 3)) = "DUB" And g_intDontCostScheduledResourcesIfJobTypeIsADub = 1 Then GoTo PROC_SkipSchedule
            
            l_strSQL = "SELECT masterresourcescheduleID, showonquote, resourcescheduleID, resourceID, actualhoursused, starttime, endtime, costcode, hiredescription FROM resourceschedule WHERE jobID = '" & lp_lngJobID & "' ORDER BY resourcescheduleID"
            Set l_rstResourceSchedule = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            
            'cycle through and cost each item
            If Not l_rstResourceSchedule.EOF Then l_rstResourceSchedule.MoveFirst
            
            'loop through all the resource schedule items
            Do While Not l_rstResourceSchedule.EOF
            
                'pick up the cost code for this resource
                If Trim(" " & l_rstResourceSchedule("hiredescription")) <> "" Then
                    l_strFormat = l_rstResourceSchedule("costcode")
                Else
                    l_strFormat = GetData("resource", "nominalcode", "resourceID", l_rstResourceSchedule("resourceID"))
                End If
                
                'if there is a valid cost code and its not NO COST
                If (l_strFormat <> "") And (UCase(l_strFormat) <> "NO COST") Then
                    
                    'set the quantity to = 1
                    l_dblQuantity = 1
                    
                    'get the resource schedule ID
                    l_lngResourceScheduleID = l_rstResourceSchedule("resourcescheduleID")
                    
                    'type R for resource schedule
                    l_strCopyType = "R"
                    
                    'get the master resource schedule ID
                    l_lngMasterResourceScheduleID = Val(Format(l_rstResourceSchedule("masterresourcescheduleID")))
                    
                    'check if this item should be costed (kits mainly)
                    l_intShowOnQuote = Val(Format(l_rstResourceSchedule("showonquote")))
                    
                    'need to check for the type of unit that they are charging for:
                    l_sngMachineTime = Val(Format(l_rstResourceSchedule("actualhoursused"))) '* 60
                                        
                    Dim l_strChargeUnit As String
                    l_strChargeUnit = GetData("job", "chargeunits", "jobID", lp_lngJobID)
                    
                    
                    'use the booked duration to calculate the costs
                    If l_sngMachineTime = 0 And (UCase(l_strJobType) = "HIRE" Or UCase(l_strJobType) = "EDIT") Then
                        l_strChargeUnit = "day"
                        l_sngMachineTime = DateDiff("h", l_rstResourceSchedule("starttime"), l_rstResourceSchedule("endtime"))
                    End If
                    
                    'change the machine time to the correct number
                    Select Case UCase(l_strChargeUnit)
                    Case "DAY"
                        l_sngMachineTime = l_sngMachineTime / 24
                    Case "WEEK"
                        l_sngMachineTime = l_sngMachineTime / 24 / 7
                    Case "HOUR"
                        'stays the same as hours are assumed
                    Case "1/2 DAY"
                        l_sngMachineTime = l_sngMachineTime * 2
                    Case Else
                        l_strChargeUnit = "HOUR" 'just use the default
                    End Select
                                    
                    If UCase(g_strRateCardType) <> "HIRE" Then
                                    
                        Dim l_strUnit As String
                        l_strUnit = UCase(GetDataSQL("SELECT TOP 1 unit FROM ratecard WHERE cetacode = '" & l_strFormat & "' AND companyID = '0'"))
                            
                        If Left(l_strUnit, 4) = "WEEK" Then
                            If UCase(l_strChargeUnit) = "DAY" Then
                                If l_sngMachineTime < 7 Then
                                    l_sngMachineTime = 1
                                Else
                                    l_sngMachineTime = l_sngMachineTime / 7
                                End If
                            End If
                        End If
                        
                        If Left(l_strUnit, 3) = "DAY" Then
                            If UCase(l_strChargeUnit) = "WEEK" Then
                                l_sngMachineTime = l_sngMachineTime * 4
                            End If
                        End If
                    Else
                        l_strUnit = l_strChargeUnit
                    End If
                    
                    'update the machine time to be 1 if its currently 0
                    If l_sngMachineTime = 0 Then l_sngMachineTime = 1
                    
                    'set the base charge to be 0
                    l_dblUnitCharge = 0
                    
                    'get the resource ID
                    l_lngResourceID = Format(l_rstResourceSchedule("resourceID"))
                    
                    'if you are using a quote, then it should get the quoted rates, rather than the rate card rates
                                        
                    'if there was no quote, get the ratecard price
                    
                    l_sngDiscount = GetCompanyDiscount(l_lngCompanyID, l_strFormat)
                                    
                    l_dblUnitCharge = GetPrice(l_strFormat, CStr(l_intRatecard), l_lngCompanyID, l_strChargeUnit, l_lngProductID)
                    
                    If l_dblUnitCharge = 0 Then
                        l_dblUnitCharge = GetPrice(l_strFormat, CStr(l_intRatecard), l_lngCompanyID, l_lngProductID)
                    End If
                
                    'get the basic ratecard price
                    If UCase(g_strRateCardType) = "HIRE" Then
                        l_dblRateCardPrice = GetPrice(l_strFormat, g_intBaseRateCard, 0, l_strChargeUnit, 0)
                    Else
                        l_dblRateCardPrice = GetPrice(l_strFormat, g_intBaseRateCard, 0, "", 0)
                    End If
                    
                    'Get the Minimum charge if it exists
                    
                    l_dblMinimumCharge = GetMinimumCharge(l_strFormat, l_lngCompanyID)
                    l_dblRateCardMinimumCharge = GetMinimumCharge(l_strFormat, 0)
                    
                    'use the ratecard description
                    If g_optAlwaysUseRateCardDescriptions = 1 Then
                        l_strDescription = Format(GetData("ratecard", "description", "cetacode", l_strFormat))
                    Else
                        l_strDescription = Format(GetData("resource", "description", "resourceID", l_lngResourceID))
                        If l_strDescription = "" Then
                            l_strDescription = Format(GetData("resource", "name", "resourceID", l_lngResourceID))
                        End If
                    End If
                    
                    If g_optDeductPreSetDiscountsFromUnitPrice Then
                        'hide the discount, and make it look like the standard rate
                        l_dblUnitCharge = (l_dblUnitCharge / 100) * (100 - l_sngDiscount)
                        l_sngDiscount = 0
                    End If

                    'if this is  a kit item, set the cost to be ZERO
                    
                    If l_lngMasterResourceScheduleID <> 0 Then
                        l_dblRateCardPrice = 0
                        l_dblUnitCharge = 0
                    End If
                    
                    If l_intShowOnQuote <> 0 Or l_lngMasterResourceScheduleID = 0 Then
                        'add the stock to the costing
                        CreateCostingLine l_lngResourceScheduleID, lp_lngJobID, l_strCopyType, l_strDescription, l_strFormat, l_dblQuantity, l_sngMachineTime, l_dblUnitCharge, l_sngDiscount, l_sngVATRate, l_intTwoTierPricing, l_dblRateCardPrice, l_dblMinimumCharge, l_dblRateCardMinimumCharge, , l_strUnit
                    End If
                End If
                
                'cycle
                l_rstResourceSchedule.MoveNext
            Loop
            
            l_rstResourceSchedule.Close
            Set l_rstResourceSchedule = Nothing
            

        End If
        
PROC_SkipSchedule:
        
        'should we do the dub style costings?
        
        If lp_blnCostDub = True Then
            
            'do the query on the jobdetail table
            
            l_strSQL = "SELECT * FROM jobdetail WHERE jobID = '" & lp_lngJobID & "' AND copytype <> 'T' ORDER BY fd_orderby, jobdetailID"
            'open the recordset
            Dim l_rstJobDetail As New ADODB.Recordset
            Set l_rstJobDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            
            If Not l_rstJobDetail.EOF Then
                l_rstJobDetail.MoveFirst
            End If
            
            If lp_blnSilent = False And l_rstJobDetail.RecordCount > 0 Then
                frmJob.ProgressBar1.Max = l_rstJobDetail.RecordCount
                l_lngCount = 0
                frmJob.ProgressBar1.Visible = True
            End If
            Do While Not l_rstJobDetail.EOF
                
'                If (g_intWebInvoicing = 1 And GetData("costing", "costingID", "jobdetailID", l_rstJobDetail("jobdetailID")) = 0) Or g_intWebInvoicing = 0 Then
                    If lp_blnSilent = False Then
                        l_lngCount = l_lngCount + 1
                        frmJob.ProgressBar1.Value = l_lngCount
                    End If
                    l_lngJobDetailID = l_rstJobDetail("jobdetailID")
                    l_strFormat = Format(l_rstJobDetail("format"))
                    
                    
                    
                    If Not IsNull(l_rstJobDetail("runningtime")) Then
                        If InStr(l_rstJobDetail("runningtime"), ":") <> 0 Then
                            l_sngMachineTime = Left(l_rstJobDetail("runningtime"), InStr(l_rstJobDetail("runningtime"), ":") - 1)
                        Else
                            l_sngMachineTime = Val(l_rstJobDetail("runningtime"))
                        End If
                        
                    Else
                        l_sngMachineTime = 0
                    End If
                    
                    l_strCopyType = UCase(Format(l_rstJobDetail("copytype")))
                    
                    'need to check here to see if the job is going to be charged per minute, or per 1/4 hour, and check whether valid duration entered
                    
                    If l_strCopyType = "M" Or l_strCopyType = "C" Or l_strCopyType = "A" Or l_strCopyType = "I" Then
                        If l_sngMachineTime = 0 Then
                            If g_optAbortIfAZeroDuration <> 0 Then
                                If lp_blnSilent = False Then MsgBox "There are lines with Zero duration that must be corrected!", vbCritical, "Costing Aborted"
                                GoTo Proc_Check_Record_Error
                            End If
                        End If
                    End If
                    
                    If l_strCopyType = "I" Or l_strCopyType = "IS" Then
                        l_strChargeBandName = GetData("xref", "information", "format", l_strFormat)
                        Set l_rsChargeBands = ExecuteSQL("SELECT Top 1 * FROM CostingChargeBandEntry WHERE Bandname = '" & l_strChargeBandName & "' AND LowestTime <= " & l_sngMachineTime & " AND HighestTime >= " & l_sngMachineTime, g_strExecuteError)
                        If l_rsChargeBands.RecordCount <= 0 Then
                            l_sngMachineTime = Int((l_sngMachineTime + 12) / 15) * 15
                            If l_sngMachineTime < 15 Then l_sngMachineTime = 15
                        Else
                            l_sngMachineTime = l_rsChargeBands("ChargeBracket")
                        End If
                        l_rsChargeBands.Close
                    ElseIf LCase(GetData("job", "chargeunits", "jobID", lp_lngJobID)) <> "mins" Then
                        If GetDataSQL("SELECT TOP 1 format FROM xref WHERE description = '" & l_strFormat & "' AND category = 'dformat';") = "30" Then
                            l_sngMachineTime = Int((l_sngMachineTime + 27) / 30) * 30
                            If l_sngMachineTime < 30 Then l_sngMachineTime = 30
                        ElseIf IsNull(l_rstJobDetail("chargerealminutes")) Or l_rstJobDetail("chargerealminutes") = False Then
                            l_sngMachineTime = Int((l_sngMachineTime + 12) / 15) * 15
                            If l_sngMachineTime < 15 Then l_sngMachineTime = 15
                        End If
                    End If
                    
                    
                    'do we want to work out the duration / cost too?
                    If g_optAddDurationToRateCode <> 0 Or l_strCopyType = "I" Or l_strCopyType = "IS" Then
                        l_strFormat = l_strFormat & Format(l_sngMachineTime, "000")
                        l_sngMachineTime = 1
                    Else
                        If g_optDontMakeMachineTimeADecimal = 0 Then l_sngMachineTime = l_sngMachineTime / 60
                    End If
                    
                    l_dblQuantity = Val(Format(l_rstJobDetail("quantity")))
                    l_sngDiscount = GetCompanyDiscount(l_lngCompanyID, l_strFormat)
                    l_dblUnitCharge = GetUnitCharge(l_intRatecard, l_strFormat, l_lngCompanyID)
                    
                    l_dblRateCardPrice = GetUnitCharge(g_intBaseRateCard, l_strFormat, 0)
                    
                    l_dblMinimumCharge = GetMinimumCharge(l_strFormat, l_lngCompanyID)
                    l_dblRateCardMinimumCharge = GetMinimumCharge(l_strFormat, 0)
                    
                    'either use the ratecard description or use the standard ie. "Digi playback 20 mins"
                    If g_optUseAdditionalDescriptionsWhenCosting Then
                        l_strDescription = Format(GetData("ratecard", "description", "cetacode", l_strFormat))
                    Else
                        If g_optDontMakeMachineTimeADecimal = 1 Then
                            l_strDescription = RTrim(l_rstJobDetail("format")) & " playback " & l_sngMachineTime & " mins"
                        Else
                            l_strDescription = RTrim(l_rstJobDetail("format")) & " playback " & l_sngMachineTime * 60 & " mins"
                        End If
                    End If
                    
                    Select Case l_strCopyType
                        Case "M" 'master
                            'use the job detail description on the invoice for the master
                            If g_optUseMasterDetailOnInvoice Then
                                l_strDescription = Trim(" " & l_rstJobDetail("description"))
                            End If
                            
                        Case "C"
                            'if they want descriptions on all lines (set on job)
                            If l_blnJobDetailOnInvoice <> 0 Then
                                l_strDescription = l_rstJobDetail("description")
                            Else
                                If g_optDontMakeMachineTimeADecimal = 1 Then
                                    l_strDescription = RTrim(l_rstJobDetail("format")) & " record " & l_sngMachineTime & " mins"
                                Else
                                    l_strDescription = RTrim(l_rstJobDetail("format")) & " record" & l_sngMachineTime * 60 & " mins"
                                End If
                            End If
                            
                        Case "O", "I", "IS"
                            
                            'l_strDescription = Trim(l_rstJobDetail("description") & " - " & l_rstJobDetail("workflowdescription"))
                            l_strDescription = Trim(" " & l_rstJobDetail("description"))
                            l_sngMachineTime = 1
                                                        
                        Case "A"
                            If InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/progress") > 0 Or InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/tracker") > 0 Or InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/dadctracker") > 0 Or InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/DADCFixes") > 0 Then
                                l_strDescription = Trim(" " & l_rstJobDetail("description"))
                            End If
                            
                        Case "T"
                            
                    End Select
                    
                    If g_optDeductPreSetDiscountsFromUnitPrice Then
                        'hide the discount, and make it look like the standard rate
                        l_dblUnitCharge = (l_dblUnitCharge / 100) * (100 - l_sngDiscount)
                        l_sngDiscount = 0
                    End If
                    
                    CreateCostingLine l_lngJobDetailID, lp_lngJobID, l_strCopyType, l_strDescription, l_strFormat, l_dblQuantity, l_sngMachineTime, l_dblUnitCharge, l_sngDiscount, l_sngVATRate, l_intTwoTierPricing, l_dblRateCardPrice, l_dblMinimumCharge, l_dblRateCardMinimumCharge
    
                    'Cost the stock for this line
                    If UCase(l_rstJobDetail("copytype")) = "C" Then
                        
                        'get both stock 1 and stock 2
                        For l_intLoop = 1 To 2
                            l_dblQuantity = Val(Format(l_rstJobDetail("stock" & l_intLoop & "ours")))
                            
                            'if there is a quantity of stock
                            If l_dblQuantity > 0 Then
                                
                                'set variables
                                If Trim(" " & l_rstJobDetail("stock" & l_intLoop & "type")) = "" Then
                                    MsgBox "There is an error with one of the stock codes - any line with a quantity of stock must have a stock code", vbCritical, "Error COsting Job"
                                    Exit Function
                                End If
                                l_lngJobDetailID = l_rstJobDetail("jobdetailID")
                                l_strCopyType = "S"
                                l_strDescription = ""
                                l_strFormat = l_rstJobDetail("stock" & l_intLoop & "type")
                                l_sngMachineTime = 0
                                l_sngDiscount = GetCompanyDiscount(l_lngCompanyID, l_strFormat)
                                l_dblUnitCharge = GetUnitCharge(l_intRatecard, l_strFormat, l_lngCompanyID)
                                l_dblRateCardPrice = GetUnitCharge(g_intBaseRateCard, l_strFormat, l_lngCompanyID)
                                l_dblMinimumCharge = GetMinimumCharge(l_strFormat, l_lngCompanyID)
                                l_dblRateCardMinimumCharge = GetMinimumCharge(l_strFormat, 0)
                                
                                'use the ratecard description
                                l_strDescription = Format(GetData("ratecard", "description", "cetacode", l_strFormat))
                                
                                If g_optDeductPreSetDiscountsFromUnitPrice Then
                                    'hide the discount, and make it look like the standard rate
                                    l_dblUnitCharge = (l_dblUnitCharge / 100) * (100 - l_sngDiscount)
                                    l_sngDiscount = 0
                                End If
                                
                                'add the stock to the costing
                                CreateCostingLine l_lngJobDetailID, lp_lngJobID, l_strCopyType, l_strDescription, l_strFormat, l_dblQuantity, l_sngMachineTime, l_dblUnitCharge, l_sngDiscount, l_sngVATRate, l_intTwoTierPricing, l_dblRateCardPrice, l_dblMinimumCharge, l_dblRateCardMinimumCharge
                            End If
                        Next
                    End If
                    
'                End If

                'cycle through the records
                l_rstJobDetail.MoveNext
                
                DoEvents
                
            Loop
            If lp_blnSilent = False Then frmJob.ProgressBar1.Visible = False
                        
Proc_Check_Record_Error:
            
            l_rstJobDetail.Close
            Set l_rstJobDetail = Nothing
            
        End If ' end of dub costing
        
        If g_optCostDespatch <> False Then
            
            l_strSQL = "SELECT despatchID, postcode, charge, companyname, despatchnumber, purchaseheaderID, direction, costcode  FROM despatch WHERE jobID = '" & lp_lngJobID & "' ORDER BY despatchID"
            
            Dim l_strPostCode As String
            Dim l_dblCharge As Double
            Dim l_lngDespatchID As Long
            Dim l_strCompany  As String
            Dim l_strPONumber As Long
            
            Dim l_rstDespatch As ADODB.Recordset
            Set l_rstDespatch = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            
            'cycle through and cost each item, if required
            'go back and cost all the stock on the job
            If Not l_rstDespatch.EOF Then l_rstDespatch.MoveFirst
           
            Do While Not l_rstDespatch.EOF
                
                    l_strPONumber = Format(l_rstDespatch("purchaseheaderID"))
                    Dim l_lngDespatchNumber  As Long
                    l_strCompany = l_rstDespatch("companyname")
                    l_lngDespatchID = l_rstDespatch("despatchID")
                    l_lngDespatchNumber = l_rstDespatch("despatchnumber")
                    l_strPostCode = l_rstDespatch("postcode")
                    l_dblCharge = Val(Format(l_rstDespatch("charge")))
                    l_strFormat = Format(l_rstDespatch("costcode"))
                    
                    l_strCopyType = "X"
                    
                    Dim l_strPurchaseOrder As String
                    If l_strPONumber <> 0 Then
                        l_strPurchaseOrder = " PO# " & l_strPONumber
                    Else
                        l_strPurchaseOrder = " "
                    End If
                    
                    If l_rstDespatch("direction") = "OUT" Then
                        l_strDescription = "Delivery to " & l_strCompany & " @ " & l_strPostCode & l_strPurchaseOrder & " (DNote: " & l_lngDespatchNumber & ")"
                    Else
                        l_strDescription = "Collection from " & l_strCompany & " @ " & l_strPostCode & l_strPurchaseOrder & "(DNote: " & l_lngDespatchNumber & ")"
                    End If
                    
                    If l_strFormat = "" Then l_strFormat = "DELIVERY"
                    
                    l_dblQuantity = 1
                    l_sngMachineTime = 1
                    l_dblUnitCharge = l_dblCharge
                    l_sngDiscount = 0
                    
                    
                    'add the stock to the costing
                    CreateCostingLine l_lngDespatchID, lp_lngJobID, l_strCopyType, l_strDescription, l_strFormat, l_dblQuantity, l_sngMachineTime, l_dblUnitCharge, l_sngDiscount, l_sngVATRate, l_intTwoTierPricing, l_dblCharge, 0, 0
                
                'cycle
                l_rstDespatch.MoveNext
            Loop
            
            l_rstDespatch.Close
            Set l_rstDespatch = Nothing
        End If

        
        'ShowJob lp_lngJobID, frmJob.tab1.Tab
    
    
    End If
    
    If IsFormLoaded("frmJob") = True Then
        l_strSQL = "SELECT * FROM costing WHERE ((creditnotenumber IS NULL) OR (creditnotenumber = 0)) AND (jobID = '" & lp_lngJobID & "') ORDER BY ratecardorderby, costingID"
        
        frmJob.adoCosting.RecordSource = l_strSQL
        frmJob.adoCosting.ConnectionString = g_strConnection
        
        If g_optUseMinimumCharging = 1 Then
            
            Dim l_dblMinCharge As Double
            Dim l_strStatus  As String
            l_strStatus = GetData("job", "fd_status", "jobID", lp_lngJobID)
            
            'if this is a credit note, or this job has reached the 'costed' stage then dont min charge
            If Left(LCase(l_strJobType), 6) = "credit" Or GetStatusNumber(l_strStatus) >= 8 Then
            Else
            
                'Check whether the job total is less than the minimum charge for that client and set a Min charge if it is
                l_dblMinCharge = GetUnitCharge(l_intRatecard, "MC", l_lngCompanyID)
                If GetTotalCostsForJob(lp_lngJobID, "total", "INVOICE") < l_dblMinCharge Then
                    frmJob.cmdMinimumCharge.Value = True
                End If
            End If
            
        End If
    
        
        l_strSQL = "SELECT ratecardID, category, cetacode, nominalcode, description, unit, price1 as rate0 FROM ratecard WHERE companyID = 0 ORDER BY cetacode, nominalcode"
       
        Dim l_conSearch As ADODB.Connection
        Dim l_rstSearch As ADODB.Recordset
        Set l_conSearch = New ADODB.Connection
        Set l_rstSearch = New ADODB.Recordset
        
        l_conSearch.ConnectionString = g_strConnection
        l_conSearch.Open
        
        With l_rstSearch
            .CursorLocation = adUseClient
            .LockType = adLockBatchOptimistic
            .CursorType = adOpenDynamic
            .Open l_strSQL, l_conSearch, adOpenDynamic
        End With
        
        l_rstSearch.ActiveConnection = Nothing
        
        Set frmJob.ddnRateCard.DataSource = l_rstSearch
        
        l_conSearch.Close
        Set l_conSearch = Nothing
    
        
        'update the job status
        'If l_blnCostingsExist = False Then
        '    If UCase(GetData("job", "jobtype", "jobID", lp_lngJobID)) <> "HIRE" Then
        '        UpdateJobStatus lp_lngJobID, "Hold Cost"
        '    End If
        'End If
        
    
        frmJob.adoCosting.Refresh
    
        g_blnLoadingCostings = False
        
        Dim l_intCostingsLocked As Integer
        l_intCostingsLocked = GetData("job", "costingslocked", "jobID", lp_lngJobID)
    
        frmJob.lblHoldCostDate.Caption = GetData("job", "holdcostdate", "jobID", lp_lngJobID)
        frmJob.lblCostedDate.Caption = GetData("job", "costeddate", "jobID", lp_lngJobID)
        frmJob.lblInvoicedDate.Caption = GetData("job", "invoiceddate", "jobID", lp_lngJobID)
        frmJob.lblSentToAccountsDate(1).Caption = GetData("job", "senttoaccountsdate", "jobID", lp_lngJobID)
        
        
        If l_intCostingsLocked <> 0 Then
            frmJob.cmdLockCosts.FontBold = True
            frmJob.cmdLockCosts.Enabled = False
            frmJob.cmdReCost.Enabled = False
            frmJob.cmdNoCharge.Enabled = False
            frmJob.cmdApplyDeal.Enabled = False
            frmJob.cmdCopyCostsFrom.Enabled = False
            
        
    '        With frmJob.grdCostingDetail
    '            .AllowAddNew = False
    '            .AllowDelete = False
    '            .AllowUpdate = False
    '        End With
            frmJob.grdCostingDetail.Enabled = False
        Else
            frmJob.cmdLockCosts.FontBold = False
            frmJob.cmdLockCosts.Enabled = True
            
        End If
        
        Dim l_dblJobTotal As Double, l_dblTotalIncludingVAT As Double, l_dblRateCardTotal As Double, l_dblAdditionalDiscount As Double
        
        l_dblJobTotal = GetTotalCostsForJob(Val(lp_lngJobID), "total", "INVOICE")
        l_dblTotalIncludingVAT = GetTotalCostsForJob(Val(lp_lngJobID), "totalincludingvat", "INVOICE")
        l_dblRateCardTotal = GetTotalCostsForJob(Val(lp_lngJobID), "ratecardtotal", "INVOICE")
        l_dblAdditionalDiscount = GetTotalCostsForJob(Val(lp_lngJobID), "additionaldiscount", "INVOICE")
        
        frmJob.txtTotal.Text = g_strCurrency & Format(RoundToCurrency(l_dblJobTotal), "0.00#")
        frmJob.txtTotalIncVAT.Text = g_strCurrency & Format(RoundToCurrency(l_dblTotalIncludingVAT), "0.00#")
        frmJob.txtRateCardTotal.Text = g_strCurrency & Format(RoundToCurrency(l_dblRateCardTotal), "0.00#")
        frmJob.txtAdditionalDiscount.Text = g_strCurrency & Format(RoundToCurrency(l_dblAdditionalDiscount), "0.00#")
        
        frmJob.txtTotalDiscountPercent.Text = GetPercentageDiscount(l_dblRateCardTotal, l_dblJobTotal)
End If
    
    Screen.MousePointer = vbDefault
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description, vbExclamation, CStr(Err.Number)
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetMinimumCharge(lp_strCostCode As String, lp_lngCompanyID As Long) As Double

Dim l_dblPrice As Double, l_rst As ADODB.Recordset

Set l_rst = ExecuteSQL("SELECT minimumcharge FROM ratecard WHERE companyID = '" & lp_lngCompanyID & "' AND cetacode = '" & QuoteSanitise(lp_strCostCode) & "';", g_strExecuteError)

If l_rst.RecordCount > 0 Then
    l_dblPrice = l_rst(0)
Else
    l_dblPrice = GetDataSQL("SELECT TOP 1 minimumcharge FROM ratecard WHERE companyID = '0' AND cetacode = '" & QuoteSanitise(lp_strCostCode) & "';")
End If

l_rst.Close
Set l_rst = Nothing

GetMinimumCharge = l_dblPrice

End Function

Function GetPrice(ByVal lp_strCostCode As String, ByVal lp_strRateCardColumn As String, ByVal lp_lngCompanyID As Long, Optional ByVal lp_strUnit As String, Optional ByVal lp_lngProductID As Long)

Dim l_dblPrice As Double
Dim l_strAdditionalSQL  As String


If UCase(g_strRateCardType) = "HIRE" Then GoTo PROC_HireRateCard

PROC_StandardRateCard:

If lp_lngProductID = 0 Then

    'check to see if a unit was specified
    If lp_strUnit <> "" Then
        l_strAdditionalSQL = " AND unit = '" & QuoteSanitise(lp_strUnit) & "'"
    End If

PROC_StandardCosting:
    'normal costing - not using the new product rate cards
    
    l_dblPrice = GetDataSQL("SELECT TOP 1 price" & lp_strRateCardColumn & " FROM ratecard WHERE companyID = '" & lp_lngCompanyID & "' AND cetacode = '" & QuoteSanitise(lp_strCostCode) & "'" & l_strAdditionalSQL & ";")
    
    If l_dblPrice = 0 Then
        l_dblPrice = GetDataSQL("SELECT TOP 1 price" & lp_strRateCardColumn & " FROM ratecard WHERE companyID = '0' AND cetacode = '" & QuoteSanitise(lp_strCostCode) & "'" & l_strAdditionalSQL & ";")
    End If
    
    GoTo PROC_COMPLETE

Else
    'use theproduct based rate cards when a product ID is specified

PROC_ProductCosting:

    Dim l_lngCustomRateCardID As Long
    l_lngCustomRateCardID = GetData("product", "customratecardID", "productID", lp_lngProductID)
    
    'if there is no custom rate card them skip to standard costing
    If l_lngCustomRateCardID = 0 Then GoTo PROC_StandardCosting
    
    Dim l_lngCustomRateCardDetailID  As Long
    
    'get the price for this unit type
    l_lngCustomRateCardDetailID = Val(GetDataSQL("SELECT TOP 1 customratecarddetailID FROM customratecarddetail WHERE customratecardID = '" & l_lngCustomRateCardID & "' AND costcode = '" & QuoteSanitise(lp_strCostCode) & "'" & l_strAdditionalSQL & ";"))
    
    'found a costing record so use this price
    If l_lngCustomRateCardDetailID > 0 Then
        l_dblPrice = GetData("customratecarddetail", "price", "customratecarddetailID", l_lngCustomRateCardDetailID)
        GoTo PROC_COMPLETE
    End If
    
    'still no price, try without the unit type.
    l_lngCustomRateCardDetailID = Val(GetDataSQL("SELECT TOP 1 customratecarddetailID FROM customratecarddetail WHERE customratecardID = '" & l_lngCustomRateCardID & "' AND costcode = '" & QuoteSanitise(lp_strCostCode) & "';"))
    
    If l_lngCustomRateCardDetailID > 0 Then
        l_dblPrice = GetData("customratecarddetail", "price", "customratecarddetailID", l_lngCustomRateCardDetailID)
        GoTo PROC_COMPLETE
    Else
        'no price found still so do the normal costing
        GoTo PROC_StandardCosting
    End If
    
End If

PROC_HireRateCard:

If lp_strUnit = "" Then GoTo PROC_StandardRateCard

    Select Case LCase(lp_strUnit)
    Case "each", "hour", "halfday", "day", "week", "month"
    
        l_dblPrice = GetDataSQL("SELECT TOP 1 " & lp_strUnit & "rate FROM ratecard WHERE companyID = '" & lp_lngCompanyID & "' AND cetacode = '" & QuoteSanitise(lp_strCostCode) & "';")
        
        If l_dblPrice = 0 Then
            l_dblPrice = GetDataSQL("SELECT TOP 1 " & lp_strUnit & "rate FROM ratecard WHERE companyID = '0' AND cetacode = '" & QuoteSanitise(lp_strCostCode) & "';")
        End If

    Case Else
        GoTo PROC_StandardRateCard
        
    End Select

PROC_COMPLETE:

GetPrice = l_dblPrice

End Function

Function IsVirtualCode(ByVal lp_strCostCode As String) As Boolean

Dim l_intNoRateCardPrice As Integer

l_intNoRateCardPrice = GetData("ratecard", "noratecardprice", "cetacode", lp_strCostCode)

If l_intNoRateCardPrice = 0 Then
    IsVirtualCode = False
Else
    IsVirtualCode = True
End If

End Function

Sub PrintUninvoicedWorkReport()

Dim l_strSQL As String, l_rstJobs As ADODB.Recordset, l_rstCostings As ADODB.Recordset

If MsgBox("Do you wish to go through all the un-invoiced jobs, generating costings?", vbYesNo, "Print Uninvoiced Work Report") = vbYes Then

    l_strSQL = "SELECT jobID FROM job WHERE completeddate IS NULL AND (flagquote IS NULL or flagquote = 0) AND createddate > '01-01-" & Year(Now) & "'"
    l_strSQL = l_strSQL & "AND companyID > 100 ORDER BY jobID;"
    Set l_rstJobs = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    If l_rstJobs.RecordCount > 0 Then
        l_rstJobs.MoveFirst
        Do While Not l_rstJobs.EOF
            l_strSQL = "SELECT count(costingID) FROM costing WHERE jobID = " & l_rstJobs("jobID") & ";"
            Set l_rstCostings = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            If l_rstCostings(0) = 0 Then
                'MsgBox "Calculating Costing for Job " & l_rstJobs("jobID"), vbInformation, "Costing Jobs"
                CostJob l_rstJobs("jobID"), 0 - GetFlag(g_optCostDubbing), 0 - GetFlag(g_optCostSchedule), True
            End If
            l_rstCostings.Close
            l_rstJobs.MoveNext
        Loop
    End If
    l_rstJobs.Close
    Set l_rstCostings = Nothing
    Set l_rstJobs = Nothing

End If

PrintCrystalReport g_strLocationOfCrystalReportFiles & "WorkList_NotInvoiced.rpt", "", True

End Sub

Sub PrintUninvoicedWorkreportAll()

Dim l_strSQL As String, l_rstJobs As ADODB.Recordset, l_rstCostings As ADODB.Recordset

If MsgBox("Do you wish to go through all the un-invoiced jobs, generating costings?", vbYesNo, "Print Uninvoiced Work Report") = vbYes Then

    l_strSQL = "SELECT jobID FROM job WHERE completeddate IS NULL AND (flagquote IS NULL or flagquote = 0) AND createddate > '01-01-" & Year(Now) & "'"
    l_strSQL = l_strSQL & "AND companyID > 100 ORDER BY jobID;"
    Set l_rstJobs = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    If l_rstJobs.RecordCount > 0 Then
        l_rstJobs.MoveFirst
        Do While Not l_rstJobs.EOF
            l_strSQL = "SELECT count(costingID) FROM costing WHERE jobID = " & l_rstJobs("jobID") & ";"
            Set l_rstCostings = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            If l_rstCostings(0) = 0 Then
                'MsgBox "Calculating Costing for Job " & l_rstJobs("jobID"), vbInformation, "Costing Jobs"
                CostJob l_rstJobs("jobID"), 0 - GetFlag(g_optCostDubbing), 0 - GetFlag(g_optCostSchedule), True
            End If
            l_rstCostings.Close
            l_rstJobs.MoveNext
        Loop
    End If
    l_rstJobs.Close
    Set l_rstCostings = Nothing
    Set l_rstJobs = Nothing

End If

PrintCrystalReport g_strLocationOfCrystalReportFiles & "WorkList_NotInvoiced_all.rpt", "", True

End Sub
