VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTrackerSvensk 
   Caption         =   "Svensk Tracker"
   ClientHeight    =   15930
   ClientLeft      =   3060
   ClientTop       =   3210
   ClientWidth     =   28725
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   15930
   ScaleWidth      =   28725
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   34
      Left            =   21300
      TabIndex        =   374
      Top             =   3660
      Width           =   195
   End
   Begin VB.CheckBox chkMoreThan5Fixes 
      BackColor       =   &H80000004&
      Caption         =   "More than 5 Fixes"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   19020
      TabIndex        =   373
      Top             =   3660
      Width           =   2235
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   33
      Left            =   7620
      TabIndex        =   372
      Top             =   1500
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   32
      Left            =   7620
      TabIndex        =   371
      Top             =   1140
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   31
      Left            =   7620
      TabIndex        =   370
      Top             =   780
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   30
      Left            =   7620
      TabIndex        =   369
      Top             =   420
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   29
      Left            =   7620
      TabIndex        =   368
      Top             =   60
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   28
      Left            =   4260
      TabIndex        =   367
      Top             =   1500
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   24
      Left            =   4260
      TabIndex        =   366
      Top             =   1860
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   27
      Left            =   4260
      TabIndex        =   365
      Top             =   1140
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   26
      Left            =   4260
      TabIndex        =   364
      Top             =   780
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   25
      Left            =   4260
      TabIndex        =   363
      Top             =   60
      Width           =   195
   End
   Begin VB.CommandButton cmdDuplicateGridForResupply 
      Caption         =   "Duplicate Grid for Resupply"
      Height          =   315
      Left            =   25200
      TabIndex        =   362
      Top             =   2580
      Width           =   2355
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Validation List"
      Height          =   255
      Index           =   25
      Left            =   9840
      TabIndex        =   361
      Tag             =   "NOCLEAR"
      Top             =   1560
      Width           =   1815
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   23
      Left            =   4260
      TabIndex        =   360
      Top             =   420
      Width           =   195
   End
   Begin VB.TextBox txtAlphaCode 
      Height          =   315
      Left            =   1620
      TabIndex        =   359
      Top             =   360
      Width           =   2595
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   22
      Left            =   24780
      TabIndex        =   355
      Top             =   3360
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   8
      Left            =   4260
      TabIndex        =   354
      Top             =   2220
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   21
      Left            =   4260
      TabIndex        =   353
      Top             =   2940
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   20
      Left            =   4260
      TabIndex        =   352
      Top             =   2580
      Width           =   195
   End
   Begin VB.CheckBox chkDTAudioSimpleConform 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Simp Conform"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   19020
      TabIndex        =   328
      Top             =   3300
      Width           =   2235
   End
   Begin VB.CheckBox chkDTTimecode 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Timecode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   19020
      TabIndex        =   327
      Top             =   780
      Width           =   2235
   End
   Begin VB.CheckBox chkDTStereoDownmix 
      BackColor       =   &H00C0C0FF&
      Caption         =   "ST Downmix"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   19020
      TabIndex        =   326
      Top             =   2220
      Width           =   2235
   End
   Begin VB.CheckBox chkDTRemoveReleaseDates 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Rem Rel Dates"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   19020
      TabIndex        =   325
      Top             =   2580
      Width           =   2235
   End
   Begin VB.CheckBox chkDTAudioFramerateConvert 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Audio Fps Conv"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   19020
      TabIndex        =   324
      Top             =   2940
      Width           =   2235
   End
   Begin VB.CheckBox chkDTBlackAtEnd 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Black At End"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   19020
      TabIndex        =   323
      Top             =   1140
      Width           =   2235
   End
   Begin VB.CheckBox chkDTAudioChannelConfig 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Audio Ch Fix"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   19020
      TabIndex        =   322
      Top             =   1500
      Width           =   2235
   End
   Begin VB.CheckBox chkDTQuicktimeTagging 
      BackColor       =   &H00C0C0FF&
      Caption         =   "QT Tagging"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   19020
      TabIndex        =   321
      Top             =   1860
      Width           =   2235
   End
   Begin VB.CheckBox chkDTBarsAndTone 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Bars and Tone"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   19020
      TabIndex        =   320
      Top             =   420
      Width           =   2235
   End
   Begin VB.CheckBox chkDTAddClock 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Add Clock"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   19020
      TabIndex        =   319
      Top             =   60
      Width           =   2235
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   19
      Left            =   21300
      TabIndex        =   318
      Top             =   3300
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   18
      Left            =   21300
      TabIndex        =   317
      Top             =   2940
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   17
      Left            =   21300
      TabIndex        =   316
      Top             =   2580
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   16
      Left            =   21300
      TabIndex        =   315
      Top             =   2220
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   15
      Left            =   21300
      TabIndex        =   314
      Top             =   1860
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   14
      Left            =   21300
      TabIndex        =   313
      Top             =   1500
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   13
      Left            =   21300
      TabIndex        =   312
      Top             =   1140
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   12
      Left            =   21300
      TabIndex        =   311
      Top             =   780
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   11
      Left            =   21300
      TabIndex        =   310
      Top             =   420
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   10
      Left            =   21300
      TabIndex        =   309
      Top             =   60
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   9
      Left            =   24780
      TabIndex        =   306
      Top             =   3000
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   7
      Left            =   24780
      TabIndex        =   304
      Top             =   2640
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   6
      Left            =   24780
      TabIndex        =   302
      Top             =   2280
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   5
      Left            =   24780
      TabIndex        =   300
      Top             =   1920
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   4
      Left            =   24780
      TabIndex        =   298
      Top             =   1560
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   3
      Left            =   24780
      TabIndex        =   296
      Top             =   1200
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   2
      Left            =   24780
      TabIndex        =   294
      Top             =   840
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   1
      Left            =   24780
      TabIndex        =   292
      Top             =   480
      Width           =   195
   End
   Begin VB.TextBox txtSeries 
      Height          =   315
      Left            =   5760
      TabIndex        =   291
      Top             =   0
      Width           =   1815
   End
   Begin VB.CommandButton cmdCommentsBulk 
      Caption         =   "Comments Bulk Update"
      Height          =   435
      Left            =   16920
      TabIndex        =   290
      Top             =   2040
      Width           =   1935
   End
   Begin VB.PictureBox fraUpdateComments 
      Height          =   8775
      Left            =   9180
      ScaleHeight     =   8715
      ScaleWidth      =   8775
      TabIndex        =   283
      Top             =   5880
      Visible         =   0   'False
      Width           =   8835
      Begin VB.ComboBox cmbCommentType 
         Height          =   315
         Left            =   2040
         TabIndex        =   351
         Top             =   5640
         Width           =   2895
      End
      Begin VB.CheckBox chkissueresolved 
         Caption         =   "Check2"
         Height          =   255
         Left            =   2040
         TabIndex        =   347
         Top             =   6720
         Width           =   255
      End
      Begin VB.CheckBox chkemailtextlesspresent 
         Caption         =   "Check1"
         Height          =   315
         Left            =   2040
         TabIndex        =   346
         Top             =   6180
         Width           =   255
      End
      Begin VB.TextBox txtUpExternalFile 
         Height          =   375
         Left            =   2040
         TabIndex        =   343
         Top             =   5100
         Width           =   6315
      End
      Begin VB.TextBox txtUpInternalFile 
         Height          =   375
         Left            =   2040
         TabIndex        =   342
         Top             =   1980
         Width           =   6315
      End
      Begin VB.CommandButton cmdUpdateExternalRows 
         BackColor       =   &H00C0C0FF&
         Caption         =   "Bulk Update External Comments Selected Rows"
         Height          =   735
         Left            =   2040
         Style           =   1  'Graphical
         TabIndex        =   341
         Top             =   7140
         Width           =   2235
      End
      Begin VB.CommandButton cmdUpdateExternalGrid 
         BackColor       =   &H00FFFFC0&
         Caption         =   "Bulk Update External Comments Grid"
         Height          =   735
         Left            =   6060
         Style           =   1  'Graphical
         TabIndex        =   340
         Top             =   7140
         Width           =   2235
      End
      Begin VB.CommandButton cmdUpdateInternalGrid 
         BackColor       =   &H00FFFFC0&
         Caption         =   "Bulk Update Internal Comments Grid"
         Height          =   735
         Left            =   6120
         Style           =   1  'Graphical
         TabIndex        =   339
         Top             =   2520
         Width           =   2235
      End
      Begin VB.CommandButton cmdUpdateInternalRows 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0FF&
         Caption         =   "Bulk Update Internal Comments Selected Rows"
         Height          =   735
         Left            =   2040
         Style           =   1  'Graphical
         TabIndex        =   338
         Top             =   2520
         UseMaskColor    =   -1  'True
         Width           =   2235
      End
      Begin VB.TextBox txtUpExternalComments 
         Height          =   1155
         Left            =   2040
         MultiLine       =   -1  'True
         TabIndex        =   288
         Top             =   3840
         Width           =   6315
      End
      Begin VB.CommandButton cmdCancelUpdate 
         Caption         =   "Cancel"
         Height          =   315
         Left            =   300
         TabIndex        =   285
         Top             =   8160
         Width           =   1395
      End
      Begin VB.TextBox txtUpInternalComments 
         Height          =   1155
         Left            =   2040
         MultiLine       =   -1  'True
         TabIndex        =   284
         Top             =   720
         Width           =   6315
      End
      Begin VB.Label Label12 
         Caption         =   "Resolved"
         Height          =   435
         Index           =   4
         Left            =   360
         TabIndex        =   350
         Top             =   6720
         Width           =   1635
      End
      Begin VB.Label Label12 
         Caption         =   "Textless Not Rq but Present"
         Height          =   435
         Index           =   3
         Left            =   360
         TabIndex        =   349
         Top             =   6180
         Width           =   1515
      End
      Begin VB.Label Label12 
         Caption         =   "Comment Type"
         Height          =   435
         Index           =   2
         Left            =   360
         TabIndex        =   348
         Top             =   5640
         Width           =   1635
      End
      Begin VB.Line Line1 
         BorderStyle     =   6  'Inside Solid
         DrawMode        =   2  'Blackness
         X1              =   360
         X2              =   8340
         Y1              =   3540
         Y2              =   3540
      End
      Begin VB.Label Label12 
         Caption         =   "File Location"
         Height          =   435
         Index           =   1
         Left            =   360
         TabIndex        =   345
         Top             =   5100
         Width           =   1635
      End
      Begin VB.Label Label12 
         Caption         =   "File Location"
         Height          =   435
         Index           =   0
         Left            =   360
         TabIndex        =   344
         Top             =   1920
         Width           =   1635
      End
      Begin VB.Label Label11 
         Caption         =   "External Comments"
         Height          =   555
         Left            =   360
         TabIndex        =   289
         Top             =   3900
         Width           =   1695
      End
      Begin VB.Label Label20 
         Alignment       =   2  'Center
         Caption         =   "Comments Bulk Update"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   -3660
         TabIndex        =   287
         Top             =   -420
         Width           =   8655
      End
      Begin VB.Label Label19 
         Caption         =   "Internal Comments"
         Height          =   255
         Left            =   360
         TabIndex        =   286
         Top             =   720
         Width           =   1575
      End
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   0
      Left            =   24780
      TabIndex        =   282
      Top             =   120
      Width           =   195
   End
   Begin VB.CommandButton cmdBulkUpdate 
      BackColor       =   &H00FFFFC0&
      Caption         =   "Bulk Update Grid"
      Height          =   435
      Left            =   16920
      Style           =   1  'Graphical
      TabIndex        =   280
      ToolTipText     =   "Clear the form"
      Top             =   2580
      Width           =   1935
   End
   Begin VB.CommandButton cmdBulkUpdateSelected 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Bulk Update Selected Rows"
      Height          =   495
      Left            =   16920
      Style           =   1  'Graphical
      TabIndex        =   279
      ToolTipText     =   "Clear the form"
      Top             =   3120
      Width           =   1935
   End
   Begin VB.CheckBox chkOperatorIsNull 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Operator is Null"
      Height          =   195
      Left            =   5580
      TabIndex        =   277
      Top             =   2580
      Width           =   1935
   End
   Begin VB.CheckBox chkSpecialProjectIsNull 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Special Project is Null"
      Height          =   195
      Left            =   5580
      TabIndex        =   276
      Top             =   2220
      Width           =   1935
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Internal Pending"
      Height          =   255
      Index           =   15
      Left            =   8040
      TabIndex        =   275
      Tag             =   "NOCLEAR"
      Top             =   660
      Width           =   1635
   End
   Begin VB.ComboBox txtSpecialProject 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1620
      TabIndex        =   274
      ToolTipText     =   "What Copy Type is the tape?"
      Top             =   2520
      Width           =   2595
   End
   Begin VB.ComboBox cmbSorting 
      Height          =   315
      Left            =   5580
      TabIndex        =   272
      Top             =   2940
      Width           =   2055
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Urgent + Workable"
      Height          =   255
      Index           =   23
      Left            =   9840
      TabIndex        =   270
      Tag             =   "NOCLEAR"
      Top             =   1260
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "File Sent and Not Complete"
      ForeColor       =   &H00C00000&
      Height          =   255
      Index           =   22
      Left            =   11760
      TabIndex        =   269
      Tag             =   "NOCLEAR"
      Top             =   960
      Width           =   2295
   End
   Begin VB.ComboBox cmbOperator 
      Height          =   315
      Left            =   1620
      TabIndex        =   267
      Top             =   2880
      Width           =   2595
   End
   Begin VB.ComboBox cmbTitle 
      Height          =   315
      Left            =   1620
      TabIndex        =   266
      Top             =   720
      Width           =   2595
   End
   Begin VB.Frame Frame15 
      Height          =   555
      Left            =   23880
      TabIndex        =   262
      Top             =   15480
      Visible         =   0   'False
      Width           =   3795
      Begin VB.OptionButton optSortOrder 
         Caption         =   "Invoice sorting"
         Height          =   255
         Index           =   1
         Left            =   2040
         TabIndex        =   264
         Top             =   180
         Width           =   1575
      End
      Begin VB.OptionButton optSortOrder 
         Caption         =   "Normal sorting"
         Height          =   255
         Index           =   0
         Left            =   180
         TabIndex        =   263
         Top             =   180
         Width           =   1575
      End
   End
   Begin VB.Frame Frame14 
      Caption         =   "Date Searches"
      Height          =   1875
      Left            =   16320
      TabIndex        =   255
      Top             =   60
      Width           =   2535
      Begin VB.CommandButton cmdClearRadioButtons 
         Caption         =   "Clr"
         Height          =   315
         Left            =   180
         TabIndex        =   278
         Top             =   1260
         Width           =   495
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "Work On Today"
         ForeColor       =   &H00C00000&
         Height          =   255
         Index           =   24
         Left            =   900
         TabIndex        =   271
         Tag             =   "NOCLEAR"
         Top             =   1560
         Width           =   1515
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "Target Date"
         ForeColor       =   &H00C00000&
         Height          =   315
         Index           =   20
         Left            =   900
         TabIndex        =   257
         Tag             =   "NOCLEAR"
         Top             =   960
         Width           =   1515
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "Due Date"
         ForeColor       =   &H00C00000&
         Height          =   255
         Index           =   21
         Left            =   900
         TabIndex        =   256
         Tag             =   "NOCLEAR"
         Top             =   1260
         Width           =   1515
      End
      Begin MSComCtl2.DTPicker datFrom 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Left            =   720
         TabIndex        =   258
         Tag             =   "NOCHECK"
         ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
         Top             =   240
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   167378945
         CurrentDate     =   39580
      End
      Begin MSComCtl2.DTPicker datTo 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Left            =   720
         TabIndex        =   259
         Tag             =   "NOCHECK"
         ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
         Top             =   600
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   167378945
         CurrentDate     =   39580
      End
      Begin VB.Label lblCaption 
         Caption         =   "Start"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   34
         Left            =   120
         TabIndex        =   261
         Top             =   300
         Width           =   795
      End
      Begin VB.Label lblCaption 
         Caption         =   "End"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   35
         Left            =   120
         TabIndex        =   260
         Top             =   660
         Width           =   735
      End
   End
   Begin VB.TextBox txtJobID 
      Height          =   315
      Left            =   5760
      TabIndex        =   253
      Top             =   1440
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "All Items"
      Height          =   255
      Index           =   4
      Left            =   8040
      TabIndex        =   252
      Tag             =   "NOCLEAR"
      Top             =   1860
      Width           =   1455
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnUsers 
      Height          =   855
      Left            =   23100
      TabIndex        =   251
      Top             =   11160
      Width           =   2175
      DataFieldList   =   "column 0"
      ListAutoValidate=   0   'False
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   3200
      Columns(0).Caption=   "value"
      Columns(0).Name =   "value"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   3836
      _ExtentY        =   1508
      _StockProps     =   77
      DataFieldToDisplay=   "column 0"
   End
   Begin VB.CommandButton cmdFieldsDecisionTree 
      Caption         =   "Operator"
      Height          =   315
      Left            =   12180
      TabIndex        =   249
      Top             =   2340
      Width           =   2175
   End
   Begin VB.CommandButton cmdBreakoutLines 
      Caption         =   "Special Command"
      Height          =   495
      Left            =   18600
      TabIndex        =   248
      Top             =   15600
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Frame Frame13 
      Caption         =   "Bulk Management"
      Height          =   1575
      Left            =   25140
      TabIndex        =   243
      Top             =   16080
      Visible         =   0   'False
      Width           =   2955
      Begin VB.CommandButton cmdPendingOn 
         Caption         =   "On Pending"
         Height          =   495
         Left            =   300
         TabIndex        =   246
         Top             =   840
         Width           =   1095
      End
      Begin VB.CommandButton cmdPendingOff 
         Caption         =   "Off Pending"
         Height          =   495
         Left            =   1560
         TabIndex        =   245
         Top             =   840
         Width           =   1095
      End
      Begin VB.TextBox Text1 
         Height          =   315
         Left            =   960
         TabIndex        =   244
         Top             =   360
         Width           =   1755
      End
      Begin VB.Label Label10 
         Caption         =   "Comment"
         Height          =   375
         Left            =   180
         TabIndex        =   247
         Top             =   420
         Width           =   735
      End
   End
   Begin VB.CommandButton cmdMakeFixJobsheet 
      Caption         =   "Make Fix Jobsheet for Item"
      Height          =   315
      Left            =   25200
      TabIndex        =   242
      Top             =   2940
      Width           =   2355
   End
   Begin VB.CommandButton cmdFieldsSummary 
      Caption         =   "Summary Fields"
      Height          =   315
      Left            =   7740
      TabIndex        =   241
      Top             =   2340
      Width           =   2175
   End
   Begin VB.CommandButton cmdDuplicateLineForResupply 
      Caption         =   "Duplicate Item for Resupply"
      Height          =   315
      Left            =   25200
      TabIndex        =   239
      Top             =   2220
      Width           =   2355
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Urgent"
      Height          =   255
      Index           =   19
      Left            =   9840
      TabIndex        =   238
      Tag             =   "NOCLEAR"
      Top             =   960
      Width           =   1395
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnCommentTypes 
      Height          =   855
      Left            =   16200
      TabIndex        =   129
      Top             =   16860
      Width           =   2295
      DataFieldList   =   "column 0"
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   3200
      Columns(0).Caption=   "value"
      Columns(0).Name =   "value"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4048
      _ExtentY        =   1508
      _StockProps     =   77
      DataFieldToDisplay=   "column 0"
   End
   Begin VB.PictureBox picValidationSubs 
      Height          =   4755
      Left            =   11400
      ScaleHeight     =   4695
      ScaleWidth      =   4335
      TabIndex        =   188
      Top             =   15540
      Visible         =   0   'False
      Width           =   4395
      Begin VB.CommandButton cmdSubsValidationSave 
         Caption         =   "Save Validation Info"
         Height          =   375
         Left            =   180
         TabIndex        =   217
         Top             =   4080
         Width           =   2055
      End
      Begin VB.Frame Frame46 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   2340
         TabIndex        =   214
         Top             =   3540
         Width           =   975
         Begin VB.OptionButton optSubsValidationComplete 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   216
            Top             =   120
            Width           =   195
         End
         Begin VB.OptionButton optSubsValidationComplete 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   215
            Top             =   120
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.CommandButton cmdCloseSubsValidation 
         Caption         =   "Close"
         Height          =   375
         Left            =   2460
         TabIndex        =   213
         Top             =   4080
         Width           =   1695
      End
      Begin VB.Frame Frame36 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   209
         Top             =   1500
         Width           =   1455
         Begin VB.OptionButton optEnglishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   212
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optEnglishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   211
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optEnglishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   210
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame35 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   205
         Top             =   1860
         Width           =   1455
         Begin VB.OptionButton optNorwegianSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   208
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optNorwegianSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   207
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optNorwegianSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   206
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.Frame Frame34 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   201
         Top             =   2220
         Width           =   1455
         Begin VB.OptionButton optSwedishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   204
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optSwedishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   203
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optSwedishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   202
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame33 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   197
         Top             =   2580
         Width           =   1455
         Begin VB.OptionButton optDanishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   200
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optDanishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   199
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optDanishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   198
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame32 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   193
         Top             =   2940
         Width           =   1455
         Begin VB.OptionButton optFinnishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   196
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optFinnishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   195
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optFinnishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   194
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.Frame Frame24 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   189
         Top             =   3300
         Width           =   1455
         Begin VB.OptionButton optIcelandicSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   192
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optIcelandicSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   191
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optIcelandicSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   190
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Label lblCaption 
         Caption         =   "Sr."
         Height          =   255
         Index           =   71
         Left            =   240
         TabIndex        =   234
         Top             =   780
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Caption         =   "Ep."
         Height          =   255
         Index           =   70
         Left            =   900
         TabIndex        =   233
         Top             =   780
         Width           =   255
      End
      Begin VB.Label Label9 
         Alignment       =   2  'Center
         Caption         =   "Subs File Validation"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   315
         Left            =   0
         TabIndex        =   232
         Top             =   60
         Width           =   4275
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "Yes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   69
         Left            =   2400
         TabIndex        =   231
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "No"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   68
         Left            =   2940
         TabIndex        =   230
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Caption         =   "Validation Complete?"
         Height          =   255
         Index           =   67
         Left            =   240
         TabIndex        =   229
         Top             =   3660
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "N/A"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   61
         Left            =   3480
         TabIndex        =   228
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label Label8 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   780
         TabIndex        =   227
         Top             =   420
         Width           =   3555
      End
      Begin VB.Label Label7 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   480
         TabIndex        =   226
         Top             =   780
         Width           =   375
      End
      Begin VB.Label Label6 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   1200
         TabIndex        =   225
         Top             =   780
         Width           =   375
      End
      Begin VB.Label lblCaption 
         Caption         =   "Title:"
         Height          =   255
         Index           =   51
         Left            =   240
         TabIndex        =   224
         Top             =   480
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Caption         =   "English Subs In Spec ?"
         Height          =   255
         Index           =   41
         Left            =   240
         TabIndex        =   223
         Top             =   1500
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Norwegian Subs In Spec ?"
         Height          =   255
         Index           =   40
         Left            =   240
         TabIndex        =   222
         Top             =   1860
         Width           =   1995
      End
      Begin VB.Label lblCaption 
         Caption         =   "Swedish Subs In Spec ?"
         Height          =   255
         Index           =   39
         Left            =   240
         TabIndex        =   221
         Top             =   2220
         Width           =   1875
      End
      Begin VB.Label lblCaption 
         Caption         =   "Danish Subs In Spec ?"
         Height          =   255
         Index           =   38
         Left            =   240
         TabIndex        =   220
         Top             =   2580
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Finnish Subs In Spec ?"
         Height          =   255
         Index           =   37
         Left            =   240
         TabIndex        =   219
         Top             =   2940
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Icelandic Subs In Spec ?"
         Height          =   255
         Index           =   36
         Left            =   240
         TabIndex        =   218
         Top             =   3300
         Width           =   1815
      End
   End
   Begin VB.PictureBox picValidationAudioFiles 
      Height          =   4755
      Left            =   6960
      ScaleHeight     =   4695
      ScaleWidth      =   4335
      TabIndex        =   133
      Top             =   15540
      Visible         =   0   'False
      Width           =   4395
      Begin VB.Frame Frame23 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   184
         Top             =   2580
         Width           =   1455
         Begin VB.OptionButton optDanishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   187
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optDanishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   186
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optDanishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   185
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.Frame Frame22 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   180
         Top             =   2940
         Width           =   1455
         Begin VB.OptionButton optFinnishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   183
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optFinnishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   182
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optFinnishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   181
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame18 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   176
         Top             =   3300
         Width           =   1455
         Begin VB.OptionButton optIcelandicInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   179
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optIcelandicInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   178
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optIcelandicInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   177
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.Frame Frame16 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   158
         Top             =   6840
         Width           =   1455
      End
      Begin VB.Frame Frame17 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   157
         Top             =   6480
         Width           =   1455
      End
      Begin VB.Frame Frame19 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   156
         Top             =   5760
         Width           =   1455
      End
      Begin VB.Frame Frame20 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   155
         Top             =   5400
         Width           =   1455
      End
      Begin VB.Frame Frame21 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   154
         Top             =   5040
         Width           =   1455
      End
      Begin VB.CommandButton cmdCloseAudioValidation 
         Caption         =   "Close"
         Height          =   375
         Left            =   2460
         TabIndex        =   153
         Top             =   4080
         Width           =   1695
      End
      Begin VB.Frame Frame25 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   149
         Top             =   2220
         Width           =   1455
         Begin VB.OptionButton optSwedishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   152
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optSwedishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   151
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optSwedishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   150
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame26 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   145
         Top             =   1860
         Width           =   1455
         Begin VB.OptionButton optNorwegianInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   148
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optNorwegianInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   147
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optNorwegianInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   146
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.Frame Frame27 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   141
         Top             =   1500
         Width           =   1455
         Begin VB.OptionButton optEnglishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   144
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optEnglishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   143
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optEnglishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   142
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame28 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   140
         Top             =   2520
         Width           =   975
      End
      Begin VB.Frame Frame29 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   139
         Top             =   2160
         Width           =   975
      End
      Begin VB.Frame Frame30 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   138
         Top             =   1440
         Width           =   975
      End
      Begin VB.Frame Frame31 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   2340
         TabIndex        =   135
         Top             =   3540
         Width           =   975
         Begin VB.OptionButton optAudioValidationComplete 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   137
            Top             =   120
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optAudioValidationComplete 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   136
            Top             =   120
            Width           =   195
         End
      End
      Begin VB.CommandButton cmdAudioValidationSave 
         Caption         =   "Save Validation Info"
         Height          =   375
         Left            =   180
         TabIndex        =   134
         Top             =   4080
         Width           =   2055
      End
      Begin VB.Label lblCaption 
         Caption         =   "Icelandic In Spec ?"
         Height          =   255
         Index           =   42
         Left            =   240
         TabIndex        =   175
         Top             =   3300
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Title:"
         Height          =   255
         Index           =   43
         Left            =   240
         TabIndex        =   174
         Top             =   480
         Width           =   315
      End
      Begin VB.Label Label2 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   1200
         TabIndex        =   173
         Top             =   780
         Width           =   375
      End
      Begin VB.Label Label3 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   480
         TabIndex        =   172
         Top             =   780
         Width           =   375
      End
      Begin VB.Label Label4 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   720
         TabIndex        =   171
         Top             =   480
         Width           =   3555
      End
      Begin VB.Label lblCaption 
         Caption         =   "Finnish In Spec ?"
         Height          =   255
         Index           =   44
         Left            =   240
         TabIndex        =   170
         Top             =   2940
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Danish In Spec ?"
         Height          =   255
         Index           =   45
         Left            =   240
         TabIndex        =   169
         Top             =   2580
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Swedish In Spec ?"
         Height          =   255
         Index           =   46
         Left            =   240
         TabIndex        =   168
         Top             =   2220
         Width           =   1875
      End
      Begin VB.Label lblCaption 
         Caption         =   "Norwegian In Spec ?"
         Height          =   255
         Index           =   47
         Left            =   240
         TabIndex        =   167
         Top             =   1860
         Width           =   1995
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "N/A"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   48
         Left            =   3480
         TabIndex        =   166
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Caption         =   "English In Spec ?"
         Height          =   255
         Index           =   49
         Left            =   240
         TabIndex        =   165
         Top             =   1500
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Validation Complete?"
         Height          =   255
         Index           =   54
         Left            =   240
         TabIndex        =   164
         Top             =   3660
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "No"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   55
         Left            =   2940
         TabIndex        =   163
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "Yes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   56
         Left            =   2400
         TabIndex        =   162
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label Label5 
         Alignment       =   2  'Center
         Caption         =   "Audio File(s) Validation"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   315
         Left            =   0
         TabIndex        =   161
         Top             =   60
         Width           =   4275
      End
      Begin VB.Label lblCaption 
         Caption         =   "Ep."
         Height          =   255
         Index           =   57
         Left            =   900
         TabIndex        =   160
         Top             =   780
         Width           =   255
      End
      Begin VB.Label lblCaption 
         Caption         =   "Sr."
         Height          =   255
         Index           =   58
         Left            =   240
         TabIndex        =   159
         Top             =   780
         Width           =   315
      End
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Workable"
      Height          =   255
      Index           =   18
      Left            =   9840
      TabIndex        =   132
      Tag             =   "NOCLEAR"
      Top             =   660
      Width           =   1395
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Workable"
      Height          =   255
      Index           =   17
      Left            =   9840
      TabIndex        =   131
      Tag             =   "NOCLEAR"
      Top             =   360
      Width           =   1395
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnProjectManager 
      Bindings        =   "frmTrackerSvensk.frx":0000
      Height          =   1755
      Left            =   13920
      TabIndex        =   130
      Top             =   9840
      Width           =   5775
      DataFieldList   =   "name"
      ListAutoValidate=   0   'False
      MaxDropDownItems=   20
      _Version        =   196617
      ColumnHeaders   =   0   'False
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   2
      Columns(0).Width=   9710
      Columns(0).Caption=   "Name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   8819
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "contactID"
      Columns(1).Name =   "contactID"
      Columns(1).DataField=   "contactID"
      Columns(1).FieldLen=   256
      _ExtentX        =   10186
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "name"
   End
   Begin VB.ComboBox cmbProjectManager 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1620
      TabIndex        =   122
      ToolTipText     =   "What Copy Type is the tape?"
      Top             =   2160
      Width           =   2595
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnChannel 
      Bindings        =   "frmTrackerSvensk.frx":001B
      Height          =   1755
      Left            =   720
      TabIndex        =   120
      Top             =   10020
      Width           =   5775
      DataFieldList   =   "portalcompanyname"
      ListAutoValidate=   0   'False
      MaxDropDownItems=   20
      _Version        =   196617
      ColumnHeaders   =   0   'False
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   2
      Columns(0).Width=   9710
      Columns(0).Caption=   "Name"
      Columns(0).Name =   "portalcompanyname"
      Columns(0).DataField=   "portalcompanyname"
      Columns(0).FieldLen=   256
      Columns(1).Width=   8819
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   10186
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "portalcompanyname"
   End
   Begin VB.CommandButton cmdFieldsAssets 
      Caption         =   "Assets Fields"
      Height          =   375
      Left            =   21180
      TabIndex        =   111
      Top             =   15720
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CommandButton cmdFieldsAudio 
      Caption         =   "Audio Fields"
      Height          =   375
      Left            =   21240
      TabIndex        =   106
      Top             =   15300
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CommandButton cmdFieldsAll 
      Caption         =   "All Fields"
      Height          =   315
      Left            =   14400
      TabIndex        =   105
      Top             =   2340
      Width           =   2175
   End
   Begin VB.CommandButton cmdFieldsVideo 
      Caption         =   "Bookings"
      Height          =   315
      Left            =   9960
      TabIndex        =   104
      Top             =   2340
      Width           =   2175
   End
   Begin VB.Frame fraReport 
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   7740
      TabIndex        =   99
      Top             =   2760
      Width           =   6615
      Begin VB.CommandButton cmdZipAndExport 
         Caption         =   "ZIP, Export and Send"
         Height          =   315
         Left            =   0
         TabIndex        =   376
         Top             =   0
         Width           =   1875
      End
      Begin VB.CommandButton cmdReport 
         Caption         =   "Report"
         Height          =   315
         Left            =   2220
         TabIndex        =   103
         Top             =   0
         Width           =   915
      End
      Begin VB.OptionButton optReportComments 
         Caption         =   "Last Comm't"
         Height          =   195
         Index           =   0
         Left            =   3240
         TabIndex        =   102
         Top             =   60
         Width           =   1215
      End
      Begin VB.OptionButton optReportComments 
         Caption         =   "1st Comm't"
         Height          =   195
         Index           =   1
         Left            =   4620
         TabIndex        =   101
         Top             =   60
         Width           =   1095
      End
      Begin VB.CommandButton cmdClrComments 
         Caption         =   "Clr"
         Height          =   315
         Left            =   5880
         TabIndex        =   100
         Top             =   0
         Width           =   735
      End
   End
   Begin VB.CommandButton cmdGenerateSFUniqueIDs 
      Caption         =   "GenerateSFUniqueIDs"
      Height          =   495
      Left            =   16740
      TabIndex        =   92
      Top             =   15600
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.PictureBox picValidationMasterFiles 
      Height          =   6795
      Left            =   2460
      ScaleHeight     =   6735
      ScaleWidth      =   4335
      TabIndex        =   50
      Top             =   15540
      Visible         =   0   'False
      Width           =   4395
      Begin VB.Frame Frame12 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   2340
         TabIndex        =   235
         Top             =   5640
         Width           =   975
         Begin VB.OptionButton optMasterValidationComplete 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   237
            Top             =   120
            Width           =   195
         End
         Begin VB.OptionButton optMasterValidationComplete 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   236
            Top             =   120
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.Frame Frame11 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   117
         Top             =   5400
         Width           =   1455
         Begin VB.OptionButton optTextlessSeparator 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   119
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optTextlessSeparator 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   118
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame10 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   113
         Top             =   5040
         Width           =   1455
         Begin VB.OptionButton optBlackAtEnd 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   115
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optBlackAtEnd 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   114
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame9 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   107
         Top             =   4680
         Width           =   1455
         Begin VB.OptionButton optTextlessPresent 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   109
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optTextlessPresent 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   108
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.CommandButton cmdCloseMasterValidation 
         Caption         =   "Close"
         Height          =   375
         Left            =   2460
         TabIndex        =   91
         Top             =   6180
         Width           =   1695
      End
      Begin VB.Frame Frame8 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   87
         Top             =   4320
         Width           =   1455
         Begin VB.OptionButton optSurroundPresent 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   89
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optSurroundPresent 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   88
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame7 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   82
         Top             =   3960
         Width           =   1455
         Begin VB.OptionButton optMEStereoPresent 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   84
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optMEStereoPresent 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   83
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame6 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   79
         Top             =   3600
         Width           =   1455
         Begin VB.OptionButton optMainStereoPresent 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   81
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optMainStereoPresent 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   80
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame5 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   75
         Top             =   3240
         Width           =   1455
         Begin VB.OptionButton optBarsAndToneInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   77
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optBarsAndToneInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   76
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame4 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   70
         Top             =   2880
         Width           =   1455
         Begin VB.OptionButton optTimecodeInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   72
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optTimecodeInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   71
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   66
         Top             =   2520
         Width           =   975
         Begin VB.OptionButton optClockSlateInfoCorrect 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   68
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optClockSlateInfoCorrect 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   67
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   62
         Top             =   2160
         Width           =   975
         Begin VB.OptionButton optHDFlag 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   64
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optHDFlag 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   63
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.ComboBox cmbFrameRate 
         Height          =   315
         Left            =   2400
         TabIndex        =   61
         ToolTipText     =   "The Clip Codec"
         Top             =   1740
         Width           =   1395
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   57
         Top             =   1440
         Width           =   975
         Begin VB.OptionButton optVideoInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   59
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optVideoInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   58
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.CommandButton cmdMasterValidationSave 
         Caption         =   "Save Validation Info"
         Height          =   375
         Left            =   180
         TabIndex        =   52
         Top             =   6180
         Width           =   2055
      End
      Begin VB.Label lblCaption 
         Caption         =   "30 sec black befor Textless ?"
         Height          =   255
         Index           =   28
         Left            =   240
         TabIndex        =   116
         Top             =   5400
         Width           =   2175
      End
      Begin VB.Label lblCaption 
         Caption         =   "1 sec Black At End ?"
         Height          =   255
         Index           =   27
         Left            =   240
         TabIndex        =   112
         Top             =   5040
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Textless Present ?"
         Height          =   255
         Index           =   26
         Left            =   240
         TabIndex        =   110
         Top             =   4680
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Title:"
         Height          =   255
         Index           =   25
         Left            =   240
         TabIndex        =   98
         Top             =   480
         Width           =   315
      End
      Begin VB.Label lblValidationEpisode 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   1200
         TabIndex        =   95
         Top             =   780
         Width           =   375
      End
      Begin VB.Label lblValidationSeries 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   480
         TabIndex        =   94
         Top             =   780
         Width           =   375
      End
      Begin VB.Label lblValidationTitle 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   720
         TabIndex        =   93
         Top             =   480
         Width           =   3555
      End
      Begin VB.Label lblCaption 
         Caption         =   "5.1 Present ?"
         Height          =   255
         Index           =   22
         Left            =   240
         TabIndex        =   90
         Top             =   4320
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "M&&E Stereo Present ?"
         Height          =   255
         Index           =   21
         Left            =   240
         TabIndex        =   86
         Top             =   3960
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Main Stereo Present ?"
         Height          =   255
         Index           =   20
         Left            =   240
         TabIndex        =   85
         Top             =   3600
         Width           =   1875
      End
      Begin VB.Label lblCaption 
         Caption         =   "Bars && Tone Correct ?"
         Height          =   255
         Index           =   19
         Left            =   240
         TabIndex        =   78
         Top             =   3240
         Width           =   1995
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "N/A"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   18
         Left            =   3480
         TabIndex        =   74
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Caption         =   "Timecode Correct ?"
         Height          =   255
         Index           =   17
         Left            =   240
         TabIndex        =   73
         Top             =   2880
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Clock / Slate Info Correct?"
         Height          =   255
         Index           =   16
         Left            =   240
         TabIndex        =   69
         Top             =   2520
         Width           =   2055
      End
      Begin VB.Label lblCaption 
         Caption         =   "HD ?"
         Height          =   255
         Index           =   15
         Left            =   240
         TabIndex        =   65
         Top             =   2160
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Frame Rate"
         Height          =   255
         Index           =   14
         Left            =   240
         TabIndex        =   60
         Top             =   1800
         Width           =   1275
      End
      Begin VB.Label lblCaption 
         Caption         =   "Video In Spec ?"
         Height          =   255
         Index           =   12
         Left            =   240
         TabIndex        =   56
         Top             =   1440
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Validation Complete?"
         Height          =   255
         Index           =   11
         Left            =   240
         TabIndex        =   55
         Top             =   5760
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "No"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   2940
         TabIndex        =   54
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "Yes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   2400
         TabIndex        =   53
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Master File Validation"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   315
         Left            =   0
         TabIndex        =   51
         Top             =   60
         Width           =   4275
      End
      Begin VB.Label lblCaption 
         Caption         =   "Ep."
         Height          =   255
         Index           =   24
         Left            =   900
         TabIndex        =   97
         Top             =   780
         Width           =   255
      End
      Begin VB.Label lblCaption 
         Caption         =   "Sr."
         Height          =   255
         Index           =   23
         Left            =   240
         TabIndex        =   96
         Top             =   780
         Width           =   315
      End
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Decision Tree"
      Height          =   255
      Index           =   16
      Left            =   8040
      TabIndex        =   49
      Tag             =   "NOCLEAR"
      Top             =   960
      Width           =   1395
   End
   Begin VB.ComboBox cmbRightsOwner 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1620
      TabIndex        =   42
      ToolTipText     =   "What Copy Type is the tape?"
      Top             =   1440
      Width           =   2595
   End
   Begin VB.ComboBox cmbProgramType 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1620
      TabIndex        =   41
      ToolTipText     =   "What Copy Type is the tape?"
      Top             =   1800
      Width           =   2595
   End
   Begin VB.ComboBox cmbFormat 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   5760
      TabIndex        =   40
      ToolTipText     =   "What Copy Type is the tape?"
      Top             =   720
      Width           =   1815
   End
   Begin VB.ComboBox cmbComponentType 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   5760
      TabIndex        =   39
      ToolTipText     =   "What Copy Type is the tape?"
      Top             =   1080
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Master Not Here"
      Height          =   255
      Index           =   14
      Left            =   9840
      TabIndex        =   34
      Tag             =   "NOCLEAR"
      Top             =   60
      Width           =   1575
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Sent XML without File"
      ForeColor       =   &H00C00000&
      Height          =   255
      Index           =   13
      Left            =   14160
      TabIndex        =   33
      Tag             =   "NOCLEAR"
      Top             =   1560
      Width           =   1995
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Sent without XML"
      ForeColor       =   &H00C00000&
      Height          =   255
      Index           =   12
      Left            =   14160
      TabIndex        =   32
      Tag             =   "NOCLEAR"
      Top             =   1260
      Width           =   1995
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "File Made"
      ForeColor       =   &H00C00000&
      Height          =   255
      Index           =   5
      Left            =   11760
      TabIndex        =   31
      Tag             =   "NOCLEAR"
      Top             =   60
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "XML Made"
      Height          =   255
      Index           =   8
      Left            =   14160
      TabIndex        =   30
      Tag             =   "NOCLEAR"
      Top             =   60
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Billed"
      Height          =   255
      Index           =   3
      Left            =   8040
      TabIndex        =   29
      Tag             =   "NOCLEAR"
      Top             =   1560
      Width           =   1395
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Pending"
      ForeColor       =   &H00C00000&
      Height          =   255
      Index           =   1
      Left            =   8040
      TabIndex        =   28
      Tag             =   "NOCLEAR"
      Top             =   360
      Width           =   1395
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Sent with XML"
      ForeColor       =   &H00C00000&
      Height          =   255
      Index           =   11
      Left            =   14160
      TabIndex        =   27
      Tag             =   "NOCLEAR"
      Top             =   960
      Width           =   1995
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "File Not Made"
      Height          =   255
      Index           =   7
      Left            =   11760
      TabIndex        =   26
      Tag             =   "NOCLEAR"
      Top             =   660
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "XML Not Made"
      Height          =   255
      Index           =   10
      Left            =   14160
      TabIndex        =   25
      Tag             =   "NOCLEAR"
      Top             =   660
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "XML Made but Not Sent"
      Height          =   255
      Index           =   9
      Left            =   14160
      TabIndex        =   24
      Tag             =   "NOCLEAR"
      Top             =   360
      Width           =   2055
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "File Made but not Sent"
      ForeColor       =   &H00C00000&
      Height          =   255
      Index           =   6
      Left            =   11760
      TabIndex        =   23
      Tag             =   "NOCLEAR"
      Top             =   360
      Width           =   1995
   End
   Begin VB.CheckBox chkHideDemo 
      Alignment       =   1  'Right Justify
      Caption         =   "Hide Demo"
      Height          =   255
      Left            =   4620
      TabIndex        =   20
      Tag             =   "NOCLEAR"
      Top             =   3360
      Value           =   1  'Checked
      Width           =   1215
   End
   Begin VB.TextBox txtReference 
      Height          =   315
      Left            =   1620
      TabIndex        =   18
      Top             =   0
      Width           =   2595
   End
   Begin VB.CheckBox chkLockDur 
      Alignment       =   1  'Right Justify
      Caption         =   "Lock Duration"
      Height          =   255
      Left            =   6540
      TabIndex        =   16
      Top             =   3360
      Width           =   1335
   End
   Begin VB.TextBox txtEpisode 
      Height          =   315
      Left            =   5760
      TabIndex        =   13
      Top             =   360
      Width           =   1815
   End
   Begin VB.TextBox txtSubtitle 
      Height          =   315
      Left            =   1620
      TabIndex        =   12
      Top             =   1080
      Width           =   2595
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Finished"
      Height          =   255
      Index           =   2
      Left            =   8040
      TabIndex        =   7
      Tag             =   "NOCLEAR"
      Top             =   1260
      Width           =   1395
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete"
      Height          =   255
      Index           =   0
      Left            =   8040
      TabIndex        =   6
      Tag             =   "NOCLEAR"
      Top             =   60
      Value           =   -1  'True
      Width           =   1395
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   4920
      TabIndex        =   1
      Top             =   14880
      Width           =   20775
      Begin VB.CommandButton cmdExportGrid 
         Caption         =   "Export Grid"
         Height          =   315
         Left            =   660
         TabIndex        =   375
         Top             =   0
         Width           =   1755
      End
      Begin VB.CommandButton cmdBillItem 
         Caption         =   "Bill Item"
         Height          =   315
         Left            =   10560
         TabIndex        =   48
         Top             =   0
         Width           =   1755
      End
      Begin VB.CommandButton cmdManualBillItem 
         Caption         =   "Manually Mark as Billed"
         Height          =   315
         Left            =   8160
         TabIndex        =   47
         Top             =   0
         Width           =   2235
      End
      Begin VB.CommandButton cmdUnbill 
         Caption         =   "Unmark as Billed"
         Height          =   315
         Left            =   6180
         TabIndex        =   46
         Top             =   0
         Width           =   1815
      End
      Begin VB.CommandButton cmdBillAll 
         Caption         =   "Bill All"
         Height          =   315
         Left            =   4320
         TabIndex        =   45
         Top             =   0
         Width           =   1695
      End
      Begin VB.CommandButton cmdUnbillAll 
         Caption         =   "Unmark Billing All"
         Height          =   315
         Left            =   2580
         TabIndex        =   44
         Top             =   0
         Width           =   1575
      End
      Begin VB.CommandButton cmdUpdateAll 
         Caption         =   "Update File Information"
         Height          =   315
         Left            =   12420
         TabIndex        =   22
         Top             =   0
         Width           =   2235
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   18480
         TabIndex        =   5
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   17220
         TabIndex        =   4
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   15960
         TabIndex        =   3
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   14700
         TabIndex        =   2
         Top             =   0
         Width           =   1215
      End
   End
   Begin MSAdodcLib.Adodc adoItems 
      Height          =   330
      Left            =   60
      Top             =   4020
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdItems 
      Bindings        =   "frmTrackerSvensk.frx":0036
      Height          =   8355
      Left            =   60
      TabIndex        =   0
      Top             =   4020
      Width           =   28530
      ScrollBars      =   3
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   3
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTrackerSvensk.frx":004D
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmTrackerSvensk.frx":0069
      stylesets(2).Name=   "headerfield"
      stylesets(2).ForeColor=   0
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmTrackerSvensk.frx":0085
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      StyleSet        =   "headerfield"
      RowHeight       =   450
      ExtraHeight     =   26
      Columns.Count   =   90
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "tracker_itemID"
      Columns(0).Name =   "tracker_svensk_itemID"
      Columns(0).DataField=   "tracker_svensk_itemID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "Filename"
      Columns(2).Name =   "itemfilename"
      Columns(2).DataField=   "itemfilename"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "GB Sent"
      Columns(3).Name =   "gbsent"
      Columns(3).DataField=   "gbsent"
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "headerfield"
      Columns(4).Width=   2461
      Columns(4).Caption=   "RRsat Client"
      Columns(4).Name =   "channel"
      Columns(4).DataField=   "Column 104"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   767
      Columns(5).Caption=   "Repl"
      Columns(5).Name =   "Resupply"
      Columns(5).DataField=   "Resupply"
      Columns(5).FieldLen=   256
      Columns(5).Style=   2
      Columns(5).StyleSet=   "headerfield"
      Columns(6).Width=   794
      Columns(6).Caption=   "Pend"
      Columns(6).Name =   "rejected"
      Columns(6).DataField=   "rejected"
      Columns(6).FieldLen=   256
      Columns(6).Style=   2
      Columns(6).StyleSet=   "headerfield"
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "Rejected Date"
      Columns(7).Name =   "rejecteddate"
      Columns(7).DataField=   "rejecteddate"
      Columns(7).FieldLen=   256
      Columns(8).Width=   2117
      Columns(8).Caption=   "Rej Date"
      Columns(8).Name =   "Rejected Date"
      Columns(8).DataField=   "RejectedDate"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(9).Width=   2117
      Columns(9).Caption=   "Off Rej Date"
      Columns(9).Name =   "offRejectedDate"
      Columns(9).DataField=   "OffRejectedDate"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(9).Locked=   -1  'True
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "Recent Rej Date"
      Columns(10).Name=   "MostRecentRejectedDate"
      Columns(10).DataField=   "MostRecentRejectedDate"
      Columns(10).DataType=   7
      Columns(10).FieldLen=   256
      Columns(10).Locked=   -1  'True
      Columns(11).Width=   1429
      Columns(11).Caption=   "Days Rej"
      Columns(11).Name=   "DaysonRejected"
      Columns(11).DataField=   "daysonRejected"
      Columns(11).DataType=   10
      Columns(11).FieldLen=   256
      Columns(12).Width=   1191
      Columns(12).Caption=   "Pend Int"
      Columns(12).Name=   "PendingInternal"
      Columns(12).DataField=   "PendingInternal"
      Columns(12).FieldLen=   256
      Columns(12).Style=   2
      Columns(12).HasBackColor=   -1  'True
      Columns(12).BackColor=   16776960
      Columns(12).StyleSet=   "headerfield"
      Columns(13).Width=   979
      Columns(13).Caption=   "D.Tree"
      Columns(13).Name=   "decisiontree"
      Columns(13).DataField=   "decisiontree"
      Columns(13).FieldLen=   256
      Columns(13).Style=   2
      Columns(13).StyleSet=   "headerfield"
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "DT Date"
      Columns(14).Name=   "decisiontreedate"
      Columns(14).DataField=   "decisiontreedate"
      Columns(14).FieldLen=   256
      Columns(14).Locked=   -1  'True
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "mostrecentdecisiontreedate"
      Columns(15).Name=   "mostrecentdecisiontreedate"
      Columns(15).DataField=   "mostrecentdecisiontreedate"
      Columns(15).FieldLen=   256
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "Off DT Date"
      Columns(16).Name=   "offdecisiontreedate"
      Columns(16).DataField=   "offdecisiontreedate"
      Columns(16).FieldLen=   256
      Columns(16).Locked=   -1  'True
      Columns(17).Width=   3200
      Columns(17).Visible=   0   'False
      Columns(17).Caption=   "Days DT"
      Columns(17).Name=   "daysondecisiontree"
      Columns(17).DataField=   "daysondecisiontree"
      Columns(17).FieldLen=   256
      Columns(18).Width=   3200
      Columns(18).Visible=   0   'False
      Columns(18).Caption=   "Fixed"
      Columns(18).Name=   "fixed"
      Columns(18).DataField=   "fixed"
      Columns(18).FieldLen=   256
      Columns(18).Style=   2
      Columns(18).StyleSet=   "headerfield"
      Columns(19).Width=   1349
      Columns(19).Caption=   "Fix Job #"
      Columns(19).Name=   "fixedjobID"
      Columns(19).DataField=   "fixedjobID"
      Columns(19).FieldLen=   256
      Columns(19).StyleSet=   "headerfield"
      Columns(20).Width=   3201
      Columns(20).Caption=   "SF UniqueID"
      Columns(20).Name=   "uniqueID"
      Columns(20).DataField=   "uniqueID"
      Columns(20).FieldLen=   256
      Columns(20).StyleSet=   "headerfield"
      Columns(21).Width=   2249
      Columns(21).Caption=   "Rights Owner"
      Columns(21).Name=   "RightsOwner"
      Columns(21).DataField=   "RightsOwner"
      Columns(21).FieldLen=   256
      Columns(21).StyleSet=   "headerfield"
      Columns(22).Width=   2249
      Columns(22).Caption=   "Project Manager"
      Columns(22).Name=   "ProjectManager"
      Columns(22).DataField=   "ProjectManager"
      Columns(22).FieldLen=   256
      Columns(22).Locked=   -1  'True
      Columns(22).StyleSet=   "headerfield"
      Columns(23).Width=   1111
      Columns(23).Caption=   "PM ID"
      Columns(23).Name=   "ProjectManagerContactID"
      Columns(23).DataField=   "ProjectManagerContactID"
      Columns(23).FieldLen=   256
      Columns(24).Width=   794
      Columns(24).Caption=   "Urg"
      Columns(24).Name=   "urgent"
      Columns(24).DataField=   "urgent"
      Columns(24).FieldLen=   256
      Columns(24).Style=   2
      Columns(24).StyleSet=   "headerfield"
      Columns(25).Width=   979
      Columns(25).Caption=   "iTunes"
      Columns(25).Name=   "iTunes"
      Columns(25).DataField=   "iTunes"
      Columns(25).FieldLen=   256
      Columns(25).Style=   2
      Columns(25).StyleSet=   "headerfield"
      Columns(26).Width=   1561
      Columns(26).Caption=   "Alpha Code"
      Columns(26).Name=   "AlphaDisplayCode"
      Columns(26).DataField=   "AlphaDisplayCode"
      Columns(26).DataType=   8
      Columns(26).FieldLen=   256
      Columns(26).StyleSet=   "headerfield"
      Columns(27).Width=   714
      Columns(27).Caption=   "R #"
      Columns(27).Name=   "RevisionNumber"
      Columns(27).DataField=   "RevisionNumber"
      Columns(27).FieldLen=   256
      Columns(27).StyleSet=   "headerfield"
      Columns(28).Width=   1561
      Columns(28).Caption=   "Kit ID"
      Columns(28).Name=   "KitID"
      Columns(28).DataField=   "KitID"
      Columns(28).FieldLen=   256
      Columns(28).StyleSet=   "headerfield"
      Columns(29).Width=   3175
      Columns(29).Caption=   "Title"
      Columns(29).Name=   "title"
      Columns(29).DataField=   "title"
      Columns(29).FieldLen=   256
      Columns(29).StyleSet=   "headerfield"
      Columns(30).Width=   794
      Columns(30).Caption=   "SR #"
      Columns(30).Name=   "Series"
      Columns(30).DataField=   "Series"
      Columns(30).FieldLen=   256
      Columns(30).StyleSet=   "headerfield"
      Columns(31).Width=   3175
      Columns(31).Caption=   "Episode Title"
      Columns(31).Name=   "subtitle"
      Columns(31).DataField=   "subtitle"
      Columns(31).FieldLen=   256
      Columns(31).StyleSet=   "headerfield"
      Columns(32).Width=   714
      Columns(32).Caption=   "Eps"
      Columns(32).Name=   "episode"
      Columns(32).DataField=   "episode"
      Columns(32).FieldLen=   256
      Columns(32).StyleSet=   "headerfield"
      Columns(33).Width=   3200
      Columns(33).Visible=   0   'False
      Columns(33).Caption=   "storagebarcode"
      Columns(33).Name=   "storagebarcode"
      Columns(33).DataField=   "storagebarcode"
      Columns(33).FieldLen=   256
      Columns(34).Width=   1482
      Columns(34).Caption=   "Program"
      Columns(34).Name=   "programtype"
      Columns(34).DataField=   "programtype"
      Columns(34).FieldLen=   256
      Columns(34).StyleSet=   "headerfield"
      Columns(35).Width=   2117
      Columns(35).Caption=   "Barcode"
      Columns(35).Name=   "barcode"
      Columns(35).DataField=   "barcode"
      Columns(35).DataType=   10
      Columns(35).FieldLen=   256
      Columns(35).StyleSet=   "headerfield"
      Columns(36).Width=   1402
      Columns(36).Caption=   "Format"
      Columns(36).Name=   "Format"
      Columns(36).DataField=   "Format"
      Columns(36).FieldLen=   256
      Columns(36).StyleSet=   "headerfield"
      Columns(37).Width=   2064
      Columns(37).Caption=   "Project Number"
      Columns(37).Name=   "projectnumber"
      Columns(37).DataField=   "projectnumber"
      Columns(37).FieldLen=   256
      Columns(37).StyleSet=   "headerfield"
      Columns(38).Width=   1535
      Columns(38).Caption=   "Component"
      Columns(38).Name=   "componenttype"
      Columns(38).DataField=   "componenttype"
      Columns(38).FieldLen=   256
      Columns(38).StyleSet=   "headerfield"
      Columns(39).Width=   1588
      Columns(39).Caption=   "Language"
      Columns(39).Name=   "language"
      Columns(39).DataField=   "language"
      Columns(39).FieldLen=   256
      Columns(40).Width=   1085
      Columns(40).Caption=   "Rq. Fr."
      Columns(40).Name=   "requestedframerate"
      Columns(40).DataField=   "requestedframerate"
      Columns(40).FieldLen=   256
      Columns(40).StyleSet=   "headerfield"
      Columns(41).Width=   1085
      Columns(41).Caption=   "Fr."
      Columns(41).Name=   "framerate"
      Columns(41).DataField=   "framerate"
      Columns(41).FieldLen=   256
      Columns(41).Locked=   -1  'True
      Columns(41).StyleSet=   "headerfield"
      Columns(42).Width=   3200
      Columns(42).Visible=   0   'False
      Columns(42).Caption=   "Proxy"
      Columns(42).Name=   "proxyreq"
      Columns(42).DataField=   "proxyreq"
      Columns(42).FieldLen=   256
      Columns(42).Style=   2
      Columns(42).StyleSet=   "headerfield"
      Columns(43).Width=   900
      Columns(43).Caption=   "Conf"
      Columns(43).Name=   "conform"
      Columns(43).DataField=   "conform"
      Columns(43).FieldLen=   256
      Columns(43).Style=   2
      Columns(43).StyleSet=   "headerfield"
      Columns(44).Width=   1058
      Columns(44).Caption=   "Txtless"
      Columns(44).Name=   "TextlessRequired"
      Columns(44).DataField=   "TextlessRequired"
      Columns(44).FieldLen=   256
      Columns(44).Style=   2
      Columns(44).StyleSet=   "headerfield"
      Columns(45).Width=   1058
      Columns(45).Caption=   "ME 2.0"
      Columns(45).Name=   "MERequired"
      Columns(45).DataField=   "MERequired"
      Columns(45).FieldLen=   256
      Columns(45).Style=   2
      Columns(45).StyleSet=   "headerfield"
      Columns(46).Width=   1058
      Columns(46).Caption=   "ME 5.1"
      Columns(46).Name=   "ME51"
      Columns(46).DataField=   "ME51Required"
      Columns(46).FieldLen=   256
      Columns(46).Style=   2
      Columns(47).Width=   2117
      Columns(47).Caption=   "Due Date"
      Columns(47).Name=   "duedate"
      Columns(47).DataField=   "duedate"
      Columns(47).FieldLen=   256
      Columns(47).Style=   1
      Columns(48).Width=   2117
      Columns(48).Caption=   "Completable"
      Columns(48).Name=   "completeable"
      Columns(48).DataField=   "completeable"
      Columns(48).FieldLen=   256
      Columns(49).Width=   2117
      Columns(49).Caption=   "Target Date"
      Columns(49).Name=   "targetdate"
      Columns(49).DataField=   "targetdate"
      Columns(49).FieldLen=   256
      Columns(50).Width=   2117
      Columns(50).Caption=   "Work On Today"
      Columns(50).Name=   "NewWorkOnToday"
      Columns(50).DataField=   "NewWorkOnToday"
      Columns(50).FieldLen=   256
      Columns(50).Style=   1
      Columns(50).HasForeColor=   -1  'True
      Columns(50).HasBackColor=   -1  'True
      Columns(50).ForeColor=   8388736
      Columns(50).BackColor=   16777215
      Columns(50).StyleSet=   "headerfield"
      Columns(51).Width=   2117
      Columns(51).Caption=   "1st Compl"
      Columns(51).Name=   "firstcompleteable"
      Columns(51).DataField=   "firstcompleteable"
      Columns(51).FieldLen=   256
      Columns(52).Width=   1429
      Columns(52).Caption=   "Days Rm"
      Columns(52).Name=   "targetdatecountdown"
      Columns(52).DataField=   "targetdatecountdown"
      Columns(52).FieldLen=   256
      Columns(52).StyleSet=   "headerfield"
      Columns(53).Width=   2011
      Columns(53).Caption=   "Special Project"
      Columns(53).Name=   "SpecialProject"
      Columns(53).DataField=   "SpecialProject"
      Columns(53).FieldLen=   256
      Columns(53).StyleSet=   "headerfield"
      Columns(54).Width=   1773
      Columns(54).Caption=   "Operator"
      Columns(54).Name=   "operator"
      Columns(54).DataField=   "operator"
      Columns(54).DataType=   8
      Columns(54).FieldLen=   256
      Columns(55).Width=   2117
      Columns(55).Caption=   "Master Here"
      Columns(55).Name=   "masterarrived"
      Columns(55).DataField=   "masterarrived"
      Columns(55).FieldLen=   256
      Columns(55).Style=   1
      Columns(55).StyleSet=   "stagefield"
      Columns(56).Width=   794
      Columns(56).Caption=   "Here"
      Columns(56).Name=   "assetshere"
      Columns(56).DataField=   "assetshere"
      Columns(56).FieldLen=   256
      Columns(56).Style=   2
      Columns(56).StyleSet=   "conclusionfield"
      Columns(57).Width=   2117
      Columns(57).Caption=   "Validation"
      Columns(57).Name=   "Validation"
      Columns(57).DataField=   "Validation"
      Columns(57).FieldLen=   256
      Columns(57).Style=   1
      Columns(57).StyleSet=   "stagefield"
      Columns(58).Width=   2117
      Columns(58).Caption=   "Encode"
      Columns(58).Name=   "filemade"
      Columns(58).DataField=   "filemade"
      Columns(58).FieldLen=   256
      Columns(58).Style=   1
      Columns(58).StyleSet=   "stagefield"
      Columns(59).Width=   873
      Columns(59).Caption=   "Clock"
      Columns(59).Name=   "DTAddClock"
      Columns(59).DataField=   "DTAddClock"
      Columns(59).FieldLen=   256
      Columns(59).Style=   2
      Columns(59).StyleSet=   "stagefield"
      Columns(60).Width=   873
      Columns(60).Caption=   "B&T"
      Columns(60).Name=   "DTBarsAndTone"
      Columns(60).DataField=   "DTBarsAndTone"
      Columns(60).FieldLen=   256
      Columns(60).Style=   2
      Columns(60).StyleSet=   "stagefield"
      Columns(61).Width=   873
      Columns(61).Caption=   "T/C"
      Columns(61).Name=   "DTTimecode"
      Columns(61).DataField=   "DTTimecode"
      Columns(61).FieldLen=   256
      Columns(61).Style=   2
      Columns(61).StyleSet=   "stagefield"
      Columns(62).Width=   873
      Columns(62).Caption=   "Black"
      Columns(62).Name=   "DTBlackAtEnd"
      Columns(62).DataField=   "DTBlackAtEnd"
      Columns(62).FieldLen=   256
      Columns(62).Style=   2
      Columns(62).StyleSet=   "stagefield"
      Columns(63).Width=   1244
      Columns(63).Caption=   "A Ch Fix"
      Columns(63).Name=   "DTAudioChannelConfig"
      Columns(63).DataField=   "DTAudioChannelConfig"
      Columns(63).FieldLen=   256
      Columns(63).Style=   2
      Columns(63).StyleSet=   "stagefield"
      Columns(64).Width=   794
      Columns(64).Caption=   "Tag"
      Columns(64).Name=   "DTQuicktimeTagging"
      Columns(64).DataField=   "DTQuicktimeTagging"
      Columns(64).FieldLen=   256
      Columns(64).Style=   2
      Columns(64).StyleSet=   "stagefield"
      Columns(65).Width=   979
      Columns(65).Caption=   "St Mix"
      Columns(65).Name=   "DTStereoDownmix"
      Columns(65).DataField=   "DTStereoDownmix"
      Columns(65).FieldLen=   256
      Columns(65).Style=   2
      Columns(65).StyleSet=   "stagefield"
      Columns(66).Width=   1508
      Columns(66).Caption=   "Rem Dates"
      Columns(66).Name=   "DTRemoveReleaseDates"
      Columns(66).DataField=   "DTRemoveReleaseDates"
      Columns(66).FieldLen=   256
      Columns(66).Style=   2
      Columns(66).StyleSet=   "stagefield"
      Columns(67).Width=   1667
      Columns(67).Caption=   "A F'rate.Cnv"
      Columns(67).Name=   "DTAudioFramerateConvert"
      Columns(67).DataField=   "DTAudioFramerateConvert"
      Columns(67).FieldLen=   256
      Columns(67).Style=   2
      Columns(67).StyleSet=   "stagefield"
      Columns(68).Width=   1244
      Columns(68).Caption=   "Sim Conf"
      Columns(68).Name=   "DTAudioSimpleConform"
      Columns(68).DataField=   "DTAudioSimpleConform"
      Columns(68).FieldLen=   256
      Columns(68).Style=   2
      Columns(68).StyleSet=   "stagefield"
      Columns(69).Width=   1217
      Columns(69).Caption=   ">5 Fixes"
      Columns(69).Name=   "MoreThan5Fixes"
      Columns(69).DataField=   "MoreThan5Fixes"
      Columns(69).FieldLen=   256
      Columns(69).Style=   2
      Columns(69).StyleSet=   "stagefield"
      Columns(70).Width=   2117
      Columns(70).Caption=   "File Sent"
      Columns(70).Name=   "filesent"
      Columns(70).DataField=   "filesent"
      Columns(70).FieldLen=   256
      Columns(70).Style=   1
      Columns(70).StyleSet=   "stagefield"
      Columns(71).Width=   3200
      Columns(71).Visible=   0   'False
      Columns(71).Caption=   "Proxy Made"
      Columns(71).Name=   "proxymade"
      Columns(71).DataField=   "proxymade"
      Columns(71).FieldLen=   256
      Columns(71).Style=   1
      Columns(71).StyleSet=   "stagefield"
      Columns(72).Width=   3200
      Columns(72).Visible=   0   'False
      Columns(72).Caption=   "Proxy Sent"
      Columns(72).Name=   "proxysent"
      Columns(72).DataField=   "proxysent"
      Columns(72).FieldLen=   256
      Columns(72).Style=   1
      Columns(72).StyleSet=   "stagefield"
      Columns(73).Width=   2117
      Columns(73).Caption=   "XML Made"
      Columns(73).Name=   "xmlmade"
      Columns(73).DataField=   "xmlmade"
      Columns(73).FieldLen=   256
      Columns(73).Style=   1
      Columns(73).StyleSet=   "stagefield"
      Columns(74).Width=   2117
      Columns(74).Caption=   "XML Sent"
      Columns(74).Name=   "xmlsent"
      Columns(74).DataField=   "xmlsent"
      Columns(74).FieldLen=   256
      Columns(74).Style=   1
      Columns(74).StyleSet=   "stagefield"
      Columns(75).Width=   2117
      Columns(75).Caption=   "Resend"
      Columns(75).Name=   "resend"
      Columns(75).DataField=   "resend"
      Columns(75).FieldLen=   256
      Columns(75).Style=   1
      Columns(75).StyleSet=   "stagefield"
      Columns(76).Width=   3200
      Columns(76).Visible=   0   'False
      Columns(76).Caption=   "Tape Out"
      Columns(76).Name=   "tapereturned"
      Columns(76).DataField=   "tapereturned"
      Columns(76).FieldLen=   256
      Columns(76).Style=   1
      Columns(76).StyleSet=   "stagefield"
      Columns(77).Width=   2117
      Columns(77).Caption=   "Complete"
      Columns(77).Name=   "complete"
      Columns(77).DataField=   "complete"
      Columns(77).FieldLen=   256
      Columns(77).Style=   1
      Columns(77).StyleSet=   "stagefield"
      Columns(78).Width=   767
      Columns(78).Caption=   "Done"
      Columns(78).Name=   "readytobill"
      Columns(78).DataField=   "readytobill"
      Columns(78).FieldLen=   256
      Columns(78).Locked=   -1  'True
      Columns(78).Style=   2
      Columns(78).StyleSet=   "conclusionfield"
      Columns(79).Width=   794
      Columns(79).Caption=   "Billed"
      Columns(79).Name=   "billed"
      Columns(79).DataField=   "billed"
      Columns(79).FieldLen=   256
      Columns(79).Style=   2
      Columns(79).StyleSet=   "conclusionfield"
      Columns(80).Width=   926
      Columns(80).Caption=   "Faulty"
      Columns(80).Name=   "complainedabout"
      Columns(80).DataField=   "complainedabout"
      Columns(80).FieldLen=   256
      Columns(80).Style=   2
      Columns(80).StyleSet=   "conclusionfield"
      Columns(81).Width=   1270
      Columns(81).Caption=   "Inv Job #"
      Columns(81).Name=   "jobID"
      Columns(81).DataField=   "jobID"
      Columns(81).FieldLen=   256
      Columns(81).StyleSet=   "headerfield"
      Columns(82).Width=   873
      Columns(82).Caption=   "Redo"
      Columns(82).Name=   "complaintredoitem"
      Columns(82).DataField=   "complaintredoitem"
      Columns(82).FieldLen=   256
      Columns(82).Style=   2
      Columns(82).StyleSet=   "conclusionfield"
      Columns(83).Width=   1826
      Columns(83).Caption=   "cdate"
      Columns(83).Name=   "cdate"
      Columns(83).DataField=   "cdate"
      Columns(83).FieldLen=   256
      Columns(84).Width=   1323
      Columns(84).Caption=   "mdate"
      Columns(84).Name=   "mdate"
      Columns(84).DataField=   "mdate"
      Columns(84).FieldLen=   256
      Columns(85).Width=   2699
      Columns(85).Caption=   "Ref"
      Columns(85).Name=   "itemreference"
      Columns(85).DataField=   "itemreference"
      Columns(85).FieldLen=   256
      Columns(85).StyleSet=   "headerfield"
      Columns(86).Width=   714
      Columns(86).Caption=   "Dur"
      Columns(86).Name=   "duration"
      Columns(86).DataField=   "duration"
      Columns(86).FieldLen=   256
      Columns(86).StyleSet=   "headerfield"
      Columns(87).Width=   1984
      Columns(87).Caption=   "Length"
      Columns(87).Name=   "timecodeduration"
      Columns(87).DataField=   "timecodeduration"
      Columns(87).FieldLen=   256
      Columns(87).StyleSet=   "headerfield"
      Columns(88).Width=   3200
      Columns(88).Visible=   0   'False
      Columns(88).Caption=   "hdflag"
      Columns(88).Name=   "hdflag"
      Columns(88).DataField=   "hdflag"
      Columns(88).FieldLen=   256
      Columns(88).Style=   2
      Columns(88).StyleSet=   "headerfield"
      Columns(89).Width=   3200
      Columns(89).Visible=   0   'False
      Columns(89).Caption=   "BreakoutProcessed"
      Columns(89).Name=   "BreakoutProcessed"
      Columns(89).DataField=   "BreakoutProcessed"
      Columns(89).FieldLen=   256
      Columns(89).Style=   2
      _ExtentX        =   50324
      _ExtentY        =   14737
      _StockProps     =   79
      Caption         =   "Tracker Items"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoComments 
      Height          =   330
      Left            =   26220
      Top             =   18420
      Visible         =   0   'False
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdComments 
      Bindings        =   "frmTrackerSvensk.frx":00A1
      Height          =   2175
      Left            =   60
      TabIndex        =   43
      Top             =   12480
      Width           =   17205
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   1
      stylesets(0).Name=   "comment"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTrackerSvensk.frx":00BB
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12582847
      RowHeight       =   476
      Columns.Count   =   18
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "trackerhistoryID"
      Columns(0).Name =   "tracker_svensk_commentID"
      Columns(0).DataField=   "tracker_svensk_commentID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "trackerprogramID"
      Columns(1).Name =   "tracker_svensk_itemID"
      Columns(1).DataField=   "tracker_svensk_itemID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   8149
      Columns(2).Caption=   "Comments - PLEASE FOLLOW THE NEW DECISION TREE INSTRUCTIONS"
      Columns(2).Name =   "comment"
      Columns(2).DataField=   "comment"
      Columns(2).FieldLen=   255
      Columns(2).HasForeColor=   -1  'True
      Columns(2).StyleSet=   "comment"
      Columns(3).Width=   5768
      Columns(3).Caption=   "File Location"
      Columns(3).Name =   "filelocation"
      Columns(3).DataField=   "filelocation"
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "comment"
      Columns(4).Width=   2117
      Columns(4).Caption=   "Date"
      Columns(4).Name =   "cdate"
      Columns(4).DataField=   "cdate"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(4).Style=   1
      Columns(4).HasForeColor=   -1  'True
      Columns(4).ForeColor=   8388608
      Columns(4).StyleSet=   "comment"
      Columns(5).Width=   2117
      Columns(5).Caption=   "Entered By"
      Columns(5).Name =   "cuser"
      Columns(5).DataField=   "cuser"
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(5).StyleSet=   "comment"
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "Decision Tree Comment?"
      Columns(6).Name =   "commenttype"
      Columns(6).DataField=   "commenttype"
      Columns(6).FieldLen=   256
      Columns(6).StyleSet=   "comment"
      Columns(7).Width=   2275
      Columns(7).Caption=   "Comment Type"
      Columns(7).Name =   "commenttypedecoded"
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "Clock?"
      Columns(8).Name =   "Email - Clock?"
      Columns(8).DataField=   "emailclock"
      Columns(8).FieldLen=   256
      Columns(8).Style=   2
      Columns(8).StyleSet=   "comment"
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "B&T?"
      Columns(9).Name =   "Email - B&T?"
      Columns(9).DataField=   "emailbarsandtone"
      Columns(9).FieldLen=   256
      Columns(9).Style=   2
      Columns(9).StyleSet=   "comment"
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "T/C?"
      Columns(10).Name=   "Email - T/C?"
      Columns(10).DataField=   "emailtimecode"
      Columns(10).FieldLen=   256
      Columns(10).Style=   2
      Columns(10).StyleSet=   "comment"
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "Black at End?"
      Columns(11).Name=   "Email - Black at End?"
      Columns(11).DataField=   "emailblackatend"
      Columns(11).FieldLen=   256
      Columns(11).Style=   2
      Columns(11).StyleSet=   "comment"
      Columns(12).Width=   3493
      Columns(12).Caption=   "Textless Not Rq but Present?"
      Columns(12).Name=   "Email - Textless Not Rq"
      Columns(12).DataField=   "emailtextlesspresent"
      Columns(12).FieldLen=   256
      Columns(12).Style=   2
      Columns(12).StyleSet=   "comment"
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "Black on White?"
      Columns(13).Name=   "emailblackonwhitetexstless"
      Columns(13).DataField=   "emailblackonwhitetexstless"
      Columns(13).FieldLen=   256
      Columns(13).Style=   2
      Columns(13).StyleSet=   "comment"
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "Track Layout Wrong?"
      Columns(14).Name=   "emailtracklayoutwrong"
      Columns(14).DataField=   "emailtracklayoutwrong"
      Columns(14).FieldLen=   256
      Columns(14).Style=   2
      Columns(14).StyleSet=   "comment"
      Columns(15).Width=   1429
      Columns(15).Caption=   "Resolved?"
      Columns(15).Name=   "issueresolved"
      Columns(15).DataField=   "issueresolved"
      Columns(15).FieldLen=   256
      Columns(15).Style=   2
      Columns(16).Width=   2117
      Columns(16).Caption=   "Resolved By"
      Columns(16).Name=   "resolvedby"
      Columns(16).DataField=   "resolvedby"
      Columns(16).FieldLen=   256
      Columns(17).Width=   1773
      Columns(17).Caption=   "Send Email?"
      Columns(17).Name=   "Email?"
      Columns(17).DataField=   "Column 5"
      Columns(17).DataType=   8
      Columns(17).FieldLen=   256
      Columns(17).Style=   4
      Columns(17).StyleSet=   "comment"
      UseDefaults     =   0   'False
      _ExtentX        =   30348
      _ExtentY        =   3836
      _StockProps     =   79
      Caption         =   "Tracker Comments - If a comment relates to the Decision Tree, please tick the box!!!"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   1620
      TabIndex        =   121
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The company this job is for"
      Top             =   3240
      Width           =   2595
      DataFieldList   =   "portalcompanyname"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "portalcompanyname"
      Columns(0).DataField=   "portalcompanyname"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   4577
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "portalcompanyname"
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   315
      Left            =   8640
      TabIndex        =   250
      Top             =   3180
      Visible         =   0   'False
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   556
      _Version        =   393216
      Appearance      =   1
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdInternalComments 
      Bindings        =   "frmTrackerSvensk.frx":00D7
      Height          =   2175
      Left            =   17400
      TabIndex        =   265
      Top             =   12480
      Width           =   11205
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   1
      stylesets(0).Name=   "comment"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTrackerSvensk.frx":00F9
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12582847
      RowHeight       =   476
      Columns.Count   =   14
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "trackerhistoryID"
      Columns(0).Name =   "tracker_svensk_commentID"
      Columns(0).DataField=   "tracker_svensk_commentID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "trackerprogramID"
      Columns(1).Name =   "tracker_svensk_itemID"
      Columns(1).DataField=   "tracker_svensk_itemID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   8361
      Columns(2).Caption=   "Comments "
      Columns(2).Name =   "comment"
      Columns(2).DataField=   "comment"
      Columns(2).FieldLen=   255
      Columns(2).HasForeColor=   -1  'True
      Columns(2).StyleSet=   "comment"
      Columns(3).Width=   5768
      Columns(3).Caption=   "File Location"
      Columns(3).Name =   "filelocation"
      Columns(3).DataField=   "filelocation"
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "comment"
      Columns(4).Width=   2117
      Columns(4).Caption=   "Date"
      Columns(4).Name =   "cdate"
      Columns(4).DataField=   "cdate"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(4).Style=   1
      Columns(4).HasForeColor=   -1  'True
      Columns(4).ForeColor=   8388608
      Columns(4).StyleSet=   "comment"
      Columns(5).Width=   2117
      Columns(5).Caption=   "Entered By"
      Columns(5).Name =   "cuser"
      Columns(5).DataField=   "cuser"
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(5).StyleSet=   "comment"
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "Clock?"
      Columns(6).Name =   "Email - Clock?"
      Columns(6).DataField=   "emailclock"
      Columns(6).FieldLen=   256
      Columns(6).Style=   2
      Columns(6).StyleSet=   "comment"
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "B&T?"
      Columns(7).Name =   "Email - B&T?"
      Columns(7).DataField=   "emailbarsandtone"
      Columns(7).FieldLen=   256
      Columns(7).Style=   2
      Columns(7).StyleSet=   "comment"
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "T/C?"
      Columns(8).Name =   "Email - T/C?"
      Columns(8).DataField=   "emailtimecode"
      Columns(8).FieldLen=   256
      Columns(8).Style=   2
      Columns(8).StyleSet=   "comment"
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "Black at End?"
      Columns(9).Name =   "Email - Black at End?"
      Columns(9).DataField=   "emailblackatend"
      Columns(9).FieldLen=   256
      Columns(9).Style=   2
      Columns(9).StyleSet=   "comment"
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "Black on White?"
      Columns(10).Name=   "emailblackonwhitetexstless"
      Columns(10).DataField=   "emailblackonwhitetexstless"
      Columns(10).FieldLen=   256
      Columns(10).Style=   2
      Columns(10).StyleSet=   "comment"
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "Track Layout Wrong?"
      Columns(11).Name=   "emailtracklayoutwrong"
      Columns(11).DataField=   "emailtracklayoutwrong"
      Columns(11).FieldLen=   256
      Columns(11).Style=   2
      Columns(11).StyleSet=   "comment"
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "internalcomment"
      Columns(12).Name=   "internalcomment"
      Columns(12).DataField=   "internalcomment"
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "commenttype"
      Columns(13).Name=   "commenttype"
      Columns(13).DataField=   "commenttype"
      Columns(13).FieldLen=   256
      UseDefaults     =   0   'False
      _ExtentX        =   19764
      _ExtentY        =   3836
      _StockProps     =   79
      Caption         =   "Internal Comments"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoInternalComments 
      Height          =   330
      Left            =   26160
      Top             =   19200
      Visible         =   0   'False
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker datUpMasterarrived 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   23280
      TabIndex        =   308
      Tag             =   "NOCHECK"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   60
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   164560897
      CurrentDate     =   39580
   End
   Begin MSComCtl2.DTPicker datUpvalidation 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   23280
      TabIndex        =   330
      Tag             =   "NOCHECK"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   420
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   164560897
      CurrentDate     =   39580
   End
   Begin MSComCtl2.DTPicker datUpfilemade 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   23280
      TabIndex        =   331
      Tag             =   "NOCHECK"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   780
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   164560897
      CurrentDate     =   39580
   End
   Begin MSComCtl2.DTPicker datUpfilesent 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   23280
      TabIndex        =   332
      Tag             =   "NOCHECK"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   1140
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   164560897
      CurrentDate     =   39580
   End
   Begin MSComCtl2.DTPicker datUpproxymade 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   23280
      TabIndex        =   333
      Tag             =   "NOCHECK"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   1500
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   164560897
      CurrentDate     =   39580
   End
   Begin MSComCtl2.DTPicker datUpproxysent 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   23280
      TabIndex        =   334
      Tag             =   "NOCHECK"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   1860
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   164560897
      CurrentDate     =   39580
   End
   Begin MSComCtl2.DTPicker datUpxmlmade 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   23280
      TabIndex        =   335
      Tag             =   "NOCHECK"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   2220
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   164560897
      CurrentDate     =   39580
   End
   Begin MSComCtl2.DTPicker datUpxmlsent 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   23280
      TabIndex        =   336
      Tag             =   "NOCHECK"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   2580
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   164560897
      CurrentDate     =   39580
   End
   Begin MSComCtl2.DTPicker datUpcomplete 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   23280
      TabIndex        =   337
      Tag             =   "NOCHECK"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   2940
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   164560897
      CurrentDate     =   39580
   End
   Begin MSComCtl2.DTPicker datUpDueDate 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   23280
      TabIndex        =   356
      Tag             =   "NOCHECK"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   3300
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   164560897
      CurrentDate     =   39580
   End
   Begin VB.Label lblCaption 
      Caption         =   "Alpha Code"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   78
      Left            =   120
      TabIndex        =   358
      Top             =   420
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Due Date"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   77
      Left            =   22080
      TabIndex        =   357
      Top             =   3300
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   74
      Left            =   19320
      TabIndex        =   329
      Top             =   60
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Complete"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   73
      Left            =   22080
      TabIndex        =   307
      Top             =   2940
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      BackColor       =   &H00C0C0FF&
      Caption         =   "XML Sent"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   66
      Left            =   22080
      TabIndex        =   305
      Top             =   2580
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      BackColor       =   &H00C0C0FF&
      Caption         =   "XML Made"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   65
      Left            =   22080
      TabIndex        =   303
      Top             =   2220
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Proxy Sent"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   64
      Left            =   22080
      TabIndex        =   301
      Top             =   1860
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Proxy Made"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   63
      Left            =   22080
      TabIndex        =   299
      Top             =   1500
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      BackColor       =   &H00C0C0FF&
      Caption         =   "File Sent"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   62
      Left            =   22080
      TabIndex        =   297
      Top             =   1140
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Encode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   60
      Left            =   22080
      TabIndex        =   295
      Top             =   780
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Validation"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   59
      Left            =   22080
      TabIndex        =   293
      Top             =   420
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Master Here"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   53
      Left            =   22080
      TabIndex        =   281
      Top             =   60
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Sorting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   52
      Left            =   4620
      TabIndex        =   273
      Top             =   3000
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Operator"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   33
      Left            =   120
      TabIndex        =   268
      Top             =   2940
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Inv. Job #"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   50
      Left            =   4680
      TabIndex        =   254
      Top             =   1500
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Special Project"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   32
      Left            =   120
      TabIndex        =   240
      Top             =   2580
      Width           =   1515
   End
   Begin VB.Label lblSearchCompanyID 
      Height          =   315
      Left            =   3240
      TabIndex        =   128
      Top             =   14820
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      Caption         =   "On"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   31
      Left            =   21180
      TabIndex        =   127
      Top             =   16320
      Width           =   255
   End
   Begin VB.Label lblLastValidationBy 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   18900
      TabIndex        =   126
      Top             =   16260
      Width           =   2175
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      Caption         =   "Last Validation By"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   30
      Left            =   16680
      TabIndex        =   125
      Top             =   16320
      Width           =   2055
   End
   Begin VB.Label lblLastValidationDate 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   21540
      TabIndex        =   124
      Top             =   16260
      Width           =   1815
   End
   Begin VB.Label lblCaption 
      Caption         =   "Project Manager"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   29
      Left            =   120
      TabIndex        =   123
      Top             =   2220
      Width           =   1515
   End
   Begin VB.Label lblCaption 
      Caption         =   "Comp. Type"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   7
      Left            =   4680
      TabIndex        =   38
      Top             =   1140
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Program Type"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   37
      Top             =   1860
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Format"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   4680
      TabIndex        =   36
      Top             =   780
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "SF Department"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   35
      Top             =   1500
      Width           =   1395
   End
   Begin VB.Label lblLastTrackerItemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   1260
      TabIndex        =   21
      Top             =   14820
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   10
      Left            =   120
      TabIndex        =   19
      Top             =   3300
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Reference"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   13
      Left            =   120
      TabIndex        =   17
      Top             =   60
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Series"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   4680
      TabIndex        =   15
      Top             =   60
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Episode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   4680
      TabIndex        =   14
      Top             =   420
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Subtitle"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   11
      Top             =   1140
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   10
      Top             =   780
      Width           =   1395
   End
   Begin VB.Label lblTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   180
      TabIndex        =   9
      Top             =   14820
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCompanyID 
      Height          =   315
      Left            =   2340
      TabIndex        =   8
      Top             =   14820
      Visible         =   0   'False
      Width           =   735
   End
End
Attribute VB_Name = "frmTrackerSvensk"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strSearch As String
Dim msp_strSearch As String
Dim m_strOrderby As String
Dim m_blnDelete As Boolean
Dim m_blnDontVerifyXML As Boolean
Dim m_blnBilling As Boolean
Dim m_blnSilent As Boolean
Dim l_strDateSearch As String
'Things for the Billing

Dim m_blnBillAll As Boolean
Dim m_strLastUniqueID As String
Dim m_strUniqueID As String
Dim m_strLastComponent As String
Dim m_datLastCompleteDate As Date
Dim m_lngAudioValidateCount As Long
Dim m_lngAudioConformCount As Long
Dim m_lngSubsValidateCount As Long
Dim m_lngSubsConformCount As Long
Dim m_strLastProjectManager As String, m_lngLastProjectNumber As Long, m_strLastSeries As String, m_strLastEpisodeNo As String, m_strLastEpisodeTitle As String, m_strLastTitle As String, m_strLastRightsOwner As String

Private Sub HideAllColumns()

grdItems.Columns("DTAddClock").Visible = False
grdItems.Columns("DTBarsAndTone").Visible = False
grdItems.Columns("DTTimecode").Visible = False
grdItems.Columns("DTBlackAtEnd").Visible = False
grdItems.Columns("DTAudioChannelConfig").Visible = False
grdItems.Columns("DTQuicktimeTagging").Visible = False
grdItems.Columns("DTStereoDownmix").Visible = False
grdItems.Columns("DTRemoveReleaseDates").Visible = False
grdItems.Columns("DTAudioFramerateConvert").Visible = False
grdItems.Columns("DTAudioSimpleConform").Visible = False
grdItems.Columns("channel").Visible = False
grdItems.Columns("resupply").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("KitID").Visible = False
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("offRejectedDate").Visible = False
grdItems.Columns("DaysonRejected").Visible = False
grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("fixedjobID").Visible = False
grdItems.Columns("uniqueID").Visible = False
grdItems.Columns("Rightsowner").Visible = False
grdItems.Columns("projectManager").Visible = False
grdItems.Columns("projectManagercontactID").Visible = False
grdItems.Columns("series").Visible = False
grdItems.Columns("subtitle").Visible = False
grdItems.Columns("programtype").Visible = False
grdItems.Columns("barcode").Visible = False
grdItems.Columns("format").Visible = False
grdItems.Columns("ProjectNumber").Visible = False
grdItems.Columns("ComponentType").Visible = False
grdItems.Columns("RequestedFramerate").Visible = False
grdItems.Columns("Framerate").Visible = False
grdItems.Columns("proxyreq").Visible = False
grdItems.Columns("SpecialProject").Visible = False
grdItems.Columns("masterarrived").Visible = False
grdItems.Columns("validation").Visible = False
grdItems.Columns("filemade").Visible = False
grdItems.Columns("filesent").Visible = False
grdItems.Columns("proxymade").Visible = False
grdItems.Columns("proxysent").Visible = False
grdItems.Columns("xmlmade").Visible = False
grdItems.Columns("xmlsent").Visible = False
grdItems.Columns("resend").Visible = False
grdItems.Columns("tapereturned").Visible = False
grdItems.Columns("complete").Visible = False
grdItems.Columns("cdate").Visible = False
grdItems.Columns("mdate").Visible = False
grdItems.Columns("targetdatecountdown").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("jobID").Visible = False
grdItems.Columns("operator").Visible = False
grdItems.Columns("NewWorkOnToday").Visible = False
grdItems.Columns("PendingInternal").Visible = False
grdItems.Columns("assetshere").Visible = False

End Sub

Private Sub chkHideDemo_Click()

Dim l_strSQL As String

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT portalcompanyname, companyID FROM company WHERE cetaclientcode like '%/Svensktracker%' and companyID > 100 AND system_active = 1 ORDER BY name;"
Else
    l_strSQL = "SELECT portalcompanyname, companyID FROM company WHERE cetaclientcode like '%/Svensktracker%' AND system_active = 1 ORDER BY name;"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset
Dim l_rstSearch2 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset
Set l_rstSearch2 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

With l_rstSearch2
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch2.ActiveConnection = Nothing

Set ddnChannel.DataSource = l_rstSearch2
grdItems.Columns("channel").DropDownHwnd = ddnChannel.hWnd

l_conSearch.Close
Set l_conSearch = Nothing

cmdSearch.Value = True

End Sub

Private Sub cmbCommentType_DropDown()
Dim l_strTemp As String

l_strTemp = cmbSorting.Text
cmbCommentType.Clear
cmbCommentType.AddItem ""
cmbCommentType.AddItem "Decision Tree"
cmbCommentType.AddItem "General"
cmbCommentType.AddItem "Pending"
cmbCommentType.Text = l_strTemp
End Sub

Private Sub cmbCompany_Click()

lblSearchCompanyID.Caption = cmbCompany.Columns("companyID").Text
m_strSearch = " WHERE companyID = " & lblSearchCompanyID.Caption

DoEvents

Dim l_rstChoices As ADODB.Recordset, l_blnWide As Boolean, l_strSQL As String

If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
    l_blnWide = True
Else
    l_blnWide = False
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch1 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch1 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

l_strSQL = "SELECT name, contactID FROM contact WHERE contactID IN (SELECT contactID FROM employee WHERE companyID = " & Val(lblSearchCompanyID.Caption) & ") ORDER BY name;"

With l_rstSearch1
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

Set ddnProjectManager.DataSource = l_rstSearch1

l_rstSearch1.ActiveConnection = Nothing

grdItems.Columns("projectmanager").DropDownHwnd = ddnProjectManager.hWnd
grdItems.Columns("projectmanager").Locked = False

l_conSearch.Close
Set l_conSearch = Nothing

cmdSearch.Value = True

End Sub

Private Sub cmbComponentType_DropDown()
Dim l_strTemp As String
If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strTemp = cmbComponentType.Text
    cmbComponentType.Clear
    PopulateCombo "svenskcomponenttype", cmbComponentType, "companyID = " & Val(lblSearchCompanyID.Caption) & " "
    cmbComponentType.Text = l_strTemp
End If
HighLite cmbComponentType
End Sub

Private Sub cmbFormat_DropDown()
Dim l_strTemp As String
If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strTemp = cmbFormat.Text
    cmbFormat.Clear
    PopulateCombo "svenskformat", cmbFormat, "companyID = " & Val(lblSearchCompanyID.Caption) & " "
    cmbFormat.Text = l_strTemp
End If
HighLite cmbFormat
End Sub

Private Sub cmbFrameRate_GotFocus()

PopulateCombo "framerate", cmbFrameRate, "MEDIA"
HighLite cmbFrameRate

End Sub

Private Sub cmbProgramType_DropDown()
Dim l_strTemp As String
If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strTemp = cmbProgramType.Text
    cmbProgramType.Clear
    PopulateCombo "svenskprogramtype", cmbProgramType, "companyID = " & Val(lblSearchCompanyID.Caption) & " "
    cmbProgramType.Text = l_strTemp
End If
HighLite cmbProgramType
End Sub

Private Sub cmbProjectManager_DropDown()

Dim l_strTemp As String
If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strTemp = cmbProjectManager.Text
    cmbProjectManager.Clear
    PopulateCombo "svenskprojectmanager", cmbProjectManager, "companyID = " & Val(lblSearchCompanyID.Caption) & " "
    cmbProjectManager.Text = l_strTemp
End If
HighLite cmbProjectManager

End Sub



Private Sub cmbSorting_Click()

cmdSearch.Value = True
HighLite cmbSorting

End Sub

Private Sub cmbSorting_DropDown()

Dim l_strTemp As String

l_strTemp = cmbSorting.Text
cmbSorting.Clear
cmbSorting.AddItem "Normal Sorting"
cmbSorting.AddItem "Validation"
cmbSorting.AddItem "Invoice Sorting"
cmbSorting.AddItem "Title"
cmbSorting.AddItem "Operator"
cmbSorting.AddItem "Series"
cmbSorting.AddItem "Episode"
cmbSorting.AddItem "Component"
cmbSorting.AddItem "Language"
cmbSorting.Text = l_strTemp

End Sub

Private Sub cmbTitle_DropDown()

Dim l_strTemp As String
If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strTemp = cmbTitle.Text
    cmbTitle.Clear
    PopulateCombo "svensktitle", cmbTitle, SearchSQLstr
    cmbTitle.Text = l_strTemp
End If
HighLite cmbTitle

End Sub

Private Sub cmbRightsOwner_DropDown()
Dim l_strTemp As String
If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strTemp = cmbRightsOwner.Text
    cmbRightsOwner.Clear
    PopulateCombo "svenskrightsowner", cmbRightsOwner, "companyID = " & Val(lblSearchCompanyID.Caption) & " "
    cmbRightsOwner.Text = l_strTemp
End If
HighLite cmbRightsOwner
End Sub

Private Sub cmdAudioValidationSave_Click()

Dim tempdate As Date, l_strSQL As String

If optAudioValidationComplete(0).Value = True Then
    tempdate = FormatDateTime(Now, vbLongDate)
    If grdItems.Columns("englishsepaudio").Text <> 0 Then
        grdItems.Columns("englishvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("norwegiansepaudio").Text <> 0 Then
        grdItems.Columns("norwegianvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("swedishsepaudio").Text <> 0 Then
        grdItems.Columns("swedishvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("danishsepaudio").Text <> 0 Then
        grdItems.Columns("danishvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("finnishsepaudio").Text <> 0 Then
        grdItems.Columns("finnishvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("icelandicsepaudio").Text <> 0 Then
        grdItems.Columns("icelandicvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
Else
    grdItems.Columns("englishvalidation").Text = ""
    grdItems.Columns("norwegianvalidation").Text = ""
    grdItems.Columns("swedishvalidation").Text = ""
    grdItems.Columns("danishvalidation").Text = ""
    grdItems.Columns("finnishvalidation").Text = ""
    grdItems.Columns("icelandicvalidation").Text = ""
End If

If optEnglishInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "englishinspec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optEnglishInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "englishinspec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "englishinspec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optNorwegianInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "NorwegianInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optNorwegianInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "NorwegianInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "NorwegianInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optSwedishInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "SwedishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optSwedishInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "SwedishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "SwedishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optDanishInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "DanishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optDanishInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "DanishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "DanishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optFinnishInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "FinnishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optFinnishInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "FinnishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "FinnishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optIcelandicInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "IcelandicInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optIcelandicInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "IcelandicInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "IcelandicInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

l_strSQL = "INSERT INTO tracker_svensk_validation (tracker_svensk_itemID, ValidationDate, ValidatedBy) VALUES ("
l_strSQL = l_strSQL & Val(lblTrackeritemID.Caption) & ", "
l_strSQL = l_strSQL & "getdate(), "
l_strSQL = l_strSQL & "'" & g_strFullUserName & "');"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

cmdCloseAudioValidation.Value = True

End Sub

Private Sub cmdBillAll_Click()

Dim Count As Long, Bookmark As Variant

optSortOrder(1).Value = True
cmdSearch.Value = True

If Not adoItems.Recordset.EOF And cmbCompany.Text <> "" Then

    adoItems.Recordset.MoveFirst
    Count = 0
    ProgressBar1.Max = adoItems.Recordset.RecordCount
    ProgressBar1.Value = 0
    ProgressBar1.Visible = True

    m_blnBillAll = True
    m_strLastUniqueID = ""
    m_strUniqueID = ""
    m_strLastComponent = ""
    m_strLastTitle = ""
    m_datLastCompleteDate = 0
    m_lngAudioValidateCount = 0
    m_lngAudioConformCount = 0
    m_lngSubsValidateCount = 0
    m_lngSubsConformCount = 0
    m_strLastUniqueID = Trim(" " & adoItems.Recordset("uniqueID"))
    
    Do While Not adoItems.Recordset.EOF
        ProgressBar1.Value = Count
        'Bookmark = adoItems.Recordset.Bookmark
        If adoItems.Recordset("readytobill") <> 0 And adoItems.Recordset("billed") = 0 And adoItems.Recordset("complaintredoitem") = 0 Then
            cmdBillItem.Value = True
            'adoItems.Refresh
            'adoItems.Recordset.Bookmark = Bookmark
            m_strLastUniqueID = Trim(" " & adoItems.Recordset("uniqueID"))
            m_strLastComponent = Trim(" " & adoItems.Recordset("Componenttype"))
            m_datLastCompleteDate = adoItems.Recordset("complete")
            m_strLastTitle = adoItems.Recordset("title")
            m_strLastProjectManager = Trim(" " & adoItems.Recordset("ProjectManager"))
            m_lngLastProjectNumber = Val(Trim(" " & adoItems.Recordset("ProjectNumber")))
            m_strLastSeries = Trim(" " & adoItems.Recordset("series"))
            m_strLastEpisodeNo = Trim(" " & adoItems.Recordset("Episode"))
            m_strLastEpisodeTitle = Trim(" " & adoItems.Recordset("subtitle"))
            m_strLastRightsOwner = Trim(" " & adoItems.Recordset("RightsOwner"))
        End If
        adoItems.Recordset.MoveNext
        DoEvents
        Count = Count + 1
    Loop
    ProgressBar1.Visible = False
    m_blnBillAll = False
    If m_lngAudioConformCount > 0 Then
        MakeJobDetailLine Val(frmJob.txtJobID.Text), "O", m_strLastTitle & " - Simple Audio Conform 1st File", 1, "DADCAUCONF", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
        If m_lngAudioConformCount > 1 Then
            MakeJobDetailLine Val(frmJob.txtJobID.Text), "O", m_strLastTitle & " - Simple Audio Conform 2+ Files", m_lngAudioConformCount - 1, "DADCAUCONF2+", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
        End If
        m_lngAudioConformCount = 0
    End If
    If m_lngAudioValidateCount > 0 Then
        MakeJobDetailLine Val(frmJob.txtJobID.Text), "O", m_strLastTitle & " - Audio Validate 1st File", 1, "DADCAUDFILECHECK", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
        If m_lngAudioValidateCount > 1 Then
            MakeJobDetailLine Val(frmJob.txtJobID.Text), "O", m_strLastTitle & " - Audio Validate 2+ files", m_lngAudioValidateCount - 1, "DADCAUDFILECHECK2+", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
        End If
        m_lngAudioValidateCount = 0
    End If
    If m_lngSubsConformCount > 0 Then
        MakeJobDetailLine Val(frmJob.txtJobID.Text), "O", m_strLastTitle & " - Simple Conform Subtitles 1st File", 1, "DADCSUBSCONF", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
        If m_lngSubsConformCount > 1 Then
            MakeJobDetailLine Val(frmJob.txtJobID.Text), "O", m_strLastTitle & " - Simple Conform Subtitles 2+ Files", m_lngSubsConformCount - 1, "DADCSUBSCONF2+", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
        End If
        m_lngSubsConformCount = 0
    End If
    If m_lngSubsValidateCount > 0 Then
        MakeJobDetailLine Val(frmJob.txtJobID.Text), "O", m_strLastTitle & " - Validate Subtitles 1st File", 1, "DADCSUBSCHECK", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
        If m_lngSubsValidateCount > 1 Then
            MakeJobDetailLine Val(frmJob.txtJobID.Text), "O", m_strLastTitle & " - Validate Subtitles Subtitle 2+ Files", m_lngSubsValidateCount - 1, "DADCSUBSCHECK2+", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
        End If
        m_lngSubsValidateCount = 0
    End If
    
    
Else

    MsgBox "Problem with billing - either no items or no company chosen.", vbCritical, "Error..."
    
End If

'm_strOrderby = " ORDER BY urgent, targetdate, duedate, uniqueID, title, componenttype DESC, complete;"
'cmdSearch.Value = True
'
End Sub

Private Sub cmdBillItem_Click()

Dim l_strChargeCode As String, l_rstTrackerChargeCode As ADODB.Recordset, l_strJobLine As String, l_lngJobID As Long, l_lngDuration As Long, l_lngMinuteBilling As Long
Dim l_lngQuantity As Long, l_strCode As String, l_lngFactor As Long, l_blnFirstItem As Boolean, l_blnBulkBilling As Boolean, l_lngLoggingItemCount As Long
Dim l_lngEventID As Long, l_strLastComment As String, l_strLastCommentCuser As String, l_datLastCommentDate As Date, l_blnError As Boolean, l_lngSubsQuantity As Long
Dim l_strSeries As String, l_strEpisode As String, l_strEpisodeTitle As String, l_lngProjectNumber As Long, l_strProjectManager As String
Dim l_lngFixCount As Long, l_strFixList As String

l_blnFirstItem = True
m_blnBilling = True
l_blnError = False
If frmJob.txtJobID.Text <> "" Then
    'A job is loaded - now check that it isn't locked.
    l_lngJobID = Val(frmJob.txtJobID.Text)
    'Check that a company has been chosen in the Drop down - we can only add to a job sheet of the correct company
    If cmbCompany.Text <> "" Then
        If frmJob.txtStatus.Text = "Confirmed" And frmJob.lblCompanyID.Caption = lblCompanyID.Caption Then
            'The Job is valid - go ahead and create Job lines for this item.
            m_blnDontVerifyXML = True
            l_lngQuantity = 0
            'l_strJobLine = grdItems.Columns("itemreference").Text
            l_strJobLine = grdItems.Columns("title").Text & " " & grdItems.Columns("programtype").Text
            l_strSeries = grdItems.Columns("series").Text
            l_strEpisode = grdItems.Columns("episode").Text
            l_strEpisodeTitle = grdItems.Columns("subtitle").Text
            l_lngProjectNumber = grdItems.Columns("ProjectNumber").Text
            l_strProjectManager = grdItems.Columns("ProjectManager").Text
            If grdItems.Columns("complete").Text <> "" Then
                If m_blnBillAll = False Then
                'DADC Svensk Tracker Billing single item
                'If it a tape encode - if so we charge for an Encode Process
                'If it is a file submission, we only charge for the Validation process.
                'If it is a fix job, we don't charge validation from the tracker.
                    If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/svenskbillonlylogging") <= 0 Then
                        If grdItems.Columns("barcode").Text <> "" Then
                            'We have a tape
                            If grdItems.Columns("duration").Text <> "" Then
                                l_strCode = "I"
                                l_lngLoggingItemCount = 0
                                l_lngDuration = Val(grdItems.Columns("duration").Text)
                                l_lngQuantity = 1
                                If adoComments.Recordset.RecordCount > 0 Then
                                    adoComments.Recordset.MoveLast
                                    l_strLastCommentCuser = adoComments.Recordset("cuser")
                                    l_datLastCommentDate = adoComments.Recordset("cdate")
                                    l_strLastComment = adoComments.Recordset("comment")
                                Else
                                    l_strLastCommentCuser = ""
                                    l_datLastCommentDate = 0
                                    l_strLastComment = ""
                                End If
                                If grdItems.Columns("format").Text = "HDCAM-SR" Then
                                    l_strChargeCode = "DADCINGESTHDSR"
                                ElseIf grdItems.Columns("format").Text = "HDCAM" Then
                                    l_strChargeCode = "DADCINGESTHD"
                                ElseIf grdItems.Columns("format").Text = "C-FORM" Then
                                    l_strChargeCode = "DADCINGESTCFORM"
                                ElseIf grdItems.Columns("Format").Text = "D3" Then
                                    l_strChargeCode = "DADCINGESTD3"
                                Else
                                    l_strChargeCode = "DADCINGESTSD"
                                End If
                                If l_strLastComment <> "" Then
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                Else
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                End If
                            Else
                                l_blnError = True
                                MsgBox "The current Item must have a duration in order to bill it.", vbCritical, "Cannot Bill Item"
                            End If
                        Else
                            'We have file(s)
                            If grdItems.Columns("componenttype").Text <> "" Then
                                If UCase(grdItems.Columns("componenttype").Text) = "VIDEO" Or UCase(grdItems.Columns("componenttype").Text) = "TRAILER" Then
                                    l_strCode = "O"
                                    l_lngQuantity = 1
                                    l_lngDuration = 0
                                    l_lngSubsQuantity = 0
                                    If Val(grdItems.Columns("duration").Text) < 30 Then
                                        If (UCase(grdItems.Columns("componenttype").Text) = "VIDEO" Or UCase(grdItems.Columns("componenttype").Text) = "TRAILER") And grdItems.Columns("validation").Text <> "" Then
                                            If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK-HD0", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                            Else
                                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK0", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                            End If
                                        End If
                                    Else
                                        If (UCase(grdItems.Columns("componenttype").Text) = "VIDEO" Or UCase(grdItems.Columns("componenttype").Text) = "TRAILER") And grdItems.Columns("validation").Text <> "" Then
                                            If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                            Else
                                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                            End If
                                        End If
                                    End If
                                ElseIf UCase(grdItems.Columns("componenttype").Text) = "AUDIO" Then
                                    If grdItems.Columns("DTAudioSimpleConform").Text = 0 Then
                                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Validate " & grdItems.Columns("language").Text, 1, "DADCAUDFILECHECKPOST", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                    Else
                                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Simple Audio Conform " & grdItems.Columns("language").Text, 1, "DADCAUCONF", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                    End If
                                ElseIf UCase(grdItems.Columns("componenttype").Text) = "SUBTITLE" Then
                                    If grdItems.Columns("DTAudioSimpleConform").Text = 0 Then
                                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Validate Subtitle File", 1, "DADCSUBSCHECK", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                        
                                    Else
                                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Simple Conform Subtitle File", 1, "DADCSUBSCONF", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                    End If
                                End If
                            Else
                                MsgBox "Unexpected Component type"
                                l_blnError = True
                            End If
                        End If
                    End If
                Else
                    'DADC Svensk Tracker Billing Bill-All in progress
                    If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/svenskbillonlylogging") <= 0 Then
                        If grdItems.Columns("barcode").Text <> "" Then
                            'We have a tape
                            If grdItems.Columns("duration").Text <> "" Then
                                l_strCode = "I"
                                l_lngLoggingItemCount = 0
                                l_lngDuration = Val(grdItems.Columns("duration").Text)
                                l_lngQuantity = 1
                                If adoComments.Recordset.RecordCount > 0 Then
                                    adoComments.Recordset.MoveLast
                                    l_strLastCommentCuser = Trim(" " & adoComments.Recordset("cuser"))
                                    l_datLastCommentDate = adoComments.Recordset("cdate")
                                    l_strLastComment = Trim(" " & adoComments.Recordset("comment"))
                                Else
                                    l_strLastCommentCuser = ""
                                    l_datLastCommentDate = 0
                                    l_strLastComment = ""
                                End If
                                If grdItems.Columns("format").Text = "HDCAM-SR" Then
                                    l_strChargeCode = "DADCINGESTHDSR"
                                ElseIf grdItems.Columns("format").Text = "HDCAM" Then
                                    l_strChargeCode = "DADCINGESTHD"
                                ElseIf grdItems.Columns("format").Text = "C-FORM" Then
                                    l_strChargeCode = "DADCINGESTCFORM"
                                ElseIf grdItems.Columns("Format").Text = "D3" Then
                                    l_strChargeCode = "DADCINGESTD3"
                                Else
                                    l_strChargeCode = "DADCINGESTSD"
                                End If
                                If l_strLastComment <> "" Then
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                Else
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                End If
                            Else
                                l_blnError = True
                                MsgBox "The current Item must have a duration in order to bill it.", vbCritical, "Cannot Bill Item"
                            End If
                        End If
                        'We have file(s)
                        If grdItems.Columns("componenttype").Text <> "" Then
                            If (UCase(m_strLastComponent) = "AUDIO" Or UCase(m_strLastComponent) = "SUBTITLE") And Not (UCase(m_strLastComponent) = UCase(grdItems.Columns("componenttype").Text) And grdItems.Columns("uniqueID").Text = m_strLastUniqueID And Format(grdItems.Columns("complete").Text, "YYYY/MM/DD") = Format(m_datLastCompleteDate, "YYYY/MM/DD")) Then
                                If m_lngAudioConformCount > 0 Then
                                    MakeJobDetailLine l_lngJobID, "O", m_strLastTitle & " - Simple Audio Conform 1st File", 1, "DADCAUCONF", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
                                    If m_lngAudioConformCount > 1 Then
                                        MakeJobDetailLine l_lngJobID, "O", m_strLastTitle & " - Simple Audio Conform 2+ Files", m_lngAudioConformCount - 1, "DADCAUCONF2+", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
                                    End If
                                    m_lngAudioConformCount = 0
                                End If
                                If m_lngAudioValidateCount > 0 Then
                                    MakeJobDetailLine l_lngJobID, "O", m_strLastTitle & " - Audio Validate 1st File", 1, "DADCAUDFILECHECK", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
                                    If m_lngAudioValidateCount > 1 Then
                                        MakeJobDetailLine l_lngJobID, "O", m_strLastTitle & " - Audio Validate 2+ files", m_lngAudioValidateCount - 1, "DADCAUDFILECHECK2+", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
                                    End If
                                    m_lngAudioValidateCount = 0
                                End If
                                If m_lngSubsConformCount > 0 Then
                                    MakeJobDetailLine l_lngJobID, "O", m_strLastTitle & " - Simple Conform Subtitles 1st File", 1, "DADCSUBSCONF", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
                                    If m_lngSubsConformCount > 1 Then
                                        MakeJobDetailLine l_lngJobID, "O", m_strLastTitle & " - Simple Conform Subtitles 2+ Files", m_lngSubsConformCount - 1, "DADCSUBSCONF2+", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
                                    End If
                                    m_lngSubsConformCount = 0
                                End If
                                If m_lngSubsValidateCount > 0 Then
                                    MakeJobDetailLine l_lngJobID, "O", m_strLastTitle & " - Validate Subtitles 1st File", 1, "DADCSUBSCHECK", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
                                    If m_lngSubsValidateCount > 1 Then
                                        MakeJobDetailLine l_lngJobID, "O", m_strLastTitle & " - Validate Subtitles Subtitle 2+ Files", m_lngSubsValidateCount - 1, "DADCSUBSCHECK2+", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, m_strLastProjectManager, m_lngLastProjectNumber, m_strLastSeries, m_strLastEpisodeNo, m_strLastEpisodeTitle, m_strLastRightsOwner, m_strLastTitle
                                    End If
                                    m_lngSubsValidateCount = 0
                                End If
                                m_strLastUniqueID = Trim(" " & adoItems.Recordset("uniqueID"))
                            End If
                            If (UCase(grdItems.Columns("componenttype").Text) = "VIDEO" Or UCase(grdItems.Columns("componenttype").Text) = "TRAILER") And grdItems.Columns("barcode").Text = "" Then
                                l_strCode = "O"
                                l_lngQuantity = 1
                                l_lngDuration = 0
                                l_lngSubsQuantity = 0
                                If Val(grdItems.Columns("duration").Text) < 30 Then
                                    If (UCase(grdItems.Columns("componenttype").Text) = "VIDEO" Or UCase(grdItems.Columns("componenttype").Text) = "TRAILER") And grdItems.Columns("validation").Text <> "" Then
                                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK-HD0", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                        Else
                                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK0", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                        End If
                                    End If
                                Else
                                    If (UCase(grdItems.Columns("componenttype").Text) = "VIDEO" Or UCase(grdItems.Columns("componenttype").Text) = "TRAILER") And grdItems.Columns("validation").Text <> "" Then
                                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                        Else
                                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                                        End If
                                    End If
                                End If
                            ElseIf UCase(grdItems.Columns("componenttype").Text) = "AUDIO" Then
                                If grdItems.Columns("uniqueID").Text = m_strLastUniqueID Then
                                    If grdItems.Columns("DTAudioSimpleConform").Text <> 0 Then
                                        m_lngAudioConformCount = m_lngAudioConformCount + 1
                                    ElseIf grdItems.Columns("barcode").Text = "" Then
                                        m_lngAudioValidateCount = m_lngAudioValidateCount + 1
                                    End If
                                End If
                            ElseIf UCase(grdItems.Columns("componenttype").Text) = "SUBTITLE" Then
                                If grdItems.Columns("uniqueID").Text = m_strLastUniqueID Then
                                    If grdItems.Columns("DTAudioSimpleConform").Text <> 0 Then
                                        m_lngSubsConformCount = m_lngSubsConformCount + 1
                                    Else
                                        m_lngSubsValidateCount = m_lngSubsValidateCount + 1
                                    End If
                                End If
                            End If
                        Else
                            MsgBox "Unexpected Component type"
                            l_blnError = True
                        End If
                    End If
                End If
                If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/svenskbillonlylogging") > 0 And grdItems.Columns("xmlmade").Text <> "" Then
                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING15-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                    Else
                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING15", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                    End If
                ElseIf grdItems.Columns("xmlmade").Text <> "" Then
                    l_lngLoggingItemCount = 0
                    l_lngEventID = Val(GetDataSQL("SELECT eventID FROM events WHERE clipreference = '" & QuoteSanitise(grdItems.Columns("Itemreference").Text) & "' AND libraryID <> 307744;"))
                    If l_lngEventID <> 0 Then
                        Set l_rstTrackerChargeCode = ExecuteSQL("SELECT Count(eventlogging.eventloggingID) FROM eventlogging WHERE eventID = " & l_lngEventID & ";", g_strExecuteError)
                        CheckForSQLError
                        If l_rstTrackerChargeCode.RecordCount >= 0 Then
                            l_lngLoggingItemCount = l_rstTrackerChargeCode(0)
                        End If
                        l_rstTrackerChargeCode.Close
                        Set l_rstTrackerChargeCode = Nothing
                    Else
                        MsgBox "problem counting logging items for this clip - assuming min 5 items", vbCritical, "Error Billing..."
                    End If
                    If l_lngLoggingItemCount > 15 Then
                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - " & grdItems.Columns("language").Text, 1, "DADCLOGGING25-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                        Else
                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - " & grdItems.Columns("language").Text, 1, "DADCLOGGING25", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                        End If
'                    ElseIf (l_lngLoggingItemCount <= 4 And UCase(grdItems.Columns("componenttype").Text) = "VIDEO") Or l_lngLoggingItemCount > 4 Then
                    ElseIf l_lngLoggingItemCount > 4 Then
                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - " & grdItems.Columns("language").Text, 1, "DADCLOGGING15-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                        Else
                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - " & grdItems.Columns("language").Text, 1, "DADCLOGGING15", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                        End If
                    Else
                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - " & grdItems.Columns("language").Text, 1, "DADCLOGGING04-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                        Else
                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - " & grdItems.Columns("language").Text, 1, "DADCLOGGING04", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                        End If
                    End If
                End If
                l_lngFixCount = 0
'                If grdItems.Columns("DTAddClock").Text <> 0 And grdItems.Columns("DTBarsAndTone").Text <> 0 And grdItems.Columns("DTTimecode").Text <> 0 Then
'                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Replace or Remove bars, Clock and Timecode", 1, "DADCTCBARS-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    Else
'                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Replace or Remove bars, Clock and Timecode", 1, "DADCTCBARS", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    End If
'                Else
'                    If grdItems.Columns("DTAddClock").Text <> 0 Then
'                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Replace or Remove Clock", 1, "DADCCLOCKHD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                        Else
'                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Replace or Remove Clock", 1, "DADCCLOCKSD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                        End If
'                    End If
'                    If grdItems.Columns("DTBarsAndTone").Text <> 0 Then
'                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Replace or Remove Bars and Tone", 1, "DADCBARSHD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                        Else
'                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Replace or Remove Bars and Tone", 1, "DADCBARSSD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                        End If
'                    End If
'                    If grdItems.Columns("DTTimecode").Text <> 0 Then
'                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Replace Timecode", 1, "DADCTC-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                        Else
'                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Replace Timecode", 1, "DADCTC", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                        End If
'                    End If
'                End If
                If grdItems.Columns("DTAddClock").Text <> 0 Then
                    l_lngFixCount = l_lngFixCount + 1
                    l_strFixList = "Removal of Clock"
                End If
                If grdItems.Columns("DTBarsAndTone").Text <> 0 Then
                    l_lngFixCount = l_lngFixCount + 1
                    If Len(l_strFixList) > 0 Then l_strFixList = l_strFixList & ", "
                    l_strFixList = l_strFixList & "Removal of Bars & Tone"
                End If
                If grdItems.Columns("DTTimecode").Text <> 0 Then
                    l_lngFixCount = l_lngFixCount + 1
                    If Len(l_strFixList) > 0 Then l_strFixList = l_strFixList & ", "
                    l_strFixList = l_strFixList & "Replacing Timecode"
                End If
                If grdItems.Columns("DTBlackAtEnd").Text <> 0 Then
'                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - Replace or Remove Black at End", 1, "C-EDIT-HD", 15, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    Else
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - Replace or Remove Black at End", 1, "C-EDIT", 15, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    End If
                    l_lngFixCount = l_lngFixCount + 1
                    If Len(l_strFixList) > 0 Then l_strFixList = l_strFixList & ", "
                    l_strFixList = l_strFixList & "Removing Black at End"
                End If
                If grdItems.Columns("DTAudioChannelConfig").Text <> 0 Then
'                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - Correcting audio channel configuration", 1, "PROTOOLS-HD", 15, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    Else
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - Correcting audio channel configuration", 1, "PROTOOLS", 15, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    End If
                    l_lngFixCount = l_lngFixCount + 1
                    If Len(l_strFixList) > 0 Then l_strFixList = l_strFixList & ", "
                    l_strFixList = l_strFixList & "Audio Channel Reconfiguration"
                End If
                If grdItems.Columns("DTQuicktimeTagging").Text <> 0 Then
'                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - Labelling audio channels in QuickTime", 1, "PROTOOLS-HD", 15, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    Else
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - Labelling audio channels in QuickTime", 1, "PROTOOLS", 15, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    End If
                    l_lngFixCount = l_lngFixCount + 1
                    If Len(l_strFixList) > 0 Then l_strFixList = l_strFixList & ", "
                    l_strFixList = l_strFixList & "Audio Track Tagging"
                End If
                If grdItems.Columns("DTStereoDownmix").Text <> 0 Then
'                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - 5.1 to 2.0 audio down mix", 1, "PROTOOLS-HD", 30, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    Else
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - 5.1 to 2.0 audio down mix", 1, "PROTOOLS", 30, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    End If
                    l_lngFixCount = l_lngFixCount + 1
                    If Len(l_strFixList) > 0 Then l_strFixList = l_strFixList & ", "
                    l_strFixList = l_strFixList & "Downmixing Stereo Audio"
                End If
                If grdItems.Columns("DTRemoveReleaseDates").Text <> 0 Then
'                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - Video edit for removing release dates, URL�s, MPAA cards", 1, "C-EDIT-HD", 15, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    Else
'                        MakeJobDetailLine l_lngJobID, "A", l_strJobLine & " - Video edit for removing release dates, URL�s, MPAA cards", 1, "C-EDIT", 15, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    End If
                    l_lngFixCount = l_lngFixCount + 1
                    If Len(l_strFixList) > 0 Then l_strFixList = l_strFixList & ", "
                    l_strFixList = l_strFixList & "Removal of Release Dates"
                End If
                If grdItems.Columns("DTAudioFramerateConvert").Text <> 0 Then
'                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
'                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Audio frame rate conversion", 1, "DADCAUDIOX", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    Else
'                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - Audio frame rate conversion", 1, "DADCAUDIOX", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
'                    End If
                    l_lngFixCount = l_lngFixCount + 1
                    If Len(l_strFixList) > 0 Then l_strFixList = l_strFixList & ", "
                    l_strFixList = l_strFixList & "Audio Framerate Conversion"
                End If
                If l_lngFixCount > 0 And l_lngFixCount < 3 Then
                    MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - 1-2 Fixes, " & l_strFixList, 1, "DADCFIXING1-2", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                ElseIf l_lngFixCount > 2 And l_lngFixCount < 6 Then
                    MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " - 1-2 Fixes, " & l_strFixList, 1, "DADCFIXING3-5", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text, grdItems.Columns("series").Text, grdItems.Columns("Episode").Text, grdItems.Columns("subtitle").Text, grdItems.Columns("rightsowner").Text, grdItems.Columns("title").Text
                ElseIf l_lngFixCount > 5 Then
                    MsgBox "More than 5 fixes performed - cannot bill line", vbCritical, "Error"
                    l_blnError = True
                End If
            End If
            frmJob.adoJobDetail.Refresh
            frmJob.adoJobDetail.Refresh
            If l_blnError = False Then
                grdItems.Columns("billed").Text = -1
                grdItems.Columns("jobID").Text = l_lngJobID
                grdItems.Update
                cmdBillItem.Visible = False
                cmdManualBillItem.Visible = False
            End If
            m_blnDontVerifyXML = False
        Else
            MsgBox "The current Job must belong to the correct company and be confirmed in order to bill an item.", vbCritical, "Cannot Bill Item"
        End If
    Else
        MsgBox "There must be a company chosen in order to do billing.", vbCritical, "Cannot Bill Item"
    End If
Else
    MsgBox "A Job must be created and loaded into the Jobs form in order to bill an item.", vbCritical, "Cannot Bill Item"
End If

m_blnBilling = False

End Sub

Private Sub cmdBreakoutLines_Click()

adoItems.Recordset.MoveFirst

Do While Not adoItems.Recordset.EOF

    grdItems.Columns("completeable").Text = grdItems.Columns("masterarrived").Text
    grdItems.Columns("firstcompleteable").Text = grdItems.Columns("masterarrived").Text
    grdItems.Columns("targetdate").Text = DateAdd("d", 3, CDate(grdItems.Columns("completeable").Text))
    grdItems.Columns("targetdatecountDown").Text = 0
    adoItems.Recordset.MoveNext
Loop

End Sub

Private Sub cmdCancelUpdate_Click()
fraUpdateComments.Visible = False
End Sub

Private Sub cmdClear_Click()

ClearFields Me
cmbCompany.Text = ""
lblCompanyID.Caption = ""
lblSearchCompanyID.Caption = ""
m_strSearch = " WHERE 1 = 1 "
cmdSearch.Value = True

grdItems.Columns("ProjectManager").DropDownHwnd = 0

End Sub

Private Sub cmdClearRadioButtons_Click()

optComplete(20).Value = False
optComplete(21).Value = False
optComplete(24).Value = False

cmdSearch.Value = True

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdCloseAudioValidation_Click()

picValidationAudioFiles.Visible = False

End Sub

Private Sub cmdCloseMasterValidation_Click()

picValidationMasterFiles.Visible = False

End Sub

Private Sub cmdCloseSubsValidation_Click()

picValidationSubs.Visible = False

End Sub

Private Sub cmdClrComments_Click()

optReportComments(0).Value = False
optReportComments(1).Value = False

End Sub

Private Sub cmdCommentsBulk_Click()
fraUpdateComments.Visible = True
End Sub

Private Sub cmdDuplicateGridForResupply_Click()

Dim l_lngNewTrackerID As Long, l_rsItems As ADODB.Recordset

If adoItems.Recordset.RecordCount <= 0 Then Exit Sub

Set l_rsItems = ExecuteSQL(adoItems.RecordSource, g_strExecuteError)
l_rsItems.MoveFirst
Do While Not l_rsItems.EOF
    l_lngNewTrackerID = CreateNewSvenskTrackerLine(l_rsItems("tracker_svensk_itemID"))
    If l_lngNewTrackerID <> 0 Then
        SetData "tracker_svensk_item", "resupply", "tracker_svensk_itemID", l_lngNewTrackerID, 1
        SetData "tracker_svensk_item", "complaintredoitem", "tracker_svensk_itemID", l_lngNewTrackerID, 0
        SetData "tracker_svensk_item", "CancelledForResupply", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
    End If
    l_rsItems.MoveNext
Loop

l_rsItems.Close
Set l_rsItems = Nothing

adoItems.Refresh
grdItems.Refresh
Me.Refresh

End Sub

Private Sub cmdDuplicateLineForResupply_Click()

Dim l_lngNewTrackerID As Long

If Val(lblTrackeritemID.Caption) <> 0 Then
    l_lngNewTrackerID = CreateNewSvenskTrackerLine(Val(lblTrackeritemID.Caption))
    If l_lngNewTrackerID <> 0 Then
        SetData "tracker_svensk_item", "resupply", "tracker_svensk_itemID", l_lngNewTrackerID, 1
        SetData "tracker_svensk_item", "complaintredoitem", "tracker_svensk_itemID", l_lngNewTrackerID, 0
        SetData "tracker_svensk_item", "CancelledForResupply", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
        adoItems.Refresh
        grdItems.Refresh
'        MsgBox ("Updated")
        Me.Refresh
    End If
End If

End Sub

Private Sub cmdExportGrid_Click()

If grdItems.Rows < 1 Then Exit Sub

Dim l_strFilePath As String
l_strFilePath = fGetSpecialFolder(CSIDL_DOCUMENTS)

grdItems.Export ssExportTypeText, ssExportAllRows + ssExportFieldNames, l_strFilePath & "Svensk_Ingest_Export.txt"
MsgBox "Exported to a text file in 'My Documents' called 'Svensk_Ingest_Export.txt'", vbInformation

End Sub

Private Sub cmdFieldsAll_Click()

grdItems.Columns("DTAddClock").Visible = True
grdItems.Columns("DTBarsAndTone").Visible = True
grdItems.Columns("DTTimecode").Visible = True
grdItems.Columns("DTBlackAtEnd").Visible = True
grdItems.Columns("DTAudioChannelConfig").Visible = True
grdItems.Columns("DTQuicktimeTagging").Visible = True
grdItems.Columns("DTStereoDownmix").Visible = True
grdItems.Columns("DTRemoveReleaseDates").Visible = True
grdItems.Columns("DTAudioFramerateConvert").Visible = True
grdItems.Columns("DTAudioSimpleConform").Visible = True
grdItems.Columns("channel").Visible = True
grdItems.Columns("resupply").Visible = True
grdItems.Columns("AlphaDisplayCode").Visible = True
grdItems.Columns("KitID").Visible = True
grdItems.Columns("Rejected Date").Visible = True
grdItems.Columns("offRejectedDate").Visible = True
grdItems.Columns("DaysonRejected").Visible = True
grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = True
grdItems.Columns("timecodeduration").Visible = True
grdItems.Columns("fixedjobID").Visible = True
grdItems.Columns("uniqueID").Visible = True
grdItems.Columns("Rightsowner").Visible = True
grdItems.Columns("projectManager").Visible = True
grdItems.Columns("projectManagercontactID").Visible = True
grdItems.Columns("series").Visible = True
grdItems.Columns("subtitle").Visible = True
grdItems.Columns("programtype").Visible = True
grdItems.Columns("barcode").Visible = True
grdItems.Columns("format").Visible = True
grdItems.Columns("ProjectNumber").Visible = True
grdItems.Columns("ComponentType").Visible = True
grdItems.Columns("RequestedFramerate").Visible = True
grdItems.Columns("Framerate").Visible = True
grdItems.Columns("proxyreq").Visible = True
grdItems.Columns("SpecialProject").Visible = True
grdItems.Columns("masterarrived").Visible = True
grdItems.Columns("validation").Visible = True
grdItems.Columns("filemade").Visible = True
grdItems.Columns("filesent").Visible = True
grdItems.Columns("proxymade").Visible = True
grdItems.Columns("proxysent").Visible = True
grdItems.Columns("xmlmade").Visible = True
grdItems.Columns("xmlsent").Visible = True
grdItems.Columns("resend").Visible = True
grdItems.Columns("tapereturned").Visible = False
grdItems.Columns("complete").Visible = True
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = True
grdItems.Columns("targetdatecountdown").Visible = True
grdItems.Columns("completeable").Visible = True
grdItems.Columns("firstcompleteable").Visible = True
grdItems.Columns("jobID").Visible = True
grdItems.Columns("language").Visible = True
grdItems.Columns("billed").Visible = True
grdItems.Columns("decisiontree").Visible = True
grdItems.Columns("urgent").Visible = True
grdItems.Columns("RevisionNumber").Visible = True
grdItems.Columns("TargetDate").Visible = True
grdItems.Columns("iTunes").Visible = True
grdItems.Columns("duedate").Visible = True
grdItems.Columns("complaintredoitem").Visible = True
grdItems.Columns("complainedabout").Visible = True
grdItems.Columns("readytobill").Visible = True
grdItems.Columns("conform").Visible = True
grdItems.Columns("assetshere").Visible = True
grdItems.Columns("urgent").Visible = True


End Sub

Private Sub cmdFieldsAssets_Click()

grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("offRejectedDate").Visible = False
grdItems.Columns("DaysonRejected").Visible = False
grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("fixedjobID").Visible = False
grdItems.Columns("uniqueID").Visible = False
grdItems.Columns("Rightsowner").Visible = False
grdItems.Columns("projectManager").Visible = False
grdItems.Columns("series").Visible = True
grdItems.Columns("subtitle").Visible = True
grdItems.Columns("programtype").Visible = True
grdItems.Columns("barcode").Visible = False
grdItems.Columns("format").Visible = False
grdItems.Columns("ProjectNumber").Visible = False
grdItems.Columns("ComponentType").Visible = False
grdItems.Columns("RequestedFramerate").Visible = False
grdItems.Columns("Framerate").Visible = False
grdItems.Columns("proxyreq").Visible = True
grdItems.Columns("englishsepaudio").Visible = False
grdItems.Columns("englishsubs").Visible = False
grdItems.Columns("norwegiansepaudio").Visible = False
grdItems.Columns("norwegiansubs").Visible = False
grdItems.Columns("swedishsepaudio").Visible = False
grdItems.Columns("swedishsubs").Visible = False
grdItems.Columns("danishsepaudio").Visible = False
grdItems.Columns("danishsubs").Visible = False
grdItems.Columns("finnishsepaudio").Visible = False
grdItems.Columns("finnishsubs").Visible = False
grdItems.Columns("icelandicsepaudio").Visible = False
grdItems.Columns("icelandicsubs").Visible = False
grdItems.Columns("masterarrived").Visible = True
grdItems.Columns("validation").Visible = False
grdItems.Columns("filemade").Visible = False
grdItems.Columns("filesent").Visible = False
grdItems.Columns("englisharrived").Visible = True
grdItems.Columns("englishsubsarrived").Visible = True
grdItems.Columns("englishvalidation").Visible = False
grdItems.Columns("englishsubsvalidation").Visible = False
grdItems.Columns("englishsent").Visible = False
grdItems.Columns("englishsubssent").Visible = False
grdItems.Columns("norwegianarrived").Visible = True
grdItems.Columns("norwegiansubsarrived").Visible = True
grdItems.Columns("norwegiansubsvalidation").Visible = False
grdItems.Columns("norwegianvalidation").Visible = False
grdItems.Columns("norwegiansent").Visible = False
grdItems.Columns("norwegiansubssent").Visible = False
grdItems.Columns("swedisharrived").Visible = True
grdItems.Columns("swedishsubsarrived").Visible = True
grdItems.Columns("swedishvalidation").Visible = False
grdItems.Columns("swedishsubsvalidation").Visible = False
grdItems.Columns("swedishsent").Visible = False
grdItems.Columns("swedishsubssent").Visible = False
grdItems.Columns("danisharrived").Visible = True
grdItems.Columns("danishsubsarrived").Visible = True
grdItems.Columns("danishvalidation").Visible = False
grdItems.Columns("danishsubsvalidation").Visible = False
grdItems.Columns("danishsent").Visible = False
grdItems.Columns("danishsubssent").Visible = False
grdItems.Columns("finnisharrived").Visible = True
grdItems.Columns("finnishsubsarrived").Visible = True
grdItems.Columns("finnishvalidation").Visible = False
grdItems.Columns("finnishsubsvalidation").Visible = False
grdItems.Columns("finnishsent").Visible = False
grdItems.Columns("finnishsubssent").Visible = False
grdItems.Columns("icelandicarrived").Visible = True
grdItems.Columns("icelandicsubsarrived").Visible = True
grdItems.Columns("icelandicvalidation").Visible = False
grdItems.Columns("icelandicsubsvalidation").Visible = False
grdItems.Columns("icelandicsent").Visible = False
grdItems.Columns("icelandicsubssent").Visible = False
grdItems.Columns("proxymade").Visible = False
grdItems.Columns("proxysent").Visible = False
grdItems.Columns("xmlmade").Visible = False
grdItems.Columns("xmlsent").Visible = False
grdItems.Columns("resend").Visible = False
grdItems.Columns("tapereturned").Visible = False
grdItems.Columns("complete").Visible = True
grdItems.Columns("cdate").Visible = False
grdItems.Columns("mdate").Visible = False

End Sub

Private Sub cmdFieldsAudio_Click()

grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("offRejectedDate").Visible = False
grdItems.Columns("DaysonRejected").Visible = False
grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("fixedjobID").Visible = False
grdItems.Columns("uniqueID").Visible = False
grdItems.Columns("Rightsowner").Visible = False
grdItems.Columns("projectManager").Visible = False
grdItems.Columns("series").Visible = False
grdItems.Columns("subtitle").Visible = False
grdItems.Columns("programtype").Visible = False
grdItems.Columns("barcode").Visible = False
grdItems.Columns("format").Visible = False
grdItems.Columns("ProjectNumber").Visible = False
grdItems.Columns("ComponentType").Visible = False
grdItems.Columns("RequestedFramerate").Visible = True
grdItems.Columns("Framerate").Visible = True
grdItems.Columns("proxyreq").Visible = False
grdItems.Columns("englishsepaudio").Visible = True
grdItems.Columns("englishsubs").Visible = True
grdItems.Columns("norwegiansepaudio").Visible = True
grdItems.Columns("norwegiansubs").Visible = True
grdItems.Columns("swedishsepaudio").Visible = True
grdItems.Columns("swedishsubs").Visible = True
grdItems.Columns("danishsepaudio").Visible = True
grdItems.Columns("danishsubs").Visible = True
grdItems.Columns("finnishsepaudio").Visible = True
grdItems.Columns("finnishsubs").Visible = True
grdItems.Columns("icelandicsepaudio").Visible = True
grdItems.Columns("icelandicsubs").Visible = True
grdItems.Columns("masterarrived").Visible = False
grdItems.Columns("validation").Visible = False
grdItems.Columns("filemade").Visible = False
grdItems.Columns("filesent").Visible = False
grdItems.Columns("englisharrived").Visible = True
grdItems.Columns("englishsubsarrived").Visible = True
grdItems.Columns("englishvalidation").Visible = True
grdItems.Columns("englishsubsvalidation").Visible = True
grdItems.Columns("englishsent").Visible = True
grdItems.Columns("englishsubssent").Visible = True
grdItems.Columns("norwegianarrived").Visible = True
grdItems.Columns("norwegiansubsarrived").Visible = True
grdItems.Columns("norwegiansubsvalidation").Visible = True
grdItems.Columns("norwegianvalidation").Visible = True
grdItems.Columns("norwegiansent").Visible = True
grdItems.Columns("norwegiansubssent").Visible = True
grdItems.Columns("swedisharrived").Visible = True
grdItems.Columns("swedishsubsarrived").Visible = True
grdItems.Columns("swedishvalidation").Visible = True
grdItems.Columns("swedishsubsvalidation").Visible = True
grdItems.Columns("swedishsent").Visible = True
grdItems.Columns("swedishsubssent").Visible = True
grdItems.Columns("danisharrived").Visible = True
grdItems.Columns("danishsubsarrived").Visible = True
grdItems.Columns("danishvalidation").Visible = True
grdItems.Columns("danishsubsvalidation").Visible = True
grdItems.Columns("danishsent").Visible = True
grdItems.Columns("danishsubssent").Visible = True
grdItems.Columns("finnisharrived").Visible = True
grdItems.Columns("finnishsubsarrived").Visible = True
grdItems.Columns("finnishvalidation").Visible = True
grdItems.Columns("finnishsubsvalidation").Visible = True
grdItems.Columns("finnishsent").Visible = True
grdItems.Columns("finnishsubssent").Visible = True
grdItems.Columns("icelandicarrived").Visible = True
grdItems.Columns("icelandicsubsarrived").Visible = True
grdItems.Columns("icelandicvalidation").Visible = True
grdItems.Columns("icelandicsubsvalidation").Visible = True
grdItems.Columns("icelandicsent").Visible = True
grdItems.Columns("icelandicsubssent").Visible = True
grdItems.Columns("proxymade").Visible = False
grdItems.Columns("proxysent").Visible = False
grdItems.Columns("xmlmade").Visible = False
grdItems.Columns("xmlsent").Visible = False
grdItems.Columns("resend").Visible = False
grdItems.Columns("tapereturned").Visible = False
grdItems.Columns("complete").Visible = True
grdItems.Columns("cdate").Visible = False
grdItems.Columns("mdate").Visible = False

End Sub

Private Sub cmdFieldsDecisionTree_Click() ' Operator

grdItems.Columns("DTAddClock").Visible = False
grdItems.Columns("DTBarsAndTone").Visible = False
grdItems.Columns("DTTimecode").Visible = False
grdItems.Columns("DTBlackAtEnd").Visible = False
grdItems.Columns("DTAudioChannelConfig").Visible = False
grdItems.Columns("DTQuicktimeTagging").Visible = False
grdItems.Columns("DTStereoDownmix").Visible = False
grdItems.Columns("DTRemoveReleaseDates").Visible = False
grdItems.Columns("DTAudioFramerateConvert").Visible = False
grdItems.Columns("DTAudioSimpleConform").Visible = False
grdItems.Columns("channel").Visible = False
grdItems.Columns("resupply").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = True
grdItems.Columns("KitID").Visible = True
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("offRejectedDate").Visible = False
grdItems.Columns("DaysonRejected").Visible = False
grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("fixedjobID").Visible = False
grdItems.Columns("uniqueID").Visible = False
grdItems.Columns("Rightsowner").Visible = False
grdItems.Columns("projectManager").Visible = False
grdItems.Columns("projectManagercontactID").Visible = False
grdItems.Columns("series").Visible = True
grdItems.Columns("subtitle").Visible = True
grdItems.Columns("programtype").Visible = True
grdItems.Columns("barcode").Visible = False
grdItems.Columns("format").Visible = False
grdItems.Columns("ProjectNumber").Visible = True
grdItems.Columns("ComponentType").Visible = True
grdItems.Columns("RequestedFramerate").Visible = True
grdItems.Columns("Framerate").Visible = True
grdItems.Columns("proxyreq").Visible = True
grdItems.Columns("SpecialProject").Visible = False
grdItems.Columns("masterarrived").Visible = True
grdItems.Columns("validation").Visible = True
grdItems.Columns("filemade").Visible = True
grdItems.Columns("filesent").Visible = True
grdItems.Columns("proxymade").Visible = True
grdItems.Columns("proxysent").Visible = True
grdItems.Columns("xmlmade").Visible = True
grdItems.Columns("xmlsent").Visible = True
grdItems.Columns("resend").Visible = False
grdItems.Columns("complete").Visible = True
grdItems.Columns("itemreference").Visible = True
grdItems.Columns("tapereturned").Visible = False
grdItems.Columns("cdate").Visible = False
grdItems.Columns("mdate").Visible = False
grdItems.Columns("targetdatecountdown").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("jobID").Visible = False
grdItems.Columns("operator").Visible = True
grdItems.Columns("NewWorkOnToday").Visible = True
grdItems.Columns("Title").Visible = True
grdItems.Columns("Assetshere").Visible = False
grdItems.Columns("series").Visible = True
grdItems.Columns("episode").Visible = True
grdItems.Columns("proxysent").Visible = True
grdItems.Columns("language").Visible = True
grdItems.Columns("billed").Visible = False
grdItems.Columns("decisiontree").Visible = False
grdItems.Columns("urgent").Visible = False
grdItems.Columns("RevisionNumber").Visible = True
grdItems.Columns("TargetDate").Visible = True
grdItems.Columns("iTunes").Visible = False
grdItems.Columns("duedate").Visible = False
grdItems.Columns("complaintredoitem").Visible = False
grdItems.Columns("complainedabout").Visible = False
grdItems.Columns("readytobill").Visible = False
grdItems.Columns("conform").Visible = True
grdItems.Columns("TextlessRequired").Visible = True
grdItems.Columns("MERequired").Visible = True
grdItems.Columns("ME51").Visible = True
grdItems.Columns("DTAddClock").Visible = True
grdItems.Columns("DTBarsAndTone").Visible = True
grdItems.Columns("DTTimecode").Visible = True
grdItems.Columns("DTBlackAtEnd").Visible = True
grdItems.Columns("DTAudioChannelConfig").Visible = True
grdItems.Columns("DTQuicktimeTagging").Visible = True
grdItems.Columns("DTStereoDownmix").Visible = True
grdItems.Columns("DTRemoveReleaseDates").Visible = True
grdItems.Columns("DTAudioFramerateConvert").Visible = True
grdItems.Columns("DTAudioSimpleConform").Visible = True
grdItems.Columns("assetshere").Visible = False
grdItems.Columns("urgent").Visible = True
grdItems.Columns("decisiontree").Visible = True

End Sub

Private Sub cmdFieldsSummary_Click()

grdItems.Columns("DTAddClock").Visible = False
grdItems.Columns("DTBarsAndTone").Visible = False
grdItems.Columns("DTTimecode").Visible = False
grdItems.Columns("DTBlackAtEnd").Visible = False
grdItems.Columns("DTAudioChannelConfig").Visible = False
grdItems.Columns("DTQuicktimeTagging").Visible = False
grdItems.Columns("DTStereoDownmix").Visible = False
grdItems.Columns("DTRemoveReleaseDates").Visible = False
grdItems.Columns("DTAudioFramerateConvert").Visible = False
grdItems.Columns("DTAudioSimpleConform").Visible = False
grdItems.Columns("channel").Visible = True
grdItems.Columns("resupply").Visible = True
grdItems.Columns("AlphaDisplayCode").Visible = True
grdItems.Columns("KitID").Visible = True
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("offRejectedDate").Visible = False
grdItems.Columns("DaysonRejected").Visible = False
grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("fixedjobID").Visible = True
grdItems.Columns("uniqueID").Visible = False
grdItems.Columns("Rightsowner").Visible = False
grdItems.Columns("projectManager").Visible = False
grdItems.Columns("projectManagercontactID").Visible = False
grdItems.Columns("series").Visible = True
grdItems.Columns("subtitle").Visible = True
grdItems.Columns("programtype").Visible = True
grdItems.Columns("barcode").Visible = False
grdItems.Columns("format").Visible = False
grdItems.Columns("ProjectNumber").Visible = False
grdItems.Columns("ComponentType").Visible = True
grdItems.Columns("RequestedFramerate").Visible = True
grdItems.Columns("Framerate").Visible = True
grdItems.Columns("proxyreq").Visible = True
grdItems.Columns("SpecialProject").Visible = True
grdItems.Columns("masterarrived").Visible = False
grdItems.Columns("validation").Visible = False
grdItems.Columns("filemade").Visible = False
grdItems.Columns("filesent").Visible = False
grdItems.Columns("proxymade").Visible = False
grdItems.Columns("proxysent").Visible = False
grdItems.Columns("xmlmade").Visible = False
grdItems.Columns("xmlsent").Visible = False
grdItems.Columns("resend").Visible = False
grdItems.Columns("tapereturned").Visible = False
grdItems.Columns("complete").Visible = True
grdItems.Columns("cdate").Visible = False
grdItems.Columns("mdate").Visible = False
grdItems.Columns("targetdatecountdown").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("jobID").Visible = False

grdItems.Columns("language").Visible = True
grdItems.Columns("billed").Visible = True
grdItems.Columns("decisiontree").Visible = True
grdItems.Columns("urgent").Visible = True
grdItems.Columns("RevisionNumber").Visible = True
grdItems.Columns("TargetDate").Visible = True
grdItems.Columns("iTunes").Visible = True
grdItems.Columns("duedate").Visible = True
grdItems.Columns("complaintredoitem").Visible = True
grdItems.Columns("complainedabout").Visible = True
grdItems.Columns("readytobill").Visible = True
grdItems.Columns("conform").Visible = True
grdItems.Columns("assetshere").Visible = True



End Sub

Private Sub cmdFieldsVideo_Click() ' Bookings

grdItems.Columns("DTAddClock").Visible = False
grdItems.Columns("DTBarsAndTone").Visible = False
grdItems.Columns("DTTimecode").Visible = False
grdItems.Columns("DTBlackAtEnd").Visible = False
grdItems.Columns("DTAudioChannelConfig").Visible = False
grdItems.Columns("DTQuicktimeTagging").Visible = False
grdItems.Columns("DTStereoDownmix").Visible = False
grdItems.Columns("DTRemoveReleaseDates").Visible = False
grdItems.Columns("DTAudioFramerateConvert").Visible = False
grdItems.Columns("DTAudioSimpleConform").Visible = False
grdItems.Columns("channel").Visible = False
grdItems.Columns("resupply").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = True
grdItems.Columns("KitID").Visible = True
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("offRejectedDate").Visible = False
grdItems.Columns("DaysonRejected").Visible = False
grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = True
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("fixedjobID").Visible = False
grdItems.Columns("uniqueID").Visible = False
grdItems.Columns("Rightsowner").Visible = False
grdItems.Columns("projectManager").Visible = False
grdItems.Columns("projectManagercontactID").Visible = False
grdItems.Columns("series").Visible = True
grdItems.Columns("subtitle").Visible = True
grdItems.Columns("programtype").Visible = True
grdItems.Columns("barcode").Visible = False
grdItems.Columns("format").Visible = False
grdItems.Columns("ProjectNumber").Visible = True
grdItems.Columns("ComponentType").Visible = True
grdItems.Columns("RequestedFramerate").Visible = True
grdItems.Columns("Framerate").Visible = True
grdItems.Columns("proxyreq").Visible = True
grdItems.Columns("SpecialProject").Visible = True
grdItems.Columns("masterarrived").Visible = True
grdItems.Columns("validation").Visible = True
grdItems.Columns("filemade").Visible = True
grdItems.Columns("filesent").Visible = True
grdItems.Columns("proxymade").Visible = True
grdItems.Columns("proxysent").Visible = True
grdItems.Columns("xmlmade").Visible = True
grdItems.Columns("xmlsent").Visible = True
grdItems.Columns("resend").Visible = False
grdItems.Columns("tapereturned").Visible = False
grdItems.Columns("complete").Visible = True
grdItems.Columns("cdate").Visible = False
grdItems.Columns("mdate").Visible = False
grdItems.Columns("targetdatecountdown").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("jobID").Visible = False
grdItems.Columns("assetshere").Visible = True
grdItems.Columns("language").Visible = True
grdItems.Columns("urgent").Visible = True
grdItems.Columns("decisiontree").Visible = True

End Sub

Private Sub cmdGenerateSFUniqueIDs_Click()

Dim l_strSFUniqueID As String

adoItems.Recordset.MoveFirst

Do While Not adoItems.Recordset.EOF
    If Trim(" " & adoItems.Recordset("projectnumber")) <> "" Then
        adoItems.Recordset("uniqueID") = adoItems.Recordset("projectnumber") & "_" & Replace(adoItems.Recordset("title"), " ", "_") & "_" & adoItems.Recordset("programtype") & "_" & Format(Val(Trim(" " & adoItems.Recordset("series"))), "00") & "_" & Format(Val(Trim(" " & adoItems.Recordset("episode"))), "00")
        adoItems.Recordset.Update
    End If
    adoItems.Recordset.MoveNext
Loop

End Sub

Private Sub cmdMakeFixJobsheet_Click()

Dim l_strSQL As String, l_lngJobID As Long

If Val(lblTrackeritemID.Caption) <> 0 Then

    l_strSQL = "INSERT INTO job (createduserID, createduser, createddate, modifieddate, modifieduserID, modifieduser, fd_status, jobtype, joballocation, deadlinedate, despatchdate, projectID, productID, companyID, companyname) VALUES ("
    
    l_strSQL = l_strSQL & g_lngUserID & ", "
    l_strSQL = l_strSQL & "'" & g_strUserName & "', "
    l_strSQL = l_strSQL & "getdate(), "
    l_strSQL = l_strSQL & "getdate(), "
    l_strSQL = l_strSQL & g_lngUserID & ","
    l_strSQL = l_strSQL & "'" & g_strUserName & "', "
    l_strSQL = l_strSQL & "'Confirmed', "
    l_strSQL = l_strSQL & "'Dubbing', "
    l_strSQL = l_strSQL & "'Regular', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(DateAdd("d", 2, Now)) & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(DateAdd("d", 2, Now)) & "', "
    l_strSQL = l_strSQL & "0, "
    l_strSQL = l_strSQL & "0, "
    l_strSQL = l_strSQL & "1330, "
    l_strSQL = l_strSQL & "'Sony DADC Ltd (Svensk Fixes)'); "
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_lngJobID = g_lngLastID
    
    l_strSQL = grdItems.Columns("title").Text & " - " & grdItems.Columns("programtype").Text
    SetData "job", "title1", "jobID", l_lngJobID, l_strSQL
    l_strSQL = grdItems.Columns("language").Text
    SetData "job", "title2", "jobID", l_lngJobID, l_strSQL
    l_strSQL = grdItems.Columns("projectnumber").Text & " - " & grdItems.Columns("projectmanager").Text
    SetData "job", "orderreference", "jobID", l_lngJobID, l_strSQL
    SetData "job", "contactID", "jobID", l_lngJobID, 3356
    SetData "job", "contactname", "jobID", l_lngJobID, "Hilary Weeks"
    SetData "job", "DADCProjectNumber", "jobID", l_lngJobID, grdItems.Columns("projectnumber").Text
    SetData "job", "DADCProjectManager", "jobID", l_lngJobID, grdItems.Columns("projectmanager").Text
    SetData "job", "DADCSeries", "jobID", l_lngJobID, grdItems.Columns("Series").Text
    SetData "job", "DADCEpisodetitle", "jobID", l_lngJobID, grdItems.Columns("subtitle").Text
    SetData "job", "DADCEpisodeNumber", "jobID", l_lngJobID, grdItems.Columns("episode").Text
    SetData "job", "DADCRightsOwner", "jobID", l_lngJobID, grdItems.Columns("RightsOwner").Text
    
    grdItems.Columns("fixedjobID").Text = l_lngJobID
    grdItems.Update
    
    ShowJob l_lngJobID, 1, True
'    ShowJob l_lngJobID, 1, True

End If

End Sub

Private Sub cmdManualBillItem_Click()

grdItems.Columns("billed").Text = -1
grdItems.Columns("jobid").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False

End Sub

Private Sub cmdMasterValidationSave_Click()

Dim tempdate As Date, l_strSQL As String

If optMasterValidationComplete(0).Value = True Then
    tempdate = FormatDateTime(Now, vbLongDate)
    grdItems.Columns("Validation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
Else
    grdItems.Columns("Validation").Text = ""
End If

If optVideoInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "videoinspec", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "videoinspec", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

SetData "tracker_svensk_item", "framerate", "tracker_svensk_itemID", Val(lblTrackeritemID), cmbFrameRate.Text

If optHDFlag(0).Value = True Then
    SetData "tracker_svensk_item", "hdflag", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "hdflag", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

If optTimecodeInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "TimecodeCorrect", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "TimecodeCorrect", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

If optBarsAndToneInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "BarsAndToneCorrect", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "BarsAndToneCorrect", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

If optMainStereoPresent(0).Value = True Then
    SetData "tracker_svensk_item", "MainStereoPresent", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "MainStereoPresent", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

If optMEStereoPresent(0).Value = True Then
    SetData "tracker_svensk_item", "MEStereoPresent", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "MEStereoPresent", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

If optSurroundPresent(0).Value = True Then
    SetData "tracker_svensk_item", "SurroundPresent", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "SurroundPresent", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

If optTextlessPresent(0).Value = True Then
    SetData "tracker_svensk_item", "TextlessPresent", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "TextlessPresent", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

If optBlackAtEnd(0).Value = True Then
    SetData "tracker_svensk_item", "BlackAtEnd", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "BlackAtEnd", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

If optTextlessSeparator(0).Value = True Then
    SetData "tracker_svensk_item", "TextlessSeparator", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "TextlessSeparator", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

l_strSQL = "INSERT INTO tracker_svensk_validation (tracker_svensk_itemID, ValidationDate, ValidatedBy) VALUES ("
l_strSQL = l_strSQL & Val(lblTrackeritemID.Caption) & ", "
l_strSQL = l_strSQL & "getdate(), "
l_strSQL = l_strSQL & "'" & g_strFullUserName & "');"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

cmdCloseMasterValidation.Value = True

End Sub

Private Sub cmdPendingOff_Click()
    
    If MsgBox("You are about to bulk update selected records to Off Pending." & vbCrLf & "Are you sure?", vbYesNo, "Confirm") = vbNo Then Exit Sub

End Sub

Private Sub cmdPendingOn_Click()

    If MsgBox("You are about to bulk update selected records to On Pending." & vbCrLf & "Are you sure?", vbYesNo, "Confirm") = vbNo Then Exit Sub

End Sub

Private Sub cmdPrint_Click()

Dim l_strSQL As String

If lblCompanyID.Caption <> "" Then
    l_strSQL = adoItems.RecordSource
    PrintCrystalReportUsingCleanSQL g_strLocationOfCrystalReportFiles & "GenericTrackerReport.rpt", l_strSQL, True
End If

End Sub

Private Sub cmdReport_Click()

Dim l_strSelectionFormula As String
Dim l_strReportFile As String
Dim l_strDateSearch As String

If lblSearchCompanyID.Caption = "" Then
    MsgBox "Please Select a Company Before running the report", vbCritical, "Error..."
    Exit Sub
End If

If optComplete(0).Value = True Then 'not complete
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) "
ElseIf optComplete(1).Value = True Then 'Pending
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) or {tracker_svensk_item.readytobill} = 0) and {tracker_svensk_item.rejected} <> 0"
ElseIf optComplete(2).Value = True Then 'Complete
    l_strSelectionFormula = "{tracker_svensk_item.readytobill} <> 0 and {tracker_svensk_item.billed} = 0"
ElseIf optComplete(3).Value = True Then 'Billed
    l_strSelectionFormula = "{tracker_svensk_item.readytobill} <> 0 and {tracker_svensk_item.billed} <> 0"
ElseIf optComplete(4).Value = True Then 'All Items
    l_strSelectionFormula = "1 = 1"
ElseIf optComplete(5).Value = True Then 'File Made
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.filemade} <> 0"
ElseIf optComplete(6).Value = True Then 'File made but not sent
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.filemade} <> 0 and (isnull({tracker_svensk_item.filesent}) OR {tracker_svensk_item.filesent} = 0)"
ElseIf optComplete(7).Value = True Then 'File Not Made
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND (isnull({tracker_svensk_item.filemade}) Or {tracker_svensk_item.filemade} = 0)"
ElseIf optComplete(8).Value = True Then 'xml Made
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.xmlmade} <> 0"
ElseIf optComplete(9).Value = True Then 'xml Made but not sent
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.xmlmade} <> 0 and (isnull({tracker_svensk_item.xmlsent}) OR {tracker_svensk_item.xmlsent} = 0)"
ElseIf optComplete(10).Value = True Then 'xml not made
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND (isnull({tracker_svensk_item.xmlmade}) or {tracker_svensk_item.xmlmade} = 0)"
ElseIf optComplete(11).Value = True Then 'Sent with xml
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.filesent} <> 0 AND {tracker_svensk_item.xmlsent} <> 0"
ElseIf optComplete(12).Value = True Then 'Sent without xml
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.filesent} <> 0 AND (isnull({tracker_svensk_item.xmlsent}) or {tracker_svensk_item.xmlsent} = 0)"
ElseIf optComplete(13).Value = True Then 'xml sent without File
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.xmlsent} <> 0 AND (isnull({tracker_svensk_item.filesent}) or {tracker_svensk_item.filesent} = 0)"
ElseIf optComplete(14).Value = True Then 'Master Not Here
    l_strSelectionFormula = "(isnull({tracker_svensk_item.masterarrived}) or (not isnull({tracker_svensk_item.masterarrived}) AND (not isnull({tracker_svensk_item.tapereturned})))"
ElseIf optComplete(15).Value = True Then 'Internal Pending
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) or {tracker_svensk_item.readytobill} = 0) and {tracker_svensk_item.pendinginternal} <> 0"
ElseIf optComplete(16).Value = True Then 'Decision Tree
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.decisiontree} <> 0"
ElseIf optComplete(17).Value = True Then 'Workable
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.rejected} = 0 AND {tracker_svensk_item.assetshere} <> 0 "
ElseIf optComplete(18).Value = True Then 'Not workable
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND ({tracker_svensk_item.rejected} <> 0 OR {tracker_svensk_item.assetshere} = 0) "
ElseIf optComplete(19).Value = True Then 'Urgent
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND ({tracker_svensk_item.urgent} <> 0) "
ElseIf optComplete(22).Value = True Then 'File Sent N/C
     l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND ({tracker_svensk_item.urgent} <> 0) AND {tracker_svensk_item.filesent} <> 0 "
ElseIf optComplete(23).Value = True Then 'Urgent + workable
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND ({tracker_svensk_item.urgent} <> 0) AND {tracker_svensk_item.rejected} = 0 AND {tracker_svensk_item.pendinginternal} = 0 AND {tracker_svensk_item.assetshere} <> 0 "
End If

If txtReference.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.itemreference} LIKE '" & txtReference.Text & "*' "
End If

If cmbTitle.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.title} LIKE '" & cmbTitle.Text & "*' "
End If
If txtSubtitle.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.subtitle} LIKE '" & txtSubtitle.Text & "*' "
End If
If cmbRightsOwner.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.rightsowner} LIKE '" & cmbRightsOwner.Text & "*' "
End If
If cmbProgramType.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND (not isnull({tracker_svensk_item.programtype}) AND {tracker_svensk_item.programtype} like '" & cmbProgramType.Text & "*') "
End If
If txtSeries.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND (not isnull({tracker_svensk_item.series}) AND {tracker_svensk_item.series} like '" & txtSeries.Text & "*') "
End If
If cmbFormat.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.format} LIKE '" & cmbFormat.Text & "*' "
End If
If Val(txtEpisode.Text) <> 0 Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.episode} = " & Val(txtEpisode.Text) & " "
End If
If cmbComponentType.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND (not isnull({tracker_svensk_item.componenttype}) AND {tracker_svensk_item.componenttype} like '" & cmbComponentType.Text & "*') "
End If
If txtSpecialProject.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND (not isnull({tracker_svensk_item.SpecialProject}) AND {tracker_svensk_item.SpecialProject} like '" & txtSpecialProject.Text & "*') "
End If


    
l_strDateSearch = ""
If Not IsNull(datFrom.Value) Then
    
    If optComplete(5).Value = True Or optComplete(6).Value = True Then 'File Made
        l_strDateSearch = " AND {tracker_svensk_item.filemade} >= #" & Format(datFrom.Value, "yyyy-mm-dd") & "# "
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.filemade} < #" & Format(datTo.Value, "yyyy-mm-dd") & "# "
        Else
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.filemade} < #" & Format(DateAdd("d", 1, datFrom.Value), "yyyy-mm-dd") & "# "
        End If
    End If


    If optComplete(1).Value = True Then 'Pending
        l_strDateSearch = ""
        l_strDateSearch = " AND {tracker_svensk_item.rejecteddate} >= #" & Format(datFrom.Value, "yyyy-mm-dd") & "# "
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.rejecteddate} < #" & Format(datTo.Value, "yyyy-mm-dd") & "# "
        Else
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.rejecteddate} < #" & Format(DateAdd("d", 1, datFrom.Value), "yyyy-mm-dd") & "# "
        End If
    End If
            
    If optComplete(11).Value = True Or optComplete(12).Value = True Then  'File Sent
        l_strDateSearch = ""
        l_strDateSearch = " AND {tracker_svensk_item.filesent} >= #" & Format(datFrom.Value, "yyyy-mm-dd") & "# "
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.filesent} < #" & Format(datTo.Value, "yyyy-mm-dd") & "# "
        Else
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.filesent} < #" & Format(DateAdd("d", 1, datFrom.Value), "yyyy-mm-dd") & "# "
        End If
    End If
    
    If optComplete(13).Value = True Then 'File Sent
        l_strDateSearch = ""
        l_strDateSearch = " AND {tracker_svensk_item.xmlsent} >= #" & Format(datFrom.Value, "yyyy-mm-dd") & "# "
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.xmlsent} < #" & Format(datTo.Value, "yyyy-mm-dd") & "# "
        Else
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.xmlsent} < #" & Format(DateAdd("d", 1, datFrom.Value), "yyyy-mm-dd") & "# "
        End If
    End If
        
        
    If optComplete(20).Value = True Then 'Target Date
        l_strDateSearch = ""
        l_strDateSearch = " AND {tracker_svensk_item.targetdate} >= #" & Format(datFrom.Value, "yyyy-mm-dd") & "# "
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.targetdate} < #" & Format(datTo.Value, "yyyy-mm-dd") & "# "
        Else
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.targetdate} < #" & Format(DateAdd("d", 1, datFrom.Value), "yyyy-mm-dd") & "# "
        End If
    End If
    
    If optComplete(2).Value = True Then 'complete
        l_strDateSearch = ""
        l_strDateSearch = " AND {tracker_svensk_item.complete} >= #" & Format(datFrom.Value, "yyyy-mm-dd") & "# "
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.complete} < #" & Format(datTo.Value, "yyyy-mm-dd") & "# "
        Else
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.complete} < #" & Format(DateAdd("d", 1, datFrom.Value), "yyyy-mm-dd") & "# "
        End If
    End If
            
    If optComplete(21).Value = True Then 'due date
        l_strDateSearch = ""
        l_strDateSearch = " AND {tracker_svensk_item.duedate} >= #" & Format(datFrom.Value, "yyyy-mm-dd") & "# "
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.duedate} < #" & Format(datTo.Value, "yyyy-mm-dd") & "# "
        Else
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.duedate} < #" & Format(DateAdd("d", 1, datFrom.Value), "yyyy-mm-dd") & "# "
        End If
    End If
     l_strSelectionFormula = l_strSelectionFormula & l_strDateSearch
            
End If
    
 l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.companyID} = " & Val(lblSearchCompanyID.Caption) & " "
    
    
If optReportComments(0).Value = True Then
    l_strReportFile = g_strLocationOfCrystalReportFiles & "Svensk_report_lastcommentonly.rpt"
ElseIf optReportComments(1).Value = True Then
    l_strReportFile = g_strLocationOfCrystalReportFiles & "Svensk_report_firstcommentonly.rpt"
Else
    l_strReportFile = g_strLocationOfCrystalReportFiles & "Svensk_report.rpt"
End If

Debug.Print l_strSelectionFormula
'MsgBox l_strSelectionFormula, vbInformation
PrintCrystalReport l_strReportFile, l_strSelectionFormula, True
    
End Sub

Private Sub cmdSaveUpdateInternal_Click()

Dim l_strSQL As String, l_lngCount As Long
Dim l_strUpdateSet As String
Dim bkmrk As Variant
Dim l_bulkDate As Date
l_bulkDate = Format(Year(Now) & "/" & Month(Now) & "/" & Day(Now) & " " & Hour(Now) & ":" & Minute(Now) & ":" & Second(Now))
 
If adoItems.Recordset.RecordCount = 0 Then Beep: Exit Sub

On Error GoTo BULK_UPDATE_ERROR

l_lngCount = grdItems.SelBookmarks.Count

If l_lngCount <= 0 Then
    MsgBox "No rows selected.", vbCritical, "Error with Bulk Update"
    Exit Sub
End If


'If MsgBox("Updating " & l_lngCount & " records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
If MsgBox("Updating " & l_lngCount & " records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
If l_lngCount > 10 Then
    If MsgBox("Updating more than 10 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 50 Then
    If MsgBox("Updating more than 50 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 100 Then
    If Not CheckAccess("/bulkupdatelots") Then Exit Sub
End If

For l_lngCount = 0 To grdItems.SelBookmarks.Count - 1

    bkmrk = grdItems.SelBookmarks(l_lngCount)
    l_strSQL = "Insert INTO tracker_svensk_comment(tracker_svensk_itemID, cdate, cuser, comment, internalcomment)VALUES     (" & Val(grdItems.Columns("tracker_Svensk_itemID").CellText(bkmrk)) & ", getdate(), '" & g_strFullUserName & "' , '" & txtUpInternalComments & "', -1)"
    Debug.Print l_strSQL

    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError

Next



cmdSearch.Value = True
fraUpdateComments.Visible = False

BULK_UPDATE_ERROR:

Exit Sub


End Sub

Private Sub cmdSearch_Click()

'Dim l_strDateSearch As String

l_strDateSearch = ""
If Not IsNull(datFrom.Value) Then
    
    If optComplete(5).Value = True Or optComplete(6).Value = True Then 'File Made
        l_strDateSearch = " AND filemade >= '" & ConvertDate(datFrom.Value) & "'"
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND filemade < '" & ConvertDate(datTo.Value) & "'"
        Else
            l_strDateSearch = l_strDateSearch & " AND filemade < '" & ConvertDate(DateAdd("d", 1, datFrom.Value)) & "'"
        End If
    End If

    If optComplete(1).Value = True Then 'Pending
        l_strDateSearch = ""
        l_strDateSearch = " AND rejecteddate >= '" & ConvertDate(datFrom.Value) & "'"
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND rejecteddate < '" & ConvertDate(datTo.Value) & "'"
        Else
            l_strDateSearch = l_strDateSearch & " AND rejecteddate < '" & ConvertDate(DateAdd("d", 1, datFrom.Value)) & "'"
        End If
    End If
            
    If optComplete(11).Value = True Or optComplete(12).Value = True Then  'File Sent
        l_strDateSearch = ""
        l_strDateSearch = " AND filesent >= '" & ConvertDate(datFrom.Value) & "'"
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND filesent < '" & ConvertDate(datTo.Value) & "'"
        Else
            l_strDateSearch = l_strDateSearch & " AND filesent < '" & ConvertDate(DateAdd("d", 1, datFrom.Value)) & "'"
        End If
    End If
    
    If optComplete(13).Value = True Then 'File Sent
        l_strDateSearch = ""
        l_strDateSearch = " AND xmlsent >= '" & ConvertDate(datFrom.Value) & "'"
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND xmlsent < '" & ConvertDate(datTo.Value) & "'"
        Else
            l_strDateSearch = l_strDateSearch & " AND xmlsent < '" & ConvertDate(DateAdd("d", 1, datFrom.Value)) & "'"
        End If
    End If
        
        
    If optComplete(20).Value = True Then 'Target Date
        l_strDateSearch = ""
        l_strDateSearch = " AND targetdate >= '" & ConvertDate(datFrom.Value) & "'"
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND targetdate < '" & ConvertDate(datTo.Value) & "'"
        Else
            l_strDateSearch = l_strDateSearch & " AND targetdate < '" & ConvertDate(DateAdd("d", 1, datFrom.Value)) & "'"
        End If
    End If
    
    If optComplete(2).Value = True Then 'complete
        l_strDateSearch = ""
        l_strDateSearch = " AND complete >= '" & ConvertDate(datFrom.Value) & "'"
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND complete < '" & ConvertDate(datTo.Value) & "'"
        Else
            l_strDateSearch = l_strDateSearch & " AND complete < '" & ConvertDate(DateAdd("d", 1, datFrom.Value)) & "'"
        End If
    End If
            
    If optComplete(21).Value = True Then 'due date
        l_strDateSearch = ""
        l_strDateSearch = " AND duedate >= '" & ConvertDate(datFrom.Value) & "'"
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND duedate < '" & ConvertDate(datTo.Value) & "'"
        Else
            l_strDateSearch = l_strDateSearch & " AND duedate < '" & ConvertDate(DateAdd("d", 1, datFrom.Value)) & "'"
        End If
    End If
            
    If optComplete(24).Value = True Then  'Work On Today
        l_strDateSearch = ""
        l_strDateSearch = " AND NewWorkOnToday >= '" & ConvertDate(datFrom.Value) & "'"
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND NewWorkOnToday < '" & ConvertDate(datTo.Value) & "'"
        Else
            l_strDateSearch = l_strDateSearch & " AND NewWorkOnToday < '" & ConvertDate(DateAdd("d", 1, datFrom.Value)) & "'"
        End If
    End If
            
            
End If
        
If cmbSorting.Text <> "" Then
    
    Select Case cmbSorting.Text
        Case "Invoice Sorting"
            m_strOrderby = " ORDER BY uniqueID, title, componenttype DESC, complete;"
        Case "Title"
            m_strOrderby = " ORDER BY title, urgent, CASE WHEN newworkontoday IS NULL THEN 1 ELSE 0 END, newworkontoday, CASE WHEN targetdate IS NULL THEN 1 ELSE 0 END, targetdate, CASE WHEN duedate IS NULL THEN 1 ELSE 0 END, duedate, uniqueID, componenttype DESC, complete;"
        Case "Operator"
            m_strOrderby = " ORDER BY operator, urgent, CASE WHEN newworkontoday IS NULL THEN 1 ELSE 0 END, newworkontoday, CASE WHEN targetdate IS NULL THEN 1 ELSE 0 END, targetdate, CASE WHEN duedate IS NULL THEN 1 ELSE 0 END, duedate, uniqueID, title, componenttype DESC, complete;"
        Case "Series"
            m_strOrderby = " ORDER BY series, urgent, CASE WHEN newworkontoday IS NULL THEN 1 ELSE 0 END, newworkontoday, CASE WHEN targetdate IS NULL THEN 1 ELSE 0 END, targetdate, CASE WHEN duedate IS NULL THEN 1 ELSE 0 END, duedate, uniqueID, title, componenttype DESC, complete;"
        Case "Episode"
            m_strOrderby = " ORDER BY episode, urgent, CASE WHEN newworkontoday IS NULL THEN 1 ELSE 0 END, newworkontoday, CASE WHEN targetdate IS NULL THEN 1 ELSE 0 END, targetdate, CASE WHEN duedate IS NULL THEN 1 ELSE 0 END, duedate, uniqueID, title, componenttype DESC, complete;"
        Case "Component"
            m_strOrderby = " ORDER BY componenttype DESC, urgent, CASE WHEN newworkontoday IS NULL THEN 1 ELSE 0 END, newworkontoday, CASE WHEN targetdate IS NULL THEN 1 ELSE 0 END, targetdate, CASE WHEN duedate IS NULL THEN 1 ELSE 0 END, duedate, uniqueID, title, complete;"
        Case "Language"
            m_strOrderby = " ORDER BY language, urgent, CASE WHEN newworkontoday IS NULL THEN 1 ELSE 0 END, newworkontoday, CASE WHEN targetdate IS NULL THEN 1 ELSE 0 END, targetdate, CASE WHEN duedate IS NULL THEN 1 ELSE 0 END, duedate, uniqueID, title, componenttype DESC, complete;"
        Case "Validation"
            m_strOrderby = " ORDER BY urgent, CASE WHEN targetdate IS NULL THEN 1 ELSE 0 END, targetdate, CASE WHEN duedate IS NULL THEN 1 ELSE 0 END, duedate, uniqueID, title, componenttype DESC, complete;"
        Case Else
            m_strOrderby = " ORDER BY urgent, CASE WHEN newworkontoday IS NULL THEN 1 ELSE 0 END, newworkontoday, CASE WHEN targetdate IS NULL THEN 1 ELSE 0 END, targetdate, CASE WHEN duedate IS NULL THEN 1 ELSE 0 END, duedate, uniqueID, title, componenttype DESC, complete;"
    End Select
End If
        
m_blnSilent = True
adoItems.ConnectionString = g_strConnection
msp_strSearch = SearchSQLstr()
adoItems.RecordSource = "SELECT * FROM tracker_Svensk_item " & m_strSearch & l_strDateSearch & msp_strSearch & m_strOrderby & ";"
Debug.Print adoItems.RecordSource

adoItems.Refresh

adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"
m_blnSilent = False

If Not adoItems.Recordset.EOF Then
    adoItems.Recordset.MoveLast
    adoItems.Recordset.MoveFirst
End If

End Sub

Function SearchSQLstr() As String

Dim l_strSQL As String

l_strSQL = ""

If chkHideDemo.Value <> 0 Then
    l_strSQL = l_strSQL & " AND companyID > 100 "
End If

If optComplete(0).Value = True Then 'Not Complete
    l_strSQL = l_strSQL & " AND (readytobill IS NULL OR readytobill = 0) "
ElseIf optComplete(1).Value = True Then 'Pending
    l_strSQL = l_strSQL & " AND (readytobill IS NULL OR readytobill = 0) AND rejected <> 0 "
ElseIf optComplete(2).Value = True Then 'Complete
    l_strSQL = l_strSQL & "AND readytobill <> 0  AND billed = 0 "
ElseIf optComplete(3).Value = True Then 'Billed
    l_strSQL = l_strSQL & "AND readytobill <> 0 AND billed <> 0 "
ElseIf optComplete(4).Value = True Then
    l_strSQL = l_strSQL & "AND 1=1 "
ElseIf optComplete(5).Value = True Then 'File Made
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND filemade <> 0 "
ElseIf optComplete(6).Value = True Then 'File Made but not sent
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND filemade <> 0 AND (filesent IS NULL or filesent = 0) "
ElseIf optComplete(7).Value = True Then 'File Not Made
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND (filemade IS NULL OR filemade = 0) "
ElseIf optComplete(8).Value = True Then 'XML Made
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND (xmlmade <> 0) "
ElseIf optComplete(9).Value = True Then 'XML Made but not sent
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND xmlmade <> 0 AND (xmlsent IS NULL or xmlsent = 0) "
ElseIf optComplete(10).Value = True Then ' XML not made
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND (xmlmade IS NULL OR xmlmade = 0) "
ElseIf optComplete(11).Value = True Then 'Sent with XML
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND filesent <> 0 AND xmlsent <> 0 "
ElseIf optComplete(12).Value = True Then ' Sent without xml
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND filesent <> 0 AND (xmlsent IS NULL or xmlsent = 0) "
ElseIf optComplete(13).Value = True Then 'Sent xml without file
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND (filesent IS NULL or filesent = 0) AND (xmlsent <> 0) "
ElseIf optComplete(14).Value = True Then 'Master Not Here
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND (masterarrived IS NULL OR (masterarrived IS NOT NULL AND tapereturned IS NOT NULL)) "
ElseIf optComplete(15).Value = True Then 'Internal Pending
    l_strSQL = l_strSQL & " AND (readytobill IS NULL OR readytobill = 0) AND pendinginternal <> 0 "
ElseIf optComplete(16).Value = True Then 'Decision Tree
    l_strSQL = l_strSQL & " AND (readytobill IS NULL OR readytobill = 0) AND decisiontree <> 0  "
ElseIf optComplete(17).Value = True Then 'Workable
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND rejected = 0 AND pendinginternal = 0 AND assetshere <> 0 "
ElseIf optComplete(18).Value = True Then 'Not workable
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND (rejected <> 0 OR pendinginternal <> 0 OR assetshere = 0) "
ElseIf optComplete(19).Value = True Then 'Urgent
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND (urgent <> 0) "
ElseIf optComplete(22).Value = True Then 'File Sent and Not Complete
     l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND filesent <> 0 "
ElseIf optComplete(23).Value = True Then 'Urgent + workable
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND (urgent <> 0) AND rejected = 0 AND pendinginternal = 0 AND assetshere <> 0 "
ElseIf optComplete(25).Value = True Then 'Urgent + workable
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND rejected = 0 AND pendinginternal = 0 AND decisiontree = 0 AND assetshere <> 0 "
End If

If chkMoreThan5Fixes.Value <> 0 Then
    l_strSQL = l_strSQL & " AND MoreThan5Fixes <> 0 "
End If
If txtReference.Text <> "" Then
    l_strSQL = l_strSQL & " AND itemreference LIKE '" & QuoteSanitise(txtReference.Text) & "%' "
End If
If txtAlphaCode.Text <> "" Then
    l_strSQL = l_strSQL & " AND AlphaDisplayCode LIKE '" & QuoteSanitise(txtAlphaCode.Text) & "%' "
End If
If cmbTitle.Text <> "" Then
    l_strSQL = l_strSQL & " AND (title IS NOT NULL AND title like '" & cmbTitle.Text & "%') "
End If
If txtSubtitle.Text <> "" Then
    l_strSQL = l_strSQL & " AND subtitle LIKE '" & QuoteSanitise(txtSubtitle.Text) & "%' "
End If
If cmbRightsOwner.Text <> "" Then
    l_strSQL = l_strSQL & " AND rightsowner LIKE '" & QuoteSanitise(cmbRightsOwner.Text) & "%' "
End If
If cmbProgramType.Text <> "" Then
    l_strSQL = l_strSQL & " AND (programtype IS NOT NULL AND programtype like '" & cmbProgramType.Text & "%') "
End If
If txtSeries.Text <> "" Then
    l_strSQL = l_strSQL & " AND (series IS NOT NULL AND series like '" & txtSeries.Text & "%') "
End If
If Val(txtEpisode.Text) <> 0 Then
    l_strSQL = l_strSQL & " AND (episode IS NOT NULL AND episode = '" & txtEpisode.Text & "') "
End If
If cmbFormat.Text <> "" Then
    l_strSQL = l_strSQL & " AND (format IS NOT NULL AND series = '" & cmbFormat.Text & "') "
End If
If cmbComponentType.Text <> "" Then
    l_strSQL = l_strSQL & " AND (componenttype IS NOT NULL AND componenttype like '" & cmbComponentType.Text & "%') "
End If
If cmbProjectManager.Text <> "" Then
    l_strSQL = l_strSQL & " AND (projectmanager IS NOT NULL AND projectmanager like '" & cmbProjectManager.Text & "%') "
End If
If cmbOperator.Text <> "" Then
    l_strSQL = l_strSQL & " AND (Operator IS NOT NULL AND Operator like '" & cmbOperator.Text & "%') "
ElseIf chkOperatorIsNull.Value <> 0 Then
    l_strSQL = l_strSQL & " AND (Operator IS NULL OR Operator = '') "
End If
If txtSpecialProject.Text <> "" Then
    l_strSQL = l_strSQL & " AND (SpecialProject IS NOT NULL AND SpecialProject like '" & QuoteSanitise(txtSpecialProject.Text) & "%') "
ElseIf chkSpecialProjectIsNull.Value <> 0 Then
    l_strSQL = l_strSQL & " AND (SpecialProject IS NULL OR SpecialProject = '') "
End If
If txtJobID.Text <> "" Then
    l_strSQL = l_strSQL & " AND jobID = " & Val(txtJobID.Text) & " "
End If

If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strSQL = l_strSQL & " AND companyID = " & Val(lblSearchCompanyID.Caption) & " "
End If

SearchSQLstr = l_strSQL

End Function


Private Sub cmdSubsValidationSave_Click()

Dim tempdate As Date, l_strSQL As String

If optSubsValidationComplete(0).Value = True Then
    tempdate = FormatDateTime(Now, vbLongDate)
    If grdItems.Columns("englishsubs").Text <> 0 Then
        grdItems.Columns("englishsubsvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("norwegiansubs").Text <> 0 Then
        grdItems.Columns("norwegiansubsvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("swedishsubs").Text <> 0 Then
        grdItems.Columns("swedishsubsvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("danishsubs").Text <> 0 Then
        grdItems.Columns("danishsubsvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("finnishsubs").Text <> 0 Then
        grdItems.Columns("finnishsubsvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("icelandicsubs").Text <> 0 Then
        grdItems.Columns("icelandicsubsvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
Else
    grdItems.Columns("englishsubsvalidation").Text = ""
    grdItems.Columns("norwegiansubsvalidation").Text = ""
    grdItems.Columns("swedishsubsvalidation").Text = ""
    grdItems.Columns("danishsubsvalidation").Text = ""
    grdItems.Columns("finnishsubsvalidation").Text = ""
    grdItems.Columns("icelandicsubsvalidation").Text = ""
End If

If optEnglishSubsInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "englishsubsinspec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optEnglishSubsInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "englishsubsinspec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "englishsubsinspec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optNorwegianSubsInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "NorwegianSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optNorwegianSubsInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "NorwegianSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "NorwegianSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optSwedishSubsInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "SwedishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optSwedishSubsInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "SwedishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "SwedishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optDanishSubsInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "DanishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optDanishSubsInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "DanishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "DanishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optFinnishSubsInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "FinnishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optFinnishSubsInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "FinnishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "FinnishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optIcelandicSubsInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "IcelandicSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optIcelandicSubsInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "IcelandicSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "IcelandicSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

l_strSQL = "INSERT INTO tracker_svensk_validation (tracker_svensk_itemID, ValidationDate, ValidatedBy) VALUES ("
l_strSQL = l_strSQL & Val(lblTrackeritemID.Caption) & ", "
l_strSQL = l_strSQL & "getdate(), "
l_strSQL = l_strSQL & "'" & g_strFullUserName & "');"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

cmdCloseSubsValidation.Value = True

End Sub

Private Sub cmdUnbill_Click()

grdItems.Columns("billed").Text = 0
grdItems.Columns("jobid").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False

End Sub

Private Sub cmdUnbillAll_Click()

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then

    Do While Not adoItems.Recordset.EOF
        adoItems.Recordset("billed") = 0
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
    Loop
End If

grdItems.Refresh

End Sub

Private Sub cmdUpdateAll_Click()

If adoItems.Recordset.RecordCount > 0 Then

    adoItems.Recordset.MoveFirst
    Do While Not adoItems.Recordset.EOF
        adoItems.Recordset("itemreference") = adoItems.Recordset("itemreference") & " "
        adoItems.Recordset("itemreference") = Trim(adoItems.Recordset("itemreference"))
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
    Loop
    adoItems.Recordset.MoveFirst
End If

End Sub

Private Sub cmdUpdateExternalGrid_Click()
Dim l_strSQL As String, l_lngCount As Long
Dim l_strUpdateSet As String
Dim l_rstTestCount As ADODB.Recordset
Dim l_commenttypedecoded As String
Dim l_commenttype As String


If adoItems.Recordset.RecordCount = 0 Then Beep: Exit Sub

On Error GoTo BULK_UPDATE_ERROR
Set l_rstTestCount = ExecuteSQL("SELECT tracker_Svensk_itemID FROM tracker_svensk_item " & m_strSearch & l_strDateSearch & SearchSQLstr() & m_strOrderby & ";", g_strExecuteError)

'Set l_rstTestCount = ExecuteSQL("SELECT tracker_Svensk_itemID FROM tracker_svensk_item " & m_strSearch & SearchSQLstr() & m_strOrderby & ";", g_strExecuteError)
CheckForSQLError

l_lngCount = l_rstTestCount.RecordCount
l_rstTestCount.Close
Set l_rstTestCount = Nothing

If l_lngCount <= 0 Then
    MsgBox "No selection criteria in place. Please do a search.", vbCritical, "Error with Bulk Update"
    Exit Sub
End If

If MsgBox("Updating " & l_lngCount & " records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub

If l_lngCount > 10 Then
    If MsgBox("Updating more than 10 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 50 Then
    If MsgBox("Updating more than 50 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 100 Then
    If Not CheckAccess("/bulkupdatelots") Then Exit Sub
End If

If cmbCommentType.Text = "" Then
    l_commenttypedecoded = "General"
Else
    l_commenttypedecoded = cmbCommentType.Text
End If
    
l_commenttype = GetDataSQL("SELECT information FROM xref WHERE category = 'trackercommenttypes' AND description = '" & l_commenttypedecoded & "';")



l_strSQL = adoItems.RecordSource
Set l_rstTestCount = ExecuteSQL(l_strSQL, g_strExecuteError)
If l_rstTestCount.RecordCount > 0 Then
    l_rstTestCount.MoveFirst
    Do While Not l_rstTestCount.EOF
   
        l_strSQL = "Insert INTO tracker_svensk_comment(tracker_svensk_itemID, cdate, cuser, commenttype, comment, emailtextlesspresent, filelocation, issueresolved, internalcomment)VALUES     (" & Val(l_rstTestCount("tracker_svensk_itemID")) & ", getdate(), '" & g_strFullUserName & "', '" & l_commenttype & "', '" & QuoteSanitise(txtUpExternalComments.Text) & "', '" & chkemailtextlesspresent.Value & "', '" & QuoteSanitise(txtUpExternalFile.Text) & "', '" & chkissueresolved.Value & "', 0)"
        Debug.Print l_strSQL

        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_rstTestCount.MoveNext
    Loop
    
End If

l_rstTestCount.Close
Set l_rstTestCount = Nothing

adoComments.Refresh
fraUpdateComments.Visible = False

BULK_UPDATE_ERROR:

Exit Sub
End Sub

Private Sub cmdUpdateExternalRows_Click()
Dim l_strSQL As String, l_lngCount As Long
Dim l_strUpdateSet As String
Dim bkmrk As Variant
Dim l_commenttypedecoded As String
Dim l_commenttype As String

If adoItems.Recordset.RecordCount = 0 Then Beep: Exit Sub

On Error GoTo BULK_UPDATE_ERROR

l_lngCount = grdItems.SelBookmarks.Count

If l_lngCount <= 0 Then
    MsgBox "No rows selected.", vbCritical, "Error with Bulk Update"
    Exit Sub
End If


'If MsgBox("Updating " & l_lngCount & " records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
If MsgBox("Updating " & l_lngCount & " records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
If l_lngCount > 10 Then
    If MsgBox("Updating more than 10 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 50 Then
    If MsgBox("Updating more than 50 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 100 Then
    If Not CheckAccess("/bulkupdatelots") Then Exit Sub
End If
    
If cmbCommentType.Text = "" Then
    l_commenttypedecoded = "General"
Else
    l_commenttypedecoded = cmbCommentType.Text
End If
    
l_commenttype = GetDataSQL("SELECT information FROM xref WHERE category = 'trackercommenttypes' AND description = '" & l_commenttypedecoded & "';")
'
'    If (grdItems.Columns("decisiontree").Text <> 0 And Val(grdComments.Columns("commenttype").Text) <> 1) Or (grdItems.Columns("rejected").Text <> 0 And Val(grdComments.Columns("commenttype").Text) <> 0) _
'    Or (grdItems.Columns("decisiontree").Text = 0 And grdItems.Columns("rejected").Text = 0 And Val(grdComments.Columns("commenttype").Text) <> 2) Then
'        If MsgBox("Comment type does not match up with Tracker item Decision Tree status." & vbCrLf & "Are you Sure", vbYesNo, "Please Confirm Decision Tree Comment Status") = vbNo Then
'            Cancel = True
'            Exit Sub
'        End If
'    End If

'    If grdComments.Columns("commenttype").Text = 0 And grdComments.Columns("comment").Text <> "" Then
'
'        If MsgBox("If the issue requires fixing, have you included details of what needs doing?" & vbCrLf, vbYesNo, "Please Confirm you know how to fix") = vbNo Then
'            Cancel = True
'            Exit Sub
'        End If
'    End If

For l_lngCount = 0 To grdItems.SelBookmarks.Count - 1

    bkmrk = grdItems.SelBookmarks(l_lngCount)
    l_strSQL = "Insert INTO tracker_svensk_comment(tracker_svensk_itemID, cdate, cuser, commenttype, comment, emailtextlesspresent, filelocation, issueresolved, internalcomment)VALUES     (" & Val(grdItems.Columns("tracker_Svensk_itemID").CellText(bkmrk)) & ", getdate(), '" & g_strFullUserName & "', '" & l_commenttype & "', '" & QuoteSanitise(txtUpExternalComments.Text) & "', '" & chkemailtextlesspresent.Value & "', '" & QuoteSanitise(txtUpExternalFile.Text) & "', '" & chkissueresolved.Value & "', 0)"
    Debug.Print l_strSQL

    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError

Next

adoComments.Refresh
fraUpdateComments.Visible = False

BULK_UPDATE_ERROR:

Exit Sub
End Sub

Private Sub cmdUpdateInternalGrid_Click()
Dim l_strSQL As String, l_lngCount As Long
Dim l_strUpdateSet As String
Dim l_rstTestCount As ADODB.Recordset
'm_strSearch = " WHERE 1 = 1 "


If adoItems.Recordset.RecordCount = 0 Then Beep: Exit Sub

On Error GoTo BULK_UPDATE_ERROR
Set l_rstTestCount = ExecuteSQL("SELECT tracker_Svensk_itemID FROM tracker_svensk_item " & m_strSearch & l_strDateSearch & SearchSQLstr() & m_strOrderby & ";", g_strExecuteError)

'Set l_rstTestCount = ExecuteSQL("SELECT tracker_Svensk_itemID FROM tracker_svensk_item " & m_strSearch & SearchSQLstr() & m_strOrderby & ";", g_strExecuteError)
CheckForSQLError

l_lngCount = l_rstTestCount.RecordCount
l_rstTestCount.Close
Set l_rstTestCount = Nothing

If l_lngCount <= 0 Then
    MsgBox "No selection criteria in place. Please do a search.", vbCritical, "Error with Bulk Update"
    Exit Sub
End If

If MsgBox("Updating " & l_lngCount & " records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub

If l_lngCount > 10 Then
    If MsgBox("Updating more than 10 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 50 Then
    If MsgBox("Updating more than 50 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 100 Then
    If Not CheckAccess("/bulkupdatelots") Then Exit Sub
End If

l_strSQL = adoItems.RecordSource
Set l_rstTestCount = ExecuteSQL(l_strSQL, g_strExecuteError)
If l_rstTestCount.RecordCount > 0 Then
    l_rstTestCount.MoveFirst
    Do While Not l_rstTestCount.EOF
    
        l_strSQL = "Insert INTO tracker_svensk_comment(tracker_svensk_itemID, cdate, cuser, comment, filelocation, internalcomment)VALUES     (" & Val(l_rstTestCount("tracker_svensk_itemID")) & ", getdate(), '" & g_strFullUserName & "' , '" & QuoteSanitise(txtUpInternalComments) & "', '" & QuoteSanitise(txtUpInternalFile) & "', -1)"
        Debug.Print l_strSQL

        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_rstTestCount.MoveNext
    Loop
    
End If

l_rstTestCount.Close
Set l_rstTestCount = Nothing

adoInternalComments.Refresh
fraUpdateComments.Visible = False

BULK_UPDATE_ERROR:

Exit Sub
End Sub

Private Sub cmdUpdateInternalRows_Click()
Dim l_strSQL As String, l_lngCount As Long
Dim l_strUpdateSet As String
Dim bkmrk As Variant
Dim l_bulkDate As Date
l_bulkDate = Format(Year(Now) & "/" & Month(Now) & "/" & Day(Now) & " " & Hour(Now) & ":" & Minute(Now) & ":" & Second(Now))
 
If adoItems.Recordset.RecordCount = 0 Then Beep: Exit Sub

On Error GoTo BULK_UPDATE_ERROR

l_lngCount = grdItems.SelBookmarks.Count

If l_lngCount <= 0 Then
    MsgBox "No rows selected.", vbCritical, "Error with Bulk Update"
    Exit Sub
End If


'If MsgBox("Updating " & l_lngCount & " records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
If MsgBox("Updating " & l_lngCount & " records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
If l_lngCount > 10 Then
    If MsgBox("Updating more than 10 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 50 Then
    If MsgBox("Updating more than 50 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 100 Then
    If Not CheckAccess("/bulkupdatelots") Then Exit Sub
End If

For l_lngCount = 0 To grdItems.SelBookmarks.Count - 1

    bkmrk = grdItems.SelBookmarks(l_lngCount)
    l_strSQL = "Insert INTO tracker_svensk_comment(tracker_svensk_itemID, cdate, cuser, comment, filelocation, internalcomment)VALUES     (" & Val(grdItems.Columns("tracker_Svensk_itemID").CellText(bkmrk)) & ", getdate(), '" & g_strFullUserName & "' , '" & QuoteSanitise(txtUpInternalComments) & "', '" & QuoteSanitise(txtUpInternalFile) & "', -1)"
    Debug.Print l_strSQL

    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError

Next


adoInternalComments.Refresh
fraUpdateComments.Visible = False

BULK_UPDATE_ERROR:

Exit Sub
End Sub

Private Sub cmdZipAndExport_Click()

Dim l_strSelectionFormula As String
Dim l_strReportFile As String
Dim l_strDateSearch As String

If lblSearchCompanyID.Caption = "" Then
    MsgBox "Please Select a Company Before running the report", vbCritical, "Error..."
    Exit Sub
End If

If optComplete(0).Value = True Then 'not complete
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) "
ElseIf optComplete(1).Value = True Then 'Pending
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) or {tracker_svensk_item.readytobill} = 0) and {tracker_svensk_item.rejected} <> 0"
ElseIf optComplete(2).Value = True Then 'Complete
    l_strSelectionFormula = "{tracker_svensk_item.readytobill} <> 0 and {tracker_svensk_item.billed} = 0"
ElseIf optComplete(3).Value = True Then 'Billed
    l_strSelectionFormula = "{tracker_svensk_item.readytobill} <> 0 and {tracker_svensk_item.billed} <> 0"
ElseIf optComplete(4).Value = True Then 'All Items
    l_strSelectionFormula = "1 = 1"
ElseIf optComplete(5).Value = True Then 'File Made
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.filemade} <> 0"
ElseIf optComplete(6).Value = True Then 'File made but not sent
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.filemade} <> 0 and (isnull({tracker_svensk_item.filesent}) OR {tracker_svensk_item.filesent} = 0)"
ElseIf optComplete(7).Value = True Then 'File Not Made
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND (isnull({tracker_svensk_item.filemade}) Or {tracker_svensk_item.filemade} = 0)"
ElseIf optComplete(8).Value = True Then 'xml Made
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.xmlmade} <> 0"
ElseIf optComplete(9).Value = True Then 'xml Made but not sent
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.xmlmade} <> 0 and (isnull({tracker_svensk_item.xmlsent}) OR {tracker_svensk_item.xmlsent} = 0)"
ElseIf optComplete(10).Value = True Then 'xml not made
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND (isnull({tracker_svensk_item.xmlmade}) or {tracker_svensk_item.xmlmade} = 0)"
ElseIf optComplete(11).Value = True Then 'Sent with xml
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.filesent} <> 0 AND {tracker_svensk_item.xmlsent} <> 0"
ElseIf optComplete(12).Value = True Then 'Sent without xml
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.filesent} <> 0 AND (isnull({tracker_svensk_item.xmlsent}) or {tracker_svensk_item.xmlsent} = 0)"
ElseIf optComplete(13).Value = True Then 'xml sent without File
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.xmlsent} <> 0 AND (isnull({tracker_svensk_item.filesent}) or {tracker_svensk_item.filesent} = 0)"
ElseIf optComplete(14).Value = True Then 'Master Not Here
    l_strSelectionFormula = "(isnull({tracker_svensk_item.masterarrived}) or (not isnull({tracker_svensk_item.masterarrived}) AND (not isnull({tracker_svensk_item.tapereturned})))"
ElseIf optComplete(15).Value = True Then 'Internal Pending
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) or {tracker_svensk_item.readytobill} = 0) and {tracker_svensk_item.pendinginternal} <> 0"
ElseIf optComplete(16).Value = True Then 'Decision Tree
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.decisiontree} <> 0"
ElseIf optComplete(17).Value = True Then 'Workable
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.rejected} = 0 AND {tracker_svensk_item.assetshere} <> 0 "
ElseIf optComplete(18).Value = True Then 'Not workable
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND ({tracker_svensk_item.rejected} <> 0 OR {tracker_svensk_item.assetshere} = 0) "
ElseIf optComplete(19).Value = True Then 'Urgent
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND ({tracker_svensk_item.urgent} <> 0) "
ElseIf optComplete(22).Value = True Then 'File Sent N/C
     l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND ({tracker_svensk_item.urgent} <> 0) AND {tracker_svensk_item.filesent} <> 0 "
ElseIf optComplete(23).Value = True Then 'Urgent + workable
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND ({tracker_svensk_item.urgent} <> 0) AND {tracker_svensk_item.rejected} = 0 AND {tracker_svensk_item.pendinginternal} = 0 AND {tracker_svensk_item.assetshere} <> 0 "
End If

If txtReference.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.itemreference} LIKE '" & txtReference.Text & "*' "
End If

If cmbTitle.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.title} LIKE '" & cmbTitle.Text & "*' "
End If
If txtSubtitle.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.subtitle} LIKE '" & txtSubtitle.Text & "*' "
End If
If cmbRightsOwner.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.rightsowner} LIKE '" & cmbRightsOwner.Text & "*' "
End If
If cmbProgramType.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND (not isnull({tracker_svensk_item.programtype}) AND {tracker_svensk_item.programtype} like '" & cmbProgramType.Text & "*') "
End If
If txtSeries.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND (not isnull({tracker_svensk_item.series}) AND {tracker_svensk_item.series} like '" & txtSeries.Text & "*') "
End If
If cmbFormat.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.format} LIKE '" & cmbFormat.Text & "*' "
End If
If Val(txtEpisode.Text) <> 0 Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.episode} = " & Val(txtEpisode.Text) & " "
End If
If cmbComponentType.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND (not isnull({tracker_svensk_item.componenttype}) AND {tracker_svensk_item.componenttype} like '" & cmbComponentType.Text & "*') "
End If
If txtSpecialProject.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND (not isnull({tracker_svensk_item.SpecialProject}) AND {tracker_svensk_item.SpecialProject} like '" & txtSpecialProject.Text & "*') "
End If


    
l_strDateSearch = ""
If Not IsNull(datFrom.Value) Then
    
    If optComplete(5).Value = True Or optComplete(6).Value = True Then 'File Made
        l_strDateSearch = " AND {tracker_svensk_item.filemade} >= #" & Format(datFrom.Value, "yyyy-mm-dd") & "# "
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.filemade} < #" & Format(datTo.Value, "yyyy-mm-dd") & "# "
        Else
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.filemade} < #" & Format(DateAdd("d", 1, datFrom.Value), "yyyy-mm-dd") & "# "
        End If
    End If


    If optComplete(1).Value = True Then 'Pending
        l_strDateSearch = ""
        l_strDateSearch = " AND {tracker_svensk_item.rejecteddate} >= #" & Format(datFrom.Value, "yyyy-mm-dd") & "# "
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.rejecteddate} < #" & Format(datTo.Value, "yyyy-mm-dd") & "# "
        Else
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.rejecteddate} < #" & Format(DateAdd("d", 1, datFrom.Value), "yyyy-mm-dd") & "# "
        End If
    End If
            
    If optComplete(11).Value = True Or optComplete(12).Value = True Then  'File Sent
        l_strDateSearch = ""
        l_strDateSearch = " AND {tracker_svensk_item.filesent} >= #" & Format(datFrom.Value, "yyyy-mm-dd") & "# "
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.filesent} < #" & Format(datTo.Value, "yyyy-mm-dd") & "# "
        Else
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.filesent} < #" & Format(DateAdd("d", 1, datFrom.Value), "yyyy-mm-dd") & "# "
        End If
    End If
    
    If optComplete(13).Value = True Then 'File Sent
        l_strDateSearch = ""
        l_strDateSearch = " AND {tracker_svensk_item.xmlsent} >= #" & Format(datFrom.Value, "yyyy-mm-dd") & "# "
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.xmlsent} < #" & Format(datTo.Value, "yyyy-mm-dd") & "# "
        Else
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.xmlsent} < #" & Format(DateAdd("d", 1, datFrom.Value), "yyyy-mm-dd") & "# "
        End If
    End If
        
        
    If optComplete(20).Value = True Then 'Target Date
        l_strDateSearch = ""
        l_strDateSearch = " AND {tracker_svensk_item.targetdate} >= #" & Format(datFrom.Value, "yyyy-mm-dd") & "# "
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.targetdate} < #" & Format(datTo.Value, "yyyy-mm-dd") & "# "
        Else
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.targetdate} < #" & Format(DateAdd("d", 1, datFrom.Value), "yyyy-mm-dd") & "# "
        End If
    End If
    
    If optComplete(2).Value = True Then 'complete
        l_strDateSearch = ""
        l_strDateSearch = " AND {tracker_svensk_item.complete} >= #" & Format(datFrom.Value, "yyyy-mm-dd") & "# "
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.complete} < #" & Format(datTo.Value, "yyyy-mm-dd") & "# "
        Else
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.complete} < #" & Format(DateAdd("d", 1, datFrom.Value), "yyyy-mm-dd") & "# "
        End If
    End If
            
    If optComplete(21).Value = True Then 'due date
        l_strDateSearch = ""
        l_strDateSearch = " AND {tracker_svensk_item.duedate} >= #" & Format(datFrom.Value, "yyyy-mm-dd") & "# "
        If Not IsNull(datTo.Value) Then
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.duedate} < #" & Format(datTo.Value, "yyyy-mm-dd") & "# "
        Else
            l_strDateSearch = l_strDateSearch & " AND {tracker_svensk_item.duedate} < #" & Format(DateAdd("d", 1, datFrom.Value), "yyyy-mm-dd") & "# "
        End If
    End If
     l_strSelectionFormula = l_strSelectionFormula & l_strDateSearch
            
End If
    
 l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.companyID} = " & Val(lblSearchCompanyID.Caption) & " "
    
    
If optReportComments(0).Value = True Then
    l_strReportFile = g_strLocationOfCrystalReportFiles & "Svensk_report_lastcommentonly.rpt"
ElseIf optReportComments(1).Value = True Then
    l_strReportFile = g_strLocationOfCrystalReportFiles & "Svensk_report_firstcommentonly.rpt"
Else
    l_strReportFile = g_strLocationOfCrystalReportFiles & "Svensk_report.rpt"
End If

Debug.Print l_strSelectionFormula
'MsgBox l_strSelectionFormula, vbInformation
    
Dim crxApplication As CRAXDRT.Application
Dim crxReport As CRAXDRT.Report

Set crxApplication = New CRAXDRT.Application
Set crxReport = crxApplication.OpenReport(GetUNCNameNT(l_strReportFile))

If Len(l_strSelectionFormula) > 0 Then
    crxReport.RecordSelectionFormula = l_strSelectionFormula
End If

crxReport.DiscardSavedData

Dim FSO As Scripting.FileSystemObject, l_strExportName As String
Set FSO = New Scripting.FileSystemObject
If optReportComments(0).Value = True Then
    l_strExportName = "Svensk_report_lastcommentonly"
ElseIf optReportComments(1).Value = True Then
    l_strExportName = "Svensk_report_firstcommentonly"
Else
    l_strExportName = "Svensk_report"
End If
If FSO.FileExists(App.Path & "\" & l_strExportName & ".xls") Then FSO.DeleteFile App.Path & "\" & l_strExportName & ".xls", True
If FSO.FileExists(App.Path & "\" & l_strExportName & ".zip") Then FSO.DeleteFile App.Path & "\" & l_strExportName & ".zip", True
crxReport.ExportOptions.DiskFileName = App.Path & "\" & l_strExportName & ".xls"
crxReport.ExportOptions.DestinationType = crEDTDiskFile
crxReport.ExportOptions.FormatType = crEFTExcelDataOnly
crxReport.Export False
If Create_Empty_Zip(App.Path & "\" & l_strExportName & ".zip") = True Then
    Zip_Activity "ZIPFILE", App.Path & "\" & l_strExportName & ".xls", App.Path & "\" & l_strExportName & ".zip"

    Dim rs As ADODB.Recordset, SQL As String
    
    SQL = "SELECT email, fullname FROM trackernotification WHERE contractgroup = 'DADC' and trackermessageID = 54;"
    Set rs = ExecuteSQL(SQL, g_strExecuteError)
    
    If rs.RecordCount > 0 Then
        rs.MoveFirst
        Do While Not rs.EOF
            SendSMTPMail rs("email"), rs("fullname"), "MX1 DADC Svensk Tracker Data", App.Path & "\" & l_strExportName & ".zip", "A Zipped MX1 DADC Svensk Tracker Data export is attached", True, "", ""
            rs.MoveNext
        Loop
    End If
    rs.Close
    Set rs = Nothing
    
End If

Set crxReport = Nothing
crxApplication.CanClose
Set crxApplication = Nothing

DoEvents

Screen.MousePointer = vbDefault
    
End Sub

Private Sub ddnChannel_CloseUp()

grdItems.Columns("companyID").Text = ddnChannel.Columns("companyID").Text
grdItems.Columns("channel").Text = ddnChannel.Columns("name").Text

End Sub

Private Sub ddnProjectManager_CloseUp()

grdItems.Columns("projectmanagercontactID").Text = ddnProjectManager.Columns("contactID").Text

End Sub

Private Sub DTPicker1_CallbackKeyDown(ByVal KeyCode As Integer, ByVal Shift As Integer, ByVal CallbackField As String, CallbackDate As Date)

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

Dim l_blnWide As Boolean

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch1 As ADODB.Recordset
Dim l_rstSearch2 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch1 = New ADODB.Recordset
Set l_rstSearch2 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT portalcompanyname, companyID FROM company WHERE cetaclientcode like '%/Svensktracker%' and companyID > 100 ORDER BY name;"
Else
    l_strSQL = "SELECT portalcompanyname, companyID FROM company WHERE cetaclientcode like '%/Svensktracker%' ORDER BY name;"
End If

With l_rstSearch1
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch1.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch1

With l_rstSearch2
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch2.ActiveConnection = Nothing

Set ddnChannel.DataSource = l_rstSearch2
grdItems.Columns("channel").DropDownHwnd = ddnChannel.hWnd

l_conSearch.Close
Set l_conSearch = Nothing

adoComments.ConnectionString = g_strConnection

grdItems.StyleSets("headerfield").BackColor = &HE7FFE7
grdItems.StyleSets("stagefield").BackColor = &HE7FFFF
grdItems.StyleSets("conclusionfield").BackColor = &HFFFFE7

grdItems.StyleSets.Add "Error"
grdItems.StyleSets("Error").BackColor = &HA0A0FF

grdItems.StyleSets.Add "NotRequired"
grdItems.StyleSets("NotRequired").BackColor = &H10101

grdItems.StyleSets.Add "Priority"
grdItems.StyleSets("Priority").BackColor = &H70B0FF

grdItems.StyleSets.Add "Fixed"
grdItems.StyleSets("Fixed").BackColor = &HA0FFA0
grdItems.StyleSets.Add "Notify"
grdItems.StyleSets("Notify").BackColor = &HFFFF40

grdItems.StyleSets.Add "Internal"
grdItems.StyleSets("Internal").BackColor = &HCCCC99
grdItems.Columns("rejected").HeadStyleSet = "Error"
grdItems.Columns("pendinginternal").HeadStyleSet = "Internal"
grdItems.Columns("urgent").HeadStyleSet = "Error"

optComplete(0).Value = True

m_strSearch = " WHERE 1 = 1 "
m_strOrderby = " ORDER BY urgent, CASE WHEN newworkontoday IS NULL THEN 1 ELSE 0 END, newworkontoday, CASE WHEN targetdate IS NULL THEN 1 ELSE 0 END, targetdate, CASE WHEN duedate IS NULL THEN 1 ELSE 0 END, duedate, uniqueID, title, componenttype DESC, complete;"
m_blnSilent = False

PopulateCombo "trackercommenttypes", ddnCommentTypes
grdComments.Columns("commenttypedecoded").DropDownHwnd = ddnCommentTypes.hWnd

PopulateCombo "dadcoperators", ddnUsers
grdItems.Columns("operator").DropDownHwnd = ddnUsers.hWnd

PopulateCombo "dadcoperators", cmbOperator

HideAllColumns

DoEvents

cmdSearch.Value = True
cmdFieldsSummary.Value = True

End Sub

Private Sub Form_Resize()

On Error Resume Next

grdItems.Width = Me.ScaleWidth - grdItems.Left - 120
grdItems.Height = (Me.ScaleHeight - grdItems.Top - frmButtons.Height) * 0.75 - 240
frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120
grdComments.Top = grdItems.Top + grdItems.Height + 120
grdComments.Height = frmButtons.Top - grdComments.Top - 120
grdInternalComments.Width = grdItems.Width - grdComments.Width - 120
grdInternalComments.Left = grdComments.Left + grdComments.Width + 120
grdInternalComments.Top = grdItems.Top + grdItems.Height + 120
grdInternalComments.Height = frmButtons.Top - grdInternalComments.Top - 120

End Sub



Private Sub grdComments_AfterDelete(RtnDispErrMsg As Integer)

m_blnDelete = False

End Sub

Private Sub grdInternalComments_AfterDelete(RtnDispErrMsg As Integer)

m_blnDelete = False

End Sub

Private Sub grdComments_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

m_blnDelete = True

End Sub
Private Sub grdInternalComments_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

m_blnDelete = True

End Sub
Private Sub grdComments_BeforeUpdate(Cancel As Integer)

If m_blnDelete = False Then
    If grdComments.Columns("comment").Text = "" Then
        MsgBox "Cannot Save a Comment with no actual comment", vbCritical, "Comment Not Saved"
        Cancel = True
        Exit Sub
    End If
    
    
    
    grdComments.Columns("tracker_Svensk_itemID").Text = lblTrackeritemID.Caption
    If grdComments.Columns("cdate").Text = "" Then
        grdComments.Columns("cdate").Text = Now
    End If
    
    If grdComments.Columns("cuser").Text = "" Then
        grdComments.Columns("cuser").Text = g_strFullUserName
    End If
    
    If grdComments.Columns("resolvedby").Text = "" And Val(grdComments.Columns("issueresolved").Text) <> 0 And Val(Trim(" " & adoComments.Recordset("issueresolved"))) = 0 Then
        grdComments.Columns("resolvedby").Text = g_strFullUserName
    End If
    
    If grdComments.Columns("resolvedby").Text <> "" And Val(grdComments.Columns("issueresolved").Text) = 0 And Val(Trim(" " & adoComments.Recordset("issueresolved"))) <> 0 Then
        grdComments.Columns("resolvedby").Text = ""
    End If
    
    If grdComments.Columns("commenttypedecoded").Text = "" Then grdComments.Columns("commenttypedecoded").Text = "General"
    grdComments.Columns("commenttype").Text = GetDataSQL("SELECT information FROM xref WHERE category = 'trackercommenttypes' AND description = '" & grdComments.Columns("commenttypedecoded").Text & "';")
        
    If (grdItems.Columns("decisiontree").Text <> 0 And Val(grdComments.Columns("commenttype").Text) <> 1) Or (grdItems.Columns("rejected").Text <> 0 And Val(grdComments.Columns("commenttype").Text) <> 0) _
    Or (grdItems.Columns("decisiontree").Text = 0 And grdItems.Columns("rejected").Text = 0 And Val(grdComments.Columns("commenttype").Text) <> 2) Then
        If MsgBox("Comment type does not match up with Tracker item Decision Tree status." & vbCrLf & "Are you Sure", vbYesNo, "Please Confirm Decision Tree Comment Status") = vbNo Then
            Cancel = True
            Exit Sub
        End If
    End If

    If grdComments.Columns("commenttype").Text = 0 And grdComments.Columns("comment").Text <> "" Then
    
        If MsgBox("If the issue requires fixing, have you included details of what needs doing?" & vbCrLf, vbYesNo, "Please Confirm you know how to fix") = vbNo Then
            Cancel = True
            Exit Sub
        End If
    End If

End If


End Sub

Private Sub grdInternalComments_BeforeUpdate(Cancel As Integer)

If m_blnDelete = False Then
    If grdInternalComments.Columns("comment").Text = "" Then
        MsgBox "Cannot Save a Comment with no actual comment", vbCritical, "Comment Not Saved"
        Cancel = True
        Exit Sub
    End If
    
    
    grdInternalComments.Columns("internalcomment").Text = True
    grdInternalComments.Columns("tracker_Svensk_itemID").Text = lblTrackeritemID.Caption
    If grdInternalComments.Columns("cdate").Text = "" Then
        grdInternalComments.Columns("cdate").Text = Now
    End If
    grdInternalComments.Columns("cuser").Text = g_strFullUserName
    
   ' If grdComments.Columns("commenttypedecoded").Text = "" Then grdComments.Columns("commenttypedecoded").Text = "General"
    grdInternalComments.Columns("commenttype").Text = GetDataSQL("SELECT information FROM xref WHERE category = 'trackercommenttypes' AND description = 'General';")
        
End If

End Sub

Private Sub grdComments_BtnClick()

Dim l_strOurEmailContact As String, l_strOurContactName As String, l_strEmailBody As String

Dim l_rstWhoToEmail As ADODB.Recordset

If MsgBox("Send Email?", vbYesNo, "Automatic Email") = vbYes Then
    
    l_strEmailBody = "A comment was created " & _
        "By: " & grdComments.Columns("cuser").Text & vbCrLf & _
        "About: " & grdItems.Columns("itemreference").Text & vbCrLf & _
        "Title: " & grdItems.Columns("title").Text & vbCrLf & _
        "Series: " & grdItems.Columns("series").Text & vbCrLf & _
        "Episode #: " & grdItems.Columns("episode").Text & vbCrLf & _
        "Language: " & grdItems.Columns("Language").Text & vbCrLf & _
        "Comment: " & grdComments.Columns("comment").Text & vbCrLf
        If Val(grdComments.Columns("Email - Clock?").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "RRsat will replace Clock." & vbCrLf
        If Val(grdComments.Columns("Email - B&T?").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "RRsat will replace Bars and Tone." & vbCrLf
        If Val(grdComments.Columns("Email - T/C?").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "RRsat will re-stripe the Timecode." & vbCrLf
        If Val(grdComments.Columns("Email - Black at End?").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "RRsat will add correct Black at End." & vbCrLf
        If Val(grdComments.Columns("Email - Textless Not Rq").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "Textless Elements Present but not Requested" & vbCrLf & "RRsat will take the Textless Elements." & vbCrLf
        If Val(grdComments.Columns("emailblackonwhitetexstless").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "White on Black Textless Elements." & vbCrLf
        If Val(grdComments.Columns("emailtracklayoutwrong").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "RRsat will Fix Track Layout." & vbCrLf
        
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND trackermessageID = 32;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "Svensk Tracker Comment Created", "", l_strEmailBody, True, "", ""
    
            l_rstWhoToEmail.MoveNext
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
    
End If

End Sub

Private Sub grdComments_RowLoaded(ByVal Bookmark As Variant)

If grdComments.Columns("commenttype").Text = 0 Then
    grdComments.Columns("Commenttypedecoded").Text = "Pending"
ElseIf grdComments.Columns("Commenttype").Text = 2 Then
    grdComments.Columns("Commenttypedecoded").Text = "General"
Else
    grdComments.Columns("Commenttypedecoded").Text = "Decision Tree"
End If



End Sub
Private Sub grdInternalComments_RowLoaded(ByVal Bookmark As Variant)

'If grdInternalComments.Columns("commenttype").Text = 0 Then
'    grdInternalComments.Columns("Commenttypedecoded").Text = "Pending"
'ElseIf grdInternalComments.Columns("Commenttype").Text = 2 Then
'    grdInternalComments.Columns("Commenttypedecoded").Text = "General"
'Else
'    grdInternalComments.Columns("Commenttypedecoded").Text = "Decision Tree"
'End If



End Sub



Private Sub grdItems_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdItems_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdItems_BeforeRowColChange(Cancel As Integer)

If picValidationMasterFiles.Visible = True Then
    cmdMasterValidationSave.Value = True
    picValidationMasterFiles.Visible = False
End If

If picValidationAudioFiles.Visible = True Then
    cmdAudioValidationSave.Value = True
    picValidationAudioFiles.Visible = False
End If

If picValidationSubs.Visible = True Then
    cmdSubsValidationSave.Value = True
    picValidationSubs.Visible = False
End If

End Sub

Private Sub grdItems_BeforeUpdate(Cancel As Integer)

If m_blnDelete = True Then Exit Sub

If grdItems.Columns("companyID").Text = "" Then

    MsgBox "Please select an MX1 Client." & vbCrLf & "Row not saved", vbCritical, "Error..."
    Cancel = 1
    Exit Sub
End If

Dim temp As Boolean, Count As Long, l_strDuration As String, l_strFrameRate As String, l_lngRunningTime As Long, l_curFileSize As Currency, l_strFilename As String, l_rst As ADODB.Recordset
Dim l_lngClipID As Long, l_strNetworkPath As String, l_strSQL As String, l_lngProjectManagerContactID As Long, l_datNewTargetDate As Date
Dim l_datRejectDateNow As Date
Dim l_datRejectDateCur As Date
Dim l_datMostRecentdDateCur As Date

'Update the UniqueID

grdItems.Columns("uniqueID").Text = grdItems.Columns("projectnumber").Text & "_" & Replace(grdItems.Columns("title").Text, " ", "_") & "_" & grdItems.Columns("programtype").Text & "_" & Format(Val(grdItems.Columns("series").Text), "00") & "_" & Format(Val(grdItems.Columns("episode").Text), "00")

'Check ReadytoBill - setting temp to false if any items are not ready to bill
If grdItems.Columns("complete").Text <> "" Then
    grdItems.Columns("readytobill").Text = 1
    If grdItems.Columns("proxyreq").Text <> 0 Then
        If grdItems.Columns("proxymade").Text = "" Then
            If MsgBox("Please confirm that you are about to mark a row as complete when the proxy does not appear to have been done yet." & vbCrLf & "Do you wish to continue?", vbYesNo, "Cancel") = vbNo Then
                Cancel = 1
                Exit Sub
            End If
        End If
    End If
Else
    grdItems.Columns("readytobill").Text = 0
End If

'Check if Project Manager has changed, and if so, look up the new contactID and enter it in the hidden field
If Val(lblTrackeritemID.Caption) <> 0 Then
'    If grdItems.Columns("projectmanager").Text <> GetData("tracker_svensk_item", "projectmanager", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption)) Then
        l_lngProjectManagerContactID = Val(Trim(" " & GetDataSQL("SELECT contactID FROM contact WHERE name = '" & grdItems.Columns("projectmanager").Text & "' AND contactID IN (SELECT contactID FROM employee WHERE companyID = " & Val(lblCompanyID.Caption) & ");")))
        If l_lngProjectManagerContactID <> 0 Then
            grdItems.Columns("projectmanagercontactID").Text = l_lngProjectManagerContactID
        Else
            MsgBox "You have typed or chosen a Project Manager who is not listed as a contact for that client." & vbCrLf & "Line not saved", vbCritical, "Error..."
            Cancel = 1
            Exit Sub
        End If
'    End If
End If

If grdItems.Columns("cdate").Text = "" Then grdItems.Columns("cdate").Text = Now
grdItems.Columns("mdate").Text = Now
l_strSQL = "INSERT INTO tracker_svensk_audit (tracker_svensk_itemID, mdate, muser) VALUES (" & Val(lblTrackeritemID.Caption) & ", getdate(), '" & g_strFullUserName & "');"
Debug.Print l_strSQL
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

temp = True
If ((UCase(grdItems.Columns("componenttype").Text) = "VIDEO" Or UCase(grdItems.Columns("componenttype").Text) = "AUDIO" Or UCase(grdItems.Columns("componenttype").Text) = "SUBTITLE") And grdItems.Columns("masterarrived").Text = "") Then temp = False
'If Val(grdItems.Columns("englishsepaudio").Text) <> 0 And grdItems.Columns("englisharrived").Text = "" Then temp = False
'If Val(grdItems.Columns("englishsubs").Text) <> 0 And grdItems.Columns("englishsubsarrived").Text = "" Then temp = False
'If Val(grdItems.Columns("norwegiansepaudio").Text) <> 0 And grdItems.Columns("norwegianarrived").Text = "" Then temp = False
'If Val(grdItems.Columns("norwegiansubs").Text) <> 0 And grdItems.Columns("norwegiansubsarrived").Text = "" Then temp = False
'If Val(grdItems.Columns("swedishsepaudio").Text) <> 0 And grdItems.Columns("swedisharrived").Text = "" Then temp = False
'If Val(grdItems.Columns("swedishsubs").Text) <> 0 And grdItems.Columns("swedishsubsarrived").Text = "" Then temp = False
'If Val(grdItems.Columns("danishsepaudio").Text) <> 0 And grdItems.Columns("danisharrived").Text = "" Then temp = False
'If Val(grdItems.Columns("danishsubs").Text) <> 0 And grdItems.Columns("danishsubsarrived").Text = "" Then temp = False
'If Val(grdItems.Columns("finnishsepaudio").Text) <> 0 And grdItems.Columns("finnisharrived").Text = "" Then temp = False
'If Val(grdItems.Columns("finnishsubs").Text) <> 0 And grdItems.Columns("finnishsubsarrived").Text = "" Then temp = False
'If Val(grdItems.Columns("icelandicsepaudio").Text) <> 0 And grdItems.Columns("icelandicarrived").Text = "" Then temp = False
'If Val(grdItems.Columns("icelandicsubs").Text) <> 0 And grdItems.Columns("icelandicsubsarrived").Text = "" Then temp = False
    
grdItems.Columns("assetshere").Text = CLng(temp)

'Check the Pending situation
If Val(grdItems.Columns("rejected").Text) <> 0 And (IsNull(grdItems.Columns("rejecteddate").Text) Or grdItems.Columns("rejecteddate").Text = "") Then
    l_datRejectDateNow = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("rejecteddate").Text = Format(l_datRejectDateNow, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("MostRecentRejectedDate").Text = Format(l_datRejectDateNow, "yyyy-mm-dd hh:nn:ss")
      
ElseIf Val(grdItems.Columns("rejected").Text) <> 0 And adoItems.Recordset("rejected") = 0 Then
     
    grdItems.Columns("MostRecentRejectedDate").Text = Format(Now, "yyyy-mm-dd hh:nn:ss")
    
ElseIf Val(grdItems.Columns("rejected").Text) = 0 And adoItems.Recordset("rejected") <> 0 Then
    l_datRejectDateNow = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("offRejectedDate").Text = l_datRejectDateNow

    l_datRejectDateCur = FormatDateTime(grdItems.Columns("rejecteddate").Text, vbShortDate)
    l_datMostRecentdDateCur = FormatDateTime(grdItems.Columns("MostRecentRejectedDate").Text, vbShortDate)

'    If l_datRejectDateCur = l_datMostRecentdDateCur Then 'rejectdatenow Then
'        grdItems.Columns("daysonRejected").Text = 1
'    Else
        grdItems.Columns("daysonRejected").Text = Val(grdItems.Columns("daysonRejected").Text) + DateDiff("d", l_datMostRecentdDateCur, CDate(grdItems.Columns("offRejectedDate").Text) + 1)
'    End If
End If

'Check the Decisiontree situation
If Val(grdItems.Columns("decisiontree").Text) <> 0 And (IsNull(grdItems.Columns("decisiontreedate").Text) Or grdItems.Columns("decisiontreedate").Text = "") Then
    l_datRejectDateNow = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("decisiontreedate").Text = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("mostrecentdecisiontreedate").Text = Format(l_datRejectDateNow, "yyyy-mm-dd hh:nn:ss")

ElseIf Val(grdItems.Columns("decisiontree").Text) <> 0 And adoItems.Recordset("decisiontree") = 0 Then
     
    grdItems.Columns("MostRecentDecisionTreeDate").Text = Format(Now, "yyyy-mm-dd hh:nn:ss")
    
ElseIf Val(grdItems.Columns("decisiontree").Text) = 0 And adoItems.Recordset("decisiontree") <> 0 Then
    l_datRejectDateNow = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("offdecisiontreeDate").Text = l_datRejectDateNow

    l_datRejectDateCur = FormatDateTime(grdItems.Columns("decisiontreedate").Text, vbShortDate)
    l_datMostRecentdDateCur = FormatDateTime(grdItems.Columns("MostRecentdecisiontreeDate").Text, vbShortDate)
    grdItems.Columns("daysondecisiontree").Text = Val(grdItems.Columns("daysondecisiontree").Text) + DateDiff("d", l_datMostRecentdDateCur, CDate(grdItems.Columns("offdecisiontreeDate").Text) + 1)

End If

'Get and update the file related items from the reference field.

If grdItems.Columns("itemreference").Text <> "" Then
    'see if there is a clip record to get the duration from reference
    l_strSQL = "SELECT fd_length FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' "
'    l_strSQL = l_strSQL & "AND companyid IN (SELECT companyID FROM company WHERE source = '" & GetData("company", "source", "companyID", Val(lblCompanyID.Caption)) & "') "
'    l_strSQL = l_strSQL & "AND companyID = " & lblCompanyID.Caption & " "
    l_strSQL = l_strSQL & "AND clipfilename = '" & grdItems.Columns("itemreference").Text & ".mov' AND system_deleted = 0;"
    Debug.Print l_strSQL
    l_strDuration = GetDataSQL(l_strSQL)
    'see if there is a clip record to get the framerate from reference
    l_strSQL = "SELECT clipframerate FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' "
'    l_strSQL = l_strSQL & "AND companyid IN (SELECT companyID FROM company WHERE source = '" & GetData("company", "source", "companyID", Val(lblCompanyID.Caption)) & "') "
'    l_strSQL = l_strSQL & "AND companyID = " & lblCompanyID.Caption & " "
    l_strSQL = l_strSQL & "AND clipfilename = '" & grdItems.Columns("itemreference").Text & ".mov' AND system_deleted = 0;"
    Debug.Print l_strSQL
    l_strFrameRate = GetDataSQL(l_strSQL)
    grdItems.Columns("timecodeduration").Text = l_strDuration
    grdItems.Columns("framerate").Text = l_strFrameRate
    'work out the duration in minutes from the timecode type duration
    If l_strDuration <> "" Then
        l_lngRunningTime = 60 * Val(Mid(l_strDuration, 1, 2))
        l_lngRunningTime = l_lngRunningTime + Val(Mid(l_strDuration, 4, 2))
        If Val(Mid(l_strDuration, 7, 2)) > 30 Then
            l_lngRunningTime = l_lngRunningTime + 1
        End If
        If l_lngRunningTime = 0 Then l_lngRunningTime = 1
        If (grdItems.Columns("duration").Text = "") Or (grdItems.Columns("duration").Text <> "" And chkLockDur.Value = 0) Then
            grdItems.Columns("duration").Text = l_lngRunningTime
        End If
    End If
    
    'See if there is a size to go in the GB sent column
    l_curFileSize = 0
    Set l_rst = ExecuteSQL("SELECT bigfilesize FROM events WHERE clipreference = '" & QuoteSanitise(grdItems.Columns("itemreference").Text) & "' AND companyID = " & Val(grdItems.Columns("companyID").Text) & " AND clipfilename = '" & QuoteSanitise(grdItems.Columns("itemreference").Text) & ".mov' AND system_deleted = 0;", g_strExecuteError)
    CheckForSQLError
    If l_rst.RecordCount > 0 Then
        l_rst.MoveFirst
        Do While Not l_rst.EOF
            If Not IsNull(l_rst("bigfilesize")) Then
                If l_rst("bigfilesize") > l_curFileSize Then l_curFileSize = l_rst("bigfilesize")
            End If
            l_rst.MoveNext
        Loop
    End If
    l_rst.Close
    If l_curFileSize <> 0 Then
        If (grdItems.Columns("gbsent").Text = "") Or (grdItems.Columns("gbsent").Text <> "") Then
            grdItems.Columns("gbsent").Text = Int(l_curFileSize / 1024 / 1024 / 1024 + 0.999)
        End If
    End If
    
    Set l_rst = Nothing
End If

'Check the Complaint situation
If Val(grdItems.Columns("complainedabout").Text) <> 0 And adoItems.Recordset("complainedabout") = 0 Then
    l_strSQL = "INSERT INTO complaint (companyID, contactID, complainttype, originalsvensktrackerID, cdate, cuser, mdate, muser) VALUES (" & lblCompanyID.Caption & ", 2840, 2, " & grdItems.Columns("tracker_svensk_itemID").Text & ", getdate(), '" & g_strFullUserName & "', getdate(), '" & g_strFullUserName & "');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
End If

'Check the Rush Job hasn't been changed and adjust target dates if it is.
If Val(grdItems.Columns("urgent").Text) <> 0 And Val(Trim(" " & adoItems.Recordset("urgent"))) = 0 Then
    If Val(grdItems.Columns("targetdatecountdown").Text) > 1 Then
        Count = 1 - Val(grdItems.Columns("targetdatecountdown").Text)
        grdItems.Columns("targetdatecountdown").Text = Val(grdItems.Columns("targetdatecountdown").Text) + Count
        If grdItems.Columns("TargetDate").Text <> "" Then
            Set l_rst = ExecuteSQL("exec fn_getWorkingDaysBackFromDate @fromdate='" & Format(grdItems.Columns("TargetDate").Text, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & -Count, g_strExecuteError)
            l_datNewTargetDate = l_rst(0)
            l_rst.Close
            grdItems.Columns("TargetDate").Text = l_datNewTargetDate
        End If
    End If
ElseIf Val(grdItems.Columns("urgent").Text) = 0 And Val(Trim(" " & adoItems.Recordset("urgent"))) <> 0 Then
    grdItems.Columns("targetdatecountdown").Text = Val(grdItems.Columns("targetdatecountdown").Text) + 2
    If Val(grdItems.Columns("targetdatecountdown").Text) > 3 Then grdItems.Columns("targetdatecountdown").Text = 3
    If grdItems.Columns("TargetDate").Text <> "" Then
        Count = Val(grdItems.Columns("targetdatecountdown").Text) - Val(adoItems.Recordset("targetdatecountdown"))
        Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(grdItems.Columns("TargetDate").Text, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Count, g_strExecuteError)
        l_datNewTargetDate = l_rst(0)
        l_rst.Close
        grdItems.Columns("TargetDate").Text = l_datNewTargetDate
    End If
End If

'Check the Target Date situation
If grdItems.Columns("assetshere").Text <> 0 And Val(grdItems.Columns("rejected").Text) = 0 And Val(grdItems.Columns("decisiontree").Text) = 0 And grdItems.Columns("readytobill").Text = 0 Then

    Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Val(grdItems.Columns("targetdatecountdown").Text), g_strExecuteError)
    l_datNewTargetDate = l_rst(0)
    l_rst.Close
    If Trim(" " & grdItems.Columns("Completeable").Text) = "" Then grdItems.Columns("Completeable").Text = Now()
    If Trim(" " & grdItems.Columns("firstcompleteable").Text) = "" Then grdItems.Columns("firstCompleteable").Text = grdItems.Columns("Completeable").Text
    If Trim(" " & grdItems.Columns("TargetDate").Text) = "" Then grdItems.Columns("TargetDate").Text = l_datNewTargetDate

ElseIf grdItems.Columns("readytobill").Text = 0 Then

    grdItems.Columns("Completeable").Text = ""
    grdItems.Columns("TargetDate").Text = ""
        
End If

'Check the iTunes Tracker situation
If grdItems.Columns("complete").Text <> "" And Trim(" " & adoItems.Recordset("complete")) = "" And grdItems.Columns("iTunes").Text <> 0 Then
    'Inform the list category that there is a new item that is being marked completed that needs to be emailed to the iTunes team.
    If MsgBox("Should E-mail Notices be sent out?", vbYesNo, "Item required for iTunes is being marked complete") = vbYes Then

        Dim l_rstWhoToEmail As ADODB.Recordset, l_strOurEmailContact As String, l_strOurContactName As String

        Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & _
        "' AND trackermessageID = 44;", g_strExecuteError)
        CheckForSQLError

        If l_rstWhoToEmail.RecordCount > 0 Then
            l_rstWhoToEmail.MoveFirst
            While Not l_rstWhoToEmail.EOF
                SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "Svensk item ready for iTunes", "", "Item: " & _
                grdItems.Columns("title").Text & ", Ep: " & grdItems.Columns("episode").Text & ", Ep. Title: " & grdItems.Columns("subtitle").Text & vbCrLf & _
                "Component: " & grdItems.Columns("componenttype").Text & ", Language: " & grdItems.Columns("Language").Text & vbCrLf & _
                "This item is now ready for iTunes use." & vbCrLf, True, "", "", "", True

                l_rstWhoToEmail.MoveNext
            Wend
        End If
        l_rstWhoToEmail.Close
        Set l_rstWhoToEmail = Nothing

    End If

End If

End Sub

Private Sub grdItems_BtnClick()

Dim tempdate As String

'If grdItems.Columns(grdItems.Col).Name = "Validation" Then
'    picValidationMasterFiles.Visible = True
'    lblValidationTitle.Caption = grdItems.Columns("title").Text
'    lblValidationSeries.Caption = Format(Val(grdItems.Columns("series").Text), "00")
'    lblValidationEpisode.Caption = Format(Val(grdItems.Columns("episode").Text), "00")
'    If grdItems.Columns("validation").Text <> "" Then
'        optMasterValidationComplete(0).Value = True
'    Else
'        optMasterValidationComplete(1).Value = True
'    End If
'    If GetData("tracker_svensk_item", "videoinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption) <> 0 Then
'        optVideoInSpec(0).Value = True
'    Else
'        optVideoInSpec(1).Value = True
'    End If
'    cmbFrameRate.Text = GetData("tracker_svensk_item", "framerate", "tracker_svensk_itemID", lblTrackeritemID.Caption)
'    If GetData("tracker_svensk_item", "hdflag", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
'        optHDFlag(0).Value = True
'    Else
'        optHDFlag(1).Value = True
'    End If
'    If GetData("tracker_svensk_item", "MainStereoPresent", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
'        optMainStereoPresent(0).Value = True
'    Else
'        optMainStereoPresent(1).Value = True
'    End If
'    If GetData("tracker_svensk_item", "MEStereoPresent", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
'        optMEStereoPresent(0).Value = True
'    Else
'        optMEStereoPresent(1).Value = True
'    End If
'    If GetData("tracker_svensk_item", "SurroundPresent", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
'        optSurroundPresent(0).Value = True
'    Else
'        optSurroundPresent(1).Value = True
'    End If
'    If GetData("tracker_svensk_item", "TextlessPresent", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
'        optTextlessPresent(0).Value = True
'    Else
'        optTextlessPresent(1).Value = True
'    End If
'    If GetData("tracker_svensk_item", "BlackAtEnd", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
'        optBlackAtEnd(0).Value = True
'    Else
'        optBlackAtEnd(1).Value = True
'    End If
'    If GetData("tracker_svensk_item", "TextlessSeparator", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
'        optTextlessSeparator(0).Value = True
'    Else
'        optTextlessSeparator(1).Value = True
'    End If
'
'ElseIf grdItems.Columns(grdItems.Col).Name = "englishvalidation" Or grdItems.Columns(grdItems.Col).Name = "norwegianvalidation" _
'Or grdItems.Columns(grdItems.Col).Name = "swedishvalidation" Or grdItems.Columns(grdItems.Col).Name = "danishvalidation" Or grdItems.Columns(grdItems.Col).Name = "finnishvalidation" _
'Or grdItems.Columns(grdItems.Col).Name = "icelandicvalidation" Then
'    If grdItems.Columns(grdItems.Col).Text <> "" Then
'        optAudioValidationComplete(0).Value = True
'    Else
'        optAudioValidationComplete(1).Value = True
'    End If
'    picValidationAudioFiles.Visible = True
'    lblValidationTitle.Caption = grdItems.Columns("title").Text
'    lblValidationSeries.Caption = Format(Val(grdItems.Columns("series").Text), "00")
'    lblValidationEpisode.Caption = Format(Val(grdItems.Columns("episode").Text), "00")
'    optEnglishInSpec(Val(GetData("tracker_svensk_item", "englishinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optNorwegianInSpec(Val(GetData("tracker_svensk_item", "norwegianinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optSwedishInSpec(Val(GetData("tracker_svensk_item", "swedishinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optDanishInSpec(Val(GetData("tracker_svensk_item", "danishinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optFinnishInSpec(Val(GetData("tracker_svensk_item", "finnishinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optIcelandicInSpec(Val(GetData("tracker_svensk_item", "icelandicinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    If grdItems.Columns("englishsepaudio").Text = 0 Then optEnglishInSpec(2).Value = True
'    If grdItems.Columns("norwegiansepaudio").Text = 0 Then optNorwegianInSpec(2).Value = True
'    If grdItems.Columns("swedishsepaudio").Text = 0 Then optSwedishInSpec(2).Value = True
'    If grdItems.Columns("danishsepaudio").Text = 0 Then optDanishInSpec(2).Value = True
'    If grdItems.Columns("finnishsepaudio").Text = 0 Then optFinnishInSpec(2).Value = True
'    If grdItems.Columns("icelandicsepaudio").Text = 0 Then optIcelandicInSpec(2).Value = True
'ElseIf grdItems.Columns(grdItems.Col).Name = "englishsubsvalidation" Or grdItems.Columns(grdItems.Col).Name = "norwegiansubsvalidation" _
'Or grdItems.Columns(grdItems.Col).Name = "swedishsubsvalidation" Or grdItems.Columns(grdItems.Col).Name = "danishsubsvalidation" Or grdItems.Columns(grdItems.Col).Name = "finnishsubsvalidation" _
'Or grdItems.Columns(grdItems.Col).Name = "icelandicsubsvalidation" Then
'    If grdItems.Columns(grdItems.Col).Text <> "" Then
'        optSubsValidationComplete(0).Value = True
'    Else
'        optSubsValidationComplete(1).Value = True
'    End If
'    picValidationSubs.Visible = True
'    lblValidationTitle.Caption = grdItems.Columns("title").Text
'    lblValidationSeries.Caption = Format(Val(grdItems.Columns("series").Text), "00")
'    lblValidationEpisode.Caption = Format(Val(grdItems.Columns("episode").Text), "00")
'    optEnglishSubsInSpec(Val(GetData("tracker_svensk_item", "englishsubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optNorwegianSubsInSpec(Val(GetData("tracker_svensk_item", "norwegiansubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optSwedishSubsInSpec(Val(GetData("tracker_svensk_item", "swedishsubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optDanishSubsInSpec(Val(GetData("tracker_svensk_item", "danishsubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optFinnishSubsInSpec(Val(GetData("tracker_svensk_item", "finnishsubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    optIcelandicSubsInSpec(Val(GetData("tracker_svensk_item", "icelandicsubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
'    If grdItems.Columns("englishsubs").Text = 0 Then optEnglishSubsInSpec(2).Value = True
'    If grdItems.Columns("norwegiansubs").Text = 0 Then optNorwegianSubsInSpec(2).Value = True
'    If grdItems.Columns("swedishsubs").Text = 0 Then optSwedishSubsInSpec(2).Value = True
'    If grdItems.Columns("danishsubs").Text = 0 Then optDanishSubsInSpec(2).Value = True
'    If grdItems.Columns("finnishsubs").Text = 0 Then optFinnishSubsInSpec(2).Value = True
'    If grdItems.Columns("icelandicsubs").Text = 0 Then optIcelandicSubsInSpec(2).Value = True

Select Case LCase(grdItems.Columns(grdItems.Col).Name)

Case "newworkontoday"
    If grdItems.ActiveCell.Text <> "" Then
        grdItems.ActiveCell.Text = ""
    Else
        tempdate = FormatDateTime(Now, vbLongDate)
        grdItems.ActiveCell.Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If

Case Else
    If grdItems.ActiveCell.Text <> "" Then
        grdItems.ActiveCell.Text = ""
    Else
        If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
            grdItems.ActiveCell.Text = Now
        Else
            tempdate = FormatDateTime(Now, vbLongDate)
            grdItems.ActiveCell.Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
        End If
    End If

End Select

End Sub

Private Sub grdItems_DblClick()

ShowClipSearch "", grdItems.Columns("itemreference").Text

End Sub

Private Sub grdItems_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim l_strSQL As String

If m_blnSilent = False Then
    lblTrackeritemID.Caption = grdItems.Columns("tracker_Svensk_itemID").Text
    lblCompanyID.Caption = grdItems.Columns("companyID").Text
    
    If Val(lblTrackeritemID.Caption) <> Val(lblLastTrackerItemID.Caption) Then
        
        If Val(lblTrackeritemID.Caption) = 0 Then
            
            l_strSQL = "SELECT * FROM tracker_Svensk_comment WHERE tracker_Svensk_itemID = -1 and internalcomment = 0 ORDER BY cdate;"
            
            adoComments.RecordSource = l_strSQL
            adoComments.ConnectionString = g_strConnection
            adoComments.Refresh
            
            l_strSQL = "SELECT * FROM tracker_Svensk_comment WHERE tracker_Svensk_itemID = -1 and internalcomment <>0 ORDER BY cdate;"
            
            adoInternalComments.RecordSource = l_strSQL
            adoInternalComments.ConnectionString = g_strConnection
            adoInternalComments.Refresh
            
            lblLastTrackerItemID.Caption = ""
        
            Exit Sub
        
        End If
        
        l_strSQL = "SELECT * FROM tracker_Svensk_comment WHERE tracker_Svensk_itemID = " & lblTrackeritemID.Caption & " and internalcomment = 0 ORDER BY cdate;"
        
        adoComments.RecordSource = l_strSQL
        adoComments.ConnectionString = g_strConnection
        adoComments.Refresh
    
        l_strSQL = "SELECT * FROM tracker_Svensk_comment WHERE tracker_Svensk_itemID = " & lblTrackeritemID.Caption & " and internalcomment <> 0 ORDER BY cdate;"
        
        adoInternalComments.RecordSource = l_strSQL
        adoInternalComments.ConnectionString = g_strConnection
        adoInternalComments.Refresh
    
    
        lblLastTrackerItemID.Caption = lblTrackeritemID.Caption
        
        If grdItems.Columns("readytobill").Text <> 0 And grdItems.Columns("billed").Text = 0 And grdItems.Columns("complaintredoitem").Text = 0 Then
            cmdBillItem.Visible = True
            cmdManualBillItem.Visible = True
        ElseIf grdItems.Columns("complaintredoitem").Text <> 0 Then
            cmdBillItem.Visible = False
            cmdManualBillItem.Visible = True
        Else
            cmdBillItem.Visible = False
            cmdManualBillItem.Visible = False
        End If

        If grdItems.Columns("billed").Text <> 0 Then
            cmdUnbill.Visible = True
        Else
            cmdUnbill.Visible = False
        End If

        lblLastValidationBy.Caption = GetDataSQL("SELECT ValidatedBy FROM tracker_svensk_validation WHERE tracker_svensk_itemID = " & Val(lblTrackeritemID) & " ORDER BY ValidationDate DESC;")
        lblLastValidationDate.Caption = GetDataSQL("SELECT ValidationDate FROM tracker_svensk_validation WHERE tracker_svensk_itemID = " & Val(lblTrackeritemID) & " ORDER BY ValidationDate DESC;")
    
    End If

End If

End Sub

Private Sub grdItems_RowLoaded(ByVal Bookmark As Variant)

'Set the error states on the individual columns that can have errors

grdItems.Columns("channel").Text = GetData("company", "portalcompanyname", "companyID", Val(grdItems.Columns("companyID").Text))

If Val(grdItems.Columns("rejected").Text) <> 0 Then
    grdItems.Columns("rejected").CellStyleSet "Error"
End If

If Val(grdItems.Columns("resupply").Text) <> 0 Then
    grdItems.Columns("resupply").CellStyleSet "Error"
End If

If Val(grdItems.Columns("decisiontree").Text) <> 0 Then
    grdItems.Columns("decisiontree").CellStyleSet "Error"
End If

If Val(grdItems.Columns("urgent").Text) <> 0 Then
    grdItems.Columns("urgent").CellStyleSet "Error"
End If

If Val(grdItems.Columns("iTunes").Text) <> 0 Then
  '  grdItems.Columns("iTunes").CellStyleSet "Notify"
  '  grdItems.Columns("title").CellStyleSet "Notify"
End If

If grdItems.Columns("AlphaDisplayCode").Text <> "" Then
    grdItems.Columns("AlphaDisplayCode").CellStyleSet "Notify"
End If



If Val(grdItems.Columns("PendingInternal").Text) <> 0 Then
    grdItems.Columns("PendingInternal").CellStyleSet "Internal"
     grdItems.Columns("title").CellStyleSet "Internal"
End If

'If Val(grdItems.Columns("englishsepaudio").Text) = 0 Then
'    grdItems.Columns("englisharrived").CellStyleSet "NotRequired"
'ElseIf Trim(" " & grdItems.Columns("englisharrived").Text) = "" Then
'    grdItems.Columns("englisharrived").CellStyleSet "Error"
'End If
'
'If Val(grdItems.Columns("englishsubs").Text) = 0 Then
'    grdItems.Columns("englishsubsarrived").CellStyleSet "NotRequired"
'ElseIf Trim(" " & grdItems.Columns("englishsubsarrived").Text) = "" Then
'    grdItems.Columns("englishsubsarrived").CellStyleSet "Error"
'End If
'
'If Val(grdItems.Columns("norwegiansepaudio").Text) = 0 Then
'    grdItems.Columns("norwegianarrived").CellStyleSet "NotRequired"
'ElseIf Trim(" " & grdItems.Columns("norwegianarrived").Text) = "" Then
'    grdItems.Columns("norwegianarrived").CellStyleSet "Error"
'End If
'
'If Val(grdItems.Columns("norwegiansubs").Text) = 0 Then
'    grdItems.Columns("norwegiansubsarrived").CellStyleSet "NotRequired"
'ElseIf Trim(" " & grdItems.Columns("norwegiansubsarrived").Text) = "" Then
'    grdItems.Columns("norwegiansubsarrived").CellStyleSet "Error"
'End If
'
'If Val(grdItems.Columns("swedishsepaudio").Text) = 0 Then
'    grdItems.Columns("swedisharrived").CellStyleSet "NotRequired"
'ElseIf Trim(" " & grdItems.Columns("swedisharrived").Text) = "" Then
'    grdItems.Columns("swedisharrived").CellStyleSet "Error"
'End If
'
'If Val(grdItems.Columns("swedishsubs").Text) = 0 Then
'    grdItems.Columns("swedishsubsarrived").CellStyleSet "NotRequired"
'ElseIf Trim(" " & grdItems.Columns("swedishsubsarrived").Text) = "" Then
'    grdItems.Columns("swedishsubsarrived").CellStyleSet "Error"
'End If
'
'If Val(grdItems.Columns("danishsepaudio").Text) = 0 Then
'    grdItems.Columns("danisharrived").CellStyleSet "NotRequired"
'ElseIf Trim(" " & grdItems.Columns("danisharrived").Text) = "" Then
'    grdItems.Columns("danisharrived").CellStyleSet "Error"
'End If
'
'If Val(grdItems.Columns("danishsubs").Text) = 0 Then
'    grdItems.Columns("danishsubsarrived").CellStyleSet "NotRequired"
'ElseIf Trim(" " & grdItems.Columns("danishsubsarrived").Text) = "" Then
'    grdItems.Columns("danishsubsarrived").CellStyleSet "Error"
'End If
'
'If Val(grdItems.Columns("finnishsepaudio").Text) = 0 Then
'    grdItems.Columns("finnisharrived").CellStyleSet "NotRequired"
'ElseIf Trim(" " & grdItems.Columns("finnisharrived").Text) = "" Then
'    grdItems.Columns("finnisharrived").CellStyleSet "Error"
'End If
'
'If Val(grdItems.Columns("finnishsubs").Text) = 0 Then
'    grdItems.Columns("finnishsubsarrived").CellStyleSet "NotRequired"
'ElseIf Trim(" " & grdItems.Columns("finnishsubsarrived").Text) = "" Then
'    grdItems.Columns("finnishsubsarrived").CellStyleSet "Error"
'End If
'
'If Val(grdItems.Columns("icelandicsepaudio").Text) = 0 Then
'    grdItems.Columns("icelandicarrived").CellStyleSet "NotRequired"
'ElseIf Trim(" " & grdItems.Columns("icelandicarrived").Text) = "" Then
'    grdItems.Columns("icelandicarrived").CellStyleSet "Error"
'End If
'
'If Val(grdItems.Columns("icelandicsubs").Text) = 0 Then
'    grdItems.Columns("icelandicsubsarrived").CellStyleSet "NotRequired"
'ElseIf Trim(" " & grdItems.Columns("icelandicsubsarrived").Text) = "" Then
'    grdItems.Columns("icelandicsubsarrived").CellStyleSet "Error"
'End If

'If Val(grdItems.Columns("rejected").Text) <> 0 Then
'    grdItems.Columns("rejected").CellStyleSet "Error"
'    grdItems.Columns("readytobill").CellStyleSet "Error"
'    grdItems.Columns("billed").CellStyleSet "Error"
'    grdItems.Columns("fixed").CellStyleSet "Error"
'End If
'
'If Val(grdItems.Columns("fixed").Text) <> 0 Then
'    grdItems.Columns("rejected").CellStyleSet "Fixed"
'    grdItems.Columns("readytobill").CellStyleSet "Fixed"
'    grdItems.Columns("billed").CellStyleSet "Fixed"
'    grdItems.Columns("fixed").CellStyleSet "Fixed"
'End If

End Sub

'Private Sub grdItems_SelChange(ByVal SelType As Integer, Cancel As Integer, DispSelRowOverflow As Integer)
'MsgBox ("selected")
'End Sub

Private Sub optComplete_Click(Index As Integer)

cmbSorting.Text = "Validation"
cmdSearch.Value = True

End Sub

Function CheckReadyForTX() As Boolean

Dim temp As Boolean

temp = True

If Trim(" " & grdItems.Columns("masterarrived").Text) = "" Then temp = False
If Trim(" " & grdItems.Columns("filemade").Text) = "" Then temp = False
'If grdItems.Columns("senttosoundhouse").Text = "" Or UCase(Right(grdItems.Columns("senttosoundhouse").Text, 2)) = "ER" Then temp = False
'If Val(grdItems.Columns("englishaudioneeded").Text) <> 0 Then
'    If grdItems.Columns("englishaudioarrived").Text = "" Or UCase(Right(grdItems.Columns("englishaudioarrived").Text, 2)) = "ER" Then temp = False
'End If
'If Val(grdItems.Columns("frenchaudioneeded").Text) <> 0 Then
'    If grdItems.Columns("frenchaudioarrived").Text = "" Or UCase(Right(grdItems.Columns("frenchaudioarrived").Text, 2)) = "ER" Then temp = False
'End If
'If Val(grdItems.Columns("brazillianportaudioneeded").Text) <> 0 Then
'    If grdItems.Columns("brazillianportaudioarrived").Text = "" Or UCase(Right(grdItems.Columns("brazillianportaudioarrived").Text, 2)) = "ER" Then temp = False
'End If

CheckReadyForTX = temp

End Function

Private Sub optSortOrder_Click(Index As Integer)

If optSortOrder(0).Value = True Then
    m_strOrderby = " ORDER BY urgent, targetdate, duedate, uniqueID, title, componenttype DESC, complete;"
Else
    m_strOrderby = " ORDER BY uniqueID, title, componenttype DESC, complete;"
End If

cmdSearch.Value = True

End Sub

Private Sub Text3_Change()

End Sub

Private Sub txtSpecialProject_DropDown()

Dim l_strTemp As String
If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strTemp = txtSpecialProject.Text
    txtSpecialProject.Clear
    PopulateCombo "svenskspecialproject", txtSpecialProject, SearchSQLstr
    txtSpecialProject.Text = l_strTemp
End If
HighLite txtSpecialProject

End Sub
Private Sub cmdBulkUpdate_Click()

Dim l_strSQL As String, l_lngCount As Long
Dim l_strUpdateSet As String
Dim l_rstTestCount As ADODB.Recordset
Dim l_blnUpdatingCompleted As Boolean

If adoItems.Recordset.RecordCount = 0 Then Beep: Exit Sub

On Error GoTo BULK_UPDATE_ERROR
Set l_rstTestCount = ExecuteSQL("SELECT tracker_Svensk_itemID FROM tracker_Svensk_item " & m_strSearch & l_strDateSearch & msp_strSearch & ";", g_strExecuteError)
CheckForSQLError

l_lngCount = l_rstTestCount.RecordCount
l_rstTestCount.Close
Set l_rstTestCount = Nothing

'If there were any fields to update, then complete the SQL statement and execute it
If l_lngCount <= 0 Then
    MsgBox "No selection criteria in place. Please do a search.", vbCritical, "Error with Bulk Update"
    Exit Sub
End If

If MsgBox("Updating " & l_lngCount & " records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub

If l_lngCount > 10 Then
    If MsgBox("Updating more than 10 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 50 Then
    If MsgBox("Updating more than 50 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 100 Then
    If Not CheckAccess("/bulkupdatelots") Then Exit Sub
End If

l_blnUpdatingCompleted = False

'Check all the checkboxes, and for any that are true include their field in the update set
l_strUpdateSet = "mdate = '" & FormatSQLDate(Now) & "', muser = '" & g_strUserInitials & "'"

If chkBulkUpdate(0).Value <> 0 Then
    If Not IsNull(datUpMasterarrived.Value) Then
        l_strUpdateSet = l_strUpdateSet & ", masterarrived = '" & FormatSQLDate(datUpMasterarrived.Value) & "'"
    Else
        l_strUpdateSet = l_strUpdateSet & ", masterarrived = Null"
    End If
End If
If chkBulkUpdate(1).Value <> 0 Then
    If Not IsNull(datUpvalidation.Value) Then
        l_strUpdateSet = l_strUpdateSet & ", validation = '" & FormatSQLDate(datUpvalidation.Value) & "'"
    Else
        l_strUpdateSet = l_strUpdateSet & ", validation = Null"
    End If
End If
If chkBulkUpdate(2).Value <> 0 Then
    If Not IsNull(datUpfilemade.Value) Then
        l_strUpdateSet = l_strUpdateSet & ", filemade = '" & FormatSQLDate(datUpfilemade.Value) & "'"
    Else
        l_strUpdateSet = l_strUpdateSet & ", filemade = Null"
    End If
End If
If chkBulkUpdate(3).Value <> 0 Then
    If Not IsNull(datUpfilesent.Value) Then
        l_strUpdateSet = l_strUpdateSet & ", filesent = '" & FormatSQLDate(datUpfilesent.Value) & "'"
    Else
        l_strUpdateSet = l_strUpdateSet & ", filesent = Null"
    End If
End If
If chkBulkUpdate(4).Value <> 0 Then
    If Not IsNull(datUpproxymade.Value) Then
        l_strUpdateSet = l_strUpdateSet & ", proxymade = '" & FormatSQLDate(datUpproxymade.Value) & "'"
    Else
        l_strUpdateSet = l_strUpdateSet & ", proxymade = Null"
    End If
End If
If chkBulkUpdate(5).Value <> 0 Then
    If Not IsNull(datUpproxysent.Value) Then
        l_strUpdateSet = l_strUpdateSet & ", proxysent = '" & FormatSQLDate(datUpproxysent.Value) & "'"
    Else
        l_strUpdateSet = l_strUpdateSet & ", proxysent = Null"
    End If
End If
If chkBulkUpdate(6).Value <> 0 Then
    If Not IsNull(datUpxmlmade.Value) Then
        l_strUpdateSet = l_strUpdateSet & ", xmlmade = '" & FormatSQLDate(datUpxmlmade.Value) & "'"
    Else
        l_strUpdateSet = l_strUpdateSet & ", xmlmade = Null"
    End If
End If
If chkBulkUpdate(7).Value <> 0 Then
    If Not IsNull(datUpxmlsent.Value) Then
        l_strUpdateSet = l_strUpdateSet & ", xmlsent = '" & FormatSQLDate(datUpxmlsent.Value) & "'"
    Else
        l_strUpdateSet = l_strUpdateSet & ", xmlsent = Null"
    End If
End If
If chkBulkUpdate(9).Value <> 0 Then
    If Not IsNull(datUpcomplete.Value) Then
        l_strUpdateSet = l_strUpdateSet & ", complete = '" & FormatSQLDate(datUpcomplete.Value) & "'"
        l_strUpdateSet = l_strUpdateSet & ", readytobill = 1"
        l_blnUpdatingCompleted = True
    Else
        l_strUpdateSet = l_strUpdateSet & ", complete = Null"
        l_strUpdateSet = l_strUpdateSet & ", readytobill = 0"
        l_blnUpdatingCompleted = False
    End If
End If
If chkBulkUpdate(22).Value <> 0 Then
    If Not IsNull(datUpDueDate.Value) Then
        l_strUpdateSet = l_strUpdateSet & ", duedate = '" & FormatSQLDate(datUpDueDate.Value) & "'"
    Else
        l_strUpdateSet = l_strUpdateSet & ", duedate = Null"
    End If
End If

If chkBulkUpdate(10).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTAddClock = '" & chkDTAddClock.Value & "'"
If chkBulkUpdate(11).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTBarsAndTone = '" & chkDTBarsAndTone.Value & "'"
If chkBulkUpdate(12).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTTimecode = '" & chkDTTimecode.Value & "'"
If chkBulkUpdate(13).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTBlackAtEnd = '" & chkDTBlackAtEnd.Value & "'"
If chkBulkUpdate(14).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTAudioChannelConfig = '" & chkDTAudioChannelConfig.Value & "'"
If chkBulkUpdate(15).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTQuicktimeTagging = '" & chkDTQuicktimeTagging.Value & "'"
If chkBulkUpdate(16).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTStereoDownmix = '" & chkDTStereoDownmix.Value & "'"
If chkBulkUpdate(17).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTRemoveReleaseDates = '" & chkDTRemoveReleaseDates.Value & "'"
If chkBulkUpdate(18).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTAudioFramerateConvert = '" & chkDTAudioFramerateConvert.Value & "'"
If chkBulkUpdate(19).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTAudioSimpleConform = '" & chkDTAudioSimpleConform.Value & "'"


If chkBulkUpdate(20).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", SpecialProject = '" & QuoteSanitise(txtSpecialProject.Text) & "'"
If chkBulkUpdate(21).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", Operator = '" & QuoteSanitise(cmbOperator.Text) & "'"
If chkBulkUpdate(8).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", ProjectManager = '" & QuoteSanitise(cmbProjectManager.Text) & "'"
If chkBulkUpdate(23).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", AlphaDisplayCode = '" & QuoteSanitise(txtAlphaCode.Text) & "'"
If chkBulkUpdate(24).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", Programtype = '" & QuoteSanitise(cmbProgramType.Text) & "'"
If chkBulkUpdate(25).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", itemreference = '" & QuoteSanitise(txtReference.Text) & "'"
If chkBulkUpdate(26).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", title = '" & QuoteSanitise(cmbTitle.Text) & "'"
If chkBulkUpdate(27).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", subtitle = '" & QuoteSanitise(txtSubtitle.Text) & "'"
If chkBulkUpdate(28).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", rightsowner = '" & QuoteSanitise(cmbRightsOwner.Text) & "'"
If chkBulkUpdate(29).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", series = '" & QuoteSanitise(txtSeries.Text) & "'"
If chkBulkUpdate(30).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", episode = '" & QuoteSanitise(txtEpisode.Text) & "'"
If chkBulkUpdate(31).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", format = '" & QuoteSanitise(cmbFormat.Text) & "'"
If chkBulkUpdate(32).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", componenttype = '" & QuoteSanitise(cmbComponentType.Text) & "'"
If chkBulkUpdate(33).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", jobID = '" & Val(txtJobID.Text) & "'"
If chkBulkUpdate(34).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", MoreThanFiveFixes = " & chkMoreThan5Fixes.Value

' l_strSQL = "UPDATE tracker_svensk_item SET " & l_strUpdateSet & m_strSearch & l_strDateSearch & SearchSQLstr() & ";"
l_strSQL = "UPDATE tracker_svensk_item SET " & l_strUpdateSet & m_strSearch & msp_strSearch & ";"
Debug.Print l_strSQL

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

Dim l_rstWhoToEmail As ADODB.Recordset, l_strOurEmailContact As String, l_strOurContactName As String

'Send out Completed notices for items, if completion was set.
If l_blnUpdatingCompleted = True Then
    
    adoItems.Recordset.MoveFirst
    Do While Not adoItems.Recordset.EOF
    
        If adoItems.Recordset("iTunes") <> 0 Then
            
            If MsgBox("Should E-mail Notices be sent out?", vbYesNo, "Item required for iTunes is being marked complete") = vbYes Then
                
                Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & _
                "' AND trackermessageID = 44;", g_strExecuteError)
                CheckForSQLError
        
                If l_rstWhoToEmail.RecordCount > 0 Then
                    l_rstWhoToEmail.MoveFirst
                    
                    Do While Not l_rstWhoToEmail.EOF
                    
                        SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "Svensk item ready for iTunes", "", "Item: " & _
                        adoItems.Recordset("title") & ", Ep: " & adoItems.Recordset("episode") & ", Ep. Title: " & adoItems.Recordset("subtitle") & vbCrLf & _
                        "Component: " & adoItems.Recordset("componenttype") & ", Language: " & adoItems.Recordset("Language") & vbCrLf & _
                        "This item is now ready for iTunes use." & vbCrLf, True, "", "", "", True
        
                        l_rstWhoToEmail.MoveNext
                        
                    Loop
                    
                End If
                l_rstWhoToEmail.Close
                Set l_rstWhoToEmail = Nothing
            End If
        End If
        adoItems.Recordset.MoveNext
        
    Loop
            
End If

cmdSearch.Value = True

For l_lngCount = 0 To 33
    chkBulkUpdate(l_lngCount).Value = 0
Next
    
BULK_UPDATE_ERROR:

Exit Sub

End Sub

Private Sub cmdBulkUpdateSelected_Click()

Dim l_strSQL As String, l_lngCount As Long
Dim l_strUpdateSet As String
Dim bkmrk As Variant
Dim l_bulkDate As Date
Dim l_blnUpdatingCompleted As Boolean
Dim l_blnSendMessages As Boolean

l_bulkDate = Format(Year(Now) & "/" & Month(Now) & "/" & Day(Now) & " " & Hour(Now) & ":" & Minute(Now) & ":" & Second(Now))

If adoItems.Recordset.RecordCount = 0 Then Beep: Exit Sub

On Error GoTo BULK_UPDATE_ERROR

l_lngCount = grdItems.SelBookmarks.Count

If l_lngCount <= 0 Then
    MsgBox "No rows selected.", vbCritical, "Error with Bulk Update"
    Exit Sub
End If

'If MsgBox("Updating " & l_lngCount & " records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
If MsgBox("Updating " & l_lngCount & " records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
If l_lngCount > 10 Then
    If MsgBox("Updating more than 10 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 50 Then
    If MsgBox("Updating more than 50 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 100 Then
    If Not CheckAccess("/bulkupdatelots") Then Exit Sub
End If

''Check all the checkboxes, and for any that are true include their field in the update set   #" & Format(datTo.Value, "yyyy-mm-dd") & "#FormatSQLDate
l_strUpdateSet = "mdate = '" & FormatSQLDate(Now) & "', muser = '" & g_strUserInitials & "'"

If chkBulkUpdate(0).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", masterarrived = '" & FormatSQLDate(datUpMasterarrived.Value) & "'"
If chkBulkUpdate(1).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", validation = '" & FormatSQLDate(datUpvalidation.Value) & "'"
If chkBulkUpdate(2).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", filemade = '" & FormatSQLDate(datUpfilemade.Value) & "'"
If chkBulkUpdate(3).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", filesent = '" & FormatSQLDate(datUpfilesent.Value) & "'"
If chkBulkUpdate(4).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", proxymade = '" & FormatSQLDate(datUpproxymade.Value) & "'"
If chkBulkUpdate(5).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", proxysent = '" & FormatSQLDate(datUpproxysent.Value) & "'"
If chkBulkUpdate(6).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", xmlmade = '" & FormatSQLDate(datUpxmlmade.Value) & "'"
If chkBulkUpdate(7).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", xmlsent = '" & FormatSQLDate(datUpxmlsent.Value) & "'"
If chkBulkUpdate(9).Value <> 0 Then
    If Not IsNull(datUpcomplete.Value) Then
        l_strUpdateSet = l_strUpdateSet & ", complete = '" & FormatSQLDate(datUpcomplete.Value) & "'"
        l_strUpdateSet = l_strUpdateSet & ", readytobill = 1"
        l_blnUpdatingCompleted = True
    Else
        l_strUpdateSet = l_strUpdateSet & ", complete = Null"
        l_strUpdateSet = l_strUpdateSet & ", readytobill = 0"
        l_blnUpdatingCompleted = False
    End If
End If
        
If chkBulkUpdate(22).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", duedate = '" & FormatSQLDate(datUpDueDate.Value) & "'"

If chkBulkUpdate(10).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTAddClock = '" & chkDTAddClock.Value & "'"
If chkBulkUpdate(11).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTBarsAndTone = '" & chkDTBarsAndTone.Value & "'"
If chkBulkUpdate(12).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTTimecode = '" & chkDTTimecode.Value & "'"
If chkBulkUpdate(13).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTBlackAtEnd = '" & chkDTBlackAtEnd.Value & "'"
If chkBulkUpdate(14).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTAudioChannelConfig = '" & chkDTAudioChannelConfig.Value & "'"
If chkBulkUpdate(15).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTQuicktimeTagging = '" & chkDTQuicktimeTagging.Value & "'"
If chkBulkUpdate(16).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTStereoDownmix = '" & chkDTStereoDownmix.Value & "'"
If chkBulkUpdate(17).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTRemoveReleaseDates = '" & chkDTRemoveReleaseDates.Value & "'"
If chkBulkUpdate(18).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTAudioFramerateConvert = '" & chkDTAudioFramerateConvert.Value & "'"
If chkBulkUpdate(19).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", DTAudioSimpleConform = '" & chkDTAudioSimpleConform.Value & "'"

If chkBulkUpdate(20).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", SpecialProject = '" & QuoteSanitise(txtSpecialProject.Text) & "'"
If chkBulkUpdate(21).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", Operator = '" & QuoteSanitise(cmbOperator.Text) & "'"
If chkBulkUpdate(8).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", ProjectManager = '" & QuoteSanitise(cmbProjectManager.Text) & "'"
If chkBulkUpdate(23).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", AlphaDisplayCode = '" & QuoteSanitise(txtAlphaCode.Text) & "'"
If chkBulkUpdate(24).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", Programtype = '" & QuoteSanitise(cmbProgramType.Text) & "'"
If chkBulkUpdate(25).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", itemreference = '" & QuoteSanitise(txtReference.Text) & "'"
If chkBulkUpdate(26).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", title = '" & QuoteSanitise(cmbTitle.Text) & "'"
If chkBulkUpdate(27).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", subtitle = '" & QuoteSanitise(txtSubtitle.Text) & "'"
If chkBulkUpdate(28).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", rightsowner = '" & QuoteSanitise(cmbRightsOwner.Text) & "'"
If chkBulkUpdate(29).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", series = '" & QuoteSanitise(txtSeries.Text) & "'"
If chkBulkUpdate(30).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", episode = '" & QuoteSanitise(txtEpisode.Text) & "'"
If chkBulkUpdate(31).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", format = '" & QuoteSanitise(cmbFormat.Text) & "'"
If chkBulkUpdate(32).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", componenttype = '" & QuoteSanitise(cmbComponentType.Text) & "'"
If chkBulkUpdate(33).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", jobID = '" & Val(txtJobID.Text) & "'"
If chkBulkUpdate(34).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", MoreThanFiveFixes = " & chkMoreThan5Fixes.Value

Dim l_rstWhoToEmail As ADODB.Recordset, l_strOurEmailContact As String, l_strOurContactName As String

'If there were any fields to update, then complete the SQL statement and execute it
For l_lngCount = 0 To grdItems.SelBookmarks.Count - 1

    bkmrk = grdItems.SelBookmarks(l_lngCount)
    l_strSQL = "UPDATE tracker_svensk_item SET " & l_strUpdateSet & " WHERE tracker_Svensk_itemID = " & Val(grdItems.Columns("tracker_Svensk_itemID").CellText(bkmrk)) & ";"
    Debug.Print l_strSQL

    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError

    If l_blnUpdatingCompleted = True Then
    
        If grdItems.Columns("iTunes").CellText(bkmrk) <> 0 Then
            
            If MsgBox("Should E-mail Notices be sent out?", vbYesNo, "Item required for iTunes is being marked complete") = vbYes Then
            
                Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & _
                "' AND trackermessageID = 44;", g_strExecuteError)
                CheckForSQLError
        
                If l_rstWhoToEmail.RecordCount > 0 Then
                    l_rstWhoToEmail.MoveFirst
                    
                    While Not l_rstWhoToEmail.EOF
                    
                        SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "Svensk item ready for iTunes", "", "Item: " & _
                        grdItems.Columns("title").CellText(bkmrk) & ", Ep: " & grdItems.Columns("episode").CellText(bkmrk) & ", Ep. Title: " & grdItems.Columns("subtitle").CellText(bkmrk) & vbCrLf & _
                        "Component: " & grdItems.Columns("componenttype").CellText(bkmrk) & ", Language: " & grdItems.Columns("Language").CellText(bkmrk) & vbCrLf & _
                        "This item is now ready for iTunes use." & vbCrLf, True, "", "", "", True
        
                        l_rstWhoToEmail.MoveNext
                        
                    Wend
                    
                End If
                
                l_rstWhoToEmail.Close
                Set l_rstWhoToEmail = Nothing
            End If
        End If
    End If
Next

cmdSearch.Value = True

For l_lngCount = 0 To 33
    chkBulkUpdate(l_lngCount).Value = 0
Next
    
BULK_UPDATE_ERROR:

Exit Sub

End Sub


