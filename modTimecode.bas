Attribute VB_Name = "modTimecode"
Option Explicit

'Timecode constants for framerates

Public Const TC_UN = 0
Public Const TC_25 = 1
Public Const TC_29 = 2
Public Const TC_30 = 3
Public Const TC_24 = 4
Public Const TC_50 = 5
Public Const TC_59 = 6
Public Const TC_60 = 7
Public Const TC_23 = 8
Public Const TC_QT = 9

Public Function FrameBefore(lp_strTimecode As String, Optional ByVal Framerate As Integer) As String

' This routine will subtract 1 frame from a timecode value. Unless Framerate specified then PAL is assumed.
Dim OneFrame As String
If Framerate = TC_UN Then Framerate = TC_25

If Framerate = TC_29 Or Framerate = TC_59 Then

    If lp_strTimecode = "" Then
        lp_strTimecode = "00:00:00;00"
    End If
    OneFrame = "00:00:00;01"

Else
    
    If lp_strTimecode = "" Then
        lp_strTimecode = "00:00:00:00"
    End If
    OneFrame = "00:00:00:01"

End If

If Validate_Timecode(lp_strTimecode, Framerate) = False Then FrameBefore = "TC-Error": Exit Function

FrameBefore = Timecode_Subtract(lp_strTimecode, OneFrame, Framerate)

End Function

Public Function FrameAfter(lp_strTimecode As String, Optional ByVal Framerate As Integer) As String

' This routine will subtract 1 frame from a timecode value. Unless Framerate specified then PAL is assumed.

Dim OneFrame As String

If Framerate = TC_UN Then Framerate = TC_25

If Framerate = TC_29 Or Framerate = TC_59 Then

    If lp_strTimecode = "" Then
        lp_strTimecode = "00:00:00;00"
    End If
    OneFrame = "00:00:00;01"

Else
    
    If lp_strTimecode = "" Then
        lp_strTimecode = "00:00:00:00"
    End If
    OneFrame = "00:00:00:01"

End If

If Validate_Timecode(lp_strTimecode, Framerate) = False Then FrameAfter = "TC-Error": Exit Function

FrameAfter = Timecode_Add(lp_strTimecode, OneFrame, Framerate)

End Function

Public Function Validate_Timecode(lp_strTimecode As Variant, Optional ByVal Framerate As Integer, Optional silent As Boolean) As Boolean

'Validates that the first parameter is a timecode string - default PAL unless specified

If Framerate = TC_UN Then Framerate = TC_25

Validate_Timecode = True

If IsNull(lp_strTimecode) Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
If lp_strTimecode = "" Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function

If Len(lp_strTimecode) <> 11 Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
If Mid(lp_strTimecode, 3, 1) <> ":" Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
If Mid(lp_strTimecode, 6, 1) <> ":" Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
If Framerate = TC_29 Or Framerate = TC_59 Then
    If Mid(lp_strTimecode, 9, 1) <> ";" Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
Else
    If Mid(lp_strTimecode, 9, 1) <> ":" Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
End If

If Not IsNumeric(Mid(lp_strTimecode, 1, 1)) Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
If Not IsNumeric(Mid(lp_strTimecode, 2, 1)) Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
If Not IsNumeric(Mid(lp_strTimecode, 4, 1)) Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
If Not IsNumeric(Mid(lp_strTimecode, 5, 1)) Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
If Not IsNumeric(Mid(lp_strTimecode, 7, 1)) Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
If Not IsNumeric(Mid(lp_strTimecode, 8, 1)) Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
If Not IsNumeric(Mid(lp_strTimecode, 10, 1)) Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
If Not IsNumeric(Mid(lp_strTimecode, 11, 1)) Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function

If Val(Mid(lp_strTimecode, 1, 2)) > 23 Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
If Val(Mid(lp_strTimecode, 4, 2)) > 59 Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
If Val(Mid(lp_strTimecode, 7, 2)) > 59 Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function

Select Case Framerate
    Case TC_24
        If Val(Mid(lp_strTimecode, 10, 2)) > 23 Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
    
    Case TC_25
        If Val(Mid(lp_strTimecode, 10, 2)) > 24 Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
    
    Case TC_29
        If Val(Mid(lp_strTimecode, 10, 2)) > 29 Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
    
    Case TC_30
        If Val(Mid(lp_strTimecode, 10, 2)) > 29 Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
    
    Case TC_50
        If Val(Mid(lp_strTimecode, 10, 2)) > 49 Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
    
    Case TC_59
        If Val(Mid(lp_strTimecode, 10, 2)) > 59 Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function

    Case TC_60
        If Val(Mid(lp_strTimecode, 10, 2)) > 59 Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
End Select

If Framerate = TC_29 Then
    If (Int(Val(Mid(lp_strTimecode, 4, 2)) / 10) <> Val(Mid(lp_strTimecode, 4, 2)) / 10) And Mid(lp_strTimecode, 7, 2) = "00" Then
        If Mid(lp_strTimecode, 10, 2) = "00" Or Mid(lp_strTimecode, 10, 2) = "01" Then Validate_Timecode = False: TimecodeReportError lp_strTimecode, Framerate, silent: Exit Function
    End If
End If

If Framerate = TC_59 Then
    If (Int(Val(Mid(lp_strTimecode, 4, 2)) / 10) <> Val(Mid(lp_strTimecode, 4, 2)) / 10) And Mid(lp_strTimecode, 7, 2) = "00" Then
        If Mid(lp_strTimecode, 10, 2) = "00" Or Mid(lp_strTimecode, 10, 2) = "01" Or Mid(lp_strTimecode, 10, 2) = "02" Or Mid(lp_strTimecode, 10, 2) = "03" Then
            Validate_Timecode = False
            TimecodeReportError lp_strTimecode, Framerate, silent
            Exit Function
        End If
    End If
End If

End Function

Public Function Timecode_Add(lp_strTimecode1 As String, lp_strtimecode2 As String, Optional Framerate As Integer) As String

' This routine will add two timecodes together. Unless Framerate specified then PAL is assumed.

Dim FramesPerDay As Long, FramesPerHour As Long
Dim FrameNumber1 As Long, FrameNumber2 As Long, FrameNumberCalc As Long

If Framerate = TC_UN Then Framerate = TC_25

Select Case Framerate

    Case TC_24
        FramesPerHour = 86400
    Case TC_25
        FramesPerHour = 90000
    Case TC_29, TC_30
        FramesPerHour = 108000
    Case TC_50
        FramesPerHour = 180000
    Case TC_59, TC_60
        FramesPerHour = 216000
    Case Else
        FramesPerHour = 90000
End Select

FramesPerDay = FramesPerHour * 24

If lp_strTimecode1 = "" Then
    If Framerate = TC_29 Or Framerate = TC_59 Then
        lp_strTimecode1 = "00:00:00;00"
    Else
        lp_strTimecode1 = "00:00:00:00"
    End If
End If

If lp_strtimecode2 = "" Then
    If Framerate = TC_29 Or Framerate = TC_59 Then
        lp_strtimecode2 = "00:00:00;00"
    Else
        lp_strtimecode2 = "00:00:00:00"
    End If
End If

If Validate_Timecode(lp_strTimecode1, Framerate) = False Then Timecode_Add = "TC-Error": Exit Function
If Validate_Timecode(lp_strtimecode2, Framerate) = False Then Timecode_Add = "TC-Error": Exit Function

FrameNumber1 = FrameNumber(lp_strTimecode1, Framerate)
FrameNumber2 = FrameNumber(lp_strtimecode2, Framerate)

FrameNumberCalc = FrameNumber1 + FrameNumber2

If FrameNumberCalc > FramesPerDay Then FrameNumberCalc = FrameNumberCalc - FramesPerDay

Timecode_Add = Timecode_From_FrameNumber(FrameNumberCalc, Framerate)

End Function
Public Function Timecode_Subtract(lp_strTimecode1 As String, lp_strtimecode2 As String, Optional Framerate As Integer) As String

' This routine will subtracts Timecode2 from Timecode1. Unless Framerate specified then PAL is assumed.

Dim FramesPerDay As Long, FramesPerHour As Long
Dim FrameNumber1 As Long, FrameNumber2 As Long, FrameNumberCalc As Long

If Framerate = TC_UN Then Framerate = TC_25

Select Case Framerate

    Case TC_24
        FramesPerHour = 86400
    Case TC_25
        FramesPerHour = 90000
    Case TC_29, TC_30
        FramesPerHour = 108000
    Case TC_50
        FramesPerHour = 180000
    Case TC_59, TC_60
        FramesPerHour = 216000
    Case Else
        FramesPerHour = 90000
End Select

FramesPerDay = FramesPerHour * 24

If lp_strTimecode1 = "" Then
    If Framerate = TC_29 Or Framerate = TC_59 Then
        lp_strTimecode1 = "00:00:00;00"
    Else
        lp_strTimecode1 = "00:00:00:00"
    End If
End If

If lp_strtimecode2 = "" Then
    If Framerate = TC_29 Or Framerate = TC_59 Then
        lp_strtimecode2 = "00:00:00;00"
    Else
        lp_strtimecode2 = "00:00:00:00"
    End If
End If

If Validate_Timecode(lp_strTimecode1, Framerate) = False Then Timecode_Subtract = "TC-Error": Exit Function
If Validate_Timecode(lp_strtimecode2, Framerate) = False Then Timecode_Subtract = "TC-Error": Exit Function

FrameNumber1 = FrameNumber(lp_strTimecode1, Framerate)
FrameNumber2 = FrameNumber(lp_strtimecode2, Framerate)

FrameNumberCalc = FrameNumber1 - FrameNumber2

If FrameNumberCalc < 0 Then FrameNumberCalc = FrameNumberCalc + FramesPerDay

Timecode_Subtract = Timecode_From_FrameNumber(FrameNumberCalc, Framerate)

End Function

Public Function Make_Timecode_String(l_lngCalcHours As Long, l_lngCalcMins As Long, l_lngCalcSecs As Long, l_lngCalcFrames As Long, Framerate As Integer) As String

If l_lngCalcHours <> 0 Then
    If l_lngCalcHours < 10 Then Make_Timecode_String = "0" & l_lngCalcHours & ":" Else Make_Timecode_String = l_lngCalcHours & ":"
Else
    Make_Timecode_String = "00:"
End If

If l_lngCalcMins <> 0 Then
    If l_lngCalcMins < 10 Then
        Make_Timecode_String = Make_Timecode_String & "0" & l_lngCalcMins & ":"
    Else
        Make_Timecode_String = Make_Timecode_String & l_lngCalcMins & ":"
    End If
Else
    Make_Timecode_String = Make_Timecode_String & "00:"
End If
If l_lngCalcSecs <> 0 Then
    If l_lngCalcSecs < 10 Then
        Make_Timecode_String = Make_Timecode_String & "0" & l_lngCalcSecs
    Else
        Make_Timecode_String = Make_Timecode_String & l_lngCalcSecs
    End If
Else
    Make_Timecode_String = Make_Timecode_String & "00"
End If

If Framerate = TC_29 Or Framerate = TC_59 Then
    Make_Timecode_String = Make_Timecode_String & ";"
Else
    Make_Timecode_String = Make_Timecode_String & ":"
End If

If l_lngCalcFrames = 0 Then
    Make_Timecode_String = Make_Timecode_String & "00"
ElseIf l_lngCalcFrames < 10 Then
    Make_Timecode_String = Make_Timecode_String & "0" & l_lngCalcFrames
Else
    Make_Timecode_String = Make_Timecode_String & l_lngCalcFrames
End If

End Function

Public Function Duration_From_Timecode(lp_strTimecode As String, Optional ByVal Framerate As Integer) As String

If Framerate = TC_UN Then Framerate = TC_25

If lp_strTimecode = "" Then Duration_From_Timecode = "": Exit Function

If Validate_Timecode(lp_strTimecode, Framerate) = False Then Duration_From_Timecode = "TC-Error": Exit Function

Dim l_lngHours As Long, l_lngMins As Long, l_lngSecs As Long

l_lngHours = Mid(lp_strTimecode, 1, 2)
l_lngMins = Mid(lp_strTimecode, 4, 2)
l_lngSecs = Mid(lp_strTimecode, 7, 2)

Select Case Framerate

    Case TC_24
        If Mid(lp_strTimecode, 10, 2) > 11 Then l_lngSecs = l_lngSecs + 1
    Case TC_25
        If Mid(lp_strTimecode, 10, 2) > 12 Then l_lngSecs = l_lngSecs + 1
    Case TC_29, TC_30
        If Mid(lp_strTimecode, 10, 2) > 15 Then l_lngSecs = l_lngSecs + 1
    Case TC_50
        If Mid(lp_strTimecode, 10, 2) > 25 Then l_lngSecs = l_lngSecs + 1
    Case TC_59, TC_60
        If Mid(lp_strTimecode, 10, 2) > 30 Then l_lngSecs = l_lngSecs + 1

End Select

If l_lngSecs = 60 Then l_lngSecs = 0: l_lngMins = l_lngMins + 1
If l_lngMins = 60 Then l_lngMins = 0: l_lngHours = l_lngHours + 1
If l_lngHours = 24 Then l_lngHours = 0

If l_lngHours <> 0 Then
    If l_lngHours < 10 Then Duration_From_Timecode = "0" & l_lngHours & ":" Else Duration_From_Timecode = l_lngHours & ":"
Else
    Duration_From_Timecode = "00:"
End If

If l_lngMins <> 0 Then
    If l_lngMins < 10 Then
        Duration_From_Timecode = Duration_From_Timecode & "0" & l_lngMins & ":"
    Else
        Duration_From_Timecode = Duration_From_Timecode & l_lngMins & ":"
    End If
Else
    Duration_From_Timecode = Duration_From_Timecode & "00:"
End If

If l_lngSecs <> 0 Then
    If l_lngSecs < 10 Then
        Duration_From_Timecode = Duration_From_Timecode & "0" & l_lngSecs
    Else
        Duration_From_Timecode = Duration_From_Timecode & l_lngSecs
    End If
Else
    Duration_From_Timecode = Duration_From_Timecode & "00"
End If

End Function

Public Sub Timecode_Check_Control(Control As Control, KeyAscii As Integer, Framerate As Integer)

If KeyAscii = 46 Then KeyAscii = 58
If Control.SelStart = 11 And KeyAscii <> 8 Then
    KeyAscii = 0
    Exit Sub
End If

If Control.SelLength > 1 And KeyAscii = 8 Then
    KeyAscii = 0
    Exit Sub
End If

If Control.SelStart = 0 And KeyAscii = 8 Then Exit Sub

Select Case Control.SelStart 'Position Control of formating of Timecode.

Case 2 And KeyAscii <> 8, 5 And KeyAscii <> 8, 8 And KeyAscii <> 8
        
    Control.SelStart = Control.SelStart + 1
    If KeyAscii = Asc(":") Or (Control.SelStart = 8 And KeyAscii = Asc(";")) Then
        KeyAscii = 0
    Else
        Control.SelLength = 1
    End If

Case Else
    If KeyAscii = 58 Then KeyAscii = 0: Exit Sub
    
    If KeyAscii = 8 And Control.SelStart <> 0 Then
        Select Case Control.SelStart
            Case 3, 6, 9
                Control.SelStart = Control.SelStart - 1
                KeyAscii = 0
                Exit Sub
            End Select
            Control.SelStart = Control.SelStart - 1
            Control.SelLength = 1
            Control.SelText = "0"
            Control.SelStart = Control.SelStart - 1
            Control.SelLength = 0
            KeyAscii = 0
            Exit Sub
    End If
    Control.SelLength = 1
End Select

Select Case KeyAscii
    Case 48 To 57   'VALIDATE THE TIME CODE.
        Select Case Control.SelStart
            Case 0  ' Check 1st digit of Hours is not > 2
                If KeyAscii > 50 Then Beep: KeyAscii = 0: Exit Sub

            Case 1  ' Check 2nd digit of Hours is not > 3 if 20 something hours.
                If Val(Mid$(Control.Text, 1, 1)) = 2 Then
                    If KeyAscii > 51 Then Beep: KeyAscii = 0: Exit Sub
                End If
            Case 3  ' Check Minutes
                If KeyAscii > 53 Then Beep: KeyAscii = 0: Exit Sub
            Case 6  ' Check Seconds
                If KeyAscii > 53 Then Beep: KeyAscii = 0: Exit Sub
            Case 9 ' Check Frames tens
                Select Case Framerate
                    Case TC_24, TC_25, TC_29, TC_30
                        If KeyAscii > 50 Then Beep: KeyAscii = 0: Exit Sub
                    Case TC_50
                        If KeyAscii > 52 Then Beep: KeyAscii = 0: Exit Sub
                    Case TC_59, TC_60
                        If KeyAscii > 53 Then Beep: KeyAscii = 0: Exit Sub
                End Select
            Case 10 ' Check frames units
                Select Case Framerate
                    Case TC_25
                        If Val(Mid$(Control.Text, 10, 1)) = 2 Then
                            If KeyAscii > 52 Then Beep: KeyAscii = 0: Exit Sub
                        End If
                    Case TC_24
                        If Val(Mid$(Control.Text, 10, 1)) = 2 Then
                            If KeyAscii > 51 Then Beep: KeyAscii = 0: Exit Sub
                        End If
                End Select

        End Select
    
    Case 58, 8
    Case Else
        KeyAscii = 0
        Exit Sub
End Select

End Sub

Sub Timecode_Check_Grid(Control As Control, KeyAscii As Integer, Framerate As Integer)

If KeyAscii = 46 Then KeyAscii = 58
If Control.ActiveCell.SelStart = 11 And KeyAscii <> 8 Then
    KeyAscii = 0
    Exit Sub
End If

If Control.ActiveCell.SelLength > 1 And KeyAscii = 8 Then
    KeyAscii = 0
    Exit Sub
End If

If Control.ActiveCell.SelStart = 0 And KeyAscii = 8 Then Exit Sub

Select Case Control.ActiveCell.SelStart 'Position Control of formating of Timecode.

    Case 2 And KeyAscii <> 8, 5 And KeyAscii <> 8, (8 And KeyAscii <> 8 And Framerate <> TC_QT)
            
        Control.ActiveCell.SelStart = Control.ActiveCell.SelStart + 1
        If KeyAscii = Asc(":") Or KeyAscii = Asc(";") Then
            KeyAscii = 0
        Else
            Control.ActiveCell.SelLength = 1
        End If
    
    Case Else
        If KeyAscii = 58 Or KeyAscii = 59 Then KeyAscii = 0: Exit Sub
        
        If KeyAscii = 8 And Control.ActiveCell.SelStart <> 0 Then
            Select Case Control.ActiveCell.SelStart
                Case 3, 6, 9
                    If Framerate <> TC_QT Then
                        Control.ActiveCell.SelStart = Control.ActiveCell.SelStart - 1
                        KeyAscii = 0
                        Exit Sub
                    End If
                End Select
                Control.ActiveCell.SelStart = Control.ActiveCell.SelStart - 1
                Control.ActiveCell.SelLength = 1
                Control.ActiveCell.SelText = "0"
                Control.ActiveCell.SelStart = Control.ActiveCell.SelStart - 1
                Control.ActiveCell.SelLength = 0
                KeyAscii = 0
                Exit Sub
        End If
        Control.ActiveCell.SelLength = 1
End Select

Select Case KeyAscii
    Case 48 To 57   'VALIDATE THE TIME CODE.
        Select Case Control.ActiveCell.SelStart
            Case 0  ' Check 1st digit of Hours is not > 2
                If KeyAscii > 50 Then Beep: KeyAscii = 0: Exit Sub

            Case 1  ' Check 2nd digit of Hours is not > 3 if 20 something hours.
                If Val(Mid$(Control.ActiveCell.Text, 1, 1)) = 2 Then
                    If KeyAscii > 51 Then Beep: KeyAscii = 0: Exit Sub
                End If
            Case 3  ' Check Minutes
                If KeyAscii > 53 Then Beep: KeyAscii = 0: Exit Sub
            Case 6  ' Check Seconds
                If KeyAscii > 53 Then Beep: KeyAscii = 0: Exit Sub
            Case 8  ' In TC_QT there are no frames, and this would be the colon for frames
                If Framerate = TC_QT Then Beep: KeyAscii = 0: Exit Sub
            Case 9 ' Check Frames tens
                Select Case Framerate
                    Case TC_24, TC_25, TC_29, TC_30
                        If KeyAscii > 50 Then Beep: KeyAscii = 0: Exit Sub
                    Case TC_50
                        If KeyAscii > 52 Then Beep: KeyAscii = 0: Exit Sub
                    Case TC_59, TC_60
                        If KeyAscii > 53 Then Beep: KeyAscii = 0: Exit Sub
                    Case TC_QT
                        KeyAscii = 0: Exit Sub
                End Select
            Case 10 ' Check frames units
                Select Case Framerate
                    Case TC_25
                        If Val(Mid$(Control.ActiveCell.Text, 10, 1)) = 2 Then
                            If KeyAscii > 52 Then Beep: KeyAscii = 0: Exit Sub
                        End If
                    Case TC_24
                        If Val(Mid$(Control.ActiveCell.Text, 10, 1)) = 2 Then
                            If KeyAscii > 51 Then Beep: KeyAscii = 0: Exit Sub
                        End If
                    Case TC_QT
                        KeyAscii = 0: Exit Sub
                End Select

        End Select
    
    Case 58, 8
    Case Else
        KeyAscii = 0
        Exit Sub
End Select

End Sub

Sub ChangeTimecodeDF(lp_lngLibraryID As Long, lp_strCharacter As String, Framerate As Integer, FrameAccurate As Boolean)

Dim l_rstEvent As ADODB.Recordset
Dim l_strSQL As String, l_strCode As String, Frames As Integer
Dim l_strCalcDuration As String

l_strSQL = "SELECT timecodestart, timecodestop, fd_length, break1, break1elapsed, break2, break2elapsed, break3, break3elapsed, break4, break4elapsed, break5, break5elapsed, endcredits, endcreditselapsed FROM tapeevents WHERE libraryID = " & lp_lngLibraryID & ";"
Set l_rstEvent = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If Not l_rstEvent.EOF Then
    l_rstEvent.MoveFirst
    Do While Not l_rstEvent.EOF
        l_strCode = Trim(" " & l_rstEvent("timecodestart"))
        If l_strCode <> "" And Len(l_strCode) = 11 Then
            If Mid(l_strCode, 9, 1) <> lp_strCharacter Then
                Mid(l_strCode, 9, 1) = lp_strCharacter
                Do While Validate_Timecode(l_strCode, Framerate, True) = False
                    Frames = Mid(l_strCode, 10, 2)
                    Mid(l_strCode, 10, 2) = Format(Frames + 1, "00")
                Loop
                l_rstEvent("timecodestart") = l_strCode
            End If
        End If
        l_strCode = Trim(" " & l_rstEvent("timecodestop"))
        If l_strCode <> "" And Len(l_strCode) = 11 Then
            If Mid(l_strCode, 9, 1) <> lp_strCharacter Then
                Mid(l_strCode, 9, 1) = lp_strCharacter
                Do While Validate_Timecode(l_strCode, Framerate, True) = False
                    Frames = Mid(l_strCode, 10, 2)
                    Mid(l_strCode, 10, 2) = Format(Frames + 1, "00")
                Loop
                l_rstEvent("timecodestop") = l_strCode
            End If
        End If
        l_strCode = Trim(" " & l_rstEvent("break1"))
        If l_strCode <> "" And Len(l_strCode) = 11 Then
            If Mid(l_strCode, 9, 1) <> lp_strCharacter Then
                Mid(l_strCode, 9, 1) = lp_strCharacter
                Do While Validate_Timecode(l_strCode, Framerate, True) = False
                    Frames = Mid(l_strCode, 10, 2)
                    Mid(l_strCode, 10, 2) = Format(Frames + 1, "00")
                Loop
                l_rstEvent("break1") = l_strCode
                l_rstEvent("break1elapsed") = Timecode_Subtract(l_strCode, l_rstEvent("timecodestart"), Framerate)
            End If
        End If
        l_strCode = Trim(" " & l_rstEvent("break2"))
        If l_strCode <> "" And Len(l_strCode) = 11 Then
            If Mid(l_strCode, 9, 1) <> lp_strCharacter Then
                Mid(l_strCode, 9, 1) = lp_strCharacter
                Do While Validate_Timecode(l_strCode, Framerate, True) = False
                    Frames = Mid(l_strCode, 10, 2)
                    Mid(l_strCode, 10, 2) = Format(Frames + 1, "00")
                Loop
                l_rstEvent("break2") = l_strCode
                l_rstEvent("break2elapsed") = Timecode_Subtract(l_strCode, l_rstEvent("timecodestart"), Framerate)
            End If
        End If
        l_strCode = Trim(" " & l_rstEvent("break3"))
        If l_strCode <> "" And Len(l_strCode) = 11 Then
            If Mid(l_strCode, 9, 1) <> lp_strCharacter Then
                Mid(l_strCode, 9, 1) = lp_strCharacter
                Do While Validate_Timecode(l_strCode, Framerate, True) = False
                    Frames = Mid(l_strCode, 10, 2)
                    Mid(l_strCode, 10, 2) = Format(Frames + 1, "00")
                Loop
                l_rstEvent("break3") = l_strCode
                l_rstEvent("break3elapsed") = Timecode_Subtract(l_strCode, l_rstEvent("timecodestart"), Framerate)
            End If
        End If
        l_strCode = Trim(" " & l_rstEvent("break4"))
        If l_strCode <> "" And Len(l_strCode) = 11 Then
            If Mid(l_strCode, 9, 1) <> lp_strCharacter Then
                Mid(l_strCode, 9, 1) = lp_strCharacter
                Do While Validate_Timecode(l_strCode, Framerate, True) = False
                    Frames = Mid(l_strCode, 10, 2)
                    Mid(l_strCode, 10, 2) = Format(Frames + 1, "00")
                Loop
                l_rstEvent("break4") = l_strCode
                l_rstEvent("break4elapsed") = Timecode_Subtract(l_strCode, l_rstEvent("timecodestart"), Framerate)
            End If
        End If
        l_strCode = Trim(" " & l_rstEvent("break5"))
        If l_strCode <> "" And Len(l_strCode) = 11 Then
            If Mid(l_strCode, 9, 1) <> lp_strCharacter Then
                Mid(l_strCode, 9, 1) = lp_strCharacter
                Do While Validate_Timecode(l_strCode, Framerate, True) = False
                    Frames = Mid(l_strCode, 10, 2)
                    Mid(l_strCode, 10, 2) = Format(Frames + 1, "00")
                Loop
                l_rstEvent("break5") = l_strCode
                l_rstEvent("break5elapsed") = Timecode_Subtract(l_strCode, l_rstEvent("timecodestart"), Framerate)
            End If
        End If
        l_strCode = Trim(" " & l_rstEvent("endcredits"))
        If l_strCode <> "" And Len(l_strCode) = 11 Then
            If Mid(l_strCode, 9, 1) <> lp_strCharacter Then
                Mid(l_strCode, 9, 1) = lp_strCharacter
                Do While Validate_Timecode(l_strCode, Framerate, True) = False
                    Frames = Mid(l_strCode, 10, 2)
                    Mid(l_strCode, 10, 2) = Format(Frames + 1, "00")
                Loop
                l_rstEvent("endcredits") = l_strCode
                l_rstEvent("endcreditselapsed") = Timecode_Subtract(l_strCode, l_rstEvent("timecodestart"), Framerate)
            End If
        End If
        If Trim(" " & l_rstEvent("fd_length")) <> "" Then
            l_strCalcDuration = Timecode_Subtract(Trim(" " & l_rstEvent("timecodestop")), l_rstEvent("timecodestart"), Framerate)
            If FrameAccurate = False Then l_strCalcDuration = Duration_From_Timecode(l_strCalcDuration, Framerate)
            l_rstEvent("fd_length") = l_strCalcDuration
        End If
        l_rstEvent.Update
        l_rstEvent.MoveNext
    Loop
End If

l_rstEvent.Close
Set l_rstEvent = Nothing

End Sub

Sub TimecodeReportError(lp_strTimecode As Variant, Framerate As Integer, silent As Boolean)

Dim msgString As String

If Not silent Then
    
    msgString = "Timecode '" & lp_strTimecode & "' is not a valid timecode in the "
    
    Select Case Framerate
        Case TC_25, TC_50
            msgString = msgString & "EBU"
        Case TC_29, TC_59
            msgString = msgString & "SMPTE-DF"
        Case TC_30, TC_60
            msgString = msgString & "SMPTE-NDF"
        Case TC_24
            msgString = msgString & "24FPS"
    End Select
    
    msgString = msgString & " standard."
    
    MsgBox msgString, vbCritical, "Error Validating a Timecode Value"

End If

End Sub

Public Function FrameNumber(lp_strTimecode As Variant, Framerate As Integer) As Long

Dim l_lngHours As Long, l_lngMins As Long, l_lngSecs As Long, l_lngFrames As Long, totalMinutes  As Long

If Validate_Timecode(lp_strTimecode, Framerate, True) Then

    l_lngHours = Mid(lp_strTimecode, 1, 2)
    l_lngMins = Mid(lp_strTimecode, 4, 2)
    l_lngSecs = Mid(lp_strTimecode, 7, 2)
    l_lngFrames = Mid(lp_strTimecode, 10, 2)

    Select Case Framerate
        
        Case TC_25
        
            FrameNumber = (l_lngHours * 60 * 60 * 25) + (l_lngMins * 60 * 25) + (l_lngSecs * 25) + l_lngFrames
        
        Case TC_50
        
            FrameNumber = (l_lngHours * 60 * 60 * 50) + (l_lngMins * 60 * 50) + (l_lngSecs * 50) + l_lngFrames
        
        Case TC_60
        
            FrameNumber = (l_lngHours * 60 * 60 * 60) + (l_lngMins * 60 * 60) + (l_lngSecs * 60) + l_lngFrames
        
        Case TC_29
        
            totalMinutes = 60 * l_lngHours + l_lngMins
            FrameNumber = 108000 * l_lngHours + 1800 * l_lngMins + 30 * l_lngSecs + l_lngFrames - 2 * (totalMinutes - totalMinutes \ 10)
        
        Case TC_59
        
            totalMinutes = 60 * l_lngHours + l_lngMins
            FrameNumber = 216000 * l_lngHours + 3600 * l_lngMins + 60 * l_lngSecs + l_lngFrames - 4 * (totalMinutes - totalMinutes \ 10)
        
        Case TC_30
        
            FrameNumber = (l_lngHours * 60 * 60 * 30) + (l_lngMins * 60 * 30) + (l_lngSecs * 30) + l_lngFrames
        
        Case TC_24
        
            FrameNumber = (l_lngHours * 60 * 60 * 24) + (l_lngMins * 60 * 24) + (l_lngSecs * 24) + l_lngFrames
        
    End Select

Else

    FrameNumber = 0

End If

End Function

Public Function Timecode_From_FrameNumber(lp_lngFrameNumber As Long, Framerate As Integer) As String

Dim l_lngHours As Long, l_lngMins As Long, l_lngSecs As Long, l_lngFrames As Long, totalMinutes  As Long, d As Long, M As Long

Select Case Framerate

    Case TC_25, TC_UN
    
        l_lngFrames = lp_lngFrameNumber Mod 25
        l_lngSecs = (lp_lngFrameNumber \ 25) Mod 60
        l_lngMins = ((lp_lngFrameNumber \ 25) \ 60) Mod 60
        l_lngHours = (((lp_lngFrameNumber \ 25) \ 60) \ 60) Mod 24
    
    Case TC_29
    
        d = lp_lngFrameNumber \ 17982
        M = lp_lngFrameNumber Mod 17982
        lp_lngFrameNumber = lp_lngFrameNumber + 18 * d + 2 * ((M - 2) \ 1798)
        
        l_lngFrames = lp_lngFrameNumber Mod 30
        l_lngSecs = (lp_lngFrameNumber \ 30) Mod 60
        l_lngMins = ((lp_lngFrameNumber \ 30) \ 60) Mod 60
        l_lngHours = (((lp_lngFrameNumber \ 30) \ 60) \ 60) Mod 24
        
    Case TC_59
    
        d = lp_lngFrameNumber \ 35964
        M = lp_lngFrameNumber Mod 35964
        lp_lngFrameNumber = lp_lngFrameNumber + 36 * d + 4 * ((M - 4) \ 3596)
        
        l_lngFrames = lp_lngFrameNumber Mod 60
        l_lngSecs = (lp_lngFrameNumber \ 60) Mod 60
        l_lngMins = ((lp_lngFrameNumber \ 60) \ 60) Mod 60
        l_lngHours = (((lp_lngFrameNumber \ 60) \ 60) \ 60) Mod 24
        
    Case TC_30
    
        l_lngFrames = lp_lngFrameNumber Mod 30
        l_lngSecs = (lp_lngFrameNumber \ 30) Mod 60
        l_lngMins = ((lp_lngFrameNumber \ 30) \ 60) Mod 60
        l_lngHours = (((lp_lngFrameNumber \ 30) \ 60) \ 60) Mod 24
    
    Case TC_50
    
        l_lngFrames = lp_lngFrameNumber Mod 50
        l_lngSecs = (lp_lngFrameNumber \ 50) Mod 60
        l_lngMins = ((lp_lngFrameNumber \ 50) \ 60) Mod 60
        l_lngHours = (((lp_lngFrameNumber \ 50) \ 60) \ 60) Mod 24
    
    Case TC_60
    
        l_lngFrames = lp_lngFrameNumber Mod 60
        l_lngSecs = (lp_lngFrameNumber \ 60) Mod 60
        l_lngMins = ((lp_lngFrameNumber \ 60) \ 60) Mod 60
        l_lngHours = (((lp_lngFrameNumber \ 60) \ 60) \ 60) Mod 24
    
    Case TC_24

        l_lngFrames = lp_lngFrameNumber Mod 24
        l_lngSecs = (lp_lngFrameNumber \ 24) Mod 60
        l_lngMins = ((lp_lngFrameNumber \ 24) \ 60) Mod 60
        l_lngHours = (((lp_lngFrameNumber \ 24) \ 60) \ 60) Mod 24
        
End Select

Timecode_From_FrameNumber = Make_Timecode_String(l_lngHours, l_lngMins, l_lngSecs, l_lngFrames, Framerate)

End Function

Function Translate_NDF_To_DF(lp_strTimecode As String) As String

Dim l_lngHours As Long, l_lngMins As Long, l_lngSecs As Long, l_lngFrames As Long

If Len(lp_strTimecode) = 11 Then
    l_lngHours = Mid(lp_strTimecode, 1, 2)
    l_lngMins = Mid(lp_strTimecode, 4, 2)
    l_lngSecs = Mid(lp_strTimecode, 7, 2)
    l_lngFrames = Mid(lp_strTimecode, 10, 2)
    
    Translate_NDF_To_DF = Make_Timecode_String(l_lngHours, l_lngMins, l_lngSecs, l_lngFrames, TC_29)
Else
    Translate_NDF_To_DF = lp_strTimecode
End If

End Function

Function Seconds_From_Timecode(lp_strTimecode As String, Framerate As Integer) As String

If Framerate = TC_UN Then Framerate = TC_25

If lp_strTimecode = "" Then Seconds_From_Timecode = "": Exit Function

If Validate_Timecode(lp_strTimecode, Framerate) = False Then Seconds_From_Timecode = "TC-Error": Exit Function

Dim l_lngHours As Long, l_lngMins As Long, l_lngSecs As Long, l_lngFrames As Long, l_dblSeconds As Double

l_lngHours = Mid(lp_strTimecode, 1, 2)
l_lngMins = Mid(lp_strTimecode, 4, 2)
l_lngSecs = Mid(lp_strTimecode, 7, 2)
l_lngFrames = Mid(lp_strTimecode, 10, 2)

Select Case Framerate

    Case TC_24
        l_dblSeconds = l_lngFrames / 24
    Case TC_25
        l_dblSeconds = l_lngFrames / 25
    Case TC_29, TC_30
        l_dblSeconds = l_lngFrames / 30
    Case TC_50
        l_dblSeconds = l_lngFrames / 50
    Case TC_59, TC_60
        l_dblSeconds = l_lngFrames / 60

End Select

l_dblSeconds = l_dblSeconds + l_lngSecs
l_dblSeconds = l_dblSeconds + l_lngMins * 60
l_dblSeconds = l_dblSeconds + l_lngHours * 60 * 60

Seconds_From_Timecode = l_dblSeconds

End Function
