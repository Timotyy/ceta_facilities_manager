VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmJobListing 
   Caption         =   "Job Listing"
   ClientHeight    =   10605
   ClientLeft      =   3060
   ClientTop       =   3210
   ClientWidth     =   17805
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmJobListing.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   10605
   ScaleWidth      =   17805
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtSeriesIDSearch 
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   8580
      TabIndex        =   86
      ToolTipText     =   "The job title to search for"
      Top             =   360
      Width           =   2895
   End
   Begin VB.CommandButton cmdHoldAllJobs 
      Caption         =   "Hold All Jobs"
      Height          =   315
      Left            =   5760
      TabIndex        =   83
      ToolTipText     =   "Refresh details"
      Top             =   1080
      Width           =   1155
   End
   Begin VB.TextBox txtSubtitleSearch 
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   8580
      TabIndex        =   80
      ToolTipText     =   "The job title to search for"
      Top             =   960
      Width           =   2895
   End
   Begin VB.CheckBox chkHideDemo 
      Alignment       =   1  'Right Justify
      Caption         =   "Hide Demo"
      Height          =   255
      Left            =   14100
      TabIndex        =   79
      Top             =   960
      Value           =   1  'Checked
      Width           =   1515
   End
   Begin VB.ComboBox cmbDirection 
      Height          =   315
      ItemData        =   "frmJobListing.frx":08CA
      Left            =   12060
      List            =   "frmJobListing.frx":08D4
      Style           =   2  'Dropdown List
      TabIndex        =   78
      ToolTipText     =   "Which data field to search against"
      Top             =   9480
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ComboBox cmbOrderBy 
      Height          =   315
      Left            =   9480
      TabIndex        =   77
      Text            =   "cmbOrderBy"
      ToolTipText     =   "Which data field to search against"
      Top             =   9480
      Visible         =   0   'False
      Width           =   2235
   End
   Begin VB.Frame Frame5 
      Caption         =   "Start Date..."
      Height          =   1095
      Left            =   3420
      TabIndex        =   71
      Top             =   60
      Width           =   2235
      Begin MSComCtl2.DTPicker datStartDateFrom 
         Height          =   315
         Left            =   600
         TabIndex        =   72
         Tag             =   "NOCHECK"
         ToolTipText     =   "search start date (within Range)"
         Top             =   240
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   166920193
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datStartDateTo 
         Height          =   315
         Left            =   600
         TabIndex        =   73
         Tag             =   "NOCHECK"
         ToolTipText     =   "search start date (within Range)"
         Top             =   660
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   166920193
         CurrentDate     =   37870
      End
      Begin VB.Label lblCaption 
         Caption         =   "From"
         Height          =   255
         Index           =   13
         Left            =   120
         TabIndex        =   75
         Top             =   240
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Height          =   255
         Index           =   19
         Left            =   120
         TabIndex        =   74
         Top             =   600
         Width           =   1035
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "ID Range"
      Height          =   1095
      Left            =   60
      TabIndex        =   56
      Top             =   60
      Width           =   3255
      Begin VB.TextBox txtInvoiceNumberFrom 
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   3360
         TabIndex        =   66
         ToolTipText     =   "The unique identifier for this job (Start of range)"
         Top             =   420
         Width           =   1035
      End
      Begin VB.TextBox txtInvoiceNumberTo 
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   3360
         TabIndex        =   65
         ToolTipText     =   "The unique identifier for this job (End of range)"
         Top             =   720
         Width           =   1035
      End
      Begin VB.TextBox txtProjectIDTo 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   2040
         TabIndex        =   60
         ToolTipText     =   "The unique identifier for this job (End of range)"
         Top             =   720
         Width           =   1035
      End
      Begin VB.TextBox txtProjectIDFrom 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   2040
         TabIndex        =   59
         ToolTipText     =   "The unique identifier for this job (Start of range)"
         Top             =   420
         Width           =   1035
      End
      Begin VB.TextBox txtJobIDTo 
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   780
         TabIndex        =   58
         ToolTipText     =   "The unique identifier for this job (End of range)"
         Top             =   720
         Width           =   1035
      End
      Begin VB.TextBox txtJobIDFrom 
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   780
         TabIndex        =   57
         ToolTipText     =   "The unique identifier for this job (Start of range)"
         Top             =   420
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "Invoice #"
         Height          =   255
         Index           =   14
         Left            =   3360
         TabIndex        =   67
         Top             =   180
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "Project #"
         Height          =   255
         Index           =   26
         Left            =   2040
         TabIndex        =   64
         Top             =   180
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "From"
         Height          =   255
         Index           =   18
         Left            =   120
         TabIndex        =   63
         Top             =   300
         Width           =   375
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "Job ID"
         Height          =   255
         Index           =   17
         Left            =   780
         TabIndex        =   62
         Top             =   180
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Height          =   255
         Index           =   15
         Left            =   120
         TabIndex        =   61
         Top             =   660
         Width           =   255
      End
   End
   Begin VB.CheckBox chkAutoRefresh 
      Alignment       =   1  'Right Justify
      Caption         =   "Auto-refresh Dub / Tech"
      Height          =   255
      Left            =   11640
      TabIndex        =   55
      Top             =   960
      Value           =   1  'Checked
      Width           =   2415
   End
   Begin VB.Timer timAutoRefresh 
      Interval        =   1000
      Left            =   4680
      Top             =   9420
   End
   Begin VB.CheckBox chkShowResources 
      Caption         =   "Resources"
      Height          =   195
      Left            =   15720
      TabIndex        =   54
      ToolTipText     =   "Show Our Contact instead of Allocated To"
      Top             =   360
      Width           =   1515
   End
   Begin VB.CheckBox chkMPC_Columns 
      Caption         =   "Our Contact"
      Height          =   195
      Left            =   15720
      TabIndex        =   52
      ToolTipText     =   "Show Our Contact instead of Allocated To"
      Top             =   660
      Width           =   1695
   End
   Begin VB.CheckBox chkShowCostingTotal 
      Caption         =   "Costings"
      Height          =   195
      Left            =   15720
      TabIndex        =   49
      Top             =   60
      Width           =   1395
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdJobListing 
      Bindings        =   "frmJobListing.frx":08E3
      Height          =   2595
      Left            =   60
      TabIndex        =   47
      Top             =   2460
      Width           =   15135
      _Version        =   196617
      stylesets.count =   1
      stylesets(0).Name=   "vtstart"
      stylesets(0).ForeColor=   0
      stylesets(0).BackColor=   10420113
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmJobListing.frx":08FF
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16051436
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   23
      Columns(0).Width=   1402
      Columns(0).Caption=   "Job ID"
      Columns(0).Name =   "jobID"
      Columns(0).DataField=   "jobID"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "Project #"
      Columns(1).Name =   "projectname"
      Columns(1).DataField=   "projectnumber"
      Columns(1).DataType=   3
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "Allocated To"
      Columns(2).Name =   "allocatedto"
      Columns(2).DataField=   "allocatedto"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3440
      Columns(3).Caption=   "Company"
      Columns(3).Name =   "company"
      Columns(3).DataField=   "companyname"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   767
      Columns(4).Caption=   "User"
      Columns(4).Name =   "createduser"
      Columns(4).DataField=   "createduser"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   3387
      Columns(5).Caption=   "Title"
      Columns(5).Name =   "title"
      Columns(5).DataField=   "title1"
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   1402
      Columns(6).Caption=   "Series ID"
      Columns(6).Name =   "seriesID"
      Columns(6).DataField=   "seriesID"
      Columns(6).FieldLen=   256
      Columns(7).Width=   3916
      Columns(7).Caption=   "Dubbing Requirements"
      Columns(7).Name =   "requirements"
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   3200
      Columns(8).Caption=   "VT or Digital"
      Columns(8).Name =   "jobdigitaltype"
      Columns(8).DataField=   "jobdigitaltype"
      Columns(8).DataType=   17
      Columns(8).FieldLen=   256
      Columns(9).Width=   2990
      Columns(9).Caption=   "VT Started"
      Columns(9).Name =   "vtstartdate"
      Columns(9).DataField=   "vtstartdate"
      Columns(9).DataType=   7
      Columns(9).FieldLen=   256
      Columns(10).Width=   767
      Columns(10).Caption=   "Op"
      Columns(10).Name=   "vtstartuser"
      Columns(10).DataField=   "vtstartuser"
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Caption=   "Est Time Req. (mins)"
      Columns(11).Name=   "operatorduration"
      Columns(11).DataField=   "operatorduration"
      Columns(11).FieldLen=   256
      Columns(12).Width=   2990
      Columns(12).Caption=   "Job Start Time"
      Columns(12).Name=   "startdate"
      Columns(12).DataField=   "startdate"
      Columns(12).DataType=   7
      Columns(12).FieldLen=   256
      Columns(13).Width=   1191
      Columns(13).Caption=   "End"
      Columns(13).Name=   "enddate"
      Columns(13).DataField=   "enddate"
      Columns(13).DataType=   7
      Columns(13).NumberFormat=   "HH:nn"
      Columns(13).FieldLen=   256
      Columns(14).Width=   2672
      Columns(14).Caption=   "Product"
      Columns(14).Name=   "productname"
      Columns(14).DataField=   "productname"
      Columns(14).FieldLen=   256
      Columns(15).Width=   2170
      Columns(15).Caption=   "Status"
      Columns(15).Name=   "status"
      Columns(15).DataField=   "fd_status"
      Columns(15).FieldLen=   256
      Columns(15).Locked=   -1  'True
      Columns(16).Width=   3200
      Columns(16).Caption=   "Total"
      Columns(16).Name=   "Total"
      Columns(16).Alignment=   1
      Columns(16).DataField=   "Total"
      Columns(16).DataType=   6
      Columns(16).NumberFormat=   "�00.00"
      Columns(16).FieldLen=   256
      Columns(16).Locked=   -1  'True
      Columns(17).Width=   3200
      Columns(17).Caption=   "Resources Booked"
      Columns(17).Name=   "resourcesbooked"
      Columns(17).DataField=   "Column 16"
      Columns(17).DataType=   8
      Columns(17).FieldLen=   256
      Columns(17).Locked=   -1  'True
      Columns(18).Width=   2963
      Columns(18).Caption=   "Despatch Deadline"
      Columns(18).Name=   "deadlinedate"
      Columns(18).DataField=   "deadlinedate"
      Columns(18).DataType=   7
      Columns(18).FieldLen=   256
      Columns(18).Locked=   -1  'True
      Columns(19).Width=   2725
      Columns(19).Caption=   "Do Not Deliver Yet"
      Columns(19).Name=   "Do Not Deliver Yet"
      Columns(19).DataField=   "donotdeliveryet"
      Columns(19).FieldLen=   256
      Columns(19).Style=   2
      Columns(20).Width=   3493
      Columns(20).Caption=   "Date Completed"
      Columns(20).Name=   "completeddate"
      Columns(20).DataField=   "completeddate"
      Columns(20).DataType=   7
      Columns(20).FieldLen=   256
      Columns(20).Style=   1
      Columns(21).Width=   2090
      Columns(21).Caption=   "Completed By"
      Columns(21).Name=   "completeduser"
      Columns(21).DataField=   "completeduser"
      Columns(21).DataType=   8
      Columns(21).FieldLen=   256
      Columns(22).Width=   794
      Columns(22).Caption=   "Hold"
      Columns(22).Name=   "flaghold"
      Columns(22).DataField=   "flaghold"
      Columns(22).FieldLen=   256
      Columns(22).Style=   2
      _ExtentX        =   26696
      _ExtentY        =   4577
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.TextBox txtBookedBy 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   4320
      TabIndex        =   41
      ToolTipText     =   "The job title to search for"
      Top             =   1200
      Width           =   1155
   End
   Begin VB.CheckBox chkOnlyJobsWithDubs 
      Caption         =   "Show jobs with dubbing instructions only"
      Height          =   255
      Left            =   60
      TabIndex        =   40
      ToolTipText     =   "Display dub jobs only"
      Top             =   1230
      Width           =   3255
   End
   Begin VB.TextBox txtTitleSearch 
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   8580
      TabIndex        =   36
      ToolTipText     =   "The job title to search for"
      Top             =   660
      Width           =   2895
   End
   Begin VB.PictureBox picTextBoxes 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   4575
      Left            =   60
      ScaleHeight     =   4575
      ScaleWidth      =   4455
      TabIndex        =   20
      Top             =   5100
      Width           =   4455
      Begin VB.CheckBox chkDoWholeSeries 
         Alignment       =   1  'Right Justify
         Caption         =   "Do Whole Series"
         Height          =   255
         Left            =   2460
         TabIndex        =   90
         Top             =   1320
         Value           =   1  'Checked
         Width           =   1515
      End
      Begin VB.TextBox txtClientContractReference 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   224
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   88
         ToolTipText     =   "The clients order reference number"
         Top             =   2520
         Width           =   3495
      End
      Begin VB.TextBox txtSeriesID 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   224
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   84
         ToolTipText     =   "When the Job was Created"
         Top             =   1320
         Width           =   1275
      End
      Begin VB.TextBox txtOperatorNotes 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   945
         Left            =   540
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   43
         ToolTipText     =   "The clients order reference number"
         Top             =   3420
         Width           =   3855
      End
      Begin VB.TextBox txtOrderReference 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   224
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   8
         ToolTipText     =   "The clients order reference number"
         Top             =   2220
         Width           =   3495
      End
      Begin VB.TextBox txtTelephone 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   224
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   7
         ToolTipText     =   "The job specific telephone number"
         Top             =   1020
         Width           =   3495
      End
      Begin VB.TextBox txtJobID 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   4
         ToolTipText     =   "The unique identifier for this job"
         Top             =   0
         Width           =   1275
      End
      Begin VB.TextBox txtCompany 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFC0FF&
         BorderStyle     =   0  'None
         Height          =   224
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   5
         ToolTipText     =   "The company name"
         Top             =   420
         Width           =   3495
      End
      Begin VB.TextBox txtContact 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFC0FF&
         BorderStyle     =   0  'None
         Height          =   224
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   6
         ToolTipText     =   "The contact name"
         Top             =   720
         Width           =   3495
      End
      Begin VB.TextBox txtDeadlineDate 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   224
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   13
         ToolTipText     =   "The job Deadline date"
         Top             =   3120
         Width           =   1275
      End
      Begin VB.TextBox txtDeadlineTime 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   224
         Left            =   3180
         Locked          =   -1  'True
         TabIndex        =   14
         ToolTipText     =   "The job Deadline Time"
         Top             =   3120
         Width           =   1215
      End
      Begin VB.TextBox txtTitle 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   224
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   9
         ToolTipText     =   "The job title"
         Top             =   1620
         Width           =   3495
      End
      Begin VB.TextBox txtSubTitle 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   224
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   10
         ToolTipText     =   "The job Subtitle"
         Top             =   1920
         Width           =   3495
      End
      Begin VB.TextBox txtCreatedBy 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   224
         Left            =   3180
         Locked          =   -1  'True
         TabIndex        =   12
         ToolTipText     =   "Job created by?"
         Top             =   2820
         Width           =   1215
      End
      Begin VB.TextBox txtCreatedOn 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   224
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   11
         ToolTipText     =   "When the Job was Created"
         Top             =   2820
         Width           =   1275
      End
      Begin MSAdodcLib.Adodc adoJobListing 
         Height          =   330
         Left            =   2220
         Top             =   0
         Width           =   2205
         _ExtentX        =   3889
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   2
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   16761024
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   "cetasoft"
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoJobListing"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.Label lblCaption 
         Caption         =   "Contract #"
         Height          =   225
         Index           =   29
         Left            =   0
         TabIndex        =   89
         Top             =   2520
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Series ID"
         Height          =   225
         Index           =   11
         Left            =   0
         TabIndex        =   85
         Top             =   1320
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Op. Notes"
         Height          =   405
         Index           =   22
         Left            =   0
         TabIndex        =   44
         Top             =   3420
         Width           =   555
      End
      Begin VB.Label lblCaption 
         Caption         =   "Time"
         Height          =   225
         Index           =   7
         Left            =   2460
         TabIndex        =   31
         Top             =   3120
         Width           =   495
      End
      Begin VB.Label lblCaption 
         Caption         =   "Deadline"
         Height          =   225
         Index           =   6
         Left            =   0
         TabIndex        =   30
         Top             =   3120
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Order Ref"
         Height          =   225
         Index           =   4
         Left            =   0
         TabIndex        =   29
         Top             =   2220
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Contact"
         Height          =   225
         Index           =   5
         Left            =   0
         TabIndex        =   28
         Top             =   720
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Telephone"
         Height          =   225
         Index           =   3
         Left            =   0
         TabIndex        =   27
         Top             =   1020
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Company"
         Height          =   225
         Index           =   2
         Left            =   0
         TabIndex        =   26
         Top             =   420
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Job ID"
         Height          =   224
         Index           =   0
         Left            =   0
         TabIndex        =   25
         Top             =   0
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Title"
         Height          =   225
         Index           =   1
         Left            =   0
         TabIndex        =   24
         Top             =   1620
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Sub Title"
         Height          =   225
         Index           =   8
         Left            =   0
         TabIndex        =   23
         Top             =   1920
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Created"
         Height          =   225
         Index           =   9
         Left            =   0
         TabIndex        =   22
         Top             =   2820
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "By"
         Height          =   225
         Index           =   10
         Left            =   2460
         TabIndex        =   21
         Top             =   2820
         Width           =   495
      End
   End
   Begin VB.PictureBox picFooter 
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   5160
      ScaleHeight     =   315
      ScaleWidth      =   10035
      TabIndex        =   19
      Top             =   8760
      Width           =   10035
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   7560
         TabIndex        =   53
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdStartJob 
         Caption         =   "Start Job"
         Height          =   315
         Left            =   1260
         TabIndex        =   48
         ToolTipText     =   "Refresh details"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   2520
         TabIndex        =   39
         ToolTipText     =   "Refresh details"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdRefresh 
         Caption         =   "Refresh (F5)"
         Height          =   315
         Left            =   3780
         TabIndex        =   38
         ToolTipText     =   "Refresh details"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   8820
         TabIndex        =   17
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdViewJob 
         Caption         =   "View Job"
         Height          =   315
         Left            =   6300
         TabIndex        =   16
         ToolTipText     =   "View the Job Detail"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdCompleteJob 
         Caption         =   "Complete Job"
         Height          =   315
         Left            =   5040
         TabIndex        =   15
         ToolTipText     =   "Complete the job"
         Top             =   0
         Width           =   1155
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbAllocation 
      Height          =   255
      Left            =   8580
      TabIndex        =   0
      ToolTipText     =   "Part of project or regular job"
      Top             =   60
      Width           =   2895
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BackColorEven   =   -2147483643
      BackColorOdd    =   -2147483643
      RowHeight       =   423
      Columns(0).Width=   4868
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   12640511
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbJobType 
      Height          =   255
      Left            =   12780
      TabIndex        =   2
      ToolTipText     =   "Type of job"
      Top             =   360
      Width           =   2895
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BevelColorFrame =   -2147483644
      CheckBox3D      =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   4868
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Bindings        =   "frmJobListing.frx":091B
      Height          =   255
      Left            =   12780
      TabIndex        =   1
      ToolTipText     =   "The company this job is for"
      Top             =   60
      Width           =   2895
      BevelWidth      =   0
      DataFieldList   =   "name"
      BevelType       =   0
      _Version        =   196617
      BorderStyle     =   0
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "accountcode"
      Columns(2).Name =   "accountcode"
      Columns(2).DataField=   "accountcode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "telephone"
      Columns(3).Name =   "telephone"
      Columns(3).DataField=   "telephone"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "cetaclientcode"
      Columns(4).Name =   "cetaclientcode"
      Columns(4).DataField=   "cetaclientcode"
      Columns(4).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   450
      _StockProps     =   93
      ForeColor       =   -2147483640
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbOurContact 
      Height          =   255
      Left            =   15300
      TabIndex        =   45
      ToolTipText     =   "Our contact on this job"
      Top             =   6120
      Width           =   2355
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BevelColorFrame =   -2147483644
      CheckBox3D      =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   4868
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4154
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   16761024
      DataFieldToDisplay=   "Column 0"
   End
   Begin MSAdodcLib.Adodc adoJobDetail 
      Height          =   330
      Left            =   13200
      Top             =   4920
      Visible         =   0   'False
      Width           =   1905
      _ExtentX        =   3360
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoJobDetail"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbAllocatedTo 
      Height          =   255
      Left            =   15300
      TabIndex        =   50
      ToolTipText     =   "Our contact on this job"
      Top             =   6720
      Width           =   2355
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BevelColorFrame =   -2147483644
      CheckBox3D      =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   4868
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4154
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   16761024
      DataFieldToDisplay=   "Column 0"
   End
   Begin MSComCtl2.DTPicker datDeadlineFrom 
      Height          =   315
      Left            =   5760
      TabIndex        =   68
      Tag             =   "NOCHECK"
      ToolTipText     =   "The starting date of the job"
      Top             =   300
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   165347329
      CurrentDate     =   37870
   End
   Begin MSComCtl2.DTPicker datDeadlineTo 
      Height          =   315
      Left            =   5760
      TabIndex        =   69
      Tag             =   "NOCHECK"
      ToolTipText     =   "The ending date of the job"
      Top             =   720
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   165347329
      CurrentDate     =   37870
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDetail 
      Bindings        =   "frmJobListing.frx":0934
      Height          =   1695
      Left            =   4620
      TabIndex        =   76
      TabStop         =   0   'False
      Top             =   5100
      Width           =   10425
      _Version        =   196617
      stylesets.count =   1
      stylesets(0).Name=   "COMPLETED"
      stylesets(0).BackColor=   7011261
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmJobListing.frx":094F
      BeveColorScheme =   1
      AllowUpdate     =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      BackColorOdd    =   16051436
      RowHeight       =   370
      Columns.Count   =   29
      Columns(0).Width=   370
      Columns(0).Name =   "copytype"
      Columns(0).DataField=   "copytype"
      Columns(0).Case =   2
      Columns(0).FieldLen=   1
      Columns(1).Width=   5292
      Columns(1).Caption=   "Description / Work to be done"
      Columns(1).Name =   "description"
      Columns(1).DataField=   "description"
      Columns(1).FieldLen=   256
      Columns(2).Width=   2170
      Columns(2).Caption=   "Clip ID"
      Columns(2).Name =   "clipID"
      Columns(2).DataField=   "clipID"
      Columns(2).FieldLen=   256
      Columns(2).Style=   1
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   14671839
      Columns(3).Width=   1931
      Columns(3).Caption=   "Barcode"
      Columns(3).Name =   "librarybarcode"
      Columns(3).DataField=   "librarybarcode"
      Columns(3).FieldLen=   256
      Columns(4).Width=   2540
      Columns(4).Caption=   "Clock Number"
      Columns(4).Name =   "clocknumber"
      Columns(4).DataField=   "clocknumber"
      Columns(4).FieldLen=   256
      Columns(5).Width=   2302
      Columns(5).Caption=   "Start Time Code"
      Columns(5).Name =   "starttimecode"
      Columns(5).DataField=   "starttimecode"
      Columns(5).FieldLen=   256
      Columns(6).Width=   2117
      Columns(6).Caption=   "Location"
      Columns(6).Name =   "librarylocation"
      Columns(6).FieldLen=   256
      Columns(7).Width=   714
      Columns(7).Caption=   "Qty"
      Columns(7).Name =   "quantity"
      Columns(7).DataField=   "quantity"
      Columns(7).FieldLen=   256
      Columns(8).Width=   1614
      Columns(8).Caption=   "Format"
      Columns(8).Name =   "format"
      Columns(8).DataField=   "format"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   1508
      Columns(9).Caption=   "Standard"
      Columns(9).Name =   "standard"
      Columns(9).DataField=   "videostandard"
      Columns(9).FieldLen=   256
      Columns(10).Width=   2117
      Columns(10).Caption=   "Snd Ch 1"
      Columns(10).Name=   "ch1"
      Columns(10).DataField=   "sound1"
      Columns(10).FieldLen=   256
      Columns(11).Width=   2117
      Columns(11).Caption=   "Snd Ch 2"
      Columns(11).Name=   "ch2"
      Columns(11).DataField=   "sound2"
      Columns(11).FieldLen=   256
      Columns(12).Width=   2117
      Columns(12).Caption=   "Snd Ch 3"
      Columns(12).Name=   "ch3"
      Columns(12).DataField=   "sound3"
      Columns(12).FieldLen=   256
      Columns(13).Width=   2117
      Columns(13).Caption=   "Snd Ch 4"
      Columns(13).Name=   "ch4"
      Columns(13).DataField=   "sound4"
      Columns(13).FieldLen=   256
      Columns(14).Width=   1614
      Columns(14).Caption=   "Ratio"
      Columns(14).Name=   "ratio"
      Columns(14).DataField=   "aspectratio"
      Columns(14).FieldLen=   256
      Columns(15).Width=   1508
      Columns(15).Caption=   "T/C"
      Columns(15).Name=   "timecode"
      Columns(15).DataField=   "timecode"
      Columns(15).FieldLen=   256
      Columns(16).Width=   979
      Columns(16).Caption=   "R/T"
      Columns(16).Name=   "runningtime"
      Columns(16).DataField=   "runningtime"
      Columns(16).FieldLen=   256
      Columns(17).Width=   1508
      Columns(17).Caption=   "Stock 1"
      Columns(17).Name=   "stocktype1"
      Columns(17).DataField=   "stock1type"
      Columns(17).FieldLen=   256
      Columns(18).Width=   794
      Columns(18).Caption=   "Ours"
      Columns(18).Name=   "ourstock1"
      Columns(18).DataField=   "stock1ours"
      Columns(18).FieldLen=   256
      Columns(19).Width=   794
      Columns(19).Caption=   "Cl"
      Columns(19).Name=   "clientstock1"
      Columns(19).DataField=   "stock1clients"
      Columns(19).FieldLen=   256
      Columns(20).Width=   1508
      Columns(20).Caption=   "Stock 2"
      Columns(20).Name=   "stocktype2"
      Columns(20).DataField=   "stock2type"
      Columns(20).FieldLen=   256
      Columns(21).Width=   794
      Columns(21).Caption=   "Ours"
      Columns(21).Name=   "ourstock2"
      Columns(21).DataField=   "stock2ours"
      Columns(21).FieldLen=   256
      Columns(22).Width=   794
      Columns(22).Caption=   "Cl"
      Columns(22).Name=   "clientstock2"
      Columns(22).DataField=   "stock2clients"
      Columns(22).FieldLen=   256
      Columns(23).Width=   873
      Columns(23).Caption=   "Order"
      Columns(23).Name=   "fd_orderby"
      Columns(23).DataField=   "fd_orderby"
      Columns(23).FieldLen=   256
      Columns(23).HasBackColor=   -1  'True
      Columns(23).BackColor=   14671839
      Columns(24).Width=   3200
      Columns(24).Visible=   0   'False
      Columns(24).Caption=   "jobID"
      Columns(24).Name=   "jobID"
      Columns(24).DataField=   "jobID"
      Columns(24).FieldLen=   256
      Columns(25).Width=   3200
      Columns(25).Visible=   0   'False
      Columns(25).Caption=   "jobdetailID"
      Columns(25).Name=   "jobdetailID"
      Columns(25).DataField=   "jobdetailID"
      Columns(25).FieldLen=   256
      Columns(26).Width=   3200
      Columns(26).Caption=   "Completed Date"
      Columns(26).Name=   "completeddate"
      Columns(26).DataField=   "completeddate"
      Columns(26).DataType=   7
      Columns(26).FieldLen=   256
      Columns(26).Style=   4
      Columns(27).Width=   1402
      Columns(27).Caption=   "By"
      Columns(27).Name=   "completeduser"
      Columns(27).DataField=   "completeduser"
      Columns(27).FieldLen=   256
      Columns(28).Width=   3200
      Columns(28).Caption=   "Email To"
      Columns(28).Name=   "emailto"
      Columns(28).DataField=   "emailto"
      Columns(28).FieldLen=   256
      Columns(28).Style=   1
      Columns(28).HasBackColor=   -1  'True
      Columns(28).BackColor=   13294580
      _ExtentX        =   18389
      _ExtentY        =   2990
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TabDlg.SSTab tabFilter 
      Height          =   1020
      Left            =   60
      TabIndex        =   18
      Top             =   1620
      Width           =   15135
      _ExtentX        =   26696
      _ExtentY        =   1799
      _Version        =   393216
      Tabs            =   9
      TabsPerRow      =   9
      TabHeight       =   520
      TabCaption(0)   =   "Outstanding"
      TabPicture(0)   =   "frmJobListing.frx":096B
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).ControlCount=   0
      TabCaption(1)   =   "Completed."
      TabPicture(1)   =   "frmJobListing.frx":0987
      Tab(1).ControlEnabled=   0   'False
      Tab(1).ControlCount=   0
      TabCaption(2)   =   "Invoiced"
      TabPicture(2)   =   "frmJobListing.frx":09A3
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      TabCaption(3)   =   "Credited"
      TabPicture(3)   =   "frmJobListing.frx":09BF
      Tab(3).ControlEnabled=   0   'False
      Tab(3).ControlCount=   0
      TabCaption(4)   =   "Sent To Accounts"
      TabPicture(4)   =   "frmJobListing.frx":09DB
      Tab(4).ControlEnabled=   0   'False
      Tab(4).ControlCount=   0
      TabCaption(5)   =   "No Charge"
      TabPicture(5)   =   "frmJobListing.frx":09F7
      Tab(5).ControlEnabled=   0   'False
      Tab(5).ControlCount=   0
      TabCaption(6)   =   "Quote"
      TabPicture(6)   =   "frmJobListing.frx":0A13
      Tab(6).ControlEnabled=   0   'False
      Tab(6).ControlCount=   0
      TabCaption(7)   =   "Abandoned"
      TabPicture(7)   =   "frmJobListing.frx":0A2F
      Tab(7).ControlEnabled=   0   'False
      Tab(7).ControlCount=   0
      TabCaption(8)   =   "All"
      TabPicture(8)   =   "frmJobListing.frx":0A4B
      Tab(8).ControlEnabled=   0   'False
      Tab(8).ControlCount=   0
   End
   Begin TabDlg.SSTab tabReplacement 
      Height          =   600
      Left            =   60
      TabIndex        =   3
      Top             =   1560
      Width           =   15135
      _ExtentX        =   26696
      _ExtentY        =   1058
      _Version        =   393216
      Style           =   1
      Tabs            =   11
      TabsPerRow      =   11
      TabHeight       =   520
      TabCaption(0)   =   "2nd Pencil"
      TabPicture(0)   =   "frmJobListing.frx":0A67
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).ControlCount=   0
      TabCaption(1)   =   "Pencil"
      TabPicture(1)   =   "frmJobListing.frx":0A83
      Tab(1).ControlEnabled=   0   'False
      Tab(1).ControlCount=   0
      TabCaption(2)   =   "Confirmed / Prepared / Despatched"
      TabPicture(2)   =   "frmJobListing.frx":0A9F
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      TabCaption(3)   =   "Completed / Returned"
      TabPicture(3)   =   "frmJobListing.frx":0ABB
      Tab(3).ControlEnabled=   0   'False
      Tab(3).ControlCount=   0
      TabCaption(4)   =   "Hold Cost"
      TabPicture(4)   =   "frmJobListing.frx":0AD7
      Tab(4).ControlEnabled=   0   'False
      Tab(4).ControlCount=   0
      TabCaption(5)   =   "Costed"
      TabPicture(5)   =   "frmJobListing.frx":0AF3
      Tab(5).ControlEnabled=   0   'False
      Tab(5).ControlCount=   0
      TabCaption(6)   =   "Sent To Accounts"
      TabPicture(6)   =   "frmJobListing.frx":0B0F
      Tab(6).ControlEnabled=   0   'False
      Tab(6).ControlCount=   0
      TabCaption(7)   =   "No Charge"
      TabPicture(7)   =   "frmJobListing.frx":0B2B
      Tab(7).ControlEnabled=   0   'False
      Tab(7).ControlCount=   0
      TabCaption(8)   =   "Cancelled"
      TabPicture(8)   =   "frmJobListing.frx":0B47
      Tab(8).ControlEnabled=   0   'False
      Tab(8).ControlCount=   0
      TabCaption(9)   =   "All"
      TabPicture(9)   =   "frmJobListing.frx":0B63
      Tab(9).ControlEnabled=   0   'False
      Tab(9).ControlCount=   0
      TabCaption(10)  =   "Dubbing Control"
      TabPicture(10)  =   "frmJobListing.frx":0B7F
      Tab(10).ControlEnabled=   0   'False
      Tab(10).ControlCount=   0
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnjobDigitalType 
      Height          =   795
      Left            =   15360
      TabIndex        =   82
      Tag             =   "CLEAR"
      Top             =   4740
      Width           =   2175
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   3200
      Columns(0).Caption=   "advisorysystem"
      Columns(0).Name =   "advisorysystem"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   3836
      _ExtentY        =   1402
      _StockProps     =   77
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.Label lblCaption 
      Caption         =   "Series ID"
      Height          =   255
      Index           =   12
      Left            =   7440
      TabIndex        =   87
      Top             =   360
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Subtitle"
      Height          =   255
      Index           =   27
      Left            =   7440
      TabIndex        =   81
      Top             =   960
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "Deadline Date"
      Height          =   255
      Index           =   28
      Left            =   5940
      TabIndex        =   70
      Top             =   0
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Allocated To"
      Height          =   255
      Index           =   25
      Left            =   15300
      TabIndex        =   51
      Top             =   6420
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Our Contact"
      Height          =   255
      Index           =   23
      Left            =   15300
      TabIndex        =   46
      Top             =   5820
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Booked By"
      Height          =   255
      Index           =   21
      Left            =   3480
      TabIndex        =   42
      Top             =   1260
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      Height          =   255
      Index           =   20
      Left            =   7440
      TabIndex        =   37
      Top             =   660
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order By"
      Height          =   255
      Index           =   24
      Left            =   13260
      TabIndex        =   35
      Top             =   8220
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Client"
      Height          =   255
      Index           =   16
      Left            =   11640
      TabIndex        =   34
      Top             =   60
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job Type"
      Height          =   255
      Index           =   34
      Left            =   11640
      TabIndex        =   33
      Top             =   360
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Project Type"
      Height          =   255
      Index           =   49
      Left            =   7440
      TabIndex        =   32
      Top             =   60
      Width           =   1035
   End
End
Attribute VB_Name = "frmJobListing"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public m_lngTimer As Long

Private Sub adoJobListing_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

If adoJobListing.Recordset.EOF Then Exit Sub
If adoJobListing.Recordset.BOF Then Exit Sub

Dim l_strSQL As String
l_strSQL = "SELECT * FROM job WHERE jobID = '" & adoJobListing.Recordset("jobID") & "';"
Dim l_rstJob As New ADODB.Recordset
Set l_rstJob = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If Not l_rstJob.EOF Then

    With l_rstJob
        txtJobID.Text = .Fields("jobID")
        txtCompany.Text = Trim(" " & .Fields("companyname"))
        txtContact.Text = Trim(" " & .Fields("contactname"))
        txtTelephone.Text = Trim(" " & .Fields("telephone"))
        txtOrderReference.Text = Trim(" " & .Fields("orderreference"))
        txtClientContractReference.Text = Trim(" " & .Fields("customercontractreference"))
        txtDeadlineDate.Text = Format(.Fields("deadlinedate"), vbShortDateFormat)
        txtDeadlineTime.Text = Format(.Fields("deadlinedate"), vbTimeFormat)
        txtSeriesID.Text = Trim(" " & .Fields("seriesID"))
        txtTitle.Text = Trim(" " & .Fields("title1"))
        txtSubtitle.Text = Trim(" " & .Fields("title2"))
        txtCreatedOn.Text = Format(.Fields("createddate"), vbShortDateFormat)
        txtCreatedBy.Text = Trim(" " & .Fields("createduser"))
        txtOperatorNotes.Text = Trim(" " & .Fields("notes2"))
        chkDoWholeSeries.Value = GetFlag(.Fields("dowholeseries"))
    End With
    
    ShowDetailForJob adoJobListing.Recordset("jobID")
    
End If

l_rstJob.Close
Set l_rstJob = Nothing


End Sub

Private Sub chkHideDemo_Click()

cmdRefresh.Value = True

End Sub

Private Sub chkShowCostingTotal_Click()

If chkShowCostingTotal.Value <> 0 Then chkShowCostingTotal.Enabled = False

End Sub

Private Sub cmbCompany_DropDown()

Dim l_strSQL As String
l_strSQL = "SELECT name, accountcode, telephone, companyID, fax, accountstatus FROM company WHERE (name LIKE '" & QuoteSanitise(cmbCompany.Text) & "%') AND (iscustomer = 1 or isprospective = 1) AND system_active = 1 ORDER BY name;"
Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing
End Sub

Private Sub cmdClear_Click()
ClearFields Me
End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdCompleteJob_Click()

If grdJobListing.Row = grdJobListing.Rows Then Exit Sub

Dim l_lngJobID As Long, l_strSQL As String

l_lngJobID = Val(grdJobListing.Columns("jobID").Text)

If g_optUseSingleCompletionScreenForDubbing = 1 Then
    frmCompleteJobSingle.lblJobID.Caption = l_lngJobID
    frmCompleteJobSingle.cmdLoadJobDetails.Value = True
    frmCompleteJobSingle.Show vbModal
    Unload frmCompleteJobSingle
    Set frmCompleteJobSingle = Nothing
    Exit Sub
End If


'should we use the simple completion?
If g_intDisableDetailedDubbingCompletion = 1 Or g_intUseTerseJobDetailCompletion = 1 Then

    'is this already completed?
    If grdJobListing.Columns("completeddate").Text <> "" Then
        'if so, reset it...
        l_strSQL = "UPDATE job SET completeddate = NULL, completeduser = NULL, fd_status = 'Confirmed' WHERE jobID = '" & l_lngJobID & "';"
    Else
        'otherwise, set the completed date and user to be now, and me!
        l_strSQL = "UPDATE job SET completeddate = '" & FormatSQLDate(Now) & "', completeduser = '" & g_strUserInitials & "', fd_status = 'Completed' WHERE jobID = '" & l_lngJobID & "';"
    End If
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    Dim l_lngBookmark As Long
    l_lngBookmark = grdJobListing.Bookmark
    adoJobListing.Refresh
    grdJobListing.Bookmark = l_lngBookmark
    
    If g_optPromptForDetailedCompletion Then PromptForCompletionDetails l_lngJobID
    
Else
    frmCompleteJob.Show vbModal
    UpdateJobStatus Val(grdJobListing.Columns("jobID").Text), "Completed"
End If

'make sure the job knows that is has been updated

DoEvents

'this checks it is being completed from the confirmed tab, then will send an email...
If InStr(LCase(tabReplacement.Caption), "confirmed") <> 0 Then
    If InStr(GetData("company", "cetaclientcode", "name", grdJobListing.Columns("company").Text), "/emailoncompletion") <> 0 Or GetFlag(GetData("job", "flagemail", "jobID", grdJobListing.Columns("jobID").Text)) <> False Then
        SendEmailFromJobID grdJobListing.Columns("jobID").Text
    End If
End If
End Sub

Private Sub cmdShowAbandoned_Click()
ShowJobListing "abandoned"
End Sub

Private Sub cmdShowAll_Click()
ShowJobListing "all"
End Sub

Private Sub cmdShowCompleted_Click()
ShowJobListing "completed"
End Sub

Private Sub cmdShowInvoiced_Click()
ShowJobListing "invoiced"
End Sub

Private Sub cmdShowOutstanding_Click()
ShowJobListing "outstanding"
End Sub

Private Sub cmdShowQuote_Click()
ShowJobListing "quote"
End Sub

Private Sub cmdShowToBeInvoiced_Click()
ShowJobListing "to be invoiced"
End Sub

Private Sub cmdPrint_Click()

Exit Sub

Dim l_searchstring As String
l_searchstring = adoJobListing.RecordSource

If l_searchstring <> "" Then
    PrintCrystalReportUsingSQL g_strLocationOfCrystalReportFiles & "jobsearch.rpt", l_searchstring, g_blnPreviewReport
End If

End Sub

Private Sub cmdRefresh_Click()

If tabFilter.Visible = False Then
    ShowJobListing LCase(tabReplacement.Caption)
Else
    ShowJobListing LCase(tabFilter.Caption)
End If

End Sub

Private Sub cmdStartJob_Click()

Dim l_lngJobID As Long
Dim l_strSQL As String
Dim l_strUserInitials As String
Dim l_intMsgbox As Integer

cmdStartJob.Caption = "Please Wait..."
cmdStartJob.Enabled = False

'pick up the job ID
l_lngJobID = grdJobListing.Columns("jobID").Text

If l_lngJobID = 0 Then Exit Sub

'get the initials - if there are any
l_strUserInitials = GetData("job", "vtstartuser", "jobID", l_lngJobID)

If l_strUserInitials <> "" Then
    l_intMsgbox = NewMsgBox("Job already started", "This job has already been started. What do you want to do?", "Remove", "Re-start", "Cancel")
    Select Case l_intMsgbox
    Case 0
        
        l_strSQL = "UPDATE job SET vtstartdate = Null, vtstartuser = Null WHERE jobID = '" & l_lngJobID & "';"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError

        GoTo Proc_Refresh
        
    Case 1
        'do nothing, carry on as usual
    Case 2
        Exit Sub
    End Select
End If

frmStartVTJob.Show vbModal

Select Case frmStartVTJob.Tag
Case "Start", "Print"
    GoTo Proc_Start
Case "Cancel"
    GoTo Proc_Unload
End Select

Proc_Start:

l_strUserInitials = GetData("cetauser", "initials", "fullname", frmStartVTJob.lstOperator.Text)

Do Until l_strUserInitials <> ""
    If l_strUserInitials = "" Then l_strUserInitials = UCase(InputBox("Please enter your initials"))
Loop

'grdJobListing.Columns("vtstartdate").Value = FormatSQLDate(Now())
'grdJobListing.Columns("vtstartuser").Text = l_strUserInitials
'grdJobListing.Update


l_strSQL = "UPDATE job SET vtstartdate = '" & FormatSQLDate(Now) & "', vtstartuser = '" & l_strUserInitials & "', fd_status = 'In Progress' WHERE jobID = '" & l_lngJobID & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

'allocate the clip ID numbers to any "D" lines on the job (digital)
If g_intNoAutomaticClipNumbers = 0 Then
    AllocateClipIDNumbers l_lngJobID
End If


If frmStartVTJob.Tag = "Print" Then PrintJobSheet Val(txtJobID.Text), "jobsheetdub.rpt"


AddJobHistory Val(txtJobID.Text), "Started VT Job"

Proc_Refresh:

Dim l_intCurrentPosition As Long

l_intCurrentPosition = grdJobListing.Bookmark

adoJobListing.Refresh

grdJobListing.Bookmark = l_intCurrentPosition

Proc_Unload:

Unload frmStartVTJob
Set frmStartVTJob = Nothing

cmdStartJob.Caption = "Start Job"
cmdStartJob.Enabled = True




End Sub

Private Sub cmdViewJob_Click()
'Unload frmJob

If grdJobListing.Rows < 1 Then
    NoJobSelectedMessage
    Exit Sub
End If

ShowJob grdJobListing.Columns("jobID").Text, 0, True
'ShowJob grdJobListing.Columns("jobID").Text, 0, True

End Sub

Private Sub cmdHoldAllJobs_Click()

Dim l_rstTemp As ADODB.Recordset

If MsgBox("Are you Sure?", vbYesNo, "Setting all jobs in Grid to 'On Hold'") = vbYes Then

    Set l_rstTemp = ExecuteSQL(adoJobListing.RecordSource, g_strExecuteError)
    CheckForSQLError
    
    If l_rstTemp.RecordCount > 0 Then
        l_rstTemp.MoveFirst
        Do While Not l_rstTemp.EOF
            l_rstTemp("flaghold") = 1
            l_rstTemp.Update
            l_rstTemp.MoveNext
        Loop
    End If
    l_rstTemp.Close
    Set l_rstTemp = Nothing
    adoJobListing.Refresh

End If

End Sub

Private Sub Form_Activate()
Me.WindowState = vbMaximized
End Sub

Private Sub Form_Load()

If Not CheckAccess("/viewjobcosttotals", True) Then chkShowCostingTotal.Visible = False

chkMPC_Columns.Value = g_optAutoTickOurContactInJobLists

grdJobListing.StyleSets.Add "2nd Pencil"
grdJobListing.StyleSets("2nd Pencil").BackColor = g_lngColour2ndPencil

grdJobListing.StyleSets.Add "Submitted"
grdJobListing.StyleSets("Submitted").BackColor = g_lngColour2ndPencil

grdJobListing.StyleSets.Add "Pencil"
grdJobListing.StyleSets("Pencil").BackColor = g_lngColourPencil

grdJobListing.StyleSets.Add "Confirmed"
grdJobListing.StyleSets("Confirmed").BackColor = g_lngColourConfirmed

grdJobListing.StyleSets.Add "Masters Here"
grdJobListing.StyleSets("Masters Here").BackColor = g_lngColourMastersHere

grdJobListing.StyleSets.Add "In Progress"
grdJobListing.StyleSets("In Progress").BackColor = g_lngColourInProgress

grdJobListing.StyleSets.Add "VT Done"
grdJobListing.StyleSets("VT Done").BackColor = g_lngColourVTDone

grdJobListing.StyleSets.Add "Pending"
grdJobListing.StyleSets("Pending").BackColor = g_lngColourOnHold

grdJobListing.StyleSets.Add "On Hold"
grdJobListing.StyleSets("On Hold").BackColor = g_lngColourOnHold

grdJobListing.StyleSets.Add "Rejected"
grdJobListing.StyleSets("Rejected").BackColor = g_lngColourOnHold

grdJobListing.StyleSets.Add "Completed"
grdJobListing.StyleSets("Completed").BackColor = g_lngColourCompleted

grdJobListing.StyleSets.Add "Hold Cost"
grdJobListing.StyleSets("Hold Cost").BackColor = g_lngColourHoldCost

grdJobListing.StyleSets.Add "Scheduled"
grdJobListing.StyleSets("Scheduled").BackColor = g_lngColourScheduled

grdJobListing.StyleSets.Add "Costed"
grdJobListing.StyleSets("Costed").BackColor = g_lngColourCosted

grdJobListing.StyleSets.Add "Sent To Accounts"
grdJobListing.StyleSets("Sent To Accounts").BackColor = g_lngColourSentToAccounts

grdJobListing.StyleSets.Add "Quick"
grdJobListing.StyleSets("Quick").BackColor = g_lngColourQuick

grdJobListing.StyleSets.Add "Cancelled"
grdJobListing.StyleSets("Cancelled").BackColor = g_lngColourCancelled

grdJobListing.StyleSets.Add "No Charge"
grdJobListing.StyleSets("No Charge").BackColor = vbRed

'hire colours
grdJobListing.StyleSets.Add "Prepared"
grdJobListing.StyleSets("Prepared").BackColor = g_lngColourPrepared

grdJobListing.StyleSets.Add "Despatched"
grdJobListing.StyleSets("Despatched").BackColor = g_lngColourDespatched

grdJobListing.StyleSets.Add "Returned"
grdJobListing.StyleSets("Returned").BackColor = g_lngColourReturned


'core functionality disabled
If g_intDisableProjects = 1 Then
    cmbAllocation.Visible = False
    lblCaption(49).Visible = False
    lblCaption(26).Visible = False
    txtProjectIDFrom.Visible = False
    txtProjectIDTo.Visible = False
    Frame2.Width = 1935
    lblCaption(12).Visible = False
    
End If

If g_intDisableResourceList = 1 Then
   chkShowResources.Visible = False
   
End If

If g_intDisableSchedule = 1 Then
   Frame5.Visible = False
   tabFilter.Visible = True
   tabReplacement.Visible = False
Else
    tabReplacement.Visible = True
    tabFilter.Visible = False
End If

If g_intDisableOurContact = 1 Then
    cmbOurContact.Visible = False
    lblCaption(23).Visible = False
    chkMPC_Columns.Visible = False
End If

If g_optHideHireDespatchTab = 1 Then
    cmbAllocatedTo.Visible = False
    lblCaption(25).Visible = False
End If


If g_intDisableAllocatedTo = 1 Then
    cmbAllocatedTo.Visible = False
    lblCaption(25).Visible = False

End If


MakeLookLikeOffice Me

PopulateCombo "fields-job", cmbOrderBy
PopulateCombo "allocation", cmbAllocation
PopulateCombo "ourcontact", cmbOurContact
PopulateCombo "jobdigitaltype", ddnjobDigitalType
grdJobListing.Columns("jobdigitaltype").DropDownHwnd = ddnjobDigitalType.hWnd

If g_optOrderByNumbers = 1 Then
    cmbOrderBy.Text = "job.jobID"
Else
    cmbOrderBy.Text = "job.deadlinedate"
End If

cmbDirection.ListIndex = 0

PopulateCombo "jobtype", cmbJobType
PopulateCombo "allocatedto", cmbAllocatedTo

'If Dir(App.Path & "\layouts\grdDetail.grd") = "" Then grdDetail.SaveLayout App.Path & "\layouts\grdDetail.grd", ssSaveLayoutAll

CenterForm Me

End Sub

Private Sub Form_Resize()
    
    On Error Resume Next
    
    picFooter.Top = Me.ScaleHeight - picFooter.Height - 120
    picFooter.Left = Me.ScaleWidth - picFooter.Width - 120
    
    picTextBoxes.Top = Me.ScaleHeight - 120 - 120 - picTextBoxes.Height
    
    grdJobListing.Height = Me.ScaleHeight - 120 - 120 - picTextBoxes.Height - 120 - grdJobListing.Top
    grdJobListing.Width = Me.ScaleWidth - 120 - grdJobListing.Left
    
    grdDetail.Top = picTextBoxes.Top
    
    If tabReplacement.Caption = "Dubbing Control" Or g_intDisableSchedule = 1 Then
        grdDetail.Height = (Me.ScaleHeight - picFooter.Height - 120 - 120 - grdDetail.Top)
        grdDetail.RowHeight = 600
    Else
        grdDetail.Height = (Me.ScaleHeight - picFooter.Height - 120 - 120 - grdDetail.Top) / 2
        grdDetail.RowHeight = 255
    End If
    
    
    grdDetail.Width = Me.ScaleWidth - 120 - grdDetail.Left
    
    tabFilter.Width = Me.ScaleWidth - 120 - 120
    tabReplacement.Width = tabFilter.Width

End Sub

Private Sub Form_Unload(Cancel As Integer)

'SaveColumnLayout grdJobListing, "Job Listing"
'SaveColumnLayout frmJobListing.grdDetail, "Job Detail"
'SaveColumnLayout frmJobListing.grdResourceSchedule, "Resource Schedule"

End Sub

Private Sub grdDetail_DblClick()

If grdDetail.Columns("completeddate").Text <> "" Then
    MsgBox "This line has already been completed", vbExclamation
End If

If grdDetail.Columns("copytype").Text = "M" Or grdDetail.Columns("copytype").Text = "T" Then
    MsgBox "You do not need to complete master (M) or text (T) lines.", vbExclamation
    Exit Sub
End If

Dim l_lngCompleted As Long
l_lngCompleted = CompleteJobDetailItem(grdDetail.Columns("jobdetailID").Text)

If l_lngCompleted <> 0 Then
    ShowDetailForJob grdDetail.Columns("jobID").Text
End If


End Sub

Private Sub grdDetail_InitColumnProps()

If g_optUseClipID = 0 Then
    grdDetail.Columns("clipID").Visible = False
Else
    grdDetail.Columns("clipID").Caption = g_strClipIDCaption
End If

End Sub

Private Sub grdDetail_RowLoaded(ByVal Bookmark As Variant)

On Error GoTo grdDetail_Error

grdDetail.Columns("librarylocation").Text = GetData("library", "location", "barcode", grdDetail.Columns("barcode").Text)
If grdDetail.Columns("completeddate").Text <> "" Then
    grdDetail.Columns("description").CellStyleSet "COMPLETED"
Else
    grdDetail.Columns("description").CellStyleSet ""
End If

Exit Sub

grdDetail_Error:

'LoadColumnLayout frmJobListing.grdDetail, "Job Detail"
'grdDetail.LoadLayout App.Path & "\layouts\grdDetail.grd"

MsgBox Err.Description

Exit Sub

End Sub

Private Sub grdJobListing_BtnClick()
cmdCompleteJob_Click
End Sub

Sub ShowDetailForJob(lp_lngJobID As Long)

    On Error GoTo ShowDetailForJob_Error

    Dim l_strSQL As String
    
    l_strSQL = "SELECT * FROM jobdetail WHERE jobID = " & lp_lngJobID & " ORDER BY fd_orderby, jobdetailID"
    
    adoJobDetail.RecordSource = l_strSQL
    adoJobDetail.ConnectionString = g_strConnection
    adoJobDetail.Refresh
        
    If g_optUseClipID = 0 Then
        frmJobListing.grdDetail.Columns("clipID").Visible = False
        frmJobListing.grdDetail.Columns("emailto").Visible = False
    End If
    
    If g_intDisableDetailedDubbingCompletion = 1 Then
        frmJobListing.grdDetail.Columns("completeddate").Visible = False
        frmJobListing.grdDetail.Columns("completeduser").Visible = False
    End If

    If g_optHideSoundChannelsOnVTDetailGrid = 1 Then
        frmJobListing.grdDetail.Columns("ch1").Visible = False
        frmJobListing.grdDetail.Columns("ch2").Visible = False
        frmJobListing.grdDetail.Columns("ch3").Visible = False
        frmJobListing.grdDetail.Columns("ch4").Visible = False
    End If
    
    
PROC_Close:
    
  
    
    Exit Sub
    
ShowDetailForJob_Error:
    
    MsgBox Err.Description, vbExclamation
    Resume PROC_Close
    
End Sub
Private Sub grdJobListing_DblClick()

cmdViewJob_Click

End Sub

Private Sub grdJobListing_HeadClick(ByVal ColIndex As Integer)

If grdJobListing.Columns(ColIndex).Caption = "Dubbing Requirements" Then
    MsgBox "You can not order by this field", vbInformation
    Exit Sub
End If

If cmbOrderBy.Text <> "project." & grdJobListing.Columns(ColIndex).DataField And cmbOrderBy.Text <> "job." & grdJobListing.Columns(ColIndex).DataField Then
    cmbOrderBy.Text = "job." & grdJobListing.Columns(ColIndex).DataField
Else
    cmbDirection.ListIndex = 1 - cmbDirection.ListIndex
End If

cmdRefresh_Click
End Sub

Private Sub grdJobListing_InitColumnProps()

If chkMPC_Columns.Value = vbChecked Then
    grdJobListing.Columns("allocatedto").DataField = "ourcontact"
    grdJobListing.Columns("allocatedto").Caption = "Our Contact"
Else
    grdJobListing.Columns("allocatedto").DataField = "allocatedto"
    grdJobListing.Columns("allocatedto").Caption = "Allocated To"
End If


On Error Resume Next


'core functionality disabled
If g_intDisableProjects = 1 Then
    grdJobListing.Columns("projectname").Visible = False
End If

If g_intDisableProducts = 1 Then
    grdJobListing.Columns("productname").Visible = False
End If

If g_intDisableSchedule = 1 Then
    grdJobListing.Columns("startdate").Visible = False
    grdJobListing.Columns("enddate").Visible = False
    grdJobListing.Columns("resourcesbooked").Visible = False
End If

If g_intDisableAllocatedTo = 1 Then
    grdJobListing.Columns("allocatedto").Visible = False
End If


End Sub

Private Sub grdJobListing_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
If Button = 2 Then
    Dim l_lngJobID As Long
    l_lngJobID = grdJobListing.Columns("jobID").Text
    If l_lngJobID <> -1 Then
        PopupMenu MDIForm1.mnuHiddenActionItems
    End If
End If
End Sub

Private Sub grdJobListing_RowLoaded(ByVal Bookmark As Variant)

On Error GoTo grdJobListing_Error

If grdJobListing.Rows < 1 Then Exit Sub

grdJobListing.Columns("Status").CellStyleSet grdJobListing.Columns("Status").Text

If grdJobListing.Columns("jobID").Text <> "" Then
    Dim l_strTextToAdd As String
    l_strTextToAdd = GetJobDescription(grdJobListing.Columns("jobID").Text)
    grdJobListing.Columns("requirements").Text = l_strTextToAdd
    
End If

If grdJobListing.Columns("vtstartdate").Text <> "" Then
    grdJobListing.Columns("requirements").CellStyleSet "vtstart"
    grdJobListing.Columns("startdate").CellStyleSet "vtstart"
    grdJobListing.Columns("enddate").CellStyleSet "vtstart"
    grdJobListing.Columns("productname").CellStyleSet "vtstart"
    grdJobListing.Columns("vtstartdate").CellStyleSet "vtstart"
    grdJobListing.Columns("vtstartuser").CellStyleSet "vtstart"
End If

Dim l_strResourcesBooked As String
If chkShowResources.Value = 1 Then
    l_strResourcesBooked = GetJobDescription(Val(grdJobListing.Columns("jobID").Text))
    grdJobListing.Columns("resourcesbooked").Text = l_strResourcesBooked
End If

Exit Sub

grdJobListing_Error:

MsgBox Err.Description
Exit Sub

End Sub


Public Sub tabFilter_Click(PreviousTab As Integer)
Select Case tabFilter.Tab
Case 0
    ShowJobListing "outstanding"
Case 1
    ShowJobListing "completed."
Case 2
    ShowJobListing "invoiced"
Case 3
    ShowJobListing "credited"
Case 4
    ShowJobListing "senttoaccounts"
Case 5
    ShowJobListing "nocharge"
Case 6
    ShowJobListing "quote"
Case 7
    ShowJobListing "abandoned"
Case 8
    ShowJobListing "all"
End Select

End Sub

Private Sub tabReplacement_Click(PreviousTab As Integer)
ShowJobListing LCase(tabReplacement.TabCaption(tabReplacement.Tab))
Form_Resize
End Sub

Private Sub timAutoRefresh_Timer()

'trying this out to see if it stops the error jennifer @ MPC was getting
On Error Resume Next

If chkAutoRefresh.Value = 0 Then Exit Sub

m_lngTimer = m_lngTimer + 1
'Me.Caption = "Job Listing: " & 60 - m_lngTimer
DoEvents

If m_lngTimer >= 60 And chkAutoRefresh.Value = 1 And (frmJobListing.tabReplacement.Caption = "Dubbing Control" Or frmJobListing.tabReplacement.Caption = "Tech Jobs") Then
    adoJobListing.Refresh
    m_lngTimer = 0
End If

End Sub


