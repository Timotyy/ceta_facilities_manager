VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmJobStatusSelect 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Job Status Selector"
   ClientHeight    =   2145
   ClientLeft      =   7710
   ClientTop       =   8775
   ClientWidth     =   12165
   Icon            =   "frmJobStatusSelect.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2145
   ScaleWidth      =   12165
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdStatus 
      Caption         =   "Returned"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   2
      Left            =   2520
      TabIndex        =   14
      Top             =   1740
      Visible         =   0   'False
      Width           =   1100
   End
   Begin VB.CommandButton cmdStatus 
      Caption         =   "Despatched"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   1
      Left            =   1380
      TabIndex        =   13
      Top             =   1740
      Visible         =   0   'False
      Width           =   1100
   End
   Begin VB.CommandButton cmdStatus 
      Caption         =   "Prepared"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   240
      TabIndex        =   12
      Top             =   1740
      Visible         =   0   'False
      Width           =   1100
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8760
      TabIndex        =   2
      ToolTipText     =   "Close this form"
      Top             =   1740
      Width           =   1155
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7500
      TabIndex        =   1
      ToolTipText     =   "Confirm job status"
      Top             =   1740
      Width           =   1155
   End
   Begin MSComctlLib.Slider sldStatus 
      Height          =   510
      Left            =   360
      TabIndex        =   3
      ToolTipText     =   "Use the slider to change this jobs status"
      Top             =   600
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   900
      _Version        =   393216
      LargeChange     =   1
      Max             =   11
      SelStart        =   1
      Value           =   1
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "Submitted"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   1020
      TabIndex        =   19
      Top             =   1080
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "Pending"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   6
      Left            =   6240
      TabIndex        =   18
      Top             =   1080
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "Scheduled"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Index           =   4
      Left            =   4200
      TabIndex        =   17
      Top             =   1080
      Width           =   915
   End
   Begin VB.Label lblStatus 
      Caption         =   "Original Status:"
      Height          =   255
      Left            =   360
      TabIndex        =   16
      Top             =   180
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "Sent To Accounts"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   11
      Left            =   11220
      TabIndex        =   15
      Top             =   1080
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "Costed"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Index           =   10
      Left            =   10380
      TabIndex        =   11
      Top             =   1080
      Width           =   675
   End
   Begin VB.Label lblOriginalStatus 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1560
      TabIndex        =   0
      ToolTipText     =   "Original Status"
      Top             =   180
      Width           =   2835
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "In Progress"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   5
      Left            =   5160
      TabIndex        =   10
      Top             =   1080
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "Hold Cost"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Index           =   9
      Left            =   9240
      TabIndex        =   9
      Top             =   1080
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "Completed"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   8
      Left            =   8220
      TabIndex        =   8
      Top             =   1080
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "VT Done"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   7
      Left            =   7260
      TabIndex        =   7
      Top             =   1080
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "Masters Here"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   3
      Left            =   3180
      TabIndex        =   6
      Top             =   1080
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "Confirmed"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   2040
      TabIndex        =   5
      Top             =   1080
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "Pencil"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   180
      TabIndex        =   4
      Top             =   1080
      Width           =   735
   End
End
Attribute VB_Name = "frmJobStatusSelect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdCancel_Click()
Me.Tag = "xcancel"
Me.Hide

End Sub

Private Sub cmdOK_Click()

Me.Tag = lblCaption(sldStatus.Value).Caption
Me.Hide

End Sub

Private Sub cmdStatus_Click(Index As Integer)
Me.Tag = cmdStatus(Index).Caption
Me.Hide
End Sub

Private Sub Form_Load()

CenterForm Me
lblCaption(0).ForeColor = g_lngColourPencil
lblCaption(1).ForeColor = g_lngColour2ndPencil
lblCaption(2).ForeColor = g_lngColourConfirmed
lblCaption(3).ForeColor = g_lngColourMastersHere
lblCaption(4).ForeColor = g_lngColourScheduled
lblCaption(5).ForeColor = g_lngColourInProgress
lblCaption(6).ForeColor = g_lngColourOnHold
lblCaption(7).ForeColor = g_lngColourVTDone
lblCaption(8).ForeColor = g_lngColourCompleted
lblCaption(9).ForeColor = g_lngColourHoldCost
lblCaption(10).ForeColor = g_lngColourCosted
lblCaption(11).ForeColor = g_lngColourSentToAccounts

End Sub

