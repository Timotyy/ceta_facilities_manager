VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmCompleteJobSingle 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Complete VT Details"
   ClientHeight    =   6945
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   12885
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6945
   ScaleWidth      =   12885
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdInsert 
      Caption         =   "Insert"
      Height          =   315
      Left            =   11640
      TabIndex        =   20
      Top             =   4920
      Width           =   1155
   End
   Begin VB.TextBox txtOperator 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      Height          =   1155
      Left            =   4740
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   19
      Top             =   5280
      Width           =   8055
   End
   Begin VB.TextBox txtOperatorComment 
      Appearance      =   0  'Flat
      Height          =   675
      Left            =   4740
      TabIndex        =   18
      Top             =   4200
      Width           =   8055
   End
   Begin VB.CommandButton cmdLoadJobDetails 
      Caption         =   "LOAD JOB DETAILS"
      Height          =   375
      Left            =   1080
      TabIndex        =   15
      Top             =   7260
      Visible         =   0   'False
      Width           =   1935
   End
   Begin MSAdodcLib.Adodc adoJobDetail 
      Height          =   270
      Left            =   120
      Top             =   4200
      Width           =   4560
      _ExtentX        =   8043
      _ExtentY        =   476
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   0
      BackColor       =   16744576
      ForeColor       =   16777215
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Description here...."
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox txtQuantity 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   255
      Index           =   1
      Left            =   1620
      TabIndex        =   13
      Top             =   5700
      Width           =   1455
   End
   Begin VB.CheckBox chkConversion 
      Alignment       =   1  'Right Justify
      Caption         =   "Conversion"
      Height          =   195
      Left            =   3240
      TabIndex        =   7
      Top             =   5520
      Width           =   1155
   End
   Begin VB.TextBox txtQuantity 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   255
      Index           =   0
      Left            =   1620
      TabIndex        =   6
      Top             =   5340
      Width           =   1455
   End
   Begin VB.CheckBox chkArc 
      Alignment       =   1  'Right Justify
      Caption         =   "&ARC"
      Height          =   195
      Left            =   3240
      TabIndex        =   5
      Top             =   4620
      Width           =   1155
   End
   Begin VB.CheckBox chkFlash 
      Alignment       =   1  'Right Justify
      Caption         =   "&Flash"
      Height          =   195
      Left            =   3240
      TabIndex        =   4
      Top             =   4920
      Width           =   1155
   End
   Begin VB.CheckBox chkLegal 
      Alignment       =   1  'Right Justify
      Caption         =   "&Legal"
      Height          =   195
      Left            =   3240
      TabIndex        =   3
      Top             =   5220
      Width           =   1155
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdJobDetail 
      Bindings        =   "frmCompleteJobSingle.frx":0000
      Height          =   3975
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   12675
      _Version        =   196617
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   22357
      _ExtentY        =   7011
      _StockProps     =   79
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   315
      Left            =   11640
      TabIndex        =   1
      Top             =   6540
      Width           =   1155
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Complete"
      Height          =   315
      Left            =   10380
      TabIndex        =   0
      Top             =   6540
      Width           =   1155
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbStockType 
      Height          =   255
      Left            =   1620
      TabIndex        =   8
      ToolTipText     =   "Our contact on this job"
      Top             =   4980
      Width           =   1455
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BevelColorFrame =   -2147483644
      CheckBox3D      =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   4868
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   2566
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.Label lblJobDetailID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   1080
      TabIndex        =   17
      Top             =   8100
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Label lblJobID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   1080
      TabIndex        =   16
      Top             =   7680
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Line Line1 
      Index           =   2
      X1              =   300
      X2              =   4500
      Y1              =   6060
      Y2              =   6060
   End
   Begin VB.Line Line1 
      Index           =   1
      X1              =   4500
      X2              =   4500
      Y1              =   4440
      Y2              =   6060
   End
   Begin VB.Line Line1 
      Index           =   0
      X1              =   300
      X2              =   300
      Y1              =   4440
      Y2              =   6060
   End
   Begin VB.Label lblCaption 
      Caption         =   "Clients"
      Height          =   195
      Index           =   0
      Left            =   420
      TabIndex        =   14
      Top             =   5700
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Stock type"
      Height          =   195
      Index           =   12
      Left            =   420
      TabIndex        =   12
      Top             =   4980
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Ours"
      Height          =   195
      Index           =   5
      Left            =   420
      TabIndex        =   11
      Top             =   5340
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Format"
      Height          =   195
      Index           =   7
      Left            =   420
      TabIndex        =   10
      Tag             =   "fO"
      Top             =   4620
      Width           =   915
   End
   Begin VB.Label lblFormat 
      Caption         =   "FORMAT"
      Height          =   255
      Left            =   1620
      TabIndex        =   9
      Top             =   4620
      Width           =   1455
   End
End
Attribute VB_Name = "frmCompleteJobSingle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub adoJobDetail_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

If Not adoJobDetail.Recordset.EOF And Not adoJobDetail.Recordset.BOF Then
    adoJobDetail.Caption = adoJobDetail.Recordset("description")
    lblFormat.Caption = adoJobDetail.Recordset("format")
End If

End Sub

Private Sub cmdCancel_Click()

Me.Tag = "CANCELLED"
Me.Hide

End Sub

Private Sub cmdLoadJobDetails_Click()

Dim l_strSQL As String

l_strSQL = "SELECT copytype, description, quantity, format, videostandard, runningtime, aspectratio, timecode FROM jobdetail WHERE jobID = '" & lblJobID.Caption & "' ORDER BY fd_orderby;"
adoJobDetail.ConnectionString = g_strConnection
adoJobDetail.RecordSource = l_strSQL
adoJobDetail.Refresh


End Sub

