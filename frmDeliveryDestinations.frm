VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmDeliveryDestinations 
   Caption         =   "Delivery Destinations"
   ClientHeight    =   10635
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   27105
   Icon            =   "frmDeliveryDestinations.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   10635
   ScaleWidth      =   27105
   WindowState     =   2  'Maximized
   Begin VB.Frame Frame2 
      Height          =   4275
      Left            =   19620
      TabIndex        =   5
      Top             =   120
      Width           =   7335
      Begin VB.Label lblInstructions 
         Caption         =   "Any other entry in this column will be ignored"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   7
         Left            =   60
         TabIndex        =   13
         Top             =   2100
         Width           =   6255
      End
      Begin VB.Label lblInstructions 
         Caption         =   "Any other entry in this column will cause a subfolder to be made with that name"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   6
         Left            =   60
         TabIndex        =   12
         Top             =   1140
         Width           =   6615
      End
      Begin VB.Label lblInstructions 
         Caption         =   "{movielabs_key} will cause any non .xml files to be placed in an an additional /resources subfolder"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   5
         Left            =   60
         TabIndex        =   11
         Top             =   1860
         Width           =   7155
      End
      Begin VB.Label lblInstructions 
         Caption         =   "Destination Secondary Folder:"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   4
         Left            =   60
         TabIndex        =   10
         Top             =   1560
         Width           =   6135
      End
      Begin VB.Label lblInstructions 
         Caption         =   "{clipreference} will add a subfolder with the reference of the clips being sent"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   3
         Left            =   60
         TabIndex        =   9
         Top             =   900
         Width           =   6675
      End
      Begin VB.Label lblInstructions 
         Caption         =   "{DateTime} will add a subfolder with today's date and time as YYYY-MM-DD_HH:NN:SS"
         ForeColor       =   &H000000FF&
         Height          =   315
         Index           =   2
         Left            =   60
         TabIndex        =   8
         Top             =   660
         Width           =   6975
      End
      Begin VB.Label lblInstructions 
         Caption         =   "{Date} will add a subfolder with today's date in YYYY-MM-DD format"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   1
         Left            =   60
         TabIndex        =   7
         Top             =   420
         Width           =   6135
      End
      Begin VB.Label lblInstructions 
         Caption         =   "Destination Subfolder:"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   0
         Left            =   60
         TabIndex        =   6
         Top             =   120
         Width           =   6135
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnDeliveryType 
      Height          =   1755
      Left            =   3360
      TabIndex        =   4
      Top             =   2100
      Width           =   3135
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   3200
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "Description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   5530
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   240
      TabIndex        =   1
      Top             =   5520
      Width           =   12315
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11100
         TabIndex        =   3
         ToolTipText     =   "Close this form"
         Top             =   120
         Width           =   1155
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Refresh"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9840
         TabIndex        =   2
         ToolTipText     =   "Close this form"
         Top             =   120
         Width           =   1155
      End
   End
   Begin MSAdodcLib.Adodc adoDeliveryDestinations 
      Height          =   330
      Left            =   60
      Top             =   120
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoDeliveryDestinations"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDestinations 
      Bindings        =   "frmDeliveryDestinations.frx":08CA
      Height          =   4515
      Left            =   60
      TabIndex        =   0
      ToolTipText     =   "The Options on a Combo Box"
      Top             =   120
      Width           =   19410
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      BackColorOdd    =   16777152
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   6
      Columns(0).Width=   2699
      Columns(0).Caption=   "DigitalDestinationID"
      Columns(0).Name =   "DigitalDestinationID"
      Columns(0).DataField=   "DigitalDestinationID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3440
      Columns(1).Caption=   "Destination Type"
      Columns(1).Name =   "DestinationType"
      Columns(1).DataField=   "DestinationType"
      Columns(1).FieldLen=   255
      Columns(2).Width=   4868
      Columns(2).Caption=   "Destination Name"
      Columns(2).Name =   "DestinationName"
      Columns(2).DataField=   "DestinationName"
      Columns(2).FieldLen=   50
      Columns(3).Width=   9393
      Columns(3).Caption=   "Destination DropFolder"
      Columns(3).Name =   "DestinationDropfolder"
      Columns(3).DataField=   "DestinationDropfolder"
      Columns(3).FieldLen=   256
      Columns(4).Width=   6641
      Columns(4).Caption=   "Destination Subfolder"
      Columns(4).Name =   "DestinationSubfolder"
      Columns(4).DataField=   "DestinationSubfolder"
      Columns(4).FieldLen=   256
      Columns(5).Width=   6112
      Columns(5).Caption=   "Destination Secondary Folder"
      Columns(5).Name =   "DestinationSecondaryFolder"
      Columns(5).DataField=   "DestinationSecondaryFolder"
      Columns(5).FieldLen=   256
      _ExtentX        =   34237
      _ExtentY        =   7964
      _StockProps     =   79
      Caption         =   "Digital Destinations"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmDeliveryDestinations"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdSearch_Click()

Dim l_strSQL As String

PopulateCombo "DigitalDestinationType", ddnDeliveryType
grdDestinations.Columns("DestinationType").DropDownHwnd = ddnDeliveryType.hWnd

l_strSQL = "SELECT * FROM DigitalDestination"

l_strSQL = l_strSQL & " WHERE 1=1 "

l_strSQL = l_strSQL & "  ORDER BY DestinationType, DestinationName"

adoDeliveryDestinations.ConnectionString = g_strConnection
adoDeliveryDestinations.RecordSource = l_strSQL
adoDeliveryDestinations.Refresh

End Sub

Private Sub Form_Load()

cmdSearch.Value = True

End Sub

Private Sub Form_Resize()

Frame1.Top = Me.ScaleHeight - Frame1.Height - 120
Frame1.Left = Me.ScaleWidth - Frame1.Width - 120

grdDestinations.Height = Me.ScaleHeight - Frame1.Height - 120 - 120
'grdBBCAlias.Width = Me.ScaleWidth - 120 - 120

End Sub

