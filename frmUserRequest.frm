VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmUserRequest 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "My User Requests"
   ClientHeight    =   8130
   ClientLeft      =   2925
   ClientTop       =   4005
   ClientWidth     =   12015
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8130
   ScaleWidth      =   12015
   WindowState     =   2  'Maximized
   Begin VB.PictureBox picHeader 
      Align           =   1  'Align Top
      Height          =   615
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   11955
      TabIndex        =   1
      Top             =   0
      Width           =   12015
      Begin VB.CommandButton cmdRefresh 
         Caption         =   "Refresh"
         Height          =   315
         Left            =   4200
         TabIndex        =   5
         Top             =   120
         Width           =   1155
      End
      Begin VB.CheckBox chkCompleted 
         Caption         =   "Show Completed"
         Height          =   195
         Left            =   1980
         TabIndex        =   4
         Top             =   120
         Width           =   1635
      End
      Begin VB.OptionButton optShowWhat 
         Caption         =   "All Requests"
         Height          =   195
         Index           =   1
         Left            =   240
         TabIndex        =   3
         Top             =   300
         Width           =   1335
      End
      Begin VB.OptionButton optShowWhat 
         Caption         =   "My Requests"
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   2
         Top             =   60
         Value           =   -1  'True
         Width           =   1335
      End
   End
   Begin MSAdodcLib.Adodc adoRequests 
      Height          =   330
      Left            =   1380
      Top             =   6780
      Visible         =   0   'False
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoRequests"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdRequests 
      Align           =   1  'Align Top
      Bindings        =   "frmUserRequest.frx":0000
      Height          =   6255
      Left            =   0
      TabIndex        =   0
      Top             =   615
      Width           =   12015
      _Version        =   196617
      AllowUpdate     =   0   'False
      BackColorOdd    =   12648384
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   21193
      _ExtentY        =   11033
      _StockProps     =   79
      Caption         =   "Comments"
   End
End
Attribute VB_Name = "frmUserRequest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdRefresh_Click()

adoRequests.ConnectionString = g_strConnection

Dim l_strSQL As String

l_strSQL = "SELECT usercommentID AS 'ID', cdate AS 'Created', cuser AS 'User', versionadded AS 'Version', comment AS 'Comment',  area AS 'Area', severity AS 'Severity',commenttype AS 'Type', completeddate AS 'Completed', completedversion AS 'Fixed', completedcomment AS 'Result', admincomment AS 'Admin Comment' FROM usercomment WHERE usercommentID > 0 "

If optShowWhat(0).Value = True Then
    l_strSQL = l_strSQL & " AND cuser = '" & g_strUserInitials & "'"
End If

If chkCompleted.Value = 0 Then
    l_strSQL = l_strSQL & " AND completeddate IS NULL"
End If

l_strSQL = l_strSQL & " ORDER BY cdate DESC"

adoRequests.RecordSource = l_strSQL
adoRequests.Refresh

grdRequests.Columns(0).Width = 600
grdRequests.Columns(1).Width = 1000
grdRequests.Columns(2).Width = 500
grdRequests.Columns(3).Width = 700
grdRequests.Columns(4).Width = 5000
grdRequests.Columns(5).Width = 1000
grdRequests.Columns(6).Width = 1000
grdRequests.Columns(7).Width = 1000
grdRequests.Columns(8).Width = 1000
grdRequests.Columns(9).Width = 700
grdRequests.Columns(10).Width = 3200

grdRequests.RowHeight = 660

End Sub

Private Sub Form_Load()
cmdRefresh_Click
End Sub

Private Sub Form_Resize()
On Error Resume Next
grdRequests.Height = Me.ScaleHeight - picHeader.Height
End Sub


Private Sub grdRequests_DblClick()

If CheckAccess("/completerequests", True) Then

    
    Dim l_lngTaskID As Long
    Dim l_strVersion  As String
    Dim l_intMsg As Integer
    Dim l_strEmailTo As String
    Dim l_strRequestedBy As String
    Dim l_strSQL As String
    Dim l_strbody As String
    Dim l_strRequest As String
    
    l_lngTaskID = Val(grdRequests.Columns("ID").Text)
    
    If l_lngTaskID = 0 Then Exit Sub
    
    l_strRequest = grdRequests.Columns("Comment").Text
    
   
    'prompt for the version number it was fixed in
    
    l_strVersion = App.Major & "." & App.Minor & "." & App.Revision

    frmRequestComplete.txtvVersion = l_strVersion
    
    frmRequestComplete.txtComment.Text = grdRequests.Columns("Result").Text
    frmRequestComplete.txtUserComment.Text = grdRequests.Columns("Comment").Text
    frmRequestComplete.cmbArea.Text = grdRequests.Columns("Area").Text
    frmRequestComplete.cmbSeverity.Text = grdRequests.Columns("Severity").Text
    frmRequestComplete.cmbType.Text = grdRequests.Columns("Type").Text
    frmRequestComplete.lblCreatedOn.Caption = grdRequests.Columns("Created").Text
    frmRequestComplete.lblCreatedBy.Caption = grdRequests.Columns("User").Text
    frmRequestComplete.lblUserRequestID.Caption = l_lngTaskID
    
    frmRequestComplete.txtAdminComment.Text = grdRequests.Columns("Admin Comment").Text
    
    
    frmRequestComplete.Show vbModal
    
    l_strVersion = frmRequestComplete.txtvVersion.Text
    l_strComment = frmRequestComplete.txtComment.Text
    
    Select Case frmRequestComplete.Tag
    Case "CANCELLED"
        l_strVersion = ""
        Unload frmRequestComplete
        Set frmRequestComplete = Nothing
        Exit Sub
    Case "SAVED"
        adoRequests.Refresh
        Unload frmRequestComplete
        Set frmRequestComplete = Nothing
        Exit Sub
    Case "UNABLE"
        l_strVersion = "N/A"
        l_strComment = "We were unable to reproduce this fault, and so it is assumed fixed. This task was marked as closed. Please resubmit this request if there is still an issue."
    Case "INCOMPLETE"
        l_strVersion = "N/A"
        l_strComment = "This task was marked as incomplete and has been closed. Please resubmit this request if you wish."
    Case "VOID"
        l_strVersion = "N/A"
        l_strComment = "This task was marked as void and has been closed. Please resubmit this request if you wish."
    End Select
       
    
    Unload frmRequestComplete
    Set frmRequestComplete = Nothing
    
    
    
    l_strEmailTo = GetData("cetauser", "email", "initials", grdRequests.Columns("User").Text)
    l_strRequestedBy = GetData("cetauser", "fullname", "initials", grdRequests.Columns("User").Text)
    
    
    
    l_strSQL = "UPDATE usercomment SET completeddate = '" & FormatSQLDate(Now) & "', completeduser = '" & g_strUserInitials & "', completedversion = '" & QuoteSanitise(l_strVersion) & "', completedcomment = '" & QuoteSanitise(l_strComment) & "' WHERE usercommentID = '" & l_lngTaskID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    
    
    l_strbody = "Dear " & l_strRequestedBy & "," & vbCrLf
    l_strbody = l_strbody & vbCrLf & "A request you have entered in to the F1 log has been completed."
    l_strbody = l_strbody & vbCrLf & "The changes may not be available straight away, as they were only applied from version " & l_strVersion & "."
    l_strbody = l_strbody & vbCrLf
    l_strbody = l_strbody & vbCrLf & "If you find that the request has not been correctly completed, please contact us at " & g_strCETAAdministratorEmail & " and quote the ID number #" & l_lngTaskID & "."
    l_strbody = l_strbody & vbCrLf
    l_strbody = l_strbody & vbCrLf & "-----  Solution  -----"
    l_strbody = l_strbody & vbCrLf
    l_strbody = l_strbody & vbCrLf & l_strComment
    l_strbody = l_strbody & vbCrLf
    l_strbody = l_strbody & vbCrLf & "---- Your Request ----"
    l_strbody = l_strbody & vbCrLf
    l_strbody = l_strbody & vbCrLf & l_strRequest
    l_strbody = l_strbody & vbCrLf
    l_strbody = l_strbody & vbCrLf
    
    'send the person who created this an email to say it is done
    SendSMTPMail l_strEmailTo, l_strRequestedBy, "CFM Task Completed (" & l_lngTaskID & ")", "", l_strbody, False, g_strUserEmailAddress, g_strFullUserName
    
    SendSMTPMail g_strCETAAdministratorEmail, "CETA Administrator", "CC: CFM Task Completed (" & l_lngTaskID & ")", "", l_strbody, False, "", ""
    
    'refresh the list
    'adoRequests.Refresh
End If
End Sub

Private Sub grdRequests_HeadClick(ByVal ColIndex As Integer)
On Error Resume Next
adoRequests.Recordset.Sort = grdRequests.Columns(ColIndex).DataField
End Sub
