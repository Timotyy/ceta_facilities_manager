VERSION 5.00
Begin VB.Form frmExcelRead 
   Caption         =   "Process Excel Orders"
   ClientHeight    =   5010
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   14535
   LinkTopic       =   "Excel Orders"
   ScaleHeight     =   5010
   ScaleWidth      =   14535
   Begin VB.CommandButton cmdNewLionIngestRequest 
      Caption         =   "Lion DADC Ingest"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3720
      TabIndex        =   46
      Top             =   1560
      Width           =   1995
   End
   Begin VB.CommandButton cmdLibraryShelves 
      Caption         =   "Library Shelves"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1560
      TabIndex        =   45
      Top             =   2040
      Width           =   1995
   End
   Begin VB.CommandButton cmdDADCIngestRequestClearout 
      Caption         =   "BBC DADC Clearout"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3720
      TabIndex        =   44
      Top             =   3960
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdESIXDCAMDeliveryLog 
      Caption         =   "ESI XDCAM Del Report"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3720
      TabIndex        =   43
      Top             =   7200
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdReadBackUnivesal 
      Caption         =   "Read Back Universal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7920
      TabIndex        =   42
      Top             =   6420
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdESIDeliveryLog 
      Caption         =   "ESI DOND Del Report"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3720
      TabIndex        =   41
      Top             =   6780
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdBFIDeliveries 
      Caption         =   "BFI Delivery Report"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10020
      TabIndex        =   40
      Top             =   1560
      Width           =   1995
   End
   Begin VB.CommandButton cmdLMHiTunesTV 
      Caption         =   "LMH iTunes Data"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5820
      TabIndex        =   39
      Top             =   1560
      Width           =   1995
   End
   Begin VB.CommandButton cmdBBCMGTapesFromDump 
      Caption         =   "BBCMG Tapes From Dump"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11940
      TabIndex        =   38
      Top             =   7080
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdBBCMGTapesFromCRM 
      Caption         =   "BBCMG Tapes From CRM"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11940
      TabIndex        =   37
      Top             =   6600
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdMTARead 
      Caption         =   "MTA"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10020
      TabIndex        =   36
      Top             =   2040
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdReadMattelDataTrackerData 
      Caption         =   "Mattel (Barbie)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9180
      TabIndex        =   35
      Top             =   7140
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdBritishFilmInstitute 
      Caption         =   "BFI Data Dump"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10020
      TabIndex        =   34
      Top             =   1080
      Width           =   1995
   End
   Begin VB.CommandButton cmdBBCPremTracker 
      Caption         =   "BBC Prem Content Track"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8220
      TabIndex        =   33
      Top             =   7800
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdHattrickTunesTV 
      Caption         =   "Hattrick iTunes Data"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   960
      TabIndex        =   32
      Top             =   5400
      Width           =   1995
   End
   Begin VB.CommandButton cmdGSN_DPP 
      Caption         =   "GSN DPP Sheet"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8220
      TabIndex        =   31
      Top             =   8280
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdBulkVideoAssetXMLRead 
      Height          =   375
      Left            =   3720
      MaskColor       =   &H00808080&
      Picture         =   "frmExcelRead.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   30
      Top             =   2520
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdDADCBulkAudioIngest 
      Caption         =   "BBC DADC Bulk Audio"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3720
      TabIndex        =   29
      Top             =   2040
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdDisneyAmazon 
      Caption         =   "Disney Amazon Data"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5820
      TabIndex        =   28
      Top             =   2040
      Width           =   1995
   End
   Begin VB.CommandButton cmdDisneyiTunesTV 
      Caption         =   "Disney iTunes Data"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5820
      TabIndex        =   27
      Top             =   1080
      Width           =   1995
   End
   Begin VB.CommandButton cmdESITrackerRead 
      Caption         =   "ESI Tracker Data Import"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3720
      TabIndex        =   26
      Top             =   6300
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdDADCBBCPurgeList 
      Caption         =   "BBC DADC Purge List"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3720
      TabIndex        =   25
      Top             =   3480
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdGoogle 
      Caption         =   "Google Tracker Sheet"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7920
      TabIndex        =   24
      Top             =   1080
      Width           =   1995
   End
   Begin VB.CommandButton cmdHDDtask 
      Caption         =   "BBCW HDD Task"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3720
      TabIndex        =   23
      Top             =   3000
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdOffAir 
      Caption         =   "Process WW Offairs"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1320
      TabIndex        =   22
      Top             =   7380
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdBBCPremAmazon 
      Caption         =   "BBC Premium Amazon"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5820
      TabIndex        =   21
      Top             =   2520
      Width           =   1995
   End
   Begin VB.CommandButton cmdBBCXboxData 
      Caption         =   "BBC Premium Xbox"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5820
      TabIndex        =   20
      Top             =   3000
      Width           =   1995
   End
   Begin VB.CommandButton cmdPatheKeywords 
      Caption         =   "Pathe Keyword Update 1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6120
      TabIndex        =   19
      Top             =   7080
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdNewDADCIngestRequest 
      Caption         =   "BBC DADC Ingest"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3720
      TabIndex        =   18
      Top             =   1080
      Width           =   1995
   End
   Begin VB.CommandButton cmdMoMediaiTunesTV 
      Caption         =   "Momedia iTunes Data"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3480
      TabIndex        =   17
      Top             =   5280
      Width           =   1995
   End
   Begin VB.CommandButton cmdDisneyTracker 
      Caption         =   "Disney Xbox EIDR Sheets"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7920
      TabIndex        =   16
      Top             =   1560
      Width           =   1995
   End
   Begin VB.CommandButton cmdNorthernIrelandScreen 
      Caption         =   "Northern Ireland Screen"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3720
      TabIndex        =   15
      Top             =   7860
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdBBCMGMetadata 
      Caption         =   "BBCMG Metadata Sheet"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11940
      TabIndex        =   14
      Top             =   6120
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdUniversityOfLeeds 
      Caption         =   "University of Leeds"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1200
      TabIndex        =   13
      Top             =   6840
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdDisneyMO 
      Caption         =   "Disney MO Data"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11640
      TabIndex        =   12
      Top             =   8280
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdDespatchBarcodes 
      Caption         =   "Despatch Barcodes"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1560
      TabIndex        =   11
      Top             =   1560
      Width           =   1995
   End
   Begin VB.CommandButton cmdDHCourier 
      Caption         =   "Global Courier Charges"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6120
      TabIndex        =   10
      Top             =   5880
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdLibraryMovements 
      Caption         =   "Library Movements"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1560
      TabIndex        =   9
      Top             =   1080
      Width           =   1995
   End
   Begin VB.CommandButton cmdSPESpottersSheet 
      Caption         =   "SPE Spotters Guide"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1260
      TabIndex        =   6
      Top             =   7800
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdTradBBCWW 
      Caption         =   "Process Trad BBCWW"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6120
      TabIndex        =   5
      Top             =   8280
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdSelectFile 
      Caption         =   "Select Excel File"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1560
      TabIndex        =   4
      Top             =   120
      Width           =   1995
   End
   Begin VB.CommandButton cmdInclBBCWW 
      Caption         =   "Process Incl BBCWW"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6120
      TabIndex        =   3
      Top             =   7800
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.TextBox txtExcelFilename 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1440
      TabIndex        =   1
      Top             =   600
      Width           =   12795
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   12120
      TabIndex        =   0
      Top             =   1080
      Width           =   1995
   End
   Begin VB.Label lblProgress 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   480
      TabIndex        =   8
      Top             =   4560
      Width           =   13695
   End
   Begin VB.Label lblFileTitle 
      Height          =   255
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Visible         =   0   'False
      Width           =   5055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Filename"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   300
      TabIndex        =   2
      Top             =   660
      Width           =   915
   End
End
Attribute VB_Name = "frmExcelRead"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_lngCommacount As Long

'declare a variable to hold which comma we are on
Dim m_DADC_xml As DADC_Structure
Dim m_lngCurrentComma As Long

Private Sub cmdBBCMGMetadata_Click()

'have a variable to hold the filename we are going to process
Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub
    
End If

'open the spreadsheet

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "BBCMG Metadata Read", "report1524214302878")

Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "BBCMG Metadata Read", 2))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

Dim l_strBarcode As String, l_strTitle As String, l_strProgramID As String, l_strTXDate As String, l_strSynopsis As String, l_strDetail As String
Dim l_datTXDate As Date
Dim l_strSQL As String

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_strProgramID = Trim(GetXLData(l_lngXLRowCounter, 1)) 'A
    l_strBarcode = Trim(GetXLData(l_lngXLRowCounter, 2)) 'B
    l_strTitle = Trim(GetXLData(l_lngXLRowCounter, 3)) 'C
    l_strTXDate = Trim(GetXLData(l_lngXLRowCounter, 4)) 'D
    l_strSynopsis = Trim(GetXLData(l_lngXLRowCounter, 5)) 'E
    l_strDetail = Trim(GetXLData(l_lngXLRowCounter, 6)) 'F
    
    If IsDate(l_strTXDate) Then l_datTXDate = CDate(l_strTXDate) Else l_datTXDate = 0

    lblProgress.Caption = l_lngXLRowCounter & " - Prog:" & l_strProgramID & ", Barcode: " & l_strBarcode
    DoEvents
    
    If GetDataSQL("SELECT TOP 1 Metadata_ID FROM BBCMG_Metadata WHERE Program_ID = '" & QuoteSanitise(l_strProgramID) & "' AND Media_ID = '" & l_strBarcode & "'") = "" Then
        lblProgress.Caption = lblProgress.Caption & ", new line"
        DoEvents
        l_strSQL = "INSERT INTO BBCMG_Metadata (Program_ID, Media_ID, Program_Title, Annotation, Cataloguing, TX_Date) VALUES ("
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strProgramID) & "', "
        l_strSQL = l_strSQL & "'" & l_strBarcode & "', "
        l_strSQL = l_strSQL & IIf(LCase(l_strTitle) <> "null", "'" & QuoteSanitise(l_strTitle) & "', ", "Null, ")
        l_strSQL = l_strSQL & IIf(LCase(l_strSynopsis) <> "null", "'" & QuoteSanitise(l_strSynopsis) & "', ", "Null, ")
        l_strSQL = l_strSQL & IIf(LCase(l_strDetail) <> "null", "'" & QuoteSanitise(l_strDetail) & "', ", "Null, ")
        l_strSQL = l_strSQL & IIf(l_datTXDate <> 0, "'" & Format(l_datTXDate, "yyyy-mm-dd") & "'", "Null") & ") "
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    Else
        lblProgress.Caption = lblProgress.Caption & ", update"
        DoEvents
        l_strSQL = "UPDATE BBCMG_Metadata SET "
        l_strSQL = l_strSQL & IIf(LCase(l_strTitle) <> "null", "Program_Title = '" & QuoteSanitise(l_strTitle) & "', ", "Program_Title = Null, ")
        l_strSQL = l_strSQL & IIf(l_datTXDate <> 0, "TX_Date = '" & Format(l_datTXDate, "yyyy-mm-dd") & "', ", "TX_Date = Null, ")
        l_strSQL = l_strSQL & IIf(LCase(l_strSynopsis) <> "null", "Annotation = '" & QuoteSanitise(l_strSynopsis) & "', ", "Annotation = Null, ")
        l_strSQL = l_strSQL & IIf(LCase(l_strDetail) <> "null", "Cataloguing = '" & QuoteSanitise(l_strDetail) & "' ", "Cataloguing = Null ")
        l_strSQL = l_strSQL & "WHERE Program_ID = '" & QuoteSanitise(l_strProgramID) & "' AND Media_ID = '" & l_strBarcode & "'"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Loop

lblProgress.Caption = ""

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

MsgBox "Done"

End Sub

Private Sub cmdBBCMGPurgeList_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strSQL As String
Dim l_lngMediaKey As Long
Dim l_strStatus As String, l_lngCurrentStatus As Long, l_lngStatusPurgeable As Long, l_lngStatusPurgeQueue As Long, l_lngStatusArchiveQueue As Long

l_lngStatusPurgeable = GetData("BBCMG_Media_Status", "Media_Status_ID", "Media_Status", "Purgeable")
l_lngStatusPurgeQueue = GetData("BBCMG_Media_Status", "Media_Status_ID", "Media_Status", "PurgeQueue")
l_lngStatusArchiveQueue = GetData("BBCMG_Media_Status", "Media_Status_ID", "Media_Status", "ArchiveQueue")

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "Sheet1")

If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "DADC Excel Read", 3))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_lngMediaKey = GetXLData(l_lngXLRowCounter, 1)
    lblProgress.Caption = l_lngMediaKey
    DoEvents
    l_strStatus = GetXLData(l_lngXLRowCounter, 13)
    If l_strStatus = "Purge" Then
        l_lngCurrentStatus = GetData("BBCMG_Media_Key", "Media_Status_ID", "Media_Key_ID", l_lngMediaKey)
        If l_lngCurrentStatus = l_lngStatusPurgeable Or l_lngCurrentStatus = l_lngStatusArchiveQueue Then
            SetData "BBCMG_Media_Key", "Media_Status_ID", "Media_Key_Id", l_lngMediaKey, l_lngStatusPurgeQueue
        End If
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Loop

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
MsgBox "Done"

End Sub

Private Sub ParseThroughNodes(ByRef Nodes As MSXML2.IXMLDOMNodeList)

Dim xNode As MSXML2.IXMLDOMNode

For Each xNode In Nodes
    If xNode.nodeType = NODE_TEXT Then
        Select Case xNode.parentNode.nodeName
            Case "a:externalalphakey"
                m_DADC_xml.CVCode = xNode.nodeValue
            Case "a:displaycode"
                m_DADC_xml.KitID = xNode.nodeValue
        End Select
    End If
    
    If xNode.hasChildNodes Then
        ParseThroughNodes xNode.childNodes
    End If
Next xNode

End Sub

Private Sub cmdBBCMGTapesFromCRM_Click()

'have a variable to hold the filename we are going to process
Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strPreExistingList As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub
    
End If

'open the spreadsheet

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "BBCMG Metadata Read", "Sheet1")

Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "BBCMG Metadata Read", 2))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

Dim l_strBarcode As String, l_strTitle As String, l_strProgramID As String, l_strTXDate As String, l_strSynopsis As String, l_strDetail As String, l_strVideoFormat As String, l_lngLibraryID As Long
Dim l_datTXDate As Date
Dim l_strSQL As String

l_strPreExistingList = ""
Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_strProgramID = Trim(GetXLData(l_lngXLRowCounter, 8))
    l_strBarcode = Trim(GetXLData(l_lngXLRowCounter, 10))
    l_strTitle = Trim(GetXLData(l_lngXLRowCounter, 7))
    l_strTXDate = Trim(GetXLData(l_lngXLRowCounter, 9))
    l_strVideoFormat = "UMATIC"
    
    lblProgress.Caption = l_lngXLRowCounter & " - Prog:" & l_strProgramID & ", Barcode: " & l_strBarcode & ", Title: " & l_strTitle
    DoEvents
    
    If Val(Trim(" " & GetData("library", "LibraryID", "barcode", l_strBarcode))) = 0 Then
        l_lngLibraryID = CreateLibraryItem(l_strBarcode, Left(l_strTitle, 255), 0, GetNextSequence("InternalReference"), 924, False, "", "", GetAlias(l_strVideoFormat), "", l_strTXDate)
    Else
        l_strPreExistingList = l_strPreExistingList & "Barcode: " & l_strBarcode & vbCrLf
    End If

    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Loop

SendSMTPMail "thart@visualdatamedia.com", "Tim Hart", "BBMG Pre-Existing Tapes", "", l_strPreExistingList, False, "", ""

lblProgress.Caption = ""

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

MsgBox "Done"

End Sub

Private Sub cmdBBCMGTapesFromDump_Click()

'have a variable to hold the filename we are going to process
Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strPreExistingList As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub
    
End If

'open the spreadsheet

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "BBCMG Metadata Read", "Sheet1")

Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "BBCMG Metadata Read", 2))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

Dim l_strBarcode As String, l_strTitle As String, l_strProgramID As String, l_strTXDate As String, l_strSynopsis As String, l_strDetail As String, l_strVideoFormat As String, l_lngLibraryID As Long
Dim l_datTXDate As Date
Dim l_strSQL As String

l_strPreExistingList = ""
Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_strProgramID = Trim(GetXLData(l_lngXLRowCounter, 2))
    l_strBarcode = Trim(GetXLData(l_lngXLRowCounter, 3))
    l_strTitle = Trim(GetXLData(l_lngXLRowCounter, 1))
    l_strTXDate = ""
    l_strVideoFormat = "UMATIC"
    
    lblProgress.Caption = l_lngXLRowCounter & " - Prog:" & l_strProgramID & ", Barcode: " & l_strBarcode & ", Title: " & l_strTitle
    DoEvents
    
    If Val(Trim(" " & GetData("library", "LibraryID", "barcode", l_strBarcode))) = 0 Then
        l_lngLibraryID = CreateLibraryItem(l_strBarcode, Left(l_strTitle, 255), 0, GetNextSequence("InternalReference"), 924, False, "", "", GetAlias(l_strVideoFormat), "", l_strTXDate)
    Else
        l_strPreExistingList = l_strPreExistingList & "Barcode: " & l_strBarcode & vbCrLf
    End If

    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Loop

SendSMTPMail "thart@visualdatamedia.com", "Tim Hart", "BBMG Pre-Existing Tapes", "", l_strPreExistingList, False, "", ""

lblProgress.Caption = ""

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

MsgBox "Done"

End Sub

Private Sub cmdBBCPremAmazon_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_lngTemp As Long, l_strSQL As String, l_lngPackageID As Long
Dim l_lngCount As Long, l_strTemp As String
Dim l_lngColOriginal_Spoken_Locale As Long
Dim l_lngColUnique_ID As Long
Dim l_lngColUniqueSeriesID As Long
Dim l_lngColUniqueSeasonID As Long
Dim l_lngColSeason_Number As Long
Dim l_lngColLanguage As Long
Dim l_lngColTerritory As Long
Dim l_lngColTitle As Long
Dim l_lngColShortSynopsis As Long
Dim l_lngColLongSynopsis As Long
Dim l_lngColSeasonTitle As Long
Dim l_lngColSeasonProductionCompany As Long
Dim l_lngColGenre As Long
Dim l_lngColSeason_Unique_ID As Long
Dim l_lngColEpisode_Unique_ID As Long
Dim l_lngColEpisodeTitle As Long
Dim l_lngColSequence As Long
Dim l_lngColRatingType As Long
Dim l_lngColRating As Long
Dim l_lngColOriginalAirDate As Long
Dim l_lngColSDOfferStart As Long
Dim l_lngColSDVODOfferStart As Long
Dim l_lngColHDOfferStart As Long
Dim l_lngColHDVODOfferStart As Long
Dim l_lngColCopyright As Long
Dim l_lngColCopyrightYear As Long
Dim l_lngColContentType As Long
Dim l_lngColCountryOfOrigin As Long

Dim l_strOriginal_Spoken_Locale As String
Dim l_strUnique_ID As String
Dim l_strUniqueSeriesID As String
Dim l_strUniqueSeasonID As String
Dim l_strSeason_Number As String
Dim l_strLanguage As String
Dim l_strTerritory As String
Dim l_strTitle As String
Dim l_strShortSynopsis As String
Dim l_strLongSynopsis As String
Dim l_strSeasonProductionCompany As String
Dim l_strGenre As String
Dim l_strSeason_Unique_ID As String
Dim l_strEpisode_Unique_ID As String
Dim l_strEpisodeTitle As String
Dim l_strSequence As String
Dim l_strRatingType As String
Dim l_strRating As String
Dim l_datOriginalAirDate As Date
Dim l_datSDOfferStart As Date
Dim l_datSDVODOfferStart As Date
Dim l_datHDOfferStart As Date
Dim l_datHDVODOfferStart As Date
Dim l_strCopyright As String
Dim l_strCopyrightYear As String
Dim l_strContentType As String
Dim l_strCountryOfOrigin As String
Dim l_blnFound As Boolean

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = "Metadata Template"
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

For l_lngCount = 1 To 53
    l_strTemp = GetXLData(1, l_lngCount)
    Select Case l_strTemp
        Case "Content Type" 'C
            l_lngColContentType = l_lngCount
        Case "OriginalLanguage" 'AB
            l_lngColOriginal_Spoken_Locale = l_lngCount
        Case "Unique ID" 'B
            l_lngColUnique_ID = l_lngCount
        Case "Series ID"
            l_lngColUniqueSeriesID = l_lngCount
        Case "Season ID"
            l_lngColUniqueSeasonID = l_lngCount
        Case "Season Sequence Number"
            l_lngColSeason_Number = l_lngCount
        Case "Language"
            l_lngColLanguage = l_lngCount
        Case "CountryOfOrigin"
            l_lngColCountryOfOrigin = l_lngCount
        Case "Territory"
            l_lngColTerritory = l_lngCount
        Case "Title"
            l_lngColTitle = l_lngCount
        Case "Short Synopsis"
            l_lngColShortSynopsis = l_lngCount
        Case "Long Synopsis"
            l_lngColLongSynopsis = l_lngCount
        Case "Producers"
            l_lngColSeasonProductionCompany = l_lngCount
        Case "Genre"
            l_lngColGenre = l_lngCount
        Case "Episode Sequence Number"
            l_lngColSequence = l_lngCount
        Case "Rating System"
            l_lngColRatingType = l_lngCount
        Case "Rating"
            l_lngColRating = l_lngCount
        Case "Release Date", "First Aired Date"
            l_lngColOriginalAirDate = l_lngCount
        Case "SD EST - Start"
            l_lngColSDOfferStart = l_lngCount
        Case "HD EST - Start"
            l_lngColHDOfferStart = l_lngCount
        Case "SD VOD - Start"
            l_lngColSDVODOfferStart = l_lngCount
        Case "HD VOD - Start"
            l_lngColHDVODOfferStart = l_lngCount
        Case "Copyright Holder"
            l_lngColCopyright = l_lngCount
        Case "Copyright Year"
            l_lngColCopyrightYear = l_lngCount
            
    End Select
Next

l_lngXLRowCounter = 3

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    If GetXLData(l_lngXLRowCounter, l_lngColContentType) = "TV Episode" Then
        If l_lngColOriginal_Spoken_Locale <> 0 Then l_strOriginal_Spoken_Locale = Trim(GetXLData(l_lngXLRowCounter, l_lngColOriginal_Spoken_Locale)) Else l_strOriginal_Spoken_Locale = ""
        If l_lngColUnique_ID <> 0 Then l_strUnique_ID = Trim(GetXLData(l_lngXLRowCounter, l_lngColUnique_ID)) Else l_strUnique_ID = ""
        If l_lngColUniqueSeriesID <> 0 Then l_strUniqueSeriesID = Trim(GetXLData(l_lngXLRowCounter, l_lngColUniqueSeriesID)) Else l_strUniqueSeriesID = ""
        If l_lngColUniqueSeasonID <> 0 Then l_strUniqueSeasonID = Trim(GetXLData(l_lngXLRowCounter, l_lngColUniqueSeasonID)) Else l_strUniqueSeasonID = ""
        If l_lngColLanguage <> 0 Then l_strLanguage = Trim(GetXLData(l_lngXLRowCounter, l_lngColLanguage)) Else l_strLanguage = ""
        If l_lngColCountryOfOrigin <> 0 Then l_strCountryOfOrigin = Trim(GetXLData(l_lngXLRowCounter, l_lngColCountryOfOrigin)) Else l_strCountryOfOrigin = ""
        If l_lngColTerritory <> 0 Then l_strTerritory = Trim(GetXLData(l_lngXLRowCounter, l_lngColTerritory)) Else l_strTerritory = ""
        If l_lngColTitle <> 0 Then l_strTitle = Trim(GetXLData(l_lngXLRowCounter, l_lngColTitle)) Else l_strTitle = ""
        If l_lngColShortSynopsis <> 0 Then l_strShortSynopsis = Trim(GetXLData(l_lngXLRowCounter, l_lngColShortSynopsis)) Else l_strShortSynopsis = ""
        If l_lngColLongSynopsis <> 0 Then l_strLongSynopsis = Trim(GetXLData(l_lngXLRowCounter, l_lngColLongSynopsis)) Else l_strLongSynopsis = ""
        If l_lngColGenre <> 0 Then l_strGenre = Trim(GetXLData(l_lngXLRowCounter, l_lngColGenre)) Else l_strGenre = ""
        If l_lngColSequence <> 0 Then l_strSequence = Trim(GetXLData(l_lngXLRowCounter, l_lngColSequence)) Else l_strSequence = ""
        If l_lngColRatingType <> 0 Then l_strRatingType = Trim(GetXLData(l_lngXLRowCounter, l_lngColRatingType)) Else l_strRatingType = ""
        If l_lngColRating <> 0 Then l_strRating = Trim(GetXLData(l_lngXLRowCounter, l_lngColRating)) Else l_lngColRating = ""
        If l_lngColOriginalAirDate <> 0 Then l_datOriginalAirDate = Trim(GetXLData(l_lngXLRowCounter, l_lngColOriginalAirDate)) Else l_datOriginalAirDate = 0
        If l_lngColSDOfferStart <> 0 Then
            If Trim(GetXLData(l_lngXLRowCounter, l_lngColSDOfferStart)) <> "" Then
                l_datSDOfferStart = Trim(GetXLData(l_lngXLRowCounter, l_lngColSDOfferStart))
            Else
                l_datSDOfferStart = 0
            End If
        Else
            l_datSDOfferStart = 0
        End If
        If l_lngColHDOfferStart <> 0 Then
            If Trim(GetXLData(l_lngXLRowCounter, l_lngColHDOfferStart)) <> "" Then
                l_datHDOfferStart = Trim(GetXLData(l_lngXLRowCounter, l_lngColHDOfferStart))
            Else
                l_datHDOfferStart = 0
            End If
        Else
            l_datHDOfferStart = 0
        End If
        If l_lngColSDVODOfferStart <> 0 Then
            If Trim(GetXLData(l_lngXLRowCounter, l_lngColSDVODOfferStart)) <> "" Then
                l_datSDVODOfferStart = Trim(GetXLData(l_lngXLRowCounter, l_lngColSDVODOfferStart))
            Else
                l_datSDVODOfferStart = 0
            End If
        Else
            l_datSDVODOfferStart = 0
        End If
        If l_lngColHDVODOfferStart <> 0 Then
            If Trim(GetXLData(l_lngXLRowCounter, l_lngColHDVODOfferStart)) <> "" Then
                l_datHDVODOfferStart = Trim(GetXLData(l_lngXLRowCounter, l_lngColHDVODOfferStart))
            Else
                l_datHDVODOfferStart = 0
            End If
        Else
            l_datHDVODOfferStart = 0
        End If
        If l_lngColCopyright <> 0 Then l_strCopyright = Trim(GetXLData(l_lngXLRowCounter, l_lngColCopyright)) Else l_strCopyright = ""
        If l_lngColCopyrightYear <> 0 Then l_strCopyrightYear = Trim(GetXLData(l_lngXLRowCounter, l_lngColCopyrightYear)) Else l_strCopyrightYear = ""
    
        l_strSQL = "INSERT INTO iTunes_Package (companyID, video_type) VALUES (1552, 'Amazon');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_lngPackageID = g_lngLastID
        
'        l_strLanguage = GetData("iso639language", "iso639Language_2", "LanguageNameEnglish", l_strLanguage)
        If l_strOriginal_Spoken_Locale = "en" Then l_strOriginal_Spoken_Locale = "en-GB"
        If l_strLanguage = "en" And l_strOriginal_Spoken_Locale = "en-GB" Then l_strLanguage = "en-GB"
        If l_strLanguage = "de" Then l_strLanguage = "de-DE"
        l_strSQL = "UPDATE iTunes_Package SET "
        l_strSQL = l_strSQL & "originalspokenlocale = '" & l_strOriginal_Spoken_Locale & "', "
        l_strSQL = l_strSQL & "AmazonCountryOfOrigin = '" & l_strCountryOfOrigin & "', "
        l_strSQL = l_strSQL & "video_language = '" & l_strLanguage & "', "
        l_strSQL = l_strSQL & "country = '" & l_strTerritory & "', "
        l_strSQL = l_strSQL & "AmazonGenre = '" & l_strGenre & "', "
        l_strSQL = l_strSQL & "video_vendorID = '" & l_strUnique_ID & "', "
        l_strSQL = l_strSQL & "AmazonSeriesUniqueID = '" & l_strUniqueSeriesID & "', "
        l_strSQL = l_strSQL & "AmazonSeasonUniqueID = '" & l_strUniqueSeasonID & "', "
        l_strSQL = l_strSQL & "video_title = '" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "AmazonSequence = '" & l_strSequence & "', "
        l_strSQL = l_strSQL & "AmazonRatingType = '" & l_strRatingType & "', "
        l_strSQL = l_strSQL & "AmazonRating = '" & IIf(Left(l_strRating, 4) = "FSK ", Mid(l_strRating, 5), l_strRating) & "', "
        l_strSQL = l_strSQL & "AmazonOriginalAirDate = '" & Format(l_datOriginalAirDate, "YYYY-MM-DD") & "', "
        If l_datSDOfferStart <> 0 Then l_strSQL = l_strSQL & "AmazonSDOfferStart = '" & Format(l_datSDOfferStart, "YYYY-MM-DD") & "', " Else l_strSQL = l_strSQL & "AmazonSDOfferStart = Null, "
        If l_datHDOfferStart <> 0 Then l_strSQL = l_strSQL & "AmazonHDOfferStart = '" & Format(l_datHDOfferStart, "YYYY-MM-DD") & "', " Else l_strSQL = l_strSQL & "AmazonHDOfferStart = Null, "
        If l_datSDVODOfferStart <> 0 Then l_strSQL = l_strSQL & "AmazonSDVODOfferStart = '" & Format(l_datSDVODOfferStart, "YYYY-MM-DD") & "', " Else l_strSQL = l_strSQL & "AmazonSDVODOfferStart = Null, "
        If l_datHDVODOfferStart <> 0 Then l_strSQL = l_strSQL & "AmazonHDVODOfferStart = '" & Format(l_datHDVODOfferStart, "YYYY-MM-DD") & "', " Else l_strSQL = l_strSQL & "AmazonHDVODOfferStart = Null, "
        l_strSQL = l_strSQL & "AmazonCopyrightYear = '" & l_strCopyrightYear & "', "
        l_strSQL = l_strSQL & "AmazonCopyrightHolder = '" & QuoteSanitise(l_strCopyright) & "', "
        l_strSQL = l_strSQL & "AmazonEpisodeShortSynopsis = '" & QuoteSanitise(l_strShortSynopsis) & "', "
        l_strSQL = l_strSQL & "AmazonEpisodeLongSynopsis = '" & QuoteSanitise(l_strLongSynopsis) & "' "
        l_strSQL = l_strSQL & "WHERE iTunes_PackageID = " & l_lngPackageID
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    If GetXLData(l_lngXLRowCounter, 1) = "" Then l_lngXLRowCounter = l_lngXLRowCounter + 1
Loop

l_lngXLRowCounter = 3

Do While GetXLData(l_lngXLRowCounter, 1) <> ""

    If GetXLData(l_lngXLRowCounter, l_lngColContentType) = "TV Season" Then
    
        If l_lngColUnique_ID <> 0 Then l_strUnique_ID = Trim(GetXLData(l_lngXLRowCounter, l_lngColUnique_ID)) Else l_strUnique_ID = ""
        If l_lngColTitle <> 0 Then l_strTitle = Trim(GetXLData(l_lngXLRowCounter, l_lngColTitle)) Else l_strTitle = ""
        If l_lngColSeasonProductionCompany <> 0 Then l_strSeasonProductionCompany = Trim(GetXLData(l_lngXLRowCounter, l_lngColSeasonProductionCompany)) Else l_strSeasonProductionCompany = ""
        If l_lngColSeason_Number <> 0 Then l_strSeason_Number = Trim(GetXLData(l_lngXLRowCounter, l_lngColSeason_Number)) Else l_strSeason_Number = ""
        If l_lngColShortSynopsis <> 0 Then l_strShortSynopsis = Trim(GetXLData(l_lngXLRowCounter, l_lngColShortSynopsis)) Else l_strShortSynopsis = ""
        If l_lngColLongSynopsis <> 0 Then l_strLongSynopsis = Trim(GetXLData(l_lngXLRowCounter, l_lngColLongSynopsis)) Else l_strLongSynopsis = ""
    
        l_strSQL = "UPDATE iTunes_Package SET "
        l_strSQL = l_strSQL & "AmazonSeasonTitle = '" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "AmazonSeasonProductionCompany = '" & QuoteSanitise(l_strSeasonProductionCompany) & "', "
        l_strSQL = l_strSQL & "AmazonSeasonNumber = '" & QuoteSanitise(l_strSeason_Number) & "', "
        l_strSQL = l_strSQL & "AmazonSeasonShortSynopsis = '" & QuoteSanitise(l_strShortSynopsis) & "', "
        l_strSQL = l_strSQL & "AmazonSeasonLongSynopsis = '" & QuoteSanitise(l_strLongSynopsis) & "' "
        l_strSQL = l_strSQL & "WHERE video_type = 'Amazon' AND companyID = 1552 AND AmazonSeasonUniqueID = '" & l_strUnique_ID & "';"
    
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    If GetXLData(l_lngXLRowCounter, 1) = "" Then l_lngXLRowCounter = l_lngXLRowCounter + 1
Loop
    
l_lngXLRowCounter = 3

Do While GetXLData(l_lngXLRowCounter, 1) <> ""

    If GetXLData(l_lngXLRowCounter, l_lngColContentType) = "TV Series" Then
    
        If l_lngColUnique_ID <> 0 Then l_strUnique_ID = Trim(GetXLData(l_lngXLRowCounter, l_lngColUnique_ID)) Else l_strUnique_ID = ""
        If l_lngColTitle <> 0 Then l_strTitle = Trim(GetXLData(l_lngXLRowCounter, l_lngColTitle)) Else l_strTitle = ""
        If l_lngColSeason_Number <> 0 Then l_strSeason_Number = Trim(GetXLData(l_lngXLRowCounter, l_lngColSeason_Number)) Else l_strSeason_Number = ""
        If l_lngColShortSynopsis <> 0 Then l_strShortSynopsis = Trim(GetXLData(l_lngXLRowCounter, l_lngColShortSynopsis)) Else l_strShortSynopsis = ""
        
        l_strSQL = "UPDATE iTunes_Package SET "
        l_strSQL = l_strSQL & "AmazonSeriesName = '" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "AmazonSeriesShortSynopsis = '" & QuoteSanitise(l_strShortSynopsis) & "', "
        l_strSQL = l_strSQL & "AmazonSeriesLongSynopsis = '" & QuoteSanitise(l_strLongSynopsis) & "' "
        l_strSQL = l_strSQL & "WHERE video_type = 'Amazon' AND companyID = 1552 AND AmazonSeriesUniqueID = '" & l_strUnique_ID & "';"
    
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    If GetXLData(l_lngXLRowCounter, 1) = "" Then l_lngXLRowCounter = l_lngXLRowCounter + 1
Loop

lblProgress.Caption = ""

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
MsgBox "Done"

End Sub

Private Sub cmdBBCPremTracker_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_strSQL As String, l_lngTrackerID As Long, l_lngRunningTime As Long, l_rst As ADODB.Recordset, l_curFileSize As Currency
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strClient As String, l_strTitle As String, l_strEpisode As String, l_strPO As String, l_strCategory As String, l_strDuration As String, l_strDeliverable As String, l_strDueDate As String, l_strCompleted As String, l_strVersion As String
Dim l_lngFirstColumn As Long

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
oExcel.Visible = True
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets(InputBox("Tab name", "Tab name", "Sheet1"))

l_lngXLRowCounter = Val(InputBox("First Row", "First Row", "2"))
l_lngFirstColumn = Val(InputBox("Number of First Active Column", "Number of First Active Column", "2")) - 1

Do While GetXLData(l_lngXLRowCounter, l_lngFirstColumn + 1) <> ""
    
    l_strClient = GetXLData(l_lngXLRowCounter, l_lngFirstColumn + 1)
    l_strTitle = GetXLData(l_lngXLRowCounter, l_lngFirstColumn + 2)
    l_strEpisode = GetXLData(l_lngXLRowCounter, l_lngFirstColumn + 3)
    l_strPO = GetXLData(l_lngXLRowCounter, l_lngFirstColumn + 4)
    l_strCategory = GetXLData(l_lngXLRowCounter, l_lngFirstColumn + 5)
    l_strVersion = GetXLData(l_lngXLRowCounter, l_lngFirstColumn + 6)
    l_strDuration = GetXLData(l_lngXLRowCounter, l_lngFirstColumn + 7)
    l_strDeliverable = GetXLData(l_lngXLRowCounter, l_lngFirstColumn + 8)
    l_strDueDate = GetXLData(l_lngXLRowCounter, l_lngFirstColumn + 9)
    l_strCompleted = GetXLData(l_lngXLRowCounter, l_lngFirstColumn + 10)
    
    lblProgress.Caption = "Line: " & l_lngXLRowCounter
    l_strSQL = "INSERT INTO tracker_item (companyID, headertext2, headertext3, headerint1, headertext4, headertext5, duration, headertext6, headertext7, requiredby, stagefield25) VALUES ("
    l_strSQL = l_strSQL & "1552, "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strClient) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
    l_strSQL = l_strSQL & "'" & Val(l_strEpisode) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strPO) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strCategory) & "', "
    l_strSQL = l_strSQL & "'" & Val(l_strDuration) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strDeliverable) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strVersion) & "', "
    l_strSQL = l_strSQL & IIf(IsDate(l_strDueDate), "'" & Format(l_strDueDate, "YYYY-MM-DD") & "', ", "Null, ")
    l_strSQL = l_strSQL & IIf(IsDate(l_strCompleted), "'" & Format(l_strCompleted, "YYYY-MM-DD") & "'", "Null") & "); "
    
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_lngTrackerID = g_lngLastID
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Loop

lblProgress.Caption = ""
Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

MsgBox "Done"

End Sub

Private Sub cmdBBCXboxData_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_lngTemp As Long, l_strSQL As String, l_lngPackageID As Long
Dim l_lngCount As Long, l_strTemp As String
Dim l_lngColOriginal_Spoken_Locale As Long
Dim l_lngColUnique_ID As Long
Dim l_lngColUniqueSeriesID As Long
Dim l_lngColUniqueSeasonID As Long
Dim l_lngColSeason_Number As Long
Dim l_lngColLanguage As Long
Dim l_lngColTerritory As Long
Dim l_lngColSeriesName As Long
Dim l_lngColSeasonName As Long
Dim l_lngColEpisodeName As Long
Dim l_lngColSeriesShortDescription As Long
Dim l_lngColSeasonShortDescription As Long
Dim l_lngColEpisodeShortDescription As Long
Dim l_lngColSeriesLongDescription As Long
Dim l_lngColSeasonLongDescription As Long
Dim l_lngColEpisodeLongDescription As Long
Dim l_lngColSeasonProductionCompany As Long
Dim l_lngColGenre As Long
Dim l_lngColSeason_Unique_ID As Long
Dim l_lngColEpisode_Unique_ID As Long
Dim l_lngColEpisodeTitle As Long
Dim l_lngColSequence As Long
Dim l_lngColRatingType As Long
Dim l_lngColRating As Long
Dim l_lngColOriginalAirDate As Long
Dim l_lngColSDOfferStart As Long
Dim l_lngColSDVODOfferStart As Long
Dim l_lngColHDOfferStart As Long
Dim l_lngColHDVODOfferStart As Long
Dim l_lngColCopyright As Long
Dim l_lngColCopyrightYear As Long
Dim l_lngColContentType As Long
Dim l_lngColCountryOfOrigin As Long
Dim l_lngColLicensor As Long

Dim l_strOriginal_Spoken_Locale As String
Dim l_strUnique_ID As String
Dim l_strUniqueSeriesID As String
Dim l_strUniqueSeasonID As String
Dim l_strSeason_Number As String
Dim l_strLanguage As String
Dim l_strTerritory As String
Dim l_strSeriesName As String
Dim l_strSeasonName As String
Dim l_strEpisodeName As String
Dim l_strSeriesShortDescription As String
Dim l_strSeasonShortDescription As String
Dim l_strEpisodeShortDescription As String
Dim l_strSeriesLongDescription As String
Dim l_strSeasonLongDescription As String
Dim l_strEpisodeLongDescription As String
Dim l_strSeasonProductionCompany As String
Dim l_strGenre As String
Dim l_strSeason_Unique_ID As String
Dim l_strEpisode_Unique_ID As String
Dim l_strEpisodeTitle As String
Dim l_strSequence As String
Dim l_strRatingType As String
Dim l_strRating As String
Dim l_datOriginalAirDate As Date
Dim l_datSDOfferStart As Date
Dim l_datSDVODOfferStart As Date
Dim l_datHDOfferStart As Date
Dim l_datHDVODOfferStart As Date
Dim l_strCopyright As String
Dim l_strCopyrightYear As String
Dim l_strContentType As String
Dim l_strCountryOfOrigin As String
Dim l_strLicensor As String
Dim l_blnFound As Boolean


l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = "BBCW Xbox Metadata Specificatio"
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

For l_lngCount = 1 To 32
    l_strTemp = GetXLData(2, l_lngCount)
    Select Case l_strTemp
        Case "Episode ID" 'B
            l_lngColUnique_ID = l_lngCount
        Case "Series ID"
            l_lngColUniqueSeriesID = l_lngCount
        Case "Season ID"
            l_lngColUniqueSeasonID = l_lngCount
        Case "Season Number"
            l_lngColSeason_Number = l_lngCount
        Case "Country"
            l_lngColTerritory = l_lngCount
        Case "Series Name"
            l_lngColSeriesName = l_lngCount
        Case "Season Name"
            l_lngColSeasonName = l_lngCount
        Case "Episode Name"
            l_lngColEpisodeName = l_lngCount
        Case "Series Short Desc"
            l_lngColSeriesShortDescription = l_lngCount
        Case "Season Short Desc"
            l_lngColSeasonShortDescription = l_lngCount
        Case "Episode Short Desc"
            l_lngColEpisodeShortDescription = l_lngCount
        Case "Series Long Desc"
            l_lngColSeriesLongDescription = l_lngCount
        Case "Season Long Desc"
            l_lngColSeasonLongDescription = l_lngCount
        Case "Episode Long Desc"
            l_lngColEpisodeLongDescription = l_lngCount
        Case "Network"
            l_lngColSeasonProductionCompany = l_lngCount
        Case "Genres"
            l_lngColGenre = l_lngCount
        Case "Episode Number"
            l_lngColSequence = l_lngCount
        Case "Rating System"
            l_lngColRatingType = l_lngCount
        Case "Rating Value"
            l_lngColRating = l_lngCount
        Case "ReleaseDate"
            l_lngColOriginalAirDate = l_lngCount
        Case "EST HD Start Date"
            l_lngColHDOfferStart = l_lngCount
        Case "Copyright"
            l_lngColCopyright = l_lngCount
        Case "Licensor"
            l_lngColLicensor = l_lngCount
            
    End Select
Next

l_lngXLRowCounter = 3

Do While GetXLData(l_lngXLRowCounter, l_lngColUnique_ID) <> ""
    If l_lngColUnique_ID <> 0 Then l_strUnique_ID = Trim(GetXLData(l_lngXLRowCounter, l_lngColUnique_ID)) Else l_strUnique_ID = ""
    If l_lngColUniqueSeriesID <> 0 Then l_strUniqueSeriesID = Trim(GetXLData(l_lngXLRowCounter, l_lngColUniqueSeriesID)) Else l_strUniqueSeriesID = ""
    If l_lngColUniqueSeasonID <> 0 Then l_strUniqueSeasonID = Trim(GetXLData(l_lngXLRowCounter, l_lngColUniqueSeasonID)) Else l_strUniqueSeasonID = ""
    If l_lngColLanguage <> 0 Then l_strLanguage = Trim(GetXLData(l_lngXLRowCounter, l_lngColLanguage)) Else l_strLanguage = ""
    If l_lngColCountryOfOrigin <> 0 Then l_strCountryOfOrigin = Trim(GetXLData(l_lngXLRowCounter, l_lngColCountryOfOrigin)) Else l_strCountryOfOrigin = ""
    If l_lngColTerritory <> 0 Then l_strTerritory = Trim(GetXLData(l_lngXLRowCounter, l_lngColTerritory)) Else l_strTerritory = ""
    If l_lngColSeriesName <> 0 Then l_strSeriesName = Trim(GetXLData(l_lngXLRowCounter, l_lngColSeriesName)) Else l_strSeriesName = ""
    If l_lngColSeasonName <> 0 Then l_strSeasonName = Trim(GetXLData(l_lngXLRowCounter, l_lngColSeasonName)) Else l_strSeasonName = ""
    If l_lngColEpisodeName <> 0 Then l_strEpisodeName = Trim(GetXLData(l_lngXLRowCounter, l_lngColEpisodeName)) Else l_strEpisodeName = ""
    If l_lngColSeriesShortDescription <> 0 Then l_strSeriesShortDescription = Trim(GetXLData(l_lngXLRowCounter, l_lngColSeriesShortDescription)) Else l_strSeriesShortDescription = ""
    If l_lngColSeasonShortDescription <> 0 Then l_strSeasonShortDescription = Trim(GetXLData(l_lngXLRowCounter, l_lngColSeasonShortDescription)) Else l_strSeasonShortDescription = ""
    If l_lngColEpisodeShortDescription <> 0 Then l_strEpisodeShortDescription = Trim(GetXLData(l_lngXLRowCounter, l_lngColEpisodeShortDescription)) Else l_strEpisodeShortDescription = ""
    If l_lngColSeriesLongDescription <> 0 Then l_strSeriesLongDescription = Trim(GetXLData(l_lngXLRowCounter, l_lngColSeriesLongDescription)) Else l_strSeriesLongDescription = ""
    If l_lngColSeasonLongDescription <> 0 Then l_strSeasonLongDescription = Trim(GetXLData(l_lngXLRowCounter, l_lngColSeasonLongDescription)) Else l_strSeasonLongDescription = ""
    If l_lngColEpisodeLongDescription <> 0 Then l_strEpisodeLongDescription = Trim(GetXLData(l_lngXLRowCounter, l_lngColEpisodeLongDescription)) Else l_strEpisodeLongDescription = ""
    If l_lngColGenre <> 0 Then l_strGenre = Trim(GetXLData(l_lngXLRowCounter, l_lngColGenre)) Else l_strGenre = ""
    If l_lngColSequence <> 0 Then l_strSequence = Trim(GetXLData(l_lngXLRowCounter, l_lngColSequence)) Else l_strSequence = ""
    If l_lngColRatingType <> 0 Then l_strRatingType = Trim(GetXLData(l_lngXLRowCounter, l_lngColRatingType)) Else l_strRatingType = ""
    If l_lngColRating <> 0 Then l_strRating = Trim(GetXLData(l_lngXLRowCounter, l_lngColRating)) Else l_strRating = ""
    If l_lngColLicensor <> 0 Then l_strLicensor = Trim(GetXLData(l_lngXLRowCounter, l_lngColLicensor)) Else l_strLicensor = ""
    If l_lngColSeason_Number <> 0 Then l_strSeason_Number = Trim(GetXLData(l_lngXLRowCounter, l_lngColSeason_Number)) Else l_strSeason_Number = ""
    If l_lngColOriginalAirDate <> 0 Then l_datOriginalAirDate = Trim(GetXLData(l_lngXLRowCounter, l_lngColOriginalAirDate)) Else l_datOriginalAirDate = 0
    If l_lngColHDOfferStart <> 0 Then
        If Trim(GetXLData(l_lngXLRowCounter, l_lngColHDOfferStart)) <> "" Then
            l_datHDOfferStart = Trim(GetXLData(l_lngXLRowCounter, l_lngColHDOfferStart))
        Else
            l_datHDOfferStart = 0
        End If
    Else
        l_datHDOfferStart = 0
    End If
    If l_lngColCopyright <> 0 Then l_strCopyright = Trim(GetXLData(l_lngXLRowCounter, l_lngColCopyright)) Else l_strCopyright = ""

    l_strSQL = "INSERT INTO iTunes_Package (companyID, video_type) VALUES (1552, 'Xbox');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    l_lngPackageID = g_lngLastID
    
'        l_strLanguage = GetData("iso639language", "iso639Language_2", "LanguageNameEnglish", l_strLanguage)
    If l_strOriginal_Spoken_Locale = "en" Then l_strOriginal_Spoken_Locale = "en-GB"
    If l_strLanguage = "en" And l_strOriginal_Spoken_Locale = "en-GB" Then l_strLanguage = "en-GB"
    If l_strLanguage = "de" Then l_strLanguage = "de-DE"
    l_strSQL = "UPDATE iTunes_Package SET "
    l_strSQL = l_strSQL & "originalspokenlocale = '" & l_strOriginal_Spoken_Locale & "', "
    l_strSQL = l_strSQL & "AmazonCountryOfOrigin = '" & l_strCountryOfOrigin & "', "
    l_strSQL = l_strSQL & "video_language = '" & l_strLanguage & "', "
    l_strSQL = l_strSQL & "country = '" & l_strTerritory & "', "
    l_strSQL = l_strSQL & "AmazonGenre = '" & l_strGenre & "', "
    l_strSQL = l_strSQL & "video_vendorID = '" & l_strUnique_ID & "', "
    l_strSQL = l_strSQL & "AmazonSeriesUniqueID = '" & l_strUniqueSeriesID & "', "
    l_strSQL = l_strSQL & "AmazonSeasonUniqueID = '" & l_strUniqueSeasonID & "', "
    l_strSQL = l_strSQL & "video_title = '" & QuoteSanitise(l_strEpisodeName) & "', "
    l_strSQL = l_strSQL & "AmazonSeriesName = '" & QuoteSanitise(l_strSeriesName) & "', "
    l_strSQL = l_strSQL & "AmazonSeasonTitle = '" & QuoteSanitise(l_strSeasonName) & "', "
    l_strSQL = l_strSQL & "AmazonSeriesShortSynopsis = '" & QuoteSanitise(l_strSeriesShortDescription) & "', "
    l_strSQL = l_strSQL & "AmazonSeasonShortSynopsis = '" & QuoteSanitise(l_strSeasonShortDescription) & "', "
    l_strSQL = l_strSQL & "AmazonEpisodeShortSynopsis = '" & QuoteSanitise(l_strEpisodeShortDescription) & "', "
    l_strSQL = l_strSQL & "AmazonSeriesLongSynopsis = '" & QuoteSanitise(l_strSeriesLongDescription) & "', "
    l_strSQL = l_strSQL & "AmazonSeasonLongSynopsis = '" & QuoteSanitise(l_strSeasonLongDescription) & "', "
    l_strSQL = l_strSQL & "AmazonEpisodeLongSynopsis = '" & QuoteSanitise(l_strEpisodeLongDescription) & "', "
    l_strSQL = l_strSQL & "AmazonSequence = '" & l_strSequence & "', "
    l_strSQL = l_strSQL & "AmazonSeasonNumber = '" & l_strSeason_Number & "', "
    l_strSQL = l_strSQL & "AmazonRatingType = '" & l_strRatingType & "', "
    l_strSQL = l_strSQL & "AmazonRating = '" & IIf(Left(l_strRating, 4) = "FSK ", Mid(l_strRating, 5), l_strRating) & "', "
    l_strSQL = l_strSQL & "AmazonOriginalAirDate = '" & Format(l_datOriginalAirDate, "YYYY-MM-DD") & "', "
    If l_datHDOfferStart <> 0 Then l_strSQL = l_strSQL & "AmazonHDOfferStart = '" & Format(l_datHDOfferStart, "YYYY-MM-DD") & "', " Else l_strSQL = l_strSQL & "AmazonHDOfferStart = Null, "
    l_strSQL = l_strSQL & "AmazonCopyrightYear = '" & l_strCopyrightYear & "', "
    l_strSQL = l_strSQL & "AmazonCopyrightHolder = '" & QuoteSanitise(l_strCopyright) & "', "
    l_strSQL = l_strSQL & "XboxLicensor = '" & QuoteSanitise(l_strLicensor) & "' "
    l_strSQL = l_strSQL & "WHERE iTunes_PackageID = " & l_lngPackageID
    
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    If GetXLData(l_lngXLRowCounter, l_lngColUnique_ID) = "" Then l_lngXLRowCounter = l_lngXLRowCounter + 1
Loop

lblProgress.Caption = ""

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
MsgBox "Done"

End Sub

Private Sub cmdBFIDeliveries_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_strSQL As String, l_lngTrackerID As Long, l_lngRunningTime As Long, l_rst As ADODB.Recordset, l_curFileSize As Currency
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strReference As String, l_strFilename As String, l_strTitle  As String, l_strItemType As String, l_strVideoFormat As String, l_strCans As String, l_strAcquisitionSourceType As String, l_strPartOfReference As String
Dim l_strBarcode As String, l_strCanID As String, l_strCrateNumber As String, l_lngCrateNumber As Long, l_strCrateBarcode As String, l_strPRIref As String, l_lngPRIref As Long
Dim l_lngFirstColumn As Long, l_lngLibraryID As Long, l_strObjectNumber As String, l_strPrefix As String, l_strNewBarcode As String
Dim l_datDeliveryDate As Date

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub
 
End If

Set oExcel = CreateObject("Excel.Application")
oExcel.Visible = True
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets("Sheet1")

l_lngXLRowCounter = 11

Do While GetXLData(l_lngXLRowCounter, 3) <> ""
    
    l_strFilename = GetXLDataText(l_lngXLRowCounter, 3)
    If LCase(Right(l_strFilename, 4)) = ".mkv" Then
        l_strFilename = Left(l_strFilename, InStr(LCase(l_strFilename), ".mkv") - 1) ' & ".mov"
    End If
    l_datDeliveryDate = GetXLDataText(l_lngXLRowCounter, 11)
    
    lblProgress.Caption = "Row: " & l_lngXLRowCounter & ", " & l_strFilename & ", finished at " & Format(l_datDeliveryDate, "YYYY-MM-DD HH:NN:SS")
    DoEvents
    
    l_lngTrackerID = Val(Trim(" " & GetDataSQL("SELECT tracker_itemID from tracker_item WHERE companyID = 1244 AND itemreference = '" & Trim(l_strFilename) & "' AND (stagefield6 IS NULL OR stagefield6 = '') AND readytobill = 0;")))
    If l_lngTrackerID <> 0 Then
        l_strSQL = "UPDATE tracker_item SET "
        l_strSQL = l_strSQL & "stagefield6 = '" & Format(l_datDeliveryDate, "YYYY-MM-DD HH:NN:SS") & "', "
        l_strSQL = l_strSQL & "readytobill = 1 "
        l_strSQL = l_strSQL & "WHERE tracker_itemID = " & l_lngTrackerID & ";"
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Loop

lblProgress.Caption = ""
Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

MsgBox "Done"

End Sub

Private Sub cmdDADCIngestRequestClearout_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long, l_lngXLRowCounter2 As Long
Dim l_strBarcode As String, l_strNewBarcode As String, l_strLastBarcode As String, l_strReference As String, l_strTitle As String, l_strSubTitle As String, l_lngEpisode As Long, l_strAlphaBusiness As String
Dim l_lngCRID As Long, l_strFormat As String, l_strLegacyLineStandard As String, l_strImageAspectRatio As String, l_strLegacyPictureElement As String, l_strLegacyFrameRate As String, l_strBBCCoreID As String
Dim l_strLanguage As String, l_strSubtitlesLanguage As String, l_strAudioConfiguration As String, l_strDurationEstimate As String, l_lngLastTrackerID As Long, l_strAudioContent As String
Dim l_strDADCStatus As String, l_datDueDate As Date, l_strDueDate As String, l_strEmailBody As String, l_rstWhoToEmail As ADODB.Recordset, l_lngExternalAlphaKey As Long, l_lngWorkflowID As Long
Dim l_lngMonth As Long, l_lngDay As Long, l_lngYear As Long, l_lngCount1 As Long, l_lngCount2 As Long, l_blnDateChange As Boolean, l_datOldDate As Date, l_lngBatchNumber As Long
Dim l_strAdditionalElements As String, l_strContentVersionCode As String, l_lngContentVersionID As Long, l_strRush As String, l_blnRushChanged As Boolean, l_datNewTargetDate As Date
Dim l_rst As ADODB.Recordset, Count2 As Long, l_strVendorGroup As String, l_strTotalAudio As String, l_strMasterSpec As String, TrackerID As Long, l_strIngestRequestor As String, l_strLegacyOracTVAENumber As String
Dim l_lngClipID As Long, FSO As Scripting.FileSystemObject, l_strPathToFile As String, l_lngLibraryID As Long
Dim l_strEPV_ID As String, l_strIngestFilenameToUse As String, l_lngSeriesID As Long

Dim l_strSQL As String, TempStr As String, Count As Long, l_strTextInPicture As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

If MsgBox("This routine will check the existing lines in the DADC Manual Ingest tracker, against the Selected IR sheet, and all items not found in the current IR sheet will be cancelled." & vbCrLf & vbCrLf & "Do you wish to proceed", vbYesNo + vbDefaultButton2, "DADC Tracker Clearout") = vbNo Then
    Exit Sub
End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'l_strExcelSheetName = GetData("setting", "value", "name", "DADC_IR_Tab_Name")
'If l_strExcelSheetName = "" Then
'    oWorkbook.Close
'    Set oWorkbook = Nothing
'    Set oExcel = Nothing
'    Exit Sub
'End If
'Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)
Set oWorksheet = oWorkbook.Worksheets(1)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "DADC Excel Read", 3))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

l_strLastBarcode = ""
l_strEmailBody = ""

l_strSQL = "DELETE FROM tracker_dadc_clearout;"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_blnDateChange = False
    l_lngCRID = Val(Trim(GetXLData(l_lngXLRowCounter, 2))) 'B
    l_strBarcode = Trim(GetXLData(l_lngXLRowCounter, 30)) 'AD
    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", l_strBarcode) <> 0 Then
        l_strBarcode = GetData("bbc_barcode_correction", "corrected_barcode", "original_barcode", l_strBarcode)
    End If
    l_strNewBarcode = l_strBarcode
    If UCase(Right(l_strBarcode, 3)) = "FIX" Then
        l_strNewBarcode = Left(l_strBarcode, Len(l_strBarcode) - 4)
    ElseIf UCase(Right(l_strBarcode, 4)) = "REDO" Then
        l_strNewBarcode = Left(l_strBarcode, Len(l_strBarcode) - 5)
    End If
    l_strMasterSpec = Trim(GetXLData(l_lngXLRowCounter, 31)) 'AE
    l_strBBCCoreID = Trim(GetXLData(l_lngXLRowCounter, 29)) 'AC
    l_strDADCStatus = Trim(GetXLData(l_lngXLRowCounter, 3)) 'C
    l_strAlphaBusiness = ""
    l_strEPV_ID = Trim(GetXLData(l_lngXLRowCounter, 14)) 'N - the new EPV ID
    l_strContentVersionCode = ""
    l_lngContentVersionID = 0
    l_strVendorGroup = Trim(GetXLData(l_lngXLRowCounter, 76)) 'BX
    l_lngWorkflowID = Val(GetXLData(l_lngXLRowCounter, 53)) 'BA
    l_strTitle = GetXLData(l_lngXLRowCounter, 10) 'J
    l_lngSeriesID = Val(GetXLData(l_lngXLRowCounter, 6)) 'F
    l_strSubTitle = GetXLData(l_lngXLRowCounter, 13) 'M
    l_lngEpisode = Val(GetXLData(l_lngXLRowCounter, 12)) 'L
    l_strFormat = ""
    l_strLegacyFrameRate = GetXLData(l_lngXLRowCounter, 33) 'AG
    l_strLegacyLineStandard = GetXLData(l_lngXLRowCounter, 34) 'AH
    l_strImageAspectRatio = GetXLData(l_lngXLRowCounter, 37) 'AK
    l_strLegacyPictureElement = ""
    l_strAdditionalElements = GetXLData(l_lngXLRowCounter, 44) 'AR
    l_strLanguage = GetXLData(l_lngXLRowCounter, 47) 'AU
    If l_strLanguage = "None" Then l_strLanguage = ""
    l_strAudioConfiguration = GetXLData(l_lngXLRowCounter, 45) 'AS
    l_strAudioContent = GetXLData(l_lngXLRowCounter, 46) 'AT
    l_strDurationEstimate = ""
    l_strTextInPicture = Trim(" " & GetXLData(l_lngXLRowCounter, 51)) 'AY
    l_strRush = Trim(" " & GetXLData(l_lngXLRowCounter, 71)) 'BS
    l_strDueDate = GetXLData(l_lngXLRowCounter, 74) 'BV
    l_strIngestRequestor = GetXLData(l_lngXLRowCounter, 65) 'BM
    l_strLegacyOracTVAENumber = GetXLData(l_lngXLRowCounter, 25) 'Y
    l_strIngestFilenameToUse = GetXLData(l_lngXLRowCounter, 79) 'CA
    
    If LCase(l_strDADCStatus) = "requested" And l_strBarcode <> "" And (InStr(l_strVendorGroup, "MX1") > 0 Or InStr(l_strVendorGroup, "VDMS - London") > 0) Then
    
        l_lngBatchNumber = Format(l_datDueDate, "ddmmyy")
        If l_strIngestFilenameToUse = "" Then
            l_strReference = SanitiseBarcode(l_strBarcode) & "_" & l_strEPV_ID & "_" & l_lngWorkflowID
        Else
            l_strReference = l_strIngestFilenameToUse
        End If
        
        If Not IsDate(l_strDueDate) Then l_strDueDate = ""
        
        lblProgress.Caption = "Row: " & l_lngXLRowCounter & " - " & l_strBarcode & " - " & l_strTitle & " Ep. " & l_lngEpisode
        DoEvents
        
        TempStr = ""
        TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_clearoutID FROM tracker_dadc_clearout WHERE newbarcode='" & UCase(l_strNewBarcode) & "' AND contentversioncode = '" & l_strEPV_ID & "' AND companyID = 1261 AND workflowID = " & l_lngWorkflowID & ";")
        If Val(TempStr) = 0 Then
            TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_clearoutID FROM tracker_dadc_clearout WHERE newbarcode='" & UCase(l_strNewBarcode) & "' AND externalalphaID = '" & l_strAlphaBusiness & "' AND companyID = 1239;")
            l_strSQL = "INSERT INTO tracker_dadc_clearout (cdate, mdate, companyID, IngestRequestor, itemreference, title, series, subtitle, barcode, newbarcode, format, contentversioncode, BBCCoreID, legacylinestandard, legacyframerate, "
            l_strSQL = l_strSQL & "imageaspectratio, legacypictureelement, additionalelementsrequested, language, AudioConfiguration, LegacyOracTVAENumber, "
            l_strSQL = l_strSQL & "subtitleslanguage, fivepointonerequested, dolbyErequested, textinpicture, stereorequested, monorequested, "
            l_strSQL = l_strSQL & "musicandeffectsrequested, episode, dbbcrid, priority, targetdatecountdown, duedateupload, contentversionID, workflowID, durationestimate, batch) VALUES ("
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "1261, "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strIngestRequestor) & "', "
            If l_strReference <> "" Then
                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strReference) & "', "
            Else
                l_strSQL = l_strSQL & "NULL, "
            End If
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
            l_strSQL = l_strSQL & IIf(l_lngSeriesID <> 0, l_lngSeriesID, "Null") & ", "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSubTitle) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(UCase(l_strBarcode)) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(UCase(l_strNewBarcode)) & "', "
            l_strSQL = l_strSQL & "'" & l_strFormat & "', "
'            If l_strContentVersionCode = "" Then
                l_strSQL = l_strSQL & "'" & l_strEPV_ID & "', "
'            Else
'                l_strSQL = l_strSQL & "'" & l_strContentVersionCode & "', "
'            End If
            l_strSQL = l_strSQL & "'" & l_strBBCCoreID & "', "
            l_strSQL = l_strSQL & "'" & l_strLegacyLineStandard & "', "
            l_strSQL = l_strSQL & "'" & l_strLegacyFrameRate & "', "
            l_strSQL = l_strSQL & "'" & l_strImageAspectRatio & "', "
            l_strSQL = l_strSQL & "'" & l_strLegacyPictureElement & "', "
            If l_strAdditionalElements = "Yes" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
            l_strTotalAudio = IIf(InStr(l_strMasterSpec, "Muxed") > 0, "", l_strMasterSpec) & IIf(l_strAudioConfiguration <> "", IIf(InStr(l_strMasterSpec, "Muxed") > 0, "", ", ") & l_strAudioConfiguration, "") & IIf(l_strAudioContent <> "", ", " & l_strAudioContent, "") & IIf(l_strLanguage <> "", ", " & l_strLanguage & vbCrLf, vbCrLf)
            l_strSQL = l_strSQL & "'" & l_strTotalAudio & "', "
            l_strSQL = l_strSQL & "'" & l_strLegacyOracTVAENumber & "', "
            l_strSQL = l_strSQL & "'" & l_strSubtitlesLanguage & "', "
            If l_strAudioConfiguration = "5.1" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            If Left(l_strAudioConfiguration, 7) = "Dolby E" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            l_strSQL = l_strSQL & "'" & l_strTextInPicture & "', "
            If l_strAudioConfiguration = "Standard Stereo" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            If l_strAudioConfiguration = "Mono" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            If l_strAudioContent = "M&E" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            l_strSQL = l_strSQL & IIf(l_lngEpisode <> 0, l_lngEpisode, "Null") & ", "
            l_strSQL = l_strSQL & l_lngCRID & ", "
            If UCase(l_strRush) = "YES" Then
                l_strSQL = l_strSQL & "1, 1, "
            Else
                l_strSQL = l_strSQL & "0, 3, "
            End If
            l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDueDate) & "', "
            l_strSQL = l_strSQL & l_lngContentVersionID & ", "
            l_strSQL = l_strSQL & l_lngWorkflowID & ", "
            l_strSQL = l_strSQL & Int(Val(l_strDurationEstimate)) & ", "
            l_strSQL = l_strSQL & l_lngBatchNumber & ");"
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
        Else
            TrackerID = Val(TempStr)
            TempStr = ""
            l_strSQL = "UPDATE tracker_dadc_clearout SET "
            If l_strReference <> "" Then l_strSQL = l_strSQL & "itemreference = '" & QuoteSanitise(l_strReference) & "', "
            If l_lngSeriesID <> 0 Then l_strSQL = l_strSQL & "series = " & l_lngSeriesID & ", "
            If l_strImageAspectRatio <> "" Then l_strSQL = l_strSQL & "imageaspectratio = '" & l_strImageAspectRatio & "', "
            If l_strLegacyFrameRate <> "" Then l_strSQL = l_strSQL & "legacyframerate = '" & l_strLegacyFrameRate & "', "
            If l_strLegacyLineStandard <> "" Then l_strSQL = l_strSQL & "legacylinestandard = '" & l_strLegacyLineStandard & "', "
            If l_strIngestRequestor <> "" Then l_strSQL = l_strSQL & "IngestRequestor = '" & QuoteSanitise(l_strIngestRequestor) & "', "
            If l_lngExternalAlphaKey <> 0 Then l_strSQL = l_strSQL & "externalalphakey = " & l_lngExternalAlphaKey & ", "
            If l_lngContentVersionID <> 0 Then l_strSQL = l_strSQL & "contentversionID = " & l_lngContentVersionID & ", "
            If l_strBBCCoreID <> "" Then l_strSQL = l_strSQL & "BBCCoreID = '" & l_strBBCCoreID & "', "
            If l_lngWorkflowID <> 0 Then l_strSQL = l_strSQL & "workflowID = " & l_lngWorkflowID & ", "
            l_strSQL = l_strSQL & "dbbcrid = " & l_lngCRID & ", "
            If l_strAudioConfiguration = "5.1" Then l_strSQL = l_strSQL & "fivepointonerequested = 1, " Else l_strSQL = l_strSQL & "fivepointonerequested = 0, "
            If l_strAudioConfiguration = "Standard Stereo" Then l_strSQL = l_strSQL & "stereorequested = 1, " Else l_strSQL = l_strSQL & "stereorequested = 0, "
            If l_strAudioConfiguration = "Mono" Then l_strSQL = l_strSQL & "monorequested = 1, " Else l_strSQL = l_strSQL & "monorequested = 0, "
            If l_strTextInPicture <> "" Then l_strSQL = l_strSQL & "textinpicture = '" & l_strTextInPicture & "', "
            If Left(l_strAudioConfiguration, 7) = "Dolby E" Then l_strSQL = l_strSQL & "dolbyErequested = 1, " Else l_strSQL = l_strSQL & "dolbyErequested = 0, "
            If l_strAudioContent = "M&E" Then l_strSQL = l_strSQL & "musicandeffectsrequested = 1, " Else l_strSQL = l_strSQL & "musicandeffectsrequested = 0, "
            If l_strAdditionalElements = "Yes" Then l_strSQL = l_strSQL & "additionalelementsrequested = 1, " Else l_strSQL = l_strSQL & "additionalelementsrequested = 0, "
            If l_strLanguage <> "" Then l_strSQL = l_strSQL & "language = '" & l_strLanguage & "', "
            l_strTotalAudio = IIf(InStr(l_strMasterSpec, "Muxed") > 0, "", l_strMasterSpec) & IIf(l_strAudioConfiguration <> "", IIf(InStr(l_strMasterSpec, "Muxed") > 0, "", ", ") & l_strAudioConfiguration, "") & IIf(l_strAudioContent <> "", ", " & l_strAudioContent, "") & IIf(l_strLanguage <> "", ", " & l_strLanguage & vbCrLf, vbCrLf)
            If l_strLegacyOracTVAENumber <> "" Then l_strSQL = l_strSQL & "LegacyOracTVAENumber = '" & l_strLegacyOracTVAENumber & "', "
            If InStr(GetData("tracker_dadc_clearout", "audioconfiguration", "tracker_dadc_clearoutID", TrackerID), l_strTotalAudio) <= 0 Then
                l_strSQL = l_strSQL & "audioconfiguration = '" & GetData("tracker_dadc_clearout", "audioconfiguration", "tracker_dadc_clearoutID", TrackerID) & l_strTotalAudio & "', "
            End If
            l_strSQL = l_strSQL & "mdate = '" & FormatSQLDate(Now) & "' "
            l_strSQL = l_strSQL & "WHERE tracker_DADC_clearoutID = " & TrackerID & ";"
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
        End If
        
        l_strLastBarcode = l_strBarcode
        
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Loop

l_strSQL = "SELECT * from tracker_dadc_item where readytobill = 0;"
Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
If l_rst.RecordCount > 0 Then
    l_rst.MoveFirst
    Do While Not l_rst.EOF
        TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_clearoutID FROM tracker_dadc_clearout WHERE itemreference = '" & l_rst("itemreference") & "' AND newbarcode='" & l_rst("newbarcode") & "' AND contentversioncode = '" & l_rst("contentversioncode") & "' AND companyID = 1261 AND workflowID = " & l_rst("workflowID") & ";")
        If TempStr = "" Then
            l_rst("stagefield13") = Now
            l_rst("readytobill") = 1
            l_rst.Update
            l_strEmailBody = l_strEmailBody & "Cancelled: " & l_rst("itemreference") & " - Request ID " & l_rst("dbbcrid") & ", " & l_rst("title") & ", Ep " & l_rst("episode") & ", " & l_rst("subtitle") & " - was not found in latest IR sheet" & vbCrLf
        Else
            'MsgBox "Found"
        End If
        l_rst.MoveNext
    Loop
End If
l_rst.Close
Set l_rst = Nothing

Dim l_strEmailSubject As String

If l_strEmailBody <> "" Then
    
    If InStr(LCase(g_strConnection), "miniceta") > 0 Then
        l_strEmailSubject = "DADC Manual Ingest Tracker (Latimer) Items Cancelled by Omission"
    Else
        l_strEmailSubject = "DADC Manual Ingest Tracker Items Cancelled by Omission"
    End If
    
    l_strEmailBody = "The following items were not found in the current IR sheet, and have been cancelled from the DBB Manual Ingest Tracker: " & vbCrLf & vbCrLf & l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", 1261) & "' AND trackermessageID = 13;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), l_strEmailSubject, "", l_strEmailBody, True, "", "", g_strAdministratorEmailAddress, False, g_strUserEmailAddress
    
            l_rstWhoToEmail.MoveNext
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
MsgBox "Done!"

End Sub

Private Sub cmdDisneyMO_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_strSQL As String, l_lngTrackerID As Long, l_lngRunningTime As Long, l_rst As ADODB.Recordset, l_curFileSize As Currency
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strTitle As String, l_strAbbreviation As String, l_strElementNumber As String, l_strBarcode As String, l_strElementType As String, l_strLanguage As String, l_strProductType As String, l_strVersion As String
Dim l_lngFirstColumn As Long, l_lngLibraryID As Long, l_strObjectNumber As String, l_strPrefix As String, l_strNewBarcode As String, l_strForeignEpisode As String, l_strDomesticEpisode As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
oExcel.Visible = True
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets(InputBox("Tab name", "Tab name", "Sheet1"))

l_lngXLRowCounter = Val(InputBox("First Row", "First Row", "2"))
l_lngFirstColumn = Val(InputBox("Number of First Active Column", "Number of First Active Column", "1")) - 1

l_strPrefix = InputBox("Please give the Project Prefix for this set of data", "Project Prefix Code", "MO-Disk")

Do While GetXLData(l_lngXLRowCounter, l_lngFirstColumn + 1) <> ""
    
    l_strBarcode = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 4)
    l_strTitle = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 1)
    l_strAbbreviation = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 2)
    l_strElementNumber = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 3)
    l_strElementType = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 8)
    l_strLanguage = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 11)
    l_strProductType = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 13)
    l_strVersion = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 14)
    l_strForeignEpisode = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 9)
    l_strDomesticEpisode = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 10)
        
    lblProgress.Caption = "Line: " & l_lngXLRowCounter & ", " & l_strBarcode & ", " & l_strTitle
    DoEvents
    If Trim(" " & GetDataSQL("SELECT TOP 1 tracker_itemID FROM tracker_item WHERE headertext1 = '" & QuoteSanitise(l_strTitle) & "' AND barcode = '" & l_strBarcode & " AND companyID = 1602")) = "" Then
        l_strSQL = "INSERT INTO tracker_item (companyID, headerint2, headertext9, headertext10, headertext8, headertext7, headertext1, headertext2, headertext3, headertext4, headertext5, headertext6, headerint1) VALUES ("
        l_strSQL = l_strSQL & "1602, 0, "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strForeignEpisode) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strDomesticEpisode) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strBarcode) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strPrefix) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strAbbreviation) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strElementType) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strLanguage) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strProductType) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strVersion) & "', "
        l_strSQL = l_strSQL & QuoteSanitise(Val(l_strElementNumber)) & ");"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_lngTrackerID = g_lngLastID
    Else
        MsgBox "Duplicate Item Row: " & l_lngXLRowCounter & ", Barcode: " & l_strBarcode
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Loop

lblProgress.Caption = ""
Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

MsgBox "Done"

End Sub

Private Sub cmdESIDeliveryLog_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_strSQL As String, l_lngTrackerID As Long, l_lngRunningTime As Long, l_rst As ADODB.Recordset, l_curFileSize As Currency
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strReference As String, l_strFilename As String, l_strTitle  As String, l_strItemType As String, l_strVideoFormat As String, l_strCans As String, l_strAcquisitionSourceType As String, l_strPartOfReference As String
Dim l_strBarcode As String, l_strCanID As String, l_strCrateNumber As String, l_lngCrateNumber As Long, l_strCrateBarcode As String, l_strPRIref As String, l_lngPRIref As Long
Dim l_lngFirstColumn As Long, l_lngLibraryID As Long, l_strObjectNumber As String, l_strPrefix As String, l_strNewBarcode As String
Dim l_datDeliveryDate As Date

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub
 
End If

Open l_strExcelFileName For Input As 1

l_datDeliveryDate = Mid(l_strExcelFileTitle, 8, 10)

Do While Not EOF(1)
    
    Line Input #1, l_strFilename
    
    lblProgress.Caption = "File: " & l_strFilename
    DoEvents
    
    l_lngTrackerID = Val(Trim(" " & GetDataSQL("SELECT tracker_itemID from tracker_item WHERE companyID = 769 AND itemfilename = '" & Trim(l_strFilename) & "' AND (stagefield5 IS NULL OR stagefield5 = '') AND readytobill = 0;")))
    If l_lngTrackerID <> 0 Then
        l_strSQL = "UPDATE tracker_item SET "
        l_strSQL = l_strSQL & "stagefield5 = '" & Format(l_datDeliveryDate, "YYYY-MM-DD HH:NN:SS") & "', "
        l_strSQL = l_strSQL & "stagefield25 = '" & Format(l_datDeliveryDate, "YYYY-MM-DD HH:NN:SS") & "', "
        l_strSQL = l_strSQL & "readytobill = 1 "
        l_strSQL = l_strSQL & "WHERE tracker_itemID = " & l_lngTrackerID & ";"
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
    End If
    
Loop

lblProgress.Caption = ""
Close #1

MsgBox "Done"

End Sub

Private Sub cmdESITrackerRead_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_strSQL As String, l_lngTrackerID As Long, l_lngRunningTime As Long, l_rst As ADODB.Recordset, l_curFileSize As Currency
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strReference As String, l_strFilename As String, l_strTitle  As String, l_strShow As String, l_strSubTitle As String, l_strTapeFormat As String, l_strEpisodeNumber As String
Dim l_strBarcode As String, l_strCanID As String, l_strCrateNumber As String, l_lngCrateNumber As Long, l_strCrateBarcode As String, l_strPRIref As String, l_lngPRIref As Long
Dim l_lngFirstColumn As Long, l_lngLibraryID As Long, l_strObjectNumber As String, l_strPrefix As String, l_strNewBarcode As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
oExcel.Visible = True
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets(InputBox("Tab name", "Tab name", "Sheet1"))

l_lngXLRowCounter = Val(InputBox("First Row", "First Row", "2"))
l_lngFirstColumn = Val(InputBox("Number of First Active Column", "Number of First Active Column", "1")) - 1

l_strPrefix = InputBox("Please give the Project Prefix for this set of data", "Project Prefix Code", "DOND-S")

Do While GetXLData(l_lngXLRowCounter, l_lngFirstColumn + 1) <> ""
    
    l_strTitle = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 2) 'B
    l_strSubTitle = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 3) 'C
    l_strTapeFormat = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 4) 'D
    l_strFilename = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 9) 'I
    l_strBarcode = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 1) 'A
    l_strShow = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 8) 'H
    l_strEpisodeNumber = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 6) 'F
    
    If Len(l_strFilename) > 4 Then
        l_strReference = Left(l_strFilename, Len(l_strFilename) - 4)
    Else
        l_strReference = ""
    End If
    
    lblProgress.Caption = "Line: " & l_lngXLRowCounter & ", " & l_strReference & ", " & l_strTitle & ", Ep: " & l_strEpisodeNumber
    DoEvents
    If Trim(" " & GetDataSQL("SELECT TOP 1 tracker_itemID FROM tracker_item WHERE itemreference = '" & QuoteSanitise(l_strReference) & "' AND barcodde = '" & l_strBarcode & "' AND companyID = 769")) = "" Then
        l_strSQL = "INSERT INTO tracker_item (companyID, itemreference, itemfilename, WorkflowVariantID, barcode, headertext1, headertext3, headertext4, headertext5, headertext6, headertext7) VALUES ("
        l_strSQL = l_strSQL & "769, "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strReference) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strFilename) & "', "
        l_strSQL = l_strSQL & "137, "
        l_strSQL = l_strSQL & IIf(l_strBarcode <> "", "'" & QuoteSanitise(l_strBarcode) & "', ", "Null, ")
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strPrefix) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSubTitle) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strShow) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTapeFormat) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strEpisodeNumber) & "');"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_lngTrackerID = g_lngLastID
        
        If l_strBarcode <> "" Then
            If Val(Trim(" " & GetData("library", "LibraryID", "barcode", l_strBarcode))) = 0 Then
                l_lngLibraryID = CreateLibraryItem(l_strBarcode, l_strTitle, 0, GetNextSequence("InternalReference"), 769, False, "", l_strSubTitle, "DBETA")
            Else
                'MsgBox "Tape record already exists for Row: " & l_lngXLRowCounter & ", Barcode: " & l_strBarcode & ", new tape record not made."
            End If
        End If
    Else
'        MsgBox "Duplicate Item Row: " & l_lngXLRowCounter & ", Barcode: " & l_strBarcode
        If l_strBarcode <> "" Then
            If Val(Trim(" " & GetData("library", "LibraryID", "barcode", l_strBarcode))) = 0 Then
                l_lngLibraryID = CreateLibraryItem(l_strBarcode, l_strTitle, 0, GetNextSequence("InternalReference"), 769, False, "", l_strSubTitle, "DBETA")
            Else
                'MsgBox "Tape record already exists for Row: " & l_lngXLRowCounter & ", Barcode: " & l_strBarcode & ", new tape record not made."
            End If
        End If
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Loop

lblProgress.Caption = ""
Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

MsgBox "Done"

End Sub

Private Sub cmdESIXDCAMDeliveryLog_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_strSQL As String, l_lngTrackerID As Long, l_lngRunningTime As Long, l_rst As ADODB.Recordset, l_curFileSize As Currency
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strReference As String, l_strFilename As String, l_strTitle  As String, l_strItemType As String, l_strVideoFormat As String, l_strCans As String, l_strAcquisitionSourceType As String, l_strPartOfReference As String
Dim l_strBarcode As String, l_strCanID As String, l_strCrateNumber As String, l_lngCrateNumber As Long, l_strCrateBarcode As String, l_strPRIref As String, l_lngPRIref As Long
Dim l_lngFirstColumn As Long, l_lngLibraryID As Long, l_strObjectNumber As String, l_strPrefix As String, l_strNewBarcode As String
Dim l_datDeliveryDate As Date

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub
 
End If

Set oExcel = CreateObject("Excel.Application")
oExcel.Visible = True
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets("Sheet1")

l_lngXLRowCounter = 11

Do While GetXLData(l_lngXLRowCounter, 3) <> ""
    
    l_strFilename = GetXLDataText(l_lngXLRowCounter, 3)
    l_datDeliveryDate = GetXLDataText(l_lngXLRowCounter, 11)
    
    lblProgress.Caption = "Row: " & l_lngXLRowCounter & ", " & l_strFilename & ", finished at " & Format(l_datDeliveryDate, "YYYY-MM-DD HH:NN:SS")
    DoEvents
    
    l_lngTrackerID = Val(Trim(" " & GetDataSQL("SELECT tracker_itemID from tracker_item WHERE companyID = 769 AND itemfilename = '" & Trim(l_strFilename) & "' AND (stagefield7 IS NULL OR stagefield7 = '') AND readytobill = 0;")))
    If l_lngTrackerID <> 0 Then
        l_strSQL = "UPDATE tracker_item SET "
        l_strSQL = l_strSQL & "stagefield7 = '" & Format(l_datDeliveryDate, "YYYY-MM-DD HH:NN:SS") & "', "
        l_strSQL = l_strSQL & "stagefield25 = '" & Format(l_datDeliveryDate, "YYYY-MM-DD HH:NN:SS") & "', "
        l_strSQL = l_strSQL & "readytobill = 1 "
        l_strSQL = l_strSQL & "WHERE tracker_itemID = " & l_lngTrackerID & ";"
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Loop

lblProgress.Caption = ""
Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

MsgBox "Done"

End Sub

Private Sub cmdGSN_DPP_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_strSQL As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strFilename As String, l_strSeriesTitle As String, l_strProgramTitle As String, l_strEpisodeTitle As String, l_strProdNumber As String, l_strSynopsis As String
Dim l_strOriginator As String, l_strCopyrightYear As String, l_strIdentifierType As String, l_strGenre As String, l_strDistributor As String
Dim l_strShowsAdded As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    MsgBox "Action Cancelled"
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets("Sheet1")

l_lngXLRowCounter = Val(InputBox("First Row", "First Active Row", "7"))
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    oExcel.Quit
    Exit Sub
End If

l_strShowsAdded = "Global Series Network Files added to Tracker:" & vbCrLf & vbCrLf

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_strFilename = GetXLData(l_lngXLRowCounter, 8)
    
    lblProgress.Caption = "Row: " & l_lngXLRowCounter & ", " & l_strFilename
    DoEvents
    
    l_strSeriesTitle = GetXLData(l_lngXLRowCounter, 1)
    l_strProgramTitle = GetXLData(l_lngXLRowCounter, 2)
    l_strEpisodeTitle = GetXLData(l_lngXLRowCounter, 3)
    l_strProdNumber = GetXLData(l_lngXLRowCounter, 4)
    l_strSynopsis = GetXLData(l_lngXLRowCounter, 5)
    l_strOriginator = GetXLData(l_lngXLRowCounter, 6)
    l_strCopyrightYear = GetXLData(l_lngXLRowCounter, 7)
    l_strIdentifierType = GetXLData(l_lngXLRowCounter, 9)
    l_strGenre = GetXLData(l_lngXLRowCounter, 10)
    l_strDistributor = GetXLData(l_lngXLRowCounter, 11)
    
    If GetDataSQL("SELECT tracker_itemID from tracker_item WHERE itemfilename = '" & l_strFilename & "' AND companyID = 1535 AND readytobill = 0") = "" Then
        l_strSQL = "INSERT INTO tracker_item (companyID, itemfilename, itemreference, headertext1, headertext2, headertext3, headertext4, headertext5, headertext6, headertext7, headertext8, headertext9, headertext10) VALUES ("
        l_strSQL = l_strSQL & "1535, "
        l_strSQL = l_strSQL & "'" & l_strFilename & "', "
        l_strSQL = l_strSQL & "'" & Left(l_strFilename, Len(l_strFilename) - 4) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSeriesTitle) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strProgramTitle) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strEpisodeTitle) & "', "
        l_strSQL = l_strSQL & "'" & l_strProdNumber & "',"
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSynopsis) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strOriginator) & "', "
        l_strSQL = l_strSQL & "'" & l_strCopyrightYear & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strIdentifierType) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strGenre) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strDistributor) & "');"
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        l_strShowsAdded = l_strShowsAdded & l_strFilename & vbCrLf
    
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Loop

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing
SendSMTPMail "thart@visualdatamedia.com", "Tim Hart", "GSN DPP Sheet Imported", "", l_strShowsAdded, False, "dnickolls@visualdatamedia.com", "David Nickolls"
lblProgress.Caption = ""
MsgBox "Done"

End Sub

Private Sub cmdLibraryShelves_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long, l_strShelf As String, l_strBarcode As String, l_strBox As String
Dim l_lngLibraryID As Long

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets(1)

l_lngXLRowCounter = 2

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_strShelf = GetXLData(l_lngXLRowCounter, 2)
    l_strBarcode = GetXLData(l_lngXLRowCounter, 1)
    l_strBox = GetXLData(l_lngXLRowCounter, 3)
    
    l_lngLibraryID = GetData("library", "libraryID", "barcode", l_strBarcode)
    If l_lngLibraryID <> 0 Then
        SetData "library", "location", "libraryID", l_lngLibraryID, "LIBRARY"
        SetData "library", "inLibrary", "libraryID", l_lngLibraryID, "YES"
        SetData "library", "shelf", "libraryID", l_lngLibraryID, l_strShelf
        If l_strBox <> "" Then
            SetData "library", "box", "libraryID", l_lngLibraryID, l_strBox
        End If
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Loop

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

MsgBox "Done"

End Sub

Private Sub cmdLMHiTunesTV_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strTitle As String, l_strVendorID As String, l_strContainerID As String, l_strContainerPosition As String, l_strReleaseDate As String, l_strRating As String, l_strCopyright As String, l_strProvider As String, l_strStudio_Release_Title As String
Dim l_strSynopsis As String, l_strPreviewStartTime As String, l_strTerritory As String, l_strSalesStartDate As String, l_strClearedForSale As String, l_strLanguage As String, l_strOriginalLocale As String, l_strEpisodeProductionNumber As String
Dim l_lngTemp As Long, l_strSQL As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "Cover Sheet", "")
'If l_strExcelSheetName = "" Then
'    oWorkbook.Close
'    Set oWorkbook = Nothing
'    Set oExcel = Nothing
'    Exit Sub
'End If
l_strExcelSheetName = "Sheet1"
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = 2

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    l_strVendorID = Trim(GetXLData(l_lngXLRowCounter, 5))
    lblProgress.Caption = l_strVendorID
    DoEvents
    l_strProvider = GetXLData(l_lngXLRowCounter, 1)
    l_strLanguage = GetXLData(l_lngXLRowCounter, 3)
    Select Case l_strLanguage
        Case "French-FR"
            l_strLanguage = "fr-FR"
        Case "German-DE"
            l_strLanguage = "de-DE"
        Case "English-GB"
            l_strLanguage = "en-GB"
        Case Else
            'Do nothing
    End Select
        
    l_strOriginalLocale = Trim(GetXLData(l_lngXLRowCounter, 4))
    If Len(l_strOriginalLocale) = 2 Then l_strOriginalLocale = LCase(l_strOriginalLocale)
    l_strEpisodeProductionNumber = Trim(GetXLData(l_lngXLRowCounter, 6))
    l_strTitle = Trim(GetXLData(l_lngXLRowCounter, 7))
    l_strContainerID = Trim(GetXLData(l_lngXLRowCounter, 8))
    l_strContainerPosition = Trim(GetXLData(l_lngXLRowCounter, 9))
    l_strReleaseDate = Trim(GetXLData(l_lngXLRowCounter, 10))
    l_strRating = Trim(GetXLData(l_lngXLRowCounter, 11))
    l_strCopyright = Trim(GetXLData(l_lngXLRowCounter, 12))
    l_strSynopsis = Trim(GetXLData(l_lngXLRowCounter, 13))
    l_strPreviewStartTime = Trim(GetXLData(l_lngXLRowCounter, 17))
    l_strTerritory = Trim(GetXLData(l_lngXLRowCounter, 18))
    l_strTerritory = UCase(l_strTerritory)
    l_strSalesStartDate = Trim(GetXLData(l_lngXLRowCounter, 19))
    l_strClearedForSale = Trim(GetXLData(l_lngXLRowCounter, 20))
    l_strStudio_Release_Title = Trim(GetXLData(l_lngXLRowCounter, 21))
    
    l_lngTemp = GetData("iTunes_Package", "iTunes_PackageID", "video_vendorID", l_strVendorID)
    If Val(l_lngTemp) = 0 Then
        l_strSQL = "INSERT INTO iTunes_Package (companyID, provider, video_type, video_vendorID, video_ep_prod_no, video_title, video_title_utf8, studio_release_title, studio_release_title_utf8, video_containerID, video_containerposition, video_releasedate, "
        Select Case l_strTerritory
            Case "FR"
                l_strSQL = l_strSQL & "rating_fr, "
            Case "DE"
                l_strSQL = l_strSQL & "rating_de, "
            Case "GB"
                l_strSQL = l_strSQL & "rating_uk, "
            Case "US"
                l_strSQL = l_strSQL & "rating_us, "
            Case "CA"
                l_strSQL = l_strSQL & "rating_ca, "
            Case Else
                MsgBox "Unhandled Territory - please get CETA updated. Row will be wrong"
        End Select
                
        l_strSQL = l_strSQL & "video_copyright_cline, video_copyright_cline_utf8, video_longdescription, video_longdescription_utf8, video_previewstarttime, video_language, metadata_language, originalspokenlocale, "
        l_strSQL = l_strSQL & "fullcroptop, fullcropbottom, fullcropleft, fullcropright) VALUES ("
        l_strSQL = l_strSQL & "1566, "
        l_strSQL = l_strSQL & "'" & l_strProvider & "', "
        l_strSQL = l_strSQL & "'tv', "
        l_strSQL = l_strSQL & "'" & l_strVendorID & "', "
        l_strSQL = l_strSQL & "'" & l_strEpisodeProductionNumber & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(l_strTitle)) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strStudio_Release_Title) & "', "
        l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(l_strStudio_Release_Title)) & "', "
        l_strSQL = l_strSQL & "'" & l_strContainerID & "', "
        l_strSQL = l_strSQL & "'" & l_strContainerPosition & "', "
        l_strSQL = l_strSQL & "'" & Format(l_strReleaseDate, "YYYY-MM-DD") & "', "
        l_strSQL = l_strSQL & "'" & l_strRating & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strCopyright) & "', "
        l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(l_strCopyright)) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSynopsis) & "', "
        l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(l_strSynopsis)) & "', "
        l_strSQL = l_strSQL & IIf(l_strPreviewStartTime <> "", l_strPreviewStartTime, 0) & ", "
        l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
        l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
        l_strSQL = l_strSQL & "'" & l_strOriginalLocale & "', 4, 4, 4, 4);"
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_lngTemp = g_lngLastID
        
        
        l_strSQL = "INSERT INTO iTunes_product (iTunes_packageID, territory, salesstartdate, clearedforsale) VALUES ("
        l_strSQL = l_strSQL & l_lngTemp & ", "
        l_strSQL = l_strSQL & "'" & l_strTerritory & "', "
        l_strSQL = l_strSQL & "'" & Format(l_strSalesStartDate, "YYYY-MM-DD") & "', "
        l_strSQL = l_strSQL & "'" & l_strClearedForSale & "');"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    Else
        MsgBox "Vendor ID: " & l_strVendorID & " is already present in the iTunes Packages list. This line was not read in." & vbCrLf & "If you wish to read this line in, please manually delete the old package first.", vbInformation
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1
Loop
lblProgress.Caption = ""

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
MsgBox "Done"

End Sub

Private Sub cmdMTARead_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_strSQL As String, l_lngTrackerID As Long, l_lngRunningTime As Long, l_rst As ADODB.Recordset, l_curFileSize As Currency
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strReference As String, l_strTitle  As String, l_strItemType As String, l_strVideoFormat As String, l_strCans As String, l_strAcquisitionSourceType As String, l_strPartOfReference As String
Dim l_strBarcode As String, l_strCanID As String, l_strCrateNumber As String, l_lngCrateNumber As Long, l_strCrateBarcode As String, l_strPRIref As String, l_lngPRIref As Long
Dim l_lngFirstColumn As Long, l_lngLibraryID As Long, l_strObjectNumber As String
Dim l_strDate As String, l_datDate As Date, l_strOriginalFormat As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
oExcel.Visible = True
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets(InputBox("Tab name", "Tab name", "Sheet1"))

l_lngXLRowCounter = Val(InputBox("First Row", "First Active Row", "3"))
l_lngFirstColumn = 0

Do While GetXLData(l_lngXLRowCounter, l_lngFirstColumn + 1) <> ""
    
    l_strBarcode = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 1)
    l_strBarcode = "MTA" & Format(Val(l_strBarcode), "000")
    l_strCrateNumber = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 2)
    l_strDate = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 3)
    l_strTitle = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 4)
    l_strReference = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 5)
    l_strOriginalFormat = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 6)
    
    lblProgress.Caption = "Line: " & l_lngXLRowCounter & ", " & l_strReference & ", " & l_strTitle
    DoEvents
    If Trim(" " & GetDataSQL("SELECT TOP 1 tracker_itemID FROM tracker_item WHERE itemreference = '" & QuoteSanitise(l_strReference) & "' and companyID = 1583")) = "" Then
        l_strSQL = "INSERT INTO tracker_item (companyID, itemreference, itemfilename, barcode, headertext1, headertext2, headertext3, headertext4) VALUES ("
        l_strSQL = l_strSQL & "1583, "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strReference) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strReference) & ".mov', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strBarcode) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strDate) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strCrateNumber) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strOriginalFormat) & "'); "
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_lngTrackerID = g_lngLastID
        
        If Val(Trim(" " & GetData("library", "LibraryID", "barcode", l_strBarcode))) = 0 Then
            l_lngLibraryID = CreateLibraryItem(l_strBarcode, l_strTitle, 0, GetNextSequence("InternalReference"), 1583, False, "", "", GetAlias(l_strOriginalFormat))
        Else
            MsgBox "Tape record already exists for Row: " & l_lngXLRowCounter & ", Barcode: " & l_strBarcode & ", new tape record not made."
        End If
    Else
        MsgBox "Duplicate Item Row: " & l_lngXLRowCounter & ", Barcode: " & l_strReference
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Loop

lblProgress.Caption = ""
Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

MsgBox "Done"

End Sub
Private Sub cmdBritishFilmInstitute_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_strSQL As String, l_lngTrackerID As Long, l_lngRunningTime As Long, l_rst As ADODB.Recordset, l_curFileSize As Currency
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strReference As String, l_strFilename As String, l_strTitle  As String, l_strItemType As String, l_strVideoFormat As String, l_strCans As String, l_strAcquisitionSourceType As String, l_strPartOfReference As String
Dim l_strBarcode As String, l_strCanID As String, l_strCrateNumber As String, l_lngCrateNumber As Long, l_strCrateBarcode As String, l_strPRIref As String, l_lngPRIref As Long
Dim l_lngFirstColumn As Long, l_lngLibraryID As Long, l_strObjectNumber As String, l_strPrefix As String, l_strNewBarcode As String, l_strPONumber As String
Dim l_lngPOColumnNumber As Long

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
oExcel.Visible = True
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets(InputBox("Tab name", "Tab name", "INVENTORY_INFORMATION_REQUIRED"))

l_lngXLRowCounter = Val(InputBox("First Row", "First Row", "5"))
l_lngFirstColumn = Val(InputBox("Number of First Active Column", "Number of First Active Column", "1")) - 1
l_lngPOColumnNumber = Val(InputBox("What Column number is the PO in", "PO Column", "78"))

l_strPrefix = InputBox("Please give the Project Prefix for this set of data", "Project Prefix Code", "")

Do While GetXLData(l_lngXLRowCounter, l_lngFirstColumn + 1) <> ""
'    If GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 42) = 16 Then 'AP
        l_strTitle = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 15) 'O
        l_strFilename = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 11) 'K
        l_strBarcode = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 6) 'F
        l_strVideoFormat = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 9) 'I
        l_strCrateBarcode = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 5) 'E
        l_strPONumber = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + l_lngPOColumnNumber) 'Not sure - it moves from sheet to sheet
        
        If l_strBarcode = "" Then
            l_strNewBarcode = l_strPrefix & "/" & GetNextSequence("barcodenumber")
        Else
            l_strNewBarcode = l_strBarcode
        End If
        
        l_strReference = Left(l_strFilename, InStr(l_strFilename, ".") - 1)
        
        lblProgress.Caption = "Line: " & l_lngXLRowCounter & ", " & l_strReference & ", " & l_strTitle
        DoEvents
        If Trim(" " & GetDataSQL("SELECT TOP 1 tracker_itemID FROM tracker_item WHERE itemreference = '" & QuoteSanitise(l_strReference) & "' and companyID = 1244 ANd headertext9 = '" & l_strPrefix & "'")) = "" Then
            l_strSQL = "INSERT INTO tracker_item (companyID, itemreference, itemfilename, WorkflowVariantID, headertext2, barcode, headertext1, headertext4, headertext9, headertext8, headertext7) VALUES ("
            l_strSQL = l_strSQL & "1244, "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strReference) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strFilename) & "', "
            l_strSQL = l_strSQL & "151, "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strBarcode) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewBarcode) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strVideoFormat) & "', "
            l_strSQL = l_strSQL & "'" & l_strPrefix & "', "
            l_strSQL = l_strSQL & "'" & l_strPONumber & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strCrateBarcode) & "');"
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            l_lngTrackerID = g_lngLastID

            If Val(Trim(" " & GetData("library", "LibraryID", "barcode", l_strNewBarcode))) = 0 Then
                l_lngLibraryID = CreateLibraryItem(l_strNewBarcode, l_strTitle, 0, GetNextSequence("InternalReference"), 1244, False, "", "", GetAlias(l_strVideoFormat))
'            Else
'                MsgBox "Tape record already exists for Row: " & l_lngXLRowCounter & ", Barcode: " & l_strBarcode & ", new tape record not made."
            End If
        Else
            l_lngTrackerID = GetDataSQL("SELECT TOP 1 tracker_itemID FROM tracker_item WHERE itemreference = '" & QuoteSanitise(l_strReference) & "' and companyID = 1244")
            l_strNewBarcode = l_strPrefix & "/" & GetNextSequence("barcodenumber")
            If l_strPONumber <> "" Then
                l_strSQL = "UPDATE tracker_item SET headertext2 = '" & l_strBarcode & "', barcode = '" & l_strNewBarcode & "', headertext8 = '" & l_strPONumber & "' WHERE tracker_itemID = " & l_lngTrackerID & ";"
            Else
                l_strSQL = "UPDATE tracker_item SET headertext2 = '" & l_strBarcode & "', barcode = '" & l_strNewBarcode & "' WHERE tracker_itemID = " & l_lngTrackerID & ";"
            End If
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            If Val(Trim(" " & GetData("library", "LibraryID", "barcode", l_strNewBarcode))) = 0 Then
                l_lngLibraryID = CreateLibraryItem(l_strNewBarcode, l_strTitle, 0, GetNextSequence("InternalReference"), 1244, False, "", "", GetAlias(l_strVideoFormat))
'            Else
'                MsgBox "Tape record already exists for Row: " & l_lngXLRowCounter & ", Barcode: " & l_strNewBarcode & ", new tape record not made."
            End If
    '        MsgBox "Duplicate Item Row: " & l_lngXLRowCounter & ", Barcode: " & l_strBarcode
        End If
'    End If
'    If l_strPrefix = "YFA2" Then
'        If Trim(" " & GetDataSQL("SELECT TOP 1 tracker_itemID FROM tracker_item WHERE itemreference = '" & QuoteSanitise(l_strReference) & "' and companyID = 1244 ANd headertext9 = 'YFA' and readytobill = 0")) <> "" Then
'            l_lngTrackerID = GetDataSQL("SELECT TOP 1 tracker_itemID FROM tracker_item WHERE itemreference = '" & QuoteSanitise(l_strReference) & "' and companyID = 1244 and headertext9 = 'YFA' and readytobill = 0")
'            l_strSQL = "DELETE from tracker_item WHERE tracker_itemID = " & l_lngTrackerID & ";"
'            Debug.Print l_strSQL
'            ExecuteSQL l_strSQL, g_strExecuteError
'            CheckForSQLError
'        End If
'    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Loop

lblProgress.Caption = ""
Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

MsgBox "Done"

End Sub

Private Sub cmdBulkVideoAssetXMLRead_Click()

Dim l_xmlDOC As Object, l_lngTrackerID As Long, temp As String
Dim l_strSQL As String, l_lngPackageID As Long

Set l_xmlDOC = CreateObject("MSXML2.DOMDocument")

l_xmlDOC.async = False
MDIForm1.dlgMain.InitDir = g_strExcelOrderLocation
MDIForm1.dlgMain.Filter = "All Files|*.*"
MDIForm1.dlgMain.ShowOpen

If MDIForm1.dlgMain.Filename = "" Then
    Beep
    Exit Sub
End If

temp = Right(MDIForm1.dlgMain.Filename, 25)
If InStr(temp, "_") < 8 Then
    temp = Mid(temp, InStr(temp, "_") + 1)
End If
temp = Left(temp, InStr(temp, "_") - 1)

m_DADC_xml.AlphaKey = temp

If l_xmlDOC.Load(MDIForm1.dlgMain.Filename) Then
    ParseThroughNodes l_xmlDOC.childNodes
    temp = m_DADC_xml.AlphaKey & "_" & m_DADC_xml.CVCode & "_" & m_DADC_xml.KitID
    l_lngTrackerID = Val(Trim(" ") & GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE companyID = 1239 AND readytobill = 0 AND contentversioncode = '" & m_DADC_xml.AlphaKey & "'"))
    If l_lngTrackerID <> 0 Then
        l_strSQL = "UPDATE tracker_dadc_item SET contentversionID = " & m_DADC_xml.CVCode & ", kitID = " & m_DADC_xml.KitID & ", itemreference = '" & temp & "' WHERE tracker_dadc_itemID = " & l_lngTrackerID & ";"
    Else
        MsgBox "Could not match XML to an existing Bulk Audio tracker item"
        Exit Sub
    End If
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
Else
    MsgBox "Failed to Load XML"
End If

Set l_xmlDOC = Nothing
MsgBox "Done"

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdDADCBatch_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strBarcode As String, l_strReference As String, l_strTitle As String, l_strSubTitle As String, l_lngEpisode As Long, l_strAlphaBusiness As String
Dim l_lngBatchNumber As Long, l_strFormat As String, l_strLegacyLineStandard As String, l_strImageAspectRatio As String, l_strLegacyPictureElement As String
Dim l_strLanguage As String, l_strSubtitlesLanguage As String, l_strAudioConfiguration As String, l_strDurationEstimate As String

Dim l_strSQL As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "Sheet1")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "DADC Excel Read", 4))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_lngBatchNumber = GetXLData(l_lngXLRowCounter, 1)
    l_strBarcode = Trim(GetXLData(l_lngXLRowCounter, 51))
    l_strAlphaBusiness = Trim(GetXLData(l_lngXLRowCounter, 32))
    l_strReference = l_strAlphaBusiness & "_" & SanitiseBarcode(l_strBarcode)
    l_strTitle = GetXLData(l_lngXLRowCounter, 10)
    l_strSubTitle = GetXLData(l_lngXLRowCounter, 21)
    l_lngEpisode = GetXLData(l_lngXLRowCounter, 22)
    l_strFormat = GetAlias(GetXLData(l_lngXLRowCounter, 56))
    l_strLegacyLineStandard = GetXLData(l_lngXLRowCounter, 59)
    l_strImageAspectRatio = GetXLData(l_lngXLRowCounter, 65)
    l_strLegacyPictureElement = GetXLData(l_lngXLRowCounter, 58)
    l_strLanguage = GetXLData(l_lngXLRowCounter, 61)
    l_strSubtitlesLanguage = GetXLData(l_lngXLRowCounter, 64)
    l_strAudioConfiguration = GetXLData(l_lngXLRowCounter, 60)
    l_strDurationEstimate = GetXLData(l_lngXLRowCounter, 57)
    
    lblProgress.Caption = l_strBarcode & " - " & l_strTitle & " Ep. " & l_lngEpisode
    DoEvents
    
    l_strSQL = "INSERT INTO tracker_dadc_item (companyID, itemreference, title, subtitle, barcode, newbarcode, format, externalalphaID, originalexternalalphaID, legacylinestandard, imageaspectratio, legacypictureelement, language, "
    l_strSQL = l_strSQL & "subtitleslanguage, audioconfiguration, episode, batch, durationestimate) VALUES ("
    l_strSQL = l_strSQL & "1239, "
    l_strSQL = l_strSQL & "'" & l_strReference & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSubTitle) & "', "
    l_strSQL = l_strSQL & "'" & l_strBarcode & "', "
    l_strSQL = l_strSQL & "'" & l_strBarcode & "', "
    l_strSQL = l_strSQL & "'" & l_strFormat & "', "
    l_strSQL = l_strSQL & "'" & l_strAlphaBusiness & "', "
    l_strSQL = l_strSQL & "'" & l_strAlphaBusiness & "', "
    l_strSQL = l_strSQL & "'" & l_strLegacyLineStandard & "', "
    l_strSQL = l_strSQL & "'" & l_strImageAspectRatio & "', "
    l_strSQL = l_strSQL & "'" & l_strLegacyPictureElement & "', "
    l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
    l_strSQL = l_strSQL & "'" & l_strSubtitlesLanguage & "', "
    l_strSQL = l_strSQL & "'" & l_strAudioConfiguration & "', "
    l_strSQL = l_strSQL & l_lngEpisode & ", "
    l_strSQL = l_strSQL & l_lngBatchNumber & ", "
    l_strSQL = l_strSQL & Int(Val(l_strDurationEstimate)) & ");"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Loop

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

Private Sub cmdDADCBBCPurgeList_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strBarcode As String, l_strLastBarcode As String, l_strReference As String, l_strTitle As String, l_strSubTitle As String, l_lngEpisode As Long, l_strAlphaBusiness As String
Dim l_lngCRID As Long, l_strFormat As String, l_strLegacyLineStandard As String, l_strImageAspectRatio As String, l_strLegacyPictureElement As String, l_strLegacyFrameRate As String
Dim l_strLanguage As String, l_strSubtitlesLanguage As String, l_strAudioConfiguration As String, l_strDurationEstimate As String, l_lngLastTrackerID As Long, l_strAudioContent As String
Dim l_strDADCStatus As String, l_datDueDate As Date, l_strDueDate As String, l_strEmailBody As String, l_strEmailBody2 As String, l_rstWhoToEmail As ADODB.Recordset, l_lngExternalAlphaKey As Long, l_lngWorkflowID As Long
Dim l_lngMonth As Long, l_lngDay As Long, l_lngYear As Long, l_lngCount1 As Long, l_lngCount2 As Long, l_blnDateChange As Boolean, l_datOldDate As Date, l_lngBatchNumber As Long
Dim l_strAdditionalElements As String, l_strContentVersionCode As String, l_lngContentVersionID As Long, l_strRush As String, l_blnRushChanged As Boolean, l_datNewTargetDate As Date
Dim l_rst As ADODB.Recordset, Count2 As Long, l_strVendorGroup As String

Dim l_strSQL As String, TempStr As String, Count As Long, l_strTextInPicture As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "BBCW Ingest Queue Inventory Rep")
l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "BBCW Ingest Management Report �")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "DADC Excel Read", 4))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

l_strLastBarcode = ""
l_strEmailBody = ""
l_strEmailBody2 = ""

Do While GetXLData(l_lngXLRowCounter, 31) <> ""
    
    l_blnDateChange = False
    l_lngCRID = Val(Trim(GetXLData(l_lngXLRowCounter, 2))) 'B
    l_strBarcode = Trim(GetXLData(l_lngXLRowCounter, 31)) 'AE
    l_strDADCStatus = Trim(GetXLData(l_lngXLRowCounter, 3)) 'C
    l_strAlphaBusiness = Trim(GetXLData(l_lngXLRowCounter, 29)) 'AC
    l_strContentVersionCode = Trim(GetXLData(l_lngXLRowCounter, 15)) 'O
    l_lngContentVersionID = Val(Trim(GetXLData(l_lngXLRowCounter, 80))) 'CB
    l_strVendorGroup = Trim(GetXLData(l_lngXLRowCounter, 77)) 'BY
    l_lngWorkflowID = Val(GetXLData(l_lngXLRowCounter, 54)) 'BB
    
'    If LCase(l_strDADCStatus) = "cancelled" And l_strBarcode <> "" Then
    If l_strBarcode <> "" Then
    
        lblProgress.Caption = l_strBarcode
        DoEvents
        
        TempStr = ""
        TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE barcode='" & UCase(l_strBarcode) & "' AND companyID = 1261 and readytobill = 0;")
        If Val(TempStr) <> 0 Then
            l_strSQL = "UPDATE tracker_dadc_item set stagefield13 = '" & FormatSQLDate(Now) & "', readytobill = 1, rejected = 0 WHERE tracker_dadc_itemID = " & TempStr & ";"
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            l_strEmailBody = l_strEmailBody & "Cancelled Item: " & l_strBarcode & vbCrLf
        End If
        
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Loop

If l_strEmailBody <> "" Then
    
    l_strEmailBody = "The following items were cancelled from the DBB Manual Ingest Tracker: " & vbCrLf & vbCrLf & l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", 1261) & "' AND trackermessageID = 13;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "DADC Manual Ingest Tracker Items Cancelled", "", l_strEmailBody, True, "", ""
    
            l_rstWhoToEmail.MoveNext
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
MsgBox "Done!"

End Sub

Private Sub cmdDADCBulkAudioIngest_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strTitle As String, l_strSeries As String, l_strSubTitle As String, l_strEpisode As String, l_strComponentType As String, l_strConform As String, l_strBarcode As String, l_strAlphaDisplayCode As String
Dim l_strAudioContent As String, l_strAudioConfig As String, l_strLanguage As String, l_strIngestType As String, l_strScheduledBy As String
Dim l_strDADCStatus As String, l_strEmailBody As String, l_rstWhoToEmail As ADODB.Recordset
Dim l_lngMonth As Long, l_lngDay As Long, l_lngYear As Long, l_lngCount1 As Long, l_lngCount2 As Long, l_blnDateChange As Boolean, l_datOldDate As Date, l_lngTrackerID As Long
Dim l_strKitID As String
Dim l_strSQL As String, TempStr As String, l_blnError As Boolean, l_strSFUniqueID As String, l_strOldLanguage As String, l_strTextlessRequired As String, l_striTunesRequired As String
Dim l_strAuditDescription As String, l_strRevisionNumber As String, l_strUrgent As String, l_strMERequired As String, l_strME51Required As String, l_strSpecialProject As String

Dim l_blnFoundOne As Boolean

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "BBCW Ingest Queue Inventory Rep")
l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "SF Inbound Material Data")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "DADC Excel Read", 2))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

l_strEmailBody = ""

Do While GetXLData(l_lngXLRowCounter, 4) <> ""
    
    l_blnError = False
    l_strBarcode = Trim(GetXLData(l_lngXLRowCounter, 5)) 'E
    l_strTitle = Trim(GetXLData(l_lngXLRowCounter, 9)) 'I
    l_strSeries = Trim(GetXLData(l_lngXLRowCounter, 10)) 'J
    l_strSubTitle = Trim(GetXLData(l_lngXLRowCounter, 11)) 'K
    l_strEpisode = Trim(GetXLData(l_lngXLRowCounter, 12)) 'L
    l_strComponentType = Trim(GetXLData(l_lngXLRowCounter, 13)) 'M
    l_strLanguage = Trim(GetXLData(l_lngXLRowCounter, 16)) 'P
    l_strAudioConfig = Trim(GetXLData(l_lngXLRowCounter, 15)) 'O
    l_strAudioContent = Trim(GetXLData(l_lngXLRowCounter, 14)) 'N
    l_strConform = Trim(GetXLData(l_lngXLRowCounter, 17)) 'Q
    l_strAlphaDisplayCode = Trim(GetXLData(l_lngXLRowCounter, 4)) 'D
    l_strUrgent = Trim(GetXLData(l_lngXLRowCounter, 6)) 'F
    l_strScheduledBy = Trim(GetXLData(l_lngXLRowCounter, 2)) 'B
    l_strAudioConfig = l_strAudioConfig & IIf(l_strAudioContent <> "", ", " & l_strAudioContent, "") & IIf(l_strLanguage <> "", ", " & l_strLanguage, "")
    lblProgress.Caption = "Row:" & l_lngXLRowCounter & ", " & l_strAlphaDisplayCode & " - " & l_strTitle & " - " & l_strComponentType
    
    
    l_blnFoundOne = False
    'Test whether a constructable line of this item already exists
    l_strSQL = "SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE title='" & QuoteSanitise(l_strTitle) & "'"
    If l_strAlphaDisplayCode <> "" Then
        l_strSQL = l_strSQL & " AND contentversioncode = '" & l_strAlphaDisplayCode & "' "
    End If
    If l_strSeries <> "" Then
        l_strSQL = l_strSQL & " AND series = '" & l_strSeries & "' "
    End If
    If l_strEpisode = "" Then
        l_strSQL = l_strSQL & " AND episode IS NULL"
    Else
        l_strSQL = l_strSQL & " AND episode = '" & l_strEpisode & "'"
    End If
    If l_strAudioConfig <> "" Then
        l_strSQL = l_strSQL & " AND audioconfiguration = '" & l_strAudioConfig & "'"
    Else
        l_strSQL = l_strSQL & " AND componenttype IS NULL"
    End If
    If l_strLanguage <> "" Then
        l_strSQL = l_strSQL & " AND language = '" & l_strLanguage & "'"
    Else
        l_strSQL = l_strSQL & " AND language IS NULL"
    End If
    l_strSQL = l_strSQL & " AND companyID = 1239 AND readytobill = 0;"
    Debug.Print l_strSQL
    TempStr = GetDataSQL(l_strSQL)
    
    If Val(TempStr) <> 0 Then
        
        l_blnFoundOne = True
        l_strSQL = ""
'        l_strSQL = "UPDATE tracker_dadc_item SET "
'        l_strSQL = l_strSQL & "barcode = '" & l_strBarcode & "', "
'        l_strSQL = l_strSQL & "newbarcode = '" & l_strBarcode & "', "
'
'        If l_strSQL <> "" Then l_strSQL = l_strSQL & " WHERE tracker_svensk_itemID = " & TempStr & ";"
'
'        If l_blnError = False And l_strSQL <> "" Then
'            Debug.Print l_strSQL
'            ExecuteSQL l_strSQL, g_strExecuteError
'            CheckForSQLError
'            l_strEmailBody = l_strEmailBody & "Updated: " & l_strTitle & ", Ep " & l_strEpisode & ", " & l_strSubTitle & ", Lang: " & l_strLanguage & ", Component Type: " & l_strComponentType & ", for: " & l_strScheduledBy & vbCrLf
'            If l_strAuditDescription <> "" Then
'                l_strEmailBody = l_strEmailBody & l_strAuditDescription & vbCrLf
'                l_strSQL = "INSERT INTO tracker_svensk_audit (tracker_svensk_itemID, mdate, muser, description) VALUES (" & TempStr & ", getdate(), '" & g_strFullUserName & "', 'Update from IMD: " & QuoteSanitise(l_strAuditDescription) & "');"
'                Debug.Print l_strSQL
'                ExecuteSQL l_strSQL, g_strExecuteError
'                CheckForSQLError
'            End If
'        End If
            
    End If
                            
    If l_blnFoundOne = False Then
        
        l_blnFoundOne = True
        'Check whether this is an existing item that is completed...
        l_strSQL = "SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE title='" & QuoteSanitise(l_strTitle) & "'"
        If l_strAlphaDisplayCode <> "" Then
            l_strSQL = l_strSQL & " AND contentversioncode = '" & l_strAlphaDisplayCode & "' "
        End If
        If l_strSeries <> "" Then
            l_strSQL = l_strSQL & " AND series = '" & l_strSeries & "' "
        End If
        If l_strEpisode = "" Then
            l_strSQL = l_strSQL & " AND episode IS NULL"
        Else
            l_strSQL = l_strSQL & " AND episode = '" & l_strEpisode & "'"
        End If
        If l_strLanguage <> "" Then
            l_strSQL = l_strSQL & " AND language = '" & l_strLanguage & "'"
        Else
            l_strSQL = l_strSQL & " AND language IS NULL"
        End If
        l_strSQL = l_strSQL & " AND companyID = 1239 AND readytobill <> 0;"
        Debug.Print l_strSQL
        TempStr = GetDataSQL(l_strSQL)

        If Val(TempStr) <> 0 Then
            MsgBox "Row " & l_lngXLRowCounter & " was an update for an item that we have already completed. New Line will be made.", vbInformation, "Error......"
        End If
        
        l_blnError = False
        l_strSQL = "INSERT INTO tracker_dadc_item ("
        If l_strBarcode <> "" Then l_strSQL = l_strSQL & "barcode, "
        If UCase(Left(l_strUrgent, 1)) = "Y" Then l_strSQL = l_strSQL & "urgent, TargetDateCountdown, "
        l_strSQL = l_strSQL & "cdate, mdate, companyID, specialproject, title, series, "
        l_strSQL = l_strSQL & "subtitle, episode, language, audioconfiguration, contentversioncode, needsconform) VALUES ("
        If l_strBarcode <> "" Then l_strSQL = l_strSQL & "'" & l_strBarcode & "', "
        If UCase(Left(l_strUrgent, 1)) = "Y" Then l_strSQL = l_strSQL & "-1, 1, "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "1239, "
        l_strSQL = l_strSQL & "'" & l_strScheduledBy & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "'" & l_strSeries & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSubTitle) & "', "
        If l_strEpisode <> "" Then
            l_strSQL = l_strSQL & "'" & l_strEpisode & "', "
        Else
            l_strSQL = l_strSQL & "NULL, "
        End If
        l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
        l_strSQL = l_strSQL & "'" & l_strAudioConfig & "', "
        l_strSQL = l_strSQL & "'" & l_strAlphaDisplayCode & "', "
        l_strSQL = l_strSQL & "'" & l_strConform & "');"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_strEmailBody = l_strEmailBody & "Added: " & l_strTitle & ", Ep " & l_strEpisode & ", " & l_strSubTitle & ", Lang: " & l_strLanguage & ", Component Type: " & l_strComponentType & ", for: " & l_strScheduledBy & vbCrLf
    End If
    If l_blnFoundOne = False Then
        MsgBox "Line " & l_lngXLRowCounter & " had an unrecognised error - Line not read in.", vbCritical, "Error Reading Sheet"
        l_blnError = True
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Loop

If l_strEmailBody <> "" Then
    
    l_strEmailBody = "The following new items were added to the DADC BBC Bulk Audio Ingest Tracker: " & vbCrLf & vbCrLf & l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", 1239) & "' AND trackermessageID = 13;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "DADC Bulk Audio Ingest Tracker Items Added or Updated", "", l_strEmailBody, True, "", "", "", False, g_strUserEmailAddress
    
            l_rstWhoToEmail.MoveNext
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
MsgBox "Done"

End Sub

Private Sub cmdDHCourier_Click()

If MsgBox("Are all the tracker entries covered by this sheet already contract billed?", vbYesNo, "Reading Skibbly Courier Costs") = vbNo Then Exit Sub

'have a variable to hold the filename we are going to process
Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String

Dim l_strXLDate As String, l_lngXLJCADespatchNumber As Long, l_lngJCADespatchID As Long, l_dblCourierCost As Double, l_lngXLRowCounter As Long, l_strWaybillNumber As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Dim l_strSQL As String

'open the spreadsheet

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "Wellcome Excel Read", "Invoice ??????")

Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = 3
l_strXLDate = ""

l_strXLDate = GetXLData(l_lngXLRowCounter, 1)

Do While l_strXLDate <> "TOTAL"
    
    'Read the relevant items from the XL line
    l_lngXLJCADespatchNumber = Val(GetXLData(l_lngXLRowCounter, 2))
    If l_lngXLJCADespatchNumber <> 0 Then
        
        l_lngJCADespatchID = GetData("despatch", "despatchID", "despatchnumber", l_lngXLJCADespatchNumber)
        l_dblCourierCost = Val(Mid(GetXLData(l_lngXLRowCounter, 10), 2))
        l_strWaybillNumber = GetXLData(l_lngXLRowCounter, 6)
                
        If l_dblCourierCost <> 0 Then
            
            lblProgress.Caption = l_lngXLJCADespatchNumber
            DoEvents
        
            'Enter the data into a Skibbly tracker line
            SetData "skibblytracker", "waybillnumber", "despatchID", l_lngJCADespatchID, l_strWaybillNumber
            SetData "skibblytracker", "despatchcost", "despatchID", l_lngJCADespatchID, l_dblCourierCost
            SetData "skibblytracker", "readytobilldelivery", "despatchID", l_lngJCADespatchID, -1
        
        End If
    
    End If
    
    'Increment the count and loop
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    l_strXLDate = GetXLData(l_lngXLRowCounter, 1)

Loop

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
Beep
DoEvents

End Sub

Private Sub cmdDisneyAmazon_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_lngTemp As Long, l_strSQL As String, l_lngPackageID As Long
Dim l_lngCount As Long, l_strTemp As String
Dim l_lngColOriginal_Spoken_Locale As Long
Dim l_lngColUnique_ID As Long
Dim l_lngColSeason_Number As Long
Dim l_lngColLanguage As Long
Dim l_lngColTerritory As Long
Dim l_lngColTitle As Long
Dim l_lngColShortSynopsis As Long
Dim l_lngColLongSynopsis As Long
Dim l_lngColSeasonTitle As Long
Dim l_lngColSeasonProductionCompany As Long
Dim l_lngColGenre As Long
Dim l_lngColSeason_Unique_ID As Long
Dim l_lngColEpisode_Unique_ID As Long
Dim l_lngColEpisodeTitle As Long
Dim l_lngColSequence As Long
Dim l_lngColRatingType As Long
Dim l_lngColRating As Long
Dim l_lngColOriginalAirDate As Long
Dim l_lngColSDOfferStart As Long
Dim l_lngColSDVODOfferStart As Long
Dim l_lngColHDOfferStart As Long
Dim l_lngColHDVODOfferStart As Long
Dim l_lngColCopyright As Long
Dim l_lngColCopyrightYear As Long
Dim l_lngColContentType As Long
Dim l_lngColCountryOfOrigin As Long

Dim l_strOriginal_Spoken_Locale As String
Dim l_strUnique_ID As String
Dim l_strSeason_Number As String
Dim l_strLanguage As String
Dim l_strTerritory As String
Dim l_strTitle As String
Dim l_strShortSynopsis As String
Dim l_strLongSynopsis As String
Dim l_strSeasonProductionCompany As String
Dim l_strGenre As String
Dim l_strSeason_Unique_ID As String
Dim l_strEpisode_Unique_ID As String
Dim l_strEpisodeTitle As String
Dim l_strSequence As String
Dim l_strRatingType As String
Dim l_strRating As String
Dim l_datOriginalAirDate As Date
Dim l_datSDOfferStart As Date
Dim l_datSDVODOfferStart As Date
Dim l_datHDOfferStart As Date
Dim l_datHDVODOfferStart As Date
Dim l_strCopyright As String
Dim l_strCopyrightYear As String
Dim l_strContentType As String
Dim l_strCountryOfOrigin As String
Dim l_blnFound As Boolean

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = "Sheet1"
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

For l_lngCount = 1 To 64
    l_strTemp = GetXLData(1, l_lngCount)
    Select Case l_strTemp
        Case "Content Type"
            l_lngColContentType = l_lngCount
        Case "OriginalLanguage"
            l_lngColOriginal_Spoken_Locale = l_lngCount
        Case "Unique ID"
            l_lngColUnique_ID = l_lngCount
        Case "Season Number"
            l_lngColSeason_Number = l_lngCount
        Case "Language"
            l_lngColLanguage = l_lngCount
        Case "CountryOfOrigin"
            l_lngColCountryOfOrigin = l_lngCount
        Case "Territory"
            l_lngColTerritory = l_lngCount
        Case "Title"
            l_lngColTitle = l_lngCount
        Case "Short Synopsis"
            l_lngColShortSynopsis = l_lngCount
        Case "Long Synopsis"
            l_lngColLongSynopsis = l_lngCount
        Case "Studio"
            l_lngColSeasonProductionCompany = l_lngCount
        Case "Genre"
            l_lngColGenre = l_lngCount
        Case "Local Run Order"
            l_lngColSequence = l_lngCount
        Case "Rating System"
            l_lngColRatingType = l_lngCount
        Case "Rating"
            l_lngColRating = l_lngCount
        Case "Release Date", "First Aired Date"
            l_lngColOriginalAirDate = l_lngCount
        Case "SD EST - Start"
            l_lngColSDOfferStart = l_lngCount
        Case "HD EST - Start"
            l_lngColHDOfferStart = l_lngCount
        Case "SD VOD - Start"
            l_lngColSDVODOfferStart = l_lngCount
        Case "HD VOD - Start"
            l_lngColHDVODOfferStart = l_lngCount
        Case "Copyright Holder"
            l_lngColCopyright = l_lngCount
        Case "Copyright Year"
            l_lngColCopyrightYear = l_lngCount
            
    End Select
Next

l_lngXLRowCounter = 2

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    If GetXLData(l_lngXLRowCounter, l_lngColContentType) = "TV Episode" Then
        If l_lngColOriginal_Spoken_Locale <> 0 Then l_strOriginal_Spoken_Locale = Trim(GetXLData(l_lngXLRowCounter, l_lngColOriginal_Spoken_Locale)) Else l_strOriginal_Spoken_Locale = ""
        If l_lngColUnique_ID <> 0 Then l_strUnique_ID = Trim(GetXLData(l_lngXLRowCounter, l_lngColUnique_ID)) Else l_strUnique_ID = ""
        If l_lngColLanguage <> 0 Then l_strLanguage = Trim(GetXLData(l_lngXLRowCounter, l_lngColLanguage)) Else l_strLanguage = ""
        If l_lngColCountryOfOrigin <> 0 Then l_strCountryOfOrigin = Trim(GetXLData(l_lngXLRowCounter, l_lngColCountryOfOrigin)) Else l_strCountryOfOrigin = ""
        If l_lngColTerritory <> 0 Then l_strTerritory = Trim(GetXLData(l_lngXLRowCounter, l_lngColTerritory)) Else l_strTerritory = ""
        If l_lngColTitle <> 0 Then l_strTitle = Trim(GetXLData(l_lngXLRowCounter, l_lngColTitle)) Else l_strTitle = ""
        If l_lngColShortSynopsis <> 0 Then l_strShortSynopsis = Trim(GetXLData(l_lngXLRowCounter, l_lngColShortSynopsis)) Else l_strShortSynopsis = ""
        If l_lngColLongSynopsis <> 0 Then l_strLongSynopsis = Trim(GetXLData(l_lngXLRowCounter, l_lngColLongSynopsis)) Else l_strLongSynopsis = ""
        If l_lngColGenre <> 0 Then l_strGenre = Trim(GetXLData(l_lngXLRowCounter, l_lngColGenre)) Else l_strGenre = ""
        If l_lngColSequence <> 0 Then l_strSequence = Trim(GetXLData(l_lngXLRowCounter, l_lngColSequence)) Else l_strSequence = ""
        If l_lngColRatingType <> 0 Then l_strRatingType = Trim(GetXLData(l_lngXLRowCounter, l_lngColRatingType)) Else l_strRatingType = ""
        If l_lngColRating <> 0 Then l_strRating = Trim(GetXLData(l_lngXLRowCounter, l_lngColRating)) Else l_lngColRating = ""
        If l_lngColOriginalAirDate <> 0 Then l_datOriginalAirDate = Trim(GetXLData(l_lngXLRowCounter, l_lngColOriginalAirDate)) Else l_datOriginalAirDate = 0
        If l_lngColSDOfferStart <> 0 Then
            If Trim(GetXLData(l_lngXLRowCounter, l_lngColSDOfferStart)) <> "" Then
                l_datSDOfferStart = Trim(GetXLData(l_lngXLRowCounter, l_lngColSDOfferStart))
            Else
                l_datSDOfferStart = 0
            End If
        Else
            l_datSDOfferStart = 0
        End If
        If l_lngColHDOfferStart <> 0 Then
            If Trim(GetXLData(l_lngXLRowCounter, l_lngColHDOfferStart)) <> "" Then
                l_datHDOfferStart = Trim(GetXLData(l_lngXLRowCounter, l_lngColHDOfferStart))
            Else
                l_datHDOfferStart = 0
            End If
        Else
            l_datHDOfferStart = 0
        End If
        If l_lngColSDVODOfferStart <> 0 Then
            If Trim(GetXLData(l_lngXLRowCounter, l_lngColSDVODOfferStart)) <> "" Then
                l_datSDVODOfferStart = Trim(GetXLData(l_lngXLRowCounter, l_lngColSDVODOfferStart))
            Else
                l_datSDVODOfferStart = 0
            End If
        Else
            l_datSDVODOfferStart = 0
        End If
        If l_lngColHDVODOfferStart <> 0 Then
            If Trim(GetXLData(l_lngXLRowCounter, l_lngColHDVODOfferStart)) <> "" Then
                l_datHDVODOfferStart = Trim(GetXLData(l_lngXLRowCounter, l_lngColHDVODOfferStart))
            Else
                l_datHDVODOfferStart = 0
            End If
        Else
            l_datHDVODOfferStart = 0
        End If
        If l_lngColCopyright <> 0 Then l_strCopyright = Trim(GetXLData(l_lngXLRowCounter, l_lngColCopyright)) Else l_strCopyright = ""
        If l_lngColCopyrightYear <> 0 Then l_strCopyrightYear = Trim(GetXLData(l_lngXLRowCounter, l_lngColCopyrightYear)) Else l_strCopyrightYear = ""
    
        l_strSQL = "INSERT INTO iTunes_Package (companyID, video_type) VALUES (1163, 'Amazon');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_lngPackageID = g_lngLastID
        
'        l_strLanguage = GetData("iso639language", "iso639Language_2", "LanguageNameEnglish", l_strLanguage)
        If l_strOriginal_Spoken_Locale = "en" Then l_strOriginal_Spoken_Locale = "en-US"
        If l_strLanguage = "en" And l_strOriginal_Spoken_Locale = "en-US" Then l_strLanguage = "en-US"
        If l_strLanguage = "de" Then l_strLanguage = "de-DE"
        l_strSQL = "UPDATE iTunes_Package SET "
        l_strSQL = l_strSQL & "originalspokenlocale = '" & l_strOriginal_Spoken_Locale & "', "
        l_strSQL = l_strSQL & "AmazonCountryOfOrigin = '" & l_strCountryOfOrigin & "', "
        l_strSQL = l_strSQL & "video_language = '" & l_strLanguage & "', "
        l_strSQL = l_strSQL & "country = '" & l_strTerritory & "', "
        l_strSQL = l_strSQL & "AmazonGenre = '" & l_strGenre & "', "
        l_strSQL = l_strSQL & "video_vendorID = '" & l_strUnique_ID & "', "
        l_strSQL = l_strSQL & "video_title = '" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "AmazonSequence = '" & l_strSequence & "', "
        l_strSQL = l_strSQL & "AmazonRatingType = '" & l_strRatingType & "', "
        l_strSQL = l_strSQL & "AmazonRating = '" & IIf(Left(l_strRating, 4) = "FSK ", Mid(l_strRating, 5), l_strRating) & "', "
        l_strSQL = l_strSQL & "AmazonOriginalAirDate = '" & Format(l_datOriginalAirDate, "YYYY-MM-DD") & "', "
        If l_datSDOfferStart <> 0 Then l_strSQL = l_strSQL & "AmazonSDOfferStart = '" & Format(l_datSDOfferStart, "YYYY-MM-DD") & "', " Else l_strSQL = l_strSQL & "AmazonSDOfferStart = Null, "
        If l_datHDOfferStart <> 0 Then l_strSQL = l_strSQL & "AmazonHDOfferStart = '" & Format(l_datHDOfferStart, "YYYY-MM-DD") & "', " Else l_strSQL = l_strSQL & "AmazonHDOfferStart = Null, "
        If l_datSDVODOfferStart <> 0 Then l_strSQL = l_strSQL & "AmazonSDVODOfferStart = '" & Format(l_datSDVODOfferStart, "YYYY-MM-DD") & "', " Else l_strSQL = l_strSQL & "AmazonSDVODOfferStart = Null, "
        If l_datHDVODOfferStart <> 0 Then l_strSQL = l_strSQL & "AmazonHDVODOfferStart = '" & Format(l_datHDVODOfferStart, "YYYY-MM-DD") & "', " Else l_strSQL = l_strSQL & "AmazonHDVODOfferStart = Null, "
        l_strSQL = l_strSQL & "AmazonCopyrightYear = '" & l_strCopyrightYear & "', "
        l_strSQL = l_strSQL & "AmazonCopyrightHolder = '" & QuoteSanitise(l_strCopyright) & "', "
        l_strSQL = l_strSQL & "AmazonEpisodeShortSynopsis = '" & QuoteSanitise(l_strShortSynopsis) & "', "
        l_strSQL = l_strSQL & "AmazonEpisodeLongSynopsis = '" & QuoteSanitise(l_strLongSynopsis) & "' "
        l_strSQL = l_strSQL & "WHERE iTunes_PackageID = " & l_lngPackageID
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    If GetXLData(l_lngXLRowCounter, 1) = "" Then l_lngXLRowCounter = l_lngXLRowCounter + 1
Loop

l_lngXLRowCounter = 2

Do While GetXLData(l_lngXLRowCounter, 1) <> ""

    If GetXLData(l_lngXLRowCounter, l_lngColContentType) = "TV Season" Then
    
        If l_lngColUnique_ID <> 0 Then l_strUnique_ID = Trim(GetXLData(l_lngXLRowCounter, l_lngColUnique_ID)) Else l_strUnique_ID = ""
        If l_lngColTitle <> 0 Then l_strTitle = Trim(GetXLData(l_lngXLRowCounter, l_lngColTitle)) Else l_strTitle = ""
        If l_lngColSeasonProductionCompany <> 0 Then l_strSeasonProductionCompany = Trim(GetXLData(l_lngXLRowCounter, l_lngColSeasonProductionCompany)) Else l_strSeasonProductionCompany = ""
        If l_lngColSeason_Number <> 0 Then l_strSeason_Number = Trim(GetXLData(l_lngXLRowCounter, l_lngColSeason_Number)) Else l_strSeason_Number = ""
        If l_lngColShortSynopsis <> 0 Then l_strShortSynopsis = Trim(GetXLData(l_lngXLRowCounter, l_lngColShortSynopsis)) Else l_strShortSynopsis = ""
        If l_lngColLongSynopsis <> 0 Then l_strLongSynopsis = Trim(GetXLData(l_lngXLRowCounter, l_lngColLongSynopsis)) Else l_strLongSynopsis = ""
    
        l_strSQL = "UPDATE iTunes_Package SET "
        l_strSQL = l_strSQL & "AmazonSeasonTitle = '" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "AmazonSeasonProductionCompany = '" & QuoteSanitise(l_strSeasonProductionCompany) & "', "
        l_strSQL = l_strSQL & "AmazonSeasonNumber = '" & QuoteSanitise(l_strSeason_Number) & "', "
        l_strSQL = l_strSQL & "AmazonSeasonUniqueID = '" & QuoteSanitise(l_strUnique_ID) & "', "
        l_strSQL = l_strSQL & "AmazonSeasonShortSynopsis = '" & QuoteSanitise(l_strShortSynopsis) & "', "
        l_strSQL = l_strSQL & "AmazonSeasonLongSynopsis = '" & QuoteSanitise(l_strLongSynopsis) & "' "
        l_strSQL = l_strSQL & "WHERE video_type = 'Amazon' AND companyID = 1163 AND video_vendorID LIKE '" & Left(l_strUnique_ID, 12) & "%';"
    
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    If GetXLData(l_lngXLRowCounter, 1) = "" Then l_lngXLRowCounter = l_lngXLRowCounter + 1
Loop
    
l_lngXLRowCounter = 2

Do While GetXLData(l_lngXLRowCounter, 1) <> ""

    If GetXLData(l_lngXLRowCounter, l_lngColContentType) = "TV Series" Then
    
        If l_lngColUnique_ID <> 0 Then l_strUnique_ID = Trim(GetXLData(l_lngXLRowCounter, l_lngColUnique_ID)) Else l_strUnique_ID = ""
        If l_lngColTitle <> 0 Then l_strTitle = Trim(GetXLData(l_lngXLRowCounter, l_lngColTitle)) Else l_strTitle = ""
        If l_lngColSeason_Number <> 0 Then l_strSeason_Number = Trim(GetXLData(l_lngXLRowCounter, l_lngColSeason_Number)) Else l_strSeason_Number = ""
        If l_lngColShortSynopsis <> 0 Then l_strShortSynopsis = Trim(GetXLData(l_lngXLRowCounter, l_lngColShortSynopsis)) Else l_strShortSynopsis = ""
        
        l_strSQL = "UPDATE iTunes_Package SET "
        l_strSQL = l_strSQL & "AmazonSeriesName = '" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "AmazonSeriesUniqueID = '" & QuoteSanitise(l_strUnique_ID) & "', "
        l_strSQL = l_strSQL & "AmazonSeriesShortSynopsis = '" & QuoteSanitise(l_strShortSynopsis) & "', "
        l_strSQL = l_strSQL & "AmazonSeriesLongSynopsis = '" & QuoteSanitise(l_strLongSynopsis) & "' "
        l_strSQL = l_strSQL & "WHERE video_type = 'Amazon' AND companyID = 1163 AND video_vendorID LIKE '" & l_strUnique_ID & "%';"
    
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    If GetXLData(l_lngXLRowCounter, 1) = "" Then l_lngXLRowCounter = l_lngXLRowCounter + 1
Loop

lblProgress.Caption = ""

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
MsgBox "Done"

End Sub

Private Sub cmdDisneyiTunesTV_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strTitle As String, l_strVendorID As String, l_strContainerID As String, l_strContainerPosition As String, l_strReleaseDate As String, l_strRating As String, l_strCopyright As String, l_strProvider As String, l_strStudio_Release_Title As String
Dim l_strSynopsis As String, l_strPreviewStartTime As String, l_strTerritory As String, l_strSalesStartDate As String, l_strClearedForSale As String, l_strLanguage As String, l_strOriginalLocale As String, l_strEpisodeProductionNumber As String
Dim l_lngTemp As Long, l_strSQL As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "Cover Sheet", "")
'If l_strExcelSheetName = "" Then
'    oWorkbook.Close
'    Set oWorkbook = Nothing
'    Set oExcel = Nothing
'    Exit Sub
'End If
l_strExcelSheetName = "Sheet1"
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = 2

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    l_strVendorID = Trim(GetXLData(l_lngXLRowCounter, 5))
    lblProgress.Caption = l_strVendorID
    DoEvents
    l_strProvider = "abc"
    l_strLanguage = GetXLData(l_lngXLRowCounter, 3)
    Select Case l_strLanguage
        Case "French-FR"
            l_strLanguage = "fr-FR"
        Case "German-DE"
            l_strLanguage = "de-DE"
        Case "English-GB"
            l_strLanguage = "en-GB"
        Case Else
            MsgBox "Unhandled Language - row may be wrong. Get CETA updated."
    End Select
        
    l_strOriginalLocale = Trim(GetXLData(l_lngXLRowCounter, 4))
    If Len(l_strOriginalLocale) = 2 Then l_strOriginalLocale = LCase(l_strOriginalLocale)
    l_strEpisodeProductionNumber = Trim(GetXLData(l_lngXLRowCounter, 6))
    l_strTitle = Trim(GetXLData(l_lngXLRowCounter, 7))
    l_strContainerID = Trim(GetXLData(l_lngXLRowCounter, 8))
    l_strContainerPosition = Trim(GetXLData(l_lngXLRowCounter, 9))
    l_strReleaseDate = Trim(GetXLData(l_lngXLRowCounter, 10))
    l_strRating = Trim(GetXLData(l_lngXLRowCounter, 11))
    l_strCopyright = Trim(GetXLData(l_lngXLRowCounter, 12))
    l_strSynopsis = Trim(GetXLData(l_lngXLRowCounter, 13))
    l_strPreviewStartTime = Trim(GetXLData(l_lngXLRowCounter, 17))
    l_strTerritory = Trim(GetXLData(l_lngXLRowCounter, 18))
    l_strTerritory = UCase(l_strTerritory)
    l_strSalesStartDate = Trim(GetXLData(l_lngXLRowCounter, 19))
    l_strClearedForSale = Trim(GetXLData(l_lngXLRowCounter, 20))
    l_strStudio_Release_Title = Trim(GetXLData(l_lngXLRowCounter, 21))
    
    l_lngTemp = GetData("iTunes_Package", "iTunes_PackageID", "video_vendorID", l_strVendorID)
    If Val(l_lngTemp) = 0 Then
        l_strSQL = "INSERT INTO iTunes_Package (companyID, provider, video_type, video_vendorID, video_ep_prod_no, video_title, video_title_utf8, studio_release_title, studio_release_title_utf8, video_containerID, video_containerposition, video_releasedate, "
        Select Case l_strTerritory
            Case "FR"
                l_strSQL = l_strSQL & "rating_fr, "
            Case "DE"
                l_strSQL = l_strSQL & "rating_de, "
            Case "GB"
                l_strSQL = l_strSQL & "rating_uk, "
            Case "US"
                l_strSQL = l_strSQL & "rating_us, "
            Case "CA"
                l_strSQL = l_strSQL & "rating_ca, "
            Case Else
                MsgBox "Unhandled Territory - please get CETA updated. Row will be wrong"
        End Select
                
        l_strSQL = l_strSQL & "video_copyright_cline, video_copyright_cline_utf8, video_longdescription, video_longdescription_utf8, video_previewstarttime, video_language, metadata_language, originalspokenlocale, "
        l_strSQL = l_strSQL & "fullcroptop, fullcropbottom, fullcropleft, fullcropright) VALUES ("
        l_strSQL = l_strSQL & "1163, "
        l_strSQL = l_strSQL & "'" & l_strProvider & "', "
        l_strSQL = l_strSQL & "'tv', "
        l_strSQL = l_strSQL & "'" & l_strVendorID & "', "
        l_strSQL = l_strSQL & "'" & l_strEpisodeProductionNumber & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(l_strTitle)) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strStudio_Release_Title) & "', "
        l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(l_strStudio_Release_Title)) & "', "
        l_strSQL = l_strSQL & "'" & l_strContainerID & "', "
        l_strSQL = l_strSQL & "'" & l_strContainerPosition & "', "
        l_strSQL = l_strSQL & "'" & Format(l_strReleaseDate, "YYYY-MM-DD") & "', "
        l_strSQL = l_strSQL & "'" & l_strRating & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strCopyright) & "', "
        l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(l_strCopyright)) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSynopsis) & "', "
        l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(l_strSynopsis)) & "', "
        l_strSQL = l_strSQL & l_strPreviewStartTime & ", "
        l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
        l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
        l_strSQL = l_strSQL & "'" & l_strOriginalLocale & "', 4, 4, 4, 4);"
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_lngTemp = g_lngLastID
        
        
        l_strSQL = "INSERT INTO iTunes_product (iTunes_packageID, territory, salesstartdate, clearedforsale) VALUES ("
        l_strSQL = l_strSQL & l_lngTemp & ", "
        l_strSQL = l_strSQL & "'" & l_strTerritory & "', "
        l_strSQL = l_strSQL & "'" & Format(l_strSalesStartDate, "YYYY-MM-DD") & "', "
        l_strSQL = l_strSQL & "'" & l_strClearedForSale & "');"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    Else
        MsgBox "Vendor ID: " & l_strVendorID & " is already present in the iTunes Packages list. This line was not read in." & vbCrLf & "If you wish to read this line in, please manually delete the old package first.", vbInformation
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1
Loop
lblProgress.Caption = ""

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
MsgBox "Done"

End Sub

Private Sub cmdDisneyTracker_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strSeasonTitle As String, l_strEpisode As String, l_strEIDR As String
Dim l_lngTrackerID As Long

Dim l_strSQL As String, l_datMaterialsInDate As Date

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = "IDENTIFIERS"
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = 2

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_strSeasonTitle = GetXLData(l_lngXLRowCounter, 2)
    l_strEpisode = GetXLData(l_lngXLRowCounter, 8)
    l_strEIDR = GetXLData(l_lngXLRowCounter, 12)
    
    lblProgress.Caption = "Row: " & l_lngXLRowCounter
    DoEvents
    
    If l_strSeasonTitle <> "" Then
        
        Debug.Print l_strEIDR
        If GetCount("SELECT count(tracker_iTunes_itemID) FROM tracker_itunes_item WHERE title = '" & QuoteSanitise(l_strSeasonTitle) & "' AND episode = '" & QuoteSanitise(l_strEpisode) & "' AND iTunesOrderType = 'Xbox' AND readytobill = 0;") = 1 Then
            l_lngTrackerID = GetDataSQL("SELECT TOP 1 tracker_iTunes_itemID FROM tracker_itunes_item WHERE title = '" & QuoteSanitise(l_strSeasonTitle) & "' AND episode = '" & QuoteSanitise(l_strEpisode) & "' AND iTunesOrderType = 'Xbox' AND readytobill = 0;")
            l_strSQL = "UPDATE tracker_itunes_item SET EIDR = '" & l_strEIDR & "' WHERE tracker_itunes_itemID = " & l_lngTrackerID & ";"
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
        End If
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Loop

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

Private Sub cmdGoogle_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_strExcelSheetName As String, l_lngXLRowCounter As Long, l_strSQL As String, l_rst As ADODB.Recordset, l_blnDone As Boolean

Dim Video_Order_Id As String, Google_Channel_ID As String, Title As String, Show_Season_Title As String, Episodic_Title As String, Original_Episodic_Title As String
Dim Territory As String, Season_Name As String, Series_Number As Long, Episode_Number As Long, FLM_Episode_Number As Long, l_strOrder_Date As String, l_strDue_Date As String
Dim l_strAccess_Letter_Received As String, Disney_Coordinator As String, Key_Words As String, Show_Genre As String, Episode_Genre As String
Dim Rating_System As String, Rating As String, Spoken_Language As String, Custom_ID_MPM_Product_ID As String, Production_Year As String, l_strOriginal_Release_Date As String
Dim Show_Description As String, Season_Description As String, Episodic_Description As String, WMLS_Number As String, l_strPull_Date As String, Pull_ID As Long, l_strReceived_Date As String
Dim Incoming_File_Name As String, Google_Video_File_Name As String, File_Location_Barcode As String, File_Location As String
Dim l_strArtwork_Due_On As String, l_strArtwork_Pull_Date As String, Landscape As String, Square As String, l_strArtwork_Received_Date As String
Dim l_strMetadata_Due_From_Disney As String, l_strMetadata_Received_Date As String, l_strGoogle_Metadata_Complete As String
Dim Subtitles_Required As String, l_strSubtitles_Due As String, l_strSubtitles_Received As String
Dim JobID As Long, l_strGoogle_Due_Date As String, l_strRRmedia_Due_Date As String, l_strActual_Delivery_Date As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "BBCW Ingest Queue Inventory Rep")
l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "Disney TV")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "DADC Excel Read", 5))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

Do While GetXLData(l_lngXLRowCounter, 1) <> ""

    l_blnDone = False
    lblProgress.Caption = "Row: " & l_lngXLRowCounter & ", Order: " & GetXLData(l_lngXLRowCounter, 1)
    DoEvents
    
    'Read the row
    Video_Order_Id = GetXLData(l_lngXLRowCounter, 1)
    Google_Channel_ID = GetXLData(l_lngXLRowCounter, 2)
    Title = GetXLData(l_lngXLRowCounter, 3)
    Show_Season_Title = GetXLData(l_lngXLRowCounter, 4)
    Episodic_Title = GetXLData(l_lngXLRowCounter, 5)
    Original_Episodic_Title = GetXLData(l_lngXLRowCounter, 6)
    Territory = GetXLData(l_lngXLRowCounter, 7)
    Season_Name = GetXLData(l_lngXLRowCounter, 8)
    Series_Number = Val(GetXLData(l_lngXLRowCounter, 9))
    FLM_Episode_Number = Val(GetXLData(l_lngXLRowCounter, 10))
    Episode_Number = Val(GetXLData(l_lngXLRowCounter, 11))
    l_strOrder_Date = GetXLData(l_lngXLRowCounter, 12)
    l_strDue_Date = GetXLData(l_lngXLRowCounter, 13)
    l_strAccess_Letter_Received = GetXLData(l_lngXLRowCounter, 14)
    Disney_Coordinator = GetXLData(l_lngXLRowCounter, 15)
    Key_Words = GetXLData(l_lngXLRowCounter, 16)
    Show_Genre = GetXLData(l_lngXLRowCounter, 17)
    Episode_Genre = GetXLData(l_lngXLRowCounter, 18)
    Rating_System = GetXLData(l_lngXLRowCounter, 19)
    Rating = GetXLData(l_lngXLRowCounter, 20)
    Spoken_Language = GetXLData(l_lngXLRowCounter, 21)
    Custom_ID_MPM_Product_ID = GetXLData(l_lngXLRowCounter, 22)
    Production_Year = GetXLData(l_lngXLRowCounter, 23)
    l_strOriginal_Release_Date = GetXLData(l_lngXLRowCounter, 24)
    Show_Description = GetXLData(l_lngXLRowCounter, 25)
    Season_Description = GetXLData(l_lngXLRowCounter, 26)
    Episodic_Description = GetXLData(l_lngXLRowCounter, 27)
    WMLS_Number = GetXLData(l_lngXLRowCounter, 28)
    l_strPull_Date = GetXLData(l_lngXLRowCounter, 29)
    Pull_ID = Val(GetXLData(l_lngXLRowCounter, 30))
    l_strReceived_Date = GetXLData(l_lngXLRowCounter, 31)
    Incoming_File_Name = GetXLData(l_lngXLRowCounter, 32)
    Google_Video_File_Name = GetXLData(l_lngXLRowCounter, 33)
    File_Location = GetXLData(l_lngXLRowCounter, 34)
    Landscape = GetXLData(l_lngXLRowCounter, 35)
    Square = GetXLData(l_lngXLRowCounter, 36)
    l_strArtwork_Received_Date = GetXLData(l_lngXLRowCounter, 37)
    l_strMetadata_Received_Date = GetXLData(l_lngXLRowCounter, 38)
    Subtitles_Required = GetXLData(l_lngXLRowCounter, 39)
    l_strSubtitles_Due = GetXLData(l_lngXLRowCounter, 40)
    l_strSubtitles_Received = GetXLData(l_lngXLRowCounter, 41)
    JobID = Val(GetXLData(l_lngXLRowCounter, 42))
    l_strGoogle_Due_Date = GetXLData(l_lngXLRowCounter, 43)
    l_strRRmedia_Due_Date = GetXLData(l_lngXLRowCounter, 44)
    l_strActual_Delivery_Date = GetXLData(l_lngXLRowCounter, 45)
    
'    If IsDate(l_strAccess_Letter_Received) Then access_latter_received = l_strAccess_Letter_Received Else Access_Letter_Received = Null
'    If IsDate(l_strOriginal_Release_Date) Then Original_Release_Date = l_strOriginal_Release_Date Else Original_Release_Date = Null
'    If IsDate(l_strpulldate) Then Pull_Date = l_strpulldate Else Pull_Date = Null
'    If IsDate(l_strReceived_Date) Then Received_Date = l_strReceived_Date Else receoved_date = Null
'    If IsDate(l_strArtwork_Due_On) Then Artwork_Due_On = l_strArtwork_Due_On Else Artwork_Due_On = Null
'    If IsDate(l_strArtwork_Pull_Date) Then Artwork_Pull_Date = l_strArtwork_Pull_Date Else Artwork_Pull_Date = Null
'    If IsDate(l_strArtwork_Received_Date) Then Artwork_Received_Date = l_strArtwork_Received_Date Else Artwork_Received_Date = Null
'    If IsDate(l_strMetadata_Due_From_Disney) Then Metadata_Due_From_Disney = l_strMetadata_Due_From_Disney Else Metadata_Due_From_Disney = Null
'    If IsDate(l_strMetadata_Received_Date) Then Metadata_Received_Date = l_strMetadata_Received_Date Else Metadata_Received_Date = Null
'    If IsDate(l_strGoogle_Metadata_Complete) Then Google_Metadata_Complete = l_strGoogle_Metadata_Complete Else Google_Metadata_Complete = Null
'    If IsDate(l_strSubtitles_Due) Then Subtitles_Due = l_strSubtitles_Due Else Subtitles_Due = Null
'    If IsDate(l_strSubtitles_Received) Then Subtitles_Received = l_strSubtitles_Received Else Subtitles_Received = Null
'    If IsDate(l_strGoogle_Due_Date) Then Google_Due_Date = l_strGoogle_Due_Date Else Google_Due_Date = Null
'    If IsDate(l_strRRmedia_Due_Date) Then RRmedia_Due_Date = l_strRRmedia_Due_Date Else RRmedia_Due_Date = Null
'    If IsDate(l_strActual_Delivery_Date) Then Actual_Delivery_Date = l_strActual_Delivery_Date Else Actual_Delivery_Date = Null
'
    'Handle the Update or insert
    l_strSQL = "SELECT tracker_google_itemID, readytobill FROM tracker_google_item WHERE Video_Order_Id = '" & Video_Order_Id & "';"
    Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
    If l_rst.RecordCount > 0 Then
        l_rst.MoveFirst
        Do While Not l_rst.EOF
            If l_rst("readytobill") = 0 Then
                'Modify the line
                l_strSQL = "UPDATE tracker_google_item SET "
                l_strSQL = l_strSQL & "Video_Order_Id = '" & Video_Order_Id & "', "
                l_strSQL = l_strSQL & "Google_Channel_ID = '" & Google_Channel_ID & "', "
                l_strSQL = l_strSQL & "Title = '" & QuoteSanitise(Title) & "', "
                l_strSQL = l_strSQL & "Show_Season_Title = '" & QuoteSanitise(Show_Season_Title) & "', "
                l_strSQL = l_strSQL & "Episodic_Title = '" & QuoteSanitise(Episodic_Title) & "', "
                l_strSQL = l_strSQL & "Original_Episodic_Title = '" & QuoteSanitise(Original_Episodic_Title) & "', "
                l_strSQL = l_strSQL & "Territory = '" & QuoteSanitise(Territory) & "', "
                l_strSQL = l_strSQL & "Season_Name = '" & QuoteSanitise(Season_Name) & "', "
                l_strSQL = l_strSQL & "Series_Number = " & Series_Number & ", "
                l_strSQL = l_strSQL & "Episode_Number = " & Episode_Number & ", "
                l_strSQL = l_strSQL & "Disney_FLM_Episode_Number = " & FLM_Episode_Number & ", "
                If IsDate(l_strOrder_Date) Then l_strSQL = l_strSQL & "Order_Date = '" & Format(l_strOrder_Date, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Order_Date = Null, "
                If IsDate(l_strDue_Date) Then l_strSQL = l_strSQL & "Due_Date = '" & Format(l_strDue_Date, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Due_Date = Null, "
                If IsDate(l_strAccess_Letter_Received) Then l_strSQL = l_strSQL & "Access_Letter_Received = '" & Format(l_strAccess_Letter_Received, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Access_Letter_Received = Null, "
                l_strSQL = l_strSQL & "Disney_Coordinator = '" & QuoteSanitise(Disney_Coordinator) & "', "
                l_strSQL = l_strSQL & "Key_Words = '" & QuoteSanitise(Key_Words) & "', "
                l_strSQL = l_strSQL & "Show_Genre = '" & QuoteSanitise(Show_Genre) & "', "
                l_strSQL = l_strSQL & "Episode_Genre = '" & QuoteSanitise(Episode_Genre) & "', "
                l_strSQL = l_strSQL & "Rating_System = '" & Rating_System & "', "
                l_strSQL = l_strSQL & "Rating = '" & Rating & "', "
                l_strSQL = l_strSQL & "Spoken_Language = '" & Spoken_Language & "', "
                l_strSQL = l_strSQL & "Custom_ID_MPM_Product_ID = '" & Custom_ID_MPM_Product_ID & "', "
                l_strSQL = l_strSQL & "Production_Year = '" & Production_Year & "', "
                If IsDate(l_strOriginal_Release_Date) Then l_strSQL = l_strSQL & "Original_Release_Date = '" & FormatSQLDate(l_strOriginal_Release_Date) & "', " Else l_strSQL = l_strSQL & "Original_Release_Date = Null, "
                l_strSQL = l_strSQL & "Show_Description = '" & QuoteSanitise(Show_Description) & "', "
                l_strSQL = l_strSQL & "Season_Description = '" & QuoteSanitise(Season_Description) & "', "
                l_strSQL = l_strSQL & "Episodic_Description = '" & QuoteSanitise(Episodic_Description) & "', "
                l_strSQL = l_strSQL & "WMLS_Number = '" & WMLS_Number & "', "
                If IsDate(l_strPull_Date) Then l_strSQL = l_strSQL & "Pull_Date = '" & Format(l_strPull_Date, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Pull_Date = Null, "
                l_strSQL = l_strSQL & "Pull_ID = " & Pull_ID & ", "
                If IsDate(l_strReceived_Date) Then l_strSQL = l_strSQL & "Received_Date = '" & Format(l_strReceived_Date, "DD mmm YY") & "', " Else l_strSQL = l_strSQL & "Received_Date = Null, "
                l_strSQL = l_strSQL & "Incoming_File_Name = '" & Incoming_File_Name & "', "
                l_strSQL = l_strSQL & "Google_Video_File_Name = '" & Google_Video_File_Name & "', "
                l_strSQL = l_strSQL & "File_Location_Barcode = '" & File_Location_Barcode & "', "
        '        l_strSQL = l_strSQL & File_Location
                If IsDate(l_strArtwork_Due_On) Then l_strSQL = l_strSQL & "Artwork_Due_On = '" & Format(l_strArtwork_Due_On, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Artwork_Due_On = Null, "
                If IsDate(l_strArtwork_Pull_Date) Then l_strSQL = l_strSQL & "Artwork_Pull_Date = '" & Format(l_strArtwork_Pull_Date, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Artwork_Pull_Date = Null, "
                l_strSQL = l_strSQL & "Landscape = '" & Landscape & "', "
                l_strSQL = l_strSQL & "Square = '" & Square & "', "
                If IsDate(l_strArtwork_Received_Date) Then l_strSQL = l_strSQL & "Artwork_Received_Date = '" & Format(l_strArtwork_Received_Date, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Artwork_Received_Date = Null, "
                If IsDate(l_strMetadata_Due_From_Disney) Then l_strSQL = l_strSQL & "Metadata_Due_From_Disney = '" & Format(l_strMetadata_Due_From_Disney, "DD mmm YY") & "', " Else l_strSQL = l_strSQL & "Metadata_Due_From_Disney = Null, "
                If IsDate(l_strMetadata_Received_Date) Then l_strSQL = l_strSQL & "Metadata_Received_Date = '" & Format(l_strMetadata_Received_Date, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Metadata_Received_Date = Null, "
                If IsDate(l_strGoogle_Metadata_Complete) Then l_strSQL = l_strSQL & "Google_Metadata_Complete = '" & Format(l_strGoogle_Metadata_Complete, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Google_Metadata_Complete = Null, "
                If UCase(Subtitles_Required) = "YES" Then l_strSQL = l_strSQL & "Subtitles_Required = 1, " Else l_strSQL = l_strSQL & "Subtitles_Required = 0, "
                If IsDate(l_strSubtitles_Due) Then l_strSQL = l_strSQL & "Subtitles_Due = '" & Format(l_strSubtitles_Due, "DD mmm YY") & "', " Else l_strSQL = l_strSQL & "Subtitles_Due = Null, "
                If IsDate(l_strSubtitles_Received) Then l_strSQL = l_strSQL & "Subtitles_Received = '" & Format(l_strSubtitles_Received, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Subtitles_Received = Null, "
                l_strSQL = l_strSQL & "JobID = '" & JobID & "', "
                If IsDate(l_strGoogle_Due_Date) Then l_strSQL = l_strSQL & "Google_Due_Date = '" & Format(l_strGoogle_Due_Date, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Google_Due_Date = Null, "
                If IsDate(l_strRRmedia_Due_Date) Then l_strSQL = l_strSQL & "RRmedia_Due_Date = '" & Format(l_strRRmedia_Due_Date, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "RRmedia_Due_Date = Null, "
                If IsDate(l_strActual_Delivery_Date) Then l_strSQL = l_strSQL & "Actual_Delivery_Date = '" & Format(l_strActual_Delivery_Date, "DD mmm YYYY") & "' " Else l_strSQL = l_strSQL & "Actual_Delivery_Date = Null "
                l_strSQL = l_strSQL & "WHERE tracker_google_itemID = " & l_rst("tracker_google_itemID") & ";"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                Exit Do
            End If
            l_rst.MoveNext
        Loop
        l_blnDone = True
    End If
    l_rst.Close
    
    If l_blnDone = False Then
        'Insert the line
        l_strSQL = "INSERT INTO tracker_google_item (CompanyID, Video_Order_Id, Google_Channel_ID, Title, Show_Season_Title, Episodic_Title, Original_Episodic_Title, Territory, " & _
        "Season_Name, Series_Number, Episode_Number, Disney_FLM_Episode_Number, Order_Date, Due_Date, Access_Letter_Received, Disney_Coordinator, Key_Words, Show_Genre, Episode_Genre, Rating_System, Rating, Spoken_Language, " & _
        "Custom_ID_MPM_Product_ID, Production_Year, Original_Release_Date, Show_Description, Season_Description, Episodic_Description, WMLS_Number, Pull_Date, Pull_ID, Received_Date, Incoming_File_Name, Google_Video_File_Name, " & _
        "File_Location_Barcode, Artwork_Due_On, Artwork_Pull_Date, Landscape, Square, Artwork_Received_Date, Metadata_Due_From_Disney, Metadata_Received_Date, Google_Metadata_Complete, " & _
        "Subtitles_Required, Subtitles_Due, Subtitles_Received, JobID, Google_Due_Date, RRmedia_Due_Date, Actual_Delivery_Date) VALUES ("
        l_strSQL = l_strSQL & "1489, "
        l_strSQL = l_strSQL & "'" & Video_Order_Id & "', "
        l_strSQL = l_strSQL & "'" & Google_Channel_ID & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(Title) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(Show_Season_Title) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(Episodic_Title) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(Original_Episodic_Title) & "', "
        l_strSQL = l_strSQL & "'" & Territory & "', "
        l_strSQL = l_strSQL & "'" & Season_Name & "', "
        l_strSQL = l_strSQL & "'" & Series_Number & "', "
        l_strSQL = l_strSQL & Episode_Number & ", "
        l_strSQL = l_strSQL & FLM_Episode_Number & ", "
        If IsDate(l_strOrder_Date) Then l_strSQL = l_strSQL & "'" & Format(l_strOrder_Date, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Null, "
        If IsDate(l_strDue_Date) Then l_strSQL = l_strSQL & "'" & Format(l_strDue_Date, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Null, "
        If IsDate(l_strAccess_Letter_Received) Then l_strSQL = l_strSQL & "'" & Format(l_strAccess_Letter_Received, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Null, "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(Disney_Coordinator) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(Key_Words) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(Show_Genre) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(Episode_Genre) & "', "
        l_strSQL = l_strSQL & "'" & Rating_System & "', "
        l_strSQL = l_strSQL & "'" & Rating & "', "
        l_strSQL = l_strSQL & "'" & Spoken_Language & "', "
        l_strSQL = l_strSQL & "'" & Custom_ID_MPM_Product_ID & "', "
        l_strSQL = l_strSQL & "'" & Production_Year & "', "
        If IsDate(l_strOriginal_Release_Date) Then l_strSQL = l_strSQL & "'" & Format(l_strOriginal_Release_Date, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Null, "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(Show_Description) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(Season_Description) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(Episodic_Description) & "', "
        l_strSQL = l_strSQL & "'" & WMLS_Number & "', "
        If IsDate(l_strPull_Date) Then l_strSQL = l_strSQL & "'" & Format(l_strPull_Date, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Null, "
        l_strSQL = l_strSQL & Pull_ID & ", "
        If IsDate(l_strReceived_Date) Then l_strSQL = l_strSQL & "'" & Format(l_strReceived_Date, "DD mmm YY") & "', " Else l_strSQL = l_strSQL & "Null, "
        l_strSQL = l_strSQL & "'" & Incoming_File_Name & "', "
        l_strSQL = l_strSQL & "'" & Google_Video_File_Name & "', "
        l_strSQL = l_strSQL & "'" & File_Location_Barcode & "', "
'        l_strSQL = l_strSQL & File_Location
        If IsDate(l_strArtwork_Due_On) Then l_strSQL = l_strSQL & "'" & Format(l_strArtwork_Due_On, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Null, "
        If IsDate(l_strArtwork_Pull_Date) Then l_strSQL = l_strSQL & "'" & Format(l_strArtwork_Pull_Date, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Null, "
        l_strSQL = l_strSQL & "'" & Landscape & "', "
        l_strSQL = l_strSQL & "'" & Square & "', "
        If IsDate(l_strArtwork_Received_Date) Then l_strSQL = l_strSQL & "'" & Format(l_strArtwork_Received_Date, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Null, "
        If IsDate(l_strMetadata_Due_From_Disney) Then l_strSQL = l_strSQL & "'" & Format(l_strMetadata_Due_From_Disney, "DD mmm YY") & "', " Else l_strSQL = l_strSQL & "Null, "
        If IsDate(l_strMetadata_Received_Date) Then l_strSQL = l_strSQL & "'" & Format(l_strMetadata_Received_Date, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Null, "
        If IsDate(l_strGoogle_Metadata_Complete) Then l_strSQL = l_strSQL & "'" & Format(l_strGoogle_Metadata_Complete, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Null, "
        If UCase(Subtitles_Required) = "YES" Then l_strSQL = l_strSQL & "1, " Else l_strSQL = l_strSQL & "0, "
        If IsDate(l_strSubtitles_Due) Then l_strSQL = l_strSQL & "'" & Format(l_strSubtitles_Due, "DD mmm YY") & "', " Else l_strSQL = l_strSQL & "Null, "
        If IsDate(l_strSubtitles_Received) Then l_strSQL = l_strSQL & "'" & Format(l_strSubtitles_Received, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Null, "
        l_strSQL = l_strSQL & "'" & JobID & "', "
        If IsDate(l_strGoogle_Due_Date) Then l_strSQL = l_strSQL & "'" & Format(l_strGoogle_Due_Date, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Null, "
        If IsDate(l_strRRmedia_Due_Date) Then l_strSQL = l_strSQL & "'" & Format(l_strRRmedia_Due_Date, "DD mmm YYYY") & "', " Else l_strSQL = l_strSQL & "Null, "
        If IsDate(l_strActual_Delivery_Date) Then l_strSQL = l_strSQL & "'" & Format(l_strActual_Delivery_Date, "DD mmm YYYY") & "');" Else l_strSQL = l_strSQL & "Null);"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError

    End If

    l_lngXLRowCounter = l_lngXLRowCounter + 1

Loop
Set l_rst = Nothing

lblProgress.Caption = ""
Set oWorksheet = Nothing
oWorkbook.Close vbNo
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

Beep

End Sub

Private Sub cmdHattrickTunesTV_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strTitle As String, l_strVendorID As String, l_strContainerID As String, l_strContainerPosition As String, l_strReleaseDate As String, l_strRating As String, l_strCopyright As String, l_strProvider As String, l_strStudio_Release_Title As String
Dim l_strSynopsis As String, l_strPreviewStartTime As String, l_strTerritory As String, l_strSalesStartDate As String, l_strClearedForSale As String, l_strLanguage As String, l_strOriginalLocale As String, l_strEpisodeProductionNumber As String
Dim l_lngTemp As Long, l_strSQL As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "Cover Sheet", "")
'If l_strExcelSheetName = "" Then
'    oWorkbook.Close
'    Set oWorkbook = Nothing
'    Set oExcel = Nothing
'    Exit Sub
'End If
l_strExcelSheetName = "Sheet1"
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = 2

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    l_strVendorID = Trim(GetXLData(l_lngXLRowCounter, 5))
    lblProgress.Caption = l_strVendorID
    DoEvents
    l_strProvider = "HatTrickProductionsLimited"
    l_strLanguage = GetXLData(l_lngXLRowCounter, 3)
    Select Case l_strLanguage
        Case "French-FR"
            l_strLanguage = "fr-FR"
        Case "German-DE"
            l_strLanguage = "de-DE"
        Case "English-GB"
            l_strLanguage = "en-GB"
        Case Else
            If l_strLanguage <> "en" Then
                MsgBox "Unhandled Language - row may be wrong. Get CETA updated."
            Else
                'Do nothing
            End If
    End Select
        
    l_strOriginalLocale = Trim(GetXLData(l_lngXLRowCounter, 4))
    If Len(l_strOriginalLocale) = 2 Then l_strOriginalLocale = LCase(l_strOriginalLocale)
    l_strEpisodeProductionNumber = Trim(GetXLData(l_lngXLRowCounter, 6))
    l_strTitle = Trim(GetXLData(l_lngXLRowCounter, 7))
    l_strContainerID = Trim(GetXLData(l_lngXLRowCounter, 8))
    l_strContainerPosition = Trim(GetXLData(l_lngXLRowCounter, 9))
    l_strReleaseDate = Trim(GetXLData(l_lngXLRowCounter, 10))
    l_strRating = Trim(GetXLData(l_lngXLRowCounter, 11))
    l_strCopyright = Trim(GetXLData(l_lngXLRowCounter, 12))
    l_strSynopsis = Trim(GetXLData(l_lngXLRowCounter, 13))
    l_strPreviewStartTime = Trim(GetXLData(l_lngXLRowCounter, 17))
    l_strTerritory = Trim(GetXLData(l_lngXLRowCounter, 18))
    l_strTerritory = UCase(l_strTerritory)
    l_strSalesStartDate = Trim(GetXLData(l_lngXLRowCounter, 19))
    l_strClearedForSale = Trim(GetXLData(l_lngXLRowCounter, 20))
    l_strStudio_Release_Title = Trim(GetXLData(l_lngXLRowCounter, 21))
    
    l_lngTemp = GetData("iTunes_Package", "iTunes_PackageID", "video_vendorID", l_strVendorID)
    If Val(l_lngTemp) = 0 Then
        l_strSQL = "INSERT INTO iTunes_Package (companyID, provider, video_type, video_vendorID, video_ep_prod_no, video_title, video_title_utf8, studio_release_title, studio_release_title_utf8, video_containerID, video_containerposition, video_releasedate, "
        Select Case l_strTerritory
            Case "FR"
                l_strSQL = l_strSQL & "rating_fr, "
            Case "DE"
                l_strSQL = l_strSQL & "rating_de, "
            Case "GB"
                l_strSQL = l_strSQL & "rating_uk, "
            Case "US"
                l_strSQL = l_strSQL & "rating_us, "
            Case "CA"
                l_strSQL = l_strSQL & "rating_ca, "
            Case Else
                MsgBox "Unhandled Territory - please get CETA updated. Row will be wrong"
        End Select
                
        l_strSQL = l_strSQL & "video_copyright_cline, video_copyright_cline_utf8, video_longdescription, video_longdescription_utf8, video_previewstarttime, video_language, metadata_language, originalspokenlocale, "
        l_strSQL = l_strSQL & "fullcroptop, fullcropbottom, fullcropleft, fullcropright) VALUES ("
        l_strSQL = l_strSQL & "305, "
        l_strSQL = l_strSQL & "'" & l_strProvider & "', "
        l_strSQL = l_strSQL & "'tv', "
        l_strSQL = l_strSQL & "'" & l_strVendorID & "', "
        l_strSQL = l_strSQL & "'" & l_strEpisodeProductionNumber & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(l_strTitle)) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strStudio_Release_Title) & "', "
        l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(l_strStudio_Release_Title)) & "', "
        l_strSQL = l_strSQL & "'" & l_strContainerID & "', "
        l_strSQL = l_strSQL & "'" & l_strContainerPosition & "', "
        l_strSQL = l_strSQL & "'" & Format(l_strReleaseDate, "YYYY-MM-DD") & "', "
        l_strSQL = l_strSQL & "'" & l_strRating & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strCopyright) & "', "
        l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(l_strCopyright)) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSynopsis) & "', "
        l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(l_strSynopsis)) & "', "
        l_strSQL = l_strSQL & l_strPreviewStartTime & ", "
        l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
        l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
        l_strSQL = l_strSQL & "'" & l_strOriginalLocale & "', 4, 4, 4, 4);"
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_lngTemp = g_lngLastID
        
        
        l_strSQL = "INSERT INTO iTunes_product (iTunes_packageID, territory, salesstartdate, clearedforsale) VALUES ("
        l_strSQL = l_strSQL & l_lngTemp & ", "
        l_strSQL = l_strSQL & "'" & l_strTerritory & "', "
        l_strSQL = l_strSQL & "'" & Format(l_strSalesStartDate, "YYYY-MM-DD") & "', "
        l_strSQL = l_strSQL & "'" & l_strClearedForSale & "');"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    Else
        MsgBox "Vendor ID: " & l_strVendorID & " is already present in the iTunes Packages list. This line was not read in." & vbCrLf & "If you wish to read this line in, please manually delete the old package first.", vbInformation
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1
Loop
lblProgress.Caption = ""

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
MsgBox "Done"

End Sub

Private Sub cmdHDDtask_Click()

'If Not CheckAccess("/processexcelorders") Then
'    Exit Sub
'End If


Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long

Dim l_strClient As String, l_strTerritory As String, l_lngRequestID As Long, l_lngLineItemID As Long, l_datDueDate As Date
Dim l_datDoNotDeliverBeforeDate As Date, l_strRequestName As String, l_strScheduledBy As String, l_strComponentsAvailable As String
Dim l_strSeriesTitle As String, l_strEpisodeTitle As String, l_strEspisodeNumber As String, l_strCVDisplayName As String, l_strFileLocation As String
Dim l_strFilename As String, l_dblFileSizeGB As Double, l_strFormatRequiredType As String, l_strClientContact As String, l_strDestination As String
Dim l_strStatus As String, l_lngShippingMethodTypeID As Long, l_strTrackingNumber As String, l_strMediaRequiredType As String, l_strMediaType As String
Dim l_strMediaBarcodeNo As String, l_datShipDate As Date, l_strDepartmentChargeCode As String

Dim l_strSQL As String, l_lngLineID As Long

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

l_strClient = 0
l_strTerritory = 0
l_lngRequestID = 0
l_lngLineItemID = 0
l_datDueDate = 0
l_datDoNotDeliverBeforeDate = 0
l_strRequestName = 0
l_strScheduledBy = 0
l_strComponentsAvailable = 0
l_strSeriesTitle = 0
l_strEpisodeTitle = 0
l_strEspisodeNumber = 0
l_strCVDisplayName = 0
l_strFileLocation = 0
l_strFilename = 0
l_dblFileSizeGB = 0
l_strFormatRequiredType = 0
l_strClientContact = 0
l_strDestination = 0
l_strStatus = 0
l_lngShippingMethodTypeID = 0
l_strTrackingNumber = 0
l_strMediaRequiredType = 0
l_strMediaType = 0
l_strMediaBarcodeNo = 0
l_datShipDate = 0
l_strDepartmentChargeCode = 0


Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = "BBCW HDD Task Report"
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = 4

Do While GetXLData(l_lngXLRowCounter, 2) <> ""
    
    l_strClient = Trim(GetXLData(l_lngXLRowCounter, 1)) 'A
    l_strTerritory = Trim(GetXLData(l_lngXLRowCounter, 2)) 'B
    l_lngRequestID = Trim(Val(GetXLData(l_lngXLRowCounter, 3))) 'C
    l_lngLineItemID = Trim(Val(GetXLData(l_lngXLRowCounter, 4))) 'D
 '   If IsDate(l_datDueDate = GetXLData(l_lngXLRowCounter, 5)) Then l_datDueDate = GetXLData(l_lngXLRowCounter, 5) Else l_datDueDate = 0
 '   If IsDate(l_datDoNotDeliverBeforeDate = GetXLData(l_lngXLRowCounter, 6)) Then l_datDoNotDeliverBeforeDate = GetXLData(l_lngXLRowCounter, 6) Else l_datDoNotDeliverBeforeDate = 0
    l_datDueDate = Trim(GetXLData(l_lngXLRowCounter, 5)) 'E
    l_datDoNotDeliverBeforeDate = Trim(GetXLData(l_lngXLRowCounter, 6)) 'F
    l_strRequestName = Trim(GetXLData(l_lngXLRowCounter, 7)) 'G
    l_strScheduledBy = Trim(GetXLData(l_lngXLRowCounter, 8)) 'H
    l_strComponentsAvailable = Trim(GetXLData(l_lngXLRowCounter, 9)) 'I
    l_strSeriesTitle = Trim(GetXLData(l_lngXLRowCounter, 10)) 'J
    l_strEpisodeTitle = Trim(GetXLData(l_lngXLRowCounter, 11)) 'K
    l_strEspisodeNumber = Trim(GetXLData(l_lngXLRowCounter, 12)) 'L
    l_strCVDisplayName = Trim(GetXLData(l_lngXLRowCounter, 13)) 'M
    l_strFileLocation = Trim(GetXLData(l_lngXLRowCounter, 14)) 'N
    l_strFilename = Trim(GetXLData(l_lngXLRowCounter, 15)) 'O
    l_dblFileSizeGB = Trim(Val(GetXLData(l_lngXLRowCounter, 16))) 'P
    l_strFormatRequiredType = Trim(GetXLData(l_lngXLRowCounter, 17)) 'Q
    l_strClientContact = Trim(GetXLData(l_lngXLRowCounter, 18)) 'R
    l_strDestination = Trim(GetXLData(l_lngXLRowCounter, 19)) 'S
    l_strStatus = Trim(GetXLData(l_lngXLRowCounter, 20)) 'T
    l_lngShippingMethodTypeID = Trim(Val(GetXLData(l_lngXLRowCounter, 21))) 'U
    l_strTrackingNumber = Trim(GetXLData(l_lngXLRowCounter, 22)) 'V
    l_strMediaRequiredType = Trim(GetXLData(l_lngXLRowCounter, 23)) 'W
    l_strMediaType = Trim(GetXLData(l_lngXLRowCounter, 24)) 'X
    l_strMediaBarcodeNo = Trim(GetXLData(l_lngXLRowCounter, 25)) 'Y
    If IsDate(l_datShipDate = Trim(GetXLData(l_lngXLRowCounter, 26))) Then l_datShipDate = GetXLData(l_lngXLRowCounter, 26) Else l_datShipDate = 0 'Z
    l_strDepartmentChargeCode = Trim(GetXLData(l_lngXLRowCounter, 28)) 'AB
    
    lblProgress.Caption = l_strClient
    DoEvents
    
    l_strSQL = "SELECT TOP 1 tracker_hdd_itemID FROM tracker_hdd_item WHERE LineItemID = " & l_lngLineItemID & ";"
    l_lngLineID = Val(GetDataSQL(l_strSQL))
    If l_lngLineID <> 0 Then
        
        'Existing Line; update the file reference and the status
        If Trim(" " & GetData("tracker_hdd_item", "datearrived", "tracker_hdd_itemID", l_lngLineID)) = "0" Then
            l_strSQL = "UPDATE tracker_hdd_item SET "
            l_strSQL = l_strSQL & "FileLocation = '" & l_strFileLocation & "', "
            l_strSQL = l_strSQL & "ComponentsAvailable = '" & l_strComponentsAvailable & "', "
            l_strSQL = l_strSQL & "FileReference = '" & l_strFilename & "', "
            l_strSQL = l_strSQL & "FileSizeGB = '" & l_dblFileSizeGB & "', "
            l_strSQL = l_strSQL & "Status = '" & l_strStatus & "' WHERE tracKer_hdd_ItemID = " & l_lngLineID & ";"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
        End If
    Else
        
        'New Line; Insert the data
        l_strSQL = "INSERT INTO tracker_hdd_item (CompanyID, Client, Territory, RequestID, LineItemID, DueDate, DoNotDeliverBeforeDate, RequestName, ScheduledBy, ComponentsAvailable, "
        l_strSQL = l_strSQL & "SeriesTitle, EpisodeTitle, EpisodeNumber, CVDisplayName, FileLocation, fileReference, FileSizeGB, FormatRequiredType, ClientContact, Destination, Status, MediaRequiredType, DepartmentChargeCode) VALUES ("
        'l_strSQL = l_strSQL & "ShippingMethodTypeID, TrackingNumber, MediaRequiredType, MediaType, MediaBarcodeNo, DepartmentChargeCode)VALUES("
        l_strSQL = l_strSQL & "1418, "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strClient) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTerritory) & "', "
        l_strSQL = l_strSQL & "'" & l_lngRequestID & "', "
        l_strSQL = l_strSQL & "'" & l_lngLineItemID & "', "
        If l_datDueDate <> 0 Then
            l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDueDate) & "', "
        Else
            l_strSQL = l_strSQL & "Null, "
        End If
        If l_datDoNotDeliverBeforeDate <> 0 Then
            l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDoNotDeliverBeforeDate) & "', "
        Else
            l_strSQL = l_strSQL & "Null, "
        End If
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strRequestName) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strScheduledBy) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strComponentsAvailable) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSeriesTitle) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strEpisodeTitle) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strEspisodeNumber) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strCVDisplayName) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strFileLocation) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strFilename) & "', "
        l_strSQL = l_strSQL & "'" & l_dblFileSizeGB & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strFormatRequiredType) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strClientContact) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strDestination) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strStatus) & "', "
    '    l_strSQL = l_strSQL & "'" & l_lngShippingMethodTypeID & "', "
    '    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTrackingNumber) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strMediaRequiredType) & "', "
    '    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strMediaType) & "', "
    '    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strMediaBarcodeNo) & "', "
    '    If l_datShipDate <> 0 Then
    '        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datShipDate) & "', "
    '    Else
    '        l_strSQL = l_strSQL & "Null, "
    '    End If
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strDepartmentChargeCode) & "'"
    
      
        
        l_strSQL = l_strSQL & ");"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Loop

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep


End Sub

Private Sub cmdiConcert_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_strSQL As String, l_lngTrackerID As Long, l_lngRunningTime As Long, l_rst As ADODB.Recordset, l_curFileSize As Currency
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long, l_strID As String, l_strTimecode As String, l_strDuration As String, l_strQC As String, l_strComment As String, l_strCustomer As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets("Sheet1")

MsgBox "This reader does not check for Duplicates, so don't read the same sheet in twice.", vbInformation

l_lngXLRowCounter = 2

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    
'    l_strID = Left(GetXLData(l_lngXLRowCounter, 1), Len(GetXLData(l_lngXLRowCounter, 1)) - 4)
    l_strID = GetXLData(l_lngXLRowCounter, 1)
    l_strTimecode = GetXLData(l_lngXLRowCounter, 2)
    l_strDuration = GetXLData(l_lngXLRowCounter, 3)
    l_strQC = GetXLData(l_lngXLRowCounter, 4)
    l_strComment = GetXLData(l_lngXLRowCounter, 5)
    l_strCustomer = GetXLData(l_lngXLRowCounter, 6)
    
    lblProgress.Caption = "ID: " & l_strID
    l_strSQL = "INSERT INTO tracker_item (companyID, itemreference, headertext1, headertext2, headertext3, headertext4, headertext5, headertext6) VALUES ("
    l_strSQL = l_strSQL & GetData("company", "companyID", "name", "MX1 (iConcerts)") & ", "
    l_strSQL = l_strSQL & "'" & l_strID & "', "
    l_strSQL = l_strSQL & "'" & l_strID & "', "
    l_strSQL = l_strSQL & "'" & l_strTimecode & "', "
    l_strSQL = l_strSQL & "'" & l_strDuration & "', "
    l_strSQL = l_strSQL & "'" & l_strQC & "', "
    l_strSQL = l_strSQL & "'" & l_strComment & "', "
    l_strSQL = l_strSQL & "'" & l_strCustomer & "'); "
    
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_lngTrackerID = g_lngLastID
    
    If l_strID <> "" Then
        'see if there is a clip record to get the duration from reference
        l_strDuration = GetDataSQL("SELECT TOP 1 fd_length FROM events WHERE clipreference = '" & l_strID & "' AND companyID = " & GetData("company", "companyID", "name", "MX1 (iConcerts)") & " AND system_deleted = 0 AND fd_length <> '' and fd_length IS NOT NULL;")
        SetData "tracker_item", "timecodeduration", "tracker_itemid", l_lngTrackerID, l_strDuration
        If l_strDuration <> "" Then
            l_lngRunningTime = 60 * Val(Mid(l_strDuration, 1, 2))
            l_lngRunningTime = l_lngRunningTime + Val(Mid(l_strDuration, 4, 2))
            If Val(Mid(l_strDuration, 7, 2)) > 30 Then
                l_lngRunningTime = l_lngRunningTime + 1
            End If
            If l_lngRunningTime = 0 Then l_lngRunningTime = 1
            SetData "tracker_item", "duration", "tracker_itemid", l_lngTrackerID, l_lngRunningTime
            SetData "tracker_item", "stagefield1", "tracker_itemid", l_lngTrackerID, Format(Now, "YYYY-MM-DD HH:NN:SS")
        End If
        
        'See if there is a size to go in the GB sent column
        l_curFileSize = 0
        Set l_rst = ExecuteSQL("SELECT bigfilesize, clipfilename FROM events WHERE clipreference = '" & l_strID & "' AND companyID = " & GetData("company", "companyID", "name", "MX1 (iConcerts)") & " AND system_deleted = 0;", g_strExecuteError)
        CheckForSQLError
        If l_rst.RecordCount > 0 Then
            l_rst.MoveFirst
            Do While Not l_rst.EOF
                If Not IsNull(l_rst("bigfilesize")) Then
                    If l_rst("bigfilesize") > l_curFileSize Then
                        l_curFileSize = l_rst("bigfilesize")
                        SetData "tracker_item", "itemfilename", "tracker_itemID", l_lngTrackerID, l_rst("clipfilename")
                    End If
                End If
                l_rst.MoveNext
            Loop
        End If
        l_rst.Close
        If l_curFileSize <> 0 Then
            SetData "tracker_item", "gbsent", "tracker_itemID", l_lngTrackerID, Int(l_curFileSize / 1024 / 1024 / 1024 + 0.999)
        End If
        Set l_rst = Nothing
        
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Loop

lblProgress.Caption = ""
Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

Beep

End Sub

Private Sub cmdInclBBCWW_Click()

If Not CheckAccess("/processexcelorders") Then
    Exit Sub
End If

'MsgBox "This routine not yet implemented.", vbInformation, "No activity Possible"

'have a variable to hold the filename we are going to process
Dim l_strExcelFileName As String, l_strExcelFileTitle As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Dim l_rsJobs As ADODB.Recordset, l_rsDetails As ADODB.Recordset, l_rsLib As ADODB.Recordset, l_rsHistory As ADODB.Recordset, l_rsxJobs As ADODB.Recordset
Dim l_rsxDetails As ADODB.Recordset, l_lngLibraryID As Long, l_rst As ADODB.Recordset

Dim l_strSQL As String, l_lngdetailID As Long
            
'open the recordsets
Set l_rsJobs = New ADODB.Recordset
Set l_rsDetails = New ADODB.Recordset
Set l_rsxJobs = New ADODB.Recordset
Set l_rsxDetails = New ADODB.Recordset
Set l_rsLib = New ADODB.Recordset
Set l_rsHistory = New ADODB.Recordset

Set l_rsJobs = ExecuteSQL("SELECT * FROM job WHERE jobID = -1", g_strExecuteError)
CheckForSQLError
Set l_rsDetails = ExecuteSQL("SELECT * FROM jobdetail WHERE jobID = -1", g_strExecuteError)
CheckForSQLError
Set l_rsxJobs = ExecuteSQL("SELECT * FROM extendedjob WHERE jobID = -1", g_strExecuteError)
CheckForSQLError
Set l_rsxDetails = ExecuteSQL("SELECT * FROM extendedjobdetail WHERE jobID = -1", g_strExecuteError)
CheckForSQLError

'free up locks
DoEvents

'open the spreadsheet from the root of C:\

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets("sheet1")

'get the fields and place them into variables
Dim l_BookedBy$, l_AccountNumber$, l_Telephone$, l_FACNumber$, l_jcacontact$, l_ClientName$, l_ContactName$, l_BookingNumber$, l_OrderDate$, l_datOrderDate As Date, l_datRequiredDate As Date, l_lngTargetDateCountdown As Integer
Dim l_Lastprogrammetitle$

'get the header details, i.e. Client name, etc.
l_AccountNumber$ = GetXLData(g_cntAccountNumberRow%, g_cntAccountNumberColumn%)
l_Telephone$ = GetXLData(g_cntTelephoneRow%, g_cntTelephoneColumn%)
l_ClientName$ = "BBC Worldwide"
l_ContactName$ = GetXLData(5, 6)
l_OrderDate$ = GetXLData(5, 2)
If IsDate(l_OrderDate$) Then l_datOrderDate = l_OrderDate$
l_FACNumber$ = GetXLData(3, 2)
l_FACNumber$ = Mid(l_FACNumber$, 16, Len(l_FACNumber$))
l_jcacontact$ = GetXLData(7, 2)


'declare a variable for keeping track of how far down the page we are
Dim l_CurrentTop&

'set it to the first load of data
l_CurrentTop& = 10

'declare lots of variables for taking the values off the spreadsheet
Dim l_OrderNumber$, l_CustomerName$, l_ChargeCode$, l_BusinessAreaCode$, l_NominalCode$, l_ProgrammeTitle$, l_Requireddate$, l_Notes$
Dim l_MasterEpisodeNumber$, l_MasterSpoolNumber$, l_MasterLanguage$, l_MasterSpoolDuration$, l_MasterAspectRatio$, l_MasterTapeFormat$, l_MasterLineStandard$
Dim l_DupeEpisodeNumber$, l_DupeAudio$, l_DupeCeefax$, l_DupeTotalDuration$, l_DupeLanguage$, l_DupeQuantity$, l_DupeTapeFormat$, l_DupeLineStandard$, l_DupeAspectRatio$
Dim l_LastorderNumber$, l_PreviousJobNumber&, l_Original_MasterStandard$, l_Original_DupeStandard$, l_Saveflag%, l_Stk&, l_CalculatedDuration&, l_Mastercount&
Dim l_Orig_masterspool$, l_Orig_masterformat$, l_Orig_masterstandard$, l_Orig_masteraspect$, l_Orig_masterduration$, l_comma&, l_IndividualFAC$, l_ProgrammeNumber$
Dim l_CountryCode$, l_BusinessArea$, l_OldMasterSpoolNumber$, l_NewJobNumber&, l_BBCMasterTapeFormat$, l_BBCDupeTapeFormat$, l_EpisodeNumber$
Dim l_EpisodeCount As Long, l_CopyCount As Long, l_OrigCopyCount As Long, l_intExtraCopiesFromSameReplay As Integer
Dim l_DespatchMethod$, l_CourierAccountNumber$, l_DespatchCostCentre$, l_DespatchBusinessAreaCode$, l_DespatchNominalCode$, l_MasterContactName$, l_DespatchTelephone$, l_MasterEmail$, l_DespatchAddress$, Notes3$
Dim l_DupeMasterSpoolNumber$, l_DupeMastercount&
Dim l_DupeMasterSpoolDuration$, l_DupeMasterTapeFormat$, l_DupeBBCMasterTapeFormat$, l_DupeMasterLineStandard$, l_DupeMasterAspectRatio$
Dim l_Dupe_Orig_Masterspool$, l_Dupe_Orig_Masterformat$, l_Dupe_Orig_Masterstandard$, l_Dupe_Orig_Masterduration$, l_Dupe_Orig_Masteraspect$
Dim l_Counter&
Dim l_datNewTargetDate As Date, l_datNewTargetDate2 As Date

l_LastorderNumber$ = "CETA"
l_Saveflag% = False


'declare the Sound channels
Dim l_Sound1$, l_Sound2$, l_Sound3$, l_Sound4$, l_Sound5$, l_Sound6$, l_Sound7$, l_Sound8$

'start a loop that cycles through each tenth line
l_EpisodeCount = 1
l_CopyCount = 1
l_intExtraCopiesFromSameReplay = 0
Do
    
    'if there is no data in the Bookingnumber field then this is either one of those blue lines or the end of the order.
    'Check which and then exit the do loop if necessary. While there, count the number of episodes in the current group,
    'and enter the data for the last one, rather than any others.
    
    If GetXLData(l_CurrentTop&, 2) = "" Then
        l_CurrentTop& = l_CurrentTop& + 2
        l_MasterSpoolNumber$ = ""
        l_OldMasterSpoolNumber$ = "GINGER"
        If GetXLData(l_CurrentTop&, 2) = "" Then Exit Do
    End If
    
    l_lngLibraryID = 0
    l_EpisodeCount = 1
    l_CopyCount = 1
    l_Mastercount& = 1
    l_BookingNumber$ = GetXLData(l_CurrentTop&, 2)
    
    If l_BookingNumber$ = "" Then Exit Do
    
    l_MasterSpoolNumber$ = GetXLData(l_CurrentTop& + 2, 4)
    
    'here is where we work out if there is two masters on this sheet, so check for a comma
    If InStr(l_MasterSpoolNumber$, ",") <> 0 Then
    
        'There's a comma in the master spoolnumbers line. This means we're dealing with multiple masters, and need to pick them apart.
        
        l_Mastercount& = Countcommas(l_MasterSpoolNumber$) + 1
        
    End If
        
    l_Orig_masterspool$ = l_MasterSpoolNumber$
    l_Orig_masterformat$ = GetXLData(l_CurrentTop& + 4, 4)
    l_Orig_masterstandard$ = GetXLData(l_CurrentTop& + 5, 4)
    l_Orig_masterduration$ = GetXLData(l_CurrentTop& + 3, 4)
    l_Orig_masteraspect$ = GetXLData(l_CurrentTop& + 6, 4)

    'go through the spreadsheet and retrieve the values and put them into
    'variables to be used later. These are from the Order Details section
    l_OrderNumber$ = GetXLData(l_CurrentTop& + 1, 2)
    l_IndividualFAC$ = GetXLData(l_CurrentTop& + 0, 2)
    l_CustomerName$ = GetXLData(l_CurrentTop& + 2, 2)
    l_ChargeCode$ = GetXLData(l_CurrentTop& + 3, 2)
    l_BusinessArea$ = GetXLData(l_CurrentTop& + 4, 2)
    l_BusinessAreaCode$ = GetXLData(l_CurrentTop& + 5, 2)
    l_NominalCode$ = GetXLData(l_CurrentTop& + 6, 2)
    l_ProgrammeTitle$ = GetXLData(l_CurrentTop& + 7, 2)
    l_ProgrammeTitle$ = StrConv(l_ProgrammeTitle$, vbProperCase)
    l_ProgrammeNumber$ = GetXLData(l_CurrentTop& + 8, 2)
    l_CountryCode$ = GetXLData(l_CurrentTop& + 8, 4)
    l_EpisodeNumber$ = GetXLData(l_CurrentTop& + 1, 4)
    l_Requireddate$ = GetXLData(l_CurrentTop& + 9, 2)
    l_DespatchMethod$ = Trim(GetXLData(l_CurrentTop& + 0, 8))
    l_CourierAccountNumber$ = Trim(GetXLData(l_CurrentTop& + 1, 8))
    l_DespatchCostCentre$ = Trim(GetXLData(l_CurrentTop& + 2, 8))
    l_DespatchBusinessAreaCode$ = Trim(GetXLData(l_CurrentTop& + 3, 8))
    l_DespatchNominalCode$ = Trim(GetXLData(l_CurrentTop& + 4, 8))
    l_MasterContactName$ = Trim(GetXLData(l_CurrentTop& + 5, 8))
    l_DespatchTelephone$ = Trim(GetXLData(l_CurrentTop& + 6, 8))
    l_MasterEmail$ = Trim(GetXLData(l_CurrentTop& + 7, 8))
    l_DespatchAddress$ = Trim(GetXLData(l_CurrentTop& + 8, 8))
    If IsDate(l_Requireddate$) Then l_datRequiredDate = l_Requireddate$
    l_lngTargetDateCountdown = 3
    
    l_Notes$ = GetXLData(l_CurrentTop& + 10, 2)
    
    'get the next chapter, which is Master Material
    l_MasterEpisodeNumber$ = GetXLData(l_CurrentTop& + 1, 4)
    
    If l_Mastercount& = 1 Then
        l_MasterSpoolDuration$ = GetXLData(l_CurrentTop& + 3, 4)
        l_MasterTapeFormat$ = GetAlias(GetXLData(l_CurrentTop& + 4, 4))
        l_BBCMasterTapeFormat$ = GetXLData(l_CurrentTop& + 4, 4)
        l_MasterLineStandard$ = GetAlias(GetXLData(l_CurrentTop& + 5, 4))
        l_MasterAspectRatio$ = GetAlias(GetXLData(l_CurrentTop& + 6, 4))
    End If
    
    l_MasterLanguage$ = GetAlias(GetXLData(l_CurrentTop& + 7, 4))
    
    'increment the looper so we move to the next section  or empty line.
    l_CurrentTop& = l_CurrentTop& + 11
        
    If UCase(l_OrderNumber$) <> UCase(l_LastorderNumber$) Then
                    
        If l_LastorderNumber$ <> "STAR ORDER" Then
        
            'add a new record to the Jobs table
            If l_LastorderNumber$ <> "CETA" Then
                l_strSQL = "DELETE FROM jobdetail WHERE copytype = 'M' and jobID = " & l_NewJobNumber& & ";"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            End If
            l_rsJobs.AddNew
            l_rsJobs("createduserID") = 33
            l_rsJobs("createduser") = "Excel"
            l_rsJobs("createddate") = Now
            l_rsJobs("modifieddate") = Now
            l_rsJobs("modifieduserID") = 33
            l_rsJobs("modifieduser") = l_jcacontact$
            l_rsJobs("flagemail") = 1
            l_rsJobs("fd_status") = "Confirmed"
            l_rsJobs("jobtype") = "Dubbing"
            l_rsJobs("joballocation") = "Regular"
            l_rsJobs("deadlinedate") = Format(l_Requireddate$, "dd/mm/yyyy")
            l_rsJobs("despatchdate") = Format(l_Requireddate$, "dd/mm/yyyy")
            l_rsJobs("projectID") = 0
            l_rsJobs("productID") = 0
            If l_Telephone$ <> "" Then l_rsJobs("telephone") = Left$(l_Telephone$, 50)
'            Dim l_NewJobNumber&
'            l_NewJobNumber& = GetNextJobNumber()
'            l_PreviousJobNumber& = l_NewJobNumber&
'
'            l_rsJobs("job number") = l_NewJobNumber&
            l_rsJobs("companyID") = 570
            l_rsJobs("companyname") = "BBC Worldwide Int Ops"
            
            'parse the contactname to get the CETA contact ID and name.
            If l_ContactName$ <> "" Then
                l_rsJobs("contactname") = l_ContactName$
                l_rsJobs("contactID") = GetContactID(l_ContactName$)
            End If
            
            If l_ProgrammeTitle$ <> "" Then l_rsJobs("title1") = Left$(l_ProgrammeTitle$, 50)
            l_Lastprogrammetitle$ = UCase(l_ProgrammeTitle$)
            If l_MasterEpisodeNumber$ <> "" Then l_rsJobs("title2") = Left$(l_MasterEpisodeNumber$, 50)
            If l_Notes$ <> "" Then l_rsJobs("notes2") = "PLEASE CHECK ORIGINAL WW ORDER SHEETS FOR JOB DETAILS. " & l_Notes$
            Notes3$ = ""
            If l_DespatchMethod$ <> "" Then Notes3$ = Notes3$ & "Despatch Method: " & l_DespatchMethod$ & vbCrLf
            If l_CourierAccountNumber$ <> "" Then Notes3$ = Notes3$ & "Courier Account Number: " & l_CourierAccountNumber$ & vbCrLf
            If l_MasterContactName$ <> "" Then Notes3$ = Notes3$ & "FAO: " & l_MasterContactName$ & vbCrLf
            If l_DespatchTelephone$ <> "" Then Notes3$ = Notes3$ & "Tel: " & l_DespatchTelephone$ & vbCrLf
            If l_MasterEmail$ <> "" Then Notes3$ = Notes3$ & "Email: " & l_MasterEmail$ & vbCrLf
            If l_DespatchAddress$ <> "" Then Notes3$ = Notes3$ & "Address:" & vbCrLf & l_DespatchAddress$ & vbCrLf
            If Notes3$ <> "" Then l_rsJobs("notes3") = Notes3$
            
            'check whether this is a STAR order or a normal one.
            
'            If l_OrderNumber$ Like "STAR*" Then
'                l_rsJobs("orderreference") = "STAR ORDER"
'                l_LastorderNumber$ = "STAR ORDER"
'                l_rsJobs("title1") = "Various Titles - Star Order"
'            Else
                If l_OrderNumber$ <> "" Then l_rsJobs("orderreference") = UCase(l_OrderNumber$)
                l_LastorderNumber$ = UCase(l_OrderNumber$)
'            End If
                
            l_rsJobs.Update
            
            
            'this part here!
            l_rsJobs.Bookmark = l_rsJobs.Bookmark
            l_NewJobNumber& = l_rsJobs("JobID")
            l_PreviousJobNumber& = l_NewJobNumber&
            
            'Set the projectnumber to be the job ID - new CETA feature.
            l_rsJobs("projectnumber") = l_rsJobs("JobID")
            l_rsJobs.Update
                        
            DBEngine.Idle
            DoEvents
            
            If l_NewJobNumber& <> 0 Then
                
                l_rsxJobs.AddNew
                l_rsxJobs("JobID") = l_NewJobNumber&
                l_rsxJobs("BBCStarJob") = 1
                l_rsxJobs("BBCBookingnumber") = l_FACNumber$
                If l_CustomerName$ <> "" Then l_rsxJobs("bbccustomername") = l_CustomerName$
                If l_ChargeCode$ <> "" Then l_rsxJobs("bbcchargecode") = l_ChargeCode$
                If l_BusinessArea$ <> "" Then l_rsxJobs("bbcbusinessarea") = l_BusinessArea$
                If l_BusinessAreaCode$ <> "" Then l_rsxJobs("bbcbusinessareacode") = l_BusinessAreaCode$
                If l_NominalCode$ <> "" Then l_rsxJobs("bbcnominalcode") = l_NominalCode$
                If l_CountryCode$ <> "" Then l_rsxJobs("BBCCountryCode") = l_CountryCode$
                l_rsxJobs.Update
                
            End If

            DBEngine.Idle
            DoEvents
            
        End If
        
    Else
        'make sure the title program stays the same all the way
        If UCase(l_ProgrammeTitle$) <> UCase(l_Lastprogrammetitle$) Then
            'need to update the other job to change the title
            l_rsJobs.Close
            Set l_rsJobs = ExecuteSQL("Select [title1] from [Job] where [jobID] = " & l_PreviousJobNumber&, g_strExecuteError)
            CheckForSQLError
                        
            If l_rsJobs.RecordCount <> 0 Then
                l_rsJobs("title1") = "Various Titles - See Below"
                l_rsJobs.Update
                l_rsJobs.Close
                DBEngine.Idle
                DoEvents
                Set l_rsJobs = ExecuteSQL("SELECT * FROM [Job] WHERE [JobID] = -1", g_strExecuteError)
                CheckForSQLError
            End If
        End If
    End If

    DBEngine.Idle: DoEvents
    
    'The master detail line(s) get added next
    Do While l_Mastercount& > 0
        
        'Get next master into normal variables if there is more than 1, trim the original variables and decrease the mastercount.
        
        If l_Mastercount > 1 Then
            l_comma& = InStr(l_Orig_masterspool$, ",")
            l_MasterSpoolNumber$ = Left(l_Orig_masterspool$, l_comma& - 1)
            If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", l_MasterSpoolNumber$) <> 0 Then
                l_MasterSpoolNumber$ = GetData("bbc_barcode_correction", "corrected_barcode", "original_barcode", l_MasterSpoolNumber$)
            End If
            l_Orig_masterspool$ = Mid(l_Orig_masterspool$, l_comma& + 1, Len(l_Orig_masterspool$))
            l_comma& = InStr(l_Orig_masterformat$, ",")
            l_BBCMasterTapeFormat$ = Left(l_Orig_masterformat$, l_comma& - 1)
            l_MasterTapeFormat$ = GetAlias(l_BBCMasterTapeFormat$)
            l_Orig_masterformat$ = Mid(l_Orig_masterformat$, l_comma + 1, Len(l_Orig_masterformat$))
            l_comma& = InStr(l_Orig_masterstandard$, ",")
            l_MasterLineStandard$ = GetAlias(Left(l_Orig_masterstandard$, l_comma - 1))
            l_Orig_masterstandard$ = Mid(l_Orig_masterstandard$, l_comma + 1, Len(l_Orig_masterstandard$))
            l_comma& = InStr(l_Orig_masteraspect$, ",")
            If l_comma > 0 Then
                l_MasterAspectRatio$ = GetAlias(Left(l_Orig_masteraspect$, l_comma - 1))
                l_Orig_masteraspect$ = Mid(l_Orig_masteraspect$, l_comma + 1, Len(l_Orig_masteraspect$))
            Else
                l_MasterAspectRatio$ = GetAlias(l_Orig_masteraspect$)
            End If
            l_comma& = InStr(l_Orig_masterduration$, ",")
            l_MasterSpoolDuration$ = Left(l_Orig_masterduration$, l_comma - 1)
            l_Orig_masterduration$ = Mid(l_Orig_masterduration$, l_comma + 1, Len(l_Orig_masterduration$))
        End If
        l_Mastercount& = l_Mastercount& - 1
        
        'process that master
        l_rsDetails.AddNew
        l_rsDetails("jobID") = l_NewJobNumber&
        If l_MasterSpoolNumber$ <> "" Then
            If l_EpisodeNumber$ <> "" Then
                l_rsDetails("description") = Left(l_ProgrammeTitle$, 50) & " Ep." & l_EpisodeNumber$
            Else
                l_rsDetails("description") = Left(l_ProgrammeTitle$, 50)
            End If
        End If
        'check if we know about this tape in the library and get details if we do.
        'otherwise use the details they provide on the excel sheet.
        l_strSQL = "SELECT * FROM Library WHERE Barcode = '" & Trim(l_MasterSpoolNumber$) & "'"
        Set l_rsLib = ExecuteSQL(l_strSQL, g_strExecuteError)
        If Not l_rsLib.EOF Then
            l_rsLib("title") = l_ProgrammeTitle$
            l_rsLib("episode") = l_EpisodeNumber$
            l_rsLib.Update
            If l_rsLib("format") <> "" Then
                l_rsDetails("format") = Left(l_rsLib("format"), 50)
                If l_rsDetails("format") Like "HI8DAT*" Then l_rsDetails("format") = "HI8DAT"
                If l_rsDetails("format") Like "DAT*" Then l_rsDetails("format") = "DAT"
            ElseIf l_MasterTapeFormat$ <> "" Then
                l_rsDetails("format") = l_MasterTapeFormat$
            End If
            l_rsDetails("copytype") = "M"
            l_rsDetails("quantity") = l_EpisodeCount
            l_rsDetails("videostandard") = Left(l_rsLib("videostandard"), 10)
            If l_rsLib("videostandard") <> "" Then l_MasterLineStandard$ = Left(l_rsLib("videostandard"), 10)
            If l_rsLib("AspectRatio") <> "" Then
                l_rsDetails("aspectratio") = Left((l_rsLib("AspectRatio") & l_rsLib("GeometricLinearity")), 50)
                l_MasterAspectRatio$ = l_rsDetails("aspectratio")
            Else
                If l_MasterAspectRatio$ <> "" Then
                    l_rsDetails("aspectratio") = l_MasterAspectRatio$
                Else
                    If l_Orig_masterstandard$ Like "*16:9 WI*" Then
                        l_rsDetails("aspectratio") = "16:9AN"
                        l_MasterAspectRatio$ = "16:9AN"
                    End If
                    If l_Orig_masterstandard$ Like "*16:9 LE*" Then
                        l_rsDetails("aspectratio") = "16:9LE"
                        l_MasterAspectRatio$ = "16:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*15:9*" Then
                        l_rsDetails("aspectratio") = "15:9LE"
                        l_MasterAspectRatio$ = "15:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*14:9*" Then
                        l_rsDetails("aspectratio") = "14:9LE"
                        l_MasterAspectRatio$ = "14:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*4:3*" Then
                        l_rsDetails("aspectratio") = "4:3NOR"
                        l_MasterAspectRatio$ = "4:3NOR"
                    End If
                End If
            End If
            If l_rsLib("Totalduration") <> "" Then
                l_CalculatedDuration& = 60 * Val(Left(l_rsLib("Totalduration"), 2)) + Val(Mid(l_rsLib("totalduration"), 4, 2))
                If Val(Mid(l_rsLib("totalduration"), 7, 2)) > 0 Then l_CalculatedDuration& = l_CalculatedDuration& + 1
            End If
            If l_CalculatedDuration& > 0 Then
                l_rsDetails("runningtime") = l_CalculatedDuration&
                l_MasterSpoolDuration$ = Str(l_CalculatedDuration&)
            Else
                l_rsDetails("runningtime") = Int(Val(l_MasterSpoolDuration$))
            End If
            If l_rsLib("ch1") <> "" Then
                l_rsDetails("sound1") = Process_Sound(l_rsLib("ch1"))
            Else
                l_rsDetails("sound1") = Left(l_rsLib("ch1"), 50)
            End If
            If l_rsLib("ch2") <> "" Then
                l_rsDetails("sound2") = Process_Sound(l_rsLib("ch2"))
            Else
                l_rsDetails("sound2") = Left(l_rsLib("ch2"), 50)
            End If
            If l_rsLib("ch3") <> "" Then
                l_rsDetails("sound3") = Process_Sound(l_rsLib("ch3"))
            Else
                l_rsDetails("sound3") = Left(l_rsLib("ch3"), 50)
            End If
            If l_rsLib("ch4") <> "" Then
                l_rsDetails("sound4") = Process_Sound(l_rsLib("ch4"))
            Else
                l_rsDetails("sound4") = Left(l_rsLib("ch4"), 50)
            End If
            
            'Make the tape required entry
            l_lngLibraryID = l_rsLib("libraryID")
            l_rsHistory.Open "SELECT * FROM requiredmedia WHERE jobID = " & l_NewJobNumber& & " AND libraryID = " & l_lngLibraryID & ";", g_strConnection, adOpenKeyset, adLockOptimistic
            If l_rsHistory.RecordCount <= 0 Then
                l_strSQL = "INSERT INTO requiredmedia (jobID, libraryID, barcode) VALUES (" & l_NewJobNumber& & ", " & l_lngLibraryID & ", '" & l_MasterSpoolNumber$ & "');"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            End If
            l_rsHistory.Close
        Else
            'As well as loading up the job detail line from the provided data, we create a new tape entry in the library.
            l_rsLib.AddNew
            l_rsLib("barcode") = Trim(l_MasterSpoolNumber$)
            l_rsLib("title") = l_ProgrammeTitle$
            l_rsLib("Episode") = l_EpisodeNumber$
            l_rsLib("companyID") = 287
            l_rsLib("companyname") = "BBC Worldwide Ltd"
            l_rsLib("location") = "OFF SITE"
            If l_MasterTapeFormat$ <> "" Then
                l_rsLib("format") = l_MasterTapeFormat$
                l_rsDetails("format") = l_MasterTapeFormat$
            End If
            l_rsLib("copytype") = "MASTER"
            l_rsLib("version") = "MASTER"
            l_rsDetails("copytype") = "M"
            l_rsDetails("quantity") = l_EpisodeCount
            If l_MasterLineStandard$ = "ASK" Then
                l_MasterLineStandard$ = "625PAL"
                l_rsDetails("videoStandard") = "625PAL"
                l_rsLib("videostandard") = "625PAL"
            Else
                If l_MasterLineStandard$ <> "" Then
                    l_rsDetails("videostandard") = Left(l_MasterLineStandard$, 10)
                    l_rsLib("videostandard") = Left(l_MasterLineStandard$, 10)
                End If
            End If
            If l_MasterAspectRatio$ <> "" Then
                l_rsDetails("aspectratio") = l_MasterAspectRatio$
            Else
                If l_Orig_masterstandard$ Like "*16:9 WI*" Then
                    l_rsDetails("aspectratio") = "16:9AN"
                    l_MasterAspectRatio$ = "16:9AN"
                    l_rsLib("aspectratio") = "16:9"
                    l_rsLib("geometriclinearity") = "ANAMORPHIC"
                End If
                If l_Orig_masterstandard$ Like "*16:9 LE*" Then
                    l_rsDetails("aspectratio") = "16:9LE"
                    l_MasterAspectRatio$ = "16:9LE"
                    l_rsLib("aspectratio") = "16:9"
                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*15:9*" Then
                    l_rsDetails("aspectratio") = "15:9LE"
                    l_MasterAspectRatio$ = "15:9LE"
                    l_rsLib("aspectratio") = "15:9"
                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*14:9*" Then
                    l_rsDetails("aspectratio") = "14:9LE"
                    l_MasterAspectRatio$ = "14:9LE"
                    l_rsLib("aspectratio") = "14:9"
                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*4:3*" Then
                    l_rsDetails("aspectratio") = "4:3NOR"
                    l_MasterAspectRatio$ = "4:3NOR"
                    l_rsLib("aspectratio") = "4:3"
                    l_rsLib("geometriclinearity") = "NORMAL"
                End If
            End If
            If l_MasterAspectRatio$ = "ASK" Then
                l_rsDetails("aspectratio") = "4:3NOR"
                l_MasterAspectRatio$ = "4:3NOR"
                l_rsLib("aspectratio") = "4:3"
                l_rsLib("geometriclinearity") = "NORMAL"
            End If
            l_rsDetails("runningtime") = Int(Val(l_MasterSpoolDuration$))
            l_rsLib("cuser") = "XLS"
            l_rsLib("cdate") = Now
            l_rsLib("muser") = "XLS"
            l_rsLib("mdate") = Now
            l_rsLib("code128barcode") = CIA_CODE128(Trim(l_MasterSpoolNumber$))
            l_rsLib.Update
            
            'Then we make a history entry too.
            l_rsLib.Bookmark = l_rsLib.Bookmark
            l_lngLibraryID = l_rsLib("libraryID")
            l_rsHistory.Open "SELECT * FROM [trans] WHERE [libraryID] = -1", g_strConnection, adOpenKeyset, adLockOptimistic
            l_rsHistory.AddNew
            l_rsHistory("libraryID") = l_lngLibraryID
            l_rsHistory("destination") = "Created"
            l_rsHistory("cuser") = "XLS"
            l_rsHistory("cdate") = Now
            l_rsHistory.Update
            l_rsHistory.Close
            
            'Make the tape required entry
            l_rsHistory.Open "SELECT * FROM requiredmedia WHERE jobID = " & l_NewJobNumber& & " AND libraryID = " & l_lngLibraryID & ";", g_strConnection, adOpenKeyset, adLockOptimistic
            If l_rsHistory.RecordCount <= 0 Then
                l_strSQL = "INSERT INTO requiredmedia (jobID, libraryID, barcode) VALUES (" & l_NewJobNumber& & ", " & l_lngLibraryID & ", '" & l_MasterSpoolNumber$ & "');"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            End If
            l_rsHistory.Close
        End If
        l_rsLib.Close
'        l_BBCMasterTapeFormat$ = l_MasterTapeFormat$
        If l_rsDetails("format") = "" Then l_rsDetails("format") = "ASK"
        
        l_rsDetails.Update
        
        l_rsDetails.Bookmark = l_rsDetails.Bookmark
        l_lngdetailID = l_rsDetails("jobdetailID")
        l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
        l_rsDetails.Update

        
        l_rsxDetails.AddNew
        l_rsxDetails("jobID") = l_NewJobNumber&
        l_rsxDetails("jobdetailID") = l_lngdetailID
        If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
        If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
        If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
        If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
        If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
        If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
        If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
        If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
        If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
        If l_BBCMasterTapeFormat$ <> "" Then l_rsxDetails("bbcsourceformat") = l_BBCMasterTapeFormat$
        If l_DupeTapeFormat$ <> "" Then l_rsxDetails("bbcRecordFormat") = l_DupeTapeFormat$
        l_rsxDetails.Update
                
        'update the variables if the mastercount is currentrly 1
        If l_Mastercount& = 1 Then
            l_BBCMasterTapeFormat$ = l_Orig_masterformat$
            l_MasterTapeFormat$ = GetAlias(l_BBCMasterTapeFormat$)
            l_MasterSpoolNumber$ = l_Orig_masterspool$
            l_MasterLineStandard$ = GetAlias(l_Orig_masterstandard$)
            l_MasterAspectRatio$ = GetAlias(l_Orig_masteraspect$)
            l_MasterSpoolDuration$ = l_Orig_masterduration$
        End If
    Loop
    
    'Then the copy detail line gets added
    l_OrigCopyCount = l_CopyCount
    l_DupeMastercount& = 1
    Do While l_CopyCount > 0
        
        l_CurrentTop& = l_CurrentTop& - 11
        l_CopyCount = l_CopyCount - 1
        
        'Get the Duplication details for this copy
        l_DupeMasterSpoolNumber$ = GetXLData(l_CurrentTop& + 2, 4)
        
        'here is where we work out if there is two masters on this sheet, so check for a comma
        If InStr(l_DupeMasterSpoolNumber$, ",") <> 0 Then
            'There's a comma in the master spoolnumbers line. This means we're dealing with multiple masters, and need to pick them apart.
            l_DupeMastercount& = Countcommas(l_DupeMasterSpoolNumber$) + 1
        End If
            
        If l_DupeMastercount& = 1 Then
            l_DupeMasterSpoolDuration$ = GetXLData(l_CurrentTop& + 3, 4)
            l_DupeMasterTapeFormat$ = GetAlias(GetXLData(l_CurrentTop& + 4, 4))
            l_DupeBBCMasterTapeFormat$ = GetXLData(l_CurrentTop& + 4, 4)
            l_DupeMasterLineStandard$ = GetAlias(GetXLData(l_CurrentTop& + 5, 4))
            l_DupeMasterAspectRatio$ = GetAlias(GetXLData(l_CurrentTop& + 6, 4))
        End If
        
        l_Dupe_Orig_Masterspool$ = l_DupeMasterSpoolNumber$
        l_Dupe_Orig_Masterformat$ = GetXLData(l_CurrentTop& + 4, 4)
        l_Dupe_Orig_Masterstandard$ = GetXLData(l_CurrentTop& + 5, 4)
        l_Dupe_Orig_Masterduration$ = GetXLData(l_CurrentTop& + 3, 4)
        l_Dupe_Orig_Masteraspect$ = GetXLData(l_CurrentTop& + 6, 4)
    
        Do While l_DupeMastercount& > 0
        
            If l_DupeMastercount& > 1 Then
                l_comma& = InStr(l_Dupe_Orig_Masterspool$, ",")
                l_DupeMasterSpoolNumber$ = Left(l_Dupe_Orig_Masterspool$, l_comma& - 1)
                If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", l_DupeMasterSpoolNumber$) <> 0 Then
                    l_DupeMasterSpoolNumber$ = GetData("bbc_barcode_correction", "corrected_barcode", "original_barcode", l_DupeMasterSpoolNumber$)
                End If
                l_Dupe_Orig_Masterspool$ = Mid(l_Dupe_Orig_Masterspool$, l_comma& + 1, Len(l_Dupe_Orig_Masterspool$))
                l_comma& = InStr(l_Dupe_Orig_Masterformat$, ",")
                l_DupeBBCMasterTapeFormat$ = Left(l_Dupe_Orig_Masterformat$, l_comma& - 1)
                l_DupeMasterTapeFormat$ = GetAlias(l_DupeBBCMasterTapeFormat$)
                l_Dupe_Orig_Masterformat$ = Mid(l_Dupe_Orig_Masterformat$, l_comma + 1, Len(l_Dupe_Orig_Masterformat$))
                l_comma& = InStr(l_Dupe_Orig_Masterstandard$, ",")
                l_DupeMasterLineStandard$ = GetAlias(Left(l_Dupe_Orig_Masterstandard$, l_comma - 1))
                l_Dupe_Orig_Masterstandard$ = Mid(l_Dupe_Orig_Masterstandard$, l_comma + 1, Len(l_Dupe_Orig_Masterstandard$))
                l_comma& = InStr(l_Dupe_Orig_Masteraspect$, ",")
                If l_comma > 0 Then
                    l_DupeMasterAspectRatio$ = GetAlias(Left(l_Dupe_Orig_Masteraspect$, l_comma - 1))
                    l_Dupe_Orig_Masteraspect$ = Mid(l_Dupe_Orig_Masteraspect$, l_comma + 1, Len(l_Dupe_Orig_Masteraspect$))
                Else
                    l_DupeMasterAspectRatio$ = GetAlias(l_Dupe_Orig_Masteraspect$)
                End If
                l_comma& = InStr(l_Dupe_Orig_Masterduration$, ",")
                l_DupeMasterSpoolDuration$ = Left(l_Dupe_Orig_Masterduration$, l_comma - 1)
                l_Dupe_Orig_Masterduration$ = Mid(l_Dupe_Orig_Masterduration$, l_comma + 1, Len(l_Dupe_Orig_Masterduration$))
            End If
            l_DupeMastercount& = l_DupeMastercount& - 1
            l_lngLibraryID = GetData("library", "libraryID", "barcode", l_DupeMasterSpoolNumber$)
            
            l_DupeEpisodeNumber$ = GetXLData(l_CurrentTop&, 6)
            l_DupeTotalDuration$ = GetXLData(l_CurrentTop& + 1, 6)
            l_DupeQuantity$ = GetXLData(l_CurrentTop& + 2, 6)
            l_BBCDupeTapeFormat$ = GetXLData(l_CurrentTop& + 3, 6)
            l_DupeTapeFormat$ = GetAlias(l_BBCDupeTapeFormat$)
            l_DupeLineStandard$ = GetAlias(GetXLData(l_CurrentTop& + 4, 6))
            l_Original_DupeStandard$ = GetXLData(l_CurrentTop& + 4, 6)
            l_DupeAspectRatio$ = GetAlias(GetXLData(l_CurrentTop& + 5, 6))
            l_DupeLanguage$ = GetAlias(GetXLData(l_CurrentTop& + 6, 6))
            l_DupeCeefax$ = GetAlias(GetXLData(l_CurrentTop& + 7, 6))
            l_DupeAudio$ = GetXLData(l_CurrentTop& + 8, 6)
            
            'we need to get the different sections of the sounds out of the sound notes
            If l_DupeAudio$ <> "" Then
            
                'append another comma seperated value onto the end of the audio
                'channels to ensure we get the last value
                l_DupeAudio$ = l_DupeAudio$ & ",END"
                    
                'get each Sound channel using the GetTextPart function from
                'modRoutines
                l_Sound1$ = GetAlias(GetTextPart(l_DupeAudio$, 1, ","))
                l_Sound2$ = GetAlias(GetTextPart(l_DupeAudio$, 2, ","))
                l_Sound3$ = GetAlias(GetTextPart(l_DupeAudio$, 3, ","))
                l_Sound4$ = GetAlias(GetTextPart(l_DupeAudio$, 4, ","))
                l_Sound5$ = GetAlias(GetTextPart(l_DupeAudio$, 5, ","))
                l_Sound6$ = GetAlias(GetTextPart(l_DupeAudio$, 6, ","))
                l_Sound7$ = GetAlias(GetTextPart(l_DupeAudio$, 7, ","))
                l_Sound8$ = GetAlias(GetTextPart(l_DupeAudio$, 8, ","))
            
            End If
            
            l_rsDetails.AddNew
            l_rsDetails("jobID") = l_NewJobNumber&
            
            If l_lngLibraryID <> 0 Then
                l_rsDetails("OriginalBarcode") = Trim(l_DupeMasterSpoolNumber$)
                l_rsDetails("MasterBarcode") = Trim(l_DupeMasterSpoolNumber$)
                l_rsDetails("MasterLibraryID") = l_lngLibraryID
                If IsItHere(GetData("library", "location", "libraryID", l_lngLibraryID)) = True Then
                    l_rsDetails("DateMasterArrived") = Now
                    l_rsDetails("Completeable") = Now
                    l_rsDetails("FirstCompleteable") = Now
                    Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & l_lngTargetDateCountdown, g_strExecuteError)
                    l_datNewTargetDate = l_rst(0)
                    l_rst.Close
                    If IsDate(GetData("job", "deadlinedate", "jobID", l_NewJobNumber&)) Then
                        l_datNewTargetDate2 = GetData("job", "deadlinedate", "jobID", l_NewJobNumber&)
                        l_datNewTargetDate2 = DateAdd("h", -12, l_datNewTargetDate2)
                    End If
                    If l_datNewTargetDate2 > l_datNewTargetDate Then l_datNewTargetDate = l_datNewTargetDate2
                    l_rsDetails("TargetDate") = l_datNewTargetDate
                End If
            End If
            l_rsDetails("SpecialProject") = Left(l_Notes$, 49)
            l_rsDetails("TargetDateCountdown") = l_lngTargetDateCountdown
            If l_EpisodeNumber$ <> "" Then
                l_rsDetails("description") = Left(l_ProgrammeTitle$, 50) & " Ep." & l_EpisodeNumber$
            Else
                l_rsDetails("description") = Left(l_ProgrammeTitle$, 50)
            End If
            If l_DupeTapeFormat$ <> "" Then
                l_rsDetails("format") = GetInclusiveCode(l_DupeTapeFormat$, l_DupeMasterTapeFormat$, l_DupeMasterLineStandard$, l_DupeLineStandard$)
                l_rsDetails("workflowdescription") = Trim(" " & GetDataSQL("SELECT TOP 1 description FROM xref WHERE category = 'inclusive' AND format = '" & l_rsDetails("format") & "'"))
            End If
            l_rsDetails("copytype") = "I"
            If l_DupeTapeFormat$ = "DVD" Then
                l_rsDetails("quantity") = Val(l_DupeQuantity$) * l_EpisodeCount
            Else
                l_rsDetails("quantity") = l_EpisodeCount
            End If
            If l_DupeTapeFormat$ = "DBETA" Or l_DupeTapeFormat$ = "BETASP" Then
                If Val(l_DupeQuantity$) > 1 Then
                    l_intExtraCopiesFromSameReplay = Val(l_DupeQuantity$) - 1
                End If
            End If
    '        If l_rsDetails("format") = "WWSDVHS" Then
    '            If Val(l_DupeQuantity$) <= 10 Then
    '                l_rsDetails("format") = l_rsDetails("format") & "10"
    '            ElseIf Val(l_DupeQuantity$) <= 20 Then
    '                l_rsDetails("format") = l_rsDetails("format") & "20"
    '            Else
    '                l_rsDetails("format") = l_rsDetails("format") & "20+"
    '            End If
    '        End If
            If l_DupeLineStandard$ <> "" Then l_rsDetails("videostandard") = Left(l_DupeLineStandard$, 10)
            If Int(Val(l_DupeMasterSpoolDuration$)) > Int(Val(l_DupeTotalDuration$)) Then
                l_rsDetails("runningtime") = Int(Val(l_DupeMasterSpoolDuration$))
                l_DupeTotalDuration$ = l_DupeMasterSpoolDuration$
            Else
                l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
            End If
            If l_DupeAspectRatio$ <> "" And l_DupeAspectRatio$ <> "ASK" Then
                l_rsDetails("aspectratio") = l_DupeAspectRatio$
            Else
                If l_Original_DupeStandard$ Like "*16:9 WI*" Then
                    l_rsDetails("aspectratio") = "16:9AN"
                    l_DupeAspectRatio$ = "16:9AN"
                End If
                If l_Original_DupeStandard$ Like "*16:9 LE*" Then
                    l_rsDetails("aspectratio") = "16:9LE"
                    l_DupeAspectRatio$ = "16:9LE"
                End If
                If l_Original_DupeStandard$ Like "*15:9*" Then
                    l_rsDetails("aspectratio") = "15:9LE"
                    l_DupeAspectRatio$ = "15:9LE"
                End If
                If l_Original_DupeStandard$ Like "*14:9*" Then
                    l_rsDetails("aspectratio") = "14:9LE"
                    l_DupeAspectRatio$ = "14:9LE"
                End If
                If l_Original_DupeStandard$ Like "*4:3*" Then
                    l_rsDetails("aspectratio") = "4:3NOR"
                    l_DupeAspectRatio$ = "4:3NOR"
                End If
            End If
    '        If l_rsDetails("aspectratio") = "" Then
    '            l_rsDetails("aspectratio") = "4:3NOR"
    '            l_DupeAspectRatio$ = "4:3NOR"
    '        End If
            If l_Sound1$ <> "" Then l_rsDetails("sound1") = Left(l_Sound1$, 6)
            If l_Sound2$ <> "" Then l_rsDetails("sound2") = Left(l_Sound2$, 6)
            If l_Sound3$ <> "" Then l_rsDetails("sound3") = Left(l_Sound3$, 6)
            If l_Sound4$ <> "" Then l_rsDetails("sound4") = Left(l_Sound4$, 6)
            
        
            l_rsDetails.Update
            
            l_rsDetails.Bookmark = l_rsDetails.Bookmark
            l_lngdetailID = l_rsDetails("jobdetailID")
            l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
            l_rsDetails.Update
            
            l_rsxDetails.AddNew
            l_rsxDetails("jobID") = l_NewJobNumber&
            l_rsxDetails("jobdetailID") = l_lngdetailID
            If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
            If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
            If l_OrderNumber$ <> "" Then l_rsxDetails("BBCorderNumber") = l_OrderNumber$
            If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
            If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
            If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
            If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
            If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
            If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
            If l_DupeBBCMasterTapeFormat$ <> "" Then l_rsxDetails("bbcSourceFormat") = l_DupeBBCMasterTapeFormat$
            If l_BBCDupeTapeFormat$ <> "" Then l_rsxDetails("bbcRecordFormat") = l_BBCDupeTapeFormat$
            If l_DespatchMethod$ <> "" Then l_rsxDetails("DespatchMethod") = l_DespatchMethod$
            If l_CourierAccountNumber$ <> "" Then l_rsxDetails("CourierAccountNumber") = l_CourierAccountNumber$
            If l_DespatchCostCentre$ <> "" Then l_rsxDetails("DespatchCostCentre") = l_DespatchCostCentre$
            If l_DespatchNominalCode$ <> "" Then l_rsxDetails("DespatchNominalCode") = l_DespatchNominalCode$
            If l_MasterContactName$ <> "" Then l_rsxDetails("MasterContactName") = l_MasterContactName$
            If l_DespatchTelephone$ <> "" Then l_rsxDetails("DespatchTelephone") = l_DespatchTelephone$
            If l_MasterEmail$ <> "" Then l_rsxDetails("MasterEmail") = l_MasterEmail$
            If l_DespatchAddress$ <> "" Then l_rsxDetails("DespatchAddress") = l_DespatchAddress$
            If l_DespatchBusinessAreaCode$ <> "" Then l_rsxDetails("DespatchBusinessAreaCode") = l_DespatchBusinessAreaCode$
            If l_DupeMasterLineStandard$ <> "" Then l_rsxDetails("MasterVideoStandard") = l_DupeMasterLineStandard$
            If l_DupeMasterAspectRatio$ <> "" Then l_rsxDetails("MasterAspectRatio") = l_DupeMasterAspectRatio$
            If l_DupeLineStandard$ <> "" Then l_rsxDetails("CopyVideoStandard") = l_DupeLineStandard$
            If l_DupeAspectRatio$ <> "" Then l_rsxDetails("CopyAspectRatio") = l_DupeAspectRatio$
            
            l_rsxDetails.Update
                
                
            'Are extra broadcast copies needed
            If l_intExtraCopiesFromSameReplay <> 0 Then
                l_rsDetails.AddNew
                l_rsDetails("jobID") = l_NewJobNumber&
                l_rsDetails("SpecialProject") = Left(l_Notes$, 49)
                l_rsDetails("description") = "Extra " & l_DupeTapeFormat$ & " copies"
                If l_DupeTapeFormat$ = "DBETA" Then
                    l_rsDetails("format") = "WWADDB"
                Else
                    l_rsDetails("format") = "WWADSP"
                End If
                l_rsDetails("copytype") = "I"
                l_rsDetails("quantity") = l_intExtraCopiesFromSameReplay * l_EpisodeCount
                l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
                l_rsDetails.Update
        
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_lngdetailID = l_rsDetails("jobdetailID")
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                
                l_rsxDetails.AddNew
                l_rsxDetails("jobID") = l_NewJobNumber&
                l_rsxDetails("jobdetailID") = l_lngdetailID
                If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
                If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
                If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
                If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
                If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
                If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
                If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
                If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
                If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
                l_rsxDetails.Update
                l_intExtraCopiesFromSameReplay = 0
            End If
            
            
            If UCase(l_DupeCeefax$) = "TRUE" Then
                'create a new ceefax line
                l_rsDetails.AddNew
                l_rsDetails("jobID") = l_NewJobNumber&
                l_rsDetails("SpecialProject") = Left(l_Notes$, 49)
                l_rsDetails("description") = "CEEFAX Subtitler"
                l_rsDetails("format") = "SUBT"
                l_rsDetails("copytype") = "A"
                l_rsDetails("quantity") = l_EpisodeCount
                l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
                l_rsDetails.Update
        
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_lngdetailID = l_rsDetails("jobdetailID")
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                
                l_rsxDetails.AddNew
                l_rsxDetails("jobID") = l_NewJobNumber&
                l_rsxDetails("jobdetailID") = l_lngdetailID
                If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
                If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
                If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
                If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
                If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
                If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
                If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
                If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
                If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
                l_rsxDetails.Update
        
            End If
            
            If l_DupeLineStandard$ <> l_DupeMasterLineStandard$ Then
                ' create a standards converter line
                l_rsDetails.AddNew
                l_rsDetails("jobID") = l_NewJobNumber&
                l_rsDetails("SpecialProject") = Left(l_Notes$, 49)
                l_rsDetails("description") = "Standards Converter"
                If l_DupeTapeFormat$ = "VHS" Then
                    l_rsDetails("format") = "CONV"
                Else
                    l_rsDetails("format") = "B-CONV"
                End If
                l_rsDetails("copytype") = "A"
                l_rsDetails("quantity") = l_EpisodeCount
                l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
                l_rsDetails.Update
                
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_lngdetailID = l_rsDetails("jobdetailID")
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                
                l_rsxDetails.AddNew
                l_rsxDetails("jobID") = l_NewJobNumber&
                l_rsxDetails("jobdetailID") = l_lngdetailID
                If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
                If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
                If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
                If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
                If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
                If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
                If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
                If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
                If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
                l_rsxDetails.Update
            
            End If
            
            If Left(l_DupeMasterAspectRatio$, 6) <> Left(l_DupeAspectRatio$, 6) Then
                If Not (l_DupeMasterAspectRatio$ Like "*4:3*" And l_DupeAspectRatio$ Like "*4:3*") Then
                    ' create a ARC line
                    l_rsDetails.AddNew
                    l_rsDetails("jobID") = l_NewJobNumber&
                    l_rsDetails("SpecialProject") = Left(l_Notes$, 49)
                    l_rsDetails("description") = "Aspect Ratio Converter"
                    l_rsDetails("format") = "ARATIO"
                    l_rsDetails("copytype") = "A"
                    l_rsDetails("quantity") = l_EpisodeCount
                    l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
                    l_rsDetails.Update
                
                    l_rsDetails.Bookmark = l_rsDetails.Bookmark
                    l_lngdetailID = l_rsDetails("jobdetailID")
                    l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                    l_rsDetails.Update
                    
                    l_rsxDetails.AddNew
                    l_rsxDetails("jobID") = l_NewJobNumber&
                    l_rsxDetails("jobdetailID") = l_lngdetailID
                    If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
                    If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
                    If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
                    If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
                    If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
                    If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
                    If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
                    If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
                    If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
                    l_rsxDetails.Update
                End If
            End If
        
            If (l_DupeTapeFormat$ = "VHS" Or l_DupeTapeFormat$ = "DVD") And Val(l_DupeQuantity$) > 1 Then
                
                'Do another line to add the extra DVD and VHS copies
                
                l_rsDetails.AddNew
                l_rsDetails("jobID") = l_NewJobNumber&
                l_rsDetails("SpecialProject") = Left(l_Notes$, 49)
                l_rsDetails("description") = "Additional Copies"
                If l_DupeTapeFormat$ = "VHS" Then
                    l_rsDetails("format") = "WWADVHS"
                Else
                    l_rsDetails("format") = "WWADDVD"
                End If
                l_rsDetails("copytype") = "I"
                l_rsDetails("quantity") = (Val(l_DupeQuantity$) - 1) * l_EpisodeCount
                l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
                l_rsDetails.Update
        
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_lngdetailID = l_rsDetails("jobdetailID")
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                
                l_rsxDetails.AddNew
                l_rsxDetails("jobID") = l_NewJobNumber&
                l_rsxDetails("jobdetailID") = l_lngdetailID
                If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
                If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
                If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
                If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
                If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
                If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
                If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
                If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
                If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
                l_rsxDetails.Update
                
            End If
            DBEngine.Idle
            DoEvents
        
            If l_DupeMastercount& = 1 Then
                l_DupeBBCMasterTapeFormat$ = l_Dupe_Orig_Masterformat$
                l_DupeMasterTapeFormat$ = GetAlias(l_DupeBBCMasterTapeFormat$)
                l_DupeMasterSpoolNumber$ = l_Dupe_Orig_Masterspool$
                l_DupeMasterLineStandard$ = GetAlias(l_Dupe_Orig_Masterstandard$)
                l_DupeMasterAspectRatio$ = GetAlias(l_Dupe_Orig_Masteraspect$)
                l_DupeMasterSpoolDuration$ = l_Dupe_Orig_Masterduration$
            End If
        Loop
    Loop
    'Copy Loop has finished - need to reset the currenttop to beyond the end of the last group again
    l_CurrentTop& = l_CurrentTop& + (11 * l_OrigCopyCount)
Loop

If l_NewJobNumber& <> 0 Then
    l_strSQL = "DELETE FROM jobdetail WHERE copytype = 'M' and jobID = " & l_NewJobNumber& & ";"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    l_strSQL = "DELETE FROM jobdetail WHERE copytype = 'A' and jobID = " & l_NewJobNumber& & ";"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    If GetCount("SELECT jobdetailID FROM jobdetail WHERE jobID = " & l_NewJobNumber&) > 0 Then
        If GetCount("SELECT jobdetailID FROM jobdetail WHERE jobID = " & l_NewJobNumber& & " AND DateMasterArrived IS NULL") > 0 Then
            SetData "job", "fd_status", "jobID", l_NewJobNumber&, "Confirmed"
        Else
            SetData "job", "fd_status", "jobID", l_NewJobNumber&, "Masters Here"
        End If
    End If
    l_rsDetails.Close
    Set l_rsDetails = ExecuteSQL("SELECT fd_orderby FROM jobdetail WHERE jobID = " & l_NewJobNumber&, g_strExecuteError)
    If l_rsDetails.RecordCount > 0 Then
        l_rsDetails.MoveFirst
        l_Counter& = 0
        Do While Not l_rsDetails.EOF
            l_rsDetails("fd_orderby") = l_Counter&
            l_Counter& = l_Counter& + 1
            l_rsDetails.MoveNext
        Loop
    End If
End If

'close the database and recordset
l_rsJobs.Close: Set l_rsJobs = Nothing
l_rsDetails.Close: Set l_rsDetails = Nothing
Set l_rsLib = Nothing

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

Dim FSO As Scripting.FileSystemObject
Set FSO = New Scripting.FileSystemObject
'Rename file into the done folder - commented out during testing.
On Error Resume Next
FSO.MoveFile l_strExcelFileName, "\\jcaserver.jcatv.co.uk\Ceta\Processed Star Orders\" & l_strExcelFileTitle

MsgBox "File Sucessfully Read", vbInformation, "Excel Read"

End Sub

Private Sub cmdMoMediaiTunesTV_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strTitle As String, l_strVendorID As String, l_strContainerID As String, l_strContainerPosition As String, l_strReleaseDate As String, l_strRating As String, l_strCopyright As String, l_strProvider As String, l_strStudio_Release_Title As String
Dim l_strSynopsis As String, l_strPreviewStartTime As String, l_strTerritory As String, l_strSalesStartDate As String, l_strClearedForSale As String, l_strLanguage As String, l_strOriginalLocale As String, l_strEpisodeProductionNumber As String
Dim l_lngTemp As Long, l_strSQL As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "Cover Sheet", "")
'If l_strExcelSheetName = "" Then
'    oWorkbook.Close
'    Set oWorkbook = Nothing
'    Set oExcel = Nothing
'    Exit Sub
'End If
l_strExcelSheetName = "Sheet1"
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = 2

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    l_strVendorID = Trim(GetXLData(l_lngXLRowCounter, 5))
    lblProgress.Caption = l_strVendorID
    DoEvents
    l_strProvider = GetXLData(l_lngXLRowCounter, 1)
    l_strLanguage = GetXLData(l_lngXLRowCounter, 3)
    Select Case l_strLanguage
        Case "French-FR"
            l_strLanguage = "fr-FR"
        Case "German-DE"
            l_strLanguage = "de-DE"
        Case "English-GB"
            l_strLanguage = "en-GB"
        Case Else
            'Do nothing
    End Select
        
    l_strOriginalLocale = Trim(GetXLData(l_lngXLRowCounter, 4))
    If Len(l_strOriginalLocale) = 2 Then l_strOriginalLocale = LCase(l_strOriginalLocale)
    l_strEpisodeProductionNumber = Trim(GetXLData(l_lngXLRowCounter, 6))
    l_strTitle = Trim(GetXLData(l_lngXLRowCounter, 7))
    l_strContainerID = Trim(GetXLData(l_lngXLRowCounter, 8))
    l_strContainerPosition = Trim(GetXLData(l_lngXLRowCounter, 9))
    l_strReleaseDate = Trim(GetXLData(l_lngXLRowCounter, 10))
    l_strRating = Trim(GetXLData(l_lngXLRowCounter, 11))
    l_strCopyright = Trim(GetXLData(l_lngXLRowCounter, 12))
    l_strSynopsis = Trim(GetXLData(l_lngXLRowCounter, 13))
    l_strPreviewStartTime = Trim(GetXLData(l_lngXLRowCounter, 17))
    l_strTerritory = Trim(GetXLData(l_lngXLRowCounter, 18))
    l_strTerritory = UCase(l_strTerritory)
    l_strSalesStartDate = Trim(GetXLData(l_lngXLRowCounter, 19))
    l_strClearedForSale = Trim(GetXLData(l_lngXLRowCounter, 20))
    l_strStudio_Release_Title = Trim(GetXLData(l_lngXLRowCounter, 21))
    
    l_lngTemp = GetData("iTunes_Package", "iTunes_PackageID", "video_vendorID", l_strVendorID)
    If Val(l_lngTemp) = 0 Then
        l_strSQL = "INSERT INTO iTunes_Package (companyID, provider, video_type, video_vendorID, video_ep_prod_no, video_title, video_title_utf8, studio_release_title, studio_release_title_utf8, video_containerID, video_containerposition, video_releasedate, "
        Select Case l_strTerritory
            Case "FR"
                l_strSQL = l_strSQL & "rating_fr, "
            Case "DE"
                l_strSQL = l_strSQL & "rating_de, "
            Case "GB"
                l_strSQL = l_strSQL & "rating_uk, "
            Case "US"
                l_strSQL = l_strSQL & "rating_us, "
            Case "CA"
                l_strSQL = l_strSQL & "rating_ca, "
            Case Else
                MsgBox "Unhandled Territory - please get CETA updated. Row will be wrong"
        End Select
                
        l_strSQL = l_strSQL & "video_copyright_cline, video_copyright_cline_utf8, video_longdescription, video_longdescription_utf8, video_previewstarttime, video_language, metadata_language, originalspokenlocale, "
        l_strSQL = l_strSQL & "fullcroptop, fullcropbottom, fullcropleft, fullcropright) VALUES ("
        l_strSQL = l_strSQL & "1567, "
        l_strSQL = l_strSQL & "'" & l_strProvider & "', "
        l_strSQL = l_strSQL & "'tv', "
        l_strSQL = l_strSQL & "'" & l_strVendorID & "', "
        l_strSQL = l_strSQL & "'" & l_strEpisodeProductionNumber & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(l_strTitle)) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strStudio_Release_Title) & "', "
        l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(l_strStudio_Release_Title)) & "', "
        l_strSQL = l_strSQL & "'" & l_strContainerID & "', "
        l_strSQL = l_strSQL & "'" & l_strContainerPosition & "', "
        l_strSQL = l_strSQL & "'" & Format(l_strReleaseDate, "YYYY-MM-DD") & "', "
        l_strSQL = l_strSQL & "'" & l_strRating & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strCopyright) & "', "
        l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(l_strCopyright)) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSynopsis) & "', "
        l_strSQL = l_strSQL & "'" & UTF8_Encode(QuoteSanitise(l_strSynopsis)) & "', "
        l_strSQL = l_strSQL & IIf(l_strPreviewStartTime <> "", l_strPreviewStartTime, 0) & ", "
        l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
        l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
        l_strSQL = l_strSQL & "'" & l_strOriginalLocale & "', 4, 4, 4, 4);"
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_lngTemp = g_lngLastID
        
        
        l_strSQL = "INSERT INTO iTunes_product (iTunes_packageID, territory, salesstartdate, clearedforsale) VALUES ("
        l_strSQL = l_strSQL & l_lngTemp & ", "
        l_strSQL = l_strSQL & "'" & l_strTerritory & "', "
        l_strSQL = l_strSQL & "'" & Format(l_strSalesStartDate, "YYYY-MM-DD") & "', "
        l_strSQL = l_strSQL & "'" & l_strClearedForSale & "');"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    Else
        MsgBox "Vendor ID: " & l_strVendorID & " is already present in the iTunes Packages list. This line was not read in." & vbCrLf & "If you wish to read this line in, please manually delete the old package first.", vbInformation
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1
Loop
lblProgress.Caption = ""

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
MsgBox "Done"

End Sub

Private Sub cmdNewLionIngestRequest_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long, l_lngXLRowCounter2 As Long
Dim l_strBarcode As String, l_strNewBarcode As String, l_strLastBarcode As String, l_strReference As String, l_strTitle As String, l_strSubTitle As String, l_lngEpisode As Long, l_strAlphaBusiness As String
Dim l_lngCRID As Long, l_strFormat As String, l_strLegacyLineStandard As String, l_strImageAspectRatio As String, l_strLegacyPictureElement As String, l_strLegacyFrameRate As String, l_strBBCCoreID As String
Dim l_strLanguage As String, l_strSubtitlesLanguage As String, l_strAudioConfiguration As String, l_strDurationEstimate As String, l_lngLastTrackerID As Long, l_strAudioContent As String
Dim l_strDADCStatus As String, l_datDueDate As Date, l_strDueDate As String, l_strEmailBody As String, l_strEmailBody2 As String, l_strEmailBody3 As String, l_rstWhoToEmail As ADODB.Recordset, l_lngExternalAlphaKey As Long, l_lngWorkflowID As Long
Dim l_lngMonth As Long, l_lngDay As Long, l_lngYear As Long, l_lngCount1 As Long, l_lngCount2 As Long, l_blnDateChange As Boolean, l_datOldDate As Date, l_lngBatchNumber As Long
Dim l_strAdditionalElements As String, l_strContentVersionCode As String, l_lngContentVersionID As Long, l_strRush As String, l_blnRushChanged As Boolean, l_datNewTargetDate As Date
Dim l_rst As ADODB.Recordset, Count2 As Long, l_strVendorGroup As String, l_strTotalAudio As String, l_strMasterSpec As String, TrackerID As Long, l_strIngestRequestor As String, l_strLegacyOracTVAENumber As String
Dim l_lngClipID As Long, FSO As Scripting.FileSystemObject, l_strPathToFile As String, l_lngLibraryID As Long
Dim l_strEPV_ID As String, l_strIngestFilenameToUse As String, l_lngSeriesID As Long

Dim l_strSQL As String, TempStr As String, Count As Long, l_strTextInPicture As String, l_strOldReference As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "BBCW Ingest Queue Inventory Rep")
'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "BBCW Ingest Management Report")
'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "BBCW_Ingest_Management_Report")
'l_strExcelSheetName = GetData("setting", "value", "name", "DADC_IR_Tab_Name")
'If l_strExcelSheetName = "" Then
'    oWorkbook.Close
'    Set oWorkbook = Nothing
'    Set oExcel = Nothing
'    Exit Sub
'End If
'Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)
Set oWorksheet = oWorkbook.Worksheets(1)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "DADC Excel Read", GetData("setting", "intvalue", "name", "DADC_IR_FirstRow")))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

l_strLastBarcode = ""
l_strEmailBody = ""
l_strEmailBody2 = ""
l_strEmailBody3 = ""

SaveCETASetting "DADCUpdateInProgress", "1"
l_lngXLRowCounter2 = l_lngXLRowCounter
'A first run through to clear out the requirements string of all currently open rows that are about to be updated.
Do While GetXLData(l_lngXLRowCounter2, 1) <> ""
    l_lngWorkflowID = Val(GetXLData(l_lngXLRowCounter2, 53)) 'BA
    l_strAlphaBusiness = ""
    l_strEPV_ID = Trim(GetXLData(l_lngXLRowCounter2, 14)) 'N
    l_strBarcode = Trim(GetXLData(l_lngXLRowCounter2, 30)) 'AD
    l_strContentVersionCode = ""
    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", l_strBarcode) <> 0 Then
        l_strBarcode = GetData("bbc_barcode_correction", "corrected_barcode", "original_barcode", l_strBarcode)
    End If
    l_strNewBarcode = l_strBarcode
    If UCase(Right(l_strBarcode, 3)) = "FIX" Then
        l_strNewBarcode = Left(l_strBarcode, Len(l_strBarcode) - 4)
    ElseIf UCase(Right(l_strBarcode, 4)) = "REDO" Then
        l_strNewBarcode = Left(l_strBarcode, Len(l_strBarcode) - 5)
    End If
    l_strDADCStatus = Trim(GetXLData(l_lngXLRowCounter2, 3)) 'C
    l_strVendorGroup = Trim(GetXLData(l_lngXLRowCounter2, 76)) 'BX
    
    If LCase(l_strDADCStatus) = "requested" And l_strBarcode <> "" And (InStr(l_strVendorGroup, "MX1") > 0 Or InStr(l_strVendorGroup, "VDMS - London") > 0) Then
        lblProgress.Caption = "Row: " & l_lngXLRowCounter2 & " - " & l_strBarcode & " - Clearing old requirements information"
'        If l_strContentVersionCode = "" Then
            TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE newbarcode='" & UCase(QuoteSanitise(l_strNewBarcode)) & "' AND contentversioncode = '" & l_strEPV_ID & "' AND companyID = 1608 AND workflowID = " & l_lngWorkflowID & ";")
'        Else
'            TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE newbarcode='" & UCase(l_strNewBarcode) & "' AND contentversioncode = '" & l_strContentVersionCode & "' AND companyID = 1608 AND workflowID = " & l_lngWorkflowID & ";")
'        End If
        If Val(TempStr) <> 0 Then
            l_strSQL = "UPDATE tracker_dadc_item SET "
            l_strSQL = l_strSQL & "audioconfiguration = '' "
            
'            If l_strContentVersionCode = "" Then
                l_strSQL = l_strSQL & "WHERE newbarcode = '" & QuoteSanitise(QuoteSanitise(l_strNewBarcode)) & "' AND contentversioncode = '" & l_strEPV_ID & "' AND companyID = 1608 AND workflowID = " & l_lngWorkflowID & ";"
'            Else
'                l_strSQL = l_strSQL & "WHERE newbarcode = '" & QuoteSanitise(l_strNewBarcode) & "' AND contentversioncode = '" & l_strContentVersionCode & "' AND companyID = 1608 AND workflowID = " & l_lngWorkflowID & ";"
'            End If
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
        End If
    End If
    l_lngXLRowCounter2 = l_lngXLRowCounter2 + 1
Loop

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_blnDateChange = False
    l_lngCRID = Val(Trim(GetXLData(l_lngXLRowCounter, 2))) 'B
    l_strBarcode = Trim(GetXLData(l_lngXLRowCounter, 30)) 'AD
    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", l_strBarcode) <> 0 Then
        l_strBarcode = GetData("bbc_barcode_correction", "corrected_barcode", "original_barcode", l_strBarcode)
    End If
    l_strNewBarcode = l_strBarcode
    If UCase(Right(l_strBarcode, 3)) = "FIX" Then
        l_strNewBarcode = Left(l_strBarcode, Len(l_strBarcode) - 4)
    ElseIf UCase(Right(l_strBarcode, 4)) = "REDO" Then
        l_strNewBarcode = Left(l_strBarcode, Len(l_strBarcode) - 5)
    End If
    l_strMasterSpec = Trim(GetXLData(l_lngXLRowCounter, 31)) 'AE
    l_strBBCCoreID = Trim(GetXLData(l_lngXLRowCounter, 29)) 'AC
    l_strDADCStatus = Trim(GetXLData(l_lngXLRowCounter, 3)) 'C
    l_strAlphaBusiness = ""
    l_strEPV_ID = Trim(GetXLData(l_lngXLRowCounter, 14)) 'N - the new EPV ID
    l_strContentVersionCode = ""
    l_lngContentVersionID = 0
    l_strVendorGroup = Trim(GetXLData(l_lngXLRowCounter, 76)) 'BX
    l_lngWorkflowID = Val(GetXLData(l_lngXLRowCounter, 53)) 'BA
    l_strTitle = GetXLData(l_lngXLRowCounter, 10) 'J
    l_lngSeriesID = Val(GetXLData(l_lngXLRowCounter, 6)) 'F
    l_strSubTitle = GetXLData(l_lngXLRowCounter, 13) 'M
    l_lngEpisode = Val(GetXLData(l_lngXLRowCounter, 12)) 'L
    l_strFormat = ""
    l_strLegacyFrameRate = GetXLData(l_lngXLRowCounter, 33) 'AG
    l_strLegacyLineStandard = GetXLData(l_lngXLRowCounter, 34) 'AH
    l_strImageAspectRatio = GetXLData(l_lngXLRowCounter, 37) 'AK
    l_strLegacyPictureElement = ""
    l_strAdditionalElements = GetXLData(l_lngXLRowCounter, 44) 'AR
    l_strLanguage = GetXLData(l_lngXLRowCounter, 47) 'AU
    If l_strLanguage = "None" Then l_strLanguage = ""
    l_strAudioConfiguration = GetAlias(GetXLData(l_lngXLRowCounter, 45)) 'AS
    l_strAudioContent = GetXLData(l_lngXLRowCounter, 46) 'AT
    l_strDurationEstimate = ""
    l_strTextInPicture = Trim(" " & GetXLData(l_lngXLRowCounter, 51)) 'AY
    l_strRush = Trim(" " & GetXLData(l_lngXLRowCounter, 71)) 'BS
    l_strDueDate = GetXLData(l_lngXLRowCounter, 74) 'BV
    l_strIngestRequestor = GetXLData(l_lngXLRowCounter, 65) 'BM
    l_strLegacyOracTVAENumber = GetXLData(l_lngXLRowCounter, 25) 'Y
    l_strIngestFilenameToUse = GetXLData(l_lngXLRowCounter, 79) 'CA
    
    If LCase(l_strDADCStatus) = "requested" And l_strBarcode <> "" And (InStr(l_strVendorGroup, "MX1") > 0 Or InStr(l_strVendorGroup, "VDMS - London") > 0) Then
    
        If l_strDueDate <> "" Then
'            'Code for dwaling with US format dates
'            l_lngCount1 = InStr(l_strDueDate, "/")
'            l_lngDay = Val(Mid(l_strDueDate, l_lngCount1 + 1))
'            l_lngCount2 = InStr(Mid(l_strDueDate, l_lngCount1 + 1), "/")
'            l_lngYear = Val(Mid(Mid(l_strDueDate, l_lngCount1 + 1), l_lngCount2 + 1, 4))
'            l_lngMonth = Val(l_strDueDate)
'            l_datDueDate = l_lngYear & "-" & Format(l_lngMonth, "00") & "-" & Format(l_lngDay, "00")
            'Code for dealing with UK format dates
            If IsDate(l_strDueDate) Then
                l_datDueDate = l_strDueDate
            Else
                MsgBox "Unreadable Due Date - " & l_strDueDate & vbCrLf & "2099-12-12  will be used instead.", vbInformation, "Error reading Excel Sheet"
                l_datDueDate = "2099-12-12"
            End If
        Else
            l_datDueDate = "2099-12-12"
        End If
        l_lngBatchNumber = Format(l_datDueDate, "ddmmyy")
'        l_lngExternalAlphaKey = Val(GetXLData(l_lngXLRowCounter, 30)) 'AD
'        If l_strContentVersionCode <> "" Then l_strAlphaBusiness = ""
'        If l_strContentVersionCode <> "" Then
'            If l_strIngestFilenameToUse = "" Then
'                l_strReference = SanitiseBarcode(l_strBarcode) & "_" & l_lngContentVersionID & "_" & l_lngWorkflowID
'            Else
'                l_strReference = l_strIngestFilenameToUse
'            End If
'        Else
            If l_strIngestFilenameToUse = "" Then
                l_strReference = SanitiseBarcode(l_strBarcode) & "_" & l_strEPV_ID & "_" & l_lngWorkflowID
            Else
                l_strReference = l_strIngestFilenameToUse
            End If
'        End If
        If Not IsDate(l_strDueDate) Then l_strDueDate = ""
        
        lblProgress.Caption = "Row: " & l_lngXLRowCounter & " - " & l_strBarcode & " - " & l_strTitle & " Ep. " & l_lngEpisode
        DoEvents
        
        TempStr = ""
'        If l_strContentVersionCode = "" Then
            TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE newbarcode='" & UCase(QuoteSanitise(l_strNewBarcode)) & "' AND contentversioncode = '" & l_strEPV_ID & "' AND companyID = 1608 AND workflowID = " & l_lngWorkflowID & ";")
'        Else
'            TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE newbarcode='" & UCase(l_strNewBarcode) & "' AND contentversioncode = '" & l_strContentVersionCode & "' AND companyID = 1608 AND workflowID = " & l_lngWorkflowID & ";")
'        End If
        If Val(TempStr) = 0 Then
            TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE newbarcode='" & UCase(QuoteSanitise(l_strNewBarcode)) & "' AND externalalphaID = '" & l_strAlphaBusiness & "' AND companyID = 1239;")
            l_strSQL = "INSERT INTO tracker_dadc_item (cdate, mdate, companyID, IngestRequestor, itemreference, title, series, subtitle, barcode, newbarcode, format, contentversioncode, BBCCoreID, legacylinestandard, legacyframerate, "
            l_strSQL = l_strSQL & "imageaspectratio, legacypictureelement, additionalelementsrequested, language, AudioConfiguration, LegacyOracTVAENumber, "
            l_strSQL = l_strSQL & "subtitleslanguage, fivepointonerequested, dolbyErequested, textinpicture, stereorequested, monorequested, "
            l_strSQL = l_strSQL & "musicandeffectsrequested, episode, dbbcrid, priority, targetdatecountdown, duedateupload, contentversionID, workflowID, durationestimate, batch) VALUES ("
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "1608, "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strIngestRequestor) & "', "
            If l_strReference <> "" Then
                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strReference) & "', "
            Else
                l_strSQL = l_strSQL & "NULL, "
            End If
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
            l_strSQL = l_strSQL & IIf(l_lngSeriesID <> 0, l_lngSeriesID, "Null") & ", "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSubTitle) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(UCase(l_strBarcode)) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(UCase(l_strNewBarcode)) & "', "
            l_strSQL = l_strSQL & "'" & l_strFormat & "', "
'            If l_strContentVersionCode = "" Then
                l_strSQL = l_strSQL & "'" & l_strEPV_ID & "', "
'            Else
'                l_strSQL = l_strSQL & "'" & l_strContentVersionCode & "', "
'            End If
            l_strSQL = l_strSQL & "'" & l_strBBCCoreID & "', "
            l_strSQL = l_strSQL & "'" & l_strLegacyLineStandard & "', "
            l_strSQL = l_strSQL & "'" & l_strLegacyFrameRate & "', "
            l_strSQL = l_strSQL & "'" & l_strImageAspectRatio & "', "
            l_strSQL = l_strSQL & "'" & l_strLegacyPictureElement & "', "
            If l_strAdditionalElements = "Yes" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
            l_strTotalAudio = IIf(InStr(l_strMasterSpec, "Muxed") > 0, "", l_strMasterSpec) & IIf(l_strAudioConfiguration <> "", IIf(InStr(l_strMasterSpec, "Muxed") > 0, "", ", ") & l_strAudioConfiguration, "") & IIf(l_strAudioContent <> "", ", " & l_strAudioContent, "") & IIf(l_strLanguage <> "", ", " & l_strLanguage & vbCrLf, vbCrLf)
            l_strSQL = l_strSQL & "'" & l_strTotalAudio & "', "
            l_strSQL = l_strSQL & "'" & l_strLegacyOracTVAENumber & "', "
            l_strSQL = l_strSQL & "'" & l_strSubtitlesLanguage & "', "
            If l_strAudioConfiguration = "5.1" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            If Left(l_strAudioConfiguration, 7) = "Dolby E" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            l_strSQL = l_strSQL & "'" & l_strTextInPicture & "', "
            If l_strAudioConfiguration = "Standard Stereo" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            If l_strAudioConfiguration = "Mono" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            If l_strAudioContent = "M&E" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            l_strSQL = l_strSQL & IIf(l_lngEpisode <> 0, l_lngEpisode, "Null") & ", "
            l_strSQL = l_strSQL & l_lngCRID & ", "
            If UCase(l_strRush) = "YES" Then
                l_strSQL = l_strSQL & "1, 1, "
                l_strEmailBody2 = l_strEmailBody2 & "Rush item Added: " & l_strReference & " - Request ID " & l_lngCRID & ", " & l_strTitle & ", Ep " & l_lngEpisode & ", " & l_strSubTitle & vbCrLf
            Else
                l_strSQL = l_strSQL & "0, 3, "
            End If
            l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDueDate) & "', "
            l_strSQL = l_strSQL & l_lngContentVersionID & ", "
            l_strSQL = l_strSQL & l_lngWorkflowID & ", "
            l_strSQL = l_strSQL & Int(Val(l_strDurationEstimate)) & ", "
            l_strSQL = l_strSQL & l_lngBatchNumber & ");"
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            If Val(TempStr) <> 0 Then
                l_strEmailBody = l_strEmailBody & "Added: " & l_strReference & " - Request ID " & l_lngCRID & ", " & l_strTitle & ", Ep " & l_lngEpisode & ", " & l_strSubTitle & " - already existed for Bulk Ingest" & vbCrLf
            Else
                l_strEmailBody = l_strEmailBody & "Added: " & l_strReference & " - Request ID " & l_lngCRID & ", " & l_strTitle & ", Ep " & l_lngEpisode & ", " & l_strSubTitle & vbCrLf
            End If
        Else
            TrackerID = Val(TempStr)
            l_strOldReference = GetData("tracker_dadc_item", "itemreference", "tracker_dadc_itemID", TrackerID)
            If Trim(" " & GetData("tracker_dadc_item", "duedateupload", "tracker_dadc_itemID", TrackerID)) <> "" Then
                l_datOldDate = CDate(Trim(" " & GetData("tracker_dadc_item", "duedateupload", "tracker_dadc_itemID", TrackerID)))
                If l_datDueDate <> l_datOldDate And (l_datOldDate = "2099-12-12" Or l_datDueDate <> "2099-12-12") Then
                    l_blnDateChange = True
                End If
            Else
                l_blnDateChange = True
            End If
            If (GetData("tracker_dadc_item", "priority", "tracker_dadc_itemID", TrackerID) = 0 And UCase(l_strRush) = "YES") Or _
            (GetData("tracker_dadc_item", "priority", "tracker_dadc_itemID", TrackerID) <> 0 And UCase(l_strRush) <> "YES") Then
                l_blnRushChanged = True
            End If
            If l_blnRushChanged = True Then
                Count = GetData("tracker_dadc_item", "targetdatecountdown", "tracker_dadc_itemID", TrackerID)
                If UCase(l_strRush) = "YES" Then
                    If Count > 1 Then
                        Count = 1 - Count
                        If Trim(" " & GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TrackerID)) <> "" And Trim(" " & GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TrackerID)) <> 0 Then
                            Set l_rst = ExecuteSQL("exec fn_getWorkingDaysBackFromDate @fromdate='" & Format(GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TrackerID), "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & -Count, g_strExecuteError)
                            l_datNewTargetDate = l_rst(0)
                            l_rst.Close
                        End If
                        Count = GetData("tracker_dadc_item", "targetdatecountdown", "tracker_dadc_itemID", TrackerID) + Count
                    End If
                Else
                    Count = Count + 2
                    If Count > 3 Then Count = 3
                    If Trim(" " & GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TrackerID)) <> "" And Trim(" " & GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TrackerID)) <> 0 Then
                        Count2 = Count - GetData("tracker_dadc_item", "targetdatecountdown", "tracker_dadc_itemID", TrackerID)
                        If Count2 > 0 Then
                            Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TrackerID), "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Count, g_strExecuteError)
                            l_datNewTargetDate = l_rst(0)
                            l_rst.Close
                        End If
                    End If
                End If
            End If
            TempStr = ""
            If l_strAlphaBusiness <> "" Then TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE newbarcode='" & UCase(l_strBarcode) & "' AND externalalphaID = '" & l_strAlphaBusiness & "' AND companyID = 1239;")
            l_strSQL = "UPDATE tracker_dadc_item SET "
            If l_strReference <> "" Then l_strSQL = l_strSQL & "itemreference = '" & QuoteSanitise(l_strReference) & "', "
            If l_lngSeriesID <> 0 Then l_strSQL = l_strSQL & "series = " & l_lngSeriesID & ", "
            If l_strImageAspectRatio <> "" Then l_strSQL = l_strSQL & "imageaspectratio = '" & l_strImageAspectRatio & "', "
            If l_strLegacyFrameRate <> "" Then l_strSQL = l_strSQL & "legacyframerate = '" & l_strLegacyFrameRate & "', "
            If l_strLegacyLineStandard <> "" Then l_strSQL = l_strSQL & "legacylinestandard = '" & l_strLegacyLineStandard & "', "
            If l_strIngestRequestor <> "" Then l_strSQL = l_strSQL & "IngestRequestor = '" & QuoteSanitise(l_strIngestRequestor) & "', "
            If l_blnDateChange = True Then
                l_strSQL = l_strSQL & "duedateupload = '" & FormatSQLDate(l_datDueDate) & "', "
                l_strSQL = l_strSQL & "batch = " & l_lngBatchNumber & ", "
            End If
            If l_blnRushChanged = True Then
                If UCase(l_strRush) = "YES" Then
                    l_strSQL = l_strSQL & "priority = 1, targetdatecountdown = " & Count & ", "
                    If Trim(" " & GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TempStr)) <> "" And Trim(" " & GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TempStr)) <> 0 Then
                        l_strSQL = l_strSQL & "targetdate = '" & FormatSQLDate(l_datNewTargetDate) & "', "
                    End If
                    l_strEmailBody2 = l_strEmailBody2 & "Item Updated to Rush: " & l_strReference & " - Request ID " & l_lngCRID & ", " & l_strTitle & ", Ep " & l_lngEpisode & ", " & l_strSubTitle & vbCrLf
                Else
                    l_strSQL = l_strSQL & "priority = 0, targetdatecountdown = " & Count & ", "
                    If Trim(" " & GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TempStr)) <> "" And Trim(" " & GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TempStr)) <> 0 Then
                        l_strSQL = l_strSQL & "targetdate = '" & FormatSQLDate(l_datNewTargetDate) & "', "
                    End If
                End If
            End If
            If l_lngExternalAlphaKey <> 0 Then l_strSQL = l_strSQL & "externalalphakey = " & l_lngExternalAlphaKey & ", "
            If l_lngContentVersionID <> 0 Then l_strSQL = l_strSQL & "contentversionID = " & l_lngContentVersionID & ", "
            If l_strBBCCoreID <> "" Then l_strSQL = l_strSQL & "BBCCoreID = '" & l_strBBCCoreID & "', "
            If l_lngWorkflowID <> 0 Then l_strSQL = l_strSQL & "workflowID = " & l_lngWorkflowID & ", "
            l_strSQL = l_strSQL & "dbbcrid = " & l_lngCRID & ", "
            If l_strAudioConfiguration = "5.1" Then l_strSQL = l_strSQL & "fivepointonerequested = 1, " Else l_strSQL = l_strSQL & "fivepointonerequested = 0, "
            If l_strAudioConfiguration = "Standard Stereo" Then l_strSQL = l_strSQL & "stereorequested = 1, " Else l_strSQL = l_strSQL & "stereorequested = 0, "
            If l_strAudioConfiguration = "Mono" Then l_strSQL = l_strSQL & "monorequested = 1, " Else l_strSQL = l_strSQL & "monorequested = 0, "
            If l_strTextInPicture <> "" Then l_strSQL = l_strSQL & "textinpicture = '" & l_strTextInPicture & "', "
            If Left(l_strAudioConfiguration, 7) = "Dolby E" Then l_strSQL = l_strSQL & "dolbyErequested = 1, " Else l_strSQL = l_strSQL & "dolbyErequested = 0, "
            If l_strAudioContent = "M&E" Then l_strSQL = l_strSQL & "musicandeffectsrequested = 1, " Else l_strSQL = l_strSQL & "musicandeffectsrequested = 0, "
            If l_strAdditionalElements = "Yes" Then l_strSQL = l_strSQL & "additionalelementsrequested = 1, " Else l_strSQL = l_strSQL & "additionalelementsrequested = 0, "
            If l_strLanguage <> "" Then l_strSQL = l_strSQL & "language = '" & l_strLanguage & "', "
            l_strTotalAudio = IIf(InStr(l_strMasterSpec, "Muxed") > 0, "", l_strMasterSpec) & IIf(l_strAudioConfiguration <> "", IIf(InStr(l_strMasterSpec, "Muxed") > 0, "", ", ") & l_strAudioConfiguration, "") & IIf(l_strAudioContent <> "", ", " & l_strAudioContent, "") & IIf(l_strLanguage <> "", ", " & l_strLanguage & vbCrLf, vbCrLf)
            If l_strLegacyOracTVAENumber <> "" Then l_strSQL = l_strSQL & "LegacyOracTVAENumber = '" & l_strLegacyOracTVAENumber & "', "
            If InStr(GetData("tracker_dadc_item", "audioconfiguration", "tracker_dadc_itemID", TrackerID), l_strTotalAudio) <= 0 Then
                l_strSQL = l_strSQL & "audioconfiguration = '" & GetData("tracker_dadc_item", "audioconfiguration", "tracker_dadc_itemID", TrackerID) & l_strTotalAudio & "', "
            End If
            l_strSQL = l_strSQL & "mdate = '" & FormatSQLDate(Now) & "' "
            l_strSQL = l_strSQL & "WHERE tracker_DADC_ItemID = " & TrackerID & ";"
'            If l_strAlphaBusiness <> "" Then
'                l_strSQL = l_strSQL & "WHERE newbarcode = '" & QuoteSanitise(l_strNewBarcode) & "' AND externalalphaID = '" & l_strAlphaBusiness & "' AND companyID = 1608 AND workflowID = " & l_lngWorkflowID & ";"
'            Else
'                l_strSQL = l_strSQL & "WHERE newbarcode = '" & QuoteSanitise(l_strNewBarcode) & "' AND contentversioncode = '" & l_strContentVersionCode & "' AND companyID = 1608 AND workflowID = " & l_lngWorkflowID & ";"
'            End If
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            If l_strOldReference <> l_strReference Then
                Set l_rst = ExecuteSQL("SELECT eventID, LibraryID, clipfilename FROM vw_Events_on_DISCSTORE WHERE companyID = 1608 AND system_deleted = 0 and clipreference = '" & l_strOldReference & "'", g_strExecuteError)
                If l_rst.RecordCount > 0 Then
                    l_rst.MoveFirst
                    Do While Not l_rst.EOF
                        l_strSQL = "INSERT INTO event_file_request (eventID, sourcelibraryID, event_file_Request_typeID, NewLibraryID, NewFileName, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
                        l_strSQL = l_strSQL & l_rst("eventID") & ", "
                        l_strSQL = l_strSQL & l_rst("libraryID") & ", "
                        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "File Rename") & ", "
                        l_strSQL = l_strSQL & l_rst("libraryID") & ", "
                        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strReference) & Right(l_rst("clipfilename"), 4) & "', "
                        l_strSQL = l_strSQL & "1, "
                        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                        Debug.Print l_strSQL
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                        l_rst.MoveNext
                    Loop
                End If
                l_rst.Close
            End If
            If l_blnDateChange = True Or l_blnRushChanged = True Then
                l_strEmailBody = l_strEmailBody & "Updated with Date Change or Priority Change: " & l_strReference & " - Request ID " & l_lngCRID & ", " & l_strTitle & ", Ep " & l_lngEpisode & ", " & l_strSubTitle
            Else
                l_strEmailBody = l_strEmailBody & "Updated: " & l_strReference & " - Request ID " & l_lngCRID & ", " & l_strTitle & ", Ep " & l_lngEpisode & ", " & l_strSubTitle
            End If
            If Val(TempStr) <> 0 Then
                l_strEmailBody = l_strEmailBody & " - already existed for Bulk Ingest" & vbCrLf
            Else
                l_strEmailBody = l_strEmailBody & vbCrLf
            End If
        End If
        
        l_strLastBarcode = l_strBarcode
        
    ElseIf LCase(l_strDADCStatus) = "requested" And l_strBarcode <> "" And InStr(l_strVendorGroup, "MX1") <= 0 And InStr(l_strVendorGroup, "VDMS - London") <= 0 Then
        TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE workflowID = " & l_lngWorkflowID & " AND contentversioncode = '" & l_strEPV_ID & "' AND companyID = 1608;")
        If Val(TempStr) <> 0 Then
            l_strSQL = "DELETE FROM tracker_dadc_item WHERE tracker_dadc_itemID = " & TempStr & ";"
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
        End If
    ElseIf LCase(l_strDADCStatus) = "cancelled" And l_strBarcode <> "" Then
        TempStr = ""
        TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE workflowID = " & l_lngWorkflowID & " AND companyID = 1608 and readytobill = 0;")
        If Val(TempStr) <> 0 Then
            TrackerID = Val(TempStr)
            Dim l_lngCount As Long, l_strOldReq As String
            l_strTotalAudio = l_strMasterSpec & IIf(l_strAudioConfiguration <> "", ", " & l_strAudioConfiguration, "") & IIf(l_strAudioContent <> "", ", " & l_strAudioContent, "") & IIf(l_strLanguage <> "", ", " & l_strLanguage & vbCrLf, vbCrLf)
            If InStr(GetData("tracker_dadc_item", "audioconfiguration", "tracker_dadc_itemID", TrackerID), l_strTotalAudio) > 0 Then
                l_lngCount = InStr(GetData("tracker_dadc_item", "audioconfiguration", "tracker_dadc_itemID", TrackerID), l_strTotalAudio)
                l_strOldReq = Left(GetData("tracker_dadc_item", "audioconfiguration", "tracker_dadc_itemID", TrackerID), l_lngCount)
                l_strOldReq = l_strOldReq & Mid(GetData("tracker_dadc_item", "audioconfiguration", "tracker_dadc_itemID", TrackerID), l_lngCount + Len(l_strTotalAudio))
                If l_strOldReq <> "" Then
                    l_strSQL = "UPDATE tracker_dadc_item SET audioconfiguration = '" & l_strOldReq & "', mdate = '" & FormatSQLDate(Now) & "' WHERE tracker_dadc_itemID = " & TrackerID & ";"
                Else
                    l_strSQL = "UPDATE tracker_dadc_item SET stagefield13 = '" & FormatSQLDate(Now) & "', readytobill = 1, rejected = 0, mdate = '" & FormatSQLDate(Now) & "' WHERE tracker_dadc_itemID = " & TrackerID & ";"
                End If
            Else
                l_strSQL = "UPDATE tracker_dadc_item SET stagefield13 = '" & FormatSQLDate(Now) & "', readytobill = 1, rejected = 0, mdate = '" & FormatSQLDate(Now) & "' WHERE tracker_dadc_itemID = " & TrackerID & ";"
            End If
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            l_strEmailBody3 = l_strEmailBody3 & "Cancelled Item: " & l_strBarcode & vbCrLf
        End If
    ElseIf LCase(l_strDADCStatus) = "available" And l_strBarcode <> "" Then
        TempStr = ""
        TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE workflowID = " & l_lngWorkflowID & " AND companyID = 1608 and readytobill = 0;")
        If Val(TempStr) <> 0 Then
            TrackerID = Val(TempStr)
            l_strSQL = "UPDATE tracker_dadc_item SET stagefield10 = '" & FormatSQLDate(Now) & "', stagefield15 = '" & FormatSQLDate(Now) & "', readytobill = 1, rejected = 0, mdate = '" & FormatSQLDate(Now) & "' WHERE tracker_dadc_itemID = " & TrackerID & ";"
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            l_strEmailBody3 = l_strEmailBody3 & "Completed Item: " & l_strBarcode & vbCrLf
            l_strReference = GetData("tracker_dadc_item", "itemreference", "tracker_dadc_itemID", TrackerID)
            Set FSO = New Scripting.FileSystemObject
            l_lngClipID = Val(Trim(" " & GetDataSQL("SELECT eventID FROM events WHERE clipreference = '" & l_strReference & "' AND system_deleted = 0 AND altlocation LIKE '1608\04_FILES_TO_BE_MOVED%'")))
            If l_lngClipID <> 0 Then
                l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngClipID)
                l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, RequestName, RequesterEmail) VALUES ("
                l_strSQL = l_strSQL & l_lngClipID & ", "
                l_strSQL = l_strSQL & l_lngLibraryID & ", "
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
                l_strSQL = l_strSQL & GetData("setting", "intvalue", "name", "DADC_Store_Archive_ID") & ", "
                l_strSQL = l_strSQL & "'1608\AutoDelete30', "
                l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                l_strPathToFile = GetData("library", "subtitle", "libraryID", l_lngLibraryID) & "\" & GetData("events", "altlocation", "eventID", l_lngClipID)
                l_strPathToFile = l_strPathToFile & "\" & l_strReference & ".xml"
                If FSO.FileExists(l_strPathToFile) Then
                    FSO.DeleteFile (l_strPathToFile)
                End If
            End If
            Set FSO = Nothing
        End If
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Loop

SaveCETASetting "DADCUpdateInProgress", "0"

Dim l_strEmailSubject As String

If l_strEmailBody <> "" Then
    
    If InStr(LCase(g_strConnection), "miniceta") > 0 Then
        l_strEmailSubject = "DADC Manual Ingest Tracker (Latimer) Items Added"
    Else
        l_strEmailSubject = "DADC Manual Ingest Tracker Items Added"
    End If
    
    l_strEmailBody = "The following new items were added to the DBB Manual Ingest Tracker: " & vbCrLf & vbCrLf & l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", 1608) & "' AND trackermessageID = 13;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), l_strEmailSubject, "", l_strEmailBody, True, "", "", g_strAdministratorEmailAddress, False, g_strUserEmailAddress
    
            l_rstWhoToEmail.MoveNext
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

If l_strEmailBody2 <> "" Then
    
    If InStr(LCase(g_strConnection), "miniceta") > 0 Then
        l_strEmailSubject = "DADC Manual Ingest Tracker (Latimer) Rush Jobs"
    Else
        l_strEmailSubject = "DADC Manual Ingest Tracker Rush Jobs"
    End If
    
    l_strEmailBody2 = "The following items were added or changed to Rush Jobs in the DBB Manual Ingest Tracker: " & vbCrLf & vbCrLf & l_strEmailBody2
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", 1608) & "' AND trackermessageID = 13;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), l_strEmailSubject, "", l_strEmailBody2, True, "", "", g_strAdministratorEmailAddress, False, g_strUserEmailAddress
    
            l_rstWhoToEmail.MoveNext
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

If l_strEmailBody3 <> "" Then
    
    If InStr(LCase(g_strConnection), "miniceta") > 0 Then
        l_strEmailSubject = "DADC Manual Ingest Tracker (Latimer) Items Completed or Cancelled"
    Else
        l_strEmailSubject = "DADC Manual Ingest Tracker Items Completed or Cancelled"
    End If
    
    l_strEmailBody3 = "The following items were Completed or cancelled from the DBB Manual Ingest Tracker: " & vbCrLf & vbCrLf & l_strEmailBody3
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", 1608) & "' AND trackermessageID = 13;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), l_strEmailSubject, "", l_strEmailBody3, True, "", "", g_strAdministratorEmailAddress, False, g_strUserEmailAddress
    
            l_rstWhoToEmail.MoveNext
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
MsgBox "Done!"

End Sub

Private Sub cmdNorthernIrelandScreen_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_strSQL As String, l_lngTrackerID As Long, l_lngRunningTime As Long, l_rst As ADODB.Recordset, l_curFileSize As Currency
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strReference As String, l_strSeriesTitle  As String, l_strProgTitle As String, l_strContentInfo As String, l_strHighlight As String, l_strDate As String
Dim l_strTXM As String, l_strRunTime As String, l_strRunTimeProg As String, l_strOriginatingElement As String, l_strSound As String, l_strMultipleCopies As String, l_strOriginalFrom As String
Dim l_lngFirstColumn As Long, l_lngLibraryID As Long

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
oExcel.Visible = True
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets(InputBox("Tab name", "Tab name", "Sheet1"))

l_lngXLRowCounter = Val(InputBox("First Row", "First Row", "2"))
l_lngFirstColumn = Val(InputBox("Number of First Active Column", "Number of First Active Column", "1")) - 1

Do While GetXLData(l_lngXLRowCounter, l_lngFirstColumn + 1) <> ""
    
    l_strReference = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 1)
    l_strOriginalFrom = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 2)
    l_strSeriesTitle = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 3)
    l_strProgTitle = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 4)
    l_strContentInfo = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 5)
    l_strHighlight = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 6)
    l_strDate = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 7)
    l_strTXM = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 8)
    l_strRunTime = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 9)
    l_strRunTimeProg = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 10)
    l_strOriginatingElement = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 11)
    l_strSound = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 12)
    l_strMultipleCopies = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 13)
    
    lblProgress.Caption = "Line: " & l_lngXLRowCounter
    If Trim(" " & GetDataSQL("SELECT TOP 1 tracker_itemID FROM tracker_item WHERE itemreference = '" & QuoteSanitise(l_strReference) & "' and companyID = 1575")) = "" Then
        l_strSQL = "INSERT INTO tracker_item (companyID, itemreference, itemfilename, barcode, headertext10, headertext1, headertext2, headertext3, headertext4, headertext9, headertext5, headertext6, timecodeduration, headertext7, headertext8, headerflag1) VALUES ("
        l_strSQL = l_strSQL & "1575, "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strReference) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strReference) & "_M.mov', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strReference) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strOriginalFrom) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSeriesTitle) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strProgTitle) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strContentInfo) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strHighlight) & "', "
        l_strSQL = l_strSQL & "'" & l_strDate & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTXM) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strRunTime) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strRunTimeProg) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strOriginatingElement) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strMultipleCopies) & "', "
        If LCase(Trim(l_strSound)) = "yes" Then l_strSQL = l_strSQL & "1);" Else l_strSQL = l_strSQL & "0);"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_lngTrackerID = g_lngLastID
        
        l_lngLibraryID = CreateLibraryItem(l_strReference, l_strSeriesTitle, 0, GetNextSequence("InternalReference"), 1575, False, "", l_strProgTitle)
        SetData "Library", "location", "libraryID", l_lngLibraryID, l_strOriginalFrom
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Loop

lblProgress.Caption = ""
Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

MsgBox "Done"

End Sub

Private Sub cmdReadBackUnivesal_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim eventID As Long
Dim SeriesName As String
Dim Title As String
Dim Note As String
Dim SeriesNumber As String
Dim EpisodeNumber As String
Dim Filename As String
Dim SQL As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName <> "" Then
    
    Set oExcel = CreateObject("Excel.Application")
    Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
    Set oWorksheet = oWorkbook.Worksheets("Data")
    
    l_lngXLRowCounter = 1
    
    Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    
        eventID = Val(GetXLData(l_lngXLRowCounter, 1)) 'A
        SeriesName = GetXLData(l_lngXLRowCounter, 3) 'C
        Title = GetXLData(l_lngXLRowCounter, 7) 'G
        Note = GetXLData(l_lngXLRowCounter, 8) 'H
        SeriesNumber = GetXLData(l_lngXLRowCounter, 10) 'J
        EpisodeNumber = GetXLData(l_lngXLRowCounter, 12) 'L
        Filename = GetXLData(l_lngXLRowCounter, 13) 'M
        
        lblProgress.Caption = l_lngXLRowCounter & " - Evenrt:" & eventID & ", Filename: " & Filename
        DoEvents
        
        If eventID <> 0 Then
            If GetData("events", "companyID", "eventID", eventID) = 537 Or GetData("events", "companyID", "eventID", eventID) = 1455 Then
                SetData "events", "eventtitle", "eventID", eventID, QuoteSanitise(SeriesName)
                SetData "events", "eventsubtitle", "eventID", eventID, QuoteSanitise(Title)
                If Trim(" " & GetData("events", "eventversion", "eventID", eventID)) = "" And Note <> "" Then
                    SetData "events", "eventversion", "eventID", eventID, Note
                ElseIf Trim(" " & GetData("events", "eventversion", "eventID", eventID)) <> "" And Note <> "" _
                And Trim(" " & GetData("events", "eventversion", "eventID", eventID)) <> Note Then
                    If MsgBox("Version for " & eventID & " is not empty." & vbCrLf & _
                    "Current value: " & GetData("events", "eventversion", "eventID", eventID) & vbCrLf & _
                    "New Value: " & Note & vbCrLf & _
                    "Proceed", vbYesNo + vbDefaultButton2, "Updating Version Field entry") = vbYes Then
                        SetData "events", "eventversion", "eventID", eventID, Note
                    End If
                End If
                SetData "events", "eventepisode", "eventID", eventID, EpisodeNumber
                SetData "events", "eventseries", "eventID", eventID, SeriesNumber
                SetData "events", "muser", "eventID", eventID, "VDMS Readback"
                SetData "events", "mdate", "eventID", eventID, Format(Now, "YYYY-MM-DD HH:NN:SS")
            Else
                MsgBox "Event: " & eventID & " does not belong to Universal Studios. Line skipped."
            End If
        End If
        l_lngXLRowCounter = l_lngXLRowCounter + 1
    Loop

End If

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

MsgBox "Done"

End Sub

Private Sub cmdReadMattelDataTrackerData_Click()

Dim l_strSQL As String, l_lngTrackerID As Long, l_strExcelFileName As String, l_strExcelFileTitle As String, l_rst As ADODB.Recordset, l_curFileSize As Currency
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strReference As String, l_strTitle  As String, l_strSubTitle As String, l_strBarcode As String
Dim l_lngFirstColumn As Long, l_lngLibraryID As Long

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
oExcel.Visible = True
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets(InputBox("Tab name", "Tab name", "Sheet1"))

l_lngXLRowCounter = Val(InputBox("First Row", "First Row", "2"))
l_lngFirstColumn = Val(InputBox("Number of First Active Column", "Number of First Active Column", "1")) - 1

Do While GetXLData(l_lngXLRowCounter, l_lngFirstColumn + 1) <> ""
    
    l_strBarcode = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 2)
    l_strTitle = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 4)
    l_strSubTitle = GetXLDataText(l_lngXLRowCounter, l_lngFirstColumn + 7)
    
    lblProgress.Caption = "Line: " & l_lngXLRowCounter & ", " & l_strBarcode & ", " & l_strTitle
    DoEvents
    If Trim(" " & GetDataSQL("SELECT TOP 1 tracker_itemID FROM tracker_item WHERE itemreference = '" & QuoteSanitise(l_strBarcode) & "' and companyID = 1103")) = "" Then
        l_strSQL = "INSERT INTO tracker_item (companyID, itemreference, itemfilename, barcode, headertext1, headertext2, headertext3) VALUES ("
        l_strSQL = l_strSQL & "1103, "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strBarcode) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strBarcode) & ".mov', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strBarcode) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSubTitle) & "', "
        l_strSQL = l_strSQL & "'" & GetData("library", "format", "barcode", l_strBarcode) & "');"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    Else
        MsgBox "Duplicate Item Row: " & l_lngXLRowCounter & ", Barcode: " & l_strReference
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Loop

lblProgress.Caption = ""
Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

MsgBox "Done"

End Sub

Private Sub cmdSPESpottersSheet_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strTitle As String, l_strAlphaID As String, l_strTitle_Abbrev As String, l_strOriginalLanguage As String, l_strLanguage As String, l_strMasteringVendor As String, l_strDeadlineDate As String
Dim l_datDeadlineDate As Date, l_strClipReference As String, l_strAspectRatio As String

Dim l_strSQL As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "Cover Sheet", "")
'If l_strExcelSheetName = "" Then
'    oWorkbook.Close
'    Set oWorkbook = Nothing
'    Set oExcel = Nothing
'    Exit Sub
'End If
l_strExcelSheetName = "Cover Sheet"
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_strAlphaID = Trim(GetXLData(10, 5))
l_strTitle = Trim(GetXLData(6, 3))
l_strTitle_Abbrev = Trim(GetXLData(9, 3))
l_strOriginalLanguage = Trim(GetXLData(7, 3))
l_strLanguage = Trim(GetXLData(7, 5))
l_strMasteringVendor = Trim(GetXLData(19, 3))
'l_strDeadlineDate = Trim(GetXLData(14, 5))
l_strClipReference = l_strTitle_Abbrev & "_" & l_strAlphaID
l_strAspectRatio = Trim(GetXLData(19, 5))

If l_strAspectRatio = "16:9 FF & LB" Then
    If GetCount("SELECT tracker_spe_itemID FROM tracker_spe_item WHERE Alpha_ID = " & l_strAlphaID & "AND Language = '" & l_strLanguage & "' AND AspectRatio = '16:9 FF Only'") = 0 Then
        l_strSQL = "INSERT INTO tracker_spe_item (CompanyID, Alpha_ID, clipreference, Title, Title_Abbrev, Original_Language, Language, AspectRatio, Mastering_Vendor, Deadline_Date) VALUES ("
        l_strSQL = l_strSQL & "1473, "
        l_strSQL = l_strSQL & l_strAlphaID & ", "
        l_strSQL = l_strSQL & "'" & l_strClipReference & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle_Abbrev) & "', "
        l_strSQL = l_strSQL & "'" & l_strOriginalLanguage & "', "
        l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
        l_strSQL = l_strSQL & "'16:9 FF Only', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strMasteringVendor) & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_strDeadlineDate) & "')"
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    End If
    If GetCount("SELECT tracker_spe_itemID FROM tracker_spe_item WHERE Alpha_ID = " & l_strAlphaID & "AND Language = '" & l_strLanguage & "' AND AspectRatio = '16:9 LB Only'") = 0 Then
        l_strSQL = "INSERT INTO tracker_spe_item (CompanyID, Alpha_ID, clipreference, Title, Title_Abbrev, Original_Language, Language, AspectRatio, Mastering_Vendor, Deadline_Date) VALUES ("
        l_strSQL = l_strSQL & "1473, "
        l_strSQL = l_strSQL & l_strAlphaID & ", "
        l_strSQL = l_strSQL & "'" & l_strClipReference & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle_Abbrev) & "', "
        l_strSQL = l_strSQL & "'" & l_strOriginalLanguage & "', "
        l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
        l_strSQL = l_strSQL & "'16:9 LB Only', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strMasteringVendor) & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_strDeadlineDate) & "')"
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    End If
Else
    If GetCount("SELECT tracker_spe_itemID FROM tracker_spe_item WHERE Alpha_ID = " & l_strAlphaID & "AND Language = '" & l_strLanguage & "' AND AspectRatio = '" & l_strAspectRatio & "'") = 0 Then
        l_strSQL = "INSERT INTO tracker_spe_item (CompanyID, Alpha_ID, clipreference, Title, Title_Abbrev, Original_Language, Language, AspectRatio, Mastering_Vendor, Deadline_Date) VALUES ("
        l_strSQL = l_strSQL & "1473, "
        l_strSQL = l_strSQL & l_strAlphaID & ", "
        l_strSQL = l_strSQL & "'" & l_strClipReference & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle_Abbrev) & "', "
        l_strSQL = l_strSQL & "'" & l_strOriginalLanguage & "', "
        l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strAspectRatio) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strMasteringVendor) & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_strDeadlineDate) & "')"
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    End If
End If
Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
MsgBox "Done."

End Sub

Private Sub cmdLibraryMovements_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long, l_strShelf As String, l_strBarcode As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

frmMovement.Show
frmMovement.cmbDeliveredBy.Text = g_strFullUserName
frmMovement.cmbLocation.Text = "Library"

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets("Sheet1")

l_lngXLRowCounter = 2

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_strShelf = GetXLData(l_lngXLRowCounter, 1)
    l_strBarcode = GetXLData(l_lngXLRowCounter, 2)
    
    frmMovement.cmbShelf.Text = l_strShelf
    frmMovement.txtBarcode.Text = l_strBarcode
    frmMovement.cmdProcessMovement.Value = True
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Loop

frmMovement.cmdSave.Value = True

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

Beep

End Sub

Private Sub cmdNewDADCIngestRequest_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long, l_lngXLRowCounter2 As Long
Dim l_strBarcode As String, l_strNewBarcode As String, l_strLastBarcode As String, l_strReference As String, l_strTitle As String, l_strSubTitle As String, l_lngEpisode As Long, l_strAlphaBusiness As String
Dim l_lngCRID As Long, l_strFormat As String, l_strLegacyLineStandard As String, l_strImageAspectRatio As String, l_strLegacyPictureElement As String, l_strLegacyFrameRate As String, l_strBBCCoreID As String
Dim l_strLanguage As String, l_strSubtitlesLanguage As String, l_strAudioConfiguration As String, l_strDurationEstimate As String, l_lngLastTrackerID As Long, l_strAudioContent As String
Dim l_strDADCStatus As String, l_datDueDate As Date, l_strDueDate As String, l_strEmailBody As String, l_strEmailBody2 As String, l_strEmailBody3 As String, l_rstWhoToEmail As ADODB.Recordset, l_lngExternalAlphaKey As Long, l_lngWorkflowID As Long
Dim l_lngMonth As Long, l_lngDay As Long, l_lngYear As Long, l_lngCount1 As Long, l_lngCount2 As Long, l_blnDateChange As Boolean, l_datOldDate As Date, l_lngBatchNumber As Long
Dim l_strAdditionalElements As String, l_strContentVersionCode As String, l_lngContentVersionID As Long, l_strRush As String, l_blnRushChanged As Boolean, l_datNewTargetDate As Date
Dim l_rst As ADODB.Recordset, Count2 As Long, l_strVendorGroup As String, l_strTotalAudio As String, l_strMasterSpec As String, TrackerID As Long, l_strIngestRequestor As String, l_strLegacyOracTVAENumber As String
Dim l_lngClipID As Long, FSO As Scripting.FileSystemObject, l_strPathToFile As String, l_lngLibraryID As Long
Dim l_strEPV_ID As String, l_strIngestFilenameToUse As String, l_lngSeriesID As Long, l_strEPVDuration As String
Dim l_strUID As String

Dim l_strSQL As String, TempStr As String, Count As Long, l_strTextInPicture As String, l_strOldReference As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "BBCW Ingest Queue Inventory Rep")
'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "BBCW Ingest Management Report")
'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "BBCW_Ingest_Management_Report")
'l_strExcelSheetName = GetData("setting", "value", "name", "DADC_IR_Tab_Name")
'If l_strExcelSheetName = "" Then
'    oWorkbook.Close
'    Set oWorkbook = Nothing
'    Set oExcel = Nothing
'    Exit Sub
'End If
'Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)
Set oWorksheet = oWorkbook.Worksheets(1)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "DADC Excel Read", GetData("setting", "intvalue", "name", "DADC_IR_FirstRow")))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

l_strLastBarcode = ""
l_strEmailBody = ""
l_strEmailBody2 = ""
l_strEmailBody3 = ""

SaveCETASetting "DADCUpdateInProgress", "1"
l_lngXLRowCounter2 = l_lngXLRowCounter
'A first run through to clear out the requirements string of all currently open rows that are about to be updated.
Do While GetXLData(l_lngXLRowCounter2, 1) <> ""
    l_lngWorkflowID = Val(GetXLData(l_lngXLRowCounter2, 53)) 'BA
    l_strAlphaBusiness = ""
    l_strEPV_ID = Trim(GetXLData(l_lngXLRowCounter2, 14)) 'N
    l_strBarcode = Trim(GetXLData(l_lngXLRowCounter2, 30)) 'AD
    l_strContentVersionCode = ""
    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", l_strBarcode) <> 0 Then
        l_strBarcode = GetData("bbc_barcode_correction", "corrected_barcode", "original_barcode", l_strBarcode)
    End If
    l_strNewBarcode = l_strBarcode
    If UCase(Right(l_strBarcode, 3)) = "FIX" Then
        l_strNewBarcode = Left(l_strBarcode, Len(l_strBarcode) - 4)
    ElseIf UCase(Right(l_strBarcode, 4)) = "REDO" Then
        l_strNewBarcode = Left(l_strBarcode, Len(l_strBarcode) - 5)
    End If
    l_strDADCStatus = Trim(GetXLData(l_lngXLRowCounter2, 3)) 'C
    l_strVendorGroup = Trim(GetXLData(l_lngXLRowCounter2, 76)) 'BX
    
    If LCase(l_strDADCStatus) = "requested" And l_strBarcode <> "" And (InStr(l_strVendorGroup, "MX1") > 0 Or InStr(l_strVendorGroup, "VDMS - London") > 0) Then
        lblProgress.Caption = "Row: " & l_lngXLRowCounter2 & " - " & l_strBarcode & " - Clearing old requirements information"
'        If l_strContentVersionCode = "" Then
            TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE newbarcode='" & UCase(QuoteSanitise(l_strNewBarcode)) & "' AND contentversioncode = '" & l_strEPV_ID & "' AND companyID = 1261 AND workflowID = " & l_lngWorkflowID & ";")
'        Else
'            TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE newbarcode='" & UCase(l_strNewBarcode) & "' AND contentversioncode = '" & l_strContentVersionCode & "' AND companyID = 1261 AND workflowID = " & l_lngWorkflowID & ";")
'        End If
        If Val(TempStr) <> 0 Then
            l_strSQL = "UPDATE tracker_dadc_item SET "
            l_strSQL = l_strSQL & "audioconfiguration = '' "
            
'            If l_strContentVersionCode = "" Then
                l_strSQL = l_strSQL & "WHERE newbarcode = '" & QuoteSanitise(QuoteSanitise(l_strNewBarcode)) & "' AND contentversioncode = '" & l_strEPV_ID & "' AND companyID = 1261 AND workflowID = " & l_lngWorkflowID & ";"
'            Else
'                l_strSQL = l_strSQL & "WHERE newbarcode = '" & QuoteSanitise(l_strNewBarcode) & "' AND contentversioncode = '" & l_strContentVersionCode & "' AND companyID = 1261 AND workflowID = " & l_lngWorkflowID & ";"
'            End If
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
        End If
    End If
    l_lngXLRowCounter2 = l_lngXLRowCounter2 + 1
Loop

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_blnDateChange = False
    l_lngCRID = Val(Trim(GetXLData(l_lngXLRowCounter, 2))) 'B
    l_strBarcode = Trim(GetXLData(l_lngXLRowCounter, 30)) 'AD
    If GetData("bbc_barcode_correction", "bbc_barcode_correctionID", "original_barcode", l_strBarcode) <> 0 Then
        l_strBarcode = GetData("bbc_barcode_correction", "corrected_barcode", "original_barcode", l_strBarcode)
    End If
    l_strNewBarcode = l_strBarcode
    If UCase(Right(l_strBarcode, 3)) = "FIX" Then
        l_strNewBarcode = Left(l_strBarcode, Len(l_strBarcode) - 4)
    ElseIf UCase(Right(l_strBarcode, 4)) = "REDO" Then
        l_strNewBarcode = Left(l_strBarcode, Len(l_strBarcode) - 5)
    End If
    l_strMasterSpec = Trim(GetXLData(l_lngXLRowCounter, 31)) 'AE
    l_strBBCCoreID = Trim(GetXLData(l_lngXLRowCounter, 29)) 'AC
    l_strDADCStatus = Trim(GetXLData(l_lngXLRowCounter, 3)) 'C
    l_strAlphaBusiness = ""
    l_strEPV_ID = Trim(GetXLData(l_lngXLRowCounter, 14)) 'N - the new EPV ID
    l_strEPVDuration = Trim(GetXLDataText(l_lngXLRowCounter, 20)) 'T
    l_strContentVersionCode = ""
    l_lngContentVersionID = 0
    l_strVendorGroup = Trim(GetXLData(l_lngXLRowCounter, 76)) 'BX
    l_lngWorkflowID = Val(GetXLData(l_lngXLRowCounter, 53)) 'BA
    l_strTitle = GetXLData(l_lngXLRowCounter, 10) 'J
    l_lngSeriesID = Val(GetXLData(l_lngXLRowCounter, 6)) 'F
    l_strSubTitle = GetXLData(l_lngXLRowCounter, 13) 'M
    l_lngEpisode = Val(GetXLData(l_lngXLRowCounter, 12)) 'L
    l_strFormat = ""
    l_strLegacyFrameRate = GetXLData(l_lngXLRowCounter, 33) 'AG
    l_strLegacyLineStandard = GetXLData(l_lngXLRowCounter, 34) 'AH
    l_strImageAspectRatio = GetXLData(l_lngXLRowCounter, 37) 'AK
    l_strLegacyPictureElement = ""
    l_strAdditionalElements = GetXLData(l_lngXLRowCounter, 44) 'AR
    l_strLanguage = GetXLData(l_lngXLRowCounter, 47) 'AU
    If l_strLanguage = "None" Then l_strLanguage = ""
    l_strAudioConfiguration = GetAlias(GetXLData(l_lngXLRowCounter, 45)) 'AS
    l_strAudioContent = GetXLData(l_lngXLRowCounter, 46) 'AT
    l_strDurationEstimate = ""
    l_strTextInPicture = Trim(" " & GetXLData(l_lngXLRowCounter, 51)) 'AY
    l_strRush = Trim(" " & GetXLData(l_lngXLRowCounter, 71)) 'BS
    l_strDueDate = GetXLData(l_lngXLRowCounter, 74) 'BV
    l_strIngestRequestor = GetXLData(l_lngXLRowCounter, 65) 'BM
    l_strLegacyOracTVAENumber = GetXLData(l_lngXLRowCounter, 25) 'Y
    l_strIngestFilenameToUse = GetXLData(l_lngXLRowCounter, 79) 'CA
    l_strUID = GetXLData(l_lngXLRowCounter, 80) 'CB
    
    If LCase(l_strDADCStatus) = "requested" And l_strBarcode <> "" And (InStr(l_strVendorGroup, "MX1") > 0 Or InStr(l_strVendorGroup, "VDMS - London") > 0) Then
    
        If l_strDueDate <> "" Then
'            'Code for dwaling with US format dates
'            l_lngCount1 = InStr(l_strDueDate, "/")
'            l_lngDay = Val(Mid(l_strDueDate, l_lngCount1 + 1))
'            l_lngCount2 = InStr(Mid(l_strDueDate, l_lngCount1 + 1), "/")
'            l_lngYear = Val(Mid(Mid(l_strDueDate, l_lngCount1 + 1), l_lngCount2 + 1, 4))
'            l_lngMonth = Val(l_strDueDate)
'            l_datDueDate = l_lngYear & "-" & Format(l_lngMonth, "00") & "-" & Format(l_lngDay, "00")
            'Code for dealing with UK format dates
            If IsDate(l_strDueDate) Then
                l_datDueDate = l_strDueDate
            Else
                MsgBox "Unreadable Due Date - " & l_strDueDate & vbCrLf & "2099-12-12  will be used instead.", vbInformation, "Error reading Excel Sheet"
                l_datDueDate = "2099-12-12"
            End If
        Else
            l_datDueDate = "2099-12-12"
        End If
        l_lngBatchNumber = Format(l_datDueDate, "ddmmyy")
'        l_lngExternalAlphaKey = Val(GetXLData(l_lngXLRowCounter, 30)) 'AD
'        If l_strContentVersionCode <> "" Then l_strAlphaBusiness = ""
'        If l_strContentVersionCode <> "" Then
'            If l_strIngestFilenameToUse = "" Then
'                l_strReference = SanitiseBarcode(l_strBarcode) & "_" & l_lngContentVersionID & "_" & l_lngWorkflowID
'            Else
'                l_strReference = l_strIngestFilenameToUse
'            End If
'        Else
            If l_strIngestFilenameToUse = "" Then
                l_strReference = SanitiseBarcode(l_strBarcode) & "_" & l_strEPV_ID & "_" & l_lngWorkflowID
            Else
                l_strReference = l_strIngestFilenameToUse
            End If
'        End If
        If Not IsDate(l_strDueDate) Then l_strDueDate = ""
        
        lblProgress.Caption = "Row: " & l_lngXLRowCounter & " - " & l_strBarcode & " - " & l_strTitle & " Ep. " & l_lngEpisode
        DoEvents
        
        TempStr = ""
'        If l_strContentVersionCode = "" Then
            TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE newbarcode='" & UCase(QuoteSanitise(l_strNewBarcode)) & "' AND contentversioncode = '" & l_strEPV_ID & "' AND companyID = 1261 AND workflowID = " & l_lngWorkflowID & ";")
'        Else
'            TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE newbarcode='" & UCase(l_strNewBarcode) & "' AND contentversioncode = '" & l_strContentVersionCode & "' AND companyID = 1261 AND workflowID = " & l_lngWorkflowID & ";")
'        End If
        If Val(TempStr) = 0 Then
            TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE newbarcode='" & UCase(QuoteSanitise(l_strNewBarcode)) & "' AND externalalphaID = '" & l_strAlphaBusiness & "' AND companyID = 1239;")
            l_strSQL = "INSERT INTO tracker_dadc_item (cdate, mdate, companyID, IngestRequestor, itemreference, title, series, subtitle, barcode, newbarcode, format, contentversioncode, BBCCoreID, legacylinestandard, legacyframerate, "
            l_strSQL = l_strSQL & "imageaspectratio, legacypictureelement, additionalelementsrequested, language, AudioConfiguration, LegacyOracTVAENumber, "
            l_strSQL = l_strSQL & "subtitleslanguage, fivepointonerequested, dolbyErequested, textinpicture, stereorequested, monorequested, "
            l_strSQL = l_strSQL & "musicandeffectsrequested, episode, dbbcrid, epvduration, priority, targetdatecountdown, duedateupload, contentversionID, workflowID, durationestimate, batch, UID) VALUES ("
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "1261, "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strIngestRequestor) & "', "
            If l_strReference <> "" Then
                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strReference) & "', "
            Else
                l_strSQL = l_strSQL & "NULL, "
            End If
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
            l_strSQL = l_strSQL & IIf(l_lngSeriesID <> 0, l_lngSeriesID, "Null") & ", "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSubTitle) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(UCase(l_strBarcode)) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(UCase(l_strNewBarcode)) & "', "
            l_strSQL = l_strSQL & "'" & l_strFormat & "', "
'            If l_strContentVersionCode = "" Then
                l_strSQL = l_strSQL & "'" & l_strEPV_ID & "', "
'            Else
'                l_strSQL = l_strSQL & "'" & l_strContentVersionCode & "', "
'            End If
            l_strSQL = l_strSQL & "'" & l_strBBCCoreID & "', "
            l_strSQL = l_strSQL & "'" & l_strLegacyLineStandard & "', "
            l_strSQL = l_strSQL & "'" & l_strLegacyFrameRate & "', "
            l_strSQL = l_strSQL & "'" & l_strImageAspectRatio & "', "
            l_strSQL = l_strSQL & "'" & l_strLegacyPictureElement & "', "
            If l_strAdditionalElements = "Yes" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
            l_strTotalAudio = IIf(InStr(l_strMasterSpec, "Muxed") > 0, "", l_strMasterSpec) & IIf(l_strAudioConfiguration <> "", IIf(InStr(l_strMasterSpec, "Muxed") > 0, "", ", ") & l_strAudioConfiguration, "") & IIf(l_strAudioContent <> "", ", " & l_strAudioContent, "") & IIf(l_strLanguage <> "", ", " & l_strLanguage & vbCrLf, vbCrLf)
            l_strSQL = l_strSQL & "'" & l_strTotalAudio & "', "
            l_strSQL = l_strSQL & "'" & l_strLegacyOracTVAENumber & "', "
            l_strSQL = l_strSQL & "'" & l_strSubtitlesLanguage & "', "
            If l_strAudioConfiguration = "5.1" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            If Left(l_strAudioConfiguration, 7) = "Dolby E" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            l_strSQL = l_strSQL & "'" & l_strTextInPicture & "', "
            If l_strAudioConfiguration = "Standard Stereo" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            If l_strAudioConfiguration = "Mono" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            If l_strAudioContent = "M&E" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            l_strSQL = l_strSQL & IIf(l_lngEpisode <> 0, l_lngEpisode, "Null") & ", "
            l_strSQL = l_strSQL & l_lngCRID & ", "
            l_strSQL = l_strSQL & "'" & l_strEPVDuration & "', "
            If UCase(l_strRush) = "YES" Then
                l_strSQL = l_strSQL & "1, 1, "
                l_strEmailBody2 = l_strEmailBody2 & "Rush item Added: " & l_strReference & " - Request ID " & l_lngCRID & ", " & l_strTitle & ", Ep " & l_lngEpisode & ", " & l_strSubTitle & vbCrLf
            Else
                l_strSQL = l_strSQL & "0, 3, "
            End If
            l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDueDate) & "', "
            l_strSQL = l_strSQL & l_lngContentVersionID & ", "
            l_strSQL = l_strSQL & l_lngWorkflowID & ", "
            l_strSQL = l_strSQL & Int(Val(l_strDurationEstimate)) & ", "
            l_strSQL = l_strSQL & l_lngBatchNumber & ", "
            If l_strUID <> "" Then
                l_strSQL = l_strSQL & "'" & l_strUID & "');"
            Else
                l_strSQL = l_strSQL & "Null);"
            End If
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            If Val(TempStr) <> 0 Then
                l_strEmailBody = l_strEmailBody & "Added: " & l_strReference & " - Request ID " & l_lngCRID & ", " & l_strTitle & ", Ep " & l_lngEpisode & ", " & l_strSubTitle & " - already existed for Bulk Ingest" & vbCrLf
            Else
                l_strEmailBody = l_strEmailBody & "Added: " & l_strReference & " - Request ID " & l_lngCRID & ", " & l_strTitle & ", Ep " & l_lngEpisode & ", " & l_strSubTitle & vbCrLf
            End If
        Else
            TrackerID = Val(TempStr)
            l_strOldReference = GetData("tracker_dadc_item", "itemreference", "tracker_dadc_itemID", TrackerID)
            If Trim(" " & GetData("tracker_dadc_item", "duedateupload", "tracker_dadc_itemID", TrackerID)) <> "" Then
                l_datOldDate = CDate(Trim(" " & GetData("tracker_dadc_item", "duedateupload", "tracker_dadc_itemID", TrackerID)))
                If l_datDueDate <> l_datOldDate And (l_datOldDate = "2099-12-12" Or l_datDueDate <> "2099-12-12") Then
                    l_blnDateChange = True
                End If
            Else
                l_blnDateChange = True
            End If
            If (GetData("tracker_dadc_item", "priority", "tracker_dadc_itemID", TrackerID) = 0 And UCase(l_strRush) = "YES") Or _
            (GetData("tracker_dadc_item", "priority", "tracker_dadc_itemID", TrackerID) <> 0 And UCase(l_strRush) <> "YES") Then
                l_blnRushChanged = True
            End If
            If l_blnRushChanged = True Then
                Count = GetData("tracker_dadc_item", "targetdatecountdown", "tracker_dadc_itemID", TrackerID)
                If UCase(l_strRush) = "YES" Then
                    If Count > 1 Then
                        Count = 1 - Count
                        If Trim(" " & GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TrackerID)) <> "" And Trim(" " & GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TrackerID)) <> 0 Then
                            Set l_rst = ExecuteSQL("exec fn_getWorkingDaysBackFromDate @fromdate='" & Format(GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TrackerID), "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & -Count, g_strExecuteError)
                            l_datNewTargetDate = l_rst(0)
                            l_rst.Close
                        End If
                        Count = GetData("tracker_dadc_item", "targetdatecountdown", "tracker_dadc_itemID", TrackerID) + Count
                    End If
                Else
                    Count = Count + 2
                    If Count > 3 Then Count = 3
                    If Trim(" " & GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TrackerID)) <> "" And Trim(" " & GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TrackerID)) <> 0 Then
                        Count2 = Count - GetData("tracker_dadc_item", "targetdatecountdown", "tracker_dadc_itemID", TrackerID)
                        If Count2 > 0 Then
                            Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TrackerID), "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Count, g_strExecuteError)
                            l_datNewTargetDate = l_rst(0)
                            l_rst.Close
                        End If
                    End If
                End If
            End If
            TempStr = ""
            If l_strAlphaBusiness <> "" Then TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE newbarcode='" & UCase(l_strBarcode) & "' AND externalalphaID = '" & l_strAlphaBusiness & "' AND companyID = 1239;")
            l_strSQL = "UPDATE tracker_dadc_item SET "
            If l_strReference <> "" Then l_strSQL = l_strSQL & "itemreference = '" & QuoteSanitise(l_strReference) & "', "
            If l_lngSeriesID <> 0 Then l_strSQL = l_strSQL & "series = " & l_lngSeriesID & ", "
            If l_strImageAspectRatio <> "" Then l_strSQL = l_strSQL & "imageaspectratio = '" & l_strImageAspectRatio & "', "
            If l_strLegacyFrameRate <> "" Then l_strSQL = l_strSQL & "legacyframerate = '" & l_strLegacyFrameRate & "', "
            If l_strLegacyLineStandard <> "" Then l_strSQL = l_strSQL & "legacylinestandard = '" & l_strLegacyLineStandard & "', "
            If l_strIngestRequestor <> "" Then l_strSQL = l_strSQL & "IngestRequestor = '" & QuoteSanitise(l_strIngestRequestor) & "', "
            If l_strEPVDuration <> "" Then l_strSQL = l_strSQL & "EPVDuration = '" & l_strEPVDuration & "', "
            If l_blnDateChange = True Then
                l_strSQL = l_strSQL & "duedateupload = '" & FormatSQLDate(l_datDueDate) & "', "
                l_strSQL = l_strSQL & "batch = " & l_lngBatchNumber & ", "
            End If
            If l_blnRushChanged = True Then
                If UCase(l_strRush) = "YES" Then
                    l_strSQL = l_strSQL & "priority = 1, targetdatecountdown = " & Count & ", "
                    If Trim(" " & GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TempStr)) <> "" And Trim(" " & GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TempStr)) <> 0 Then
                        l_strSQL = l_strSQL & "targetdate = '" & FormatSQLDate(l_datNewTargetDate) & "', "
                    End If
                    l_strEmailBody2 = l_strEmailBody2 & "Item Updated to Rush: " & l_strReference & " - Request ID " & l_lngCRID & ", " & l_strTitle & ", Ep " & l_lngEpisode & ", " & l_strSubTitle & vbCrLf
                Else
                    l_strSQL = l_strSQL & "priority = 0, targetdatecountdown = " & Count & ", "
                    If Trim(" " & GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TempStr)) <> "" And Trim(" " & GetData("tracker_dadc_item", "targetdate", "tracker_dadc_itemID", TempStr)) <> 0 Then
                        l_strSQL = l_strSQL & "targetdate = '" & FormatSQLDate(l_datNewTargetDate) & "', "
                    End If
                End If
            End If
            If l_lngExternalAlphaKey <> 0 Then l_strSQL = l_strSQL & "externalalphakey = " & l_lngExternalAlphaKey & ", "
            If l_lngContentVersionID <> 0 Then l_strSQL = l_strSQL & "contentversionID = " & l_lngContentVersionID & ", "
            If l_strBBCCoreID <> "" Then l_strSQL = l_strSQL & "BBCCoreID = '" & l_strBBCCoreID & "', "
            If l_lngWorkflowID <> 0 Then l_strSQL = l_strSQL & "workflowID = " & l_lngWorkflowID & ", "
            l_strSQL = l_strSQL & "dbbcrid = " & l_lngCRID & ", "
            If l_strAudioConfiguration = "5.1" Then l_strSQL = l_strSQL & "fivepointonerequested = 1, " Else l_strSQL = l_strSQL & "fivepointonerequested = 0, "
            If l_strAudioConfiguration = "Standard Stereo" Then l_strSQL = l_strSQL & "stereorequested = 1, " Else l_strSQL = l_strSQL & "stereorequested = 0, "
            If l_strAudioConfiguration = "Mono" Then l_strSQL = l_strSQL & "monorequested = 1, " Else l_strSQL = l_strSQL & "monorequested = 0, "
            If l_strTextInPicture <> "" Then l_strSQL = l_strSQL & "textinpicture = '" & l_strTextInPicture & "', "
            If Left(l_strAudioConfiguration, 7) = "Dolby E" Then l_strSQL = l_strSQL & "dolbyErequested = 1, " Else l_strSQL = l_strSQL & "dolbyErequested = 0, "
            If l_strAudioContent = "M&E" Then l_strSQL = l_strSQL & "musicandeffectsrequested = 1, " Else l_strSQL = l_strSQL & "musicandeffectsrequested = 0, "
            If l_strAdditionalElements = "Yes" Then l_strSQL = l_strSQL & "additionalelementsrequested = 1, " Else l_strSQL = l_strSQL & "additionalelementsrequested = 0, "
            If l_strLanguage <> "" Then l_strSQL = l_strSQL & "language = '" & l_strLanguage & "', "
            l_strTotalAudio = IIf(InStr(l_strMasterSpec, "Muxed") > 0, "", l_strMasterSpec) & IIf(l_strAudioConfiguration <> "", IIf(InStr(l_strMasterSpec, "Muxed") > 0, "", ", ") & l_strAudioConfiguration, "") & IIf(l_strAudioContent <> "", ", " & l_strAudioContent, "") & IIf(l_strLanguage <> "", ", " & l_strLanguage & vbCrLf, vbCrLf)
            If l_strLegacyOracTVAENumber <> "" Then l_strSQL = l_strSQL & "LegacyOracTVAENumber = '" & l_strLegacyOracTVAENumber & "', "
            If l_strUID <> "" Then
                l_strSQL = l_strSQL & "UID = '" & l_strUID & "', "
            End If
            If InStr(GetData("tracker_dadc_item", "audioconfiguration", "tracker_dadc_itemID", TrackerID), l_strTotalAudio) <= 0 Then
                l_strSQL = l_strSQL & "audioconfiguration = '" & GetData("tracker_dadc_item", "audioconfiguration", "tracker_dadc_itemID", TrackerID) & l_strTotalAudio & "', "
            End If
            l_strSQL = l_strSQL & "mdate = '" & FormatSQLDate(Now) & "' "
            l_strSQL = l_strSQL & "WHERE tracker_DADC_ItemID = " & TrackerID & ";"
'            If l_strAlphaBusiness <> "" Then
'                l_strSQL = l_strSQL & "WHERE newbarcode = '" & QuoteSanitise(l_strNewBarcode) & "' AND externalalphaID = '" & l_strAlphaBusiness & "' AND companyID = 1261 AND workflowID = " & l_lngWorkflowID & ";"
'            Else
'                l_strSQL = l_strSQL & "WHERE newbarcode = '" & QuoteSanitise(l_strNewBarcode) & "' AND contentversioncode = '" & l_strContentVersionCode & "' AND companyID = 1261 AND workflowID = " & l_lngWorkflowID & ";"
'            End If
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            If l_strOldReference <> l_strReference Then
                Set l_rst = ExecuteSQL("SELECT eventID, LibraryID, clipfilename FROM vw_Events_on_DISCSTORE WHERE companyID = 1261 AND system_deleted = 0 and clipreference = '" & l_strOldReference & "'", g_strExecuteError)
                If l_rst.RecordCount > 0 Then
                    l_rst.MoveFirst
                    Do While Not l_rst.EOF
                        l_strSQL = "INSERT INTO event_file_request (eventID, sourcelibraryID, event_file_Request_typeID, NewLibraryID, NewFileName, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
                        l_strSQL = l_strSQL & l_rst("eventID") & ", "
                        l_strSQL = l_strSQL & l_rst("libraryID") & ", "
                        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "File Rename") & ", "
                        l_strSQL = l_strSQL & l_rst("libraryID") & ", "
                        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strReference) & Right(l_rst("clipfilename"), 4) & "', "
                        l_strSQL = l_strSQL & "1, "
                        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                        Debug.Print l_strSQL
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                        l_rst.MoveNext
                    Loop
                End If
                l_rst.Close
            End If
            If l_blnDateChange = True Or l_blnRushChanged = True Then
                l_strEmailBody = l_strEmailBody & "Updated with Date Change or Priority Change: " & l_strReference & " - Request ID " & l_lngCRID & ", " & l_strTitle & ", Ep " & l_lngEpisode & ", " & l_strSubTitle
            Else
                l_strEmailBody = l_strEmailBody & "Updated: " & l_strReference & " - Request ID " & l_lngCRID & ", " & l_strTitle & ", Ep " & l_lngEpisode & ", " & l_strSubTitle
            End If
            If Val(TempStr) <> 0 Then
                l_strEmailBody = l_strEmailBody & " - already existed for Bulk Ingest" & vbCrLf
            Else
                l_strEmailBody = l_strEmailBody & vbCrLf
            End If
        End If
        
        l_strLastBarcode = l_strBarcode
        
    ElseIf LCase(l_strDADCStatus) = "requested" And l_strBarcode <> "" And InStr(l_strVendorGroup, "MX1") <= 0 And InStr(l_strVendorGroup, "VDMS - London") <= 0 Then
        TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE workflowID = " & l_lngWorkflowID & " AND contentversioncode = '" & l_strEPV_ID & "' AND companyID = 1261;")
        If Val(TempStr) <> 0 Then
            l_strSQL = "DELETE FROM tracker_dadc_item WHERE tracker_dadc_itemID = " & TempStr & ";"
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
        End If
    ElseIf LCase(l_strDADCStatus) = "cancelled" And l_strBarcode <> "" Then
        TempStr = ""
        TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE workflowID = " & l_lngWorkflowID & " AND companyID = 1261 and readytobill = 0;")
        If Val(TempStr) <> 0 Then
            TrackerID = Val(TempStr)
            Dim l_lngCount As Long, l_strOldReq As String
            l_strTotalAudio = l_strMasterSpec & IIf(l_strAudioConfiguration <> "", ", " & l_strAudioConfiguration, "") & IIf(l_strAudioContent <> "", ", " & l_strAudioContent, "") & IIf(l_strLanguage <> "", ", " & l_strLanguage & vbCrLf, vbCrLf)
            If InStr(GetData("tracker_dadc_item", "audioconfiguration", "tracker_dadc_itemID", TrackerID), l_strTotalAudio) > 0 Then
                l_lngCount = InStr(GetData("tracker_dadc_item", "audioconfiguration", "tracker_dadc_itemID", TrackerID), l_strTotalAudio)
                l_strOldReq = Left(GetData("tracker_dadc_item", "audioconfiguration", "tracker_dadc_itemID", TrackerID), l_lngCount)
                l_strOldReq = l_strOldReq & Mid(GetData("tracker_dadc_item", "audioconfiguration", "tracker_dadc_itemID", TrackerID), l_lngCount + Len(l_strTotalAudio))
                If l_strOldReq <> "" Then
                    l_strSQL = "UPDATE tracker_dadc_item SET audioconfiguration = '" & l_strOldReq & "', mdate = '" & FormatSQLDate(Now) & "' WHERE tracker_dadc_itemID = " & TrackerID & ";"
                Else
                    l_strSQL = "UPDATE tracker_dadc_item SET stagefield13 = '" & FormatSQLDate(Now) & "', readytobill = 1, rejected = 0, mdate = '" & FormatSQLDate(Now) & "' WHERE tracker_dadc_itemID = " & TrackerID & ";"
                End If
            Else
                l_strSQL = "UPDATE tracker_dadc_item SET stagefield13 = '" & FormatSQLDate(Now) & "', readytobill = 1, rejected = 0, mdate = '" & FormatSQLDate(Now) & "' WHERE tracker_dadc_itemID = " & TrackerID & ";"
            End If
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            l_strEmailBody3 = l_strEmailBody3 & "Cancelled Item: " & l_strBarcode & vbCrLf
        End If
    ElseIf LCase(l_strDADCStatus) = "available" And l_strBarcode <> "" Then
        TempStr = ""
        TempStr = GetDataSQL("SELECT TOP 1 tracker_dadc_itemID FROM tracker_dadc_item WHERE workflowID = " & l_lngWorkflowID & " AND companyID = 1261 and readytobill = 0;")
        If Val(TempStr) <> 0 Then
            TrackerID = Val(TempStr)
            l_strSQL = "UPDATE tracker_dadc_item SET stagefield10 = '" & FormatSQLDate(Now) & "', stagefield15 = '" & FormatSQLDate(Now) & "', readytobill = 1, rejected = 0, mdate = '" & FormatSQLDate(Now) & "' WHERE tracker_dadc_itemID = " & TrackerID & ";"
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            l_strEmailBody3 = l_strEmailBody3 & "Completed Item: " & l_strBarcode & vbCrLf
            l_strReference = GetData("tracker_dadc_item", "itemreference", "tracker_dadc_itemID", TrackerID)
            Set FSO = New Scripting.FileSystemObject
            l_lngClipID = Val(Trim(" " & GetDataSQL("SELECT eventID FROM events WHERE clipreference = '" & l_strReference & "' AND system_deleted = 0 AND altlocation LIKE '1261\04_FILES_TO_BE_MOVED%'")))
            If l_lngClipID <> 0 Then
                l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngClipID)
                l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, RequestName, RequesterEmail) VALUES ("
                l_strSQL = l_strSQL & l_lngClipID & ", "
                l_strSQL = l_strSQL & l_lngLibraryID & ", "
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
                l_strSQL = l_strSQL & GetData("setting", "intvalue", "name", "DADC_Store_Archive_ID") & ", "
                l_strSQL = l_strSQL & "'1261\AutoDelete30', "
                l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                l_strPathToFile = GetData("library", "subtitle", "libraryID", l_lngLibraryID) & "\" & GetData("events", "altlocation", "eventID", l_lngClipID)
                l_strPathToFile = l_strPathToFile & "\" & l_strReference & ".xml"
                If FSO.FileExists(l_strPathToFile) Then
                    FSO.DeleteFile (l_strPathToFile)
                End If
            End If
            Set FSO = Nothing
        End If
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Loop

SaveCETASetting "DADCUpdateInProgress", "0"

Dim l_strEmailSubject As String

If l_strEmailBody <> "" Then
    
    If InStr(LCase(g_strConnection), "miniceta") > 0 Then
        l_strEmailSubject = "DADC Manual Ingest Tracker (Latimer) Items Added"
    Else
        l_strEmailSubject = "DADC Manual Ingest Tracker Items Added"
    End If
    
    l_strEmailBody = "The following new items were added to the DBB Manual Ingest Tracker: " & vbCrLf & vbCrLf & l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", 1261) & "' AND trackermessageID = 13;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), l_strEmailSubject, "", l_strEmailBody, True, "", "", g_strAdministratorEmailAddress, False, g_strUserEmailAddress
    
            l_rstWhoToEmail.MoveNext
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

If l_strEmailBody2 <> "" Then
    
    If InStr(LCase(g_strConnection), "miniceta") > 0 Then
        l_strEmailSubject = "DADC Manual Ingest Tracker (Latimer) Rush Jobs"
    Else
        l_strEmailSubject = "DADC Manual Ingest Tracker Rush Jobs"
    End If
    
    l_strEmailBody2 = "The following items were added or changed to Rush Jobs in the DBB Manual Ingest Tracker: " & vbCrLf & vbCrLf & l_strEmailBody2
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", 1261) & "' AND trackermessageID = 13;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), l_strEmailSubject, "", l_strEmailBody2, True, "", "", g_strAdministratorEmailAddress, False, g_strUserEmailAddress
    
            l_rstWhoToEmail.MoveNext
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

If l_strEmailBody3 <> "" Then
    
    If InStr(LCase(g_strConnection), "miniceta") > 0 Then
        l_strEmailSubject = "DADC Manual Ingest Tracker (Latimer) Items Completed or Cancelled"
    Else
        l_strEmailSubject = "DADC Manual Ingest Tracker Items Completed or Cancelled"
    End If
    
    l_strEmailBody3 = "The following items were Completed or cancelled from the DBB Manual Ingest Tracker: " & vbCrLf & vbCrLf & l_strEmailBody3
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", 1261) & "' AND trackermessageID = 13;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), l_strEmailSubject, "", l_strEmailBody3, True, "", "", g_strAdministratorEmailAddress, False, g_strUserEmailAddress
    
            l_rstWhoToEmail.MoveNext
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
MsgBox "Done!"

End Sub

Private Sub cmdPatheKeywords_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_lngXLRowCounter As Long
Dim l_strExcelSheetName As String, i As Long, l_blnKeepKeywords As Boolean
Dim l_rstClip As ADODB.Recordset, l_strSQL As String
Dim l_strClipReference As String, l_strExp_Title As String, l_strExp_Desc As String, l_strFilm_ID As String, l_strSummary As String, l_strKeyword As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

'open the spreadsheet from the root of C:\

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'Get the sheet name and open it or close down and exit
l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "Worksheet Name", "Pathe YT Data")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "Worksheet Row", 2))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

If MsgBox("Replace Keywords on clip " & l_strClipReference, vbYesNo, "Replace Keywords") = vbYes Then
    l_blnKeepKeywords = False
Else
    l_blnKeepKeywords = True
End If

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    lblProgress.Caption = GetXLData(l_lngXLRowCounter, 1)
    DoEvents
    
    l_strClipReference = GetXLData(l_lngXLRowCounter, 6)
    l_strFilm_ID = GetXLData(l_lngXLRowCounter, 4)
    l_strSummary = GetXLData(l_lngXLRowCounter, 5)
    l_strExp_Title = GetXLData(l_lngXLRowCounter, 2)
    l_strExp_Desc = GetXLData(l_lngXLRowCounter, 3)
    
    If Val(Trim(" " & GetDataSQL("SELECT TOP 1 eventID FROM events WHERE companyID = 905 AND clipreference = '" & l_strClipReference & "';"))) <> 0 Then

        l_strSQL = "UPDATE events SET video_longdescription = '" & QuoteSanitise(l_strSummary) & "', "
        l_strSQL = l_strSQL & "notes = '" & QuoteSanitise(l_strExp_Desc) & "', "
        l_strSQL = l_strSQL & "eventtitle = '" & QuoteSanitise(l_strExp_Title) & "' "
        l_strSQL = l_strSQL & "WHERE companyID = 905 AND clipreference = '" & l_strClipReference & "';"
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    
        If l_blnKeepKeywords = False Then
            l_strSQL = "DELETE FROM eventkeyword WHERE companyID = 905 and clipreference = '" & l_strClipReference & "';"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
        End If
        
        For i = 7 To 71
            l_strKeyword = GetXLData(l_lngXLRowCounter, i)
            If l_strKeyword <> "" Then
                l_strSQL = "INSERT INTO eventkeyword (companyID, clipreference, keywordtext) VALUES ("
                l_strSQL = l_strSQL & "905, "
                l_strSQL = l_strSQL & "'" & l_strClipReference & "', "
                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strKeyword) & "');"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            End If
        Next
    Else
        
        MsgBox "Row - " & GetXLData(l_lngXLRowCounter, 1) & ", reference " & l_strClipReference & " record not found", vbInformation, "No Record"
    
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Loop

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

'Private Sub cmdSvensk_Click()
'
'If Not CheckAccess("/processexcelorders") Then
'    Exit Sub
'End If
'
''have a variable to hold the filename we are going to process
'Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_datRequiredDate As Date, l_datSoundHouseDate As Date, l_datTXDate As Date
'Dim l_strExcelSheetName As String, i As Long
'
'
'l_strExcelFileName = txtExcelFilename.Text
'l_strExcelFileTitle = lblFileTitle.Caption
'
'If l_strExcelFileName = "" Then
'
'    Beep
'    Exit Sub
'
'End If
'
'Dim l_rsTracker As ADODB.Recordset
'Dim l_strTmpDate As String
'
'Dim l_strSQL As String
'
''open the recordsets
'Set l_rsTracker = New ADODB.Recordset
'
'Set l_rsTracker = ExecuteSQL("SELECT * FROM tracker_Svensk_item WHERE tracker_Svensk_itemID = -1", g_strExecuteError)
'CheckForSQLError
'
''free up locks
'DoEvents
'
''open the spreadsheet from the root of C:\
'
'Set oExcel = CreateObject("Excel.Application")
'Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
'
''Get the sheet name and open it or close down and exit
''l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "KidsCo Excel Read", "1")
''If l_strExcelSheetName = "" Then
''    oWorkbook.Close
''    l_rsTracker.Close
''    Exit Sub
''End If
'
'For i = 1 To oExcel.Worksheets.Count
'
'    If oExcel.Worksheets(i).Name <> "" Then
'        lblProgress.Caption = oExcel.Worksheets(i).Name
'        DoEvents
'
'        Set oWorksheet = oWorkbook.Worksheets(oExcel.Worksheets(i).Name)
'
'        'declare lots of variables for taking the values off the spreadsheet
'        Dim l_OrderNumber$, l_ProgrammeTitle$, l_ProgrammeEpisode$, l_Priority$, l_Notes$
'        Dim l_Channel$
'        Dim l_ProgrammeNumber$, l_Version$
'        Dim l_NewJobNumber&
'        Dim l_Supplier$
'
'        l_OrderNumber$ = GetXLData(4, 4)
'
'        If l_OrderNumber$ = "" Then
'            Beep
'            MsgBox "Sheet could not be read", vbCritical, "Problem!"
'            Exit Sub
'        End If
'
'        'go through the spreadsheet and retrieve the values and put them into
'        'variables to be used later. These are from the Order Details section
'        l_ProgrammeTitle$ = GetXLData(6, 4)
'        l_ProgrammeEpisode$ = GetXLData(6, 11)
'        l_ProgrammeNumber$ = GetXLData(9, 16)
'        l_Supplier$ = GetXLData(9, 4)
'        l_Version$ = GetXLData(7, 4)
'        l_Channel$ = GetXLData(12, 15)
'        l_Channel$ = LCase(l_Channel$)
'        l_Channel$ = UCase(Left(l_Channel$, 1)) & Mid(l_Channel$, 2)
'        l_Priority$ = GetXLData(6, 16)
'
'        l_Notes$ = GetXLData(29, 2)
'
'        l_rsTracker.AddNew
'        'l_rsTracker("TXdate") = l_datTXDate
'        l_rsTracker("companyID") = 1262
'
'        If l_ProgrammeTitle$ <> "" Then l_rsTracker("title") = Left$(l_ProgrammeTitle$, 255)
'        If l_Supplier$ <> "" Then l_rsTracker("supplier") = Left(l_Supplier$, 50)
'        If l_ProgrammeEpisode$ <> "" Then
'            If Val(l_ProgrammeEpisode$) <> 0 Then
'                l_rsTracker("episode") = Val(l_ProgrammeEpisode$)
'            Else
'                l_rsTracker("subtitle") = Left(l_ProgrammeEpisode$, 255)
'            End If
'        End If
'
'        If l_ProgrammeNumber$ <> "" Then l_rsTracker("itemreference") = l_ProgrammeNumber$
'        If l_Channel$ <> "" Then
'            l_rsTracker("channel") = l_Channel$
'            Select Case l_Channel$
'                Case "English"
'                    l_rsTracker("englishaudioneeded") = 1
'                    l_rsTracker("frenchaudioneeded") = 0
'                    l_rsTracker("brazillianportaudioneeded") = 0
''                    l_rsTracker("englishsubsneeded") = 1
''                    l_rsTracker("frenchsubsneeded") = 0
''                    l_rsTracker("brazillianportsubsneeded") = 0
'                Case "French"
'                    l_rsTracker("englishaudioneeded") = 0
'                    l_rsTracker("frenchaudioneeded") = 1
'                    l_rsTracker("brazillianportaudioneeded") = 0
''                    l_rsTracker("englishsubsneeded") = 0
''                    l_rsTracker("frenchsubsneeded") = 1
''                    l_rsTracker("brazillianportsubsneeded") = 0
'                Case Else
'                    l_rsTracker("englishaudioneeded") = 0
'                    l_rsTracker("frenchaudioneeded") = 0
'                    l_rsTracker("brazillianportaudioneeded") = 1
' '                   l_rsTracker("englishsubsneeded") = 0
' '                   l_rsTracker("frenchsubsneeded") = 0
' '                   l_rsTracker("brazillianportsubsneeded") = 1
'            End Select
'        End If
'
'        If l_Priority$ = "SEND TO TX WITHIN 24 HOURS OF DELIVERY" Then
'            l_rsTracker("priorityflag") = 1
'        Else
'            l_rsTracker("priorityflag") = 0
'        End If
'
'        l_rsTracker("readyforTX") = 0
'
'        l_rsTracker.Update
'
'        l_strSQL = "INSERT INTO job (createduserID, createduser, createddate, modifieddate, modifieduserID, modifieduser, fd_status, jobtype, joballocation, deadlinedate, despatchdate, projectID, productID, "
'        l_strSQL = l_strSQL & "companyID, companyname, contactID, contactname, title1, title2, orderreference) VALUES ("
'
'        l_strSQL = l_strSQL & "33, "
'        l_strSQL = l_strSQL & "'Excel', "
'        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
'        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
'        l_strSQL = l_strSQL & "33, "
'        l_strSQL = l_strSQL & "'Excel', "
'        l_strSQL = l_strSQL & "'Confirmed', "
'        l_strSQL = l_strSQL & "'Dubbing', "
'        l_strSQL = l_strSQL & "'Regular', "
'        l_strSQL = l_strSQL & "'" & FormatSQLDate(DateAdd("d", 1, Now)) & "', "
'        l_strSQL = l_strSQL & "'" & FormatSQLDate(DateAdd("d", 1, Now)) & "', "
'        l_strSQL = l_strSQL & "0, "
'        l_strSQL = l_strSQL & "0, "
'        l_strSQL = l_strSQL & "1262, "
'        l_strSQL = l_strSQL & "'Svensk Modem (Renault TV)', "
'        l_strSQL = l_strSQL & "2555, "
'        l_strSQL = l_strSQL & "'Nikki Cripps-Harding', "
'        l_strSQL = l_strSQL & "'" & Left(l_ProgrammeTitle$, 50) & "', "
'        If l_ProgrammeEpisode$ <> "" Then
'            If Val(l_ProgrammeEpisode$) <> 0 Then
'                l_strSQL = l_strSQL & "' Ep. " & Val(l_ProgrammeEpisode$) & "', "
'            Else
'                l_strSQL = l_strSQL & "'" & Left(l_ProgrammeEpisode$, 50) & "', "
'            End If
'        Else
'            l_strSQL = l_strSQL & "Null, "
'        End If
'        l_strSQL = l_strSQL & "'" & l_OrderNumber$ & "');"
'        ExecuteSQL l_strSQL, g_strExecuteError
'        CheckForSQLError
'
'        DBEngine.Idle
'        DoEvents
'
'    End If
'
'Next
'
''close the database and recordset
'l_rsTracker.Close: Set l_rsTracker = Nothing
'
'Set oWorksheet = Nothing
'oWorkbook.Close
'Set oWorkbook = Nothing
'oExcel.Quit
'Set oExcel = Nothing
'lblProgress.Caption = ""
'
''Make some clips in advance
''MakeClipInAdvance 65, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "", _
''    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & ".m2v", "clip.dir\media.dir"
''MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "English", _
''    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "ENGA.wav", "clip.dir\media.dir"
''MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "Polish", _
''    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "POLA.wav", "clip.dir\media.dir"
''MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "Hungarian", _
''    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "HUNA.wav", "clip.dir\media.dir"
''MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "Romanian", _
''    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "RONA.wav", "clip.dir\media.dir"
''MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "Turkish", _
''    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "TURA.wav", "clip.dir\media.dir"
''MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "Russian", _
''    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "RUSA.wav", "clip.dir\media.dir"
''MakeClipInAdvance 64, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "", _
''    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & ".mov", "clip.dir"
'
''Rename file into the done folder - commented out during testing.
'Name l_strExcelFileName As "\\Jcaserver\ceta\Svensk Processed\" & l_strExcelFileTitle
'
'MsgBox "File Sucessfully Read", vbInformation, "Excel Read"
'
'End Sub
'
Private Sub cmdSelectFile_Click()

'Select the file using MDIform's common dialog box.

MDIForm1.dlgMain.InitDir = g_strExcelOrderLocation
MDIForm1.dlgMain.Filter = "All Files|*.*"
MDIForm1.dlgMain.ShowOpen

txtExcelFilename.Text = MDIForm1.dlgMain.Filename
lblFileTitle.Caption = MDIForm1.dlgMain.FileTitle

End Sub

Private Sub cmdUniversityOfLeeds_Click()

'have a variable to hold the filename we are going to process
Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub
    
End If

'open the spreadsheet

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "Univ. of Leeds Excel Read", "metadata to import")

Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "Univ. of Leeds Excel Read", 2))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

Dim l_strBarcode As String, l_strSeriesTitle As String, l_strAssignedME As String, l_strAssignedSeries As String, l_strTXDate As String, l_strProgSubj As String
Dim l_strNotes As String, l_strRecDate As String, l_strMediaType As String, l_strStatus As String, l_strContent As String
Dim l_datTXDate As Date, l_datRecDate As Date
Dim l_strSQL As String, l_lngLibraryID As Long

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_strBarcode = Trim(GetXLData(l_lngXLRowCounter, 1))
    l_strSeriesTitle = Trim(GetXLData(l_lngXLRowCounter, 5))
    l_strAssignedME = Trim(GetXLData(l_lngXLRowCounter, 6))
    l_strAssignedSeries = Trim(GetXLData(l_lngXLRowCounter, 7))
    l_strTXDate = Trim(GetXLData(l_lngXLRowCounter, 8))
    l_strProgSubj = Trim(GetXLData(l_lngXLRowCounter, 9))
    l_strNotes = Trim(GetXLData(l_lngXLRowCounter, 10))
    l_strRecDate = Trim(GetXLData(l_lngXLRowCounter, 12))
    l_strMediaType = GetAlias(Trim(GetXLData(l_lngXLRowCounter, 13)))
    l_strStatus = Trim(GetXLData(l_lngXLRowCounter, 15))
    l_strContent = Trim(GetXLData(l_lngXLRowCounter, 16))
    
    If IsDate(l_strTXDate) Then l_datTXDate = CDate(l_strTXDate) Else l_datTXDate = 0
    If IsDate(l_strRecDate) Then l_datRecDate = CDate(l_strRecDate) Else l_datRecDate = 0

    lblProgress.Caption = l_lngXLRowCounter & " - " & l_strBarcode
    DoEvents
    
    l_lngLibraryID = GetData("library", "libraryID", "barcode", l_strBarcode)
    If l_lngLibraryID = 0 Then
        l_lngLibraryID = CreateLibraryItem(l_strBarcode, l_strSeriesTitle, 0, 0, 1549)
        SetData "library", "subtitle", "libraryID", l_lngLibraryID, l_strProgSubj
        SetData "library", "series", "libraryID", l_lngLibraryID, l_strAssignedSeries
        SetData "library", "format", "libraryID", l_lngLibraryID, l_strMediaType
        SetData "library", "location", "libraryID", l_lngLibraryID, "OFF SITE"
    End If
    
    If GetDataSQL("SELECT TOP 1 tracker_itemID FROM tracker_item WHERE companyID = 1549 AND barcode = '" & l_strBarcode & "'") = "" Then
        l_strSQL = "INSERT INTO tracker_item (companyID, itemreference, itemfilename, barcode, headertext1, headerint1, headerint2, headertext2, headertext3, headerdate1, headerdate2, headerflag1, headerflag2, headertext4, headertext5, headertext6, cuser, muser) VALUES ("
        l_strSQL = l_strSQL & "1549, "
        l_strSQL = l_strSQL & "'" & l_strBarcode & "', "
        l_strSQL = l_strSQL & "'" & l_strBarcode & ".mov', "
        l_strSQL = l_strSQL & "'" & l_strBarcode & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSeriesTitle) & "', "
        l_strSQL = l_strSQL & Val(l_strAssignedME) & ", "
        l_strSQL = l_strSQL & Val(l_strAssignedSeries) & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strProgSubj) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNotes) & "', "
        l_strSQL = l_strSQL & IIf(l_datTXDate <> 0, "'" & Format(l_datTXDate, "yyyy-mm-dd") & "'", "Null") & ", "
        l_strSQL = l_strSQL & IIf(l_datRecDate <> 0, "'" & Format(l_datRecDate, "yyyy-mm-dd") & "'", "Null") & ", "
        l_strSQL = l_strSQL & IIf(InStr(l_strMediaType, "FILM") > 0, 1, 0) & ", "
        l_strSQL = l_strSQL & IIf(InStr(l_strMediaType, "FILM") <= 0, 1, 0) & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strStatus) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strContent) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strMediaType) & "', "
        l_strSQL = l_strSQL & "'" & g_strUserInitials & "', '" & g_strUserInitials & "'); "
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Loop

lblProgress.Caption = ""

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

MsgBox "Done"

End Sub

Private Sub cmdTradBBCWW_Click()

If Not CheckAccess("/processexcelorders") Then
    Exit Sub
End If


'emtpy the comma count field
m_lngCommacount = 0

'have a variable to hold the filename we are going to process
Dim l_strExcelFileName As String, l_strExcelFileTitle As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Dim l_rsJobs As ADODB.Recordset, l_rsDetails As ADODB.Recordset, l_rsLib As ADODB.Recordset, l_rsHistory As ADODB.Recordset, l_rsxJobs As ADODB.Recordset
Dim l_rsxDetails As ADODB.Recordset, l_lngLibraryID As Long

Dim l_strSQL As String, l_lngdetailID As Long
            
'open the recordsets
Set l_rsJobs = New ADODB.Recordset
Set l_rsDetails = New ADODB.Recordset
Set l_rsxJobs = New ADODB.Recordset
Set l_rsxDetails = New ADODB.Recordset
Set l_rsLib = New ADODB.Recordset
Set l_rsHistory = New ADODB.Recordset

Set l_rsJobs = ExecuteSQL("SELECT * FROM [job] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError
Set l_rsDetails = ExecuteSQL("SELECT * FROM [jobdetail] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError
Set l_rsxJobs = ExecuteSQL("SELECT * FROM [extendedjob] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError
Set l_rsxDetails = ExecuteSQL("SELECT * FROM [extendedjobdetail] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError

'free up locks
DoEvents

'open the spreadsheet from the root of C:\

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets("sheet1")

'get the fields and place them into variables
Dim l_BookedBy$, l_AccountNumber$, l_Telephone$, l_FACNumber$, l_jcacontact$, l_ClientName$, l_ContactName$, l_BookingNumber$
Dim l_Lastprogrammetitle$

'get the header details, i.e. Client name, etc.
l_AccountNumber$ = GetXLData(g_cntAccountNumberRow%, g_cntAccountNumberColumn%)
l_Telephone$ = GetXLData(g_cntTelephoneRow%, g_cntTelephoneColumn%)
l_ClientName$ = "BBC Worldwide"
l_ContactName$ = GetXLData(5, 6)
l_FACNumber$ = GetXLData(3, 2)
l_FACNumber$ = Mid(l_FACNumber$, 16, Len(l_FACNumber$))
l_jcacontact$ = GetXLData(7, 2)


'declare a variable for keeping track of how far down the page we are
Dim l_CurrentTop&

'set it to the first load of data
l_CurrentTop& = 10

'declare lots of variables for taking the values off the spreadsheet
Dim l_OrderNumber$, l_CustomerName$, l_ChargeCode$, l_BusinessAreaCode$, l_NominalCode$, l_ProgrammeTitle$, l_Requireddate$, l_Notes$
Dim l_MasterEpisodeNumber$, l_MasterSpoolNumber$, l_MasterLanguage$, l_MasterSpoolDuration$, l_MasterAspectRatio$, l_MasterTapeFormat$, l_MasterLineStandard$
Dim l_DupeEpisodeNumber$, l_DupeAudio$, l_DupeCeefax$, l_DupeTotalDuration$, l_DupeLanguage$, l_DupeQuantity$, l_DupeTapeFormat$, l_DupeLineStandard$, l_DupeAspectRatio$
Dim l_LastorderNumber$, l_PreviousJobNumber&, l_Original_MasterStandard$, l_Original_DupeStandard$, l_Saveflag%, l_Stk&, l_CalculatedDuration&, l_Mastercount&
Dim l_Orig_masterspool$, l_Orig_masterformat$, l_Orig_masterstandard$, l_Orig_masteraspect$, l_Orig_masterduration$, l_comma&, l_IndividualFAC$, l_ProgrammeNumber$
Dim l_CountryCode$, l_BusinessArea$, l_OldMasterSpoolNumber$, l_NewJobNumber&, l_BBCMasterTapeFormat$, l_BBCDupeTapeFormat$
Dim l_EpisodeCount As Long, l_CopyCount As Long, l_OrigCopyCount As Long

l_LastorderNumber$ = "CETA"
l_Saveflag% = False


'declare the Sound channels
Dim l_Sound1$, l_Sound2$, l_Sound3$, l_Sound4$, l_Sound5$, l_Sound6$, l_Sound7$, l_Sound8$

'start a loop that cycles through each tenth line
l_EpisodeCount = 0
l_CopyCount = 1
Do
    
    'if there is no data in the Bookingnumber field then this is either one of those blue lines or the end of the order.
    'Check which and then exit the do loop if necessary. While there, count the number of episodes in the current group,
    'and enter the data for the last one, rather than any others.
    
    If GetXLData(l_CurrentTop&, 2) = "" Then
        l_CurrentTop& = l_CurrentTop& + 2
        l_EpisodeCount = 0
        l_CopyCount = 1
        l_MasterSpoolNumber$ = ""
        l_OldMasterSpoolNumber$ = "GINGER"
        If GetXLData(l_CurrentTop&, 2) = "" Then Exit Do
    End If
    
    Do While GetXLData(l_CurrentTop&, 2) <> ""
        l_EpisodeCount = l_EpisodeCount + 1
        l_MasterSpoolNumber$ = GetXLData(l_CurrentTop& + 2, 4)
        If l_MasterSpoolNumber$ = l_OldMasterSpoolNumber$ Then
            'We've just found another different copy from the same master:
            l_EpisodeCount = l_EpisodeCount - 1
            l_CopyCount = l_CopyCount + 1
        Else
            'Set the old Spool number for checking the next set isn't the same master spool
            l_OldMasterSpoolNumber$ = l_MasterSpoolNumber$
        End If
        'increment the looper so we move to the next section - when we fall out of this loop we should be sitting on the last Ep in the group
        l_CurrentTop& = l_CurrentTop& + 11
    Loop
    
    l_CurrentTop& = l_CurrentTop& - 11
    
    l_Mastercount& = 1
    l_BookingNumber$ = GetXLData(l_CurrentTop&, 2)
    
    If l_BookingNumber$ = "" Then Exit Do
    
    l_MasterSpoolNumber$ = GetXLData(l_CurrentTop& + 2, 4)
    
    'here is where we work out if there is two masters on this sheet, so check for a comma
    If InStr(l_MasterSpoolNumber$, ",") <> 0 Then
    
        'There's a comma in the master spoolnumbers line. This means we're dealing with multiple masters, and need to pick them apart.
        
        l_Mastercount& = Countcommas(l_MasterSpoolNumber$) + 1
        
    End If
        
    l_Orig_masterspool$ = l_MasterSpoolNumber$
    l_Orig_masterformat$ = GetXLData(l_CurrentTop& + 4, 4)
    l_Orig_masterstandard$ = GetXLData(l_CurrentTop& + 5, 4)
    l_Orig_masterduration$ = GetXLData(l_CurrentTop& + 3, 4)
    l_Orig_masteraspect$ = GetXLData(l_CurrentTop& + 6, 4)

    'go through the spreadsheet and retrieve the values and put them into
    'variables to be used later. These are from the Order Details section
    l_OrderNumber$ = GetXLData(l_CurrentTop& + 1, 2)
    l_IndividualFAC$ = GetXLData(l_CurrentTop& + 0, 2)
    l_CustomerName$ = GetXLData(l_CurrentTop& + 2, 2)
    l_ChargeCode$ = GetXLData(l_CurrentTop& + 3, 2)
    l_BusinessArea$ = GetXLData(l_CurrentTop& + 4, 2)
    l_BusinessAreaCode$ = GetXLData(l_CurrentTop& + 5, 2)
    l_NominalCode$ = GetXLData(l_CurrentTop& + 6, 2)
    l_ProgrammeTitle$ = GetXLData(l_CurrentTop& + 7, 2)
    l_ProgrammeNumber$ = GetXLData(l_CurrentTop& + 8, 2)
    l_CountryCode$ = GetXLData(l_CurrentTop& + 8, 4)
    l_Requireddate$ = GetXLData(l_CurrentTop& + 9, 2)
    l_Notes$ = GetXLData(l_CurrentTop& + 10, 2)
    
    'get the next chapter, which is Master Material
    l_MasterEpisodeNumber$ = GetXLData(l_CurrentTop& + 1, 4)
    
    If l_Mastercount& = 1 Then
        l_MasterSpoolDuration$ = GetXLData(l_CurrentTop& + 3, 4)
        l_MasterTapeFormat$ = GetAlias(GetXLData(l_CurrentTop& + 4, 4))
        l_MasterLineStandard$ = GetAlias(GetXLData(l_CurrentTop& + 5, 4))
        l_MasterAspectRatio$ = GetAlias(GetXLData(l_CurrentTop& + 6, 4))
    End If
    
    l_MasterLanguage$ = GetAlias(GetXLData(l_CurrentTop& + 7, 4))
    
    'increment the looper so we move to the next section  or empty line.
    l_CurrentTop& = l_CurrentTop& + 11
        
    If UCase(l_OrderNumber$) <> UCase(l_LastorderNumber$) Then
                    
        If l_LastorderNumber$ <> "STAR ORDER" Then
        
            'add a new record to the Jobs table
            l_rsJobs.AddNew
            l_rsJobs("createduserID") = 33
            l_rsJobs("createduser") = "Excel"
            l_rsJobs("createddate") = Now
            l_rsJobs("modifieddate") = Now
            l_rsJobs("modifieduserID") = 33
            l_rsJobs("modifieduser") = l_jcacontact$
            l_rsJobs("flagemail") = 1
            l_rsJobs("fd_status") = "Confirmed"
            l_rsJobs("jobtype") = "Dubbing"
            l_rsJobs("joballocation") = "Regular"
            l_rsJobs("deadlinedate") = Format(l_Requireddate$, "dd/mm/yyyy")
            l_rsJobs("despatchdate") = Format(l_Requireddate$, "dd/mm/yyyy")
            l_rsJobs("projectID") = 0
            l_rsJobs("productID") = 0
            If l_Telephone$ <> "" Then l_rsJobs("telephone") = Left$(l_Telephone$, 50)
'            Dim l_NewJobNumber&
'            l_NewJobNumber& = GetNextJobNumber()
'            l_PreviousJobNumber& = l_NewJobNumber&
'
'            l_rsJobs("job number") = l_NewJobNumber&
            l_rsJobs("companyID") = 570
            l_rsJobs("companyname") = "BBC Worldwide Int Ops"
            
            'parse the contactname to get the CETA contact ID and name.
            If l_ContactName$ <> "" Then
                l_rsJobs("contactname") = l_ContactName$
                l_rsJobs("contactID") = GetContactID(l_ContactName$)
            End If
            
            If l_ProgrammeTitle$ <> "" Then l_rsJobs("title1") = Left$(l_ProgrammeTitle$, 50)
            l_Lastprogrammetitle$ = UCase(l_ProgrammeTitle$)
            If l_MasterEpisodeNumber$ <> "" Then l_rsJobs("title2") = Left$(l_MasterEpisodeNumber$, 50)
            If l_Notes$ <> "" Then l_rsJobs("notes2") = "PLEASE CHECK ORIGINAL WW ORDER SHEETS FOR JOB DETAILS. " & l_Notes$
            
            'check whether this is a STAR order or a normal one.
            
            If l_OrderNumber$ Like "STAR*" Then
                l_rsJobs("orderreference") = "STAR ORDER"
                l_LastorderNumber$ = "STAR ORDER"
                l_rsJobs("title1") = "Various Titles - Star Order"
            Else
                If l_OrderNumber$ <> "" Then l_rsJobs("orderreference") = UCase(l_OrderNumber$)
                l_LastorderNumber$ = UCase(l_OrderNumber$)
            End If
                
            l_rsJobs.Update
            
            
            'this part here!
            l_rsJobs.Bookmark = l_rsJobs.Bookmark
            l_NewJobNumber& = l_rsJobs("JobID")
            l_PreviousJobNumber& = l_NewJobNumber&
            
            'Set the projectnumber to be the job ID - new CETA feature.
            l_rsJobs("projectnumber") = l_rsJobs("JobID")
            l_rsJobs.Update
                        
            DBEngine.Idle
            DoEvents
            
            If l_NewJobNumber& <> 0 Then
                
                l_rsxJobs.AddNew
                l_rsxJobs("JobID") = l_NewJobNumber&
                l_rsxJobs("BBCStarJob") = 1
                l_rsxJobs("BBCBookingnumber") = l_FACNumber$
                If l_CustomerName$ <> "" Then l_rsxJobs("bbccustomername") = l_CustomerName$
                If l_ChargeCode$ <> "" Then l_rsxJobs("bbcchargecode") = l_ChargeCode$
                If l_BusinessArea$ <> "" Then l_rsxJobs("bbcbusinessarea") = l_BusinessArea$
                If l_BusinessAreaCode$ <> "" Then l_rsxJobs("bbcbusinessareacode") = l_BusinessAreaCode$
                If l_NominalCode$ <> "" Then l_rsxJobs("bbcnominalcode") = l_NominalCode$
                If l_CountryCode$ <> "" Then l_rsxJobs("BBCCountryCode") = l_CountryCode$
                l_rsxJobs.Update
                
            End If
            
            DBEngine.Idle
            DoEvents
            
        End If
        
    Else
        'make sure the title program stays the same all the way
        If UCase(l_ProgrammeTitle$) <> UCase(l_Lastprogrammetitle$) Then
            'need to update the other job to change the title
            l_rsJobs.Close
            Set l_rsJobs = ExecuteSQL("Select [title1] from [Job] where [jobID] = " & l_PreviousJobNumber&, g_strExecuteError)
            CheckForSQLError
                        
            If l_rsJobs.RecordCount <> 0 Then
                l_rsJobs("title1") = "Various Titles - See Below"
                l_rsJobs.Update
                l_rsJobs.Close
                DBEngine.Idle
                DoEvents
                Set l_rsJobs = ExecuteSQL("SELECT * FROM [Job] WHERE [JobID] = -1", g_strExecuteError)
                CheckForSQLError
            End If
        End If
    End If

    DBEngine.Idle: DoEvents
    
    'The master detail line(s) get added next
    Do While l_Mastercount& > 0
        
        'Get next master into normal variables if there is more than 1, trim the original variables and decrease the mastercount.
        
        If l_Mastercount > 1 Then
            l_comma& = InStr(l_Orig_masterspool$, ",")
            l_MasterSpoolNumber$ = Left(l_Orig_masterspool$, l_comma& - 1)
            l_Orig_masterspool$ = Mid(l_Orig_masterspool$, l_comma& + 1, Len(l_Orig_masterspool$))
            l_comma& = InStr(l_Orig_masterformat$, ",")
            l_BBCMasterTapeFormat$ = Left(l_Orig_masterformat$, l_comma& - 1)
            l_MasterTapeFormat$ = GetAlias(l_BBCMasterTapeFormat$)
            l_Orig_masterformat$ = Mid(l_Orig_masterformat$, l_comma + 1, Len(l_Orig_masterformat$))
            l_comma& = InStr(l_Orig_masterstandard$, ",")
            l_MasterLineStandard$ = GetAlias(Left(l_Orig_masterstandard$, l_comma - 1))
            l_Orig_masterstandard$ = Mid(l_Orig_masterstandard$, l_comma + 1, Len(l_Orig_masterstandard$))
            l_comma& = InStr(l_Orig_masteraspect$, ",")
            If l_comma > 0 Then
                l_MasterAspectRatio$ = GetAlias(Left(l_Orig_masteraspect$, l_comma - 1))
                l_Orig_masteraspect$ = Mid(l_Orig_masteraspect$, l_comma + 1, Len(l_Orig_masteraspect$))
            Else
                l_MasterAspectRatio$ = GetAlias(l_Orig_masteraspect$)
            End If
            l_comma& = InStr(l_Orig_masterduration$, ",")
            l_MasterSpoolDuration$ = Left(l_Orig_masterduration$, l_comma - 1)
            l_Orig_masterduration$ = Mid(l_Orig_masterduration$, l_comma + 1, Len(l_Orig_masterduration$))
        End If
        l_Mastercount& = l_Mastercount& - 1
        
        'process that master
        l_rsDetails.AddNew
        l_rsDetails("jobID") = l_NewJobNumber&
        If l_MasterSpoolNumber$ <> "" Then l_rsDetails("description") = Left(l_ProgrammeTitle$, 50)
        'check if we know about this tape in the library and get details if we do.
        'otherwise use the details they provide on the excel sheet.
        l_strSQL = "SELECT * FROM Library WHERE Barcode = '" & l_MasterSpoolNumber$ & "'"
        Set l_rsLib = ExecuteSQL(l_strSQL, g_strExecuteError)
        If Not l_rsLib.EOF Then
            If l_rsLib("format") <> "" Then
                l_rsDetails("format") = Left(l_rsLib("format"), 50)
                If l_rsDetails("format") Like "HI8DAT*" Then l_rsDetails("format") = "HI8DAT"
                If l_rsDetails("format") Like "DAT*" Then l_rsDetails("format") = "DAT"
            ElseIf l_MasterTapeFormat$ <> "" Then
                l_rsDetails("format") = l_MasterTapeFormat$
            End If
            l_rsDetails("copytype") = "M"
            l_rsDetails("quantity") = l_EpisodeCount
            l_rsDetails("videostandard") = Left(l_rsLib("videostandard"), 10)
            If l_rsLib("videostandard") <> "" Then l_MasterLineStandard$ = Left(l_rsLib("videostandard"), 10)
            If l_rsLib("AspectRatio") <> "" Then
                l_rsDetails("aspectratio") = Left((l_rsLib("AspectRatio") & l_rsLib("GeometricLinearity")), 50)
                l_MasterAspectRatio$ = l_rsDetails("aspectratio")
            Else
                If l_MasterAspectRatio$ <> "" Then
                    l_rsDetails("aspectratio") = l_MasterAspectRatio$
                Else
                    If l_Orig_masterstandard$ Like "*16:9 WI*" Then
                        l_rsDetails("aspectratio") = "16:9AN"
                        l_MasterAspectRatio$ = "16:9AN"
                    End If
                    If l_Orig_masterstandard$ Like "*16:9 LE*" Then
                        l_rsDetails("aspectratio") = "16:9LE"
                        l_MasterAspectRatio$ = "16:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*15:9*" Then
                        l_rsDetails("aspectratio") = "15:9LE"
                        l_MasterAspectRatio$ = "15:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*14:9*" Then
                        l_rsDetails("aspectratio") = "14:9LE"
                        l_MasterAspectRatio$ = "14:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*4:3*" Then
                        l_rsDetails("aspectratio") = "4:3NOR"
                        l_MasterAspectRatio$ = "4:3NOR"
                    End If
                End If
            End If
            If l_rsLib("Totalduration") <> "" Then
                l_CalculatedDuration& = 60 * Val(Left(l_rsLib("Totalduration"), 2)) + Val(Mid(l_rsLib("totalduration"), 4, 2))
                If Val(Mid(l_rsLib("totalduration"), 7, 2)) > 0 Then l_CalculatedDuration& = l_CalculatedDuration& + 1
            End If
            If l_CalculatedDuration& > 0 Then
                l_rsDetails("runningtime") = l_CalculatedDuration&
                l_MasterSpoolDuration$ = Str(l_CalculatedDuration&)
            Else
                l_rsDetails("runningtime") = Int(Val(l_MasterSpoolDuration$))
            End If
            If l_rsLib("ch1") <> "" Then
                l_rsDetails("sound1") = Process_Sound(l_rsLib("ch1"))
            Else
                l_rsDetails("sound1") = Left(l_rsLib("ch1"), 50)
            End If
            If l_rsLib("ch2") <> "" Then
                l_rsDetails("sound2") = Process_Sound(l_rsLib("ch2"))
            Else
                l_rsDetails("sound2") = Left(l_rsLib("ch2"), 50)
            End If
            If l_rsLib("ch3") <> "" Then
                l_rsDetails("sound3") = Process_Sound(l_rsLib("ch3"))
            Else
                l_rsDetails("sound3") = Left(l_rsLib("ch3"), 50)
            End If
            If l_rsLib("ch4") <> "" Then
                l_rsDetails("sound4") = Process_Sound(l_rsLib("ch4"))
            Else
                l_rsDetails("sound4") = Left(l_rsLib("ch4"), 50)
            End If
        Else
            'As well as loading up the job detail line from the provided data, we create a new tape entry in the library.
            'Well - we used to - I'm commenting all that code out, because we're not sure its helpful to do that any more.
'            l_rsLib.AddNew
'            l_rsLib("barcode") = l_MasterSpoolNumber$
'            l_rsLib("title") = l_ProgrammeTitle$
'            l_rsLib("subtitle") = l_DupeEpisodeNumber$
'            l_rsLib("companyID") = 287
'            l_rsLib("companyname") = "BBC Worldwide Ltd"
'            l_rsLib("location") = "OFF SITE"
            If l_MasterTapeFormat$ <> "" Then
'                l_rsLib("format") = l_MasterTapeFormat$
                l_rsDetails("format") = l_MasterTapeFormat$
            End If
'            l_rsLib("copytype") = "MASTER"
'            l_rsLib("version") = "MASTER"
            l_rsDetails("copytype") = "M"
            l_rsDetails("quantity") = l_EpisodeCount
            If l_MasterLineStandard$ = "ASK" Then
                l_MasterLineStandard$ = "625PAL"
                l_rsDetails("videoStandard") = "625PAL"
'                l_rsLib("videostandard") = "625PAL"
            Else
                If l_MasterLineStandard$ <> "" Then
                    l_rsDetails("videostandard") = Left(l_MasterLineStandard$, 10)
'                    l_rsLib("videostandard") = Left(l_MasterLineStandard$, 10)
                End If
            End If
            If l_MasterAspectRatio$ <> "" Then
                l_rsDetails("aspectratio") = l_MasterAspectRatio$
            Else
                If l_Orig_masterstandard$ Like "*16:9 WI*" Then
                    l_rsDetails("aspectratio") = "16:9AN"
                    l_MasterAspectRatio$ = "16:9AN"
'                    l_rsLib("aspectratio") = "16:9"
'                    l_rsLib("geometriclinearity") = "ANAMORPHIC"
                End If
                If l_Orig_masterstandard$ Like "*16:9 LE*" Then
                    l_rsDetails("aspectratio") = "16:9LE"
                    l_MasterAspectRatio$ = "16:9LE"
'                    l_rsLib("aspectratio") = "16:9"
'                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*15:9*" Then
                    l_rsDetails("aspectratio") = "15:9LE"
                    l_MasterAspectRatio$ = "15:9LE"
'                    l_rsLib("aspectratio") = "15:9"
'                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*14:9*" Then
                    l_rsDetails("aspectratio") = "14:9LE"
                    l_MasterAspectRatio$ = "14:9LE"
'                    l_rsLib("aspectratio") = "14:9"
'                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*4:3*" Then
                    l_rsDetails("aspectratio") = "4:3NOR"
                    l_MasterAspectRatio$ = "4:3NOR"
'                    l_rsLib("aspectratio") = "4:3"
'                    l_rsLib("geometriclinearity") = "NORMAL"
                End If
            End If
            If l_MasterAspectRatio$ = "ASK" Then
                l_rsDetails("aspectratio") = "4:3NOR"
                l_MasterAspectRatio$ = "4:3NOR"
'                l_rsLib("aspectratio") = "4:3"
'                l_rsLib("geometriclinearity") = "NORMAL"
            End If
            l_rsDetails("runningtime") = Int(Val(l_MasterSpoolDuration$))
'            l_rsLib("cuser") = "XLS"
'            l_rsLib("cdate") = Now
'            l_rsLib("muser") = "XLS"
'            l_rsLib("mdate") = Now
'            l_rsLib("code128barcode") = CIA_CODE128(l_MasterSpoolNumber$)
'            l_rsLib.Update
            'Then we make a history entry too.
            'No - we don't any more.
'            l_rsLib.Bookmark = l_rsLib.Bookmark
'            l_lngLibraryID = l_rsLib("libraryID")
'            l_rsHistory.Open "SELECT * FROM [trans] WHERE [libraryID] = -1", l_conn, adOpenKeyset, adLockOptimistic
'            l_rsHistory.AddNew
'            l_rsHistory("libraryID") = l_lngLibraryID
'            l_rsHistory("destination") = "Created"
'            l_rsHistory("cuser") = "XLS"
'            l_rsHistory("cdate") = Now
'            l_rsHistory.Update
'            l_rsHistory.Close
        End If
        l_rsLib.Close
        l_BBCMasterTapeFormat$ = l_MasterTapeFormat$
        If l_rsDetails("format") = "" Then l_rsDetails("format") = "ASK"
        
        l_rsDetails.Update
        
        l_rsDetails.Bookmark = l_rsDetails.Bookmark
        l_lngdetailID = l_rsDetails("jobdetailID")
        l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
        l_rsDetails.Update

        
        l_rsxDetails.AddNew
        l_rsxDetails("jobID") = l_NewJobNumber&
        l_rsxDetails("jobdetailID") = l_lngdetailID
        If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
        If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
        If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
        If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
        If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
        If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
        If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
        If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
        If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
        If l_BBCMasterTapeFormat$ <> "" Then l_rsxDetails("bbcsourceformat") = l_BBCMasterTapeFormat$
        If l_DupeTapeFormat$ <> "" Then l_rsxDetails("bbcRecordFormat") = l_DupeTapeFormat$
        l_rsxDetails.Update
                
        'update the variables if the mastercount is currentrly 1
        If l_Mastercount& = 1 Then
            l_BBCMasterTapeFormat$ = l_Orig_masterformat$
            l_MasterTapeFormat$ = GetAlias(l_BBCMasterTapeFormat$)
            l_MasterSpoolNumber$ = l_Orig_masterspool$
            l_MasterLineStandard$ = GetAlias(l_Orig_masterstandard$)
            l_MasterAspectRatio$ = GetAlias(l_Orig_masteraspect$)
            l_MasterSpoolDuration$ = l_Orig_masterduration$
        End If
    Loop
    
    'Then the copy detail line gets added
    l_OrigCopyCount = l_CopyCount
    Do While l_CopyCount > 0
        
        l_CurrentTop& = l_CurrentTop& - 11
        l_CopyCount = l_CopyCount - 1
        
        'Get the Duplication details for this copy
        
        l_DupeEpisodeNumber$ = GetXLData(l_CurrentTop&, 6)
        l_DupeTotalDuration$ = GetXLData(l_CurrentTop& + 1, 6)
        l_DupeQuantity$ = GetXLData(l_CurrentTop& + 2, 6)
        l_BBCDupeTapeFormat$ = GetXLData(l_CurrentTop& + 3, 6)
        l_DupeTapeFormat$ = GetAlias(l_BBCDupeTapeFormat$)
        l_DupeLineStandard$ = GetAlias(GetXLData(l_CurrentTop& + 4, 6))
        l_Original_DupeStandard$ = GetXLData(l_CurrentTop& + 4, 6)
        l_DupeAspectRatio$ = GetAlias(GetXLData(l_CurrentTop& + 5, 6))
        l_DupeLanguage$ = GetAlias(GetXLData(l_CurrentTop& + 6, 6))
        l_DupeCeefax$ = GetAlias(GetXLData(l_CurrentTop& + 7, 6))
        l_DupeAudio$ = GetXLData(l_CurrentTop& + 8, 6)
        
        'we need to get the different sections of the sounds out of the sound notes
        If l_DupeAudio$ <> "" Then
        
            'append another comma seperated value onto the end of the audio
            'channels to ensure we get the last value
            l_DupeAudio$ = l_DupeAudio$ & ",END"
                
            'get each Sound channel using the GetTextPart function from
            'modRoutines
            l_Sound1$ = GetAlias(GetTextPart(l_DupeAudio$, 1, ","))
            l_Sound2$ = GetAlias(GetTextPart(l_DupeAudio$, 2, ","))
            l_Sound3$ = GetAlias(GetTextPart(l_DupeAudio$, 3, ","))
            l_Sound4$ = GetAlias(GetTextPart(l_DupeAudio$, 4, ","))
            l_Sound5$ = GetAlias(GetTextPart(l_DupeAudio$, 5, ","))
            l_Sound6$ = GetAlias(GetTextPart(l_DupeAudio$, 6, ","))
            l_Sound7$ = GetAlias(GetTextPart(l_DupeAudio$, 7, ","))
            l_Sound8$ = GetAlias(GetTextPart(l_DupeAudio$, 8, ","))
        
        End If
        
        l_rsDetails.AddNew
        l_rsDetails("jobID") = l_NewJobNumber&
        l_rsDetails("description") = Left(l_ProgrammeTitle$, 50)
        If l_DupeTapeFormat$ <> "" Then l_rsDetails("format") = l_DupeTapeFormat$
        l_rsDetails("copytype") = "C"
        If l_DupeTapeFormat$ = "VHS" Or l_DupeTapeFormat$ = "DVD" Then
            l_rsDetails("quantity") = l_EpisodeCount
        Else
            l_rsDetails("quantity") = Val(l_DupeQuantity$) * l_EpisodeCount
        End If
        If l_DupeLineStandard$ <> "" Then l_rsDetails("videostandard") = Left(l_DupeLineStandard$, 10)
        If Int(Val(l_MasterSpoolDuration$)) > Int(Val(l_DupeTotalDuration$)) Then
            l_rsDetails("runningtime") = Int(Val(l_MasterSpoolDuration$))
            l_DupeTotalDuration$ = l_MasterSpoolDuration$
        Else
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
        End If
        If l_DupeAspectRatio$ <> "" And l_DupeAspectRatio$ <> "ASK" Then
            l_rsDetails("aspectratio") = l_DupeAspectRatio$
        Else
            If l_Original_DupeStandard$ Like "*16:9 WI*" Then
                l_rsDetails("aspectratio") = "16:9AN"
                l_DupeAspectRatio$ = "16:9AN"
            End If
            If l_Original_DupeStandard$ Like "*16:9 LE*" Then
                l_rsDetails("aspectratio") = "16:9LE"
                l_DupeAspectRatio$ = "16:9LE"
            End If
            If l_Original_DupeStandard$ Like "*15:9*" Then
                l_rsDetails("aspectratio") = "15:9LE"
                l_DupeAspectRatio$ = "15:9LE"
            End If
            If l_Original_DupeStandard$ Like "*14:9*" Then
                l_rsDetails("aspectratio") = "14:9LE"
                l_DupeAspectRatio$ = "14:9LE"
            End If
            If l_Original_DupeStandard$ Like "*4:3*" Then
                l_rsDetails("aspectratio") = "4:3NOR"
                l_DupeAspectRatio$ = "4:3NOR"
            End If
        End If
        If l_rsDetails("aspectratio") = "" Then
            l_rsDetails("aspectratio") = "4:3NOR"
            l_DupeAspectRatio$ = "4:3NOR"
        End If
        If l_Sound1$ <> "" Then l_rsDetails("sound1") = Left(l_Sound1$, 6)
        If l_Sound2$ <> "" Then l_rsDetails("sound2") = Left(l_Sound2$, 6)
        If l_Sound3$ <> "" Then l_rsDetails("sound3") = Left(l_Sound3$, 6)
        If l_Sound4$ <> "" Then l_rsDetails("sound4") = Left(l_Sound4$, 6)
        
    
        l_rsDetails.Update
        
        l_rsDetails.Bookmark = l_rsDetails.Bookmark
        l_lngdetailID = l_rsDetails("jobdetailID")
        l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
        l_rsDetails.Update
        
        l_rsxDetails.AddNew
        l_rsxDetails("jobID") = l_NewJobNumber&
        l_rsxDetails("jobdetailID") = l_lngdetailID
        If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
        If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
        If l_OrderNumber$ <> "" Then l_rsxDetails("BBCorderNumber") = l_OrderNumber$
        If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
        If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
        If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
        If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
        If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
        If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
        If l_BBCMasterTapeFormat$ <> "" Then l_rsxDetails("bbcSourceFormat") = l_BBCMasterTapeFormat$
        If l_BBCDupeTapeFormat$ <> "" Then l_rsxDetails("bbcRecordFormat") = l_BBCDupeTapeFormat$
        l_rsxDetails.Update
            
        If UCase(l_DupeCeefax$) = "TRUE" Then
            'create a new ceefax line
            l_rsDetails.AddNew
            l_rsDetails("jobID") = l_NewJobNumber&
            l_rsDetails("description") = "CEEFAX Subtitler"
            l_rsDetails("format") = "SUBT"
            l_rsDetails("copytype") = "A"
            l_rsDetails("quantity") = l_EpisodeCount
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
            l_rsDetails.Update
    
            l_rsDetails.Bookmark = l_rsDetails.Bookmark
            l_lngdetailID = l_rsDetails("jobdetailID")
            l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
            l_rsDetails.Update
            
            l_rsxDetails.AddNew
            l_rsxDetails("jobID") = l_NewJobNumber&
            l_rsxDetails("jobdetailID") = l_lngdetailID
            If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
            If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
            If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
            If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
            If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
            If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
            If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
            If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
            If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
            l_rsxDetails.Update
    
        End If
        
        If l_DupeLineStandard$ <> l_MasterLineStandard$ Then
            ' create a standards converter line
            l_rsDetails.AddNew
            l_rsDetails("jobID") = l_NewJobNumber&
            l_rsDetails("description") = "Standards Converter"
            If l_DupeTapeFormat$ = "VHS" Then
                l_rsDetails("format") = "CONV"
            Else
                l_rsDetails("format") = "B-CONV"
            End If
            l_rsDetails("copytype") = "A"
            l_rsDetails("quantity") = l_EpisodeCount
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
            l_rsDetails.Update
            
            l_rsDetails.Bookmark = l_rsDetails.Bookmark
            l_lngdetailID = l_rsDetails("jobdetailID")
            l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
            l_rsDetails.Update
            
            l_rsxDetails.AddNew
            l_rsxDetails("jobID") = l_NewJobNumber&
            l_rsxDetails("jobdetailID") = l_lngdetailID
            If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
            If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
            If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
            If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
            If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
            If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
            If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
            If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
            If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
            l_rsxDetails.Update
        
        End If
        
        If Left(l_MasterAspectRatio$, 6) <> Left(l_DupeAspectRatio$, 6) Then
            If Not (l_MasterAspectRatio$ Like "*4:3*" And l_DupeAspectRatio$ Like "*4:3*") Then
                ' create a ARC line
                l_rsDetails.AddNew
                l_rsDetails("jobID") = l_NewJobNumber&
                l_rsDetails("description") = "Aspect Ratio Converter"
                l_rsDetails("format") = "ARATIO"
                l_rsDetails("copytype") = "A"
                l_rsDetails("quantity") = l_EpisodeCount
                l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
                l_rsDetails.Update
            
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_lngdetailID = l_rsDetails("jobdetailID")
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                
                l_rsxDetails.AddNew
                l_rsxDetails("jobID") = l_NewJobNumber&
                l_rsxDetails("jobdetailID") = l_lngdetailID
                If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
                If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
                If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
                If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
                If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
                If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
                If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
                If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
                If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
                l_rsxDetails.Update
            End If
        End If
    
        If (l_DupeTapeFormat$ = "VHS" Or l_DupeTapeFormat$ = "DVD") And Val(l_DupeQuantity$) > 1 Then
            
            'Do another line to add the extra DVD and VHS copies
            
            l_rsDetails.AddNew
            l_rsDetails("jobID") = l_NewJobNumber&
            l_rsDetails("description") = "Additional Copies"
            If l_DupeTapeFormat$ = "VHS" Then
                l_rsDetails("format") = "WWADVHS"
            Else
                l_rsDetails("format") = "WWADDVD"
            End If
            l_rsDetails("copytype") = "I"
            l_rsDetails("quantity") = (Val(l_DupeQuantity$) - 1) * l_EpisodeCount
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
            l_rsDetails.Update
    
            l_rsDetails.Bookmark = l_rsDetails.Bookmark
            l_lngdetailID = l_rsDetails("jobdetailID")
            l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
            l_rsDetails.Update
            
            l_rsxDetails.AddNew
            l_rsxDetails("jobID") = l_NewJobNumber&
            l_rsxDetails("jobdetailID") = l_lngdetailID
            If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
            If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
            If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
            If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
            If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
            If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
            If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
            If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
            If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
            l_rsxDetails.Update
            
        End If
        DBEngine.Idle
        DoEvents
    Loop
    'Copy Loop has finished - need to reset the currenttop to beyond the end of the last group again
    l_CurrentTop& = l_CurrentTop& + (11 * l_OrigCopyCount)
Loop

'close the database and recordset
l_rsJobs.Close: Set l_rsJobs = Nothing
l_rsDetails.Close: Set l_rsDetails = Nothing
l_rsxJobs.Close: Set l_rsxJobs = Nothing
l_rsxDetails.Close: Set l_rsxDetails = Nothing
Set l_rsLib = Nothing
Set l_rsHistory = Nothing

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

'Rename file into the done folder - commented out during testing.
Name l_strExcelFileName As "\\jcaserver\ceta\Processed Star Orders\" & l_strExcelFileTitle

MsgBox "File Sucessfully Read", vbInformation, "Excel Read"

End Sub

Private Sub cmdOffAir_Click()

If Not CheckAccess("/processexcelorders") Then
    Exit Sub
End If


'emtpy the comma count field
m_lngCommacount = 0

'have a variable to hold the filename we are going to process
Dim l_strExcelFileName As String, l_strExcelFileTitle As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Dim l_rsJobs As ADODB.Recordset, l_rsDetails As ADODB.Recordset, l_rsLib As ADODB.Recordset, l_rsHistory As ADODB.Recordset, l_rsxJobs As ADODB.Recordset
Dim l_rsxDetails As ADODB.Recordset, l_lngLibraryID As Long

Dim l_rsOffAir As ADODB.Recordset

Dim l_strSQL As String, l_lngdetailID As Long
            
'open the recordsets
Set l_rsJobs = New ADODB.Recordset
Set l_rsDetails = New ADODB.Recordset
Set l_rsxJobs = New ADODB.Recordset
Set l_rsxDetails = New ADODB.Recordset
Set l_rsLib = New ADODB.Recordset
Set l_rsHistory = New ADODB.Recordset

Set l_rsJobs = ExecuteSQL("SELECT * FROM [job] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError
Set l_rsDetails = ExecuteSQL("SELECT * FROM [jobdetail] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError
Set l_rsxJobs = ExecuteSQL("SELECT * FROM [extendedjob] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError
Set l_rsxDetails = ExecuteSQL("SELECT * FROM [extendedjobdetail] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError

'free up locks
DoEvents

'open the spreadsheet from the root of C:\

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets("sheet1")

'get the fields and place them into variables
Dim l_BookedBy$, l_AccountNumber$, l_Telephone$, l_FACNumber$, l_jcacontact$, l_ClientName$, l_ContactName$, l_BookingNumber$
Dim l_Lastprogrammetitle$

'get the header details, i.e. Client name, etc.
l_AccountNumber$ = GetXLData(g_cntAccountNumberRow%, g_cntAccountNumberColumn%)
l_Telephone$ = GetXLData(g_cntTelephoneRow%, g_cntTelephoneColumn%)
l_ClientName$ = "BBC Worldwide"
l_ContactName$ = GetXLData(5, 6)
l_FACNumber$ = GetXLData(3, 2)
l_FACNumber$ = Mid(l_FACNumber$, 16, Len(l_FACNumber$))
l_jcacontact$ = GetXLData(7, 2)


'declare a variable for keeping track of how far down the page we are
Dim l_CurrentTop&

'set it to the first load of data
l_CurrentTop& = 10

'declare lots of variables for taking the values off the spreadsheet
Dim l_OrderNumber$, l_CustomerName$, l_ChargeCode$, l_BusinessAreaCode$, l_NominalCode$, l_ProgrammeTitle$, l_Requireddate$, l_Notes$
Dim l_MasterEpisodeNumber$, l_MasterSpoolNumber$, l_MasterLanguage$, l_MasterSpoolDuration$, l_MasterAspectRatio$, l_MasterTapeFormat$, l_MasterLineStandard$
Dim l_DupeEpisodeNumber$, l_DupeAudio$, l_DupeCeefax$, l_DupeTotalDuration$, l_DupeLanguage$, l_DupeQuantity$, l_DupeTapeFormat$, l_DupeLineStandard$, l_DupeAspectRatio$
Dim l_LastorderNumber$, l_PreviousJobNumber&, l_Original_MasterStandard$, l_Original_DupeStandard$, l_Saveflag%, l_Stk&, l_CalculatedDuration&, l_Mastercount&
Dim l_Orig_masterspool$, l_Orig_masterformat$, l_Orig_masterstandard$, l_Orig_masteraspect$, l_Orig_masterduration$, l_comma&, l_IndividualFAC$, l_ProgrammeNumber$
Dim l_CountryCode$, l_BusinessArea$, l_OldMasterSpoolNumber$, l_NewJobNumber&, l_BBCMasterTapeFormat$, l_BBCDupeTapeFormat$
Dim l_EpisodeCount As Long, l_CopyCount As Long, l_OrigCopyCount As Long

Dim l_strChannel As String, l_datTXDateAndTime As Date, l_strTXDate As String, l_strTXTime As String, l_intCount As Integer, l_strProgRefID As String
Dim l_datPublishedStartTime As Date, l_datPublishedEndTime As Date, l_strMainTitle As String

l_LastorderNumber$ = "CETA"
l_Saveflag% = False


'declare the Sound channels
Dim l_Sound1$, l_Sound2$, l_Sound3$, l_Sound4$, l_Sound5$, l_Sound6$, l_Sound7$, l_Sound8$

'start a loop that cycles through each tenth line
l_EpisodeCount = 0
l_CopyCount = 1
Do
    
    'if there is no data in the Bookingnumber field then this is either one of those blue lines or the end of the order.
    'Check which and then exit the do loop if necessary. While there, count the number of episodes in the current group,
    'and enter the data for the last one, rather than any others.
    
    If GetXLData(l_CurrentTop&, 2) = "" Then
        l_CurrentTop& = l_CurrentTop& + 2
        l_MasterSpoolNumber$ = ""
        l_OldMasterSpoolNumber$ = "GINGER"
        If GetXLData(l_CurrentTop&, 2) = "" Then Exit Do
    End If
    
    l_EpisodeCount = 1
    l_CopyCount = 1
    l_Mastercount& = 1
    l_BookingNumber$ = GetXLData(l_CurrentTop&, 2)
    
    If l_BookingNumber$ = "" Then Exit Do
    
    l_MasterSpoolNumber$ = GetXLData(l_CurrentTop& + 2, 4)
    
    'here is where we work out if there is two masters on this sheet, so check for a comma
    If InStr(l_MasterSpoolNumber$, ",") <> 0 Then
    
        'There's a comma in the master spoolnumbers line. This means we're dealing with multiple masters, and need to pick them apart.
        
        l_Mastercount& = Countcommas(l_MasterSpoolNumber$) + 1
        
    End If
        
    l_Orig_masterspool$ = l_MasterSpoolNumber$
    l_Orig_masterformat$ = GetXLData(l_CurrentTop& + 4, 4)
    l_Orig_masterstandard$ = GetXLData(l_CurrentTop& + 5, 4)
    l_Orig_masterduration$ = GetXLData(l_CurrentTop& + 3, 4)
    l_Orig_masteraspect$ = GetXLData(l_CurrentTop& + 6, 4)

    'go through the spreadsheet and retrieve the values and put them into
    'variables to be used later. These are from the Order Details section
    l_OrderNumber$ = GetXLData(l_CurrentTop& + 1, 2)
    l_IndividualFAC$ = GetXLData(l_CurrentTop& + 0, 2)
    l_CustomerName$ = GetXLData(l_CurrentTop& + 2, 2)
    l_ChargeCode$ = GetXLData(l_CurrentTop& + 3, 2)
    l_BusinessArea$ = GetXLData(l_CurrentTop& + 4, 2)
    l_BusinessAreaCode$ = GetXLData(l_CurrentTop& + 5, 2)
    l_NominalCode$ = GetXLData(l_CurrentTop& + 6, 2)
    l_ProgrammeTitle$ = GetXLData(l_CurrentTop& + 7, 2)
    l_ProgrammeNumber$ = GetXLData(l_CurrentTop& + 8, 2)
    l_CountryCode$ = GetXLData(l_CurrentTop& + 8, 4)
    l_Requireddate$ = GetXLData(l_CurrentTop& + 9, 2)
    l_Notes$ = GetXLData(l_CurrentTop& + 10, 2)
    l_DupeLineStandard$ = GetAlias(GetXLData(l_CurrentTop& + 4, 6))
    l_DupeQuantity$ = GetXLData(l_CurrentTop& + 2, 6)
    l_BBCDupeTapeFormat$ = GetXLData(l_CurrentTop& + 3, 6)

    
    'get the next chapter, which is Master Material
    l_MasterEpisodeNumber$ = GetXLData(l_CurrentTop& + 1, 4)
    
    If l_Mastercount& = 1 Then
        l_MasterSpoolDuration$ = GetXLData(l_CurrentTop& + 3, 4)
        l_MasterTapeFormat$ = GetAlias(GetXLData(l_CurrentTop& + 4, 4))
        l_MasterLineStandard$ = GetAlias(GetXLData(l_CurrentTop& + 5, 4))
        l_MasterAspectRatio$ = GetAlias(GetXLData(l_CurrentTop& + 6, 4))
    End If
    
    l_MasterLanguage$ = GetAlias(GetXLData(l_CurrentTop& + 7, 4))
    
    'increment the looper so we move to the next section  or empty line.
    l_CurrentTop& = l_CurrentTop& + 11
        
    If UCase(l_OrderNumber$) <> UCase(l_LastorderNumber$) Then
                    
        If l_LastorderNumber$ <> "STAR ORDER" Then
        
            'add a new record to the Jobs table
            l_rsJobs.AddNew
            l_rsJobs("createduserID") = 33
            l_rsJobs("createduser") = "Excel"
            l_rsJobs("createddate") = Now
            l_rsJobs("modifieddate") = Now
            l_rsJobs("modifieduserID") = 33
            l_rsJobs("modifieduser") = l_jcacontact$
            l_rsJobs("flagemail") = 1
            l_rsJobs("fd_status") = "Confirmed"
            l_rsJobs("jobtype") = "Dubbing"
            l_rsJobs("joballocation") = "Regular"
            l_rsJobs("deadlinedate") = Format(l_Requireddate$, "dd/mm/yyyy")
            l_rsJobs("despatchdate") = Format(l_Requireddate$, "dd/mm/yyyy")
            l_rsJobs("projectID") = 0
            l_rsJobs("productID") = 0
            If l_Telephone$ <> "" Then l_rsJobs("telephone") = Left$(l_Telephone$, 50)
'            Dim l_NewJobNumber&
'            l_NewJobNumber& = GetNextJobNumber()
'            l_PreviousJobNumber& = l_NewJobNumber&
'
'            l_rsJobs("job number") = l_NewJobNumber&
            l_rsJobs("companyID") = 570
            l_rsJobs("companyname") = "BBC Worldwide Int Ops"
            
            'parse the contactname to get the CETA contact ID and name.
            If l_ContactName$ <> "" Then
                l_rsJobs("contactname") = l_ContactName$
                l_rsJobs("contactID") = GetContactID(l_ContactName$)
            End If
            
            If l_ProgrammeTitle$ <> "" Then l_rsJobs("title1") = Left$(l_ProgrammeTitle$, 50)
            l_Lastprogrammetitle$ = UCase(l_ProgrammeTitle$)
            If l_MasterEpisodeNumber$ <> "" Then l_rsJobs("title2") = Left$(l_MasterEpisodeNumber$, 50)
            If l_Notes$ <> "" Then l_rsJobs("notes2") = "PLEASE CHECK ORIGINAL WW ORDER SHEETS FOR JOB DETAILS. " & l_Notes$
            
            'check whether this is a STAR order or a normal one.
            
            If l_OrderNumber$ Like "STAR*" Then
                l_rsJobs("orderreference") = "STAR ORDER"
                l_LastorderNumber$ = "STAR ORDER"
                l_rsJobs("title1") = "Various Titles - Star Order"
            Else
                If l_OrderNumber$ <> "" Then l_rsJobs("orderreference") = UCase(l_OrderNumber$)
                l_LastorderNumber$ = UCase(l_OrderNumber$)
            End If
                
            l_rsJobs.Update
            
            
            'this part here!
            l_rsJobs.Bookmark = l_rsJobs.Bookmark
            l_NewJobNumber& = l_rsJobs("JobID")
            l_PreviousJobNumber& = l_NewJobNumber&
            
            'Set the projectnumber to be the job ID - new CETA feature.
            l_rsJobs("projectnumber") = l_rsJobs("JobID")
            l_rsJobs.Update
                        
            DBEngine.Idle
            DoEvents
            
            If l_NewJobNumber& <> 0 Then
                
                l_rsxJobs.AddNew
                l_rsxJobs("JobID") = l_NewJobNumber&
                l_rsxJobs("BBCStarJob") = 1
                l_rsxJobs("BBCBookingnumber") = l_FACNumber$
                If l_CustomerName$ <> "" Then l_rsxJobs("bbccustomername") = l_CustomerName$
                If l_ChargeCode$ <> "" Then l_rsxJobs("bbcchargecode") = l_ChargeCode$
                If l_BusinessArea$ <> "" Then l_rsxJobs("bbcbusinessarea") = l_BusinessArea$
                If l_BusinessAreaCode$ <> "" Then l_rsxJobs("bbcbusinessareacode") = l_BusinessAreaCode$
                If l_NominalCode$ <> "" Then l_rsxJobs("bbcnominalcode") = l_NominalCode$
                If l_CountryCode$ <> "" Then l_rsxJobs("BBCCountryCode") = l_CountryCode$
                l_rsxJobs.Update
                
            End If
            
            DBEngine.Idle
            DoEvents
            
        End If
        
    Else
        'make sure the title program stays the same all the way
        If UCase(l_ProgrammeTitle$) <> UCase(l_Lastprogrammetitle$) Then
            'need to update the other job to change the title
            l_rsJobs.Close
            Set l_rsJobs = ExecuteSQL("Select [title1] from [Job] where [jobID] = " & l_PreviousJobNumber&, g_strExecuteError)
            CheckForSQLError
                        
            If l_rsJobs.RecordCount <> 0 Then
                l_rsJobs("title1") = "Various Titles - See Below"
                l_rsJobs.Update
                l_rsJobs.Close
                DBEngine.Idle
                DoEvents
                Set l_rsJobs = ExecuteSQL("SELECT * FROM [Job] WHERE [JobID] = -1", g_strExecuteError)
                CheckForSQLError
            End If
        End If
    End If

    DBEngine.Idle: DoEvents
    
    'The master detail line(s) get added next
    Do While l_Mastercount& > 0
        
        'Get next master into normal variables if there is more than 1, trim the original variables and decrease the mastercount.
        
        If l_Mastercount > 1 Then
            l_comma& = InStr(l_Orig_masterspool$, ",")
            l_MasterSpoolNumber$ = Left(l_Orig_masterspool$, l_comma& - 1)
            l_Orig_masterspool$ = Mid(l_Orig_masterspool$, l_comma& + 1, Len(l_Orig_masterspool$))
            l_comma& = InStr(l_Orig_masterformat$, ",")
            l_BBCMasterTapeFormat$ = Left(l_Orig_masterformat$, l_comma& - 1)
            l_MasterTapeFormat$ = GetAlias(l_BBCMasterTapeFormat$)
            l_Orig_masterformat$ = Mid(l_Orig_masterformat$, l_comma + 1, Len(l_Orig_masterformat$))
            l_comma& = InStr(l_Orig_masterstandard$, ",")
            l_MasterLineStandard$ = GetAlias(Left(l_Orig_masterstandard$, l_comma - 1))
            l_Orig_masterstandard$ = Mid(l_Orig_masterstandard$, l_comma + 1, Len(l_Orig_masterstandard$))
            l_comma& = InStr(l_Orig_masteraspect$, ",")
            If l_comma > 0 Then
                l_MasterAspectRatio$ = GetAlias(Left(l_Orig_masteraspect$, l_comma - 1))
                l_Orig_masteraspect$ = Mid(l_Orig_masteraspect$, l_comma + 1, Len(l_Orig_masteraspect$))
            Else
                l_MasterAspectRatio$ = GetAlias(l_Orig_masteraspect$)
            End If
            l_comma& = InStr(l_Orig_masterduration$, ",")
            l_MasterSpoolDuration$ = Left(l_Orig_masterduration$, l_comma - 1)
            l_Orig_masterduration$ = Mid(l_Orig_masterduration$, l_comma + 1, Len(l_Orig_masterduration$))
        End If
        l_Mastercount& = l_Mastercount& - 1
        
        'process that master
        
        'process the OffAir details first
        
        'Make an entry in the old offair schedule table
        
        l_strChannel = ""
        l_strTXDate = ""
        l_strTXTime = ""
        l_datTXDateAndTime = 0
        Set l_rsOffAir = ExecuteSQL("select * from offairschedule where offairscheduleID = -1", g_strExecuteError)
        CheckForSQLError
        l_rsOffAir.AddNew
        l_rsOffAir("starordernumber") = l_OrderNumber$
        For l_intCount = 1 To Len(l_ProgrammeNumber$)
            If Mid(l_ProgrammeNumber$, l_intCount, 3) <> " - " Then
                l_strChannel = l_strChannel & Mid(l_ProgrammeNumber$, l_intCount, 1)
            Else
                Exit For
            End If
        Next
        l_rsOffAir("offairchannel") = l_strChannel
        l_strTXTime = Right(l_ProgrammeNumber$, 5)
        l_strTXDate = l_MasterSpoolNumber$
        l_datTXDateAndTime = Format(l_strTXDate, "dd/mm/yy") & " " & l_strTXTime
        l_rsOffAir("offairdate") = l_datTXDateAndTime
        l_rsOffAir("progtitle") = l_ProgrammeTitle$
        l_rsOffAir("duration") = l_MasterSpoolDuration$
    
        If l_BBCDupeTapeFormat$ Like "DVD*" Then
            If UCase(l_ProgrammeTitle$) Like "NEWSNIGHT*" Then
                l_rsOffAir("dvdjca") = l_DupeQuantity$
            Else
                l_rsOffAir("dvdpal") = l_DupeQuantity$
            End If
        Else
            l_rsOffAir("dvdntsc") = l_DupeQuantity$
        End If
        l_rsOffAir.Update
        l_rsOffAir.Close
        
        'Look up the necessary details in the EPG schedule and make an entry in the epg_recording_request table
        
        If fb_bst(l_datTXDateAndTime) = True Then l_datTXDateAndTime = DateAdd("h", -1, l_datTXDateAndTime)

        Set l_rsOffAir = ExecuteSQL("SELECT progrefID, publishedstarttime, publishedendtime FROM epg_schedule WHERE channelname = '" & l_strChannel & "' AND publishedstarttime = '" & FormatSQLDate(l_datTXDateAndTime) & "';", g_strExecuteError)
        CheckForSQLError
        If l_rsOffAir.RecordCount > 0 Then
            l_strProgRefID = Trim(" " & l_rsOffAir("progrefID"))
            l_datPublishedStartTime = l_rsOffAir("publishedstarttime")
            l_datPublishedEndTime = l_rsOffAir("publishedendtime")
            l_rsOffAir.Close
            l_strMainTitle = GetData("epg_programme", "maintitle", "progrefid", l_strProgRefID)
            Set l_rsOffAir = ExecuteSQL("SELECT * FROM epg_recording_request WHERE epg_recording_requestID = -1;", g_strExecuteError)
            CheckForSQLError
            l_rsOffAir.AddNew
            l_rsOffAir("userID") = 667
            l_rsOffAir("recording_statusID") = 3
            l_rsOffAir("progrefID") = l_strProgRefID
            l_rsOffAir("maintitle") = l_strMainTitle
            l_rsOffAir("publishedstarttime") = l_datPublishedStartTime
            l_rsOffAir("actualstarttime") = l_datPublishedStartTime
            l_rsOffAir("publishedendtime") = l_datPublishedEndTime
            l_rsOffAir("actualendtime") = l_datPublishedEndTime
            l_rsOffAir("channelname") = l_strChannel
            l_rsOffAir("daterequested") = Now
            l_rsOffAir("dateconfirmed") = Now
            l_rsOffAir("businessarea") = 5
            If l_BBCDupeTapeFormat$ Like "DVD*" Then
                l_rsOffAir("dvdpalrequested") = l_DupeQuantity$
            Else
                l_rsOffAir("olcrequested") = l_DupeQuantity$
            End If
            l_rsOffAir("starordernumber") = l_OrderNumber$
            l_rsOffAir.Update
            l_rsOffAir.Close
        Else
            l_rsOffAir.Close
            MsgBox "One request found with no EPG entry: " & l_ProgrammeTitle$ & " " & l_strChannel & " " & l_datTXDateAndTime, vbInformation, "EPG Error"
        End If

        Set l_rsOffAir = Nothing
        
        'Then do the actual job stuff
        l_rsDetails.AddNew
        l_rsDetails("jobID") = l_NewJobNumber&
        If l_MasterSpoolNumber$ <> "" Then l_rsDetails("description") = Left(l_ProgrammeTitle$, 50)
        'check if we know about this tape in the library and get details if we do.
        'otherwise use the details they provide on the excel sheet.
        l_strSQL = "SELECT * FROM Library WHERE Barcode = '" & l_MasterSpoolNumber$ & "'"
        Set l_rsLib = ExecuteSQL(l_strSQL, g_strExecuteError)
        If Not l_rsLib.EOF Then
            If l_rsLib("format") <> "" Then
                l_rsDetails("format") = Left(l_rsLib("format"), 50)
                If l_rsDetails("format") Like "HI8DAT*" Then l_rsDetails("format") = "HI8DAT"
                If l_rsDetails("format") Like "DAT*" Then l_rsDetails("format") = "DAT"
            ElseIf l_MasterTapeFormat$ <> "" Then
                l_rsDetails("format") = l_MasterTapeFormat$
            End If
            l_rsDetails("copytype") = "M"
            l_rsDetails("quantity") = l_EpisodeCount
            l_rsDetails("videostandard") = Left(l_rsLib("videostandard"), 10)
            If l_rsLib("videostandard") <> "" Then l_MasterLineStandard$ = Left(l_rsLib("videostandard"), 10)
            If l_rsLib("AspectRatio") <> "" Then
                l_rsDetails("aspectratio") = Left((l_rsLib("AspectRatio") & l_rsLib("GeometricLinearity")), 50)
                l_MasterAspectRatio$ = l_rsDetails("aspectratio")
            Else
                If l_MasterAspectRatio$ <> "" Then
                    l_rsDetails("aspectratio") = l_MasterAspectRatio$
                Else
                    If l_Orig_masterstandard$ Like "*16:9 WI*" Then
                        l_rsDetails("aspectratio") = "16:9AN"
                        l_MasterAspectRatio$ = "16:9AN"
                    End If
                    If l_Orig_masterstandard$ Like "*16:9 LE*" Then
                        l_rsDetails("aspectratio") = "16:9LE"
                        l_MasterAspectRatio$ = "16:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*15:9*" Then
                        l_rsDetails("aspectratio") = "15:9LE"
                        l_MasterAspectRatio$ = "15:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*14:9*" Then
                        l_rsDetails("aspectratio") = "14:9LE"
                        l_MasterAspectRatio$ = "14:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*4:3*" Then
                        l_rsDetails("aspectratio") = "4:3NOR"
                        l_MasterAspectRatio$ = "4:3NOR"
                    End If
                End If
            End If
            If l_rsLib("Totalduration") <> "" Then
                l_CalculatedDuration& = 60 * Val(Left(l_rsLib("Totalduration"), 2)) + Val(Mid(l_rsLib("totalduration"), 4, 2))
                If Val(Mid(l_rsLib("totalduration"), 7, 2)) > 0 Then l_CalculatedDuration& = l_CalculatedDuration& + 1
            End If
            If l_CalculatedDuration& > 0 Then
                l_rsDetails("runningtime") = l_CalculatedDuration&
                l_MasterSpoolDuration$ = Str(l_CalculatedDuration&)
            Else
                l_rsDetails("runningtime") = Int(Val(l_MasterSpoolDuration$))
            End If
            If l_rsLib("ch1") <> "" Then
                l_rsDetails("sound1") = Process_Sound(l_rsLib("ch1"))
            Else
                l_rsDetails("sound1") = Left(l_rsLib("ch1"), 50)
            End If
            If l_rsLib("ch2") <> "" Then
                l_rsDetails("sound2") = Process_Sound(l_rsLib("ch2"))
            Else
                l_rsDetails("sound2") = Left(l_rsLib("ch2"), 50)
            End If
            If l_rsLib("ch3") <> "" Then
                l_rsDetails("sound3") = Process_Sound(l_rsLib("ch3"))
            Else
                l_rsDetails("sound3") = Left(l_rsLib("ch3"), 50)
            End If
            If l_rsLib("ch4") <> "" Then
                l_rsDetails("sound4") = Process_Sound(l_rsLib("ch4"))
            Else
                l_rsDetails("sound4") = Left(l_rsLib("ch4"), 50)
            End If
        Else
            'As well as loading up the job detail line from the provided data, we create a new tape entry in the library.
            'Well - we used to - I'm commenting all that code out, because we're not sure its helpful to do that any more.
'            l_rsLib.AddNew
'            l_rsLib("barcode") = l_MasterSpoolNumber$
'            l_rsLib("title") = l_ProgrammeTitle$
'            l_rsLib("subtitle") = l_DupeEpisodeNumber$
'            l_rsLib("companyID") = 287
'            l_rsLib("companyname") = "BBC Worldwide Ltd"
'            l_rsLib("location") = "OFF SITE"
            If l_MasterTapeFormat$ <> "" Then
'                l_rsLib("format") = l_MasterTapeFormat$
                l_rsDetails("format") = l_MasterTapeFormat$
            End If
'            l_rsLib("copytype") = "MASTER"
'            l_rsLib("version") = "MASTER"
            l_rsDetails("copytype") = "M"
            l_rsDetails("quantity") = l_EpisodeCount
            If l_MasterLineStandard$ = "ASK" Then
                l_MasterLineStandard$ = "625PAL"
                l_rsDetails("videoStandard") = "625PAL"
'                l_rsLib("videostandard") = "625PAL"
            Else
                If l_MasterLineStandard$ <> "" Then
                    l_rsDetails("videostandard") = Left(l_MasterLineStandard$, 10)
'                    l_rsLib("videostandard") = Left(l_MasterLineStandard$, 10)
                End If
            End If
            If l_MasterAspectRatio$ <> "" Then
                l_rsDetails("aspectratio") = l_MasterAspectRatio$
            Else
                If l_Orig_masterstandard$ Like "*16:9 WI*" Then
                    l_rsDetails("aspectratio") = "16:9AN"
                    l_MasterAspectRatio$ = "16:9AN"
'                    l_rsLib("aspectratio") = "16:9"
'                    l_rsLib("geometriclinearity") = "ANAMORPHIC"
                End If
                If l_Orig_masterstandard$ Like "*16:9 LE*" Then
                    l_rsDetails("aspectratio") = "16:9LE"
                    l_MasterAspectRatio$ = "16:9LE"
'                    l_rsLib("aspectratio") = "16:9"
'                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*15:9*" Then
                    l_rsDetails("aspectratio") = "15:9LE"
                    l_MasterAspectRatio$ = "15:9LE"
'                    l_rsLib("aspectratio") = "15:9"
'                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*14:9*" Then
                    l_rsDetails("aspectratio") = "14:9LE"
                    l_MasterAspectRatio$ = "14:9LE"
'                    l_rsLib("aspectratio") = "14:9"
'                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*4:3*" Then
                    l_rsDetails("aspectratio") = "4:3NOR"
                    l_MasterAspectRatio$ = "4:3NOR"
'                    l_rsLib("aspectratio") = "4:3"
'                    l_rsLib("geometriclinearity") = "NORMAL"
                End If
            End If
            If l_MasterAspectRatio$ = "ASK" Then
                l_rsDetails("aspectratio") = "4:3NOR"
                l_MasterAspectRatio$ = "4:3NOR"
'                l_rsLib("aspectratio") = "4:3"
'                l_rsLib("geometriclinearity") = "NORMAL"
            End If
            l_rsDetails("runningtime") = Int(Val(l_MasterSpoolDuration$))
'            l_rsLib("cuser") = "XLS"
'            l_rsLib("cdate") = Now
'            l_rsLib("muser") = "XLS"
'            l_rsLib("mdate") = Now
'            l_rsLib("code128barcode") = CIA_CODE128(l_MasterSpoolNumber$)
'            l_rsLib.Update
            'Then we make a history entry too.
            'No - we don't any more.
'            l_rsLib.Bookmark = l_rsLib.Bookmark
'            l_lngLibraryID = l_rsLib("libraryID")
'            l_rsHistory.Open "SELECT * FROM [trans] WHERE [libraryID] = -1", l_conn, adOpenKeyset, adLockOptimistic
'            l_rsHistory.AddNew
'            l_rsHistory("libraryID") = l_lngLibraryID
'            l_rsHistory("destination") = "Created"
'            l_rsHistory("cuser") = "XLS"
'            l_rsHistory("cdate") = Now
'            l_rsHistory.Update
'            l_rsHistory.Close
        End If
        l_rsLib.Close
        l_BBCMasterTapeFormat$ = l_MasterTapeFormat$
        If l_rsDetails("format") = "" Then l_rsDetails("format") = "ASK"
        
        l_rsDetails.Update
        
        l_rsDetails.Bookmark = l_rsDetails.Bookmark
        l_lngdetailID = l_rsDetails("jobdetailID")
        l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
        l_rsDetails.Update

        
        l_rsxDetails.AddNew
        l_rsxDetails("jobID") = l_NewJobNumber&
        l_rsxDetails("jobdetailID") = l_lngdetailID
        If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
        If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
        If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
        If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
        If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
        If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
        If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
        If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
        If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
        If l_BBCMasterTapeFormat$ <> "" Then l_rsxDetails("bbcsourceformat") = l_BBCMasterTapeFormat$
        If l_DupeTapeFormat$ <> "" Then l_rsxDetails("bbcRecordFormat") = l_DupeTapeFormat$
        l_rsxDetails.Update
                
        'update the variables if the mastercount is currentrly 1
        If l_Mastercount& = 1 Then
            l_BBCMasterTapeFormat$ = l_Orig_masterformat$
            l_MasterTapeFormat$ = GetAlias(l_BBCMasterTapeFormat$)
            l_MasterSpoolNumber$ = l_Orig_masterspool$
            l_MasterLineStandard$ = GetAlias(l_Orig_masterstandard$)
            l_MasterAspectRatio$ = GetAlias(l_Orig_masteraspect$)
            l_MasterSpoolDuration$ = l_Orig_masterduration$
        End If
    Loop
    
    'Then the copy detail line gets added
    l_OrigCopyCount = l_CopyCount
    Do While l_CopyCount > 0
        
        l_CurrentTop& = l_CurrentTop& - 11
        l_CopyCount = l_CopyCount - 1
        
        'Get the Duplication details for this copy
        
        l_DupeEpisodeNumber$ = GetXLData(l_CurrentTop&, 6)
        l_DupeTotalDuration$ = GetXLData(l_CurrentTop& + 1, 6)
        l_DupeQuantity$ = GetXLData(l_CurrentTop& + 2, 6)
        l_BBCDupeTapeFormat$ = GetXLData(l_CurrentTop& + 3, 6)
        l_DupeTapeFormat$ = GetAlias(l_BBCDupeTapeFormat$)
        l_DupeLineStandard$ = GetAlias(GetXLData(l_CurrentTop& + 4, 6))
        l_Original_DupeStandard$ = GetXLData(l_CurrentTop& + 4, 6)
        l_DupeAspectRatio$ = GetAlias(GetXLData(l_CurrentTop& + 5, 6))
        l_DupeLanguage$ = GetAlias(GetXLData(l_CurrentTop& + 6, 6))
        l_DupeCeefax$ = GetAlias(GetXLData(l_CurrentTop& + 7, 6))
        l_DupeAudio$ = GetXLData(l_CurrentTop& + 8, 6)
        
        'we need to get the different sections of the sounds out of the sound notes
        If l_DupeAudio$ <> "" Then
        
            'append another comma seperated value onto the end of the audio
            'channels to ensure we get the last value
            l_DupeAudio$ = l_DupeAudio$ & ",END"
                
            'get each Sound channel using the GetTextPart function from
            'modRoutines
            l_Sound1$ = GetAlias(GetTextPart(l_DupeAudio$, 1, ","))
            l_Sound2$ = GetAlias(GetTextPart(l_DupeAudio$, 2, ","))
            l_Sound3$ = GetAlias(GetTextPart(l_DupeAudio$, 3, ","))
            l_Sound4$ = GetAlias(GetTextPart(l_DupeAudio$, 4, ","))
            l_Sound5$ = GetAlias(GetTextPart(l_DupeAudio$, 5, ","))
            l_Sound6$ = GetAlias(GetTextPart(l_DupeAudio$, 6, ","))
            l_Sound7$ = GetAlias(GetTextPart(l_DupeAudio$, 7, ","))
            l_Sound8$ = GetAlias(GetTextPart(l_DupeAudio$, 8, ","))
        
        End If
        
        l_rsDetails.AddNew
        l_rsDetails("jobID") = l_NewJobNumber&
        l_rsDetails("description") = Left(l_ProgrammeTitle$, 50)
        If l_DupeTapeFormat$ <> "" Then l_rsDetails("format") = l_DupeTapeFormat$
        l_rsDetails("copytype") = "C"
        If l_DupeTapeFormat$ = "VHS" Or l_DupeTapeFormat$ = "DVD" Then
            l_rsDetails("quantity") = l_EpisodeCount
        Else
            l_rsDetails("quantity") = Val(l_DupeQuantity$) * l_EpisodeCount
        End If
        If l_DupeLineStandard$ <> "" Then l_rsDetails("videostandard") = Left(l_DupeLineStandard$, 10)
        If Int(Val(l_MasterSpoolDuration$)) > Int(Val(l_DupeTotalDuration$)) Then
            l_rsDetails("runningtime") = Int(Val(l_MasterSpoolDuration$))
            l_DupeTotalDuration$ = l_MasterSpoolDuration$
        Else
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
        End If
        If l_DupeAspectRatio$ <> "" And l_DupeAspectRatio$ <> "ASK" Then
            l_rsDetails("aspectratio") = l_DupeAspectRatio$
        Else
            If l_Original_DupeStandard$ Like "*16:9 WI*" Then
                l_rsDetails("aspectratio") = "16:9AN"
                l_DupeAspectRatio$ = "16:9AN"
            End If
            If l_Original_DupeStandard$ Like "*16:9 LE*" Then
                l_rsDetails("aspectratio") = "16:9LE"
                l_DupeAspectRatio$ = "16:9LE"
            End If
            If l_Original_DupeStandard$ Like "*15:9*" Then
                l_rsDetails("aspectratio") = "15:9LE"
                l_DupeAspectRatio$ = "15:9LE"
            End If
            If l_Original_DupeStandard$ Like "*14:9*" Then
                l_rsDetails("aspectratio") = "14:9LE"
                l_DupeAspectRatio$ = "14:9LE"
            End If
            If l_Original_DupeStandard$ Like "*4:3*" Then
                l_rsDetails("aspectratio") = "4:3NOR"
                l_DupeAspectRatio$ = "4:3NOR"
            End If
        End If
        If l_rsDetails("aspectratio") = "" Then
            l_rsDetails("aspectratio") = "4:3NOR"
            l_DupeAspectRatio$ = "4:3NOR"
        End If
        If l_Sound1$ <> "" Then l_rsDetails("sound1") = Left(l_Sound1$, 6)
        If l_Sound2$ <> "" Then l_rsDetails("sound2") = Left(l_Sound2$, 6)
        If l_Sound3$ <> "" Then l_rsDetails("sound3") = Left(l_Sound3$, 6)
        If l_Sound4$ <> "" Then l_rsDetails("sound4") = Left(l_Sound4$, 6)
        
    
        l_rsDetails.Update
        
        l_rsDetails.Bookmark = l_rsDetails.Bookmark
        l_lngdetailID = l_rsDetails("jobdetailID")
        l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
        l_rsDetails.Update
        
        l_rsxDetails.AddNew
        l_rsxDetails("jobID") = l_NewJobNumber&
        l_rsxDetails("jobdetailID") = l_lngdetailID
        If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
        If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
        If l_OrderNumber$ <> "" Then l_rsxDetails("BBCorderNumber") = l_OrderNumber$
        If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
        If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
        If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
        If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
        If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
        If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
        If l_BBCMasterTapeFormat$ <> "" Then l_rsxDetails("bbcSourceFormat") = l_BBCMasterTapeFormat$
        If l_BBCDupeTapeFormat$ <> "" Then l_rsxDetails("bbcRecordFormat") = l_BBCDupeTapeFormat$
        l_rsxDetails.Update
            
        If UCase(l_DupeCeefax$) = "TRUE" Then
            'create a new ceefax line
            l_rsDetails.AddNew
            l_rsDetails("jobID") = l_NewJobNumber&
            l_rsDetails("description") = "CEEFAX Subtitler"
            l_rsDetails("format") = "SUBT"
            l_rsDetails("copytype") = "A"
            l_rsDetails("quantity") = l_EpisodeCount
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
            l_rsDetails.Update
    
            l_rsDetails.Bookmark = l_rsDetails.Bookmark
            l_lngdetailID = l_rsDetails("jobdetailID")
            l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
            l_rsDetails.Update
            
            l_rsxDetails.AddNew
            l_rsxDetails("jobID") = l_NewJobNumber&
            l_rsxDetails("jobdetailID") = l_lngdetailID
            If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
            If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
            If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
            If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
            If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
            If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
            If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
            If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
            If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
            l_rsxDetails.Update
    
        End If
        
        If l_DupeLineStandard$ <> l_MasterLineStandard$ Then
            ' create a standards converter line
            l_rsDetails.AddNew
            l_rsDetails("jobID") = l_NewJobNumber&
            l_rsDetails("description") = "Standards Converter"
            If l_DupeTapeFormat$ = "VHS" Then
                l_rsDetails("format") = "CONV"
            Else
                l_rsDetails("format") = "B-CONV"
            End If
            l_rsDetails("copytype") = "A"
            l_rsDetails("quantity") = l_EpisodeCount
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
            l_rsDetails.Update
            
            l_rsDetails.Bookmark = l_rsDetails.Bookmark
            l_lngdetailID = l_rsDetails("jobdetailID")
            l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
            l_rsDetails.Update
            
            l_rsxDetails.AddNew
            l_rsxDetails("jobID") = l_NewJobNumber&
            l_rsxDetails("jobdetailID") = l_lngdetailID
            If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
            If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
            If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
            If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
            If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
            If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
            If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
            If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
            If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
            l_rsxDetails.Update
        
        End If
        
        If Left(l_MasterAspectRatio$, 6) <> Left(l_DupeAspectRatio$, 6) Then
            If Not (l_MasterAspectRatio$ Like "*4:3*" And l_DupeAspectRatio$ Like "*4:3*") Then
                ' create a ARC line
                l_rsDetails.AddNew
                l_rsDetails("jobID") = l_NewJobNumber&
                l_rsDetails("description") = "Aspect Ratio Converter"
                l_rsDetails("format") = "ARATIO"
                l_rsDetails("copytype") = "A"
                l_rsDetails("quantity") = l_EpisodeCount
                l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
                l_rsDetails.Update
            
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_lngdetailID = l_rsDetails("jobdetailID")
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                
                l_rsxDetails.AddNew
                l_rsxDetails("jobID") = l_NewJobNumber&
                l_rsxDetails("jobdetailID") = l_lngdetailID
                If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
                If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
                If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
                If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
                If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
                If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
                If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
                If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
                If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
                l_rsxDetails.Update
            End If
        End If
    
        If (l_DupeTapeFormat$ = "VHS" Or l_DupeTapeFormat$ = "DVD") And Val(l_DupeQuantity$) > 1 Then
            
            'Do another line to add the extra DVD and VHS copies
            
            l_rsDetails.AddNew
            l_rsDetails("jobID") = l_NewJobNumber&
            l_rsDetails("description") = "Additional Copies"
            If l_DupeTapeFormat$ = "VHS" Then
                l_rsDetails("format") = "WWADVHS"
            Else
                l_rsDetails("format") = "WWADDVD"
            End If
            l_rsDetails("copytype") = "I"
            l_rsDetails("quantity") = (Val(l_DupeQuantity$) - 1) * l_EpisodeCount
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
            l_rsDetails.Update
    
            l_rsDetails.Bookmark = l_rsDetails.Bookmark
            l_lngdetailID = l_rsDetails("jobdetailID")
            l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
            l_rsDetails.Update
            
            l_rsxDetails.AddNew
            l_rsxDetails("jobID") = l_NewJobNumber&
            l_rsxDetails("jobdetailID") = l_lngdetailID
            If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
            If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
            If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
            If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
            If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
            If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
            If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
            If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
            If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
            l_rsxDetails.Update
            
        End If
        DBEngine.Idle
        DoEvents
    Loop
    'Copy Loop has finished - need to reset the currenttop to beyond the end of the last group again
    l_CurrentTop& = l_CurrentTop& + (11 * l_OrigCopyCount)
Loop

'close the database and recordset
l_rsJobs.Close: Set l_rsJobs = Nothing
l_rsDetails.Close: Set l_rsDetails = Nothing
l_rsxJobs.Close: Set l_rsxJobs = Nothing
l_rsxDetails.Close: Set l_rsxDetails = Nothing
Set l_rsLib = Nothing
Set l_rsHistory = Nothing

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

'Rename file into the done folder - commented out during testing.
'Name l_strExcelFileName As "\\jcaserver\ceta\Processed Star Orders\" & l_strExcelFileTitle

MsgBox "File Sucessfully Read", vbInformation, "Excel Read"

End Sub

Private Sub cmdDespatchBarcodes_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long, l_strShelf As String, l_strBarcode As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets("Sheet1")

l_lngXLRowCounter = 1

Do While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_strBarcode = GetXLData(l_lngXLRowCounter, 1)
    
    frmJobDespatch.txtBarcode.Text = l_strBarcode
    frmJobDespatch.cmdBarcodeKeypressReturn.Value = True
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Loop

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

Beep

End Sub

Private Sub Form_Load()

CenterForm Me

End Sub

Function Construct_Svensk_Update_String(lp_strTrackerID As String, ByRef l_strAuditDescription As String, ByRef l_blnError As Boolean, l_strAlphaDisplayCode As String, l_strKitID, l_strRevisionNumber As String, _
l_strUrgent As String, l_datDueDate As Date, l_strProjectManager As String, l_strRightsOwner As String, l_strSpecialProject As String, l_strProxyRequired As String, l_strMERequired As String, l_strME51Required As String, _
l_strSubTitle As String, l_striTunesRequired As String, l_strConform As String, l_lngXLRowCounter As Long) As String

'Existing line that is not completed yet...
l_strAuditDescription = ""
Construct_Svensk_Update_String = "UPDATE tracker_svensk_item SET "
If l_strAlphaDisplayCode <> "" Then
    If GetData("tracker_svensk_item", "alphadisplaycode", "tracker_svensk_itemID", lp_strTrackerID) <> l_strAlphaDisplayCode Then
        Construct_Svensk_Update_String = Construct_Svensk_Update_String & "alphadisplaycode = '" & l_strAlphaDisplayCode & "', "
        l_strAuditDescription = l_strAuditDescription & "AlphaDisplayCode = " & l_strAlphaDisplayCode & ", "
    End If
End If
If l_strKitID <> "" Then
    If GetData("tracker_svensk_item", "KitID", "tracker_svensk_itemID", lp_strTrackerID) <> l_strKitID Then
        Construct_Svensk_Update_String = Construct_Svensk_Update_String & "KitID = '" & l_strKitID & "', "
        l_strAuditDescription = l_strAuditDescription & "KitID = " & l_strKitID & ", "
    End If
End If
If l_strRevisionNumber <> "" Then
    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "RevisionNumber = '" & l_strRevisionNumber & "', "
    l_strAuditDescription = l_strAuditDescription & "RevisionNumer = " & l_strRevisionNumber & ", "
End If
If l_strUrgent <> "" Then
    If GetData("tracker_svensk_item", "urgent", "tracker_svensk_itemID", Val(lp_strTrackerID)) <> -1 Then
        Construct_Svensk_Update_String = Construct_Svensk_Update_String & "urgent = -1, "
        l_strAuditDescription = l_strAuditDescription & "urgent = -1, "
    End If
'Else
'    If GetData("tracker_svensk_item", "urgent", "tracker_svensk_itemID", Val(lp_strTrackerID)) <> 0 Then
'        Construct_Svensk_Update_String = Construct_Svensk_Update_String & "urgent = 0, "
'        l_strAuditDescription = l_strAuditDescription & "urgent = 0, "
'    End If
End If
If GetData("tracker_svensk_item", "duedate", "tracker_svensk_itemID", lp_strTrackerID) <> l_datDueDate Then
    If l_datDueDate > Now Then
        Construct_Svensk_Update_String = Construct_Svensk_Update_String & "duedate = '" & FormatSQLDate(l_datDueDate) & "', "
        l_strAuditDescription = l_strAuditDescription & "duedate = '" & l_datDueDate & "', "
    Else
        MsgBox "Line " & l_lngXLRowCounter & " has a Due Date that is earlier than Now - Line not read in.", vbCritical, "Error Reading Sheet"
        l_blnError = True
    End If
End If
If Val(Trim(" " & GetDataSQL("SELECT TOP 1 contactID FROM contact WHERE name = '" & l_strProjectManager & "' AND contactID IN (SELECT contactID FROM employee WHERE companyID = 1415);"))) <> 0 Then
    If GetData("tracker_svensk_item", "projectmanager", "tracker_svensk_itemID", lp_strTrackerID) <> l_strProjectManager Then
        Construct_Svensk_Update_String = Construct_Svensk_Update_String & "projectmanager = '" & QuoteSanitise(l_strProjectManager) & "', "
        Construct_Svensk_Update_String = Construct_Svensk_Update_String & "projectmanagercontactID = " & GetData("contact", "contactID", "name", l_strProjectManager) & ", "
        l_strAuditDescription = l_strAuditDescription & "projectmanager = '" & QuoteSanitise(l_strProjectManager) & "', "
    End If
Else
    MsgBox "Line " & l_lngXLRowCounter & " has an unidentified Project Manager - Line not read in.", vbCritical, "Error Reading Sheet"
    l_blnError = True
End If
If GetData("tracker_svensk_item", "rightsowner", "tracker_svensk_itemID", lp_strTrackerID) <> l_strRightsOwner Then
    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "rightsowner = '" & QuoteSanitise(l_strRightsOwner) & "', "
    l_strAuditDescription = l_strAuditDescription & "rightsowner = '" & QuoteSanitise(l_strRightsOwner) & "', "
End If
If GetData("tracker_svensk_item", "SpecialProject", "tracker_svensk_itemID", lp_strTrackerID) <> l_strSpecialProject Then
    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "SpecialProject = '" & QuoteSanitise(l_strSpecialProject) & "', "
    l_strAuditDescription = l_strAuditDescription & "SpecialProject = '" & QuoteSanitise(l_strSpecialProject) & "', "
End If
If Left(UCase(l_strProxyRequired), 1) = "Y" Then
    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "proxyreq = 1, "
'Else
'    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "proxyreq = 0, "
End If
If Left(UCase(l_strMERequired), 1) = "Y" Then
    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "MErequired = 1, "
'Else
'    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "MErequired = 0, "
End If
If Left(UCase(l_strME51Required), 1) = "Y" Then
    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "ME51required = 1, "
'Else
'    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "ME51required = 0, "
End If
Construct_Svensk_Update_String = Construct_Svensk_Update_String & "subtitle = '" & QuoteSanitise(l_strSubTitle) & "', "
If Left(UCase(l_striTunesRequired), 1) = "Y" Then
    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "itunes = 1, "
'Else
'    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "itunes = 0, "
End If
If Left(UCase(l_strConform), 1) = "Y" Then
    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "conform = 1, "
'Else
'    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "conform = 0, "
End If

If Right(Construct_Svensk_Update_String, 2) = ", " Then Construct_Svensk_Update_String = Left(Construct_Svensk_Update_String, Len(Construct_Svensk_Update_String) - 2) & " "

End Function
