VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmDisneyTracker 
   Caption         =   "Disney Tracker"
   ClientHeight    =   16125
   ClientLeft      =   3060
   ClientTop       =   3210
   ClientWidth     =   28620
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   16125
   ScaleWidth      =   28620
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdUpdateDueDate 
      Caption         =   "Update Due Dates"
      Height          =   375
      Left            =   13320
      TabIndex        =   26
      Top             =   540
      Width           =   2295
   End
   Begin VB.CommandButton cmdReport 
      Caption         =   "Report"
      Height          =   375
      Left            =   10920
      TabIndex        =   25
      Top             =   540
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.CommandButton cmdNewOrder 
      Caption         =   "Enter New Order"
      Height          =   375
      Left            =   10920
      TabIndex        =   23
      Top             =   60
      Width           =   2295
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete"
      Height          =   375
      Index           =   5
      Left            =   5820
      TabIndex        =   22
      Top             =   60
      Value           =   -1  'True
      Width           =   2955
   End
   Begin VB.CommandButton cmdWorkStatusUpdate 
      Caption         =   "Update Work Statis"
      Height          =   375
      Left            =   13320
      TabIndex        =   21
      Top             =   60
      Width           =   2295
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnUsers 
      Height          =   795
      Left            =   12960
      TabIndex        =   20
      Top             =   4560
      Width           =   2235
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   3200
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "Description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   3942
      _ExtentY        =   1402
      _StockProps     =   77
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnContact 
      Bindings        =   "frmDisneyTracker.frx":0000
      Height          =   1155
      Left            =   5580
      TabIndex        =   19
      Top             =   7680
      Width           =   7875
      DataFieldList   =   "name"
      ListAutoValidate=   0   'False
      MaxDropDownItems=   20
      _Version        =   196617
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   2
      Columns(0).Width=   9710
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3519
      Columns(1).Caption=   "contactID"
      Columns(1).Name =   "contactID"
      Columns(1).DataField=   "contactID"
      Columns(1).FieldLen=   256
      _ExtentX        =   13891
      _ExtentY        =   2037
      _StockProps     =   77
      DataFieldToDisplay=   "name"
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "All"
      Height          =   375
      Index           =   4
      Left            =   8880
      TabIndex        =   18
      Top             =   660
      Width           =   1875
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Billed"
      Height          =   375
      Index           =   3
      Left            =   8880
      TabIndex        =   17
      Top             =   360
      Width           =   1875
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Complete, Not Billed"
      Height          =   375
      Index           =   2
      Left            =   8880
      TabIndex        =   16
      Top             =   60
      Width           =   1875
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete, Materials in Place"
      Height          =   375
      Index           =   1
      Left            =   5820
      TabIndex        =   15
      Top             =   660
      Width           =   2955
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete, Materials Not in Place"
      Height          =   375
      Index           =   0
      Left            =   5820
      TabIndex        =   14
      Top             =   360
      Width           =   2955
   End
   Begin VB.TextBox txtSearchOrderNumber 
      Height          =   315
      Left            =   2460
      TabIndex        =   13
      Top             =   780
      Width           =   3015
   End
   Begin VB.TextBox txtSearchJobID 
      Height          =   315
      Left            =   2460
      TabIndex        =   10
      Top             =   420
      Width           =   3015
   End
   Begin VB.TextBox txtSearchTitle 
      Height          =   315
      Left            =   2460
      TabIndex        =   8
      Top             =   60
      Width           =   3015
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   2700
      TabIndex        =   1
      Top             =   14460
      Width           =   17835
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   16560
         TabIndex        =   4
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   15300
         TabIndex        =   3
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   14040
         TabIndex        =   2
         Top             =   0
         Width           =   1215
      End
   End
   Begin MSAdodcLib.Adodc adoItems 
      Height          =   330
      Left            =   60
      Top             =   1620
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdItems 
      Bindings        =   "frmDisneyTracker.frx":0019
      Height          =   10035
      Left            =   60
      TabIndex        =   0
      Top             =   1620
      Width           =   27885
      _Version        =   196617
      stylesets.count =   3
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmDisneyTracker.frx":0030
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmDisneyTracker.frx":004C
      stylesets(2).Name=   "headerfield"
      stylesets(2).ForeColor=   0
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmDisneyTracker.frx":0068
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   32
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "disneytrackerID"
      Columns(0).Name =   "disneytrackerID"
      Columns(0).DataField=   "disneytrackerID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   1244
      Columns(2).Caption=   "jobID"
      Columns(2).Name =   "jobID"
      Columns(2).DataField=   "jobID"
      Columns(2).FieldLen=   256
      Columns(2).StyleSet=   "headerfield"
      Columns(3).Width=   3200
      Columns(3).Caption=   "title"
      Columns(3).Name =   "title"
      Columns(3).DataField=   "title"
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "headerfield"
      Columns(4).Width=   1429
      Columns(4).Caption=   "episode"
      Columns(4).Name =   "episode"
      Columns(4).DataField=   "episode"
      Columns(4).FieldLen=   256
      Columns(4).StyleSet=   "headerfield"
      Columns(5).Width=   3200
      Columns(5).Caption=   "subtitle"
      Columns(5).Name =   "subtitle"
      Columns(5).DataField=   "subtitle"
      Columns(5).FieldLen=   256
      Columns(5).StyleSet=   "headerfield"
      Columns(6).Width=   3200
      Columns(6).Caption=   "requirement"
      Columns(6).Name =   "requirement"
      Columns(6).DataField=   "requirement"
      Columns(6).FieldLen=   256
      Columns(6).StyleSet=   "headerfield"
      Columns(7).Width=   3200
      Columns(7).Caption=   "ordernumber"
      Columns(7).Name =   "ordernumber"
      Columns(7).DataField=   "ordernumber"
      Columns(7).FieldLen=   256
      Columns(7).StyleSet=   "headerfield"
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "disneycontactID"
      Columns(8).Name =   "disneycontactID"
      Columns(8).DataField=   "disneycontactID"
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Caption=   "disneycontact"
      Columns(9).Name =   "disneycontact"
      Columns(9).FieldLen=   256
      Columns(9).StyleSet=   "headerfield"
      Columns(10).Width=   3200
      Columns(10).Caption=   "jcacontact"
      Columns(10).Name=   "jcacontact"
      Columns(10).DataField=   "jcacontact"
      Columns(10).FieldLen=   256
      Columns(10).StyleSet=   "headerfield"
      Columns(11).Width=   2302
      Columns(11).Caption=   "language"
      Columns(11).Name=   "language"
      Columns(11).DataField=   "language"
      Columns(11).FieldLen=   256
      Columns(11).StyleSet=   "headerfield"
      Columns(12).Width=   3519
      Columns(12).Caption=   "duedate"
      Columns(12).Name=   "duedate"
      Columns(12).DataField=   "duedate"
      Columns(12).DataType=   7
      Columns(12).FieldLen=   256
      Columns(12).Style=   1
      Columns(12).StyleSet=   "headerfield"
      Columns(13).Width=   2302
      Columns(13).Caption=   "audio duedate"
      Columns(13).Name=   "audioduedate"
      Columns(13).DataField=   "audioduedate"
      Columns(13).FieldLen=   256
      Columns(13).Style=   1
      Columns(13).StyleSet=   "headerfield"
      Columns(14).Width=   1376
      Columns(14).Caption=   "On Hold"
      Columns(14).Name=   "hold"
      Columns(14).DataField=   "hold"
      Columns(14).FieldLen=   256
      Columns(14).Style=   2
      Columns(14).StyleSet=   "headerfield"
      Columns(15).Width=   2302
      Columns(15).Caption=   "arrivedfromJR"
      Columns(15).Name=   "arrivedfromJR"
      Columns(15).DataField=   "arrivedfromJR"
      Columns(15).FieldLen=   256
      Columns(15).Style=   1
      Columns(15).StyleSet=   "stagefield"
      Columns(16).Width=   2302
      Columns(16).Caption=   "referredtodisney"
      Columns(16).Name=   "referredtodisney"
      Columns(16).DataField=   "referredtodisney"
      Columns(16).DataType=   7
      Columns(16).FieldLen=   256
      Columns(16).Style=   1
      Columns(16).StyleSet=   "stagefield"
      Columns(17).Width=   2302
      Columns(17).Caption=   "materialsindate"
      Columns(17).Name=   "materialsindate"
      Columns(17).DataField=   "materialsindate"
      Columns(17).DataType=   7
      Columns(17).FieldLen=   256
      Columns(17).Style=   1
      Columns(17).StyleSet=   "stagefield"
      Columns(18).Width=   873
      Columns(18).Caption=   "I"
      Columns(18).Name=   "insertsneeded"
      Columns(18).Alignment=   2
      Columns(18).DataField=   "insertsneeded"
      Columns(18).FieldLen=   256
      Columns(18).Style=   2
      Columns(18).StyleSet=   "headerfield"
      Columns(19).Width=   2117
      Columns(19).Caption=   "inserts"
      Columns(19).Name=   "insertsarrived"
      Columns(19).DataField=   "insertsarrived"
      Columns(19).DataType=   7
      Columns(19).FieldLen=   256
      Columns(19).Style=   1
      Columns(19).StyleSet=   "stagefield"
      Columns(20).Width=   873
      Columns(20).Caption=   "DC"
      Columns(20).Name=   "dubbingcreditsneeded"
      Columns(20).Alignment=   2
      Columns(20).DataField=   "dubbingcreditsneeded"
      Columns(20).FieldLen=   256
      Columns(20).Style=   2
      Columns(20).StyleSet=   "headerfield"
      Columns(21).Width=   2117
      Columns(21).Caption=   "dubb credits"
      Columns(21).Name=   "dubbingcreditsarrived"
      Columns(21).DataField=   "dubbingcreditsarrived"
      Columns(21).DataType=   7
      Columns(21).FieldLen=   256
      Columns(21).Style=   1
      Columns(21).StyleSet=   "stagefield"
      Columns(22).Width=   2117
      Columns(22).Caption=   "edit done"
      Columns(22).Name=   "editdone"
      Columns(22).DataField=   "editdone"
      Columns(22).DataType=   7
      Columns(22).FieldLen=   256
      Columns(22).Style=   1
      Columns(22).StyleSet=   "stagefield"
      Columns(23).Width=   3200
      Columns(23).Caption=   "disney email"
      Columns(23).Name=   "disneyemailsto"
      Columns(23).DataField=   "disneyemailsto"
      Columns(23).FieldLen=   256
      Columns(23).StyleSet=   "headerfield"
      Columns(24).Width=   2117
      Columns(24).Caption=   "wmls number"
      Columns(24).Name=   "wmlsnumber"
      Columns(24).DataField=   "wmlsnumber"
      Columns(24).FieldLen=   256
      Columns(24).StyleSet=   "headerfield"
      Columns(25).Width=   2117
      Columns(25).Caption=   "MD5done"
      Columns(25).Name=   "MD5done"
      Columns(25).DataField=   "MD5done"
      Columns(25).FieldLen=   256
      Columns(25).Style=   1
      Columns(25).StyleSet=   "stagefield"
      Columns(26).Width=   2117
      Columns(26).Caption=   "sent to diisney"
      Columns(26).Name=   "uploadtodisneydone"
      Columns(26).DataField=   "uploadtodisneydone"
      Columns(26).DataType=   7
      Columns(26).FieldLen=   256
      Columns(26).Style=   1
      Columns(26).StyleSet=   "stagefield"
      Columns(27).Width=   3200
      Columns(27).Visible=   0   'False
      Columns(27).Caption=   "src tapes returned"
      Columns(27).Name=   "videotapesreturned"
      Columns(27).DataField=   "videotapesreturned"
      Columns(27).DataType=   7
      Columns(27).FieldLen=   256
      Columns(27).Style=   1
      Columns(28).Width=   3200
      Columns(28).Visible=   0   'False
      Columns(28).Caption=   "snf tapes returned"
      Columns(28).Name=   "soundtapesreturned"
      Columns(28).DataField=   "soundtapesreturned"
      Columns(28).DataType=   7
      Columns(28).FieldLen=   256
      Columns(28).Style=   1
      Columns(29).Width=   2117
      Columns(29).Caption=   "work done"
      Columns(29).Name=   "workdone"
      Columns(29).Alignment=   2
      Columns(29).DataField=   "workdone"
      Columns(29).FieldLen=   256
      Columns(29).Style=   2
      Columns(29).StyleSet=   "conclusionfield"
      Columns(30).Width=   2117
      Columns(30).Caption=   "billed"
      Columns(30).Name=   "billed"
      Columns(30).Alignment=   2
      Columns(30).DataField=   "billed"
      Columns(30).FieldLen=   256
      Columns(30).Style=   2
      Columns(30).StyleSet=   "conclusionfield"
      Columns(31).Width=   4657
      Columns(31).Caption=   "notes"
      Columns(31).Name=   "notes"
      Columns(31).DataField=   "notes"
      Columns(31).FieldLen=   256
      Columns(31).StyleSet=   "headerfield"
      _ExtentX        =   49186
      _ExtentY        =   17701
      _StockProps     =   79
      Caption         =   "Tracker Items"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoComments 
      Height          =   330
      Left            =   1920
      Top             =   13680
      Visible         =   0   'False
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoComments"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdComments 
      Bindings        =   "frmDisneyTracker.frx":0084
      Height          =   1875
      Left            =   60
      TabIndex        =   6
      Top             =   12480
      Width           =   12495
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12582847
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   6
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "disneytrackercommentID"
      Columns(0).Name =   "disneytrackercommentID"
      Columns(0).DataField=   "disneytrackercommentID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "disneytrackerID"
      Columns(1).Name =   "disneytrackerID"
      Columns(1).DataField=   "disneytrackerID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   13070
      Columns(2).Caption=   "Comments"
      Columns(2).Name =   "comment"
      Columns(2).DataField=   "comment"
      Columns(2).FieldLen=   255
      Columns(3).Width=   3360
      Columns(3).Caption=   "Date"
      Columns(3).Name =   "cdate"
      Columns(3).DataField=   "cdate"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).Style=   1
      Columns(4).Width=   3519
      Columns(4).Caption=   "Entered By"
      Columns(4).Name =   "cuser"
      Columns(4).DataField=   "cuser"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   1005
      Columns(5).Caption=   "E-mail"
      Columns(5).Name =   "E-mail"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   4
      UseDefaults     =   0   'False
      _ExtentX        =   22040
      _ExtentY        =   3307
      _StockProps     =   79
      Caption         =   "Tracker Comments"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoContact 
      Height          =   330
      Left            =   60
      Top             =   14460
      Visible         =   0   'False
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoContact"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label lblNotification 
      Height          =   255
      Left            =   10920
      TabIndex        =   24
      Top             =   1200
      Width           =   4695
   End
   Begin VB.Label lblCaption 
      Caption         =   "Search Order Number"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   12
      Top             =   840
      Width           =   2175
   End
   Begin VB.Label lblCaption 
      Caption         =   "Search Job Number"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   11
      Top             =   480
      Width           =   2175
   End
   Begin VB.Label lblCaption 
      Caption         =   "Search Title"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   2175
   End
   Begin VB.Label lblTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   18360
      TabIndex        =   7
      Top             =   14040
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCompanyID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   19620
      TabIndex        =   5
      Top             =   14040
      Visible         =   0   'False
      Width           =   735
   End
End
Attribute VB_Name = "frmDisneyTracker"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strSearch As String
Dim m_strOrderby As String

Private Sub CheckWorkDone()

Dim l_strJobStatus As String, l_lngJobStatusNumber As Long

'Dim temp As Boolean
'
'temp = True
'
'If Val(grdItems.Columns("editneeded").Text) <> 0 And grdItems.Columns("editdone").Text = "" Then temp = False
'If Val(grdItems.Columns("slatechangeneeded").Text) <> 0 And grdItems.Columns("slatechangedone").Text = "" Then temp = False
'If Val(grdItems.Columns("uploadtodisneyneeded").Text) <> 0 And grdItems.Columns("uploadtodisneydone").Text = "" Then temp = False
'If Val(grdItems.Columns("itunesneeded").Text) <> 0 And grdItems.Columns("itunesuploaddone").Text = "" Then temp = False
'If Val(grdItems.Columns("digitaldeliveryneeded").Text) <> 0 And grdItems.Columns("sent").Text = "" Then temp = False
'If Val(grdItems.Columns("encodingneeded").Text) <> 0 And grdItems.Columns("encodedone").Text = "" Then temp = False
'If Val(grdItems.Columns("tapestobemade").Text) <> 0 And grdItems.Columns("tapesdone").Text = "" Then temp = False
'
'grdItems.Columns("workdone").Text = temp

If Val(grdItems.Columns("jobID").Text) <> 0 Then
    l_strJobStatus = GetData("job", "fd_status", "jobID", Val(grdItems.Columns("jobID").Text))
    l_lngJobStatusNumber = GetStatusNumber(l_strJobStatus)
    
    If l_lngJobStatusNumber >= 7 Then
        grdItems.Columns("workdone").Text = True
    End If
    
    If l_lngJobStatusNumber >= 9 Then
        grdItems.Columns("billed").Text = True
    Else
        grdItems.Columns("billed").Text = False
    End If
    
End If

End Sub

Private Sub cmdClear_Click()

ClearFields Me

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdNewOrder_Click()

'Positions for the Frame
'Top = 4440
'Left = 180

'frmDisneyOrderEntry.fraDADC.Visible = False
frmDisneyOrderEntry.fraNormal.Top = 4440
frmDisneyOrderEntry.fraNormal.Left = 180
'frmDisneyOrderEntry.fraNormal.Visible = True
frmDisneyOrderEntry.cmbCompany.Columns("companyID").Text = 1163
frmDisneyOrderEntry.cmbCompany.Columns("name").Text = GetData("company", "name", "companyID", 1163)
frmDisneyOrderEntry.lblCompanyID.Caption = "1163"
frmDisneyOrderEntry.txtClientStatus.Text = GetData("company", "accountstatus", "companyID", 1163)
frmDisneyOrderEntry.cmbCompany.Columns("accountstatus").Text = frmDisneyOrderEntry.txtClientStatus.Text
frmDisneyOrderEntry.cmbCompany.Visible = False
frmDisneyOrderEntry.lblCompanyID.Visible = False
frmDisneyOrderEntry.txtClientStatus.Visible = False
frmDisneyOrderEntry.lblCaption(16).Visible = False
frmDisneyOrderEntry.optOrderType(0).Value = True
frmDisneyOrderEntry.Show vbModal

End Sub

Private Sub cmdReport_Click()

Dim l_strSelectionFormula As String
Dim l_strReportFile As String

l_strSelectionFormula = "{disneytracker.companyID} = 1163 "

If optComplete(0).Value = True Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {disneytracker.materialinplace} = 0 AND {disneytracker.workdone} = 0 "
ElseIf optComplete(1).Value = True Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {disneytracker.materialinplace} <> 0 AND {disneytracker.workdone} = 0 "
ElseIf optComplete(2).Value = True Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {disneytracker.workdone} <> 0 AND {disneytracker.billed} = 0"
ElseIf optComplete(3).Value = True Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {disneytracker.billed} <> 0 "
ElseIf optComplete(4).Value = True Then
    'nothing
Else
    l_strSelectionFormula = l_strSelectionFormula & " AND {disneytracker.workdone} = 0 "
End If

If txtSearchTitle.Text <> "" Then l_strSelectionFormula = l_strSelectionFormula & " AND {disneytracker.title} LIKE '" & txtSearchTitle.Text & "*' "
If txtSearchJobID.Text <> "" Then l_strSelectionFormula = l_strSelectionFormula & " AND {disneytracker.jobID} = " & Val(txtSearchJobID.Text)
If txtSearchOrderNumber.Text <> "" Then l_strSelectionFormula = l_strSelectionFormula & " AND {disneytracker.ordernumber} LIKE '" & txtSearchOrderNumber.Text & "*' "

Debug.Print l_strSelectionFormula
l_strReportFile = g_strLocationOfCrystalReportFiles & "Disney_report.rpt"
PrintCrystalReport l_strReportFile, l_strSelectionFormula, True
    
End Sub

Private Sub cmdSearch_Click()

m_strSearch = " WHERE companyID = " & lblCompanyID.Caption

m_strOrderby = "ORDER BY CASE WHEN duedate IS NULL then 2 ELSE 1 END ASC, duedate ASC, disneytrackerID ASC "

If txtSearchTitle.Text <> "" Then m_strSearch = m_strSearch & " AND title LIKE '" & txtSearchTitle.Text & "%' "
If txtSearchJobID.Text <> "" Then m_strSearch = m_strSearch & " AND jobID = " & Val(txtSearchJobID.Text)
If txtSearchOrderNumber.Text <> "" Then m_strSearch = m_strSearch & " AND ordernumber LIKE '" & txtSearchOrderNumber.Text & "%' "

If optComplete(0).Value = True Then
    m_strSearch = m_strSearch & " AND materialsindate is null AND workdone = 0 "
ElseIf optComplete(1).Value = True Then
    m_strSearch = m_strSearch & " AND materialsindate is not null AND workdone = 0 "
ElseIf optComplete(2).Value = True Then
    m_strSearch = m_strSearch & " AND workdone <> 0 AND billed = 0 "
ElseIf optComplete(3).Value = True Then
    m_strSearch = m_strSearch & " AND billed <> 0 "
ElseIf optComplete(4).Value = True Then
    'nothing
Else
    m_strSearch = m_strSearch & " AND workdone = 0 "
End If

If lblCompanyID.Caption <> "" Then

    Dim l_strSQL As String
    
    adoItems.ConnectionString = g_strConnection
    adoItems.RecordSource = "SELECT * FROM disneytracker " & m_strSearch & l_strSQL & m_strOrderby & ";"
    adoItems.Refresh
    
    adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"

End If

End Sub

Private Sub cmdUpdateDueDate_Click()

Dim l_datNewDate As Date

If adoItems.Recordset.RecordCount <= 0 Then Exit Sub

frmEditDate.datDueDate.Value = Null

frmEditDate.Show vbModal

If Not IsNull(frmEditDate.datDueDate.Value) Then
    l_datNewDate = frmEditDate.datDueDate.Value
    If MsgBox("Changing Due Dates to " & l_datNewDate & vbCrLf & "Are you sure", vbYesNo, "Changing Due Dates") = vbYes Then
        adoItems.Recordset.MoveFirst
        Do While Not adoItems.Recordset.EOF
            adoItems.Recordset("duedate") = l_datNewDate
            adoItems.Recordset.Update
            adoItems.Recordset.MoveNext
        Loop
    End If
End If

Unload frmEditDate

End Sub

Private Sub cmdWorkStatusUpdate_Click()

Dim l_lngJobID As Long
Dim l_strJobStatus As String, l_lngJobStatusNumber As Long


If adoItems.Recordset.RecordCount > 0 Then
    With adoItems.Recordset
        .MoveFirst
        Do While Not .EOF
            If Val(Trim(" " & adoItems.Recordset("jobID"))) = 0 And Trim(" " & adoItems.Recordset("ordernumber")) <> "" Then
                If GetData("job", "jobID", "orderreference", adoItems.Recordset("ordernumber")) <> 0 Then
                    adoItems.Recordset("jobid") = GetData("job", "jobID", "orderreference", adoItems.Recordset("ordernumber"))
                End If
            ElseIf Val(Trim(" " & adoItems.Recordset("jobID"))) <> 0 And Trim(" " & adoItems.Recordset("ordernumber")) = "" Then
                If Val(GetData("Job", "orderreference", "jobid", Val(adoItems.Recordset("jobID")))) <> 0 Then
                    adoItems.Recordset("ordernumber") = GetData("Job", "orderreference", "jobid", Val(adoItems.Recordset("jobID")))
                End If
            End If
            
            CheckWorkDone
    '        CheckMaterialsInPlace
            .MoveNext
        Loop
    End With
End If

End Sub

Private Sub ddnContact_Click()

grdItems.Columns("disneycontact").Text = ddnContact.Columns("Name").Text
grdItems.Columns("disneycontactID").Text = ddnContact.Columns("contactID").Text

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

grdItems.StyleSets("headerfield").BackColor = &HE7FFE7
grdItems.StyleSets("stagefield").BackColor = &HE7FFFF
grdItems.StyleSets("conclusionfield").BackColor = &HFFFFE7

grdItems.StyleSets.Add "Error"
grdItems.StyleSets("Error").BackColor = &HA0A0FF

lblCompanyID.Caption = "1163"
cmdSearch.Value = True
'HideAllColumns

l_strSQL = "SELECT company.companyID, contact.contactID, contact.name, contact.telephone, employee.jobtitle, contact.email FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((company.companyID)=" & lblCompanyID.Caption & ")) AND contact.system_active = 1 ORDER BY contact.name;"
adoContact.ConnectionString = g_strConnection
adoContact.RecordSource = l_strSQL
adoContact.Refresh

grdItems.Columns("disneycontact").DropDownHwnd = ddnContact.hWnd

PopulateCombo "Users", ddnUsers
grdItems.Columns("jcacontact").DropDownHwnd = ddnUsers.hWnd

End Sub

Private Sub Form_Resize()

On Error Resume Next

grdItems.Width = Me.ScaleWidth - grdItems.Left - 120
grdItems.Height = (Me.ScaleHeight - grdItems.Top - frmButtons.Height) * 0.75 - 240
frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120
grdComments.Top = grdItems.Top + grdItems.Height + 120
grdComments.Height = frmButtons.Top - grdComments.Top - 120
grdComments.Width = grdItems.Width

End Sub

Private Sub grdComments_BeforeUpdate(Cancel As Integer)

grdComments.Columns("disneytrackerID").Text = grdItems.Columns("disneytrackerID").Text
If grdComments.Columns("cdate").Text = "" Then
    grdComments.Columns("cdate").Text = Now
End If
grdComments.Columns("cuser").Text = g_strFullUserName

End Sub

Private Sub grdComments_BtnClick()

Dim l_strOurEmailContact As String, l_strOurContactName As String, l_strEmailBody As String

Dim l_rstWhoToEmail As ADODB.Recordset

If MsgBox("Send Email?", vbYesNo, "Automatic Email") = vbYes Then
    
    l_strEmailBody = "A Disney Tracker comment was created " & _
        "By: " & grdComments.Columns("cuser").Text & vbCrLf & _
        "Title: " & grdItems.Columns("title").Text & ", Episode: " & grdItems.Columns("Episode").Text & vbCrLf & _
        "JobID: " & grdItems.Columns("jobID").Text & ", Order Number: " & grdItems.Columns("ordernumber").Text & vbCrLf & _
        "Comment: " & grdComments.Columns("comment").Text & vbCrLf
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND trackermessageID = 14;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
            lblNotification.Caption = "E-mailing " & l_rstWhoToEmail("fullname")
            DoEvents
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "Disney Tracker Comment Created", "", l_strEmailBody, True, "", ""
            l_rstWhoToEmail.MoveNext
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
    lblNotification.Caption = ""
    DoEvents
    
    Beep
    
End If

End Sub

Private Sub grdItems_BeforeUpdate(Cancel As Integer)

grdItems.Columns("companyID").Text = lblCompanyID.Caption

If grdItems.Columns("jobID").Text <> "" Then
    grdItems.Columns("ordernumber").Text = GetData("Job", "orderreference", "jobid", Val(grdItems.Columns("jobID").Text))
End If

CheckWorkDone

'If Val(" " & grdItems.Columns("materialinplace").Text) = 0 And Trim(" " & grdItems.Columns("materialsindate").Text) <> "" And Format(grdItems.Columns("materialsindate").Text, "yyyymmdd") < Format(Now, "yyyymmdd") Then
'    grdItems.Columns("materialsindate").CellStyleSet "Error"
'    grdItems.Columns("materialinplace").CellStyleSet "Error"
'Else
'    grdItems.Columns("materialsindate").CellStyleSet ""
'    grdItems.Columns("materialinplace").CellStyleSet ""
'End If
'
'If Val(" " & grdItems.Columns("workdone").Text) = 0 And Trim(" " & grdItems.Columns("duedate").Text) <> "" And Format(grdItems.Columns("duedate").Text, "yyyymmdd") < Format(Now, "yyyymmdd") Then
'    grdItems.Columns("duedate").CellStyleSet "Error"
'    grdItems.Columns("workdone").CellStyleSet "Error"
'Else
'    grdItems.Columns("duedate").CellStyleSet ""
'    grdItems.Columns("workdone").CellStyleSet ""
'End If

End Sub

Private Sub grdItems_BtnClick()

Dim tempdate As String

If grdItems.ActiveCell.Text <> "" Then
    grdItems.ActiveCell.Text = ""
Else
    tempdate = FormatDateTime(Now, vbLongDate)
    grdItems.ActiveCell.Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
End If

End Sub

Private Sub grdItems_DblClick()

If Val(grdItems.Columns("jobID").Text) <> 0 Then
    ShowJob Val(grdItems.Columns("jobID").Text), 1, False
'    ShowJob Val(grdItems.Columns("jobID").Text), 1, False
End If
End Sub

Private Sub grdItems_HeadClick(ByVal ColIndex As Integer)

'm_strOrderby = " ORDER BY " & grdItems.Columns(ColIndex).Name
'adoItems.RecordSource = "SELECT * FROM disneytracker " & m_strSearch & m_strOrderby
'adoItems.Refresh

End Sub

Private Sub grdItems_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

lblTrackeritemID.Caption = grdItems.Columns("disneytrackerID").Text

If lblTrackeritemID.Caption <> "" Then
    adoComments.RecordSource = "SELECT * FROM disneytrackercomment WHERE disneytrackerID = " & Val(lblTrackeritemID.Caption) & " ORDER BY cdate ASC;"
    adoComments.ConnectionString = g_strConnection
    adoComments.Refresh
Else
    adoComments.RecordSource = "SELECT * FROM disneytrackercomment WHERE disneytrackerID = -1 ORDER BY cdate ASC;"
    adoComments.ConnectionString = g_strConnection
    adoComments.Refresh
End If

End Sub

Private Sub grdItems_RowLoaded(ByVal Bookmark As Variant)

If Val(grdItems.Columns("disneycontactID").Text) <> 0 Then
    grdItems.Columns("disneycontact").Text = GetData("contact", "name", "contactID", Val(grdItems.Columns("disneycontactID").Text))
End If

If Trim(" " & grdItems.Columns("materialsindate").Text) <> "" And Format(grdItems.Columns("materialsindate").Text, "yyyymmdd") < Format(Now, "yyyymmdd") Then
    grdItems.Columns("materialsindate").CellStyleSet "Error"
Else
    grdItems.Columns("materialsindate").CellStyleSet ""
End If

If Val(" " & grdItems.Columns("workdone").Text) = 0 And Trim(" " & grdItems.Columns("duedate").Text) <> "" And Format(grdItems.Columns("duedate").Text, "yyyymmdd") < Format(Now, "yyyymmdd") Then
    grdItems.Columns("duedate").CellStyleSet "Error"
    grdItems.Columns("workdone").CellStyleSet "Error"
Else
    grdItems.Columns("duedate").CellStyleSet ""
    grdItems.Columns("workdone").CellStyleSet ""
End If

'adoComments.RecordSource = "SELECT * FROM disneytrackercomment WHERE disneytrackerID = " & Val(grdItems.Columns("disneytrackerID").Text) & " ORDER BY cdate ASC;"
'adoComments.ConnectionString = g_strConnection
'adoComments.Refresh
'
End Sub

Private Sub optComplete_Click(Index As Integer)

cmdSearch.Value = True

End Sub

