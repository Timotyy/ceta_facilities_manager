VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCompleteJob 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Complete VT Job"
   ClientHeight    =   8220
   ClientLeft      =   1980
   ClientTop       =   6570
   ClientWidth     =   7230
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8220
   ScaleWidth      =   7230
   Begin TabDlg.SSTab tabComplete 
      Height          =   5415
      Left            =   2400
      TabIndex        =   34
      Top             =   2280
      Width           =   4755
      _ExtentX        =   8387
      _ExtentY        =   9551
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Physical Media Details"
      TabPicture(0)   =   "frmCompleteJob.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblCaption(3)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblCaption(12)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblCaption(5)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Line2(0)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblCaption(7)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblFormat"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Line2(5)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "lblCaption(14)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "lblCaption(13)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "lblCaption(11)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "lblCaption(10)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "Line2(1)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "Line2(6)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "lblCaption(19)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "lblCaption(9)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "lblCaption(15)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "cmbDestinationMachine"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "cmbSourceMachine"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "cmbStockType"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "chkConversion"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "txtNotes"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "txtQuantity"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "chkArc"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "chkFlash"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "chkLegal"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "chkFullReview"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "chkSpotCheck"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "txtDownTime"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).Control(28)=   "txtBarcode"
      Tab(0).Control(28).Enabled=   0   'False
      Tab(0).Control(29)=   "cmdIssueNext"
      Tab(0).Control(29).Enabled=   0   'False
      Tab(0).ControlCount=   30
      TabCaption(1)   =   "Digital File Details"
      TabPicture(1)   =   "frmCompleteJob.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).ControlCount=   0
      Begin VB.CommandButton cmdIssueNext 
         Caption         =   "Issue Next"
         Height          =   315
         Left            =   3540
         TabIndex        =   47
         Top             =   4860
         Width           =   1035
      End
      Begin VB.TextBox txtBarcode 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1320
         TabIndex        =   17
         Top             =   4920
         Width           =   1935
      End
      Begin VB.TextBox txtDownTime 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   1320
         TabIndex        =   14
         Top             =   3660
         Width           =   1455
      End
      Begin VB.CheckBox chkSpotCheck 
         Caption         =   "&Spot Check"
         Enabled         =   0   'False
         Height          =   195
         Left            =   1320
         TabIndex        =   15
         Top             =   4020
         Width           =   1155
      End
      Begin VB.CheckBox chkFullReview 
         Caption         =   "Full &Review"
         Enabled         =   0   'False
         Height          =   195
         Left            =   2880
         TabIndex        =   16
         Top             =   4020
         Width           =   1155
      End
      Begin VB.CheckBox chkLegal 
         Alignment       =   1  'Right Justify
         Caption         =   "&Legal"
         Height          =   195
         Left            =   3360
         TabIndex        =   9
         Top             =   1020
         Width           =   1155
      End
      Begin VB.CheckBox chkFlash 
         Alignment       =   1  'Right Justify
         Caption         =   "&Flash"
         Height          =   195
         Left            =   3360
         TabIndex        =   8
         Top             =   720
         Width           =   1155
      End
      Begin VB.CheckBox chkArc 
         Alignment       =   1  'Right Justify
         Caption         =   "&ARC"
         Height          =   195
         Left            =   3360
         TabIndex        =   7
         Top             =   420
         Width           =   1155
      End
      Begin VB.TextBox txtQuantity 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   1320
         TabIndex        =   6
         Top             =   1140
         Width           =   1455
      End
      Begin VB.TextBox txtNotes 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   675
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   11
         Top             =   1920
         Width           =   4455
      End
      Begin VB.CheckBox chkConversion 
         Alignment       =   1  'Right Justify
         Caption         =   "Conversion"
         Height          =   195
         Left            =   3360
         TabIndex        =   10
         Top             =   1320
         Width           =   1155
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbStockType 
         Height          =   255
         Left            =   1320
         TabIndex        =   5
         ToolTipText     =   "Our contact on this job"
         Top             =   780
         Width           =   1455
         BevelWidth      =   0
         DataFieldList   =   "Column 0"
         BevelType       =   0
         _Version        =   196617
         DataMode        =   2
         BorderStyle     =   0
         ColumnHeaders   =   0   'False
         BeveColorScheme =   1
         BevelColorFrame =   -2147483644
         CheckBox3D      =   0   'False
         BackColorOdd    =   16761024
         RowHeight       =   423
         Columns(0).Width=   4868
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "description"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   2566
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSourceMachine 
         Height          =   255
         Left            =   1320
         TabIndex        =   12
         ToolTipText     =   "Our contact on this job"
         Top             =   2820
         Width           =   2895
         BevelWidth      =   0
         DataFieldList   =   "Column 0"
         BevelType       =   0
         _Version        =   196617
         DataMode        =   2
         BorderStyle     =   0
         ColumnHeaders   =   0   'False
         BeveColorScheme =   1
         BevelColorFrame =   -2147483644
         CheckBox3D      =   0   'False
         BackColorOdd    =   16761024
         RowHeight       =   423
         Columns(0).Width=   4868
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "description"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   5106
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
         Enabled         =   0   'False
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbDestinationMachine 
         Height          =   255
         Left            =   1320
         TabIndex        =   13
         ToolTipText     =   "Our contact on this job"
         Top             =   3180
         Width           =   2895
         BevelWidth      =   0
         DataFieldList   =   "Column 0"
         BevelType       =   0
         _Version        =   196617
         DataMode        =   2
         BorderStyle     =   0
         ColumnHeaders   =   0   'False
         BeveColorScheme =   1
         BevelColorFrame =   -2147483644
         CheckBox3D      =   0   'False
         BackColorOdd    =   16761024
         RowHeight       =   423
         Columns(0).Width=   4868
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "description"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   5106
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
         Enabled         =   0   'False
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblCaption 
         Caption         =   "If you want to make a library record of this item, enter the barcode below:"
         Enabled         =   0   'False
         Height          =   375
         Index           =   15
         Left            =   120
         TabIndex        =   46
         Top             =   4440
         Width           =   4455
      End
      Begin VB.Label lblCaption 
         Caption         =   "Barcode"
         Height          =   195
         Index           =   9
         Left            =   120
         TabIndex        =   45
         Top             =   4920
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Q.C."
         Height          =   195
         Index           =   19
         Left            =   120
         TabIndex        =   44
         Top             =   4020
         Width           =   915
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000010&
         Index           =   6
         X1              =   120
         X2              =   4620
         Y1              =   2700
         Y2              =   2700
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000010&
         Index           =   1
         X1              =   120
         X2              =   4620
         Y1              =   3540
         Y2              =   3540
      End
      Begin VB.Label lblCaption 
         Caption         =   "Source M/C"
         Enabled         =   0   'False
         Height          =   195
         Index           =   10
         Left            =   120
         TabIndex        =   43
         Top             =   2820
         Width           =   915
      End
      Begin VB.Label lblCaption 
         Caption         =   "Dest M/C"
         Enabled         =   0   'False
         Height          =   195
         Index           =   11
         Left            =   120
         TabIndex        =   42
         Top             =   3180
         Width           =   915
      End
      Begin VB.Label lblCaption 
         Caption         =   "Down Time"
         Height          =   195
         Index           =   13
         Left            =   120
         TabIndex        =   41
         Top             =   3660
         Width           =   915
      End
      Begin VB.Label lblCaption 
         Caption         =   "minutes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   14
         Left            =   2940
         TabIndex        =   40
         Top             =   3660
         Width           =   1035
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000010&
         Index           =   5
         X1              =   120
         X2              =   4620
         Y1              =   4320
         Y2              =   4320
      End
      Begin VB.Label lblFormat 
         Caption         =   "FORMAT"
         Height          =   255
         Left            =   1320
         TabIndex        =   39
         Top             =   420
         Width           =   1455
      End
      Begin VB.Label lblCaption 
         Caption         =   "Format"
         Height          =   195
         Index           =   7
         Left            =   120
         TabIndex        =   38
         Tag             =   "fO"
         Top             =   420
         Width           =   915
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000010&
         Index           =   0
         X1              =   120
         X2              =   4620
         Y1              =   1620
         Y2              =   1620
      End
      Begin VB.Label lblCaption 
         Caption         =   "Quantity"
         Height          =   195
         Index           =   5
         Left            =   120
         TabIndex        =   37
         Top             =   1140
         Width           =   915
      End
      Begin VB.Label lblCaption 
         Caption         =   "Stock type"
         Height          =   195
         Index           =   12
         Left            =   120
         TabIndex        =   36
         Top             =   780
         Width           =   915
      End
      Begin VB.Label lblCaption 
         Caption         =   "Notes / Problems with job"
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   35
         Top             =   1680
         Width           =   4455
      End
   End
   Begin VB.TextBox txtItems 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   3720
      TabIndex        =   4
      Top             =   1920
      Width           =   1455
   End
   Begin VB.TextBox txtDuration 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   3720
      TabIndex        =   1
      ToolTipText     =   "Operator duration (how long did this job take?)"
      Top             =   1200
      Width           =   615
   End
   Begin VB.TextBox txtRunningTimeMinutes 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   3720
      TabIndex        =   2
      Top             =   1560
      Width           =   615
   End
   Begin VB.TextBox txtRunningTimeSeconds 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   4560
      TabIndex        =   3
      Top             =   1560
      Width           =   615
   End
   Begin VB.CommandButton cmdComplete 
      Caption         =   "Complete"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2400
      TabIndex        =   18
      Top             =   7800
      Width           =   1155
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   315
      Left            =   6000
      TabIndex        =   19
      ToolTipText     =   "Close this form"
      Top             =   7800
      Width           =   1155
   End
   Begin VB.TextBox txtCompletedDate 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   120
      TabIndex        =   26
      Top             =   7320
      Width           =   2175
   End
   Begin VB.CommandButton cmdLoadCompletionDetails 
      Caption         =   "LOAD"
      Height          =   255
      Left            =   6060
      TabIndex        =   24
      Top             =   60
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstOperator 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      Height          =   5490
      Left            =   120
      TabIndex        =   0
      Top             =   1680
      Width           =   2175
   End
   Begin VB.TextBox txtJobDetails 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      Height          =   675
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   20
      Top             =   420
      Width           =   7035
   End
   Begin VB.Label lblCaption 
      Caption         =   "no. of events / progs etc"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   8
      Left            =   5340
      TabIndex        =   33
      Top             =   1920
      Width           =   1635
   End
   Begin VB.Label lblCaption 
      Caption         =   "Items"
      Height          =   195
      Index           =   6
      Left            =   2520
      TabIndex        =   32
      Top             =   1920
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "minutes"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   4
      Left            =   4440
      TabIndex        =   31
      Top             =   1200
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Operator Time"
      Height          =   195
      Index           =   2
      Left            =   2520
      TabIndex        =   30
      Top             =   1200
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Running Time"
      Height          =   195
      Index           =   16
      Left            =   2520
      TabIndex        =   29
      Top             =   1560
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "minutes : seconds"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   17
      Left            =   5340
      TabIndex        =   28
      Top             =   1560
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   ":"
      Height          =   195
      Index           =   18
      Left            =   4440
      TabIndex        =   27
      Top             =   1560
      Width           =   135
   End
   Begin VB.Label lblJobID 
      Caption         =   "JobID"
      ForeColor       =   &H00C0C0C0&
      Height          =   255
      Left            =   180
      TabIndex        =   25
      Top             =   7740
      Visible         =   0   'False
      Width           =   1395
   End
   Begin VB.Label lblJobDetailID 
      Caption         =   "JobDetailID"
      ForeColor       =   &H00C0C0C0&
      Height          =   255
      Left            =   1320
      TabIndex        =   23
      Top             =   7740
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job Details as text"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   120
      TabIndex        =   22
      Top             =   120
      Width           =   1815
   End
   Begin VB.Label lblCaption 
      Caption         =   "Please highlight your name on the list below..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   120
      TabIndex        =   21
      Top             =   1200
      Width           =   2175
   End
End
Attribute VB_Name = "frmCompleteJob"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub chkArc_Click()
UpdateDescription
End Sub

Private Sub chkConversion_Click()
UpdateDescription
End Sub

Private Sub chkFlash_Click()
UpdateDescription
End Sub

Private Sub chkLegal_Click()
UpdateDescription
End Sub

Private Sub cmbStockType_Change()
UpdateDescription
End Sub

Private Sub cmbStockType_Click()
UpdateDescription
End Sub

Private Sub cmdCancel_Click()
Me.Tag = "CANCELLED"
Me.Hide

End Sub

Private Sub cmdComplete_Click()


If tabComplete.Enabled = False Then
    MsgBox "Please select your name from the list of operators", vbExclamation
    lstOperator.SetFocus
    Exit Sub
End If

'check valid information has been filled in
If Not IsNumeric(txtDuration.Text) Or txtDuration.Text = "" Then
    MsgBox "Please ensure you enter a valid duration (must be a round number of minutes)", vbExclamation
    txtDuration.SetFocus
    Exit Sub
End If

If Not IsNumeric(txtRunningTimeMinutes.Text) Or txtRunningTimeMinutes.Text = "" Then
    MsgBox "Please ensure you enter a valid running time (Minutes)", vbExclamation
    txtRunningTimeMinutes.SetFocus
    Exit Sub
End If

If (Not IsNumeric(txtRunningTimeSeconds.Text) Or txtRunningTimeSeconds.Text = "") And txtRunningTimeSeconds.Text <> "0" And txtRunningTimeSeconds.Text <> "00" Then
    MsgBox "Please ensure you enter a valid running time (Seconds)", vbExclamation
    txtRunningTimeSeconds.SetFocus
    Exit Sub
End If

If cmbStockType.Text = "" Then
    MsgBox "Please ensure you select a valid stock type", vbExclamation
    cmbStockType.SetFocus
    Exit Sub
End If

If Not IsNumeric(txtItems.Text) Or txtItems.Text = "" Then
    MsgBox "Please ensure you enter a valid item count", vbExclamation
    txtItems.SetFocus
    Exit Sub
End If

If Not IsNumeric(txtQuantity.Text) Or txtQuantity.Text = "" Then
    MsgBox "Please ensure you enter a valid copy quantity", vbExclamation
    txtQuantity.SetFocus
    Exit Sub
End If

If txtCompletedDate.Text <> "" Then
    Dim l_intMsg As Integer
    l_intMsg = MsgBox("This job has already been completed. Are you sure you want to re-complete it and overwrite the completion details?", vbYesNo + vbQuestion + vbDefaultButton2, "TASK ALREADY COMPLETE")
    If l_intMsg = vbNo Then Exit Sub
End If

Me.Hide

If txtBarcode.Text <> "" Then ShowLibrary GetData("library", "libraryID", "barcode", txtBarcode.Text)

End Sub



Private Sub cmdIssueNext_Click()


If cmbStockType.Text = "" Then
    MsgBox "Please ensure you select a valid stock type", vbExclamation
    cmbStockType.SetFocus
    Exit Sub
End If


Dim l_intMsg As Integer
l_intMsg = MsgBox("Do you want to create a new library record, using the next available barcode number?", vbQuestion + vbYesNo)

If l_intMsg = vbYes Then

    cmdIssueNext.Caption = "Generating"
    DoEvents

    Dim l_strNextBarcode As String
    l_strNextBarcode = Format(GetNextSequence("barcodenumber"), "00000")

    txtBarcode.Text = l_strNextBarcode

    DoEvents

    Dim l_lngLibraryID As Long
    l_lngLibraryID = CreateLibraryItem(l_strNextBarcode, "NEW ITEM")
    
    'save the basic details
    SetData "library", "stocktype", "libraryID", l_lngLibraryID, CStr(cmbStockType.Text)
    SetData "library", "format", "libraryID", l_lngLibraryID, CStr(lblFormat.Caption)
    
    txtBarcode_KeyPress vbKeyReturn

    Me.Caption = "Issue Next"
    DoEvents

End If

End Sub

Private Sub cmdLoadCompletionDetails_Click()
'this fills this form in with details from the jobdetail item

lblJobID.Caption = GetData("jobdetail", "jobID", "jobdetailID", lblJobDetailID.Caption)
txtBarcode.Text = GetData("jobdetail", "librarybarcode", "jobdetailID", lblJobDetailID.Caption)
lblFormat.Caption = GetData("jobdetail", "format", "jobdetailID", lblJobDetailID.Caption)
txtJobDetails.Text = GetData("jobdetail", "quantity", "jobdetailID", lblJobDetailID.Caption) & " x " & GetData("jobdetail", "format", "jobdetailID", lblJobDetailID.Caption)
txtDuration.Text = 0 'GetData("jobdetail", "runningtime", "jobdetailID", lblJobDetailID.Caption)

Dim l_strRunningTime As String
l_strRunningTime = GetData("jobdetail", "runningtime", "jobdetailID", lblJobDetailID.Caption)

If InStr(l_strRunningTime, ":") <> 0 Then
    txtRunningTimeMinutes.Text = Left(l_strRunningTime, InStr(l_strRunningTime, ":") - 1)
    txtRunningTimeSeconds.Text = Right(l_strRunningTime, InStr(l_strRunningTime, ":") - 1)
Else
    If IsNumeric(l_strRunningTime) Then
        If InStr(l_strRunningTime, ".") Then
            txtRunningTimeMinutes.Text = Left(l_strRunningTime, InStr(l_strRunningTime, ".") - 1)
            txtRunningTimeSeconds.Text = Right(l_strRunningTime, InStr(l_strRunningTime, ".") - 1)
        Else
            txtRunningTimeMinutes.Text = l_strRunningTime
            txtRunningTimeSeconds.Text = "00"
        End If
    Else
        txtRunningTimeMinutes.Text = "00"
        txtRunningTimeSeconds.Text = "00"
    End If
End If

txtQuantity.Text = GetData("jobdetail", "quantity", "jobdetailID", lblJobDetailID.Caption)
txtCompletedDate.Text = GetData("completed", "completeddate", "jobdetailID", lblJobDetailID.Caption)

PopulateCombo "stockcodesforformat" & lblFormat.Caption, cmbStockType

If txtCompletedDate.Text = "0" Then txtCompletedDate.Text = ""
If txtRunningTimeMinutes.Text = "0" Then txtRunningTimeMinutes.Text = ""
If txtRunningTimeSeconds.Text = "0" Then txtRunningTimeSeconds.Text = ""
If txtDuration.Text = "0" Then txtDuration.Text = ""

'this job is completed already
If txtCompletedDate.Text <> "" Then

        tabComplete.Enabled = False
        
        txtQuantity.Text = GetData("completed", "quantity", "jobdetailID", lblJobDetailID.Caption)
        txtDuration.Text = GetData("completed", "operatorduration", "jobdetailID", lblJobDetailID.Caption)
        txtItems.Text = GetData("completed", "itemcount", "jobdetailID", lblJobDetailID.Caption)
        txtNotes.Text = GetData("completed", "notes", "jobdetailID", lblJobDetailID.Caption)
        cmbStockType.Text = GetData("completed", "stocktype", "jobdetailID", lblJobDetailID.Caption)
        cmbSourceMachine.Text = GetData("completed", "sourcemachine", "jobdetailID", lblJobDetailID.Caption)
        cmbDestinationMachine.Text = GetData("completed", "destinationmachine", "jobdetailID", lblJobDetailID.Caption)
        txtDownTime.Text = GetData("completed", "downtime", "jobdetailID", lblJobDetailID.Caption)
        chkArc.Value = GetFlag(GetData("completed", "usedarc", "jobdetailID", lblJobDetailID.Caption))
        chkFlash.Value = GetFlag(GetData("completed", "usedflash", "jobdetailID", lblJobDetailID.Caption))
        chkLegal.Value = GetFlag(GetData("completed", "usedlegal", "jobdetailID", lblJobDetailID.Caption))
        chkConversion.Value = GetFlag(GetData("completed", "usedconversion", "jobdetailID", lblJobDetailID.Caption))
        lstOperator.Text = GetData("completed", "completeduser", "jobdetailID", lblJobDetailID.Caption)
        
        tabComplete.Enabled = True
        
        UpdateDescription
        
        'if the jobs status is not completed or above...
        If GetStatusNumber(lblJobID.Caption) < 3 Then
            tabComplete.Enabled = True
        Else
            tabComplete.Enabled = False
            MsgBox "You can not update the completion details because the status of the job has passed the completed stage.", vbInformation
        End If
        
End If


End Sub

Sub UpdateDescription()

If tabComplete.Enabled = False Then Exit Sub

txtJobDetails.Text = lstOperator.List(lstOperator.ListIndex) & " made " & txtQuantity.Text & " x " & cmbStockType.Text & " copies, with " & txtItems.Text & " items on each."
If chkArc.Value <> 0 Then txtJobDetails.Text = txtJobDetails.Text & " Also used ARC."
If chkLegal.Value <> 0 Then txtJobDetails.Text = txtJobDetails.Text & " Also used Legaliser."
If chkFlash.Value <> 0 Then txtJobDetails.Text = txtJobDetails.Text & " Also used Flash."
If chkConversion.Value <> 0 Then txtJobDetails.Text = txtJobDetails.Text & " Plus standards conversion."
txtJobDetails.Text = txtJobDetails.Text & " Op. Time: " & txtDuration.Text & " mins."
txtJobDetails.Text = txtJobDetails.Text & " R/T: " & Format(txtRunningTimeMinutes.Text, "00") & ":" & Format(txtRunningTimeSeconds.Text, "00")
If txtNotes.Text <> "" Then txtJobDetails.Text = txtJobDetails.Text & " (" & txtNotes.Text & ")"


End Sub

Private Sub Form_Load()

    MakeLookLikeOffice Me

   
    PopulateCombo "operator", lstOperator

    PopulateCombo "machine", cmbSourceMachine
    PopulateCombo "machine", cmbDestinationMachine
    
    Me.MousePointer = vbDefault
        
    CenterForm Me
End Sub


Private Sub lstOperator_Click()

tabComplete.Enabled = True

UpdateDescription

'txtDuration.SetFocus

End Sub

Private Sub txtBarcode_GotFocus()
HighLite txtBarcode
End Sub

Private Sub txtBarcode_KeyPress(KeyAscii As Integer)
'this is where we want to update a tape and add the details.

If KeyAscii = vbKeyReturn Then
    
    KeyAscii = 0
    
    Dim l_intMsg As Integer
    Dim l_lngLibraryID As Long
    Dim l_strStockCode As String
    Dim l_strSQL As String
    Dim l_strFormat As String
    
    'get the library ID from the barcode
    l_lngLibraryID = GetData("library", "libraryID", "barcode", txtBarcode.Text)
    
    'is there such a barcode?
    If l_lngLibraryID = 0 Then
        MsgBox "This barcode does not exist in your library database.", vbExclamation
        txtBarcode.Text = ""
        Beep
        Exit Sub
    End If

    'compare the stock type and pick up the format for that
    l_strStockCode = GetData("library", "stocktype", "libraryID", l_lngLibraryID)
    l_strFormat = GetData("library", "format", "libraryID", l_lngLibraryID)
    
    If LCase(cmdIssueNext.Caption) <> "generating" Then
        l_intMsg = MsgBox("Do you want to copy this job's information to the media record you have scanned?", vbYesNo + vbQuestion)
    Else
        l_intMsg = vbYes
    End If
        
    Select Case l_intMsg
    Case vbYes
        'add job information to library record
        UpdateLibraryRecordWithJobDetailInfo l_lngLibraryID, Val(lblJobID.Caption), Val(lblJobDetailID.Caption), , , , "ALL"
        'UpdateLibraryRecordWithJobDetailInfo Val(lblLibraryID.Caption), Val(txtJobID.Text), 0
    'Case 1
    '    UpdateLibraryRecordWithJobDetailInfo l_lngLibraryID, Val(lblJobID.Caption), Val(lblJobDetailID.Caption), , , , "THIS"
    Case Else
        txtBarcode.Text = ""
        MsgBox "You have chosen not to allocate this barcode to this job detail row", vbExclamation
        Exit Sub

    End Select
    
    'for this to work, the user must be set up with the exact same name in both tables
    Dim l_strUserInitials As String
    l_strUserInitials = GetData("cetauser", "initials", "fullname", lstOperator.Text)
    
    Do Until l_strUserInitials <> ""
        If l_strUserInitials = "" Then l_strUserInitials = UCase(InputBox("Please enter your initials"))
    Loop
    
    'update the library record's details
    l_strSQL = "UPDATE library SET format = '" & lblFormat.Caption & "', location = 'VT', mdate = '" & FormatSQLDate(Now) & "', muser = '" & l_strUserInitials & "' WHERE libraryID = '" & l_lngLibraryID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'make a transactions noting that the library item is now in VT
    CreateLibraryTransaction l_lngLibraryID, "VT", 0, l_strUserInitials
    
    'update the job detail line and set the barcode
    l_strSQL = "UPDATE jobdetail SET librarybarcode = '" & txtBarcode.Text & "' WHERE jobdetailID = '" & lblJobDetailID.Caption & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError

End If

End Sub

Private Sub txtCompletedDate_GotFocus()
HighLite txtCompletedDate
End Sub

Private Sub txtDownTime_GotFocus()
HighLite txtDownTime
End Sub

Private Sub txtDuration_Change()
UpdateDescription
End Sub

Private Sub txtDuration_GotFocus()
HighLite txtDuration
End Sub

Private Sub txtItems_Change()
UpdateDescription
End Sub

Private Sub txtItems_GotFocus()
HighLite txtItems
End Sub

Private Sub txtJobDetails_KeyPress(KeyAscii As Integer)
HighLite txtJobDetails
End Sub

Private Sub txtNotes_Change()
UpdateDescription
End Sub

Private Sub txtNotes_GotFocus()
HighLite txtNotes
End Sub

Private Sub txtQuantity_Change()
UpdateDescription
End Sub

Private Sub txtQuantity_GotFocus()
HighLite txtQuantity
End Sub

Private Sub txtRunningTimeMinutes_Change()
UpdateDescription
End Sub

Private Sub txtRunningTimeMinutes_GotFocus()
HighLite txtRunningTimeMinutes
End Sub

Private Sub txtRunningTimeSeconds_Change()
UpdateDescription
End Sub

Private Sub txtRunningTimeSeconds_GotFocus()
HighLite txtRunningTimeSeconds
End Sub
