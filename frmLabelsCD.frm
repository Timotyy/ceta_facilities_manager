VERSION 5.00
Begin VB.Form frmLabelsCD 
   Caption         =   "CD Labels"
   ClientHeight    =   5295
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6120
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLabelsCD.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5295
   ScaleWidth      =   6120
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox Text1 
      Alignment       =   1  'Right Justify
      Height          =   315
      Index           =   14
      Left            =   2220
      TabIndex        =   33
      Text            =   "1"
      ToolTipText     =   "Number of individual VHS Label sets to be printed"
      Top             =   3720
      Width           =   435
   End
   Begin VB.Timer timFormClose 
      Interval        =   20000
      Left            =   240
      Top             =   4680
   End
   Begin VB.TextBox Text1 
      BackColor       =   &H00FFC0C0&
      Height          =   315
      Index           =   13
      Left            =   4380
      TabIndex        =   31
      ToolTipText     =   "Job Number for the CD Labels"
      Top             =   120
      Width           =   1635
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   12
      Left            =   3120
      TabIndex        =   27
      Top             =   960
      Width           =   1035
   End
   Begin VB.CheckBox Check1 
      Alignment       =   1  'Right Justify
      Caption         =   "Don't Print Barcodes"
      Height          =   255
      Left            =   90
      TabIndex        =   26
      Top             =   4140
      Width           =   2595
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   11
      Left            =   4980
      TabIndex        =   23
      Top             =   960
      Width           =   1035
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   10
      Left            =   1200
      TabIndex        =   22
      Top             =   960
      Width           =   1035
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   4860
      TabIndex        =   21
      ToolTipText     =   "Close the form"
      Top             =   4140
      Width           =   1155
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print Labels"
      Height          =   315
      Left            =   3600
      TabIndex        =   20
      ToolTipText     =   "Print the CD Labels (Needs Manual Feeding of Stock into Printer)"
      Top             =   4140
      Width           =   1155
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   9
      Left            =   4380
      TabIndex        =   9
      Top             =   3060
      Width           =   1635
   End
   Begin VB.TextBox Text1 
      Enabled         =   0   'False
      Height          =   315
      Index           =   8
      Left            =   4380
      TabIndex        =   8
      Top             =   2640
      Width           =   1635
   End
   Begin VB.TextBox Text1 
      Enabled         =   0   'False
      Height          =   315
      Index           =   7
      Left            =   4380
      TabIndex        =   7
      ToolTipText     =   "Master Number for the CD Labels"
      Top             =   2220
      Width           =   1635
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   6
      Left            =   1200
      TabIndex        =   6
      ToolTipText     =   "A1 Content for the CD Labels"
      Top             =   3060
      Width           =   1635
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   5
      Left            =   1200
      TabIndex        =   5
      ToolTipText     =   "A1 Content for the CD Labels"
      Top             =   2640
      Width           =   1635
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   4
      Left            =   1200
      TabIndex        =   4
      ToolTipText     =   "Standard for the CD Labels"
      Top             =   2220
      Width           =   1635
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   3
      Left            =   1200
      TabIndex        =   3
      ToolTipText     =   "Version for the CD Labels"
      Top             =   1800
      Width           =   4815
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   2
      Left            =   1200
      TabIndex        =   2
      ToolTipText     =   "SubTitle for the CD Labels"
      Top             =   1380
      Width           =   4815
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   1
      Left            =   1200
      TabIndex        =   1
      ToolTipText     =   "Title for the CD Labels"
      Top             =   540
      Width           =   4815
   End
   Begin VB.TextBox Text1 
      BackColor       =   &H00FFC0C0&
      Height          =   315
      Index           =   0
      Left            =   1200
      TabIndex        =   0
      ToolTipText     =   "Job Number for the CD Labels"
      Top             =   120
      Width           =   1635
   End
   Begin VB.Label Label1 
      Caption         =   "Number of copies to Print"
      Height          =   195
      Index           =   15
      Left            =   120
      TabIndex        =   34
      Top             =   3780
      Width           =   1935
   End
   Begin VB.Label Label1 
      Caption         =   "Order Number"
      Height          =   255
      Index           =   14
      Left            =   3180
      TabIndex        =   32
      Top             =   180
      Width           =   1095
   End
   Begin VB.Label lblWorkstationprops 
      Height          =   195
      Left            =   4380
      TabIndex        =   30
      Top             =   3540
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "CD/DVD Labels are printed to the Thermal Priner if available, or the R-Quest system for use with the robot printer."
      Height          =   615
      Index           =   13
      Left            =   900
      TabIndex        =   29
      Top             =   4560
      Width           =   4455
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Set"
      Height          =   255
      Index           =   12
      Left            =   2340
      TabIndex        =   28
      Top             =   1020
      Width           =   615
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Episode"
      Height          =   255
      Index           =   11
      Left            =   3900
      TabIndex        =   25
      Top             =   1020
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Series"
      Height          =   255
      Index           =   10
      Left            =   120
      TabIndex        =   24
      Top             =   1020
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Format"
      Height          =   255
      Index           =   9
      Left            =   3000
      TabIndex        =   19
      Top             =   3120
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Barcode"
      Height          =   255
      Index           =   8
      Left            =   3000
      TabIndex        =   18
      Top             =   2700
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Master Number"
      Height          =   255
      Index           =   7
      Left            =   3000
      TabIndex        =   17
      Top             =   2280
      Width           =   1275
   End
   Begin VB.Label Label1 
      Caption         =   "Audio 2"
      Height          =   255
      Index           =   6
      Left            =   120
      TabIndex        =   16
      Top             =   3120
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Audio 1"
      Height          =   255
      Index           =   5
      Left            =   120
      TabIndex        =   15
      Top             =   2700
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Standard"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   14
      Top             =   2280
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Version"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   13
      Top             =   1860
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Sub Title"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   12
      Top             =   1440
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Title"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   11
      Top             =   600
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Job Number"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   10
      Top             =   180
      Width           =   975
   End
End
Attribute VB_Name = "frmLabelsCD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_intMinutesToCloseForm As Integer
Private Sub cmdClose_Click()
Me.Hide
End Sub

Private Sub cmdPrint_Click()

Dim l_strReportToPrint As String, l_strSQL As String

Dim l_lngID As Long, l_strWorkstationprops As String

'Select the report to print
l_strWorkstationprops = lblWorkstationprops.Caption

l_strReportToPrint = g_strLocationOfCrystalReportFiles & "DirectThermalCDlabel.rpt"

'load up the labelprint table with the values from the form

l_strSQL = "INSERT INTO labelprinting ("
l_strSQL = l_strSQL & "cuser, cdate, field0, field1, field2, field3, field4, field5, field6, field7, "
l_strSQL = l_strSQL & "field8, field9, field10, field12, field13, field11) VALUES ("

l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'" & Text1(0).Text & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(1).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(2).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(3).Text) & "', "
l_strSQL = l_strSQL & "'" & Text1(4).Text & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(5).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(6).Text) & "', "
If Check1.Value = False Then
    l_strSQL = l_strSQL & "'" & Text1(7).Text & "', "
    l_strSQL = l_strSQL & "'" & Text1(8).Text & "', "
Else
    l_strSQL = l_strSQL & "' ', "
    l_strSQL = l_strSQL & "' ', "
End If
l_strSQL = l_strSQL & "'" & Text1(9).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(10).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(12).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(13).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(11).Text & "')"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_lngID = g_lngLastID

'print the report
PrintCrystalReport l_strReportToPrint, "{labelprinting.labelprintingID} =" & Str(l_lngID), g_blnPreviewReport, Val(Text1(14).Text)

'get rid of that labelprint record again.

l_strSQL = "DELETE FROM labelprinting WHERE labelprintingID = " & Str(l_lngID)
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Sub

Private Sub Form_Load()

m_intMinutesToCloseForm = 4

End Sub

Private Sub timFormClose_Timer()

g_optCloseSystemDown = GetFlag(GetData("setting", "value", "name", "CloseSystemDown"))

If g_optCloseSystemDown <> 0 Then Unload Me

End Sub
