VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmEquipment 
   Caption         =   "Equipment Management"
   ClientHeight    =   9570
   ClientLeft      =   2850
   ClientTop       =   2610
   ClientWidth     =   12765
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9570
   ScaleWidth      =   12765
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   10500
      TabIndex        =   15
      Top             =   7560
      Width           =   1155
   End
   Begin VB.PictureBox picFooter 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1215
      Left            =   180
      ScaleHeight     =   1215
      ScaleWidth      =   8355
      TabIndex        =   10
      Top             =   6720
      Width           =   8355
      Begin VB.CommandButton cmdUpdate 
         Caption         =   "Make Unavailable"
         Height          =   315
         Index           =   1
         Left            =   6540
         TabIndex        =   14
         Top             =   840
         Width           =   1515
      End
      Begin VB.CommandButton cmdUpdate 
         Caption         =   "Make Available"
         Height          =   315
         Index           =   0
         Left            =   4920
         TabIndex        =   13
         Top             =   840
         Width           =   1515
      End
      Begin VB.TextBox txtOfficeClient 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   0  'None
         DataField       =   "Unavailable Reason"
         DataSource      =   "adoResourceSchedule"
         Height          =   855
         Left            =   0
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   11
         ToolTipText     =   "Notes for Invoice"
         Top             =   300
         Width           =   4815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Unavailble Reason"
         Height          =   195
         Index           =   0
         Left            =   0
         TabIndex        =   12
         Top             =   60
         Width           =   2175
      End
   End
   Begin VB.Frame fraBarcode 
      Caption         =   "Scan Items"
      Height          =   1035
      Left            =   60
      TabIndex        =   5
      Top             =   60
      Width           =   3435
      Begin VB.TextBox txtBarcode 
         Appearance      =   0  'Flat
         BackColor       =   &H0080FFFF&
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   900
         TabIndex        =   8
         ToolTipText     =   "Scan the Barcodes of the items on the Despatch"
         Top             =   600
         Width           =   2355
      End
      Begin VB.OptionButton optDoWhat 
         Caption         =   "Make Available"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Value           =   -1  'True
         Width           =   1395
      End
      Begin VB.OptionButton optDoWhat 
         Caption         =   "Make Unavailable"
         Height          =   195
         Index           =   1
         Left            =   1680
         TabIndex        =   6
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Barcode"
         Height          =   315
         Index           =   11
         Left            =   120
         TabIndex        =   9
         Top             =   600
         Width           =   1035
      End
   End
   Begin MSAdodcLib.Adodc adoResourceSchedule 
      Height          =   330
      Left            =   8220
      Top             =   2880
      Visible         =   0   'False
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoResourceSchedule"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.ComboBox cmbOrderBy 
      Height          =   315
      Left            =   9060
      TabIndex        =   3
      Tag             =   "NOCLEAR"
      Text            =   "cmbOrderBy"
      ToolTipText     =   "Which data field to search against"
      Top             =   8880
      Visible         =   0   'False
      Width           =   2235
   End
   Begin VB.ComboBox cmbDirection 
      Height          =   315
      ItemData        =   "frmEquipment.frx":0000
      Left            =   11400
      List            =   "frmEquipment.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   2
      ToolTipText     =   "Which data field to search against"
      Top             =   8880
      Visible         =   0   'False
      Width           =   1095
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdEquipment 
      Bindings        =   "frmEquipment.frx":0019
      Height          =   4635
      Left            =   60
      TabIndex        =   1
      Top             =   1560
      Width           =   12615
      _Version        =   196617
      AllowUpdate     =   0   'False
      BackColorOdd    =   16773365
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   22251
      _ExtentY        =   8176
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TabDlg.SSTab tabEquip 
      Height          =   600
      Left            =   60
      TabIndex        =   0
      Top             =   1200
      Width           =   12615
      _ExtentX        =   22251
      _ExtentY        =   1058
      _Version        =   393216
      TabsPerRow      =   10
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Available"
      TabPicture(0)   =   "frmEquipment.frx":003B
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).ControlCount=   0
      TabCaption(1)   =   "Unavailable"
      TabPicture(1)   =   "frmEquipment.frx":0057
      Tab(1).ControlEnabled=   0   'False
      Tab(1).ControlCount=   0
      TabCaption(2)   =   "Unchecked"
      TabPicture(2)   =   "frmEquipment.frx":0073
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
   End
   Begin VB.Label lblResourceID 
      Height          =   255
      Left            =   5220
      TabIndex        =   16
      Top             =   360
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order By"
      Height          =   255
      Index           =   22
      Left            =   7920
      TabIndex        =   4
      Top             =   8880
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "frmEquipment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdUpdate_Click(Index As Integer)

Dim l_strSQL As String


If Not IsNumeric(lblResourceID.Caption) Then
    MsgBox "You need to select a valid resource before attempting to change its status", vbExclamation
    Exit Sub
End If
    
Dim l_intMsg As Integer
Dim l_strResourceDetails  As String
Dim l_lngResourceID As Long


l_lngResourceID = Val(lblResourceID.Caption)

l_strResourceDetails = "Name: " & GetData("resource", "name", "resourceID", l_lngResourceID)
l_strResourceDetails = l_strResourceDetails & vbCrLf & "S/N: " & GetData("resource", "serialnumber", "resourceID", l_lngResourceID)

l_intMsg = MsgBox(l_strResourceDetails & vbCrLf & vbCrLf & "Are you sure you want to update this resource?", vbExclamation + vbYesNo)
If l_intMsg = vbNo Then Exit Sub

    
Select Case Index
Case 0

    'Make available
    l_strSQL = "lastmadeavailabledate = '" & FormatSQLDate(Now) & "', lastmadeavailableuser = '" & g_strUserInitials & "', fd_status = 'Available', jobID = NULL, unavailablereason = NULL, unavailablestage = NULL"
Case 1
    
    frmUnavailable.cmbStage.Text = GetData("resource", "unavailablestage", "resourceID", l_lngResourceID)
    frmUnavailable.txtReason.Text = GetData("resource", "unavailablereason", "resourceID", l_lngResourceID)
    
    frmUnavailable.Show vbModal

    If frmUnavailable.Tag = "CANCELLED" Then
        Unload frmUnavailable
        Set frmUnavailable = Nothing
        Exit Sub
    End If
    
    'Make unavailable
    l_strSQL = "madeunavailableuser = '" & g_strUserInitials & "', madeunavailabledate = '" & FormatSQLDate(Now) & "', fd_status = 'Unavailable', unavailablereason = '" & frmUnavailable.txtReason.Text & "', unavailablestage = '" & frmUnavailable.cmbStage.Text & "'"
    
    CreateResourceHistory l_lngResourceID, frmUnavailable.cmbStage.Text, frmUnavailable.txtReason.Text
    
    Unload frmUnavailable
    Set frmUnavailable = Nothing
    
End Select

l_strSQL = "UPDATE resource SET " & l_strSQL & " WHERE resourceID = '" & l_lngResourceID & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

adoResourceSchedule.Refresh

End Sub

Private Sub Form_Load()
cmbOrderBy.Text = "Main Category"
cmbDirection.ListIndex = 1
tabEquip_Click 0
End Sub


Private Sub Form_Resize()

On Error Resume Next
grdEquipment.Height = Me.ScaleHeight - grdEquipment.Top - 120 - picFooter.Height

picFooter.Top = Me.ScaleHeight - picFooter.Height
picFooter.Left = 120
grdEquipment.Width = Me.ScaleWidth - 120
tabEquip.Width = Me.ScaleWidth - 120
cmdClose.Left = Me.ScaleWidth - 120 - cmdClose.Width
cmdClose.Top = Me.ScaleHeight - 60 - cmdClose.Height



End Sub

Private Sub grdEquipment_Click()
txtBarcode.Text = grdEquipment.Columns("barcode").Text
lblResourceID.Caption = grdEquipment.Columns("Resource ID").Text
End Sub

Private Sub grdEquipment_HeadClick(ByVal ColIndex As Integer)


If cmbOrderBy.Text <> grdEquipment.Columns(ColIndex).DataField Then
    cmbOrderBy.Text = grdEquipment.Columns(ColIndex).DataField
Else
    cmbDirection.ListIndex = 1 - cmbDirection.ListIndex
End If

tabEquip_Click tabEquip.Tab

End Sub

Private Sub optDoWhat_Click(Index As Integer)
txtBarcode.SetFocus
End Sub

Private Sub tabEquip_Click(PreviousTab As Integer)

Dim l_strSQL As String
Dim l_strFields As String

l_strFields = "resource.resourceID AS 'Resource ID', category as 'Main Category', subcategory1 AS 'Sub Category 1', subcategory2 AS 'Sub Category 2', name AS 'Item Name', location AS 'Current Location', resource.jobID AS 'Job ID', job.companyname AS 'Job Company', resource.fd_status as 'Status', resource.barcode AS 'Barcode', resource.serialnumber AS 'Serial Number', unavailablestage AS 'Unavailable Stage', unavailablereason AS 'Unavailable Reason'"

Select Case tabEquip.Caption
Case "Late"
    l_strSQL = "SELECT " & l_strFields & " FROM resource LEFT JOIN job ON resource.jobID = job.jobID INNER JOIN resourceschedule ON resource.resourceID = resourceschedule.resourceID WHERE resourceschedule.endtime < '" & FormatSQLDate(Now) & "' AND resourceschedule.fd_status = 'Despatched' "
Case "Unavailable"
    l_strSQL = "SELECT " & l_strFields & " FROM resource LEFT JOIN job ON resource.jobID = job.jobID WHERE resource.fd_status = 'Unavailable'"
Case "Available"
    l_strSQL = "SELECT " & l_strFields & " FROM resource LEFT JOIN job ON resource.jobID = job.jobID WHERE resource.fd_status = 'Available' "
Case "Prepared"
    l_strSQL = "SELECT " & l_strFields & " FROM resource LEFT JOIN job ON resource.jobID = job.jobID WHERE resource.fd_status = 'Prepared' "
Case "Despatched"
    l_strSQL = "SELECT " & l_strFields & " FROM resource LEFT JOIN job ON resource.jobID = job.jobID WHERE resource.fd_status = 'Despatched' "
Case "Unchecked"
    l_strSQL = "SELECT " & l_strFields & " FROM resource LEFT JOIN job ON resource.jobID = job.jobID WHERE resource.fd_status = 'Unchecked' "

End Select

Screen.MousePointer = vbHourglass

adoResourceSchedule.ConnectionString = g_strConnection
adoResourceSchedule.RecordSource = l_strSQL & " ORDER BY '" & cmbOrderBy.Text & "' " & cmbDirection.Text
adoResourceSchedule.Refresh

Screen.MousePointer = vbDefault

End Sub


Private Sub txtBarcode_GotFocus()
HighLite txtBarcode
End Sub

Private Sub txtBarcode_KeyPress(KeyAscii As Integer)

If KeyAscii = vbKeyReturn Then
    KeyAscii = 0
    
    'check the resource ID
    Dim l_lngResourceID As Long
    l_lngResourceID = GetData("resource", "resourceID", "barcode", txtBarcode.Text)
    
    If l_lngResourceID = 0 Then
        MsgBox "The barcode you scanned can not be located in your resource list", vbExclamation
        Exit Sub
    End If
    
    'check item is of correct status to move to the new status
    Dim l_strCurrentStatus  As String
    l_strCurrentStatus = GetData("resource", "fd_status", "resourceID", l_lngResourceID)
    
    'click the correct button
    
    lblResourceID.Caption = l_lngResourceID
    
    If optDoWhat(0).Value = True Then
        cmdUpdate_Click 0
    Else
        cmdUpdate_Click 1
    End If
    
    HighLite txtBarcode
        
End If

End Sub
