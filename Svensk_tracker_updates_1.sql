use cetasoft
go

update tracker_svensk_item set ProjectManager = 'DADC MIS' WHERE ProjectManagerContactID = 3366 AND ((ProjectManager is null) or (projectmanager = ''))

/* tracker_svensk tables */
alter table tracker_svensk_item
alter column itemreference nvarchar(255)

alter table tracker_svensk_item
alter column itemfilename nvarchar(255)

alter table tracker_svensk_item
alter column timecodeduration nvarchar(50)

alter table tracker_svensk_item
alter column RightsOwner nvarchar(50)

alter table tracker_svensk_item
alter column Supplier nvarchar(50)

alter table tracker_svensk_item
alter column ProjectManager nvarchar(50)

alter table tracker_svensk_item
alter column title nvarchar(255)

alter table tracker_svensk_item
alter column series nvarchar(50)

alter table tracker_svensk_item
alter column subtitle nvarchar(255)

alter table tracker_svensk_item
alter column episode nvarchar(50)

alter table tracker_svensk_item
alter column componenttype nvarchar(50)

alter table tracker_svensk_item
alter column programtype nvarchar(50)

alter table tracker_svensk_item
alter column [language] nvarchar(50)

alter table tracker_svensk_item
alter column barcode nvarchar(50)

alter table tracker_svensk_item
alter column format nvarchar(50)

alter table tracker_svensk_item
alter column cuser nvarchar(50)

alter table tracker_svensk_item
alter column muser nvarchar(50)

alter table tracker_svensk_item
alter column projectnumber nvarchar(50)

alter table tracker_svensk_item
alter column framerate nvarchar(50)

alter table tracker_svensk_item
alter column AlphaDisplayCode nvarchar(50)

alter table tracker_svensk_item
alter column RevisionNumber nvarchar(50)

alter table tracker_svensk_item
alter column audioconfig nvarchar(50)

alter table tracker_svensk_item
alter column requestedframerate nvarchar(50)

alter table tracker_svensk_item
alter column SpecialProject nvarchar(50)

alter table tracker_svensk_validation
alter column ValidatedBy nvarchar(50)

alter table tracker_svensk_comment
alter column cuser nvarchar(50)

alter table tracker_svensk_comment
alter column Comment nvarchar(MAX)

alter table tracker_svensk_comment
alter column filelocation nvarchar(255)

alter table tracker_svensk_audit
alter column muser nvarchar(50)

alter table tracker_svensk_audit
alter column [description] nvarchar(max)

