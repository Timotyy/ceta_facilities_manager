VERSION 5.00
Begin VB.Form frmChangePassword 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Change Password"
   ClientHeight    =   1485
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6990
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1485
   ScaleWidth      =   6990
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   315
      Left            =   4020
      TabIndex        =   4
      Top             =   540
      Width           =   1155
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   315
      Left            =   4020
      TabIndex        =   3
      Top             =   120
      Width           =   1155
   End
   Begin VB.TextBox txtNewPasswordConfirm 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1620
      PasswordChar    =   "*"
      TabIndex        =   2
      Top             =   960
      Width           =   2235
   End
   Begin VB.TextBox txtNewPassword 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1620
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   540
      Width           =   2235
   End
   Begin VB.TextBox txtOldPassword 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1620
      PasswordChar    =   "*"
      TabIndex        =   0
      Top             =   120
      Width           =   2235
   End
   Begin VB.Label lblDaysRemaining 
      Alignment       =   2  'Center
      Caption         =   "You have x days remaining before you must change your password."
      ForeColor       =   &H000000C0&
      Height          =   195
      Left            =   120
      TabIndex        =   15
      Top             =   1260
      Visible         =   0   'False
      Width           =   5055
   End
   Begin VB.Shape Shape1 
      Height          =   1275
      Left            =   5280
      Top             =   120
      Width           =   1635
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      BackColor       =   &H80000002&
      Caption         =   "Requirements"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000009&
      Height          =   255
      Index           =   9
      Left            =   5280
      TabIndex        =   14
      Top             =   120
      Width           =   1635
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "0"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   8
      Left            =   6480
      TabIndex        =   13
      Top             =   480
      Width           =   405
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "0"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   7
      Left            =   6480
      TabIndex        =   12
      Top             =   780
      Width           =   405
   End
   Begin VB.Label lblCaption 
      Alignment       =   2  'Center
      Caption         =   "0"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   6
      Left            =   6480
      TabIndex        =   11
      Top             =   1080
      Width           =   405
   End
   Begin VB.Label lblCaption 
      Caption         =   "Min Length"
      Height          =   255
      Index           =   5
      Left            =   5340
      TabIndex        =   10
      Top             =   480
      Width           =   1140
   End
   Begin VB.Label lblCaption 
      Caption         =   "Capital Letters"
      Height          =   255
      Index           =   4
      Left            =   5340
      TabIndex        =   9
      Top             =   780
      Width           =   1140
   End
   Begin VB.Label lblCaption 
      Caption         =   "Numbers"
      Height          =   255
      Index           =   3
      Left            =   5340
      TabIndex        =   8
      Top             =   1080
      Width           =   1140
   End
   Begin VB.Label lblCaption 
      Caption         =   "Confirm Password"
      Height          =   255
      Index           =   2
      Left            =   180
      TabIndex        =   7
      Top             =   960
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "New Password"
      Height          =   255
      Index           =   1
      Left            =   180
      TabIndex        =   6
      Top             =   540
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Old Password"
      Height          =   255
      Index           =   0
      Left            =   180
      TabIndex        =   5
      Top             =   120
      Width           =   1155
   End
End
Attribute VB_Name = "frmChangePassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdCancel_Click()

Me.Tag = "CANCELLED"
Me.Hide
    
End Sub

Private Sub cmdOK_Click()

    Dim l_strPassword As String
    l_strPassword = txtOldPassword.Text
    
    If txtNewPassword.Text = txtOldPassword.Text Then
        MsgBox "You can not use the same password as you used last time.", vbExclamation
        txtNewPassword.SetFocus
        Exit Sub
    End If
        
    If g_optUseMD5Passwords = 1 Then
        If l_strPassword <> "" Then l_strPassword = MD5Encrypt(l_strPassword)
    End If
    
    If txtOldPassword.Text = "" Then l_strPassword = "nopassword"
    
    Dim l_strSQL As String
    
    l_strSQL = "SELECT COUNT(cetauserID) FROM cetauser WHERE cetauserID = '" & g_lngUserID & "' "
    
    If l_strPassword = "" Then
        l_strSQL = l_strSQL & "AND fd_password = 'nopassword'"
    Else
        l_strSQL = l_strSQL & "AND fd_password = '" & QuoteSanitise(l_strPassword) & "'"
    End If

    Dim l_intCount As Integer
    l_intCount = GetCount(l_strSQL)
    
    If l_intCount = 0 Then
        MsgBox "Incorrect old password.", vbExclamation
        txtOldPassword.SetFocus
        Exit Sub
    End If
    
    If txtNewPassword.Text <> txtNewPasswordConfirm.Text Then
        MsgBox "New passwords do not match.", vbExclamation
        txtNewPassword.SetFocus
        Exit Sub
    End If
    
    Dim l_blnPasswordStrength As Boolean
    l_blnPasswordStrength = CheckPasswordStrength(CStr(txtNewPassword.Text))
    
    If l_blnPasswordStrength = False Then
        MsgBox "Your new password does not match the password strength requirements. Please change it and try again.", vbExclamation
        txtNewPassword.SetFocus
        Exit Sub
    End If
    
    Dim l_strNewPassword As String
    l_strNewPassword = txtNewPassword.Text
    
    If g_optUseMD5Passwords = 1 Then
        If l_strNewPassword <> "" Then l_strNewPassword = MD5Encrypt(l_strNewPassword)
    End If
    
    l_strSQL = "UPDATE cetauser SET lastchangedpassworddate = '" & FormatSQLDate(Date) & "', fd_password = '" & QuoteSanitise(l_strNewPassword) & "', oldpassword = '" & QuoteSanitise(l_strPassword) & "', modifieddate = '" & FormatSQLDate(Now) & "' WHERE cetauserID = '" & g_lngUserID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
        
        
    Me.Tag = "CHANGED"
    Me.Hide

End Sub

Private Sub Form_Load()

lblCaption(8).Caption = g_intPasswordLength
lblCaption(7).Caption = g_intPasswordUpperCase
lblCaption(6).Caption = g_intPasswordNumbers

End Sub

Private Sub txtNewPassword_GotFocus()
HighLite txtNewPassword
End Sub

Private Sub txtNewPasswordConfirm_GotFocus()
HighLite txtNewPasswordConfirm
End Sub

Private Sub txtOldPassword_GotFocus()
HighLite txtOldPassword
End Sub
