VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmAddSelectContact 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Add / Select Contact"
   ClientHeight    =   8760
   ClientLeft      =   2760
   ClientTop       =   3810
   ClientWidth     =   8745
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8760
   ScaleWidth      =   8745
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   2775
      Left            =   4140
      TabIndex        =   7
      Top             =   1680
      Width           =   4515
      Begin VB.TextBox txtContactName 
         Height          =   315
         Index           =   0
         Left            =   1140
         TabIndex        =   13
         Top             =   240
         Width           =   3255
      End
      Begin VB.TextBox txtContactName 
         Height          =   315
         Index           =   1
         Left            =   1140
         TabIndex        =   12
         Top             =   660
         Width           =   3255
      End
      Begin VB.TextBox txtContactName 
         Height          =   315
         Index           =   2
         Left            =   1140
         TabIndex        =   11
         Top             =   1080
         Width           =   3255
      End
      Begin VB.CommandButton cmdAddNewAndSelect 
         Caption         =   "Add new and select"
         Height          =   315
         Left            =   2400
         TabIndex        =   10
         Top             =   2340
         Width           =   1995
      End
      Begin VB.TextBox txtContactName 
         Height          =   315
         Index           =   3
         Left            =   1140
         TabIndex        =   9
         Top             =   1500
         Width           =   3255
      End
      Begin VB.TextBox txtContactName 
         Height          =   315
         Index           =   4
         Left            =   1140
         TabIndex        =   8
         Top             =   1920
         Width           =   3255
      End
      Begin VB.Label Label1 
         Caption         =   "Known As"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "First Name"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   17
         Top             =   660
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Last Name"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   16
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Telephone"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   15
         Top             =   1500
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Email"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   14
         Top             =   1920
         Width           =   975
      End
   End
   Begin MSAdodcLib.Adodc adoContact 
      Height          =   330
      Left            =   3060
      Top             =   9000
      Visible         =   0   'False
      Width           =   1200
      _ExtentX        =   2117
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoContact"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton cmdLoadContacts 
      Caption         =   "LOAD"
      Height          =   315
      Left            =   3060
      TabIndex        =   3
      Top             =   9840
      Visible         =   0   'False
      Width           =   1155
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdContacts 
      Bindings        =   "frmAddSelectContact.frx":0000
      Height          =   6915
      Left            =   60
      TabIndex        =   0
      Top             =   1740
      Width           =   3915
      _Version        =   196617
      ForeColorEven   =   0
      BackColorOdd    =   15794160
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   6906
      _ExtentY        =   12197
      _StockProps     =   79
      Caption         =   "Existing Contacts"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   315
      Left            =   7500
      TabIndex        =   2
      Top             =   8340
      Width           =   1155
   End
   Begin VB.CommandButton cmdSelect 
      Caption         =   "Select"
      Height          =   315
      Left            =   6240
      TabIndex        =   1
      Top             =   8340
      Width           =   1155
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000002&
      X1              =   60
      X2              =   8640
      Y1              =   480
      Y2              =   480
   End
   Begin VB.Label Label2 
      Caption         =   "You did not select a contact from the drop down list. Please confirm the contact now."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   255
      Index           =   4
      Left            =   60
      TabIndex        =   22
      Top             =   120
      Width           =   8535
   End
   Begin VB.Label lblContactID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   4380
      TabIndex        =   21
      Top             =   9420
      Visible         =   0   'False
      Width           =   1155
   End
   Begin VB.Label Label2 
      Caption         =   "Create New Contact Record?"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   315
      Index           =   3
      Left            =   4140
      TabIndex        =   20
      Top             =   600
      Width           =   3855
   End
   Begin VB.Label Label2 
      Caption         =   "Check existing contacts first!"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   315
      Index           =   2
      Left            =   60
      TabIndex        =   19
      Top             =   600
      Width           =   3855
   End
   Begin VB.Label Label2 
      Caption         =   $"frmAddSelectContact.frx":0019
      Height          =   615
      Index           =   1
      Left            =   60
      TabIndex        =   6
      Top             =   1020
      Width           =   3855
   End
   Begin VB.Label Label2 
      Caption         =   $"frmAddSelectContact.frx":00A7
      Height          =   675
      Index           =   0
      Left            =   4140
      TabIndex        =   5
      Top             =   1020
      Width           =   4455
   End
   Begin VB.Label lblCompanyID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   3060
      TabIndex        =   4
      Top             =   9420
      Visible         =   0   'False
      Width           =   1155
   End
End
Attribute VB_Name = "frmAddSelectContact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub cmdAddNewAndSelect_Click()

Dim l_strSQL As String

'add the contact record
l_strSQL = "INSERT INTO contact "
l_strSQL = l_strSQL & " (name, firstname, lastname, telephone, email)"
l_strSQL = l_strSQL & " VALUES "
l_strSQL = l_strSQL & " ('" & QuoteSanitise(txtContactName(0).Text) & "','" & QuoteSanitise(txtContactName(1).Text) & "','" & QuoteSanitise(txtContactName(2).Text) & "','" & QuoteSanitise(txtContactName(3).Text) & "','" & QuoteSanitise(txtContactName(4).Text) & "');"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

Dim l_lngContactID As Long
l_lngContactID = g_lngLastID

'add the employee record
l_strSQL = "INSERT INTO employee "
l_strSQL = l_strSQL & "(contactID, companyID)"
l_strSQL = l_strSQL & " VALUES "
l_strSQL = l_strSQL & " ('" & l_lngContactID & "', '" & lblCompanyID.Caption & "');"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

lblContactID.Caption = l_lngContactID

cmdSelect.Value = True

End Sub

Private Sub cmdCancel_Click()
Me.Tag = "0"
Me.Hide
End Sub

Private Sub cmdLoadContacts_Click()

If lblCompanyID.Caption = "" Then
    NoCompanySelectedMessage
    Exit Sub
End If

Dim l_strSQL As String

l_strSQL = "SELECT contact.contactID, contact.name AS 'Contact Name' FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((company.companyID)=" & lblCompanyID.Caption & ")) ORDER BY contact.name;"

adoContact.ConnectionString = g_strConnection
adoContact.RecordSource = l_strSQL
adoContact.Refresh

End Sub

Private Sub cmdSelect_Click()
Me.Tag = lblContactID.Caption
Me.Hide

End Sub

Private Sub grdContacts_Click()
lblContactID.Caption = grdContacts.Columns(0).Text
End Sub

Private Sub grdContacts_DblClick()
If grdContacts.Rows < 1 Then Exit Sub

lblContactID.Caption = grdContacts.Columns(0).Text
cmdSelect.Value = True


End Sub

Private Sub grdContacts_InitColumnProps()
grdContacts.Columns("contactID").Visible = False
grdContacts.Columns(1).Width = 3000
End Sub

Private Sub txtContactName_GotFocus(Index As Integer)
HighLite txtContactName(Index)
End Sub
