VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmDespatchList2 
   Caption         =   "Despatch Information"
   ClientHeight    =   9615
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13695
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9615
   ScaleWidth      =   13695
   Begin VB.CommandButton cmdCompleteDespatch 
      Caption         =   "Complete Despatch"
      Height          =   375
      Left            =   780
      TabIndex        =   16
      Top             =   7440
      Visible         =   0   'False
      Width           =   1515
   End
   Begin VB.ComboBox cmbDirection 
      Height          =   315
      ItemData        =   "frmDespatchList2.frx":0000
      Left            =   5820
      List            =   "frmDespatchList2.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   14
      ToolTipText     =   "Which data field to search against"
      Top             =   6900
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ComboBox cmbOrderBy 
      Height          =   315
      Left            =   3480
      TabIndex        =   13
      Tag             =   "NOCLEAR"
      Text            =   "cmbOrderBy"
      ToolTipText     =   "Which data field to search against"
      Top             =   6900
      Visible         =   0   'False
      Width           =   2235
   End
   Begin VB.PictureBox picHeader 
      Align           =   1  'Align Top
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1395
      Left            =   0
      ScaleHeight     =   1335
      ScaleWidth      =   13635
      TabIndex        =   0
      Top             =   0
      Width           =   13695
      Begin VB.CommandButton cmdRefresh 
         Caption         =   "Refresh (F5)"
         Height          =   315
         Left            =   12180
         TabIndex        =   12
         ToolTipText     =   "Send Despatch"
         Top             =   120
         Width           =   1275
      End
      Begin VB.CheckBox chkShow 
         Caption         =   "Out"
         Height          =   195
         Index           =   1
         Left            =   8100
         TabIndex        =   11
         Top             =   420
         Width           =   1095
      End
      Begin VB.CheckBox chkShow 
         Caption         =   "In"
         Height          =   195
         Index           =   0
         Left            =   8100
         TabIndex        =   10
         Top             =   120
         Width           =   1095
      End
      Begin VB.TextBox Text2 
         Height          =   315
         Left            =   1380
         TabIndex        =   9
         Top             =   960
         Width           =   2115
      End
      Begin VB.TextBox Text1 
         Height          =   315
         Left            =   1380
         TabIndex        =   7
         Top             =   540
         Width           =   2115
      End
      Begin VB.TextBox txtDespatchID 
         Height          =   315
         Left            =   1380
         TabIndex        =   5
         Top             =   120
         Width           =   2115
      End
      Begin VB.Label lblCaption 
         Caption         =   "Project No."
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   8
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Job ID"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   540
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Despatch ID"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   4
         Top             =   120
         Width           =   1095
      End
   End
   Begin MSAdodcLib.Adodc adoDelivery 
      Height          =   330
      Left            =   4260
      Top             =   3120
      Visible         =   0   'False
      Width           =   1905
      _ExtentX        =   3360
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoDelivery"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnDeliveredBy 
      Height          =   1755
      Left            =   6720
      TabIndex        =   1
      Top             =   3240
      Width           =   3495
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   12648447
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   3200
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "Description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   6165
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnMethod 
      Height          =   1755
      Left            =   11280
      TabIndex        =   2
      Top             =   4200
      Width           =   2115
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   12648447
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   3200
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "Description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   3731
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDelivery 
      Align           =   1  'Align Top
      Bindings        =   "frmDespatchList2.frx":0019
      Height          =   1395
      Left            =   0
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   1395
      Width           =   13695
      _Version        =   196617
      BeveColorScheme =   1
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   0
      BackColorOdd    =   16773364
      RowHeight       =   370
      ExtraHeight     =   53
      Columns.Count   =   17
      Columns(0).Width=   1296
      Columns(0).Caption=   "DNote"
      Columns(0).Name =   "despatchnumber"
      Columns(0).DataField=   "despatchnumber"
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   10744793
      Columns(1).Width=   1296
      Columns(1).Caption=   "Job ID"
      Columns(1).Name =   "jobID"
      Columns(1).DataField=   "jobID"
      Columns(1).DataType=   17
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16239283
      Columns(2).Width=   1296
      Columns(2).Caption=   "Project"
      Columns(2).Name =   "projectnumber"
      Columns(2).DataField=   "projectnumber"
      Columns(2).FieldLen=   256
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   9040629
      Columns(3).Width=   1296
      Columns(3).Caption=   "P/O"
      Columns(3).Name =   "purchaseauthorisationnumber"
      Columns(3).DataField=   "purchaseauthorisationnumber"
      Columns(3).FieldLen=   256
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   33023
      Columns(4).Width=   3016
      Columns(4).Caption=   "Description"
      Columns(4).Name =   "description"
      Columns(4).DataField=   "description"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   3387
      Columns(5).Caption=   "Company"
      Columns(5).Name =   "company"
      Columns(5).DataField=   "companyname"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   1614
      Columns(6).Caption=   "Post Code"
      Columns(6).Name =   "postcode"
      Columns(6).DataField=   "postcode"
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "Date Created"
      Columns(7).Name =   "createddate"
      Columns(7).DataField=   "createddate"
      Columns(7).DataType=   7
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   794
      Columns(8).Caption=   "User"
      Columns(8).Name =   "creadtedby"
      Columns(8).DataField=   "createduser"
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(9).Width=   3069
      Columns(9).Caption=   "Deadline"
      Columns(9).Name =   "deadlinedate"
      Columns(9).DataField=   "deadlinedate"
      Columns(9).DataType=   7
      Columns(9).FieldLen=   256
      Columns(9).Locked=   -1  'True
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "Direction"
      Columns(10).Name=   "direction"
      Columns(10).DataField=   "direction"
      Columns(10).FieldLen=   256
      Columns(10).Locked=   -1  'True
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "despatcheduser"
      Columns(11).Name=   "despatcheduser"
      Columns(11).DataField=   "despatcheduser"
      Columns(11).FieldLen=   256
      Columns(12).Width=   3069
      Columns(12).Caption=   "Despatched"
      Columns(12).Name=   "despatcheddate"
      Columns(12).DataField=   "despatcheddate"
      Columns(12).DataType=   7
      Columns(12).FieldLen=   256
      Columns(12).Style=   1
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "flagurgent"
      Columns(13).Name=   "flagurgent"
      Columns(13).DataField=   "flagurgent"
      Columns(13).FieldLen=   256
      Columns(13).Locked=   -1  'True
      Columns(14).Width=   2646
      Columns(14).Caption=   "Method"
      Columns(14).Name=   "deliverymethod"
      Columns(14).DataField=   "deliverymethod"
      Columns(14).FieldLen=   256
      Columns(15).Width=   2143
      Columns(15).Caption=   "Delivered By"
      Columns(15).Name=   "deliveredby"
      Columns(15).DataField=   "deliveredby"
      Columns(15).FieldLen=   256
      Columns(16).Width=   1720
      Columns(16).Caption=   "Job Type"
      Columns(16).Name=   "typeofjob"
      Columns(16).DataField=   "typeofjob"
      Columns(16).FieldLen=   256
      _ExtentX        =   24156
      _ExtentY        =   2461
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order By"
      Height          =   255
      Index           =   22
      Left            =   2340
      TabIndex        =   15
      Top             =   6900
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "frmDespatchList2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdCompleteDespatch_Click()


If grdDelivery(Index).Row = grdDelivery(Index).Rows Then Exit Sub

Dim l_lngDespatchID As Long
l_lngDespatchID = GetData("despatch", "despatchID", "despatchnumber", grdDelivery.Columns("despatchnumber").Text)

If grdDelivery.Columns("jobID").Text <> "" Then
    AddJobHistory grdDelivery.Columns("jobID").Text, "Completed Despatch " & grdDelivery.Columns("despatchnumber").Text
    
End If

If grdDelivery.Columns("despatcheddate").Text <> "" Then
    grdDelivery.Columns("despatcheddate").Text = ""
    grdDelivery.Columns("despatcheduser").Text = ""
Else
    grdDelivery.Columns("despatcheddate").Text = Now
    grdDelivery.Columns("despatcheduser").Text = g_strUserInitials
End If

DoEvents

grdDelivery.Update

If chkShowDespatchOnClick.Value = 1 Then ShowDespatchDetail l_lngDespatchID

End Sub

Private Sub cmdRefresh_Click()


    If Not CheckAccess("/showdespatch") Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    DoEvents
        
    Dim l_strSQL As String
    
    l_strSQL = "SELECT * FROM despatch WHERE (despatcheddate Is NULL)"

   ' If Not IsNull(frmDespatch.datDeadlineDate.Value) Then
   '     l_datSpecifiedDate = Format(frmDespatch.datDeadlineDate.Value, vbShortDateFormat) & " 00:00"
   '     l_strSQL = l_strSQL & " AND (deadlinedate BETWEEN '" & FormatSQLDate(l_datSpecifiedDate) & "' AND '" & FormatSQLDate(DateAdd("d", 1, l_datSpecifiedDate)) & "')"
   ' End If
    
    
    If cmbOrderBy.Text = "cmbOrderBy" Then
        cmbOrderBy.Text = "deadlinedate"
    End If
    
    
    Dim l_conJobHistory As ADODB.Connection
    Dim l_rstJobHistory As ADODB.Recordset
    
    Dim l_strSort As String
    l_strSort = " ORDER BY despatch." & cmbOrderBy.Text & " " & cmbDirection.Text
    
    adoDelivery.RecordSource = l_strSQL & " AND (direction = 'OUT')"
    
    adoDelivery.RecordSource = adoDelivery.RecordSource & l_strSort
    adoDelivery.ConnectionString = g_strConnection
    adoDelivery.Refresh
    
    Screen.MousePointer = vbDefault

End Sub

Private Sub Form_Resize()
On Error Resume Next
grdDelivery.Height = Me.ScaleHeight - picHeader.Height

End Sub

Private Sub grdDelivery_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

Dim l_intMsg As Integer
l_intMsg = MsgBox("Do you really want to delete this despatch record?", vbYesNo + vbQuestion)

If l_intMsg = vbNo Then
    Cancel = True
    Exit Sub
End If

If grdDelivery.Columns("jobID").Text <> "" Then
    AddJobHistory grdDelivery.Columns("jobID").Text, "Deleted despatch " & grdDelivery.Columns("despatchnumber")
End If

DispPromptMsg = 0

Dim l_strSQL As String, l_lngDespatchID As Long
l_lngDespatchID = GetData("despatch", "despatchID", "despatchnumber", grdDelivery.Columns("despatchnumber").Text)

l_strSQL = "DELETE FROM despatch WHERE despatchID = '" & l_lngDespatchID & "';"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_strSQL = "DELETE FROM despatchdetail WHERE despatchID = '" & l_lngDespatchID & "';"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

Cancel = True

End Sub

Private Sub grdDelivery_BtnClick()

Dim l_lngDespatchID As Long
l_lngDespatchID = GetData("despatch", "despatchID", "despatchnumber", grdDelivery.Columns("despatchnumber").Text)

cmdCompleteDespatch_Click

CompleteDespatch l_lngDespatchID

DoEvents

End Sub

Private Sub grdDelivery_DblClick()

Dim l_lngDespatchID As Long
l_lngDespatchID = GetData("despatch", "despatchID", "despatchnumber", Val(grdDelivery.Columns("despatchnumber").Text))

If LCase(grdDelivery.Columns("typeofjob").Text) = "hire" Then
    frmTechJob.txtDespatchNumber.Text = Val(grdDelivery.Columns("despatchnumber").Text)
    frmTechJob.txtDespatchNumber_KeyPress 13
    frmTechJob.Show
    Exit Sub
End If

If grdDelivery.Columns("jobID").Text <> "" Then
    ShowDespatchForJob grdDelivery.Columns("jobID").Text
End If

ShowDespatchDetail l_lngDespatchID

End Sub

Private Sub grdDelivery_HeadClick(ByVal ColIndex As Integer)


If cmbOrderBy.Text <> grdDelivery.Columns(ColIndex).DataField Then
    cmbOrderBy.Text = grdDelivery.Columns(ColIndex).DataField
Else
    cmbDirection.ListIndex = 1 - cmbDirection.ListIndex
End If

End Sub

