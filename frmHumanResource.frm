VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmHumanResource 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Holiday Information"
   ClientHeight    =   5085
   ClientLeft      =   3735
   ClientTop       =   4515
   ClientWidth     =   8940
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5085
   ScaleWidth      =   8940
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdLoadHoliday 
      Caption         =   "LOAD HOLIDAY"
      Height          =   615
      Left            =   240
      TabIndex        =   10
      ToolTipText     =   "Close this form"
      Top             =   840
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   7680
      TabIndex        =   9
      ToolTipText     =   "Close this form"
      Top             =   4680
      Width           =   1155
   End
   Begin MSComCtl2.MonthView datDateSelect 
      Height          =   2370
      Left            =   720
      TabIndex        =   5
      Top             =   1020
      Width           =   2595
      _ExtentX        =   4577
      _ExtentY        =   4180
      _Version        =   393216
      ForeColor       =   -2147483630
      BackColor       =   -2147483633
      Appearance      =   1
      StartOfWeek     =   24838146
      CurrentDate     =   38484
   End
   Begin VB.TextBox txtResourceName 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1320
      Locked          =   -1  'True
      TabIndex        =   4
      Top             =   120
      Width           =   2595
   End
   Begin VB.TextBox txtResourceID 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1440
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   4740
      Visible         =   0   'False
      Width           =   2415
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdHoliday 
      Bindings        =   "frmHumanResource.frx":0000
      Height          =   4455
      Left            =   4020
      TabIndex        =   0
      Top             =   120
      Width           =   4815
      _Version        =   196617
      AllowDelete     =   -1  'True
      AllowUpdate     =   0   'False
      SelectTypeRow   =   1
      BackColorOdd    =   12648447
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3360
      Columns(0).Caption=   "Date"
      Columns(0).Name =   "holidaydate"
      Columns(0).DataField=   "holidaydate"
      Columns(0).DataType=   7
      Columns(0).FieldLen=   256
      Columns(1).Width=   4048
      Columns(1).Caption=   "Type"
      Columns(1).Name =   "holidaytype"
      Columns(1).DataField=   "holidaytype"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "holidayID"
      Columns(2).Name =   "holidayID"
      Columns(2).DataField=   "holidayID"
      Columns(2).FieldLen=   256
      _ExtentX        =   8493
      _ExtentY        =   7858
      _StockProps     =   79
      Caption         =   "Holiday Schedule"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbHolidayType 
      Height          =   255
      Left            =   1320
      TabIndex        =   6
      ToolTipText     =   "Type of job"
      Top             =   480
      Width           =   2595
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BevelColorFrame =   -2147483644
      CheckBox3D      =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   4868
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4577
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin MSAdodcLib.Adodc adoHoliday 
      Height          =   330
      Left            =   4020
      Top             =   4680
      Visible         =   0   'False
      Width           =   2355
      _ExtentX        =   4154
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoHoliday"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label Label1 
      Caption         =   "Double click a day in the calendar to add it as the specified holiday type."
      Height          =   435
      Left            =   720
      TabIndex        =   8
      Top             =   3660
      Width           =   2595
   End
   Begin VB.Label lblCaption 
      Caption         =   "Holiday Type"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   7
      Top             =   480
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Name"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Resource ID"
      Height          =   255
      Index           =   0
      Left            =   60
      TabIndex        =   1
      Top             =   4740
      Visible         =   0   'False
      Width           =   1275
   End
End
Attribute VB_Name = "frmHumanResource"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdClose_Click()
Me.Hide
End Sub

Private Sub cmdLoadHoliday_Click()

adoHoliday.ConnectionString = g_strConnection
adoHoliday.RecordSource = "SELECT holidaydate, holidaytype, holidayID FROM holiday WHERE resourceID = '" & txtResourceID.Text & "' ORDER BY holidaydate DESC;"
adoHoliday.Refresh

End Sub

Private Sub datDateSelect_DateDblClick(ByVal DateDblClicked As Date)

If cmbHolidayType.Text = "" Then
    MsgBox "Please select a valid holiday type first", vbExclamation
    Exit Sub
End If

Dim l_strSQL As String

l_strSQL = "DELETE FROM holiday WHERE holidaydate = '" & FormatSQLDate(DateDblClicked) & "' AND resourceID = '" & txtResourceID.Text & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError


l_strSQL = "INSERT INTO holiday (holidaydate, holidaytype, resourceID, cuser, cdate) VALUES ('" & FormatSQLDate(DateDblClicked) & "','" & cmbHolidayType.Text & "', '" & txtResourceID.Text & "', '" & g_strUserInitials & "', '" & FormatSQLDate(Now) & "');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

cmdLoadHoliday_Click

cmdClose.SetFocus

End Sub

Private Sub Form_Load()
PopulateCombo "holiday", cmbHolidayType
CenterForm Me
End Sub

Private Sub grdHoliday_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

'dont show the message
DispPromptMsg = False

Dim l_intMsg As Integer
l_intMsg = MsgBox("Are you sure you want to delete this holiday record (" & grdHoliday.Columns("holidaytype").Text & " on " & grdHoliday.Columns("holidaydate").Text & ")?", vbOKCancel + vbQuestion)

If l_intMsg = vbOK Then
    
    Dim l_strSQL As String
    
    l_strSQL = "DELETE FROM holiday WHERE holidayID = '" & Val(grdHoliday.Columns("holidayID").Text) & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    Cancel = True
Else
    Cancel = True
    
End If

adoHoliday.Refresh

End Sub

