VERSION 5.00
Begin VB.Form frmShutdownWarning 
   Caption         =   "CETA System Shutdown"
   ClientHeight    =   1665
   ClientLeft      =   6030
   ClientTop       =   5670
   ClientWidth     =   6765
   Icon            =   "frmShutdownWarning.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   1665
   ScaleWidth      =   6765
   Begin VB.Label lblPleaseSaveAndQuite 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Please Save and Quit!"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   480
      TabIndex        =   1
      Top             =   900
      Width           =   5835
   End
   Begin VB.Label lblShutdownWarning 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "The CETA System Will Shutdown In 2 Minutes"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   480
      TabIndex        =   0
      Top             =   240
      Width           =   5835
   End
End
Attribute VB_Name = "frmShutdownWarning"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
