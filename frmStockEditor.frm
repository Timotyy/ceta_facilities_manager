VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmStockEditor 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Format and Stock Type Editor"
   ClientHeight    =   6240
   ClientLeft      =   3465
   ClientTop       =   2685
   ClientWidth     =   11145
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6240
   ScaleWidth      =   11145
   Begin VB.CheckBox chkStockCodeUpdate 
      Caption         =   "Update stock-codes"
      Height          =   255
      Left            =   5460
      TabIndex        =   34
      Top             =   3120
      Width           =   1815
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   9900
      TabIndex        =   15
      Top             =   5820
      Width           =   1155
   End
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "<<  Refresh List"
      Height          =   555
      Left            =   4080
      TabIndex        =   29
      Top             =   2580
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adoLibraryFormats 
      Height          =   330
      Left            =   8880
      Top             =   4020
      Visible         =   0   'False
      Width           =   1875
      _ExtentX        =   3307
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoLibraryFormats"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdLibraryFormats 
      Bindings        =   "frmStockEditor.frx":0000
      Height          =   3555
      Left            =   120
      TabIndex        =   28
      Top             =   2580
      Width           =   3810
      _Version        =   196617
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "NotInXRef"
      stylesets(0).BackColor=   255
      stylesets(0).Picture=   "frmStockEditor.frx":0020
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   6720
      _ExtentY        =   6271
      _StockProps     =   79
      Caption         =   "Double Click To Update Library"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame2 
      Caption         =   "Stock Code Information"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3135
      Left            =   7320
      TabIndex        =   20
      Top             =   360
      Width           =   3675
      Begin VB.TextBox txtNominalCode 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   1140
         TabIndex        =   12
         Top             =   2220
         Width           =   2415
      End
      Begin VB.TextBox txtRateCardDescription 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   1140
         TabIndex        =   10
         Top             =   1380
         Width           =   2415
      End
      Begin VB.CommandButton cmdNewStockCode 
         Caption         =   "New"
         Height          =   315
         Left            =   2700
         TabIndex        =   7
         Top             =   0
         Width           =   855
      End
      Begin VB.CommandButton cmdDeleteStockCode 
         Caption         =   "Delete"
         Enabled         =   0   'False
         Height          =   315
         Left            =   1140
         TabIndex        =   13
         Top             =   2700
         Width           =   1155
      End
      Begin VB.TextBox txtStockPrice 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   1140
         TabIndex        =   11
         Top             =   1800
         Width           =   2415
      End
      Begin VB.TextBox txtStockAlias 
         Height          =   315
         Left            =   1560
         TabIndex        =   9
         Top             =   840
         Width           =   1995
      End
      Begin VB.TextBox txtStockDescription 
         Height          =   315
         Left            =   1560
         TabIndex        =   8
         Top             =   480
         Width           =   1995
      End
      Begin VB.CommandButton cmdSaveStockCode 
         Caption         =   "Save"
         Height          =   315
         Left            =   2400
         TabIndex        =   14
         Top             =   2700
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Nominal"
         Height          =   375
         Index           =   8
         Left            =   180
         TabIndex        =   26
         Top             =   2160
         Width           =   795
      End
      Begin VB.Label lblCaption 
         Caption         =   "Rate Card Description"
         Height          =   375
         Index           =   3
         Left            =   180
         TabIndex        =   25
         Top             =   1320
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "�"
         Height          =   255
         Index           =   7
         Left            =   960
         TabIndex        =   24
         Top             =   1800
         Width           =   135
      End
      Begin VB.Label lblCaption 
         Caption         =   "Price"
         Height          =   255
         Index           =   4
         Left            =   180
         TabIndex        =   23
         Top             =   1800
         Width           =   435
      End
      Begin VB.Label lblCaption 
         Caption         =   "Stock Code"
         Height          =   255
         Index           =   6
         Left            =   180
         TabIndex        =   22
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Alias"
         Height          =   255
         Index           =   5
         Left            =   180
         TabIndex        =   21
         Top             =   840
         Width           =   1215
      End
   End
   Begin VB.ListBox lstStocktype 
      Height          =   2595
      Left            =   5460
      TabIndex        =   6
      Top             =   420
      Width           =   1755
   End
   Begin VB.Frame Frame1 
      Caption         =   "Format Information"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2115
      Left            =   2100
      TabIndex        =   17
      Top             =   360
      Width           =   3255
      Begin VB.CommandButton cmdAddNewFormat 
         Caption         =   "New"
         Height          =   315
         Left            =   2220
         TabIndex        =   1
         Top             =   0
         Width           =   915
      End
      Begin VB.CommandButton cmdDeleteFormat 
         Caption         =   "Delete"
         Enabled         =   0   'False
         Height          =   315
         Left            =   720
         TabIndex        =   4
         Top             =   1680
         Width           =   1155
      End
      Begin VB.TextBox txtFormatAlias 
         Height          =   315
         Left            =   1140
         TabIndex        =   3
         Top             =   840
         Width           =   1995
      End
      Begin VB.CommandButton cmdSaveFormat 
         Caption         =   "Save"
         Height          =   315
         Left            =   1980
         TabIndex        =   5
         Top             =   1680
         Width           =   1155
      End
      Begin VB.TextBox txtFormatDescription 
         Height          =   315
         Left            =   1140
         TabIndex        =   2
         Top             =   480
         Width           =   1995
      End
      Begin VB.Label lblCaption 
         Caption         =   "Alias"
         Height          =   255
         Index           =   2
         Left            =   180
         TabIndex        =   19
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Description"
         Height          =   255
         Index           =   1
         Left            =   180
         TabIndex        =   18
         Top             =   480
         Width           =   1215
      End
   End
   Begin VB.ListBox lstFormat 
      Height          =   2010
      Left            =   120
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   420
      Width           =   1875
   End
   Begin VB.Label lblCaption 
      Caption         =   "The list does not auto-refresh so that you can update lots of formats in one go. Refresh manually when you are done."
      Height          =   615
      Index           =   13
      Left            =   4020
      TabIndex        =   33
      Top             =   5520
      Width           =   3795
   End
   Begin VB.Label lblCaption 
      Caption         =   $"frmStockEditor.frx":003C
      Height          =   795
      Index           =   12
      Left            =   4020
      TabIndex        =   32
      Top             =   4620
      Width           =   3795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Red rows appear where the format is not in your list of pre-defined formats."
      Height          =   435
      Index           =   11
      Left            =   4020
      TabIndex        =   31
      Top             =   4080
      Width           =   3795
   End
   Begin VB.Label lblCaption 
      Caption         =   "The list to the left shows all the formats that are attached to records in the library database."
      Height          =   435
      Index           =   10
      Left            =   4020
      TabIndex        =   30
      Top             =   3540
      Width           =   3795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Stock Codes"
      Height          =   255
      Index           =   9
      Left            =   5520
      TabIndex        =   27
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Format List"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   16
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmStockEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdAddNewFormat_Click()

Dim l_strNewFormat  As String
l_strNewFormat = InputBox("Please enter the new format you want to add", "New Format")
If l_strNewFormat = "" Then Exit Sub

Dim l_strSQL As String
l_strSQL = "INSERT INTO xref (description, category) VALUES ('" & QuoteSanitise(l_strNewFormat) & "', 'format');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

lstFormat.Clear
PopulateCombo "format", lstFormat

txtFormatDescription.Text = l_strNewFormat

lstFormat.ListIndex = GetListIndex(lstFormat, l_strNewFormat)

End Sub

Private Sub cmdClose_Click()
Unload Me

End Sub

Private Sub cmdNewStockCode_Click()


If lstFormat.Text = "" Then MsgBox "Please select a format before adding stock codes.", vbExclamation: Exit Sub


Dim l_strNewStockCode  As String
l_strNewStockCode = InputBox("Please enter the new stock code you want to add", "New Stock Code")
If l_strNewStockCode = "" Then Exit Sub

Dim l_strSQL As String
l_strSQL = "INSERT INTO xref (description, category, format) VALUES ('" & QuoteSanitise(l_strNewStockCode) & "', 'stockcode', '" & QuoteSanitise(lstFormat.Text) & "');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

lstFormat_Click

txtStockDescription.Text = l_strNewStockCode

lstStocktype.ListIndex = GetListIndex(lstStocktype, l_strNewStockCode)


End Sub

Private Sub cmdRefresh_Click()

adoLibraryFormats.ConnectionString = g_strConnection
adoLibraryFormats.RecordSource = "SELECT format, COUNT(format) FROM library GROUP BY format ORDER BY format;"
adoLibraryFormats.Refresh

End Sub

Private Sub cmdSaveFormat_Click()
Screen.MousePointer = vbHourglass

Dim l_strSQL As String
l_strSQL = "UPDATE xref SET description = '" & QuoteSanitise(txtFormatDescription.Text) & "', descriptionalias = '" & QuoteSanitise(txtFormatAlias.Text) & "' WHERE category = 'format' AND description = '" & QuoteSanitise(lstFormat.Text) & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_strSQL = "UPDATE xref SET format = '" & QuoteSanitise(txtFormatDescription.Text) & "', format = '" & QuoteSanitise(txtFormatDescription.Text) & "' WHERE category = 'stockcode' AND format = '" & QuoteSanitise(lstFormat.Text) & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_strSQL = "UPDATE library SET format = '" & QuoteSanitise(txtFormatDescription.Text) & "' WHERE format = '" & QuoteSanitise(lstFormat.Text) & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

lstFormat.Clear
PopulateCombo "format", lstFormat

Screen.MousePointer = vbDefault

End Sub

Private Sub cmdSaveStockCode_Click()


'declare used variables
Dim l_strDescription As String
Dim l_strOldStockCode As String
Dim l_strSQL As String
Dim l_strMsg As Integer

'pick up the description in to a variable
l_strDescription = txtStockDescription.Text
l_strOldStockCode = lstStocktype.Text

'did the user mean to click this button?
l_strMsg = MsgBox("Are you sure you want to update the stock code from '" & l_strOldStockCode & "' to '" & l_strDescription & "'?", vbQuestion + vbYesNo)

'if not, quit out of the routine
If l_strMsg = vbNo Then Exit Sub

'set the mousepointer
Screen.MousePointer = vbHourglass

'update the xref
l_strSQL = "UPDATE xref SET description = '" & QuoteSanitise(l_strDescription) & "', descriptionalias = '" & QuoteSanitise(txtStockAlias.Text) & "' WHERE category = 'stockcode' AND description = '" & QuoteSanitise(lstStocktype.Text) & "' AND format = '" & QuoteSanitise(lstFormat.Text) & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

'check if the user wants to update the library
l_intMsg = MsgBox("Do you want to update any library records that have the old code to the new stock code?", vbQuestion + vbYesNo)

If l_intMsg = vbYes Then
    'update the library records to make sure they are all changed to the new code
    l_strSQL = "UPDATE library SET stockcode = '" & QuoteSanitise(l_strDescription) & "' WHERE stockcode = '" & QuoteSanitise(l_strOldStockCode) & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
End If

'update the ratecard
l_strSQL = "DELETE FROM ratecard WHERE cetacode = '" & QuoteSanitise(l_strOldStockCode) & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

'if the rate card description is blank then use the format and add the word stock to the end of it
If txtRateCardDescription.Text = "" Then
    txtRateCardDescription.Text = lstFormat.Text & " Stock (" & l_strDescription & ")"
End If

If GetData("ratecard", "description", "cetacode", l_strDescription) = "" Then
    'create a new row in the rate card for this stock code
    l_strSQL = "INSERT INTO ratecard (fd_orderby, category, cetacode, description, nominalcode, price1, price2, price3, price4, price5, price6, price7, price8, price9, price0, unit) VALUES ('99', 'Stock','" & QuoteSanitise(l_strDescription) & "','" & QuoteSanitise(txtRateCardDescription.Text) & "','" & QuoteSanitise(txtNominalCode.Text) & "', '" & Val(txtStockPrice.Text) & "', '" & Val(txtStockPrice.Text) & "', '" & Val(txtStockPrice.Text) & "', '" & Val(txtStockPrice.Text) & "', '" & Val(txtStockPrice.Text) & "', '" & Val(txtStockPrice.Text) & "', '" & Val(txtStockPrice.Text) & "', '" & Val(txtStockPrice.Text) & "', '" & Val(txtStockPrice.Text) & "', '" & Val(txtStockPrice.Text) & "', 'Each');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
Else
    MsgBox "A rate card entry already exists for this cost code, so please check that the existing details are correct for this record.", vbInformation
End If

'reset the list box and highlight the correct item
lstFormat_Click
lstStocktype.ListIndex = GetListIndex(lstStocktype, l_strDescription)

Screen.MousePointer = vbDefault

End Sub

Private Sub Command2_Click()



End Sub

Private Sub Form_Load()
PopulateCombo "Format", lstFormat
lblCaption(7).Caption = g_strCurrency

adoLibraryFormats.ConnectionString = g_strConnection
adoLibraryFormats.RecordSource = "SELECT format, COUNT(format) FROM library GROUP BY format ORDER BY format;"
adoLibraryFormats.Refresh


CenterForm Me


grdLibraryFormats.Refresh

End Sub

Private Sub grdLibraryFormats_DblClick()

If Not CheckAccess("/updatelibraryformat") Then Exit Sub

Dim l_strOriginalFormat As String
Dim l_strNewFormat As String, l_strNewStockCode As String

l_strOriginalFormat = grdLibraryFormats.Columns("format").Text
l_strNewFormat = lstFormat.Text

l_strNewStockCode = lstStocktype.Text

If l_strNewFormat = "" Then
    MsgBox "You must select a new format from the list in the top left of the form.", vbExclamation
    Exit Sub
End If

If chkStockCodeUpdate.Value <> 0 And l_strNewStockCode = "" Then
    MsgBox "You must select a new stock code from the list in the top of the form.", vbExclamation
    Exit Sub
End If

Dim l_intMsg As Integer
l_intMsg = MsgBox("Are you sure you want to change all the records in your library database from " & l_strOriginalFormat & " to use the format " & l_strNewFormat & " and the stock code to " & l_strNewStockCode & "?", vbYesNo + vbQuestion)
If l_intMsg = vbNo Then Exit Sub

Screen.MousePointer = vbHourglass

Dim l_strSQL As String
l_strSQL = "UPDATE library SET format = '" & QuoteSanitise(l_strNewFormat) & "'"
'if stock code tick box is checked
If chkStockCodeUpdate.Value <> 0 Then l_strSQL = l_strSQL & ", stockcode = '" & QuoteSanitise(l_strNewStockCode) & "'"

l_strSQL = l_strSQL & "WHERE format = '" & QuoteSanitise(l_strOriginalFormat) & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError


Screen.MousePointer = vbDefault

MsgBox "Update completed", vbExclamation


End Sub

Private Sub grdLibraryFormats_RowLoaded(ByVal Bookmark As Variant)
If GetData("xref", "description", "description", grdLibraryFormats.Columns("format").Text) = "" Then
    grdLibraryFormats.Columns("format").CellStyleSet "NotInXRef"
End If


End Sub

Private Sub lstFormat_Click()

lstStocktype.Clear
PopulateCombo "stockcodesforformat" & lstFormat.Text, lstStocktype

txtFormatDescription.Text = lstFormat.Text
txtFormatAlias.Text = GetData("xref", "descriptionalias", "description", lstFormat.Text)

txtStockAlias.Text = ""
txtStockDescription.Text = ""
txtStockPrice.Text = ""


End Sub

Private Sub lstStocktype_Click()

txtStockDescription.Text = lstStocktype.Text
txtStockAlias.Text = GetData("xref", "descriptionalias", "description", lstStocktype.Text)

txtStockPrice.Text = GetData("ratecard", "price1", "cetacode", lstStocktype.Text)
txtRateCardDescription.Text = GetData("ratecard", "description", "cetacode", lstStocktype.Text)
txtNominalCode.Text = GetData("ratecard", "nominalcode", "cetacode", lstStocktype.Text)

End Sub

