VERSION 5.00
Begin VB.Form frmUpdateTotal 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Update Total"
   ClientHeight    =   1770
   ClientLeft      =   6375
   ClientTop       =   4230
   ClientWidth     =   5880
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1770
   ScaleWidth      =   5880
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtOldTotal 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   1560
      Locked          =   -1  'True
      TabIndex        =   0
      ToolTipText     =   "The rate card total"
      Top             =   120
      Width           =   1275
   End
   Begin VB.TextBox txtNewTotal 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      BorderStyle     =   0  'None
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   1560
      TabIndex        =   1
      ToolTipText     =   "The total cost for the job"
      Top             =   540
      Width           =   1275
   End
   Begin VB.TextBox txtDifferencePercent 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1560
      TabIndex        =   2
      ToolTipText     =   "The total discount"
      Top             =   960
      Width           =   1275
   End
   Begin VB.CommandButton CancelButton 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   315
      Left            =   1680
      TabIndex        =   4
      ToolTipText     =   "Close Form"
      Top             =   1380
      Width           =   1155
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   315
      Left            =   360
      TabIndex        =   3
      ToolTipText     =   "Save updated total"
      Top             =   1380
      Width           =   1155
   End
   Begin VB.Label Label1 
      Caption         =   $"frmUpdateTotal.frx":0000
      Height          =   795
      Index           =   1
      Left            =   2940
      TabIndex        =   9
      Top             =   900
      Width           =   2835
   End
   Begin VB.Label Label1 
      Caption         =   "Currency values are now rounded to the nearest penny. This means that your totals may be slightly off."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Index           =   0
      Left            =   2940
      TabIndex        =   8
      Top             =   60
      Width           =   2835
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      Caption         =   "Rate Card Total �"
      Height          =   255
      Index           =   6
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   1335
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      Caption         =   "New Total �"
      Height          =   255
      Index           =   7
      Left            =   120
      TabIndex        =   6
      Top             =   540
      Width           =   1335
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      Caption         =   "Discount"
      Height          =   255
      Index           =   11
      Left            =   120
      TabIndex        =   5
      Top             =   960
      Width           =   1335
   End
End
Attribute VB_Name = "frmUpdateTotal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub CancelButton_Click()
txtDifferencePercent.Text = "CANCEL"
Me.Hide
End Sub

Private Sub OKButton_Click()
Me.Hide
End Sub

Private Sub txtDifferencePercent_KeyUp(KeyCode As Integer, Shift As Integer)
Dim l_dblDiscount As Double
l_dblDiscount = (Val(txtOldTotal.Text) / 100) * Val(txtDifferencePercent.Text)

txtNewTotal.Text = Val(txtOldTotal.Text) - l_dblDiscount
End Sub

Private Sub txtNewTotal_KeyUp(KeyCode As Integer, Shift As Integer)
txtDifferencePercent.Text = GetPercentageDiscount(Val(txtOldTotal.Text), Val(txtNewTotal.Text))
End Sub
