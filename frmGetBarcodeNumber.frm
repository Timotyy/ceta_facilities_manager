VERSION 5.00
Begin VB.Form frmGetBarcodeNumber 
   Caption         =   "Barcode"
   ClientHeight    =   1035
   ClientLeft      =   7530
   ClientTop       =   8580
   ClientWidth     =   4200
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmGetBarcodeNumber.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   1035
   ScaleWidth      =   4200
   Begin VB.CommandButton cmdIssueNext 
      Caption         =   "Issue Next"
      Height          =   315
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   1335
   End
   Begin VB.CommandButton cmbOK 
      Caption         =   "OK"
      Height          =   315
      Left            =   2820
      TabIndex        =   1
      ToolTipText     =   "Accept the offered Barcode"
      Top             =   600
      Width           =   1275
   End
   Begin VB.TextBox txtBarcode 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   2160
      TabIndex        =   0
      ToolTipText     =   "Hit return to accept the offered barcode, or type or scan a new one"
      Top             =   120
      Width           =   1935
   End
   Begin VB.Label Label1 
      Caption         =   "Scan or Type the Barcode"
      Height          =   315
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   1995
   End
End
Attribute VB_Name = "frmGetBarcodeNumber"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmbOK_Click()
Me.Hide
End Sub

Private Sub cmdIssueNext_Click()


Dim l_intMsg As Integer


l_intMsg = MsgBox("Do you want to create a new library record, using the next available barcode number?", vbQuestion + vbYesNo)

If l_intMsg = vbYes Then

    cmdIssueNext.Caption = "Generating"
    DoEvents

    Dim l_strNextBarcode As String
    l_strNextBarcode = Format(GetNextSequence("barcodenumber"), "00000")

    txtBarcode.Text = l_strNextBarcode

    DoEvents

    Dim l_lngLibraryID As Long
    l_lngLibraryID = CreateLibraryItem(l_strNextBarcode, "NEW ITEM")
    
    DoEvents
    
    SetData "library", "location", "libraryID", l_lngLibraryID, "BLANK STOCK"
    SetData "library", "format", "libraryID", l_lngLibraryID, frmLibrary.cmbRecordFormat.Text
    SetData "library", "stocktype", "libraryID", l_lngLibraryID, frmLibrary.cmbStockCode.Text
    
    DoEvents
    
    txtBarcode_KeyPress vbKeyReturn


    
    
    Me.Caption = "Issue Next"
    DoEvents

End If

End Sub

Private Sub Form_Load()
MakeLookLikeOffice Me
End Sub

Private Sub txtBarcode_GotFocus()
txtBarcode.SelStart = 0
txtBarcode.SelLength = Len(txtBarcode.Text)
End Sub

Private Sub txtBarcode_KeyPress(KeyAscii As Integer)

If KeyAscii = vbKeyEscape Then
    txtBarcode.Text = ""
    KeyAscii = 0
    Me.Hide
ElseIf KeyAscii = vbKeyReturn Then
    KeyAscii = 0
    Me.Hide
End If

End Sub
