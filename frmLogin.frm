VERSION 5.00
Begin VB.Form frmLogin 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CETA Login"
   ClientHeight    =   1305
   ClientLeft      =   7590
   ClientTop       =   6660
   ClientWidth     =   4080
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLogin.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1305
   ScaleWidth      =   4080
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Interval        =   20000
      Left            =   60
      Top             =   600
   End
   Begin VB.PictureBox picImage 
      BorderStyle     =   0  'None
      Height          =   585
      Left            =   180
      Picture         =   "frmLogin.frx":08CA
      ScaleHeight     =   585
      ScaleWidth      =   585
      TabIndex        =   6
      Top             =   180
      Width           =   585
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   315
      Left            =   1560
      TabIndex        =   2
      ToolTipText     =   "Log in to CETA using the above credentials"
      Top             =   900
      Width           =   1155
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   315
      Left            =   2820
      TabIndex        =   3
      ToolTipText     =   "Quit CETA completely"
      Top             =   900
      Width           =   1155
   End
   Begin VB.TextBox txtUserName 
      Height          =   315
      Left            =   1800
      TabIndex        =   0
      ToolTipText     =   "Your unique user name"
      Top             =   120
      Width           =   2175
   End
   Begin VB.TextBox txtPassword 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   1800
      PasswordChar    =   "*"
      TabIndex        =   1
      ToolTipText     =   "Non case-sensitive password"
      Top             =   480
      Width           =   2175
   End
   Begin VB.Label lblCaption 
      ForeColor       =   &H80000011&
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   7
      Top             =   960
      Width           =   1335
   End
   Begin VB.Label lblCaption 
      Caption         =   "User Name"
      Height          =   255
      Index           =   4
      Left            =   900
      TabIndex        =   5
      Top             =   120
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Password"
      Height          =   255
      Index           =   8
      Left            =   900
      TabIndex        =   4
      Top             =   480
      Width           =   1035
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()
ExitCETA True
End Sub

Private Sub cmdOK_Click()

Dim l_blnResult As Boolean, l_strUserName As String, l_strPassword As String

l_strUserName = txtUserName.Text
l_strPassword = txtPassword.Text

l_blnResult = CheckLogon(l_strUserName, l_strPassword)

If l_blnResult = True Then
    Me.Hide
    ShowNotices
    MDIForm1.Show
Else
    MsgBox "Your login details are invalid.", vbExclamation
    On Error Resume Next
    txtUserName.SetFocus
End If

End Sub

Private Sub lblPasswordChange_Click()

If txtUserName.Text = "" Then
    MsgBox "Please enter your username before trying to change your password", vbExclamation
    txtUserName.SetFocus
    Exit Sub
End If

End Sub

Private Sub Timer1_Timer()

g_optCloseSystemDown = GetFlag(GetData("setting", "value", "name", "CloseSystemDown"))

If g_optCloseSystemDown <> 0 Then Me.Hide

End Sub

Private Sub txtPassword_GotFocus()
HighLite txtPassword
End Sub

Private Sub txtUserName_GotFocus()
HighLite txtUserName
End Sub

Private Sub Form_Load()
    
    lblCaption(0).Caption = "V. " & App.Major & "." & App.Minor & "." & App.Revision
    CenterForm Me
    DoEvents
    
End Sub
