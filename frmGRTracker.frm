VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmGRTracker 
   Caption         =   "Grade & Restore Tracker"
   ClientHeight    =   14670
   ClientLeft      =   3060
   ClientTop       =   3210
   ClientWidth     =   26010
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   14670
   ScaleWidth      =   26010
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtReference 
      Height          =   315
      Left            =   1740
      TabIndex        =   49
      Top             =   1620
      Width           =   2895
   End
   Begin VB.CommandButton cmdCopyBack 
      Caption         =   "Copy Back Into Tracker"
      Height          =   315
      Left            =   15360
      TabIndex        =   47
      Top             =   1440
      Width           =   2235
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "All Items"
      Height          =   255
      Index           =   3
      Left            =   15360
      TabIndex        =   44
      Tag             =   "NOCLEAR"
      Top             =   1080
      Width           =   2355
   End
   Begin VB.CheckBox chkLockGB 
      Alignment       =   1  'Right Justify
      Caption         =   "Lock GB size"
      Height          =   255
      Left            =   180
      TabIndex        =   43
      Top             =   1260
      Width           =   1755
   End
   Begin VB.CheckBox chkLockDur 
      Alignment       =   1  'Right Justify
      Caption         =   "Lock Duration"
      Height          =   255
      Left            =   180
      TabIndex        =   42
      Top             =   960
      Width           =   1755
   End
   Begin VB.TextBox txtTotalSize 
      Height          =   315
      Left            =   20040
      TabIndex        =   40
      Top             =   480
      Width           =   1275
   End
   Begin VB.TextBox txtheaderint5 
      Height          =   315
      Left            =   13920
      TabIndex        =   36
      Top             =   1620
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.TextBox txtheaderint4 
      Height          =   315
      Left            =   13920
      TabIndex        =   35
      Top             =   1260
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.TextBox txtheaderint3 
      Height          =   315
      Left            =   13920
      TabIndex        =   34
      Top             =   900
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.TextBox txtTotalDuration 
      Height          =   315
      Left            =   20040
      TabIndex        =   32
      Top             =   120
      Width           =   1275
   End
   Begin VB.TextBox txtheadertext5 
      Height          =   315
      Left            =   7020
      TabIndex        =   28
      Top             =   1620
      Visible         =   0   'False
      Width           =   4515
   End
   Begin VB.TextBox txtheadertext4 
      Height          =   315
      Left            =   7020
      TabIndex        =   27
      Top             =   1260
      Visible         =   0   'False
      Width           =   4515
   End
   Begin VB.TextBox txtheadertext3 
      Height          =   315
      Left            =   7020
      TabIndex        =   26
      Top             =   900
      Visible         =   0   'False
      Width           =   4515
   End
   Begin VB.TextBox txtheaderint2 
      Height          =   315
      Left            =   13920
      TabIndex        =   22
      Top             =   540
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.TextBox txtheaderint1 
      Height          =   315
      Left            =   13920
      TabIndex        =   19
      Top             =   180
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.TextBox txtheadertext2 
      Height          =   315
      Left            =   7020
      TabIndex        =   18
      Top             =   540
      Visible         =   0   'False
      Width           =   4515
   End
   Begin VB.TextBox txtheadertext1 
      Height          =   315
      Left            =   7020
      TabIndex        =   15
      Top             =   180
      Visible         =   0   'False
      Width           =   4515
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Billed"
      Height          =   255
      Index           =   2
      Left            =   15360
      TabIndex        =   11
      Tag             =   "NOCLEAR"
      Top             =   780
      Width           =   2355
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Finished"
      Height          =   255
      Index           =   1
      Left            =   15360
      TabIndex        =   10
      Tag             =   "NOCLEAR"
      Top             =   480
      Width           =   2355
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete"
      Height          =   255
      Index           =   0
      Left            =   15360
      TabIndex        =   9
      Tag             =   "NOCLEAR"
      Top             =   180
      Width           =   2355
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   7800
      TabIndex        =   4
      Top             =   14100
      Width           =   17835
      Begin VB.CommandButton cmdUnbillAll 
         Caption         =   "Unmark Billing All"
         Height          =   315
         Left            =   1380
         TabIndex        =   46
         Top             =   0
         Width           =   2235
      End
      Begin VB.CommandButton cmdBillAll 
         Caption         =   "Bill All"
         Height          =   315
         Left            =   3780
         TabIndex        =   45
         Top             =   0
         Width           =   2235
      End
      Begin VB.CommandButton cmdUnbill 
         Caption         =   "Unmark as Billed"
         Height          =   315
         Left            =   6180
         TabIndex        =   25
         Top             =   0
         Visible         =   0   'False
         Width           =   2235
      End
      Begin VB.CommandButton cmdManualBillItem 
         Caption         =   "Manually Mark as Billed"
         Height          =   315
         Left            =   8580
         TabIndex        =   24
         Top             =   0
         Visible         =   0   'False
         Width           =   2235
      End
      Begin VB.CommandButton cmdBillItem 
         Caption         =   "Bill Item"
         Height          =   315
         Left            =   10920
         TabIndex        =   23
         Top             =   0
         Visible         =   0   'False
         Width           =   1755
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   16560
         TabIndex        =   8
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   15300
         TabIndex        =   7
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   14040
         TabIndex        =   6
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   12780
         TabIndex        =   5
         Top             =   0
         Width           =   1215
      End
   End
   Begin MSAdodcLib.Adodc adoItems 
      Height          =   330
      Left            =   120
      Top             =   2100
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoItems"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdItems 
      Height          =   9915
      Left            =   120
      TabIndex        =   3
      Top             =   2100
      Width           =   25545
      _Version        =   196617
      stylesets.count =   3
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmGRTracker.frx":0000
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmGRTracker.frx":001C
      stylesets(2).Name=   "headerfield"
      stylesets(2).ForeColor=   0
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmGRTracker.frx":0038
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   37
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "tracker_itemID"
      Columns(0).Name =   "tracker_itemID"
      Columns(0).DataField=   "tracker_itemID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   4233
      Columns(2).Caption=   "Ref"
      Columns(2).Name =   "itemreference"
      Columns(2).DataField=   "itemreference"
      Columns(2).FieldLen=   256
      Columns(2).StyleSet=   "headerfield"
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "Filename"
      Columns(3).Name =   "itemfilename"
      Columns(3).DataField=   "itemfilename"
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "headerfield"
      Columns(4).Width=   979
      Columns(4).Caption=   "Dur"
      Columns(4).Name =   "duration"
      Columns(4).DataField=   "duration"
      Columns(4).FieldLen=   256
      Columns(4).StyleSet=   "headerfield"
      Columns(5).Width=   1323
      Columns(5).Caption=   "GB Sent"
      Columns(5).Name =   "gbsent"
      Columns(5).DataField=   "gbsent"
      Columns(5).FieldLen=   256
      Columns(5).StyleSet=   "headerfield"
      Columns(6).Width=   4233
      Columns(6).Caption=   "headertext1"
      Columns(6).Name =   "headertext1"
      Columns(6).DataField=   "headertext1"
      Columns(6).FieldLen=   256
      Columns(6).StyleSet=   "headerfield"
      Columns(7).Width=   4233
      Columns(7).Caption=   "headertext2"
      Columns(7).Name =   "headertext2"
      Columns(7).DataField=   "headertext2"
      Columns(7).FieldLen=   256
      Columns(7).StyleSet=   "headerfield"
      Columns(8).Width=   4233
      Columns(8).Caption=   "headertext3"
      Columns(8).Name =   "headertext3"
      Columns(8).DataField=   "headertext3"
      Columns(8).FieldLen=   256
      Columns(8).StyleSet=   "headerfield"
      Columns(9).Width=   4233
      Columns(9).Caption=   "headertext4"
      Columns(9).Name =   "headertext4"
      Columns(9).DataField=   "headertext4"
      Columns(9).FieldLen=   256
      Columns(9).StyleSet=   "headerfield"
      Columns(10).Width=   4233
      Columns(10).Caption=   "headertext5"
      Columns(10).Name=   "headertext5"
      Columns(10).DataField=   "headertext5"
      Columns(10).FieldLen=   256
      Columns(10).StyleSet=   "headerfield"
      Columns(11).Width=   1773
      Columns(11).Caption=   "headerint1"
      Columns(11).Name=   "headerint1"
      Columns(11).DataField=   "headerint1"
      Columns(11).FieldLen=   256
      Columns(11).StyleSet=   "headerfield"
      Columns(12).Width=   1773
      Columns(12).Caption=   "headerint2"
      Columns(12).Name=   "headerint2"
      Columns(12).DataField=   "headerint2"
      Columns(12).FieldLen=   256
      Columns(12).StyleSet=   "headerfield"
      Columns(13).Width=   1773
      Columns(13).Caption=   "headerint3"
      Columns(13).Name=   "headerint3"
      Columns(13).DataField=   "headerint3"
      Columns(13).FieldLen=   256
      Columns(13).StyleSet=   "headerfield"
      Columns(14).Width=   1773
      Columns(14).Caption=   "headerint4"
      Columns(14).Name=   "headerint4"
      Columns(14).DataField=   "headerint4"
      Columns(14).FieldLen=   256
      Columns(14).StyleSet=   "headerfield"
      Columns(15).Width=   1773
      Columns(15).Caption=   "headerint5"
      Columns(15).Name=   "headerint5"
      Columns(15).DataField=   "headerint5"
      Columns(15).FieldLen=   256
      Columns(15).StyleSet=   "headerfield"
      Columns(16).Width=   2646
      Columns(16).Caption=   "stagefield1"
      Columns(16).Name=   "stagefield1"
      Columns(16).DataField=   "stagefield1"
      Columns(16).FieldLen=   256
      Columns(16).Style=   1
      Columns(16).StyleSet=   "stagefield"
      Columns(17).Width=   2646
      Columns(17).Caption=   "stagefield2"
      Columns(17).Name=   "stagefield2"
      Columns(17).DataField=   "stagefield2"
      Columns(17).FieldLen=   256
      Columns(17).Style=   1
      Columns(17).StyleSet=   "stagefield"
      Columns(18).Width=   2646
      Columns(18).Caption=   "stagefield3"
      Columns(18).Name=   "stagefield3"
      Columns(18).DataField=   "stagefield3"
      Columns(18).FieldLen=   256
      Columns(18).Style=   1
      Columns(18).StyleSet=   "stagefield"
      Columns(19).Width=   2646
      Columns(19).Caption=   "stagefield4"
      Columns(19).Name=   "stagefield4"
      Columns(19).DataField=   "stagefield4"
      Columns(19).FieldLen=   256
      Columns(19).Style=   1
      Columns(19).StyleSet=   "stagefield"
      Columns(20).Width=   2646
      Columns(20).Caption=   "stagefield5"
      Columns(20).Name=   "stagefield5"
      Columns(20).DataField=   "stagefield5"
      Columns(20).FieldLen=   256
      Columns(20).Style=   1
      Columns(20).StyleSet=   "stagefield"
      Columns(21).Width=   2646
      Columns(21).Caption=   "stagefield6"
      Columns(21).Name=   "stagefield6"
      Columns(21).DataField=   "stagefield6"
      Columns(21).FieldLen=   256
      Columns(21).Style=   1
      Columns(21).StyleSet=   "stagefield"
      Columns(22).Width=   2646
      Columns(22).Caption=   "stagefield7"
      Columns(22).Name=   "stagefield7"
      Columns(22).DataField=   "stagefield7"
      Columns(22).FieldLen=   256
      Columns(22).Style=   1
      Columns(22).StyleSet=   "stagefield"
      Columns(23).Width=   2646
      Columns(23).Caption=   "stagefield8"
      Columns(23).Name=   "stagefield8"
      Columns(23).DataField=   "stagefield8"
      Columns(23).FieldLen=   256
      Columns(23).Style=   1
      Columns(23).StyleSet=   "stagefield"
      Columns(24).Width=   2646
      Columns(24).Caption=   "stagefield9"
      Columns(24).Name=   "stagefield9"
      Columns(24).DataField=   "stagefield9"
      Columns(24).FieldLen=   256
      Columns(24).Style=   1
      Columns(24).StyleSet=   "stagefield"
      Columns(25).Width=   2646
      Columns(25).Caption=   "stagefield10"
      Columns(25).Name=   "stagefield10"
      Columns(25).DataField=   "stagefield10"
      Columns(25).FieldLen=   256
      Columns(25).Style=   1
      Columns(25).StyleSet=   "stagefield"
      Columns(26).Width=   2646
      Columns(26).Caption=   "stagefield11"
      Columns(26).Name=   "stagefield11"
      Columns(26).DataField=   "stagefield11"
      Columns(26).FieldLen=   256
      Columns(26).Style=   1
      Columns(26).StyleSet=   "stagefield"
      Columns(27).Width=   2646
      Columns(27).Caption=   "stagefield12"
      Columns(27).Name=   "stagefield12"
      Columns(27).DataField=   "stagefield12"
      Columns(27).FieldLen=   256
      Columns(27).Style=   1
      Columns(27).StyleSet=   "stagefield"
      Columns(28).Width=   2646
      Columns(28).Caption=   "stagefield13"
      Columns(28).Name=   "stagefield13"
      Columns(28).DataField=   "stagefield13"
      Columns(28).FieldLen=   256
      Columns(28).Style=   1
      Columns(28).StyleSet=   "stagefield"
      Columns(29).Width=   2646
      Columns(29).Caption=   "stagefield14"
      Columns(29).Name=   "stagefield14"
      Columns(29).DataField=   "stagefield14"
      Columns(29).FieldLen=   256
      Columns(29).Style=   1
      Columns(29).StyleSet=   "stagefield"
      Columns(30).Width=   2646
      Columns(30).Caption=   "stagefield15"
      Columns(30).Name=   "stagefield15"
      Columns(30).DataField=   "stagefield15"
      Columns(30).FieldLen=   256
      Columns(30).Style=   1
      Columns(30).StyleSet=   "stagefield"
      Columns(31).Width=   1376
      Columns(31).Caption=   "conclusionfield1"
      Columns(31).Name=   "conclusionfield1"
      Columns(31).DataField=   "conclusionfield1"
      Columns(31).FieldLen=   256
      Columns(31).Locked=   -1  'True
      Columns(31).Style=   2
      Columns(31).StyleSet=   "conclusionfield"
      Columns(32).Width=   1376
      Columns(32).Caption=   "conclusionfield2"
      Columns(32).Name=   "conclusionfield2"
      Columns(32).DataField=   "conclusionfield2"
      Columns(32).FieldLen=   256
      Columns(32).Locked=   -1  'True
      Columns(32).Style=   2
      Columns(32).StyleSet=   "conclusionfield"
      Columns(33).Width=   1376
      Columns(33).Caption=   "conclusionfield3"
      Columns(33).Name=   "conclusionfield3"
      Columns(33).DataField=   "conclusionfield3"
      Columns(33).FieldLen=   256
      Columns(33).Locked=   -1  'True
      Columns(33).Style=   2
      Columns(33).StyleSet=   "conclusionfield"
      Columns(34).Width=   1376
      Columns(34).Caption=   "Finished?"
      Columns(34).Name=   "readytobill"
      Columns(34).DataField=   "readytobill"
      Columns(34).FieldLen=   256
      Columns(34).Locked=   -1  'True
      Columns(34).Style=   2
      Columns(34).StyleSet=   "conclusionfield"
      Columns(35).Width=   3200
      Columns(35).Visible=   0   'False
      Columns(35).Caption=   "jobID"
      Columns(35).Name=   "jobID"
      Columns(35).DataField=   "jobID"
      Columns(35).FieldLen=   256
      Columns(36).Width=   1376
      Columns(36).Caption=   "Billed?"
      Columns(36).Name=   "billed"
      Columns(36).DataField=   "billed"
      Columns(36).FieldLen=   256
      Columns(36).Locked=   -1  'True
      Columns(36).Style=   2
      Columns(36).StyleSet=   "conclusionfield"
      _ExtentX        =   45059
      _ExtentY        =   17489
      _StockProps     =   79
      Caption         =   "Tracker Items"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CheckBox chkHideDemo 
      Alignment       =   1  'Right Justify
      Caption         =   "Hide Demo"
      Height          =   255
      Left            =   180
      TabIndex        =   0
      Tag             =   "NOCLEAR"
      Top             =   540
      Value           =   1  'Checked
      Width           =   1755
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   1740
      TabIndex        =   1
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The company this job is for"
      Top             =   180
      Width           =   2895
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin MSAdodcLib.Adodc adoComments 
      Height          =   330
      Left            =   1920
      Top             =   13260
      Visible         =   0   'False
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoComments"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdComments 
      Height          =   1875
      Left            =   120
      TabIndex        =   13
      Top             =   12060
      Width           =   12015
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12582847
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   5
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "trackerhistoryID"
      Columns(0).Name =   "tracker_commentID"
      Columns(0).DataField=   "tracker_commentID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "trackerprogramID"
      Columns(1).Name =   "tracker_itemID"
      Columns(1).DataField=   "tracker_itemID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   13070
      Columns(2).Caption=   "Comments"
      Columns(2).Name =   "comment"
      Columns(2).DataField=   "comment"
      Columns(2).FieldLen=   255
      Columns(3).Width=   3360
      Columns(3).Caption=   "Date"
      Columns(3).Name =   "cdate"
      Columns(3).DataField=   "cdate"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).Style=   1
      Columns(4).Width=   3519
      Columns(4).Caption=   "Entered By"
      Columns(4).Name =   "cuser"
      Columns(4).DataField=   "cuser"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      UseDefaults     =   0   'False
      _ExtentX        =   21193
      _ExtentY        =   3307
      _StockProps     =   79
      Caption         =   "Tracker Comments"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker datFrom 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   20040
      TabIndex        =   50
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   1260
      Visible         =   0   'False
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   164757505
      CurrentDate     =   39580
   End
   Begin MSComCtl2.DTPicker datTo 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   20040
      TabIndex        =   51
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   1620
      Visible         =   0   'False
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   164757505
      CurrentDate     =   39580
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSearchField 
      Height          =   315
      Left            =   20040
      TabIndex        =   55
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The company this job is for"
      Top             =   900
      Visible         =   0   'False
      Width           =   1455
      DataFieldList   =   "itemheading"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "itemfield"
      Columns(0).Name =   "itemfield"
      Columns(0).DataField=   "itemfield"
      Columns(0).FieldLen=   256
      Columns(1).Width=   6641
      Columns(1).Caption=   "itemheading"
      Columns(1).Name =   "itemheading"
      Columns(1).DataField=   "itemheading"
      Columns(1).FieldLen=   256
      _ExtentX        =   2566
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "itemheading"
   End
   Begin VB.Label lblCaption 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   16
      Left            =   18840
      TabIndex        =   54
      Top             =   960
      Visible         =   0   'False
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   15
      Left            =   18840
      TabIndex        =   53
      Top             =   1320
      Visible         =   0   'False
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "End"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   14
      Left            =   18900
      TabIndex        =   52
      Top             =   1680
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Search Reference"
      Height          =   255
      Index           =   13
      Left            =   210
      TabIndex        =   48
      Top             =   1680
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Total Size (GB)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   12
      Left            =   17940
      TabIndex        =   41
      Top             =   540
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Total Duration"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   11
      Left            =   17940
      TabIndex        =   39
      Top             =   180
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   9
      Left            =   11820
      TabIndex        =   38
      Top             =   1680
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   8
      Left            =   11820
      TabIndex        =   37
      Top             =   1320
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   7
      Left            =   11820
      TabIndex        =   33
      Top             =   960
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   11820
      TabIndex        =   31
      Top             =   600
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   11820
      TabIndex        =   30
      Top             =   240
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   4800
      TabIndex        =   29
      Top             =   1680
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   4800
      TabIndex        =   21
      Top             =   1320
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   4800
      TabIndex        =   20
      Top             =   960
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   4800
      TabIndex        =   17
      Top             =   600
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   4800
      TabIndex        =   16
      Top             =   240
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   2100
      TabIndex        =   14
      Top             =   540
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCompanyID 
      Height          =   255
      Left            =   3360
      TabIndex        =   12
      Top             =   600
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   10
      Left            =   210
      TabIndex        =   2
      Top             =   240
      Width           =   915
   End
End
Attribute VB_Name = "frmGRTracker"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strSearch As String
Dim m_strOrderby As String
Dim m_blnStageField1conc1 As Boolean, m_blnStageField2conc1 As Boolean, m_blnStageField3conc1 As Boolean, m_blnStageField4conc1 As Boolean, m_blnStageField5conc1 As Boolean
Dim m_blnStageField6conc1 As Boolean, m_blnStageField7conc1 As Boolean, m_blnStageField8conc1 As Boolean, m_blnStageField9conc1 As Boolean, m_blnStageField10conc1 As Boolean
Dim m_blnStageField11conc1 As Boolean, m_blnStageField12conc1 As Boolean, m_blnStageField13conc1 As Boolean, m_blnStageField14conc1 As Boolean, m_blnStageField15conc1 As Boolean
Dim m_blnStageField1conc2 As Boolean, m_blnStageField2conc2 As Boolean, m_blnStageField3conc2 As Boolean, m_blnStageField4conc2 As Boolean, m_blnStageField5conc2 As Boolean
Dim m_blnStageField6conc2 As Boolean, m_blnStageField7conc2 As Boolean, m_blnStageField8conc2 As Boolean, m_blnStageField9conc2 As Boolean, m_blnStageField10conc2 As Boolean
Dim m_blnStageField11conc2 As Boolean, m_blnStageField12conc2 As Boolean, m_blnStageField13conc2 As Boolean, m_blnStageField14conc2 As Boolean, m_blnStageField15conc2 As Boolean
Dim m_blnStageField1conc3 As Boolean, m_blnStageField2conc3 As Boolean, m_blnStageField3conc3 As Boolean, m_blnStageField4conc3 As Boolean, m_blnStageField5conc3 As Boolean
Dim m_blnStageField6conc3 As Boolean, m_blnStageField7conc3 As Boolean, m_blnStageField8conc3 As Boolean, m_blnStageField9conc3 As Boolean, m_blnStageField10conc3 As Boolean
Dim m_blnStageField11conc3 As Boolean, m_blnStageField12conc3 As Boolean, m_blnStageField13conc3 As Boolean, m_blnStageField14conc3 As Boolean, m_blnStageField15conc3 As Boolean
Dim m_blnStageField1conc As Boolean, m_blnStageField2conc As Boolean, m_blnStageField3conc As Boolean, m_blnStageField4conc As Boolean, m_blnStageField5conc As Boolean
Dim m_blnStageField6conc As Boolean, m_blnStageField7conc As Boolean, m_blnStageField8conc As Boolean, m_blnStageField9conc As Boolean, m_blnStageField10conc As Boolean
Dim m_blnStageField11conc As Boolean, m_blnStageField12conc As Boolean, m_blnStageField13conc As Boolean, m_blnStageField14conc As Boolean, m_blnStageField15conc As Boolean

Private Sub HideAllColumns()

grdItems.Columns("stagefield1").Visible = False
grdItems.Columns("stagefield2").Visible = False
grdItems.Columns("stagefield3").Visible = False
grdItems.Columns("stagefield4").Visible = False
grdItems.Columns("stagefield5").Visible = False
grdItems.Columns("stagefield6").Visible = False
grdItems.Columns("stagefield7").Visible = False
grdItems.Columns("stagefield8").Visible = False
grdItems.Columns("stagefield9").Visible = False
grdItems.Columns("stagefield10").Visible = False
grdItems.Columns("stagefield11").Visible = False
grdItems.Columns("stagefield12").Visible = False
grdItems.Columns("stagefield13").Visible = False
grdItems.Columns("stagefield14").Visible = False
grdItems.Columns("stagefield15").Visible = False
grdItems.Columns("conclusionfield1").Visible = False
grdItems.Columns("conclusionfield2").Visible = False
grdItems.Columns("conclusionfield3").Visible = False
grdItems.Columns("headertext1").Visible = False
grdItems.Columns("headertext2").Visible = False
grdItems.Columns("headertext3").Visible = False
grdItems.Columns("headertext4").Visible = False
grdItems.Columns("headertext5").Visible = False
grdItems.Columns("headerint1").Visible = False
grdItems.Columns("headerint2").Visible = False
grdItems.Columns("headerint3").Visible = False
grdItems.Columns("headerint4").Visible = False
grdItems.Columns("headerint5").Visible = False
lblCaption(0).Visible = False
lblCaption(1).Visible = False
lblCaption(2).Visible = False
lblCaption(3).Visible = False
lblCaption(4).Visible = False
lblCaption(5).Visible = False
lblCaption(6).Visible = False
lblCaption(7).Visible = False
lblCaption(8).Visible = False
lblCaption(9).Visible = False
txtheadertext1.Visible = False
txtheadertext2.Visible = False
txtheadertext3.Visible = False
txtheadertext4.Visible = False
txtheadertext5.Visible = False
txtheaderint1.Visible = False
txtheaderint2.Visible = False
txtheaderint3.Visible = False
txtheaderint4.Visible = False
txtheaderint5.Visible = False

m_blnStageField1conc1 = False
m_blnStageField2conc1 = False
m_blnStageField3conc1 = False
m_blnStageField4conc1 = False
m_blnStageField5conc1 = False
m_blnStageField6conc1 = False
m_blnStageField7conc1 = False
m_blnStageField8conc1 = False
m_blnStageField9conc1 = False
m_blnStageField10conc1 = False
m_blnStageField11conc1 = False
m_blnStageField12conc1 = False
m_blnStageField13conc1 = False
m_blnStageField14conc1 = False
m_blnStageField15conc1 = False

m_blnStageField1conc2 = False
m_blnStageField2conc2 = False
m_blnStageField3conc2 = False
m_blnStageField4conc2 = False
m_blnStageField5conc2 = False
m_blnStageField6conc2 = False
m_blnStageField7conc2 = False
m_blnStageField8conc2 = False
m_blnStageField9conc2 = False
m_blnStageField10conc2 = False
m_blnStageField11conc2 = False
m_blnStageField12conc2 = False
m_blnStageField13conc2 = False
m_blnStageField14conc2 = False
m_blnStageField15conc2 = False

m_blnStageField1conc3 = False
m_blnStageField2conc3 = False
m_blnStageField3conc3 = False
m_blnStageField4conc3 = False
m_blnStageField5conc3 = False
m_blnStageField6conc3 = False
m_blnStageField7conc3 = False
m_blnStageField8conc3 = False
m_blnStageField9conc3 = False
m_blnStageField10conc3 = False
m_blnStageField11conc3 = False
m_blnStageField12conc3 = False
m_blnStageField13conc3 = False
m_blnStageField14conc3 = False
m_blnStageField15conc3 = False

End Sub

Private Sub adoItems_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

Dim l_strSQL As String

If adoItems.Recordset.EOF = True Then
    cmdBillItem.Visible = False
    Exit Sub
Else
    If adoItems.Recordset("tracker_itemID") = "" Then
        cmdBillItem.Visible = False
        Exit Sub
    End If
End If

lblTrackeritemID.Caption = adoItems.Recordset("tracker_itemID")

l_strSQL = "SELECT * FROM tracker_comment WHERE tracker_itemID = " & adoItems.Recordset("tracker_itemID") & "ORDER BY cdate;"

adoComments.RecordSource = l_strSQL
adoComments.ConnectionString = g_strConnection
adoComments.Refresh

If adoItems.Recordset("readytobill") <> 0 And adoItems.Recordset("billed") = 0 Then
    cmdBillItem.Visible = True
    cmdManualBillItem.Visible = True
Else
    cmdBillItem.Visible = False
    cmdManualBillItem.Visible = False
End If

If adoItems.Recordset("billed") <> 0 Then
    cmdUnbill.Visible = True
Else
    cmdUnbill.Visible = False
End If

End Sub

Private Sub chkHideDemo_Click()

Dim l_strSQL As String

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%tracker%' and companyID > 100 AND system_active = 1 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%tracker%' AND system_active = 1 ORDER BY name;"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

cmdSearch.Value = True

End Sub

Private Sub cmbCompany_Click()

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text
m_strSearch = " WHERE companyID = " & lblCompanyID.Caption
m_strOrderby = " ORDER BY headertext1, headerint1, headerint2, headerint3, headerint4, headerint5"

HideAllColumns

If InStr(GetData("company", "cetaclientcode", "companyID", cmbCompany.Columns("companyID").Text), "/trackerfilename") > 0 Then
    grdItems.Columns("itemfilename").Visible = True
    grdItems.Columns("itemfilename").Width = 3000
Else
    grdItems.Columns("itemfilename").Visible = False
End If

DoEvents

Dim l_rstChoices As ADODB.Recordset

Set l_rstChoices = ExecuteSQL("SELECT * FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & ";", g_strExecuteError)
CheckForSQLError

If l_rstChoices.RecordCount > 0 Then
    l_rstChoices.MoveFirst
    Do While Not l_rstChoices.EOF
        Select Case l_rstChoices("itemfield")
            Case "stagefield1"
                If l_rstChoices("stage1conclusion") <> 0 Then
                    m_blnStageField1conc1 = True
                End If
                If l_rstChoices("stage2conclusion") <> 0 Then
                    m_blnStageField1conc2 = True
                End If
                If l_rstChoices("stage3conclusion") <> 0 Then
                    m_blnStageField1conc3 = True
                End If
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField1conc = True
                End If
                grdItems.Columns("stagefield1").Visible = True
                grdItems.Columns("stagefield1").Caption = l_rstChoices("itemheading")
                
            Case "stagefield2"
                If l_rstChoices("stage1conclusion") <> 0 Then
                    m_blnStageField2conc1 = True
                End If
                If l_rstChoices("stage2conclusion") <> 0 Then
                    m_blnStageField2conc2 = True
                End If
                If l_rstChoices("stage3conclusion") <> 0 Then
                    m_blnStageField2conc3 = True
                End If
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField2conc = True
                End If
                grdItems.Columns("stagefield2").Visible = True
                grdItems.Columns("stagefield2").Caption = l_rstChoices("itemheading")
                
            Case "stagefield3"
                If l_rstChoices("stage1conclusion") <> 0 Then
                    m_blnStageField3conc1 = True
                End If
                If l_rstChoices("stage2conclusion") <> 0 Then
                    m_blnStageField3conc2 = True
                End If
                If l_rstChoices("stage3conclusion") <> 0 Then
                    m_blnStageField3conc3 = True
                End If
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField3conc = True
                End If
                grdItems.Columns("stagefield3").Visible = True
                grdItems.Columns("stagefield3").Caption = l_rstChoices("itemheading")
                
            Case "stagefield4"
                If l_rstChoices("stage1conclusion") <> 0 Then
                    m_blnStageField4conc1 = True
                End If
                If l_rstChoices("stage2conclusion") <> 0 Then
                    m_blnStageField4conc2 = True
                End If
                If l_rstChoices("stage3conclusion") <> 0 Then
                    m_blnStageField4conc3 = True
                End If
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField4conc = True
                End If
                grdItems.Columns("stagefield4").Visible = True
                grdItems.Columns("stagefield4").Caption = l_rstChoices("itemheading")
                
            Case "stagefield5"
                If l_rstChoices("stage1conclusion") <> 0 Then
                    m_blnStageField5conc1 = True
                End If
                If l_rstChoices("stage2conclusion") <> 0 Then
                    m_blnStageField5conc2 = True
                End If
                If l_rstChoices("stage3conclusion") <> 0 Then
                    m_blnStageField5conc3 = True
                End If
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField5conc = True
                End If
                grdItems.Columns("stagefield5").Visible = True
                grdItems.Columns("stagefield5").Caption = l_rstChoices("itemheading")
                
            Case "stagefield6"
                If l_rstChoices("stage1conclusion") <> 0 Then
                    m_blnStageField6conc1 = True
                End If
                If l_rstChoices("stage2conclusion") <> 0 Then
                    m_blnStageField6conc2 = True
                End If
                If l_rstChoices("stage3conclusion") <> 0 Then
                    m_blnStageField6conc3 = True
                End If
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField6conc = True
                End If
                grdItems.Columns("stagefield6").Visible = True
                grdItems.Columns("stagefield6").Caption = l_rstChoices("itemheading")
                
            Case "stagefield7"
                If l_rstChoices("stage1conclusion") <> 0 Then
                    m_blnStageField7conc1 = True
                End If
                If l_rstChoices("stage2conclusion") <> 0 Then
                    m_blnStageField7conc2 = True
                End If
                If l_rstChoices("stage3conclusion") <> 0 Then
                    m_blnStageField7conc3 = True
                End If
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField7conc = True
                End If
                grdItems.Columns("stagefield7").Visible = True
                grdItems.Columns("stagefield7").Caption = l_rstChoices("itemheading")
                
            Case "stagefield8"
                If l_rstChoices("stage1conclusion") <> 0 Then
                    m_blnStageField8conc1 = True
                End If
                If l_rstChoices("stage2conclusion") <> 0 Then
                    m_blnStageField8conc2 = True
                End If
                If l_rstChoices("stage3conclusion") <> 0 Then
                    m_blnStageField8conc3 = True
                End If
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField8conc = True
                End If
                grdItems.Columns("stagefield8").Visible = True
                grdItems.Columns("stagefield8").Caption = l_rstChoices("itemheading")
                
            Case "stagefield9"
                If l_rstChoices("stage1conclusion") <> 0 Then
                    m_blnStageField9conc1 = True
                End If
                If l_rstChoices("stage2conclusion") <> 0 Then
                    m_blnStageField9conc2 = True
                End If
                If l_rstChoices("stage3conclusion") <> 0 Then
                    m_blnStageField9conc3 = True
                End If
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField9conc = True
                End If
                grdItems.Columns("stagefield9").Visible = True
                grdItems.Columns("stagefield9").Caption = l_rstChoices("itemheading")
                
            Case "stagefield10"
                If l_rstChoices("stage1conclusion") <> 0 Then
                    m_blnStageField10conc1 = True
                End If
                If l_rstChoices("stage2conclusion") <> 0 Then
                    m_blnStageField10conc2 = True
                End If
                If l_rstChoices("stage3conclusion") <> 0 Then
                    m_blnStageField10conc3 = True
                End If
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField10conc = True
                End If
                grdItems.Columns("stagefield10").Visible = True
                grdItems.Columns("stagefield10").Caption = l_rstChoices("itemheading")
                
            Case "stagefield11"
                If l_rstChoices("stage1conclusion") <> 0 Then
                    m_blnStageField11conc1 = True
                End If
                If l_rstChoices("stage2conclusion") <> 0 Then
                    m_blnStageField11conc2 = True
                End If
                If l_rstChoices("stage3conclusion") <> 0 Then
                    m_blnStageField11conc3 = True
                End If
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField11conc = True
                End If
                grdItems.Columns("stagefield11").Visible = True
                grdItems.Columns("stagefield11").Caption = l_rstChoices("itemheading")
                
            Case "stagefield12"
                If l_rstChoices("stage1conclusion") <> 0 Then
                    m_blnStageField12conc1 = True
                End If
                If l_rstChoices("stage2conclusion") <> 0 Then
                    m_blnStageField12conc2 = True
                End If
                If l_rstChoices("stage3conclusion") <> 0 Then
                    m_blnStageField12conc3 = True
                End If
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField12conc = True
                End If
                grdItems.Columns("stagefield12").Visible = True
                grdItems.Columns("stagefield12").Caption = l_rstChoices("itemheading")
                
            Case "stagefield13"
                If l_rstChoices("stage1conclusion") <> 0 Then
                    m_blnStageField13conc1 = True
                End If
                If l_rstChoices("stage2conclusion") <> 0 Then
                    m_blnStageField13conc2 = True
                End If
                If l_rstChoices("stage3conclusion") <> 0 Then
                    m_blnStageField13conc3 = True
                End If
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField13conc = True
                End If
                grdItems.Columns("stagefield13").Visible = True
                grdItems.Columns("stagefield13").Caption = l_rstChoices("itemheading")
                
            Case "stagefield14"
                If l_rstChoices("stage1conclusion") <> 0 Then
                    m_blnStageField14conc1 = True
                End If
                If l_rstChoices("stage2conclusion") <> 0 Then
                    m_blnStageField14conc2 = True
                End If
                If l_rstChoices("stage3conclusion") <> 0 Then
                    m_blnStageField14conc3 = True
                End If
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField14conc = True
                End If
                grdItems.Columns("stagefield14").Visible = True
                grdItems.Columns("stagefield14").Caption = l_rstChoices("itemheading")
                
            Case "stagefield15"
                If l_rstChoices("stage1conclusion") <> 0 Then
                    m_blnStageField15conc1 = True
                End If
                If l_rstChoices("stage2conclusion") <> 0 Then
                    m_blnStageField15conc2 = True
                End If
                If l_rstChoices("stage3conclusion") <> 0 Then
                    m_blnStageField15conc3 = True
                End If
                If l_rstChoices("conclusion") <> 0 Then
                    m_blnStageField15conc = True
                End If
                grdItems.Columns("stagefield15").Visible = True
                grdItems.Columns("stagefield15").Caption = l_rstChoices("itemheading")
        
            Case "conclusionfield1"
                grdItems.Columns("conclusionfield1").Visible = True
                grdItems.Columns("conclusionfield1").Caption = l_rstChoices("itemheading")
            
            Case "conclusionfield2"
                grdItems.Columns("conclusionfield2").Visible = True
                grdItems.Columns("conclusionfield2").Caption = l_rstChoices("itemheading")
            
            Case "conclusionfield3"
                grdItems.Columns("conclusionfield3").Visible = True
                grdItems.Columns("conclusionfield3").Caption = l_rstChoices("itemheading")
            
            Case "headertext1"
                grdItems.Columns("headertext1").Visible = True
                grdItems.Columns("headertext1").Caption = l_rstChoices("itemheading")
                lblCaption(0).Caption = "Search " & l_rstChoices("itemheading")
                lblCaption(0).Visible = True
                txtheadertext1.Visible = True
            
            Case "headertext2"
                grdItems.Columns("headertext2").Visible = True
                grdItems.Columns("headertext2").Caption = l_rstChoices("itemheading")
                lblCaption(1).Visible = True
                lblCaption(1).Caption = "Search " & l_rstChoices("itemheading")
                txtheadertext2.Visible = True
            
            Case "headertext3"
                grdItems.Columns("headertext3").Visible = True
                grdItems.Columns("headertext3").Caption = l_rstChoices("itemheading")
                lblCaption(2).Visible = True
                lblCaption(2).Caption = "Search " & l_rstChoices("itemheading")
                txtheadertext3.Visible = True
            
            Case "headertext4"
                grdItems.Columns("headertext4").Visible = True
                grdItems.Columns("headertext4").Caption = l_rstChoices("itemheading")
                lblCaption(3).Visible = True
                lblCaption(3).Caption = "Search " & l_rstChoices("itemheading")
                txtheadertext4.Visible = True
            
            Case "headertext5"
                grdItems.Columns("headertext5").Visible = True
                grdItems.Columns("headertext5").Caption = l_rstChoices("itemheading")
                lblCaption(4).Visible = True
                lblCaption(4).Caption = "Search " & l_rstChoices("itemheading")
                txtheadertext5.Visible = True
            
            Case "headerint1"
                grdItems.Columns("headerint1").Visible = True
                grdItems.Columns("headerint1").Caption = l_rstChoices("itemheading")
                lblCaption(5).Visible = True
                lblCaption(5).Caption = "Search " & l_rstChoices("itemheading")
                txtheaderint1.Visible = True
            
            Case "headerint2"
                grdItems.Columns("headerint2").Visible = True
                grdItems.Columns("headerint2").Caption = l_rstChoices("itemheading")
                lblCaption(6).Visible = True
                lblCaption(6).Caption = "Search " & l_rstChoices("itemheading")
                txtheaderint2.Visible = True
            
            Case "headerint3"
                grdItems.Columns("headerint3").Visible = True
                grdItems.Columns("headerint3").Caption = l_rstChoices("itemheading")
                lblCaption(7).Visible = True
                lblCaption(7).Caption = "Search " & l_rstChoices("itemheading")
                txtheaderint3.Visible = True
            
            Case "headerint4"
                grdItems.Columns("headerint4").Visible = True
                grdItems.Columns("headerint4").Caption = l_rstChoices("itemheading")
                lblCaption(8).Visible = True
                lblCaption(8).Caption = "Search " & l_rstChoices("itemheading")
                txtheaderint4.Visible = True
            
            Case "headerint5"
                grdItems.Columns("headerint5").Visible = True
                grdItems.Columns("headerint5").Caption = l_rstChoices("itemheading")
                lblCaption(9).Visible = True
                lblCaption(9).Caption = "Search " & l_rstChoices("itemheading")
                txtheaderint5.Visible = True
            
        End Select
        
        l_rstChoices.MoveNext
    Loop
End If

l_rstChoices.Close
Set l_rstChoices = Nothing

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset
Dim l_strSQL As String

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

l_strSQL = "SELECT itemfield, itemheading FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & ";"

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbSearchField.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

cmdSearch.Value = True

End Sub


Private Sub cmdBillAll_Click()

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then
    
    Do While Not adoItems.Recordset.EOF
        If cmdBillItem.Visible = True Then
            cmdBillItem.Value = True
        End If
        adoItems.Recordset.MoveNext
    Loop
End If

End Sub

Private Sub cmdBillItem_Click()

Dim l_strChargeCode As String, l_rstTrackerChargeCode As ADODB.Recordset, l_strJobLine As String, l_lngJobID As Long, l_lngDuration As Long, l_lngMinuteBilling As Long
Dim l_lngQuantity As Long, l_strCode As String, l_lngFactor As Long

If frmJob.txtJobID.Text <> "" Then
    'A job is loaded - now check that it isn't locked.
    l_lngJobID = Val(frmJob.txtJobID.Text)
    If frmJob.txtStatus.Text = "Confirmed" And frmJob.lblCompanyID.Caption = lblCompanyID.Caption Then
        If grdItems.Columns("duration").Text <> "" Then
            'The Job is valid - go ahead and create Job lines for this item.
            l_lngQuantity = 0
            l_strJobLine = QuoteSanitise(grdItems.Columns("headertext1").Text)
            If grdItems.Columns("headerint1").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("headerint1").Text
            If grdItems.Columns("headerint2").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("headerint2").Text
            If grdItems.Columns("headertext2").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("headertext2").Text
            If grdItems.Columns("itemreference").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("itemreference").Text
            If grdItems.Columns("stagefield1").Text <> "" Then
                l_strCode = "M"
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield1';", g_strExecuteError)
                CheckForSQLError
                l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
                    Case "N"
                        l_lngQuantity = Val(grdItems.Columns("stagefield1").Text)
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "T"
                        l_lngQuantity = 1
                        l_lngDuration = Val(grdItems.Columns("stagefield1").Text)
                    Case "M"
                        l_lngMinuteBilling = 1
                    Case "S"
                        l_lngFactor = 0
                        If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
                        If l_lngFactor <> 0 Then
                            If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
                                l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
                            Else
                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                            End If
                        Else
                            l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                        End If
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "1"
                        l_lngQuantity = 1
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "I"
                        l_strCode = "I"
                    Case Else
                        l_lngMinuteBilling = 0
                End Select
                If l_lngQuantity = 0 Then l_lngQuantity = 1
                l_rstTrackerChargeCode.Close
                If l_strChargeCode <> "" Then
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
                    l_lngQuantity = 0
                End If
            End If
            frmJob.adoJobDetail.Refresh
            If grdItems.Columns("stagefield2").Text <> "" Then
                l_strCode = "A"
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield2';", g_strExecuteError)
                CheckForSQLError
                l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
                    Case "N"
                        l_lngQuantity = Val(grdItems.Columns("stagefield2").Text)
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "T"
                        l_lngQuantity = 1
                        l_lngDuration = Val(grdItems.Columns("stagefield2").Text)
                    Case "M"
                        l_lngMinuteBilling = 1
                    Case "S"
                        l_lngFactor = 1
                        If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
                        If l_lngFactor <> 0 Then
                            If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
                                l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
                            Else
                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                            End If
                        Else
                            l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                        End If
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "1"
                        l_lngQuantity = 1
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "I"
                        l_strCode = "I"
                    Case Else
                        l_lngMinuteBilling = 0
                End Select
                If l_lngQuantity = 0 Then l_lngQuantity = 1
                l_rstTrackerChargeCode.Close
                If l_strChargeCode <> "" Then
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
                    l_lngQuantity = 0
                End If
            End If
            frmJob.adoJobDetail.Refresh
            If grdItems.Columns("stagefield3").Text <> "" Then
                l_strCode = "A"
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield3';", g_strExecuteError)
                CheckForSQLError
                l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
                    Case "N"
                        l_lngQuantity = Val(grdItems.Columns("stagefield3").Text)
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "T"
                        l_lngQuantity = 1
                        l_lngDuration = Val(grdItems.Columns("stagefield3").Text)
                    Case "M"
                        l_lngMinuteBilling = 1
                    Case "S"
                        l_lngFactor = 1
                        If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
                        If l_lngFactor <> 0 Then
                            If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
                                l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
                            Else
                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                            End If
                        Else
                            l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                        End If
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "1"
                        l_lngQuantity = 1
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "I"
                        l_strCode = "I"
                    Case Else
                        l_lngMinuteBilling = 0
                End Select
                If l_lngQuantity = 0 Then l_lngQuantity = 1
                l_rstTrackerChargeCode.Close
                If l_strChargeCode <> "" Then
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
                    l_lngQuantity = 0
                End If
            End If
            frmJob.adoJobDetail.Refresh
            If grdItems.Columns("stagefield4").Text <> "" Then
                l_strCode = "A"
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield4';", g_strExecuteError)
                CheckForSQLError
                l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
                    Case "N"
                        l_lngQuantity = Val(grdItems.Columns("stagefield4").Text)
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "T"
                        l_lngQuantity = 1
                        l_lngDuration = Val(grdItems.Columns("stagefield4").Text)
                    Case "M"
                        l_lngMinuteBilling = 1
                    Case "S"
                        l_lngFactor = 1
                        If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
                        If l_lngFactor <> 0 Then
                            If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
                                l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
                            Else
                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                            End If
                        Else
                            l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                        End If
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "1"
                        l_lngQuantity = 1
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "I"
                        l_strCode = "I"
                    Case Else
                        l_lngMinuteBilling = 0
                End Select
                If l_lngQuantity = 0 Then l_lngQuantity = 1
                l_rstTrackerChargeCode.Close
                If l_strChargeCode <> "" Then
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
                    l_lngQuantity = 0
                End If
            End If
            frmJob.adoJobDetail.Refresh
            If grdItems.Columns("stagefield5").Text <> "" Then
                l_strCode = "A"
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield5';", g_strExecuteError)
                CheckForSQLError
                l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
                    Case "N"
                        l_lngQuantity = Val(grdItems.Columns("stagefield5").Text)
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "T"
                        l_lngQuantity = 1
                        l_lngDuration = Val(grdItems.Columns("stagefield5").Text)
                    Case "M"
                        l_lngMinuteBilling = 1
                    Case "S"
                        l_lngFactor = 1
                        If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
                        If l_lngFactor <> 0 Then
                            If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
                                l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
                            Else
                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                            End If
                        Else
                            l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                        End If
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "1"
                        l_lngQuantity = 1
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "I"
                        l_strCode = "I"
                    Case Else
                        l_lngMinuteBilling = 0
                End Select
                If l_lngQuantity = 0 Then l_lngQuantity = 1
                l_rstTrackerChargeCode.Close
                If l_strChargeCode <> "" Then
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
                    l_lngQuantity = 0
                End If
            End If
            frmJob.adoJobDetail.Refresh
            If grdItems.Columns("stagefield6").Text <> "" Then
                l_strCode = "A"
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield6';", g_strExecuteError)
                CheckForSQLError
                l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
                    Case "N"
                        l_lngQuantity = Val(grdItems.Columns("stagefield6").Text)
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "T"
                        l_lngQuantity = 1
                        l_lngDuration = Val(grdItems.Columns("stagefield6").Text)
                    Case "M"
                        l_lngMinuteBilling = 1
                    Case "S"
                        l_lngFactor = 1
                        If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
                        If l_lngFactor <> 0 Then
                            If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
                                l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
                            Else
                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                            End If
                        Else
                            l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                        End If
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "1"
                        l_lngQuantity = 1
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "I"
                        l_strCode = "I"
                    Case Else
                        l_lngMinuteBilling = 0
                End Select
                If l_lngQuantity = 0 Then l_lngQuantity = 1
                l_rstTrackerChargeCode.Close
                If l_strChargeCode <> "" Then
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
                    l_lngQuantity = 0
                End If
            End If
            If grdItems.Columns("stagefield7").Text <> "" Then
            frmJob.adoJobDetail.Refresh
                l_strCode = "A"
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield7';", g_strExecuteError)
                CheckForSQLError
                l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
                    Case "N"
                        l_lngQuantity = Val(grdItems.Columns("stagefield7").Text)
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "T"
                        l_lngQuantity = 1
                        l_lngDuration = Val(grdItems.Columns("stagefield7").Text)
                    Case "M"
                        l_lngMinuteBilling = 1
                    Case "S"
                        l_lngFactor = 1
                        If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
                        If l_lngFactor <> 0 Then
                            If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
                                l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
                            Else
                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                            End If
                        Else
                            l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                        End If
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "1"
                        l_lngQuantity = 1
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "I"
                        l_strCode = "I"
                    Case Else
                        l_lngMinuteBilling = 0
                End Select
                If l_lngQuantity = 0 Then l_lngQuantity = 1
                l_rstTrackerChargeCode.Close
                If l_strChargeCode <> "" Then
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
                    l_lngQuantity = 0
                End If
            End If
            frmJob.adoJobDetail.Refresh
            If grdItems.Columns("stagefield8").Text <> "" Then
                l_strCode = "A"
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield8';", g_strExecuteError)
                CheckForSQLError
                l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
                    Case "N"
                        l_lngQuantity = Val(grdItems.Columns("stagefield8").Text)
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "T"
                        l_lngQuantity = 1
                        l_lngDuration = Val(grdItems.Columns("stagefield8").Text)
                    Case "M"
                        l_lngMinuteBilling = 1
                    Case "S"
                        l_lngFactor = 1
                        If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
                        If l_lngFactor <> 0 Then
                            If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
                                l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
                            Else
                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                            End If
                        Else
                            l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                        End If
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "1"
                        l_lngQuantity = 1
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "I"
                        l_strCode = "I"
                    Case Else
                        l_lngMinuteBilling = 0
                End Select
                If l_lngQuantity = 0 Then l_lngQuantity = 1
                l_rstTrackerChargeCode.Close
                If l_strChargeCode <> "" Then
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
                    l_lngQuantity = 0
                End If
            End If
            frmJob.adoJobDetail.Refresh
            If grdItems.Columns("stagefield9").Text <> "" Then
                l_strCode = "A"
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield9';", g_strExecuteError)
                CheckForSQLError
                l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
                    Case "N"
                        l_lngQuantity = Val(grdItems.Columns("stagefield9").Text)
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "T"
                        l_lngQuantity = 1
                        l_lngDuration = Val(grdItems.Columns("stagefield9").Text)
                    Case "M"
                        l_lngMinuteBilling = 1
                    Case "S"
                        l_lngFactor = 1
                        If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
                        If l_lngFactor <> 0 Then
                            If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
                                l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
                            Else
                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                            End If
                        Else
                            l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                        End If
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "1"
                        l_lngQuantity = 1
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "I"
                        l_strCode = "I"
                    Case Else
                        l_lngMinuteBilling = 0
                End Select
                If l_lngQuantity = 0 Then l_lngQuantity = 1
                l_rstTrackerChargeCode.Close
                If l_strChargeCode <> "" Then
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
                    l_lngQuantity = 0
                End If
            End If
            frmJob.adoJobDetail.Refresh
            If grdItems.Columns("stagefield10").Text <> "" Then
                l_strCode = "A"
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield10';", g_strExecuteError)
                CheckForSQLError
                l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
                    Case "N"
                        l_lngQuantity = Val(grdItems.Columns("stagefield10").Text)
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "T"
                        l_lngQuantity = 1
                        l_lngDuration = Val(grdItems.Columns("stagefield10").Text)
                    Case "M"
                        l_lngMinuteBilling = 1
                    Case "S"
                        l_lngFactor = 1
                        If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
                        If l_lngFactor <> 0 Then
                            If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
                                l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
                            Else
                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                            End If
                        Else
                            l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                        End If
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "1"
                        l_lngQuantity = 1
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "I"
                        l_strCode = "I"
                    Case Else
                        l_lngMinuteBilling = 0
                End Select
                If l_lngQuantity = 0 Then l_lngQuantity = 1
                l_rstTrackerChargeCode.Close
                If l_strChargeCode <> "" Then
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
                    l_lngQuantity = 0
                End If
            End If
            frmJob.adoJobDetail.Refresh
            If grdItems.Columns("stagefield11").Text <> "" Then
                l_strCode = "A"
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield11';", g_strExecuteError)
                CheckForSQLError
                l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
                    Case "N"
                        l_lngQuantity = Val(grdItems.Columns("stagefield11").Text)
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "T"
                        l_lngQuantity = 1
                        l_lngDuration = Val(grdItems.Columns("stagefield11").Text)
                    Case "M"
                        l_lngMinuteBilling = 1
                    Case "S"
                        l_lngFactor = 1
                        If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
                        If l_lngFactor <> 0 Then
                            If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
                                l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
                            Else
                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                            End If
                        Else
                            l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                        End If
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "1"
                        l_lngQuantity = 1
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "I"
                        l_strCode = "I"
                    Case Else
                        l_lngMinuteBilling = 0
                End Select
                If l_lngQuantity = 0 Then l_lngQuantity = 1
                l_rstTrackerChargeCode.Close
                If l_strChargeCode <> "" Then
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
                    l_lngQuantity = 0
                End If
            End If
            frmJob.adoJobDetail.Refresh
            If grdItems.Columns("stagefield12").Text <> "" Then
                l_strCode = "A"
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield12';", g_strExecuteError)
                CheckForSQLError
                l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
                    Case "N"
                        l_lngQuantity = Val(grdItems.Columns("stagefield12").Text)
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "T"
                        l_lngQuantity = 1
                        l_lngDuration = Val(grdItems.Columns("stagefield12").Text)
                    Case "M"
                        l_lngMinuteBilling = 1
                    Case "S"
                        l_lngFactor = 1
                        If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
                        If l_lngFactor <> 0 Then
                            If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
                                l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
                            Else
                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                            End If
                        Else
                            l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                        End If
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "1"
                        l_lngQuantity = 1
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "I"
                        l_strCode = "I"
                    Case Else
                        l_lngMinuteBilling = 0
                End Select
                If l_lngQuantity = 0 Then l_lngQuantity = 1
                l_rstTrackerChargeCode.Close
                If l_strChargeCode <> "" Then
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
                    l_lngQuantity = 0
                End If
            End If
            frmJob.adoJobDetail.Refresh
            If grdItems.Columns("stagefield13").Text <> "" Then
                l_strCode = "A"
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield13';", g_strExecuteError)
                CheckForSQLError
                l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
                    Case "N"
                        l_lngQuantity = Val(grdItems.Columns("stagefield13").Text)
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "T"
                        l_lngQuantity = 1
                        l_lngDuration = Val(grdItems.Columns("stagefield13").Text)
                    Case "M"
                        l_lngMinuteBilling = 1
                    Case "S"
                        l_lngFactor = 1
                        If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
                        If l_lngFactor <> 0 Then
                            If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
                                l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
                            Else
                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                            End If
                        Else
                            l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                        End If
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "1"
                        l_lngQuantity = 1
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "I"
                        l_strCode = "I"
                    Case Else
                        l_lngMinuteBilling = 0
                End Select
                If l_lngQuantity = 0 Then l_lngQuantity = 1
                l_rstTrackerChargeCode.Close
                If l_strChargeCode <> "" Then
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
                    l_lngQuantity = 0
                End If
            End If
            frmJob.adoJobDetail.Refresh
            If grdItems.Columns("stagefield14").Text <> "" Then
                l_strCode = "A"
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield14';", g_strExecuteError)
                CheckForSQLError
                l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
                    Case "N"
                        l_lngQuantity = Val(grdItems.Columns("stagefield14").Text)
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "T"
                        l_lngQuantity = 1
                        l_lngDuration = Val(grdItems.Columns("stagefield14").Text)
                    Case "M"
                        l_lngMinuteBilling = 1
                    Case "S"
                        l_lngFactor = 1
                        If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
                        If l_lngFactor <> 0 Then
                            If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
                                l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
                            Else
                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                            End If
                        Else
                            l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                        End If
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "1"
                        l_lngQuantity = 1
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "I"
                        l_strCode = "I"
                    Case Else
                        l_lngMinuteBilling = 0
                End Select
                If l_lngQuantity = 0 Then l_lngQuantity = 1
                l_rstTrackerChargeCode.Close
                If l_strChargeCode <> "" Then
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
                    l_lngQuantity = 0
                End If
            End If
            frmJob.adoJobDetail.Refresh
            If grdItems.Columns("stagefield15").Text <> "" Then
                l_strCode = "A"
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield15';", g_strExecuteError)
                CheckForSQLError
                l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
                    Case "N"
                        l_lngQuantity = Val(grdItems.Columns("stagefield15").Text)
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "T"
                        l_lngQuantity = 1
                        l_lngDuration = Val(grdItems.Columns("stagefield15").Text)
                    Case "M"
                        l_lngMinuteBilling = 1
                    Case "S"
                        l_lngFactor = 1
                        If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
                        If l_lngFactor <> 0 Then
                            If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
                                l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
                            ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
                                l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
                            Else
                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                            End If
                        Else
                            l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                        End If
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "1"
                        l_lngQuantity = 1
                        l_lngDuration = 0
                        l_strCode = "O"
                    Case "I"
                        l_strCode = "I"
                    Case Else
                        l_lngMinuteBilling = 0
                End Select
                If l_lngQuantity = 0 Then l_lngQuantity = 1
                l_rstTrackerChargeCode.Close
                If l_strChargeCode <> "" Then
                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
                    l_lngQuantity = 0
                End If
            End If
            frmJob.adoJobDetail.Refresh
            grdItems.Columns("billed").Text = -1
            grdItems.Columns("jobID").Text = l_lngJobID
            grdItems.Update
            cmdBillItem.Visible = False
            cmdManualBillItem.Visible = False
        Else
            MsgBox "The current Item must have a duration in order to bill it.", vbCritical, "Cannot Bill Item"
        End If
    Else
        MsgBox "The current Job must belong to the correct company and be confirmed in order to bill an item.", vbCritical, "Cannot Bill Item"
    End If
Else
    MsgBox "A Job must be created and loaded into the Jobs form in order to bill an item.", vbCritical, "Cannot Bill Item"
End If

End Sub

Private Sub cmdClear_Click()

ClearFields Me

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdCopyBack_Click()

Dim i As Long, bkmrk As Variant, l_lngTotalSelRows As Long

If lblTrackeritemID.Caption = "" Then Exit Sub
If lblCompanyID.Caption = "" Then Exit Sub

If grdItems.SelBookmarks.Count <= 0 Then
    CopyTrackerItem Val(lblTrackeritemID.Caption)
Else
    l_lngTotalSelRows = grdItems.SelBookmarks.Count - 1
    
    For i = 0 To l_lngTotalSelRows
        bkmrk = grdItems.SelBookmarks(i)
        CopyTrackerItem Val(grdItems.Columns("tracker_itemID").CellText(bkmrk))
    Next i
End If

End Sub

Private Sub cmdManualBillItem_Click()

grdItems.Columns("billed").Text = -1
grdItems.Columns("jobid").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False

End Sub

Private Sub cmdPrint_Click()

Dim l_strSQL As String

If lblCompanyID.Caption <> "" Then
    l_strSQL = adoItems.RecordSource
    PrintCrystalReportUsingCleanSQL g_strLocationOfCrystalReportFiles & "GenericTrackerReport.rpt", l_strSQL, True
End If

End Sub

Private Sub cmdSearch_Click()

Dim l_rstTotal As ADODB.Recordset, l_lngVideoTotal As Long

If lblCompanyID.Caption <> "" Then

    Dim l_strSQL As String
    If optComplete(0).Value = True Then
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0)"
    ElseIf optComplete(1).Value = True Then
        l_strSQL = "AND (billed IS NULL OR billed = 0) AND readytobill <> 0"
    ElseIf optComplete(2).Value = True Then
        l_strSQL = "AND billed IS NOT NULL AND billed <> 0"
    End If
    
    If txtReference.Text <> "" Then
        l_strSQL = l_strSQL & " AND itemreference LIKE '" & QuoteSanitise(txtReference.Text) & "%' "
    End If
    
    If txtheadertext1.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext1 LIKE '" & QuoteSanitise(txtheadertext1.Text) & "%' "
    End If
    If txtheadertext2.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext2 LIKE '" & QuoteSanitise(txtheadertext2.Text) & "%' "
    End If
    If txtheadertext3.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext3 LIKE '" & QuoteSanitise(txtheadertext3.Text) & "%' "
    End If
    If txtheadertext4.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext4 LIKE '" & QuoteSanitise(txtheadertext4.Text) & "%' "
    End If
    If txtheadertext5.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext5 LIKE '" & QuoteSanitise(txtheadertext5.Text) & "%' "
    End If
    If Val(txtheaderint1.Text) <> 0 Then
        l_strSQL = l_strSQL & " AND headerint1 = " & Val(txtheaderint1.Text) & " "
    End If
    If Val(txtheaderint2.Text) <> 0 Then
        l_strSQL = l_strSQL & " AND headerint2 = " & Val(txtheaderint2.Text) & " "
    End If
    If Val(txtheaderint3.Text) <> 0 Then
        l_strSQL = l_strSQL & " AND headerint3 = " & Val(txtheaderint3.Text) & " "
    End If
    If Val(txtheaderint4.Text) <> 0 Then
        l_strSQL = l_strSQL & " AND headerint4 = " & Val(txtheaderint4.Text) & " "
    End If
    If Val(txtheaderint5.Text) <> 0 Then
        l_strSQL = l_strSQL & " AND headerint5 = " & Val(txtheaderint5.Text) & " "
    End If
    
'    If cmbSearchField.Text <> "" And (Not IsNull(datFrom.Value) Or Not IsNull(datTo.Value)) Then
'        If Not IsNull(datFrom.Value) Then
'            l_strSQL = l_strSQL & " AND " & cmbSearchField.Columns("itemfield")
        
    
    adoItems.ConnectionString = g_strConnection
    adoItems.RecordSource = "SELECT * FROM tracker_item " & m_strSearch & l_strSQL & m_strOrderby & ";"
    adoItems.Refresh
    
    adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"

    Set l_rstTotal = ExecuteSQL("SELECT Sum(duration) FROM tracker_item " & m_strSearch & l_strSQL & ";", g_strExecuteError)
    CheckForSQLError
    
    If IsNull(l_rstTotal(0)) Then
        l_lngVideoTotal = 0
    Else
        l_lngVideoTotal = l_rstTotal(0)
    End If
    
    l_rstTotal.Close

    txtTotalDuration.Text = l_lngVideoTotal

    Set l_rstTotal = ExecuteSQL("SELECT Sum(gbsent) FROM tracker_item " & m_strSearch & l_strSQL & ";", g_strExecuteError)
    CheckForSQLError
    
    If IsNull(l_rstTotal(0)) Then
        l_lngVideoTotal = 0
    Else
        l_lngVideoTotal = l_rstTotal(0)
    End If
    
    l_rstTotal.Close

    txtTotalSize.Text = l_lngVideoTotal

    Set l_rstTotal = Nothing
End If

End Sub

Private Sub cmdUnbill_Click()

grdItems.Columns("billed").Text = 0
grdItems.Columns("jobid").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False

End Sub

Private Sub Command2_Click()

End Sub

Private Sub cmdUnbillAll_Click()

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then

    Do While Not adoItems.Recordset.EOF
        adoItems.Recordset("billed") = 0
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
    Loop
End If

grdItems.Refresh

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

grdItems.StyleSets("headerfield").BackColor = &HE7FFE7
grdItems.StyleSets("stagefield").BackColor = &HE7FFFF
grdItems.StyleSets("conclusionfield").BackColor = &HFFFFE7

grdItems.StyleSets.Add "Error"
grdItems.StyleSets("Error").BackColor = &HA0A0FF

optComplete(0).Value = True

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%tracker%' and companyID > 100 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%tracker%' ORDER BY name;"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub Form_Resize()

On Error Resume Next

grdItems.Width = Me.ScaleWidth - grdItems.Left - 120
grdItems.Height = (Me.ScaleHeight - grdItems.Top - frmButtons.Height) * 0.75 - 240
frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120
grdComments.Top = grdItems.Top + grdItems.Height + 120
grdComments.Height = frmButtons.Top - grdComments.Top - 120
grdComments.Width = grdItems.Width

End Sub

Private Sub grdComments_BeforeUpdate(Cancel As Integer)

grdComments.Columns("tracker_itemID").Text = lblTrackeritemID.Caption
If grdComments.Columns("cdate").Text = "" Then
    grdComments.Columns("cdate").Text = Now
End If
grdComments.Columns("cuser").Text = g_strFullUserName

End Sub

Private Sub grdItems_BeforeUpdate(Cancel As Integer)

Dim temp As Boolean, l_strDuration As String, l_lngRunningTime As Long, l_curFileSize As Currency, l_strFilename As String, l_rst As ADODB.Recordset

temp = True

If grdItems.Columns("conclusionfield1").Visible = True Then
    'Check Conclusion 1
    If m_blnStageField1conc1 = True Then
        If grdItems.Columns("stagefield1").Text = "" Or UCase(Right(grdItems.Columns("stagefield1").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField2conc1 = True Then
        If grdItems.Columns("stagefield2").Text = "" Or UCase(Right(grdItems.Columns("stagefield2").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField3conc1 = True Then
        If grdItems.Columns("stagefield3").Text = "" Or UCase(Right(grdItems.Columns("stagefield3").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField4conc1 = True Then
        If grdItems.Columns("stagefield4").Text = "" Or UCase(Right(grdItems.Columns("stagefield4").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField5conc1 = True Then
        If grdItems.Columns("stagefield5").Text = "" Or UCase(Right(grdItems.Columns("stagefield5").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField6conc1 = True Then
        If grdItems.Columns("stagefield6").Text = "" Or UCase(Right(grdItems.Columns("stagefield6").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField7conc1 = True Then
        If grdItems.Columns("stagefield7").Text = "" Or UCase(Right(grdItems.Columns("stagefield7").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField8conc1 = True Then
        If grdItems.Columns("stagefield8").Text = "" Or UCase(Right(grdItems.Columns("stagefield8").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField9conc1 = True Then
        If grdItems.Columns("stagefield9").Text = "" Or UCase(Right(grdItems.Columns("stagefield9").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField10conc1 = True Then
        If grdItems.Columns("stagefield10").Text = "" Or UCase(Right(grdItems.Columns("stagefield10").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField11conc1 = True Then
        If grdItems.Columns("stagefield11").Text = "" Or UCase(Right(grdItems.Columns("stagefield11").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField12conc1 = True Then
        If grdItems.Columns("stagefield12").Text = "" Or UCase(Right(grdItems.Columns("stagefield12").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField13conc1 = True Then
        If grdItems.Columns("stagefield13").Text = "" Or UCase(Right(grdItems.Columns("stagefield13").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField14conc1 = True Then
        If grdItems.Columns("stagefield14").Text = "" Or UCase(Right(grdItems.Columns("stagefield14").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField15conc1 = True Then
        If grdItems.Columns("stagefield15").Text = "" Or UCase(Right(grdItems.Columns("stagefield15").Text, 2)) = "ER" Then temp = False
    End If
    grdItems.Columns("conclusionfield1").Text = temp
End If

temp = True

If grdItems.Columns("conclusionfield2").Visible = True Then
    'Check Conclusion 2
    If m_blnStageField1conc2 = True Then
        If grdItems.Columns("stagefield1").Text = "" Or UCase(Right(grdItems.Columns("stagefield1").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField2conc2 = True Then
        If grdItems.Columns("stagefield2").Text = "" Or UCase(Right(grdItems.Columns("stagefield2").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField3conc2 = True Then
        If grdItems.Columns("stagefield3").Text = "" Or UCase(Right(grdItems.Columns("stagefield3").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField4conc2 = True Then
        If grdItems.Columns("stagefield4").Text = "" Or UCase(Right(grdItems.Columns("stagefield4").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField5conc2 = True Then
        If grdItems.Columns("stagefield5").Text = "" Or UCase(Right(grdItems.Columns("stagefield5").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField6conc2 = True Then
        If grdItems.Columns("stagefield6").Text = "" Or UCase(Right(grdItems.Columns("stagefield6").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField7conc2 = True Then
        If grdItems.Columns("stagefield7").Text = "" Or UCase(Right(grdItems.Columns("stagefield7").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField8conc2 = True Then
        If grdItems.Columns("stagefield8").Text = "" Or UCase(Right(grdItems.Columns("stagefield8").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField9conc2 = True Then
        If grdItems.Columns("stagefield9").Text = "" Or UCase(Right(grdItems.Columns("stagefield9").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField10conc2 = True Then
        If grdItems.Columns("stagefield10").Text = "" Or UCase(Right(grdItems.Columns("stagefield10").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField11conc2 = True Then
        If grdItems.Columns("stagefield11").Text = "" Or UCase(Right(grdItems.Columns("stagefield11").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField12conc2 = True Then
        If grdItems.Columns("stagefield12").Text = "" Or UCase(Right(grdItems.Columns("stagefield12").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField13conc2 = True Then
        If grdItems.Columns("stagefield13").Text = "" Or UCase(Right(grdItems.Columns("stagefield13").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField14conc2 = True Then
        If grdItems.Columns("stagefield14").Text = "" Or UCase(Right(grdItems.Columns("stagefield14").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField15conc2 = True Then
        If grdItems.Columns("stagefield15").Text = "" Or UCase(Right(grdItems.Columns("stagefield15").Text, 2)) = "ER" Then temp = False
    End If
    grdItems.Columns("conclusionfield2").Text = temp
End If

temp = True

If grdItems.Columns("conclusionfield3").Visible = True Then
    'Check Conclusion 3
    If m_blnStageField1conc3 = True Then
        If grdItems.Columns("stagefield1").Text = "" Or UCase(Right(grdItems.Columns("stagefield1").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField2conc3 = True Then
        If grdItems.Columns("stagefield2").Text = "" Or UCase(Right(grdItems.Columns("stagefield2").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField3conc3 = True Then
        If grdItems.Columns("stagefield3").Text = "" Or UCase(Right(grdItems.Columns("stagefield3").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField4conc3 = True Then
        If grdItems.Columns("stagefield4").Text = "" Or UCase(Right(grdItems.Columns("stagefield4").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField5conc3 = True Then
        If grdItems.Columns("stagefield5").Text = "" Or UCase(Right(grdItems.Columns("stagefield5").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField6conc3 = True Then
        If grdItems.Columns("stagefield6").Text = "" Or UCase(Right(grdItems.Columns("stagefield6").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField7conc3 = True Then
        If grdItems.Columns("stagefield7").Text = "" Or UCase(Right(grdItems.Columns("stagefield7").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField8conc3 = True Then
        If grdItems.Columns("stagefield8").Text = "" Or UCase(Right(grdItems.Columns("stagefield8").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField9conc3 = True Then
        If grdItems.Columns("stagefield9").Text = "" Or UCase(Right(grdItems.Columns("stagefield9").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField10conc3 = True Then
        If grdItems.Columns("stagefield10").Text = "" Or UCase(Right(grdItems.Columns("stagefield10").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField11conc3 = True Then
        If grdItems.Columns("stagefield11").Text = "" Or UCase(Right(grdItems.Columns("stagefield11").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField12conc3 = True Then
        If grdItems.Columns("stagefield12").Text = "" Or UCase(Right(grdItems.Columns("stagefield12").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField13conc3 = True Then
        If grdItems.Columns("stagefield13").Text = "" Or UCase(Right(grdItems.Columns("stagefield13").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField14conc3 = True Then
        If grdItems.Columns("stagefield14").Text = "" Or UCase(Right(grdItems.Columns("stagefield14").Text, 2)) = "ER" Then temp = False
    End If
    If m_blnStageField15conc3 = True Then
        If grdItems.Columns("stagefield15").Text = "" Or UCase(Right(grdItems.Columns("stagefield15").Text, 2)) = "ER" Then temp = False
    End If
    grdItems.Columns("conclusionfield3").Text = temp
End If

temp = True

'Check ReadytoBill
If m_blnStageField1conc = True Then
    If grdItems.Columns("stagefield1").Text = "" Or UCase(Right(grdItems.Columns("stagefield1").Text, 2)) = "ER" Then temp = False
End If
If m_blnStageField2conc = True Then
    If grdItems.Columns("stagefield2").Text = "" Or UCase(Right(grdItems.Columns("stagefield2").Text, 2)) = "ER" Then temp = False
End If
If m_blnStageField3conc = True Then
    If grdItems.Columns("stagefield3").Text = "" Or UCase(Right(grdItems.Columns("stagefield3").Text, 2)) = "ER" Then temp = False
End If
If m_blnStageField4conc = True Then
    If grdItems.Columns("stagefield4").Text = "" Or UCase(Right(grdItems.Columns("stagefield4").Text, 2)) = "ER" Then temp = False
End If
If m_blnStageField5conc = True Then
    If grdItems.Columns("stagefield5").Text = "" Or UCase(Right(grdItems.Columns("stagefield5").Text, 2)) = "ER" Then temp = False
End If
If m_blnStageField6conc = True Then
    If grdItems.Columns("stagefield6").Text = "" Or UCase(Right(grdItems.Columns("stagefield6").Text, 2)) = "ER" Then temp = False
End If
If m_blnStageField7conc = True Then
    If grdItems.Columns("stagefield7").Text = "" Or UCase(Right(grdItems.Columns("stagefield7").Text, 2)) = "ER" Then temp = False
End If
If m_blnStageField8conc = True Then
    If grdItems.Columns("stagefield8").Text = "" Or UCase(Right(grdItems.Columns("stagefield8").Text, 2)) = "ER" Then temp = False
End If
If m_blnStageField9conc = True Then
    If grdItems.Columns("stagefield9").Text = "" Or UCase(Right(grdItems.Columns("stagefield9").Text, 2)) = "ER" Then temp = False
End If
If m_blnStageField10conc = True Then
    If grdItems.Columns("stagefield10").Text = "" Or UCase(Right(grdItems.Columns("stagefield10").Text, 2)) = "ER" Then temp = False
End If
If m_blnStageField11conc = True Then
    If grdItems.Columns("stagefield11").Text = "" Or UCase(Right(grdItems.Columns("stagefield11").Text, 2)) = "ER" Then temp = False
End If
If m_blnStageField12conc = True Then
    If grdItems.Columns("stagefield12").Text = "" Or UCase(Right(grdItems.Columns("stagefield12").Text, 2)) = "ER" Then temp = False
End If
If m_blnStageField13conc = True Then
    If grdItems.Columns("stagefield13").Text = "" Or UCase(Right(grdItems.Columns("stagefield13").Text, 2)) = "ER" Then temp = False
End If
If m_blnStageField14conc = True Then
    If grdItems.Columns("stagefield14").Text = "" Or UCase(Right(grdItems.Columns("stagefield14").Text, 2)) = "ER" Then temp = False
End If
If m_blnStageField15conc = True Then
    If grdItems.Columns("stagefield15").Text = "" Or UCase(Right(grdItems.Columns("stagefield15").Text, 2)) = "ER" Then temp = False
End If

If temp = True Then
    grdItems.Columns("readytobill").Text = 1
Else
    grdItems.Columns("readytobill").Text = 0
End If

grdItems.Columns("companyID").Text = lblCompanyID.Caption

If grdItems.Columns("itemreference").Text <> "" Then
    'see if there is a clip record to get the duration from reference
    l_strDuration = GetData("events", "fd_length", "clipreference", grdItems.Columns("itemreference").Text)
    If l_strDuration <> "" Then
        l_lngRunningTime = 60 * Val(Mid(l_strDuration, 1, 2))
        l_lngRunningTime = l_lngRunningTime + Val(Mid(l_strDuration, 4, 2))
        If Val(Mid(l_strDuration, 7, 2)) > 30 Then
            l_lngRunningTime = l_lngRunningTime + 1
        End If
        If l_lngRunningTime = 0 Then l_lngRunningTime = 1
        If (grdItems.Columns("duration").Text = "") Or (grdItems.Columns("duration").Text <> "" And chkLockDur.Value = 0) Then
            grdItems.Columns("duration").Text = l_lngRunningTime
        End If
    End If
    
    'See if there is a size to go in the GB sent column
    l_curFileSize = 0
    Set l_rst = ExecuteSQL("SELECT bigfilesize FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "';", g_strExecuteError)
    CheckForSQLError
    If l_rst.RecordCount > 0 Then
        l_rst.MoveFirst
        Do While Not l_rst.EOF
            If Not IsNull(l_rst("bigfilesize")) Then
                If l_rst("bigfilesize") > l_curFileSize Then l_curFileSize = l_rst("bigfilesize")
            End If
            l_rst.MoveNext
        Loop
    End If
    l_rst.Close
    Set l_rst = Nothing
    If l_curFileSize <> 0 Then
        If (grdItems.Columns("gbsent").Text = "") Or (grdItems.Columns("gbsent").Text <> "" And chkLockGB.Value = 0) Then
            grdItems.Columns("gbsent").Text = Int(l_curFileSize / 1024 / 1024 / 1024 + 0.999)
        End If
    End If
    
    'See if there is a filename to go in the filename column
    l_strFilename = GetData("events", "clipfilename", "clipreference", grdItems.Columns("itemreference").Text)
    If l_strFilename <> "" Then
        grdItems.Columns("itemfilename").Text = l_strFilename
    End If

End If

End Sub

Private Sub grdItems_BtnClick()

Dim tempdate As String

If grdItems.ActiveCell.Text <> "" Then
    grdItems.ActiveCell.Text = ""
Else
    tempdate = FormatDateTime(Now, vbLongDate)
    grdItems.ActiveCell.Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
End If

End Sub

Private Sub grdItems_DblClick()

ShowClipSearch "", grdItems.Columns("itemreference").Text

End Sub

Private Sub grdItems_HeadClick(ByVal ColIndex As Integer)

m_strOrderby = " ORDER BY " & grdItems.Columns(ColIndex).Name
adoItems.RecordSource = "SELECT * FROM tracker_item " & m_strSearch & m_strOrderby
adoItems.Refresh

End Sub

Private Sub grdItems_RowLoaded(ByVal Bookmark As Variant)

If UCase(Right(grdItems.Columns("stagefield1").Text, 2)) = "ER" Then
    grdItems.Columns("stagefield1").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("stagefield2").Text, 2)) = "ER" Then
    grdItems.Columns("stagefield2").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("stagefield3").Text, 2)) = "ER" Then
    grdItems.Columns("stagefield3").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("stagefield4").Text, 2)) = "ER" Then
    grdItems.Columns("stagefield4").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("stagefield5").Text, 2)) = "ER" Then
    grdItems.Columns("stagefield5").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("stagefield6").Text, 2)) = "ER" Then
    grdItems.Columns("stagefield6").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("stagefield7").Text, 2)) = "ER" Then
    grdItems.Columns("stagefield7").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("stagefield8").Text, 2)) = "ER" Then
    grdItems.Columns("stagefield8").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("stagefield9").Text, 2)) = "ER" Then
    grdItems.Columns("stagefield9").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("stagefield10").Text, 2)) = "ER" Then
    grdItems.Columns("stagefield10").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("stagefield11").Text, 2)) = "ER" Then
    grdItems.Columns("stagefield11").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("stagefield12").Text, 2)) = "ER" Then
    grdItems.Columns("stagefield12").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("stagefield13").Text, 2)) = "ER" Then
    grdItems.Columns("stagefield13").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("stagefield14").Text, 2)) = "ER" Then
    grdItems.Columns("stagefield14").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("stagefield15").Text, 2)) = "ER" Then
    grdItems.Columns("stagefield15").CellStyleSet "Error"
End If

adoComments.RecordSource = "SELECT * FROM tracker_comment WHERE tracker_itemID = " & grdItems.Columns("tracker_itemID").Text & " ORDER BY cdate ASC;"
adoComments.ConnectionString = g_strConnection
adoComments.Refresh

End Sub

Private Sub optComplete_Click(Index As Integer)

cmdSearch.Value = True

End Sub
