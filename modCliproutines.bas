Attribute VB_Name = "modCliproutines"
Option Explicit
Type MediaInfoData
    txtFormat As String
    txtStreamType As String
    txtVideoCodec As String
    txtAudioCodec As String
    txtFrameRate As String
    txtCBRVBR As String
    txtVideoBitrate As String
    txtAudioBitrate As String
    txtOverallBitrate As String
    txtTrackCount As String
    txtChannelCount As String
    txtWidth As String
    txtHeight As String
    txtInterlace As String
    txtAspect As String
    txtGeometry As String
    txtDuration As String
    txtTimecodeStart As String
    txtTimecodeStop As String
    txtColorSpace As String
    txtChromaSubsmapling As String
    txtSeriesTitle As String
    txtSeriesNumber As String
    txtProgrammeTitle As String
    txtEpisodeNumber As String
    txtLineUpStart As String
    txtIdentClockStart As String
    txtTotalProgrammeDuration As String
    txtSynopsis As String
    txtCopyrightYear As String
    txtNumberOfParts As String
    txtAudioLayout As String
    txtPrimaryAudioLanguage As String
    txtSecondaryAudioLanguage As String
    txtTertiaryAudioLanguage As String
End Type

Type ScreenDimensions
    lngTopLeftX As Long
    lngTopLeftY As Long
    lngBottomRightX As Long
    lngBottomRightY As Long
End Type

Function CalculateAspectCoordinates(lp_lngHoriz As Long, lp_lngVert As Long, lp_strAspect As String, lp_strActivePictureRatio As String) As ScreenDimensions

Dim l_aspSquareDimensions As ScreenDimensions, l_dblRatio As Double, l_lngActiveVert As Long, l_lngActiveHoriz As Long, l_lngBlack As Long

l_dblRatio = Val(lp_strActivePictureRatio)

If lp_strAspect = "16:9" Then
    If lp_lngHoriz = 720 And lp_lngVert = 576 Then
        If l_dblRatio > 1.78 Then
            l_lngActiveVert = Int(1024 / l_dblRatio)
            l_lngBlack = 576 - l_lngActiveVert
            CalculateAspectCoordinates.lngTopLeftX = 0
            CalculateAspectCoordinates.lngTopLeftY = l_lngBlack / 2
            CalculateAspectCoordinates.lngBottomRightX = lp_lngHoriz - 1
            CalculateAspectCoordinates.lngBottomRightX = CalculateAspectCoordinates.lngTopLeftY + l_lngActiveVert - 1
        ElseIf l_dblRatio = 1.78 Then
            CalculateAspectCoordinates.lngTopLeftX = 0
            CalculateAspectCoordinates.lngTopLeftY = 0
            CalculateAspectCoordinates.lngBottomRightX = 719
            CalculateAspectCoordinates.lngBottomRightY = 575
        Else
            l_lngActiveHoriz = 576 * l_dblRatio
            l_lngBlack = 1024 - l_lngActiveHoriz
            CalculateAspectCoordinates.lngTopLeftX = l_lngBlack / 2
            CalculateAspectCoordinates.lngTopLeftY = 0
            CalculateAspectCoordinates.lngBottomRightX = CalculateAspectCoordinates.lngTopLeftX + l_lngActiveHoriz - 1
            CalculateAspectCoordinates.lngBottomRightY = lp_lngVert - 1
        End If
    ElseIf lp_lngHoriz = 720 And lp_lngVert = 480 Then
        If l_dblRatio > 1.78 Then
            l_lngActiveVert = Int(853 / l_dblRatio)
            l_lngBlack = 480 - l_lngActiveVert
            CalculateAspectCoordinates.lngTopLeftX = 0
            CalculateAspectCoordinates.lngTopLeftY = l_lngBlack / 2
            CalculateAspectCoordinates.lngBottomRightX = lp_lngHoriz - 1
            CalculateAspectCoordinates.lngBottomRightX = CalculateAspectCoordinates.lngTopLeftY + l_lngActiveVert - 1
        ElseIf l_dblRatio = 1.78 Then
            CalculateAspectCoordinates.lngTopLeftX = 0
            CalculateAspectCoordinates.lngTopLeftY = 0
            CalculateAspectCoordinates.lngBottomRightX = 719
            CalculateAspectCoordinates.lngBottomRightY = 479
        Else
            l_lngActiveHoriz = 480 * l_dblRatio
            l_lngBlack = 853 - l_lngActiveHoriz
            CalculateAspectCoordinates.lngTopLeftX = l_lngBlack / 2
            CalculateAspectCoordinates.lngTopLeftY = 0
            CalculateAspectCoordinates.lngBottomRightX = CalculateAspectCoordinates.lngTopLeftX + l_lngActiveHoriz - 1
            CalculateAspectCoordinates.lngBottomRightY = lp_lngVert - 1
        End If
    Else
        If l_dblRatio > 1.78 Then
            l_lngActiveVert = Int(1920 / l_dblRatio)
            l_lngBlack = 1080 - l_lngActiveVert
            CalculateAspectCoordinates.lngTopLeftX = 0
            CalculateAspectCoordinates.lngTopLeftY = l_lngBlack / 2
            CalculateAspectCoordinates.lngBottomRightX = lp_lngHoriz - 1
            CalculateAspectCoordinates.lngBottomRightX = CalculateAspectCoordinates.lngTopLeftY + l_lngActiveVert - 1
        ElseIf l_dblRatio = 1.78 Then
            CalculateAspectCoordinates.lngTopLeftX = 0
            CalculateAspectCoordinates.lngTopLeftY = 0
            CalculateAspectCoordinates.lngBottomRightX = 1919
            CalculateAspectCoordinates.lngBottomRightY = 1079
        Else
            l_lngActiveHoriz = 1080 * l_dblRatio
            l_lngBlack = 1920 - l_lngActiveHoriz
            CalculateAspectCoordinates.lngTopLeftX = l_lngBlack / 2
            CalculateAspectCoordinates.lngTopLeftY = 0
            CalculateAspectCoordinates.lngBottomRightX = CalculateAspectCoordinates.lngTopLeftX + l_lngActiveHoriz - 1
            CalculateAspectCoordinates.lngBottomRightY = lp_lngVert - 1
        End If
    
    End If
ElseIf lp_strAspect = "4:3" Then
    If lp_lngHoriz = 720 And lp_lngVert = 576 Then
        l_lngActiveVert = Int(768 / l_dblRatio)
        l_lngBlack = 576 - l_lngActiveVert
        CalculateAspectCoordinates.lngTopLeftX = 0
        CalculateAspectCoordinates.lngTopLeftY = l_lngBlack / 2
        CalculateAspectCoordinates.lngBottomRightX = lp_lngHoriz - 1
        CalculateAspectCoordinates.lngBottomRightX = CalculateAspectCoordinates.lngTopLeftY + l_lngActiveVert - 1
    ElseIf lp_lngHoriz = 720 And lp_lngVert = 480 Then
        l_lngActiveVert = Int(640 / l_dblRatio)
        l_lngBlack = 480 - l_lngActiveVert
        CalculateAspectCoordinates.lngTopLeftX = 0
        CalculateAspectCoordinates.lngTopLeftY = l_lngBlack / 2
        CalculateAspectCoordinates.lngBottomRightX = lp_lngHoriz - 1
        CalculateAspectCoordinates.lngBottomRightX = CalculateAspectCoordinates.lngTopLeftY + l_lngActiveVert - 1
    Else
        MsgBox "Error calculating Actiove Pixel sizes - HD must be 16:9 raster ratio", vbCritical
    End If
End If

End Function
Function EmptyCheck(lp_varItem As Variant) As Boolean

If IsNull(lp_varItem) Or lp_varItem = "" Or lp_varItem = 0 Then
    EmptyCheck = True
Else
    EmptyCheck = False
End If

End Function

Sub ShowClipSearch(Optional lp_strProgRefID As String, Optional lp_strReference As String, Optional lp_strFilename As String, Optional lp_strTitle As String)

If lp_strProgRefID <> "" Or lp_strReference <> "" Or lp_strFilename <> "" Or lp_strTitle <> "" Then
    ClearFields frmClipSearch
    frmClipSearch.cmbField3.Text = lp_strProgRefID
    frmClipSearch.txtReference.Text = lp_strReference
    frmClipSearch.txtClipfilename.Text = lp_strFilename
    frmClipSearch.optStorageType(0).Value = False
    frmClipSearch.txtTitle.Text = lp_strTitle
End If

frmClipSearch.Show
frmClipSearch.ZOrder 0

End Sub
Sub ShowClipSpec(lp_MediaSpecTable As String, lp_MediaSpecForm As Form, lp_MediaSpecID As Long, lp_blnClearIfNone As Boolean, lp_blnShowOrder As Boolean, Optional ShowForm As Boolean)

If Not CheckAccess("/showlibrary") Then Exit Sub

Dim l_strSQL As String

l_strSQL = "SELECT * FROM " & lp_MediaSpecTable & " WHERE " & lp_MediaSpecTable & "ID = '" & lp_MediaSpecID & "';"

Dim l_rstSpec As New ADODB.Recordset
Set l_rstSpec = ExecuteSQL(l_strSQL, g_strExecuteError)

If Not l_rstSpec.EOF Then

    l_rstSpec.MoveFirst
    
    With lp_MediaSpecForm
        .cmbMediaSpec.Text = Trim(" " & l_rstSpec("mediaspecname"))
        .lblMediaSpecID.Caption = l_rstSpec(lp_MediaSpecTable & "ID")
        .cmbDelivery.Text = Trim(" " & l_rstSpec("delivery"))
        .txtAltLocation.Text = Trim(" " & l_rstSpec("altlocation"))
        .cmbClipformat.Text = Trim(" " & l_rstSpec("mediaformat"))
        .cmbClipcodec.Text = Trim(" " & l_rstSpec("videocodec"))
        .lblVideoCodecFFMPEG.Caption = Trim(" " & l_rstSpec("videocodecffmpeg"))
        .cmbStreamType.Text = Trim(" " & l_rstSpec("mediastreamtype"))
        .cmbAudioCodec.Text = Trim(" " & l_rstSpec("audiocodec"))
        .lblAudioCodecFFMPEG.Caption = Trim(" " & l_rstSpec("audiocodecffmpeg"))
        .txtHorizpixels.Text = Trim(" " & l_rstSpec("horiz"))
        .txtVertpixels.Text = Trim(" " & l_rstSpec("vert"))
        .cmbFrameRate.Text = Trim(" " & l_rstSpec("framerate"))
        .txtBitrate.Text = Trim(" " & l_rstSpec("totalbitrate"))
        .txtVideoBitrate.Text = Trim(" " & l_rstSpec("videobitrate"))
        .txtAudioBitrate.Text = Trim(" " & l_rstSpec("audiobitrate"))
        .cmbAspect.Text = Trim(" " & l_rstSpec("aspectratio"))
        .cmbGeometry.Text = Trim(" " & l_rstSpec("geometry"))
        .txtOtherSpecs.Text = Trim(" " & l_rstSpec("otherspecs"))
        .cmbPurpose.Text = Trim(" " & l_rstSpec("purpose"))
        If lp_blnShowOrder = True Then .txtSortOrder.Text = Trim(" " & l_rstSpec("fd_order"))
        .cmbPasses.Text = Trim(" " & l_rstSpec("encodepasses"))
        .cmbCbrVbr.Text = Trim(" " & l_rstSpec("cbrvbr"))
        .cmbInterlace.Text = Trim(" " & l_rstSpec("interlace"))
        .cmbSource.Text = Trim(" " & l_rstSpec("mediasource"))
        .txtSourceAddress.Text = Trim(" " & l_rstSpec("sourceaddress"))
        .txtSourceLogin.Text = Trim(" " & l_rstSpec("sourcelogin"))
        .txtSourcePassword.Text = Trim(" " & l_rstSpec("sourcepassword"))
        .cmbDestination.Text = Trim(" " & l_rstSpec("mediadelivery"))
        .txtDestinationAddress.Text = Trim(" " & l_rstSpec("deliveryaddress"))
        .txtDestinationLogin.Text = Trim(" " & l_rstSpec("deliverylogin"))
        .txtDestinationPassword.Text = Trim(" " & l_rstSpec("deliverypassword"))
        .txtFirstAudioTrack.Text = Trim(" " & l_rstSpec("audiosourcefirsttrack"))
        .txtSecondAudioTrack.Text = Trim(" " & l_rstSpec("audiosourceSecondtrack"))
        .txtGraphicOverlayFilename.Text = Trim(" " & l_rstSpec("overlayfilename"))
        .txtGraphicOverlayOpacity.Text = Trim(" " & l_rstSpec("overlayopacity"))
    End With

ElseIf lp_blnClearIfNone = True Then
    With lp_MediaSpecForm
        .cmbMediaSpec.Text = ""
        .lblMediaSpecID.Caption = ""
        .cmbDelivery.Text = ""
        .txtAltLocation.Text = ""
        .cmbClipformat.Text = ""
        .cmbClipcodec.Text = ""
        .cmbStreamType.Text = ""
        .cmbAudioCodec.Text = ""
        .txtHorizpixels.Text = ""
        .txtVertpixels.Text = ""
        .cmbFrameRate.Text = ""
        .txtBitrate.Text = ""
        .txtVideoBitrate.Text = ""
        .txtAudioBitrate.Text = ""
        .cmbAspect.Text = ""
        .cmbGeometry.Text = ""
        .txtOtherSpecs.Text = ""
        .cmbPurpose.Text = ""
        If lp_blnShowOrder = True Then .txtSortOrder.Text = ""
        .cmbPasses.Text = ""
        .cmbCbrVbr.Text = ""
        .cmbInterlace.Text = ""
        .cmbSource.Text = ""
        .txtSourceAddress.Text = ""
        .txtSourceLogin.Text = ""
        .txtSourcePassword.Text = ""
        .cmbDestination.Text = ""
        .txtDestinationAddress.Text = ""
        .txtDestinationLogin.Text = ""
        .txtDestinationPassword.Text = ""
        .cmbVodPlatform.Text = ""
        .txtFirstAudioTrack.Text = ""
        .txtSecondAudioTrack.Text = ""
        .txtGraphicOverlayFilename.Text = ""
        .txtGraphicOverlayOpacity.Text = ""
    End With
    
End If

If ShowForm = True Then
    lp_MediaSpecForm.Show
    lp_MediaSpecForm.ZOrder 0
End If

End Sub

Sub DeleteMediaSpec()

Dim l_strSQL As String, l_lngMediaSpecID As Long

If frmClipSpec.lblMediaSpecID.Caption <> "" Then
    l_lngMediaSpecID = Val(frmClipSpec.lblMediaSpecID.Caption)
    
    If MsgBox("Are you sure you wish to delete" & vbCrLf & frmClipSpec.cmbMediaSpec.Text & "?", vbYesNo, "Delete Media Specification") = vbYes Then
        l_strSQL = "DELETE FROM mediaspec WHERE mediaspecID = " & l_lngMediaSpecID & ";"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        ClearFields frmClipSpec
        frmClipSpec.adoMediaSpec.Refresh
    End If
    
End If

End Sub
Sub SaveExistingMediaSpec(MediaSpecTable As String, MediaSpecForm As Form, MediaSpecID As Long, SaveSortOrder As Boolean)

Dim l_strSQL As String

With MediaSpecForm

    l_strSQL = "UPDATE " & MediaSpecTable & " SET "
    l_strSQL = l_strSQL & "mediaspecname = '" & .cmbMediaSpec.Text & "', "
    l_strSQL = l_strSQL & "mediaformat = '" & .cmbClipformat.Text & "', "
    l_strSQL = l_strSQL & "videocodec = '" & .cmbClipcodec.Text & "', "
    l_strSQL = l_strSQL & "videocodecffmpeg = '" & .lblVideoCodecFFMPEG.Caption & "', "
    l_strSQL = l_strSQL & "mediastreamtype = '" & .cmbStreamType.Text & "', "
    l_strSQL = l_strSQL & "audiocodec = '" & .cmbAudioCodec.Text & "', "
    l_strSQL = l_strSQL & "audiocodecffmpeg = '" & .lblAudioCodecFFMPEG.Caption & "', "
    l_strSQL = l_strSQL & "purpose = '" & .cmbPurpose.Text & "', "
    l_strSQL = l_strSQL & "horiz = '" & .txtHorizpixels.Text & "', "
    l_strSQL = l_strSQL & "vert = '" & .txtVertpixels.Text & "', "
    l_strSQL = l_strSQL & "framerate = '" & .cmbFrameRate.Text & "', "
    l_strSQL = l_strSQL & "videobitrate = '" & .txtVideoBitrate.Text & "', "
    l_strSQL = l_strSQL & "audiobitrate = '" & .txtAudioBitrate.Text & "', "
    l_strSQL = l_strSQL & "totalbitrate = '" & .txtBitrate.Text & "', "
    l_strSQL = l_strSQL & "aspectratio = '" & .cmbAspect.Text & "', "
    l_strSQL = l_strSQL & "geometry = '" & .cmbGeometry.Text & "', "
    l_strSQL = l_strSQL & "delivery = '" & .cmbDelivery.Text & "', "
    l_strSQL = l_strSQL & "altlocation = '" & .txtAltLocation.Text & "', "
    If SaveSortOrder = True Then l_strSQL = l_strSQL & "fd_order = '" & .txtSortOrder.Text & "', "
    l_strSQL = l_strSQL & "cbrvbr = '" & .cmbCbrVbr.Text & "', "
    l_strSQL = l_strSQL & "interlace = '" & .cmbInterlace.Text & "', "
    l_strSQL = l_strSQL & "encodepasses = '" & .cmbPasses.Text & "', "
    l_strSQL = l_strSQL & "mediasource = '" & .cmbSource.Text & "', "
    l_strSQL = l_strSQL & "sourceaddress = '" & .txtSourceAddress.Text & "', "
    l_strSQL = l_strSQL & "sourcelogin = '" & .txtSourceLogin.Text & "', "
    l_strSQL = l_strSQL & "sourcepassword = '" & .txtSourcePassword.Text & "', "
    l_strSQL = l_strSQL & "mediadelivery = '" & .cmbDestination.Text & "', "
    l_strSQL = l_strSQL & "deliveryaddress = '" & .txtDestinationAddress.Text & "', "
    l_strSQL = l_strSQL & "deliverylogin = '" & .txtDestinationLogin.Text & "', "
    l_strSQL = l_strSQL & "deliverypassword = '" & .txtDestinationPassword.Text & "', "
    l_strSQL = l_strSQL & "audiosourcefirsttrack = '" & .txtFirstAudioTrack.Text & "', "
    l_strSQL = l_strSQL & "audiosourcesecondtrack = '" & .txtSecondAudioTrack.Text & "', "
    l_strSQL = l_strSQL & "vodplatform = '" & .cmbVodPlatform.Text & "', "
    l_strSQL = l_strSQL & "overlayfilename = '" & .txtGraphicOverlayFilename.Text & "', "
    l_strSQL = l_strSQL & "overlayopacity = '" & .txtGraphicOverlayOpacity.Text & "', "
    l_strSQL = l_strSQL & "otherspecs = '" & .txtOtherSpecs.Text & "' "
    l_strSQL = l_strSQL & "WHERE " & MediaSpecTable & "ID = " & MediaSpecID

End With

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Sub

Sub SaveMediaSpec()

Dim l_lngMediaSpecID As Long
Dim l_strSQL As String

If frmClipSpec.lblMediaSpecID.Caption <> "" Then

    'Existing Specification - Check whether the Name has changed, and if it has check whether this is an edit or a new Spec after all
    
    If frmClipSpec.cmbMediaSpec.Text <> GetData("mediaspec", "mediaspecname", "mediaspecID", Val(frmClipSpec.lblMediaSpecID.Caption)) Then
        If MsgBox("Is this a New Specification?", vbYesNo, "Specification Name has been Changed") = vbYes Then GoTo NEW_CLIP_SAVE
    End If
    
    l_lngMediaSpecID = Val(frmClipSpec.lblMediaSpecID.Caption)
    
    SaveExistingMediaSpec "mediaspec", frmClipSpec, l_lngMediaSpecID, True

Else

NEW_CLIP_SAVE:

    'New Specification
    l_strSQL = "INSERT INTO mediaspec (mediaspecname, mediaformat, videocodec, videocodecffmpeg, "
    l_strSQL = l_strSQL & "audiocodec, audiocodecffmpeg, purpose, horiz, vert, framerate, videobitrate, audiobitrate, "
    l_strSQL = l_strSQL & "totalbitrate, aspectratio, geometry, delivery, altlocation, "
    l_strSQL = l_strSQL & "fd_order, encodepasses, cbrvbr, interlace, "
    l_strSQL = l_strSQL & "mediasource, sourceaddress, sourcelogin, sourcepassword, mediadelivery, deliveryaddress, deliverylogin, deliverypassword, audiosourcefirsttrack, audiosourcesecondtrack, "
    l_strSQL = l_strSQL & "overlayfilename, overlayopacity, "
    l_strSQL = l_strSQL & "otherspecs) VALUES ("
    
    With frmClipSpec
    
        l_strSQL = l_strSQL & "'" & .cmbMediaSpec.Text & "', "
        l_strSQL = l_strSQL & "'" & .cmbClipformat.Text & "', "
        l_strSQL = l_strSQL & "'" & .cmbClipcodec.Text & "', "
        l_strSQL = l_strSQL & "'" & .lblVideoCodecFFMPEG.Caption & "', "
        l_strSQL = l_strSQL & "'" & .cmbAudioCodec.Text & "', "
        l_strSQL = l_strSQL & "'" & .lblAudioCodecFFMPEG.Caption & "', "
        l_strSQL = l_strSQL & "'" & .cmbPurpose.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtHorizpixels.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtVertpixels.Text & "', "
        l_strSQL = l_strSQL & "'" & .cmbFrameRate.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtVideoBitrate.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtAudioBitrate.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtBitrate.Text & "', "
        l_strSQL = l_strSQL & "'" & .cmbAspect.Text & "', "
        l_strSQL = l_strSQL & "'" & .cmbGeometry.Text & "', "
        l_strSQL = l_strSQL & "'" & .cmbDelivery.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtAltLocation.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtSortOrder.Text & "', "
        l_strSQL = l_strSQL & "'" & .cmbPasses.Text & "', "
        l_strSQL = l_strSQL & "'" & .cmbCbrVbr.Text & "', "
        l_strSQL = l_strSQL & "'" & .cmbInterlace.Text & "', "
        l_strSQL = l_strSQL & "'" & .cmbSource.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtSourceAddress.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtSourceLogin.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtSourcePassword.Text & "', "
        l_strSQL = l_strSQL & "'" & .cmbDestination.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtDestinationAddress.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtDestinationLogin.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtDestinationPassword.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtFirstAudioTrack.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtSecondAudioTrack.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtGraphicOverlayFilename.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtGraphicOverlayOpacity.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtOtherSpecs.Text & "'); "
    
    End With
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_lngMediaSpecID = g_lngLastID
    
        
End If

frmClipSpec.adoMediaSpec.Refresh
ShowClipSpec "mediaspec", frmClipSpec, l_lngMediaSpecID, True, True

End Sub
Function PromptClipChanges(lp_lngClipID As Long) As Boolean

Dim l_intResponse As Integer
If CheckClipChanges(lp_lngClipID) = True Then
    l_intResponse = MsgBox("Data has changed. Do you wish to save?" & vbCrLf & "Think about the answer, before clicking Yes.", vbYesNo + vbDefaultButton2 + vbQuestion, "Data has changed.")
    If l_intResponse = vbYes Then
        If SaveClip = True Then PromptClipChanges = True Else PromptClipChanges = False
    Else
        PromptClipChanges = False
    End If
Else
    PromptClipChanges = True
End If

End Function

Sub NoClipSelectedMessage()
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    MsgBox "Please select a valid item first", vbExclamation
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Function CheckClipChanges(lp_lngClipID As Long) As Boolean

CheckClipChanges = False

With frmClipControl

If lp_lngClipID = 0 Then
'Clip has never been saved so check all fields have no new information
    If .lblLibraryID.Caption <> "" Then CheckClipChanges = True
    If .txtJobID.Text <> "" Then CheckClipChanges = True
    If .txtJobDetailID.Text <> "" Then CheckClipChanges = True
    If .cmbClipformat.Text <> "" Then CheckClipChanges = True
    If .cmbClipcodec.Text <> "" Then CheckClipChanges = True
    If .cmbAudioCodec.Text <> "" Then CheckClipChanges = True
    If .cmbAspect.Text <> "" Then CheckClipChanges = True
    If .cmbGeometry.Text <> "" Then CheckClipChanges = True
    If .txtTimecodeStart.Text <> "" Then CheckClipChanges = True
    If .txtTimecodeStop.Text <> "" Then CheckClipChanges = True
    If .txtDuration.Text <> "" Then CheckClipChanges = True
    If .txtSourceEventID.Text <> "" Then CheckClipChanges = True
    If .lblSourceLibraryID.Caption <> "" Then CheckClipChanges = True
    If .txtHorizpixels.Text <> "" Then CheckClipChanges = True
    If .txtVertpixels.Text <> "" Then CheckClipChanges = True
    If .txtBitrate.Text <> "" Then CheckClipChanges = True
    If .txtAudioBitrate.Text <> "" Then CheckClipChanges = True
    If .txtVideoBitrate.Text <> "" Then CheckClipChanges = True
    If .cmbPasses.Text <> "" Then CheckClipChanges = True
    If .cmbInterlace.Text <> "" Then CheckClipChanges = True
    If .cmbCbrVbr.Text <> "" Then CheckClipChanges = True
    If .cmbCompany.Text <> "" Then CheckClipChanges = True
    If .lblCompanyID.Caption <> "" Then CheckClipChanges = True
    If .txtTitle.Text <> "" Then CheckClipChanges = True
    If .cmbSeries.Text <> "" Then CheckClipChanges = True
    If .cmbSet.Text <> "" Then CheckClipChanges = True
    If .cmbEpisode.Text <> "" Then CheckClipChanges = True
    If .txtSubtitle.Text <> "" Then CheckClipChanges = True
    If .cmbVersion.Text <> "" Then CheckClipChanges = True
    If .txtAltFolder.Text <> "" Then CheckClipChanges = True
    If .txtClipfilename.Text <> "" Then CheckClipChanges = True
    If .txtNotes.Text <> "" Then CheckClipChanges = True
    If .txtInternalNotes.Text <> "" Then CheckClipChanges = True
    If .txtEndCredits.Text <> "" Then CheckClipChanges = True
    If .txtEndCreditsElapsed.Text <> "" Then CheckClipChanges = True
'    If .txtBreak1.Text <> "" Then CheckClipChanges = True
'    If .txtBreak1elapsed.Text <> "" Then CheckClipChanges = True
'    If .txtBreak2.Text <> "" Then CheckClipChanges = True
'    If .txtBreak2elapsed.Text <> "" Then CheckClipChanges = True
'    If .txtBreak3.Text <> "" Then CheckClipChanges = True
'    If .txtBreak3elapsed.Text <> "" Then CheckClipChanges = True
'    If .txtBreak4.Text <> "" Then CheckClipChanges = True
'    If .txtBreak4elapsed.Text <> "" Then CheckClipChanges = True
'    If .txtBreak5.Text <> "" Then CheckClipChanges = True
'    If .txtBreak5elapsed.Text <> "" Then CheckClipChanges = True
'    If .txtBreak6.Text <> "" Then CheckClipChanges = True
'    If .txtBreak6elapsed.Text <> "" Then CheckClipChanges = True
    If .cmbAudioType1.Text <> "" Then CheckClipChanges = True
    If .cmbAudioType2.Text <> "" Then CheckClipChanges = True
    If .cmbAudioType3.Text <> "" Then CheckClipChanges = True
    If .cmbAudioType4.Text <> "" Then CheckClipChanges = True
    If .cmbAudioType5.Text <> "" Then CheckClipChanges = True
    If .cmbAudioType6.Text <> "" Then CheckClipChanges = True
    If .cmbAudioType7.Text <> "" Then CheckClipChanges = True
    If .cmbAudioType8.Text <> "" Then CheckClipChanges = True
'    If .cmbAudioType9.Text <> "" Then CheckClipChanges = True
'    If .cmbAudioType10.Text <> "" Then CheckClipChanges = True
    If .cmbAudioContent1.Text <> "" Then CheckClipChanges = True
    If .cmbAudioContent2.Text <> "" Then CheckClipChanges = True
    If .cmbAudioContent3.Text <> "" Then CheckClipChanges = True
    If .cmbAudioContent4.Text <> "" Then CheckClipChanges = True
    If .cmbAudioContent5.Text <> "" Then CheckClipChanges = True
    If .cmbAudioContent6.Text <> "" Then CheckClipChanges = True
    If .cmbAudioContent7.Text <> "" Then CheckClipChanges = True
    If .cmbAudioContent8.Text <> "" Then CheckClipChanges = True
'    If .cmbAudioContent9.Text <> "" Then CheckClipChanges = True
'    If .cmbAudioContent10.Text <> "" Then CheckClipChanges = True
    If .cmbAudioLanguage1.Text <> "" Then CheckClipChanges = True
    If .cmbAudioLanguage2.Text <> "" Then CheckClipChanges = True
    If .cmbAudioLanguage3.Text <> "" Then CheckClipChanges = True
    If .cmbAudioLanguage4.Text <> "" Then CheckClipChanges = True
    If .cmbAudioLanguage5.Text <> "" Then CheckClipChanges = True
    If .cmbAudioLanguage6.Text <> "" Then CheckClipChanges = True
    If .cmbAudioLanguage7.Text <> "" Then CheckClipChanges = True
    If .cmbAudioLanguage8.Text <> "" Then CheckClipChanges = True
'    If .cmbAudioLanguage9.Text <> "" Then CheckClipChanges = True
'    If .cmbAudioLanguage10.Text <> "" Then CheckClipChanges = True
    If .txtClockNumber.Text <> "" Then CheckClipChanges = True
    If .cmbPurpose.Text <> "" Then CheckClipChanges = True
    If .cmbGenre.Text <> "" Then CheckClipChanges = True
    If .txtKeyframeFilename.Text <> "" Then CheckClipChanges = True
    If .txtTimecodeKeyframe.Text <> "" Then CheckClipChanges = True
    If .txtInternalReference.Text <> "" Then CheckClipChanges = True
    If .txtReference.Text <> "" Then CheckClipChanges = True
    If .cmbField1.Text <> "" Then CheckClipChanges = True
    If .cmbField2.Text <> "" Then CheckClipChanges = True
    If .cmbField3.Text <> "" Then CheckClipChanges = True
    If .cmbField4.Text <> "" Then CheckClipChanges = True
    If .cmbField5.Text <> "" Then CheckClipChanges = True
    If .cmbField6.Text <> "" Then CheckClipChanges = True
    If .txtMD5Checksum.Text <> "" Then CheckClipChanges = True
    If .txtSHA1Checksum.Text <> "" Then CheckClipChanges = True
    If .cmbStreamType.Text <> "" Then CheckClipChanges = True
Else
'Clip has been saved so check all fields against database info to see if its changed
    If Trim(" " & GetData("events", "libraryID", "eventID", lp_lngClipID)) <> .lblLibraryID.Caption Then CheckClipChanges = True
    If Trim(" " & GetData("events", "jobID", "eventID", lp_lngClipID, True)) <> .txtJobID.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "jobdetailID", "eventID", lp_lngClipID, True)) <> .txtJobDetailID.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "clipformat", "eventID", lp_lngClipID)) <> .cmbClipformat.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "clipcodec", "eventID", lp_lngClipID)) <> .cmbClipcodec.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "clipaudiocodec", "eventID", lp_lngClipID)) <> .cmbAudioCodec.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "aspectratio", "eventID", lp_lngClipID)) <> .cmbAspect.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "geometriclinearity", "eventID", lp_lngClipID)) <> .cmbGeometry.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "timecodestart", "eventID", lp_lngClipID)) <> .txtTimecodeStart.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "timecodestop", "eventID", lp_lngClipID)) <> .txtTimecodeStop.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "fd_length", "eventID", lp_lngClipID)) <> .txtDuration.Text Then CheckClipChanges = True
    If .chkSourceFromMedia.Value <> 0 Then
        If Trim(" " & GetData("events", "sourceeventID", "eventID", lp_lngClipID, True)) <> .txtSourceEventID.Text Then CheckClipChanges = True
    Else
        If Trim(" " & GetData("events", "sourcetapeeventID", "eventID", lp_lngClipID, True)) <> .txtSourceEventID.Text Then CheckClipChanges = True
    End If
    If Trim(" " & GetData("events", "sourcelibraryID", "eventID", lp_lngClipID, True)) <> .lblSourceLibraryID.Caption Then CheckClipChanges = True
    If Trim(" " & GetData("events", "cliphorizontalpixels", "eventID", lp_lngClipID, True)) <> .txtHorizpixels.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "clipverticalpixels", "eventID", lp_lngClipID, True)) <> .txtVertpixels.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "clipbitrate", "eventID", lp_lngClipID, True)) <> .txtBitrate.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "videobitrate", "eventID", lp_lngClipID, True)) <> .txtVideoBitrate.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiobitrate", "eventID", lp_lngClipID, True)) <> .txtAudioBitrate.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "companyname", "eventID", lp_lngClipID)) <> .cmbCompany.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "companyID", "eventID", lp_lngClipID, True)) <> .lblCompanyID.Caption Then CheckClipChanges = True
    If Trim(" " & GetData("events", "eventtitle", "eventID", lp_lngClipID)) <> .txtTitle.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "eventseries", "eventID", lp_lngClipID)) <> .cmbSeries.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "eventset", "eventID", lp_lngClipID)) <> .cmbSet.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "eventepisode", "eventID", lp_lngClipID)) <> .cmbEpisode.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "eventsubtitle", "eventID", lp_lngClipID)) <> .txtSubtitle.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "eventversion", "eventID", lp_lngClipID)) <> .cmbVersion.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "altlocation", "eventID", lp_lngClipID)) <> .txtAltFolder.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "clipfilename", "eventID", lp_lngClipID)) <> .txtClipfilename.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "notes", "eventID", lp_lngClipID)) <> .txtNotes.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "clocknumber", "eventID", lp_lngClipID)) <> .txtClockNumber.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "eventtype", "eventID", lp_lngClipID)) <> .cmbPurpose.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "cliptype", "eventID", lp_lngClipID)) <> .cmbClipType.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "clipgenre", "eventID", lp_lngClipID)) <> .cmbGenre.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "encodepasses", "eventID", lp_lngClipID, True)) <> .cmbPasses.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "cbrvbr", "eventID", lp_lngClipID)) <> .cmbCbrVbr.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "interlace", "eventID", lp_lngClipID)) <> .cmbInterlace.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "eventkeyframetimecode", "eventID", lp_lngClipID)) <> .txtTimecodeKeyframe.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "eventkeyframefilename", "eventID", lp_lngClipID)) <> .txtKeyframeFilename.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "internalreference", "eventID", lp_lngClipID, True)) <> .txtInternalReference.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "clipreference", "eventID", lp_lngClipID)) <> .txtReference.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "endcredits", "eventID", lp_lngClipID)) <> .txtEndCredits.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "endcreditselapsed", "eventID", lp_lngClipID)) <> .txtEndCreditsElapsed.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiotypegroup1", "eventID", lp_lngClipID)) <> .cmbAudioType1.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiotypegroup2", "eventID", lp_lngClipID)) <> .cmbAudioType2.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiotypegroup3", "eventID", lp_lngClipID)) <> .cmbAudioType3.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiotypegroup4", "eventID", lp_lngClipID)) <> .cmbAudioType4.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiotypegroup5", "eventID", lp_lngClipID)) <> .cmbAudioType5.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiotypegroup6", "eventID", lp_lngClipID)) <> .cmbAudioType6.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiotypegroup7", "eventID", lp_lngClipID)) <> .cmbAudioType7.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiotypegroup8", "eventID", lp_lngClipID)) <> .cmbAudioType8.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiocontentgroup1", "eventID", lp_lngClipID)) <> .cmbAudioContent1.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiocontentgroup2", "eventID", lp_lngClipID)) <> .cmbAudioContent2.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiocontentgroup3", "eventID", lp_lngClipID)) <> .cmbAudioContent3.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiocontentgroup4", "eventID", lp_lngClipID)) <> .cmbAudioContent4.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiocontentgroup5", "eventID", lp_lngClipID)) <> .cmbAudioContent5.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiocontentgroup6", "eventID", lp_lngClipID)) <> .cmbAudioContent6.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiocontentgroup7", "eventID", lp_lngClipID)) <> .cmbAudioContent7.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiocontentgroup8", "eventID", lp_lngClipID)) <> .cmbAudioContent8.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiolanguagegroup1", "eventID", lp_lngClipID)) <> .cmbAudioLanguage1.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiolanguagegroup2", "eventID", lp_lngClipID)) <> .cmbAudioLanguage2.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiolanguagegroup3", "eventID", lp_lngClipID)) <> .cmbAudioLanguage3.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiolanguagegroup4", "eventID", lp_lngClipID)) <> .cmbAudioLanguage4.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiolanguagegroup5", "eventID", lp_lngClipID)) <> .cmbAudioLanguage5.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiolanguagegroup6", "eventID", lp_lngClipID)) <> .cmbAudioLanguage6.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiolanguagegroup7", "eventID", lp_lngClipID)) <> .cmbAudioLanguage7.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "audiolanguagegroup8", "eventID", lp_lngClipID)) <> .cmbAudioLanguage8.Text Then CheckClipChanges = True
    If Val(GetData("events", "fourbythreeflag", "eventID", lp_lngClipID)) <> .chkfourbythreeflag.Value Then CheckClipChanges = True
    If Val(GetData("events", "sourcefrommedia", "eventID", lp_lngClipID)) <> .chkSourceFromMedia.Value Then CheckClipChanges = True
    If Trim(" " & GetData("events", "customfield1", "eventID", lp_lngClipID)) <> .cmbField1.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "customfield2", "eventID", lp_lngClipID)) <> .cmbField2.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "customfield3", "eventID", lp_lngClipID)) <> .cmbField3.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "customfield4", "eventID", lp_lngClipID)) <> .cmbField4.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "customfield5", "eventID", lp_lngClipID)) <> .cmbField5.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "customfield6", "eventID", lp_lngClipID)) <> .cmbField6.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "sha1checksum", "eventID", lp_lngClipID)) <> .txtSHA1Checksum.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "md5checksum", "eventID", lp_lngClipID)) <> .txtMD5Checksum.Text Then CheckClipChanges = True
    If Trim(" " & GetData("events", "mediastreamtype", "eventID", lp_lngClipID)) <> .cmbStreamType.Text Then CheckClipChanges = True
End If

End With

End Function

Public Function MakeClipInAdvance(lp_lngMediaSpecID As Long, lp_strTitle As String, lp_strEpisode As String, _
    lp_lngJobID As Long, lp_strMatID As String, lp_strLanguage As String, lp_strInternalReference As String, _
    lp_strBarcode As String, lp_lngCompanyID As Long, lp_strSupplier As String, lp_strFilename As String, lp_strAltLocation As String) As Long

Dim l_strSQL As String, l_rstSpec As ADODB.Recordset, l_lngClipID As Long


l_strSQL = "INSERT INTO events("
l_strSQL = l_strSQL & "eventmediatype, "
l_strSQL = l_strSQL & "cdate, "
l_strSQL = l_strSQL & "cuser) VALUES ("
l_strSQL = l_strSQL & "'FILE', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'" & g_strUserInitials & "')"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

MakeClipInAdvance = g_lngLastID
l_lngClipID = g_lngLastID
    
l_strSQL = "UPDATE events SET "

If lp_lngMediaSpecID <> 0 Then
    
    Set l_rstSpec = ExecuteSQL("SELECT * FROM mediaspec WHERE mediaspecID = '" & lp_lngMediaSpecID & "';", g_strExecuteError)
    CheckForSQLError
    
    If Not l_rstSpec.EOF Then
    
        'Get info from clipspec and set those fields
        l_strSQL = l_strSQL & "eventtype = '" & Trim(" " & l_rstSpec("purpose")) & "', "
        l_strSQL = l_strSQL & "clipformat = '" & Trim(" " & l_rstSpec("mediaformat")) & "', "
        l_strSQL = l_strSQL & "clipcodec = '" & Trim(" " & l_rstSpec("videocodec")) & "', "
        l_strSQL = l_strSQL & "clipaudiocodec  = '" & Trim(" " & l_rstSpec("audiocodec")) & "', "
        l_strSQL = l_strSQL & "encodepasses = '" & Trim(" " & l_rstSpec("encodepasses")) & "', "
        l_strSQL = l_strSQL & "cliphorizontalpixels = '" & Trim(" " & l_rstSpec("horiz")) & "', "
        l_strSQL = l_strSQL & "clipverticalpixels = '" & Trim(" " & l_rstSpec("vert")) & "', "
        l_strSQL = l_strSQL & "clipframerate = '" & Trim(" " & l_rstSpec("framerate")) & "', "
        l_strSQL = l_strSQL & "clipbitrate = '" & Trim(" " & l_rstSpec("totalbitrate")) & "', "
        l_strSQL = l_strSQL & "videobitrate = '" & Trim(" " & l_rstSpec("videobitrate")) & "', "
        l_strSQL = l_strSQL & "audiobitrate = '" & Trim(" " & l_rstSpec("audiobitrate")) & "', "
        l_strSQL = l_strSQL & "aspectratio = '" & Trim(" " & l_rstSpec("aspectratio")) & "', "
        l_strSQL = l_strSQL & "geometriclinearity = '" & Trim(" " & l_rstSpec("geometry")) & "', "
        If Trim(" " & l_rstSpec("altlocation")) <> "" Then
            l_strSQL = l_strSQL & "altlocation = '" & QuoteSanitise(Trim(" " & l_rstSpec("altlocation"))) & "', "
        End If
        l_strSQL = l_strSQL & "cbrvbr = '" & Trim(" " & l_rstSpec("cbrvbr")) & "', "
        l_strSQL = l_strSQL & "interlace = '" & Trim(" " & l_rstSpec("interlace")) & "', "
    End If
    l_rstSpec.Close
    Set l_rstSpec = Nothing
End If
        
'Then get the info passed as parameters and set those fields
If lp_strTitle <> "" Then l_strSQL = l_strSQL & "eventtitle = '" & QuoteSanitise(lp_strTitle) & "', " Else l_strSQL = l_strSQL & "eventtitle = NULL, "
If lp_strEpisode <> "" Then l_strSQL = l_strSQL & "eventepisode = '" & QuoteSanitise(lp_strEpisode) & "', " Else l_strSQL = l_strSQL & "eventepisode = NULL, "
l_strSQL = l_strSQL & "clipreference = '" & lp_strInternalReference & "', "
l_strSQL = l_strSQL & "internalreference = '" & lp_strInternalReference & "', "
l_strSQL = l_strSQL & "libraryID = '" & GetData("library", "libraryID", "barcode", lp_strBarcode) & "', "
l_strSQL = l_strSQL & "companyID = '" & lp_lngCompanyID & "', "
If InStr(GetData("company", "cetaclientcode", "companyID", lp_lngCompanyID), "/hide") > 0 Then
    l_strSQL = l_strSQL & "hidefromweb = 1, "
End If
l_strSQL = l_strSQL & "eventversion = '" & lp_strLanguage & "', "
l_strSQL = l_strSQL & "altlocation = '" & lp_strAltLocation & "', "
l_strSQL = l_strSQL & "companyname = '" & GetData("company", "name", "companyID", lp_lngCompanyID) & "', "
If InStr(Trim(" " & GetData("company", "cetaclientcode", "companyID", lp_lngCompanyID)), "/contract") <> 0 Then
    l_strSQL = l_strSQL & "customfield2 = '" & lp_strMatID & "', "
    l_strSQL = l_strSQL & "customfield1 = '" & lp_strSupplier & "', "
End If
l_strSQL = l_strSQL & "clipfilename = '" & lp_strFilename & "' "
l_strSQL = l_strSQL & "WHERE eventID = " & l_lngClipID & ";"
        
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

If GetData("library", "format", "barcode", lp_strBarcode) = "DISCSTORE" Then
    SetData "events", "online", "eventID", l_lngClipID, 1
Else
    SetData "events", "online", "eventID", l_lngClipID, 0
End If

End Function

Public Function CreateClip() As Long
Dim l_strSQL As String

l_strSQL = "INSERT INTO events("
l_strSQL = l_strSQL & "eventmediatype, "
l_strSQL = l_strSQL & "libraryID, "
l_strSQL = l_strSQL & "companyID, "
l_strSQL = l_strSQL & "companyname, "
l_strSQL = l_strSQL & "cdate, "
l_strSQL = l_strSQL & "cuser) VALUES ("
l_strSQL = l_strSQL & "'FILE', "
l_strSQL = l_strSQL & GetData("library", "libraryID", "barcode", "UNALLOCATED") & ", "
l_strSQL = l_strSQL & "1, "
l_strSQL = l_strSQL & "'" & QuoteSanitise(GetData("company", "name", "companyID", 1)) & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'" & g_strUserInitials & "')"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

CreateClip = g_lngLastID

'Record the clip usage
l_strSQL = "INSERT INTO eventusage ("
l_strSQL = l_strSQL & "eventID, dateused) VALUES ("
l_strSQL = l_strSQL & "'" & CreateClip & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "'"
l_strSQL = l_strSQL & ");"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

'Record a History event
l_strSQL = "INSERT INTO eventhistory ("
l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
l_strSQL = l_strSQL & "'" & CreateClip & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
l_strSQL = l_strSQL & "'Created' "
l_strSQL = l_strSQL & ");"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Function
Function SaveClip(Optional lp_blnIgnoreJobID As Boolean, Optional lp_strHistory As String) As Boolean

Dim l_strSQL As String, l_lngClipID As Long, l_strNetworkPath As String, l_lngFilenumber As Long, l_curFileSize As Currency, Count As Long, l_strEpisode As String, l_rstData As ADODB.Recordset
Dim l_strDivaCategory As String

'Check for some valid entries in various boxes

If frmClipControl.lblLibraryID.Caption = "" Then
    MsgBox "Please set the Library barcode of where the clip is stored.", vbCritical, "Saving Clip..."
    SaveClip = False
    Exit Function
End If

If Val(frmClipControl.lblCompanyID.Caption) = 0 Then
    If MsgBox("No owner is defined for this clip. Save?", vbYesNo, "Saving Clip...") = vbNo Then
        SaveClip = False
        Exit Function
    End If
End If

''Check the Clip Purpose, which is a critical companent of the DIVA category
'If frmClipControl.cmbPurpose.Text = "" Then
'    MsgBox "Clip Purpose must be set. Please select a value from the dropdown.", vbCritical, "Saving Clip..."
'    SaveClip = False
'    Exit Function
'End If
'
'Check some things with the filename
If frmClipControl.txtClipfilename.Text = "" Then
    frmClipControl.txtClipfilename.Text = "ZZZZZZ"
    MsgBox "Clip filename has not yet been set - setting to 'ZZZZZZ'." & Chr(13) & "Please re-save once clip filename is known.", vbOKOnly, "Saving Clip..."
ElseIf frmClipControl.chkNotCheckFilenames.Value = 0 And InStr(frmClipControl.txtClipfilename.Text, "&") > 0 Then
    MsgBox "Clip Filename has '&' character in it" & vbCrLf & "This has been found to be bad." & vbCrLf & "Please re-name the file.", vbCritical, "Problem Saving Clip..."
    SaveClip = False
    Exit Function
End If

'Check the JobID
If Not lp_blnIgnoreJobID Then
    'Check for 999999
    If frmClipControl.txtJobID.Text = "999999" Then
        MsgBox "Job ID is not properly set.", vbOKOnly, "Saving Clip..."
        If InStr(GetData("company", "cetaclientcode", "companyID", Val(frmClipControl.lblCompanyID.Caption)), "/clipnojob") = 0 Then
            SaveClip = False
            Exit Function
        End If
    'Check for blank
    ElseIf frmClipControl.txtJobID.Text = "" Then
        Do While frmClipControl.txtJobID.Text = ""
            frmClipControl.txtJobID.Text = InputBox("Please type or scan a jobID", "JobID cannot be blank", "")
        Loop
        'check fro 999999 typed
        If frmClipControl.txtJobID.Text = "999999" Then
            MsgBox "Job ID is not properly set.", vbOKOnly, "Saving Clip..."
            If InStr(GetData("company", "cetaclientcode", "companyID", Val(frmClipControl.lblCompanyID.Caption)), "/clipnojob") = 0 Then
                SaveClip = False
                Exit Function
            End If
        End If
    End If
    'Check for 0
    If frmClipControl.txtJobID.Text = "0" Then
        If MsgBox("JobID is Zero - please confirm this is correct", vbYesNo, "Saving Clip...") = vbNo Then
            SaveClip = False
            Exit Function
        End If
    Else
    'An Actual Number was there
        'Check if the job is started
        If GetStatusNumber(GetData("job", "fd_status", "jobID", Val(frmClipControl.txtJobID.Text))) < 5 Then
            If MsgBox("JobID refers to a Job that hasn't been started yet - please confirm this is correct", vbYesNo, "Saving Clip...") = vbNo Then
                SaveClip = False
                Exit Function
            End If
        End If
        
        'Check the Job owner matched the file owner
        If Val(frmClipControl.lblCompanyID.Caption) <> Val(GetData("job", "companyID", "jobID", Val(frmClipControl.txtJobID.Text))) Then
            If MsgBox("JobID refers to a job that is owned by someone other than the stated File Owner. Is this correct", vbYesNo, "Saving Clip...") = vbNo Then
                SaveClip = False
                Exit Function
            End If
        End If
        
        'Check BBCWW and a JobdetailID that belongs to the same job.
        If Val(GetData("job", "companyID", "jobID", Val(frmClipControl.txtJobID.Text))) = 570 Then
            If Val(frmClipControl.txtJobID.Text) <> GetData("jobdetail", "jobID", "jobdetailID", frmLibrary.txtJobDetailID.Text) Then
                MsgBox "Job is a BBCWW Int Ops job - a valid job detail ID is required.", vbExclamation, "Job Detail ID Required"
                SaveClip = False
                Exit Function
            End If
        End If
        
    End If
End If

'Check the rigorous Titles stuff and the Shine stuff

If InStr(GetData("company", "cetaclientcode", "companyID", Val(frmClipControl.lblCompanyID.Caption)), "/shineversion") > 0 And frmClipControl.cmbFileVersion.Text = "Original Masterfile" Then
    If frmClipControl.cmbVersion.Text = "" Then
        MsgBox "Version must be set on clips for this customer.", vbCritical, "Error..."
        SaveClip = False
        Exit Function
    End If
    If frmClipControl.cmbField1.Text = "" Then
        MsgBox "Technical Category (custom field) must be set on clips for this customer.", vbCritical, "Error..."
        SaveClip = False
        Exit Function
    End If
    If frmClipControl.cmbField2.Text = "" Then
        MsgBox "Category (custom field) must be set on clips for this customer.", vbCritical, "Error..."
        SaveClip = False
        Exit Function
    End If
'    If frmClipControl.cmbField3.Text = "" Then
'        MsgBox "Asset ID (custom field) must be set on clips for this customer.", vbCritical, "Error..."
'        SaveClip = False
'        Exit Function
'    End If
End If

'Check the Rigorous Fields
If g_intEnforceRigorousLibraryChecks <> 0 And InStr(GetData("company", "cetaclientcode", "companyID", Val(frmClipControl.lblCompanyID.Caption)), "/rigorous") > 0 Then
    Dim l_rst As ADODB.Recordset
    Set l_rst = ExecuteSQL("SELECT information FROM xref WHERE description COLLATE Latin1_General_CS_AS = '" & QuoteSanitise(frmClipControl.txtTitle.Text) _
    & "' AND information = " & Val(frmClipControl.lblCompanyID.Caption), g_strExecuteError)
    CheckForSQLError
    If l_rst.RecordCount <= 0 Then
        If MsgBox("Do you wish to define this title as a valid title for this Company?", vbYesNo + vbQuestion, "Title does not match existing list of Titles for " _
        & frmClipControl.cmbCompany.Text) = vbYes Then
            ExecuteSQL "INSERT INTO xref (description, information, category) VALUES ('" & QuoteSanitise(frmClipControl.txtTitle.Text) & "', " _
            & Val(frmClipControl.lblCompanyID.Caption) _
            & ", 'librarytitle');", g_strExecuteError
            CheckForSQLError
        Else
            MsgBox "Please use a Title from the drop down list", vbCritical, "Title filed rigorouxs checking"
            Exit Function
        End If
    End If
    l_rst.Close
    Set l_rst = Nothing
End If

'Check txtClipid = "" means new clip, or a value means modifying existing clip
If Not IsNumeric(frmClipControl.txtClipID.Text) Then
    l_lngClipID = CreateClip
    
    'Set the 'Hidden from web', if the clientcode has the flag set
    If frmClipControl.lblCompanyID.Caption <> "" Then
        If InStr(GetData("company", "cetaclientcode", "companyID", frmClipControl.lblCompanyID.Caption), "/hide") > 0 Then
            frmClipControl.chkHideFromWeb.Value = 1
        End If
    End If
    'Initialise some of the onscreen controls - internalreference and the clipID textbox
    frmClipControl.txtClipID.Text = l_lngClipID
    If frmClipControl.txtInternalReference.Text = "" Then frmClipControl.txtInternalReference.Text = GetNextSequence("internalreference")
    SetData "events", "internalreference", "eventID", l_lngClipID, frmClipControl.txtInternalReference.Text
Else
    l_lngClipID = Val(frmClipControl.txtClipID.Text)
End If

'Check the clip usage, and make an entry for this clip if it doesn't have one
If frmClipControl.lblLastUsed.Caption = "" Then
    'record a usage entry for the new clip
    l_strSQL = "INSERT INTO eventusage ("
    l_strSQL = l_strSQL & "eventID, dateused) VALUES ("
    l_strSQL = l_strSQL & "'" & l_lngClipID & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "'"
    l_strSQL = l_strSQL & ");"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
End If

'Check InternalReference and confirm if different from stored value
If frmClipControl.txtInternalReference.Text <> GetData("events", "internalreference", "eventID", l_lngClipID) Then
    If MsgBox("Internal Reference is different from stored value - Is this Correct?", vbYesNo, "Checking Internal Reference") = vbNo Then
        frmClipControl.txtInternalReference.Text = GetData("events", "internalreference", "eventID", l_lngClipID)
    End If
End If

'Check the Bitrate has only real numbers in it.
If frmClipControl.txtBitrate.Text <> "" Then
    frmClipControl.txtBitrate.Text = Val(frmClipControl.txtBitrate.Text)
    If frmClipControl.txtBitrate.Text = 0 Then
        frmClipControl.txtBitrate.Text = ""
    End If
End If

'Check if the altlocation is blank whether it is in the default folder or not.
If frmClipControl.txtAltFolder.Text = "" And frmClipControl.lblFormat.Caption <> "DIVA" Then
    If MsgBox("Is this clip in the Company Default Folder?)", vbYesNo, "Saving Clip...") = vbYes Then
        frmClipControl.txtAltFolder.Text = frmClipControl.lblCompanyID.Caption
    End If
End If

'Check that the alt location hasn't got a drive letter or a UNC path at the start of it.
If frmClipControl.txtAltFolder.Text Like "\\*" Or Mid(frmClipControl.txtAltFolder.Text, 2, 1) = ":" Then
    MsgBox "Alt Location has either drive letter or UNC network reference. Please fix this", vbOKOnly, "Saving Clip..."
End If

'check numeric or blank job number
'check library client = job client

Dim l_lngJobID As Long
l_lngJobID = Val(frmClipControl.txtJobID.Text)

If frmClipControl.txtJobID.Text <> "999999" Then
    
    If Not ((frmClipControl.txtJobID.Text = "") Or (IsNumeric(frmClipControl.txtJobID.Text))) Then
        MsgBox "Job ID must be numeric or blank", vbExclamation, "Wrong Job ID"
        frmClipControl.txtJobID.SetFocus
        Exit Function
    Else
        
        Dim l_lngJobCompanyID As Long
        Dim l_lngLibraryCompanyID As Long
        Dim l_lngJobCompanyName As String
        l_lngJobCompanyID = GetData("job", "companyID", "jobID", l_lngJobID)
        l_lngJobCompanyName = GetData("job", "companyname", "jobID", l_lngJobID)
        
        l_lngLibraryCompanyID = Val(frmClipControl.lblCompanyID.Caption)
        If l_lngJobCompanyID <> l_lngLibraryCompanyID And l_lngJobID <> 0 Then
            Dim l_intMsg As Integer
            l_intMsg = MsgBox("The job client is '" & l_lngJobCompanyName & "', however, the library media appears to be owned by someone else." & vbCrLf & vbCrLf & "Do you want to change the CLIP record so that the job client is the owner?", vbYesNoCancel + vbQuestion, "Out of sync client")
            Select Case l_intMsg
                Case vbYes
                    'update the company name and ID then save
                    frmClipControl.cmbCompany.Text = l_lngJobCompanyName
                    frmClipControl.lblCompanyID.Caption = l_lngJobCompanyID
                Case vbNo
                    'leave as is and continue with the same
                Case vbCancel
                    'stop the save process
                    Exit Function
            End Select
            
        End If
    End If
End If
    
'Routines to update and verify the file size information
If GetData("library", "format", "libraryID", frmClipControl.lblLibraryID.Caption) = "DISCSTORE" And frmClipControl.txtClipfilename.Text <> "ZZZZZZ" Then
    If frmClipControl.chkNotCheckFilenames.Value = 0 Then
        If VerifyClip(frmClipControl.txtClipID.Text, frmClipControl.txtAltFolder.Text, frmClipControl.txtClipfilename.Text, frmClipControl.lblLibraryID.Caption, False, True) = False Then
            SaveClip = False
            Exit Function
        End If
    End If
Else
    If frmClipControl.txtClipfilename.Text = "ZZZZZZ" Then
        frmClipControl.lblFileSize.Caption = ""
        frmClipControl.lblVerifiedDate.Caption = ""
    End If
End If

'Check Web logging entries

On Error GoTo CARRY_ON
If frmClipControl.adoWebLogging.Recordset.RecordCount > 0 Then
    If (frmClipControl.lblCompanyID.Caption <> frmClipControl.lblOldCompanyID.Caption) Or (frmClipControl.txtReference.Text <> frmClipControl.txtOldReference.Text) Then
        If MsgBox("There are Web Clip Log items for this Clip Reference owned by this Company, and either the Company or the Reference has been changed." & vbCrLf _
        & "Should these Web Logs be updated to the new Company and Reference?", vbYesNo, "Web Logging Items Detected.") = vbYes Then
            frmClipControl.adoWebLogging.Recordset.MoveFirst
            Do While Not frmClipControl.adoWebLogging.Recordset.EOF
                frmClipControl.adoWebLogging.Recordset("companyID") = Val(frmClipControl.lblCompanyID.Caption)
                frmClipControl.adoWebLogging.Recordset("clipreference") = frmClipControl.txtReference.Text
                frmClipControl.adoWebLogging.Recordset.Update
                frmClipControl.adoWebLogging.Recordset.MoveNext
            Loop
        End If
    End If
End If

CARRY_ON:

With frmClipControl

l_strSQL = "UPDATE events SET "
l_strSQL = l_strSQL & "mdate = '" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "muser = '" & g_strUserInitials & "', "
l_strSQL = l_strSQL & "libraryID = '" & .lblLibraryID.Caption & "', "
If .lblSourceLibraryID.Caption <> "" Then
    l_strSQL = l_strSQL & "sourcelibraryID = '" & .lblSourceLibraryID.Caption & "', "
Else
    l_strSQL = l_strSQL & "sourcelibraryID = Null, "
End If
If .txtSourceEventID.Text <> "" Then
    If .chkSourceFromMedia.Value = 0 Then
        l_strSQL = l_strSQL & "sourcetapeeventID = '" & .txtSourceEventID.Text & "', "
        l_strSQL = l_strSQL & "sourceeventID = NULL, "
    Else
        l_strSQL = l_strSQL & "sourceeventID = '" & .txtSourceEventID.Text & "', "
        l_strSQL = l_strSQL & "sourcetapeeventID = NULL, "
    End If
Else
    l_strSQL = l_strSQL & "sourceeventID = Null, "
    l_strSQL = l_strSQL & "sourcetapeeventID = Null, "
End If
l_strSQL = l_strSQL & "sourcefrommedia = " & .chkSourceFromMedia.Value & ", "
If .lblCompanyID.Caption <> "" Then
    l_strSQL = l_strSQL & "companyname = '" & QuoteSanitise(GetData("company", "name", "companyID", .lblCompanyID.Caption)) & "', "
Else
    l_strSQL = l_strSQL & "companyname = '" & GetData("company", "name", "companyID", 1) & "', "
End If
If .lblCompanyID.Caption <> "" Then
    l_strSQL = l_strSQL & "companyID = '" & .lblCompanyID.Caption & "', "
Else
    l_strSQL = l_strSQL & "companyID = 1, "
End If
l_strSQL = l_strSQL & "jobID = '" & .txtJobID.Text & "', "
l_strSQL = l_strSQL & "jobdetailID = '" & .txtJobDetailID.Text & "', "
If .txtSvenskProjectNumber.Text <> "" Then l_strSQL = l_strSQL & "svenskprojectnumber = '" & .txtSvenskProjectNumber.Text & "', " Else l_strSQL = l_strSQL & "svenskprojectnumber = NULL, "
If .txtTitle.Text <> "" Then l_strSQL = l_strSQL & "eventtitle = '" & QuoteSanitise(.txtTitle.Text) & "', " Else l_strSQL = l_strSQL & "eventtitle = NULL, "
If .txtSubtitle.Text <> "" Then l_strSQL = l_strSQL & "eventsubtitle = '" & QuoteSanitise(.txtSubtitle.Text) & "', " Else l_strSQL = l_strSQL & "eventsubtitle = NULL, "
l_strSQL = l_strSQL & "eventseries = '" & QuoteSanitise(.cmbSeries.Text) & "', "
l_strSQL = l_strSQL & "eventset = '" & .cmbSet.Text & "', "
If .cmbEpisode.Text <> "" Then
    If InStr(GetData("company", "cetaclientcode", "companyID", Val(frmClipControl.lblCompanyID.Caption)), "/2digitepnumbers") > 0 Then
        For Count = 1 To Len(.cmbEpisode.Text)
            If Not IsNumeric(Mid(.cmbEpisode.Text, Count, 1)) Then
                Exit For
            End If
        Next
        l_strEpisode = Format(Val(.cmbEpisode.Text), "00")
        If Count <= Len(.cmbEpisode.Text) Then l_strEpisode = l_strEpisode & Mid(.cmbEpisode.Text, Count)
        l_strSQL = l_strSQL & "eventepisode = '" & l_strEpisode & "', "
    Else
        l_strSQL = l_strSQL & "eventepisode = '" & .cmbEpisode.Text & "', "
    End If
Else
    l_strSQL = l_strSQL & "eventepisode = NULL, "
End If
If .cmbVersion.Text <> "" Then l_strSQL = l_strSQL & "eventversion = '" & QuoteSanitise(.cmbVersion.Text) & "', " Else l_strSQL = l_strSQL & "eventversion = NULL, "
If .cmbLanguage.Text <> "" Then l_strSQL = l_strSQL & "language = '" & .cmbLanguage.Text & "', " Else l_strSQL = l_strSQL & "language = NULL, "
l_strSQL = l_strSQL & "vodplatform = '" & .cmbVodPlatform.Text & "', "
l_strSQL = l_strSQL & "clipreference = '" & Trim(QuoteSanitise(.txtReference.Text)) & "', "
l_strSQL = l_strSQL & "fileversion = '" & .cmbFileVersion.Text & "', "
l_strSQL = l_strSQL & "clipformat = '" & .cmbClipformat.Text & "', "
l_strSQL = l_strSQL & "clipcodec = '" & .cmbClipcodec.Text & "', "
l_strSQL = l_strSQL & "ColorSpace = '" & .txtColorSpace.Text & "', "
l_strSQL = l_strSQL & "ChromaSubsampling = '" & .txtChromaSubsampling.Text & "', "
l_strSQL = l_strSQL & "clipaudiocodec = '" & .cmbAudioCodec.Text & "', "
l_strSQL = l_strSQL & "aspectratio = '" & .cmbAspect.Text & "', "
l_strSQL = l_strSQL & "geometriclinearity = '" & .cmbGeometry.Text & "', "
If .txtTapeBarcode.Text <> "" Then l_strSQL = l_strSQL & "Tape_Barcode = '" & .txtTapeBarcode.Text & "', " Else l_strSQL = l_strSQL & "Tape_Barcode = NULL, "
If .txtHorizpixels.Text <> "" Then l_strSQL = l_strSQL & "cliphorizontalpixels = '" & .txtHorizpixels.Text & "', " Else l_strSQL = l_strSQL & "cliphorizontalpixels = NULL, "
If .txtVertpixels.Text <> "" Then l_strSQL = l_strSQL & "clipverticalpixels = '" & .txtVertpixels.Text & "', " Else l_strSQL = l_strSQL & "clipverticalpixels = NULL, "
If .txtBitrate.Text <> "" Then l_strSQL = l_strSQL & "clipbitrate = '" & .txtBitrate.Text & "', " Else l_strSQL = l_strSQL & "clipbitrate = NULL, "
l_strSQL = l_strSQL & "encodepasses = '" & .cmbPasses.Text & "', "
l_strSQL = l_strSQL & "cbrvbr = '" & .cmbCbrVbr.Text & "', "
l_strSQL = l_strSQL & "interlace = '" & .cmbInterlace.Text & "', "
l_strSQL = l_strSQL & "videobitrate = '" & .txtVideoBitrate.Text & "', "
l_strSQL = l_strSQL & "audiobitrate = '" & .txtAudioBitrate.Text & "', "
If .txtTrackCount.Text <> "" Then l_strSQL = l_strSQL & "trackcount = " & Val(.txtTrackCount.Text) & ", " Else l_strSQL = l_strSQL & "trackcount = NULL, "
If .txtAudioChannelCount.Text <> "" Then l_strSQL = l_strSQL & "channelcount = " & Val(.txtAudioChannelCount.Text) & ", " Else l_strSQL = l_strSQL & "channelcount = NULL, "
l_strSQL = l_strSQL & "istextinpicture = '" & .cmbIsTextInPicture.Text & "', "
If .txtSeriesID.Text <> "" Then
    l_strSQL = l_strSQL & "seriesID = '" & .txtSeriesID.Text & "', "
Else
    l_strSQL = l_strSQL & "seriesID = NULL, "
End If
If .txtSerialNumber.Text <> "" Then
    l_strSQL = l_strSQL & "serialnumber = '" & .txtSerialNumber.Text & "', "
Else
    l_strSQL = l_strSQL & "serialnumber = NULL, "
End If
l_strSQL = l_strSQL & "clipfilename = '" & QuoteSanitise(.txtClipfilename.Text) & "', "
'Update Fileszie if it's changed and it's /Superuser
If CheckAccess("/SuperUser", True) Then
    If .txtFileSize.Visible = True Then
        If .txtFileSize.Text <> "" And (.txtFileSize.Text <> "0" Or (.lblVerifiedDate.Caption <> "" And .txtFileSize.Text = "0")) Then
            l_strSQL = l_strSQL & "bigfilesize = " & Format(.txtFileSize.Text, "#") & ", "
        Else
            l_strSQL = l_strSQL & "bigfilesize = Null, "
        End If
    End If
Else
    If .lblFileSize.Caption <> "" And (.lblFileSize.Caption <> "0" Or (.lblVerifiedDate.Caption <> "" And .lblFileSize.Caption = "0")) Then
        l_strSQL = l_strSQL & "bigfilesize = " & Format(.lblFileSize.Caption, "#") & ", "
    Else
        l_strSQL = l_strSQL & "bigfilesize = Null, "
    End If
End If
l_strSQL = l_strSQL & "altlocation = '" & QuoteSanitise(SlashForwardToBack(.txtAltFolder.Text)) & "', "
If .lblVerifiedDate.Caption <> "" Then l_strSQL = l_strSQL & "soundlay = '" & FormatSQLDate(.lblVerifiedDate.Caption) & "', " Else l_strSQL = l_strSQL & "soundlay = NULL, "
If .lblLastMediaInfoQuery.Caption <> "" Then l_strSQL = l_strSQL & "lastmediainfoquery = '" & FormatSQLDate(.lblLastMediaInfoQuery.Caption) & "', " Else l_strSQL = l_strSQL & "lastmediainfoquery = NULL, "
If .lblLastModifiedDate.Caption <> "" Then l_strSQL = l_strSQL & "FileModifiedDate = '" & FormatSQLDate(.lblLastModifiedDate.Caption) & "', " Else l_strSQL = l_strSQL & "FileModifiedDate = NULL, "
l_strSQL = l_strSQL & "timecodestart = '" & .txtTimecodeStart.Text & "', "
l_strSQL = l_strSQL & "timecodestop = '" & .txtTimecodeStop.Text & "', "
If .txtEndCredits.Text <> "00:00:00:00" And .txtEndCredits.Text <> "00:00:00;00" Then
    l_strSQL = l_strSQL & "endcredits = '" & .txtEndCredits.Text & "', "
Else
    l_strSQL = l_strSQL & "endcredits = NULL, "
End If
l_strSQL = l_strSQL & "endcreditselapsed = '" & .txtEndCreditsElapsed.Text & "', "
l_strSQL = l_strSQL & "audiotypegroup1 = '" & .cmbAudioType1.Text & "', "
l_strSQL = l_strSQL & "audiotypegroup2 = '" & .cmbAudioType2.Text & "', "
l_strSQL = l_strSQL & "audiotypegroup3 = '" & .cmbAudioType3.Text & "', "
l_strSQL = l_strSQL & "audiotypegroup4 = '" & .cmbAudioType4.Text & "', "
l_strSQL = l_strSQL & "audiotypegroup5 = '" & .cmbAudioType5.Text & "', "
l_strSQL = l_strSQL & "audiotypegroup6 = '" & .cmbAudioType6.Text & "', "
l_strSQL = l_strSQL & "audiotypegroup7 = '" & .cmbAudioType7.Text & "', "
l_strSQL = l_strSQL & "audiotypegroup8 = '" & .cmbAudioType8.Text & "', "
l_strSQL = l_strSQL & "audiocontentgroup1 = '" & .cmbAudioContent1.Text & "', "
l_strSQL = l_strSQL & "audiocontentgroup2 = '" & .cmbAudioContent2.Text & "', "
l_strSQL = l_strSQL & "audiocontentgroup3 = '" & .cmbAudioContent3.Text & "', "
l_strSQL = l_strSQL & "audiocontentgroup4 = '" & .cmbAudioContent4.Text & "', "
l_strSQL = l_strSQL & "audiocontentgroup5 = '" & .cmbAudioContent5.Text & "', "
l_strSQL = l_strSQL & "audiocontentgroup6 = '" & .cmbAudioContent6.Text & "', "
l_strSQL = l_strSQL & "audiocontentgroup7 = '" & .cmbAudioContent7.Text & "', "
l_strSQL = l_strSQL & "audiocontentgroup8 = '" & .cmbAudioContent8.Text & "', "
l_strSQL = l_strSQL & "audiolanguagegroup1 = '" & .cmbAudioLanguage1.Text & "', "
l_strSQL = l_strSQL & "audiolanguagegroup2 = '" & .cmbAudioLanguage2.Text & "', "
l_strSQL = l_strSQL & "audiolanguagegroup3 = '" & .cmbAudioLanguage3.Text & "', "
l_strSQL = l_strSQL & "audiolanguagegroup4 = '" & .cmbAudioLanguage4.Text & "', "
l_strSQL = l_strSQL & "audiolanguagegroup5 = '" & .cmbAudioLanguage5.Text & "', "
l_strSQL = l_strSQL & "audiolanguagegroup6 = '" & .cmbAudioLanguage6.Text & "', "
l_strSQL = l_strSQL & "audiolanguagegroup7 = '" & .cmbAudioLanguage7.Text & "', "
l_strSQL = l_strSQL & "audiolanguagegroup8 = '" & .cmbAudioLanguage8.Text & "', "
l_strSQL = l_strSQL & "notes = '" & QuoteSanitise(.txtNotes.Text) & "', "
l_strSQL = l_strSQL & "clocknumber = '" & QuoteSanitise(.txtClockNumber.Text) & "', "
l_strSQL = l_strSQL & "fd_length = '" & .txtDuration.Text & "', "
l_strSQL = l_strSQL & "eventtype = '" & QuoteSanitise(.cmbPurpose.Text) & "', "
l_strSQL = l_strSQL & "internalnotes = '" & QuoteSanitise(.txtInternalNotes.Text) & "', "
If .cmbFrameRate.Text <> "" Then l_strSQL = l_strSQL & "clipframerate = '" & .cmbFrameRate.Text & "', " Else l_strSQL = l_strSQL & "clipframerate = NULL, "
l_strSQL = l_strSQL & "clipgenre = '" & QuoteSanitise(.cmbGenre.Text) & "', "
l_strSQL = l_strSQL & "cliptype = '" & .cmbClipType.Text & "', "
l_strSQL = l_strSQL & "eventkeyframefilename = '" & QuoteSanitise(.txtKeyframeFilename.Text) & "', "
l_strSQL = l_strSQL & "eventkeyframetimecode = '" & .txtTimecodeKeyframe.Text & "', "
l_strSQL = l_strSQL & "fourbythreeflag = '" & .chkfourbythreeflag.Value & "', "
l_strSQL = l_strSQL & "sound_stereo_russian = '" & .chkFileLocked.Value & "', "

If .cmbField1.Text <> "" Then l_strSQL = l_strSQL & "customfield1 = '" & QuoteSanitise(.cmbField1.Text) & "', " Else l_strSQL = l_strSQL & "customfield1 = NULL, "
If .cmbField2.Text <> "" Then l_strSQL = l_strSQL & "customfield2 = '" & QuoteSanitise(.cmbField2.Text) & "', " Else l_strSQL = l_strSQL & "customfield2 = NULL, "
If .cmbField3.Text <> "" Then l_strSQL = l_strSQL & "customfield3 = '" & QuoteSanitise(.cmbField3.Text) & "', " Else l_strSQL = l_strSQL & "customfield3 = NULL, "
If .cmbField4.Text <> "" Then l_strSQL = l_strSQL & "customfield4 = '" & QuoteSanitise(.cmbField4.Text) & "', " Else l_strSQL = l_strSQL & "customfield4 = NULL, "
If .cmbField5.Text <> "" Then l_strSQL = l_strSQL & "customfield5 = '" & QuoteSanitise(.cmbField5.Text) & "', " Else l_strSQL = l_strSQL & "customfield5 = NULL, "
If .cmbField6.Text <> "" Then l_strSQL = l_strSQL & "customfield6 = '" & QuoteSanitise(.cmbField6.Text) & "', " Else l_strSQL = l_strSQL & "customfield6 = NULL, "
l_strSQL = l_strSQL & "internalreference = '" & .txtInternalReference.Text & "', "
l_strSQL = l_strSQL & "mediastreamtype = '" & .cmbStreamType.Text & "', "
l_strSQL = l_strSQL & "clipstoreID = " & IIf(.chkTextlessPresent.Value <> 0, "1, ", "Null, ")
If .txtSHA1Checksum.Text <> "" Then l_strSQL = l_strSQL & "sha1checksum = '" & .txtSHA1Checksum.Text & "', " Else l_strSQL = l_strSQL & "sha1checksum = Null, "
If .txtMD5Checksum.Text <> "" Then l_strSQL = l_strSQL & "md5checksum = '" & .txtMD5Checksum.Text & "', " Else l_strSQL = l_strSQL & "md5checksum = Null, "
l_strSQL = l_strSQL & "DADCForcedSubtitle = '" & .cmbForcedSubtitle.Text & "', "
l_strSQL = l_strSQL & "DADCPictureFormat = '" & .cmbPictFormat.Text & "', "
l_strSQL = l_strSQL & "DADCActiveRatio = '" & .cmbActiveRatio.Text & "', "
l_strSQL = l_strSQL & "TextInPictureLanguage = '" & .cmbTextInPictureLanguage.Text & "', "
l_strSQL = l_strSQL & "BurnedInSubtitleLanguage = '" & .cmbBurnedInSubtitleLanguage.Text & "', "
l_strSQL = l_strSQL & "PipelineUnit = '" & .cmbPipelineUnit.Text & "', "
l_strSQL = l_strSQL & "EncodeReplayDeck = '" & .cmbEncodeReplayDeck.Text & "', "
l_strSQL = l_strSQL & "PipelineAudioTracks = '" & .cmbAudioTracks.Text & "', "
l_strSQL = l_strSQL & "PipelineAudioBitDepth = '" & .cmbAudioTrackDepth.Text & "', "

If Val(.lblLibraryID.Caption) = 284349 Or Val(.lblLibraryID.Caption) = 248095 Then
    l_strSQL = l_strSQL & "MediaWindowDeleteDate = Null, "
    l_strSQL = l_strSQL & "hidefromweb = '0' "
Else
    If .chkHideFromWeb.Value = 0 Then l_strSQL = l_strSQL & "MediaWindowDeleteDate = Null, "
    l_strSQL = l_strSQL & "hidefromweb = '" & .chkHideFromWeb.Value & "' "
End If

'Finally we finish the save
l_strSQL = l_strSQL & "WHERE eventID = " & l_lngClipID & ";"

End With

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

Dim l_strTemp As String
l_strTemp = GetData("library", "format", "libraryID", Val(frmClipControl.lblLibraryID.Caption))
If l_strTemp = "DISCSTORE" Or l_strTemp = "PLAYLISTS" Then
    SetData "events", "online", "eventID", l_lngClipID, 1
Else
    SetData "events", "online", "eventID", l_lngClipID, 0
End If

''Display the DIVA Category
'Set l_rstData = ExecuteSQL("exec cet_Get_DIVA_Category @EventID=" & l_lngClipID, g_strExecuteError)
'CheckForSQLError
'l_strDivaCategory = l_rstData(0)
'l_rstData.Close
'If l_strDivaCategory <> "UNDEFINED" Then
'    SetData "events", "clipsoundformat", "eventID", l_lngClipID, l_strDivaCategory
'Else
'    SetData "events", "clipsoundformat", "eventID", l_lngClipID, l_strDivaCategory
'    MsgBox "Diva Category is UNDEFINED. Please select a valid Clip Purpose and a valid clip owner.", vbCritical, "Error"
'    SaveClip = False
'    Exit Function
'End If
'
'Record a History event
l_strSQL = "INSERT INTO eventhistory ("
l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
l_strSQL = l_strSQL & "'" & l_lngClipID & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
If Not IsEmpty(lp_strHistory) Then
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strHistory) & "' "
Else
    l_strSQL = l_strSQL & "'Clip Saved' "
End If
l_strSQL = l_strSQL & ");"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

SaveClip = True

End Function
Sub ShowClipControl(lp_lngClipID As Long)

If Not CheckAccess("/showlibrary") Then Exit Sub

Dim l_strSQL As String

g_blnFrameAccurateClipDurations = False

l_strSQL = "SELECT TOP 1 * FROM events WHERE eventID = '" & lp_lngClipID & "';"

Dim l_rstClip As New ADODB.Recordset, l_rstData As ADODB.Recordset
Set l_rstClip = ExecuteSQL(l_strSQL, g_strExecuteError)
Dim l_rstUsage As New ADODB.Recordset

Set l_rstUsage = ExecuteSQL("Select TOP 1 * from eventusage WHERE eventID = '" & lp_lngClipID & "' ORDER by dateused DESC;", g_strExecuteError)

If Not l_rstClip.EOF Then
    'Load up the information for that clip
    With frmClipControl
        .txtClipID.Text = lp_lngClipID
        
        'Display the Internal refence, and if it is not set make some efforts to set if from parent information.
        'If parent information fails, then set it to the next in sequence, storing away the values everywhere relevant.
        
        .txtInternalReference.Text = Trim(" " & l_rstClip("internalreference"))
            
        If Not IsNull(l_rstClip("libraryID")) Then
            .lblLibraryID.Caption = l_rstClip("libraryID")
            .cmbBarcode.Text = GetData("library", "barcode", "libraryID", l_rstClip("libraryID"))
            .lblFormat.Caption = GetData("library", "format", "libraryID", l_rstClip("libraryID"))
        Else
            .lblLibraryID.Caption = ""
            .cmbBarcode.Text = ""
            .lblFormat.Caption = ""
        End If
        
        'Set the company related fields and the title combo
        .cmbCompany.Text = Trim(" " & l_rstClip("companyname"))
        .lblCompanyID.Caption = Trim(" " & l_rstClip("companyID"))
        .lblOldCompanyID.Caption = Trim(" " & l_rstClip("companyID"))
        
        'Set the label for if it is a deletd clip
        If l_rstClip("system_deleted") = 1 Then
            .lblDeletedClip.Visible = True
            .cmdDelete.Picture = LoadPicture(App.Path & "\Undelete_file.gif")
        Else
            .lblDeletedClip.Visible = False
            .cmdDelete.Picture = LoadPicture(App.Path & "\Delete_File.gif")
        End If
    
        'Check the frameaccurate thing and set the global variable g_blnFrameAccurateClipDurations appropriately.
        If InStr(Trim(" " & GetData("company", "cetaclientcode", "companyID", Val(.lblCompanyID.Caption))), "/noframe") <> 0 Then
            g_blnFrameAccurateClipDurations = False
        Else
            g_blnFrameAccurateClipDurations = True
        End If
        .txtSHA1Checksum.Text = Trim(" " & l_rstClip("sha1checksum"))
        .txtMD5Checksum.Text = Trim(" " & l_rstClip("md5checksum"))
        .txtJobID.Text = Trim(" " & l_rstClip("jobID"))
        .txtJobDetailID.Text = Trim(" " & l_rstClip("jobdetailID"))
        .txtSvenskProjectNumber.Text = Trim(" " & l_rstClip("svenskprojectnumber"))
        .txtTitle.Text = Trim(" " & l_rstClip("eventtitle"))
        .txtSubtitle.Text = Trim(" " & l_rstClip("eventsubtitle"))
        .cmbSeries.Text = Trim(" " & l_rstClip("eventseries"))
        .cmbSet.Text = Trim(" " & l_rstClip("eventset"))
        .cmbEpisode.Text = Trim(" " & l_rstClip("eventepisode"))
        .cmbVersion.Text = Trim(" " & l_rstClip("eventversion"))
        .txtReference.Text = Trim(" " & l_rstClip("clipreference"))
        .txtTapeBarcode.Text = Trim(" " & l_rstClip("Tape_Barcode"))
        .txtOldReference.Text = Trim(" " & l_rstClip("clipreference"))
        .cmbFileVersion.Text = Trim(" " & l_rstClip("fileversion"))
        .cmbClipformat.Text = Trim(" " & l_rstClip("clipformat"))
        .cmbAspect.Text = Trim(" " & l_rstClip("aspectratio"))
        .cmbGeometry.Text = Trim(" " & l_rstClip("geometriclinearity"))
        .cmbClipcodec.Text = Trim(" " & l_rstClip("clipcodec"))
        .txtColorSpace.Text = Trim(" " & l_rstClip("ColorSpace"))
        .txtChromaSubsampling.Text = Trim(" " & l_rstClip("ChromaSubsampling"))
        .cmbPasses.Text = Trim(" " & l_rstClip("encodepasses"))
        .cmbCbrVbr.Text = Trim(" " & l_rstClip("cbrvbr"))
        .cmbInterlace.Text = Trim(" " & l_rstClip("interlace"))
        .cmbAudioCodec.Text = Trim(" " & l_rstClip("clipaudiocodec"))
        .cmbPurpose.Text = Trim(" " & l_rstClip("eventtype"))
        .txtHorizpixels.Text = Trim(" " & l_rstClip("cliphorizontalpixels"))
        .txtVertpixels.Text = Trim(" " & l_rstClip("clipverticalpixels"))
        .cmbFrameRate.Text = Trim(" " & l_rstClip("clipframerate"))
        .txtBitrate.Text = Trim(" " & l_rstClip("clipbitrate"))
        .txtAudioBitrate.Text = Trim(" " & l_rstClip("audiobitrate"))
        .txtTrackCount.Text = Trim(" " & l_rstClip("trackcount"))
        .txtAudioChannelCount.Text = Trim(" " & l_rstClip("channelcount"))
        .txtVideoBitrate.Text = Trim(" " & l_rstClip("videobitrate"))
        .txtClipfilename.Text = Trim(" " & l_rstClip("clipfilename"))
        .lblFileSize.Caption = Format(Trim(" " & l_rstClip("bigfilesize")), "#,#")
        .txtFileSize.Text = Format(Trim(" " & l_rstClip("bigfilesize")), "#,#")
        If .lblFileSize.Caption = "" Then
            .lblFileUnverified.Visible = True
        Else
            .lblFileUnverified.Visible = False
        End If
        .lblLastModifiedDate.Caption = Format(l_rstClip("FileModifiedDate"), "dd MMM yyyy HH:NN:SS")
        .cmbIsTextInPicture.Text = Trim(" " & l_rstClip("istextinpicture"))
        .txtSeriesID.Text = Trim(" " & l_rstClip("seriesID"))
        .txtSerialNumber.Text = Trim(" " & l_rstClip("serialnumber"))
        .txtTitle.Columns("seriesID").Text = Trim(" " & l_rstClip("seriesID"))
        .txtTitle.Columns("companyID").Text = .lblCompanyID.Caption
        If Not IsNull(l_rstClip("soundlay")) And l_rstClip("soundlay") <> "" Then .lblVerifiedDate.Caption = Format(l_rstClip("soundlay"), "dd MMM yyyy HH:NN:SS") Else .lblVerifiedDate.Caption = ""
        If Not IsNull(l_rstClip("lastmediainfoquery")) And l_rstClip("lastmediainfoquery") <> "" Then .lblLastMediaInfoQuery.Caption = Format(l_rstClip("lastmediainfoquery"), "dd MMM yyyy HH:NN:SS") Else .lblLastMediaInfoQuery.Caption = ""
        .chkSourceFromMedia.Value = GetFlag(Trim(" " & l_rstClip("sourcefrommedia")))
        If .chkSourceFromMedia.Value <> 0 Then
            .txtSourceEventID.Text = Trim(" " & l_rstClip("sourceeventID"))
            .lblSourceLibraryID.Caption = Trim(" " & GetData("events", "libraryID", "eventID", Val(.txtSourceEventID.Text), True))
            .txtSourcebarcode.Text = Trim(" " & GetData("library", "barcode", "libraryID", Val(.lblSourceLibraryID.Caption)))
            .lblSourceFormat.Caption = Trim(" " & GetData("library", "format", "libraryID", Val(.lblSourceLibraryID.Caption)))
        Else
            .txtSourceEventID.Text = Trim(" " & l_rstClip("sourcetapeeventID"))
            .lblSourceLibraryID.Caption = Trim(" " & l_rstClip("sourcelibraryID"))
            If .lblSourceLibraryID.Caption = "" Then
                .lblSourceLibraryID.Caption = GetData("tapeevents", "libraryID", "eventID", Val(.txtSourceEventID.Text))
            End If
            If .lblSourceLibraryID.Caption <> "" Then
                .txtSourcebarcode.Text = GetData("library", "barcode", "libraryID", Val(.lblSourceLibraryID.Caption))
                .lblSourceFormat.Caption = GetData("library", "format", "libraryID", Val(.lblSourceLibraryID.Caption))
            End If
        End If
        If .txtSourceEventID.Text = "" And .chkSourceFromMedia.Value <> 0 Then
            .lblSourceLibraryID.Caption = ""
            .txtSourcebarcode.Text = ""
            .lblSourceFormat.Caption = ""
        End If
        
        .txtTimecodeStart.Text = Trim(" " & l_rstClip("timecodestart"))
        .txtTimecodeStop.Text = Trim(" " & l_rstClip("timecodestop"))
        .txtDuration.Text = Trim(" " & l_rstClip("fd_length"))
        .txtEndCredits.Text = Trim(" " & l_rstClip("endcredits"))
        .txtEndCreditsElapsed.Text = Trim(" " & l_rstClip("endcreditselapsed"))
        .cmbAudioType1.Text = Trim(" " & l_rstClip("audiotypegroup1"))
        .cmbAudioType2.Text = Trim(" " & l_rstClip("audiotypegroup2"))
        .cmbAudioType3.Text = Trim(" " & l_rstClip("audiotypegroup3"))
        .cmbAudioType4.Text = Trim(" " & l_rstClip("audiotypegroup4"))
        .cmbAudioType5.Text = Trim(" " & l_rstClip("audiotypegroup5"))
        .cmbAudioType6.Text = Trim(" " & l_rstClip("audiotypegroup6"))
        .cmbAudioType7.Text = Trim(" " & l_rstClip("audiotypegroup7"))
        .cmbAudioType8.Text = Trim(" " & l_rstClip("audiotypegroup8"))
        .cmbAudioContent1.Text = Trim(" " & l_rstClip("audiocontentgroup1"))
        .cmbAudioContent2.Text = Trim(" " & l_rstClip("audiocontentgroup2"))
        .cmbAudioContent3.Text = Trim(" " & l_rstClip("audiocontentgroup3"))
        .cmbAudioContent4.Text = Trim(" " & l_rstClip("audiocontentgroup4"))
        .cmbAudioContent5.Text = Trim(" " & l_rstClip("audiocontentgroup5"))
        .cmbAudioContent6.Text = Trim(" " & l_rstClip("audiocontentgroup6"))
        .cmbAudioContent7.Text = Trim(" " & l_rstClip("audiocontentgroup7"))
        .cmbAudioContent8.Text = Trim(" " & l_rstClip("audiocontentgroup8"))
        .cmbAudioLanguage1.Text = Trim(" " & l_rstClip("audiolanguagegroup1"))
        .cmbAudioLanguage2.Text = Trim(" " & l_rstClip("audiolanguagegroup2"))
        .cmbAudioLanguage3.Text = Trim(" " & l_rstClip("audiolanguagegroup3"))
        .cmbAudioLanguage4.Text = Trim(" " & l_rstClip("audiolanguagegroup4"))
        .cmbAudioLanguage5.Text = Trim(" " & l_rstClip("audiolanguagegroup5"))
        .cmbAudioLanguage6.Text = Trim(" " & l_rstClip("audiolanguagegroup6"))
        .cmbAudioLanguage7.Text = Trim(" " & l_rstClip("audiolanguagegroup7"))
        .cmbAudioLanguage8.Text = Trim(" " & l_rstClip("audiolanguagegroup8"))
        .cmbLanguage.Text = Trim(" " & l_rstClip("language"))
        .cmbVodPlatform.Text = Trim(" " & l_rstClip("vodplatform"))
        If Not IsNull(l_rstClip("online")) Then
            .chkOnline.Value = Val(l_rstClip("online"))
        End If
        If Not IsNull(l_rstClip("fourbythreeflag")) Then
            .chkfourbythreeflag.Value = Val(l_rstClip("fourbythreeflag"))
        End If
        If Not IsNull(l_rstClip("hidefromweb")) Then
            .chkHideFromWeb.Value = GetFlag(Val(l_rstClip("hidefromweb")))
        End If
        If Not IsNull(l_rstClip("sound_stereo_russian")) Then
            .chkFileLocked.Value = GetFlag(Val(l_rstClip("sound_stereo_russian")))
        End If
        
        .chkTextlessPresent.Value = GetFlag(l_rstClip("clipstoreID"))
        
        .txtAltFolder.Text = Trim(" " & l_rstClip("altlocation"))
        .txtNotes.Text = Trim(" " & l_rstClip("notes"))
        .txtInternalNotes.Text = Trim(" " & l_rstClip("internalnotes"))
        .cmbGenre.Text = Trim(" " & l_rstClip("clipgenre"))
        .cmbClipType.Text = Trim(" " & l_rstClip("cliptype"))
        .txtClockNumber.Text = Trim(" " & l_rstClip("clocknumber"))
        .cmbPictFormat.Text = Trim(" " & l_rstClip("DADCPictureFormat"))
        .cmbForcedSubtitle.Text = Trim(" " & l_rstClip("DADCForcedSubtitle"))
        .cmbActiveRatio.Text = Trim(" " & l_rstClip("DADCActiveRatio"))
        .cmbTextInPictureLanguage.Text = Trim(" " & l_rstClip("TextInPictureLanguage"))
        .cmbBurnedInSubtitleLanguage.Text = Trim(" " & l_rstClip("BurnedInSubtitleLanguage"))
        .cmbPipelineUnit.Text = Trim(" " & l_rstClip("PipelineUnit"))
        .cmbEncodeReplayDeck.Text = Trim(" " & l_rstClip("EncodeReplayDeck"))
        .cmbAudioTracks.Text = Trim(" " & l_rstClip("PipelineAudioTracks"))
        .cmbAudioTrackDepth.Text = Trim(" " & l_rstClip("PipelineAudioBitDepth"))
        
        If Not l_rstUsage.EOF Then
            .lblLastUsed.Caption = Format(l_rstUsage("dateused"), "dd MMM yyyy HH:NN:SS")
        Else
            .lblLastUsed.Caption = ""
        End If
        
        .txtTimecodeKeyframe.Text = Trim(" " & l_rstClip("eventkeyframetimecode"))
        .txtKeyframeFilename.Text = Trim(" " & l_rstClip("eventkeyframefilename"))
        .cmbStreamType.Text = Trim(" " & l_rstClip("mediastreamtype"))
        
        DoEvents
        
        'Try and Display the Keyframe, if the information in the database actually points to a file.
        If .txtKeyframeFilename.Text <> "" Then
            Dim l_strNetworkPath As String
            l_strNetworkPath = g_strKeyframeStore
            l_strNetworkPath = l_strNetworkPath & "\" & .lblCompanyID.Caption

            l_strNetworkPath = l_strNetworkPath & "\" & .txtKeyframeFilename.Text

            On Error GoTo EMPTYKEYFRAME

            If Dir(l_strNetworkPath) = .txtKeyframeFilename.Text Then
                If Right(.txtKeyframeFilename.Text, 4) = ".png" Then
                    PngPictureLoad l_strNetworkPath, .picKeyframe, True
                    .picKeyframe.Refresh
                    .picKeyframe.Visible = True
                Else
                    .picKeyframe.Picture = LoadPicture(l_strNetworkPath)
                    .picKeyframe.Refresh
                    .picKeyframe.Visible = True
                End If
            Else
EMPTYKEYFRAME:
                .picKeyframe.Width = 4845
                .picKeyframe.Picture = LoadPicture(App.Path & "\Empty.gif")
                .picKeyframe.Refresh
                .picKeyframe.Visible = False
            End If
            On Error GoTo 0
        Else
            On Error Resume Next
            .picKeyframe.Width = 4845
            .picKeyframe.Picture = LoadPicture(App.Path & "\Empty.gif")
            .picKeyframe.Refresh
            .picKeyframe.Visible = False
        End If
        On Error GoTo 0
    
        'Try and load up the Custom Field Labels and dropdowns, and display the custom fields.
        'Custom field labels are stored as part of the company table, for the owner of the clip.
        Dim l_strTempLabel As String, l_lngCompanyID As Long
        
        l_lngCompanyID = Val(.lblCompanyID.Caption)
        
        If l_lngCompanyID <> 0 Then
    
            Dim l_conSearch As ADODB.Connection
            Dim l_rstSearch2 As ADODB.Recordset
            
            Set l_conSearch = New ADODB.Connection
            Set l_rstSearch2 = New ADODB.Recordset
            
            l_conSearch.ConnectionString = g_strConnection
            l_conSearch.Open
            
            l_strSQL = "SELECT * FROM masterseriestitle WHERE companyID = " & l_lngCompanyID & " ORDER BY title;"
            
            With l_rstSearch2
                .CursorLocation = adUseClient
                .LockType = adLockBatchOptimistic
                .CursorType = adOpenDynamic
                .Open l_strSQL, l_conSearch, adOpenDynamic
            End With
            
            l_rstSearch2.ActiveConnection = Nothing
            
            Set .txtTitle.DataSourceList = l_rstSearch2
            
            l_conSearch.Close
            Set l_conSearch = Nothing
            
            .lblField1.Caption = Trim(" " & GetData("company", "customfield1label", "companyID", l_lngCompanyID))
            If .lblField1.Caption = "" Then
                .lblField1.Visible = False
                .cmbField1.Visible = False
            Else
                .lblField1.Visible = True
                .cmbField1.Visible = True
                PopulateCustomFieldCombo l_lngCompanyID, 1, .cmbField1
            End If
            .lblField2.Caption = Trim(" " & GetData("company", "customfield2label", "companyID", l_lngCompanyID))
            If .lblField2.Caption = "" Then
                .lblField2.Visible = False
                .cmbField2.Visible = False
            Else
                .lblField2.Visible = True
                .cmbField2.Visible = True
                PopulateCustomFieldCombo l_lngCompanyID, 2, .cmbField2
            End If
            .lblField3.Caption = Trim(" " & GetData("company", "customfield3label", "companyID", l_lngCompanyID))
            If .lblField3.Caption = "" Then
                .lblField3.Visible = False
                .cmbField3.Visible = False
            Else
                .lblField3.Visible = True
                .cmbField3.Visible = True
                PopulateCustomFieldCombo l_lngCompanyID, 3, .cmbField3
            End If
            .lblField4.Caption = Trim(" " & GetData("company", "customfield4label", "companyID", l_lngCompanyID))
            If .lblField4.Caption = "" Then
                .lblField4.Visible = False
                .cmbField4.Visible = False
            Else
                .lblField4.Visible = True
                .cmbField4.Visible = True
                PopulateCustomFieldCombo l_lngCompanyID, 4, .cmbField4
            End If
            .lblField5.Caption = Trim(" " & GetData("company", "customfield5label", "companyID", l_lngCompanyID))
            If .lblField5.Caption = "" Then
                .lblField5.Visible = False
                .cmbField5.Visible = False
            Else
                .lblField5.Visible = True
                .cmbField5.Visible = True
                PopulateCustomFieldCombo l_lngCompanyID, 5, .cmbField5
            End If
            .lblField6.Caption = Trim(" " & GetData("company", "customfield6label", "companyID", l_lngCompanyID))
            If .lblField6.Caption = "" Then
                .lblField6.Visible = False
                .cmbField6.Visible = False
            Else
                .lblField6.Visible = True
                .cmbField6.Visible = True
                PopulateCustomFieldCombo l_lngCompanyID, 6, .cmbField6
            End If
    
        End If
        
        .cmbField1.Text = Trim(" " & l_rstClip("customfield1"))
        .cmbField2.Text = Trim(" " & l_rstClip("customfield2"))
        .cmbField3.Text = Trim(" " & l_rstClip("customfield3"))
        .cmbField4.Text = Trim(" " & l_rstClip("customfield4"))
        .cmbField5.Text = Trim(" " & l_rstClip("customfield5"))
        .cmbField6.Text = Trim(" " & l_rstClip("customfield6"))
                
        'Check the internal refernce is set, and try and set it from parent tape/clip information if it isn't.
        
        If Val(.txtInternalReference.Text) = 0 Then
            'Clip has no internal refernce - try and set it from the clip's parent
            Dim l_lngParentEventID As Long, l_lngParentTapeID As Long, l_varParentEventMediaType As Variant
            l_lngParentEventID = Val(.txtSourceEventID.Text)
            If l_lngParentEventID <> 0 Then
                l_varParentEventMediaType = GetData("events", "eventmediatype", "eventID", l_lngParentEventID)
                If Not IsNull(l_varParentEventMediaType) And l_varParentEventMediaType = "FILE" Then
                    .txtInternalReference.Text = Trim(" " & GetData("events", "internalreference", "eventID", l_lngParentEventID))
                    If Val(.txtInternalReference.Text) = 0 Then
                        .txtInternalReference.Text = GetNextSequence("internalreference")
                        SetData "events", "internalreference", "eventID", l_lngParentEventID, Val(.txtInternalReference.Text)
                        If IsNull(GetData("events", "clipreference", "eventID", l_lngParentEventID)) Or GetData("events", "clipreference", "eventID", l_lngParentEventID) = "" Then
                            SetData "events", "clipreference", "eventID", l_lngParentEventID, Val(.txtInternalReference.Text)
                        End If
                    End If
                    SetData "events", "internalreference", "eventID", lp_lngClipID, Val(.txtInternalReference.Text)
                Else
                    l_lngParentTapeID = Val(.lblSourceLibraryID.Caption)
                    If l_lngParentTapeID <> 0 Then
                        .txtInternalReference.Text = Trim(" " & GetData("library", "internalreference", "libraryID", l_lngParentTapeID))
                        If Val(.txtInternalReference.Text) = 0 And GetData("library", "format", "libraryID", l_lngParentTapeID) <> "DISCSTORE" Then
                            .txtInternalReference.Text = GetNextSequence("internalreference")
                            SetData "library", "internalreference", "libraryID", l_lngParentTapeID, Val(.txtInternalReference.Text)
                        End If
                        SetData "events", "internalreference", "eventID", lp_lngClipID, Val(.txtInternalReference.Text)
                    End If
                End If
            End If
        End If
        
        If Val(.txtInternalReference.Text) = 0 Then
            .txtInternalReference.Text = GetNextSequence("internalreference")
            SetData "events", "internalreference", "eventID", lp_lngClipID, Val(.txtInternalReference.Text)
        End If
        
        If IsNull(GetData("events", "clipreference", "eventID", lp_lngClipID)) Or GetData("events", "clipreference", "eventID", lp_lngClipID) = "" Then
            SetData "events", "clipreference", "eventID", lp_lngClipID, Val(.txtInternalReference.Text)
            .txtReference.Text = .txtInternalReference.Text
        End If
            
        'Load the FFProbe Data
        l_strSQL = "SELECT * FROM eventchannelmap WHERE eventID = " & lp_lngClipID & " ORDER BY cdate desc;"
        .adoFFProbe.RecordSource = l_strSQL
        .adoFFProbe.ConnectionString = g_strConnection
        .adoFFProbe.Refresh
        
        'Load the clip logging
        l_strSQL = "SELECT * FROM eventlogging WHERE eventID = " & lp_lngClipID & " ORDER BY timecodestart ASC;"
        .adoLogging.RecordSource = l_strSQL
        .adoLogging.ConnectionString = g_strConnection
        .adoLogging.Refresh

        'check if Tech Review exists
        
        If Trim(" " & .txtClipfilename.Text) <> "" And Trim(" " & .lblFileSize.Caption) <> "" And Trim(" " & .txtMD5Checksum.Text) <> "" Then
            If GetDataSQL("SELECT TOP 1 techrevID FROM techrev WHERE system_deleted = 0 AND companyID = " & Val(.lblCompanyID.Caption) & " AND filename = '" & .txtClipfilename.Text & "' AND bigfilesize = " & Format(.lblFileSize.Caption, "#") & " AND md5checksum = '" & .txtMD5Checksum.Text & "';") <> 0 Then
                .lblTechReviewAvalable.Visible = True
            Else
                .lblTechReviewAvalable.Visible = False
            End If
        ElseIf GetData("techrev", "techrevID", "eventID", lp_lngClipID) <> 0 Then
            .lblTechReviewAvalable.Visible = True
        Else
            .lblTechReviewAvalable.Visible = False
        End If
        
        If InStr(GetData("company", "cetaclientcode", "companyID", .lblCompanyID.Caption), "/SPEUploadPage") > 0 Then
            .lblCaption(66).Caption = "Alpha ID"
        ElseIf InStr(GetData("company", "cetaclientcode", "companyID", Val(.lblCompanyID.Caption)), "/shineversion") > 0 Then
            .lblCaption(66).Caption = "Title Code"
        Else
            .lblCaption(66).Caption = "Series ID"
        End If
        
        'Grey out XML buttons if company flags are not set, and DIVA buttons if not on right stores
        If g_blnRedNetwork = True Then
            If InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/GoogleTracker") > 0 Then
                .cmdGoogleEpisodeXML.Enabled = True
                .cmdGoogleSeasonXML.Enabled = True
                .cmdGoogleShowXML.Enabled = True
            Else
                .cmdGoogleEpisodeXML.Enabled = False
                .cmdGoogleSeasonXML.Enabled = False
                .cmdGoogleShowXML.Enabled = False
            End If
            If l_lngCompanyID = 769 Or l_lngCompanyID = 131 Then
                .cmdESIDL3XMLFile.Enabled = True
            Else
                .cmdESIDL3XMLFile.Enabled = False
            End If
        ElseIf .lblLibraryID.Caption = 796559 Then
            .cmdVisualQuery.Enabled = True
            .cmdMediaInfo.Enabled = True
            .cmdMediaInfoPreserveTimecode.Enabled = True
        Else
            .cmdGoogleEpisodeXML.Enabled = False
            .cmdGoogleSeasonXML.Enabled = False
            .cmdGoogleShowXML.Enabled = False
            .cmdVisualQuery.Enabled = False
            .cmdMediaInfo.Enabled = False
            .cmdMediaInfoPreserveTimecode.Enabled = False
        End If
        If .lblLibraryID.Caption = 744815 And GetCount("SELECT Count(*) from Wasabi_Bucket where companyID = " & .lblCompanyID.Caption) > 0 Then
            .cmdRestoreFromWasabi.Enabled = True
        Else
            .cmdRestoreFromWasabi.Enabled = False
        End If
    
        If .lblLibraryID.Caption = 659178 Then
            .cmdRestoreFromDIVA.Enabled = True
        Else
            .cmdRestoreFromDIVA.Enabled = False
        End If
    
        If InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/dadctracker") > 0 Then
            If GetCETASetting("DADCUpdateInProgress") <> "1" Then
                .lblAudioConfiguration.Caption = Replace(Trim(" " & GetDataSQL("SELECT TOP 1 audioconfiguration FROM tracker_dadc_item WHERE itemreference = '" & .txtReference.Text & "' AND readytobill = 0;")), "&", "&&")
                .lblAudioConfiguration.Visible = True
            Else
                .lblAudioConfiguration.Caption = "The DADC Tracker is currently being updated - please reload this clip later to see this information"
                .lblAudioConfiguration.Visible = True
            End If
            .lblDADCItems.Visible = True
        Else
            .lblAudioConfiguration.Visible = False
            .lblDADCItems.Visible = False
        End If
        
        If .cmbClipformat.Text = "Folder" Then
            .cmdRestoreFromWasabi.Enabled = False
        End If
        
        If l_lngCompanyID = 1261 Then
            If .txtClockNumber.Text = "" Then
                If GetData("tracker_dadc_item", "UID", "itemreference", .txtReference.Text) <> "" Then
                    .txtClockNumber.Text = GetData("tracker_dadc_item", "UID", "itemreference", .txtReference.Text)
                End If
            End If
        End If

    End With
Else
    If lp_lngClipID <> 0 Then MsgBox "Clip:" & lp_lngClipID & " Not Found.", vbCritical, "Error..."
    If frmClipControl.txtClipID.Text = "" Then
    
        ClearFields frmClipControl
        frmClipControl.lblDeletedClip.Visible = False
        frmClipControl.cmdDelete.Picture = LoadPicture(App.Path & "\Delete_File.gif")
        
        With frmClipControl
            
            'Load up the keywords
            .adoKeywords.ConnectionString = g_strConnection
            l_strSQL = "SELECT eventID, keywordtext "
            l_strSQL = l_strSQL & " FROM eventkeyword "
            l_strSQL = l_strSQL & " WHERE eventID = -1;"
            .adoKeywords.RecordSource = l_strSQL
            .adoKeywords.Refresh
            
            .adoKeyword.ConnectionString = g_strConnection
            .adoKeyword.RecordSource = "SELECT keywordtext, keywordID FROM keyword WHERE companyID = -1 ORDER BY keywordtext;"
            .adoKeyword.Refresh
            .grdKeyword.Columns("keywordtext").DropDownHwnd = 0
            
            .adoClientKeywords.ConnectionString = g_strConnection
            .adoClientKeywords.RecordSource = "SELECT keywordtext, companyID from keyword WHERE companyID = -1;"
            .adoClientKeywords.Refresh
            
            'Set the Portal Permission Stuff
            
            .adoPortalPermission.ConnectionString = g_strConnection
            .adoPortalPermission.RecordSource = "SELECT * FROM portalpermission WHERE eventID = -1 ORDER BY fullname;"
            .adoPortalPermission.Refresh
            
            .adoPortalUsers.ConnectionString = g_strConnection
            .adoPortalUsers.RecordSource = "SELECT * from portaluser WHERE companyID = -1;"
            .adoPortalUsers.Refresh
            
            .adoChecksumHistory.ConnectionString = g_strConnection
            .adoChecksumHistory.RecordSource = "SELECT * FROM eventchecksumhistory WHERE eventid = -1 order by cdate"
            .adoChecksumHistory.Refresh
            
            .adoHistory.ConnectionString = g_strConnection
            .adoHistory.RecordSource = "SELECT * FROM eventhistory WHERE eventID = -1 ORDER BY datesaved DESC;"
            .adoHistory.Refresh
                
            .adoMediaWindowHistory.ConnectionString = g_strConnection
            .adoMediaWindowHistory.RecordSource = "SELECT * FROM webdelivery WHERE clipID = -1 ORDER BY timewhen DESC;"
            .adoMediaWindowHistory.Refresh
                
            .grdPortalPermission.Columns("fullname").DropDownHwnd = 0
            On Error Resume Next
            .picKeyframe.Picture = LoadPicture(App.Path & "\Empty.gif")
            .picKeyframe.Refresh
            .lblAudioConfiguration.Visible = True
            .lblCaption(66).Caption = "Series ID"
        
        End With
    End If
End If

HighLite frmClipControl.txtClipID
On Error Resume Next
frmClipControl.txtClipID.SetFocus
On Error GoTo 0
l_rstClip.Close
Set l_rstClip = Nothing
l_rstUsage.Close
Set l_rstUsage = Nothing

frmClipControl.Show
frmClipControl.Set_Framerate
frmClipControl.ZOrder 0

g_intClipGridClick = 0
g_intTranscodeGridClick = 0
g_intFileRequestGridClick = 0

End Sub

Public Sub PopulateCustomFieldCombo(lp_lngCompanyID As Long, lp_lngFieldNumber As Long, lp_conControl As Control)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    LockControl lp_conControl, True
    
    Screen.MousePointer = vbHourglass
    
    Dim l_strSQL As String
    Dim l_rstResource As New ADODB.Recordset
            
    lp_conControl.Clear
            
    l_strSQL = "SELECT textentry FROM customfieldxref WHERE customfield = " & lp_lngFieldNumber
    l_strSQL = l_strSQL & " AND companyID = " & lp_lngCompanyID & " ORDER BY fd_order, textentry"
    
    Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If Not l_rstResource.EOF Then l_rstResource.MoveFirst
    Do While Not l_rstResource.EOF
        lp_conControl.AddItem Trim(" " & l_rstResource("textentry"))
        l_rstResource.MoveNext
    Loop
    
    l_rstResource.Close
    Set l_rstResource = Nothing
            
    LockControl lp_conControl, False
    DoEvents
    
    'MDIForm1.Caption = l_strTempCaption
    Screen.MousePointer = vbDefault
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub SetClipSpecToMediaSpec(lp_lngClipID As Long, lp_lngMediaSpecID As Long)

Dim l_strSQL As String, l_rstSpec As ADODB.Recordset, l_rstClip As ADODB.Recordset

If lp_lngClipID = 0 Or lp_lngMediaSpecID = 0 Then Exit Sub

l_strSQL = "SELECT * FROM events WHERE eventID = " & lp_lngClipID & ";"
Set l_rstClip = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

l_strSQL = "SELECT * FROM mediaspec WHERE mediaspecID = '" & lp_lngMediaSpecID & "';"
Set l_rstSpec = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If Not l_rstSpec.EOF And Not l_rstClip.EOF Then
    
    l_rstClip("eventType") = Trim(" " & l_rstSpec("purpose"))
    l_rstClip("Clipformat") = Trim(" " & l_rstSpec("mediaformat"))
    l_rstClip("Clipcodec") = Trim(" " & l_rstSpec("videocodec"))
    l_rstClip("mediaStreamType") = Trim(" " & l_rstSpec("mediastreamtype"))
    l_rstClip("clipAudioCodec") = Trim(" " & l_rstSpec("audiocodec"))
    l_rstClip("encodePasses") = Trim(" " & l_rstSpec("encodepasses"))
    l_rstClip("clipHorizontalpixels") = Trim(" " & l_rstSpec("horiz"))
    l_rstClip("clipVerticalpixels") = Trim(" " & l_rstSpec("vert"))
    l_rstClip("clipFrameRate") = Trim(" " & l_rstSpec("framerate"))
    l_rstClip("clipBitrate") = Trim(" " & l_rstSpec("totalbitrate"))
    l_rstClip("VideoBitrate") = Trim(" " & l_rstSpec("videobitrate"))
    l_rstClip("AudioBitrate") = Trim(" " & l_rstSpec("audiobitrate"))
    l_rstClip("Aspectratio") = Trim(" " & l_rstSpec("aspectratio"))
    l_rstClip("Geometriclinearity") = Trim(" " & l_rstSpec("geometry"))
    l_rstClip("Altlocation") = Trim(" " & l_rstSpec("altlocation"))
    l_rstClip("CbrVbr") = Trim(" " & l_rstSpec("cbrvbr"))
    l_rstClip("Interlace") = Trim(" " & l_rstSpec("interlace"))
    l_rstClip("vodplatform") = Trim(" " & l_rstSpec("vodplatform"))
    
    l_rstClip.Update
    
End If
l_rstSpec.Close
Set l_rstSpec = Nothing
l_rstClip.Close
Set l_rstClip = Nothing

End Sub

Sub MakeClipFromDADCTrackerEvent(lp_lngDADCTrackerID As Long)

Dim l_lngClipID As Long, l_strSQL As String
Dim l_rstNewClip As ADODB.Recordset, l_rstData As ADODB.Recordset, l_strDivaCategory As String

l_lngClipID = CreateClip

l_strSQL = "SELECT * FROM events WHERE eventID = " & l_lngClipID
Set l_rstNewClip = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If Not l_rstNewClip.EOF Then
    With frmTrackerDADC
        l_rstNewClip("sourcelibraryID") = GetData("library", "libraryID", "barcode", .grdItems.Columns("barcode").Text)
        l_rstNewClip("muser") = g_strUserInitials
        l_rstNewClip("mdate") = FormatSQLDate(Now)
        l_rstNewClip("companyID") = .lblCompanyID.Caption
        l_rstNewClip("companyname") = GetData("company", "name", "companyID", .lblCompanyID.Caption)
        l_rstNewClip("eventtype") = "Original Master"
        l_rstNewClip("hidefromweb") = 1
        If .grdItems.Columns("title").Text <> "" Then l_rstNewClip("eventtitle") = .grdItems.Columns("title").Text Else l_rstNewClip("eventtitle") = Null
        If .grdItems.Columns("subtitle").Text <> "" Then l_rstNewClip("eventsubtitle") = .grdItems.Columns("subtitle").Text Else l_rstNewClip("eventsubtitle") = Null
        If .grdItems.Columns("episode").Text <> "" Then l_rstNewClip("eventepisode") = .grdItems.Columns("episode").Text Else l_rstNewClip("eventepisode") = Null
        l_rstNewClip("clipreference") = .grdItems.Columns("itemreference").Text
        l_rstNewClip("clipfilename") = .grdItems.Columns("itemreference").Text & ".mov"
        l_rstNewClip("internalreference") = GetData("library", "internalreference", "barcode", .grdItems.Columns("barcode").Text)
        l_rstNewClip.Update
        If .grdItems.Columns("legacylinestandard").Text = "625 PAL - 16:9 WIDESCREEN" Then
            SetClipSpecToMediaSpec l_lngClipID, 333
        ElseIf .grdItems.Columns("legacylinestandard").Text = "625 PAL - 4:3 FULL SCREEN" Then
            SetClipSpecToMediaSpec l_lngClipID, 338
        ElseIf .grdItems.Columns("legacylinestandard").Text = "HD ORIGINATED 1080/50I MATERIAL" Then
            SetClipSpecToMediaSpec l_lngClipID, 339
        End If
'        'Get the DIVA Category
'        Set l_rstData = ExecuteSQL("exec cet_Get_DIVA_Category @EventID=" & l_lngClipID, g_strExecuteError)
'        CheckForSQLError
'        l_strDivaCategory = l_rstData(0)
'        l_rstData.Close
'        SetData "events", "clipsoundformat", "eventID", l_lngClipID, l_strDivaCategory
    End With
End If

l_rstNewClip.Close
Set l_rstNewClip = Nothing

ShowClipControl l_lngClipID

End Sub

Sub MakeClipFromGenericTrackerEvent()

Dim l_lngClipID As Long, l_strSQL As String
Dim l_rstNewClip As ADODB.Recordset, l_rstData As ADODB.Recordset, l_rstTapeLog As ADODB.Recordset
Dim l_strFilenameBody As String, l_strDivaCategory As String
Dim l_lngLibraryID As Long, l_strTimecodeStart As String, l_strTimecodeStop As String, l_strCetaClientCode As String

l_lngClipID = CreateClip

l_strSQL = "SELECT * FROM events WHERE eventID = " & l_lngClipID
Set l_rstNewClip = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If Not l_rstNewClip.EOF Then
    With frmTracker
        l_strFilenameBody = .grdItems.Columns("itemreference").Text
        l_rstNewClip("sourcelibraryID") = GetData("library", "libraryID", "barcode", .grdItems.Columns("barcode").Text)
        l_rstNewClip("muser") = g_strUserInitials
        l_rstNewClip("mdate") = FormatSQLDate(Now)
        l_rstNewClip("companyID") = .lblCompanyID.Caption
        l_rstNewClip("companyname") = GetData("company", "name", "companyID", .lblCompanyID.Caption)
        l_rstNewClip("eventtype") = "Original Master"
        l_rstNewClip("hidefromweb") = 1
        If Val(.lblCompanyID.Caption) = 1575 Then
            If Trim(" " & GetData("tracker_itemchoice", "itemfield", "itemheading", "Series Title")) <> "" Then l_rstNewClip("eventtitle") = .grdItems.Columns(GetData("tracker_itemchoice", "itemfield", "itemheading", "Title")).Text Else l_rstNewClip("eventtitle") = Null
            If Trim(" " & GetData("tracker_itemchoice", "itemfield", "itemheading", "Programme Title")) <> "" Then l_rstNewClip("eventsubtitle") = .grdItems.Columns(GetData("tracker_itemchoice", "itemfield", "itemheading", "Title")).Text Else l_rstNewClip("eventsubtitle") = Null
            l_rstNewClip("clipreference") = .grdItems.Columns("itemreference").Text
            l_rstNewClip("clipfilename") = .grdItems.Columns("itemreference").Text & "_M.mov"
        ElseIf Val(.lblCompanyID.Caption) = 1244 Then
            If Trim(" " & GetData("tracker_itemchoice", "itemfield", "itemheading", "Series Title")) <> "" Then l_rstNewClip("eventtitle") = .grdItems.Columns(GetData("tracker_itemchoice", "itemfield", "itemheading", "Title")).Text Else l_rstNewClip("eventtitle") = Null
            l_rstNewClip("clipreference") = .grdItems.Columns("itemreference").Text
            l_rstNewClip("clipfilename") = .grdItems.Columns("itemreference").Text & ".mov"
        Else
            If .grdItems.Columns("headertext1").Text <> "" Then l_rstNewClip("eventtitle") = .grdItems.Columns("headertext1").Text Else l_rstNewClip("eventtitle") = Null
            If .grdItems.Columns("headertext2").Text <> "" Then l_rstNewClip("eventsubtitle") = .grdItems.Columns("headertext2").Text Else l_rstNewClip("eventsubtitle") = Null
            l_rstNewClip("clipreference") = .grdItems.Columns("itemreference").Text
            l_rstNewClip("clipfilename") = .grdItems.Columns("itemreference").Text & ".mov"
            If Val(Trim(" " & GetData("library", "LibraryID", "barcode", .grdItems.Columns("barcode").Text))) <> 0 Then
                l_lngLibraryID = GetData("library", "LibraryID", "barcode", .grdItems.Columns("barcode").Text)
                l_strSQL = "SELECT * FROM tapeevents WHERE libraryID = " & l_lngLibraryID & " ORDER BY timecodestart ASC"
                Set l_rstTapeLog = ExecuteSQL(l_strSQL, g_strExecuteError)
                If l_rstTapeLog.RecordCount > 0 Then
                    l_rstTapeLog.MoveFirst
                    l_strTimecodeStart = l_rstTapeLog("timecodestart")
                    l_rstTapeLog.MoveLast
                    l_strTimecodeStop = l_rstTapeLog("timecodestop")
                End If
                If l_strTimecodeStart <> "" Then l_rstNewClip("timecodestart") = l_strTimecodeStart
                If l_strTimecodeStop <> "" Then l_rstNewClip("timecodestop") = l_strTimecodeStop
                l_rstTapeLog.Close
                Set l_rstTapeLog = Nothing
            End If
        End If
        'Ask about Audio
        l_strCetaClientCode = GetData("company", "cetaclientcode", "companyID", .lblCompanyID.Caption)
        If InStr(l_strCetaClientCode, "/trackeraskaboutaudio") > 0 Then
            If MsgBox("Is there Stereo Main English on Audio 1&2", vbYesNo, "Audio Tracks") = vbYes Then
                l_rstNewClip("audiotypegroup1") = "Standard Stereo"
                l_rstNewClip("audiocontentgroup1") = "Composite"
                l_rstNewClip("audiolanguagegroup1") = "English"
            End If
            If MsgBox("Is there an M&E on Audio 3&4", vbYesNo, "Audio Tracks") = vbYes Then
                l_rstNewClip("audiotypegroup2") = "Standard Stereo"
                l_rstNewClip("audiocontentgroup2") = "M&E"
                l_rstNewClip("audiolanguagegroup2") = "None"
            End If
        End If
        l_rstNewClip("internalreference") = GetData("library", "internalreference", "barcode", .grdItems.Columns("barcode").Text)
        l_rstNewClip.Update
        If Val(.lblCompanyID.Caption) = 1575 Or Val(.lblCompanyID.Caption) = 1244 Then
            'Do nothing
        Else
            If MsgBox("Is it HD", vbYesNo, "HD or SD") = vbNo Then
                SetClipSpecToMediaSpec l_lngClipID, 333
            Else
                SetClipSpecToMediaSpec l_lngClipID, 339
            End If
        End If
'        'Get the DIVA Category
'        Set l_rstData = ExecuteSQL("exec cet_Get_DIVA_Category @EventID=" & l_lngClipID, g_strExecuteError)
'        CheckForSQLError
'        l_strDivaCategory = l_rstData(0)
'        l_rstData.Close
'        SetData "events", "clipsoundformat", "eventID", l_lngClipID, l_strDivaCategory
    End With
End If

l_rstNewClip.Close
Set l_rstNewClip = Nothing

ShowClipControl l_lngClipID

End Sub

Sub MakeDisneyAgileClip(lp_strStore As String, lp_strAgileNumber As String, lp_strFilename As String, lp_strWMLSNumber As String)

Dim l_lngClipID As Long, l_strSQL As String
Dim l_rstNewClip As ADODB.Recordset
Dim l_lngLibraryID As Long
Dim l_strAltLocation As String

l_lngClipID = CreateClip

If l_lngClipID <> 0 Then
    SetData "events", "libraryID", "eventID", l_lngClipID, l_lngLibraryID
    Select Case lp_strStore
        Case "JELLYROLL"
            l_lngLibraryID = g_lngJellyroll1LibraryID
            SetData "events", "libraryID", "eventID", l_lngClipID, l_lngLibraryID
            If g_strLocationOfJellyroll1Folder <> "" Then
                l_strAltLocation = g_strLocationOfJellyroll1Folder & "\" & lp_strAgileNumber
            Else
                l_strAltLocation = lp_strAgileNumber
            End If
            SetData "events", "altlocation", "eventID", l_lngClipID, l_strAltLocation
        Case "JELLYROLL2"
            l_lngLibraryID = g_lngJellyroll2LibraryID
            SetData "events", "libraryID", "eventID", l_lngClipID, l_lngLibraryID
            If g_strLocationOfJellyroll2Folder <> "" Then
                l_strAltLocation = g_strLocationOfJellyroll2Folder & "\" & lp_strAgileNumber
            Else
                l_strAltLocation = lp_strAgileNumber
            End If
            SetData "events", "altlocation", "eventID", l_lngClipID, l_strAltLocation
    End Select

    SetData "events", "clipfilename", "eventID", l_lngClipID, lp_strFilename
    SetData "events", "clipreference", "eventID", l_lngClipID, Left(lp_strFilename, Len(lp_strFilename) - 4)
    SetData "events", "internalreference", "eventID", l_lngClipID, GetNextSequence("internalreference")
    SetData "events", "companyID", "eventID", l_lngClipID, 1163
    SetData "events", "companyname", "eventID", l_lngClipID, "The Walt Disney Company Ltd."
    SetData "events", "customfield1", "eventID", l_lngClipID, lp_strWMLSNumber
    SetData "events", "customfield2", "eventID", l_lngClipID, lp_strAgileNumber
    
    If InStr(lp_strFilename, "fimx-30") > 0 Then
        SetData "events", "clipformat", "eventID", l_lngClipID, "MXF OP1a"
        SetData "events", "clipcodec", "eventID", l_lngClipID, "30I VID + 4 AUD"
        SetData "events", "clipaudiocodec", "eventID", l_lngClipID, "LPCM 16bit 48kHz"
        SetData "events", "videobitrate", "eventID", l_lngClipID, 30000
        SetData "events", "audiobitrate", "eventID", l_lngClipID, 1536
        SetData "events", "clipbitrate", "eventID", l_lngClipID, 31536
        SetData "events", "encodepasses", "eventID", l_lngClipID, 1
        SetData "events", "cbrvbr", "eventID", l_lngClipID, "CBR"
        SetData "events", "interlace", "eventID", l_lngClipID, "Interlace"
        If InStr(LCase(lp_strFilename), "a16x9-178") > 0 Then
            SetData "events", "aspectratio", "eventID", l_lngClipID, "16:9"
            SetData "events", "geometriclinearity", "eventID", l_lngClipID, "ANAMORPHIC"
        ElseIf InStr(LCase(lp_strFilename), "a4x3-133") > 0 Then
            SetData "events", "aspectratio", "eventID", l_lngClipID, "4:3"
            SetData "events", "geometriclinearity", "eventID", l_lngClipID, "NORMAL"
        End If
        
    ElseIf InStr(lp_strFilename, "fprhq") > 0 Then
        SetData "events", "clipformat", "eventID", l_lngClipID, "MOV ProRes"
        SetData "events", "clipcodec", "eventID", l_lngClipID, "ProRes HQ"
        SetData "events", "clipaudiocodec", "eventID", l_lngClipID, "LPCM 24bit 48kHz"
        SetData "events", "videobitrate", "eventID", l_lngClipID, 184000
        SetData "events", "audiobitrate", "eventID", l_lngClipID, 1536
        SetData "events", "clipbitrate", "eventID", l_lngClipID, 185536
        SetData "events", "encodepasses", "eventID", l_lngClipID, 1
        SetData "events", "cbrvbr", "eventID", l_lngClipID, "CBR"
        SetData "events", "interlace", "eventID", l_lngClipID, "Progressive"
        If InStr(LCase(lp_strFilename), "a16x9-178") > 0 Then
            SetData "events", "aspectratio", "eventID", l_lngClipID, "16:9"
            SetData "events", "geometriclinearity", "eventID", l_lngClipID, "SQUARE"
        End If

    End If
    
    If InStr(lp_strFilename, "vpal") > 0 Then
        SetData "events", "cliphorizontalpixels", "eventID", l_lngClipID, 720
        SetData "events", "clipverticalpixels", "eventID", l_lngClipID, 608
        SetData "events", "clipframerate", "eventID", l_lngClipID, 25
    ElseIf InStr(lp_strFilename, "vntsc") > 0 Then
        SetData "events", "cliphorizontalpixels", "eventID", l_lngClipID, 720
        SetData "events", "clipverticalpixels", "eventID", l_lngClipID, 512
        SetData "events", "clipframerate", "eventID", l_lngClipID, 29.97
    ElseIf InStr(lp_strFilename, "v1080p25") > 0 Then
        SetData "events", "cliphorizontalpixels", "eventID", l_lngClipID, 1920
        SetData "events", "clipverticalpixels", "eventID", l_lngClipID, 1080
        SetData "events", "clipframerate", "eventID", l_lngClipID, 25
    ElseIf InStr(lp_strFilename, "v1080p29.97") > 0 Then
        SetData "events", "cliphorizontalpixels", "eventID", l_lngClipID, 1920
        SetData "events", "clipverticalpixels", "eventID", l_lngClipID, 1080
        SetData "events", "clipframerate", "eventID", l_lngClipID, 29.97
    ElseIf InStr(lp_strFilename, "v1080p23.98") > 0 Then
        SetData "events", "cliphorizontalpixels", "eventID", l_lngClipID, 1920
        SetData "events", "clipverticalpixels", "eventID", l_lngClipID, 1080
        SetData "events", "clipframerate", "eventID", l_lngClipID, 23.98
    End If
            
    VerifyClip l_lngClipID, l_strAltLocation, lp_strFilename, l_lngLibraryID, True, True
    
End If

End Sub

Sub MakeClipFromTapeEvent(lp_lngOldEventID As Long)

Dim l_lngClipID As Long, l_strSQL As String
Dim l_rstOldEvent As ADODB.Recordset, l_rstNewClip As ADODB.Recordset

'Check we can locate the old clip
l_strSQL = "SELECT * FROM tapeevents where eventID = " & lp_lngOldEventID
Set l_rstOldEvent = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If Not l_rstOldEvent.EOF Then

    'Create a new clip
    l_lngClipID = CreateClip
    l_rstOldEvent.MoveFirst
    
    l_strSQL = "SELECT * FROM events WHERE eventID = " & l_lngClipID
    Set l_rstNewClip = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If Not l_rstNewClip.EOF Then
        l_rstNewClip("sourcetapeeventID") = l_rstOldEvent("eventID")
        l_rstNewClip("sourcelibraryID") = l_rstOldEvent("libraryID")
        
        l_rstNewClip("timecodestart") = l_rstOldEvent("timecodestart")
        l_rstNewClip("timecodestop") = l_rstOldEvent("timecodestop")
        l_rstNewClip("fd_length") = l_rstOldEvent("fd_length")
        l_rstNewClip("endcredits") = l_rstOldEvent("endcredits")
        l_rstNewClip("endcreditselapsed") = l_rstOldEvent("endcreditselapsed")
        l_rstNewClip("break1") = l_rstOldEvent("break1")
        l_rstNewClip("break1elapsed") = l_rstOldEvent("break1elapsed")
        l_rstNewClip("break2") = l_rstOldEvent("break2")
        l_rstNewClip("break2elapsed") = l_rstOldEvent("break2elapsed")
        l_rstNewClip("break3") = l_rstOldEvent("break3")
        l_rstNewClip("break3elapsed") = l_rstOldEvent("break3elapsed")
        l_rstNewClip("break4") = l_rstOldEvent("break4")
        l_rstNewClip("break4elapsed") = l_rstOldEvent("break4elapsed")
        l_rstNewClip("break5") = l_rstOldEvent("break5")
        l_rstNewClip("break5elapsed") = l_rstOldEvent("break5elapsed")
        
        l_rstNewClip("clocknumber") = l_rstOldEvent("clocknumber")
        l_rstNewClip("notes") = l_rstOldEvent("notes")
        l_rstNewClip("muser") = g_strUserInitials
        l_rstNewClip("mdate") = FormatSQLDate(Now)
        l_rstNewClip("companyname") = frmLibrary.cmbCompany.Text
        l_rstNewClip("companyID") = frmLibrary.lblCompanyID.Caption
        If InStr(GetData("company", "cetaclientcode", "companyID", frmLibrary.lblCompanyID.Caption), "/hide") > 0 Then
            l_rstNewClip("hidefromweb") = 1
        End If
        l_rstNewClip("jobID") = 999999
        If frmLibrary.txtTitle.Text <> "" Then l_rstNewClip("eventtitle") = frmLibrary.txtTitle.Text Else l_rstNewClip("eventtitle") = Null
        If l_rstOldEvent("eventtitle") = "Programme" Then
            If IsNull(l_rstOldEvent("eventsubtitle")) Or l_rstOldEvent("eventsubtitle") = "" Then
                l_rstNewClip("eventsubtitle") = frmLibrary.txtSubtitle.Text
            Else
                l_rstNewClip("eventsubtitle") = l_rstOldEvent("eventsubtitle")
            End If
            l_rstNewClip("eventseries") = frmLibrary.cmbSeries.Text
            l_rstNewClip("eventset") = frmLibrary.cmbSet.Text
            If frmLibrary.cmbEpisode.Text <> "" Then l_rstNewClip("eventepisode") = frmLibrary.cmbEpisode.Text Else l_rstNewClip("eventepisode") = Null
            l_rstNewClip("eventversion") = frmLibrary.cmbVersion.Text
        Else
            l_rstNewClip("eventsubtitle") = l_rstOldEvent("eventtitle")
        End If
        l_rstNewClip("internalreference") = frmLibrary.txtInternalReference.Text
        l_rstNewClip("clipreference") = frmLibrary.txtReference.Text
        l_rstNewClip("customfield1") = frmLibrary.cmbField1.Text
        l_rstNewClip("customfield2") = frmLibrary.cmbField2.Text
        l_rstNewClip("customfield3") = frmLibrary.cmbField3.Text
        l_rstNewClip.Update
    
    End If
    
    l_rstOldEvent.Close
    Set l_rstOldEvent = Nothing
    
End If

l_rstNewClip.Close
Set l_rstNewClip = Nothing

ShowClipControl l_lngClipID

End Sub

Sub MakeNetAPorterClipFromTapeEvent(lp_lngOldEventID As Long)

Dim l_lngClipID As Long, l_strSQL As String
Dim l_rstOldEvent As ADODB.Recordset, l_rstNewClip As ADODB.Recordset

'Check we can locate the old clip
l_strSQL = "SELECT * FROM tapeevents where eventID = " & lp_lngOldEventID
Set l_rstOldEvent = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If Not l_rstOldEvent.EOF Then

    'Create a new clip
    l_lngClipID = CreateClip
    l_rstOldEvent.MoveFirst
    
    l_strSQL = "SELECT * FROM events WHERE eventID = " & l_lngClipID
    Set l_rstNewClip = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If Not l_rstNewClip.EOF Then
        l_rstNewClip("sourcetapeeventID") = l_rstOldEvent("eventID")
        l_rstNewClip("sourcelibraryID") = l_rstOldEvent("libraryID")
        
        l_rstNewClip("timecodestart") = l_rstOldEvent("timecodestart")
        l_rstNewClip("timecodestop") = l_rstOldEvent("timecodestop")
        l_rstNewClip("fd_length") = l_rstOldEvent("fd_length")
        
        l_rstNewClip("customfield1") = frmLibrary.txtTitle.Text
        l_rstNewClip("customfield2") = frmLibrary.txtSubtitle.Text
        l_rstNewClip("customfield3") = frmLibrary.cmbVersion.Text
        l_rstNewClip("customfield4") = l_rstOldEvent("clocknumber")
        
        l_rstNewClip("notes") = l_rstOldEvent("notes")
        l_rstNewClip("muser") = g_strUserInitials
        l_rstNewClip("mdate") = FormatSQLDate(Now)
        l_rstNewClip("companyname") = frmLibrary.cmbCompany.Text
        l_rstNewClip("companyID") = frmLibrary.lblCompanyID.Caption
        If InStr(GetData("company", "cetaclientcode", "companyID", frmLibrary.lblCompanyID.Caption), "/hide") > 0 Then
            l_rstNewClip("hidefromweb") = 1
        End If
        l_rstNewClip("jobID") = 999999
        If Not IsNull(l_rstOldEvent("eventtitle")) Then l_rstNewClip("eventtitle") = QuoteSanitise(l_rstOldEvent("eventtitle")) Else l_rstNewClip("eventtitle") = Null
        If frmLibrary.txtReference.Text <> "" Then l_rstNewClip("eventsubtitle") = frmLibrary.txtReference.Text Else l_rstNewClip("eventsubtitle") = Null
        l_rstNewClip("clipreference") = frmLibrary.txtInternalReference.Text
        l_rstNewClip("internalreference") = frmLibrary.txtInternalReference.Text
        l_rstNewClip.Update
    End If
    
    l_rstOldEvent.Close
    Set l_rstOldEvent = Nothing
    
End If

l_rstNewClip.Close
Set l_rstNewClip = Nothing

ShowClipControl l_lngClipID

End Sub

Sub MakeClockReferencedClipFromTapeEvent(lp_lngOldEventID As Long)
Dim l_lngClipID As Long, l_strSQL As String
Dim l_rstOldEvent As ADODB.Recordset, l_rstNewClip As ADODB.Recordset

'Check we can locate the old clip
l_strSQL = "SELECT * FROM tapeevents where eventID = " & lp_lngOldEventID
Set l_rstOldEvent = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If Not l_rstOldEvent.EOF Then

    'Create a new clip
    l_lngClipID = CreateClip
    l_rstOldEvent.MoveFirst
    
    l_strSQL = "SELECT * FROM events WHERE eventID = " & l_lngClipID
    Set l_rstNewClip = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If Not l_rstNewClip.EOF Then
        l_rstNewClip("sourcetapeeventID") = l_rstOldEvent("eventID")
        l_rstNewClip("sourcelibraryID") = l_rstOldEvent("libraryID")
        
        l_rstNewClip("timecodestart") = l_rstOldEvent("timecodestart")
        l_rstNewClip("timecodestop") = l_rstOldEvent("timecodestop")
        l_rstNewClip("fd_length") = l_rstOldEvent("fd_length")
        l_rstNewClip("endcredits") = l_rstOldEvent("endcredits")
        l_rstNewClip("endcreditselapsed") = l_rstOldEvent("endcreditselapsed")
        l_rstNewClip("break1") = l_rstOldEvent("break1")
        l_rstNewClip("break1elapsed") = l_rstOldEvent("break1elapsed")
        l_rstNewClip("break2") = l_rstOldEvent("break2")
        l_rstNewClip("break2elapsed") = l_rstOldEvent("break2elapsed")
        l_rstNewClip("break3") = l_rstOldEvent("break3")
        l_rstNewClip("break3elapsed") = l_rstOldEvent("break3elapsed")
        l_rstNewClip("break4") = l_rstOldEvent("break4")
        l_rstNewClip("break4elapsed") = l_rstOldEvent("break4elapsed")
        l_rstNewClip("break5") = l_rstOldEvent("break5")
        l_rstNewClip("break5elapsed") = l_rstOldEvent("break5elapsed")
        
        'l_rstNewClip("clocknumber") = l_rstOldEvent("clocknumber")
        l_rstNewClip("notes") = l_rstOldEvent("notes")
        l_rstNewClip("muser") = g_strUserInitials
        l_rstNewClip("mdate") = FormatSQLDate(Now)
        l_rstNewClip("companyname") = frmLibrary.cmbCompany.Text
        l_rstNewClip("companyID") = frmLibrary.lblCompanyID.Caption
        If InStr(GetData("company", "cetaclientcode", "companyID", frmLibrary.lblCompanyID.Caption), "/hide") > 0 Then
            l_rstNewClip("hidefromweb") = 1
        End If
        l_rstNewClip("jobID") = 999999
        If Not IsNull(l_rstOldEvent("eventtitle")) Then l_rstNewClip("eventtitle") = QuoteSanitise(l_rstOldEvent("eventtitle")) Else l_rstNewClip("eventtitle") = Null
        If frmLibrary.txtSubtitle.Text <> "" Then l_rstNewClip("eventsubtitle") = frmLibrary.txtSubtitle.Text Else l_rstNewClip("eventsubtitle") = Null
        l_rstNewClip("clipreference") = l_rstOldEvent("clocknumber")
        l_rstNewClip("internalreference") = frmLibrary.txtInternalReference.Text
        l_rstNewClip("customfield1") = frmLibrary.cmbField1.Text
        l_rstNewClip("customfield2") = frmLibrary.cmbField2.Text
        l_rstNewClip("customfield3") = frmLibrary.cmbField3.Text
        l_rstNewClip.Update
    End If
    
    l_rstOldEvent.Close
    Set l_rstOldEvent = Nothing
    
End If

l_rstNewClip.Close
Set l_rstNewClip = Nothing

ShowClipControl l_lngClipID

End Sub

Public Sub TranscodeClip()

'The routine to perform a Transcode on the currently displayed clip.
Dim l_lngClipID As Long
Dim l_lngNewClipID As Long
Dim l_intMsg As Integer
Dim l_strNewLibraryBarcode As String
Dim l_lngNewLibraryID As Long
Dim l_strSQL As String
Dim l_lngTapeLibraryID As Long
Dim l_lngReference As Long
Dim l_rstOldKeywords As ADODB.Recordset

PromptClipChanges (Val(frmClipControl.txtClipID.Text))
If frmClipControl.txtInternalReference.Text = "" Then
    frmClipControl.txtInternalReference.Text = GetNextSequence("internalreference")
    SetData "events", "internalreference", "eventID", Val(frmClipControl.txtClipID.Text), Val(frmClipControl.txtInternalReference.Text)
End If
frmClipControl.txtSourceEventID.Text = frmClipControl.txtClipID.Text
l_lngClipID = frmClipControl.txtClipID.Text
frmClipControl.lblSourceLibraryID.Caption = frmClipControl.lblLibraryID.Caption
frmClipControl.txtSourcebarcode.Text = frmClipControl.cmbBarcode.Text
frmClipControl.cmbClipformat.Text = ""
frmClipControl.cmbClipcodec.Text = ""
frmClipControl.cmbAspect.Text = ""
frmClipControl.cmbGeometry.Text = ""
frmClipControl.txtHorizpixels.Text = ""
frmClipControl.txtVertpixels.Text = ""
frmClipControl.txtBitrate.Text = ""
frmClipControl.txtClipID.Text = ""
frmClipControl.txtClipfilename.Text = ""
frmClipControl.txtAltFolder.Text = "RESULTS"
frmClipControl.lblVerifiedDate.Caption = ""
frmClipControl.lblFileSize.Caption = ""
frmClipControl.cmbBarcode.Text = "TRANSCODE"
frmClipControl.txtMD5Checksum.Text = ""
frmClipControl.txtSHA1Checksum.Text = ""
frmClipControl.lblLibraryID.Caption = GetData("library", "libraryID", "barcode", "TRANSCODE")
frmClipControl.lblFormat.Caption = GetData("library", "format", "barcode", "TRANSCODE")
frmClipControl.chkSourceFromMedia.Value = 1
frmClipControl.cmbFileVersion.Text = "Transcode"
DoEvents

SaveClip

'Record the clip usage
l_strSQL = "INSERT INTO eventusage ("
l_strSQL = l_strSQL & "eventID, dateused) VALUES ("
l_strSQL = l_strSQL & "'" & l_lngClipID & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "'"
l_strSQL = l_strSQL & ");"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_lngNewClipID = Val(frmClipControl.txtClipID.Text)

'Duplicate any keywords
l_strSQL = "SELECT * FROM eventkeyword WHERE eventID = '" & l_lngClipID & "';"
Set l_rstOldKeywords = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If Not l_rstOldKeywords.EOF Then
    l_rstOldKeywords.MoveFirst
    
    Do While Not l_rstOldKeywords.EOF
        l_strSQL = "INSERT INTO eventkeyword (eventID, keywordtext) VALUES ('"
        l_strSQL = l_strSQL & l_lngNewClipID & "', '"
        l_strSQL = l_strSQL & l_rstOldKeywords("keywordtext") & "');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_rstOldKeywords.MoveNext
    Loop
End If
l_rstOldKeywords.Close
Set l_rstOldKeywords = Nothing

ShowClipControl l_lngNewClipID

End Sub

Public Sub ReplayClip()
        
'The routines to perform a replay to tape of the currently displayed clip.
Dim l_lngClipID As Long
Dim l_lngNewClipID As Long
Dim l_intMsg As Integer
Dim l_strNewLibraryBarcode As String
Dim l_lngNewLibraryID As Long
Dim l_strSQL As String
Dim l_lngTapeLibraryID As Long
Dim l_lngReference As Long
Dim l_rstOldKeywords As ADODB.Recordset
Dim m_Framerate As Integer

PromptClipChanges (Val(frmClipControl.txtClipID.Text))
'First get a new barcode value for the tape to be made
If Not CheckAccess("/savelibrary") Then
    Exit Sub
End If
        
frmGetBarcodeNumber.Caption = "Please give the Barcode to duplicate to..."
If g_optAllocateBarcodes = 1 Then frmGetBarcodeNumber.txtBarcode.Text = g_strCompanyPrefix & GetNextSequence("barcodenumber")
frmGetBarcodeNumber.Show vbModal

l_strNewLibraryBarcode = UCase(frmGetBarcodeNumber.txtBarcode.Text)
Unload frmGetBarcodeNumber

'Check whether the new barcode was accepted, or escape was pressed.
If l_strNewLibraryBarcode = "" Then
    Exit Sub
End If

Select Case frmClipControl.cmbFrameRate

    Case "25"
        m_Framerate = TC_25
    Case "29.97"
        m_Framerate = TC_29
    Case "30"
        m_Framerate = TC_30
    Case "24", "23.98"
        m_Framerate = TC_24
    Case "50"
        m_Framerate = TC_50
    Case "60"
        m_Framerate = TC_60
    Case "59.94"
        m_Framerate = TC_59
    Case Else
        m_Framerate = TC_UN

End Select

'Check whether that tape exists already, and abandon if it does
l_lngTapeLibraryID = GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode)
If l_lngTapeLibraryID <> 0 Then
    MsgBox "Tape Already Exists!", vbCritical
    Exit Sub
End If

'Create a tape with that barcode and the clip's reference (or a new one if it hasn't got one.)
l_lngReference = Format(GetData("events", "internalreference", "eventID", Val(frmClipControl.txtClipID.Text)))
l_lngTapeLibraryID = CreateLibraryItem(l_strNewLibraryBarcode, QuoteSanitise(frmClipControl.txtTitle.Text), 0, l_lngReference)

'Update some information on the tape
l_strSQL = "UPDATE library SET "
l_strSQL = l_strSQL & "companyID = '" & frmClipControl.lblCompanyID.Caption & "', "
l_strSQL = l_strSQL & "companyname = '" & QuoteSanitise(frmClipControl.cmbCompany.Text) & "', "
l_strSQL = l_strSQL & "subtitle = '" & QuoteSanitise(frmClipControl.txtSubtitle.Text) & "', "
l_strSQL = l_strSQL & "series = '" & QuoteSanitise(frmClipControl.cmbSeries.Text) & "', "
l_strSQL = l_strSQL & "seriesset = '" & QuoteSanitise(frmClipControl.cmbSet.Text) & "', "
l_strSQL = l_strSQL & "episode = '" & QuoteSanitise(frmClipControl.cmbEpisode.Text) & "', "
l_strSQL = l_strSQL & "version = 'COPY', "
l_strSQL = l_strSQL & "aspectratio = '" & frmClipControl.cmbAspect.Text & "', "
l_strSQL = l_strSQL & "geometriclinearity = '" & frmClipControl.cmbGeometry.Text & "' "
l_strSQL = l_strSQL & "WHERE libraryID = " & l_lngTapeLibraryID & ";"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_lngClipID = Val(frmClipControl.txtClipID.Text)

If frmClipControl.adoLogging.Recordset.RecordCount > 0 Then
    frmClipControl.adoLogging.Recordset.MoveFirst
    Do While Not frmClipControl.adoLogging.Recordset.EOF
        AddEventToTape l_lngTapeLibraryID, frmClipControl.adoLogging.Recordset("segmentreference"), "", "", Val(frmClipControl.lblSourceLibraryID.Caption), frmClipControl.cmbAspect.Text, _
            frmClipControl.txtJobID.Text, Timecode_Subtract(frmClipControl.adoLogging.Recordset("timecodestop"), frmClipControl.adoLogging.Recordset("timecodestart"), m_Framerate), Now, _
            "", frmClipControl.adoLogging.Recordset("timecodestart"), frmClipControl.adoLogging.Recordset("timecodestop")
        frmClipControl.adoLogging.Recordset.MoveNext
    Loop
Else
    AddEventToTape l_lngTapeLibraryID, "Programme", "", frmClipControl.txtClockNumber.Text, Val(frmClipControl.lblSourceLibraryID.Caption), frmClipControl.cmbAspect.Text, _
    frmClipControl.txtJobID.Text, frmClipControl.txtDuration.Text, Now, "", frmClipControl.txtTimecodeStart.Text, frmClipControl.txtTimecodeStop.Text
End If

'l_lngNewClipID = CreateClip
'
'frmClipControl.txtClipID.Text = l_lngNewClipID
'frmClipControl.txtTitle = "Programme"
'frmClipControl.lblLibraryID.Caption = l_lngTapeLibraryID
'SaveClip
'SetData "events", "eventmediatype", "eventID", l_lngNewClipID, "TAPE"

ShowClipControl l_lngClipID
ShowLibrary l_lngTapeLibraryID

'Record the clip usage
l_strSQL = "INSERT INTO eventusage ("
l_strSQL = l_strSQL & "eventID, dateused) VALUES ("
l_strSQL = l_strSQL & "'" & l_lngClipID & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "'"
l_strSQL = l_strSQL & ");"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Sub

Function FileExists(ByVal FileFullPath As String, ByVal Filename As String) As Boolean

' Return True if a file exists

    On Error GoTo ErrorHandler

    ' get the attributes and ensure that it isn't a directory
    If UCase(Dir(FileFullPath)) = UCase(Filename) Then
        FileExists = True
    Else
        DoEvents
    End If
    Exit Function

ErrorHandler:
    'MsgBox "Error " & Err.Description & " - " & Filename
    ' if an error occurs, this function returns False

End Function

Function SaveClipBackground() As Long

Dim l_strSQL As String, l_lngClipID As Long, l_strNetworkPath As String, l_lngFilenumber As Long, l_curFileSize As Currency, Count As Long, l_strEpisode As String, l_rstData As ADODB.Recordset

'Check for some valid entries in various boxes

If frmClipControl.lblLibraryID.Caption = "" Then
    MsgBox "Please set the Library barcode of where the clip is stored.", vbCritical, "Saving Clip..."
    SaveClipBackground = 0
    Exit Function
End If

'Check the Clip Purpose, which is a critical companent of the DIVA category
If frmClipControl.cmbPurpose.Text = "" Then
    MsgBox "Clip Purpose must be set. PLease select a value from the dropdown.", vbCritical, "Saving Clip..."
    SaveClipBackground = 0
    Exit Function
End If

'Check some things witrh the filename
If frmClipControl.txtClipfilename.Text = "" Then
    frmClipControl.txtClipfilename.Text = "ZZZZZZ"
    MsgBox "Clip filename has not yet been set - setting to 'ZZZZZZ'." & Chr(13) & "Please re-save once clip filename is known.", vbOKOnly, "Saving Clip..."
ElseIf frmClipControl.chkNotCheckFilenames.Value = 0 And InStr(frmClipControl.txtClipfilename.Text, "&") > 0 Then
    MsgBox "Clip Filename has '&' character in it" & vbCrLf & "This has been found to be bad." & vbCrLf & "Please re-name the file.", vbCritical, "Problem Saving Clip..."
    SaveClipBackground = 0
    Exit Function
End If

If frmClipControl.txtJobID.Text = "999999" Then
    MsgBox "Job ID must be properly set.", vbOKOnly, "Saving Clip..."
    SaveClipBackground = 0
    Exit Function
End If

'Check txtClipid = "" means new clip, or a value means modifying existing clip
If frmClipControl.txtClipID.Text = "" Then
    l_lngClipID = CreateClip
    If frmClipControl.txtInternalReference.Text = "" Then frmClipControl.txtInternalReference.Text = GetNextSequence("internalreference")
    SetData "events", "internalreference", "eventID", l_lngClipID, frmClipControl.txtInternalReference.Text
Else
    l_lngClipID = Val(frmClipControl.txtClipID.Text)
End If

'Check InternalReference and confirm if different from stored value
If frmClipControl.txtInternalReference.Text <> GetData("events", "internalreference", "eventID", l_lngClipID) Then
    If MsgBox("Internal Reference is different from stored value - Is this Correct?", vbYesNo, "Checking Internal Reference") = vbNo Then
        frmClipControl.txtInternalReference.Text = GetData("events", "internalreference", "eventID", l_lngClipID)
    End If
End If

'Check the rigorous and Shine stuff
If InStr(GetData("company", "cetaclientcode", "companyID", Val(frmClipControl.lblCompanyID.Caption)), "/shineversion") > 0 And frmClipControl.cmbFileVersion.Text = "Original Masterfile" Then
    If frmClipControl.cmbVersion.Text = "" Then
        MsgBox "Version must be set on clips for this customer.", vbCritical, "Error..."
        SaveClipBackground = False
        Exit Function
    End If
    If frmClipControl.cmbField1.Text = "" Then
        MsgBox "Technical Category (custom field) must be set on clips for this customer.", vbCritical, "Error..."
        SaveClipBackground = False
        Exit Function
    End If
    If frmClipControl.cmbField2.Text = "" Then
        MsgBox "Category (custom field) must be set on clips for this customer.", vbCritical, "Error..."
        SaveClipBackground = False
        Exit Function
    End If
'    If frmClipControl.cmbField3.Text = "" Then
'        MsgBox "Asset ID (custom field) must be set on clips for this customer.", vbCritical, "Error..."
'        SaveClipBackground = False
'        Exit Function
'    End If
End If

'Check the Bitrate has only real numbers in it.
If frmClipControl.txtBitrate.Text <> "" Then
    frmClipControl.txtBitrate.Text = Val(frmClipControl.txtBitrate.Text)
    If frmClipControl.txtBitrate.Text = 0 Then
        frmClipControl.txtBitrate.Text = ""
    End If
End If

'Check if the altlocation is blank whether it is in the default folder or not.
If frmClipControl.txtAltFolder.Text = "" Then
    If MsgBox("Is this clip in the Company Default Folder?)", vbYesNo, "Saving Clip...") = vbYes Then
        frmClipControl.txtAltFolder.Text = frmClipControl.lblCompanyID.Caption
    End If
End If

'Check that the alt location hasn't got a drive letter or a UNC path at the start of it.
If frmClipControl.txtAltFolder.Text Like "\\*" Or Mid(frmClipControl.txtAltFolder.Text, 2, 1) = ":" Then
    MsgBox "Alt Location has either drive letter or UNC network reference. Please fix this", vbOKOnly, "Saving Clip..."
End If

'Routines to update and verify the file size information
If GetData("library", "format", "libraryID", frmClipControl.lblLibraryID.Caption) = "DISCSTORE" And frmClipControl.txtClipfilename.Text <> "ZZZZZZ" Then

    If frmClipControl.chkNotCheckFilenames.Value = 0 Then
        If VerifyClip(frmClipControl.txtClipID.Text, frmClipControl.txtAltFolder.Text, frmClipControl.txtClipfilename.Text, frmClipControl.lblLibraryID.Caption, False, True) = False Then
            SaveClipBackground = 0
            Exit Function
        End If
    End If
Else
    If frmClipControl.txtClipfilename.Text = "ZZZZZZ" Then
        frmClipControl.lblFileSize.Caption = ""
        frmClipControl.lblVerifiedDate.Caption = ""
    End If
End If

With frmClipControl

l_strSQL = "UPDATE events SET "
l_strSQL = l_strSQL & "mdate = '" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "muser = '" & g_strUserInitials & "', "
l_strSQL = l_strSQL & "libraryID = '" & .lblLibraryID.Caption & "', "
If .lblSourceLibraryID.Caption <> "" Then
    l_strSQL = l_strSQL & "sourcelibraryID = '" & .lblSourceLibraryID.Caption & "', "
Else
    l_strSQL = l_strSQL & "sourcelibraryID = Null, "
End If
If .txtSourceEventID.Text <> "" Then
    If .chkSourceFromMedia.Value = 0 Then
        l_strSQL = l_strSQL & "sourcetapeeventID = '" & .txtSourceEventID.Text & "', "
        l_strSQL = l_strSQL & "sourceeventID = NULL, "
    Else
        l_strSQL = l_strSQL & "sourceeventID = '" & .txtSourceEventID.Text & "', "
        l_strSQL = l_strSQL & "sourcetapeeventID = NULL, "
    End If
Else
    l_strSQL = l_strSQL & "sourceeventID = Null, "
    l_strSQL = l_strSQL & "sourcetapeeventID = Null, "
End If
l_strSQL = l_strSQL & "sourcefrommedia = " & .chkSourceFromMedia.Value & ", "
If .lblCompanyID.Caption <> "" Then
    l_strSQL = l_strSQL & "companyname = '" & QuoteSanitise(GetData("company", "name", "companyID", .lblCompanyID.Caption)) & "', "
Else
    l_strSQL = l_strSQL & "companyname = '" & GetData("company", "name", "companyID", 1) & "', "
End If
If .lblCompanyID.Caption <> "" Then
    l_strSQL = l_strSQL & "companyID = '" & .lblCompanyID.Caption & "', "
Else
    l_strSQL = l_strSQL & "companyID = 1, "
End If
l_strSQL = l_strSQL & "jobID = '" & .txtJobID.Text & "', "
l_strSQL = l_strSQL & "jobdetailID = '" & .txtJobDetailID.Text & "', "
If .txtSvenskProjectNumber.Text <> "" Then l_strSQL = l_strSQL & "svenskprojectnumber = '" & .txtSvenskProjectNumber.Text & "', " Else l_strSQL = l_strSQL & "svenskprojectnumber = NULL, "
If .txtTitle.Text <> "" Then l_strSQL = l_strSQL & "eventtitle = '" & QuoteSanitise(.txtTitle.Text) & "', " Else l_strSQL = l_strSQL & "eventtitle = NULL, "
If .txtSubtitle.Text <> "" Then l_strSQL = l_strSQL & "eventsubtitle = '" & QuoteSanitise(.txtSubtitle.Text) & "', " Else l_strSQL = l_strSQL & "eventsubtitle = NULL, "
l_strSQL = l_strSQL & "eventseries = '" & QuoteSanitise(.cmbSeries.Text) & "', "
l_strSQL = l_strSQL & "eventset = '" & .cmbSet.Text & "', "
If .cmbEpisode.Text <> "" Then
    If InStr(GetData("company", "cetaclientcode", "companyID", Val(frmClipControl.lblCompanyID.Caption)), "/2digitepnumbers") > 0 Then
        For Count = 1 To Len(.cmbEpisode.Text)
            If Not IsNumeric(Mid(.cmbEpisode.Text, Count, 1)) Then
                Exit For
            End If
        Next
        l_strEpisode = Format(Val(.cmbEpisode.Text), "00")
        If Count <= Len(.cmbEpisode.Text) Then l_strEpisode = l_strEpisode & Mid(.cmbEpisode.Text, Count)
        l_strSQL = l_strSQL & "eventepisode = '" & l_strEpisode & "', "
    Else
        l_strSQL = l_strSQL & "eventepisode = '" & .cmbEpisode.Text & "', "
    End If
Else
    l_strSQL = l_strSQL & "eventepisode = NULL, "
End If
If .cmbVersion.Text <> "" Then l_strSQL = l_strSQL & "eventversion = '" & QuoteSanitise(.cmbVersion.Text) & "', " Else l_strSQL = l_strSQL & "eventversion = NULL, "
If .cmbLanguage.Text <> "" Then l_strSQL = l_strSQL & "language = '" & .cmbLanguage.Text & "', " Else l_strSQL = l_strSQL & "language = NULL, "
l_strSQL = l_strSQL & "vodplatform = '" & .cmbVodPlatform.Text & "', "
l_strSQL = l_strSQL & "clipreference = '" & Trim(QuoteSanitise(.txtReference.Text)) & "', "
l_strSQL = l_strSQL & "fileversion = '" & .cmbFileVersion.Text & "', "
l_strSQL = l_strSQL & "clipformat = '" & .cmbClipformat.Text & "', "
l_strSQL = l_strSQL & "clipcodec = '" & .cmbClipcodec.Text & "', "
l_strSQL = l_strSQL & "ColorSpace = '" & .txtColorSpace.Text & "', "
l_strSQL = l_strSQL & "ChromaSubsampling = '" & .txtChromaSubsampling.Text & "', "
l_strSQL = l_strSQL & "clipaudiocodec = '" & .cmbAudioCodec.Text & "', "
l_strSQL = l_strSQL & "aspectratio = '" & .cmbAspect.Text & "', "
l_strSQL = l_strSQL & "geometriclinearity = '" & .cmbGeometry.Text & "', "
If .txtHorizpixels.Text <> "" Then l_strSQL = l_strSQL & "cliphorizontalpixels = '" & .txtHorizpixels.Text & "', " Else l_strSQL = l_strSQL & "cliphorizontalpixels = NULL, "
If .txtVertpixels.Text <> "" Then l_strSQL = l_strSQL & "clipverticalpixels = '" & .txtVertpixels.Text & "', " Else l_strSQL = l_strSQL & "clipverticalpixels = NULL, "
If .txtBitrate.Text <> "" Then l_strSQL = l_strSQL & "clipbitrate = '" & .txtBitrate.Text & "', " Else l_strSQL = l_strSQL & "clipbitrate = NULL, "
l_strSQL = l_strSQL & "encodepasses = '" & .cmbPasses.Text & "', "
l_strSQL = l_strSQL & "cbrvbr = '" & .cmbCbrVbr.Text & "', "
l_strSQL = l_strSQL & "interlace = '" & .cmbInterlace.Text & "', "
l_strSQL = l_strSQL & "videobitrate = '" & .txtVideoBitrate.Text & "', "
l_strSQL = l_strSQL & "audiobitrate = '" & .txtAudioBitrate.Text & "', "
If .txtTrackCount.Text <> "" Then l_strSQL = l_strSQL & "trackcount = " & Val(.txtTrackCount.Text) & ", " Else l_strSQL = l_strSQL & "trackcount = NULL, "
If .txtAudioChannelCount.Text <> "" Then l_strSQL = l_strSQL & "channelcount = " & Val(.txtAudioChannelCount.Text) & ", " Else l_strSQL = l_strSQL & "channelcount = NULL, "
l_strSQL = l_strSQL & "istextinpicture = '" & .cmbIsTextInPicture.Text & "', "
If .txtSeriesID.Text <> "" Then
    l_strSQL = l_strSQL & "seriesID = '" & .txtSeriesID.Text & "', "
Else
    l_strSQL = l_strSQL & "seriesID = NULL, "
End If
If .txtSerialNumber.Text <> "" Then
    l_strSQL = l_strSQL & "serialnumber = '" & .txtSerialNumber.Text & "', "
Else
    l_strSQL = l_strSQL & "serialnumber = NULL, "
End If
l_strSQL = l_strSQL & "clipfilename = '" & QuoteSanitise(.txtClipfilename.Text) & "', "
If .lblFileSize.Caption <> "" And .lblFileSize.Caption <> "0" Then
    l_strSQL = l_strSQL & "bigfilesize = " & Format(.lblFileSize.Caption, "#") & ", "
Else
    l_strSQL = l_strSQL & "bigfilesize = Null, "
End If
l_strSQL = l_strSQL & "altlocation = '" & QuoteSanitise(SlashForwardToBack(.txtAltFolder.Text)) & "', "
If .lblVerifiedDate.Caption <> "" Then l_strSQL = l_strSQL & "soundlay = '" & FormatSQLDate(.lblVerifiedDate.Caption) & "', " Else l_strSQL = l_strSQL & "soundlay = NULL, "
If .lblLastMediaInfoQuery.Caption <> "" Then l_strSQL = l_strSQL & "lastmediainfoquery = '" & FormatSQLDate(.lblLastMediaInfoQuery.Caption) & "', " Else l_strSQL = l_strSQL & "lastmediainfoquery = NULL, "
If .lblLastModifiedDate.Caption <> "" Then l_strSQL = l_strSQL & "FileModifiedDate = '" & FormatSQLDate(.lblLastModifiedDate.Caption) & "', " Else l_strSQL = l_strSQL & "FileModifiedDate = NULL, "
l_strSQL = l_strSQL & "timecodestart = '" & .txtTimecodeStart.Text & "', "
l_strSQL = l_strSQL & "timecodestop = '" & .txtTimecodeStop.Text & "', "
If .txtEndCredits.Text <> "00:00:00:00" And .txtEndCredits.Text <> "00:00:00;00" Then
    l_strSQL = l_strSQL & "endcredits = '" & .txtEndCredits.Text & "', "
Else
    l_strSQL = l_strSQL & "endcredits = NULL, "
End If
l_strSQL = l_strSQL & "endcreditselapsed = '" & .txtEndCreditsElapsed.Text & "', "
l_strSQL = l_strSQL & "audiotypegroup1 = '" & .cmbAudioType1.Text & "', "
l_strSQL = l_strSQL & "audiotypegroup2 = '" & .cmbAudioType2.Text & "', "
l_strSQL = l_strSQL & "audiotypegroup3 = '" & .cmbAudioType3.Text & "', "
l_strSQL = l_strSQL & "audiotypegroup4 = '" & .cmbAudioType4.Text & "', "
l_strSQL = l_strSQL & "audiotypegroup5 = '" & .cmbAudioType5.Text & "', "
l_strSQL = l_strSQL & "audiotypegroup6 = '" & .cmbAudioType6.Text & "', "
l_strSQL = l_strSQL & "audiotypegroup7 = '" & .cmbAudioType7.Text & "', "
l_strSQL = l_strSQL & "audiotypegroup8 = '" & .cmbAudioType8.Text & "', "
l_strSQL = l_strSQL & "audiocontentgroup1 = '" & .cmbAudioContent1.Text & "', "
l_strSQL = l_strSQL & "audiocontentgroup2 = '" & .cmbAudioContent2.Text & "', "
l_strSQL = l_strSQL & "audiocontentgroup3 = '" & .cmbAudioContent3.Text & "', "
l_strSQL = l_strSQL & "audiocontentgroup4 = '" & .cmbAudioContent4.Text & "', "
l_strSQL = l_strSQL & "audiocontentgroup5 = '" & .cmbAudioContent5.Text & "', "
l_strSQL = l_strSQL & "audiocontentgroup6 = '" & .cmbAudioContent6.Text & "', "
l_strSQL = l_strSQL & "audiocontentgroup7 = '" & .cmbAudioContent7.Text & "', "
l_strSQL = l_strSQL & "audiocontentgroup8 = '" & .cmbAudioContent8.Text & "', "
l_strSQL = l_strSQL & "audiolanguagegroup1 = '" & .cmbAudioLanguage1.Text & "', "
l_strSQL = l_strSQL & "audiolanguagegroup2 = '" & .cmbAudioLanguage2.Text & "', "
l_strSQL = l_strSQL & "audiolanguagegroup3 = '" & .cmbAudioLanguage3.Text & "', "
l_strSQL = l_strSQL & "audiolanguagegroup4 = '" & .cmbAudioLanguage4.Text & "', "
l_strSQL = l_strSQL & "audiolanguagegroup5 = '" & .cmbAudioLanguage5.Text & "', "
l_strSQL = l_strSQL & "audiolanguagegroup6 = '" & .cmbAudioLanguage6.Text & "', "
l_strSQL = l_strSQL & "audiolanguagegroup7 = '" & .cmbAudioLanguage7.Text & "', "
l_strSQL = l_strSQL & "audiolanguagegroup8 = '" & .cmbAudioLanguage8.Text & "', "
l_strSQL = l_strSQL & "notes = '" & QuoteSanitise(.txtNotes.Text) & "', "
l_strSQL = l_strSQL & "clocknumber = '" & QuoteSanitise(.txtClockNumber.Text) & "', "
l_strSQL = l_strSQL & "fd_length = '" & .txtDuration.Text & "', "
l_strSQL = l_strSQL & "eventtype = '" & QuoteSanitise(.cmbPurpose.Text) & "', "
l_strSQL = l_strSQL & "internalnotes = '" & QuoteSanitise(.txtInternalNotes.Text) & "', "
If .cmbFrameRate.Text <> "" Then l_strSQL = l_strSQL & "clipframerate = '" & .cmbFrameRate.Text & "', " Else l_strSQL = l_strSQL & "clipframerate = NULL, "
l_strSQL = l_strSQL & "clipgenre = '" & QuoteSanitise(.cmbGenre.Text) & "', "
l_strSQL = l_strSQL & "cliptype = '" & .cmbClipType.Text & "', "
l_strSQL = l_strSQL & "eventkeyframefilename = '" & QuoteSanitise(.txtKeyframeFilename.Text) & "', "
l_strSQL = l_strSQL & "eventkeyframetimecode = '" & .txtTimecodeKeyframe.Text & "', "
l_strSQL = l_strSQL & "fourbythreeflag = '" & .chkfourbythreeflag.Value & "', "
l_strSQL = l_strSQL & "sound_stereo_russian = '" & .chkFileLocked.Value & "', "

If .cmbField1.Text <> "" Then l_strSQL = l_strSQL & "customfield1 = '" & .cmbField1.Text & "', " Else l_strSQL = l_strSQL & "customfield1 = NULL, "
If .cmbField2.Text <> "" Then l_strSQL = l_strSQL & "customfield2 = '" & .cmbField2.Text & "', " Else l_strSQL = l_strSQL & "customfield2 = NULL, "
If .cmbField3.Text <> "" Then l_strSQL = l_strSQL & "customfield3 = '" & .cmbField3.Text & "', " Else l_strSQL = l_strSQL & "customfield3 = NULL, "
If .cmbField4.Text <> "" Then l_strSQL = l_strSQL & "customfield4 = '" & .cmbField4.Text & "', " Else l_strSQL = l_strSQL & "customfield4 = NULL, "
If .cmbField5.Text <> "" Then l_strSQL = l_strSQL & "customfield5 = '" & .cmbField5.Text & "', " Else l_strSQL = l_strSQL & "customfield5 = NULL, "
If .cmbField6.Text <> "" Then l_strSQL = l_strSQL & "customfield6 = '" & .cmbField6.Text & "', " Else l_strSQL = l_strSQL & "customfield6 = NULL, "
l_strSQL = l_strSQL & "internalreference = '" & .txtInternalReference.Text & "', "
l_strSQL = l_strSQL & "mediastreamtype = '" & .cmbStreamType.Text & "', "

l_strSQL = l_strSQL & "clipstoreID = " & IIf(.chkTextlessPresent.Value <> 0, "1, ", "Null, ")

If .txtSHA1Checksum.Text <> "" Then l_strSQL = l_strSQL & "sha1checksum = '" & .txtSHA1Checksum.Text & "', " Else l_strSQL = l_strSQL & "sha1checksum = Null, "
If .txtMD5Checksum.Text <> "" Then l_strSQL = l_strSQL & "md5checksum = '" & .txtMD5Checksum.Text & "', " Else l_strSQL = l_strSQL & "md5checksum = Null, "
l_strSQL = l_strSQL & "DADCForcedSubtitle = '" & .cmbForcedSubtitle.Text & "', "
l_strSQL = l_strSQL & "DADCPictureFormat = '" & .cmbPictFormat.Text & "', "
l_strSQL = l_strSQL & "DADCActiveRatio = '" & .cmbActiveRatio.Text & "', "
l_strSQL = l_strSQL & "TextInPictureLanguage = '" & .cmbTextInPictureLanguage.Text & "', "
l_strSQL = l_strSQL & "BurnedInSubtitleLanguage = '" & .cmbBurnedInSubtitleLanguage.Text & "', "
l_strSQL = l_strSQL & "PipelineUnit = '" & .cmbPipelineUnit.Text & "', "
l_strSQL = l_strSQL & "EncodeReplayDeck = '" & .cmbEncodeReplayDeck.Text & "', "
l_strSQL = l_strSQL & "PipelineAudioTracks = '" & .cmbAudioTracks.Text & "', "
l_strSQL = l_strSQL & "PipelineAudioBitDepth = '" & .cmbAudioTrackDepth.Text & "', "

If Val(.lblLibraryID.Caption) = 284349 Or Val(.lblLibraryID.Caption) = 248095 Then
    l_strSQL = l_strSQL & "MediaWindowDeleteDate = Null, "
    l_strSQL = l_strSQL & "hidefromweb = '0' "
Else
    If .chkHideFromWeb.Value = 0 Then l_strSQL = l_strSQL & "MediaWindowDeleteDate = Null, "
    l_strSQL = l_strSQL & "hidefromweb = '" & .chkHideFromWeb.Value & "' "
End If

'Finally we finish the save
l_strSQL = l_strSQL & "WHERE eventID = " & l_lngClipID & ";"

End With

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

Dim l_strTemp As String
l_strTemp = GetData("library", "format", "libraryID", Val(frmClipControl.lblLibraryID.Caption))
If l_strTemp = "DISCSTORE" Then
    SetData "events", "online", "eventID", l_lngClipID, 1
Else
    SetData "events", "online", "eventID", l_lngClipID, 0
End If

''Display the DIVA Category
'Set l_rstData = ExecuteSQL("exec cet_Get_DIVA_Category @EventID=" & l_lngClipID, g_strExecuteError)
'CheckForSQLError
'SetData "events", "clipsoundformat", "eventID", l_lngClipID, l_rstData(0)
'l_rstData.Close
'
'Record a History event
l_strSQL = "INSERT INTO eventhistory ("
l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
l_strSQL = l_strSQL & "'" & l_lngClipID & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
l_strSQL = l_strSQL & "'Clip Saved' "
l_strSQL = l_strSQL & ");"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

SaveClipBackground = l_lngClipID

End Function

Public Sub ShowClipControlFromPortalControl(lp_cntGrid As Control)

Dim l_lngClipID As Long

If lp_cntGrid.Columns("eventID").Text <> "" Then

    l_lngClipID = Val(lp_cntGrid.Columns("eventID").Text)

'    frmClipPortalUsers.Hide

'    frmClipControl.cmdBackToPortal.Visible = True
    ShowClipControl l_lngClipID

End If

End Sub

Public Function GetFullPathAndFilename(lp_lngClipID As Long) As String

Dim l_strPath As String, l_strTemp As String

l_strPath = GetData("library", "subtitle", "libraryID", GetData("events", "libraryID", "eventID", lp_lngClipID))
l_strTemp = GetData("events", "altlocation", "eventID", lp_lngClipID)
If l_strTemp <> "" Then l_strPath = l_strPath & "\" & l_strTemp
l_strTemp = GetData("events", "clipfilename", "eventID", lp_lngClipID)
l_strPath = l_strPath & "\" & l_strTemp
Debug.Print l_strPath
GetFullPathAndFilename = l_strPath

End Function

Public Sub ShowFileTechRev(lp_lngTechrevID As Long)

Dim l_rstTechRev As ADODB.Recordset
Dim l_strSQL As String

l_strSQL = "SELECT * FROM techrev WHERE system_deleted = 0 AND techrevID = " & lp_lngTechrevID
Set l_rstTechRev = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError
If l_rstTechRev.EOF = True Then
    l_rstTechRev.Close
    Set l_rstTechRev = Nothing
    Exit Sub
Else
    l_rstTechRev.MoveFirst
    With frmFileTechRev
    
        .adoTechReviews.RecordSource = "SELECT techrevID FROM techrev WHERE system_deleted = 0 AND companyID = " & Val(frmClipControl.lblCompanyID.Caption) & " AND filename = '" & frmClipControl.txtClipfilename.Text & "'" & IIf(frmClipControl.lblFileSize.Caption <> "", " AND bigfilesize = " & Format(frmClipControl.lblFileSize.Caption, "#"), "") & IIf(frmClipControl.txtMD5Checksum.Text <> "", " AND md5checksum = '" & frmClipControl.txtMD5Checksum.Text & "'", "") & ";"
        .adoTechReviews.ConnectionString = g_strConnection
        .adoTechReviews.Refresh
        If .adoTechReviews.Recordset.RecordCount > 0 Then
            .adoTechReviews.Recordset.MoveLast
            .lblLastTechrevID.Caption = .adoTechReviews.Recordset("techrevID")
        End If
        .lbltechrevID.Caption = l_rstTechRev("techrevID")
        .cmbTechReviewList.Text = CStr(l_rstTechRev("techrevID"))
        
        If Trim(" " & l_rstTechRev("companyname")) <> "" Then
            .cmbReviewCompany.Text = Trim(" " & l_rstTechRev("companyname"))
        Else
            .cmbReviewCompany.Text = frmClipControl.cmbCompany.Text
        End If
        If Val(Trim(" " & l_rstTechRev("CompanyID"))) <> 0 Then
            .lblReviewCompanyID.Caption = l_rstTechRev("companyID")
        Else
            .lblReviewCompanyID.Caption = frmClipControl.lblCompanyID.Caption
        End If
        .cmbReviewContact.Text = Trim(" " & l_rstTechRev("contactname"))
        If l_rstTechRev("contactID") <> Null Then .lblReviewContactID.Caption = l_rstTechRev("contactID")
        .datReviewDate.Value = Format(l_rstTechRev("reviewdate"), vbShortDateFormat)
        .cmbReviewMachine.Text = Trim(" " & l_rstTechRev("reviewmachine"))
        .txtReviewJobID.Text = Trim(" " & l_rstTechRev("jobID"))
        .cmbReviewUser.Text = Trim(" " & l_rstTechRev("reviewuser"))
        .chkTitles.Value = GetFlag(l_rstTechRev("flagtitles"))
        .chkCaptions.Value = GetFlag(l_rstTechRev("flagcaptions"))
        .chkSubtitles.Value = GetFlag(l_rstTechRev("flagsubtitles"))
        .chkForcedNarratives.Value = GetFlag(l_rstTechRev("flagforcednarratives"))
        .chkLogos.Value = GetFlag(l_rstTechRev("flaglogos"))
        .chkTextless.Value = GetFlag(l_rstTechRev("flagtextless"))
        .chkTrailers.Value = GetFlag(l_rstTechRev("flagtrailers"))
        .chkSpotCheckRev.Value = GetFlag(l_rstTechRev("fullqcreview"))
        .chk5PointCheck.Value = GetFlag(l_rstTechRev("FivePointCheckReview"))
        .chkiTunesReview.Value = GetFlag(l_rstTechRev("iTunesReview"))
        .chkiTunesFinal.Value = GetFlag(l_rstTechRev("iTunesFinal"))
        .chkIdentInfoCorrect.Value = GetFlag(l_rstTechRev("identinfocorrect"))
        .chkConvertedMaterial.Value = GetFlag(l_rstTechRev("convertedmaterial"))
        .cmbTitlesLanguage.Text = Trim(" " & l_rstTechRev("languagetitles"))
        .cmbCaptionsLanguage.Text = Trim(" " & l_rstTechRev("languagecaptions"))
        .cmbSubtitlesLanguage.Text = Trim(" " & l_rstTechRev("languagesubtitles"))
        .cmbForcedLanguage.Text = Trim(" " & l_rstTechRev("languageforcednarratives"))
        .txtDetailsLogos.Text = Trim(" " & l_rstTechRev("detailslogos"))
        .txtDetailsTextless.Text = Trim(" " & l_rstTechRev("detailstextless"))
        .txtDetailsTrailers.Text = Trim(" " & l_rstTechRev("detailstrailers"))
        .txtVideoTest.Text = Trim(" " & l_rstTechRev("videotest"))
        .txtChromaTest.Text = Trim(" " & l_rstTechRev("chromatest"))
        .txtBlackTest.Text = Trim(" " & l_rstTechRev("blacktest"))
        .txtVideoProg.Text = Trim(" " & l_rstTechRev("videoprog"))
        .txtChromaProg.Text = Trim(" " & l_rstTechRev("chromaprog"))
        .txtBlackProg.Text = Trim(" " & l_rstTechRev("blackprog"))
        .txtA1Test.Text = Trim(" " & l_rstTechRev("audio1test"))
        .txtA2Test.Text = Trim(" " & l_rstTechRev("audio2test"))
        .txtA3Test.Text = Trim(" " & l_rstTechRev("audio3test"))
        .txtA4Test.Text = Trim(" " & l_rstTechRev("audio4test"))
        .txtA5Test.Text = Trim(" " & l_rstTechRev("audio5test"))
        .txtA6Test.Text = Trim(" " & l_rstTechRev("audio6test"))
        .txtA7Test.Text = Trim(" " & l_rstTechRev("audio7test"))
        .txtA8Test.Text = Trim(" " & l_rstTechRev("audio8test"))
        .txtA9Test.Text = Trim(" " & l_rstTechRev("audio9test"))
        .txtA10Test.Text = Trim(" " & l_rstTechRev("audio10test"))
        .txtA11Test.Text = Trim(" " & l_rstTechRev("audio11test"))
        .txtA12Test.Text = Trim(" " & l_rstTechRev("audio12test"))
        .txtA13Test.Text = Trim(" " & l_rstTechRev("audio12test"))
        .txtA14Test.Text = Trim(" " & l_rstTechRev("audio12test"))
        .txtA15Test.Text = Trim(" " & l_rstTechRev("audio12test"))
        .txtA16Test.Text = Trim(" " & l_rstTechRev("audio12test"))
        .txtA17Test.Text = Trim(" " & l_rstTechRev("audio12test"))
        .txtA18Test.Text = Trim(" " & l_rstTechRev("audio12test"))
        .txtA19Test.Text = Trim(" " & l_rstTechRev("audio12test"))
        .txtA20Test.Text = Trim(" " & l_rstTechRev("audio12test"))
        .txtA21Test.Text = Trim(" " & l_rstTechRev("audio12test"))
        .txtA22Test.Text = Trim(" " & l_rstTechRev("audio12test"))
        .txtA23Test.Text = Trim(" " & l_rstTechRev("audio12test"))
        .txtA24Test.Text = Trim(" " & l_rstTechRev("audio12test"))
        .txtA1Prog.Text = Trim(" " & l_rstTechRev("audio1prog"))
        .txtA2Prog.Text = Trim(" " & l_rstTechRev("audio2prog"))
        .txtA3Prog.Text = Trim(" " & l_rstTechRev("audio3prog"))
        .txtA4Prog.Text = Trim(" " & l_rstTechRev("audio4prog"))
        .txtA5Prog.Text = Trim(" " & l_rstTechRev("audio5prog"))
        .txtA6Prog.Text = Trim(" " & l_rstTechRev("audio6prog"))
        .txtA7Prog.Text = Trim(" " & l_rstTechRev("audio7prog"))
        .txtA8Prog.Text = Trim(" " & l_rstTechRev("audio8prog"))
        .txtA9Prog.Text = Trim(" " & l_rstTechRev("audio9prog"))
        .txtA10Prog.Text = Trim(" " & l_rstTechRev("audio10prog"))
        .txtA11Prog.Text = Trim(" " & l_rstTechRev("audio11prog"))
        .txtA12Prog.Text = Trim(" " & l_rstTechRev("audio12prog"))
        .txtA13Prog.Text = Trim(" " & l_rstTechRev("audio13prog"))
        .txtA14Prog.Text = Trim(" " & l_rstTechRev("audio14prog"))
        .txtA15Prog.Text = Trim(" " & l_rstTechRev("audio15prog"))
        .txtA16Prog.Text = Trim(" " & l_rstTechRev("audio16prog"))
        .txtA17Prog.Text = Trim(" " & l_rstTechRev("audio17prog"))
        .txtA18Prog.Text = Trim(" " & l_rstTechRev("audio18prog"))
        .txtA19Prog.Text = Trim(" " & l_rstTechRev("audio19prog"))
        .txtA20Prog.Text = Trim(" " & l_rstTechRev("audio20prog"))
        .txtA21Prog.Text = Trim(" " & l_rstTechRev("audio21prog"))
        .txtA22Prog.Text = Trim(" " & l_rstTechRev("audio22prog"))
        .txtA23Prog.Text = Trim(" " & l_rstTechRev("audio23prog"))
        .txtA24Prog.Text = Trim(" " & l_rstTechRev("audio24prog"))
        .cmbEBU128LoudnessSet1.Text = Trim(" " & l_rstTechRev("EBU128LoudnessSet1"))
        .cmbEBU128LoudnessSet2.Text = Trim(" " & l_rstTechRev("EBU128LoudnessSet2"))
        .cmbEBU128LoudnessSet3.Text = Trim(" " & l_rstTechRev("EBU128LoudnessSet3"))
        .cmbEBU128LoudnessSet4.Text = Trim(" " & l_rstTechRev("EBU128LoudnessSet4"))
        .cmbEBU128LoudnessSet5.Text = Trim(" " & l_rstTechRev("EBU128LoudnessSet5"))
        .cmbEBU128LoudnessSet6.Text = Trim(" " & l_rstTechRev("EBU128LoudnessSet6"))
        .cmbEBU128LoudnessSet7.Text = Trim(" " & l_rstTechRev("EBU128LoudnessSet7"))
        .cmbEBU128LoudnessSet8.Text = Trim(" " & l_rstTechRev("EBU128LoudnessSet8"))
'        .cmbEBU128LoudnessSet9.Text = Trim(" " & l_rstTechRev("EBU128LoudnessSet9"))
'        .cmbEBU128LoudnessSet10.Text = Trim(" " & l_rstTechRev("EBU128LoudnessSet10"))
        .cmbEBU128TruePeakSet1.Text = Trim(" " & l_rstTechRev("EBU128TruePeakSet1"))
        .cmbEBU128TruePeakSet2.Text = Trim(" " & l_rstTechRev("EBU128TruePeakSet2"))
        .cmbEBU128TruePeakSet3.Text = Trim(" " & l_rstTechRev("EBU128TruePeakSet3"))
        .cmbEBU128TruePeakSet4.Text = Trim(" " & l_rstTechRev("EBU128TruePeakSet4"))
        .cmbEBU128TruePeakSet5.Text = Trim(" " & l_rstTechRev("EBU128TruePeakSet5"))
        .cmbEBU128TruePeakSet6.Text = Trim(" " & l_rstTechRev("EBU128TruePeakSet6"))
        .cmbEBU128TruePeakSet7.Text = Trim(" " & l_rstTechRev("EBU128TruePeakSet7"))
        .cmbEBU128TruePeakSet8.Text = Trim(" " & l_rstTechRev("EBU128TruePeakSet8"))
'        .cmbEBU128TruePeakSet9.Text = Trim(" " & l_rstTechRev("EBU128TruePeakSet9"))
'        .cmbEBU128TruePeakSet10.Text = Trim(" " & l_rstTechRev("EBU128TruePeakSet10"))
        .cmbEBU128LoudnessRangeSet1.Text = Trim(" " & l_rstTechRev("EBU128LoudnessRangeSet1"))
        .cmbEBU128LoudnessRangeSet2.Text = Trim(" " & l_rstTechRev("EBU128LoudnessRangeSet2"))
        .cmbEBU128LoudnessRangeSet3.Text = Trim(" " & l_rstTechRev("EBU128LoudnessRangeSet3"))
        .cmbEBU128LoudnessRangeSet4.Text = Trim(" " & l_rstTechRev("EBU128LoudnessRangeSet4"))
        .cmbEBU128LoudnessRangeSet5.Text = Trim(" " & l_rstTechRev("EBU128LoudnessRangeSet5"))
        .cmbEBU128LoudnessRangeSet6.Text = Trim(" " & l_rstTechRev("EBU128LoudnessRangeSet6"))
        .cmbEBU128LoudnessRangeSet7.Text = Trim(" " & l_rstTechRev("EBU128LoudnessRangeSet7"))
        .cmbEBU128LoudnessRangeSet8.Text = Trim(" " & l_rstTechRev("EBU128LoudnessRangeSet8"))
'        .cmbEBU128LoudnessRangeSet9.Text = Trim(" " & l_rstTechRev("EBU128LoudnessRangeSet9"))
'        .cmbEBU128LoudnessRangeSet10.Text = Trim(" " & l_rstTechRev("EBU128LoudnessRangeSet10"))
        .cmbEBU128MaxShortLoundnessSet1.Text = Trim(" " & l_rstTechRev("EBU128MaxShortLoundnessSet1"))
        .cmbEBU128MaxShortLoundnessSet2.Text = Trim(" " & l_rstTechRev("EBU128MaxShortLoundnessSet2"))
        .cmbEBU128MaxShortLoundnessSet3.Text = Trim(" " & l_rstTechRev("EBU128MaxShortLoundnessSet3"))
        .cmbEBU128MaxShortLoundnessSet4.Text = Trim(" " & l_rstTechRev("EBU128MaxShortLoundnessSet4"))
        .cmbEBU128MaxShortLoundnessSet5.Text = Trim(" " & l_rstTechRev("EBU128MaxShortLoundnessSet5"))
        .cmbEBU128MaxShortLoundnessSet6.Text = Trim(" " & l_rstTechRev("EBU128MaxShortLoundnessSet6"))
        .cmbEBU128MaxShortLoundnessSet7.Text = Trim(" " & l_rstTechRev("EBU128MaxShortLoundnessSet7"))
        .cmbEBU128MaxShortLoundnessSet8.Text = Trim(" " & l_rstTechRev("EBU128MaxShortLoundnessSet8"))
'        .cmbEBU128MaxShortLoundnessSet9.Text = Trim(" " & l_rstTechRev("EBU128MaxShortLoundnessSet9"))
'        .cmbEBU128MaxShortLoundnessSet10.Text = Trim(" " & l_rstTechRev("EBU128MaxShortLoundnessSet10"))
        .cmbiTunesVideoType.Text = Trim(" " & l_rstTechRev("iTunesVideoType"))
        .txtiTunesVendorID.Text = Trim(" " & l_rstTechRev("iTunesVendorID"))
        .txtHighestAudioPeak.Text = Trim(" " & l_rstTechRev("highestaudiopeak"))
        .chkCaptionsafe43ebu.Value = GetFlag(l_rstTechRev("captionsafe43ebu"))
        .chkCaptionsafe43protect.Value = GetFlag(l_rstTechRev("captionsafe43protect"))
        .chkCaptionsafe149.Value = GetFlag(l_rstTechRev("captionsafe149"))
        .chkCaptionsafe169.Value = GetFlag(l_rstTechRev("captionsafe169"))
        .chkActionsafe169.Value = GetFlag(l_rstTechRev("actionsafe169"))
        .chkActionsafe149.Value = GetFlag(l_rstTechRev("actionsafe149"))
        .chkActionsafe43.Value = GetFlag(l_rstTechRev("actionsafe43"))
        .chkFileTimecode.Value = GetFlag(l_rstTechRev("ltc"))
        .chkvitc.Value = GetFlag(l_rstTechRev("vitc"))
        .chkltcvitcmatch.Value = GetFlag(l_rstTechRev("ltcvitcmatch"))
        .chkContinuous.Value = GetFlag(l_rstTechRev("timecodecontinuous"))
        .txtvitclines.Text = Trim(" " & l_rstTechRev("vitclines"))
        .txtfirstactivehorizpixel.Text = Trim(" " & l_rstTechRev("firstactivehorizpixel"))
        .txtlastactivehorizpixel.Text = Trim(" " & l_rstTechRev("lastactivehorizpixel"))
        .txtfirstactivevertpixel.Text = Trim(" " & l_rstTechRev("firstactivevertpixel"))
        .txtlastactivevertpixel.Text = Trim(" " & l_rstTechRev("lastactivevertpixel"))
        .chkhorizontalcenteringcorrect.Value = GetFlag(l_rstTechRev("horizontalcentreingcorrect"))
        .chkverticalcenteringcorrect.Value = GetFlag(l_rstTechRev("verticalcentreingcorrect"))
        .chkAudioPhaseError.Value = GetFlag(l_rstTechRev("AudioPhaseError"))
        .txtTechNotesVideo.Text = Trim(" " & l_rstTechRev("notesvideo"))
        .txtTechNotesAudio.Text = Trim(" " & l_rstTechRev("notesaudio"))
        .txtTechNotesGeneral.Text = Trim(" " & l_rstTechRev("notesgeneral"))
        .optReviewConclusion(l_rstTechRev("reviewconclusion")).Value = True
        .optR128Summary(IIf(l_rstTechRev("R128SummaryPass"), 1, 0)).Value = True
        If Val(Trim(" " & l_rstTechRev("passedforitunes"))) = 1 Then
            .optReviewConclusion(4).Value = True
        Else
            .optReviewConclusion(3).Value = True
        End If
        
        If .chkSpotCheckRev.Value <> 0 Then
            FileFullQCFieldsOn
        Else
            FileFullQCFieldsOff
        End If
        
    End With
    l_rstTechRev.Close
    Set l_rstTechRev = Nothing
        
    If frmFileTechRev.lbltechrevID.Caption = frmFileTechRev.lblLastTechrevID.Caption Then
            
        frmFileTechRev.grdFaults.Visible = True
        frmFileTechRev.grdFaults.Enabled = True
        frmFileTechRev.fraTechReview.Visible = True
        frmFileTechRev.fraTechReview.Enabled = True
        frmFileTechRev.cmdMakeTechReview.Enabled = True
        frmFileTechRev.cmdDropTechReview.Enabled = True
        frmFileTechRev.cmdTechReviewPdf.Enabled = True
        frmFileTechRev.cmdTechReviewCopyFaults.Enabled = True
        frmFileTechRev.fraTechReviewConclusions.Visible = True
        frmFileTechRev.fraTechReviewConclusions.Enabled = True
        frmFileTechRev.lbltechrevID.Visible = True
        frmFileTechRev.lblLastTechrevID.Visible = True
    
    Else
    
        frmFileTechRev.grdFaults.Visible = True
        frmFileTechRev.grdFaults.Enabled = False
        frmFileTechRev.fraTechReview.Visible = True
        frmFileTechRev.fraTechReview.Enabled = False
        frmFileTechRev.cmdMakeTechReview.Enabled = False
        frmFileTechRev.cmdDropTechReview.Enabled = True
        frmFileTechRev.cmdTechReviewPdf.Enabled = True
        frmFileTechRev.cmdTechReviewCopyFaults.Enabled = True
        frmFileTechRev.fraTechReviewConclusions.Visible = True
        frmFileTechRev.fraTechReviewConclusions.Enabled = False
        frmFileTechRev.lbltechrevID.Visible = True
        frmFileTechRev.lblLastTechrevID.Visible = True
    
    End If
                
    'Load the tech review Faults grid.
    l_strSQL = "SELECT * FROM techrevfault WHERE techrevID = '" & lp_lngTechrevID & "' ORDER BY timecode"
    
    frmFileTechRev.adoFaults.RecordSource = l_strSQL
    frmFileTechRev.adoFaults.ConnectionString = g_strConnection
    frmFileTechRev.adoFaults.Refresh
    If CheckAccess("/savelibrary", True) = True Then frmFileTechRev.grdFaults.Enabled = True Else frmFileTechRev.grdFaults.Enabled = False
    If InStr(GetData("company", "cetaclientcode", "companyID", frmFileTechRev.lblReviewCompanyID.Caption), "/disneyqc") > 0 Then
        frmFileTechRev.txtNormalGrading.Visible = False
        frmFileTechRev.txtDisneyFaultGrading.Visible = True
    Else
        frmFileTechRev.txtNormalGrading.Visible = True
        frmFileTechRev.txtDisneyFaultGrading.Visible = False
    End If
End If

End Sub

Function MakeFileTechReview()
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/makefiletechrev") Then
        Exit Function
    End If
    
    frmFileTechRev.MousePointer = vbHourglass
    
    Dim l_strSQL As String, l_rsOldFaults As ADODB.Recordset
    Dim l_lngNewTechrevID As Long
    
    With frmFileTechRev
        
        l_lngNewTechrevID = Val(InsertTechReview(Val(frmClipControl.txtClipID.Text)))
        If Val(.lbltechrevID.Caption) <> 0 Then
            'Its an existing techreview being re-reviewed - copy all existing fields and fault list to a new techrev.
            
            l_strSQL = "UPDATE techrev SET "
            l_strSQL = l_strSQL & "jobID = '" & .txtReviewJobID.Text & "', "
            l_strSQL = l_strSQL & "reviewdate = '" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "reviewuser = '" & g_strFullUserName & "', "
            l_strSQL = l_strSQL & "companyID = '" & .lblReviewCompanyID.Caption & "', "
            l_strSQL = l_strSQL & "companyname = '" & QuoteSanitise(.cmbReviewCompany.Text) & "', "
            l_strSQL = l_strSQL & "contactID = '" & .lblReviewContactID.Caption & "', "
            l_strSQL = l_strSQL & "contactname = '" & .cmbReviewContact & "', "
            l_strSQL = l_strSQL & "flagtitles = '" & .chkTitles.Value & "', "
            l_strSQL = l_strSQL & "flagcaptions = '" & .chkCaptions.Value & "', "
            l_strSQL = l_strSQL & "flagsubtitles = '" & .chkSubtitles.Value & "', "
            l_strSQL = l_strSQL & "flaglogos = '" & .chkLogos.Value & "', "
            l_strSQL = l_strSQL & "flagtextless = '" & .chkTextless.Value & "', "
            l_strSQL = l_strSQL & "flagtrailers = '" & .chkTrailers.Value & "', "
            l_strSQL = l_strSQL & "languagetitles = '" & .cmbTitlesLanguage.Text & "', "
            l_strSQL = l_strSQL & "languagecaptions = '" & .cmbCaptionsLanguage.Text & "', "
            l_strSQL = l_strSQL & "languagesubtitles = '" & .cmbSubtitlesLanguage.Text & "', "
            l_strSQL = l_strSQL & "detailslogos = '" & .txtDetailsLogos.Text & "', "
            l_strSQL = l_strSQL & "detailstextless = '" & .txtDetailsTextless.Text & "', "
            l_strSQL = l_strSQL & "detailstrailers = '" & .txtDetailsTrailers.Text & "', "
            l_strSQL = l_strSQL & "reviewmachine = '" & .cmbReviewMachine.Text & "', "
            l_strSQL = l_strSQL & "notesvideo = '" & QuoteSanitise(.txtTechNotesVideo.Text) & "', "
            l_strSQL = l_strSQL & "notesaudio = '" & QuoteSanitise(.txtTechNotesAudio.Text) & "', "
            l_strSQL = l_strSQL & "notesgeneral = '" & QuoteSanitise(.txtTechNotesGeneral.Text) & "', "
            If .optReviewConclusion(0).Value = True Then
                l_strSQL = l_strSQL & "reviewconclusion = '" & 0 & "', "
            End If
            If .optReviewConclusion(1).Value = True Then
                l_strSQL = l_strSQL & "reviewconclusion = '" & 1 & "', "
            End If
            If .optReviewConclusion(2).Value = True Then
                l_strSQL = l_strSQL & "reviewconclusion = '" & 2 & "', "
            End If
            l_strSQL = l_strSQL & "videotest = '" & .txtVideoTest.Text & "', "
            l_strSQL = l_strSQL & "videoprog = '" & .txtVideoProg.Text & "', "
            l_strSQL = l_strSQL & "chromatest = '" & .txtChromaTest.Text & "', "
            l_strSQL = l_strSQL & "chromaprog = '" & .txtChromaProg.Text & "', "
            l_strSQL = l_strSQL & "blacktest = '" & .txtBlackTest.Text & "', "
            l_strSQL = l_strSQL & "blackprog = '" & .txtBlackProg.Text & "', "
            l_strSQL = l_strSQL & "audio1test = '" & .txtA1Test.Text & "', "
            l_strSQL = l_strSQL & "audio1prog = '" & .txtA1Prog.Text & "', "
            l_strSQL = l_strSQL & "audio2test = '" & .txtA2Test.Text & "', "
            l_strSQL = l_strSQL & "audio2prog = '" & .txtA2Prog.Text & "', "
            l_strSQL = l_strSQL & "audio3test = '" & .txtA3Test.Text & "', "
            l_strSQL = l_strSQL & "audio3prog = '" & .txtA3Prog.Text & "', "
            l_strSQL = l_strSQL & "audio4test = '" & .txtA4Test.Text & "', "
            l_strSQL = l_strSQL & "audio4prog = '" & .txtA4Prog.Text & "', "
            l_strSQL = l_strSQL & "audio5test = '" & .txtA5Test.Text & "', "
            l_strSQL = l_strSQL & "audio5prog = '" & .txtA5Prog.Text & "', "
            l_strSQL = l_strSQL & "audio6test = '" & .txtA6Test.Text & "', "
            l_strSQL = l_strSQL & "audio6prog = '" & .txtA6Prog.Text & "', "
            l_strSQL = l_strSQL & "audio7test = '" & .txtA7Test.Text & "', "
            l_strSQL = l_strSQL & "audio7prog = '" & .txtA7Prog.Text & "', "
            l_strSQL = l_strSQL & "audio8test = '" & .txtA8Test.Text & "', "
            l_strSQL = l_strSQL & "audio8prog = '" & .txtA8Prog.Text & "', "
            l_strSQL = l_strSQL & "audio9test = '" & .txtA9Test.Text & "', "
            l_strSQL = l_strSQL & "audio9prog = '" & .txtA9Prog.Text & "', "
            l_strSQL = l_strSQL & "audio10test = '" & .txtA10Test.Text & "', "
            l_strSQL = l_strSQL & "audio10prog = '" & .txtA10Prog.Text & "', "
            l_strSQL = l_strSQL & "audio11test = '" & .txtA11Test.Text & "', "
            l_strSQL = l_strSQL & "audio11prog = '" & .txtA11Prog.Text & "', "
            l_strSQL = l_strSQL & "audio12test = '" & .txtA12Test.Text & "', "
            l_strSQL = l_strSQL & "audio12prog = '" & .txtA12Prog.Text & "', "
            l_strSQL = l_strSQL & "audio13test = '" & .txtA13Test.Text & "', "
            l_strSQL = l_strSQL & "audio13prog = '" & .txtA13Prog.Text & "', "
            l_strSQL = l_strSQL & "audio14test = '" & .txtA14Test.Text & "', "
            l_strSQL = l_strSQL & "audio14prog = '" & .txtA14Prog.Text & "', "
            l_strSQL = l_strSQL & "audio15test = '" & .txtA15Test.Text & "', "
            l_strSQL = l_strSQL & "audio15prog = '" & .txtA15Prog.Text & "', "
            l_strSQL = l_strSQL & "audio16test = '" & .txtA16Test.Text & "', "
            l_strSQL = l_strSQL & "audio16prog = '" & .txtA16Prog.Text & "', "
            l_strSQL = l_strSQL & "audio17test = '" & .txtA17Test.Text & "', "
            l_strSQL = l_strSQL & "audio17prog = '" & .txtA17Prog.Text & "', "
            l_strSQL = l_strSQL & "audio18test = '" & .txtA18Test.Text & "', "
            l_strSQL = l_strSQL & "audio18prog = '" & .txtA18Prog.Text & "', "
            l_strSQL = l_strSQL & "audio19test = '" & .txtA19Test.Text & "', "
            l_strSQL = l_strSQL & "audio19prog = '" & .txtA19Prog.Text & "', "
            l_strSQL = l_strSQL & "audio20test = '" & .txtA20Test.Text & "', "
            l_strSQL = l_strSQL & "audio20prog = '" & .txtA20Prog.Text & "', "
            l_strSQL = l_strSQL & "audio21test = '" & .txtA21Test.Text & "', "
            l_strSQL = l_strSQL & "audio21prog = '" & .txtA21Prog.Text & "', "
            l_strSQL = l_strSQL & "audio22test = '" & .txtA22Test.Text & "', "
            l_strSQL = l_strSQL & "audio22prog = '" & .txtA22Prog.Text & "', "
            l_strSQL = l_strSQL & "audio23test = '" & .txtA23Test.Text & "', "
            l_strSQL = l_strSQL & "audio23prog = '" & .txtA23Prog.Text & "', "
            l_strSQL = l_strSQL & "audio24test = '" & .txtA24Test.Text & "', "
            l_strSQL = l_strSQL & "audio24prog = '" & .txtA24Prog.Text & "', "
            l_strSQL = l_strSQL & "EBU128LoudnessSet1 = '" & .cmbEBU128LoudnessSet1.Text & "', "
            l_strSQL = l_strSQL & "EBU128LoudnessSet2 = '" & .cmbEBU128LoudnessSet2.Text & "', "
            l_strSQL = l_strSQL & "EBU128LoudnessSet3 = '" & .cmbEBU128LoudnessSet3.Text & "', "
            l_strSQL = l_strSQL & "EBU128LoudnessSet4 = '" & .cmbEBU128LoudnessSet4.Text & "', "
            l_strSQL = l_strSQL & "EBU128LoudnessSet5 = '" & .cmbEBU128LoudnessSet5.Text & "', "
            l_strSQL = l_strSQL & "EBU128LoudnessSet6 = '" & .cmbEBU128LoudnessSet6.Text & "', "
            l_strSQL = l_strSQL & "EBU128LoudnessSet7 = '" & .cmbEBU128LoudnessSet7.Text & "', "
            l_strSQL = l_strSQL & "EBU128LoudnessSet8 = '" & .cmbEBU128LoudnessSet8.Text & "', "
'            l_strSQL = l_strSQL & "EBU128LoudnessSet9 = '" & .cmbEBU128LoudnessSet9.Text & "', "
'            l_strSQL = l_strSQL & "EBU128LoudnessSet10 = '" & .cmbEBU128LoudnessSet10.Text & "', "
            l_strSQL = l_strSQL & "EBU128TruePeakSet1 = '" & .cmbEBU128TruePeakSet1.Text & "', "
            l_strSQL = l_strSQL & "EBU128TruePeakSet2 = '" & .cmbEBU128TruePeakSet2.Text & "', "
            l_strSQL = l_strSQL & "EBU128TruePeakSet3 = '" & .cmbEBU128TruePeakSet3.Text & "', "
            l_strSQL = l_strSQL & "EBU128TruePeakSet4 = '" & .cmbEBU128TruePeakSet4.Text & "', "
            l_strSQL = l_strSQL & "EBU128TruePeakSet5 = '" & .cmbEBU128TruePeakSet5.Text & "', "
            l_strSQL = l_strSQL & "EBU128TruePeakSet6 = '" & .cmbEBU128TruePeakSet6.Text & "', "
            l_strSQL = l_strSQL & "EBU128TruePeakSet7 = '" & .cmbEBU128TruePeakSet7.Text & "', "
            l_strSQL = l_strSQL & "EBU128TruePeakSet8 = '" & .cmbEBU128TruePeakSet8.Text & "', "
'            l_strSQL = l_strSQL & "EBU128TruePeakSet9 = '" & .cmbEBU128TruePeakSet9.Text & "', "
'            l_strSQL = l_strSQL & "EBU128TruePeakSet10 = '" & .cmbEBU128TruePeakSet10.Text & "', "
            l_strSQL = l_strSQL & "EBU128LoudnessRangeSet1 = '" & .cmbEBU128LoudnessRangeSet1.Text & "', "
            l_strSQL = l_strSQL & "EBU128LoudnessRangeSet2 = '" & .cmbEBU128LoudnessRangeSet2.Text & "', "
            l_strSQL = l_strSQL & "EBU128LoudnessRangeSet3 = '" & .cmbEBU128LoudnessRangeSet3.Text & "', "
            l_strSQL = l_strSQL & "EBU128LoudnessRangeSet4 = '" & .cmbEBU128LoudnessRangeSet4.Text & "', "
            l_strSQL = l_strSQL & "EBU128LoudnessRangeSet5 = '" & .cmbEBU128LoudnessRangeSet5.Text & "', "
            l_strSQL = l_strSQL & "EBU128LoudnessRangeSet6 = '" & .cmbEBU128LoudnessRangeSet6.Text & "', "
            l_strSQL = l_strSQL & "EBU128LoudnessRangeSet7 = '" & .cmbEBU128LoudnessRangeSet7.Text & "', "
            l_strSQL = l_strSQL & "EBU128LoudnessRangeSet8 = '" & .cmbEBU128LoudnessRangeSet8.Text & "', "
'            l_strSQL = l_strSQL & "EBU128LoudnessRangeSet9 = '" & .cmbEBU128LoudnessRangeSet9.Text & "', "
'            l_strSQL = l_strSQL & "EBU128LoudnessRangeSet10 = '" & .cmbEBU128LoudnessRangeSet10.Text & "', "
            l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet1 = '" & .cmbEBU128MaxShortLoundnessSet1.Text & "', "
            l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet2 = '" & .cmbEBU128MaxShortLoundnessSet2.Text & "', "
            l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet3 = '" & .cmbEBU128MaxShortLoundnessSet3.Text & "', "
            l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet4 = '" & .cmbEBU128MaxShortLoundnessSet4.Text & "', "
            l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet5 = '" & .cmbEBU128MaxShortLoundnessSet5.Text & "', "
            l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet6 = '" & .cmbEBU128MaxShortLoundnessSet6.Text & "', "
            l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet7 = '" & .cmbEBU128MaxShortLoundnessSet7.Text & "', "
            l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet8 = '" & .cmbEBU128MaxShortLoundnessSet8.Text & "', "
'            l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet9 = '" & .cmbEBU128MaxShortLoundnessSet9.Text & "', "
'            l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet10 = '" & .cmbEBU128MaxShortLoundnessSet10.Text & "', "
            l_strSQL = l_strSQL & "highestaudiopeak = '" & .txtHighestAudioPeak.Text & "', "
            l_strSQL = l_strSQL & "vitclines = '" & .txtvitclines.Text & "', "
            l_strSQL = l_strSQL & "timecodecontinuous = '" & .chkContinuous.Value & "', "
            l_strSQL = l_strSQL & "ltc = '" & .chkFileTimecode.Value & "', "
            l_strSQL = l_strSQL & "vitc = '" & .chkvitc.Value & "', "
            l_strSQL = l_strSQL & "ltcvitcmatch = '" & .chkltcvitcmatch.Value & "', "
            l_strSQL = l_strSQL & "firstactivehorizpixel = '" & .txtfirstactivehorizpixel.Text & "', "
            l_strSQL = l_strSQL & "lastactivehorizpixel = '" & .txtlastactivehorizpixel.Text & "', "
            l_strSQL = l_strSQL & "firstactivevertpixel = '" & .txtfirstactivevertpixel.Text & "', "
            l_strSQL = l_strSQL & "lastactivevertpixel = '" & .txtlastactivevertpixel.Text & "', "
            l_strSQL = l_strSQL & "verticalcentreingcorrect = '" & .chkverticalcenteringcorrect.Value & "', "
            l_strSQL = l_strSQL & "horizontalcentreingcorrect = '" & .chkhorizontalcenteringcorrect.Value & "', "
            l_strSQL = l_strSQL & "captionsafe169 = '" & .chkCaptionsafe169.Value & "', "
            l_strSQL = l_strSQL & "captionsafe149 = '" & .chkCaptionsafe149.Value & "', "
            l_strSQL = l_strSQL & "captionsafe43protect = '" & .chkCaptionsafe43protect.Value & "', "
            l_strSQL = l_strSQL & "captionsafe43ebu = '" & .chkCaptionsafe43ebu.Value & "' "
            l_strSQL = l_strSQL & "WHERE techrevID = " & l_lngNewTechrevID
            
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            
            Set l_rsOldFaults = ExecuteSQL("SELECT * FROM techrevfault WHERE techrevID = '" & .lbltechrevID.Caption & "';", g_strExecuteError)
            CheckForSQLError
            
            If l_rsOldFaults.RecordCount > 0 Then
                l_rsOldFaults.MoveFirst
                Do While Not l_rsOldFaults.EOF
                    l_strSQL = "INSERT INTO techrevfault (techrevID, timecode, description, endtimecode, faultgrade) VALUES ("
                    l_strSQL = l_strSQL & "'" & l_lngNewTechrevID & "', "
                    l_strSQL = l_strSQL & "'" & l_rsOldFaults("timecode") & "', "
                    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rsOldFaults("description")) & "', "
                    l_strSQL = l_strSQL & "'" & l_rsOldFaults("endtimecode") & "', "
                    l_strSQL = l_strSQL & "'" & l_rsOldFaults("faultgrade") & "')"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    l_rsOldFaults.MoveNext
                Loop
            End If
            l_rsOldFaults.Close
            Set l_rsOldFaults = Nothing
        End If
    End With
    
    frmFileTechRev.MousePointer = vbDefault
    ShowFileTechRev l_lngNewTechrevID
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume 'PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function InsertTechReview(lp_lngEventID As Long) As Long

'Dim l_strSQL As String,, l_rstTemp As ADODB.Recordset

Dim l_lngNoOfTracks As Long, l_strSQL As String, l_blnLineupPresent As Boolean, l_lngAudioLineUpLevel As Long

'Get the number of audio tracks on this format
'l_strFormat = GetData("library", "format", "libraryID", lp_lnglibraryID)
'l_strSQL = "SELECT * from xref WHERE category = 'format' AND description = '" & l_strFormat & "'"
'Set l_rstTemp = ExecuteSQL(l_strSQL, g_strExecuteError)
'CheckForSQLError
'
'If Not l_rstTemp.EOF Then
'    l_rstTemp.MoveFirst
'    l_lngNoOfTracks = l_rstTemp("Videostandard")
'End If
'
'l_rstTemp.Close
'Set l_rstTemp = Nothing

l_blnLineupPresent = False
l_lngNoOfTracks = Val(InputBox("How many Audio Tracks in the File"))
If MsgBox("Is the Line-up in the File?", vbYesNo, "File Condition") = vbYes Then l_blnLineupPresent = True

l_lngAudioLineUpLevel = -18
If l_blnLineupPresent = True Then
    Do While MsgBox("Audio Line-up at " & l_lngAudioLineUpLevel & "dBfs" & vbCrLf & "Is this correct", vbYesNo, "Audio Line-up") = vbNo
        l_lngAudioLineUpLevel = Val(InputBox("Give the Audio Lineup Level", "Audio Lineup", "-18"))
    Loop
End If

l_strSQL = "INSERT INTO techrev ("
l_strSQL = l_strSQL & "eventID, "
l_strSQL = l_strSQL & "reviewdate, "
l_strSQL = l_strSQL & "flagtitles, "
l_strSQL = l_strSQL & "flagcaptions, "
l_strSQL = l_strSQL & "flagsubtitles, "
l_strSQL = l_strSQL & "flaglogos, "
l_strSQL = l_strSQL & "flagtextless, "
l_strSQL = l_strSQL & "flagtrailers, "
l_strSQL = l_strSQL & "ltc, "
l_strSQL = l_strSQL & "vitc, "
l_strSQL = l_strSQL & "ltcvitcmatch, "
l_strSQL = l_strSQL & "timecodecontinuous, "
l_strSQL = l_strSQL & "verticalcentreingcorrect, "
l_strSQL = l_strSQL & "horizontalcentreingcorrect, "
l_strSQL = l_strSQL & "captionsafe169, "
l_strSQL = l_strSQL & "captionsafe149, "
l_strSQL = l_strSQL & "captionsafe43protect, "
l_strSQL = l_strSQL & "captionsafe43ebu, "
l_strSQL = l_strSQL & "videotest, "
l_strSQL = l_strSQL & "videoprog, "
l_strSQL = l_strSQL & "chromatest, "
l_strSQL = l_strSQL & "chromaprog, "
l_strSQL = l_strSQL & "blacktest, "
l_strSQL = l_strSQL & "blackprog, "
l_strSQL = l_strSQL & "audio1test, "
l_strSQL = l_strSQL & "audio2test, "
l_strSQL = l_strSQL & "audio3test, "
l_strSQL = l_strSQL & "audio4test, "
l_strSQL = l_strSQL & "audio5test, "
l_strSQL = l_strSQL & "audio6test, "
l_strSQL = l_strSQL & "audio7test, "
l_strSQL = l_strSQL & "audio8test, "
l_strSQL = l_strSQL & "audio9test, "
l_strSQL = l_strSQL & "audio10test, "
l_strSQL = l_strSQL & "audio11test, "
l_strSQL = l_strSQL & "audio12test, "
l_strSQL = l_strSQL & "audio13test, "
l_strSQL = l_strSQL & "audio14test, "
l_strSQL = l_strSQL & "audio15test, "
l_strSQL = l_strSQL & "audio16test, "
l_strSQL = l_strSQL & "audio17test, "
l_strSQL = l_strSQL & "audio18test, "
l_strSQL = l_strSQL & "audio19test, "
l_strSQL = l_strSQL & "audio20test, "
l_strSQL = l_strSQL & "audio21test, "
l_strSQL = l_strSQL & "audio22test, "
l_strSQL = l_strSQL & "audio23test, "
l_strSQL = l_strSQL & "audio24test, "
l_strSQL = l_strSQL & "audio1prog, "
l_strSQL = l_strSQL & "audio2prog, "
l_strSQL = l_strSQL & "audio3prog, "
l_strSQL = l_strSQL & "audio4prog, "
l_strSQL = l_strSQL & "audio5prog, "
l_strSQL = l_strSQL & "audio6prog, "
l_strSQL = l_strSQL & "audio7prog, "
l_strSQL = l_strSQL & "audio8prog, "
l_strSQL = l_strSQL & "audio9prog, "
l_strSQL = l_strSQL & "audio10prog, "
l_strSQL = l_strSQL & "audio11prog, "
l_strSQL = l_strSQL & "audio12prog, "
l_strSQL = l_strSQL & "audio13prog, "
l_strSQL = l_strSQL & "audio14prog, "
l_strSQL = l_strSQL & "audio15prog, "
l_strSQL = l_strSQL & "audio16prog, "
l_strSQL = l_strSQL & "audio17prog, "
l_strSQL = l_strSQL & "audio18prog, "
l_strSQL = l_strSQL & "audio19prog, "
l_strSQL = l_strSQL & "audio20prog, "
l_strSQL = l_strSQL & "audio21prog, "
l_strSQL = l_strSQL & "audio22prog, "
l_strSQL = l_strSQL & "audio23prog, "
l_strSQL = l_strSQL & "audio24prog, "
l_strSQL = l_strSQL & "firstactivehorizpixel, "
l_strSQL = l_strSQL & "lastactivehorizpixel, "
l_strSQL = l_strSQL & "firstactivevertpixel, "
l_strSQL = l_strSQL & "lastactivevertpixel, "
l_strSQL = l_strSQL & "vitclines, "
l_strSQL = l_strSQL & "userbits, "
l_strSQL = l_strSQL & "filename, "
l_strSQL = l_strSQL & "bigfilesize, "
l_strSQL = l_strSQL & "md5checksum, "
l_strSQL = l_strSQL & "companyID, "
l_strSQL = l_strSQL & "companyname, "
l_strSQL = l_strSQL & "reviewconclusion, "
l_strSQL = l_strSQL & "reviewuser) VALUES ("

'now the values

l_strSQL = l_strSQL & "'" & lp_lngEventID & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'1', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'1', "
l_strSQL = l_strSQL & "'1', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'100%', "
l_strSQL = l_strSQL & "'OK', "
l_strSQL = l_strSQL & "'75%', "
l_strSQL = l_strSQL & "'OK', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'OK', "
l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
If l_lngNoOfTracks > 2 Then
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
Else
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
End If
If l_lngNoOfTracks > 4 Then
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
Else
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
End If
If l_lngNoOfTracks > 8 Then
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
Else
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
End If
If l_lngNoOfTracks > 12 Then
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
Else
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
End If
If l_lngNoOfTracks > 16 Then
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
Else
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
End If
If l_lngNoOfTracks > 20 Then
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
    l_strSQL = l_strSQL & "'" & l_lngAudioLineUpLevel & "dBfs', "
Else
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
    l_strSQL = l_strSQL & "'N/A', "
End If
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'" & frmClipControl.txtHorizpixels.Text & "', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'" & frmClipControl.txtVertpixels.Text & "', "
l_strSQL = l_strSQL & "'', "
l_strSQL = l_strSQL & "'00:00:00:00', "
l_strSQL = l_strSQL & "'" & frmClipControl.txtClipfilename.Text & "', "
l_strSQL = l_strSQL & Format(frmClipControl.lblFileSize.Caption, "#") & ", "
l_strSQL = l_strSQL & "'" & frmClipControl.txtMD5Checksum.Text & "', "
l_strSQL = l_strSQL & Val(frmClipControl.lblCompanyID.Caption) & ", "
l_strSQL = l_strSQL & "'" & QuoteSanitise(frmClipControl.cmbCompany.Text) & "', "
l_strSQL = l_strSQL & "'0', "
l_strSQL = l_strSQL & "'" & g_strFullUserName & "')"

Debug.Print l_strSQL
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError
        
InsertTechReview = g_lngLastID

End Function

Function CheckFileTechReviewChanges(lp_lngTechrevID As Long) As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    CheckFileTechReviewChanges = False
    
    With frmFileTechRev
        
        Dim l_datReviewdate As Variant
        If Not IsNull(.datReviewDate.Value) Then
            l_datReviewdate = Format(.datReviewDate.Value, vbShortDateFormat)
        Else
            l_datReviewdate = Null
        End If
        
        If Trim(" " & GetData("techrev", "jobID", "techrevID", lp_lngTechrevID)) <> .txtReviewJobID.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Format(GetData("techrev", "reviewdate", "techrevID", lp_lngTechrevID), vbShortDateFormat) <> l_datReviewdate Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "reviewuser", "techrevID", lp_lngTechrevID)) <> .cmbReviewUser.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "companyname", "techrevID", lp_lngTechrevID)) <> .cmbReviewCompany.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "contactname", "techrevID", lp_lngTechrevID)) <> .cmbReviewContact Then
            CheckFileTechReviewChanges = True
        End If
        If GetFlag(GetData("techrev", "flagtitles", "techrevID", lp_lngTechrevID)) <> .chkTitles.Value Then
            CheckFileTechReviewChanges = True
        End If
        If GetFlag(GetData("techrev", "flagcaptions", "techrevID", lp_lngTechrevID)) <> .chkCaptions.Value Then
            CheckFileTechReviewChanges = True
        End If
        If GetFlag(GetData("techrev", "flagsubtitles", "techrevID", lp_lngTechrevID)) <> .chkSubtitles.Value Then
            CheckFileTechReviewChanges = True
        End If
        If GetFlag(GetData("techrev", "flaglogos", "techrevID", lp_lngTechrevID)) <> .chkLogos.Value Then
            CheckFileTechReviewChanges = True
        End If
        If GetFlag(GetData("techrev", "flagtextless", "techrevID", lp_lngTechrevID)) <> .chkTextless.Value Then
            CheckFileTechReviewChanges = True
        End If
        If GetFlag(GetData("techrev", "flagtrailers", "techrevID", lp_lngTechrevID)) <> .chkTrailers.Value Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "languagetitles", "techrevID", lp_lngTechrevID)) <> .cmbTitlesLanguage.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "languagecaptions", "techrevID", lp_lngTechrevID)) <> .cmbCaptionsLanguage.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "languagesubtitles", "techrevID", lp_lngTechrevID)) <> .cmbSubtitlesLanguage.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "detailslogos", "techrevID", lp_lngTechrevID)) <> .txtDetailsLogos.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "detailstextless", "techrevID", lp_lngTechrevID)) <> .txtDetailsTextless.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "detailstrailers", "techrevID", lp_lngTechrevID)) <> .txtDetailsTrailers.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "reviewmachine", "techrevID", lp_lngTechrevID)) <> .cmbReviewMachine.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "notesvideo", "techrevID", lp_lngTechrevID)) <> .txtTechNotesVideo.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "notesaudio", "techrevID", lp_lngTechrevID)) <> .txtTechNotesAudio.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "notesgeneral", "techrevID", lp_lngTechrevID)) <> .txtTechNotesGeneral.Text Then
            CheckFileTechReviewChanges = True
        End If
        If .optReviewConclusion(0).Value = True Then
            If GetData("techrev", "reviewconclusion", "techrevID", lp_lngTechrevID) <> 0 Then CheckFileTechReviewChanges = True
        End If
        If .optReviewConclusion(1).Value = True Then
            If GetData("techrev", "reviewconclusion", "techrevID", lp_lngTechrevID) <> 1 Then CheckFileTechReviewChanges = True
        End If
        If .optReviewConclusion(2).Value = True Then
            If GetData("techrev", "reviewconclusion", "techrevID", lp_lngTechrevID) <> 2 Then CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "videotest", "techrevID", lp_lngTechrevID)) <> .txtVideoTest.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "videoprog", "techrevID", lp_lngTechrevID)) <> .txtVideoProg.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "chromatest", "techrevID", lp_lngTechrevID)) <> .txtChromaTest.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "chromaprog", "techrevID", lp_lngTechrevID)) <> .txtChromaProg.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "blacktest", "techrevID", lp_lngTechrevID)) <> .txtBlackTest.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "blackprog", "techrevID", lp_lngTechrevID)) <> .txtBlackProg.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio1test", "techrevID", lp_lngTechrevID)) <> .txtA1Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio1prog", "techrevID", lp_lngTechrevID)) <> .txtA1Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio2test", "techrevID", lp_lngTechrevID)) <> .txtA2Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio2prog", "techrevID", lp_lngTechrevID)) <> .txtA2Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio3test", "techrevID", lp_lngTechrevID)) <> .txtA3Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio3prog", "techrevID", lp_lngTechrevID)) <> .txtA3Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio4test", "techrevID", lp_lngTechrevID)) <> .txtA4Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio4prog", "techrevID", lp_lngTechrevID)) <> .txtA4Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio5test", "techrevID", lp_lngTechrevID)) <> .txtA5Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio5prog", "techrevID", lp_lngTechrevID)) <> .txtA5Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio6test", "techrevID", lp_lngTechrevID)) <> .txtA6Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio6prog", "techrevID", lp_lngTechrevID)) <> .txtA6Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio7test", "techrevID", lp_lngTechrevID)) <> .txtA7Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio7prog", "techrevID", lp_lngTechrevID)) <> .txtA7Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio8test", "techrevID", lp_lngTechrevID)) <> .txtA8Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio8prog", "techrevID", lp_lngTechrevID)) <> .txtA8Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio9test", "techrevID", lp_lngTechrevID)) <> .txtA9Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio9prog", "techrevID", lp_lngTechrevID)) <> .txtA9Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio10test", "techrevID", lp_lngTechrevID)) <> .txtA10Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio10prog", "techrevID", lp_lngTechrevID)) <> .txtA10Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio11test", "techrevID", lp_lngTechrevID)) <> .txtA11Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio11prog", "techrevID", lp_lngTechrevID)) <> .txtA11Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio12test", "techrevID", lp_lngTechrevID)) <> .txtA12Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio12prog", "techrevID", lp_lngTechrevID)) <> .txtA12Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio13test", "techrevID", lp_lngTechrevID)) <> .txtA13Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio13prog", "techrevID", lp_lngTechrevID)) <> .txtA13Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio14test", "techrevID", lp_lngTechrevID)) <> .txtA14Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio14prog", "techrevID", lp_lngTechrevID)) <> .txtA14Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio15test", "techrevID", lp_lngTechrevID)) <> .txtA15Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio15prog", "techrevID", lp_lngTechrevID)) <> .txtA15Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio16test", "techrevID", lp_lngTechrevID)) <> .txtA16Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio16prog", "techrevID", lp_lngTechrevID)) <> .txtA16Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio17test", "techrevID", lp_lngTechrevID)) <> .txtA17Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio17prog", "techrevID", lp_lngTechrevID)) <> .txtA17Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio18test", "techrevID", lp_lngTechrevID)) <> .txtA18Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio18prog", "techrevID", lp_lngTechrevID)) <> .txtA18Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio19test", "techrevID", lp_lngTechrevID)) <> .txtA19Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio19prog", "techrevID", lp_lngTechrevID)) <> .txtA19Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio20test", "techrevID", lp_lngTechrevID)) <> .txtA20Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio20prog", "techrevID", lp_lngTechrevID)) <> .txtA20Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio21test", "techrevID", lp_lngTechrevID)) <> .txtA21Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio21prog", "techrevID", lp_lngTechrevID)) <> .txtA21Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio22test", "techrevID", lp_lngTechrevID)) <> .txtA22Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio22prog", "techrevID", lp_lngTechrevID)) <> .txtA22Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio23test", "techrevID", lp_lngTechrevID)) <> .txtA23Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio23prog", "techrevID", lp_lngTechrevID)) <> .txtA23Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio24test", "techrevID", lp_lngTechrevID)) <> .txtA24Test.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "audio24prog", "techrevID", lp_lngTechrevID)) <> .txtA24Prog.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "EBU128LoudnessSet1", "techrevID", lp_lngTechrevID)) <> .cmbEBU128LoudnessSet1.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "EBU128LoudnessSet2", "techrevID", lp_lngTechrevID)) <> .cmbEBU128LoudnessSet2.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "EBU128LoudnessSet3", "techrevID", lp_lngTechrevID)) <> .cmbEBU128LoudnessSet3.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "EBU128LoudnessSet4", "techrevID", lp_lngTechrevID)) <> .cmbEBU128LoudnessSet4.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "EBU128LoudnessSet5", "techrevID", lp_lngTechrevID)) <> .cmbEBU128LoudnessSet5.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "EBU128LoudnessSet6", "techrevID", lp_lngTechrevID)) <> .cmbEBU128LoudnessSet6.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "EBU128LoudnessSet7", "techrevID", lp_lngTechrevID)) <> .cmbEBU128LoudnessSet7.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "EBU128LoudnessSet8", "techrevID", lp_lngTechrevID)) <> .cmbEBU128LoudnessSet8.Text Then
            CheckFileTechReviewChanges = True
        End If
'        If Trim(" " & GetData("techrev", "EBU128LoudnessSet9", "techrevID", lp_lngTechrevID)) <> .cmbEBU128LoudnessSet9.Text Then
'            CheckFileTechReviewChanges = True
'        End If
'        If Trim(" " & GetData("techrev", "EBU128LoudnessSet10", "techrevID", lp_lngTechrevID)) <> .cmbEBU128LoudnessSet10.Text Then
'            CheckFileTechReviewChanges = True
'        End If
        If Trim(" " & GetData("techrev", "vitclines", "techrevID", lp_lngTechrevID)) <> .txtvitclines.Text Then
            CheckFileTechReviewChanges = True
        End If
        If GetFlag(GetData("techrev", "timecodecontinuous", "techrevID", lp_lngTechrevID)) <> .chkContinuous.Value Then
            CheckFileTechReviewChanges = True
        End If
        If GetFlag(GetData("techrev", "vitc", "techrevID", lp_lngTechrevID)) <> .chkvitc.Value Then
            CheckFileTechReviewChanges = True
        End If
        If GetFlag(GetData("techrev", "ltc", "techrevID", lp_lngTechrevID)) <> .chkFileTimecode.Value Then
            CheckFileTechReviewChanges = True
        End If
        If GetFlag(GetData("techrev", "ltcvitcmatch", "techrevID", lp_lngTechrevID)) <> .chkltcvitcmatch.Value Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "firstactivehorizpixel", "techrevID", lp_lngTechrevID)) <> .txtfirstactivehorizpixel.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "lastactivehorizpixel", "techrevID", lp_lngTechrevID)) <> .txtlastactivehorizpixel.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "firstactivevertpixel", "techrevID", lp_lngTechrevID)) <> .txtfirstactivevertpixel.Text Then
            CheckFileTechReviewChanges = True
        End If
        If Trim(" " & GetData("techrev", "lastactivevertpixel", "techrevID", lp_lngTechrevID)) <> .txtlastactivevertpixel.Text Then
            CheckFileTechReviewChanges = True
        End If
        If GetFlag(GetData("techrev", "verticalcentreingcorrect", "techrevID", lp_lngTechrevID)) <> .chkverticalcenteringcorrect.Value Then
            CheckFileTechReviewChanges = True
        End If
        If GetFlag(GetData("techrev", "horizontalcentreingcorrect", "techrevID", lp_lngTechrevID)) <> .chkhorizontalcenteringcorrect.Value Then
            CheckFileTechReviewChanges = True
        End If
        If GetFlag(GetData("techrev", "captionsafe169", "techrevID", lp_lngTechrevID)) <> .chkCaptionsafe169.Value Then
            CheckFileTechReviewChanges = True
        End If
        If GetFlag(GetData("techrev", "captionsafe149", "techrevID", lp_lngTechrevID)) <> .chkCaptionsafe149.Value Then
            CheckFileTechReviewChanges = True
        End If
        If GetFlag(GetData("techrev", "captionsafe43protect", "techrevID", lp_lngTechrevID)) <> .chkCaptionsafe43protect.Value Then
            CheckFileTechReviewChanges = True
        End If
        If GetFlag(GetData("techrev", "captionsafe43ebu", "techrevID", lp_lngTechrevID)) <> .chkCaptionsafe43ebu.Value Then
            CheckFileTechReviewChanges = True
        End If
        
    End With
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub DeleteFileTechReview(lp_lngTechrevID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_lngLibraryID As Long
    
    If lp_lngTechrevID <> 0 Then
        
'        l_strSQL = "DELETE from techrevfault WHERE techrevID = " & lp_lngTechrevID
'        ExecuteSQL l_strSQL, g_strExecuteError
'        CheckForSQLError
        l_strSQL = "UPDATE techrev SET system_deleted = 1 WHERE techrevID = " & lp_lngTechrevID
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
    End If
    
'    frmFileTechRev.Hide
'    Unload frmFileTechRev
'    frmFileTechRev.Show
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Function CopyFileTechReviewFaults(ByVal lp_lngTechrevID As Long)
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/savelibrary") Then
        Exit Function
    End If
    
    Dim l_lngOtherTechRevID As Long
    Dim l_strSQL As String, l_rsOldFaults As ADODB.Recordset
    
    l_lngOtherTechRevID = Val(InputBox("Please give the techRev ID of the Techrev to copy faults from"))
    
    If l_lngOtherTechRevID = 0 Then
        Exit Function
    Else
        Set l_rsOldFaults = ExecuteSQL("SELECT * FROM techrevfault WHERE techrevID = '" & l_lngOtherTechRevID & "';", g_strExecuteError)
        CheckForSQLError
        If l_rsOldFaults.RecordCount > 0 Then
            l_rsOldFaults.MoveFirst
            Do While Not l_rsOldFaults.EOF
                l_strSQL = "INSERT INTO techrevfault (techrevID, timecode, description, endtimecode, faultgrade) VALUES ("
                l_strSQL = l_strSQL & "'" & lp_lngTechrevID & "', "
                l_strSQL = l_strSQL & "'" & l_rsOldFaults("timecode") & "', "
                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rsOldFaults("description")) & "', "
                l_strSQL = l_strSQL & "'" & l_rsOldFaults("endtimecode") & "', "
                l_strSQL = l_strSQL & "'" & l_rsOldFaults("faultgrade") & "')"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                l_rsOldFaults.MoveNext
            Loop
        End If
        l_rsOldFaults.Close
        Set l_rsOldFaults = Nothing
    End If
    
    frmFileTechRev.adoFaults.Refresh
    
    'Ask to see is they also want to copy the review conclusions comments.
    Dim l_intResponse As Integer
    Dim l_strComment As String
    
    l_intResponse = MsgBox("Do you wish to also copy the tech review Comments?", vbYesNo, "Copy From...")
    If l_intResponse = vbYes Then
        l_strComment = GetData("techrev", "notesvideo", "techrevID", l_lngOtherTechRevID)
        SetData "techrev", "notesvideo", "techrevID", lp_lngTechrevID, QuoteSanitise(l_strComment)
        frmFileTechRev.txtTechNotesVideo.Text = l_strComment
        l_strComment = GetData("techrev", "Notesaudio", "techrevID", l_lngOtherTechRevID)
        SetData "techrev", "notesaudio", "techrevID", lp_lngTechrevID, QuoteSanitise(l_strComment)
        frmFileTechRev.txtTechNotesAudio.Text = l_strComment
        l_strComment = GetData("techrev", "notesgeneral", "techrevID", l_lngOtherTechRevID)
        SetData "techrev", "notesgeneral", "techrevID", lp_lngTechrevID, QuoteSanitise(l_strComment)
        frmFileTechRev.txtTechNotesGeneral.Text = l_strComment
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub SaveFileTechReview()
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    
    With frmFileTechRev
        
        Dim l_datReviewdate As Variant
        If Not IsNull(.datReviewDate.Value) Then
            l_datReviewdate = Format(.datReviewDate.Value, vbShortDateFormat)
        Else
            l_datReviewdate = Null
        End If
        
        Dim l_intResponse As Integer
        
        l_strSQL = "UPDATE techrev SET "
        l_strSQL = l_strSQL & "jobID = '" & .txtReviewJobID.Text & "', "
        If Not IsNull(l_datReviewdate) Then
            l_strSQL = l_strSQL & "reviewdate = '" & FormatSQLDate(l_datReviewdate) & "', "
        Else
            l_strSQL = l_strSQL & "reviewdate = NULL, "
        End If
        l_strSQL = l_strSQL & "eventID = '" & frmClipControl.txtClipID.Text & "', "
        l_strSQL = l_strSQL & "reviewuser = '" & .cmbReviewUser.Text & "', "
        l_strSQL = l_strSQL & "companyID = '" & .lblReviewCompanyID.Caption & "', "
        l_strSQL = l_strSQL & "companyname = '" & QuoteSanitise(.cmbReviewCompany.Text) & "', "
        l_strSQL = l_strSQL & "contactID = '" & .lblReviewContactID.Caption & "', "
        l_strSQL = l_strSQL & "contactname = '" & QuoteSanitise(.cmbReviewContact) & "', "
        l_strSQL = l_strSQL & "flagtitles = '" & .chkTitles.Value & "', "
        l_strSQL = l_strSQL & "flagcaptions = '" & .chkCaptions.Value & "', "
        l_strSQL = l_strSQL & "flagsubtitles = '" & .chkSubtitles.Value & "', "
        l_strSQL = l_strSQL & "flagforcednarratives = '" & .chkForcedNarratives.Value & "', "
        l_strSQL = l_strSQL & "flaglogos = '" & .chkLogos.Value & "', "
        l_strSQL = l_strSQL & "flagtextless = '" & .chkTextless.Value & "', "
        l_strSQL = l_strSQL & "flagtrailers = '" & .chkTrailers.Value & "', "
        l_strSQL = l_strSQL & "convertedmaterial = '" & .chkConvertedMaterial.Value & "', "
        l_strSQL = l_strSQL & "identinfocorrect = '" & .chkIdentInfoCorrect.Value & "', "
        l_strSQL = l_strSQL & "fullQCreview = '" & .chkSpotCheckRev.Value & "', "
        l_strSQL = l_strSQL & "FivePointCheckReview = '" & .chk5PointCheck.Value & "', "
        l_strSQL = l_strSQL & "iTunesReview = '" & .chkiTunesReview.Value & "', "
        l_strSQL = l_strSQL & "iTunesFinal = '" & .chkiTunesFinal.Value & "', "
        l_strSQL = l_strSQL & "languagetitles = '" & .cmbTitlesLanguage.Text & "', "
        l_strSQL = l_strSQL & "languagecaptions = '" & .cmbCaptionsLanguage.Text & "', "
        l_strSQL = l_strSQL & "languagesubtitles = '" & .cmbSubtitlesLanguage.Text & "', "
        l_strSQL = l_strSQL & "languageforcednarratives = '" & .cmbForcedLanguage.Text & "', "
        l_strSQL = l_strSQL & "detailslogos = '" & QuoteSanitise(.txtDetailsLogos.Text) & "', "
        l_strSQL = l_strSQL & "detailstextless = '" & QuoteSanitise(.txtDetailsTextless.Text) & "', "
        l_strSQL = l_strSQL & "detailstrailers = '" & QuoteSanitise(.txtDetailsTrailers.Text) & "', "
        l_strSQL = l_strSQL & "reviewmachine = '" & .cmbReviewMachine.Text & "', "
        l_strSQL = l_strSQL & "notesvideo = '" & QuoteSanitise(.txtTechNotesVideo.Text) & "', "
        l_strSQL = l_strSQL & "notesaudio = '" & QuoteSanitise(.txtTechNotesAudio.Text) & "', "
        l_strSQL = l_strSQL & "notesgeneral = '" & QuoteSanitise(.txtTechNotesGeneral.Text) & "', "
        If .optReviewConclusion(0).Value = True Then
            l_strSQL = l_strSQL & "reviewconclusion = '" & 0 & "', "
        End If
        If .optReviewConclusion(1).Value = True Then
            l_strSQL = l_strSQL & "reviewconclusion = '" & 1 & "', "
        End If
        If .optReviewConclusion(2).Value = True Then
            l_strSQL = l_strSQL & "reviewconclusion = '" & 2 & "', "
        End If
        If .optReviewConclusion(3).Value = True Then
            l_strSQL = l_strSQL & "passedforitunes = '" & 0 & "', "
        End If
        If .optReviewConclusion(4).Value = True Then
            l_strSQL = l_strSQL & "passedforitunes = '" & 1 & "', "
        End If
        l_strSQL = l_strSQL & "videotest = '" & .txtVideoTest.Text & "', "
        l_strSQL = l_strSQL & "videoprog = '" & .txtVideoProg.Text & "', "
        l_strSQL = l_strSQL & "chromatest = '" & .txtChromaTest.Text & "', "
        l_strSQL = l_strSQL & "chromaprog = '" & .txtChromaProg.Text & "', "
        l_strSQL = l_strSQL & "blacktest = '" & .txtBlackTest.Text & "', "
        l_strSQL = l_strSQL & "blackprog = '" & .txtBlackProg.Text & "', "
        l_strSQL = l_strSQL & "audio1test = '" & .txtA1Test.Text & "', "
        l_strSQL = l_strSQL & "audio1prog = '" & .txtA1Prog.Text & "', "
        l_strSQL = l_strSQL & "audio2test = '" & .txtA2Test.Text & "', "
        l_strSQL = l_strSQL & "audio2prog = '" & .txtA2Prog.Text & "', "
        l_strSQL = l_strSQL & "audio3test = '" & .txtA3Test.Text & "', "
        l_strSQL = l_strSQL & "audio3prog = '" & .txtA3Prog.Text & "', "
        l_strSQL = l_strSQL & "audio4test = '" & .txtA4Test.Text & "', "
        l_strSQL = l_strSQL & "audio4prog = '" & .txtA4Prog.Text & "', "
        l_strSQL = l_strSQL & "audio5test = '" & .txtA5Test.Text & "', "
        l_strSQL = l_strSQL & "audio5prog = '" & .txtA5Prog.Text & "', "
        l_strSQL = l_strSQL & "audio6test = '" & .txtA6Test.Text & "', "
        l_strSQL = l_strSQL & "audio6prog = '" & .txtA6Prog.Text & "', "
        l_strSQL = l_strSQL & "audio7test = '" & .txtA7Test.Text & "', "
        l_strSQL = l_strSQL & "audio7prog = '" & .txtA7Prog.Text & "', "
        l_strSQL = l_strSQL & "audio8test = '" & .txtA8Test.Text & "', "
        l_strSQL = l_strSQL & "audio8prog = '" & .txtA8Prog.Text & "', "
        l_strSQL = l_strSQL & "audio9test = '" & .txtA9Test.Text & "', "
        l_strSQL = l_strSQL & "audio9prog = '" & .txtA9Prog.Text & "', "
        l_strSQL = l_strSQL & "audio10test = '" & .txtA10Test.Text & "', "
        l_strSQL = l_strSQL & "audio10prog = '" & .txtA10Prog.Text & "', "
        l_strSQL = l_strSQL & "audio11test = '" & .txtA11Test.Text & "', "
        l_strSQL = l_strSQL & "audio11prog = '" & .txtA11Prog.Text & "', "
        l_strSQL = l_strSQL & "audio12test = '" & .txtA12Test.Text & "', "
        l_strSQL = l_strSQL & "audio12prog = '" & .txtA12Prog.Text & "', "
        l_strSQL = l_strSQL & "audio13test = '" & .txtA13Test.Text & "', "
        l_strSQL = l_strSQL & "audio13prog = '" & .txtA13Prog.Text & "', "
        l_strSQL = l_strSQL & "audio14test = '" & .txtA14Test.Text & "', "
        l_strSQL = l_strSQL & "audio14prog = '" & .txtA14Prog.Text & "', "
        l_strSQL = l_strSQL & "audio15test = '" & .txtA15Test.Text & "', "
        l_strSQL = l_strSQL & "audio15prog = '" & .txtA15Prog.Text & "', "
        l_strSQL = l_strSQL & "audio16test = '" & .txtA16Test.Text & "', "
        l_strSQL = l_strSQL & "audio16prog = '" & .txtA16Prog.Text & "', "
        l_strSQL = l_strSQL & "audio17test = '" & .txtA17Test.Text & "', "
        l_strSQL = l_strSQL & "audio17prog = '" & .txtA17Prog.Text & "', "
        l_strSQL = l_strSQL & "audio18test = '" & .txtA18Test.Text & "', "
        l_strSQL = l_strSQL & "audio18prog = '" & .txtA18Prog.Text & "', "
        l_strSQL = l_strSQL & "audio19test = '" & .txtA19Test.Text & "', "
        l_strSQL = l_strSQL & "audio19prog = '" & .txtA19Prog.Text & "', "
        l_strSQL = l_strSQL & "audio20test = '" & .txtA20Test.Text & "', "
        l_strSQL = l_strSQL & "audio20prog = '" & .txtA20Prog.Text & "', "
        l_strSQL = l_strSQL & "audio21test = '" & .txtA21Test.Text & "', "
        l_strSQL = l_strSQL & "audio21prog = '" & .txtA21Prog.Text & "', "
        l_strSQL = l_strSQL & "audio22test = '" & .txtA22Test.Text & "', "
        l_strSQL = l_strSQL & "audio22prog = '" & .txtA22Prog.Text & "', "
        l_strSQL = l_strSQL & "audio23test = '" & .txtA23Test.Text & "', "
        l_strSQL = l_strSQL & "audio23prog = '" & .txtA23Prog.Text & "', "
        l_strSQL = l_strSQL & "audio24test = '" & .txtA24Test.Text & "', "
        l_strSQL = l_strSQL & "audio24prog = '" & .txtA24Prog.Text & "', "
        l_strSQL = l_strSQL & "EBU128LoudnessSet1 = '" & .cmbEBU128LoudnessSet1.Text & "', "
        l_strSQL = l_strSQL & "EBU128LoudnessSet2 = '" & .cmbEBU128LoudnessSet2.Text & "', "
        l_strSQL = l_strSQL & "EBU128LoudnessSet3 = '" & .cmbEBU128LoudnessSet3.Text & "', "
        l_strSQL = l_strSQL & "EBU128LoudnessSet4 = '" & .cmbEBU128LoudnessSet4.Text & "', "
        l_strSQL = l_strSQL & "EBU128LoudnessSet5 = '" & .cmbEBU128LoudnessSet5.Text & "', "
        l_strSQL = l_strSQL & "EBU128LoudnessSet6 = '" & .cmbEBU128LoudnessSet6.Text & "', "
        l_strSQL = l_strSQL & "EBU128LoudnessSet7 = '" & .cmbEBU128LoudnessSet7.Text & "', "
        l_strSQL = l_strSQL & "EBU128LoudnessSet8 = '" & .cmbEBU128LoudnessSet8.Text & "', "
'        l_strSQL = l_strSQL & "EBU128LoudnessSet9 = '" & .cmbEBU128LoudnessSet9.Text & "', "
'        l_strSQL = l_strSQL & "EBU128LoudnessSet10 = '" & .cmbEBU128LoudnessSet10.Text & "', "
        l_strSQL = l_strSQL & "EBU128TruePeakSet1 = '" & .cmbEBU128TruePeakSet1.Text & "', "
        l_strSQL = l_strSQL & "EBU128TruePeakSet2 = '" & .cmbEBU128TruePeakSet2.Text & "', "
        l_strSQL = l_strSQL & "EBU128TruePeakSet3 = '" & .cmbEBU128TruePeakSet3.Text & "', "
        l_strSQL = l_strSQL & "EBU128TruePeakSet4 = '" & .cmbEBU128TruePeakSet4.Text & "', "
        l_strSQL = l_strSQL & "EBU128TruePeakSet5 = '" & .cmbEBU128TruePeakSet5.Text & "', "
        l_strSQL = l_strSQL & "EBU128TruePeakSet6 = '" & .cmbEBU128TruePeakSet6.Text & "', "
        l_strSQL = l_strSQL & "EBU128TruePeakSet7 = '" & .cmbEBU128TruePeakSet7.Text & "', "
        l_strSQL = l_strSQL & "EBU128TruePeakSet8 = '" & .cmbEBU128TruePeakSet8.Text & "', "
'        l_strSQL = l_strSQL & "EBU128TruePeakSet9 = '" & .cmbEBU128TruePeakSet9.Text & "', "
'        l_strSQL = l_strSQL & "EBU128TruePeakSet10 = '" & .cmbEBU128TruePeakSet10.Text & "', "
        l_strSQL = l_strSQL & "EBU128LoudnessRangeSet1 = '" & .cmbEBU128LoudnessRangeSet1.Text & "', "
        l_strSQL = l_strSQL & "EBU128LoudnessRangeSet2 = '" & .cmbEBU128LoudnessRangeSet2.Text & "', "
        l_strSQL = l_strSQL & "EBU128LoudnessRangeSet3 = '" & .cmbEBU128LoudnessRangeSet3.Text & "', "
        l_strSQL = l_strSQL & "EBU128LoudnessRangeSet4 = '" & .cmbEBU128LoudnessRangeSet4.Text & "', "
        l_strSQL = l_strSQL & "EBU128LoudnessRangeSet5 = '" & .cmbEBU128LoudnessRangeSet5.Text & "', "
        l_strSQL = l_strSQL & "EBU128LoudnessRangeSet6 = '" & .cmbEBU128LoudnessRangeSet6.Text & "', "
        l_strSQL = l_strSQL & "EBU128LoudnessRangeSet7 = '" & .cmbEBU128LoudnessRangeSet7.Text & "', "
        l_strSQL = l_strSQL & "EBU128LoudnessRangeSet8 = '" & .cmbEBU128LoudnessRangeSet8.Text & "', "
'        l_strSQL = l_strSQL & "EBU128LoudnessRangeSet9 = '" & .cmbEBU128LoudnessRangeSet9.Text & "', "
'        l_strSQL = l_strSQL & "EBU128LoudnessRangeSet10 = '" & .cmbEBU128LoudnessRangeSet10.Text & "', "
        l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet1 = '" & .cmbEBU128MaxShortLoundnessSet1.Text & "', "
        l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet2 = '" & .cmbEBU128MaxShortLoundnessSet2.Text & "', "
        l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet3 = '" & .cmbEBU128MaxShortLoundnessSet3.Text & "', "
        l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet4 = '" & .cmbEBU128MaxShortLoundnessSet4.Text & "', "
        l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet5 = '" & .cmbEBU128MaxShortLoundnessSet5.Text & "', "
        l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet6 = '" & .cmbEBU128MaxShortLoundnessSet6.Text & "', "
        l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet7 = '" & .cmbEBU128MaxShortLoundnessSet7.Text & "', "
        l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet8 = '" & .cmbEBU128MaxShortLoundnessSet8.Text & "', "
'        l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet9 = '" & .cmbEBU128MaxShortLoundnessSet9.Text & "', "
'        l_strSQL = l_strSQL & "EBU128MaxShortLoundnessSet10 = '" & .cmbEBU128MaxShortLoundnessSet10.Text & "', "
        l_strSQL = l_strSQL & "iTunesVideoType = '" & .cmbiTunesVideoType.Text & "', "
        l_strSQL = l_strSQL & "iTunesVendorID = '" & .txtiTunesVendorID.Text & "', "
        l_strSQL = l_strSQL & "highestaudiopeak = '" & .txtHighestAudioPeak.Text & "', "
        l_strSQL = l_strSQL & "vitclines = '" & .txtvitclines.Text & "', "
        l_strSQL = l_strSQL & "ltc = '" & .chkFileTimecode.Value & "', "
        l_strSQL = l_strSQL & "vitc = '" & .chkvitc.Value & "', "
        l_strSQL = l_strSQL & "timecodecontinuous = '" & .chkContinuous.Value & "', "
        l_strSQL = l_strSQL & "ltcvitcmatch = '" & .chkltcvitcmatch.Value & "', "
        l_strSQL = l_strSQL & "firstactivehorizpixel = '" & .txtfirstactivehorizpixel.Text & "', "
        l_strSQL = l_strSQL & "lastactivehorizpixel = '" & .txtlastactivehorizpixel.Text & "', "
        l_strSQL = l_strSQL & "firstactivevertpixel = '" & .txtfirstactivevertpixel.Text & "', "
        l_strSQL = l_strSQL & "lastactivevertpixel = '" & .txtlastactivevertpixel.Text & "', "
        l_strSQL = l_strSQL & "verticalcentreingcorrect = '" & .chkverticalcenteringcorrect.Value & "', "
        l_strSQL = l_strSQL & "horizontalcentreingcorrect = '" & .chkhorizontalcenteringcorrect.Value & "', "
        l_strSQL = l_strSQL & "captionsafe169 = '" & .chkCaptionsafe169.Value & "', "
        l_strSQL = l_strSQL & "captionsafe149 = '" & .chkCaptionsafe149.Value & "', "
        l_strSQL = l_strSQL & "captionsafe43protect = '" & .chkCaptionsafe43protect.Value & "', "
        l_strSQL = l_strSQL & "captionsafe43ebu = '" & .chkCaptionsafe43ebu.Value & "', "
        l_strSQL = l_strSQL & "actionsafe169 = '" & .chkActionsafe169.Value & "', "
        l_strSQL = l_strSQL & "actionsafe149 = '" & .chkActionsafe149.Value & "', "
        l_strSQL = l_strSQL & "actionsafe43 = '" & .chkActionsafe43.Value & "', "
        l_strSQL = l_strSQL & "AudioPhaseError = '" & .chkAudioPhaseError.Value & "', "
        l_strSQL = l_strSQL & "filename = '" & frmClipControl.txtClipfilename.Text & "', "
        If .optR128Summary(1).Value <> 0 Then l_strSQL = l_strSQL & "R128SummaryPass = 1, " Else l_strSQL = l_strSQL & "R128SummaryPass = 0, "
        If frmClipControl.lblFileSize.Caption <> "" Then l_strSQL = l_strSQL & "bigfilesize = " & Format(frmClipControl.lblFileSize.Caption, "#") & ", "
        l_strSQL = l_strSQL & "md5checksum = '" & frmClipControl.txtMD5Checksum.Text & "' "
        l_strSQL = l_strSQL & "WHERE techrevID = " & .lbltechrevID.Caption
        
    End With
    
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    If g_optCreatePDFFilesOfTechReviews <> 0 Then
        If frmFileTechRev.chkSpotCheckRev.Value = True Then
            If InStr(GetData("company", "cetaclientcode", "companyID", frmClipControl.lblCompanyID.Caption), "/techrevincitunes") > 0 Then
                ReportToPDF g_strLocationOfCrystalReportFiles & "\filetechrev_Full_itunes.rpt", "{techrev.techrevID} = " & frmFileTechRev.lbltechrevID.Caption, g_strFilePDFLocation & "\" & frmClipControl.txtReference.Text & "_" & frmClipControl.txtClipID.Text & ".pdf"
            Else
                ReportToPDF g_strLocationOfCrystalReportFiles & "\filetechrev_Full.rpt", "{techrev.techrevID} = " & frmFileTechRev.lbltechrevID.Caption, g_strFilePDFLocation & "\" & frmClipControl.txtReference.Text & "_" & frmClipControl.txtClipID.Text & ".pdf"
            End If
        ElseIf frmFileTechRev.chkiTunesReview.Value = True Then
            ReportToPDF g_strLocationOfCrystalReportFiles & "\Filetechrev_iTunes.rpt", "{techrev.techrevID} = " & frmFileTechRev.lbltechrevID.Caption, g_strFilePDFLocation & "\" & frmClipControl.txtReference.Text & "_" & frmClipControl.txtClipID.Text & ".pdf"
        ElseIf frmFileTechRev.chkiTunesFinal.Value = True Then
            ReportToPDF g_strLocationOfCrystalReportFiles & "\Filetechrev_iTunes_Final.rpt", "{techrev.techrevID} = " & frmFileTechRev.lbltechrevID.Caption, g_strFilePDFLocation & "\" & frmClipControl.txtReference.Text & "_" & frmClipControl.txtClipID.Text & ".pdf"
        Else
            ReportToPDF g_strLocationOfCrystalReportFiles & "\Filetechrev_Runtime.rpt", "{techrev.techrevID} = " & frmFileTechRev.lbltechrevID.Caption, g_strFilePDFLocation & "\" & frmClipControl.txtReference.Text & "_" & frmClipControl.txtClipID.Text & ".pdf"
        End If
    End If
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Function VerifyClip(lp_varClipID As Variant, lp_strAltLocation As String, lp_strFilename As String, lp_lngLibraryID As Long, lp_blnSilent As Boolean, lp_blnSilentSuccess As Boolean) As Boolean

Dim l_strNetworkPath As String, l_lngFilenumber As Long, l_curFileSize As Currency
Dim FSO As FileSystemObject, fil As File, FileModifiedDate As Date, l_strSQL As String

VerifyClip = False

'Check that the alt location hasn't got a drive letter or a UNC path at the start of it.
If lp_strAltLocation Like "\\*" Or Mid(lp_strAltLocation, 2, 1) = ":" Then
    If lp_blnSilent = False Then MsgBox "Alt Location has either drive letter or UNC network reference. Please fix this", vbOKOnly, "Problem..."
    Exit Function
End If

If GetData("events", "clipformat", "eventID", Val(lp_varClipID)) = "PLAYLIST" Then
    VerifyClip = True
    Exit Function
End If

If Not ValidateFilename(lp_strFilename) Then
    If lp_blnSilent = False Then MsgBox "Illegal characters in filename", vbInformation
End If

If Not ValidateFoldername(lp_strAltLocation) Then
    If lp_blnSilent = False Then MsgBox "Illegal characters somewhere in path folder names", vbInformation
End If

If GetData("library", "format", "libraryID", lp_lngLibraryID) = "DISCSTORE" Then

    If g_blnRedNetwork = True Or lp_lngLibraryID = 796559 Then
    
        'Red Network machine can verify the file instantly itself...
        l_strNetworkPath = GetData("library", "subtitle", "libraryID", lp_lngLibraryID)
        l_strNetworkPath = Replace(l_strNetworkPath, "\\", "\\?\UNC\")
        If lp_strAltLocation <> "" Then l_strNetworkPath = l_strNetworkPath & "\" & IIf(Right(lp_strAltLocation, 1) = "\", Left(lp_strAltLocation, Len(lp_strAltLocation) - 1), lp_strAltLocation)
        l_strNetworkPath = l_strNetworkPath & "\" & lp_strFilename
        
        'Verify existence of file.
        Set FSO = New FileSystemObject
        On Error GoTo VERIFYFAIL2
        If FSO.FileExists(l_strNetworkPath) Then
            API_OpenFile l_strNetworkPath, l_lngFilenumber, l_curFileSize
            API_CloseFile l_lngFilenumber
            Set fil = FSO.GetFile(l_strNetworkPath)
            FileModifiedDate = fil.DateLastModified
            Set fil = Nothing
            If lp_blnSilent = False Then
                If lp_blnSilentSuccess = False Then MsgBox "File Name and path Verified", vbOKOnly, "Filename Verification"
                frmClipControl.lblFileSize.Caption = Format(l_curFileSize, "#,#")
                frmClipControl.lblVerifiedDate.Caption = Format(Now, "dd MMM yyyy HH:NN:SS")
                frmClipControl.lblLastModifiedDate.Caption = Format(FileModifiedDate, "dd MMM yyyy HH:NN:SS")
            End If
            If lp_varClipID <> "" Then
                ExecuteSQL "UPDATE events SET soundlay = '" & Format(Now, "dd MMM yyyy HH:NN:SS") & "', bigfilesize = " & l_curFileSize & " WHERE eventID = " & Val(lp_varClipID) & ";", g_strExecuteError
                CheckForSQLError
            End If
            VerifyClip = True
            On Error GoTo 0
            
        ElseIf FSO.FolderExists(l_strNetworkPath) Then
            l_curFileSize = GetDirectorySize(l_strNetworkPath, lp_blnSilent)
            If lp_blnSilent = False Then
                If lp_blnSilentSuccess = False Then MsgBox "File Name and path Verified", vbOKOnly, "Filename Verification"
                frmClipControl.cmbClipformat.Text = "Folder"
                frmClipControl.lblFileSize.Caption = Format(l_curFileSize, "#,#")
                frmClipControl.lblVerifiedDate.Caption = Format(Now, "dd MMM yyyy HH:NN:SS")
            End If
            If lp_varClipID <> "" Then
                ExecuteSQL "UPDATE events SET soundlay = '" & Format(Now, "dd MMM yyyy HH:NN:SS") & "', bigfilesize = " & l_curFileSize & ", clipformat = 'Folder' WHERE eventID = " & Val(lp_varClipID) & ";", g_strExecuteError
                CheckForSQLError
            End If
            VerifyClip = True
            On Error GoTo 0
        
        Else
VERIFYFAIL2:
            If lp_blnSilent = False Then
                frmClipControl.lblVerifiedDate.Caption = ""
                frmClipControl.lblFileSize.Caption = ""
                MsgBox "File Name and path did not verify", vbCritical, "Filename Verification"
            End If
            If lp_varClipID <> "" Then
                ExecuteSQL "UPDATE events SET soundlay = NULL, bigfilesize = NULL WHERE eventID = " & Val(lp_varClipID) & ";", g_strExecuteError
                CheckForSQLError
            End If
        End If
        Set FSO = Nothing

    Else
    
        'Non-Red Network machine has to issue a verify request.
        l_strSQL = "INSERT INTO event_file_request (event_file_request_typeID, eventID, SourceLibraryID, RequestName, RequesterEmail) VALUES ("
        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Verify File") & ", "
        l_strSQL = l_strSQL & lp_varClipID & ", "
        l_strSQL = l_strSQL & lp_lngLibraryID & ", "
        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        If lp_blnSilent = False Then MsgBox "You are on a machine that cannot see the store this file is on - a verify request has been issued."
        VerifyClip = True
        
    End If
Else

    If lp_blnSilent = False Then
        MsgBox "Clips can only be verified if they are in one of the VDMS DISCSTORES", vbOKOnly, "Attempting to Verify clip"
    End If
    
End If

End Function

Public Sub FileFullQCFieldsOn()

frmFileTechRev.fraAudioLevels.Visible = True
frmFileTechRev.fraDigitalParameters.Visible = True
frmFileTechRev.fraSafeAreas.Visible = True
frmFileTechRev.fraTimecode.Visible = True
frmFileTechRev.fraVideoLevels.Visible = True
frmFileTechRev.cmdSwitchLineupLevel.Visible = True

End Sub

Public Sub FileFullQCFieldsOff()

frmFileTechRev.fraAudioLevels.Visible = False
frmFileTechRev.fraDigitalParameters.Visible = False
frmFileTechRev.fraSafeAreas.Visible = False
frmFileTechRev.fraTimecode.Visible = False
frmFileTechRev.fraVideoLevels.Visible = False
frmFileTechRev.cmdSwitchLineupLevel.Visible = False

End Sub

Public Sub FileiTunesQCFieldsOn()

frmFileTechRev.fraAudioLevels.Visible = False
frmFileTechRev.fraDigitalParameters.Visible = False
frmFileTechRev.fraSafeAreas.Visible = False
frmFileTechRev.fraTimecode.Visible = False
frmFileTechRev.fraVideoLevels.Visible = False
frmFileTechRev.cmdSwitchLineupLevel.Visible = False

End Sub

Sub ShowClipiTunes(lp_lngClipID As Long)

If Not CheckAccess("/showlibrary") Then Exit Sub

Dim l_strSQL As String, l_lngCompanyID As Long
Dim l_rstClip As New ADODB.Recordset

frmClipiTunes.lblClipID.Caption = lp_lngClipID

If GetData("events_iTunes", "eventID", "eventID", lp_lngClipID) <> lp_lngClipID Then
    l_strSQL = "INSERT INTO events_iTunes (eventID) VALUES (" & lp_lngClipID & ")"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
End If

l_strSQL = "SELECT TOP 1 * FROM events_iTunes WHERE eventID = '" & lp_lngClipID & "';"

Set l_rstClip = ExecuteSQL(l_strSQL, g_strExecuteError)

If Not l_rstClip.EOF Then
    'Load up the information for that clip
    l_rstClip.MoveFirst
    With frmClipiTunes
        
        l_lngCompanyID = GetData("events", "companyID", "eventID", lp_lngClipID)
        .lblCompanyID.Caption = l_lngCompanyID
        'Load up the iTunes related stuff
        
        .txtiTunesProvider.Text = Trim(" " & l_rstClip("provider"))
        .cmbSubType.Text = Trim(" " & l_rstClip("video_subtype"))
        .txtiTunesVendorID.Text = Trim(" " & l_rstClip("video_vendorID"))
        .txtiTunesEpProdNo.Text = Trim(" " & l_rstClip("video_ep_prod_no"))
        .txtiTunesContainerID.Text = Trim(" " & l_rstClip("video_containerID"))
        .txtiTunesTitle.Text = Trim(" " & l_rstClip("video_title"))
        .txtiTunesContainerPosition.Text = Trim(" " & l_rstClip("video_containerposition"))
        .datiTunesReleaseDate.Value = Trim(" " & l_rstClip("video_releasedate"))
        .txtiTunesCopyrightLine.Text = Trim(" " & l_rstClip("video_copyright_cline"))
        .txtiTunesLongDescription.Text = Trim(" " & l_rstClip("video_longdescription"))
        .txtiTunesTechComments.Text = Trim(" " & l_rstClip("itunestechcomment"))
        .txtiTunesVideoLanguage.Text = Trim(" " & l_rstClip("video_language"))
        .txtiTunesOriginalSpokenLocale.Text = Trim(" " & l_rstClip("originalspokenlocale"))
        .txtMetaDataLanguage.Text = Trim(" " & l_rstClip("metadata_language"))
        .txtiTunesPreviewStartTime.Text = Trim(" " & l_rstClip("video_previewstarttime"))
        .txtCropTop.Text = Trim(" " & l_rstClip("itunescroptop"))
        .txtCropBottom.Text = Trim(" " & l_rstClip("itunescropbottom"))
        .txtCropLeft.Text = Trim(" " & l_rstClip("itunescropleft"))
        .txtCropRight.Text = Trim(" " & l_rstClip("itunescropright"))
        .txtiTunesISAN.Text = Trim(" " & l_rstClip("video_ISAN"))
        .txtStudio_Release_Title = Trim(" " & l_rstClip("studio_release_title"))
        .cmbAU.Text = Trim(" " & l_rstClip("rating_au"))
        .cmbCA.Text = Trim(" " & l_rstClip("rating_ca"))
        .cmbDE.Text = Trim(" " & l_rstClip("rating_de"))
        .cmbFR.Text = Trim(" " & l_rstClip("rating_fr"))
        .cmbUK.Text = Trim(" " & l_rstClip("rating_uk"))
        .cmbUS.Text = Trim(" " & l_rstClip("rating_us"))
        .cmbJP.Text = Trim(" " & l_rstClip("rating_jp"))
        .chkCaptionsAvailability.Value = GetFlag(Val(Trim(" " & l_rstClip("captionsavailable"))))
        .txtCaptionsClipID.Text = Trim(" " & l_rstClip("captionsclipID"))
        .cmbCaptionsReason.Text = Trim(" " & l_rstClip("captionsreason"))
        
        l_strSQL = "SELECT * FROM iTunes_package WHERE companyID = " & l_lngCompanyID & " AND video_type = 'tv' AND packageused = 0;"
    
        Dim l_conSearch As ADODB.Connection
        Dim l_rstSearch As ADODB.Recordset
    
        Set l_conSearch = New ADODB.Connection
        Set l_rstSearch = New ADODB.Recordset
    
        l_conSearch.ConnectionString = g_strConnection
        l_conSearch.Open
    
        With l_rstSearch
             .CursorLocation = adUseClient
             .LockType = adLockBatchOptimistic
             .CursorType = adOpenDynamic
             .Open l_strSQL, l_conSearch, adOpenDynamic
        End With
    
        l_rstSearch.ActiveConnection = Nothing
    
        Set .cmbiTunesPackage.DataSourceList = l_rstSearch
    
        l_conSearch.Close
        Set l_conSearch = Nothing
    
        .adoAdvisory.ConnectionString = g_strConnection
        .adoAdvisory.RecordSource = "SELECT * FROM iTunes_Clip_Advisory WHERE eventID = " & lp_lngClipID & ";"
        .adoAdvisory.Refresh
    
        .adoProduct.ConnectionString = g_strConnection
        .adoProduct.RecordSource = "SELECT * FROM iTunes_product WHERE eventID = " & lp_lngClipID & ";"
        .adoProduct.Refresh
    
    End With

Else
    
    If frmClipiTunes.lblClipID.Caption = "" Then
    
        ClearFields frmClipiTunes
        
    End If

End If

l_rstClip.Close
Set l_rstClip = Nothing

frmClipiTunes.Show vbModal

End Sub

Sub ShowClipHawk(lp_lngClipID As Long)

If Not CheckAccess("/showlibrary") Then Exit Sub

frmClipiTunes.lblClipID.Caption = lp_lngClipID

Dim l_strSQL As String, l_lngCompanyID As Long

l_strSQL = "SELECT * FROM events_iTunes WHERE eventID = '" & lp_lngClipID & "';"

Dim l_rstClip As New ADODB.Recordset
Set l_rstClip = ExecuteSQL(l_strSQL, g_strExecuteError)

If Not l_rstClip.EOF Then
    'Load up the information for that clip
    l_rstClip.MoveFirst
    With frmClipiTunes

        l_lngCompanyID = Val(Trim(" " & l_rstClip("companyID")))
        .lblCompanyID.Caption = l_lngCompanyID
        'Load up the iTunes related stuff

        .txtiTunesProvider.Text = Trim(" " & l_rstClip("provider"))
        .txtiTunesVendorID.Text = Trim(" " & l_rstClip("video_vendorID"))
        .txtiTunesEpProdNo.Text = Trim(" " & l_rstClip("video_ep_prod_no"))
        .txtiTunesContainerID.Text = Trim(" " & l_rstClip("video_containerID"))
        .txtiTunesTitle.Text = UTF8_Decode(Trim(" " & l_rstClip("video_title")))
        .txtiTunesContainerPosition.Text = Trim(" " & l_rstClip("video_containerposition"))
        .datiTunesReleaseDate.Value = Trim(" " & l_rstClip("video_releasedate"))
        .txtiTunesCopyrightLine.Text = Trim(" " & l_rstClip("video_copyright_cline"))
        .txtiTunesLongDescription.Text = UTF8_Decode(Trim(" " & l_rstClip("video_longdescription")))
        .txtiTunesVideoLanguage.Text = Trim(" " & l_rstClip("video_language"))
        .txtiTunesOriginalSpokenLocale.Text = Trim(" " & l_rstClip("originalspokenlocale"))
        .txtMetaDataLanguage.Text = Trim(" " & l_rstClip("metadata_language"))
        .txtiTunesPreviewStartTime.Text = Trim(" " & l_rstClip("video_previewstarttime"))
        .txtCropTop.Text = Trim(" " & l_rstClip("itunescroptop"))
        .txtCropBottom.Text = Trim(" " & l_rstClip("itunescropbottom"))
        .txtCropLeft.Text = Trim(" " & l_rstClip("itunescropleft"))
        .txtCropRight.Text = Trim(" " & l_rstClip("itunescropright"))
        .txtiTunesISAN.Text = Trim(" " & l_rstClip("video_ISAN"))
        .cmbAU.Text = Trim(" " & l_rstClip("rating_au"))
        .cmbCA.Text = Trim(" " & l_rstClip("rating_ca"))
        .cmbDE.Text = Trim(" " & l_rstClip("rating_de"))
        .cmbFR.Text = Trim(" " & l_rstClip("rating_fr"))
        .cmbUK.Text = Trim(" " & l_rstClip("rating_uk"))
        .cmbUS.Text = Trim(" " & l_rstClip("rating_us"))
        .cmbJP.Text = Trim(" " & l_rstClip("rating_jp"))
        .chkCaptionsAvailability.Value = GetFlag(Val(Trim(" " & l_rstClip("captionsavailable"))))
        .txtCaptionsClipID.Text = Trim(" " & l_rstClip("captionsclipID"))
        .cmbCaptionsReason.Text = Trim(" " & l_rstClip("captionsreason"))

        l_strSQL = "SELECT * FROM iTunes_package WHERE companyID = " & l_lngCompanyID & " AND video_type = 'tv';"

        Dim l_conSearch As ADODB.Connection
        Dim l_rstSearch As ADODB.Recordset

        Set l_conSearch = New ADODB.Connection
        Set l_rstSearch = New ADODB.Recordset

        l_conSearch.ConnectionString = g_strConnection
        l_conSearch.Open

        With l_rstSearch
             .CursorLocation = adUseClient
             .LockType = adLockBatchOptimistic
             .CursorType = adOpenDynamic
             .Open l_strSQL, l_conSearch, adOpenDynamic
        End With

        l_rstSearch.ActiveConnection = Nothing

        Set .cmbiTunesPackage.DataSourceList = l_rstSearch

        l_conSearch.Close
        Set l_conSearch = Nothing

        .adoAdvisory.ConnectionString = g_strConnection
        .adoAdvisory.RecordSource = "SELECT * FROM iTunes_Clip_Advisory WHERE eventID = " & lp_lngClipID & ";"
        .adoAdvisory.Refresh

        .adoProduct.ConnectionString = g_strConnection
        .adoProduct.RecordSource = "SELECT * FROM iTunes_product WHERE eventID = " & lp_lngClipID & ";"
        .adoProduct.Refresh

    End With

Else

    If frmClipiTunes.lblClipID.Caption = "" Then

        ClearFields frmClipiTunes

    End If

End If

l_rstClip.Close
Set l_rstClip = Nothing

frmClipHawk.Show vbModal

End Sub
Sub ShowClipNetflix(lp_lngClipID As Long)

If Not CheckAccess("/showlibrary") Then Exit Sub

frmClipiTunes.lblClipID.Caption = lp_lngClipID

Dim l_strSQL As String, l_lngCompanyID As Long

l_strSQL = "SELECT * FROM events_iTunes WHERE eventID = '" & lp_lngClipID & "';"

Dim l_rstClip As New ADODB.Recordset
Set l_rstClip = ExecuteSQL(l_strSQL, g_strExecuteError)

If Not l_rstClip.EOF Then
    'Load up the information for that clip
    l_rstClip.MoveFirst
    With frmClipiTunes

        l_lngCompanyID = Val(Trim(" " & l_rstClip("companyID")))
        .lblCompanyID.Caption = l_lngCompanyID
        'Load up the iTunes related stuff

        .txtiTunesProvider.Text = Trim(" " & l_rstClip("provider"))
        .txtiTunesVendorID.Text = Trim(" " & l_rstClip("video_vendorID"))
        .txtiTunesEpProdNo.Text = Trim(" " & l_rstClip("video_ep_prod_no"))
        .txtiTunesContainerID.Text = Trim(" " & l_rstClip("video_containerID"))
        .txtiTunesTitle.Text = UTF8_Decode(Trim(" " & l_rstClip("video_title")))
        .txtiTunesContainerPosition.Text = Trim(" " & l_rstClip("video_containerposition"))
        .datiTunesReleaseDate.Value = Trim(" " & l_rstClip("video_releasedate"))
        .txtiTunesCopyrightLine.Text = Trim(" " & l_rstClip("video_copyright_cline"))
        .txtiTunesLongDescription.Text = UTF8_Decode(Trim(" " & l_rstClip("video_longdescription")))
        .txtiTunesVideoLanguage.Text = Trim(" " & l_rstClip("video_language"))
        .txtiTunesOriginalSpokenLocale.Text = Trim(" " & l_rstClip("originalspokenlocale"))
        .txtMetaDataLanguage.Text = Trim(" " & l_rstClip("metadata_language"))
        .txtiTunesPreviewStartTime.Text = Trim(" " & l_rstClip("video_previewstarttime"))
        .txtCropTop.Text = Trim(" " & l_rstClip("itunescroptop"))
        .txtCropBottom.Text = Trim(" " & l_rstClip("itunescropbottom"))
        .txtCropLeft.Text = Trim(" " & l_rstClip("itunescropleft"))
        .txtCropRight.Text = Trim(" " & l_rstClip("itunescropright"))
        .txtiTunesISAN.Text = Trim(" " & l_rstClip("video_ISAN"))
        .cmbAU.Text = Trim(" " & l_rstClip("rating_au"))
        .cmbCA.Text = Trim(" " & l_rstClip("rating_ca"))
        .cmbDE.Text = Trim(" " & l_rstClip("rating_de"))
        .cmbFR.Text = Trim(" " & l_rstClip("rating_fr"))
        .cmbUK.Text = Trim(" " & l_rstClip("rating_uk"))
        .cmbUS.Text = Trim(" " & l_rstClip("rating_us"))
        .cmbJP.Text = Trim(" " & l_rstClip("rating_jp"))
        .chkCaptionsAvailability.Value = GetFlag(Val(Trim(" " & l_rstClip("captionsavailable"))))
        .txtCaptionsClipID.Text = Trim(" " & l_rstClip("captionsclipID"))
        .cmbCaptionsReason.Text = Trim(" " & l_rstClip("captionsreason"))

        l_strSQL = "SELECT * FROM iTunes_package WHERE companyID = " & l_lngCompanyID & " AND video_type = 'tv';"

        Dim l_conSearch As ADODB.Connection
        Dim l_rstSearch As ADODB.Recordset

        Set l_conSearch = New ADODB.Connection
        Set l_rstSearch = New ADODB.Recordset

        l_conSearch.ConnectionString = g_strConnection
        l_conSearch.Open

        With l_rstSearch
             .CursorLocation = adUseClient
             .LockType = adLockBatchOptimistic
             .CursorType = adOpenDynamic
             .Open l_strSQL, l_conSearch, adOpenDynamic
        End With

        l_rstSearch.ActiveConnection = Nothing

        Set .cmbiTunesPackage.DataSourceList = l_rstSearch

        l_conSearch.Close
        Set l_conSearch = Nothing

        .adoAdvisory.ConnectionString = g_strConnection
        .adoAdvisory.RecordSource = "SELECT * FROM iTunes_Clip_Advisory WHERE eventID = " & lp_lngClipID & ";"
        .adoAdvisory.Refresh

        .adoProduct.ConnectionString = g_strConnection
        .adoProduct.RecordSource = "SELECT * FROM iTunes_product WHERE eventID = " & lp_lngClipID & ";"
        .adoProduct.Refresh

    End With

Else

    If frmClipiTunes.lblClipID.Caption = "" Then

        ClearFields frmClipiTunes

    End If

End If

l_rstClip.Close
Set l_rstClip = Nothing

frmClipNetflix.Show vbModal

End Sub

Sub SaveClipiTunes()

Dim l_strSQL As String

If Val(frmClipiTunes.lblClipID.Caption) = 0 Then Exit Sub

With frmClipiTunes

    SetData "events", "mdate", "eventID", Val(.lblClipID.Caption), Format(Now, "YYYY-MM-DD HH:NN:SS")
    SetData "events", "muser", "eventID", Val(.lblClipID.Caption), g_strUserInitials
    
    l_strSQL = "UPDATE events_iTunes SET "
    
    'This next block all relates to iTunes TV content
    l_strSQL = l_strSQL & "provider = '" & QuoteSanitise(.txtiTunesProvider.Text) & "', "
    l_strSQL = l_strSQL & "video_type = 'tv', "
    l_strSQL = l_strSQL & "video_subtype = '" & QuoteSanitise(.cmbSubType.Text) & "', "
    l_strSQL = l_strSQL & "video_vendorID = '" & QuoteSanitise(.txtiTunesVendorID.Text) & "', "
    l_strSQL = l_strSQL & "iTunesVendorOfferCode = '" & .txtiTunesVendorOfferCode.Text & "', "
    l_strSQL = l_strSQL & "video_ep_prod_no = '" & QuoteSanitise(.txtiTunesEpProdNo.Text) & "', "
    l_strSQL = l_strSQL & "video_ISAN = '" & .txtiTunesISAN.Text & "', "
    l_strSQL = l_strSQL & "video_title = '" & QuoteSanitise(.txtiTunesTitle.Text) & "', "
    l_strSQL = l_strSQL & "video_title_utf8 = '" & UTF8_Encode(QuoteSanitise(.txtiTunesTitle.Text)) & "', "
    l_strSQL = l_strSQL & "studio_release_title = '" & QuoteSanitise(.txtStudio_Release_Title.Text) & "', "
    l_strSQL = l_strSQL & "studio_release_title_utf8 = '" & UTF8_Encode(QuoteSanitise(.txtStudio_Release_Title.Text)) & "', "
    l_strSQL = l_strSQL & "video_containerID = '" & QuoteSanitise(.txtiTunesContainerID.Text) & "', "
    l_strSQL = l_strSQL & "video_containerposition = '" & QuoteSanitise(.txtiTunesContainerPosition.Text) & "', "
    If Not IsNull(.datiTunesReleaseDate.Value) Then
        l_strSQL = l_strSQL & "video_releasedate = '" & FormatSQLDate(.datiTunesReleaseDate.Value) & "', "
    Else
        l_strSQL = l_strSQL & "video_releasedate = NULL, "
    End If
    l_strSQL = l_strSQL & "video_copyright_cline = '" & QuoteSanitise(.txtiTunesCopyrightLine.Text) & "', "
    l_strSQL = l_strSQL & "video_copyright_cline_utf8 = '" & UTF8_Encode(QuoteSanitise(.txtiTunesCopyrightLine.Text)) & "', "
    l_strSQL = l_strSQL & "video_longdescription = '" & QuoteSanitise(.txtiTunesLongDescription.Text) & "', "
    l_strSQL = l_strSQL & "video_longdescription_utf8 = '" & UTF8_Encode(QuoteSanitise(.txtiTunesLongDescription.Text)) & "', "
    l_strSQL = l_strSQL & "itunestechcomment = '" & QuoteSanitise(.txtiTunesTechComments.Text) & "', "
    l_strSQL = l_strSQL & "itunestechcomment_utf8 = '" & UTF8_Encode(QuoteSanitise(.txtiTunesTechComments.Text)) & "', "
    If .txtiTunesPreviewStartTime.Text <> "" Then l_strSQL = l_strSQL & "video_previewstarttime = " & Val(.txtiTunesPreviewStartTime.Text) & ", " Else l_strSQL = l_strSQL & "video_previewstarttime = 360, "
    If Val(.txtCropTop.Text) <> 0 Then l_strSQL = l_strSQL & "itunescroptop = '" & .txtCropTop.Text & "', " Else l_strSQL = l_strSQL & "itunescroptop = '', "
    If Val(.txtCropBottom.Text) <> 0 Then l_strSQL = l_strSQL & "itunescropbottom = '" & .txtCropBottom.Text & "', " Else l_strSQL = l_strSQL & "itunescropbottom = '', "
    If Val(.txtCropLeft.Text) <> 0 Then l_strSQL = l_strSQL & "itunescropleft = '" & .txtCropLeft.Text & "', " Else l_strSQL = l_strSQL & "itunescropleft = '', "
    If Val(.txtCropRight.Text) <> 0 Then l_strSQL = l_strSQL & "itunescropright = '" & .txtCropRight.Text & "', " Else l_strSQL = l_strSQL & "itunescropright = '', "
    l_strSQL = l_strSQL & "video_language = '" & .txtiTunesVideoLanguage.Text & "', "
    l_strSQL = l_strSQL & "originalspokenlocale = '" & .txtiTunesOriginalSpokenLocale.Text & "', "
    l_strSQL = l_strSQL & "metadata_language = '" & .txtMetaDataLanguage.Text & "', "
    l_strSQL = l_strSQL & "captionsavailable = " & .chkCaptionsAvailability.Value & ", "
    If .txtCaptionsClipID.Text <> "" Then
        l_strSQL = l_strSQL & "captionsclipID = " & Val(.txtCaptionsClipID.Text) & ", "
    Else
        l_strSQL = l_strSQL & "captionsclipID = NULL, "
    End If
    If .cmbCaptionsReason.Text <> "" Then
        l_strSQL = l_strSQL & "captionsreason = '" & .cmbCaptionsReason.Text & "', "
    Else
        l_strSQL = l_strSQL & "captionsreason = NULL, "
    End If
    l_strSQL = l_strSQL & "rating_au = '" & .cmbAU.Text & "', "
    l_strSQL = l_strSQL & "rating_ca = '" & .cmbCA.Text & "', "
    l_strSQL = l_strSQL & "rating_de = '" & .cmbDE.Text & "', "
    l_strSQL = l_strSQL & "rating_fr = '" & .cmbFR.Text & "', "
    l_strSQL = l_strSQL & "rating_uk = '" & .cmbUK.Text & "', "
    l_strSQL = l_strSQL & "rating_us = '" & .cmbUS.Text & "', "
    l_strSQL = l_strSQL & "rating_jp = '" & .cmbJP.Text & "' "
    
    'Finally we finish the save
    l_strSQL = l_strSQL & "WHERE eventID = " & Val(.lblClipID.Caption) & ";"

End With

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

MsgBox "Saved"

End Sub

Public Function GetMediaInfoOnFile(lp_strFilePath As String) As MediaInfoData

Dim m_Framerate As Integer
Dim Handle As Long, l_curAudioBitRate As Currency, Count As Long, TempStr As String, l_lngAudioChannelCount As Long
Dim lblFormat As String, lblGeneralFormat As String, lblVideoCodec As String, lblCodecID As String, lblAudioCodec As String, lblFrameRate As String
Dim lblVideoBitrate As String, lblInterlace As String, lblVideoBitDepth As String, lblSampleCount As String
Dim lblDisplayAspect As String, lblFrameCount As String, lblPixelRatio As String, lblBitDepth As String, lblAudioChannels As String
Dim lblAudioBitrate As String, lblTimecodeType As String, lblTimecode As String, lblMatroskaTimecode As String, lblSamplingRate As String, lblOverallBitrate As String, lblTransferCharacteristics As String
Dim lblSeriesTitle As String, lblSeriesNumber As String, lblProgrammeTitle As String, lblEpisodeTitleNumber As String, lblEpisodeNumber As String
Dim lblLineUpStart As String, lblIdentClockStart As String, lblTotalProgrammeDuration As String, lblSynopsis As String, lblCopyrightYear As String, lblNumberOfParts As String
Dim lblAudioLayout As String, lblPrimaryAudioLanguage As String, lblSecondaryAudioLanguage As String, lblTertiaryAudioLanguage As String
Dim lblAudioProfile As String, lblAudioVersion As String

'Open the File with MediaInfo.dll and collect the available information
Handle = MediaInfo_New()
lp_strFilePath = Replace(lp_strFilePath, "\\", "\\?\UNC\")
Call MediaInfo_Open(Handle, StrPtr(lp_strFilePath))
Screen.MousePointer = vbNormal

With GetMediaInfoOnFile
    lblFormat = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("Format_Profile"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If lblFormat = "" Then lblFormat = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("Format_Commercial"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblGeneralFormat = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("Format"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblVideoCodec = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("Video_Format_List"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblCodecID = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("CodecID"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblAudioCodec = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("Audio_Format_List"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblFrameRate = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("FrameRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    .txtCBRVBR = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("BitRate_Mode"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblVideoBitrate = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("BitRate_Maximum"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If lblVideoBitrate = "" Then lblVideoBitrate = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("BitRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    .txtVideoBitrate = Int(Val(lblVideoBitrate) / 1000)
    If .txtVideoBitrate = 0 Then .txtVideoBitrate = ""
    .txtWidth = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("Width_Original"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If .txtWidth = "" Then .txtWidth = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("Width"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If .txtWidth = "" Then .txtWidth = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Image, 0, StrPtr("Width"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    .txtHeight = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("Height_Original"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If .txtHeight = "" Then .txtHeight = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("Height"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If .txtHeight = "" Then .txtHeight = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Image, 0, StrPtr("Height"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblInterlace = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("ScanType"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblDisplayAspect = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("DisplayAspectRatio"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblFrameCount = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("FrameCount"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblPixelRatio = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("PixelAspectRatio_Original"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If lblPixelRatio = "" Then lblPixelRatio = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("PixelAspectRatio"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblAudioCodec = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, 0, StrPtr("Format"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If lblAudioCodec = "MPEG Audio" Then
        lblAudioProfile = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, 0, StrPtr("Format_Profile"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblAudioVersion = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, 0, StrPtr("Format_Verions"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    End If
    lblBitDepth = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, 0, StrPtr("BitDepth"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    .txtColorSpace = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("ColorSpace"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblTransferCharacteristics = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("transfer_characteristics"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If lblTransferCharacteristics = "HLG" Then .txtColorSpace = IIf(.txtColorSpace <> "", .txtColorSpace & " ", "") & "HDR"
    .txtChromaSubsmapling = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("ChromaSubsampling"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblVideoBitDepth = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("BitDepth"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblSamplingRate = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, 0, StrPtr("SamplingRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblSampleCount = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, 0, StrPtr("SamplingCount"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblAudioChannels = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, 0, StrPtr("Channel(s)"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    .txtTrackCount = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("AudioCount"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    l_curAudioBitRate = 0
    l_lngAudioChannelCount = 0
    For Count = 0 To Val(.txtTrackCount) - 1
        If Val(bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, Count, StrPtr("BitRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))) <> 0 Then
            l_curAudioBitRate = l_curAudioBitRate + Val(bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, Count, StrPtr("BitRate"), MediaInfo_Info_Text, MediaInfo_Info_Name)))
        Else
            l_curAudioBitRate = l_curAudioBitRate + Val(bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, Count, StrPtr("BitRate_Maximum"), MediaInfo_Info_Text, MediaInfo_Info_Name)))
        End If
        l_lngAudioChannelCount = l_lngAudioChannelCount + Val(bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, Count, StrPtr("Channel(s)"), MediaInfo_Info_Text, MediaInfo_Info_Name)))
    Next
    .txtChannelCount = l_lngAudioChannelCount
    If l_curAudioBitRate = 0 Then
        If lblVideoBitrate <> "" And l_lngAudioChannelCount <> 0 Then
            l_curAudioBitRate = Val(bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("OverallBitRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))) - Val(lblVideoBitrate)
        ElseIf lblVideoBitrate <> "" Then
            lblOverallBitrate = Val(lblVideoBitrate)
        Else
            lblOverallBitrate = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("OverallBitRate"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        End If
    End If
    .txtAudioBitrate = Int(l_curAudioBitRate / 1000)
    If .txtAudioBitrate = "0" Then .txtAudioBitrate = ""
    If .txtAudioBitrate = "" And .txtVideoBitrate = "" Then .txtOverallBitrate = lblOverallBitrate
    lblTimecodeType = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 0, StrPtr("Type"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblTimecode = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 0, StrPtr("TimeCode_FirstFrame"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    lblMatroskaTimecode = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("TimeCode_FirstFrame"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If lblMatroskaTimecode = "" Then lblMatroskaTimecode = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("TimeCode_FirstFrame"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    If lblVideoCodec = "DV" Then
        lblVideoCodec = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_General, 0, StrPtr("Format_Commercial"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    End If
    If lblGeneralFormat = "QuickTime" Then
        lblFormat = "Quicktime"
        lblVideoCodec = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("Format_Commercial"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    ElseIf lblGeneralFormat = "MXF" Then
        lblFormat = "MXF " & lblFormat
        lblVideoCodec = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("Format_Commercial"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    ElseIf lblVideoCodec = "MPEG Video" And lblCodecID = "61" Then
        lblFormat = "MPEG 4"
        lblVideoCodec = "XDCAM EX 35"
    ElseIf lblVideoCodec = "MPEG Video" And lblFormat <> "QuickTime" Then
        If lblCodecID = "xdv7" Then
            lblVideoCodec = "XDCAM EX 35"
        ElseIf Left(bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Video, 0, StrPtr("Format_Commercial"), MediaInfo_Info_Text, MediaInfo_Info_Name)), 6) = "MPEG-2" Then
            lblFormat = "MPEG 2"
        Else
            lblFormat = "MPEG"
        End If
    End If
    If lblFormat = "MPEG Audio" Then
        lblFormat = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Audio, 0, StrPtr("Codec"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblAudioCodec = lblFormat & " " & lblAudioChannels
    End If

    Dim ParsingOut As RegExp
    Dim Matches As MatchCollection
    Set ParsingOut = New RegExp
    ParsingOut.Pattern = "(\d+)"
    
    If bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 2, StrPtr("Format"), MediaInfo_Info_Text, MediaInfo_Info_Name)) = "AS-11 Core" Then
        lblSeriesTitle = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 2, StrPtr("SeriesTitle"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        Set Matches = ParsingOut.Execute(lblSeriesTitle)
        If Matches.Count > 0 Then
            If Matches(0).SubMatches.Count > 0 Then
                lblSeriesNumber = Matches(0).SubMatches(0)
            Else
                lblSeriesNumber = ""
            End If
        Else
            lblSeriesNumber = ""
        End If
        lblProgrammeTitle = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 2, StrPtr("ProgrammeTitle"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblEpisodeTitleNumber = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 2, StrPtr("EpisodeTitleNumber"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        Set Matches = ParsingOut.Execute(lblEpisodeTitleNumber)
        If Matches.Count > 0 Then
            If Matches(0).SubMatches.Count > 0 Then
                lblEpisodeNumber = Matches(0).SubMatches(0)
            Else
                lblEpisodeNumber = ""
            End If
        Else
            lblEpisodeNumber = ""
        End If
        lblLineUpStart = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 3, StrPtr("LineUpStart"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblIdentClockStart = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 3, StrPtr("IdentClockStart"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblTotalProgrammeDuration = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 3, StrPtr("TotalProgrammeDuration"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblSynopsis = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 3, StrPtr("Synopsis"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblCopyrightYear = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 3, StrPtr("CopyrightYear"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblNumberOfParts = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 3, StrPtr("TotalNumberOfParts"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblAudioLayout = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 2, StrPtr("AudioTrackLayout"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblPrimaryAudioLanguage = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 2, StrPtr("PrimaryAudioLanguage"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblSecondaryAudioLanguage = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 2, StrPtr("SecondaryAudioLanguage"), MediaInfo_Info_Text, MediaInfo_Info_Name))
        lblTertiaryAudioLanguage = bstr(MediaInfo_Get(Handle, MediaInfo_Stream_Chapters, 2, StrPtr("TertiaryAudioLanguage"), MediaInfo_Info_Text, MediaInfo_Info_Name))
    End If
    
    'Close the MediaInfo
    Call MediaInfo_Close(Handle)
    Call MediaInfo_Delete(Handle)
    
    'Translate the Infirmation into MX1 speak

    If lblFormat = "Wave" Then
        .txtVideoCodec = GetAlias(lblVideoCodec)
    ElseIf lblCodecID = "AVdn" Then
        .txtVideoCodec = GetAlias(Trim(lblVideoCodec & " " & lblCodecID) & " " & lblVideoBitDepth)
    ElseIf lblFormat <> "" Then
        .txtVideoCodec = GetAlias(Trim(lblVideoCodec & " " & lblCodecID))
    End If
    If lblAudioCodec = "PCM" Or lblAudioCodec = "MPA1L2" Then
        .txtAudioCodec = GetAlias(Trim(lblAudioCodec & " " & lblBitDepth & " " & lblSamplingRate & " " & lblAudioChannels))
    Else
        .txtAudioCodec = GetAlias(lblAudioCodec)
    End If
    If Val(lblFrameRate) = Int(Val(lblFrameRate)) Then
        .txtFrameRate = Int(Val(lblFrameRate))
    Else
        .txtFrameRate = Format(Val(lblFrameRate), "#.00")
    End If
    
    If .txtFrameRate = "0" Then .txtFrameRate = ""
    
    Select Case .txtFrameRate
    
        Case "25"
            m_Framerate = TC_25
        Case "29.97"
            If lblTimecode <> "" Then
                If Mid(lblTimecode, 9, 1) = ";" Then
                    m_Framerate = TC_29
                Else
                    m_Framerate = TC_30
                    .txtFrameRate = "29.97 NDF"
                End If
            Else
                m_Framerate = TC_30
                .txtFrameRate = "29.97 NDF"
            End If
        Case "30"
            m_Framerate = TC_30
        Case "24", "23.98"
            m_Framerate = TC_24
        Case "50"
            m_Framerate = TC_50
        Case "60"
            m_Framerate = TC_60
        Case "59.94"
            If lblTimecode <> "" Then
                If Mid(lblTimecode, 9, 1) = ";" Then
                    m_Framerate = TC_59
                Else
                    m_Framerate = TC_60
                    .txtFrameRate = "59.94 NDF"
                End If
            Else
                m_Framerate = TC_60
                .txtFrameRate = "59.94 NDF"
            End If
        Case Else
            m_Framerate = TC_UN
    
    End Select
    
    .txtInterlace = GetAlias(lblInterlace)
    If lblFrameCount <> "" Then
        .txtDuration = Timecode_From_FrameNumber(Val(lblFrameCount), m_Framerate)
    ElseIf Val(lblSamplingRate) <> 0 Then
        lblFrameCount = Val(lblSampleCount) / Val(lblSamplingRate) * 25
        .txtDuration = Timecode_From_FrameNumber(Val(lblFrameCount), m_Framerate)
    End If
    If lblTimecodeType = "Time code" Then
        .txtTimecodeStart = lblTimecode
        .txtTimecodeStop = Timecode_Add(.txtTimecodeStart, .txtDuration, m_Framerate)
    ElseIf lblMatroskaTimecode <> "" Then
        .txtTimecodeStart = lblMatroskaTimecode
        .txtTimecodeStop = Timecode_Add(.txtTimecodeStart, .txtDuration, m_Framerate)
    Else
        Select Case m_Framerate
            Case TC_29, TC_59
                .txtTimecodeStart = "00:00:00;00"
            Case Else
                .txtTimecodeStart = "00:00:00:00"
        End Select
        .txtTimecodeStop = .txtDuration
    End If
    If lblFormat <> "MPA1L3" Then
        If CLng(Val(lblDisplayAspect) * 9) <= 14 Then
            .txtAspect = "4:3"
        ElseIf CLng(Val(lblDisplayAspect) * 9) <= 16 Then
            .txtAspect = Int(Val(lblDisplayAspect) * 9) & ":9"
        ElseIf Val(lblDisplayAspect) < 2 Then
            .txtAspect = Format(Val(lblDisplayAspect), "#.00") & ":1"
        Else
            .txtAspect = Format(Val(lblDisplayAspect), "#.0") & ":1"
        End If
        If lblPixelRatio = "1.000" And lblFormat <> "AVI" Then
            .txtGeometry = "SQUARE"
        ElseIf Val(lblPixelRatio) < 1.3 Then
            .txtGeometry = "NORMAL"
        Else
            .txtGeometry = "ANAMORPHIC"
        End If
    End If
    If lblFormat = "MPEG 2" Then
        .txtFormat = GetAlias(lblFormat & " " & .txtAspect)
        .txtStreamType = GetAlias(lblGeneralFormat)
        If Val(.txtVideoBitrate) > 45000 Then
            If .txtAudioCodec <> "" Then
                .txtVideoCodec = "50I VID + AUD"
            Else
                .txtVideoCodec = "50I VID Only"
            End If
        Else
            If .txtAudioCodec <> "" Then
                .txtVideoCodec = "VID + AUD"
            Else
                .txtVideoCodec = "VID Only"
            End If
        End If
    ElseIf lblFormat = "Windows Media" Then
        .txtFormat = GetAlias(lblFormat & " " & .txtAspect)
        .txtStreamType = ""
        If .txtAudioCodec <> "" Then
            .txtVideoCodec = "VID + AUD"
        Else
            .txtVideoCodec = "VID Only"
        End If
    ElseIf lblFormat = "MPEG" Then
        .txtFormat = GetAlias(lblFormat)
        .txtStreamType = GetAlias(lblGeneralFormat)
        If .txtAudioCodec <> "" Then
            .txtVideoCodec = "VID + AUD"
        Else
            .txtVideoCodec = "VID Only"
        End If
    ElseIf lblFormat = "Wave" Then
        .txtFormat = GetAlias(lblFormat)
        .txtGeometry = ""
        .txtAspect = ""
    ElseIf lblFormat = "MPEG Audio" Then
        'Do nothing
    ElseIf lblFormat = "PDF" Then
        If UCase(Right(lp_strFilePath, 3)) = ".AI" Then
            .txtFormat = "Adobe AI File"
            .txtAspect = ""
        End If
    ElseIf lblFormat = "ZIP" Then
        Select Case UCase(Right(lp_strFilePath, 4))
            Case "XLSX"
                .txtFormat = "Excel File"
            Case "DOCX"
                .txtFormat = "Word File"
            Case Else
                .txtFormat = "ZIP Archive"
        End Select
    ElseIf lblFormat = "" Then
        Select Case UCase(Right(lp_strFilePath, 3))
            Case "ISO"
                .txtFormat = "DVD ISO Image"
            Case "JPG"
                .txtFormat = "JPEG Image"
            Case "PDF"
                .txtFormat = "PDF File"
            Case "PNG"
                .txtFormat = "PNG Image"
            Case "PSD"
                .txtFormat = "PSD Still Image"
            Case "SCC"
                .txtFormat = "SCC File (EIA 608)"
            Case "TIF"
                .txtFormat = "TIF Still Image"
            Case "DOC"
                .txtFormat = "Word File"
            Case "XLS"
                .txtFormat = "XLS File"
            Case "XML"
                .txtFormat = "XML File"
            Case "RAR"
                .txtFormat = "RAR Archive"
            Case "TAR"
                .txtFormat = "TAR Archive"
            Case "GPG"
                .txtFormat = "Encrypted File"
            Case Else
                .txtFormat = "Other"
        End Select
        .txtFrameRate = ""
        .txtVideoBitrate = ""
        .txtAspect = ""
        .txtGeometry = ""
        .txtTimecodeStart = ""
    Else
        .txtFormat = GetAlias(lblFormat)
    End If
    
    If lblSeriesTitle <> "" Then
        .txtSeriesTitle = lblSeriesTitle
        .txtSeriesNumber = lblSeriesNumber
        .txtProgrammeTitle = lblProgrammeTitle
        .txtEpisodeNumber = lblEpisodeNumber
        .txtSynopsis = lblSynopsis
        .txtPrimaryAudioLanguage = lblPrimaryAudioLanguage
        .txtAudioLayout = lblAudioLayout
    End If
        
End With

End Function

Sub ShowDigitalDeliveryDestination(lp_lngDigitalDeliveryDestinationID As Long, Optional lp_blnClearForm As Boolean, Optional lp_blnShowForm As Boolean)

If Not CheckAccess("/showlibrary") Then Exit Sub

Dim l_strSQL As String

l_strSQL = "SELECT * FROM DigitalDeliveryDestination WHERE DigitalDeliveryDestinationID = " & lp_lngDigitalDeliveryDestinationID & "ORDER BY DestinationName;"

Dim l_rstSpec As New ADODB.Recordset
Set l_rstSpec = ExecuteSQL(l_strSQL, g_strExecuteError)

If Not l_rstSpec.EOF Then

    l_rstSpec.MoveFirst
    
    With frmDigitalDeliveryDestination
        .cmbDestinationName.Text = Trim(" " & l_rstSpec("DestinationName"))
        .lblDigitalDeliveryDestinationID.Caption = l_rstSpec("DigitalDeliveryDestinationID")
        If Val(l_rstSpec("companyID")) <> 0 Then
            .cmbCompany.Text = GetData("company", "name", "companyID", Val(l_rstSpec("companyID")))
            .lblCompanyID.Caption = l_rstSpec("companyID")
        End If
        .txtMethodName.Text = Trim(" " & l_rstSpec("MethodName"))
        .txtSiteAddress.Text = Trim(" " & l_rstSpec("SiteAddress"))
        .txtPortNumber.Text = Trim(" " & l_rstSpec("PortNumber"))
        .txtSmartjogAccountcode.Text = Trim(" " & l_rstSpec("SmartjogAccountcode"))
        .txtSmartjogAccountcode.Text = Trim(" " & l_rstSpec("SmartjogEmailNotifications"))
        .txtLoginName.Text = Trim(" " & l_rstSpec("LoginName"))
        .txtPassword.Text = Trim(" " & l_rstSpec("Password"))
    End With

Else
    If lp_blnClearForm = True Then
        With frmDigitalDeliveryDestination
            .cmbDestinationName.Text = ""
            .lblDigitalDeliveryDestinationID.Caption = ""
            .cmbCompany.Text = ""
            .lblCompanyID.Caption = ""
            .txtMethodName.Text = ""
            .txtSiteAddress.Text = ""
            .txtSmartjogAccountcode.Text = ""
            .txtLoginName.Text = ""
            .txtPassword.Text = ""
        End With
    End If
End If

l_rstSpec.Close
Set l_rstSpec = Nothing

If lp_blnShowForm = True Then
    frmDigitalDeliveryDestination.Show
    frmDigitalDeliveryDestination.ZOrder 0
End If

End Sub

Sub DeleteDigitalDestination()

Dim l_strSQL As String, l_lngDigitalDeliveryDestinationID As Long

If frmDigitalDeliveryDestination.lblDigitalDeliveryDestinationID.Caption <> "" Then
    l_lngDigitalDeliveryDestinationID = Val(frmDigitalDeliveryDestination.lblDigitalDeliveryDestinationID.Caption)
    
    If MsgBox("Are you sure you wish to delete" & vbCrLf & frmDigitalDeliveryDestination.cmbDestinationName.Text & "?", vbYesNo, "Delete Digital Delivery Destination") = vbYes Then
        l_strSQL = "DELETE FROM DigitalDeliveryDestination WHERE DigitalDeliveryDestinationID = " & l_lngDigitalDeliveryDestinationID & ";"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        ClearFields frmDigitalDeliveryDestination
        frmDigitalDeliveryDestination.adoDestinationName.Refresh
    End If
    
End If

End Sub

Sub SaveDigitalDeliveryDestination()

Dim l_lngDigitalDeliveryDestinationID As Long
Dim l_strSQL As String

If frmDigitalDeliveryDestination.lblDigitalDeliveryDestinationID.Caption <> "" Then

    'Existing Delivery - Check whether the Name has changed, and if it has check whether this is an edit or a new Destination after all
    
    If frmDigitalDeliveryDestination.cmbDestinationName.Text <> GetData("DigitalDeliveryDestination", "DestinationName", "DigitalDeliveryDestinationID", Val(frmDigitalDeliveryDestination.lblDigitalDeliveryDestinationID.Caption)) Then
        If MsgBox("Is this a New Destination?", vbYesNo, "Destination Name has been Changed") = vbYes Then GoTo NEW_CLIP_SAVE
    End If
    
    l_lngDigitalDeliveryDestinationID = Val(frmDigitalDeliveryDestination.lblDigitalDeliveryDestinationID.Caption)
    
    SaveExistingDigitalDeliveryDestination l_lngDigitalDeliveryDestinationID

Else

NEW_CLIP_SAVE:

    'New Specification
    l_strSQL = "INSERT INTO DigitalDeliveryDestination (DestinationName, MethodName, SiteAddress, PortNumber, "
    l_strSQL = l_strSQL & "SmartjogAccountcode, SmartjogEMailNotifications, LoginName, Password, cdate, cuser, mdate, muser) VALUES ("
    
    With frmDigitalDeliveryDestination
    
        l_strSQL = l_strSQL & "'" & .cmbDestinationName.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtMethodName.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtSiteAddress.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtPortNumber.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtSmartjogAccountcode.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtSmartjogEmailNotifications.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtLoginName.Text & "', "
        l_strSQL = l_strSQL & "'" & .txtPassword.Text & "', "
        l_strSQL = l_strSQL & "getdate(), '" & g_strUserInitials & "', getdate(), '" & g_strUserInitials & "');"
    
    End With
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_lngDigitalDeliveryDestinationID = g_lngLastID
    
        
End If

frmDigitalDeliveryDestination.adoDestinationName.Refresh
ShowDigitalDeliveryDestination l_lngDigitalDeliveryDestinationID, True, True

End Sub

Sub SaveExistingDigitalDeliveryDestination(lp_lngDigitalDeliveryDestinationID As Long)

Dim l_strSQL As String

With frmDigitalDeliveryDestination

    l_strSQL = "UPDATE DigitalDeliveryDestination SET "
    l_strSQL = l_strSQL & "DestinationName = '" & .cmbDestinationName.Text & "', "
    l_strSQL = l_strSQL & "MethodName = '" & .txtMethodName.Text & "', "
    l_strSQL = l_strSQL & "SiteAddress = '" & .txtSiteAddress.Text & "', "
    l_strSQL = l_strSQL & "PortNumber = '" & .txtPortNumber.Text & "', "
    l_strSQL = l_strSQL & "SmartjogAccountcode = '" & .txtSmartjogAccountcode.Text & "', "
    l_strSQL = l_strSQL & "SmartjogEmailNotifications = '" & QuoteSanitise(.txtSmartjogEmailNotifications.Text) & "', "
    l_strSQL = l_strSQL & "LoginName = '" & .txtLoginName.Text & "', "
    l_strSQL = l_strSQL & "Password = '" & .txtPassword.Text & "', "
    l_strSQL = l_strSQL & "muser = '" & g_strUserInitials & "', "
    l_strSQL = l_strSQL & "mdate = getdate() "
    l_strSQL = l_strSQL & "WHERE DigitalDeliveryDestinationID = " & lp_lngDigitalDeliveryDestinationID & ";"
    
End With

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Sub

Sub LoadClipControl(lp_lngClipID As Long)

If Not CheckAccess("/showlibrary") Then Exit Sub

Dim l_strSQL As String

g_blnFrameAccurateClipDurations = False

l_strSQL = "SELECT * FROM events WHERE eventID = '" & lp_lngClipID & "';"

Dim l_rstClip As New ADODB.Recordset, l_rstData As ADODB.Recordset
Set l_rstClip = ExecuteSQL(l_strSQL, g_strExecuteError)
Dim l_rstUsage As New ADODB.Recordset

Set l_rstUsage = ExecuteSQL("Select * from eventusage WHERE eventID = '" & lp_lngClipID & "' ORDER by dateused ASC;", g_strExecuteError)
CheckForSQLError
If Not l_rstUsage.EOF Then l_rstUsage.MoveLast

CheckForSQLError

If Not l_rstClip.EOF Then
    'Load up the information for that clip
    l_rstClip.MoveFirst
    With frmClipControl
        .txtClipID.Text = lp_lngClipID
        
        'Display the Internal refence, and if it is not set make some efforts to set if from parent information.
        'If parent information fails, then set it to the next in sequence, storing away the values everywhere relevant.
        
        .txtInternalReference.Text = Trim(" " & l_rstClip("internalreference"))
            
        If Not IsNull(l_rstClip("libraryID")) Then
            .lblLibraryID.Caption = l_rstClip("libraryID")
            .cmbBarcode.Text = GetData("library", "barcode", "libraryID", l_rstClip("libraryID"))
            .lblFormat.Caption = GetData("library", "format", "libraryID", l_rstClip("libraryID"))
        Else
            .lblLibraryID.Caption = ""
            .cmbBarcode.Text = ""
            .lblFormat.Caption = ""
        End If
        
        'Set the company related fields and the title combo
        .cmbCompany.Text = Trim(" " & l_rstClip("companyname"))
        .lblCompanyID.Caption = Trim(" " & l_rstClip("companyID"))
        .lblOldCompanyID.Caption = Trim(" " & l_rstClip("companyID"))
        
        'Set the label for if it is a deletd clip
        If l_rstClip("system_deleted") = 1 Then
            .lblDeletedClip.Visible = True
            .cmdDelete.Picture = LoadPicture(App.Path & "\UnDelete_file.gif")
        Else
            .lblDeletedClip.Visible = False
            .cmdDelete.Picture = LoadPicture(App.Path & "\Delete_file.gif")
        End If
    
        'Check the frameaccurate thing and set the global variable g_blnFrameAccurateClipDurations appropriately.
        If InStr(Trim(" " & GetData("company", "cetaclientcode", "companyID", Val(.lblCompanyID.Caption))), "/noframe") <> 0 Then
            g_blnFrameAccurateClipDurations = False
        Else
            g_blnFrameAccurateClipDurations = True
        End If
        .txtSHA1Checksum.Text = Trim(" " & l_rstClip("sha1checksum"))
        .txtMD5Checksum.Text = Trim(" " & l_rstClip("md5checksum"))
        .txtJobID.Text = Trim(" " & l_rstClip("jobID"))
        .txtJobDetailID.Text = Trim(" " & l_rstClip("jobdetailID"))
        .txtSvenskProjectNumber.Text = Trim(" " & l_rstClip("svenskprojectnumber"))
        .txtTitle.Text = Trim(" " & l_rstClip("eventtitle"))
        .txtSubtitle.Text = Trim(" " & l_rstClip("eventsubtitle"))
        .cmbSeries.Text = Trim(" " & l_rstClip("eventseries"))
        .cmbSet.Text = Trim(" " & l_rstClip("eventset"))
        .cmbEpisode.Text = Trim(" " & l_rstClip("eventepisode"))
        .cmbVersion.Text = Trim(" " & l_rstClip("eventversion"))
        .txtReference.Text = Trim(" " & l_rstClip("clipreference"))
        .txtTapeBarcode.Text = Trim(" " & l_rstClip("Tape_Barcode"))
        .txtOldReference.Text = Trim(" " & l_rstClip("clipreference"))
        .cmbFileVersion.Text = Trim(" " & l_rstClip("fileversion"))
        .cmbClipformat.Text = Trim(" " & l_rstClip("clipformat"))
        .cmbAspect.Text = Trim(" " & l_rstClip("aspectratio"))
        .cmbGeometry.Text = Trim(" " & l_rstClip("geometriclinearity"))
        .cmbClipcodec.Text = Trim(" " & l_rstClip("clipcodec"))
        .txtColorSpace.Text = Trim(" " & l_rstClip("ColorSpace"))
        .txtChromaSubsampling.Text = Trim(" " & l_rstClip("ChromaSubsampling"))
        .cmbPasses.Text = Trim(" " & l_rstClip("encodepasses"))
        .cmbCbrVbr.Text = Trim(" " & l_rstClip("cbrvbr"))
        .cmbInterlace.Text = Trim(" " & l_rstClip("interlace"))
        .cmbAudioCodec.Text = Trim(" " & l_rstClip("clipaudiocodec"))
        .cmbPurpose.Text = Trim(" " & l_rstClip("eventtype"))
        .txtHorizpixels.Text = Trim(" " & l_rstClip("cliphorizontalpixels"))
        .txtVertpixels.Text = Trim(" " & l_rstClip("clipverticalpixels"))
        .cmbFrameRate.Text = Trim(" " & l_rstClip("clipframerate"))
        .txtBitrate.Text = Trim(" " & l_rstClip("clipbitrate"))
        .txtAudioBitrate.Text = Trim(" " & l_rstClip("audiobitrate"))
        .txtTrackCount.Text = Trim(" " & l_rstClip("trackcount"))
        .txtAudioChannelCount.Text = Trim(" " & l_rstClip("channelcount"))
        .txtVideoBitrate.Text = Trim(" " & l_rstClip("videobitrate"))
        .txtClipfilename.Text = Trim(" " & l_rstClip("clipfilename"))
        .lblFileSize.Caption = Format(Trim(" " & l_rstClip("bigfilesize")), "#,#")
        If .lblFileSize.Caption = "" Then
            .lblFileUnverified.Visible = True
        Else
            .lblFileUnverified.Visible = False
        End If
        .lblLastModifiedDate.Caption = Format(l_rstClip("FileModifiedDate"), "dd MMM yyyy HH:NN:SS")
        .cmbIsTextInPicture.Text = Trim(" " & l_rstClip("istextinpicture"))
        .txtSeriesID.Text = Trim(" " & l_rstClip("seriesID"))
        .txtSerialNumber.Text = Trim(" " & l_rstClip("serialnumber"))
        .txtTitle.Columns("seriesID").Text = Trim(" " & l_rstClip("seriesID"))
        .txtTitle.Columns("companyID").Text = .lblCompanyID.Caption
        If Not IsNull(l_rstClip("soundlay")) And l_rstClip("soundlay") <> "" Then .lblVerifiedDate.Caption = Format(l_rstClip("soundlay"), "dd MMM yyyy HH:NN:SS") Else .lblVerifiedDate.Caption = ""
        If Not IsNull(l_rstClip("lastmediainfoquery")) And l_rstClip("lastmediainfoquery") <> "" Then .lblLastMediaInfoQuery.Caption = Format(l_rstClip("lastmediainfoquery"), "dd MMM yyyy HH:NN:SS") Else .lblLastMediaInfoQuery.Caption = ""
        .chkSourceFromMedia.Value = GetFlag(Trim(" " & l_rstClip("sourcefrommedia")))
        If .chkSourceFromMedia.Value <> 0 Then
            .txtSourceEventID.Text = Trim(" " & l_rstClip("sourceeventID"))
            .lblSourceLibraryID.Caption = Trim(" " & GetData("events", "libraryID", "eventID", Val(.txtSourceEventID.Text), True))
            .txtSourcebarcode.Text = Trim(" " & GetData("library", "barcode", "libraryID", Val(.lblSourceLibraryID.Caption)))
            .lblSourceFormat.Caption = Trim(" " & GetData("library", "format", "libraryID", Val(.lblSourceLibraryID.Caption)))
        Else
            .txtSourceEventID.Text = Trim(" " & l_rstClip("sourcetapeeventID"))
            .lblSourceLibraryID.Caption = Trim(" " & l_rstClip("sourcelibraryID"))
            If .lblSourceLibraryID.Caption = "" Then
                .lblSourceLibraryID.Caption = GetData("tapeevents", "libraryID", "eventID", Val(.txtSourceEventID.Text))
            End If
            If .lblSourceLibraryID.Caption <> "" Then
                .txtSourcebarcode.Text = GetData("library", "barcode", "libraryID", Val(.lblSourceLibraryID.Caption))
                .lblSourceFormat.Caption = GetData("library", "format", "libraryID", Val(.lblSourceLibraryID.Caption))
            End If
        End If
        If .txtSourceEventID.Text = "" And .chkSourceFromMedia.Value <> 0 Then
            .lblSourceLibraryID.Caption = ""
            .txtSourcebarcode.Text = ""
            .lblSourceFormat.Caption = ""
        End If
        
        .txtTimecodeStart.Text = Trim(" " & l_rstClip("timecodestart"))
        .txtTimecodeStop.Text = Trim(" " & l_rstClip("timecodestop"))
        .txtDuration.Text = Trim(" " & l_rstClip("fd_length"))
        .txtEndCredits.Text = Trim(" " & l_rstClip("endcredits"))
        .txtEndCreditsElapsed.Text = Trim(" " & l_rstClip("endcreditselapsed"))
        .cmbAudioType1.Text = Trim(" " & l_rstClip("audiotypegroup1"))
        .cmbAudioType2.Text = Trim(" " & l_rstClip("audiotypegroup2"))
        .cmbAudioType3.Text = Trim(" " & l_rstClip("audiotypegroup3"))
        .cmbAudioType4.Text = Trim(" " & l_rstClip("audiotypegroup4"))
        .cmbAudioType5.Text = Trim(" " & l_rstClip("audiotypegroup5"))
        .cmbAudioType6.Text = Trim(" " & l_rstClip("audiotypegroup6"))
        .cmbAudioType7.Text = Trim(" " & l_rstClip("audiotypegroup7"))
        .cmbAudioType8.Text = Trim(" " & l_rstClip("audiotypegroup8"))
        .cmbAudioContent1.Text = Trim(" " & l_rstClip("audiocontentgroup1"))
        .cmbAudioContent2.Text = Trim(" " & l_rstClip("audiocontentgroup2"))
        .cmbAudioContent3.Text = Trim(" " & l_rstClip("audiocontentgroup3"))
        .cmbAudioContent4.Text = Trim(" " & l_rstClip("audiocontentgroup4"))
        .cmbAudioContent5.Text = Trim(" " & l_rstClip("audiocontentgroup5"))
        .cmbAudioContent6.Text = Trim(" " & l_rstClip("audiocontentgroup6"))
        .cmbAudioContent7.Text = Trim(" " & l_rstClip("audiocontentgroup7"))
        .cmbAudioContent8.Text = Trim(" " & l_rstClip("audiocontentgroup8"))
        .cmbAudioLanguage1.Text = Trim(" " & l_rstClip("audiolanguagegroup1"))
        .cmbAudioLanguage2.Text = Trim(" " & l_rstClip("audiolanguagegroup2"))
        .cmbAudioLanguage3.Text = Trim(" " & l_rstClip("audiolanguagegroup3"))
        .cmbAudioLanguage4.Text = Trim(" " & l_rstClip("audiolanguagegroup4"))
        .cmbAudioLanguage5.Text = Trim(" " & l_rstClip("audiolanguagegroup5"))
        .cmbAudioLanguage6.Text = Trim(" " & l_rstClip("audiolanguagegroup6"))
        .cmbAudioLanguage7.Text = Trim(" " & l_rstClip("audiolanguagegroup7"))
        .cmbAudioLanguage8.Text = Trim(" " & l_rstClip("audiolanguagegroup8"))
        .cmbLanguage.Text = Trim(" " & l_rstClip("language"))
        .cmbVodPlatform.Text = Trim(" " & l_rstClip("vodplatform"))
        If Not IsNull(l_rstClip("online")) Then
            .chkOnline.Value = Val(l_rstClip("online"))
        End If
        If Not IsNull(l_rstClip("fourbythreeflag")) Then
            .chkfourbythreeflag.Value = Val(l_rstClip("fourbythreeflag"))
        End If
        If Not IsNull(l_rstClip("hidefromweb")) Then
            .chkHideFromWeb.Value = GetFlag(Val(l_rstClip("hidefromweb")))
        End If
        
        .txtAltFolder.Text = Trim(" " & l_rstClip("altlocation"))
        .txtNotes.Text = Trim(" " & l_rstClip("notes"))
        .txtInternalNotes.Text = Trim(" " & l_rstClip("internalnotes"))
        .cmbGenre.Text = Trim(" " & l_rstClip("clipgenre"))
        .cmbClipType.Text = Trim(" " & l_rstClip("cliptype"))
        .txtClockNumber.Text = Trim(" " & l_rstClip("clocknumber"))
        .cmbPictFormat.Text = Trim(" " & l_rstClip("DADCPictureFormat"))
        .cmbForcedSubtitle.Text = Trim(" " & l_rstClip("DADCForcedSubtitle"))
        .cmbActiveRatio.Text = Trim(" " & l_rstClip("DADCActiveRatio"))
        .cmbBurnedInSubtitleLanguage.Text = Trim(" " & l_rstClip("BurnedInSubtitleLanguage"))
        
        If Not l_rstUsage.EOF Then
            .lblLastUsed.Caption = Format(l_rstUsage("dateused"), "dd MMM yyyy HH:NN:SS")
        Else
            .lblLastUsed.Caption = ""
        End If
        
        .txtTimecodeKeyframe.Text = Trim(" " & l_rstClip("eventkeyframetimecode"))
        .txtKeyframeFilename.Text = Trim(" " & l_rstClip("eventkeyframefilename"))
        .cmbStreamType.Text = Trim(" " & l_rstClip("mediastreamtype"))
        
        DoEvents
        
        'Try and Display the Keyframe, if the information in the database actually points to a file.
        If .txtKeyframeFilename.Text <> "" Then
            'If GetData("library", "format", "libraryID", .lblLibraryID.Caption) = "DISCSTORE" Then
'                .WebBrowser1.Navigate "http://online.rrsat.tv/DisplayKeyframe.asp?imagefile=/" & .lblCompanyID.Caption & "/" & .txtKeyframeFilename.Text
                Dim l_strNetworkPath As String
'                If .chkfourbythreeflag.Value = 0 Then
'                    .picKeyframe.Width = 4845
''                    .picKeyframe.Left = 11040
'                Else
'                    .picKeyframe.Width = 3660
''                    .picKeyframe.Left = 12220
'                End If
                l_strNetworkPath = g_strKeyframeStore
                l_strNetworkPath = l_strNetworkPath & "\" & .lblCompanyID.Caption

                l_strNetworkPath = l_strNetworkPath & "\" & .txtKeyframeFilename.Text

                On Error GoTo EMPTYKEYFRAME

                If Dir(l_strNetworkPath) = .txtKeyframeFilename.Text Then
                    If Right(.txtKeyframeFilename.Text, 4) = ".png" Then
                        PngPictureLoad l_strNetworkPath, .picKeyframe, True
                        .picKeyframe.Refresh
                        .picKeyframe.Visible = True
                    Else
                        .picKeyframe.Picture = LoadPicture(l_strNetworkPath)
                        .picKeyframe.Refresh
                        .picKeyframe.Visible = True
                    End If
                Else
EMPTYKEYFRAME:
                    .picKeyframe.Width = 4845
                    .picKeyframe.Picture = LoadPicture(App.Path & "\Empty.gif")
                    .picKeyframe.Refresh
                    .picKeyframe.Visible = False
                End If
                On Error GoTo 0
            'End If
        Else
            On Error Resume Next
            .picKeyframe.Width = 4845
            .picKeyframe.Picture = LoadPicture(App.Path & "\Empty.gif")
            .picKeyframe.Refresh
            .picKeyframe.Visible = False
        End If
        On Error GoTo 0
    
        'Try and load up the Custom Field Labels and dropdowns, and display the custom fields.
        'Custom field labels are stored as part of the company table, for the owner of the clip.
        Dim l_strTempLabel As String, l_lngCompanyID As Long
        
        l_lngCompanyID = Val(.lblCompanyID.Caption)
        
        If l_lngCompanyID <> 0 Then
    
            Dim l_conSearch As ADODB.Connection
            Dim l_rstSearch2 As ADODB.Recordset
            
            Set l_conSearch = New ADODB.Connection
            Set l_rstSearch2 = New ADODB.Recordset
            
            l_conSearch.ConnectionString = g_strConnection
            l_conSearch.Open
            
            l_strSQL = "SELECT * FROM masterseriestitle WHERE companyID = " & l_lngCompanyID & " ORDER BY title;"
            
            With l_rstSearch2
                .CursorLocation = adUseClient
                .LockType = adLockBatchOptimistic
                .CursorType = adOpenDynamic
                .Open l_strSQL, l_conSearch, adOpenDynamic
            End With
            
            l_rstSearch2.ActiveConnection = Nothing
            
            Set .txtTitle.DataSourceList = l_rstSearch2
            
            l_conSearch.Close
            Set l_conSearch = Nothing
            
            .lblField1.Caption = Trim(" " & GetData("company", "customfield1label", "companyID", l_lngCompanyID))
            If .lblField1.Caption = "" Then
                .lblField1.Visible = False
                .cmbField1.Visible = False
            Else
                .lblField1.Visible = True
                .cmbField1.Visible = True
                PopulateCustomFieldCombo l_lngCompanyID, 1, .cmbField1
            End If
            .lblField2.Caption = Trim(" " & GetData("company", "customfield2label", "companyID", l_lngCompanyID))
            If .lblField2.Caption = "" Then
                .lblField2.Visible = False
                .cmbField2.Visible = False
            Else
                .lblField2.Visible = True
                .cmbField2.Visible = True
                PopulateCustomFieldCombo l_lngCompanyID, 2, .cmbField2
            End If
            .lblField3.Caption = Trim(" " & GetData("company", "customfield3label", "companyID", l_lngCompanyID))
            If .lblField3.Caption = "" Then
                .lblField3.Visible = False
                .cmbField3.Visible = False
            Else
                .lblField3.Visible = True
                .cmbField3.Visible = True
                PopulateCustomFieldCombo l_lngCompanyID, 3, .cmbField3
            End If
            .lblField4.Caption = Trim(" " & GetData("company", "customfield4label", "companyID", l_lngCompanyID))
            If .lblField4.Caption = "" Then
                .lblField4.Visible = False
                .cmbField4.Visible = False
            Else
                .lblField4.Visible = True
                .cmbField4.Visible = True
                PopulateCustomFieldCombo l_lngCompanyID, 4, .cmbField4
            End If
            .lblField5.Caption = Trim(" " & GetData("company", "customfield5label", "companyID", l_lngCompanyID))
            If .lblField5.Caption = "" Then
                .lblField5.Visible = False
                .cmbField5.Visible = False
            Else
                .lblField5.Visible = True
                .cmbField5.Visible = True
                PopulateCustomFieldCombo l_lngCompanyID, 5, .cmbField5
            End If
            .lblField6.Caption = Trim(" " & GetData("company", "customfield6label", "companyID", l_lngCompanyID))
            If .lblField6.Caption = "" Then
                .lblField6.Visible = False
                .cmbField6.Visible = False
            Else
                .lblField6.Visible = True
                .cmbField6.Visible = True
                PopulateCustomFieldCombo l_lngCompanyID, 6, .cmbField6
            End If
    
        End If
        
        .cmbField1.Text = Trim(" " & l_rstClip("customfield1"))
        .cmbField2.Text = Trim(" " & l_rstClip("customfield2"))
        .cmbField3.Text = Trim(" " & l_rstClip("customfield3"))
        .cmbField4.Text = Trim(" " & l_rstClip("customfield4"))
        .cmbField5.Text = Trim(" " & l_rstClip("customfield5"))
        .cmbField6.Text = Trim(" " & l_rstClip("customfield6"))
        
        'Load up the keywords
        .adoKeywords.ConnectionString = g_strConnection
        l_strSQL = "SELECT eventID, keywordtext, clipreference, companyID "
        l_strSQL = l_strSQL & " FROM eventkeyword "
        l_strSQL = l_strSQL & " WHERE clipreference = '" & .txtReference.Text & "' and companyID = " & l_lngCompanyID & ";"
        .adoKeywords.RecordSource = l_strSQL
        .adoKeywords.Refresh
    
        .adoKeyword.ConnectionString = g_strConnection
        .adoKeyword.RecordSource = "SELECT keywordtext, keywordID FROM keyword WHERE companyID = " & l_lngCompanyID & "ORDER BY keywordtext;"
        .adoKeyword.Refresh
    
        If .adoKeyword.Recordset.RecordCount <> 0 Then .grdKeyword.Columns("keywordtext").DropDownHwnd = .ddnKeyword.hWnd
    
        .adoClientKeywords.ConnectionString = g_strConnection
        .adoClientKeywords.RecordSource = "SELECT keywordtext, companyID from keyword WHERE companyID = " & l_lngCompanyID & ";"
        .adoClientKeywords.Refresh
    
        'Set the Portal Permission Stuff
    
        .adoPortalPermission.ConnectionString = g_strConnection
        .adoPortalPermission.RecordSource = "SELECT * FROM portalpermission WHERE eventID = " & lp_lngClipID & " ORDER BY fullname;"
        .adoPortalPermission.Refresh
        If InStr(GetData("company", "cetaclientcode", "companyID", Val(frmClipControl.lblCompanyID.Caption)), "/timedpermissions") > 0 Then
            frmClipControl.grdPortalPermission.Columns("permissionstart").Visible = True
            frmClipControl.grdPortalPermission.Columns("permissionend").Visible = True
            frmClipControl.grdPortalPermission.Columns("fullname").Width = 2100
        Else
            frmClipControl.grdPortalPermission.Columns("permissionstart").Visible = False
            frmClipControl.grdPortalPermission.Columns("permissionend").Visible = False
            frmClipControl.grdPortalPermission.Columns("fullname").Width = 5490
        End If
        
'        If GetData("company", "source", "companyID", Val(frmClipControl.lblCompanyID.Caption)) = "SVENSK" Then
'            frmClipControl.cmdSvenskValidationData.Visible = True
'            frmClipControl.cmdUpdateSvenskAudio.Visible = True
'            frmClipControl.cmdSvenskItems.Visible = True
'        Else
'            frmClipControl.cmdSvenskValidationData.Visible = False
'            frmClipControl.cmdUpdateSvenskAudio.Visible = False
'            frmClipControl.cmdSvenskItems.Visible = False
'        End If
'
        .adoChecksumHistory.ConnectionString = g_strConnection
        .adoChecksumHistory.RecordSource = "SELECT * FROM eventchecksumhistory WHERE eventID = " & lp_lngClipID & " ORDER BY CASE WHEN cdate IS NULL THEN 1 ELSE 0 END, cdate desc;"
        .adoChecksumHistory.Refresh
    
        .adoPortalUsers.ConnectionString = g_strConnection
        .adoPortalUsers.RecordSource = "SELECT * from portaluser WHERE companyID = " & l_lngCompanyID & " AND activeuser = 1 ORDER BY fullname;"
        .adoPortalUsers.Refresh
    
        .adoCustomFieldDefs.ConnectionString = g_strConnection
        .adoCustomFieldDefs.RecordSource = "SELECT * from customfieldxref where companyID = " & l_lngCompanyID & "ORDER BY fd_order, textentry;"
        .adoCustomFieldDefs.Refresh
    
        If .adoPortalUsers.Recordset.RecordCount <> 0 Then .grdPortalPermission.Columns("fullname").DropDownHwnd = .ddnPortalUsers.hWnd
        
        'Check the internal refernce is set, and try and set it from parent tape/clip information if it isn't.
        
        If Val(.txtInternalReference.Text) = 0 Then
            'Clip has no internal refernce - try and set it from the clip's parent
            Dim l_lngParentEventID As Long, l_lngParentTapeID As Long, l_varParentEventMediaType As Variant
            l_lngParentEventID = Val(.txtSourceEventID.Text)
            If l_lngParentEventID <> 0 Then
                l_varParentEventMediaType = GetData("events", "eventmediatype", "eventID", l_lngParentEventID)
                If Not IsNull(l_varParentEventMediaType) And l_varParentEventMediaType = "FILE" Then
                    .txtInternalReference.Text = Trim(" " & GetData("events", "internalreference", "eventID", l_lngParentEventID))
                    If Val(.txtInternalReference.Text) = 0 Then
                        .txtInternalReference.Text = GetNextSequence("internalreference")
                        SetData "events", "internalreference", "eventID", l_lngParentEventID, Val(.txtInternalReference.Text)
                        If IsNull(GetData("events", "clipreference", "eventID", l_lngParentEventID)) Or GetData("events", "clipreference", "eventID", l_lngParentEventID) = "" Then
                            SetData "events", "clipreference", "eventID", l_lngParentEventID, Val(.txtInternalReference.Text)
                        End If
                    End If
                    SetData "events", "internalreference", "eventID", lp_lngClipID, Val(.txtInternalReference.Text)
                Else
                    l_lngParentTapeID = Val(.lblSourceLibraryID.Caption)
                    If l_lngParentTapeID <> 0 Then
                        .txtInternalReference.Text = Trim(" " & GetData("library", "internalreference", "libraryID", l_lngParentTapeID))
                        If Val(.txtInternalReference.Text) = 0 And GetData("library", "format", "libraryID", l_lngParentTapeID) <> "DISCSTORE" Then
                            .txtInternalReference.Text = GetNextSequence("internalreference")
                            SetData "library", "internalreference", "libraryID", l_lngParentTapeID, Val(.txtInternalReference.Text)
                        End If
                        SetData "events", "internalreference", "eventID", lp_lngClipID, Val(.txtInternalReference.Text)
                    End If
                End If
            End If
        End If
        
        If Val(.txtInternalReference.Text) = 0 Then
            .txtInternalReference.Text = GetNextSequence("internalreference")
            SetData "events", "internalreference", "eventID", lp_lngClipID, Val(.txtInternalReference.Text)
        End If
        
        If IsNull(GetData("events", "clipreference", "eventID", lp_lngClipID)) Or GetData("events", "clipreference", "eventID", lp_lngClipID) = "" Then
            SetData "events", "clipreference", "eventID", lp_lngClipID, Val(.txtInternalReference.Text)
            .txtReference.Text = .txtInternalReference.Text
        End If
    
        l_strSQL = "SELECT * FROM eventhistory WHERE eventID = " & lp_lngClipID & " ORDER BY datesaved DESC;"
        .adoHistory.RecordSource = l_strSQL
        .adoHistory.ConnectionString = g_strConnection
        .adoHistory.Refresh
        
        l_strSQL = "SELECT contactID, '' AS fullname, 0 AS distributionuserID, 0 AS portaluserID, accesstype, timewhen FROM webdelivery WHERE clipID = " & lp_lngClipID & _
            " UNION SELECT 0 AS contactID, fullname, 0 AS distributionuserID, portaluserID, accesstype, timewhen FROM webdeliveryportal WHERE clipid = " & lp_lngClipID & _
            " UNION SELECT 0 AS contactID, fullname, distributionuserID, 0 as portaluserID, accesstype, timewhen FROM distributiondelivery WHERE clipID = " & lp_lngClipID & " ORDER BY timewhen DESC;"
        .adoMediaWindowHistory.RecordSource = l_strSQL
        .adoMediaWindowHistory.ConnectionString = g_strConnection
        .adoMediaWindowHistory.Refresh
        
        'Load the clip logging
        l_strSQL = "SELECT * FROM eventlogging WHERE eventID = " & lp_lngClipID & " ORDER BY timecodestart ASC;"
        .adoLogging.RecordSource = l_strSQL
        .adoLogging.ConnectionString = g_strConnection
        .adoLogging.Refresh
        
        'Load the web clip logging
        l_strSQL = "SELECT * FROM eventlogging WHERE companyID = " & l_lngCompanyID & "AND clipreference = '" & .txtReference.Text & "' AND framerate = '" & .cmbFrameRate.Text & "' ORDER BY timecodestart ASC;"
        .adoWebLogging.RecordSource = l_strSQL
        .adoWebLogging.ConnectionString = g_strConnection
        .adoWebLogging.Refresh
        
        'Load the Compilation details
        l_strSQL = "SELECT sourceclipID, forder, eventID FROM eventcompilation WHERE eventID = " & lp_lngClipID & " ORDER BY forder ASC;"
        .adoCompilation.RecordSource = l_strSQL
        .adoCompilation.ConnectionString = g_strConnection
        .adoCompilation.Refresh
        
        'check if Tech Review exists
        
        If Trim(" " & .txtClipfilename.Text) <> "" And Trim(" " & .lblFileSize.Caption) <> "" And Trim(" " & .txtMD5Checksum.Text) <> "" Then
            If GetDataSQL("SELECT TOP 1 techrevID FROM techrev WHERE system_deleted = 0 AND companyID = " & Val(.lblCompanyID.Caption) & " AND filename = '" & .txtClipfilename.Text & "' AND bigfilesize = " & Format(.lblFileSize.Caption, "#") & " AND md5checksum = '" & .txtMD5Checksum.Text & "';") <> 0 Then
                .lblTechReviewAvalable.Visible = True
            Else
                .lblTechReviewAvalable.Visible = False
            End If
        ElseIf GetData("techrev", "techrevID", "eventID", lp_lngClipID) <> 0 Then
            .lblTechReviewAvalable.Visible = True
        Else
            .lblTechReviewAvalable.Visible = False
        End If
        
        'Grey out XML buttons if company flags are not set
        If g_blnRedNetwork = True Then
            If InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/dadctracker") > 0 Then
                .cmdDADCOutput.Enabled = True
                .cmdDADCOutputAudioOnly.Enabled = True
            Else
                .cmdDADCOutput.Enabled = False
                .cmdDADCOutputAudioOnly.Enabled = False
            End If
        Else
            .cmdDADCOutput.Enabled = False
            .cmdDADCOutputAudioOnly.Enabled = False
        End If
    
        
'        'Check if audio analysis info exists
'        If Trim(" " & .txtReference.Text) <> "" And Trim(" " & .lblFileSize.Caption) <> "" Then
'            If GetDataSQL("SELECT status_name FROM JelimediaAnalysis WHERE companyID = " & Val(.lblCompanyID.Caption) & " AND clipreference = '" & .txtReference.Text & "';") = "COMPLETE" Then
'                l_strSQL = "SELECT * FROM JelimediaAnalysis WHERE companyID = " & Val(.lblCompanyID.Caption) & " AND clipreference = '" & QuoteSanitise(.txtReference.Text) & "' ORDER BY dateanalysis DESC;"
'                Set l_rstData = ExecuteSQL(l_strSQL, g_strExecuteError)
'                If l_rstData.RecordCount > 0 Then
'                    l_rstData.MoveFirst
'                    .lblAnalysisDate.Caption = Format(l_rstData("dateanalysis"), "dd mmm yy hh:nn:ss")
'                    If l_rstData("lang_code_1_present") <> 0 Then
'                        .lblLangCode(1).Caption = l_rstData("lang_code_1")
'                        .lblLangConfidence(1).Caption = l_rstData("lang_code_1_confidence")
'                    End If
'                    If l_rstData("lang_code_2_present") <> 0 Then
'                        .lblLangCode(2).Caption = l_rstData("lang_code_2")
'                        .lblLangConfidence(2).Caption = l_rstData("lang_code_2_confidence")
'                    End If
'                    If l_rstData("lang_code_3_present") <> 0 Then
'                        .lblLangCode(3).Caption = l_rstData("lang_code_3")
'                        .lblLangConfidence(3).Caption = l_rstData("lang_code_3_confidence")
'                    End If
'                End If
'                .fraLanguageAnalysisData.Visible = True
'            Else
'                .fraLanguageAnalysisData.Visible = False
'            End If
'        Else
'            .fraLanguageAnalysisData.Visible = False
'        End If
'
    End With
Else
    If lp_lngClipID <> 0 Then MsgBox "Clip:" & lp_lngClipID & " Not Found.", vbCritical, "Error..."
    If frmClipControl.txtClipID.Text = "" Then
    
        ClearFields frmClipControl
        frmClipControl.lblDeletedClip.Visible = False
        frmClipControl.cmdDelete.Picture = LoadPicture(App.Path & "\Delete_File.gif")
        
        With frmClipControl
            
            'Load up the keywords
            .adoKeywords.ConnectionString = g_strConnection
            l_strSQL = "SELECT eventID, keywordtext "
            l_strSQL = l_strSQL & " FROM eventkeyword "
            l_strSQL = l_strSQL & " WHERE eventID = -1;"
            .adoKeywords.RecordSource = l_strSQL
            .adoKeywords.Refresh
            
            .adoKeyword.ConnectionString = g_strConnection
            .adoKeyword.RecordSource = "SELECT keywordtext, keywordID FROM keyword WHERE companyID = -1 ORDER BY keywordtext;"
            .adoKeyword.Refresh
            .grdKeyword.Columns("keywordtext").DropDownHwnd = 0
            
            .adoClientKeywords.ConnectionString = g_strConnection
            .adoClientKeywords.RecordSource = "SELECT keywordtext, companyID from keyword WHERE companyID = -1;"
            .adoClientKeywords.Refresh
            
            'Set the Portal Permission Stuff
            
            .adoPortalPermission.ConnectionString = g_strConnection
            .adoPortalPermission.RecordSource = "SELECT * FROM portalpermission WHERE eventID = -1 ORDER BY fullname;"
            .adoPortalPermission.Refresh
            
            .adoPortalUsers.ConnectionString = g_strConnection
            .adoPortalUsers.RecordSource = "SELECT * from portaluser WHERE companyID = -1;"
            .adoPortalUsers.Refresh
            
            .adoChecksumHistory.ConnectionString = g_strConnection
            .adoChecksumHistory.RecordSource = "SELECT * FROM eventchecksumhistory WHERE eventID = -1 ORDER BY cdate;"
            .adoChecksumHistory.Refresh
            
            .adoHistory.ConnectionString = g_strConnection
            .adoHistory.RecordSource = "SELECT * FROM eventhistory WHERE eventID = -1 ORDER BY datesaved DESC;"
            .adoHistory.Refresh
                
            .adoMediaWindowHistory.ConnectionString = g_strConnection
            .adoMediaWindowHistory.RecordSource = "SELECT * FROM webdelivery WHERE clipID = -1 ORDER BY timewhen DESC;"
            .adoMediaWindowHistory.Refresh
                
            .grdPortalPermission.Columns("fullname").DropDownHwnd = 0
            On Error Resume Next
            .picKeyframe.Picture = LoadPicture(App.Path & "\Empty.gif")
            .picKeyframe.Refresh
        
        End With
    End If
End If

l_rstClip.Close
Set l_rstClip = Nothing
l_rstUsage.Close
Set l_rstUsage = Nothing

End Sub

Function ValidateFilename(sFileName) As Boolean
    Dim myExpression As RegExp
    
    Set myExpression = New RegExp
    myExpression.Pattern = "^[a-zA-Z0-9\.\_][a-zA-Z0-9\_\-\.]*[a-zA-Z0-9]\.*[a-zA-Z0-9]{0,16}$"

    If myExpression.Test(sFileName) = True And sFileName <> ".DS_Store" And sFileName <> "._.DS_Store" And sFileName <> "Thumbs.db" Then
        ValidateFilename = True
    Else
        ValidateFilename = False
    End If
End Function

Function ValidateFoldername(sFileName) As Boolean
    Dim myExpression As RegExp
    
    Set myExpression = New RegExp
    myExpression.Pattern = "^[a-zA-Z0-9\.\_][a-zA-Z0-9\_\-\.\\ ]*[a-zA-Z0-9]{0,255}$"

    If myExpression.Test(IIf(IsNull(sFileName), "", sFileName)) = True Then
        ValidateFoldername = True
    Else
        ValidateFoldername = False
    End If
End Function

Public Function CopyFileEventToLibraryID(lp_lngEventID As Long, lp_lngLibraryID As Long, Optional lp_blnBellsAndWhistles As Boolean) As Long

'copy the info from one job to another
Dim l_rstOriginalEvent As ADODB.Recordset
Dim l_rstNewEvent As ADODB.Recordset, l_lngNewEventID As Long
Dim l_rstTemp As ADODB.Recordset, l_lngtechrevID As Long, l_rstExtras As ADODB.Recordset

'get the original job's details
Dim l_strSQL As String

l_strSQL = "INSERT INTO events("
l_strSQL = l_strSQL & "eventmediatype, libraryID, companyID) VALUES ("
l_strSQL = l_strSQL & "'FILE', " & lp_lngLibraryID & ", 1)"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_lngNewEventID = g_lngLastID

l_strSQL = "SELECT * FROM events WHERE eventID = '" & lp_lngEventID & "';"
Set l_rstOriginalEvent = ExecuteSQL(l_strSQL, g_strExecuteError)

'allow adding of a new job
l_strSQL = "SELECT * FROM events WHERE eventID = '" & l_lngNewEventID & "';"
Set l_rstNewEvent = ExecuteSQL(l_strSQL, g_strExecuteError)

Dim l_intLoop As Integer
Dim l_lngConflict As Long
    
If l_rstOriginalEvent.RecordCount <= 0 Then Exit Function
If l_rstNewEvent.RecordCount <= 0 Then Exit Function

l_rstNewEvent("libraryID") = lp_lngLibraryID

For l_intLoop = 0 To l_rstOriginalEvent.Fields.Count - 1
    If l_rstOriginalEvent.Fields(l_intLoop).Name <> "libraryID" And l_rstOriginalEvent.Fields(l_intLoop).Name <> "eventID" And l_rstOriginalEvent.Fields(l_intLoop).Name <> "sound_stereo_russian" Then
    
        If Not IsNull(l_rstOriginalEvent(l_intLoop)) Then
            l_rstNewEvent(l_intLoop) = l_rstOriginalEvent(l_intLoop)
        End If
    
    End If
Next

l_rstNewEvent("mdate") = Null
l_rstNewEvent("muser") = Null

l_rstNewEvent.Update

'MsgBox g_lngLastID, vbExclamation

'close the original record
l_rstOriginalEvent.Close

'close the new record
l_rstNewEvent.Close

Set l_rstOriginalEvent = Nothing
Set l_rstNewEvent = Nothing

If lp_blnBellsAndWhistles = True Then
    'Make a history entry for this copy
    l_strSQL = "INSERT INTO eventhistory (eventID, datesaved, description, username) VALUES ("
    l_strSQL = l_strSQL & l_lngNewEventID & ", "
    l_strSQL = l_strSQL & "getdate(), "
    l_strSQL = l_strSQL & "'Copied from " & GetData("library", "barcode", "libraryID", GetData("events", "libraryID", "eventID", lp_lngEventID)) & "', "
    l_strSQL = l_strSQL & "'" & g_strUserInitials & "');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'Duplicate any Segment Information
    l_strSQL = "SELECT * FROM eventsegment WHERE eventID = '" & lp_lngEventID & "';"
    Set l_rstExtras = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    If l_rstExtras.RecordCount > 0 Then
        l_rstExtras.MoveFirst
        
        Do While Not l_rstExtras.EOF
            l_strSQL = "INSERT INTO eventsegment (eventID, segmentreference, timecodestart, timecodestop, note) VALUES ('"
            l_strSQL = l_strSQL & l_lngNewEventID & "', '"
            l_strSQL = l_strSQL & QuoteSanitise(l_rstExtras("segmentreference")) & "', '"
            l_strSQL = l_strSQL & l_rstExtras("timecodestart") & "', '"
            l_strSQL = l_strSQL & l_rstExtras("timecodestop") & "', '"
            l_strSQL = l_strSQL & QuoteSanitise(l_rstExtras("note")) & "');"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            l_rstExtras.MoveNext
        Loop
    End If
    l_rstExtras.Close
    
    'Duplicate any Logging Information
    l_strSQL = "SELECT * FROM eventlogging WHERE eventID = '" & lp_lngEventID & "';"
    Set l_rstExtras = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    If l_rstExtras.RecordCount > 0 Then
        l_rstExtras.MoveFirst
        
        Do While Not l_rstExtras.EOF
            l_strSQL = "INSERT INTO eventlogging (eventID, segmentreference, timecodestart, timecodestop, note) VALUES ('"
            l_strSQL = l_strSQL & l_lngNewEventID & "', '"
            l_strSQL = l_strSQL & QuoteSanitise(l_rstExtras("segmentreference")) & "', '"
            l_strSQL = l_strSQL & l_rstExtras("timecodestart") & "', '"
            l_strSQL = l_strSQL & l_rstExtras("timecodestop") & "', '"
            l_strSQL = l_strSQL & QuoteSanitise(l_rstExtras("note")) & "');"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            l_rstExtras.MoveNext
        Loop
    End If
    l_rstExtras.Close
    
    'Duplicate any Ratings Advisory Information
    l_strSQL = "SELECT * FROM iTunes_Clip_Advisory WHERE eventID = '" & lp_lngEventID & "';"
    Set l_rstExtras = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    If l_rstExtras.RecordCount > 0 Then
        l_rstExtras.MoveFirst
        
        Do While Not l_rstExtras.EOF
            l_strSQL = "INSERT INTO iTunes_Clip_Advisory (eventID, advisorysystem, advisory) VALUES ('"
            l_strSQL = l_strSQL & l_lngNewEventID & "', '"
            l_strSQL = l_strSQL & l_rstExtras("advisorysystem") & "', '"
            l_strSQL = l_strSQL & l_rstExtras("advisory") & "');"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            l_rstExtras.MoveNext
        Loop
    End If
    l_rstExtras.Close
    
    'Duplicate any Products Information
    l_strSQL = "SELECT * FROM iTunes_product WHERE eventID = '" & lp_lngEventID & "';"
    Set l_rstExtras = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    If l_rstExtras.RecordCount > 0 Then
        l_rstExtras.MoveFirst
        
        Do While Not l_rstExtras.EOF
            l_strSQL = "INSERT INTO iTunes_product (eventID, territory, salesstartdate, clearedforsale, wholesalepricetier, preordersalesstartdate, "
            l_strSQL = l_strSQL & "clearedforvod, salesenddate, vodtype, availableforvoddate, unavailableforvoddate, physicalreleasedate) VALUES ('"
            l_strSQL = l_strSQL & l_lngNewEventID & "', '"
            l_strSQL = l_strSQL & l_rstExtras("territory") & "', '"
            l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("salesstartdate")) & "', '"
            l_strSQL = l_strSQL & l_rstExtras("clearedforsale") & "', '"
            l_strSQL = l_strSQL & l_rstExtras("wholesalepricetier") & "', '"
            l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("preordersalesstartdate")) & "', '"
            l_strSQL = l_strSQL & l_rstExtras("clearedforvod") & "', '"
            l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("salesenddate")) & "', '"
            l_strSQL = l_strSQL & l_rstExtras("vodtype") & "', '"
            l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("availableforvoddate")) & "', '"
            l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("unavailableforvoddate")) & "', '"
            l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("physicalreleasedate")) & "');"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            l_rstExtras.MoveNext
        Loop
    End If
    l_rstExtras.Close
    Set l_rstExtras = Nothing
    
    'Make a history entry for the original of this copy
    l_strSQL = "INSERT INTO eventhistory (eventID, datesaved, description, username) VALUES ("
    l_strSQL = l_strSQL & lp_lngEventID & ", "
    l_strSQL = l_strSQL & "getdate(), "
    l_strSQL = l_strSQL & "'Copied to " & GetData("library", "barcode", "libraryID", lp_lngLibraryID) & "', "
    l_strSQL = l_strSQL & "'" & g_strUserInitials & "');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
End If

CopyFileEventToLibraryID = l_lngNewEventID

End Function

Function CheckFileOnDisc(lp_strClipfilename As String, lp_curBigfilesize As Currency, lp_lngCompanyID As Long) As Boolean

Dim l_strSQL As String, l_rsItem As ADODB.Recordset

l_strSQL = "SELECT eventID FROM vwEventsWithLibraryFormat WHERE format = 'DISCSTORE' AND clipfilename = '" & QuoteSanitise(lp_strClipfilename) & "' AND bigfilesize = " & lp_curBigfilesize & " AND companyID = " & lp_lngCompanyID & " AND system_deleted = 0;"
Set l_rsItem = ExecuteSQL(l_strSQL, g_strExecuteError)
If l_rsItem.RecordCount > 0 Then
    CheckFileOnDisc = True
Else
    CheckFileOnDisc = False
End If
l_rsItem.Close
Set l_rsItem = Nothing

End Function

Function CheckFileOnDeliveryStore(lp_lngEventID As Long) As Boolean

Dim l_strSQL As String, l_rsItem As ADODB.Recordset

l_strSQL = "SELECT eventID FROM vw_Events_On_DeliveryStore WHERE eventID = " & lp_lngEventID & " AND system_deleted = 0;"
Set l_rsItem = ExecuteSQL(l_strSQL, g_strExecuteError)
If l_rsItem.RecordCount > 0 Then
    CheckFileOnDeliveryStore = True
Else
    CheckFileOnDeliveryStore = False
End If
l_rsItem.Close
Set l_rsItem = Nothing

End Function

Function CheckDriveWritePermissions(lp_lngLibraryID As Long) As Boolean

Dim l_lngWriterPermissions As Long

CheckDriveWritePermissions = False
l_lngWriterPermissions = Val(Trim(" " & GetData("library", "DriveWritePermissionInteger", "libraryID", lp_lngLibraryID)))

If (l_lngWriterPermissions And 128) = 128 Then
    CheckDriveWritePermissions = False
ElseIf l_lngWriterPermissions = 0 Then
    CheckDriveWritePermissions = True
Else
    If (l_lngWriterPermissions And 1) = 1 Then
        If CheckAccess("/OpsWriter", True) = True Then
            CheckDriveWritePermissions = True
        End If
    End If
    
    If (l_lngWriterPermissions And 2) = 2 Then
        If CheckAccess("/OfficeWriter", True) = True Then
            CheckDriveWritePermissions = True
        End If
    End If
    
    If (l_lngWriterPermissions And 4) = 4 Then
        If CheckAccess("/OpsAdminWriter", True) = True Then
            CheckDriveWritePermissions = True
        End If
    End If
    
    If (l_lngWriterPermissions And 8) = 8 Then
        If CheckAccess("/WarpWriter", True) = True Then
            CheckDriveWritePermissions = True
        End If
    End If

'    If (l_lngWriterPermissions And 16) = 16 Then
'        If CheckAccess("/OpsAdminWriter", True) = True Then
'            CheckDriveWritePermissions = True
'        End If
'    End If
'
'    If (l_lngWriterPermissions And 32) = 32 Then
'        If CheckAccess("/OpsAdminWriter", True) = True Then
'            CheckDriveWritePermissions = True
'        End If
'    End If
    
    If (l_lngWriterPermissions And 64) = 64 Then
        If CheckAccess("/Superuser", True) = True Then
            CheckDriveWritePermissions = True
        End If
    End If
End If

End Function

