VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmSearchJob 
   Caption         =   "Job Search"
   ClientHeight    =   10275
   ClientLeft      =   255
   ClientTop       =   2175
   ClientWidth     =   19800
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmJobSearch.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   10275
   ScaleWidth      =   19800
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdBulkCreditNotes 
      Caption         =   "Bulk Credit Notes"
      Height          =   555
      Left            =   15420
      TabIndex        =   102
      Top             =   240
      Width           =   1155
   End
   Begin VB.CheckBox chkJobStatus 
      Caption         =   "Masters Here"
      Height          =   195
      Index           =   15
      Left            =   1860
      TabIndex        =   101
      ToolTipText     =   "Checked jobs"
      Top             =   3360
      Value           =   1  'Checked
      Width           =   1275
   End
   Begin VB.TextBox txtCostingsTotal 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0FFC0&
      Height          =   315
      Left            =   1860
      TabIndex        =   100
      Top             =   2580
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.CheckBox chkJobStatus 
      Caption         =   "Pending"
      Height          =   195
      Index           =   14
      Left            =   5400
      TabIndex        =   99
      ToolTipText     =   "2nd penciled jobs"
      Top             =   3360
      Value           =   1  'Checked
      Width           =   1155
   End
   Begin VB.CheckBox chkJobStatus 
      Caption         =   "VT Done"
      Height          =   195
      Index           =   13
      Left            =   4320
      TabIndex        =   98
      ToolTipText     =   "2nd penciled jobs"
      Top             =   3360
      Value           =   1  'Checked
      Width           =   975
   End
   Begin VB.CheckBox chkCosting 
      Caption         =   "Show Costing Total"
      Height          =   195
      Left            =   60
      TabIndex        =   88
      ToolTipText     =   "Show Our Contact instead of Allocated To"
      Top             =   2640
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CheckBox chkJobStatus 
      Caption         =   "Returned"
      Height          =   195
      Index           =   12
      Left            =   16800
      TabIndex        =   87
      ToolTipText     =   "Checked jobs"
      Top             =   3360
      Value           =   1  'Checked
      Width           =   975
   End
   Begin VB.CheckBox chkShowResources 
      Caption         =   "Show Resources"
      Height          =   195
      Left            =   60
      TabIndex        =   84
      ToolTipText     =   "Show Our Contact instead of Allocated To"
      Top             =   2400
      Value           =   1  'Checked
      Width           =   1695
   End
   Begin VB.CheckBox chkJobStatus 
      Caption         =   "Quickie"
      Height          =   195
      Index           =   11
      Left            =   13500
      TabIndex        =   83
      ToolTipText     =   "Cancelled jobs"
      Top             =   3360
      Width           =   855
   End
   Begin VB.CheckBox chkJobStatus 
      Caption         =   "No Charge"
      Height          =   195
      Index           =   10
      Left            =   11280
      TabIndex        =   82
      ToolTipText     =   "Costed jobs"
      Top             =   3360
      Value           =   1  'Checked
      Width           =   1095
   End
   Begin VB.CheckBox chkJobStatus 
      Caption         =   "Prepared"
      Height          =   195
      Index           =   9
      Left            =   15780
      TabIndex        =   80
      ToolTipText     =   "Checked jobs"
      Top             =   3360
      Value           =   1  'Checked
      Width           =   975
   End
   Begin VB.CheckBox chkJobStatus 
      Caption         =   "Despatched"
      Height          =   195
      Index           =   8
      Left            =   14580
      TabIndex        =   79
      ToolTipText     =   "Returned jobs"
      Top             =   3360
      Value           =   1  'Checked
      Width           =   1155
   End
   Begin MSAdodcLib.Adodc adoJobSearch 
      Height          =   330
      Left            =   0
      Top             =   2940
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Results"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CheckBox chkJobStatus 
      Caption         =   "Cancelled"
      Height          =   195
      Index           =   7
      Left            =   12420
      TabIndex        =   19
      ToolTipText     =   "Cancelled jobs"
      Top             =   3360
      Width           =   1035
   End
   Begin VB.CheckBox chkJobStatus 
      Caption         =   "Sent To Accounts"
      Height          =   195
      Index           =   6
      Left            =   9660
      TabIndex        =   18
      ToolTipText     =   "Jobs sent to accounts"
      Top             =   3360
      Value           =   1  'Checked
      Width           =   1575
   End
   Begin VB.CheckBox chkJobStatus 
      Caption         =   "Costed"
      Height          =   195
      Index           =   5
      Left            =   8760
      TabIndex        =   17
      ToolTipText     =   "Costed jobs"
      Top             =   3360
      Value           =   1  'Checked
      Width           =   855
   End
   Begin VB.CheckBox chkJobStatus 
      Caption         =   "Hold Cost"
      Height          =   195
      Index           =   4
      Left            =   7740
      TabIndex        =   16
      ToolTipText     =   "Jobs with costs held"
      Top             =   3360
      Value           =   1  'Checked
      Width           =   1035
   End
   Begin VB.CheckBox chkJobStatus 
      Caption         =   "Completed"
      Height          =   195
      Index           =   3
      Left            =   6660
      TabIndex        =   15
      ToolTipText     =   "Completed jobs"
      Top             =   3360
      Value           =   1  'Checked
      Width           =   1095
   End
   Begin VB.CheckBox chkJobStatus 
      Caption         =   "Confirmed"
      Height          =   195
      Index           =   2
      Left            =   840
      TabIndex        =   14
      ToolTipText     =   "Confirmed jobs"
      Top             =   3360
      Value           =   1  'Checked
      Width           =   1035
   End
   Begin VB.CheckBox chkJobStatus 
      Caption         =   "Pencil"
      Height          =   195
      Index           =   1
      Left            =   60
      TabIndex        =   13
      ToolTipText     =   "Penciled jobs"
      Top             =   3360
      Value           =   1  'Checked
      Width           =   735
   End
   Begin VB.CheckBox chkJobStatus 
      Caption         =   "In Progress"
      Height          =   195
      Index           =   0
      Left            =   3120
      TabIndex        =   12
      ToolTipText     =   "2nd penciled jobs"
      Top             =   3360
      Value           =   1  'Checked
      Width           =   1155
   End
   Begin VB.ComboBox cmbOrderBy 
      Height          =   315
      Left            =   3180
      TabIndex        =   69
      Tag             =   "NOCLEAR"
      Text            =   "cmbOrderBy"
      ToolTipText     =   "Which data field to search against"
      Top             =   8820
      Visible         =   0   'False
      Width           =   2235
   End
   Begin VB.ComboBox cmbDirection 
      Height          =   315
      ItemData        =   "frmJobSearch.frx":08CA
      Left            =   5460
      List            =   "frmJobSearch.frx":08D4
      Style           =   2  'Dropdown List
      TabIndex        =   68
      ToolTipText     =   "Which data field to search against"
      Top             =   8820
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Frame Frame6 
      Caption         =   "End Date..."
      Height          =   1095
      Left            =   4740
      TabIndex        =   61
      Top             =   1200
      Width           =   2235
      Begin MSComCtl2.DTPicker datEndDateFrom 
         Height          =   315
         Left            =   600
         TabIndex        =   10
         Tag             =   "NOCHECK"
         ToolTipText     =   "search end date (within Range)"
         Top             =   240
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   123404289
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datEndDateTo 
         Height          =   315
         Left            =   600
         TabIndex        =   11
         Tag             =   "NOCHECK"
         ToolTipText     =   "search start date (within Range)"
         Top             =   660
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   123404289
         CurrentDate     =   37870
      End
      Begin VB.Label lblCaption 
         Caption         =   "From"
         Height          =   255
         Index           =   21
         Left            =   120
         TabIndex        =   63
         Top             =   240
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Height          =   255
         Index           =   20
         Left            =   120
         TabIndex        =   62
         Top             =   600
         Width           =   1035
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Start Date..."
      Height          =   1095
      Left            =   4740
      TabIndex        =   58
      Top             =   60
      Width           =   2235
      Begin MSComCtl2.DTPicker datStartDateFrom 
         Height          =   315
         Left            =   600
         TabIndex        =   8
         Tag             =   "NOCHECK"
         ToolTipText     =   "search start date (within Range)"
         Top             =   240
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   123404289
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datStartDateTo 
         Height          =   315
         Left            =   600
         TabIndex        =   9
         Tag             =   "NOCHECK"
         ToolTipText     =   "search start date (within Range)"
         Top             =   660
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   123404289
         CurrentDate     =   37870
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Height          =   255
         Index           =   19
         Left            =   120
         TabIndex        =   60
         Top             =   600
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "From"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   59
         Top             =   240
         Width           =   1035
      End
   End
   Begin VB.PictureBox picFooter 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   3720
      ScaleHeight     =   315
      ScaleWidth      =   10395
      TabIndex        =   49
      Top             =   7860
      Width           =   10395
      Begin VB.CommandButton cmdSessionPrint 
         Caption         =   "Session List"
         Height          =   315
         Left            =   2940
         TabIndex        =   95
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   4200
         TabIndex        =   81
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   6720
         TabIndex        =   35
         ToolTipText     =   "Clear the form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   9240
         TabIndex        =   37
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   8040
         TabIndex        =   36
         ToolTipText     =   "Search using set criterea"
         Top             =   0
         Width           =   1155
      End
      Begin VB.ComboBox cmbMacroSearch 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1260
         Style           =   2  'Dropdown List
         TabIndex        =   33
         ToolTipText     =   "Previously stored searches to use"
         Top             =   0
         Visible         =   0   'False
         Width           =   1695
      End
      Begin VB.CommandButton cmdRun 
         Caption         =   "Run"
         Height          =   315
         Left            =   5460
         TabIndex        =   34
         ToolTipText     =   "Run the stored search"
         Top             =   0
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Stored searches:"
         Height          =   255
         Index           =   10
         Left            =   0
         TabIndex        =   50
         Top             =   0
         Visible         =   0   'False
         Width           =   1275
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Job Booked..."
      Height          =   1095
      Left            =   2400
      TabIndex        =   44
      Top             =   1200
      Width           =   2235
      Begin MSComCtl2.DTPicker datJobBookedFrom 
         Height          =   315
         Left            =   600
         TabIndex        =   6
         Tag             =   "NOCHECK"
         ToolTipText     =   "The date the job was booked"
         Top             =   240
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   123404289
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datJobBookedTo 
         Height          =   315
         Left            =   600
         TabIndex        =   7
         Tag             =   "NOCHECK"
         ToolTipText     =   "The date the job is due to end"
         Top             =   660
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   123404289
         CurrentDate     =   37870
      End
      Begin VB.Label lblCaption 
         Caption         =   "From"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   46
         Top             =   240
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   45
         Top             =   600
         Width           =   1035
      End
   End
   Begin VB.Frame fraJobSearch1 
      Caption         =   "Other Information..."
      Height          =   3195
      Left            =   7140
      TabIndex        =   42
      Top             =   60
      Width           =   8175
      Begin VB.TextBox txtResource 
         Height          =   255
         Left            =   5100
         TabIndex        =   96
         ToolTipText     =   "The clients order reference number"
         Top             =   2460
         Width           =   2895
      End
      Begin VB.TextBox txtCostedInitials 
         Height          =   255
         Left            =   3480
         TabIndex        =   93
         ToolTipText     =   "Users initials who booked the job"
         Top             =   2820
         Width           =   915
      End
      Begin VB.TextBox txtMCS 
         BackColor       =   &H00C0C0FF&
         Height          =   255
         Left            =   1260
         TabIndex        =   91
         ToolTipText     =   "The invoice number to search for"
         Top             =   2820
         Width           =   975
      End
      Begin VB.TextBox txtTeamMember 
         BackColor       =   &H00FFC0C0&
         Height          =   255
         Left            =   1260
         TabIndex        =   89
         ToolTipText     =   "The job subtitle to search for"
         Top             =   1740
         Width           =   2655
      End
      Begin VB.TextBox txtBookedBy 
         Height          =   255
         Left            =   1260
         TabIndex        =   25
         ToolTipText     =   "Users initials who booked the job"
         Top             =   2100
         Width           =   915
      End
      Begin VB.CommandButton cmdExport 
         Caption         =   "Export TAB seperated to jobsearch.txt"
         Height          =   435
         Left            =   4800
         TabIndex        =   32
         ToolTipText     =   "Export as text file"
         Top             =   2700
         Width           =   3195
      End
      Begin VB.TextBox txtCreditNoteNumber 
         BackColor       =   &H00C0C0FF&
         Height          =   255
         Left            =   3000
         TabIndex        =   24
         ToolTipText     =   "The Credit Note Number to search for"
         Top             =   1380
         Width           =   915
      End
      Begin VB.TextBox txtInvoiceNumber 
         BackColor       =   &H00C0C0FF&
         Height          =   255
         Left            =   1260
         TabIndex        =   23
         ToolTipText     =   "The invoice number to search for"
         Top             =   1380
         Width           =   975
      End
      Begin VB.TextBox txtSubTitle 
         Height          =   255
         Left            =   5100
         TabIndex        =   30
         ToolTipText     =   "The job subtitle to search for"
         Top             =   1380
         Width           =   2895
      End
      Begin VB.TextBox txtTitle 
         Height          =   255
         Left            =   5100
         TabIndex        =   29
         ToolTipText     =   "The job title to search for"
         Top             =   1020
         Width           =   2895
      End
      Begin VB.TextBox txtOrderNumber 
         Height          =   255
         Left            =   1260
         TabIndex        =   22
         ToolTipText     =   "The clients order reference number"
         Top             =   1020
         Width           =   2655
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
         Height          =   255
         Left            =   1260
         TabIndex        =   20
         ToolTipText     =   "The company to search for"
         Top             =   300
         Width           =   2655
         DataFieldList   =   "name"
         _Version        =   196617
         BorderStyle     =   0
         BeveColorScheme =   1
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   6376
         Columns(0).Caption=   "name"
         Columns(0).Name =   "name"
         Columns(0).DataField=   "name"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "companyID"
         Columns(1).Name =   "companyID"
         Columns(1).DataField=   "companyID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "accountcode"
         Columns(2).Name =   "accountcode"
         Columns(2).DataField=   "accountcode"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "telephone"
         Columns(3).Name =   "telephone"
         Columns(3).DataField=   "telephone"
         Columns(3).FieldLen=   256
         _ExtentX        =   4683
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   16761087
         DataFieldToDisplay=   "name"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbJobType 
         Height          =   255
         Left            =   5100
         TabIndex        =   31
         ToolTipText     =   "The job type to search for"
         Top             =   1740
         Width           =   2895
         BevelWidth      =   0
         DataFieldList   =   "Column 0"
         BevelType       =   0
         _Version        =   196617
         DataMode        =   2
         BorderStyle     =   0
         ColumnHeaders   =   0   'False
         BeveColorScheme =   1
         BevelColorFrame =   -2147483644
         CheckBox3D      =   0   'False
         BackColorOdd    =   16761024
         RowHeight       =   423
         Columns(0).Width=   4868
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "description"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   5106
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbProduct 
         Height          =   255
         Left            =   5100
         TabIndex        =   28
         ToolTipText     =   "The product on this job"
         Top             =   660
         Width           =   2895
         DataFieldList   =   "name"
         _Version        =   196617
         BorderStyle     =   0
         ColumnHeaders   =   0   'False
         BeveColorScheme =   1
         CheckBox3D      =   0   'False
         ForeColorEven   =   -2147483640
         ForeColorOdd    =   -2147483640
         BackColorEven   =   -2147483643
         BackColorOdd    =   8438015
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "productID"
         Columns(0).Name =   "productID"
         Columns(0).DataField=   "productID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   5212
         Columns(1).Caption=   "Name"
         Columns(1).Name =   "name"
         Columns(1).DataField=   "name"
         Columns(1).FieldLen=   256
         _ExtentX        =   5106
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   8438015
         DataFieldToDisplay=   "name"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbProject 
         Height          =   255
         Left            =   5100
         TabIndex        =   27
         ToolTipText     =   "The name of this project. In the previous version of CETA, this was known as the 'Special Job Number'"
         Top             =   300
         Width           =   2895
         DataFieldList   =   "projectnumber"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BorderStyle     =   0
         BeveColorScheme =   1
         CheckBox3D      =   0   'False
         ForeColorEven   =   -2147483640
         ForeColorOdd    =   -2147483640
         BackColorEven   =   -2147483643
         BackColorOdd    =   12648447
         RowHeight       =   423
         Columns.Count   =   9
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "projectID"
         Columns(0).Name =   "projectID"
         Columns(0).DataField=   "projectID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   5212
         Columns(1).Caption=   "Project Number"
         Columns(1).Name =   "name"
         Columns(1).DataField=   "projectnumber"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "productname"
         Columns(2).Name =   "productname"
         Columns(2).DataField=   "productname"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "companyname"
         Columns(3).Name =   "companyname"
         Columns(3).DataField=   "companyname"
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Caption=   "contactname"
         Columns(4).Name =   "contactname"
         Columns(4).DataField=   "contactname"
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Caption=   "status"
         Columns(5).Name =   "status"
         Columns(5).DataField=   "fd_status"
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "productID"
         Columns(6).Name =   "productID"
         Columns(6).DataField=   "productID"
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "companyID"
         Columns(7).Name =   "companyID"
         Columns(7).DataField=   "companyID"
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "contactID"
         Columns(8).Name =   "contactID"
         Columns(8).DataField=   "contactID"
         Columns(8).FieldLen=   256
         _ExtentX        =   5106
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   12648447
         DataFieldToDisplay=   "projectnumber"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbOurContact 
         Height          =   255
         Left            =   1260
         TabIndex        =   26
         ToolTipText     =   "Our contact on this job"
         Top             =   2460
         Width           =   2655
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         BorderStyle     =   0
         ColumnHeaders   =   0   'False
         BeveColorScheme =   1
         BevelColorFrame =   -2147483644
         CheckBox3D      =   0   'False
         BackColorOdd    =   16761024
         RowHeight       =   423
         Columns(0).Width=   4868
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "description"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   4683
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   16761024
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbContact 
         Height          =   255
         Left            =   1260
         TabIndex        =   21
         ToolTipText     =   "The contact for this job"
         Top             =   660
         Width           =   2655
         DataFieldList   =   "name"
         _Version        =   196617
         BorderStyle     =   0
         ForeColorEven   =   -2147483640
         ForeColorOdd    =   -2147483640
         BackColorEven   =   -2147483643
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   5424
         Columns(0).Caption=   "name"
         Columns(0).Name =   "name"
         Columns(0).DataField=   "name"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "telephone"
         Columns(1).Name =   "telephone"
         Columns(1).DataField=   "telephone"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "contactID"
         Columns(2).Name =   "contactID"
         Columns(2).DataField=   "contactID"
         Columns(2).FieldLen=   256
         _ExtentX        =   4683
         _ExtentY        =   450
         _StockProps     =   93
         ForeColor       =   -2147483640
         BackColor       =   16761087
         DataFieldToDisplay=   "name"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbAllocation 
         Height          =   255
         Left            =   5100
         TabIndex        =   85
         ToolTipText     =   "Job Type"
         Top             =   2100
         Width           =   2895
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BorderStyle     =   0
         ColumnHeaders   =   0   'False
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         Columns(0).Width=   4868
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "description"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   5106
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   12640511
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblCaption 
         Caption         =   "Resource"
         Height          =   255
         Index           =   14
         Left            =   4020
         TabIndex        =   97
         Top             =   2460
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Costed Initials"
         Height          =   255
         Index           =   27
         Left            =   2340
         TabIndex        =   94
         Top             =   2820
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "MCS #"
         Height          =   255
         Index           =   26
         Left            =   120
         TabIndex        =   92
         Top             =   2820
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Team Member"
         Height          =   255
         Index           =   25
         Left            =   120
         TabIndex        =   90
         Top             =   1740
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Project Type"
         Height          =   255
         Index           =   49
         Left            =   4020
         TabIndex        =   86
         Top             =   2100
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Product"
         Height          =   255
         Index           =   46
         Left            =   4020
         TabIndex        =   72
         Top             =   660
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Project #"
         Height          =   255
         Index           =   33
         Left            =   4020
         TabIndex        =   71
         Top             =   300
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Our Contact"
         Height          =   255
         Index           =   23
         Left            =   120
         TabIndex        =   65
         Top             =   2460
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Booked Initials"
         Height          =   255
         Index           =   22
         Left            =   120
         TabIndex        =   64
         Top             =   2100
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Job Type"
         Height          =   255
         Index           =   34
         Left            =   4020
         TabIndex        =   57
         Top             =   1740
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Credit #"
         Height          =   255
         Index           =   16
         Left            =   2340
         TabIndex        =   54
         Top             =   1380
         Width           =   615
      End
      Begin VB.Label lblCaption 
         Caption         =   "Invoice #"
         Height          =   255
         Index           =   15
         Left            =   120
         TabIndex        =   53
         Top             =   1380
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Subtitle"
         Height          =   255
         Index           =   12
         Left            =   4020
         TabIndex        =   52
         Top             =   1380
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Title"
         Height          =   255
         Index           =   11
         Left            =   4020
         TabIndex        =   51
         Top             =   1020
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Contact"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   48
         Top             =   660
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Company"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   47
         Top             =   300
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Order Ref"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   43
         Top             =   1020
         Width           =   1035
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "ID Range"
      Height          =   1095
      Left            =   60
      TabIndex        =   41
      Top             =   60
      Width           =   4575
      Begin VB.TextBox txtInvoiceNumberTo 
         Height          =   255
         Left            =   3360
         TabIndex        =   77
         ToolTipText     =   "The unique identifier for this job (End of range)"
         Top             =   720
         Width           =   1035
      End
      Begin VB.TextBox txtInvoiceNumberFrom 
         Height          =   255
         Left            =   3360
         TabIndex        =   76
         ToolTipText     =   "The unique identifier for this job (Start of range)"
         Top             =   420
         Width           =   1035
      End
      Begin VB.TextBox txtJobIDFrom 
         BackColor       =   &H00FFC0C0&
         Height          =   255
         Left            =   780
         TabIndex        =   0
         ToolTipText     =   "The unique identifier for this job (Start of range)"
         Top             =   420
         Width           =   1035
      End
      Begin VB.TextBox txtJobIDTo 
         BackColor       =   &H00FFC0C0&
         Height          =   255
         Left            =   780
         TabIndex        =   1
         ToolTipText     =   "The unique identifier for this job (End of range)"
         Top             =   720
         Width           =   1035
      End
      Begin VB.TextBox txtProjectIDFrom 
         BackColor       =   &H00C0FFFF&
         Height          =   255
         Left            =   2040
         TabIndex        =   2
         ToolTipText     =   "The unique identifier for this job (Start of range)"
         Top             =   420
         Width           =   1035
      End
      Begin VB.TextBox txtProjectIDTo 
         BackColor       =   &H00C0FFFF&
         Height          =   255
         Left            =   2040
         TabIndex        =   3
         ToolTipText     =   "The unique identifier for this job (End of range)"
         Top             =   720
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "Invoice #"
         Height          =   255
         Index           =   13
         Left            =   3360
         TabIndex        =   78
         Top             =   180
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   67
         Top             =   660
         Width           =   255
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "Job ID"
         Height          =   255
         Index           =   9
         Left            =   780
         TabIndex        =   66
         Top             =   180
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "From"
         Height          =   255
         Index           =   18
         Left            =   120
         TabIndex        =   56
         Top             =   300
         Width           =   375
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "Project #"
         Height          =   255
         Index           =   17
         Left            =   2040
         TabIndex        =   55
         Top             =   180
         Width           =   1035
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Deadline..."
      Height          =   1095
      Left            =   60
      TabIndex        =   38
      Top             =   1200
      Width           =   2235
      Begin MSComCtl2.DTPicker datDeadlineDateFrom 
         Height          =   315
         Left            =   600
         TabIndex        =   4
         Tag             =   "NOCHECK"
         ToolTipText     =   "The deadline date from"
         Top             =   240
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   123404289
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datDeadlineDateTo 
         Height          =   315
         Left            =   600
         TabIndex        =   5
         Tag             =   "NOCHECK"
         ToolTipText     =   "The deadline date to"
         Top             =   660
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   123404289
         CurrentDate     =   37870
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   40
         Top             =   600
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "From"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   39
         Top             =   240
         Width           =   735
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdJobListing 
      Bindings        =   "frmJobSearch.frx":08E3
      Height          =   2595
      Left            =   0
      TabIndex        =   75
      Top             =   3660
      Width           =   13095
      _Version        =   196617
      stylesets.count =   2
      stylesets(0).Name=   "invoicenumberpresent"
      stylesets(0).ForeColor=   0
      stylesets(0).BackColor=   15912954
      stylesets(0).Picture=   "frmJobSearch.frx":08FE
      stylesets(1).Name=   "vtstart"
      stylesets(1).ForeColor=   0
      stylesets(1).BackColor=   10420113
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmJobSearch.frx":091A
      AllowUpdate     =   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   3
      BackColorOdd    =   16446965
      RowHeight       =   370
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   23098
      _ExtentY        =   4577
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblSQL 
      Height          =   135
      Left            =   2220
      TabIndex        =   74
      Top             =   7800
      Visible         =   0   'False
      Width           =   1155
   End
   Begin VB.Label lblCompanyID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   7860
      TabIndex        =   73
      Tag             =   "CLEARFIELDS"
      Top             =   9660
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order By"
      Height          =   255
      Index           =   24
      Left            =   2160
      TabIndex        =   70
      Top             =   8880
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "frmSearchJob"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub chkCosting_Click()

If chkCosting.Value <> 0 Then
    txtCostingsTotal.Visible = True
Else
    txtCostingsTotal.Visible = False
End If

End Sub

Private Sub chkJobStatus_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
If Button = 2 Then

    Dim l_intLoop As Integer
    For l_intLoop = 0 To 12
        chkJobStatus(l_intLoop).Value = 0
        
    Next

    chkJobStatus(Index).Value = 1

End If
End Sub

Private Sub cmbCompany_Click()
lblCompanyID.Caption = cmbCompany.Columns("companyID").Text
End Sub

Private Sub cmbCompany_DropDown()

Dim l_strSQL As String

l_strSQL = "SELECT name, accountcode, telephone, companyID, fax, accountstatus FROM company WHERE (name LIKE '" & QuoteSanitise(cmbCompany.Text) & "%') AND (iscustomer = 1 OR isprospective = 1) ORDER BY name;"
Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing
End Sub

Private Sub cmbContact_DropDown()
If lblCompanyID.Caption = "" Then
    NoCompanySelectedMessage
    Exit Sub
End If

Dim l_strSQL As String

l_strSQL = "SELECT company.companyID, contact.contactID, contact.name, contact.telephone, employee.jobtitle FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((company.companyID)=" & lblCompanyID.Caption & ")) ORDER BY contact.name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbContact.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmdBulkCreditNotes_Click()

'Button that is going to open up a job, then credit note it.

If adoJobSearch.Recordset.RecordCount > 0 Then
    If MsgBox("Are you Sure", vbYesNo + vbDefaultButton2, "Credit All The Items") = vbNo Then Exit Sub
    adoJobSearch.Recordset.MoveFirst
    Do While Not adoJobSearch.Recordset.EOF
        ShowJob adoJobSearch.Recordset("Job ID"), g_intDefaultTab, True
        frmJob.tabReplacementTabs.Tab = 5
        DoEvents
        frmJob.MakeBulkCreditNote
        DoEvents
        frmJob.FinaliseBulkCreditNote
        DoEvents
        adoJobSearch.Recordset.MoveNext
        DoEvents
    Loop
End If

End Sub

Private Sub cmdClear_Click()
ClearFields Me
'Me.Caption = "Job Search"
End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdExport_Click()


Dim l_strFilePath As String
l_strFilePath = fGetSpecialFolder(CSIDL_DOCUMENTS)
grdJobListing.Export ssExportTypeText, ssExportAllRows + ssExportColumnHeaders, l_strFilePath & "jobsearch.txt"

MsgBox "Exported to a text file in 'My Documents' called 'jobsearch.txt', vbInformation"


End Sub

Private Sub cmdPrint_Click()


grdJobListing.PrintData ssPrintAllRows, True, False

'Dim l_searchstring As String
'l_searchstring = adoJobSearch.RecordSource

'If l_searchstring <> "" Then
'    PrintCrystalReportUsingSQL g_strLocationOfCrystalReportFiles & "jobsearch.rpt", l_searchstring, g_blnPreviewReport, cmbOrderBy.Text
'End If

End Sub

Private Sub cmdRun_Click()

If cmbMacroSearch.Text = "" Then Exit Sub

Dim l_strSQL As String
Dim l_strFields As String

l_strFields = "jobID, companyname, contactname, title1, title2, projectnumber, startdate, enddate"
l_strSQL = "SELECT " & l_strFields & " FROM job WHERE (jobID IS NOT NULL) "
l_strSQL = l_strSQL & "AND "
l_strSQL = l_strSQL & DecodeMacroSearch(cmbMacroSearch.Text)

'Me.Caption = "Job Search - [Searching... Please wait]"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set grdJobListing.DataSource = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

'Me.Caption = "Job Search - [Showing: " & grdJobListing.Rows & "]"

End Sub

Private Sub cmdSearch_Click()

Dim l_strSQL As String

Dim l_strFields As String, l_strWhereSQL As String

l_strFields = "job.startdate AS 'Start Date', job.projectnumber AS 'Project #', job.jobID AS 'Job ID', job.ourcontact AS 'Our Contact', '' AS 'Resources Booked', job.companyname AS 'Client', " & _
"job.contactname AS 'Contact', job.productname AS 'Product', job.title1 AS 'Title', job.title2 AS 'Subtitle', job.createduser as 'Created By', job.createddate as 'Created Date', job.fd_status AS 'Status', " & _
"job.costingsheetID AS 'MCS #', job.totalprints AS 'Print #', job.holdcostdate AS 'Hold Cost Date', job.costeddate AS 'Costed Date', job.lastprintdate AS 'Printed Date', " & _
"job.invoicenumber AS 'Invoice No.', job.invoiceddate AS 'Date Invoiced'"

If chkCosting.Value = 1 Then
    l_strFields = l_strFields & ", SUM(costing.total) AS 'Total'"
Else
    l_strFields = l_strFields & ", '' AS 'Total'"
End If
'l_strFields = "job.jobID, job.projectnumber, job.deadlinedate, job.createduser, job.startdate, job.enddate, 'See Job Detail' as requirements, job.productname, job.title1, job.companyname, job.fd_status, job.vtstartdate, job.vtstartuser"

'l_strFields = GetFieldNames("Job Listing")

l_strSQL = "SELECT " & l_strFields & " FROM job"

If txtResource.Text <> "" Then l_strSQL = l_strSQL & " INNER JOIN resourceschedule ON job.jobID = resourceschedule.jobID INNER JOIN resource ON resource.resourceID = resourceschedule.resourceID "

If chkCosting.Value = 1 Then l_strSQL = l_strSQL & " LEFT JOIN costing ON job.jobID = costing.jobID "

If txtTeamMember.Text <> "" Then l_strSQL = l_strSQL & " INNER JOIN team ON team.projectID = job.projectID INNER JOIN contact ON team.contactID = contact.contactID "

l_strSQL = l_strSQL & " WHERE"

l_strWhereSQL = " (job.jobID IS NOT NULL) "

Dim l_strReportSource As String

If cmbAllocation.Text <> "" Then
    l_strWhereSQL = l_strWhereSQL & " AND (job.joballocation LIKE '" & QuoteSanitise(cmbAllocation.Text) & "%') "
    l_strReportSource = l_strReportSource & "AND {job.allocation} LIKE '" & QuoteSanitise(cmbAllocation.Text) & "*') "
End If

If txtResource.Text <> "" Then
    l_strWhereSQL = l_strWhereSQL & " AND (resource.name LIKE '" & QuoteSanitise(txtResource.Text) & "%') "
    l_strReportSource = l_strReportSource & "AND {resource.name} LIKE '" & QuoteSanitise(txtResource.Text) & "*') "
End If

If cmbCompany.Text <> "" Then
    l_strWhereSQL = l_strWhereSQL & "AND (job.companyname Like '" & QuoteSanitise(cmbCompany.Text) & "%') "
    l_strReportSource = l_strReportSource & "AND {job.companyname} LIKE '" & QuoteSanitise(cmbCompany.Text) & "*') "
End If


If cmbContact.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.contactname Like '" & QuoteSanitise(cmbContact.Text) & "%') "
If txtTitle.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.title1 Like '" & QuoteSanitise(txtTitle.Text) & "%') "
If txtSubtitle.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.title2 Like '" & QuoteSanitise(txtSubtitle.Text) & "%') "
If cmbProject.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.projectnumber Like '" & QuoteSanitise(cmbProject.Text) & "%') "
If cmbProduct.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.productname Like '" & QuoteSanitise(cmbProduct.Text) & "%') "
If cmbJobType.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.jobtype Like '" & QuoteSanitise(cmbJobType.Text) & "%') "
If txtOrderNumber.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.orderreference Like '" & QuoteSanitise(txtOrderNumber.Text) & "%') "
If txtBookedBy.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.createduser = '" & QuoteSanitise(txtBookedBy.Text) & "') "
If txtCreditNoteNumber.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.creditnotenumber Like '" & txtCreditNoteNumber.Text & "%') "
If txtInvoiceNumber.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.invoicenumber Like '" & txtInvoiceNumber.Text & "%') "
If txtMCS.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.costingsheetID Like '" & txtMCS.Text & "%') "
If txtCostedInitials.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.holdcostuser Like '" & txtCostedInitials.Text & "%') "
If cmbOurContact.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.ourcontact Like '" & QuoteSanitise(cmbOurContact.Text) & "%') "

If txtTeamMember.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (contact.name Like '" & QuoteSanitise(txtTeamMember.Text) & "%') "

If txtJobIDFrom.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.jobID >= '" & txtJobIDFrom.Text & "') "
If txtJobIDTo.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.jobID <= '" & txtJobIDTo.Text & "') "

If txtProjectIDFrom.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.projectnumber >= '" & txtProjectIDFrom.Text & "') "
If txtProjectIDTo.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.projectnumber <= '" & txtProjectIDTo.Text & "') "

If txtInvoiceNumberFrom.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.invoicenumber >= '" & txtInvoiceNumberFrom.Text & "') "
If txtInvoiceNumberTo.Text <> "" Then l_strWhereSQL = l_strWhereSQL & "AND (job.invoicenumber <= '" & txtInvoiceNumberTo.Text & "') "

If Not IsNull(datJobBookedFrom.Value) Then
    Dim l_datJobBookedFrom As Date
    l_datJobBookedFrom = Format(datJobBookedFrom.Value, vbShortDateFormat)
    l_strWhereSQL = l_strWhereSQL & "AND (job.createddate >= '" & FormatSQLDate(l_datJobBookedFrom & " 00:00") & "') "
End If

If Not IsNull(datJobBookedTo.Value) Then
    Dim l_datJobBookedTo As Date
    l_datJobBookedTo = Format(datJobBookedTo.Value, vbShortDateFormat)
    l_strWhereSQL = l_strWhereSQL & "AND (job.createddate <= '" & FormatSQLDate(l_datJobBookedTo & " 23:59") & "') "
End If

If Not IsNull(datStartDateFrom.Value) Then
    Dim l_datStartDateFrom As Date
    l_datStartDateFrom = Format(datStartDateFrom.Value, vbShortDateFormat)
    l_strWhereSQL = l_strWhereSQL & "AND (job.startdate >= '" & FormatSQLDate(l_datStartDateFrom & " 00:00") & "') "
End If

If Not IsNull(datStartDateTo.Value) Then
    Dim l_datStartDateTo As Date
    l_datStartDateTo = Format(datStartDateTo.Value, vbShortDateFormat)
    l_strWhereSQL = l_strWhereSQL & "AND (job.startdate <= '" & FormatSQLDate(l_datStartDateTo & " 23:59") & "') "
End If

If Not IsNull(datEndDateFrom.Value) Then
    Dim l_datEndDateFrom As Date
    l_datEndDateFrom = Format(datEndDateFrom.Value, vbShortDateFormat)
    l_strWhereSQL = l_strWhereSQL & "AND (job.enddate >= '" & FormatSQLDate(l_datEndDateFrom & " 00:00") & "') "
End If

If Not IsNull(datEndDateTo.Value) Then
    Dim l_datEndDateTo As Date
    l_datEndDateTo = Format(datEndDateTo.Value, vbShortDateFormat)
    l_strWhereSQL = l_strWhereSQL & "AND (job.enddate <= '" & FormatSQLDate(l_datEndDateTo & " 23:59") & "') "
End If

Dim l_intLoop As Integer
Dim l_strStatusCheckSQL As String

For l_intLoop = 0 To 15
    'loop through the tick boxes and show only jobs with selected status
    If chkJobStatus(l_intLoop).Value <> 0 Then
        l_strStatusCheckSQL = l_strStatusCheckSQL & " OR (job.fd_status = '" & chkJobStatus(l_intLoop).Caption & "') "
        If chkJobStatus(l_intLoop).Caption = "Confirmed" Then
            l_strStatusCheckSQL = l_strStatusCheckSQL & " OR (job.fd_status = 'Submitted') "
            l_strStatusCheckSQL = l_strStatusCheckSQL & " OR (job.fd_status = 'Scheduled') "
        End If
    End If
Next

If Trim(l_strStatusCheckSQL) <> "" Then
    l_strWhereSQL = l_strWhereSQL & " AND (" & Right(l_strStatusCheckSQL, Len(l_strStatusCheckSQL) - 4) & ")"
End If

l_strSQL = l_strSQL & l_strWhereSQL
If chkCosting.Value = 1 Then l_strSQL = l_strSQL & " GROUP BY job.jobID, job.startdate, job.createduser, job.createddate, job.projectnumber, job.ourcontact, job.companyname, job.contactname, job.productname, job.title1, job.title2, job.fd_status, job.costingsheetID, job.totalprints, job.holdcostdate, job.costeddate, job.lastprintdate, job.invoicenumber, job.invoiceddate  "
l_strSQL = l_strSQL & " ORDER BY '" & cmbOrderBy.Text & "' " & cmbDirection.Text

Me.MousePointer = vbHourglass

adoJobSearch.ConnectionString = g_strConnection
adoJobSearch.RecordSource = l_strSQL
adoJobSearch.Refresh

Dim rs As ADODB.Recordset

If chkCosting.Value = 1 Then
    grdJobListing.Columns("Total").Visible = True
    grdJobListing.Columns("Total").NumberFormat = "�0.00"
    Set rs = ExecuteSQL("SELECT SUM(costing.total) FROM job  LEFT JOIN costing ON job.jobID = costing.jobID WHERE " & l_strWhereSQL, g_strExecuteError)
    CheckForSQLError
    txtCostingsTotal.Text = Format(rs(0), "�#.00")
    rs.Close
    Set rs = Nothing
Else
    grdJobListing.Columns("Total").Visible = False
End If

Me.MousePointer = vbDefault

Me.Caption = "Search results: " & grdJobListing.Rows
adoJobSearch.Caption = grdJobListing.Rows & " jobs"

End Sub

Private Sub cmbProduct_Click()

'check if projects are live for this product
End Sub
Private Sub cmbProject_Click()
If cmbProject.Rows = 0 Then Exit Sub

Dim l_intMsg As Integer
l_intMsg = MsgBox("Do you want to populate the fields associated with this project?", vbYesNo + vbQuestion)
If l_intMsg = vbYes Then
    cmbCompany.Text = cmbProject.Columns("companyname").Text
    cmbContact.Text = cmbProject.Columns("contactname").Text
    cmbProduct.Text = cmbProject.Columns("productname").Text
    
    txtTitle.Text = GetData("project", "subtitle", "projectID", Val(cmbProject.Columns("projectID").Text))
End If

End Sub

Private Sub cmbProject_DropDown()

If cmbProject.Tag = "STOP" Then Exit Sub

Dim l_strSQL As String
l_strSQL = "SELECT project.projectnumber, project.projectID, project.companyname, project.contactname, project.fd_status, product.name as productname, product.productID, project.companyID, project.contactID FROM project LEFT JOIN product ON project.productID = product.productID WHERE projectnumber LIKE '" & QuoteSanitise(cmbProject.Text) & "%' AND project.fd_status <> 'COMPLETED' AND project.fd_status <> 'CANCEL' AND project.fd_status <> 'INVOICED' ORDER BY projectnumber;"
Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbProject.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub


Private Sub cmbProduct_DropDown()
Dim l_strSQL As String
l_strSQL = "SELECT name, productID FROM product WHERE name LIKE '" & QuoteSanitise(cmbProduct.Text) & "%' ORDER BY name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbProduct.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing
End Sub

Private Sub cmdSessionPrint_Click()
Dim l_searchstring As String
l_searchstring = adoJobSearch.RecordSource

If l_searchstring <> "" Then
    PrintCrystalReportUsingSQL g_strLocationOfCrystalReportFiles & "sessionlist.rpt", l_searchstring, True, cmbOrderBy.Text
End If

End Sub

Private Sub Form_Activate()

Me.WindowState = vbMaximized

If CheckAccess("/Superuser", True) Then
    cmdBulkCreditNotes.Visible = True
Else
    cmdBulkCreditNotes.Visible = False
End If

End Sub

Private Sub Form_Load()


If CheckAccess("/costjob", True) Then
    chkCosting.Visible = True
Else
    chkCosting.Visible = False
End If

grdJobListing.StyleSets.Add "2nd Pencil"
grdJobListing.StyleSets("2nd Pencil").BackColor = g_lngColour2ndPencil

grdJobListing.StyleSets.Add "Pencil"
grdJobListing.StyleSets("Pencil").BackColor = g_lngColourPencil

grdJobListing.StyleSets.Add "Submitted"
grdJobListing.StyleSets("Submitted").BackColor = g_lngColour2ndPencil

grdJobListing.StyleSets.Add "Confirmed"
grdJobListing.StyleSets("Confirmed").BackColor = g_lngColourConfirmed

grdJobListing.StyleSets.Add "Scheduled"
grdJobListing.StyleSets("Scheduled").BackColor = g_lngColourScheduled

grdJobListing.StyleSets.Add "Masters Here"
grdJobListing.StyleSets("Masters Here").BackColor = g_lngColourMastersHere

grdJobListing.StyleSets.Add "In Progress"
grdJobListing.StyleSets("In Progress").BackColor = g_lngColourInProgress

grdJobListing.StyleSets.Add "VT Done"
grdJobListing.StyleSets("VT Done").BackColor = g_lngColourVTDone

grdJobListing.StyleSets.Add "Completed"
grdJobListing.StyleSets("Completed").BackColor = g_lngColourCompleted

grdJobListing.StyleSets.Add "Hold Cost"
grdJobListing.StyleSets("Hold Cost").BackColor = g_lngColourHoldCost

grdJobListing.StyleSets.Add "Pending"
grdJobListing.StyleSets("Pending").BackColor = g_lngColourOnHold

grdJobListing.StyleSets.Add "Costed"
grdJobListing.StyleSets("Costed").BackColor = g_lngColourCosted

grdJobListing.StyleSets.Add "Sent To Accounts"
grdJobListing.StyleSets("Sent To Accounts").BackColor = g_lngColourSentToAccounts

grdJobListing.StyleSets.Add "Quick"
grdJobListing.StyleSets("Quick").BackColor = g_lngColourQuick

grdJobListing.StyleSets.Add "Cancelled"
grdJobListing.StyleSets("Cancelled").BackColor = g_lngColourCancelled

grdJobListing.StyleSets.Add "No Charge"
grdJobListing.StyleSets("No Charge").BackColor = vbRed

'hire colours
grdJobListing.StyleSets.Add "Prepared"
grdJobListing.StyleSets("Prepared").BackColor = g_lngColourPrepared

grdJobListing.StyleSets.Add "Despatched"
grdJobListing.StyleSets("Despatched").BackColor = g_lngColourDespatched

grdJobListing.StyleSets.Add "Returned"
grdJobListing.StyleSets("Returned").BackColor = g_lngColourReturned

'populate the company drop down (data bound)
PopulateCombo "jobsearch", cmbMacroSearch
PopulateCombo "fields-job", cmbOrderBy
PopulateCombo "jobtype", cmbJobType
PopulateCombo "ourcontact", cmbOurContact
PopulateCombo "allocation", cmbAllocation
cmbAllocation.AddItem ""

cmbOrderBy.Text = "Job ID"
cmbDirection.ListIndex = 1

'core functionality disable
 
 
 If g_intDisableProjects = 1 Then
     lblCaption(33).Visible = False
     cmbProject.Visible = False
     cmbAllocation.Visible = False
     lblCaption(49).Visible = False
     lblCaption(17).Visible = False
     txtProjectIDFrom.Visible = False
     txtProjectIDTo.Visible = False
End If
 
 If g_intDisableProducts = 1 Then
     cmbProduct.Visible = False
     lblCaption(46).Visible = False
 End If

If g_intDisableResourceList = 1 Then
    chkShowResources.Visible = False
    txtResource.Visible = False
    lblCaption(14).Visible = False
End If

If g_intDisableSchedule = 1 Then
    Frame5.Visible = False
    Frame6.Visible = False
    fraJobSearch1.Left = Frame5.Left
End If
    
'hide hire related tick boxes
If g_optHideHireDespatchTab = 1 Then
    chkJobStatus(8).Visible = False
    chkJobStatus(9).Visible = False
    chkJobStatus(12).Visible = False
End If

If g_intDisableOurContact = 1 Then
    cmbOurContact.Visible = False
    lblCaption(23).Visible = False
    
End If



'LoadColumnLayout grdJobListing, "Job Listing"

CenterForm Me

End Sub

Private Sub Form_Resize()

On Error Resume Next

picFooter.Top = Me.ScaleHeight - picFooter.Height - 120
picFooter.Left = Me.ScaleWidth - picFooter.Width - 120
grdJobListing.Height = Me.ScaleHeight - picFooter.Height - 120 - 120 - grdJobListing.Top
grdJobListing.Width = Me.ScaleWidth - 120
cmdBulkCreditNotes.Left = fraJobSearch1.Left + fraJobSearch1.Width + 60
cmdBulkCreditNotes.Top = fraJobSearch1.Top

End Sub

Private Sub Form_Unload(Cancel As Integer)
'SaveColumnLayout grdJobListing, "Job Listing"
End Sub

Private Sub grdJobListing_DblClick()
If grdJobListing.Row = grdJobListing.Rows Then Exit Sub
If grdJobListing.Columns("job ID").Text = "" Then Exit Sub
ShowJob grdJobListing.Columns("job ID").Text, g_intDefaultTab, True
ShowJob grdJobListing.Columns("job ID").Text, g_intDefaultTab, True
'ShowJob grdJobListing.Columns("job ID").Text, g_intDefaultTab, True
End Sub

Private Sub grdJobListing_HeadClick(ByVal ColIndex As Integer)

If cmbOrderBy.Text <> grdJobListing.Columns(ColIndex).DataField Then
    cmbOrderBy.Text = grdJobListing.Columns(ColIndex).DataField
Else
    cmbDirection.ListIndex = 1 - cmbDirection.ListIndex
End If

cmdSearch_Click

End Sub

Private Sub grdJobListing_InitColumnProps()

On Error GoTo grdJobListing_Init_Error

If g_optRequireProductBeforeSaving = 1 And g_intDisableProducts = 0 Then
    grdJobListing.Columns("Product").Visible = True
Else
    grdJobListing.Columns("Product").Visible = False
End If

If g_intDisableOurContact = 1 Then
    grdJobListing.Columns("Our Contact").Visible = False
End If

If g_intDisableProjects = 1 Then
    grdJobListing.Columns("Project #").Visible = False
End If

If g_intDisableSchedule = 1 Then
    grdJobListing.Columns("Start Date").Visible = False
End If

If g_intDisableCostingSheets = 1 Then
    grdJobListing.Columns("MCS #").Visible = False
End If

If chkCosting.Value = 1 Then
    grdJobListing.Columns("Total").Alignment = ssCaptionAlignmentRight
    grdJobListing.Columns("Total").Width = 1200
End If

grdJobListing.Columns("Status").ForeColor = vbBlack
grdJobListing.Columns("Created By").Width = 1200
grdJobListing.Columns("Created Date").Width = 1200

Exit Sub

grdJobListing_Init_Error:

'LoadColumnLayout grdJobListing, "Job Listing"

Resume Next

End Sub

Private Sub grdJobListing_RowLoaded(ByVal Bookmark As Variant)
On Error Resume Next
If grdJobListing.Rows < 1 Then Exit Sub

If grdJobListing.Columns("job ID").Text <> "" Then
    Dim l_strTextToAdd As String
    l_strTextToAdd = GetJobDescription(grdJobListing.Columns("job ID").Text)
    grdJobListing.Columns("requirements").Text = l_strTextToAdd
    
End If

grdJobListing.Columns("Status").CellStyleSet grdJobListing.Columns("Status").Text

Dim l_strResourcesBooked As String
If chkShowResources.Value = 1 Then
    l_strResourcesBooked = GetJobDescription(Val(grdJobListing.Columns("Job ID").Text))
    grdJobListing.Columns("Resources Booked").Text = l_strResourcesBooked
End If

If grdJobListing.Columns("Invoice No.").Text <> "" And grdJobListing.Columns("Invoice No.").Text <> "0" Then
    grdJobListing.Columns("Invoice No.").CellStyleSet "invoicenumberpresent"
End If

End Sub

