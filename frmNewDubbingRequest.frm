VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Begin VB.Form frmNewDubbingRequest 
   Caption         =   "New dubbing request"
   ClientHeight    =   6990
   ClientLeft      =   4245
   ClientTop       =   4410
   ClientWidth     =   12420
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   6990
   ScaleWidth      =   12420
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   10860
      TabIndex        =   19
      Top             =   6480
      Width           =   1455
   End
   Begin VB.CommandButton cmdAddToJob 
      Caption         =   "Add to job"
      Height          =   375
      Left            =   9240
      TabIndex        =   18
      Top             =   6480
      Width           =   1455
   End
   Begin VB.CommandButton cmdClientSuppliedSource 
      Caption         =   "Client supplied source -->"
      Height          =   435
      Left            =   1080
      TabIndex        =   17
      Top             =   1680
      Width           =   2175
   End
   Begin VB.ComboBox cmbTimecodeformat 
      Height          =   315
      Left            =   1680
      TabIndex        =   15
      Top             =   5400
      Width           =   1935
   End
   Begin VB.ComboBox cmbAspect 
      Height          =   315
      Left            =   1680
      TabIndex        =   13
      Top             =   4920
      Width           =   1935
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdSource 
      Height          =   2775
      Left            =   3720
      TabIndex        =   11
      Top             =   120
      Width           =   8595
      _Version        =   196617
      DataMode        =   2
      Cols            =   6
      Col.Count       =   6
      BackColorOdd    =   12648384
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   15161
      _ExtentY        =   4895
      _StockProps     =   79
      Caption         =   "Source Information"
   End
   Begin VB.TextBox txtQuantity 
      Height          =   345
      Left            =   1680
      TabIndex        =   10
      Top             =   3480
      Width           =   855
   End
   Begin VB.ComboBox cmbStandard 
      Height          =   315
      Left            =   1680
      TabIndex        =   8
      Top             =   4440
      Width           =   1935
   End
   Begin VB.CommandButton cmdSearchLibrary 
      Caption         =   "Find masters -->"
      Height          =   435
      Left            =   1080
      TabIndex        =   6
      Top             =   1080
      Width           =   2175
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "Add"
      Height          =   375
      Left            =   2400
      TabIndex        =   4
      Top             =   5940
      Width           =   1215
   End
   Begin VB.ComboBox cmbFormat 
      Height          =   315
      Left            =   1680
      TabIndex        =   1
      Top             =   3960
      Width           =   1935
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdOutput 
      Height          =   3315
      Left            =   3720
      TabIndex        =   12
      Top             =   3000
      Width           =   8595
      _Version        =   196617
      DataMode        =   2
      Cols            =   4
      Col.Count       =   4
      BackColorOdd    =   12648384
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   15161
      _ExtentY        =   5847
      _StockProps     =   79
      Caption         =   "Destination Information"
   End
   Begin VB.Label lblCaption 
      Caption         =   "Time Code Format"
      Height          =   435
      Index           =   0
      Left            =   420
      TabIndex        =   16
      Top             =   5400
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Aspect Ratio"
      Height          =   255
      Index           =   7
      Left            =   420
      TabIndex        =   14
      Top             =   4920
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Standard"
      Height          =   255
      Index           =   6
      Left            =   420
      TabIndex        =   9
      Top             =   4440
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Search..."
      Height          =   255
      Index           =   5
      Left            =   300
      TabIndex        =   7
      Top             =   600
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "1. Locate the source material"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   3435
   End
   Begin VB.Label lblCaption 
      Caption         =   "Quantity"
      Height          =   255
      Index           =   3
      Left            =   420
      TabIndex        =   3
      Top             =   3480
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Format"
      Height          =   255
      Index           =   2
      Left            =   420
      TabIndex        =   2
      Top             =   3960
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "2. Add output options"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   0
      Top             =   3000
      Width           =   3435
   End
End
Attribute VB_Name = "frmNewDubbingRequest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdAdd_Click()


grdOutput.AddItem txtQuantity.Text & vbTab & cmbFormat.Text & vbTab & cmbAspect.Text & vbTab & cmbStandard.Text

End Sub

Private Sub cmdClientSuppliedSource_Click()

grdSource.AddItem "Source supplied by client" & vbTab & "" & vbTab & "" & vbTab & "" & vbTab & "" & vbTab & ""

End Sub


Private Sub cmdSearchLibrary_Click()
frmSearchLibrary.Tag = "tempdub"
DoEvents
frmSearchLibrary.Show

End Sub

Private Sub Form_Load()

PopulateCombo "videostandard", cmbStandard
PopulateCombo "format", cmbFormat
PopulateCombo "aspectratio", cmbAspect
PopulateCombo "timecode", cmbTimecodeformat

grdSource.Columns(0).Caption = "Description"
grdSource.Columns(1).Caption = "Barcode"
grdSource.Columns(2).Caption = "Clock Number"
grdSource.Columns(3).Caption = "Start T/C"
grdSource.Columns(4).Caption = "Aspect Ratio"
grdSource.Columns(5).Caption = "Format"

grdOutput.Columns(0).Caption = "Quantity"
grdOutput.Columns(1).Caption = "Format"
grdOutput.Columns(2).Caption = "Aspect"
grdOutput.Columns(3).Caption = "Standard"


End Sub


Private Sub lstSourceEvents_DblClick()
lstSourceEvents.RemoveItem lstSourceEvents.ListIndex

End Sub


