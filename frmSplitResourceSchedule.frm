VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmSplitResourceSchedule 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Split Resource Schedule"
   ClientHeight    =   4455
   ClientLeft      =   5070
   ClientTop       =   4650
   ClientWidth     =   4590
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4455
   ScaleWidth      =   4590
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox cmbEndTime 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   3120
      TabIndex        =   15
      Text            =   "09:30"
      ToolTipText     =   "Time the job is due to end"
      Top             =   3240
      Width           =   1035
   End
   Begin VB.ComboBox cmbStartTime 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   3120
      TabIndex        =   14
      Text            =   "09:30"
      ToolTipText     =   "Time the job is due to commence"
      Top             =   2700
      Width           =   1035
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   315
      Left            =   3360
      TabIndex        =   13
      Top             =   4020
      Width           =   1155
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "Apply"
      Height          =   315
      Left            =   2100
      TabIndex        =   12
      Top             =   4020
      Width           =   1155
   End
   Begin VB.TextBox txtInfo 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   0  'None
      Height          =   255
      Index           =   5
      Left            =   1440
      Locked          =   -1  'True
      TabIndex        =   11
      Top             =   540
      Width           =   3015
   End
   Begin VB.TextBox txtInfo 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      Height          =   255
      Index           =   4
      Left            =   1440
      Locked          =   -1  'True
      TabIndex        =   10
      Top             =   180
      Width           =   3015
   End
   Begin VB.TextBox txtInfo 
      BorderStyle     =   0  'None
      Height          =   255
      Index           =   3
      Left            =   1440
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   1980
      Width           =   3015
   End
   Begin VB.TextBox txtInfo 
      BorderStyle     =   0  'None
      Height          =   255
      Index           =   2
      Left            =   1440
      Locked          =   -1  'True
      TabIndex        =   6
      Top             =   1620
      Width           =   3015
   End
   Begin VB.TextBox txtInfo 
      BorderStyle     =   0  'None
      Height          =   255
      Index           =   1
      Left            =   1440
      Locked          =   -1  'True
      TabIndex        =   5
      Top             =   1260
      Width           =   3015
   End
   Begin VB.TextBox txtInfo 
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'None
      Height          =   255
      Index           =   0
      Left            =   1440
      Locked          =   -1  'True
      TabIndex        =   4
      Top             =   900
      Width           =   3015
   End
   Begin MSComCtl2.DTPicker datStartDate 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   1440
      TabIndex        =   16
      Tag             =   "NOCHECK"
      ToolTipText     =   "Date the job is due to commence"
      Top             =   2700
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   61865985
      CurrentDate     =   37870
   End
   Begin MSComCtl2.DTPicker datEndDate 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   1440
      TabIndex        =   17
      Tag             =   "NOCHECK"
      ToolTipText     =   "Date the job is due to end"
      Top             =   3240
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   61865985
      CurrentDate     =   37870
   End
   Begin VB.Label lblResourceScheduleID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   360
      TabIndex        =   24
      Top             =   4020
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Break End"
      Height          =   255
      Index           =   7
      Left            =   180
      TabIndex        =   23
      Top             =   3300
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Break Start"
      Height          =   255
      Index           =   6
      Left            =   180
      TabIndex        =   22
      Top             =   2700
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Time"
      Height          =   255
      Index           =   32
      Left            =   3120
      TabIndex        =   21
      Top             =   3060
      Width           =   495
   End
   Begin VB.Label lblCaption 
      Caption         =   "End"
      Height          =   255
      Index           =   31
      Left            =   1440
      TabIndex        =   20
      Top             =   3060
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Time"
      Height          =   255
      Index           =   30
      Left            =   3120
      TabIndex        =   19
      Top             =   2520
      Width           =   495
   End
   Begin VB.Label lblCaption 
      Caption         =   "Start"
      Height          =   255
      Index           =   29
      Left            =   1440
      TabIndex        =   18
      Top             =   2520
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Project #"
      Height          =   255
      Index           =   5
      Left            =   180
      TabIndex        =   9
      Top             =   540
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job ID"
      Height          =   255
      Index           =   4
      Left            =   180
      TabIndex        =   8
      Top             =   180
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Resource Name"
      Height          =   255
      Index           =   3
      Left            =   180
      TabIndex        =   3
      Top             =   1980
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Current End"
      Height          =   255
      Index           =   2
      Left            =   180
      TabIndex        =   2
      Top             =   1620
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Current Start"
      Height          =   255
      Index           =   1
      Left            =   180
      TabIndex        =   1
      Top             =   1260
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job Client"
      Height          =   255
      Index           =   0
      Left            =   180
      TabIndex        =   0
      Top             =   900
      Width           =   1155
   End
End
Attribute VB_Name = "frmSplitResourceSchedule"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
