VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmPurchaseSearch 
   Caption         =   "Purchase Order Search"
   ClientHeight    =   7950
   ClientLeft      =   3525
   ClientTop       =   7050
   ClientWidth     =   15045
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPurchaseSearch.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7950
   ScaleWidth      =   15045
   WindowState     =   2  'Maximized
   Begin VB.Frame fraOrderDetail 
      Caption         =   "Order Detail Rows"
      Height          =   1995
      Left            =   11580
      TabIndex        =   75
      Top             =   60
      Width           =   3135
      Begin VB.TextBox txtFullDescription 
         BackColor       =   &H00C0E0FF&
         DataField       =   "purchasedetail.description"
         Height          =   315
         Left            =   1200
         TabIndex        =   78
         Tag             =   "%"
         ToolTipText     =   "The title for the job"
         Top             =   300
         Width           =   1815
      End
      Begin VB.TextBox txtSupplierCode 
         BackColor       =   &H00C0E0FF&
         DataField       =   "purchasedetail.suppliercode"
         Height          =   315
         Left            =   1200
         TabIndex        =   77
         Tag             =   "%"
         ToolTipText     =   "The part reference - or suppliers code"
         Top             =   720
         Width           =   1815
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbOurReference 
         DataField       =   "purchasedetail.ourreference"
         Height          =   315
         Left            =   1200
         TabIndex        =   76
         Top             =   1560
         Width           =   1815
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         Cols            =   2
         ColumnHeaders   =   0   'False
         BackColorOdd    =   12640511
         RowHeight       =   423
         Columns(0).Width=   3200
         _ExtentX        =   3201
         _ExtentY        =   556
         _StockProps     =   93
         ForeColor       =   -2147483640
         BackColor       =   12640511
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo ddnCETACodes 
         Bindings        =   "frmPurchaseSearch.frx":08CA
         DataField       =   "purchasedetail.cetacode"
         Height          =   315
         Left            =   1200
         TabIndex        =   79
         ToolTipText     =   "The Budget code or CETA Code"
         Top             =   1140
         Width           =   1815
         DataFieldList   =   "code"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BorderStyle     =   0
         ForeColorEven   =   -2147483640
         ForeColorOdd    =   -2147483640
         BackColorEven   =   -2147483643
         BackColorOdd    =   12640511
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2831
         Columns(0).Caption=   "Code"
         Columns(0).Name =   "name"
         Columns(0).DataField=   "code"
         Columns(0).FieldLen=   256
         Columns(1).Width=   6535
         Columns(1).Caption=   "Description"
         Columns(1).Name =   "telephone"
         Columns(1).DataField=   "description"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "contactID"
         Columns(2).Name =   "contactID"
         Columns(2).DataField=   "contactID"
         Columns(2).FieldLen=   256
         _ExtentX        =   3201
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   12640511
         DataFieldToDisplay=   "code"
      End
      Begin VB.Label lblCaption 
         Caption         =   "Item"
         Height          =   255
         Index           =   17
         Left            =   120
         TabIndex        =   83
         Top             =   360
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Part Ref."
         Height          =   315
         Index           =   18
         Left            =   120
         TabIndex        =   82
         Top             =   780
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Budget Code"
         Height          =   315
         Index           =   25
         Left            =   120
         TabIndex        =   81
         Top             =   1200
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Our Ref"
         Height          =   315
         Index           =   29
         Left            =   120
         TabIndex        =   80
         Top             =   1620
         Width           =   1155
      End
   End
   Begin VB.Frame fraStatus 
      Caption         =   "Order Status"
      Height          =   2115
      Left            =   4860
      TabIndex        =   48
      Top             =   60
      Width           =   2115
      Begin VB.CheckBox chkJobStatus 
         Caption         =   "Sent To Accounts"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   74
         ToolTipText     =   "Orders that have been sent to accounts"
         Top             =   1440
         Width           =   1635
      End
      Begin VB.CheckBox chkJobStatus 
         Caption         =   "Completed"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   15
         ToolTipText     =   "Completed purchase orders"
         Top             =   1140
         Width           =   1635
      End
      Begin VB.CheckBox chkJobStatus 
         Caption         =   "Authorised"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   14
         ToolTipText     =   "Authorised purchase orders"
         Top             =   840
         Width           =   1395
      End
      Begin VB.CheckBox chkJobStatus 
         Caption         =   "Awaiting Authorisation"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   13
         ToolTipText     =   "Purchase orders awaiting authorization"
         Top             =   540
         Width           =   1935
      End
      Begin VB.CheckBox chkJobStatus 
         Caption         =   "New"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   12
         ToolTipText     =   "New purchase orders"
         Top             =   240
         Width           =   1395
      End
      Begin VB.CheckBox chkJobStatus 
         Caption         =   "Cancelled"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   16
         ToolTipText     =   "Cancelled purchase orders"
         Top             =   1740
         Width           =   1635
      End
   End
   Begin VB.Frame fraDateRanges 
      Caption         =   "Date Ranges"
      Height          =   1455
      Left            =   60
      TabIndex        =   52
      Top             =   60
      Width           =   4695
      Begin MSComCtl2.DTPicker datDeliveryDate 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   1380
         TabIndex        =   0
         Tag             =   "NOCHECK"
         ToolTipText     =   "Delivery date from"
         Top             =   300
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   83623937
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datDeliveryDate 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   3060
         TabIndex        =   1
         Tag             =   "NOCHECK"
         ToolTipText     =   "Delivery date to"
         Top             =   300
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   83623937
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datReturnDate 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   3060
         TabIndex        =   3
         Tag             =   "NOCHECK"
         ToolTipText     =   "Return date to"
         Top             =   660
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   83623937
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datReturnDate 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   1380
         TabIndex        =   2
         Tag             =   "NOCHECK"
         ToolTipText     =   "Return date from"
         Top             =   660
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   83623937
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datCreatedDate 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   1380
         TabIndex        =   4
         Tag             =   "NOCHECK"
         ToolTipText     =   "Created date from"
         Top             =   1020
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   83623937
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datCreatedDate 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   3060
         TabIndex        =   5
         Tag             =   "NOCHECK"
         ToolTipText     =   "Created date to"
         Top             =   1020
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   83623937
         CurrentDate     =   37870
      End
      Begin VB.Label lblCaption 
         Caption         =   "Created Date"
         Height          =   285
         Index           =   21
         Left            =   240
         TabIndex        =   55
         Top             =   1080
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Delivery By"
         Height          =   285
         Index           =   6
         Left            =   240
         TabIndex        =   54
         Top             =   360
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Return By"
         Height          =   285
         Index           =   11
         Left            =   240
         TabIndex        =   53
         Top             =   720
         Width           =   1035
      End
   End
   Begin MSAdodcLib.Adodc adoCostCodes 
      Height          =   330
      Left            =   11640
      Top             =   4620
      Visible         =   0   'False
      Width           =   3120
      _ExtentX        =   5503
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCostCodes"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoCETAUser 
      Height          =   330
      Left            =   11640
      Top             =   4980
      Visible         =   0   'False
      Width           =   3105
      _ExtentX        =   5477
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCetaUser"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CheckBox chkZeroValue 
      Caption         =   "Zero Value Orders"
      Height          =   255
      Left            =   11580
      TabIndex        =   66
      ToolTipText     =   "Orders that have a total value of 0"
      Top             =   2100
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.ComboBox cmbDirection 
      Height          =   315
      ItemData        =   "frmPurchaseSearch.frx":08E5
      Left            =   8220
      List            =   "frmPurchaseSearch.frx":08EF
      Style           =   2  'Dropdown List
      TabIndex        =   64
      ToolTipText     =   "Which data field to search against"
      Top             =   7620
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.ComboBox cmbOrderBy 
      Height          =   315
      Left            =   5880
      TabIndex        =   63
      Tag             =   "NOCLEAR"
      Text            =   "cmbOrderBy"
      ToolTipText     =   "Which data field to search against"
      Top             =   7620
      Visible         =   0   'False
      Width           =   2235
   End
   Begin VB.Frame Frame2 
      Caption         =   "Number Ranges"
      Height          =   1755
      Left            =   60
      TabIndex        =   56
      Top             =   1560
      Width           =   4695
      Begin VB.TextBox txtPurchaseHeaderID 
         BackColor       =   &H00C0FFFF&
         Enabled         =   0   'False
         ForeColor       =   &H80000012&
         Height          =   315
         Index           =   3
         Left            =   3120
         TabIndex        =   69
         ToolTipText     =   "The unique identifier for this order (End Range)"
         Top             =   1380
         Visible         =   0   'False
         Width           =   1275
      End
      Begin VB.TextBox txtProjectNumber 
         BackColor       =   &H00C0FFFF&
         DataField       =   "purchaseheader.projectnumber"
         ForeColor       =   &H80000012&
         Height          =   315
         Index           =   2
         Left            =   1440
         TabIndex        =   68
         Top             =   1380
         Width           =   1275
      End
      Begin VB.TextBox txtPurchaseHeaderID 
         DataField       =   "purchaseheader.purchaseheaderID"
         ForeColor       =   &H80000012&
         Height          =   315
         Index           =   0
         Left            =   1440
         TabIndex        =   10
         ToolTipText     =   "The unique identifier for this order (Start range)"
         Top             =   1020
         Width           =   1275
      End
      Begin VB.TextBox txtPurchaseHeaderID 
         Enabled         =   0   'False
         ForeColor       =   &H80000012&
         Height          =   315
         Index           =   1
         Left            =   3120
         TabIndex        =   11
         ToolTipText     =   "The unique identifier for this order (End Range)"
         Top             =   1020
         Visible         =   0   'False
         Width           =   1275
      End
      Begin VB.TextBox txtJobID 
         BackColor       =   &H00FFC0C0&
         DataField       =   "purchaseheader.jobID"
         Height          =   315
         Index           =   0
         Left            =   1440
         TabIndex        =   8
         ToolTipText     =   "The unique job identifier for this job (Start range)"
         Top             =   660
         Width           =   1275
      End
      Begin VB.TextBox txtJobID 
         BackColor       =   &H00FFC0C0&
         Enabled         =   0   'False
         Height          =   315
         Index           =   1
         Left            =   3120
         TabIndex        =   9
         ToolTipText     =   "The unique job identifier for this job (End range)"
         Top             =   660
         Visible         =   0   'False
         Width           =   1275
      End
      Begin VB.TextBox txtAuthorisationNumber 
         BackColor       =   &H0080C0FF&
         DataField       =   "authorisationnumber"
         Height          =   315
         Index           =   0
         Left            =   1440
         TabIndex        =   6
         ToolTipText     =   "Authorization number (End range)"
         Top             =   300
         Width           =   1275
      End
      Begin VB.TextBox txtAuthorisationNumber 
         BackColor       =   &H0080C0FF&
         Enabled         =   0   'False
         Height          =   315
         Index           =   1
         Left            =   3120
         TabIndex        =   7
         ToolTipText     =   "Authorization number (Start range)"
         Top             =   300
         Visible         =   0   'False
         Width           =   1275
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Enabled         =   0   'False
         Height          =   285
         Index           =   27
         Left            =   2820
         TabIndex        =   71
         Top             =   1440
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Caption         =   "Project #"
         Height          =   285
         Index           =   26
         Left            =   240
         TabIndex        =   70
         Top             =   1440
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Purchase ID"
         Height          =   285
         Index           =   1
         Left            =   240
         TabIndex        =   62
         Top             =   1080
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Enabled         =   0   'False
         Height          =   285
         Index           =   14
         Left            =   2820
         TabIndex        =   61
         Top             =   1080
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Caption         =   "Job ID"
         Height          =   285
         Index           =   0
         Left            =   240
         TabIndex        =   60
         Top             =   720
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Enabled         =   0   'False
         Height          =   285
         Index           =   15
         Left            =   2820
         TabIndex        =   59
         Top             =   720
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.Label lblCaption 
         Caption         =   "P/O Auth #"
         Height          =   285
         Index           =   24
         Left            =   240
         TabIndex        =   58
         Top             =   360
         Width           =   1275
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Enabled         =   0   'False
         Height          =   285
         Index           =   9
         Left            =   2820
         TabIndex        =   57
         Top             =   360
         Visible         =   0   'False
         Width           =   195
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Who did what?"
      Height          =   1095
      Left            =   4860
      TabIndex        =   49
      Top             =   2220
      Width           =   2115
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbAuthBy 
         Bindings        =   "frmPurchaseSearch.frx":08FE
         DataField       =   "authorisedbyuser"
         Height          =   315
         Left            =   1080
         TabIndex        =   17
         ToolTipText     =   "Initials of user who authorised purchase order"
         Top             =   300
         Width           =   915
         DataFieldList   =   "initials"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BorderStyle     =   0
         ForeColorEven   =   -2147483640
         ForeColorOdd    =   -2147483640
         BackColorEven   =   -2147483643
         BackColorOdd    =   16761024
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2117
         Columns(0).Caption=   "Initials"
         Columns(0).Name =   "initials"
         Columns(0).DataField=   "initials"
         Columns(0).FieldLen=   256
         Columns(1).Width=   5556
         Columns(1).Caption=   "Full Name"
         Columns(1).Name =   "fullname"
         Columns(1).DataField=   "fullname"
         Columns(1).FieldLen=   256
         _ExtentX        =   1614
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16761024
         DataFieldToDisplay=   "initials"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCreatedBy 
         Bindings        =   "frmPurchaseSearch.frx":0918
         DataField       =   "cuser"
         Height          =   315
         Left            =   1080
         TabIndex        =   18
         ToolTipText     =   "Initials of user who created the purchase order"
         Top             =   660
         Width           =   915
         DataFieldList   =   "initials"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BorderStyle     =   0
         ForeColorEven   =   -2147483640
         ForeColorOdd    =   -2147483640
         BackColorEven   =   -2147483643
         BackColorOdd    =   16761024
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2117
         Columns(0).Caption=   "Initials"
         Columns(0).Name =   "initials"
         Columns(0).DataField=   "initials"
         Columns(0).FieldLen=   256
         Columns(1).Width=   5556
         Columns(1).Caption=   "Full Name"
         Columns(1).Name =   "fullname"
         Columns(1).DataField=   "fullname"
         Columns(1).FieldLen=   256
         _ExtentX        =   1614
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16761024
         DataFieldToDisplay=   "initials"
      End
      Begin VB.Label lblCaption 
         Caption         =   "Created By"
         Height          =   225
         Index           =   19
         Left            =   120
         TabIndex        =   51
         Top             =   720
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Auth By"
         Height          =   225
         Index           =   20
         Left            =   120
         TabIndex        =   50
         Top             =   360
         Width           =   855
      End
   End
   Begin MSAdodcLib.Adodc adoPurchaseOrder 
      Height          =   330
      Left            =   11640
      Top             =   4260
      Visible         =   0   'False
      Width           =   3105
      _ExtentX        =   5477
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoPurchaseOrder"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.ComboBox cmbReturnFor 
      Appearance      =   0  'Flat
      DataField       =   "returnfor"
      Height          =   315
      Left            =   10020
      TabIndex        =   28
      Top             =   3000
      Width           =   1455
   End
   Begin VB.ComboBox cmbReturnMethod 
      Appearance      =   0  'Flat
      DataField       =   "returnmethod"
      Height          =   315
      Left            =   8220
      TabIndex        =   27
      ToolTipText     =   "The return by time"
      Top             =   3000
      Width           =   1455
   End
   Begin VB.TextBox txtTitle 
      DataField       =   "title"
      Height          =   315
      Left            =   8220
      TabIndex        =   24
      ToolTipText     =   "The title for the job"
      Top             =   2280
      Width           =   3255
   End
   Begin VB.TextBox txtSupplierReference 
      DataField       =   "supplierreference"
      Height          =   315
      Left            =   8220
      TabIndex        =   23
      ToolTipText     =   "The clients supplier reference number"
      Top             =   1560
      Width           =   3255
   End
   Begin VB.ComboBox cmbDeliveryMethod 
      Appearance      =   0  'Flat
      DataField       =   "deliverymethod"
      Height          =   315
      Left            =   8220
      TabIndex        =   25
      ToolTipText     =   "The delivery deadline time"
      Top             =   2640
      Width           =   1455
   End
   Begin VB.ComboBox cmbOrderType 
      Appearance      =   0  'Flat
      DataField       =   "ordertype"
      Height          =   315
      Left            =   8220
      TabIndex        =   19
      ToolTipText     =   "Purchase order type"
      Top             =   120
      Width           =   3255
   End
   Begin VB.ComboBox cmbDepartment 
      Appearance      =   0  'Flat
      DataField       =   "department"
      Height          =   315
      Left            =   8220
      TabIndex        =   22
      ToolTipText     =   "Department"
      Top             =   1200
      Width           =   3255
   End
   Begin VB.ComboBox cmbDeliveryFor 
      Appearance      =   0  'Flat
      DataField       =   "deliverfor"
      Height          =   315
      Left            =   10020
      TabIndex        =   26
      Top             =   2640
      Width           =   1455
   End
   Begin VB.PictureBox picFooter 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   1140
      ScaleHeight     =   315
      ScaleWidth      =   10395
      TabIndex        =   35
      Top             =   7140
      Width           =   10395
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   5460
         TabIndex        =   67
         ToolTipText     =   "Clear the form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdRun 
         Caption         =   "Run"
         Height          =   315
         Left            =   4260
         TabIndex        =   30
         ToolTipText     =   "Run the stored search"
         Top             =   0
         Width           =   1155
      End
      Begin VB.ComboBox cmbMacroSearch 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1260
         Style           =   2  'Dropdown List
         TabIndex        =   29
         ToolTipText     =   "Previously stored searches to use"
         Top             =   0
         Width           =   1515
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   7980
         TabIndex        =   32
         ToolTipText     =   "Search using set criterea"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   9180
         TabIndex        =   33
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClear 
         Cancel          =   -1  'True
         Caption         =   "Clear (Esc)"
         Height          =   315
         Left            =   6720
         TabIndex        =   31
         ToolTipText     =   "Clear the form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Stored searches:"
         Height          =   255
         Index           =   3
         Left            =   0
         TabIndex        =   36
         Top             =   60
         Width           =   1275
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      DataField       =   "companyname"
      Height          =   315
      Left            =   8220
      TabIndex        =   20
      ToolTipText     =   "The company this job is for"
      Top             =   480
      Width           =   3255
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "accountcode"
      Columns(2).Name =   "accountcode"
      Columns(2).DataField=   "accountcode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "telephone"
      Columns(3).Name =   "telephone"
      Columns(3).DataField=   "telephone"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "cetaclientcode"
      Columns(4).Name =   "cetaclientcode"
      Columns(4).DataField=   "cetaclientcode"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "accountstatus"
      Columns(5).Name =   "accountstatus"
      Columns(5).DataField=   "accountstatus"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "fax"
      Columns(6).Name =   "fax"
      Columns(6).DataField=   "fax"
      Columns(6).FieldLen=   256
      _ExtentX        =   5741
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbContact 
      DataField       =   "contactname"
      Height          =   315
      Left            =   8220
      TabIndex        =   21
      ToolTipText     =   "The contact for this job"
      Top             =   840
      Width           =   3255
      DataFieldList   =   "name"
      _Version        =   196617
      BorderStyle     =   0
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   5424
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "telephone"
      Columns(1).Name =   "telephone"
      Columns(1).DataField=   "telephone"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "contactID"
      Columns(2).Name =   "contactID"
      Columns(2).DataField=   "contactID"
      Columns(2).FieldLen=   256
      _ExtentX        =   5741
      _ExtentY        =   556
      _StockProps     =   93
      ForeColor       =   -2147483640
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdPurchase 
      Bindings        =   "frmPurchaseSearch.frx":0932
      Height          =   3375
      Left            =   60
      TabIndex        =   34
      Top             =   3420
      Width           =   11415
      _Version        =   196617
      AllowUpdate     =   0   'False
      BackColorOdd    =   12640511
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   18
      Columns(0).Width=   1773
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "purchaseheaderID"
      Columns(0).DataField=   "purchaseheaderID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   2170
      Columns(1).Caption=   "P/O Auth #"
      Columns(1).Name =   "authorisationnumber"
      Columns(1).DataField=   "authorisationnumber"
      Columns(1).FieldLen=   256
      Columns(2).Width=   2170
      Columns(2).Caption=   "Date"
      Columns(2).Name =   "cdate"
      Columns(2).DataField=   "cdate"
      Columns(2).DataType=   7
      Columns(2).FieldLen=   256
      Columns(3).Width=   4101
      Columns(3).Caption=   "Supplier"
      Columns(3).Name =   "companyname"
      Columns(3).DataField=   "companyname"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "Product Name"
      Columns(4).Name =   "Product Name"
      Columns(4).DataField=   "productname"
      Columns(4).DataType=   10
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "Title"
      Columns(5).Name =   "title"
      Columns(5).DataField=   "title"
      Columns(5).FieldLen=   256
      Columns(6).Width=   2037
      Columns(6).Caption=   "Order Total"
      Columns(6).Name =   "total"
      Columns(6).Alignment=   1
      Columns(6).NumberFormat=   "�00.00"
      Columns(6).FieldLen=   256
      Columns(7).Width=   1773
      Columns(7).Caption=   "Created By"
      Columns(7).Name =   "cuser"
      Columns(7).DataField=   "cuser"
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Caption=   "Status"
      Columns(8).Name =   "fd_status"
      Columns(8).DataField=   "fd_status"
      Columns(8).FieldLen=   256
      Columns(9).Width=   1773
      Columns(9).Caption=   "Auth By"
      Columns(9).Name =   "authorisedbyuser"
      Columns(9).DataField=   "authorisedbyuser"
      Columns(9).FieldLen=   256
      Columns(10).Width=   2170
      Columns(10).Caption=   "Order Type"
      Columns(10).Name=   "ordertype"
      Columns(10).DataField=   "ordertype"
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Caption=   "Contact"
      Columns(11).Name=   "contactname"
      Columns(11).DataField=   "contactname"
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "jobID"
      Columns(12).Name=   "jobID"
      Columns(12).DataField=   "jobID"
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "companyID"
      Columns(13).Name=   "companyID"
      Columns(13).DataField=   "companyID"
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "contactID"
      Columns(14).Name=   "contactID"
      Columns(14).DataField=   "contactID"
      Columns(14).FieldLen=   256
      Columns(15).Width=   3200
      Columns(15).Caption=   "Supplier Reference"
      Columns(15).Name=   "supplierreference"
      Columns(15).DataField=   "supplierreference"
      Columns(15).FieldLen=   256
      Columns(16).Width=   3200
      Columns(16).Caption=   "Delivery Method"
      Columns(16).Name=   "deliverymethod"
      Columns(16).DataField=   "deliverymethod"
      Columns(16).FieldLen=   256
      Columns(17).Width=   3200
      Columns(17).Caption=   "Return Method"
      Columns(17).Name=   "returnmethod"
      Columns(17).DataField=   "returnmethod"
      Columns(17).FieldLen=   256
      _ExtentX        =   20135
      _ExtentY        =   5953
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbProduct 
      DataField       =   "productname"
      Height          =   315
      Left            =   8220
      TabIndex        =   73
      ToolTipText     =   "The product on this job"
      Top             =   1920
      Width           =   3255
      DataFieldList   =   "name"
      _Version        =   196617
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      CheckBox3D      =   0   'False
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   8438015
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "productID"
      Columns(0).Name =   "productID"
      Columns(0).DataField=   "productID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   5212
      Columns(1).Caption=   "Name"
      Columns(1).Name =   "name"
      Columns(1).DataField=   "name"
      Columns(1).FieldLen=   256
      _ExtentX        =   5741
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   8438015
      DataFieldToDisplay=   "name"
   End
   Begin VB.Label lblCaption 
      Caption         =   "Product"
      Height          =   285
      Index           =   28
      Left            =   7080
      TabIndex        =   72
      Top             =   1980
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order By"
      Height          =   255
      Index           =   22
      Left            =   4740
      TabIndex        =   65
      Top             =   7680
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "For"
      Height          =   285
      Index           =   13
      Left            =   9720
      TabIndex        =   47
      Top             =   3060
      Width           =   375
   End
   Begin VB.Label lblCaption 
      Caption         =   "Ret. Method"
      Height          =   285
      Index           =   12
      Left            =   7080
      TabIndex        =   46
      Top             =   3060
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order Type"
      Height          =   285
      Index           =   16
      Left            =   7080
      TabIndex        =   45
      Top             =   180
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      Height          =   285
      Index           =   8
      Left            =   7080
      TabIndex        =   44
      Top             =   2340
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Supplier Ref"
      Height          =   285
      Index           =   4
      Left            =   7080
      TabIndex        =   43
      Top             =   1620
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Contact"
      Height          =   285
      Index           =   5
      Left            =   7080
      TabIndex        =   42
      Top             =   900
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Supplier"
      Height          =   285
      Index           =   2
      Left            =   7080
      TabIndex        =   41
      Top             =   540
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Department"
      Height          =   285
      Index           =   10
      Left            =   7080
      TabIndex        =   40
      Top             =   1260
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Del. Method"
      Height          =   285
      Index           =   7
      Left            =   7080
      TabIndex        =   39
      Top             =   2700
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "For"
      Height          =   285
      Index           =   23
      Left            =   9720
      TabIndex        =   38
      Top             =   2700
      Width           =   375
   End
   Begin VB.Label lblCompanyID 
      BackColor       =   &H0000FFFF&
      Height          =   255
      Left            =   13140
      TabIndex        =   37
      Top             =   360
      Visible         =   0   'False
      Width           =   1275
   End
End
Attribute VB_Name = "frmPurchaseSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmbBudgetCode_Change()

End Sub

Private Sub cmbCompany_Click()
lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

End Sub

Private Sub cmbCompany_CloseUp()

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

End Sub

Private Sub cmbCompany_DropDown()

Dim l_strSQL As String
l_strSQL = "SELECT name, accountcode, telephone, companyID FROM company WHERE name LIKE '" & QuoteSanitise(cmbCompany.Text) & "%' AND issupplier IS NOT NULL AND issupplier <> 0 ORDER BY name"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmbContact_DropDown()

If lblCompanyID.Caption = "" Then
    NoCompanySelectedMessage
    Exit Sub
End If

Dim l_strSQL As String

l_strSQL = "SELECT company.companyID, contact.contactID, contact.name, contact.telephone, employee.jobtitle FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((company.companyID)=" & lblCompanyID.Caption & ")) ORDER BY contact.name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbContact.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmbOurReference_DropDown()
cmbOurReference.RemoveAll
PopulateCombo "ourreference", cmbOurReference, ddnCETACodes
cmbOurReference.Columns(0).Width = 515
cmbOurReference.Columns(1).Width = 5000
End Sub

Private Sub cmbProduct_DropDown()
Dim l_strSQL As String
l_strSQL = "SELECT name, productID FROM product WHERE name LIKE '" & QuoteSanitise(cmbProduct.Text) & "%' ORDER BY name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbProduct.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing
End Sub

Private Sub cmdBudget_Click()
If Not CheckAccess("/purchasebudgets") Then Exit Sub


End Sub

Private Sub cmdClear_Click()

ClearFields Me

adoPurchaseOrder.ConnectionString = g_strConnection
adoPurchaseOrder.RecordSource = "SELECT * FROM purchaseheader WHERE purchaseheaderID = -1"
adoPurchaseOrder.Refresh

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdPrint_Click()

Dim l_searchstring As String
l_searchstring = adoPurchaseOrder.RecordSource

If l_searchstring <> "" Then
    PrintCrystalReportUsingSQL g_strLocationOfCrystalReportFiles & "purchaseordersearch.rpt", l_searchstring, g_blnPreviewReport
End If

End Sub

Private Sub cmdSearch_Click()

Screen.MousePointer = vbHourglass

Dim l_strSQL As String
Dim l_blnFirstWhereDone As Boolean
Dim l_strFields As String
l_blnFirstWhereDone = False

'l_strSQL = "SELECT * FROM purchaseheader "
l_strFields = "SELECT distinct purchaseheader.purchaseheaderID, purchaseheader.authorisationnumber, purchaseheader.fd_status,purchaseheader.jobID, purchaseheader.companyID, purchaseheader.contactID, "
l_strFields = l_strFields & "purchaseheader.companyname, purchaseheader.contactname, purchaseheader.supplierreference, purchaseheader.deliverymethod, purchaseheader.returnmethod, "
l_strFields = l_strFields & "purchaseheader.cdate, purchaseheader.cuser, purchaseheader.title, purchaseheader.productname, purchaseheader.authorisedbyuser, purchaseheader.ordertype "

l_strSQL = " FROM purchaseheader INNER JOIN purchasedetail ON purchaseheader.purchaseheaderID = purchasedetail.purchaseheaderID "

l_blnFirstWhereDone = True
l_strSQL = l_strSQL & " WHERE (purchaseheader.purchaseheaderID <> -1) "

If cmbCompany.Text <> "" Then
    l_blnFirstWhereDone = True
    l_strSQL = l_strSQL & " AND (companyname LIKE '" & QuoteSanitise(cmbCompany.Text) & "%') "
End If

Dim l_intControlLoop As Integer

For l_intControlLoop = 0 To Me.Controls.Count - 1
    If TypeOf Me.Controls(l_intControlLoop) Is TextBox Or TypeOf Me.Controls(l_intControlLoop) Is SSOleDBCombo Or TypeOf Me.Controls(l_intControlLoop) Is ComboBox Then
        If Me.Controls(l_intControlLoop).DataField <> "" And Me.Controls(l_intControlLoop).Text <> "" Then
            If l_blnFirstWhereDone = True Then
                l_strSQL = l_strSQL & " AND (" & Me.Controls(l_intControlLoop).DataField & " LIKE '" & QuoteSanitise(Me.Controls(l_intControlLoop).Text) & "%') "
            Else
                l_strSQL = l_strSQL & " WHERE (" & Me.Controls(l_intControlLoop).DataField & " LIKE '" & QuoteSanitise(Me.Controls(l_intControlLoop).Text) & "%') "
                l_blnFirstWhereDone = True
            End If
        End If
    'ElseIf TypeOf Me.Controls(l_intControlLoop) Is DTPicker Then
    '    If Me.Controls(l_intControlLoop).Value <> "" Then
    '        If l_blnFirstWhereDone = False Then
    '            l_strSQL = l_strSQL & " WHERE (" & Me.Controls(l_intControlLoop).DataField & " = '" & Format(Me.Controls(l_intControlLoop).Value, "yyyy-mm-dd") & "') "
    '            l_blnFirstWhereDone = True
    '        Else
    '            l_strSQL = l_strSQL & " AND (" & Me.Controls(l_intControlLoop).DataField & " = '" & Format(Me.Controls(l_intControlLoop).Value, "yyyy-mm-dd") & "') "
    '        End If
    '    End If
    End If
Next

Dim l_strStatusCheckSQL As String

For l_intLoop = 0 To 5
    'loop through the tick boxes and show only jobs with selected status
    If chkJobStatus(l_intLoop).Value <> 0 Then
        l_strStatusCheckSQL = l_strStatusCheckSQL & " OR (purchaseheader.fd_status = '" & chkJobStatus(l_intLoop).Caption & "') "
    End If
Next

If Trim(l_strStatusCheckSQL) <> "" Then
    l_strSQL = l_strSQL & " AND (" & Right(l_strStatusCheckSQL, Len(l_strStatusCheckSQL) - 4) & ")"
End If

If Not IsNull(datDeliveryDate(0).Value) Then
    l_strSQL = l_strSQL & " AND (deliverydate >= '" & FormatSQLDate(Format(datDeliveryDate(0).Value, vbShortDateFormat) & " 00:00") & "')"
End If

If Not IsNull(datDeliveryDate(1).Value) Then
    l_strSQL = l_strSQL & " AND (deliverydate <= '" & FormatSQLDate(Format(datDeliveryDate(1).Value, vbShortDateFormat) & " 23:59") & "')"
End If

If Not IsNull(datReturnDate(0).Value) Then
    l_strSQL = l_strSQL & " AND (returndate >= '" & FormatSQLDate(Format(datReturnDate(0).Value, vbShortDateFormat) & " 00:00") & "')"
End If

If Not IsNull(datReturnDate(1).Value) Then
    l_strSQL = l_strSQL & " AND (returndate <= '" & FormatSQLDate(Format(datReturnDate(1).Value, vbShortDateFormat) & " 23:59") & "')"
End If

If Not IsNull(datCreatedDate(0).Value) Then
    l_strSQL = l_strSQL & " AND (cdate >= '" & FormatSQLDate(Format(datCreatedDate(0).Value, vbShortDateFormat) & " 00:00") & "')"
End If

If Not IsNull(datCreatedDate(1).Value) Then
    l_strSQL = l_strSQL & " AND (cdate <= '" & FormatSQLDate(Format(datCreatedDate(1).Value, vbShortDateFormat) & " 23:59") & "')"
End If


adoPurchaseOrder.ConnectionString = g_strConnection
adoPurchaseOrder.RecordSource = l_strFields & l_strSQL

'If chkZeroValue.Value = 1 Then adoPurchaseOrder.RecordSource = adoPurchaseOrder.RecordSource & " HAVING Sum(purchasedetail.total) = 0"
adoPurchaseOrder.RecordSource = adoPurchaseOrder.RecordSource & " ORDER BY " & cmbOrderBy.Text & " " & cmbDirection.Text & ", purchaseheader.purchaseheaderID;"

adoPurchaseOrder.Refresh

Me.Caption = "Purchase Order Search - Total P/O Value = " & g_strCurrency & Format(GetCount("SELECT SUM(purchasedetail.total) " & l_strSQL), "0.00")

DBEngine.Idle
DoEvents

Screen.MousePointer = vbDefault

End Sub

Private Sub Form_Load()


PopulateCombo "purchaseordertype", cmbOrderType
PopulateCombo "despatchmethod", cmbDeliveryMethod
PopulateCombo "despatchmethod", cmbReturnMethod
PopulateCombo "department", cmbDepartment
PopulateCombo "users", cmbDeliveryFor
PopulateCombo "users", cmbReturnFor

'PopulateCombo "fields-purchase", cmbOrderBy
cmbDirection.AddItem "DESC"
cmbDirection.AddItem "ASC"

cmbOrderBy.Text = "cdate"
cmbDirection.Text = "DESC"

If Not CheckAccess("/seeotherpeoplespurchaseorders", True) Then
    cmbCreatedBy.Text = g_strUserInitials
    cmbDepartment.Text = GetData("cetauser", "department", "initials", g_strUserInitials)
End If

l_strSQL = "SELECT code, description FROM budget ORDER BY code;"

adoCostCodes.ConnectionString = g_strConnection
adoCostCodes.RecordSource = l_strSQL
adoCostCodes.Refresh


If Not CheckAccess("/viewallpurchaseorders", True) Then
    cmbCreatedBy.Enabled = False
    cmbDepartment.Enabled = False
    cmbDepartment.Tag = "NOCLEAR"
    cmbCreatedBy.Tag = "NOCLEAR"
Else
    cmbCreatedBy.Enabled = True
    cmbDepartment.Enabled = True
    cmbDepartment.Tag = ""
    cmbCreatedBy.Tag = ""
End If

If Not CheckAccess("/viewdepartmentpurchaseorders", True) Then
    cmbCreatedBy.Enabled = False
    cmbCreatedBy.Tag = "NOCLEAR"
Else
    cmbCreatedBy.Enabled = True
    cmbCreatedBy.Tag = ""
End If


adoCETAUser.ConnectionString = g_strConnection
adoCETAUser.RecordSource = "SELECT initials, fullname FROM cetauser ORDER BY fullname"
adoCETAUser.Refresh

End Sub


Private Sub Form_Resize()

On Error Resume Next

grdPurchase.Height = Me.ScaleHeight - grdPurchase.Top - picFooter.Height - 300
grdPurchase.Width = Me.ScaleWidth - 200
picFooter.Top = Me.ScaleHeight - picFooter.Height - 60

End Sub


Private Sub grdPurchase_DblClick()

If grdPurchase.Row = grdPurchase.Rows Then Exit Sub
If Val(grdPurchase.Columns("purchaseheaderid").Text) = 0 Then Exit Sub
ShowPurchaseOrder grdPurchase.Columns("purchaseheaderID").Text, Val(grdPurchase.Columns("jobID").Text)
Me.Hide

End Sub

Private Sub grdPurchase_HeadClick(ByVal ColIndex As Integer)

If cmbOrderBy.Text <> grdPurchase.Columns(ColIndex).DataField Then
    cmbOrderBy.Text = grdPurchase.Columns(ColIndex).DataField
Else
    cmbDirection.ListIndex = 1 - cmbDirection.ListIndex
End If

cmdSearch_Click

End Sub

Private Sub picReturn_Click()

End Sub

Private Sub SSOleDBCombo1_InitColumnProps()
PopulateCombo "ourreference", cmbOurReference

End Sub

Private Sub grdPurchase_RowLoaded(ByVal Bookmark As Variant)

grdPurchase.Columns("total").Text = g_strCurrency & Format(GetCount("SELECT SUM(total) FROM purchasedetail " & "WHERE purchaseheaderID = " & grdPurchase.Columns("purchaseheaderID").Text), "0.00")

End Sub
