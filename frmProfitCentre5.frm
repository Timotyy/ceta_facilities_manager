VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmProfitCentre5 
   Caption         =   "Off Air Schedule"
   ClientHeight    =   8730
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10245
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8730
   ScaleWidth      =   10245
   WindowState     =   2  'Maximized
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdProfitCentre5 
      Bindings        =   "frmProfitCentre5.frx":0000
      Height          =   7935
      Left            =   180
      TabIndex        =   1
      Top             =   660
      Width           =   9825
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Caption=   "ProfitCentre5ID"
      Columns(0).Name =   "ProfitCentre5ID"
      Columns(0).DataField=   "ProfitCentre5ID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   8811
      Columns(1).Caption=   "ProfitCentre5Text"
      Columns(1).Name =   "ProfitCentre5Text"
      Columns(1).DataField=   "ProfitCentre5Text"
      Columns(1).FieldLen=   256
      Columns(2).Width=   4233
      Columns(2).Caption=   "VDMS_Charge_Code"
      Columns(2).Name =   "VDMS_Charge_Code"
      Columns(2).DataField=   "VDMS_Charge_Code"
      Columns(2).FieldLen=   256
      _ExtentX        =   17330
      _ExtentY        =   13996
      _StockProps     =   79
      Caption         =   "Profit Centre 5 Codes"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   6420
      TabIndex        =   0
      ToolTipText     =   "Close this form"
      Top             =   180
      Width           =   1335
   End
   Begin MSAdodcLib.Adodc adoProfitCentre5 
      Height          =   330
      Left            =   180
      Top             =   360
      Width           =   2100
      _ExtentX        =   3704
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoProfitCentre5"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
End
Attribute VB_Name = "frmProfitCentre5"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub Form_Load()

Dim l_strSQL As String

l_strSQL = "SELECT * FROM ProfitCentre5 ORDER BY ProfitCentre5ID;"
adoProfitCentre5.RecordSource = l_strSQL
adoProfitCentre5.ConnectionString = g_strConnection
adoProfitCentre5.Refresh

adoProfitCentre5.Caption = adoProfitCentre5.Recordset.RecordCount & " Items"

End Sub

Private Sub Form_Resize()

grdProfitCentre5.Height = Me.ScaleHeight - grdProfitCentre5.Top - 240

End Sub

