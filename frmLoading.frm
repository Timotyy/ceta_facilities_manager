VERSION 5.00
Begin VB.Form frmLoading 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   1755
   ClientLeft      =   4230
   ClientTop       =   6270
   ClientWidth     =   4155
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1755
   ScaleWidth      =   4155
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox picImage 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   120
      Picture         =   "frmLoading.frx":0000
      ScaleHeight     =   495
      ScaleWidth      =   495
      TabIndex        =   0
      Top             =   120
      Width           =   495
   End
   Begin VB.Shape Shape1 
      Height          =   1755
      Left            =   0
      Top             =   1
      Width           =   4155
   End
   Begin VB.Label lblVersionEtc 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "CETA Facilities Manager 2 - V."
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   840
      Width           =   3915
   End
   Begin VB.Label lblStatus 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Initialising..."
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   1320
      Width           =   3675
   End
   Begin VB.Image Image1 
      Height          =   495
      Left            =   1680
      Picture         =   "frmLoading.frx":0B0A
      Top             =   120
      Width           =   2385
   End
End
Attribute VB_Name = "frmLoading"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()

lblVersionEtc.Caption = "CETA Facilites Manager V." & App.Major & "." & App.Minor & "." & App.Revision
CenterForm Me

End Sub

