VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTracker 
   Caption         =   "Work Tracker"
   ClientHeight    =   14670
   ClientLeft      =   3060
   ClientTop       =   3210
   ClientWidth     =   28560
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmTracker.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   14670
   ScaleWidth      =   28560
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.CheckBox chkHideHeaderFields 
      Alignment       =   1  'Right Justify
      Caption         =   "Hide Header Fields"
      Height          =   255
      Left            =   2800
      TabIndex        =   105
      Tag             =   "NOCLEAR"
      Top             =   420
      Width           =   1815
   End
   Begin VB.CommandButton cmdCollectBBCStudioEnglishWork 
      Caption         =   "Collect BBC Studios English Work"
      Height          =   555
      Left            =   300
      TabIndex        =   103
      Top             =   3300
      Visible         =   0   'False
      Width           =   1515
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnWorkflowVariant 
      Bindings        =   "frmTracker.frx":08CA
      Height          =   1755
      Left            =   2400
      TabIndex        =   102
      Top             =   6360
      Width           =   7875
      DataFieldList   =   "VariantName"
      ListAutoValidate=   0   'False
      MaxDropDownItems=   20
      _Version        =   196617
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   2
      Columns(0).Width=   8819
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "portaluserID"
      Columns(0).Name =   "VariantID"
      Columns(0).DataField=   "VariantID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   4551
      Columns(1).Caption=   "VariantName"
      Columns(1).Name =   "VariantName"
      Columns(1).DataField=   "VariantName"
      Columns(1).FieldLen=   256
      _ExtentX        =   13891
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "VariantName"
   End
   Begin VB.TextBox txtDirectSQL 
      Height          =   315
      Left            =   21900
      TabIndex        =   99
      Top             =   3600
      Width           =   6135
   End
   Begin VB.ComboBox txtheadertext10 
      Height          =   315
      Left            =   8820
      TabIndex        =   96
      ToolTipText     =   "The Series for the Tape"
      Top             =   3240
      Visible         =   0   'False
      Width           =   2715
   End
   Begin VB.ComboBox txtheadertext9 
      Height          =   315
      Left            =   8820
      TabIndex        =   95
      ToolTipText     =   "The Series for the Tape"
      Top             =   2880
      Visible         =   0   'False
      Width           =   2715
   End
   Begin VB.ComboBox txtheadertext8 
      Height          =   315
      Left            =   8820
      TabIndex        =   94
      ToolTipText     =   "The Series for the Tape"
      Top             =   2520
      Visible         =   0   'False
      Width           =   2715
   End
   Begin VB.ComboBox txtheadertext7 
      Height          =   315
      Left            =   8820
      TabIndex        =   93
      ToolTipText     =   "The Series for the Tape"
      Top             =   2160
      Visible         =   0   'False
      Width           =   2715
   End
   Begin VB.ComboBox txtheadertext6 
      Height          =   315
      Left            =   8820
      TabIndex        =   92
      ToolTipText     =   "The Series for the Tape"
      Top             =   1800
      Visible         =   0   'False
      Width           =   2715
   End
   Begin VB.ComboBox txtheadertext5 
      Height          =   315
      Left            =   8820
      TabIndex        =   91
      ToolTipText     =   "The Series for the Tape"
      Top             =   1440
      Visible         =   0   'False
      Width           =   2715
   End
   Begin VB.ComboBox txtheadertext4 
      Height          =   315
      Left            =   8820
      TabIndex        =   90
      ToolTipText     =   "The Series for the Tape"
      Top             =   1080
      Visible         =   0   'False
      Width           =   2715
   End
   Begin VB.ComboBox txtheadertext3 
      Height          =   315
      Left            =   8820
      TabIndex        =   89
      ToolTipText     =   "The Series for the Tape"
      Top             =   720
      Visible         =   0   'False
      Width           =   2715
   End
   Begin VB.ComboBox txtheadertext2 
      Height          =   315
      Left            =   8820
      TabIndex        =   88
      ToolTipText     =   "The Series for the Tape"
      Top             =   360
      Visible         =   0   'False
      Width           =   2715
   End
   Begin VB.ComboBox txtheadertext1 
      Height          =   315
      Left            =   8820
      TabIndex        =   87
      ToolTipText     =   "The Series for the Tape"
      Top             =   0
      Visible         =   0   'False
      Width           =   2715
   End
   Begin VB.TextBox txtBarcode 
      Height          =   315
      Left            =   1860
      TabIndex        =   85
      Top             =   1440
      Width           =   2835
   End
   Begin VB.OptionButton optComplete 
      Height          =   255
      Index           =   11
      Left            =   16260
      TabIndex        =   83
      Tag             =   "NOCLEAR"
      Top             =   3660
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.OptionButton optComplete 
      Height          =   255
      Index           =   10
      Left            =   16260
      TabIndex        =   82
      Tag             =   "NOCLEAR"
      Top             =   3300
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.TextBox txtJobID 
      Height          =   315
      Left            =   8820
      TabIndex        =   79
      Top             =   3600
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "No Flags"
      Height          =   255
      Index           =   8
      Left            =   16260
      TabIndex        =   77
      Tag             =   "NOCLEAR"
      Top             =   1860
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.Frame fraFinalStatus 
      BorderStyle     =   0  'None
      Height          =   2175
      Left            =   19440
      TabIndex        =   71
      Top             =   60
      Width           =   1995
      Begin VB.OptionButton optComplete 
         Caption         =   "Tape Not Here"
         Height          =   255
         Index           =   12
         Left            =   0
         TabIndex        =   84
         Tag             =   "NOCLEAR"
         Top             =   600
         Width           =   2175
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "Tape Here"
         Height          =   255
         Index           =   9
         Left            =   0
         TabIndex        =   78
         Tag             =   "NOCLEAR"
         Top             =   300
         Width           =   2175
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "Not Complete"
         Height          =   255
         Index           =   0
         Left            =   0
         TabIndex        =   76
         Tag             =   "NOCLEAR"
         Top             =   0
         Width           =   2175
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "Finished"
         Height          =   255
         Index           =   1
         Left            =   0
         TabIndex        =   75
         Tag             =   "NOCLEAR"
         Top             =   1200
         Width           =   2175
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "Billed"
         Height          =   255
         Index           =   2
         Left            =   0
         TabIndex        =   74
         Tag             =   "NOCLEAR"
         Top             =   1500
         Width           =   2175
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "All Items"
         Height          =   255
         Index           =   3
         Left            =   0
         TabIndex        =   73
         Tag             =   "NOCLEAR"
         Top             =   1800
         Width           =   2175
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "Pending"
         Height          =   255
         Index           =   4
         Left            =   0
         TabIndex        =   72
         Tag             =   "NOCLEAR"
         Top             =   900
         Width           =   2175
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Special Command"
      Height          =   735
      Left            =   25200
      TabIndex        =   70
      Top             =   240
      Visible         =   0   'False
      Width           =   2775
   End
   Begin VB.CommandButton cmdNewPlaformOrder 
      Caption         =   "Create New Tracker Order"
      Height          =   555
      Left            =   300
      TabIndex        =   68
      Top             =   2700
      Width           =   1515
   End
   Begin VB.OptionButton optComplete 
      Height          =   255
      Index           =   7
      Left            =   16260
      TabIndex        =   66
      Tag             =   "NOCLEAR"
      Top             =   2940
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.OptionButton optComplete 
      Height          =   255
      Index           =   6
      Left            =   16260
      TabIndex        =   65
      Tag             =   "NOCLEAR"
      Top             =   2580
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.OptionButton optComplete 
      Height          =   255
      Index           =   5
      Left            =   16260
      TabIndex        =   64
      Tag             =   "NOCLEAR"
      Top             =   2220
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.CheckBox chkHeaderFlag5 
      Height          =   255
      Left            =   16260
      TabIndex        =   53
      Tag             =   "NOCLEAR"
      Top             =   1500
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.CheckBox chkHeaderFlag4 
      Height          =   255
      Left            =   16260
      TabIndex        =   52
      Tag             =   "NOCLEAR"
      Top             =   1140
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.CheckBox chkHeaderFlag3 
      Height          =   255
      Left            =   16260
      TabIndex        =   51
      Tag             =   "NOCLEAR"
      Top             =   780
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.CheckBox chkHeaderFlag2 
      Height          =   255
      Left            =   16260
      TabIndex        =   50
      Tag             =   "NOCLEAR"
      Top             =   420
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.CheckBox chkHeaderFlag1 
      Height          =   255
      Left            =   16260
      TabIndex        =   49
      Tag             =   "NOCLEAR"
      Top             =   60
      Visible         =   0   'False
      Width           =   2895
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnPortalUser 
      Bindings        =   "frmTracker.frx":08EB
      Height          =   1755
      Left            =   480
      TabIndex        =   44
      Top             =   5220
      Width           =   7875
      DataFieldList   =   "fullname"
      ListAutoValidate=   0   'False
      MaxDropDownItems=   20
      _Version        =   196617
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   2
      Columns(0).Width=   8819
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "portaluserID"
      Columns(0).Name =   "portaluserID"
      Columns(0).DataField=   "portaluserID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   4551
      Columns(1).Caption=   "fullname"
      Columns(1).Name =   "fullname"
      Columns(1).DataField=   "fullname"
      Columns(1).FieldLen=   256
      _ExtentX        =   13891
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "fullname"
   End
   Begin VB.TextBox txtReference 
      Height          =   315
      Left            =   1860
      TabIndex        =   40
      Top             =   1080
      Width           =   2835
   End
   Begin VB.CommandButton cmdCopyBack 
      Caption         =   "Copy Back Into Tracker"
      Height          =   315
      Left            =   1860
      TabIndex        =   38
      Top             =   1800
      Width           =   2835
   End
   Begin VB.CheckBox chkLockGB 
      Alignment       =   1  'Right Justify
      Caption         =   "Lock GB size"
      Height          =   255
      Left            =   21840
      TabIndex        =   35
      Top             =   1080
      Width           =   1635
   End
   Begin VB.CheckBox chkLockDur 
      Alignment       =   1  'Right Justify
      Caption         =   "Lock Duration"
      Height          =   255
      Left            =   21840
      TabIndex        =   34
      Top             =   780
      Width           =   1635
   End
   Begin VB.TextBox txtTotalSize 
      Height          =   315
      Left            =   23280
      TabIndex        =   32
      Top             =   360
      Width           =   1275
   End
   Begin VB.TextBox txtheaderint5 
      Height          =   315
      Left            =   14640
      TabIndex        =   28
      Top             =   1440
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtheaderint4 
      Height          =   315
      Left            =   14640
      TabIndex        =   27
      Top             =   1080
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtheaderint3 
      Height          =   315
      Left            =   14640
      TabIndex        =   26
      Top             =   720
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtTotalDuration 
      Height          =   315
      Left            =   23280
      TabIndex        =   24
      Top             =   0
      Width           =   1275
   End
   Begin VB.TextBox txtheaderint2 
      Height          =   315
      Left            =   14640
      TabIndex        =   17
      Top             =   360
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtheaderint1 
      Height          =   315
      Left            =   14640
      TabIndex        =   14
      Top             =   0
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   1080
      TabIndex        =   4
      Top             =   14220
      Width           =   24555
      Begin VB.CommandButton cmdManualBillAll 
         Caption         =   "Manually Mark All Billed"
         Height          =   315
         Left            =   8760
         TabIndex        =   104
         Top             =   0
         Width           =   2235
      End
      Begin VB.CommandButton cmdPrintCrate 
         Caption         =   "Print Crate Report"
         Height          =   315
         Left            =   18900
         TabIndex        =   101
         Top             =   0
         Width           =   1755
      End
      Begin VB.CommandButton cmdUpdateFlags 
         Caption         =   "Update Flags"
         Height          =   315
         Left            =   300
         TabIndex        =   97
         Top             =   0
         Width           =   1275
      End
      Begin VB.CommandButton cmdExportGrid 
         Caption         =   "Export Grid"
         Height          =   315
         Left            =   1680
         TabIndex        =   67
         Top             =   0
         Width           =   2235
      End
      Begin VB.CommandButton cmdUnbillAll 
         Caption         =   "Unmark Billing All"
         Height          =   315
         Left            =   4140
         TabIndex        =   37
         Top             =   0
         Width           =   2235
      End
      Begin VB.CommandButton cmdBillAll 
         Caption         =   "Bill All"
         Height          =   315
         Left            =   6480
         TabIndex        =   36
         Top             =   0
         Width           =   2235
      End
      Begin VB.CommandButton cmdUnbill 
         Caption         =   "Unmark as Billed"
         Height          =   315
         Left            =   11040
         TabIndex        =   20
         Top             =   0
         Visible         =   0   'False
         Width           =   2235
      End
      Begin VB.CommandButton cmdManualBillItem 
         Caption         =   "Manually Mark as Billed"
         Height          =   315
         Left            =   13440
         TabIndex        =   19
         Top             =   0
         Visible         =   0   'False
         Width           =   2235
      End
      Begin VB.CommandButton cmdBillItem 
         Caption         =   "Bill Item"
         Height          =   315
         Left            =   15780
         TabIndex        =   18
         Top             =   0
         Visible         =   0   'False
         Width           =   1755
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   23280
         TabIndex        =   8
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   22020
         TabIndex        =   7
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   20760
         TabIndex        =   6
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   17640
         TabIndex        =   5
         Top             =   0
         Width           =   1215
      End
   End
   Begin MSAdodcLib.Adodc adoItems 
      Height          =   330
      Left            =   120
      Top             =   3960
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdItems 
      Bindings        =   "frmTracker.frx":0906
      Height          =   8415
      Left            =   120
      TabIndex        =   3
      Top             =   3960
      Width           =   25545
      _Version        =   196617
      stylesets.count =   3
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTracker.frx":091D
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmTracker.frx":0939
      stylesets(2).Name=   "headerfield"
      stylesets(2).ForeColor=   0
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmTracker.frx":0955
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   291
      Columns.Count   =   84
      Columns(0).Width=   2117
      Columns(0).Caption=   "tracker_itemID"
      Columns(0).Name =   "tracker_itemID"
      Columns(0).DataField=   "tracker_itemID"
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).StyleSet=   "headerfield"
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "WorkFlowVariantID"
      Columns(2).Name =   "WorkFlowVariantID"
      Columns(2).DataField=   "WorkFlowVariantID"
      Columns(2).FieldLen=   256
      Columns(3).Width=   4233
      Columns(3).Caption=   "Workflow Variant"
      Columns(3).Name =   "WorkflowVariant"
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "headerfield"
      Columns(4).Width=   4233
      Columns(4).Caption=   "Ref"
      Columns(4).Name =   "itemreference"
      Columns(4).DataField=   "itemreference"
      Columns(4).FieldLen=   256
      Columns(4).Style=   1
      Columns(4).StyleSet=   "headerfield"
      Columns(5).Width=   4233
      Columns(5).Caption=   "Filename"
      Columns(5).Name =   "itemfilename"
      Columns(5).DataField=   "itemfilename"
      Columns(5).FieldLen=   256
      Columns(5).StyleSet=   "headerfield"
      Columns(6).Width=   979
      Columns(6).Caption=   "Dur"
      Columns(6).Name =   "duration"
      Columns(6).DataField=   "duration"
      Columns(6).FieldLen=   256
      Columns(6).StyleSet=   "headerfield"
      Columns(7).Width=   1879
      Columns(7).Caption=   "Length"
      Columns(7).Name =   "timecodeduration"
      Columns(7).DataField=   "timecodeduration"
      Columns(7).FieldLen=   256
      Columns(7).StyleSet=   "headerfield"
      Columns(8).Width=   1323
      Columns(8).Caption=   "Pending"
      Columns(8).Name =   "rejected"
      Columns(8).CaptionAlignment=   2
      Columns(8).DataField=   "rejected"
      Columns(8).FieldLen=   256
      Columns(8).Style=   2
      Columns(8).StyleSet=   "headerfield"
      Columns(9).Width=   1323
      Columns(9).Caption=   "GB Sent"
      Columns(9).Name =   "gbsent"
      Columns(9).DataField=   "gbsent"
      Columns(9).FieldLen=   256
      Columns(9).StyleSet=   "headerfield"
      Columns(10).Width=   2646
      Columns(10).Caption=   "Stored On"
      Columns(10).Name=   "Stored On"
      Columns(10).FieldLen=   256
      Columns(10).StyleSet=   "headerfield"
      Columns(11).Width=   1905
      Columns(11).Caption=   "ProxyID"
      Columns(11).Name=   "proxyID"
      Columns(11).DataField=   "proxyID"
      Columns(11).FieldLen=   256
      Columns(11).StyleSet=   "headerfield"
      Columns(12).Width=   1931
      Columns(12).Caption=   "Barcode"
      Columns(12).Name=   "barcode"
      Columns(12).DataField=   "barcode"
      Columns(12).FieldLen=   256
      Columns(12).StyleSet=   "headerfield"
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "Date Arrived"
      Columns(13).Name=   "datearrived"
      Columns(13).DataField=   "datearrived"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      Columns(13).StyleSet=   "stagefield"
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "Date Left"
      Columns(14).Name=   "dateleft"
      Columns(14).DataField=   "dateleft"
      Columns(14).DataType=   8
      Columns(14).FieldLen=   256
      Columns(14).StyleSet=   "stagefield"
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "Tape Location"
      Columns(15).Name=   "tapelocation"
      Columns(15).DataField=   "tapelocation"
      Columns(15).FieldLen=   256
      Columns(15).StyleSet=   "stagefield"
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "Tape Shelf"
      Columns(16).Name=   "tapeshelf"
      Columns(16).DataField=   "tapeshelf"
      Columns(16).FieldLen=   256
      Columns(16).StyleSet=   "stagefield"
      Columns(17).Width=   2831
      Columns(17).Caption=   "Customer"
      Columns(17).Name=   "Customer"
      Columns(17).FieldLen=   256
      Columns(17).StyleSet=   "headerfield"
      Columns(18).Width=   2090
      Columns(18).Caption=   "Deadline"
      Columns(18).Name=   "requiredby"
      Columns(18).DataField=   "requiredby"
      Columns(18).FieldLen=   256
      Columns(18).StyleSet=   "headerfield"
      Columns(19).Width=   1773
      Columns(19).Caption=   "jobID"
      Columns(19).Name=   "jobID"
      Columns(19).DataField=   "jobID"
      Columns(19).FieldLen=   256
      Columns(19).StyleSet=   "headerfield"
      Columns(20).Width=   2117
      Columns(20).Caption=   "Orig EventID"
      Columns(20).Name=   "originaleventID"
      Columns(20).DataField=   "originaleventID"
      Columns(20).FieldLen=   256
      Columns(20).StyleSet=   "headerfield"
      Columns(21).Width=   3200
      Columns(21).Caption=   "Transcode Seg Name"
      Columns(21).Name=   "transcodesegmentname"
      Columns(21).DataField=   "transcodesegmentname"
      Columns(21).FieldLen=   256
      Columns(21).StyleSet=   "headerfield"
      Columns(22).Width=   2117
      Columns(22).Caption=   "headertext1"
      Columns(22).Name=   "headertext1"
      Columns(22).DataField=   "headertext1"
      Columns(22).FieldLen=   256
      Columns(22).StyleSet=   "headerfield"
      Columns(23).Width=   2117
      Columns(23).Caption=   "headertext2"
      Columns(23).Name=   "headertext2"
      Columns(23).DataField=   "headertext2"
      Columns(23).FieldLen=   256
      Columns(23).StyleSet=   "headerfield"
      Columns(24).Width=   2117
      Columns(24).Caption=   "headertext3"
      Columns(24).Name=   "headertext3"
      Columns(24).DataField=   "headertext3"
      Columns(24).FieldLen=   256
      Columns(24).StyleSet=   "headerfield"
      Columns(25).Width=   2117
      Columns(25).Caption=   "headertext4"
      Columns(25).Name=   "headertext4"
      Columns(25).DataField=   "headertext4"
      Columns(25).FieldLen=   256
      Columns(25).StyleSet=   "headerfield"
      Columns(26).Width=   2117
      Columns(26).Caption=   "headertext5"
      Columns(26).Name=   "headertext5"
      Columns(26).DataField=   "headertext5"
      Columns(26).FieldLen=   256
      Columns(26).StyleSet=   "headerfield"
      Columns(27).Width=   2117
      Columns(27).Caption=   "headertext6"
      Columns(27).Name=   "headertext6"
      Columns(27).DataField=   "headertext6"
      Columns(27).FieldLen=   256
      Columns(27).StyleSet=   "headerfield"
      Columns(28).Width=   2117
      Columns(28).Caption=   "headertext7"
      Columns(28).Name=   "headertext7"
      Columns(28).DataField=   "headertext7"
      Columns(28).FieldLen=   256
      Columns(28).StyleSet=   "headerfield"
      Columns(29).Width=   2117
      Columns(29).Caption=   "headertext8"
      Columns(29).Name=   "headertext8"
      Columns(29).DataField=   "headertext8"
      Columns(29).FieldLen=   256
      Columns(29).StyleSet=   "headerfield"
      Columns(30).Width=   2117
      Columns(30).Caption=   "headertext9"
      Columns(30).Name=   "headertext9"
      Columns(30).DataField=   "headertext9"
      Columns(30).FieldLen=   256
      Columns(30).StyleSet=   "headerfield"
      Columns(31).Width=   2117
      Columns(31).Caption=   "headertext10"
      Columns(31).Name=   "headertext10"
      Columns(31).DataField=   "headertext10"
      Columns(31).FieldLen=   256
      Columns(31).StyleSet=   "headerfield"
      Columns(32).Width=   1773
      Columns(32).Caption=   "headerint1"
      Columns(32).Name=   "headerint1"
      Columns(32).DataField=   "headerint1"
      Columns(32).FieldLen=   256
      Columns(32).StyleSet=   "headerfield"
      Columns(33).Width=   1773
      Columns(33).Caption=   "headerint2"
      Columns(33).Name=   "headerint2"
      Columns(33).DataField=   "headerint2"
      Columns(33).FieldLen=   256
      Columns(33).StyleSet=   "headerfield"
      Columns(34).Width=   1773
      Columns(34).Caption=   "headerint3"
      Columns(34).Name=   "headerint3"
      Columns(34).DataField=   "headerint3"
      Columns(34).FieldLen=   256
      Columns(34).StyleSet=   "headerfield"
      Columns(35).Width=   1773
      Columns(35).Caption=   "headerint4"
      Columns(35).Name=   "headerint4"
      Columns(35).DataField=   "headerint4"
      Columns(35).FieldLen=   256
      Columns(35).StyleSet=   "headerfield"
      Columns(36).Width=   1773
      Columns(36).Caption=   "headerint5"
      Columns(36).Name=   "headerint5"
      Columns(36).DataField=   "headerint5"
      Columns(36).FieldLen=   256
      Columns(36).StyleSet=   "headerfield"
      Columns(37).Width=   3200
      Columns(37).Caption=   "headerdate1"
      Columns(37).Name=   "headerdate1"
      Columns(37).DataField=   "headerdate1"
      Columns(37).FieldLen=   256
      Columns(37).StyleSet=   "headerfield"
      Columns(38).Width=   3200
      Columns(38).Caption=   "headerdate2"
      Columns(38).Name=   "headerdate2"
      Columns(38).DataField=   "headerdate2"
      Columns(38).FieldLen=   256
      Columns(38).StyleSet=   "headerfield"
      Columns(39).Width=   3200
      Columns(39).Caption=   "headerdate3"
      Columns(39).Name=   "headerdate3"
      Columns(39).DataField=   "headerdate3"
      Columns(39).FieldLen=   256
      Columns(39).StyleSet=   "headerfield"
      Columns(40).Width=   3201
      Columns(40).Caption=   "headerdate4"
      Columns(40).Name=   "headerdate4"
      Columns(40).DataField=   "headerdate4"
      Columns(40).FieldLen=   256
      Columns(40).StyleSet=   "headerfield"
      Columns(41).Width=   3200
      Columns(41).Caption=   "headerdate5"
      Columns(41).Name=   "headerdate5"
      Columns(41).DataField=   "headerdate5"
      Columns(41).FieldLen=   256
      Columns(41).StyleSet=   "headerfield"
      Columns(42).Width=   873
      Columns(42).Caption=   "headerflag1"
      Columns(42).Name=   "headerflag1"
      Columns(42).CaptionAlignment=   2
      Columns(42).DataField=   "headerflag1"
      Columns(42).FieldLen=   256
      Columns(42).Style=   2
      Columns(42).StyleSet=   "headerfield"
      Columns(43).Width=   873
      Columns(43).Caption=   "headerflag2"
      Columns(43).Name=   "headerflag2"
      Columns(43).CaptionAlignment=   2
      Columns(43).DataField=   "headerflag2"
      Columns(43).FieldLen=   256
      Columns(43).Style=   2
      Columns(43).StyleSet=   "headerfield"
      Columns(44).Width=   873
      Columns(44).Caption=   "headerflag3"
      Columns(44).Name=   "headerflag3"
      Columns(44).CaptionAlignment=   2
      Columns(44).DataField=   "headerflag3"
      Columns(44).FieldLen=   256
      Columns(44).Style=   2
      Columns(44).StyleSet=   "headerfield"
      Columns(45).Width=   873
      Columns(45).Caption=   "headerflag4"
      Columns(45).Name=   "headerflag4"
      Columns(45).CaptionAlignment=   2
      Columns(45).DataField=   "headerflag4"
      Columns(45).FieldLen=   256
      Columns(45).Style=   2
      Columns(45).StyleSet=   "headerfield"
      Columns(46).Width=   873
      Columns(46).Caption=   "headerflag5"
      Columns(46).Name=   "headerflag5"
      Columns(46).CaptionAlignment=   2
      Columns(46).DataField=   "headerflag5"
      Columns(46).FieldLen=   256
      Columns(46).Style=   2
      Columns(46).StyleSet=   "headerfield"
      Columns(47).Width=   2646
      Columns(47).Caption=   "stagefield1"
      Columns(47).Name=   "stagefield1"
      Columns(47).DataField=   "stagefield1"
      Columns(47).DataType=   17
      Columns(47).FieldLen=   256
      Columns(47).Style=   1
      Columns(47).StyleSet=   "stagefield"
      Columns(48).Width=   2646
      Columns(48).Caption=   "stagefield2"
      Columns(48).Name=   "stagefield2"
      Columns(48).DataField=   "stagefield2"
      Columns(48).FieldLen=   256
      Columns(48).Style=   1
      Columns(48).StyleSet=   "stagefield"
      Columns(49).Width=   2646
      Columns(49).Caption=   "stagefield3"
      Columns(49).Name=   "stagefield3"
      Columns(49).DataField=   "stagefield3"
      Columns(49).FieldLen=   256
      Columns(49).Style=   1
      Columns(49).StyleSet=   "stagefield"
      Columns(50).Width=   2646
      Columns(50).Caption=   "stagefield4"
      Columns(50).Name=   "stagefield4"
      Columns(50).DataField=   "stagefield4"
      Columns(50).FieldLen=   256
      Columns(50).Style=   1
      Columns(50).StyleSet=   "stagefield"
      Columns(51).Width=   2646
      Columns(51).Caption=   "stagefield5"
      Columns(51).Name=   "stagefield5"
      Columns(51).DataField=   "stagefield5"
      Columns(51).FieldLen=   256
      Columns(51).Style=   1
      Columns(51).StyleSet=   "stagefield"
      Columns(52).Width=   2646
      Columns(52).Caption=   "stagefield6"
      Columns(52).Name=   "stagefield6"
      Columns(52).DataField=   "stagefield6"
      Columns(52).FieldLen=   256
      Columns(52).Style=   1
      Columns(52).StyleSet=   "stagefield"
      Columns(53).Width=   2646
      Columns(53).Caption=   "stagefield7"
      Columns(53).Name=   "stagefield7"
      Columns(53).DataField=   "stagefield7"
      Columns(53).FieldLen=   256
      Columns(53).Style=   1
      Columns(53).StyleSet=   "stagefield"
      Columns(54).Width=   2646
      Columns(54).Caption=   "stagefield8"
      Columns(54).Name=   "stagefield8"
      Columns(54).DataField=   "stagefield8"
      Columns(54).FieldLen=   256
      Columns(54).Style=   1
      Columns(54).StyleSet=   "stagefield"
      Columns(55).Width=   2646
      Columns(55).Caption=   "stagefield9"
      Columns(55).Name=   "stagefield9"
      Columns(55).DataField=   "stagefield9"
      Columns(55).FieldLen=   256
      Columns(55).Style=   1
      Columns(55).StyleSet=   "stagefield"
      Columns(56).Width=   2646
      Columns(56).Caption=   "stagefield10"
      Columns(56).Name=   "stagefield10"
      Columns(56).DataField=   "stagefield10"
      Columns(56).FieldLen=   256
      Columns(56).Style=   1
      Columns(56).StyleSet=   "stagefield"
      Columns(57).Width=   2646
      Columns(57).Caption=   "stagefield11"
      Columns(57).Name=   "stagefield11"
      Columns(57).DataField=   "stagefield11"
      Columns(57).FieldLen=   256
      Columns(57).Style=   1
      Columns(57).StyleSet=   "stagefield"
      Columns(58).Width=   2646
      Columns(58).Caption=   "stagefield12"
      Columns(58).Name=   "stagefield12"
      Columns(58).DataField=   "stagefield12"
      Columns(58).FieldLen=   256
      Columns(58).Style=   1
      Columns(58).StyleSet=   "stagefield"
      Columns(59).Width=   2646
      Columns(59).Caption=   "stagefield13"
      Columns(59).Name=   "stagefield13"
      Columns(59).DataField=   "stagefield13"
      Columns(59).FieldLen=   256
      Columns(59).Style=   1
      Columns(59).StyleSet=   "stagefield"
      Columns(60).Width=   2646
      Columns(60).Caption=   "stagefield14"
      Columns(60).Name=   "stagefield14"
      Columns(60).DataField=   "stagefield14"
      Columns(60).FieldLen=   256
      Columns(60).Style=   1
      Columns(60).StyleSet=   "stagefield"
      Columns(61).Width=   2646
      Columns(61).Caption=   "stagefield15"
      Columns(61).Name=   "stagefield15"
      Columns(61).DataField=   "stagefield15"
      Columns(61).FieldLen=   256
      Columns(61).Style=   1
      Columns(61).StyleSet=   "stagefield"
      Columns(62).Width=   2646
      Columns(62).Caption=   "stagefield16"
      Columns(62).Name=   "stagefield16"
      Columns(62).DataField=   "stagefield16"
      Columns(62).FieldLen=   256
      Columns(62).Style=   1
      Columns(62).StyleSet=   "stagefield"
      Columns(63).Width=   2646
      Columns(63).Caption=   "stagefield17"
      Columns(63).Name=   "stagefield17"
      Columns(63).DataField=   "stagefield17"
      Columns(63).FieldLen=   256
      Columns(63).Style=   1
      Columns(63).StyleSet=   "stagefield"
      Columns(64).Width=   2646
      Columns(64).Caption=   "stagefield18"
      Columns(64).Name=   "stagefield18"
      Columns(64).DataField=   "stagefield18"
      Columns(64).FieldLen=   256
      Columns(64).Style=   1
      Columns(64).StyleSet=   "stagefield"
      Columns(65).Width=   2646
      Columns(65).Caption=   "stagefield19"
      Columns(65).Name=   "stagefield19"
      Columns(65).DataField=   "stagefield19"
      Columns(65).FieldLen=   256
      Columns(65).Style=   1
      Columns(65).StyleSet=   "stagefield"
      Columns(66).Width=   2646
      Columns(66).Caption=   "stagefield20"
      Columns(66).Name=   "stagefield20"
      Columns(66).DataField=   "stagefield20"
      Columns(66).FieldLen=   256
      Columns(66).Style=   1
      Columns(66).StyleSet=   "stagefield"
      Columns(67).Width=   2646
      Columns(67).Caption=   "stagefield21"
      Columns(67).Name=   "stagefield21"
      Columns(67).DataField=   "stagefield21"
      Columns(67).FieldLen=   256
      Columns(67).Style=   1
      Columns(67).StyleSet=   "stagefield"
      Columns(68).Width=   2646
      Columns(68).Caption=   "stagefield22"
      Columns(68).Name=   "stagefield22"
      Columns(68).DataField=   "stagefield22"
      Columns(68).FieldLen=   256
      Columns(68).Style=   1
      Columns(68).StyleSet=   "stagefield"
      Columns(69).Width=   2646
      Columns(69).Caption=   "stagefield23"
      Columns(69).Name=   "stagefield23"
      Columns(69).DataField=   "stagefield23"
      Columns(69).FieldLen=   256
      Columns(69).Style=   1
      Columns(69).StyleSet=   "stagefield"
      Columns(70).Width=   2646
      Columns(70).Caption=   "stagefield24"
      Columns(70).Name=   "stagefield24"
      Columns(70).DataField=   "stagefield24"
      Columns(70).FieldLen=   256
      Columns(70).Style=   1
      Columns(70).StyleSet=   "stagefield"
      Columns(71).Width=   2646
      Columns(71).Caption=   "stagefield25"
      Columns(71).Name=   "stagefield25"
      Columns(71).DataField=   "stagefield25"
      Columns(71).FieldLen=   256
      Columns(71).Style=   1
      Columns(71).StyleSet=   "stagefield"
      Columns(72).Width=   1376
      Columns(72).Caption=   "conclusionfield1"
      Columns(72).Name=   "conclusionfield1"
      Columns(72).DataField=   "conclusionfield1"
      Columns(72).FieldLen=   256
      Columns(72).Locked=   -1  'True
      Columns(72).Style=   2
      Columns(72).StyleSet=   "conclusionfield"
      Columns(73).Width=   1376
      Columns(73).Caption=   "conclusionfield2"
      Columns(73).Name=   "conclusionfield2"
      Columns(73).DataField=   "conclusionfield2"
      Columns(73).FieldLen=   256
      Columns(73).Locked=   -1  'True
      Columns(73).Style=   2
      Columns(73).StyleSet=   "conclusionfield"
      Columns(74).Width=   1376
      Columns(74).Caption=   "conclusionfield3"
      Columns(74).Name=   "conclusionfield3"
      Columns(74).DataField=   "conclusionfield3"
      Columns(74).FieldLen=   256
      Columns(74).Locked=   -1  'True
      Columns(74).Style=   2
      Columns(74).StyleSet=   "conclusionfield"
      Columns(75).Width=   1376
      Columns(75).Caption=   "conclusionfield4"
      Columns(75).Name=   "conclusionfield4"
      Columns(75).DataField=   "conclusionfield4"
      Columns(75).FieldLen=   256
      Columns(75).Style=   2
      Columns(75).StyleSet=   "conclusionfield"
      Columns(76).Width=   1376
      Columns(76).Caption=   "conclusionfield5"
      Columns(76).Name=   "conclusionfield5"
      Columns(76).DataField=   "conclusionfield5"
      Columns(76).FieldLen=   256
      Columns(76).Style=   2
      Columns(76).StyleSet=   "conclusionfield"
      Columns(77).Width=   1376
      Columns(77).Caption=   "Finished?"
      Columns(77).Name=   "readytobill"
      Columns(77).DataField=   "readytobill"
      Columns(77).FieldLen=   256
      Columns(77).Locked=   -1  'True
      Columns(77).Style=   2
      Columns(77).StyleSet=   "conclusionfield"
      Columns(78).Width=   1376
      Columns(78).Caption=   "Billed?"
      Columns(78).Name=   "billed"
      Columns(78).DataField=   "billed"
      Columns(78).FieldLen=   256
      Columns(78).Locked=   -1  'True
      Columns(78).Style=   2
      Columns(78).StyleSet=   "conclusionfield"
      Columns(79).Width=   3200
      Columns(79).Visible=   0   'False
      Columns(79).Caption=   "portaluserID"
      Columns(79).Name=   "portaluserID"
      Columns(79).DataField=   "portaluserID"
      Columns(79).FieldLen=   256
      Columns(80).Width=   3200
      Columns(80).Caption=   "Created"
      Columns(80).Name=   "cdate"
      Columns(80).DataField=   "cdate"
      Columns(80).DataType=   17
      Columns(80).FieldLen=   256
      Columns(80).StyleSet=   "headerfield"
      Columns(81).Width=   3200
      Columns(81).Caption=   "Created By"
      Columns(81).Name=   "cuser"
      Columns(81).DataField=   "cuser"
      Columns(81).FieldLen=   256
      Columns(81).StyleSet=   "headerfield"
      Columns(82).Width=   3200
      Columns(82).Caption=   "Updated"
      Columns(82).Name=   "mdate"
      Columns(82).DataField=   "mdate"
      Columns(82).DataType=   17
      Columns(82).FieldLen=   256
      Columns(82).StyleSet=   "headerfield"
      Columns(83).Width=   3200
      Columns(83).Caption=   "Update By"
      Columns(83).Name=   "muser"
      Columns(83).DataField=   "muser"
      Columns(83).FieldLen=   256
      Columns(83).StyleSet=   "headerfield"
      _ExtentX        =   45059
      _ExtentY        =   14843
      _StockProps     =   79
      Caption         =   "Tracker Items"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CheckBox chkHideDemo 
      Alignment       =   1  'Right Justify
      Caption         =   "Hide Demo"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Tag             =   "NOCLEAR"
      Top             =   420
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   1860
      TabIndex        =   1
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The company this job is for"
      Top             =   0
      Width           =   2835
      DataFieldList   =   "name"
      MaxDropDownItems=   35
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   5001
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin MSAdodcLib.Adodc adoComments 
      Height          =   330
      Left            =   1920
      Top             =   13260
      Visible         =   0   'False
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoComments"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdComments 
      Bindings        =   "frmTracker.frx":0971
      Height          =   1875
      Left            =   120
      TabIndex        =   10
      Top             =   12180
      Width           =   12015
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12582847
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   5
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "trackerhistoryID"
      Columns(0).Name =   "tracker_commentID"
      Columns(0).DataField=   "tracker_commentID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "trackerprogramID"
      Columns(1).Name =   "tracker_itemID"
      Columns(1).DataField=   "tracker_itemID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   13070
      Columns(2).Caption=   "Comments"
      Columns(2).Name =   "comment"
      Columns(2).DataField=   "comment"
      Columns(2).FieldLen=   255
      Columns(3).Width=   3360
      Columns(3).Caption=   "Date"
      Columns(3).Name =   "cdate"
      Columns(3).DataField=   "cdate"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).Style=   1
      Columns(4).Width=   3519
      Columns(4).Caption=   "Entered By"
      Columns(4).Name =   "cuser"
      Columns(4).DataField=   "cuser"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      UseDefaults     =   0   'False
      _ExtentX        =   21193
      _ExtentY        =   3307
      _StockProps     =   79
      Caption         =   "Tracker Comments"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbOrderBy 
      Height          =   315
      Left            =   1860
      TabIndex        =   42
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The company this job is for"
      Top             =   720
      Width           =   2835
      DataFieldList   =   "label"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "fieldname"
      Columns(0).Name =   "fieldname"
      Columns(0).DataField=   "fieldname"
      Columns(0).FieldLen=   256
      Columns(1).Width=   4498
      Columns(1).Caption=   "label"
      Columns(1).Name =   "label"
      Columns(1).DataField=   "label"
      Columns(1).FieldLen=   256
      _ExtentX        =   5001
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "label"
   End
   Begin MSAdodcLib.Adodc adoCustomers 
      Height          =   330
      Left            =   1800
      Top             =   2220
      Visible         =   0   'False
      Width           =   2805
      _ExtentX        =   4948
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCustomers"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker datHeaderDate1 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   14640
      TabIndex        =   54
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   1800
      Visible         =   0   'False
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   120979457
      CurrentDate     =   37870
   End
   Begin MSComCtl2.DTPicker datHeaderDate2 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   14640
      TabIndex        =   55
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   2160
      Visible         =   0   'False
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   120979457
      CurrentDate     =   37870
   End
   Begin MSComCtl2.DTPicker datHeaderDate3 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   14640
      TabIndex        =   56
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   2520
      Visible         =   0   'False
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   120979457
      CurrentDate     =   37870
   End
   Begin MSComCtl2.DTPicker datHeaderDate4 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   14640
      TabIndex        =   57
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   2880
      Visible         =   0   'False
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   120979457
      CurrentDate     =   37870
   End
   Begin MSComCtl2.DTPicker datHeaderDate5 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   14640
      TabIndex        =   58
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   3240
      Visible         =   0   'False
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   120979457
      CurrentDate     =   37870
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   315
      Left            =   10140
      TabIndex        =   98
      Top             =   3600
      Visible         =   0   'False
      Width           =   5955
      _ExtentX        =   10504
      _ExtentY        =   556
      _Version        =   393216
      Appearance      =   1
   End
   Begin MSAdodcLib.Adodc adoWorkflowVariant 
      Height          =   330
      Left            =   1800
      Top             =   2580
      Visible         =   0   'False
      Width           =   2805
      _ExtentX        =   4948
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoWorkflowVariant"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label lblCaption 
      Caption         =   "Direct SQL clause AND (this text)"
      Height          =   255
      Index           =   27
      Left            =   19440
      TabIndex        =   100
      Top             =   3660
      Width           =   2415
   End
   Begin VB.Label lblCaption 
      Caption         =   "Search Barcode"
      Height          =   255
      Index           =   26
      Left            =   240
      TabIndex        =   86
      Top             =   1500
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   25
      Left            =   4860
      TabIndex        =   81
      Top             =   3300
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      Caption         =   "Search JobID"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   24
      Left            =   4860
      TabIndex        =   80
      Top             =   3660
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.Label lblInstructions 
      ForeColor       =   &H000000FF&
      Height          =   855
      Left            =   21900
      TabIndex        =   69
      Top             =   2340
      Width           =   6615
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   23
      Left            =   11640
      TabIndex        =   63
      Top             =   3300
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   22
      Left            =   11640
      TabIndex        =   62
      Top             =   2940
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   21
      Left            =   11640
      TabIndex        =   61
      Top             =   2580
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   20
      Left            =   11640
      TabIndex        =   60
      Top             =   2220
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   19
      Left            =   11640
      TabIndex        =   59
      Top             =   1860
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   18
      Left            =   4860
      TabIndex        =   48
      Top             =   2940
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   17
      Left            =   4860
      TabIndex        =   47
      Top             =   2580
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   16
      Left            =   4860
      TabIndex        =   46
      Top             =   2220
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   15
      Left            =   4860
      TabIndex        =   45
      Top             =   1860
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.Label lblLastTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   255
      Left            =   240
      TabIndex        =   43
      Top             =   2220
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Display Sort Order"
      Height          =   255
      Index           =   14
      Left            =   240
      TabIndex        =   41
      Top             =   780
      Width           =   1575
   End
   Begin VB.Label lblCaption 
      Caption         =   "Search Reference"
      Height          =   255
      Index           =   13
      Left            =   240
      TabIndex        =   39
      Top             =   1140
      Width           =   1455
   End
   Begin VB.Label lblCaption 
      Caption         =   "Total Size (GB)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   12
      Left            =   21840
      TabIndex        =   33
      Top             =   420
      Width           =   1335
   End
   Begin VB.Label lblCaption 
      Caption         =   "Total Duration"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   11
      Left            =   21840
      TabIndex        =   31
      Top             =   60
      Width           =   1335
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   9
      Left            =   11640
      TabIndex        =   30
      Top             =   1500
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   8
      Left            =   11640
      TabIndex        =   29
      Top             =   1140
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   7
      Left            =   11640
      TabIndex        =   25
      Top             =   780
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   11640
      TabIndex        =   23
      Top             =   420
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   11640
      TabIndex        =   22
      Top             =   60
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   4860
      TabIndex        =   21
      Top             =   1500
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   4860
      TabIndex        =   16
      Top             =   1140
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   4860
      TabIndex        =   15
      Top             =   780
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   4860
      TabIndex        =   13
      Top             =   420
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   4860
      TabIndex        =   12
      Top             =   60
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.Label lblTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   255
      Left            =   240
      TabIndex        =   11
      Top             =   1860
      Width           =   975
   End
   Begin VB.Label lblCompanyID 
      Height          =   195
      Left            =   2160
      TabIndex        =   9
      Top             =   3480
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   10
      Left            =   270
      TabIndex        =   2
      Top             =   60
      Width           =   915
   End
End
Attribute VB_Name = "frmTracker"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strSearch As String
Dim m_strOrderby As String
Dim m_blnDelete As Boolean
Dim m_blnInsert As Boolean

Const NumberOfConclusionFields = 5

Dim m_blnStagefieldConc(NumberOfConclusionFields, 25) As Boolean
Dim m_blnStagefieldNOTConc(NumberOfConclusionFields, 25) As Boolean
Dim m_blnStagefieldConclusion(25) As Boolean

Private Sub HideAllColumns()

'grdItems.Columns("cdate").Visible = False
'grdItems.Columns("cuser").Visible = False
'grdItems.Columns("mdate").Visible = False
'grdItems.Columns("muser").Visible = False
grdItems.Columns("tracker_itemID").Visible = False
grdItems.Columns("WorkflowVariant").Visible = False
grdItems.Columns("originaleventID").Visible = False
grdItems.Columns("transcodesegmentname").Visible = False
grdItems.Columns("stagefield1").Visible = False
grdItems.Columns("stagefield2").Visible = False
grdItems.Columns("stagefield3").Visible = False
grdItems.Columns("stagefield4").Visible = False
grdItems.Columns("stagefield5").Visible = False
grdItems.Columns("stagefield6").Visible = False
grdItems.Columns("stagefield7").Visible = False
grdItems.Columns("stagefield8").Visible = False
grdItems.Columns("stagefield9").Visible = False
grdItems.Columns("stagefield10").Visible = False
grdItems.Columns("stagefield11").Visible = False
grdItems.Columns("stagefield12").Visible = False
grdItems.Columns("stagefield13").Visible = False
grdItems.Columns("stagefield14").Visible = False
grdItems.Columns("stagefield15").Visible = False
grdItems.Columns("stagefield16").Visible = False
grdItems.Columns("stagefield17").Visible = False
grdItems.Columns("stagefield18").Visible = False
grdItems.Columns("stagefield19").Visible = False
grdItems.Columns("stagefield20").Visible = False
grdItems.Columns("stagefield21").Visible = False
grdItems.Columns("stagefield22").Visible = False
grdItems.Columns("stagefield23").Visible = False
grdItems.Columns("stagefield24").Visible = False
grdItems.Columns("stagefield25").Visible = False
grdItems.Columns("conclusionfield1").Visible = False
grdItems.Columns("conclusionfield2").Visible = False
grdItems.Columns("conclusionfield3").Visible = False
grdItems.Columns("conclusionfield4").Visible = False
grdItems.Columns("conclusionfield5").Visible = False
grdItems.Columns("headertext1").Visible = False
grdItems.Columns("headertext2").Visible = False
grdItems.Columns("headertext3").Visible = False
grdItems.Columns("headertext4").Visible = False
grdItems.Columns("headertext5").Visible = False
grdItems.Columns("headertext6").Visible = False
grdItems.Columns("headertext7").Visible = False
grdItems.Columns("headertext8").Visible = False
grdItems.Columns("headertext9").Visible = False
grdItems.Columns("headertext10").Visible = False
grdItems.Columns("headerint1").Visible = False
grdItems.Columns("headerint2").Visible = False
grdItems.Columns("headerint3").Visible = False
grdItems.Columns("headerint4").Visible = False
grdItems.Columns("headerint5").Visible = False
grdItems.Columns("headerdate1").Visible = False
grdItems.Columns("headerdate2").Visible = False
grdItems.Columns("headerdate3").Visible = False
grdItems.Columns("headerdate4").Visible = False
grdItems.Columns("headerdate5").Visible = False
grdItems.Columns("headerflag1").Visible = False
grdItems.Columns("headerflag2").Visible = False
grdItems.Columns("headerflag3").Visible = False
grdItems.Columns("headerflag4").Visible = False
grdItems.Columns("headerflag5").Visible = False
grdItems.Columns("Customer").Visible = False
grdItems.Columns("requiredby").Visible = False
grdItems.Columns("proxyID").Visible = False
grdItems.Columns("jobID").Visible = False
txtJobID.Visible = False
lblCaption(24).Visible = False
lblCaption(0).Visible = False
lblCaption(1).Visible = False
lblCaption(2).Visible = False
lblCaption(3).Visible = False
lblCaption(4).Visible = False
lblCaption(5).Visible = False
lblCaption(6).Visible = False
lblCaption(7).Visible = False
lblCaption(8).Visible = False
lblCaption(9).Visible = False
lblCaption(15).Visible = False
lblCaption(16).Visible = False
lblCaption(17).Visible = False
lblCaption(18).Visible = False
lblCaption(19).Visible = False
lblCaption(20).Visible = False
lblCaption(21).Visible = False
lblCaption(22).Visible = False
lblCaption(23).Visible = False
txtheadertext1.Visible = False
txtheadertext2.Visible = False
txtheadertext3.Visible = False
txtheadertext4.Visible = False
txtheadertext5.Visible = False
txtheadertext6.Visible = False
txtheadertext7.Visible = False
txtheadertext8.Visible = False
txtheadertext9.Visible = False
txtheaderint1.Visible = False
txtheaderint2.Visible = False
txtheaderint3.Visible = False
txtheaderint4.Visible = False
txtheaderint5.Visible = False
chkHeaderFlag1.Visible = False
chkHeaderFlag2.Visible = False
chkHeaderFlag3.Visible = False
chkHeaderFlag4.Visible = False
chkHeaderFlag5.Visible = False
datHeaderDate1.Visible = False
datHeaderDate2.Visible = False
datHeaderDate3.Visible = False
datHeaderDate4.Visible = False
datHeaderDate5.Visible = False
optComplete(5).Visible = False
optComplete(6).Visible = False
optComplete(7).Visible = False
optComplete(8).Visible = False
optComplete(9).Visible = False
optComplete(10).Visible = False
optComplete(11).Visible = False

Dim i As Integer, J As Integer

For i = 1 To NumberOfConclusionFields
    For J = 1 To 25
        m_blnStagefieldConc(i, J) = False
        m_blnStagefieldNOTConc(i, J) = False
    Next
    m_blnStagefieldConclusion(i) = False
Next

For i = 1 To 25
    m_blnStagefieldConclusion(i) = False
Next

End Sub

Private Sub HideMoreColumns()

grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("gbsent").Visible = False

End Sub

Private Sub ShowMoreColumns()

grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = True
grdItems.Columns("timecodeduration").Visible = True
grdItems.Columns("gbsent").Visible = True

End Sub

Private Sub HideTapeColumns()

grdItems.Columns("barcode").Visible = False
grdItems.Columns("datearrived").Visible = False
grdItems.Columns("dateleft").Visible = False
grdItems.Columns("tapelocation").Visible = False
grdItems.Columns("tapeshelf").Visible = False
optComplete(9).Visible = False
optComplete(12).Visible = False
txtBarcode.Visible = False
lblCaption(26).Visible = False

End Sub

Private Sub ShowTapeColumns()

grdItems.Columns("barcode").Visible = True
'grdItems.Columns("datearrived").Visible = True
'grdItems.Columns("dateleft").Visible = True
'grdItems.Columns("tapelocation").Visible = True
'grdItems.Columns("tapeshelf").Visible = True
optComplete(9).Visible = True
optComplete(12).Visible = True
txtBarcode.Visible = True
lblCaption(26).Visible = True

End Sub

Private Sub BillBBCStudiosEnglish()

Dim l_strChargeCode As String, l_rstTrackerChargeCode As ADODB.Recordset, l_strJobLine As String, l_lngJobID As Long, l_lngDuration As Long, l_lngMinuteBilling As Long
Dim l_lngQuantity As Long, l_strCode As String, l_lngFactor As Long, l_strLastComment As String, l_strLastCommentCuser As String, l_datLastCommentDate As Date, l_blnFirstLine As Boolean
Dim l_strWorkflowDescription As String
Dim l_lngStageFieldCount As Long

If frmJob.txtJobID.Text <> "" Then
    'A job is loaded - now check that it isn't locked.
    l_lngJobID = Val(frmJob.txtJobID.Text)
    Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND minutebilling in ('M', 'I');", g_strExecuteError)
    CheckForSQLError
    If l_rstTrackerChargeCode.RecordCount > 0 Then
        If Val(grdItems.Columns("duration").Text) = 0 Then
            MsgBox "Zero Duration item found when billing duration based charge codes - item cannot be billed.", vbCritical
            l_rstTrackerChargeCode.Close
            Set l_rstTrackerChargeCode = Nothing
            Exit Sub
        End If
    End If
    l_rstTrackerChargeCode.Close

    If (frmJob.txtStatus.Text = "Submitted" Or frmJob.txtStatus.Text = "Confirmed") And frmJob.lblCompanyID.Caption = lblCompanyID.Caption Then
        'The Job is valid - go ahead and create Job lines for this item.
        l_lngQuantity = 0
        l_strJobLine = "Item: " & grdItems.Columns("itemfilename").Text & ", Format: " & grdItems.Columns("headertext1").Text & ", Ingested: " & grdItems.Columns("stagefield1").Text
        l_blnFirstLine = True

        For l_lngStageFieldCount = 1 To 25
        
            If grdItems.Columns("stagefield" & l_lngStageFieldCount).Text <> "" And LCase(grdItems.Columns("stagefield" & l_lngStageFieldCount).Text) <> "n/a" Then
                l_strCode = "I"
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield" & l_lngStageFieldCount & "';", g_strExecuteError)
                CheckForSQLError
                If l_rstTrackerChargeCode.RecordCount > 0 Then
                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                    l_strCode = "I"
                    If l_lngDuration = 0 Then MsgBox "Zero Duration Inclusive item - this is probably wrong.", vbCritical
                    If l_lngQuantity = 0 Then l_lngQuantity = 1
                End If
                l_rstTrackerChargeCode.Close
                
                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0
            
            End If
            frmJob.adoJobDetail.Refresh
        Next
        
        frmJob.adoJobDetail.Refresh
        grdItems.Columns("billed").Text = -1
        grdItems.Columns("jobID").Text = l_lngJobID
        grdItems.Update
        cmdBillItem.Visible = False
        cmdManualBillItem.Visible = False
    Else
        MsgBox "The current Job must belong to the correct company and be confirmed in order to bill an item.", vbCritical, "Cannot Bill Item"
    End If
Else
    MsgBox "A Job must be created and loaded into the Jobs form in order to bill an item.", vbCritical, "Cannot Bill Item"
End If

End Sub
Private Sub BillDisneyDestruction()

Dim l_strChargeCode As String, l_rstTrackerChargeCode As ADODB.Recordset, l_strJobLine As String, l_lngJobID As Long, l_lngDuration As Long, l_lngMinuteBilling As Long
Dim l_lngQuantity As Long, l_strCode As String, l_lngFactor As Long, l_strLastComment As String, l_strLastCommentCuser As String, l_datLastCommentDate As Date, l_blnFirstLine As Boolean
Dim l_strWorkflowDescription As String
Dim l_lngStageFieldCount As Long

If frmJob.txtJobID.Text <> "" Then
    'A job is loaded - now check that it isn't locked.
    l_lngJobID = Val(frmJob.txtJobID.Text)
    Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND minutebilling in ('M', 'I');", g_strExecuteError)
    CheckForSQLError
    If l_rstTrackerChargeCode.RecordCount > 0 Then
        If Val(grdItems.Columns("duration").Text) = 0 Then
            MsgBox "Zero Duration item found when billing duration based charge codes - item cannot be billed.", vbCritical
            l_rstTrackerChargeCode.Close
            Set l_rstTrackerChargeCode = Nothing
            Exit Sub
        End If
    End If
    l_rstTrackerChargeCode.Close

    If (frmJob.txtStatus.Text = "Submitted" Or frmJob.txtStatus.Text = "Confirmed") And frmJob.lblCompanyID.Caption = lblCompanyID.Caption Then
        'The Job is valid - go ahead and create Job lines for this item.
        l_lngQuantity = 0
        l_strJobLine = "El: " & grdItems.Columns("headetint1").Text & ", Abbrev: " & grdItems.Columns("headertext2").Text & ", Lang: " & grdItems.Columns("headertext4").Text & " " & _
        grdItems.Columns("headertext5").Text & " " & grdItems.Columns("hedaertext6").Text
'        If adoComments.Recordset.RecordCount > 0 Then
'            adoComments.Recordset.MoveLast
'            l_strLastCommentCuser = adoComments.Recordset("cuser")
'            l_datLastCommentDate = adoComments.Recordset("cdate")
'            l_strLastComment = adoComments.Recordset("comment")
'        Else
'            l_strLastCommentCuser = ""
'            l_datLastCommentDate = 0
'            l_strLastComment = ""
'        End If
        l_blnFirstLine = True

        For l_lngStageFieldCount = 1 To 25
        
            If grdItems.Columns("stagefield" & l_lngStageFieldCount).Text <> "" And LCase(grdItems.Columns("stagefield" & l_lngStageFieldCount).Text) <> "n/a" Then
                l_strCode = "O"
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                'System to work out which code to use (based on duration of items on disc)
                'Take the duration of the tracker line, divide by the number of discs to get the duration of the per disc, then from that work out which code.
                'This is unique enough that we are going to hard set the codes here.
                l_lngQuantity = Val(Trim(" " & grdItems.Columns("headerint2").Text))
                
                l_strChargeCode = "Somthing"
                
                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0
            
            End If
            frmJob.adoJobDetail.Refresh
        Next
        
        frmJob.adoJobDetail.Refresh
        grdItems.Columns("billed").Text = -1
        grdItems.Columns("jobID").Text = l_lngJobID
        grdItems.Update
        cmdBillItem.Visible = False
        cmdManualBillItem.Visible = False
    Else
        MsgBox "The current Job must belong to the correct company and be confirmed in order to bill an item.", vbCritical, "Cannot Bill Item"
    End If
Else
    MsgBox "A Job must be created and loaded into the Jobs form in order to bill an item.", vbCritical, "Cannot Bill Item"
End If

End Sub

Private Sub chkHideDemo_Click()

Dim l_strSQL As String

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/tracker %' and companyID > 100 AND system_active = 1 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/tracker %' AND system_active = 1 ORDER BY name;"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

cmdSearch.Value = True

End Sub

Private Sub chkHideHeaderFields_Click()

Dim l_lngColumnOffset As Long
Dim l_rstChoices As ADODB.Recordset, l_blnWide As Boolean

If chkHideHeaderFields.Value <> 0 Then

    grdItems.Columns("headertext1").Visible = False
    grdItems.Columns("headertext2").Visible = False
    grdItems.Columns("headertext3").Visible = False
    grdItems.Columns("headertext4").Visible = False
    grdItems.Columns("headertext5").Visible = False
    grdItems.Columns("headertext6").Visible = False
    grdItems.Columns("headertext7").Visible = False
    grdItems.Columns("headertext8").Visible = False
    grdItems.Columns("headertext9").Visible = False
    grdItems.Columns("headertext10").Visible = False
    
    grdItems.Columns("headerint1").Visible = False
    grdItems.Columns("headerint2").Visible = False
    grdItems.Columns("headerint3").Visible = False
    grdItems.Columns("headerint4").Visible = False
    grdItems.Columns("headerint5").Visible = False

    grdItems.Columns("headerdate1").Visible = False
    grdItems.Columns("headerdate2").Visible = False
    grdItems.Columns("headerdate3").Visible = False
    grdItems.Columns("headerdate4").Visible = False
    grdItems.Columns("headerdate5").Visible = False

Else

    If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
        l_blnWide = True
    Else
        l_blnWide = False
    End If
    
    l_lngColumnOffset = 23

    Set l_rstChoices = ExecuteSQL("SELECT * FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " ORDER BY CASE WHEN field_order IS NULL THEN 0 ELSE 1 END, field_order;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstChoices.RecordCount > 0 Then
        l_rstChoices.MoveFirst
        Do While Not l_rstChoices.EOF
            Select Case l_rstChoices("itemfield")
                Case "requiredby"
                    grdItems.Columns("requiredby").Visible = True
                    grdItems.Columns("requiredby").Caption = l_rstChoices("itemheading")
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("requiredby").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("requiredby").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headertext1"
                    grdItems.Columns("headertext1").Visible = True
                    grdItems.Columns("headertext1").Caption = l_rstChoices("itemheading")
                    lblCaption(0).Caption = "Search " & l_rstChoices("itemheading")
                    lblCaption(0).Visible = True
                    txtheadertext1.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headertext1").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext1").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headertext2"
                    grdItems.Columns("headertext2").Visible = True
                    grdItems.Columns("headertext2").Caption = l_rstChoices("itemheading")
                    lblCaption(1).Visible = True
                    lblCaption(1).Caption = "Search " & l_rstChoices("itemheading")
                    txtheadertext2.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headertext2").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext2").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headertext3"
                    grdItems.Columns("headertext3").Visible = True
                    grdItems.Columns("headertext3").Caption = l_rstChoices("itemheading")
                    lblCaption(2).Visible = True
                    lblCaption(2).Caption = "Search " & l_rstChoices("itemheading")
                    txtheadertext3.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headertext3").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext3").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headertext4"
                    grdItems.Columns("headertext4").Visible = True
                    grdItems.Columns("headertext4").Caption = l_rstChoices("itemheading")
                    lblCaption(3).Visible = True
                    lblCaption(3).Caption = "Search " & l_rstChoices("itemheading")
                    txtheadertext4.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headertext4").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext4").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headertext5"
                    grdItems.Columns("headertext5").Visible = True
                    grdItems.Columns("headertext5").Caption = l_rstChoices("itemheading")
                    lblCaption(4).Visible = True
                    lblCaption(4).Caption = "Search " & l_rstChoices("itemheading")
                    txtheadertext5.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headertext5").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext5").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headertext6"
                    grdItems.Columns("headertext6").Visible = True
                    grdItems.Columns("headertext6").Caption = l_rstChoices("itemheading")
                    lblCaption(15).Visible = True
                    lblCaption(15).Caption = "Search " & l_rstChoices("itemheading")
                    txtheadertext6.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headertext6").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext6").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headertext7"
                    grdItems.Columns("headertext7").Visible = True
                    grdItems.Columns("headertext7").Caption = l_rstChoices("itemheading")
                    lblCaption(16).Visible = True
                    lblCaption(16).Caption = "Search " & l_rstChoices("itemheading")
                    txtheadertext7.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headertext7").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext7").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headertext8"
                    grdItems.Columns("headertext8").Visible = True
                    grdItems.Columns("headertext8").Caption = l_rstChoices("itemheading")
                    lblCaption(17).Visible = True
                    lblCaption(17).Caption = "Search " & l_rstChoices("itemheading")
                    txtheadertext8.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headertext8").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext8").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headertext9"
                    grdItems.Columns("headertext9").Visible = True
                    grdItems.Columns("headertext9").Caption = l_rstChoices("itemheading")
                    lblCaption(18).Visible = True
                    lblCaption(18).Caption = "Search " & l_rstChoices("itemheading")
                    txtheadertext9.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headertext9").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext9").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headertext10"
                    grdItems.Columns("headertext10").Visible = True
                    grdItems.Columns("headertext10").Caption = l_rstChoices("itemheading")
                    lblCaption(25).Visible = True
                    lblCaption(25).Caption = "Search " & l_rstChoices("itemheading")
                    txtheadertext10.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headertext10").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext10").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headerint1"
                    grdItems.Columns("headerint1").Visible = True
                    grdItems.Columns("headerint1").Caption = l_rstChoices("itemheading")
                    lblCaption(5).Visible = True
                    lblCaption(5).Caption = "Search " & l_rstChoices("itemheading")
                    txtheaderint1.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headerint1").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerint1").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headerint2"
                    grdItems.Columns("headerint2").Visible = True
                    grdItems.Columns("headerint2").Caption = l_rstChoices("itemheading")
                    lblCaption(6).Visible = True
                    lblCaption(6).Caption = "Search " & l_rstChoices("itemheading")
                    txtheaderint2.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headerint2").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerint2").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headerint3"
                    grdItems.Columns("headerint3").Visible = True
                    grdItems.Columns("headerint3").Caption = l_rstChoices("itemheading")
                    lblCaption(7).Visible = True
                    lblCaption(7).Caption = "Search " & l_rstChoices("itemheading")
                    txtheaderint3.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headerint3").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerint3").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headerint4"
                    grdItems.Columns("headerint4").Visible = True
                    grdItems.Columns("headerint4").Caption = l_rstChoices("itemheading")
                    lblCaption(8).Visible = True
                    lblCaption(8).Caption = "Search " & l_rstChoices("itemheading")
                    txtheaderint4.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headerint4").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerint4").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headerint5"
                    grdItems.Columns("headerint5").Visible = True
                    grdItems.Columns("headerint5").Caption = l_rstChoices("itemheading")
                    lblCaption(9).Visible = True
                    lblCaption(9).Caption = "Search " & l_rstChoices("itemheading")
                    txtheaderint5.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headerint5").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerint5").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headerdate1"
                    grdItems.Columns("headerdate1").Visible = True
                    grdItems.Columns("headerdate1").Caption = l_rstChoices("itemheading")
                    lblCaption(19).Visible = True
                    lblCaption(19).Caption = "Search " & l_rstChoices("itemheading")
                    datHeaderDate1.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headerdate1").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerdate1").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headerdate2"
                    grdItems.Columns("headerdate2").Visible = True
                    grdItems.Columns("headerdate2").Caption = l_rstChoices("itemheading")
                    lblCaption(20).Visible = True
                    lblCaption(20).Caption = "Search " & l_rstChoices("itemheading")
                    datHeaderDate2.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headerdate2").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerdate2").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headerdate3"
                    grdItems.Columns("headerdate3").Visible = True
                    grdItems.Columns("headerdate3").Caption = l_rstChoices("itemheading")
                    lblCaption(21).Visible = True
                    lblCaption(21).Caption = "Search " & l_rstChoices("itemheading")
                    datHeaderDate3.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headerdate3").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerdate3").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headerdate4"
                    grdItems.Columns("headerdate4").Visible = True
                    grdItems.Columns("headerdate4").Caption = l_rstChoices("itemheading")
                    lblCaption(22).Visible = True
                    lblCaption(22).Caption = "Search " & l_rstChoices("itemheading")
                    datHeaderDate4.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headerdate4").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerdate4").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headerdate5"
                    grdItems.Columns("headerdate5").Visible = True
                    grdItems.Columns("headerdate5").Caption = l_rstChoices("itemheading")
                    lblCaption(23).Visible = True
                    lblCaption(23).Caption = "Search " & l_rstChoices("itemheading")
                    datHeaderDate5.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headerdate5").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerdate5").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headerflag1"
                    grdItems.Columns("headerflag1").Visible = True
                    grdItems.Columns("headerflag1").Caption = l_rstChoices("itemheading")
                    chkHeaderFlag1.Caption = "Search " & l_rstChoices("itemheading")
                    chkHeaderFlag1.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headerflag1").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerflag1").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headerflag2"
                    grdItems.Columns("headerflag2").Visible = True
                    grdItems.Columns("headerflag2").Caption = l_rstChoices("itemheading")
                    chkHeaderFlag2.Caption = "Search " & l_rstChoices("itemheading")
                    chkHeaderFlag2.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headerflag2").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerflag2").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headerflag3"
                    grdItems.Columns("headerflag3").Visible = True
                    grdItems.Columns("headerflag3").Caption = l_rstChoices("itemheading")
                    chkHeaderFlag3.Caption = "Search " & l_rstChoices("itemheading")
                    chkHeaderFlag3.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headerflag3").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerflag3").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headerflag4"
                    grdItems.Columns("headerflag4").Visible = True
                    grdItems.Columns("headerflag4").Caption = l_rstChoices("itemheading")
                    chkHeaderFlag4.Caption = "Search " & l_rstChoices("itemheading")
                    chkHeaderFlag4.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headerflag4").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerflag4").Position = l_rstChoices("field_order") + l_lngColumnOffset
                Case "headerflag5"
                    grdItems.Columns("headerflag5").Visible = True
                    grdItems.Columns("headerflag5").Caption = l_rstChoices("itemheading")
                    chkHeaderFlag5.Caption = "Search " & l_rstChoices("itemheading")
                    chkHeaderFlag5.Visible = True
                    If Val(l_rstChoices("itemwidth")) <> 0 Then
                        grdItems.Columns("headerflag5").Width = Val(l_rstChoices("itemwidth"))
                    End If
                    If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerflag5").Position = l_rstChoices("field_order") + l_lngColumnOffset
            End Select
            l_rstChoices.MoveNext
        Loop
    End If
End If

End Sub

Private Sub cmbCompany_Click()

Dim l_lngColumnOffset As Long

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text
m_strSearch = " WHERE companyID = " & lblCompanyID.Caption
m_strOrderby = " ORDER BY headertext1, headerint1, headerint2, headerint3, headerint4, headerint5"

HideAllColumns

'If Val(lblCompanyID.Caption) = 1597 Then
'    cmdCollectBBCStudioEnglishWork.Visible = True
'Else
'    cmdCollectBBCStudioEnglishWork.Visible = False
'End If
'
l_lngColumnOffset = 23

'Check if this is a 'not file' use of the tracker and hide the file related core fields if it is.
If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/trackernotfile") > 0 Then
    HideMoreColumns
Else
    ShowMoreColumns
End If
If InStr(GetData("company", "cetaclientcode", "companyID", cmbCompany.Columns("companyID").Text), "/trackershowtrackerID") > 0 Then
    grdItems.Columns("tracker_itemID").Visible = True
Else
    grdItems.Columns("tracker_itemID").Visible = False
End If
If InStr(GetData("company", "cetaclientcode", "companyID", cmbCompany.Columns("companyID").Text), "/trackershowduration") > 0 Then
    grdItems.Columns("duration").Visible = True
Else
    grdItems.Columns("duration").Visible = False
End If
If InStr(GetData("company", "cetaclientcode", "companyID", cmbCompany.Columns("companyID").Text), "/trackerfilename") > 0 Then
    grdItems.Columns("itemfilename").Visible = True
Else
    grdItems.Columns("itemfilename").Visible = False
End If

If InStr(GetData("company", "cetaclientcode", "companyID", cmbCompany.Columns("companyID").Text), "/trackermasters") > 0 Then
    ShowTapeColumns
Else
    HideTapeColumns
End If

If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/trackerfilemasters") > 0 Then
    grdItems.Columns("datearrived").Visible = True
Else
    grdItems.Columns("datearrived").Visible = False
End If

If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/trackershowworkflowvariant") > 0 Then
    grdItems.Columns("WorkflowVariant").Visible = True
End If
If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/trackershowworkflowelements") > 0 Then
    grdItems.Columns("originaleventID").Visible = True
    grdItems.Columns("transcodesegmentname").Visible = True
End If

If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/trackerjobnumber") > 0 Then
    grdItems.Columns("jobID").Visible = True
    txtJobID.Visible = True
    lblCaption(24).Visible = True
End If

If InStr(GetData("company", "cetaclientcode", "companyID", cmbCompany.Columns("companyID").Text), "/trackerStoredOn") > 0 Then
    grdItems.Columns("Stored On").Visible = True
Else
    grdItems.Columns("Stored On").Visible = False
End If

If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/trackerproxy") > 0 Then
    grdItems.Columns("proxyID").Visible = True
Else
    grdItems.Columns("proxyID").Visible = False
End If

If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/nobilltracker") > 0 Then
    grdItems.Columns("billed").Visible = False
    cmdBillAll.Visible = False
    cmdUnbillAll.Visible = False
    optComplete(2).Visible = False
Else
    grdItems.Columns("billed").Visible = True
    cmdBillAll.Visible = True
    cmdUnbillAll.Visible = True
    optComplete(2).Visible = True
End If

If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/nofinishtracker") > 0 Then
    grdItems.Columns("readytobill").Visible = False
Else
    grdItems.Columns("readytobill").Visible = True
End If

DoEvents

Dim l_rstChoices As ADODB.Recordset, l_blnWide As Boolean

If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
    l_blnWide = True
Else
    l_blnWide = False
End If

Set l_rstChoices = ExecuteSQL("SELECT * FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " ORDER BY CASE WHEN field_order IS NULL THEN 0 ELSE 1 END, field_order;", g_strExecuteError)
CheckForSQLError

If l_rstChoices.RecordCount > 0 Then
    l_rstChoices.MoveFirst
    Do While Not l_rstChoices.EOF
        Select Case l_rstChoices("itemfield")
            Case "requiredby"
                grdItems.Columns("requiredby").Visible = True
                grdItems.Columns("requiredby").Caption = l_rstChoices("itemheading")
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("requiredby").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("requiredby").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headertext1"
                grdItems.Columns("headertext1").Visible = True
                grdItems.Columns("headertext1").Caption = l_rstChoices("itemheading")
                lblCaption(0).Caption = "Search " & l_rstChoices("itemheading")
                lblCaption(0).Visible = True
                txtheadertext1.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headertext1").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext1").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headertext2"
                grdItems.Columns("headertext2").Visible = True
                grdItems.Columns("headertext2").Caption = l_rstChoices("itemheading")
                lblCaption(1).Visible = True
                lblCaption(1).Caption = "Search " & l_rstChoices("itemheading")
                txtheadertext2.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headertext2").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext2").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headertext3"
                grdItems.Columns("headertext3").Visible = True
                grdItems.Columns("headertext3").Caption = l_rstChoices("itemheading")
                lblCaption(2).Visible = True
                lblCaption(2).Caption = "Search " & l_rstChoices("itemheading")
                txtheadertext3.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headertext3").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext3").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headertext4"
                grdItems.Columns("headertext4").Visible = True
                grdItems.Columns("headertext4").Caption = l_rstChoices("itemheading")
                lblCaption(3).Visible = True
                lblCaption(3).Caption = "Search " & l_rstChoices("itemheading")
                txtheadertext4.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headertext4").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext4").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headertext5"
                grdItems.Columns("headertext5").Visible = True
                grdItems.Columns("headertext5").Caption = l_rstChoices("itemheading")
                lblCaption(4).Visible = True
                lblCaption(4).Caption = "Search " & l_rstChoices("itemheading")
                txtheadertext5.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headertext5").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext5").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headertext6"
                grdItems.Columns("headertext6").Visible = True
                grdItems.Columns("headertext6").Caption = l_rstChoices("itemheading")
                lblCaption(15).Visible = True
                lblCaption(15).Caption = "Search " & l_rstChoices("itemheading")
                txtheadertext6.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headertext6").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext6").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headertext7"
                grdItems.Columns("headertext7").Visible = True
                grdItems.Columns("headertext7").Caption = l_rstChoices("itemheading")
                lblCaption(16).Visible = True
                lblCaption(16).Caption = "Search " & l_rstChoices("itemheading")
                txtheadertext7.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headertext7").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext7").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headertext8"
                grdItems.Columns("headertext8").Visible = True
                grdItems.Columns("headertext8").Caption = l_rstChoices("itemheading")
                lblCaption(17).Visible = True
                lblCaption(17).Caption = "Search " & l_rstChoices("itemheading")
                txtheadertext8.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headertext8").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext8").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headertext9"
                grdItems.Columns("headertext9").Visible = True
                grdItems.Columns("headertext9").Caption = l_rstChoices("itemheading")
                lblCaption(18).Visible = True
                lblCaption(18).Caption = "Search " & l_rstChoices("itemheading")
                txtheadertext9.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headertext9").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext9").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headertext10"
                grdItems.Columns("headertext10").Visible = True
                grdItems.Columns("headertext10").Caption = l_rstChoices("itemheading")
                lblCaption(25).Visible = True
                lblCaption(25).Caption = "Search " & l_rstChoices("itemheading")
                txtheadertext10.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headertext10").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headertext10").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headerint1"
                grdItems.Columns("headerint1").Visible = True
                grdItems.Columns("headerint1").Caption = l_rstChoices("itemheading")
                lblCaption(5).Visible = True
                lblCaption(5).Caption = "Search " & l_rstChoices("itemheading")
                txtheaderint1.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headerint1").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerint1").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headerint2"
                grdItems.Columns("headerint2").Visible = True
                grdItems.Columns("headerint2").Caption = l_rstChoices("itemheading")
                lblCaption(6).Visible = True
                lblCaption(6).Caption = "Search " & l_rstChoices("itemheading")
                txtheaderint2.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headerint2").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerint2").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headerint3"
                grdItems.Columns("headerint3").Visible = True
                grdItems.Columns("headerint3").Caption = l_rstChoices("itemheading")
                lblCaption(7).Visible = True
                lblCaption(7).Caption = "Search " & l_rstChoices("itemheading")
                txtheaderint3.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headerint3").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerint3").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headerint4"
                grdItems.Columns("headerint4").Visible = True
                grdItems.Columns("headerint4").Caption = l_rstChoices("itemheading")
                lblCaption(8).Visible = True
                lblCaption(8).Caption = "Search " & l_rstChoices("itemheading")
                txtheaderint4.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headerint4").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerint4").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headerint5"
                grdItems.Columns("headerint5").Visible = True
                grdItems.Columns("headerint5").Caption = l_rstChoices("itemheading")
                lblCaption(9).Visible = True
                lblCaption(9).Caption = "Search " & l_rstChoices("itemheading")
                txtheaderint5.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headerint5").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerint5").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headerdate1"
                grdItems.Columns("headerdate1").Visible = True
                grdItems.Columns("headerdate1").Caption = l_rstChoices("itemheading")
                lblCaption(19).Visible = True
                lblCaption(19).Caption = "Search " & l_rstChoices("itemheading")
                datHeaderDate1.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headerdate1").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerdate1").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headerdate2"
                grdItems.Columns("headerdate2").Visible = True
                grdItems.Columns("headerdate2").Caption = l_rstChoices("itemheading")
                lblCaption(20).Visible = True
                lblCaption(20).Caption = "Search " & l_rstChoices("itemheading")
                datHeaderDate2.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headerdate2").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerdate2").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headerdate3"
                grdItems.Columns("headerdate3").Visible = True
                grdItems.Columns("headerdate3").Caption = l_rstChoices("itemheading")
                lblCaption(21).Visible = True
                lblCaption(21).Caption = "Search " & l_rstChoices("itemheading")
                datHeaderDate3.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headerdate3").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerdate3").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headerdate4"
                grdItems.Columns("headerdate4").Visible = True
                grdItems.Columns("headerdate4").Caption = l_rstChoices("itemheading")
                lblCaption(22).Visible = True
                lblCaption(22).Caption = "Search " & l_rstChoices("itemheading")
                datHeaderDate4.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headerdate4").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerdate4").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headerdate5"
                grdItems.Columns("headerdate5").Visible = True
                grdItems.Columns("headerdate5").Caption = l_rstChoices("itemheading")
                lblCaption(23).Visible = True
                lblCaption(23).Caption = "Search " & l_rstChoices("itemheading")
                datHeaderDate5.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headerdate5").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerdate5").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headerflag1"
                grdItems.Columns("headerflag1").Visible = True
                grdItems.Columns("headerflag1").Caption = l_rstChoices("itemheading")
                chkHeaderFlag1.Caption = "Search " & l_rstChoices("itemheading")
                chkHeaderFlag1.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headerflag1").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerflag1").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headerflag2"
                grdItems.Columns("headerflag2").Visible = True
                grdItems.Columns("headerflag2").Caption = l_rstChoices("itemheading")
                chkHeaderFlag2.Caption = "Search " & l_rstChoices("itemheading")
                chkHeaderFlag2.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headerflag2").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerflag2").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headerflag3"
                grdItems.Columns("headerflag3").Visible = True
                grdItems.Columns("headerflag3").Caption = l_rstChoices("itemheading")
                chkHeaderFlag3.Caption = "Search " & l_rstChoices("itemheading")
                chkHeaderFlag3.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headerflag3").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerflag3").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headerflag4"
                grdItems.Columns("headerflag4").Visible = True
                grdItems.Columns("headerflag4").Caption = l_rstChoices("itemheading")
                chkHeaderFlag4.Caption = "Search " & l_rstChoices("itemheading")
                chkHeaderFlag4.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headerflag4").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerflag4").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "headerflag5"
                grdItems.Columns("headerflag5").Visible = True
                grdItems.Columns("headerflag5").Caption = l_rstChoices("itemheading")
                chkHeaderFlag5.Caption = "Search " & l_rstChoices("itemheading")
                chkHeaderFlag5.Visible = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("headerflag5").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("headerflag5").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "stagefield1"
                EnableStagefield l_rstChoices, 1, l_blnWide, l_lngColumnOffset
                
            Case "stagefield2"
                EnableStagefield l_rstChoices, 2, l_blnWide, l_lngColumnOffset
                
            Case "stagefield3"
                EnableStagefield l_rstChoices, 3, l_blnWide, l_lngColumnOffset
                
            Case "stagefield4"
                EnableStagefield l_rstChoices, 4, l_blnWide, l_lngColumnOffset
                
            Case "stagefield5"
                EnableStagefield l_rstChoices, 5, l_blnWide, l_lngColumnOffset
                
            Case "stagefield6"
                EnableStagefield l_rstChoices, 6, l_blnWide, l_lngColumnOffset
                
            Case "stagefield7"
                EnableStagefield l_rstChoices, 7, l_blnWide, l_lngColumnOffset
                
            Case "stagefield8"
                EnableStagefield l_rstChoices, 8, l_blnWide, l_lngColumnOffset
                
            Case "stagefield9"
                EnableStagefield l_rstChoices, 9, l_blnWide, l_lngColumnOffset
                
            Case "stagefield10"
                EnableStagefield l_rstChoices, 10, l_blnWide, l_lngColumnOffset
                
            Case "stagefield11"
                EnableStagefield l_rstChoices, 11, l_blnWide, l_lngColumnOffset
                
            Case "stagefield12"
                EnableStagefield l_rstChoices, 12, l_blnWide, l_lngColumnOffset
                
            Case "stagefield13"
                EnableStagefield l_rstChoices, 13, l_blnWide, l_lngColumnOffset
                
            Case "stagefield14"
                EnableStagefield l_rstChoices, 14, l_blnWide, l_lngColumnOffset
                
            Case "stagefield15"
                EnableStagefield l_rstChoices, 15, l_blnWide, l_lngColumnOffset
        
            Case "stagefield16"
                EnableStagefield l_rstChoices, 16, l_blnWide, l_lngColumnOffset
        
            Case "stagefield17"
                EnableStagefield l_rstChoices, 17, l_blnWide, l_lngColumnOffset
        
            Case "stagefield18"
                EnableStagefield l_rstChoices, 18, l_blnWide, l_lngColumnOffset
        
            Case "stagefield19"
                EnableStagefield l_rstChoices, 19, l_blnWide, l_lngColumnOffset
        
            Case "stagefield20"
                EnableStagefield l_rstChoices, 20, l_blnWide, l_lngColumnOffset
        
            Case "stagefield21"
                EnableStagefield l_rstChoices, 21, l_blnWide, l_lngColumnOffset
        
            Case "stagefield22"
                EnableStagefield l_rstChoices, 22, l_blnWide, l_lngColumnOffset
        
            Case "stagefield23"
                EnableStagefield l_rstChoices, 23, l_blnWide, l_lngColumnOffset
        
            Case "stagefield24"
                EnableStagefield l_rstChoices, 24, l_blnWide, l_lngColumnOffset
        
            Case "stagefield25"
                EnableStagefield l_rstChoices, 25, l_blnWide, l_lngColumnOffset
        
            Case "conclusionfield1"
                grdItems.Columns("conclusionfield1").Visible = True
                grdItems.Columns("conclusionfield1").Caption = l_rstChoices("itemheading")
                optComplete(5).Caption = l_rstChoices("itemheading")
                optComplete(5).Visible = True
                optComplete(8).Visible = True
                optComplete(8).Value = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("conclusionfield1").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("conclusionfield1").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "conclusionfield2"
                grdItems.Columns("conclusionfield2").Visible = True
                grdItems.Columns("conclusionfield2").Caption = l_rstChoices("itemheading")
                optComplete(6).Caption = l_rstChoices("itemheading")
                optComplete(6).Visible = True
                optComplete(8).Visible = True
                optComplete(8).Value = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("conclusionfield2").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("conclusionfield2").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "conclusionfield3"
                grdItems.Columns("conclusionfield3").Visible = True
                grdItems.Columns("conclusionfield3").Caption = l_rstChoices("itemheading")
                optComplete(7).Caption = l_rstChoices("itemheading")
                optComplete(7).Visible = True
                optComplete(8).Visible = True
                optComplete(8).Value = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("conclusionfield3").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("conclusionfield3").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "conclusionfield4"
                grdItems.Columns("conclusionfield4").Visible = True
                grdItems.Columns("conclusionfield4").Caption = l_rstChoices("itemheading")
                optComplete(10).Caption = l_rstChoices("itemheading")
                optComplete(10).Visible = True
                optComplete(8).Visible = True
                optComplete(8).Value = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("conclusionfield4").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("conclusionfield4").Position = l_rstChoices("field_order") + l_lngColumnOffset
            Case "conclusionfield5"
                grdItems.Columns("conclusionfield5").Visible = True
                grdItems.Columns("conclusionfield5").Caption = l_rstChoices("itemheading")
                optComplete(11).Caption = l_rstChoices("itemheading")
                optComplete(11).Visible = True
                optComplete(8).Visible = True
                optComplete(8).Value = True
                If Val(l_rstChoices("itemwidth")) <> 0 Then
                    grdItems.Columns("conclusionfield5").Width = Val(l_rstChoices("itemwidth"))
                End If
                If l_rstChoices("field_order") <> 0 Then grdItems.Columns("conclusionfield5").Position = l_rstChoices("field_order") + l_lngColumnOffset
        End Select
        
        l_rstChoices.MoveNext
    Loop
End If

l_rstChoices.Close
Set l_rstChoices = Nothing

If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/trackernotfile") <= 0 Then
    lblInstructions.Caption = "If using the button in the Ref column to make a File Record, that record will have Title taken from " & IIf(grdItems.Columns("headertext1").Visible = True, grdItems.Columns("headertext1").Caption, "(empty)") _
    & ", Subtitle taken from " & IIf(grdItems.Columns("headertext2").Visible = True, grdItems.Columns("headertext2").Caption, "(empty)") & ", the reference from the " & grdItems.Columns("itemreference").Caption & ", and the filename assumes a .mov is being made."
    lblInstructions.Visible = True
Else
    lblInstructions.Visible = False
End If
Dim l_strSQL As String

l_strSQL = "SELECT itemfield AS fieldname, itemheading AS label FROM tracker_itemchoice WHERE companyID = " & Val(lblCompanyID.Caption) & ";"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbOrderBy.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

If InStr(GetData("company", "cetaclientcode", "companyID", cmbCompany.Columns("companyID").Text), "/salestracker") > 0 Then
    grdItems.Columns("Customer").Visible = True
    adoCustomers.ConnectionString = g_strConnection
    adoCustomers.RecordSource = "SELECT portaluserID, fullname FROM portaluser WHERE companyID = " & cmbCompany.Columns("CompanyID").Text & " ORDER BY fullname;"
    adoCustomers.Refresh
    grdItems.Columns("Customer").DropDownHwnd = ddnPortalUser.hWnd
End If

adoWorkflowVariant.ConnectionString = g_strConnection
adoWorkflowVariant.RecordSource = "SELECT VariantID, VariantName FROM Workflow_Variant WHERE companyID = " & cmbCompany.Columns("CompanyID").Text & " ORDER BY VariantName;"
adoWorkflowVariant.Refresh
grdItems.Columns("WorkflowVariant").DropDownHwnd = ddnWorkflowVariant.hWnd

cmdSearch.Value = True

End Sub


Private Sub cmdBillAll_Click()

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then
    adoItems.Recordset.MoveFirst
    Do While Not adoItems.Recordset.EOF
        If cmdBillItem.Visible = True Then
            cmdBillItem.Value = True
        End If
        adoItems.Recordset.MoveNext
        DoEvents
    Loop
End If

End Sub

Private Sub cmdBillItem_Click()

Dim l_strChargeCode As String, l_rstTrackerChargeCode As ADODB.Recordset, l_strJobLine As String, l_lngJobID As Long, l_lngDuration As Long, l_lngMinuteBilling As Long
Dim l_lngQuantity As Long, l_strCode As String, l_lngFactor As Long, l_strLastComment As String, l_strLastCommentCuser As String, l_datLastCommentDate As Date, l_blnFirstLine As Boolean
Dim l_strWorkflowDescription As String
Dim l_lngStageFieldCount As Long
Dim l_datCompletedDate As Date
Dim l_strBFIFormat As String, l_strBFIPONumber As String

If grdItems.Columns("companyID").Text = 1602 Then
    BillDisneyDestruction
    Exit Sub
End If

If grdItems.Columns("companyID").Text = 1597 Then
    BillBBCStudiosEnglish
    Exit Sub
End If

If frmJob.txtJobID.Text <> "" Then
    'A job is loaded - now check that it isn't locked.
    l_lngJobID = Val(frmJob.txtJobID.Text)
    Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND minutebilling in ('M', 'I');", g_strExecuteError)
    CheckForSQLError
    If l_rstTrackerChargeCode.RecordCount > 0 Then
        If Val(grdItems.Columns("duration").Text) = 0 Then
            MsgBox "Zero Duration item found when billing duration based charge codes - item cannot be billed.", vbCritical
            l_rstTrackerChargeCode.Close
            Set l_rstTrackerChargeCode = Nothing
            Exit Sub
        End If
    End If
    l_rstTrackerChargeCode.Close

    If (frmJob.txtStatus.Text = "Submitted" Or frmJob.txtStatus.Text = "Confirmed") And frmJob.lblCompanyID.Caption = lblCompanyID.Caption Then
        'The Job is valid - go ahead and create Job lines for this item.
        l_lngQuantity = 0
        If InStr(GetData("company", "cetaclientcode", "companyID", grdItems.Columns("companyID").Text), "/iConcerbilling") > 0 Then
            l_strJobLine = QuoteSanitise(grdItems.Columns("itemfilename").Text)
        ElseIf InStr(GetData("company", "cetaclientcode", "companyID", grdItems.Columns("companyID").Text), "/BFIbilling") > 0 Then
            l_strJobLine = QuoteSanitise(grdItems.Columns("itemfilename").Text)
            l_strBFIPONumber = grdItems.Columns("headertext8").Text
            If grdItems.Columns("headertext2").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("headertext2").Text
            If grdItems.Columns("headertext4").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("headertext4").Text
            If grdItems.Columns("headertext1").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("headertext1").Text
        Else
            l_strJobLine = QuoteSanitise(grdItems.Columns("headertext1").Text)
            If grdItems.Columns("headerint1").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("headerint1").Text
            If grdItems.Columns("headerint2").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("headerint2").Text
            If grdItems.Columns("headertext2").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("headertext2").Text
            If grdItems.Columns("itemreference").Text <> "" Then
                If l_strJobLine <> "" Then
                    l_strJobLine = l_strJobLine & " - " & grdItems.Columns("itemreference").Text
                Else
                    l_strJobLine = grdItems.Columns("itemreference").Text
                End If
            End If
        End If
        If adoComments.Recordset.RecordCount > 0 Then
            adoComments.Recordset.MoveLast
            l_strLastCommentCuser = adoComments.Recordset("cuser")
            l_datLastCommentDate = adoComments.Recordset("cdate")
            l_strLastComment = adoComments.Recordset("comment")
        Else
            l_strLastCommentCuser = ""
            l_datLastCommentDate = 0
            l_strLastComment = ""
        End If
        l_blnFirstLine = True

        For l_lngStageFieldCount = 1 To 25
        
            If grdItems.Columns("stagefield" & l_lngStageFieldCount).Text <> "" And LCase(grdItems.Columns("stagefield" & l_lngStageFieldCount).Text) <> "n/a" Then
                If l_blnFirstLine = True Then
                    l_strCode = "M"
                    l_blnFirstLine = False
                Else
                    l_strCode = "A"
                End If
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield" & l_lngStageFieldCount & "';", g_strExecuteError)
                CheckForSQLError
                If l_rstTrackerChargeCode.RecordCount > 0 Then
                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                    Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
                        Case "C"
                            l_lngQuantity = Val(grdItems.Columns("stagefield" & l_lngStageFieldCount).Text)
                            If l_lngQuantity <= 4 Then l_strChargeCode = l_strChargeCode & "004"
                            If l_lngQuantity > 4 And l_lngQuantity <= 7 Then l_strChargeCode = l_strChargeCode & "007"
                            If l_lngQuantity > 7 And l_lngQuantity <= 10 Then l_strChargeCode = l_strChargeCode & "010"
                            If l_lngQuantity > 10 And l_lngQuantity <= 13 Then l_strChargeCode = l_strChargeCode & "013"
                            l_lngDuration = 0
                            l_strCode = "O"
                        Case "N"
                            l_lngQuantity = Val(grdItems.Columns("stagefield" & l_lngStageFieldCount).Text)
                            l_lngDuration = 0
                            l_strCode = "O"
                        Case "F"
                            l_strBFIFormat = GetData("library", "format", "barcode", grdItems.Columns("barcode").Text)
                            l_lngQuantity = 1
                            l_strCode = "O"
                            l_strChargeCode = GetDataSQL("SELECT BFIChargeCode FROM BFIFormat WHERE companyID = " & lblCompanyID.Caption & " AND BFIFORMAT = '" & l_strBFIFormat & "'")
                            If l_strChargeCode = "BFI1INCH" Then
                                If l_lngDuration > 60 Then l_strChargeCode = "BFI1INCHLONG"
                            ElseIf l_strChargeCode = "" Then
                                l_strChargeCode = GetDataSQL("SELECT BFIChargeCode FROM BFIFormat WHERE companyID = " & lblCompanyID.Caption & " AND BFIFORMAT = 'AllOtherFormats'")
                            End If
                        Case "T"
                            l_lngQuantity = 1
                            l_lngDuration = Val(grdItems.Columns("stagefield" & l_lngStageFieldCount).Text)
                        Case "M"
                            l_lngMinuteBilling = 1
                            If l_lngDuration = 0 Then MsgBox "Zero Duration Inclusive item - this is probably wrong.", vbCritical
                        Case "S"
                            l_lngFactor = 0
                            If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
                            If l_lngFactor <> 0 Then
                                If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
                                    l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
                                Else
                                    l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                                End If
                            Else
                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                            End If
                            l_lngDuration = 0
                            l_strCode = "O"
                        Case "1"
                            l_lngQuantity = 1
                            l_lngDuration = 0
                            l_strCode = "O"
                        Case "I"
                            l_strCode = "I"
                            If l_lngDuration = 0 Then MsgBox "Zero Duration Inclusive item - this is probably wrong.", vbCritical
                        Case Else
                            l_lngMinuteBilling = 0
                    End Select
                    If l_lngQuantity = 0 Then l_lngQuantity = 1
                End If
                l_rstTrackerChargeCode.Close
                If l_strChargeCode <> "" Then
                    If InStr(GetData("company", "cetaclientcode", "companyID", grdItems.Columns("companyID").Text), "/BFIbilling") > 0 Then
                        If IsDate(grdItems.Columns("stagefield6").Text) Then
                            l_datCompletedDate = grdItems.Columns("stagefield6").Text
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling, "", 0, "", 0, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", l_strBFIPONumber
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, "BFIMATROSKA", l_lngDuration, l_datCompletedDate, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling, "", 0, "", 0, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", l_strBFIPONumber
                        Else
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling, "", 0, "", 0, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", l_strBFIPONumber
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, "BFIMATROSKA", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling, "", 0, "", 0, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", l_strBFIPONumber
                        End If
                    ElseIf l_strLastComment <> "" Then
                        MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, "", "", "", "", "", "", "", "", "", "", "", "", "", "", l_strBFIPONumber
                    Else
                        MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling, "", 0, "", 0, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", l_strBFIPONumber
                    End If
                    l_lngQuantity = 0
                End If
            End If
            frmJob.adoJobDetail.Refresh
        Next
        
        frmJob.adoJobDetail.Refresh
        grdItems.Columns("billed").Text = -1
        grdItems.Columns("jobID").Text = l_lngJobID
        grdItems.Update
        cmdBillItem.Visible = False
        cmdManualBillItem.Visible = False
    Else
        MsgBox "The current Job must belong to the correct company and be confirmed in order to bill an item.", vbCritical, "Cannot Bill Item"
    End If
Else
    MsgBox "A Job must be created and loaded into the Jobs form in order to bill an item.", vbCritical, "Cannot Bill Item"
End If

End Sub

Private Sub cmdClear_Click()

ClearFields Me

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdCollectBBCStudioEnglishWork_Click()

Dim SQL As String, rs As ADODB.Recordset, CheckDate As Date, l_strDuration As String, l_lngRunningTime As Long

CheckDate = GetData("setting", "DateValue", "name", "BBCStudiosEnglishLastDateChecked")
If Not IsDate(CheckDate) Then
    MsgBox "Problem with last checked adte setting"
End If

SQL = "select Event_File_Request.RequestDate, events.eventID, events.fd_length, events.clipreference, events.clipfilename, events.cliphorizontalpixels, events.clipframerate, events.clipformat " & _
"FROM Event_File_Request INNER JOIN events " & _
"ON events.eventID = Event_File_Request.eventID " & _
"WHERE Event_File_Request.companyID = 1597 AND Event_File_Request_TypeId = 5 " & _
"AND RequestDate >= '" & Format(CheckDate, "YYYY-MM-DD HH:NN:SS") & "' " & _
"ORDER BY RequestDate ASC;"

Debug.Print SQL
Set rs = ExecuteSQL(SQL, g_strExecuteError)
ExecuteSQL "UPDATE setting SET datevalue = convert(nvarchar, getdate(), 20) WHERE name = 'BBCStudiosEnglishLastDateChecked';", g_strExecuteError
CheckForSQLError
If rs.RecordCount > 0 Then

    rs.MoveFirst
    Do While Not rs.EOF
        l_strDuration = rs("fd_length")
        l_lngRunningTime = 0
        SQL = "INSERT INTO tracker_item (companyID, itemreference, itemfilename, timecodeduration, duration, headertext1, headerint1, headertext2, stagefield1, readytobill) VALUES ("
        SQL = SQL & "1597, "
        SQL = SQL & "'" & QuoteSanitise(rs("clipreference")) & "', "
        SQL = SQL & "'" & QuoteSanitise(rs("clipfilename")) & "', "
        SQL = SQL & "'" & rs("fd_length") & "', "
        If l_strDuration <> "" Then
            l_lngRunningTime = 60 * Val(Mid(l_strDuration, 1, 2))
            l_lngRunningTime = l_lngRunningTime + Val(Mid(l_strDuration, 4, 2))
            If Val(Mid(l_strDuration, 7, 2)) > 30 Then
                l_lngRunningTime = l_lngRunningTime + 1
            End If
            If l_lngRunningTime = 0 Then l_lngRunningTime = 1
        End If
        SQL = SQL & l_lngRunningTime & ", "
        SQL = SQL & "'" & rs("clipformat") & "', "
        SQL = SQL & Val(Trim(" " & rs("cliphorizontalpixels"))) & ", "
        SQL = SQL & "'" & rs("clipframerate") & "', "
        SQL = SQL & "'" & Format(rs("RequestDate"), "YYYY-MM-DD HH:NN:SS") & "', 1);"
        
        Debug.Print SQL
        ExecuteSQL SQL, g_strExecuteError
        CheckForSQLError
        rs.MoveNext
    Loop
End If
rs.Close
Set rs = Nothing
MsgBox "Done"

adoItems.Refresh

End Sub

Private Sub cmdCopyBack_Click()

Dim i As Long, bkmrk As Variant, l_lngTotalSelRows As Long

If lblTrackeritemID.Caption = "" Then Exit Sub
If lblCompanyID.Caption = "" Then Exit Sub

If grdItems.SelBookmarks.Count <= 0 Then
    CopyTrackerItem Val(lblTrackeritemID.Caption)
Else
    l_lngTotalSelRows = grdItems.SelBookmarks.Count - 1
    
    For i = 0 To l_lngTotalSelRows
        bkmrk = grdItems.SelBookmarks(i)
        CopyTrackerItem Val(grdItems.Columns("tracker_itemID").CellText(bkmrk))
    Next i
End If

End Sub

Private Sub cmdExportGrid_Click()

If grdItems.Rows < 1 Then Exit Sub

Dim l_strFilePath As String
l_strFilePath = fGetSpecialFolder(CSIDL_DOCUMENTS)

grdItems.Export ssExportTypeText, ssExportAllRows + ssExportColumnHeaders, l_strFilePath & "GenericTrackerExport.txt"
MsgBox "Exported to a text file in 'My Documents' called 'GenericTrackerExport.txt'", vbInformation

End Sub

Private Sub cmdManualBillAll_Click()

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then
    adoItems.Recordset.MoveFirst
    Do While Not adoItems.Recordset.EOF
        adoItems.Recordset("billed") = 1
        adoItems.Recordset("jobID") = 0
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
        DoEvents
    Loop
End If

End Sub

Private Sub cmdManualBillItem_Click()

grdItems.Columns("billed").Text = -1
grdItems.Columns("jobid").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False

End Sub

Private Sub cmdNewPlaformOrder_Click()

If lblCompanyID.Caption = "" Then
    MsgBox "please select a company first"
    Exit Sub
End If

'frmDisneyOrderEntry.fraDADC.Visible = False
'frmDisneyOrderEntry.fraNormal.Visible = False
frmDisneyOrderEntry.cmbCompany.Columns("companyID").Text = lblCompanyID.Caption
frmDisneyOrderEntry.cmbCompany.Columns("name").Text = cmbCompany.Text
frmDisneyOrderEntry.lblCompanyID.Caption = lblCompanyID.Caption
frmDisneyOrderEntry.txtClientStatus.Text = GetData("company", "accountstatus", "companyID", lblCompanyID.Caption)
frmDisneyOrderEntry.cmbCompany.Columns("accountstatus").Text = ""
frmDisneyOrderEntry.cmbCompany.Visible = False
frmDisneyOrderEntry.lblCompanyID.Visible = False
frmDisneyOrderEntry.txtClientStatus.Visible = False
frmDisneyOrderEntry.lblCaption(16).Visible = False
frmDisneyOrderEntry.optOrderType(4).Value = True
frmDisneyOrderEntry.fraGeneric.Top = 4440
frmDisneyOrderEntry.fraGeneric.Left = 180
frmDisneyOrderEntry.fraWorkflow.Top = 5880
'frmDisneyOrderEntry.fraWorkflow.Height = 1075
frmDisneyOrderEntry.cmbInclusive(0).Visible = True
frmDisneyOrderEntry.cmbInclusive(1).Visible = True
frmDisneyOrderEntry.cmbInclusive(2).Visible = True
frmDisneyOrderEntry.cmbInclusive(3).Visible = True
frmDisneyOrderEntry.cmbInclusive(4).Visible = True
frmDisneyOrderEntry.cmbInclusive(5).Visible = True
frmDisneyOrderEntry.lblCaption(9).Visible = True
frmDisneyOrderEntry.lblCaption(10).Visible = True
frmDisneyOrderEntry.lblCaption(11).Visible = True
frmDisneyOrderEntry.lblCaption(12).Visible = True
frmDisneyOrderEntry.lblCaption(13).Visible = True
frmDisneyOrderEntry.lblCaption(14).Visible = True
frmDisneyOrderEntry.chkMultiple.Visible = True
frmDisneyOrderEntry.cmdClear.Value = True
frmDisneyOrderEntry.Show vbModal

Unload frmDisneyOrderEntry
cmdSearch.Value = True

End Sub

Private Sub cmdPrint_Click()

Dim l_strSQL As String

If lblCompanyID.Caption <> "" Then
    l_strSQL = adoItems.RecordSource
    l_strSQL = Mid(l_strSQL, InStr(l_strSQL, "WHERE"))
    l_strSQL = "SELECT tracker_item.*, tracker_comment.* FROM tracker_item LEFT JOIN tracker_Comment on tracker_item.tracker_itemID = tracker_comment.tracker_itemID " & l_strSQL
    LocalPrintCrystalReportUsingCleanSQL g_strLocationOfCrystalReportFiles & "Generic_Tracker_Report.rpt", l_strSQL, True
End If

End Sub

Private Sub cmdPrintCrate_Click()

Dim l_strSQL As String

If lblCompanyID.Caption <> "" Then
    l_strSQL = adoItems.RecordSource
    l_strSQL = Mid(l_strSQL, InStr(l_strSQL, "WHERE"))
    l_strSQL = "SELECT tracker_item.*, tracker_comment.* FROM tracker_item LEFT JOIN tracker_Comment on tracker_item.tracker_itemID = tracker_comment.tracker_itemID " & l_strSQL
    LocalPrintCrystalReportUsingCleanSQL g_strLocationOfCrystalReportFiles & "Generic_Tracker_Crate.rpt", l_strSQL, True
End If

End Sub

Private Sub cmdSearch_Click()

Dim l_rstTotal As ADODB.Recordset, l_lngVideoTotal As Long

If lblCompanyID.Caption <> "" Then

    Dim l_strSQL As String
    If optComplete(0).Value = True Then
        'Not Complete
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (rejected IS NULL OR rejected = 0) "
    ElseIf optComplete(9).Value = True Then
        'Tape Arrived
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (rejected IS NULL OR rejected = 0) AND (datearrived IS NOT NULL AND dateleft IS NULL) "
    ElseIf optComplete(12).Value = True Then
        'Tape Not Arrived
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (rejected IS NULL OR rejected = 0) AND (datearrived IS NULL) "
    ElseIf optComplete(1).Value = True Then
        'Finished
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND readytobill <> 0 "
    ElseIf optComplete(2).Value = True Then
        'Billed
        l_strSQL = " AND billed IS NOT NULL AND billed <> 0 "
    ElseIf optComplete(4).Value = True Then
        'Pending
        l_strSQL = " AND rejected <> 0 "
    End If
    
    If optComplete(5).Value = True Then
        'Conclusionfield1
        l_strSQL = l_strSQL & " AND (conclusionfield1 IS NOT NULL and conclusionfield1 <> 0) "
    ElseIf optComplete(6).Value = True Then
        'Conclusionfield2
        l_strSQL = l_strSQL & " AND (conclusionfield2 IS NOT NULL and conclusionfield2 <> 0) "
    ElseIf optComplete(7).Value = True Then
        'Conclusionfield3
        l_strSQL = l_strSQL & " AND (conclusionfield3 IS NOT NULL and conclusionfield3 <> 0) "
    ElseIf optComplete(10).Value = True Then
        'Conclusionfield4
        l_strSQL = l_strSQL & " AND (conclusionfield4 IS NOT NULL and conclusionfield4 <> 0) "
    ElseIf optComplete(11).Value = True Then
        'Conclusionfield5
        l_strSQL = l_strSQL & " AND (conclusionfield5 IS NOT NULL and conclusionfield5 <> 0) "
    End If
    
    If txtReference.Text <> "" Then
        l_strSQL = l_strSQL & " AND itemreference LIKE '" & QuoteSanitise(txtReference.Text) & "%' "
    End If
    
    If txtBarcode.Text <> "" Then
        l_strSQL = l_strSQL & " AND barcode LIKE '" & QuoteSanitise(txtBarcode.Text) & "%' "
    End If
    
    If txtheadertext1.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext1 LIKE '" & QuoteSanitise(txtheadertext1.Text) & "%' "
    End If
    If txtheadertext2.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext2 LIKE '" & QuoteSanitise(txtheadertext2.Text) & "%' "
    End If
    If txtheadertext3.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext3 LIKE '" & QuoteSanitise(txtheadertext3.Text) & "%' "
    End If
    If txtheadertext4.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext4 LIKE '" & QuoteSanitise(txtheadertext4.Text) & "%' "
    End If
    If txtheadertext5.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext5 LIKE '" & QuoteSanitise(txtheadertext5.Text) & "%' "
    End If
    If txtheadertext6.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext6 LIKE '" & QuoteSanitise(txtheadertext6.Text) & "%' "
    End If
    If txtheadertext7.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext7 LIKE '" & QuoteSanitise(txtheadertext7.Text) & "%' "
    End If
    If txtheadertext8.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext8 LIKE '" & QuoteSanitise(txtheadertext8.Text) & "%' "
    End If
    If txtheadertext9.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext9 LIKE '" & QuoteSanitise(txtheadertext9.Text) & "%' "
    End If
    If txtheadertext10.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext10 LIKE '" & QuoteSanitise(txtheadertext10.Text) & "%' "
    End If
    If txtheaderint1.Text <> "" Then
        l_strSQL = l_strSQL & " AND headerint1 = " & Val(txtheaderint1.Text) & " "
    End If
    If txtheaderint2.Text <> "" Then
        l_strSQL = l_strSQL & " AND headerint2 = " & Val(txtheaderint2.Text) & " "
    End If
    If txtheaderint3.Text <> "" Then
        l_strSQL = l_strSQL & " AND headerint3 = " & Val(txtheaderint3.Text) & " "
    End If
    If txtheaderint4.Text <> "" Then
        l_strSQL = l_strSQL & " AND headerint4 = " & Val(txtheaderint4.Text) & " "
    End If
    If txtheaderint5.Text <> "" Then
        l_strSQL = l_strSQL & " AND headerint5 = " & Val(txtheaderint5.Text) & " "
    End If
    If Not IsNull(datHeaderDate1.Value) Then
        l_strSQL = l_strSQL & " AND headerdate1 = '" & FormatSQLDate(Format(datHeaderDate1.Value, "yyyy-mm-dd")) & "' "
    End If
    If Not IsNull(datHeaderDate2.Value) Then
        l_strSQL = l_strSQL & " AND headerdate2 = '" & FormatSQLDate(Format(datHeaderDate2.Value, "yyyy-mm-dd")) & "' "
    End If
    If Not IsNull(datHeaderDate3.Value) Then
        l_strSQL = l_strSQL & " AND headerdate3 = '" & FormatSQLDate(Format(datHeaderDate3.Value, "yyyy-mm-dd")) & "' "
    End If
    If Not IsNull(datHeaderDate4.Value) Then
        l_strSQL = l_strSQL & " AND headerdate4 = '" & FormatSQLDate(Format(datHeaderDate4.Value, "yyyy-mm-dd")) & "' "
    End If
    If Not IsNull(datHeaderDate5.Value) Then
        l_strSQL = l_strSQL & " AND headerdate5 = '" & FormatSQLDate(Format(datHeaderDate5.Value, "yyyy-mm-dd")) & "' "
    End If
    If chkHeaderFlag1.Value <> 0 Then
        l_strSQL = l_strSQL & " AND headerflag1 <> 0 "
    End If
    If chkHeaderFlag2.Value <> 0 Then
        l_strSQL = l_strSQL & " AND headerflag2 <> 0 "
    End If
    If chkHeaderFlag3.Value <> 0 Then
        l_strSQL = l_strSQL & " AND headerflag3 <> 0 "
    End If
    If chkHeaderFlag4.Value <> 0 Then
        l_strSQL = l_strSQL & " AND headerflag4 <> 0 "
    End If
    If chkHeaderFlag5.Value <> 0 Then
        l_strSQL = l_strSQL & " AND headerflag5 <> 0 "
    End If
    If txtJobID.Text <> "" Then
        l_strSQL = l_strSQL & " AND jobID = " & Val(txtJobID.Text) & " "
    End If
    
    If txtDirectSQL.Text <> "" Then
        l_strSQL = l_strSQL & " AND (" & txtDirectSQL & ") "
    End If
    
    If cmbOrderBy.Text <> "" Then m_strOrderby = "ORDER BY " & cmbOrderBy.Columns("fieldname").Text
    
    On Error Resume Next
    
    adoItems.ConnectionString = g_strConnection
    adoItems.RecordSource = "SELECT * FROM tracker_item " & m_strSearch & l_strSQL & m_strOrderby & ";"
    adoItems.Refresh
    
    adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"

    Set l_rstTotal = ExecuteSQL("SELECT Sum(duration) FROM tracker_item " & m_strSearch & l_strSQL & ";", g_strExecuteError)
    CheckForSQLError
    
    If IsNull(l_rstTotal(0)) Then
        l_lngVideoTotal = 0
    Else
        l_lngVideoTotal = l_rstTotal(0)
    End If
    
    l_rstTotal.Close

    txtTotalDuration.Text = l_lngVideoTotal

    Set l_rstTotal = ExecuteSQL("SELECT Sum(gbsent) FROM tracker_item " & m_strSearch & l_strSQL & ";", g_strExecuteError)
    CheckForSQLError
    
    If IsNull(l_rstTotal(0)) Then
        l_lngVideoTotal = 0
    Else
        l_lngVideoTotal = l_rstTotal(0)
    End If
    
    l_rstTotal.Close

    txtTotalSize.Text = l_lngVideoTotal

    Set l_rstTotal = Nothing
    
    On Error GoTo 0
    
End If

End Sub

Private Sub cmdUnbill_Click()

grdItems.Columns("billed").Text = 0
grdItems.Columns("jobid").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False

End Sub

Private Sub cmdUnbillAll_Click()

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then

    Do While Not adoItems.Recordset.EOF
        adoItems.Recordset("billed") = 0
        adoItems.Recordset("jobID") = 0
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
    Loop
End If

grdItems.Refresh

End Sub

Private Sub cmdUpdateFlags_Click()

Dim Count As Long
Dim i As Integer, J As Integer
Dim temp As Boolean

If Not adoItems.Recordset.EOF Then
    adoItems.Recordset.MoveFirst
    ProgressBar1.Max = adoItems.Recordset.RecordCount
    ProgressBar1.Value = 0
    Count = 0
    ProgressBar1.Visible = True
    Do While Not adoItems.Recordset.EOF
        ProgressBar1.Value = Count
        DoEvents
        For i = 1 To NumberOfConclusionFields
            If grdItems.Columns("conclusionfield" & i).Visible = True Then
                temp = True
                For J = 1 To 25
                    If m_blnStagefieldConc(i, J) = True Then
                        If grdItems.Columns("stagefield" & J).Text = "" Or UCase(Right(grdItems.Columns("stagefield" & J).Text, 3)) = "ERR" Then temp = False
                    End If
                    If m_blnStagefieldNOTConc(i, J) = True Then
                        If grdItems.Columns("stagefield" & J).Text <> "" And UCase(Right(grdItems.Columns("stagefield" & J).Text, 3)) <> "ERR" Then temp = False
                    End If
                Next
                grdItems.Columns("conclusionfield" & i).Text = temp
            End If
        Next
        
        temp = True
        
        'Check ReadytoBill
        For i = 1 To 25
            If m_blnStagefieldConclusion(i) = True Then
                If grdItems.Columns("stagefield" & i).Text = "" Or UCase(Right(grdItems.Columns("stagefield" & i).Text, 3)) = "ERR" Then temp = False
            End If
        Next
        
        If temp = True Then
            grdItems.Columns("readytobill").Text = 1
        Else
            grdItems.Columns("readytobill").Text = 0
        End If
        
        'Check the error status
        temp = True
        For i = 1 To 25
            If UCase(Right(grdItems.Columns("stagefield" & i).Text, 3)) = "ERR" Then temp = False
        Next
        
        If temp = False Then
            grdItems.Columns("rejected").Text = 1
        End If
        
        If Val(grdItems.Columns("rejected").Text) = -1 Then
            grdItems.Columns("rejected").Text = 1
        End If
        
        grdItems.Update
        adoItems.Recordset.MoveNext
        Count = Count + 1
    Loop
End If

ProgressBar1.Visible = False

End Sub

Private Sub Command1_Click()

adoItems.Recordset.MoveFirst
Do While Not adoItems.Recordset.EOF
    If Not IsNull(adoItems.Recordset("barcode")) Then
        adoItems.Recordset("itemreference") = SanitiseBarcode(adoItems.Recordset("barcode"))
        adoItems.Recordset.Update
    End If
    adoItems.Recordset.MoveNext
Loop

End Sub

Private Sub ddnPortalUser_CloseUp()

grdItems.Columns("portaluserID").Text = ddnPortalUser.Columns("portaluserID").Text

End Sub

Private Sub ddnWorkflowVariant_CloseUp()

grdItems.Columns("WorkflowVariantID").Text = ddnWorkflowVariant.Columns("VariantID").Text

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

grdItems.StyleSets("headerfield").BackColor = &HE7FFE7
grdItems.StyleSets("stagefield").BackColor = &HE7FFFF
grdItems.StyleSets("conclusionfield").BackColor = &HFFFFE7

grdItems.StyleSets.Add "Error"
grdItems.StyleSets("Error").BackColor = &HA0A0FF

optComplete(0).Value = True

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE (cetaclientcode like '%/tracker %' OR cetaclientcode like '%/salestracker %') AND companyID > 100 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE (cetaclientcode like '%/tracker %' OR cetaclientcode like '%/salestracker %') ORDER BY name;"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub Form_Resize()

On Error Resume Next

grdItems.Width = Me.ScaleWidth - grdItems.Left - 120
grdItems.Height = (Me.ScaleHeight - grdItems.Top - frmButtons.Height) * 0.75 - 240
frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120
grdComments.Top = grdItems.Top + grdItems.Height + 120
grdComments.Height = frmButtons.Top - grdComments.Top - 120
grdComments.Width = grdItems.Width / 2 - 120

End Sub

Private Sub grdComments_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdComments_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdComments_BeforeInsert(Cancel As Integer)
m_blnInsert = True
End Sub

Private Sub grdComments_BeforeUpdate(Cancel As Integer)

If grdComments.Columns("comment").Text = "" Then
    MsgBox "Cannot save a Comment with no actual comment", vbCritical, "Comment Not Saved"
    Cancel = True
    Exit Sub
End If
   
grdComments.Columns("tracker_itemID").Text = lblTrackeritemID.Caption
If grdComments.Columns("cdate").Text = "" Then
    grdComments.Columns("cdate").Text = Now
End If
grdComments.Columns("cuser").Text = g_strFullUserName

If m_blnDelete = False Then

    If m_blnDelete = False Then
        Dim l_rstWhoToEmail As ADODB.Recordset
        Dim l_strEmailBody As String
        
        l_strEmailBody = "Company: " & cmbCompany.Text & vbCrLf
        l_strEmailBody = l_strEmailBody & "Comment Date: " & Format(grdComments.Columns("cdate").Text, "YYYY-MM-DD HH:NN:SS") & vbCrLf
        l_strEmailBody = l_strEmailBody & "Comment By: " & grdComments.Columns("cuser").Text & vbCrLf
        If lblCompanyID.Caption = "1602" Then l_strEmailBody = l_strEmailBody & "Element #: " & grdItems.Columns("headerint1").Text & vbCrLf
        l_strEmailBody = l_strEmailBody & "Comment: " & grdComments.Columns("comment").Text
        
        Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND trackermessageID = 20;", g_strExecuteError)
        
        If l_rstWhoToEmail.RecordCount > 0 Then
            l_rstWhoToEmail.MoveFirst
            Do While Not l_rstWhoToEmail.EOF
        
                SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "Generic Tracker Comment " & IIf(m_blnInsert = True, "Created", "Updated"), "", l_strEmailBody, True, "", "", g_strAdministratorEmailAddress
                l_rstWhoToEmail.MoveNext
            
            Loop
        End If
        l_rstWhoToEmail.Close
        Set l_rstWhoToEmail = Nothing
        m_blnInsert = False
    End If
    
End If

End Sub

Private Sub grdItems_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdItems_AfterUpdate(RtnDispErrMsg As Integer)
'If lblLastTrackeritemID.Caption <> "" Then
'    ExecuteSQL "update tracker_item set mdate = getdate() WHERE tracker_itemid = " & lblLastTrackeritemID.Caption, g_strExecuteError
'    CheckForSQLError
'End If
End Sub

Private Sub grdItems_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

Dim l_lngInstanceID As Long

l_lngInstanceID = GetData("workflow_instance", "instanceID", "tracker_itemID", lblTrackeritemID.Caption)
If l_lngInstanceID <> 0 Then
    ExecuteSQL "DELETE from workflow_instancevariables WHERE instanceID = " & l_lngInstanceID, g_strExecuteError
    CheckForSQLError
    ExecuteSQL "DELETE FROM workflow_instance WHERE tracker_itemID = " & lblTrackeritemID.Caption, g_strExecuteError
    CheckForSQLError
End If
m_blnDelete = True

End Sub

Private Sub grdItems_BeforeUpdate(Cancel As Integer)

Dim temp As Boolean, l_strDuration As String, l_lngRunningTime As Long, l_curFileSize As Currency, l_strFilename As String, l_rst As ADODB.Recordset, AnyFinals As Boolean
Dim i As Integer, J As Integer

If m_blnDelete = False Then

    If grdItems.Columns("workflowvariant").Text = "" Then grdItems.Columns("WorkflowVariantID").Text = ""
    
    For i = 1 To NumberOfConclusionFields
        If grdItems.Columns("conclusionfield" & i).Visible = True Then
            temp = True
            For J = 1 To 25
                If m_blnStagefieldConc(i, J) = True Then
                    If grdItems.Columns("stagefield" & J).Text = "" Or UCase(Right(grdItems.Columns("stagefield" & J).Text, 3)) = "ERR" Then temp = False
                End If
                If m_blnStagefieldNOTConc(i, J) = True Then
                    If grdItems.Columns("stagefield" & J).Text <> "" And UCase(Right(grdItems.Columns("stagefield" & J).Text, 3)) <> "ERR" Then temp = False
                End If
            Next
            grdItems.Columns("conclusionfield" & i).Text = temp
        End If
    Next
    
    temp = True
    AnyFinals = False
    
    'Check ReadytoBill
    For i = 1 To 25
        If m_blnStagefieldConclusion(i) = True Then
            If grdItems.Columns("stagefield" & i).Text = "" Or UCase(Right(grdItems.Columns("stagefield" & i).Text, 3)) = "ERR" Then temp = False
            AnyFinals = True
        End If
    Next
    
    If AnyFinals = True Then
        If temp = True Then
            grdItems.Columns("readytobill").Text = 1
        Else
            grdItems.Columns("readytobill").Text = 0
        End If
    End If
    
    'Check the error status
    temp = True
    For i = 1 To 25
        If UCase(Right(grdItems.Columns("stagefield" & i).Text, 3)) = "ERR" Then temp = False
    Next
    
    If temp = False Then
        grdItems.Columns("rejected").Text = 1
    End If
    
    If Val(grdItems.Columns("rejected").Text) = -1 Then
        grdItems.Columns("rejected").Text = 1
    End If
    
    grdItems.Columns("companyID").Text = lblCompanyID.Caption
    
    If grdItems.Columns("itemreference").Text <> "" Then
        'see if there is a clip record to get the duration from reference
        l_strDuration = GetDataSQL("SELECT TOP 1 fd_length FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND system_deleted = 0 AND fd_length <> '' AND fd_length IS NOT NULL;")
        If l_strDuration = "" Then l_strDuration = GetDataSQL("SELECT TOP 1 fd_length FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND system_deleted <> 0 AND fd_length <> '' AND fd_length IS NOT NULL;")
        If grdItems.Columns("timecodeduration").Text <> "" And l_strDuration <> "" Then grdItems.Columns("timecodeduration").Text = l_strDuration
        If l_strDuration <> "" Then
            l_lngRunningTime = 60 * Val(Mid(l_strDuration, 1, 2))
            l_lngRunningTime = l_lngRunningTime + Val(Mid(l_strDuration, 4, 2))
            If Val(Mid(l_strDuration, 7, 2)) > 30 Then
                l_lngRunningTime = l_lngRunningTime + 1
            End If
            If l_lngRunningTime = 0 Then l_lngRunningTime = 1
            If (grdItems.Columns("duration").Text = "") Or (grdItems.Columns("duration").Text <> "" And chkLockDur.Value = 0) Then
                grdItems.Columns("duration").Text = l_lngRunningTime
            End If
        End If
        
        'See if there is a size to go in the GB sent column
        l_curFileSize = 0
        Set l_rst = ExecuteSQL("SELECT bigfilesize, clipfilename FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND system_deleted = 0;", g_strExecuteError)
        CheckForSQLError
        If l_rst.RecordCount > 0 Then
            l_rst.MoveFirst
            Do While Not l_rst.EOF
                If Not IsNull(l_rst("bigfilesize")) Then
                    If l_rst("bigfilesize") > l_curFileSize Then
                        l_curFileSize = l_rst("bigfilesize")
                        grdItems.Columns("itemfilename").Text = l_rst("clipfilename")
                    End If
                End If
                l_rst.MoveNext
            Loop
        End If
        l_rst.Close
        If l_curFileSize <> 0 Then
            If (grdItems.Columns("gbsent").Text = "") Or (grdItems.Columns("gbsent").Text <> "" And chkLockGB.Value = 0) Then
                grdItems.Columns("gbsent").Text = Int(l_curFileSize / 1024 / 1024 / 1024 + 0.999)
            End If
        End If
        
        'See if there is a proxy ID to go in a ProxyID column
        Set l_rst = ExecuteSQL("SELECT eventID FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Flash MP4' AND cliphorizontalpixels = 640 AND system_deleted = 0 AND libraryID IN (SELECT libraryID FROM library WHERE format = 'DISCSTORE');", g_strExecuteError)
        CheckForSQLError
        If l_rst.RecordCount > 0 Then
            l_rst.MoveFirst
            grdItems.Columns("proxyID").Text = l_rst("eventID")
        End If
        l_rst.Close
        Set l_rst = Nothing
    End If
    
    If grdItems.Columns("cdate").Text = "" Then grdItems.Columns("cdate").Text = Format(Now, "YYYY-MM-DD HH:NN:SS")
    If grdItems.Columns("cuser").Text = "" Then grdItems.Columns("cuser").Text = g_strUserInitials
    grdItems.Columns("mdate").Text = Format(Now, "YYYY-MM-DD HH:NN:SS")
    grdItems.Columns("muser").Text = g_strUserInitials

End If

End Sub

Private Sub grdItems_BtnClick()

Dim tempdate As String

Select Case LCase(grdItems.Columns(grdItems.Col).Name)

Case "itemreference"
    
    MakeClipFromGenericTrackerEvent

Case Else

    If grdItems.ActiveCell.Text <> "" Then
        grdItems.ActiveCell.Text = ""
    Else
        If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
            grdItems.ActiveCell.Text = Format(Now, "YYYY-MM-DD HH:NN:SS")
        Else
            tempdate = FormatDateTime(Now, vbLongDate)
            grdItems.ActiveCell.Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
        End If
    End If

End Select

End Sub

Private Sub grdItems_DblClick()

Select Case LCase(grdItems.Columns(grdItems.Col).Name)

    Case "jobid"
        ShowJob grdItems.Columns("jobID").Text, 1, True
    
    Case "barcode"
        ShowLibrary GetData("library", "libraryID", "barcode", grdItems.Columns("barcode").Text)
        
    Case Else
        ShowClipSearch "", grdItems.Columns("itemreference").Text
        
End Select

End Sub

Private Sub grdItems_HeadClick(ByVal ColIndex As Integer)

If (grdItems.Columns(ColIndex).Name <> "Customer" And grdItems.Columns(ColIndex).Name <> "Stored On") Then
    m_strOrderby = " ORDER BY " & grdItems.Columns(ColIndex).Name
    adoItems.RecordSource = "SELECT * FROM tracker_item " & m_strSearch & m_strOrderby
    adoItems.Refresh
End If

End Sub

Private Sub grdItems_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim l_strSQL As String

lblTrackeritemID.Caption = grdItems.Columns("tracker_itemID").Text

If Val(lblTrackeritemID.Caption) <> Val(lblLastTrackerItemID.Caption) Then
    
    If Val(lblTrackeritemID.Caption) = 0 Then
        cmdBillItem.Visible = False
    
        l_strSQL = "SELECT * FROM tracker_comment WHERE tracker_itemID = -1 ORDER BY cdate;"
        adoComments.RecordSource = l_strSQL
        adoComments.ConnectionString = g_strConnection
        adoComments.Refresh
        
        lblLastTrackerItemID.Caption = ""
        
        Exit Sub
        
    End If
    
    l_strSQL = "SELECT * FROM tracker_comment WHERE tracker_itemID = " & lblTrackeritemID.Caption & " ORDER BY cdate;"
    
    adoComments.RecordSource = l_strSQL
    adoComments.ConnectionString = g_strConnection
    adoComments.Refresh

    If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/nobilltracker") > 0 Then
        cmdBillItem.Visible = False
        cmdManualBillItem.Visible = False
        cmdBillAll.Visible = False
        cmdUnbillAll.Visible = False
        cmdManualBillAll.Visible = False
    Else
        If grdItems.Columns("readytobill").Text <> 0 And grdItems.Columns("billed").Text = 0 Then
            cmdBillItem.Visible = True
            cmdManualBillItem.Visible = True
        Else
            cmdBillItem.Visible = False
            cmdManualBillItem.Visible = False
        End If
    
        If grdItems.Columns("billed").Text <> 0 Then
            cmdUnbill.Visible = True
        Else
            cmdUnbill.Visible = False
        End If
        cmdBillAll.Visible = True
        cmdUnbillAll.Visible = True
        cmdManualBillAll.Visible = True
    End If
    
    lblLastTrackerItemID.Caption = lblTrackeritemID.Caption
    
End If
    
End Sub

Private Sub grdItems_RowLoaded(ByVal Bookmark As Variant)

Dim Count As Long

If Val(grdItems.Columns("rejected").Text) <> 0 Then
    grdItems.Columns("rejected").CellStyleSet "Error"
Else
    'Do Nothing
End If

If grdItems.Columns("WorkFlowVariantID").Text <> "" Then
    grdItems.Columns("Workflow Variant").Text = GetData("Workflow_Variant", "VariantName", "VariantID", grdItems.Columns("WorkFlowVariantID").Text)
End If

For Count = 1 To 25
    If UCase(Right(grdItems.Columns("stagefield" & Count).Text, 3)) = "ERR" Then
        grdItems.Columns("stagefield" & Count).CellStyleSet "Error"
    End If
Next
If grdItems.Columns("portaluserID").Text <> "" Then
    grdItems.Columns("Customer").Text = GetData("portaluser", "fullname", "portaluserID", Val(grdItems.Columns("portaluserID").Text))
End If

If grdItems.Columns("itemreference").Text <> "" Then
    grdItems.Columns("Stored On").Text = GetData("library", "barcode", "libraryID", GetDataSQL("SELECT TOP 1 events.libraryID FROM events INNER JOIN library ON events.libraryID = library.libraryID WHERE events.clipreference = '" & grdItems.Columns("itemreference").Text & "' AND events.clipformat = 'Quicktime' AND events.clipcodec = 'ProRes HQ' AND (library.format = 'DISCSTORE' OR library.format = 'LTO5' OR library.format = 'MOBILEDISC');"))
End If

adoComments.RecordSource = "SELECT * FROM tracker_comment WHERE tracker_itemID = " & grdItems.Columns("tracker_itemID").Text & " ORDER BY cdate ASC;"
adoComments.ConnectionString = g_strConnection
adoComments.Refresh

End Sub

Private Sub optComplete_Click(Index As Integer)

cmdSearch.Value = True

End Sub

Private Sub LocalPrintCrystalReportUsingCleanSQL(lp_strRPTFileName As String, lp_strSQL As String, lp_blnPreview As Boolean)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Screen.MousePointer = vbHourglass
    
    Dim crxApplication As New CRAXDRT.Application
    Dim crxReport As New CRAXDRT.Report
    
    Set crxReport = crxApplication.OpenReport(GetUNCNameNT(lp_strRPTFileName))
    
    crxReport.SQLQueryString = lp_strSQL
    
    crxReport.DiscardSavedData
    
    If grdItems.Columns("headertext1").Visible = True Then crxReport.ParameterFields.GetItemByName("headertext1label").AddCurrentValue grdItems.Columns("headertext1").Caption
    If grdItems.Columns("headertext2").Visible = True Then crxReport.ParameterFields.GetItemByName("headertext2label").AddCurrentValue grdItems.Columns("headertext2").Caption
    If grdItems.Columns("headertext3").Visible = True Then crxReport.ParameterFields.GetItemByName("headertext3label").AddCurrentValue grdItems.Columns("headertext3").Caption
    If grdItems.Columns("headertext4").Visible = True Then crxReport.ParameterFields.GetItemByName("headertext4label").AddCurrentValue grdItems.Columns("headertext4").Caption
    If grdItems.Columns("headertext5").Visible = True Then crxReport.ParameterFields.GetItemByName("headertext5label").AddCurrentValue grdItems.Columns("headertext5").Caption
    If grdItems.Columns("headertext6").Visible = True Then crxReport.ParameterFields.GetItemByName("headertext6label").AddCurrentValue grdItems.Columns("headertext6").Caption
    If grdItems.Columns("headertext7").Visible = True Then crxReport.ParameterFields.GetItemByName("headertext7label").AddCurrentValue grdItems.Columns("headertext7").Caption
    If grdItems.Columns("headertext8").Visible = True Then crxReport.ParameterFields.GetItemByName("headertext8label").AddCurrentValue grdItems.Columns("headertext8").Caption
    If grdItems.Columns("headertext9").Visible = True Then crxReport.ParameterFields.GetItemByName("headertext9label").AddCurrentValue grdItems.Columns("headertext9").Caption
    If grdItems.Columns("headerint1").Visible = True Then crxReport.ParameterFields.GetItemByName("headerint1label").AddCurrentValue grdItems.Columns("headerint1").Caption
    If grdItems.Columns("headerint2").Visible = True Then crxReport.ParameterFields.GetItemByName("headerint2label").AddCurrentValue grdItems.Columns("headerint2").Caption
    If grdItems.Columns("headerint3").Visible = True Then crxReport.ParameterFields.GetItemByName("headerint3label").AddCurrentValue grdItems.Columns("headerint3").Caption
    If grdItems.Columns("headerint4").Visible = True Then crxReport.ParameterFields.GetItemByName("headerint4label").AddCurrentValue grdItems.Columns("headerint4").Caption
    If grdItems.Columns("headerint5").Visible = True Then crxReport.ParameterFields.GetItemByName("headerint5label").AddCurrentValue grdItems.Columns("headerint5").Caption
    If grdItems.Columns("headerdate1").Visible = True Then crxReport.ParameterFields.GetItemByName("headerdate1label").AddCurrentValue grdItems.Columns("headerdate1").Caption
    If grdItems.Columns("headerdate2").Visible = True Then crxReport.ParameterFields.GetItemByName("headerdate2label").AddCurrentValue grdItems.Columns("headerdate2").Caption
    If grdItems.Columns("headerdate3").Visible = True Then crxReport.ParameterFields.GetItemByName("headerdate3label").AddCurrentValue grdItems.Columns("headerdate3").Caption
    If grdItems.Columns("headerdate4").Visible = True Then crxReport.ParameterFields.GetItemByName("headerdate4label").AddCurrentValue grdItems.Columns("headerdate4").Caption
    If grdItems.Columns("headerdate5").Visible = True Then crxReport.ParameterFields.GetItemByName("headerdate5label").AddCurrentValue grdItems.Columns("headerdate5").Caption
    If grdItems.Columns("headerflag1").Visible = True Then crxReport.ParameterFields.GetItemByName("headerflag1label").AddCurrentValue grdItems.Columns("headerflag1").Caption
    If grdItems.Columns("headerflag2").Visible = True Then crxReport.ParameterFields.GetItemByName("headerflag2label").AddCurrentValue grdItems.Columns("headerflag2").Caption
    If grdItems.Columns("headerflag3").Visible = True Then crxReport.ParameterFields.GetItemByName("headerflag3label").AddCurrentValue grdItems.Columns("headerflag3").Caption
    If grdItems.Columns("headerflag4").Visible = True Then crxReport.ParameterFields.GetItemByName("headerflag4label").AddCurrentValue grdItems.Columns("headerflag4").Caption
    If grdItems.Columns("headerflag5").Visible = True Then crxReport.ParameterFields.GetItemByName("headerflag5label").AddCurrentValue grdItems.Columns("headerflag5").Caption
    If grdItems.Columns("stagefield1").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield1label").AddCurrentValue grdItems.Columns("stagefield1").Caption
    If grdItems.Columns("stagefield2").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield2label").AddCurrentValue grdItems.Columns("stagefield2").Caption
    If grdItems.Columns("stagefield3").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield3label").AddCurrentValue grdItems.Columns("stagefield3").Caption
    If grdItems.Columns("stagefield4").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield4label").AddCurrentValue grdItems.Columns("stagefield4").Caption
    If grdItems.Columns("stagefield5").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield5label").AddCurrentValue grdItems.Columns("stagefield5").Caption
    If grdItems.Columns("stagefield6").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield6label").AddCurrentValue grdItems.Columns("stagefield6").Caption
    If grdItems.Columns("stagefield7").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield7label").AddCurrentValue grdItems.Columns("stagefield7").Caption
    If grdItems.Columns("stagefield8").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield8label").AddCurrentValue grdItems.Columns("stagefield8").Caption
    If grdItems.Columns("stagefield9").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield9label").AddCurrentValue grdItems.Columns("stagefield9").Caption
    If grdItems.Columns("stagefield10").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield10label").AddCurrentValue grdItems.Columns("stagefield10").Caption
    If grdItems.Columns("stagefield11").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield11label").AddCurrentValue grdItems.Columns("stagefield11").Caption
    If grdItems.Columns("stagefield12").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield12label").AddCurrentValue grdItems.Columns("stagefield12").Caption
    If grdItems.Columns("stagefield13").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield13label").AddCurrentValue grdItems.Columns("stagefield13").Caption
    If grdItems.Columns("stagefield14").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield14label").AddCurrentValue grdItems.Columns("stagefield14").Caption
    If grdItems.Columns("stagefield15").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield15label").AddCurrentValue grdItems.Columns("stagefield15").Caption
    If grdItems.Columns("stagefield16").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield16label").AddCurrentValue grdItems.Columns("stagefield16").Caption
    If grdItems.Columns("stagefield17").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield17label").AddCurrentValue grdItems.Columns("stagefield17").Caption
    If grdItems.Columns("stagefield18").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield18label").AddCurrentValue grdItems.Columns("stagefield18").Caption
    If grdItems.Columns("stagefield19").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield19label").AddCurrentValue grdItems.Columns("stagefield19").Caption
    If grdItems.Columns("stagefield20").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield20label").AddCurrentValue grdItems.Columns("stagefield20").Caption
    If grdItems.Columns("stagefield21").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield21label").AddCurrentValue grdItems.Columns("stagefield21").Caption
    If grdItems.Columns("stagefield22").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield22label").AddCurrentValue grdItems.Columns("stagefield22").Caption
    If grdItems.Columns("stagefield23").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield23label").AddCurrentValue grdItems.Columns("stagefield23").Caption
    If grdItems.Columns("stagefield24").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield24label").AddCurrentValue grdItems.Columns("stagefield24").Caption
    If grdItems.Columns("stagefield25").Visible = True Then crxReport.ParameterFields.GetItemByName("stagefield25label").AddCurrentValue grdItems.Columns("stagefield25").Caption
    
    If lp_blnPreview = False Then
        
        crxReport.PrintOut False
    Else
        
        frmCrystalPreview.CRViewer91.ReportSource = crxReport
        
        frmCrystalPreview.CRViewer91.ViewReport
        frmCrystalPreview.WindowState = vbMaximized
        frmCrystalPreview.Show vbModal
        frmCrystalPreview.CRViewer91.Zoom 100
    End If
    
    Set crxReport = Nothing
    crxApplication.CanClose
    Set crxApplication = Nothing
    
    Screen.MousePointer = vbDefault
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Screen.MousePointer = vbDefault
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Private Sub EnableStagefield(l_rstChoices As ADODB.Recordset, StagefieldNumber As Integer, l_blnWide As Boolean, l_lngColumnOffset As Long)

Dim i As Integer

For i = 1 To NumberOfConclusionFields
    If l_rstChoices("stage" & i & "conclusion") <> 0 Then
        m_blnStagefieldConc(i, StagefieldNumber) = True
    Else
        m_blnStagefieldConc(i, StagefieldNumber) = False
    End If
    If l_rstChoices("stage" & i & "NOTconclusion") <> 0 Then
        m_blnStagefieldNOTConc(i, StagefieldNumber) = True
    Else
        m_blnStagefieldNOTConc(i, StagefieldNumber) = False
    End If
Next
If l_rstChoices("conclusion") <> 0 Then
   m_blnStagefieldConclusion(StagefieldNumber) = True
Else
   m_blnStagefieldConclusion(StagefieldNumber) = False
End If
grdItems.Columns("stagefield" & StagefieldNumber).Visible = True
If Val(l_rstChoices("itemwidth")) <> 0 Then
    grdItems.Columns("stagefield" & StagefieldNumber).Width = Val(l_rstChoices("itemwidth"))
ElseIf l_blnWide = True Then
    grdItems.Columns("stagefield" & StagefieldNumber).Width = 2100
Else
    grdItems.Columns("stagefield" & StagefieldNumber).Width = 1500
End If
grdItems.Columns("stagefield" & StagefieldNumber).Caption = Trim(" " & l_rstChoices("itemheading"))
If l_rstChoices("field_order") <> 0 Then grdItems.Columns("stagefield" & StagefieldNumber).Position = l_rstChoices("field_order") + l_lngColumnOffset

End Sub

Private Sub txtheadertext1_GotFocus()
PopulateLocalCombo txtheadertext1, 1, Val(lblCompanyID.Caption), True
End Sub

Private Sub txtheadertext2_GotFocus()
PopulateLocalCombo txtheadertext2, 2, Val(lblCompanyID.Caption), True
End Sub

Private Sub txtheadertext3_GotFocus()
PopulateLocalCombo txtheadertext3, 3, Val(lblCompanyID.Caption), True
End Sub

Private Sub txtheadertext4_GotFocus()
PopulateLocalCombo txtheadertext4, 4, Val(lblCompanyID.Caption), True
End Sub

Private Sub txtheadertext5_GotFocus()
PopulateLocalCombo txtheadertext5, 5, Val(lblCompanyID.Caption), True
End Sub

Private Sub txtheadertext6_GotFocus()
PopulateLocalCombo txtheadertext6, 6, Val(lblCompanyID.Caption), True
End Sub

Private Sub txtheadertext7_GotFocus()
PopulateLocalCombo txtheadertext7, 7, Val(lblCompanyID.Caption), True
End Sub

Private Sub txtheadertext8_GotFocus()
PopulateLocalCombo txtheadertext8, 8, Val(lblCompanyID.Caption), True
End Sub

Private Sub txtheadertext9_GotFocus()
PopulateLocalCombo txtheadertext9, 9, Val(lblCompanyID.Caption), True
End Sub

Private Sub txtheadertext10_GotFocus()
PopulateLocalCombo txtheadertext10, 10, Val(lblCompanyID.Caption), True
End Sub

Private Sub PopulateLocalCombo(lp_conControl As Control, lp_lngCounter As Long, lp_lngCompanyID As Long, lp_blnClearFirst As Boolean)

Dim Count As Long

On Error GoTo PROC_CheckListError
    
    If lp_conControl.ListCount > 0 Then
        If lp_blnClearFirst = False Then
            Exit Sub
        Else
            For Count = 0 To lp_conControl.ListCount - 1
                lp_conControl.RemoveItem (0)
            Next
        End If
    End If
    GoTo PROC_Continue
    
PROC_TryRowCount:
    If lp_conControl.Rows > 0 Then
        If lp_blnClearFirst = False Then
            Exit Sub
        Else
            For Count = 0 To lp_conControl.Rows - 1
                lp_conControl.RemoveItem (0)
            Next
        End If
    End If
    GoTo PROC_Continue

PROC_CheckListError:
    If Err.Number = 438 Then
        GoTo PROC_TryRowCount
    End If
    
PROC_Continue:
    
    LockControl lp_conControl, True
    
    Dim l_strSQL As String
    Dim l_rstItems As New ADODB.Recordset
    l_strSQL = "SELECT DISTINCT headertext" & lp_lngCounter & " FROM tracker_item WHERE companyID = " & lp_lngCompanyID & " ORDER BY headertext" & lp_lngCounter & ";"
    Set l_rstItems = ExecuteSQL(l_strSQL, g_strExecuteError)
    If l_rstItems.RecordCount > 0 Then
        l_rstItems.MoveFirst
        Do While Not l_rstItems.EOF
            lp_conControl.AddItem Trim(" " & l_rstItems(0))
            l_rstItems.MoveNext
        Loop
    End If
    l_rstItems.Close
    
    LockControl lp_conControl, False

End Sub
