VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   12225
   ClientLeft      =   5550
   ClientTop       =   2400
   ClientWidth     =   6585
   LinkTopic       =   "Form1"
   ScaleHeight     =   12225
   ScaleWidth      =   6585
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   855
      Left            =   1920
      TabIndex        =   0
      Top             =   1380
      Width           =   2415
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Dim l_con As New ADODB.Connection
Dim l_conLocal As New ADODB.Connection
Dim l_rstMPC As New ADODB.Recordset
Dim l_rstLocal As New ADODB.Recordset


l_con.Open g_strConnection
l_conLocal.Open "DSN=cetalocal"


l_rstMPC.Open "SELECT jobID, companyname, contactname, contactID, companyID, productname, projectnumber, title1, projectID, productname, productID FROM job WHERE projectID = 0;", l_con, adOpenForwardOnly, adLockOptimistic


l_rstMPC.MoveFirst

Do While Not l_rstMPC.EOF

    
    l_rstLocal.Open "SELECT jobID, companyname, contactname, companyID, contactID, productname, productID, ourcontact, videostandard FROM job WHERE jobID = '" & l_rstMPC("jobID") & "'", l_conLocal, adOpenDynamic, adLockOptimistic

    
    If l_rstLocal("jobID") = l_rstMPC("jobID") Then
    
        l_strSQL = "UPDATE job SET "
        l_strSQL = l_strSQL & " companyname = '" & QuoteSanitise(l_rstLocal("companyname")) & "', "
        l_strSQL = l_strSQL & " contactname = '" & QuoteSanitise(l_rstLocal("contactname")) & "', "
        l_strSQL = l_strSQL & " companyID = '" & QuoteSanitise(l_rstLocal("companyID")) & "', "
        l_strSQL = l_strSQL & " contactID = '" & QuoteSanitise(l_rstLocal("contactID")) & "', "
        l_strSQL = l_strSQL & " productname = '" & QuoteSanitise(l_rstLocal("productname")) & "', "
        l_strSQL = l_strSQL & " productID = '" & QuoteSanitise(l_rstLocal("productID")) & "', "
        l_strSQL = l_strSQL & " ourcontact = '" & QuoteSanitise(l_rstLocal("ourcontact")) & "', "
        l_strSQL = l_strSQL & " videostandard = '" & QuoteSanitise(l_rstLocal("videostandard")) & "' "
        l_strSQL = l_strSQL & " WHERE jobID = " & l_rstLocal("jobID") & ";"
    
        l_con.Execute l_strSQL
        
    Else
    
        MsgBox "CANT FIND JOB ID"
    End If
    
    l_rstLocal.Close
    
    l_rstMPC.MoveNext

Loop


l_rstMPC.Close
l_rstLocal.Close
l_conLocal.Close
l_con.Close


End Sub
