VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmDespatch 
   Caption         =   "Despatch List"
   ClientHeight    =   10515
   ClientLeft      =   1830
   ClientTop       =   4020
   ClientWidth     =   14940
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDelivery.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   10515
   ScaleWidth      =   14940
   WindowState     =   2  'Maximized
   Begin VB.ComboBox cmbOrderBy 
      Height          =   315
      Index           =   1
      Left            =   5460
      TabIndex        =   16
      Tag             =   "NOCLEAR"
      Text            =   "cmbOrderBy"
      ToolTipText     =   "Which data field to search against"
      Top             =   9060
      Visible         =   0   'False
      Width           =   2235
   End
   Begin VB.ComboBox cmbDirection 
      Height          =   315
      Index           =   1
      ItemData        =   "frmDelivery.frx":08CA
      Left            =   7800
      List            =   "frmDelivery.frx":08D4
      Style           =   2  'Dropdown List
      TabIndex        =   15
      ToolTipText     =   "Which data field to search against"
      Top             =   9060
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ComboBox cmbOrderBy 
      Height          =   315
      Index           =   0
      Left            =   5460
      TabIndex        =   13
      Tag             =   "NOCLEAR"
      Text            =   "cmbOrderBy"
      ToolTipText     =   "Which data field to search against"
      Top             =   8640
      Visible         =   0   'False
      Width           =   2235
   End
   Begin VB.ComboBox cmbDirection 
      Height          =   315
      Index           =   0
      ItemData        =   "frmDelivery.frx":08E3
      Left            =   7800
      List            =   "frmDelivery.frx":08ED
      Style           =   2  'Dropdown List
      TabIndex        =   12
      ToolTipText     =   "Which data field to search against"
      Top             =   8640
      Visible         =   0   'False
      Width           =   1095
   End
   Begin MSAdodcLib.Adodc adoDelivery 
      Height          =   330
      Index           =   1
      Left            =   4680
      Top             =   4440
      Visible         =   0   'False
      Width           =   1905
      _ExtentX        =   3360
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoDelivery"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoDelivery 
      Height          =   330
      Index           =   0
      Left            =   4440
      Top             =   120
      Visible         =   0   'False
      Width           =   1905
      _ExtentX        =   3360
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoDelivery"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnDeliveredBy 
      Height          =   1755
      Left            =   7620
      TabIndex        =   8
      Top             =   1800
      Width           =   3495
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   12648447
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   3200
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "Description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   6165
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnMethod 
      Height          =   1755
      Left            =   5280
      TabIndex        =   7
      Top             =   1800
      Width           =   2115
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   12648447
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   3200
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "Description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   3731
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.CommandButton cmdCompleteDespatch 
      Caption         =   "Send Despatch"
      Height          =   315
      Index           =   1
      Left            =   1560
      TabIndex        =   6
      ToolTipText     =   "Send Despatch"
      Top             =   7920
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.CommandButton cmdCompleteDespatch 
      Caption         =   "Send Despatch"
      Height          =   315
      Index           =   0
      Left            =   180
      TabIndex        =   5
      ToolTipText     =   "Send Despatch"
      Top             =   7920
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.PictureBox picFooter 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   3480
      ScaleHeight     =   375
      ScaleWidth      =   11295
      TabIndex        =   3
      Top             =   7860
      Width           =   11295
      Begin VB.CheckBox chkShow 
         Caption         =   "P/O"
         Height          =   375
         Index           =   3
         Left            =   7320
         TabIndex        =   21
         Top             =   0
         Width           =   615
      End
      Begin VB.CheckBox chkShow 
         Caption         =   "Edit"
         Height          =   375
         Index           =   2
         Left            =   6660
         TabIndex        =   20
         Top             =   0
         Width           =   615
      End
      Begin VB.CheckBox chkShow 
         Caption         =   "Dubbing"
         Height          =   375
         Index           =   1
         Left            =   5580
         TabIndex        =   19
         Top             =   0
         Width           =   975
      End
      Begin VB.CheckBox chkShow 
         Caption         =   "Hire"
         Height          =   375
         Index           =   0
         Left            =   4860
         TabIndex        =   18
         Top             =   0
         Width           =   675
      End
      Begin VB.CheckBox chkShowDespatchOnClick 
         Caption         =   "Show despatch detail on complete"
         Height          =   375
         Left            =   2820
         TabIndex        =   11
         Top             =   0
         Width           =   1935
      End
      Begin VB.CommandButton cmdRefresh 
         Caption         =   "Refresh (F5)"
         Height          =   315
         Left            =   8760
         TabIndex        =   10
         ToolTipText     =   "Send Despatch"
         Top             =   0
         Width           =   1275
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   10140
         TabIndex        =   2
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1155
      End
      Begin MSComCtl2.DTPicker datDeadlineDate 
         Height          =   315
         Left            =   1080
         TabIndex        =   1
         ToolTipText     =   "When checked you are only shown deliveries with their deadline on the specified date"
         Top             =   0
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   84017153
         CurrentDate     =   37870
      End
      Begin VB.Label lblCaption 
         Caption         =   "Use Date:"
         Height          =   255
         Index           =   19
         Left            =   240
         TabIndex        =   4
         Top             =   0
         Width           =   1035
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDelivery 
      Bindings        =   "frmDelivery.frx":08FC
      Height          =   4215
      Index           =   0
      Left            =   120
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   120
      Width           =   14670
      _Version        =   196617
      BeveColorScheme =   1
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   0
      BackColorOdd    =   16773364
      RowHeight       =   370
      ExtraHeight     =   53
      Columns.Count   =   17
      Columns(0).Width=   1296
      Columns(0).Caption=   "DNote"
      Columns(0).Name =   "despatchnumber"
      Columns(0).DataField=   "despatchnumber"
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   10744793
      Columns(1).Width=   1296
      Columns(1).Caption=   "Job ID"
      Columns(1).Name =   "jobID"
      Columns(1).DataField=   "jobID"
      Columns(1).DataType=   17
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16239283
      Columns(2).Width=   1296
      Columns(2).Caption=   "Project"
      Columns(2).Name =   "projectnumber"
      Columns(2).DataField=   "projectnumber"
      Columns(2).FieldLen=   256
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   9040629
      Columns(3).Width=   1296
      Columns(3).Caption=   "P/O"
      Columns(3).Name =   "purchaseauthorisationnumber"
      Columns(3).DataField=   "purchaseauthorisationnumber"
      Columns(3).FieldLen=   256
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   33023
      Columns(4).Width=   3016
      Columns(4).Caption=   "Description"
      Columns(4).Name =   "description"
      Columns(4).DataField=   "description"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   3387
      Columns(5).Caption=   "Company"
      Columns(5).Name =   "company"
      Columns(5).DataField=   "companyname"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   1614
      Columns(6).Caption=   "Post Code"
      Columns(6).Name =   "postcode"
      Columns(6).DataField=   "postcode"
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "Date Created"
      Columns(7).Name =   "createddate"
      Columns(7).DataField=   "createddate"
      Columns(7).DataType=   7
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   794
      Columns(8).Caption=   "User"
      Columns(8).Name =   "creadtedby"
      Columns(8).DataField=   "createduser"
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(9).Width=   3069
      Columns(9).Caption=   "Deadline"
      Columns(9).Name =   "deadlinedate"
      Columns(9).DataField=   "deadlinedate"
      Columns(9).DataType=   7
      Columns(9).FieldLen=   256
      Columns(9).Locked=   -1  'True
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "Direction"
      Columns(10).Name=   "direction"
      Columns(10).DataField=   "direction"
      Columns(10).FieldLen=   256
      Columns(10).Locked=   -1  'True
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "despatcheduser"
      Columns(11).Name=   "despatcheduser"
      Columns(11).DataField=   "despatcheduser"
      Columns(11).FieldLen=   256
      Columns(12).Width=   2302
      Columns(12).Caption=   "Despatched"
      Columns(12).Name=   "despatcheddate"
      Columns(12).DataField=   "despatcheddate"
      Columns(12).DataType=   7
      Columns(12).FieldLen=   256
      Columns(12).Style=   1
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "flagurgent"
      Columns(13).Name=   "flagurgent"
      Columns(13).DataField=   "flagurgent"
      Columns(13).FieldLen=   256
      Columns(13).Locked=   -1  'True
      Columns(14).Width=   2646
      Columns(14).Caption=   "Method"
      Columns(14).Name=   "deliverymethod"
      Columns(14).DataField=   "deliverymethod"
      Columns(14).FieldLen=   256
      Columns(15).Width=   2143
      Columns(15).Caption=   "Delivered By"
      Columns(15).Name=   "deliveredby"
      Columns(15).DataField=   "deliveredby"
      Columns(15).FieldLen=   256
      Columns(16).Width=   1720
      Columns(16).Caption=   "Job Type"
      Columns(16).Name=   "typeofjob"
      Columns(16).DataField=   "typeofjob"
      Columns(16).FieldLen=   256
      _ExtentX        =   25876
      _ExtentY        =   7435
      _StockProps     =   79
      Caption         =   "Outgoing Despatches"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDelivery 
      Bindings        =   "frmDelivery.frx":0919
      Height          =   3255
      Index           =   1
      Left            =   120
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   4440
      Width           =   14670
      _Version        =   196617
      BeveColorScheme =   1
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      BackColorOdd    =   16773364
      RowHeight       =   370
      ExtraHeight     =   53
      Columns.Count   =   18
      Columns(0).Width=   1296
      Columns(0).Caption=   "DNote"
      Columns(0).Name =   "despatchnumber"
      Columns(0).DataField=   "despatchnumber"
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   10744793
      Columns(1).Width=   1296
      Columns(1).Caption=   "Job ID"
      Columns(1).Name =   "jobID"
      Columns(1).DataField=   "jobID"
      Columns(1).DataType=   17
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16239283
      Columns(2).Width=   1296
      Columns(2).Caption=   "Project"
      Columns(2).Name =   "projectnumber"
      Columns(2).DataField=   "projectnumber"
      Columns(2).FieldLen=   256
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   9040629
      Columns(3).Width=   1296
      Columns(3).Caption=   "P/O"
      Columns(3).Name =   "purchaseauthorisationnumber"
      Columns(3).DataField=   "purchaseauthorisationnumber"
      Columns(3).FieldLen=   256
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   33023
      Columns(4).Width=   3016
      Columns(4).Caption=   "Description"
      Columns(4).Name =   "description"
      Columns(4).DataField=   "description"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   3387
      Columns(5).Caption=   "Company"
      Columns(5).Name =   "company"
      Columns(5).DataField=   "companyname"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   1614
      Columns(6).Caption=   "Post Code"
      Columns(6).Name =   "postcode"
      Columns(6).DataField=   "postcode"
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "Date Created"
      Columns(7).Name =   "createddate"
      Columns(7).DataField=   "createddate"
      Columns(7).DataType=   7
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   794
      Columns(8).Caption=   "User"
      Columns(8).Name =   "creadtedby"
      Columns(8).DataField=   "createduser"
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(9).Width=   3069
      Columns(9).Caption=   "Deadline"
      Columns(9).Name =   "deadlinedate"
      Columns(9).DataField=   "deadlinedate"
      Columns(9).DataType=   7
      Columns(9).FieldLen=   256
      Columns(9).Locked=   -1  'True
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "Direction"
      Columns(10).Name=   "direction"
      Columns(10).DataField=   "direction"
      Columns(10).FieldLen=   256
      Columns(10).Locked=   -1  'True
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "despatcheduser"
      Columns(11).Name=   "despatcheduser"
      Columns(11).DataField=   "despatcheduser"
      Columns(11).FieldLen=   256
      Columns(12).Width=   2302
      Columns(12).Caption=   "Received Date"
      Columns(12).Name=   "despatcheddate"
      Columns(12).DataField=   "despatcheddate"
      Columns(12).DataType=   7
      Columns(12).FieldLen=   256
      Columns(12).Style=   1
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "flagurgent"
      Columns(13).Name=   "flagurgent"
      Columns(13).DataField=   "flagurgent"
      Columns(13).FieldLen=   256
      Columns(13).Locked=   -1  'True
      Columns(14).Width=   2646
      Columns(14).Caption=   "Method"
      Columns(14).Name=   "deliverymethod"
      Columns(14).DataField=   "deliverymethod"
      Columns(14).FieldLen=   256
      Columns(15).Width=   1984
      Columns(15).Caption=   "Delivered By"
      Columns(15).Name=   "deliveredby"
      Columns(15).DataField=   "deliveredby"
      Columns(15).FieldLen=   256
      Columns(16).Width=   2302
      Columns(16).Caption=   "Delivery For"
      Columns(16).Name=   "deliveryfor"
      Columns(16).DataField=   "deliveryfor"
      Columns(16).FieldLen=   256
      Columns(17).Width=   1720
      Columns(17).Caption=   "Job Type"
      Columns(17).Name=   "typeofjob"
      Columns(17).DataField=   "typeofjob"
      Columns(17).FieldLen=   256
      _ExtentX        =   25876
      _ExtentY        =   5741
      _StockProps     =   79
      Caption         =   "Incoming Despatches"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order By"
      Height          =   255
      Index           =   0
      Left            =   4320
      TabIndex        =   17
      Top             =   9060
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order By"
      Height          =   255
      Index           =   22
      Left            =   4320
      TabIndex        =   14
      Top             =   8640
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "frmDespatch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdClose_Click()
Unload Me

End Sub

Private Sub cmdCompleteDespatch_Click(Index As Integer)

If grdDelivery(Index).Row = grdDelivery(Index).Rows Then Exit Sub

Dim l_lngDespatchID As Long
l_lngDespatchID = GetData("despatch", "despatchID", "despatchnumber", grdDelivery(Index).Columns("despatchnumber").Text)

Dim l_datDespatchedDate  As Date
Dim l_varDeadlineDate As Variant
Dim l_strLateReason As String

l_datDespatchedDate = Now
    
Dim l_intMsg As Integer
    
If g_optPromptForLateDespatchReason = 1 Then
    
    
    l_varDeadlineDate = GetData("despatch", "deadlinedate", "despatchID", l_lngDespatchID)
    
    If IsDate(l_varDeadlineDate) Then
        If DateDiff("n", l_varDeadlineDate, Now) > 1 Then
            l_intMsg = MsgBox("Was this job actually late?", vbYesNo + vbQuestion)
            If l_intMsg = vbNo Then
                l_datDespatchedDate = l_varDeadlineDate
                If grdDelivery(Index).Columns("jobID").Text <> "" Then AddJobHistory Val(grdDelivery(Index).Columns("jobID").Text), "Answered job was not actually late when prompted"
            Else
                frmLateReason.Show vbModal
                l_strLateReason = frmLateReason.txtLateReason.Text
                
            End If
        End If
    End If
End If

If l_strLateReason <> "" Then
    SetData "despatch", "reasonjobwaslate", "despatchID", l_lngDespatchID, l_strLateReason
End If
    

If grdDelivery(Index).Columns("jobID").Text <> "" And Val(grdDelivery(Index).Columns("purchaseauthorisationnumber").Text) = 0 Then
    AddJobHistory grdDelivery(Index).Columns("jobID").Text, "Completed Despatch (" & l_datDespatchedDate & ") - " & grdDelivery(Index).Columns("despatchnumber").Text
    
    If l_strLateReason <> "" Then
        AddJobHistory grdDelivery(Index).Columns("jobID").Text, "Added late job reason: " & l_strLateReason
    End If
    
    Select Case Index
    Case 0
        'out
        
    Case 1
        'in
    End Select
    
End If

If grdDelivery(Index).Columns("despatcheddate").Text <> "" Then
    grdDelivery(Index).Columns("despatcheddate").Text = ""
    grdDelivery(Index).Columns("despatcheduser").Text = ""
Else
    grdDelivery(Index).Columns("despatcheddate").Text = l_datDespatchedDate
    grdDelivery(Index).Columns("despatcheduser").Text = g_strUserInitials
End If

DoEvents

grdDelivery(Index).Update



If chkShowDespatchOnClick.Value = 1 Then ShowDespatchDetail l_lngDespatchID

End Sub





Private Sub cmdRefresh_Click()
ShowDespatch "incomplete"
End Sub

Private Sub Form_Activate()
On Error Resume Next
Me.WindowState = vbMaximized
End Sub

Private Sub Form_Load()

PopulateCombo "deliveredby", ddnDeliveredBy
PopulateCombo "despatchmethod", ddnMethod

If g_optOrderByNumbers = 1 Then
    cmbOrderBy(0).Text = "despatchnumber"
Else
    cmbOrderBy(0).Text = "deadlinedate"
End If

cmbDirection(0).ListIndex = 0

If g_optOrderByNumbers = 1 Then
    cmbOrderBy(1).Text = "despatchnumber"
Else
    cmbOrderBy(1).Text = "deadlinedate"
End If
cmbDirection(1).ListIndex = 0

grdDelivery(0).StyleSets.Add "Hire"
grdDelivery(0).StyleSets("Hire").BackColor = &HFFC0C0
grdDelivery(1).StyleSets.Add "Hire"
grdDelivery(1).StyleSets("Hire").BackColor = &HFFC0C0

grdDelivery(0).StyleSets.Add "Purchase"
grdDelivery(0).StyleSets("Purchase").BackColor = &H80FF&
grdDelivery(1).StyleSets.Add "Purchase"
grdDelivery(1).StyleSets("Purchase").BackColor = &H80FF&

grdDelivery(0).StyleSets.Add "Edit"
grdDelivery(0).StyleSets("Edit").BackColor = &HFFC0FF
grdDelivery(1).StyleSets.Add "Edit"
grdDelivery(1).StyleSets("Edit").BackColor = &HFFC0FF

grdDelivery(0).StyleSets.Add "VT"
grdDelivery(0).StyleSets("VT").BackColor = &HC0FFC0
grdDelivery(1).StyleSets.Add "VT"
grdDelivery(1).StyleSets("VT").BackColor = &HC0FFC0

grdDelivery(0).StyleSets.Add "Dubbing"
grdDelivery(0).StyleSets("Dubbing").BackColor = &HC0FFC0
grdDelivery(1).StyleSets.Add "Dubbing"
grdDelivery(1).StyleSets("Dubbing").BackColor = &HC0FFC0

CenterForm Me

End Sub


Private Sub Form_Resize()
On Error Resume Next

picFooter.Top = Me.ScaleHeight - picFooter.Height - 120
picFooter.Left = Me.ScaleWidth - picFooter.Width - 120

grdDelivery(0).Height = (Me.ScaleHeight - picFooter.Height - 120 - 120 - grdDelivery(0).Top) / 2
grdDelivery(0).Width = Me.ScaleWidth - 240

grdDelivery(1).Height = (Me.ScaleHeight - picFooter.Height - 120 - 120 - grdDelivery(0).Top - 240) / 2
grdDelivery(1).Width = Me.ScaleWidth - 240
grdDelivery(1).Top = grdDelivery(0).Top + grdDelivery(0).Height + 120

End Sub

Private Sub Form_Unload(Cancel As Integer)

'SaveColumnLayout grdDelivery(0), "Outgoing Despatch"
'SaveColumnLayout grdDelivery(1), "Incoming Despatch"

End Sub

Private Sub grdDelivery_BeforeDelete(Index As Integer, Cancel As Integer, DispPromptMsg As Integer)

Dim l_intMsg As Integer
l_intMsg = MsgBox("Do you really want to delete this despatch record?", vbYesNo + vbQuestion)

If l_intMsg = vbNo Then
    Cancel = True
    Exit Sub
End If

If grdDelivery(Index).Columns("jobID").Text <> "" Then
    AddJobHistory grdDelivery(Index).Columns("jobID").Text, "Deleted despatch " & grdDelivery(Index).Columns("despatchnumber")
End If

DispPromptMsg = 0

Dim l_strSQL As String, l_lngDespatchID As Long
l_lngDespatchID = GetData("despatch", "despatchID", "despatchnumber", grdDelivery(Index).Columns("despatchnumber").Text)

l_strSQL = "DELETE FROM despatch WHERE despatchID = '" & l_lngDespatchID & "';"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_strSQL = "DELETE FROM despatchdetail WHERE despatchID = '" & l_lngDespatchID & "';"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

Cancel = True

End Sub



Private Sub grdDelivery_BtnClick(Index As Integer)

Dim l_lngDespatchID As Long
l_lngDespatchID = GetData("despatch", "despatchID", "despatchnumber", grdDelivery(Index).Columns("despatchnumber").Text)

cmdCompleteDespatch_Click Index

CompleteDespatch l_lngDespatchID

DoEvents

End Sub

Private Sub grdDelivery_DblClick(Index As Integer)


Dim l_lngDespatchID As Long
l_lngDespatchID = GetData("despatch", "despatchID", "despatchnumber", Val(grdDelivery(Index).Columns("despatchnumber").Text))

If grdDelivery(0).Columns("jobID").Text <> "" Then
    ShowDespatchForJob grdDelivery(Index).Columns("jobID").Text
End If

ShowDespatchDetail l_lngDespatchID


End Sub


Private Sub grdDelivery_HeadClick(Index As Integer, ByVal ColIndex As Integer)

If cmbOrderBy(Index).Text <> grdDelivery(Index).Columns(ColIndex).DataField Then
    cmbOrderBy(Index).Text = grdDelivery(Index).Columns(ColIndex).DataField
Else
    cmbDirection(Index).ListIndex = 1 - cmbDirection(Index).ListIndex
End If

ShowDespatch "mdibuttonclick"



End Sub

Private Sub grdDelivery_InitColumnProps(Index As Integer)

On Error GoTo grdDelivery_Init_Error

grdDelivery(Index).Columns("method").DropDownHwnd = ddnMethod.hWnd
grdDelivery(Index).Columns("deliveredby").DropDownHwnd = ddnDeliveredBy.hWnd

Exit Sub

grdDelivery_Init_Error:

Select Case Index
Case 0
'    LoadColumnLayout grdDelivery(0), "Outgoing Despatch"
Case 1
'    LoadColumnLayout grdDelivery(1), "Incoming Despatch"
End Select

'Resume Next



End Sub

Private Sub grdDelivery_RowLoaded(Index As Integer, ByVal bookmark As Variant)

'On Error GoTo grdDelivery_RowLoaded_Error

Dim l_strTextToAdd As String

l_strTextToAdd = GetData("despatch", "despatcheddate", "despatchnumber", grdDelivery(Index).Columns("despatchnumber").Text)
If IsDate(l_strTextToAdd) Then grdDelivery(Index).Columns("despatcheddate").Text = l_strTextToAdd

grdDelivery(Index).Columns("despatcheddate").CellStyleSet grdDelivery(Index).Columns("typeofjob").Text
'grdDelivery(Index).Columns("deliverymethod").CellStyleSet grdDelivery(Index).Columns("typeofjob").Text
grdDelivery(Index).Columns("typeofjob").CellStyleSet grdDelivery(Index).Columns("typeofjob").Text

Exit Sub

grdDelivery_RowLoaded_Error:

Select Case Index
Case 0
'    LoadColumnLayout grdDelivery(0), "Outgoing Despatch"
Case 1
'    LoadColumnLayout grdDelivery(1), "Incoming Despatch"
End Select

Resume



End Sub
