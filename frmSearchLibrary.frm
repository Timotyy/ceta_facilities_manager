VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmSearchLibrary 
   Caption         =   "Library Search"
   ClientHeight    =   14730
   ClientLeft      =   3570
   ClientTop       =   2745
   ClientWidth     =   20355
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSearchLibrary.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   14730
   ScaleWidth      =   20355
   WindowState     =   2  'Maximized
   Begin VB.TextBox cmbBox 
      Height          =   315
      Left            =   6180
      TabIndex        =   209
      ToolTipText     =   "Notes"
      Top             =   3660
      Width           =   1935
   End
   Begin VB.TextBox cmbShelf 
      Height          =   315
      Left            =   6180
      TabIndex        =   208
      ToolTipText     =   "Notes"
      Top             =   3300
      Width           =   1935
   End
   Begin VB.CheckBox chkUsedBlankStock 
      Alignment       =   1  'Right Justify
      Caption         =   "Do 'Used Blank Stock' Search"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   13020
      TabIndex        =   207
      Top             =   6120
      Width           =   3435
   End
   Begin VB.CheckBox chkUnusedBlankStock 
      Alignment       =   1  'Right Justify
      Caption         =   "Do 'Unused Blank Stock' Search"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8760
      TabIndex        =   206
      Tag             =   "NOCLEAR"
      Top             =   6120
      Width           =   3795
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   31
      Left            =   5940
      TabIndex        =   204
      Top             =   2640
      Width           =   195
   End
   Begin VB.TextBox txtSerialNumber 
      Height          =   315
      Left            =   6180
      TabIndex        =   203
      ToolTipText     =   "Notes"
      Top             =   2580
      Width           =   1935
   End
   Begin VB.Frame Frame5 
      BorderStyle     =   0  'None
      Height          =   195
      Left            =   8160
      TabIndex        =   200
      Top             =   2640
      Width           =   495
      Begin VB.OptionButton optSerialNumber 
         Caption         =   "Option1"
         Height          =   195
         Left            =   0
         TabIndex        =   202
         Top             =   0
         Width           =   195
      End
      Begin VB.OptionButton optNullSerialNumber 
         Caption         =   "Option1"
         Height          =   195
         Left            =   300
         TabIndex        =   201
         Top             =   0
         Width           =   195
      End
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   30
      Left            =   1620
      TabIndex        =   198
      Top             =   3750
      Width           =   195
   End
   Begin VB.ComboBox cmbField3 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   1860
      TabIndex        =   196
      Tag             =   "CLEAR"
      ToolTipText     =   "The Clip Codec"
      Top             =   3720
      Visible         =   0   'False
      Width           =   2715
   End
   Begin VB.TextBox txtDirectSQL 
      Height          =   345
      Left            =   14520
      TabIndex        =   194
      ToolTipText     =   "Searches on several fields at once: Company Name, Product, Title, Sub Title, Series, Set, and Episode"
      Top             =   5280
      Width           =   2655
   End
   Begin VB.CommandButton cmdCleanUp 
      Caption         =   "Clean Up !"
      Height          =   375
      Left            =   8400
      TabIndex        =   193
      Top             =   4500
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.CommandButton cmdUndeleteAllTapes 
      Caption         =   "Undelete all Tapes in Grid"
      Height          =   315
      Left            =   120
      MaskColor       =   &H80000010&
      TabIndex        =   192
      Top             =   5220
      Visible         =   0   'False
      Width           =   2235
   End
   Begin VB.CheckBox chkDeletedTapes 
      Alignment       =   1  'Right Justify
      Caption         =   "Do 'Deleted Tapes' Search"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8760
      TabIndex        =   191
      Top             =   5280
      Width           =   3795
   End
   Begin VB.CommandButton cmdAssignToLibrary 
      Caption         =   "Assign Grid to Library"
      Height          =   315
      Left            =   2460
      MaskColor       =   &H80000010&
      TabIndex        =   190
      Top             =   4860
      Width           =   2175
   End
   Begin VB.CommandButton cmdRemoveFromLibrary 
      Caption         =   "De-assign Grid from Library"
      Height          =   315
      Left            =   120
      MaskColor       =   &H80000010&
      TabIndex        =   189
      Top             =   4860
      Width           =   2235
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   29
      Left            =   12360
      TabIndex        =   188
      Top             =   4740
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   28
      Left            =   12360
      TabIndex        =   187
      Top             =   4380
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   27
      Left            =   12360
      TabIndex        =   186
      Top             =   4020
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   26
      Left            =   12360
      TabIndex        =   185
      Top             =   3720
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   25
      Left            =   12360
      TabIndex        =   184
      Top             =   3420
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   24
      Left            =   12360
      TabIndex        =   183
      Top             =   3120
      Width           =   255
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   23
      Left            =   12360
      TabIndex        =   182
      Top             =   2820
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   22
      Left            =   12360
      TabIndex        =   181
      Top             =   2520
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   21
      Left            =   12360
      TabIndex        =   180
      Top             =   2220
      Width           =   195
   End
   Begin VB.CheckBox chkSurroundME 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Surround M&&E "
      Height          =   255
      Left            =   10860
      TabIndex        =   179
      Top             =   3120
      Width           =   1365
   End
   Begin VB.CheckBox chkSurroundMain 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Surround Main "
      Height          =   255
      Left            =   10860
      TabIndex        =   178
      Top             =   2520
      Width           =   1365
   End
   Begin VB.CheckBox chkLtRtME 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Lt Rt M&&E "
      Height          =   255
      Left            =   10860
      TabIndex        =   177
      Top             =   4320
      Width           =   1365
   End
   Begin VB.CheckBox chkLtRtMain 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Lt Rt Main "
      Height          =   255
      Left            =   10860
      TabIndex        =   176
      Top             =   4020
      Width           =   1365
   End
   Begin VB.CheckBox chkStereoME 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Stereo M&&E "
      Height          =   255
      Left            =   10860
      TabIndex        =   175
      Top             =   2820
      Width           =   1365
   End
   Begin VB.CheckBox chkStereoMain 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Stereo Main "
      Height          =   255
      Left            =   10860
      TabIndex        =   174
      Top             =   2220
      Width           =   1365
   End
   Begin VB.CheckBox chkStereoMMN 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Stereo MMN"
      Height          =   255
      Left            =   10860
      TabIndex        =   173
      Top             =   3420
      Width           =   1365
   End
   Begin VB.CheckBox chkSurroundMMN 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Surround MMN"
      Height          =   255
      Left            =   10860
      TabIndex        =   172
      Top             =   3720
      Width           =   1365
   End
   Begin VB.CheckBox chkTapeTextless 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Textless"
      Height          =   255
      Left            =   10860
      TabIndex        =   171
      Top             =   4740
      Width           =   1245
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   20
      Left            =   5940
      TabIndex        =   170
      Top             =   4800
      Width           =   195
   End
   Begin VB.ComboBox cmbField2 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   1860
      TabIndex        =   169
      Tag             =   "CLEAR"
      ToolTipText     =   "The Clip Codec"
      Top             =   3360
      Visible         =   0   'False
      Width           =   2715
   End
   Begin VB.ComboBox cmbField1 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   1860
      TabIndex        =   168
      Tag             =   "CLEAR"
      ToolTipText     =   "The Clip Codec"
      Top             =   3000
      Visible         =   0   'False
      Width           =   2715
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   19
      Left            =   1620
      TabIndex        =   167
      Top             =   3390
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   18
      Left            =   1620
      TabIndex        =   166
      Top             =   3030
      Width           =   195
   End
   Begin VB.CheckBox chkFalseRecord 
      Alignment       =   1  'Right Justify
      Caption         =   "Do 'False Record' Search"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   13020
      TabIndex        =   165
      Top             =   5700
      Width           =   3435
   End
   Begin VB.CheckBox chkOutOfLibrary 
      Alignment       =   1  'Right Justify
      Caption         =   "Do 'Out of Library' Search"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8760
      TabIndex        =   164
      Top             =   5700
      Width           =   3795
   End
   Begin VB.CheckBox chkExact 
      BackColor       =   &H00C0C0C0&
      Height          =   255
      Index           =   3
      Left            =   12660
      TabIndex        =   163
      Top             =   1560
      Width           =   255
   End
   Begin VB.CheckBox chkExact 
      BackColor       =   &H00C0C0C0&
      Height          =   255
      Index           =   2
      Left            =   12660
      TabIndex        =   162
      Top             =   1200
      Width           =   255
   End
   Begin VB.CheckBox chkExact 
      BackColor       =   &H00C0C0C0&
      Height          =   255
      Index           =   1
      Left            =   12660
      TabIndex        =   161
      Top             =   840
      Width           =   255
   End
   Begin VB.CheckBox chkExact 
      BackColor       =   &H00C0C0C0&
      Height          =   255
      Index           =   0
      Left            =   12660
      TabIndex        =   160
      Top             =   480
      Width           =   255
   End
   Begin VB.CommandButton cmdBulkUpdateSelected 
      Caption         =   "Bulk Update Selected Rows"
      Height          =   315
      Left            =   2460
      MaskColor       =   &H80000010&
      TabIndex        =   158
      Top             =   4500
      Width           =   2175
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   17
      Left            =   5940
      TabIndex        =   157
      Top             =   3720
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   16
      Left            =   5940
      TabIndex        =   156
      Top             =   3360
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.Frame frmBox 
      BorderStyle     =   0  'None
      Height          =   195
      Left            =   8160
      TabIndex        =   151
      Top             =   3720
      Width           =   495
      Begin VB.OptionButton optNullBox 
         Caption         =   "Option1"
         Height          =   195
         Left            =   300
         TabIndex        =   153
         Top             =   0
         Width           =   195
      End
      Begin VB.OptionButton optBox 
         Caption         =   "Option1"
         Height          =   195
         Left            =   0
         TabIndex        =   152
         Top             =   0
         Width           =   195
      End
   End
   Begin VB.ComboBox OLDcmbBox 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   2700
      TabIndex        =   149
      ToolTipText     =   "The Current Location of the Tape"
      Top             =   11160
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.CommandButton cmdBulkUpdate 
      Caption         =   "Bulk Update Grid"
      Height          =   315
      Left            =   2460
      MaskColor       =   &H80000010&
      TabIndex        =   148
      Top             =   4140
      Width           =   2175
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   15
      Left            =   5940
      TabIndex        =   147
      Top             =   6240
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   14
      Left            =   5940
      TabIndex        =   146
      Top             =   5880
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   13
      Left            =   5940
      TabIndex        =   145
      Top             =   5520
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   12
      Left            =   5940
      TabIndex        =   144
      Top             =   5160
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   11
      Left            =   5940
      TabIndex        =   143
      Top             =   4440
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   10
      Left            =   5940
      TabIndex        =   142
      Top             =   4080
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   9
      Left            =   11400
      TabIndex        =   141
      Top             =   1560
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   8
      Left            =   11400
      TabIndex        =   140
      Top             =   1200
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   7
      Left            =   11400
      TabIndex        =   139
      Top             =   840
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   6
      Left            =   11400
      TabIndex        =   138
      Top             =   480
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   5
      Left            =   5940
      TabIndex        =   137
      Top             =   3000
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   4
      Left            =   5940
      TabIndex        =   136
      Top             =   2280
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   3
      Left            =   5940
      TabIndex        =   135
      Top             =   1920
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   2
      Left            =   5940
      TabIndex        =   134
      Top             =   1560
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   1
      Left            =   5940
      TabIndex        =   133
      Top             =   1200
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   255
      Index           =   0
      Left            =   5940
      TabIndex        =   132
      Top             =   840
      Width           =   195
   End
   Begin VB.Frame Frame3 
      BorderStyle     =   0  'None
      Height          =   195
      Left            =   8160
      TabIndex        =   129
      Top             =   2280
      Width           =   495
      Begin VB.OptionButton optNullSeriesID 
         Caption         =   "Option1"
         Height          =   195
         Left            =   300
         TabIndex        =   131
         Top             =   0
         Width           =   195
      End
      Begin VB.OptionButton optSeriesID 
         Caption         =   "Option1"
         Height          =   195
         Left            =   0
         TabIndex        =   130
         Top             =   0
         Width           =   195
      End
   End
   Begin VB.TextBox txtSeriesID 
      Height          =   315
      Left            =   6180
      TabIndex        =   127
      ToolTipText     =   "Notes"
      Top             =   2220
      Width           =   1935
   End
   Begin VB.CheckBox chkShowCustomFields 
      Alignment       =   1  'Right Justify
      Caption         =   "Show Custom Fields"
      Height          =   195
      Left            =   120
      TabIndex        =   126
      ToolTipText     =   "Sub Title"
      Top             =   5880
      Width           =   2175
   End
   Begin VB.TextBox txtLocationJobID 
      Height          =   315
      Left            =   6180
      TabIndex        =   124
      ToolTipText     =   "Notes"
      Top             =   1860
      Width           =   1935
   End
   Begin VB.CheckBox chkShowRef 
      Alignment       =   1  'Right Justify
      Caption         =   "Show Reference Field"
      Height          =   195
      Left            =   120
      TabIndex        =   123
      ToolTipText     =   "Sub Title"
      Top             =   5580
      Width           =   2175
   End
   Begin VB.TextBox txtField2 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   1860
      TabIndex        =   122
      Top             =   3360
      Width           =   2715
   End
   Begin VB.TextBox txtField1 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   1860
      TabIndex        =   121
      Top             =   3000
      Width           =   2715
   End
   Begin VB.TextBox txtReference 
      Height          =   315
      Left            =   6180
      TabIndex        =   117
      ToolTipText     =   "Tapes sub title (if available)"
      Top             =   1500
      Width           =   4455
   End
   Begin VB.ComboBox cmbStockType 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6180
      TabIndex        =   115
      ToolTipText     =   "Format of the tape"
      Top             =   4740
      Width           =   1935
   End
   Begin VB.CommandButton cmdBulkBarcodePrint 
      Caption         =   "Bulk Barcode Print"
      Height          =   315
      Left            =   13020
      TabIndex        =   114
      Top             =   4800
      Width           =   2355
   End
   Begin VB.CommandButton cmdCompanyUpdate 
      Caption         =   "Update Ownership Records"
      Height          =   315
      Left            =   120
      MaskColor       =   &H80000010&
      TabIndex        =   113
      Top             =   4140
      Width           =   2235
   End
   Begin VB.CommandButton cmdDeleteAllTapes 
      Caption         =   "Delete all Tapes in Grid"
      Height          =   315
      Left            =   120
      MaskColor       =   &H80000010&
      TabIndex        =   112
      Top             =   4500
      Width           =   2235
   End
   Begin VB.ComboBox cmbEpisodeTo 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   11640
      TabIndex        =   107
      ToolTipText     =   "Video Standard of the tape"
      Top             =   1500
      Width           =   975
   End
   Begin VB.CommandButton cmdClearOptions 
      Caption         =   "CLR"
      Height          =   315
      Left            =   8160
      TabIndex        =   105
      Top             =   4020
      Width           =   495
   End
   Begin VB.Frame frmShelf 
      BorderStyle     =   0  'None
      Height          =   195
      Left            =   8160
      TabIndex        =   102
      Top             =   3360
      Width           =   495
      Begin VB.OptionButton optShelf 
         Caption         =   "Option1"
         Height          =   195
         Left            =   0
         TabIndex        =   104
         Top             =   0
         Width           =   195
      End
      Begin VB.OptionButton optNullShelf 
         Caption         =   "Option1"
         Height          =   195
         Left            =   300
         TabIndex        =   103
         Top             =   0
         Width           =   195
      End
   End
   Begin VB.Frame frmLocation 
      BorderStyle     =   0  'None
      Height          =   195
      Left            =   8160
      TabIndex        =   99
      Top             =   3000
      Width           =   495
      Begin VB.OptionButton optNullLocation 
         Caption         =   "Option1"
         Height          =   195
         Left            =   300
         TabIndex        =   101
         Top             =   0
         Width           =   195
      End
      Begin VB.OptionButton optLocation 
         Caption         =   "Option1"
         Height          =   195
         Left            =   0
         TabIndex        =   100
         Top             =   0
         Width           =   255
      End
   End
   Begin VB.ComboBox OLDcmbShelf 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   2700
      TabIndex        =   94
      ToolTipText     =   "The Current Location of the Tape"
      Top             =   11580
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.ComboBox cmbEpisode 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   11640
      TabIndex        =   93
      ToolTipText     =   "Video Standard of the tape"
      Top             =   1140
      Width           =   975
   End
   Begin VB.ComboBox cmbSet 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   11640
      TabIndex        =   92
      ToolTipText     =   "Video Standard of the tape"
      Top             =   780
      Width           =   975
   End
   Begin VB.ComboBox cmbSeries 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   11640
      TabIndex        =   88
      ToolTipText     =   "Video Standard of the tape"
      Top             =   420
      Width           =   975
   End
   Begin VB.ComboBox cmbAspectRatio 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6180
      TabIndex        =   85
      ToolTipText     =   "Video Standard of the tape"
      Top             =   5820
      Width           =   1935
   End
   Begin VB.ComboBox cmbAudioStandard 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6180
      TabIndex        =   84
      ToolTipText     =   "What Copy Type is the tape?"
      Top             =   6180
      Width           =   1935
   End
   Begin VB.ComboBox cmbStandard 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6180
      TabIndex        =   72
      ToolTipText     =   "Video Standard of the tape"
      Top             =   5100
      Width           =   1935
   End
   Begin VB.ComboBox cmbFormat 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6180
      TabIndex        =   71
      ToolTipText     =   "Format of the tape"
      Top             =   4380
      Width           =   1935
   End
   Begin VB.ComboBox cmbVersion 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6180
      TabIndex        =   70
      ToolTipText     =   "Version of the tape"
      Top             =   4020
      Width           =   1935
   End
   Begin VB.ComboBox cmbCopyType 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6180
      TabIndex        =   69
      ToolTipText     =   "What Copy Type is the tape?"
      Top             =   5460
      Width           =   1935
   End
   Begin VB.ComboBox cmbLocation 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6180
      TabIndex        =   68
      ToolTipText     =   "The tapes current location"
      Top             =   2940
      Width           =   1935
   End
   Begin VB.TextBox txtSubtitle 
      Height          =   315
      Left            =   6180
      TabIndex        =   67
      ToolTipText     =   "Tapes sub title (if available)"
      Top             =   1140
      Width           =   4455
   End
   Begin VB.CommandButton cmdGetFields 
      Caption         =   "Get Fields"
      Height          =   315
      Left            =   10680
      TabIndex        =   65
      Top             =   10500
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.TextBox txtMasterSearch 
      Height          =   345
      Left            =   14520
      TabIndex        =   63
      ToolTipText     =   "Searches on several fields at once: Company Name, Product, Title, Sub Title, Series, Set, and Episode"
      Top             =   4080
      Width           =   2655
   End
   Begin VB.OptionButton optSelect 
      Caption         =   "Tag Select"
      Height          =   195
      Index           =   1
      Left            =   13080
      TabIndex        =   62
      Top             =   4500
      Value           =   -1  'True
      Width           =   1275
   End
   Begin VB.OptionButton optSelect 
      Caption         =   "Range Select"
      Height          =   195
      Index           =   0
      Left            =   14520
      TabIndex        =   61
      Top             =   4500
      Width           =   1335
   End
   Begin VB.Frame Frame4 
      Caption         =   "Media Created Date..."
      Height          =   1035
      Left            =   2400
      TabIndex        =   30
      Top             =   840
      Width           =   2235
      Begin MSComCtl2.DTPicker datCreatedDateFrom 
         Height          =   315
         Left            =   600
         TabIndex        =   4
         Tag             =   "NOCLEAR"
         ToolTipText     =   "The date the tape was created"
         Top             =   240
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   169082881
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datCreatedDateTo 
         Height          =   315
         Left            =   600
         TabIndex        =   5
         Tag             =   "NOCLEAR"
         ToolTipText     =   "The date the job was booked"
         Top             =   600
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   169082881
         CurrentDate     =   37870
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   32
         Top             =   600
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "From"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   31
         Top             =   240
         Width           =   1035
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Media Modified Date..."
      Height          =   1035
      Left            =   120
      TabIndex        =   33
      Top             =   840
      Width           =   2235
      Begin MSComCtl2.DTPicker datModifiedDateFrom 
         Height          =   315
         Left            =   600
         TabIndex        =   2
         Tag             =   "NOCLEAR"
         ToolTipText     =   "The date the tape was modified"
         Top             =   240
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   169082881
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datModifiedDateTo 
         Height          =   315
         Left            =   600
         TabIndex        =   3
         Tag             =   "NOCLEAR"
         ToolTipText     =   "The deadline date"
         Top             =   600
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   169082881
         CurrentDate     =   37870
      End
      Begin VB.Label lblCaption 
         Caption         =   "From"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   35
         Top             =   240
         Width           =   735
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   34
         Top             =   600
         Width           =   855
      End
   End
   Begin VB.CommandButton cmdSelectAll 
      Caption         =   "Select All Rows"
      Height          =   315
      Left            =   2700
      TabIndex        =   60
      ToolTipText     =   "Select All..."
      Top             =   6240
      Width           =   1515
   End
   Begin VB.Frame fraEventDate 
      Caption         =   "Event Date"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   15060
      TabIndex        =   52
      Top             =   2640
      Width           =   2115
      Begin MSComCtl2.DTPicker datEventDateFrom 
         Height          =   315
         Left            =   540
         TabIndex        =   53
         Tag             =   "NOCLEAR"
         ToolTipText     =   "The date the tape was created"
         Top             =   240
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         _Version        =   393216
         CalendarTitleBackColor=   -2147483643
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   169082881
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datEventDateTo 
         Height          =   315
         Left            =   540
         TabIndex        =   54
         Tag             =   "NOCLEAR"
         ToolTipText     =   "The date the job was booked"
         Top             =   600
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         _Version        =   393216
         CalendarTitleBackColor=   -2147483643
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   169082881
         CurrentDate     =   37870
      End
      Begin VB.Label lblCaption 
         Caption         =   "From"
         Height          =   255
         Index           =   28
         Left            =   120
         TabIndex        =   56
         Top             =   240
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Height          =   255
         Index           =   21
         Left            =   120
         TabIndex        =   55
         Top             =   600
         Width           =   1035
      End
   End
   Begin VB.Frame fraAddToJob 
      Caption         =   "Add Selected..."
      Height          =   1095
      Left            =   12960
      TabIndex        =   51
      Top             =   2640
      Width           =   2055
      Begin VB.CommandButton cmdAddSelectedToDub 
         Caption         =   "...as a dubbing master"
         Height          =   315
         Left            =   120
         TabIndex        =   20
         ToolTipText     =   "Adds the currently selected tapes to the currently open job as master (M) rows on the dubbing requirements section"
         Top             =   300
         Width           =   1815
      End
      Begin VB.CommandButton cmdAddToRequiredMedia 
         Caption         =   "...as media required"
         Height          =   315
         Left            =   120
         TabIndex        =   21
         ToolTipText     =   "Adds the currently selected tapes to the currently open job as media required."
         Top             =   660
         Width           =   1815
      End
   End
   Begin VB.CheckBox chkEvents 
      Alignment       =   1  'Right Justify
      Caption         =   "Use events in search"
      Height          =   255
      Left            =   15180
      TabIndex        =   12
      ToolTipText     =   "Use additional search criteria"
      Top             =   120
      Width           =   1815
   End
   Begin VB.Frame fraEventSearch 
      Caption         =   "Library Events"
      Enabled         =   0   'False
      Height          =   2535
      Left            =   12960
      TabIndex        =   28
      Top             =   60
      Width           =   4215
      Begin VB.TextBox txtEventDuration 
         BackColor       =   &H00C0FFC0&
         Height          =   285
         Left            =   3000
         TabIndex        =   58
         ToolTipText     =   "Editor of the tape"
         Top             =   2160
         Width           =   1035
      End
      Begin VB.CheckBox chkOnlyClocked 
         Caption         =   "Show only items with clock number"
         Height          =   195
         Left            =   1020
         TabIndex        =   17
         ToolTipText     =   "Check to view items with clock number"
         Top             =   1620
         Width           =   2835
      End
      Begin VB.ComboBox cmbEventAspect 
         Height          =   315
         Left            =   1020
         TabIndex        =   14
         ToolTipText     =   "Aspect ratio of the tape?"
         Top             =   600
         Width           =   3075
      End
      Begin VB.TextBox txtEventEditor 
         Height          =   285
         Left            =   1020
         TabIndex        =   19
         ToolTipText     =   "Editor of the tape"
         Top             =   2160
         Width           =   1755
      End
      Begin VB.TextBox txtEventNotes 
         Height          =   285
         Left            =   1020
         TabIndex        =   18
         ToolTipText     =   "Notes"
         Top             =   1860
         Width           =   1755
      End
      Begin VB.TextBox txtEventClockNumber 
         Height          =   285
         Left            =   1020
         TabIndex        =   16
         ToolTipText     =   "Clock number"
         Top             =   1320
         Width           =   3075
      End
      Begin VB.ComboBox cmbEventType 
         Height          =   315
         Left            =   1020
         TabIndex        =   15
         ToolTipText     =   "What Copy Type is the tape?"
         Top             =   960
         Width           =   3075
      End
      Begin VB.TextBox txtEventTitle 
         BackColor       =   &H00C0FFC0&
         Height          =   285
         Left            =   1020
         TabIndex        =   13
         ToolTipText     =   "Title of the tape"
         Top             =   300
         Width           =   3075
      End
      Begin VB.Label lblCaption 
         Caption         =   "Event Duration"
         Height          =   255
         Index           =   29
         Left            =   3000
         TabIndex        =   59
         Top             =   1860
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Aspect"
         Height          =   255
         Index           =   27
         Left            =   120
         TabIndex        =   50
         Top             =   600
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Editor"
         Height          =   255
         Index           =   26
         Left            =   120
         TabIndex        =   49
         Top             =   2160
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Notes"
         Height          =   255
         Index           =   25
         Left            =   120
         TabIndex        =   48
         Top             =   1860
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Clock #"
         Height          =   255
         Index           =   24
         Left            =   120
         TabIndex        =   47
         Top             =   1320
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Type"
         Height          =   255
         Index           =   23
         Left            =   120
         TabIndex        =   46
         Top             =   960
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Event Title"
         Height          =   255
         Index           =   22
         Left            =   120
         TabIndex        =   45
         Top             =   300
         Width           =   1035
      End
   End
   Begin VB.CheckBox chktitle 
      Alignment       =   1  'Right Justify
      Caption         =   "Title"
      Height          =   195
      Left            =   15240
      TabIndex        =   10
      ToolTipText     =   "Title"
      Top             =   3840
      Width           =   735
   End
   Begin VB.CheckBox chkSubtitle 
      Alignment       =   1  'Right Justify
      Caption         =   "Sub Title"
      Height          =   195
      Left            =   16140
      TabIndex        =   11
      ToolTipText     =   "Sub Title"
      Top             =   3840
      Width           =   975
   End
   Begin VB.Frame Frame2 
      Caption         =   "ID Range"
      Height          =   1035
      Left            =   120
      TabIndex        =   40
      Top             =   1920
      Width           =   4515
      Begin VB.TextBox txtProjectIDTo 
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   3180
         TabIndex        =   7
         ToolTipText     =   "The unique identifier for this job (End of range)"
         Top             =   300
         Width           =   1215
      End
      Begin VB.TextBox txtProjectIDFrom 
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   1380
         TabIndex        =   6
         ToolTipText     =   "The unique identifier for this job (Start of range)"
         Top             =   300
         Width           =   1215
      End
      Begin VB.TextBox txtJobIDTo 
         BackColor       =   &H00FFC0C0&
         Height          =   285
         Left            =   3180
         TabIndex        =   9
         ToolTipText     =   "The unique identifier for this job (End of range)"
         Top             =   660
         Width           =   1215
      End
      Begin VB.TextBox txtJobIDFrom 
         BackColor       =   &H00FFC0C0&
         Height          =   285
         Left            =   1380
         TabIndex        =   8
         ToolTipText     =   "The unique identifier for this job (Start of range)"
         Top             =   660
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Project # From"
         Height          =   255
         Index           =   19
         Left            =   120
         TabIndex        =   44
         Top             =   300
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Height          =   255
         Index           =   18
         Left            =   2760
         TabIndex        =   43
         Top             =   300
         Width           =   255
      End
      Begin VB.Label lblCaption 
         Caption         =   "Job ID From"
         Height          =   255
         Index           =   17
         Left            =   120
         TabIndex        =   42
         Top             =   660
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "To"
         Height          =   255
         Index           =   16
         Left            =   2760
         TabIndex        =   41
         Top             =   660
         Width           =   255
      End
   End
   Begin VB.PictureBox picFooter 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   2580
      ScaleHeight     =   315
      ScaleWidth      =   14655
      TabIndex        =   38
      Top             =   9300
      Width           =   14655
      Begin VB.ComboBox cmbOrderBy 
         Height          =   315
         Left            =   5160
         Locked          =   -1  'True
         TabIndex        =   110
         Tag             =   "NOCLEAR"
         Text            =   "cmbOrderBy"
         ToolTipText     =   "Order by"
         Top             =   0
         Width           =   2235
      End
      Begin VB.ComboBox cmbDirection 
         Height          =   315
         ItemData        =   "frmSearchLibrary.frx":08CA
         Left            =   7440
         List            =   "frmSearchLibrary.frx":08D4
         Style           =   2  'Dropdown List
         TabIndex        =   109
         ToolTipText     =   "View order"
         Top             =   0
         Width           =   1095
      End
      Begin VB.CommandButton cmdExport 
         Caption         =   "Export"
         Height          =   315
         Left            =   8700
         TabIndex        =   57
         ToolTipText     =   "Export the results to a text file"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   9900
         TabIndex        =   24
         ToolTipText     =   "Print the Search results"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdRun 
         Caption         =   "Run"
         Height          =   315
         Left            =   3600
         TabIndex        =   23
         ToolTipText     =   "Run the selected stored search"
         Top             =   0
         Width           =   615
      End
      Begin VB.ComboBox cmbMacroSearch 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   22
         ToolTipText     =   "Previously Stored Searches"
         Top             =   0
         Width           =   1995
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   12300
         TabIndex        =   26
         ToolTipText     =   "Search using set criterea"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   13500
         TabIndex        =   27
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   11100
         TabIndex        =   25
         ToolTipText     =   "Clear the form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Order By"
         Height          =   255
         Index           =   20
         Left            =   4380
         TabIndex        =   111
         Top             =   60
         Width           =   735
      End
      Begin VB.Label lblCaption 
         Caption         =   "Stored searches:"
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   39
         Top             =   0
         Width           =   1275
      End
   End
   Begin MSAdodcLib.Adodc adoLibrary 
      Height          =   315
      Left            =   120
      Top             =   6240
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   556
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   0
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Record Count"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox txtCriterea 
      Height          =   285
      Left            =   1560
      TabIndex        =   1
      ToolTipText     =   "What should be in that data field?"
      Top             =   480
      Width           =   3075
   End
   Begin VB.ComboBox cmbFields 
      Appearance      =   0  'Flat
      Height          =   315
      ItemData        =   "frmSearchLibrary.frx":08E3
      Left            =   1560
      List            =   "frmSearchLibrary.frx":08E5
      Style           =   2  'Dropdown List
      TabIndex        =   0
      ToolTipText     =   "Which data field to search against"
      Top             =   120
      Width           =   3075
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdLibrary 
      Bindings        =   "frmSearchLibrary.frx":08E7
      Height          =   2595
      Left            =   120
      TabIndex        =   29
      ToolTipText     =   "Search results"
      Top             =   6540
      Width           =   17115
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowUpdate     =   0   'False
      BackColorOdd    =   16446965
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   30189
      _ExtentY        =   4577
      _StockProps     =   79
   End
   Begin MSAdodcLib.Adodc adoCompany 
      Height          =   330
      Left            =   7380
      Top             =   10500
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCompany"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Bindings        =   "frmSearchLibrary.frx":0900
      Height          =   315
      Left            =   6180
      TabIndex        =   73
      ToolTipText     =   "The Company the tape belongs to"
      Top             =   60
      Width           =   3675
      DataFieldList   =   "name"
      _Version        =   196617
      BorderStyle     =   0
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "accountcode"
      Columns(2).Name =   "accountcode"
      Columns(2).DataField=   "accountcode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "telephone"
      Columns(3).Name =   "telephone"
      Columns(3).DataField=   "telephone"
      Columns(3).FieldLen=   256
      _ExtentX        =   6482
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbProduct 
      Bindings        =   "frmSearchLibrary.frx":0919
      Height          =   315
      Left            =   6180
      TabIndex        =   74
      ToolTipText     =   "The name of the product"
      Top             =   420
      Width           =   4455
      DataFieldList   =   "name"
      _Version        =   196617
      BorderStyle     =   0
      BackColorOdd    =   8438015
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "accountcode"
      Columns(2).Name =   "accountcode"
      Columns(2).DataField=   "accountcode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "telephone"
      Columns(3).Name =   "telephone"
      Columns(3).DataField=   "telephone"
      Columns(3).FieldLen=   256
      _ExtentX        =   7858
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   8438015
      DataFieldToDisplay=   "name"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo txtTitle 
      Height          =   315
      Left            =   6180
      TabIndex        =   154
      ToolTipText     =   "The company this tape belongs to"
      Top             =   780
      Width           =   4455
      DataFieldList   =   "title"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "masterseriestitleID"
      Columns(0).Name =   "masterseriestitleID"
      Columns(0).DataField=   "masterseriestitleID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   1773
      Columns(1).Caption=   "seriesID"
      Columns(1).Name =   "seriesID"
      Columns(1).DataField=   "seriesID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   8149
      Columns(2).Caption=   "title"
      Columns(2).Name =   "title"
      Columns(2).DataField=   "title"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "companyID"
      Columns(3).Name =   "companyID"
      Columns(3).DataField=   "companyID"
      Columns(3).FieldLen=   256
      _ExtentX        =   7858
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16777215
      DataFieldToDisplay=   "title"
   End
   Begin VB.TextBox txtField3 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   1860
      TabIndex        =   199
      Top             =   3720
      Width           =   2715
   End
   Begin VB.Label lblCaption 
      Caption         =   "Serial #"
      Height          =   255
      Index           =   48
      Left            =   4860
      TabIndex        =   205
      Top             =   2640
      Width           =   1035
   End
   Begin VB.Label lblField3 
      Caption         =   "Custom Field 3"
      Height          =   255
      Left            =   180
      TabIndex        =   197
      Tag             =   "2"
      Top             =   3780
      Width           =   1635
   End
   Begin VB.Label lblCaption 
      Caption         =   "Direct SQL Search"
      Height          =   255
      Index           =   47
      Left            =   13080
      TabIndex        =   195
      Top             =   5340
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Exact Match"
      Height          =   195
      Index           =   46
      Left            =   12000
      TabIndex        =   159
      Top             =   180
      Width           =   915
   End
   Begin VB.Label lblCompanyID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   9900
      TabIndex        =   155
      Tag             =   "CLEARFIELDS"
      Top             =   60
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Caption         =   "Detail / Box"
      Height          =   255
      Index           =   45
      Left            =   4860
      TabIndex        =   150
      Top             =   3720
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Series ID"
      Height          =   255
      Index           =   44
      Left            =   4860
      TabIndex        =   128
      Top             =   2280
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Here for Job"
      Height          =   255
      Index           =   43
      Left            =   4860
      TabIndex        =   125
      Top             =   1920
      Width           =   1095
   End
   Begin VB.Label lblField1 
      Caption         =   "Custom Field 1"
      Height          =   255
      Left            =   180
      TabIndex        =   120
      Tag             =   "1"
      Top             =   3060
      Width           =   1635
   End
   Begin VB.Label lblField2 
      Caption         =   "Custom Field 2"
      Height          =   255
      Left            =   180
      TabIndex        =   119
      Tag             =   "2"
      Top             =   3420
      Width           =   1635
   End
   Begin VB.Label lblCaption 
      Caption         =   "Reference"
      Height          =   255
      Index           =   42
      Left            =   4860
      TabIndex        =   118
      Top             =   1560
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Stock Type"
      Height          =   255
      Index           =   41
      Left            =   4860
      TabIndex        =   116
      Top             =   4800
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "to"
      Height          =   255
      Index           =   40
      Left            =   11220
      TabIndex        =   108
      Top             =   1560
      Width           =   195
   End
   Begin VB.Label lblSortOrder 
      BackColor       =   &H0000FFFF&
      Height          =   315
      Left            =   8880
      TabIndex        =   106
      Top             =   9780
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.Label lblCaption 
      Caption         =   "'F' finds items with something in the field, and 'E' finds items with nothing in the field"
      Height          =   1095
      Index           =   39
      Left            =   8820
      TabIndex        =   98
      Top             =   2460
      Width           =   1635
   End
   Begin VB.Label lblCaption 
      Caption         =   "E"
      Height          =   255
      Index           =   38
      Left            =   8520
      TabIndex        =   97
      Top             =   1920
      Width           =   75
   End
   Begin VB.Label lblCaption 
      Caption         =   "F"
      Height          =   255
      Index           =   37
      Left            =   8220
      TabIndex        =   96
      Top             =   1920
      Width           =   75
   End
   Begin VB.Label lblCaption 
      Caption         =   "Sub Location"
      Height          =   255
      Index           =   36
      Left            =   4860
      TabIndex        =   95
      Top             =   3360
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Episode"
      Height          =   255
      Index           =   35
      Left            =   10800
      TabIndex        =   91
      Top             =   1200
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Series"
      Height          =   255
      Index           =   34
      Left            =   10800
      TabIndex        =   90
      Top             =   480
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Set"
      Height          =   255
      Index           =   33
      Left            =   10800
      TabIndex        =   89
      Top             =   840
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Aspect Ratio"
      Height          =   255
      Index           =   32
      Left            =   4860
      TabIndex        =   87
      Top             =   5880
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Audio Std"
      Height          =   255
      Index           =   31
      Left            =   4860
      TabIndex        =   86
      Top             =   6240
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      Height          =   255
      Index           =   2
      Left            =   4860
      TabIndex        =   83
      Top             =   120
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      Height          =   255
      Index           =   5
      Left            =   4860
      TabIndex        =   82
      Top             =   840
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Standard"
      Height          =   255
      Index           =   3
      Left            =   4860
      TabIndex        =   81
      Top             =   5160
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Format"
      Height          =   255
      Index           =   4
      Left            =   4860
      TabIndex        =   80
      Top             =   4440
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Version"
      Height          =   255
      Index           =   11
      Left            =   4860
      TabIndex        =   79
      Top             =   4080
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Copy Type"
      Height          =   255
      Index           =   12
      Left            =   4860
      TabIndex        =   78
      Top             =   5520
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Location"
      Height          =   255
      Index           =   13
      Left            =   4860
      TabIndex        =   77
      Top             =   3000
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Sub Title"
      Height          =   255
      Index           =   14
      Left            =   4860
      TabIndex        =   76
      Top             =   1200
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Product"
      Height          =   255
      Index           =   15
      Left            =   4860
      TabIndex        =   75
      Top             =   480
      Width           =   1035
   End
   Begin VB.Label lblSelectionFormula 
      BackColor       =   &H0000FFFF&
      Height          =   315
      Left            =   6420
      TabIndex        =   66
      Top             =   9780
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.Label lblCaption 
      Caption         =   "Master Search"
      Height          =   255
      Index           =   30
      Left            =   13080
      TabIndex        =   64
      Top             =   4140
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Criterea"
      Height          =   255
      Index           =   9
      Left            =   120
      TabIndex        =   37
      Top             =   480
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Field To Search"
      Height          =   255
      Index           =   8
      Left            =   120
      TabIndex        =   36
      Top             =   120
      Width           =   1275
   End
End
Attribute VB_Name = "frmSearchLibrary"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strSQLSearch As String
Dim m_strFields As String
Dim m_strSQLOrderby As String

Private Sub chkDeletedTapes_Click()

If chkDeletedTapes.Value <> 0 Then
    If CheckAccess("/undeletetapes", True) = True Then
        cmdUndeleteAllTapes.Visible = True
    Else
        cmdUndeleteAllTapes.Visible = False
    End If
Else
    cmdUndeleteAllTapes.Visible = False
End If

End Sub

Private Sub chkEvents_Click()

fraEventSearch.Enabled = GetFlag(chkEvents.Value)
fraEventDate.Enabled = GetFlag(chkEvents.Value)

cmbOrderBy.Text = ""

End Sub

Private Sub chkUnusedBlankStock_Click()

cmdClear.Value = True

If chkUnusedBlankStock.Value <> 0 Then
    txtTitle.Text = "Blank Stock"
    lblCompanyID.Caption = "501"
    cmbCompany.Text = "MX1 Ltd."
    cmbCopyType.Text = "BLANK STOCK"
End If

cmdSearch.Value = True

End Sub

Private Sub cmbAspectRatio_KeyPress(KeyAscii As Integer)
AutoMatch cmbAspectRatio, KeyAscii
End Sub

Private Sub cmbAudioStandard_KeyPress(KeyAscii As Integer)
AutoMatch cmbAudioStandard, KeyAscii
End Sub

Private Sub cmbCompany_Click()

Dim l_strSQL As String
lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

If GetData("Company", "customfield1label", "companyID", Val(lblCompanyID.Caption)) <> "" Then
    lblField1.Caption = GetData("Company", "customfield1label", "companyID", Val(lblCompanyID.Caption))
    cmbField1.Visible = True
    PopulateCustomFieldCombo Val(lblCompanyID.Caption), 1, cmbField1
Else
    lblField1.Caption = "Custom Fld 1"
    cmbField1.Visible = False
End If
If GetData("Company", "customfield2label", "companyID", Val(lblCompanyID.Caption)) <> "" Then
    lblField2.Caption = GetData("Company", "customfield2label", "companyID", Val(lblCompanyID.Caption))
    cmbField2.Visible = True
    PopulateCustomFieldCombo Val(lblCompanyID.Caption), 2, cmbField2
Else
    lblField2.Caption = "Custom Fld 2"
    cmbField2.Visible = False
End If
If GetData("Company", "customfield3label", "companyID", Val(lblCompanyID.Caption)) <> "" Then
    lblField3.Caption = GetData("Company", "customfield3label", "companyID", Val(lblCompanyID.Caption))
    cmbField3.Visible = True
    PopulateCustomFieldCombo Val(lblCompanyID.Caption), 3, cmbField3
Else
    lblField3.Caption = "Custom Fld 3"
    cmbField3.Visible = False
End If

If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/shineversion") > 0 Then
    PopulateCombo "shineversion", cmbVersion, "", True
    lblCaption(44).Caption = "Title Code"
Else
    PopulateCombo "version", cmbVersion, "", True
    lblCaption(44).Caption = "Series ID"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch2 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch2 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

l_strSQL = "SELECT * FROM masterseriestitle WHERE companyID = " & Val(lblCompanyID.Caption) & " ORDER BY title;"

With l_rstSearch2
    .CursorLocation = adUseClient
    .LockType = adLockBatchOptimistic
    .CursorType = adOpenDynamic
    .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch2.ActiveConnection = Nothing

Set txtTitle.DataSourceList = l_rstSearch2

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmbCopyType_KeyPress(KeyAscii As Integer)
AutoMatch cmbCopyType, KeyAscii
End Sub

Private Sub cmbFormat_KeyPress(KeyAscii As Integer)
AutoMatch cmbFormat, KeyAscii
End Sub

Private Sub cmbLocation_KeyPress(KeyAscii As Integer)
AutoMatch cmbLocation, KeyAscii
End Sub

Private Sub cmbProduct_DropDown()
Dim l_strSQL As String
l_strSQL = "SELECT name, productID FROM product WHERE name LIKE '" & QuoteSanitise(cmbProduct.Text) & "%' ORDER BY name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbProduct.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing
End Sub

Private Sub cmbStandard_KeyPress(KeyAscii As Integer)
AutoMatch cmbStandard, KeyAscii
End Sub


Private Sub cmbStockType_GotFocus()

Dim l_strStockType As String
l_strStockType = cmbStockType.Text
cmbStockType.Clear
PopulateCombo "stockcodesforformat" & cmbFormat.Text, cmbStockType
cmbStockType.Text = l_strStockType

End Sub

Private Sub cmbVersion_KeyPress(KeyAscii As Integer)
AutoMatch cmbVersion, KeyAscii
End Sub

Private Sub cmdAddSelectedToDub_Click()

Dim l_lngEventID As Long

On Error GoTo cmdAddSelectedToDub_Error

    If g_optRequireEventWhenAddingMasters = 1 Then
        If chkEvents.Value = 0 Then
            Dim l_intQuestion As Integer
            l_intQuestion = MsgBox("You must select an event (with time code etc), rather than a tape. Do you want to perform this search again with the events search option enabled?", vbQuestion + vbYesNo)
            If l_intQuestion = vbNo Then
                Exit Sub
            Else
                chkEvents.Value = 1
                cmdSearch_Click
                Exit Sub
            End If
        Else
            GoTo PROC_ADD_USING_EVENT
        End If
    Else
        If chkEvents.Value = 0 Then GoTo PROC_ADD_USING_LIBRARY
    End If

PROC_ADD_USING_EVENT:

  'variable
    Dim l_intBookmark As Integer, l_intTotalBookmarks As Integer, l_varCurrentBookmark As Variant
    
    'store the bookmark count
    l_intTotalBookmarks = grdLibrary.SelBookmarks.Count
    
    'loop through the grid bookmarks
    For l_intBookmark = 0 To l_intTotalBookmarks - 1
    
        'get the bookmark
        l_varCurrentBookmark = grdLibrary.SelBookmarks(l_intBookmark)
                
        'store that sucker
        l_lngEventID = grdLibrary.Columns("event ID").CellText(l_varCurrentBookmark)


        'check that there is an event selected
        If l_lngEventID <> 0 Then
            g_CurrentlySelectedEventID = l_lngEventID 'grdLibrary.Columns("event ID").Text
            If Me.Tag = "tempdub" Then
                frmNewDubbingRequest.grdSource.AddItem grdLibrary.Columns("event title").CellText(l_varCurrentBookmark) & vbTab & grdLibrary.Columns("barcode").CellText(l_varCurrentBookmark) & vbTab & grdLibrary.Columns("clock #").CellText(l_varCurrentBookmark) & vbTab & grdLibrary.Columns("Start TC").CellText(l_varCurrentBookmark) & vbTab & grdLibrary.Columns("Aspect Ratio").CellText(l_varCurrentBookmark) & vbTab & grdLibrary.Columns("Format").CellText(l_varCurrentBookmark)
                
            Else
                AddEventToJobAsMaster
            End If
        Else
            NoEventSelectedMessage
        End If
    
    
    Next
    
    Exit Sub

PROC_ADD_USING_LIBRARY:

    'MsgBox "Sorry, this feature is not yet working", vbExclamation
    'Exit Sub
    
    If Val(GetData("library", "libraryID", "barcode", grdLibrary.Columns("barcode").Text)) <> 0 Then
        
        Dim l_lngJobID As Long
    
        'get the currently loaded job
        l_lngJobID = GetLoadedJobID()
        
        If l_lngJobID = -1 Then GoTo cmdAddSelectedToDub_Error
        
        Dim l_strInsertSQL As String, l_lngDuration As Long
        
        'build it (its only easy, even a monkey could do it)
        Dim l_lngOrderby As Long
        l_lngOrderby = GetCount("SELECT COUNT(jobdetailID) FROM jobdetail WHERE jobID = '" & l_lngJobID & "';")
        
        l_lngDuration = CalcDurationFromString(grdLibrary.Columns("Prog Durn").Text)
        l_strInsertSQL = "INSERT INTO jobdetail (description, librarybarcode, quantity, copytype, jobid, format, videostandard, runningtime, fd_orderby) VALUES ('" & QuoteSanitise(grdLibrary.Columns("title").Text) & "', '" & QuoteSanitise(grdLibrary.Columns("barcode").Text) & "', '1', 'M','" & l_lngJobID & "','" & QuoteSanitise(grdLibrary.Columns("format").Text) & "','" & QuoteSanitise(grdLibrary.Columns("Video Standard").Text) & "','" & l_lngDuration & "', '" & l_lngOrderby & "');"
    
        'then we need to run it
        ExecuteSQL l_strInsertSQL, g_strExecuteError
        CheckForSQLError
   '   '
    Else
        NoLibraryItemSelectedMessage
    End If
   
   
MsgBox "Added to job dubbing instructions as a master row.", vbInformation
   
Exit Sub

cmdAddSelectedToDub_Error:

If Err.Number = 91 Then
    
    MsgBox Err.Description & vbCrLf & vbCrLf & "You must select a job before using this feature", vbExclamation
Resume
    Exit Sub
Else
    MsgBox Err.Number & "  " & Err.Description, vbExclamation
    Exit Sub
End If

End Sub

Private Sub cmdAddToRequiredMedia_Click()

On Error GoTo cmdAddToRequiredMedia_Error

'if we don't have a selected tape
If grdLibrary.Rows = 0 Then Exit Sub
If grdLibrary.Row = grdLibrary.Rows Then Exit Sub

'keep some pigeon holes free
Dim l_lngLibraryID As Long, l_lngJobID As Long, l_lngEventID As Long

'other wise we want to write a simple insert sql
Dim l_strInsertSQL As String
    
'if just one selected
If grdLibrary.SelBookmarks.Count = 0 Or grdLibrary.SelBookmarks.Count = 1 Then

    'store that sucker
    l_lngLibraryID = GetData("library", "libraryID", "barcode", grdLibrary.Columns("barcode").Text)
    
    'go and get the current job id
    l_lngJobID = GetLoadedJobID()
    
    'if there ain't one guv
    If l_lngJobID = -1 Then
        NoJobSelectedMessage
        Exit Sub
    End If
    
    If chkEvents.Value = 0 Then
        l_lngEventID = 0
    Else
        l_lngEventID = grdLibrary.Columns("Event ID").Text
    End If
    
    'build it (its only easy, even a monkey could do it)
    l_strInsertSQL = "INSERT INTO requiredmedia (libraryid, eventID, jobid, cdate, cuser) VALUES ('" & l_lngLibraryID & "','" & l_lngEventID & "','" & l_lngJobID & "','" & FormatSQLDate(Now) & "','" & g_strUserName & "');"
        
    'then we need to run it
    ExecuteSQL l_strInsertSQL, g_strExecuteError
    
    'check for any errors
    If CheckForSQLError() = False Then MsgBox "Tape was added to required media list", vbInformation, "Added"

ElseIf grdLibrary.SelBookmarks.Count > 0 Then

    'variable
    Dim l_intBookmark As Integer, l_intTotalBookmarks As Integer, l_varCurrentBookmark As Variant
    
    'store the bookmark count
    l_intTotalBookmarks = grdLibrary.SelBookmarks.Count
    
    'loop through the grid bookmarks
    For l_intBookmark = 0 To l_intTotalBookmarks - 1
    
        'get the bookmark
        l_varCurrentBookmark = grdLibrary.SelBookmarks(l_intBookmark)
                
        'store that sucker
        l_lngLibraryID = GetData("library", "libraryID", "barcode", grdLibrary.Columns("barcode").CellText(l_varCurrentBookmark))
        
        If chkEvents.Value = 0 Then
            l_lngEventID = 0
        Else
            l_lngEventID = grdLibrary.Columns("event ID").CellText(l_varCurrentBookmark)
        End If
        
        'go and get the current job id
        l_lngJobID = GetLoadedJobID()
        
        'if there ain't one guv
        If l_lngJobID = -1 Then MsgBox "There is no currently loaded job. You cannot add this tape to required media", vbExclamation, "No loaded job": Exit Sub
            
        'build it (its only easy, even a monkey could do it)
        l_strInsertSQL = "INSERT INTO requiredmedia (libraryid, eventID, jobid, cdate, cuser) VALUES ('" & l_lngLibraryID & "','" & l_lngEventID & "','" & l_lngJobID & "','" & FormatSQLDate(Now) & "','" & g_strUserName & "');"
            
        'then we need to run it
        ExecuteSQL l_strInsertSQL, g_strExecuteError
        
        CheckForSQLError
    
    Next

    MsgBox "Tape(s) added to the required media list", vbInformation, "Added"

End If


Exit Sub

cmdAddToRequiredMedia_Error:

If Err.Number = 91 Then
    MsgBox "You must select a job before using this feature", vbExclamation
    Exit Sub
Else
    MsgBox Err.Number & "  " & Err.Description, vbExclamation
    Exit Sub
End If


End Sub


Private Sub cmdAssignToLibrary_Click()

If adoLibrary.Recordset.RecordCount > 0 Then

    adoLibrary.Recordset.MoveFirst
    Do While Not adoLibrary.Recordset.EOF
        SetData "library", "inlibrary", "barcode", adoLibrary.Recordset("barcode"), "YES"
        adoLibrary.Recordset.MoveNext
    Loop
    
    adoLibrary.Refresh

End If

End Sub

Private Sub cmdBulkUpdate_Click()

Dim l_strSQL As String, l_lngCount As Long
Dim l_strUpdateSet As String
Dim l_rstTestCount As ADODB.Recordset

If adoLibrary.Recordset.RecordCount = 0 Then Beep: Exit Sub

Dim i As Long, bkmrk As Variant, l_lngTotalSelRows As Long

'Check all the checkboxes, and for any that are true include their field in the update set
l_strUpdateSet = "mdate = '" & FormatSQLDate(Now) & "', muser = '" & g_strUserInitials & "'"

If chkBulkUpdate(0).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", title = '" & QuoteSanitise(txtTitle.Text) & "'"
If chkBulkUpdate(1).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", subtitle = '" & QuoteSanitise(txtSubtitle.Text) & "'"
If chkBulkUpdate(2).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", reference = '" & QuoteSanitise(txtReference.Text) & "'"
If chkBulkUpdate(3).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", locationjobid = '" & txtLocationJobID.Text & "'"
If chkBulkUpdate(4).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", seriesID = " & IIf(txtSeriesID.Text <> "", "'" & txtSeriesID.Text & "'", "NULL")
If chkBulkUpdate(5).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", location = '" & cmbLocation.Text & "'"
If chkBulkUpdate(6).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", series = '" & cmbSeries.Text & "'"
If chkBulkUpdate(7).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", seriesset = '" & cmbSet.Text & "'"
If chkBulkUpdate(8).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", episode = '" & cmbEpisode.Text & "'"
If chkBulkUpdate(9).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", endepisode = '" & cmbEpisodeTo.Text & "'"
If chkBulkUpdate(10).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", version = '" & QuoteSanitise(cmbVersion.Text) & "'"
If chkBulkUpdate(11).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", format = '" & cmbFormat.Text & "'"
If chkBulkUpdate(12).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", videostandard = '" & cmbStandard.Text & "'"
If chkBulkUpdate(13).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", copytype = '" & cmbCopyType.Text & "'"
If chkBulkUpdate(14).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", aspectratio = '" & cmbAspectRatio.Text & "'"
If chkBulkUpdate(15).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", audiostandard = '" & cmbAudioStandard.Text & "'"
If chkBulkUpdate(16).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", shelf = '" & cmbShelf.Text & "'"
If chkBulkUpdate(17).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", box = '" & cmbBox.Text & "'"
If chkBulkUpdate(18).Value <> 0 Then
    If GetData("Company", "customfield1label", "companyID", Val(lblCompanyID.Caption)) <> "" Then
        l_strUpdateSet = l_strUpdateSet & ", customfield1 = '" & cmbField1.Text & "'"
    Else
        l_strUpdateSet = l_strUpdateSet & ", customfield1 = '" & txtField1.Text & "'"
    End If
End If
If chkBulkUpdate(19).Value <> 0 Then
    If GetData("Company", "customfield2label", "companyID", Val(lblCompanyID.Caption)) <> "" Then
        l_strUpdateSet = l_strUpdateSet & ", customfield2 = '" & cmbField2.Text & "'"
    Else
        l_strUpdateSet = l_strUpdateSet & ", customfield2 = '" & txtField2.Text & "'"
    End If
End If
If chkBulkUpdate(20).Value <> 0 Then
    l_strUpdateSet = l_strUpdateSet & ", stocktype = '" & cmbStockType.Text & "'"
    l_strUpdateSet = l_strUpdateSet & ", stockcode = '" & GetData("xref", "information", "description", cmbStockType.Text) & "'"
    l_strUpdateSet = l_strUpdateSet & ", stockduration = '" & GetData("xref", "videostandard", "description", cmbStockType.Text) & "'"
End If
If chkBulkUpdate(21).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_stereo_main = '" & chkStereoMain.Value & "'"
If chkBulkUpdate(22).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_surround_main = '" & chkSurroundMain.Value & "'"
If chkBulkUpdate(23).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_stereo_me = '" & chkStereoME.Value & "'"
If chkBulkUpdate(24).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_surround_me = '" & chkSurroundME.Value & "'"
If chkBulkUpdate(25).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_stereo_mmn = '" & chkStereoMMN.Value & "'"
If chkBulkUpdate(26).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_surround_mmn = '" & chkSurroundMMN.Value & "'"
If chkBulkUpdate(27).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_ltrt_main = '" & chkLtRtMain.Value & "'"
If chkBulkUpdate(28).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_ltrt_me = '" & chkLtRtME.Value & "'"
If chkBulkUpdate(29).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", textless = '" & chkTapeTextless.Value & "'"
If chkBulkUpdate(30).Value <> 0 Then
    If GetData("Company", "customfield3label", "companyID", Val(lblCompanyID.Caption)) <> "" Then
        l_strUpdateSet = l_strUpdateSet & ", customfield3 = '" & cmbField3.Text & "'"
    Else
        l_strUpdateSet = l_strUpdateSet & ", customfield3 = '" & txtField3.Text & "'"
    End If
End If
If chkBulkUpdate(31).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", serialnumber = " & IIf(txtSerialNumber.Text <> "", "'" & txtSerialNumber.Text & "'", "NULL")

On Error GoTo BULK_UPDATE_ERROR

l_strSQL = "SELECT libraryID FROM library " & m_strSQLSearch & ";"
Debug.Print l_strSQL
Set l_rstTestCount = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

l_lngCount = l_rstTestCount.RecordCount

If l_lngCount <= 0 Then
    MsgBox "No selection criteria in place. Please do a search.", vbCritical, "Error with Bulk Update"
    Exit Sub
End If

If MsgBox("Updating " & l_lngCount & " records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub

If l_lngCount > 10 Then
    If MsgBox("Updating more than 10 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 50 Then
    If MsgBox("Updating more than 50 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 100 Then
    If Not CheckAccess("/bulkupdatelots") Then Exit Sub
End If


'If there were any fields to update, then complete the SQL statement and execute it

l_strSQL = "UPDATE library SET " & l_strUpdateSet & m_strSQLSearch & ";"
Debug.Print l_strSQL

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

cmdSearch.Value = True

BULK_UPDATE_ERROR:

Exit Sub

End Sub

Private Sub cmdCleanUp_Click()

adoLibrary.Recordset.MoveFirst

Do While Not adoLibrary.Recordset.EOF
    If Trim(" " & adoLibrary.Recordset("title")) <> "" Then
        adoLibrary.Recordset("title") = Replace(adoLibrary.Recordset("title"), vbCrLf, "")
        adoLibrary.Recordset.Update
    End If
    adoLibrary.Recordset.MoveNext
Loop

End Sub

Private Sub cmdClear_Click()
ClearFields Me
'Me.Caption = "Library Search"
End Sub

Private Sub cmdClearOptions_Click()

optLocation.Value = False
optNullLocation.Value = False
optShelf.Value = False
optNullShelf.Value = False
optSeriesID.Value = False
optNullSeriesID.Value = False
optSerialNumber.Value = False
optNullSerialNumber.Value = False

End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdCompanyUpdate_Click()

Dim l_lngNewCompanyID As Long, l_strNewCompanyName As String

If adoLibrary.Recordset.RecordCount > 0 Then

    frmGetNewCompany.Show vbModal
    
    If frmGetNewCompany.lblCompanyID.Caption <> "" Then
        l_lngNewCompanyID = Val(frmGetNewCompany.lblCompanyID.Caption)
        l_strNewCompanyName = frmGetNewCompany.cmbCompany.Text
        adoLibrary.Recordset.MoveFirst
        Do While Not adoLibrary.Recordset.EOF
            SetData "library", "companyID", "barcode", adoLibrary.Recordset("barcode"), l_lngNewCompanyID
            SetData "library", "companyname", "barcode", adoLibrary.Recordset("barcode"), l_strNewCompanyName
            adoLibrary.Recordset.MoveNext
        Loop
    End If
    
    Unload frmGetNewCompany
    adoLibrary.Refresh

End If

End Sub

Private Sub cmdDeleteAllTapes_Click()

If adoLibrary.Recordset.RecordCount > 0 Then

    If MsgBox("Are you Sure?", vbYesNo, "Deleting All Tapes.") = vbYes Then
        If MsgBox("Are you Really Sure?", vbYesNo, "Deleting All Tapes.") = vbYes Then
            adoLibrary.Recordset.MoveFirst
            Do While Not adoLibrary.Recordset.EOF
                DeleteLibrary adoLibrary.Recordset("libraryID")
                adoLibrary.Recordset.MoveNext
            Loop
            cmdSearch.Value = True
        End If
    End If

End If

End Sub

Private Sub cmdExport_Click()

If grdLibrary.Rows < 1 Then Exit Sub

Dim l_strFilePath As String
l_strFilePath = fGetSpecialFolder(CSIDL_DOCUMENTS)

grdLibrary.Export ssExportTypeText, ssExportAllRows + ssExportFieldNames, l_strFilePath & "libsearch.txt"
MsgBox "Exported to a text file in 'My Documents' called 'libsearch.txt'", vbInformation

End Sub

Private Sub cmdGetFields_Click()
Dim l_int As Integer

Open "C:\fields.txt" For Output As 1

For l_int = 0 To grdLibrary.Columns.Count - 1
    If grdLibrary.Columns(l_int).Visible = True Then Print #1, grdLibrary.Columns(l_int).DataField
Next

Close 1

MsgBox "Done", vbInformation

End Sub

Private Sub cmdPrint_Click()

Dim l_searchstring As String
l_searchstring = adoLibrary.RecordSource

If l_searchstring <> "" Then
    If g_intUseJCALibrarySearchPrinting = 1 Then
        PrintCrystalReport g_strLocationOfCrystalReportFiles & "librarysearch.rpt", lblSelectionFormula.Caption, g_blnPreviewReport
    ElseIf g_blnPreviewReport = True Then
        PrintCrystalReport g_strLocationOfCrystalReportFiles & "librarysearch.rpt", lblSelectionFormula.Caption, g_blnPreviewReport
        'PrintCrystalReportUsingSQL g_strLocationOfCrystalReportFiles & "librarysearch.rpt", l_searchstring, g_blnPreviewReport
    Else
        If grdLibrary.SelBookmarks.Count = 0 Then
            Dim l_intMsg As Integer
            l_intMsg = MsgBox("There are no rows selected... Select all rows?", vbOKCancel + vbQuestion)
            Select Case l_intMsg
            Case vbOK
                On Error Resume Next

                Dim l_intRow As Integer
                
                For l_intRow = 0 To grdLibrary.Rows
                    grdLibrary.SelBookmarks.Add l_intRow
                Next
                
            Case vbCancel
                Exit Sub
            End Select
        End If
        
        
        grdLibrary.PrintData ssPrintSelectedRows, True, False
    End If
End If

End Sub

Private Sub cmdRemoveFromLibrary_Click()

If adoLibrary.Recordset.RecordCount > 0 Then

    adoLibrary.Recordset.MoveFirst
    Do While Not adoLibrary.Recordset.EOF
        SetData "library", "inlibrary", "barcode", adoLibrary.Recordset("barcode"), "NO"
        adoLibrary.Recordset.MoveNext
    Loop
    
    adoLibrary.Refresh

End If

End Sub

Private Sub cmdRun_Click()

If cmbMacroSearch.Text = "" Then Exit Sub

Dim l_strSQL As String

Dim l_strFields As String

l_strFields = "libraryID, barcode, location, title, subtitle, companyname, format, videostandard, copytype, ch1, ch2, ch3, ch4, aspectratio, geometriclinearity, progduration, totalduration"

l_strSQL = "SELECT " & l_strFields & " FROM library WHERE (libraryID IS NOT NULL) "
l_strSQL = l_strSQL & "AND "
l_strSQL = l_strSQL & DecodeMacroSearch(cmbMacroSearch.Text)

'Me.Caption = "Library Search - [Searching... Please wait]"
m_strSQLSearch = l_strSQL

If chktitle.Value <> False Then
    If chkSubtitle.Value <> False Then
        m_strSQLOrderby = "ORDER BY title, subtitle"
    Else
        m_strSQLOrderby = "ORDER BY title"
    End If
ElseIf chkSubtitle.Value <> False Then
    m_strSQLOrderby = "ORDER BY subtitle"
Else
    m_strSQLOrderby = "ORDER BY barcode"
End If

adoLibrary.RecordSource = m_strSQLSearch & m_strSQLOrderby
adoLibrary.ConnectionString = g_strConnection
adoLibrary.Refresh

adoLibrary.Caption = "Records: " & grdLibrary.Rows

'Me.Caption = "Library Search - [Showing: " & grdLibrary.Rows & "]"

End Sub

Private Sub cmdSearch_Click()

Dim l_strSQL As String
Dim m_strFields As String
Dim l_lngCount As Long
Dim l_strLabel1 As String, l_strLabel2 As String, l_strLabel3 As String

For l_lngCount = 0 To 31
    chkBulkUpdate(l_lngCount).Value = 0
Next

If chkShowCustomFields.Value <> 0 Then
    If lblCompanyID.Caption <> "" Then
        l_strLabel1 = GetData("company", "customfield1label", "companyID", Val(lblCompanyID.Caption))
        l_strLabel2 = GetData("company", "customfield2label", "companyID", Val(lblCompanyID.Caption))
        l_strLabel3 = GetData("company", "customfield3label", "companyID", Val(lblCompanyID.Caption))
    End If
    If l_strLabel1 = "" Then l_strLabel1 = "CustomFld1"
    If l_strLabel2 = "" Then l_strLabel2 = "CustomFld2"
    If l_strLabel3 = "" Then l_strLabel3 = "CustomFld3"
End If


If chkEvents.Value = 1 Then


    m_strFields = "tapeevents.eventdate AS 'Event Date', library.format AS 'Format', library.productname AS 'Product', library.barcode AS 'Barcode', tapeevents.timecodestart AS 'Start TC', tapeevents.timecodestop AS 'Stop TC', tapeevents.fd_length AS 'Duration', tapeevents.eventtitle AS 'Event Title', tapeevents.eventtype AS 'Event Type', tapeevents.clocknumber AS 'Clock #', tapeevents.aspectratio AS 'Aspect Ratio', tapeevents.notes AS 'Notes', library.location AS 'Location', tapeevents.projectnumber AS 'Project #', tapeevents.eventID AS 'Event ID' FROM library INNER JOIN tapeevents ON library.libraryID = tapeevents.libraryID"
    
    
    
    'm_strFields = "library.libraryID, library.format, library.barcode, project.projectnumber, tapeevents.cdate, library.productname, tapeevents.eventID, tapeevents.eventtitle, tapeevents.eventtype, tapeevents.clocknumber, tapeevents.aspectratio, library.notes1, library.shelf FROM library INNER JOIN tapeevents ON library.libraryID = tapeevents.libraryID "
Else
    
    If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/shineversion") > 0 Then
        m_strFields = "library.libraryID, barcode AS 'Barcode', location AS 'Location', shelf AS 'Sub Location', box AS 'Detail / Box', locationjobID as 'Here for Job', title AS 'Title', seriesID AS 'Title Code', serialnumber as 'Serial #', series AS 'Ser.', seriesset AS 'Set', episode AS 'Ep.', endepisode AS 'To Ep.', version AS 'Version', subtitle AS 'Sub Title', "
    ElseIf cmbFormat.Text = "DISCSTORE" Then
        m_strFields = "library.libraryID, barcode AS 'Barcode', MinimumFreePercentage AS 'Min Free Perc', location AS 'Location', shelf AS 'Sub Location', box AS 'Detail / Box', locationjobID as 'Here for Job', title AS 'Title', seriesID AS 'Series ID', serialnumber as 'Serial #', series AS 'Ser.', seriesset AS 'Set', episode AS 'Ep.', endepisode AS 'To Ep.', version AS 'Version', subtitle AS 'Sub Title', "
    Else
        m_strFields = "library.libraryID, barcode AS 'Barcode', location AS 'Location', shelf AS 'Sub Location', box AS 'Detail / Box', locationjobID as 'Here for Job', title AS 'Title', seriesID AS 'Series ID', serialnumber as 'Serial #', series AS 'Ser.', seriesset AS 'Set', episode AS 'Ep.', endepisode AS 'To Ep.', version AS 'Version', subtitle AS 'Sub Title', "
    End If
    
    If chkShowRef.Value <> 0 Then
        m_strFields = m_strFields & "reference as 'Reference', "
    End If
    If chkShowCustomFields.Value <> 0 Then
        m_strFields = m_strFields & "customfield1 as '" & l_strLabel1 & "', customfield2 as '" & l_strLabel2 & "', customfield3 as '" & l_strLabel3 & "', "
    End If
    m_strFields = m_strFields & "companyname AS 'Company Name', format AS 'Format', stocktype as 'Stock', videostandard AS 'Video Standard', copytype AS 'Copy', aspectratio AS 'Ratio', audiostandard AS 'Language', progduration AS 'Prog Durn', totalduration AS 'Total Durn', ch1 AS 'Sound 1', ch2 AS 'Sound 2', ch3 AS 'Sound 3', ch4 AS 'Sound 4', CASE WHEN libraryID IN (Select libraryID FROM techrev WHERE system_deleted = 0) THEN 'Yes' ELSE '' END AS TechRev FROM library "

End If


'm_strFields = m_strFields & " LEFT JOIN project ON library.projectID = project.projectID "

m_strSQLSearch = " WHERE (library.libraryID IS NOT NULL) "

If txtDirectSQL.Text <> "" Then m_strSQLSearch = m_strSQLSearch & "AND " & txtDirectSQL.Text & " "

Dim l_strSF As String

If chkFalseRecord.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (flagfalserecord <> 0) "
    l_strSF = l_strSF & "AND ({library.flagfalserecord} <> 0) "
End If

If chkUnusedBlankStock.Value <> 0 Or chkUsedBlankStock.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (blankstock <> 0) "
    l_strSF = l_strSF & "AND ({library.blankstock} <> 0) "
End If

If chkOutOfLibrary.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (location <> 'LIBRARY' AND inlibrary = 'YES') "
    l_strSF = l_strSF & "AND ({library.location} = " & Chr(34) & "LIBRARY" & Chr(34) & " AND {library.inlibrary} = " & Chr(34) & "YES" & Chr(34) & ") "
End If

If txtMasterSearch.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.companyname Like '%" & QuoteSanitise(txtMasterSearch.Text) & "%' OR library.title Like '%" & QuoteSanitise(txtMasterSearch.Text) & "%' OR library.productname Like '%" & QuoteSanitise(txtMasterSearch.Text) & "%' OR library.subtitle Like '%" & QuoteSanitise(txtMasterSearch.Text) & "%' OR library.series Like '%" & QuoteSanitise(txtMasterSearch.Text) & "%' OR library.episode Like '%" & QuoteSanitise(txtMasterSearch.Text) & "%' OR library.seriesset Like '%" & QuoteSanitise(txtMasterSearch.Text) & "%') "
    l_strSF = l_strSF & "AND ({library.companyname} Like " & Chr(34) & "*" & txtMasterSearch.Text & "*" & Chr(34) & " OR {library.title} Like " & Chr(34) & "*" & txtMasterSearch.Text & "*" & Chr(34) & " OR {library.productname} Like " & Chr(34) & "*" & txtMasterSearch.Text & "*" & Chr(34) & "{library.subtitle} Like " & Chr(34) & "*" & txtMasterSearch.Text & "*" & Chr(34) & " OR {library.series} Like " & Chr(34) & "*" & txtMasterSearch.Text & "*" & Chr(34) & " OR {library.episode} Like " & Chr(34) & "*" & txtMasterSearch.Text & "*" & Chr(34) & " OR {library.seriesset} Like " & Chr(34) & "*" & txtMasterSearch.Text & "*" & Chr(34) & ") "
End If

If cmbCompany.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.companyname Like '" & QuoteSanitise(cmbCompany.Text) & "%') "
    l_strSF = l_strSF & "AND ({library.companyname} Like " & Chr(34) & cmbCompany.Text & "*" & Chr(34) & ") "
End If

'new search fields - MX1

If cmbAspectRatio.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.aspectratio Like '" & QuoteSanitise(cmbAspectRatio.Text) & "%') "
    l_strSF = l_strSF & "AND ({library.aspectratio} Like " & Chr(34) & cmbAspectRatio.Text & "*" & Chr(34) & ") "
End If

If cmbAudioStandard.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.audiostandard Like '" & QuoteSanitise(cmbAudioStandard.Text) & "%') "
    l_strSF = l_strSF & "AND ({library.audiostandard} Like " & Chr(34) & cmbAudioStandard.Text & "*" & Chr(34) & ") "
End If

If cmbSeries.Text <> "" Then
    If chkExact(0).Value <> 0 Then
        m_strSQLSearch = m_strSQLSearch & "AND (library.series = '" & QuoteSanitise(cmbSeries.Text) & "') "
        l_strSF = l_strSF & "AND ({library.series} = " & Chr(34) & cmbSeries.Text & Chr(34) & ") "
    Else
        m_strSQLSearch = m_strSQLSearch & "AND (library.series Like '" & QuoteSanitise(cmbSeries.Text) & "%') "
        l_strSF = l_strSF & "AND ({library.series} Like " & Chr(34) & cmbSeries.Text & "*" & Chr(34) & ") "
    End If
End If

If cmbEpisode.Text <> "" Then
    If chkExact(2).Value <> 0 Then
        m_strSQLSearch = m_strSQLSearch & "AND (library.episode = '" & QuoteSanitise(cmbEpisode.Text) & "') "
        l_strSF = l_strSF & "AND ({library.episode} = " & Chr(34) & cmbEpisode.Text & Chr(34) & ") "
    Else
        m_strSQLSearch = m_strSQLSearch & "AND (library.episode Like '" & QuoteSanitise(cmbEpisode.Text) & "%') "
        l_strSF = l_strSF & "AND ({library.episode} Like " & Chr(34) & cmbEpisode.Text & "*" & Chr(34) & ") "
    End If
End If

If cmbEpisodeTo.Text <> "" Then
    If chkExact(3).Value <> 0 Then
        m_strSQLSearch = m_strSQLSearch & "AND (library.endepisode = '" & QuoteSanitise(cmbEpisodeTo.Text) & "') "
        l_strSF = l_strSF & "AND ({library.endepisode} = " & Chr(34) & cmbEpisodeTo.Text & Chr(34) & ") "
    Else
        m_strSQLSearch = m_strSQLSearch & "AND (library.endepisode Like '" & QuoteSanitise(cmbEpisodeTo.Text) & "%') "
        l_strSF = l_strSF & "AND ({library.endepisode} Like " & Chr(34) & cmbEpisodeTo.Text & "*" & Chr(34) & ") "
    End If
End If

If cmbSet.Text <> "" Then
    If chkExact(1).Value <> 0 Then
        m_strSQLSearch = m_strSQLSearch & "AND (library.seriesset = '" & QuoteSanitise(cmbSet.Text) & "') "
        l_strSF = l_strSF & "AND ({library.seriesset} = " & Chr(34) & cmbSet.Text & Chr(34) & ") "
    Else
        m_strSQLSearch = m_strSQLSearch & "AND (library.seriesset Like '" & QuoteSanitise(cmbSet.Text) & "%') "
        l_strSF = l_strSF & "AND ({library.seriesset} Like " & Chr(34) & cmbSet.Text & "*" & Chr(34) & ") "
    End If
End If
' end of new fields

If txtTitle.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.title Like '" & QuoteSanitise(txtTitle.Text) & "%') "
    l_strSF = l_strSF & "AND ({library.title} Like " & Chr(34) & txtTitle.Text & "*" & Chr(34) & ") "
End If

If txtSubtitle.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.subtitle Like '" & QuoteSanitise(txtSubtitle.Text) & "%') "
    l_strSF = l_strSF & "AND ({library.subtitle} Like " & Chr(34) & txtSubtitle.Text & "*" & Chr(34) & ") "
End If

If txtReference.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.reference Like '" & QuoteSanitise(txtReference.Text) & "%') "
    l_strSF = l_strSF & "AND ({library.reference} Like " & Chr(34) & txtReference.Text & "*" & Chr(34) & ") "
End If

If cmbFormat.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.format Like '" & cmbFormat.Text & "%') "
    l_strSF = l_strSF & "AND ({library.format} Like " & Chr(34) & cmbFormat.Text & "*" & Chr(34) & ") "
End If

If cmbStockType.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.stocktype Like '" & cmbStockType.Text & "%') "
    l_strSF = l_strSF & "AND ({library.stocktype} Like " & Chr(34) & cmbStockType.Text & "*" & Chr(34) & ") "
End If

If cmbStandard.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.videostandard Like '" & cmbStandard.Text & "%') "
    l_strSF = l_strSF & "AND ({library.videostandard} Like " & Chr(34) & cmbStandard.Text & "*" & Chr(34) & ") "
End If

If cmbCopyType.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.copytype Like '" & cmbCopyType.Text & "%') "
    l_strSF = l_strSF & "AND ({library.copytype} Like " & Chr(34) & cmbCopyType.Text & "*" & Chr(34) & ") "
End If
    
If cmbVersion.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.version Like '" & cmbVersion.Text & "%') "
    l_strSF = l_strSF & "AND ({library.version} Like " & Chr(34) & cmbVersion.Text & "*" & Chr(34) & ") "
End If

If cmbLocation.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.location Like '" & cmbLocation.Text & "%') "
    l_strSF = l_strSF & "AND ({library.location} Like " & Chr(34) & cmbLocation.Text & "*" & Chr(34) & ") "
ElseIf optLocation.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.location IS NOT NULL AND library.location <> '') "
    l_strSF = l_strSF & "AND (NOT ISNULL({library.location}) AND {library.location} <> " & Chr(34) & Chr(34) & ") "
ElseIf optNullLocation.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.location IS NULL OR library.location = '') "
    l_strSF = l_strSF & "AND (ISNULL({library.location}) OR {library.location} = " & Chr(34) & Chr(34) & ") "
End If

If txtLocationJobID.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.locationjobID = " & Val(txtLocationJobID.Text) & ") "
    l_strSF = l_strSF & "AND ({library.locationjobID} = " & Val(txtLocationJobID.Text) & ") "
End If

If txtSeriesID.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.seriesID = '" & txtSeriesID.Text & "') "
    l_strSF = l_strSF & "AND ({library.seriesID} = " & Chr(34) & txtSeriesID.Text & Chr(34) & ") "
ElseIf optSeriesID.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.seriesID IS NOT NULL AND library.seriesID <> '') "
    l_strSF = l_strSF & "AND (NOT ISNULL({library.seriesID}) AND {library.seriesID} <> " & Chr(34) & Chr(34) & ") "
ElseIf optNullSeriesID.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.seriesID IS NULL OR library.seriesID = '') "
    l_strSF = l_strSF & "AND (ISNULL({library.seriesID}) OR {library.seriesID} = " & Chr(34) & Chr(34) & ") "
End If

If txtSerialNumber.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.serialnumber = '" & txtSerialNumber.Text & "') "
    l_strSF = l_strSF & "AND ({library.serialnumber} = " & Chr(34) & txtSerialNumber.Text & Chr(34) & ") "
ElseIf optSeriesID.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.serialnumber IS NOT NULL AND library.serialnumber <> '') "
    l_strSF = l_strSF & "AND (NOT ISNULL({library.serialnumber}) AND {library.seriialnumber} <> " & Chr(34) & Chr(34) & ") "
ElseIf optNullSeriesID.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.serialnumber IS NULL OR library.serialnumber = '') "
    l_strSF = l_strSF & "AND (ISNULL({library.serialnumber}) OR {library.serialnumber} = " & Chr(34) & Chr(34) & ") "
End If

If cmbShelf.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.shelf Like '" & cmbShelf.Text & "%') "
    l_strSF = l_strSF & "AND ({library.shelf} Like " & Chr(34) & cmbShelf.Text & "*" & Chr(34) & ") "
ElseIf optShelf.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.shelf IS NOT NULL AND library.shelf <> '') "
    l_strSF = l_strSF & "AND (NOT ISNULL({library.shelf}) AND {library.shelf} <> " & Chr(34) & Chr(34) & ") "
ElseIf optNullShelf.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.shelf IS NULL OR library.shelf = '') "
    l_strSF = l_strSF & "AND (ISNULL({library.shelf}) OR {library.shelf} <> " & Chr(34) & Chr(34) & ") "
End If

If cmbBox.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.box Like '" & cmbBox.Text & "%') "
    l_strSF = l_strSF & "AND ({library.box} Like " & Chr(34) & cmbBox.Text & "*" & Chr(34) & ") "
ElseIf optBox.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.box IS NOT NULL AND library.box <> '') "
    l_strSF = l_strSF & "AND (NOT ISNULL({library.box}) AND {library.box} <> " & Chr(34) & Chr(34) & ") "
ElseIf optNullBox.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.box IS NULL OR library.box = '') "
    l_strSF = l_strSF & "AND (ISNULL({library.box}) OR {library.box} <> " & Chr(34) & Chr(34) & ") "
End If

If txtJobIDFrom.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.jobID >= '" & txtJobIDFrom.Text & "') "
    l_strSF = l_strSF & "AND ({library.jobID} >= " & txtJobIDFrom.Text & ") "
End If

If txtJobIDTo.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.jobID <= '" & txtJobIDTo.Text & "') "
    l_strSF = l_strSF & "AND ({library.jobID} <=" & txtJobIDTo.Text & ") "
End If

If txtProjectIDFrom.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.projectnumber >= '" & txtProjectIDFrom.Text & "') "
    l_strSF = l_strSF & "AND ({library.projectnumber} <=" & txtProjectIDFrom.Text & ") "
End If

If txtProjectIDTo.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.projectnumber <= '" & txtProjectIDTo.Text & "') "
    l_strSF = l_strSF & "AND ({library.projectnumber} >=" & txtProjectIDTo.Text & ") "
End If


If cmbFields.Text <> "" And txtCriterea.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (" & cmbFields.Text & " Like '" & QuoteSanitise(txtCriterea.Text) & "%') "
    l_strSF = l_strSF & "AND ({" & cmbFields.Text & "} Like " & Chr(34) & txtCriterea.Text & "*" & Chr(34) & ") "
End If

If cmbProduct.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.productname Like '" & QuoteSanitise(cmbProduct.Text) & "%') "
    l_strSF = l_strSF & "AND ({library.productname} Like " & Chr(34) & cmbProduct.Text & "*" & Chr(34) & ") "
End If

If txtField1.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.customfield1 Like '" & QuoteSanitise(txtField1.Text) & "%') "
    l_strSF = l_strSF & "AND ({library.customfield1} Like " & Chr(34) & txtField1.Text & "*" & Chr(34) & ") "
End If

If txtField2.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.customfield2 Like '" & QuoteSanitise(txtField2.Text) & "%') "
    l_strSF = l_strSF & "AND ({library.customfield2} Like " & Chr(34) & txtField2.Text & "*" & Chr(34) & ") "
End If

If txtField3.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.customfield3 Like '" & QuoteSanitise(txtField3.Text) & "%') "
    l_strSF = l_strSF & "AND ({library.customfield3} Like " & Chr(34) & txtField3.Text & "*" & Chr(34) & ") "
End If

If cmbField1.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.customfield1 Like '" & QuoteSanitise(cmbField1.Text) & "%') "
    l_strSF = l_strSF & "AND ({library.customfield1} Like " & Chr(34) & cmbField1.Text & "*" & Chr(34) & ") "
End If

If cmbField2.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.customfield2 Like '" & QuoteSanitise(cmbField2.Text) & "%') "
    l_strSF = l_strSF & "AND ({library.customfield2} Like " & Chr(34) & cmbField2.Text & "*" & Chr(34) & ") "
End If

If cmbField3.Text <> "" Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.customfield3 Like '" & QuoteSanitise(cmbField3.Text) & "%') "
    l_strSF = l_strSF & "AND ({library.customfield3} Like " & Chr(34) & cmbField3.Text & "*" & Chr(34) & ") "
End If

'this bit not yet included in search print out

If Not IsNull(datModifiedDateFrom.Value) Then
    Dim l_datModifiedDateFrom As Date
    l_datModifiedDateFrom = Format(datModifiedDateFrom.Value, vbShortDateFormat)
    m_strSQLSearch = m_strSQLSearch & "AND (library.mdate >= '" & FormatSQLDate(l_datModifiedDateFrom & " 00:00") & "') "
    
    l_strSF = l_strSF & "AND ({library.mdate} >= #" & Format(l_datModifiedDateFrom, "YYYY/MM/DD") & " 00:00#)"
    
    
End If

If Not IsNull(datModifiedDateTo.Value) Then
    Dim l_datModifiedDateTo As Date
    l_datModifiedDateTo = Format(datModifiedDateTo.Value, vbShortDateFormat)
    m_strSQLSearch = m_strSQLSearch & "AND (library.mdate <= '" & FormatSQLDate(l_datModifiedDateTo & " 23:59") & "') "
    l_strSF = l_strSF & "AND ({library.mdate} <= #" & Format(l_datModifiedDateTo, "YYYY/MM/DD") & " 23:59#)"
End If


If Not IsNull(datCreatedDateFrom.Value) Then
    Dim l_datCreatedDateFrom As Date
    l_datCreatedDateFrom = Format(datCreatedDateFrom.Value, vbShortDateFormat)
    m_strSQLSearch = m_strSQLSearch & "AND (library.cdate >= '" & FormatSQLDate(l_datCreatedDateFrom & " 00:00") & "') "
    l_strSF = l_strSF & "AND ({library.cdate} >= #" & Format(l_datCreatedDateFrom, "YYYY/MM/DD") & " 00:00#)"
End If

If Not IsNull(datCreatedDateTo.Value) Then
    Dim l_datCreatedDateTo As Date
    l_datCreatedDateTo = Format(datCreatedDateTo.Value, vbShortDateFormat)
    m_strSQLSearch = m_strSQLSearch & "AND (library.cdate <= '" & FormatSQLDate(l_datCreatedDateTo & " 23:59") & "') "
    l_strSF = l_strSF & "AND ({library.cdate} <= #" & Format(l_datCreatedDateTo, "YYYY/MM/DD") & " 23:59#)"
End If

' Sound checkboxes

If chkStereoMain.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.sound_stereo_main <> 0) "
    l_strSF = l_strSF & "AND ({library.sound_stereo_main} <> 0) "
End If
If chkStereoME.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.sound_stereo_me <> 0) "
    l_strSF = l_strSF & "AND ({library.sound_stereo_me} <> 0) "
End If
If chkSurroundMain.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.sound_surround_main <> 0) "
    l_strSF = l_strSF & "AND ({library.sound_surround_main} <> 0) "
End If
If chkSurroundME.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.sound_surround_me <> 0) "
    l_strSF = l_strSF & "AND ({library.sound_surround_me} <> 0) "
End If
If chkStereoMMN.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.sound_stereo_mmn <> 0) "
    l_strSF = l_strSF & "AND ({library.sound_stereo_mmn} <> 0) "
End If
If chkSurroundMMN.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.sound_surround_mmn <> 0) "
    l_strSF = l_strSF & "AND ({library.sound_surround_mmn} <> 0) "
End If
If chkLtRtMain.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.sound_ltrt_main <> 0) "
    l_strSF = l_strSF & "AND ({library.sound_ltrt_main} <> 0) "
End If
If chkLtRtME.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.sound_ltrt_me <> 0) "
    l_strSF = l_strSF & "AND ({library.sound_ltrt_me} <> 0) "
End If
If chkTapeTextless.Value <> 0 Then
    m_strSQLSearch = m_strSQLSearch & "AND (library.textless <> 0) "
    l_strSF = l_strSF & "AND ({library.textless} <> 0) "
End If

'search the events too
If chkEvents.Value = 1 Then
    
    If txtEventTitle.Text <> "" Then
        m_strSQLSearch = m_strSQLSearch & "AND (tapeevents.eventtitle Like '" & QuoteSanitise(txtEventTitle.Text) & "%') "
        l_strSF = l_strSF & "AND ({tapeevents.eventtitle} Like " & Chr(34) & txtEventTitle.Text & "*" & Chr(34) & ") "
    End If
    
    If cmbEventType.Text <> "" Then
        m_strSQLSearch = m_strSQLSearch & "AND (tapeevents.eventtype Like '" & QuoteSanitise(cmbEventType.Text) & "%') "
        l_strSF = l_strSF & "AND ({tapeevents.eventtype} Like " & Chr(34) & cmbEventType.Text & "*" & Chr(34) & ") "
    End If
    
    If cmbEventAspect.Text <> "" Then
        m_strSQLSearch = m_strSQLSearch & "AND (tapeevents.aspectratio Like '" & QuoteSanitise(cmbEventAspect.Text) & "%') "
        l_strSF = l_strSF & "AND ({tapeevents.aspectratio} Like " & Chr(34) & cmbEventAspect.Text & "*" & Chr(34) & ") "
    End If
    
    If txtEventClockNumber.Text <> "" Then
        m_strSQLSearch = m_strSQLSearch & "AND (tapeevents.clocknumber Like '" & QuoteSanitise(txtEventClockNumber.Text) & "%') "
        l_strSF = l_strSF & "AND ({tapeevents.clocknumber} Like " & Chr(34) & txtEventClockNumber.Text & "*" & Chr(34) & ") "
    End If
    
    If txtEventNotes.Text <> "" Then
        m_strSQLSearch = m_strSQLSearch & "AND (tapeevents.notes Like '" & QuoteSanitise(txtEventNotes.Text) & "%') "
        l_strSF = l_strSF & "AND ({tapeevents.notes} Like " & Chr(34) & txtEventNotes.Text & "*" & Chr(34) & ") "
    End If
    
    If txtEventEditor.Text <> "" Then
        m_strSQLSearch = m_strSQLSearch & "AND (tapeevents.editor1 Like '" & QuoteSanitise(txtEventEditor.Text) & "%') "
        l_strSF = l_strSF & "AND ({tapeevents.editor1} Like " & Chr(34) & txtEventEditor.Text & "*" & Chr(34) & ") "
    End If
    
    If txtEventDuration.Text <> "" Then
        m_strSQLSearch = m_strSQLSearch & "AND (tapeevents.fd_length Like '" & QuoteSanitise(txtEventDuration.Text) & "%') "
        l_strSF = l_strSF & "AND ({tapeevents.fd_length} Like " & Chr(34) & txtEventDuration.Text & "*" & Chr(34) & ") "
    End If
    
    If chkOnlyClocked.Value <> 0 Then
        m_strSQLSearch = m_strSQLSearch & "AND (tapeevents.clocknumber IS NOT NULL AND tapeevents.clocknumber <> '') "
        l_strSF = l_strSF & "AND (Not IsNull({tapeevents.clocknumber}) AND {tapeevents.clocknumber} <> '') "
    End If

    'dates not yet included in print out formula

    If Not IsNull(datEventDateFrom.Value) Then
        Dim l_datEventDateFrom As Date
        l_datEventDateFrom = Format(datEventDateFrom.Value, vbShortDateFormat)
        m_strSQLSearch = m_strSQLSearch & "AND (tapeevents.eventdate >= '" & FormatSQLDate(l_datEventDateFrom & " 00:00") & "') "
        l_strSF = l_strSF & "AND ({tapeevents.eventdate} >= #" & Format(l_datEventDateFrom, "YYYY/MM/DD") & " 00:00#)"
    End If
    
    If Not IsNull(datEventDateTo.Value) Then
        Dim l_datEventDateTo As Date
        l_datEventDateTo = Format(datEventDateTo.Value, vbShortDateFormat)
        m_strSQLSearch = m_strSQLSearch & "AND (tapeevents.eventdate <= '" & FormatSQLDate(l_datEventDateTo & " 23:59") & "') "
        l_strSF = l_strSF & "AND ({tapeevents.eventdate} <= #" & Format(l_datEventDateTo, "YYYY/MM/DD") & " 23:59#)"
    End If

End If

If cmbOrderBy.Text = "" Then
    If chkEvents.Value = 1 Then
        cmbOrderBy.Text = "Event Date"
    Else
        cmbOrderBy.Text = "Barcode"
    End If
End If

Dim l_intCount

For l_intCount = 1 To Len(l_strSF)
    If Mid(l_strSF, l_intCount, 1) = "%" Then Mid(l_strSF, l_intCount, 1) = "*"
Next

lblSelectionFormula.Caption = "{library.libraryID} > 0 AND {library.system_deleted} = 0 " & l_strSF

If chkDeletedTapes.Value <> 0 Then
    lblSelectionFormula.Caption = "{library.libraryID} > 0 AND {library.system_deleted} <> 0 " & l_strSF
    m_strSQLSearch = m_strSQLSearch & " AND system_deleted <> 0 "
Else
    lblSelectionFormula.Caption = "{library.libraryID} > 0 AND {library.system_deleted} = 0 " & l_strSF
    m_strSQLSearch = m_strSQLSearch & " AND system_deleted = 0 "
End If

m_strSQLOrderby = "ORDER BY '" & cmbOrderBy.Text & "' " & cmbDirection.Text

l_strSQL = "SELECT " & m_strFields & m_strSQLSearch & m_strSQLOrderby & ";"

Me.MousePointer = vbHourglass
DoEvents

Debug.Print adoLibrary.RecordSource

adoLibrary.RecordSource = l_strSQL
adoLibrary.ConnectionString = g_strConnection
adoLibrary.Refresh

'added by chris to show the records
Set grdLibrary.DataSource = Nothing
grdLibrary.Refresh
Set grdLibrary.DataSource = adoLibrary
grdLibrary.Refresh

Me.MousePointer = vbDefault

adoLibrary.Caption = "Records: " & grdLibrary.Rows

If chkEvents.Value = 1 Then
    grdLibrary.Columns(0).Width = 1000
    grdLibrary.Columns(1).Width = 1200
    grdLibrary.Columns(2).Width = 2200
    grdLibrary.Columns(3).Width = 1000
    grdLibrary.Columns(4).Width = 1000
    grdLibrary.Columns(5).Width = 1000
    grdLibrary.Columns(6).Width = 800
    grdLibrary.Columns(7).Width = 2200
    grdLibrary.Columns(8).Width = 1200
    grdLibrary.Columns(9).Width = 1600
    grdLibrary.Columns(10).Width = 900
Else

    'MX1 Setting
    grdLibrary.Columns(0).Width = 800
    grdLibrary.Columns(1).Width = 1400
    grdLibrary.Columns(2).Width = 1200
    grdLibrary.Columns(3).Width = 1200
    grdLibrary.Columns(4).Width = 1200
    grdLibrary.Columns(5).Width = 1100
    grdLibrary.Columns(6).Width = 2400
    grdLibrary.Columns(7).Width = 900
    grdLibrary.Columns(8).Width = 800
    grdLibrary.Columns(9).Width = 400
    grdLibrary.Columns(10).Width = 400
    grdLibrary.Columns(11).Width = 400
    grdLibrary.Columns(12).Width = 400
    grdLibrary.Columns(13).Width = 2400
    grdLibrary.Columns(14).Width = 2400
    If chkShowRef.Value <> 0 Then
        grdLibrary.Columns(15).Width = 2000
        grdLibrary.Columns(16).Width = 1000
        grdLibrary.Columns(17).Width = 1000
        grdLibrary.Columns(18).Width = 1000
        grdLibrary.Columns(19).Width = 1000
        grdLibrary.Columns(20).Width = 1000
        grdLibrary.Columns(21).Width = 1400
        grdLibrary.Columns(22).Width = 1000
        grdLibrary.Columns(23).Width = 1000
        grdLibrary.Columns(24).Width = 800
        grdLibrary.Columns(25).Width = 800
        grdLibrary.Columns(26).Width = 800
        grdLibrary.Columns(27).Width = 800
        grdLibrary.Columns(28).Width = 800
        grdLibrary.Columns(29).Width = 800
        grdLibrary.Columns(29).Alignment = ssCaptionAlignmentCenter
    Else
        grdLibrary.Columns(15).Width = 1000
        grdLibrary.Columns(16).Width = 1000
        grdLibrary.Columns(17).Width = 1000
        grdLibrary.Columns(18).Width = 1000
        grdLibrary.Columns(19).Width = 1000
        grdLibrary.Columns(20).Width = 1400
        grdLibrary.Columns(21).Width = 1000
        grdLibrary.Columns(22).Width = 1000
        grdLibrary.Columns(23).Width = 800
        grdLibrary.Columns(24).Width = 800
        grdLibrary.Columns(25).Width = 800
        grdLibrary.Columns(26).Width = 800
        grdLibrary.Columns(27).Width = 800
        grdLibrary.Columns(28).Width = 800
        grdLibrary.Columns(28).Alignment = ssCaptionAlignmentCenter
    End If
            
End If

End Sub

Private Sub cmdSelectAll_Click()

Dim i As Integer


If grdLibrary.Rows > 50 Then
    i = MsgBox("There are " & grdLibrary.Rows & " rows to select. Are you sure you want to select all of them? This may take some time...", vbOKCancel + vbQuestion)
    If i = vbCancel Then Exit Sub
End If
    

grdLibrary.MoveFirst ' Position at the first row
For i = 0 To grdLibrary.Rows - 1
    grdLibrary.SelBookmarks.Add grdLibrary.Bookmark
    grdLibrary.MoveNext
Next i


End Sub

Private Sub cmdBulkBarcodePrint_Click()

If MsgBox("Are you sure you want to print new barcode labels for all these tapes?", vbYesNo, "Bulk Barcode Printing") <> vbYes Then Exit Sub

Dim l_strReportToPrint As String

If adoLibrary.Recordset.RecordCount > 0 Then
    
    l_strReportToPrint = g_strLocationOfCrystalReportFiles & GetData("xref", "information", "description", CurrentMachineName)
    
    adoLibrary.Recordset.MoveFirst
    Do While Not adoLibrary.Recordset.EOF
    
        PrintCrystalReport l_strReportToPrint, "{library.libraryID} = " & adoLibrary.Recordset.Fields("libraryID"), False
        adoLibrary.Recordset.MoveNext
        
    Loop

End If

End Sub

Private Sub cmdBulkUpdateSelected_Click()

Dim l_strSQL As String, l_lngCount As Long
Dim l_strUpdateSet As String
Dim l_rstTestCount As ADODB.Recordset

If adoLibrary.Recordset.RecordCount = 0 Then Beep: Exit Sub

Dim i As Long, bkmrk As Variant, l_lngTotalSelRows As Long

'Check all the checkboxes, and for any that are true include their field in the update set
l_strUpdateSet = "mdate = '" & FormatSQLDate(Now) & "', muser = '" & g_strUserInitials & "'"

If chkBulkUpdate(0).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", title = '" & QuoteSanitise(txtTitle.Text) & "'"
If chkBulkUpdate(1).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", subtitle = '" & QuoteSanitise(txtSubtitle.Text) & "'"
If chkBulkUpdate(2).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", reference = '" & QuoteSanitise(txtReference.Text) & "'"
If chkBulkUpdate(3).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", locationjobid = '" & txtLocationJobID.Text & "'"
If chkBulkUpdate(4).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", seriesID = " & IIf(txtSeriesID.Text <> "", "'" & txtSeriesID.Text & "'", "NULL")
If chkBulkUpdate(5).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", location = '" & cmbLocation.Text & "'"
If chkBulkUpdate(6).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", series = '" & cmbSeries.Text & "'"
If chkBulkUpdate(7).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", seriesset = '" & cmbSet.Text & "'"
If chkBulkUpdate(8).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", episode = '" & cmbEpisode.Text & "'"
If chkBulkUpdate(9).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", endepisode = '" & cmbEpisodeTo.Text & "'"
If chkBulkUpdate(10).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", version = '" & QuoteSanitise(cmbVersion.Text) & "'"
If chkBulkUpdate(11).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", format = '" & cmbFormat.Text & "'"
If chkBulkUpdate(12).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", videostandard = '" & cmbStandard.Text & "'"
If chkBulkUpdate(13).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", copytype = '" & cmbCopyType.Text & "'"
If chkBulkUpdate(14).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", aspectratio = '" & cmbAspectRatio.Text & "'"
If chkBulkUpdate(15).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", audiostandard = '" & cmbAudioStandard.Text & "'"
If chkBulkUpdate(16).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", shelf = '" & cmbShelf.Text & "'"
If chkBulkUpdate(17).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", box = '" & cmbBox.Text & "'"
If chkBulkUpdate(18).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", customfield1 = '" & txtField1.Text & "'"
If chkBulkUpdate(19).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", customfield2 = '" & txtField2.Text & "'"
If chkBulkUpdate(20).Value <> 0 Then
    l_strUpdateSet = l_strUpdateSet & ", stocktype = '" & cmbStockType.Text & "'"
    l_strUpdateSet = l_strUpdateSet & ", stockcode = '" & GetData("xref", "information", "description", cmbStockType.Text) & "'"
    l_strUpdateSet = l_strUpdateSet & ", stockduration = '" & GetData("xref", "videostandard", "description", cmbStockType.Text) & "'"
End If
If chkBulkUpdate(21).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_stereo_main = '" & chkStereoMain.Value & "'"
If chkBulkUpdate(22).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_surround_main = '" & chkSurroundMain.Value & "'"
If chkBulkUpdate(23).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_stereo_me = '" & chkStereoME.Value & "'"
If chkBulkUpdate(24).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_surround_me = '" & chkSurroundME.Value & "'"
If chkBulkUpdate(25).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_stereo_mmn = '" & chkStereoMMN.Value & "'"
If chkBulkUpdate(26).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_surround_mmn = '" & chkSurroundMMN.Value & "'"
If chkBulkUpdate(27).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_ltrt_main = '" & chkLtRtMain.Value & "'"
If chkBulkUpdate(28).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_ltrt_me = '" & chkLtRtME.Value & "'"
If chkBulkUpdate(29).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", textless = '" & chkTapeTextless.Value & "'"
If chkBulkUpdate(30).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", customfield3 = '" & txtField3.Text & "'"
If chkBulkUpdate(31).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", serialnumber = " & IIf(txtSerialNumber.Text <> "", "'" & txtSerialNumber.Text & "'", "NULL")

If grdLibrary.SelBookmarks.Count > 0 Then
    
    If MsgBox("Updating " & grdLibrary.SelBookmarks.Count & " selected records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
    
    If grdLibrary.SelBookmarks.Count > 10 Then
        If MsgBox("Updating more than 10 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
    End If
    
    If grdLibrary.SelBookmarks.Count > 50 Then
        If MsgBox("Updating more than 50 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
    End If
    
    If grdLibrary.SelBookmarks.Count > 100 Then
        If Not CheckAccess("/bulkupdatelots") Then Exit Sub
    End If
    
    
    l_lngTotalSelRows = grdLibrary.SelBookmarks.Count - 1
    
    For i = 0 To l_lngTotalSelRows
        bkmrk = grdLibrary.SelBookmarks(i)
        l_strSQL = "UPDATE library SET " & l_strUpdateSet & " WHERE libraryID = " & Val(grdLibrary.Columns("libraryID").CellText(bkmrk)) & ";"
        Debug.Print
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    Next i
    
End If

cmdSearch.Value = True

End Sub

Private Sub cmdUndeleteAllTapes_Click()

If adoLibrary.Recordset.RecordCount > 0 Then

    If MsgBox("Are you Sure?", vbYesNo, "Undeleting All Tapes.") = vbYes Then
        If MsgBox("Are you Really Sure?", vbYesNo, "Undeleting All Tapes.") = vbYes Then
            adoLibrary.Recordset.MoveFirst
            Do While Not adoLibrary.Recordset.EOF
                UnDeleteLibrary adoLibrary.Recordset("libraryID")
                adoLibrary.Recordset.MoveNext
            Loop
            cmdSearch.Value = True
        End If
    End If

End If

End Sub

Private Sub Form_Activate()
Me.WindowState = vbMaximized
End Sub

Private Sub Form_Load()

chkEvents.Value = g_optDefaultEventSearchInLibrary

'MakeLookLikeOffice Me


'populate the company drop down (data bound)
Dim l_strSQL As String
l_strSQL = "SELECT name, accountcode, telephone, companyID FROM company WHERE (iscustomer = 1 OR isprospective = 1) AND system_active = 1 ORDER BY name"

adoCompany.ConnectionString = g_strConnection
adoCompany.RecordSource = l_strSQL
adoCompany.Refresh

'populate all the other combos, including ones bound to the grid
PopulateCombo "location", cmbLocation
'PopulateCombo "shelf", cmbShelf
'PopulateCombo "box", cmbBox
PopulateCombo "version", cmbVersion
PopulateCombo "format", cmbFormat

PopulateCombo "videostandard", cmbStandard
PopulateCombo "copytype", cmbCopyType
PopulateCombo "librarysearch", cmbMacroSearch
PopulateCombo "fields-library", cmbFields
PopulateCombo "fields-library", cmbOrderBy
PopulateCombo "aspectratio", cmbEventAspect
PopulateCombo "copytype", cmbEventType


PopulateCombo "aspectratio", cmbAspectRatio
PopulateCombo "series", cmbSeries
PopulateCombo "series", cmbSet
PopulateCombo "series", cmbEpisode

cmbFields.ListIndex = 1
cmbOrderBy.Text = ""
cmbDirection.ListIndex = 0


If g_optUseAudioStandardInLibraryToStoreLanguage = 1 Then
    lblCaption(31).Caption = "Language"
    PopulateCombo "language", cmbAudioStandard
Else
    PopulateCombo "audiostandard", cmbAudioStandard
End If

If g_intDisableProducts = 1 Then
    cmbProduct.Visible = False
    lblCaption(15).Visible = False
End If

If g_intDisableProjects = 1 Then
    txtProjectIDFrom.Visible = False
    txtProjectIDTo.Visible = False
    lblCaption(18).Visible = False
    lblCaption(19).Visible = False

End If

If CheckAccess("/librarybulkupdatemovements", True) Then
    chkBulkUpdate(5).Visible = True
    chkBulkUpdate(16).Visible = True
    chkBulkUpdate(17).Visible = True
End If

CenterForm Me

End Sub

Private Sub Form_Resize()

On Error Resume Next

picFooter.Top = Me.ScaleHeight - picFooter.Height - 120
picFooter.Left = Me.ScaleWidth - picFooter.Width - 120
grdLibrary.Height = Me.ScaleHeight - picFooter.Height - 120 - 120 - grdLibrary.Top
grdLibrary.Width = Me.ScaleWidth - 240

End Sub

Private Sub grdLibrary_DblClick()
If grdLibrary.Row = grdLibrary.Rows Then Exit Sub
ShowLibrary GetData("library", "libraryID", "barcode", grdLibrary.Columns("barcode").Text)
End Sub

Private Sub grdLibrary_HeadClick(ByVal ColIndex As Integer)

If cmbOrderBy.Text <> grdLibrary.Columns(ColIndex).DataField Then
    cmbOrderBy.Text = grdLibrary.Columns(ColIndex).DataField
    lblSortOrder.Caption = "{" & grdLibrary.Columns(ColIndex).DataField & "}"
Else
    cmbDirection.ListIndex = 1 - cmbDirection.ListIndex
End If

cmdSearch_Click

End Sub

Private Sub optSelect_Click(Index As Integer)
Select Case Index
Case 0
    grdLibrary.SelectTypeRow = ssSelectionTypeMultiSelectRange
Case 1
    grdLibrary.SelectTypeRow = ssSelectionTypeMultiSelect
End Select
End Sub

Private Sub txtTitle_Click()
txtSeriesID.Text = txtTitle.Columns("seriesID").Text
End Sub
