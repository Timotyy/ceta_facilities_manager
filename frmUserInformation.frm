VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmUserInformation 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "User Information"
   ClientHeight    =   13260
   ClientLeft      =   780
   ClientTop       =   1845
   ClientWidth     =   16650
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmUserInformation.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   13260
   ScaleWidth      =   16650
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkListAsMediaServices 
      Alignment       =   1  'Right Justify
      Caption         =   "List as Media Services"
      Height          =   255
      Left            =   11580
      TabIndex        =   61
      Top             =   2280
      Width           =   2175
   End
   Begin VB.CheckBox chkInvestigateIssues 
      Alignment       =   1  'Right Justify
      Caption         =   "Investigate Issues"
      Height          =   255
      Left            =   11580
      TabIndex        =   60
      Top             =   2640
      Width           =   2175
   End
   Begin VB.CheckBox chkSignoffIssues 
      Alignment       =   1  'Right Justify
      Caption         =   "Signoff Issue"
      Height          =   255
      Left            =   11580
      TabIndex        =   59
      Top             =   3000
      Width           =   2175
   End
   Begin VB.CheckBox chkShowFormerStaff 
      Alignment       =   1  'Right Justify
      Caption         =   "Include Former Staff"
      Height          =   315
      Left            =   10800
      TabIndex        =   58
      Top             =   12720
      Width           =   1995
   End
   Begin VB.CheckBox chkListAsOperator 
      Alignment       =   1  'Right Justify
      Caption         =   "List as Operator"
      Height          =   255
      Left            =   11580
      TabIndex        =   57
      Top             =   1920
      Width           =   2175
   End
   Begin VB.TextBox txtMobileLoginCode 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   8820
      TabIndex        =   55
      ToolTipText     =   "Confirm the User's Password"
      Top             =   3840
      Width           =   2475
   End
   Begin MSAdodcLib.Adodc adoSessions 
      Height          =   330
      Left            =   7680
      Top             =   5460
      Visible         =   0   'False
      Width           =   1905
      _ExtentX        =   3360
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoSessions"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CheckBox chkPasswordNeverExpires 
      Caption         =   "Password never expires"
      Height          =   195
      Left            =   8820
      TabIndex        =   52
      Top             =   3600
      Width           =   2115
   End
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "Refresh"
      Height          =   435
      Left            =   6840
      TabIndex        =   51
      Top             =   13560
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.ComboBox cmbDirection 
      Height          =   315
      ItemData        =   "frmUserInformation.frx":08CA
      Left            =   5280
      List            =   "frmUserInformation.frx":08D4
      Style           =   2  'Dropdown List
      TabIndex        =   49
      ToolTipText     =   "View order"
      Top             =   13440
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ComboBox cmbOrderBy 
      Height          =   315
      Left            =   3000
      TabIndex        =   48
      Tag             =   "NOCLEAR"
      Text            =   "cmbOrderBy"
      ToolTipText     =   "Order by"
      Top             =   13440
      Visible         =   0   'False
      Width           =   2235
   End
   Begin VB.TextBox txtJobTitle 
      Height          =   315
      Left            =   8820
      TabIndex        =   5
      ToolTipText     =   "The Full Name of the User"
      Top             =   900
      Width           =   2475
   End
   Begin VB.ComboBox cmbSecurityTemplate 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   13020
      TabIndex        =   41
      ToolTipText     =   "Select another user here to use their access codes in addition to any in the box below"
      Top             =   4020
      Width           =   3015
   End
   Begin VB.CommandButton cmdEncryptAllPasswords 
      Caption         =   "Encrypt All Passwords MD5"
      Height          =   435
      Left            =   9360
      TabIndex        =   40
      Top             =   13320
      Visible         =   0   'False
      Width           =   1515
   End
   Begin VB.ComboBox cmbDefaultJobTab 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   13020
      Style           =   2  'Dropdown List
      TabIndex        =   38
      ToolTipText     =   "Department"
      Top             =   1560
      Width           =   3375
   End
   Begin VB.ComboBox cmbDefaultAllocation 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   13020
      Style           =   2  'Dropdown List
      TabIndex        =   36
      ToolTipText     =   "Department"
      Top             =   1200
      Width           =   3375
   End
   Begin VB.ComboBox cmbDefaultDisplay 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   13020
      Style           =   2  'Dropdown List
      TabIndex        =   34
      ToolTipText     =   "Department"
      Top             =   840
      Width           =   3375
   End
   Begin VB.ComboBox cmbDefaultJobType 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   13020
      Style           =   2  'Dropdown List
      TabIndex        =   31
      ToolTipText     =   "Department"
      Top             =   480
      Width           =   3375
   End
   Begin VB.ComboBox cmbDefaultView 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   13020
      Style           =   2  'Dropdown List
      TabIndex        =   29
      ToolTipText     =   "Department"
      Top             =   120
      Width           =   3375
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdSession 
      Bindings        =   "frmUserInformation.frx":08E3
      Height          =   7035
      Left            =   7680
      TabIndex        =   28
      Top             =   5460
      Width           =   5235
      _Version        =   196617
      BeveColorScheme =   1
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Caption=   "Login"
      Columns(0).Name =   "Login"
      Columns(0).DataField=   "Login"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "Logout"
      Columns(1).Name =   "Logout"
      Columns(1).DataField=   "Logout"
      Columns(1).FieldLen=   256
      Columns(2).Width=   1773
      Columns(2).Caption=   "PC"
      Columns(2).Name =   "PC"
      Columns(2).DataField=   "PC"
      Columns(2).FieldLen=   256
      _ExtentX        =   9234
      _ExtentY        =   12409
      _StockProps     =   79
      Caption         =   "Logon Sessions"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.TextBox txtAccessCode 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7755
      IMEMode         =   3  'DISABLE
      Left            =   13020
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   13
      Top             =   4740
      Width           =   3375
   End
   Begin VB.TextBox txtMaxPOValue 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H008080FF&
      BorderStyle     =   0  'None
      Height          =   255
      IMEMode         =   3  'DISABLE
      Left            =   13020
      TabIndex        =   12
      ToolTipText     =   "Maximum Purchase Order Limit"
      Top             =   3600
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ComboBox cmbDepartment 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   8820
      Style           =   2  'Dropdown List
      TabIndex        =   4
      ToolTipText     =   "Department"
      Top             =   480
      Width           =   2475
   End
   Begin VB.CommandButton cmdImport 
      Caption         =   "MySQL"
      Height          =   315
      Left            =   7680
      TabIndex        =   1
      ToolTipText     =   "Import SQL file"
      Top             =   12720
      Visible         =   0   'False
      Width           =   975
   End
   Begin MSAdodcLib.Adodc adoUser 
      Height          =   330
      Left            =   120
      Top             =   120
      Width           =   2145
      _ExtentX        =   3784
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoUser"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox txtUserName 
      Height          =   315
      Left            =   8820
      TabIndex        =   3
      ToolTipText     =   "Short login name of the user"
      Top             =   120
      Width           =   2475
   End
   Begin VB.TextBox txtUserID 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   15240
      TabIndex        =   2
      ToolTipText     =   "The unique identifier of the user"
      Top             =   3240
      Visible         =   0   'False
      Width           =   1155
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   15360
      TabIndex        =   16
      ToolTipText     =   "Close this form"
      Top             =   12720
      Width           =   1095
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save"
      Height          =   315
      Left            =   14160
      TabIndex        =   15
      ToolTipText     =   "Save changes to this user"
      Top             =   12720
      Width           =   1095
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear"
      Height          =   315
      Left            =   12960
      TabIndex        =   14
      ToolTipText     =   "Clear the form"
      Top             =   12720
      Width           =   1095
   End
   Begin VB.TextBox txtConfirm 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   8820
      PasswordChar    =   "*"
      TabIndex        =   11
      ToolTipText     =   "Confirm the User's Password"
      Top             =   3240
      Width           =   2475
   End
   Begin VB.TextBox txtPassword 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   8820
      PasswordChar    =   "*"
      TabIndex        =   10
      ToolTipText     =   "User's Password"
      Top             =   2880
      Width           =   2475
   End
   Begin VB.TextBox txtFullName 
      Height          =   315
      Left            =   8820
      TabIndex        =   6
      ToolTipText     =   "The Full Name of the User"
      Top             =   1320
      Width           =   2475
   End
   Begin VB.TextBox txtInitials 
      Height          =   315
      Left            =   8820
      MaxLength       =   3
      TabIndex        =   7
      ToolTipText     =   "User's Initials"
      Top             =   1680
      Width           =   675
   End
   Begin VB.TextBox txtTelephone 
      Height          =   315
      Left            =   8820
      TabIndex        =   8
      ToolTipText     =   "User's telephone number"
      Top             =   2040
      Width           =   2475
   End
   Begin VB.TextBox txtEmail 
      Height          =   315
      Left            =   8820
      TabIndex        =   9
      ToolTipText     =   "User's email address"
      Top             =   2400
      Width           =   2475
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdUsers 
      Bindings        =   "frmUserInformation.frx":08FD
      Height          =   12975
      Left            =   120
      TabIndex        =   0
      ToolTipText     =   "User List"
      Top             =   120
      Width           =   7335
      _Version        =   196617
      AllowUpdate     =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   4
      Columns(0).Width=   3254
      Columns(0).Caption=   "Name"
      Columns(0).Name =   "username"
      Columns(0).DataField=   "username"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "cetauserID"
      Columns(1).Name =   "cetauserID"
      Columns(1).DataField=   "cetauserID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   4710
      Columns(2).Caption=   "Email Address"
      Columns(2).Name =   "email"
      Columns(2).DataField=   "email"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3969
      Columns(3).Caption=   "Profile"
      Columns(3).Name =   "securityprofile"
      Columns(3).DataField=   "securityprofile"
      Columns(3).FieldLen=   256
      _ExtentX        =   12938
      _ExtentY        =   22886
      _StockProps     =   79
      Caption         =   "User List"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblCaption 
      Caption         =   "Mobile Login Code"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Index           =   22
      Left            =   7680
      TabIndex        =   56
      Top             =   3780
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Last Changed Password"
      Height          =   375
      Index           =   21
      Left            =   7680
      TabIndex        =   54
      Top             =   4980
      Width           =   1035
   End
   Begin VB.Label lblLastChangedPasswordDate 
      Height          =   255
      Left            =   8820
      TabIndex        =   53
      Top             =   5100
      Width           =   2295
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order By"
      Height          =   255
      Index           =   20
      Left            =   1860
      TabIndex        =   50
      Top             =   13440
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblModifiedDate 
      Height          =   255
      Left            =   8820
      TabIndex        =   47
      Top             =   4680
      Width           =   2295
   End
   Begin VB.Label lblCreatedDate 
      Height          =   255
      Left            =   8820
      TabIndex        =   46
      Top             =   4380
      Width           =   2295
   End
   Begin VB.Label lblCaption 
      Caption         =   "Modified Date"
      Height          =   255
      Index           =   19
      Left            =   7680
      TabIndex        =   45
      Top             =   4680
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Created Date"
      Height          =   255
      Index           =   18
      Left            =   7680
      TabIndex        =   44
      Top             =   4380
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job Title"
      Height          =   255
      Index           =   17
      Left            =   7680
      TabIndex        =   43
      Top             =   960
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Security Template"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   16
      Left            =   11580
      TabIndex        =   42
      Top             =   3960
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job Tab"
      Height          =   285
      Index           =   15
      Left            =   11580
      TabIndex        =   39
      Top             =   1560
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Allocation"
      Height          =   285
      Index           =   14
      Left            =   11580
      TabIndex        =   37
      Top             =   1200
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Display"
      Height          =   285
      Index           =   13
      Left            =   11580
      TabIndex        =   35
      Top             =   840
      Width           =   1035
   End
   Begin VB.Label lblSessions 
      Alignment       =   1  'Right Justify
      Caption         =   "0 Sessions"
      Height          =   195
      Left            =   11460
      TabIndex        =   33
      Top             =   5160
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job Type"
      Height          =   285
      Index           =   12
      Left            =   11580
      TabIndex        =   32
      Top             =   480
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Schedule View"
      Height          =   285
      Index           =   11
      Left            =   11580
      TabIndex        =   30
      Top             =   120
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Access Codes"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   9
      Left            =   13020
      TabIndex        =   27
      Top             =   4440
      Width           =   1575
   End
   Begin VB.Label lblCaption 
      Caption         =   "P.O Limit"
      Height          =   255
      Index           =   2
      Left            =   11580
      TabIndex        =   26
      Top             =   3600
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Department"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   10
      Left            =   7680
      TabIndex        =   25
      Top             =   540
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "User Logon"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   8
      Left            =   7680
      TabIndex        =   24
      Top             =   120
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "CETA User ID"
      Height          =   255
      Index           =   0
      Left            =   14040
      TabIndex        =   23
      Top             =   3240
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Confirm"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   7
      Left            =   7680
      TabIndex        =   22
      Top             =   3240
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Password"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   7680
      TabIndex        =   21
      Top             =   2880
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Full Name"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   7680
      TabIndex        =   20
      Top             =   1380
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Initials"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   7680
      TabIndex        =   19
      Top             =   1740
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Telephone"
      Height          =   255
      Index           =   5
      Left            =   7680
      TabIndex        =   18
      Top             =   2100
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Email"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   7680
      TabIndex        =   17
      Top             =   2460
      Width           =   1035
   End
End
Attribute VB_Name = "frmUserInformation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub chkShowFormerStaff_Click()

Dim l_strSQL As String

If chkShowFormerStaff.Value <> 0 Then

    l_strSQL = "SELECT cetauser.username, cetauser.fullname, cetauser.cetauserID, cetauser.email, cetauser2.username as 'securityprofile' FROM cetauser LEFT JOIN cetauser AS cetauser2 ON cetauser.securitytemplate = cetauser2.cetauserID ORDER BY cetauser.username"
    
    adoUser.ConnectionString = g_strConnection
    adoUser.RecordSource = l_strSQL
    adoUser.Refresh
    adoUser.Caption = adoUser.Recordset.RecordCount & " users"
    
Else

    l_strSQL = "SELECT cetauser.username, cetauser.fullname, cetauser.cetauserID, cetauser.email, cetauser2.username as 'securityprofile' FROM cetauser LEFT JOIN cetauser AS cetauser2 ON cetauser.securitytemplate = cetauser2.cetauserID WHERE cetauser.securitytemplate <> 120 ORDER BY cetauser.username"
    
    adoUser.ConnectionString = g_strConnection
    adoUser.RecordSource = l_strSQL
    adoUser.Refresh
    adoUser.Caption = adoUser.Recordset.RecordCount & " users"
    
End If

End Sub

Private Sub cmdClear_Click()

ShowUser -1
ClearFields Me
End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdCopyTo_Click()

End Sub

Private Sub cmdEncryptAllPasswords_Click()

Dim i As Integer
i = MsgBox("Are you sure you want to encrypt all CFM passwords? If you have already done this before it will render your passwords unusable!", vbQuestion + vbYesNo)
If i = vbNo Then Exit Sub



Dim l_con As New ADODB.Connection
Dim l_rst As New ADODB.Recordset

l_con.Open g_strConnection
l_rst.Open "SELECT cetauserID, fd_password FROM cetauser ORDER BY cetauserID", l_con, adOpenDynamic, adLockOptimistic

'start at the beginning
l_rst.MoveFirst

'loop through each record
Do While Not l_rst.EOF
    
    ExecuteSQL "UPDATE cetauser SET fd_password = '" & MD5Encrypt(l_rst("fd_password")) & "' WHERE cetauserID = '" & l_rst("cetauserID") & "';", g_strExecuteError
    CheckForSQLError
    
    'move to the next
    l_rst.MoveNext
Loop

l_rst.Close
Set l_rst = Nothing

MsgBox "All users now have encrypted passwords", vbExclamation

'send us an email to say that the passwords have changed
'SendSMTPMail "support@cetasoft.net", "CETA Support", "Passwords have been encrypted", "", "The password list has just been encrypted!", False, "", ""


End Sub

Private Sub cmdImport_Click()
frmSQLAdmin.Show vbModal
End Sub



Private Sub cmdRefresh_Click()

Dim l_strSQL As String
l_strSQL = "SELECT cetauser.username, cetauser.fullname, cetauser.cetauserID, cetauser.email, cetauser2.username as 'securityprofile' FROM cetauser LEFT JOIN cetauser AS cetauser2 ON cetauser.securitytemplate = cetauser2.cetauserID ORDER BY " & cmbOrderBy.Text & " " & cmbDirection.List(cmbDirection.ListIndex) & ";"

adoUser.ConnectionString = g_strConnection
adoUser.RecordSource = l_strSQL
adoUser.Refresh

End Sub

Private Sub cmdSave_Click()

'check passwords match
If txtPassword.Text <> txtConfirm.Text Then
    MsgBox "The passwords entered do not match", vbExclamation
    txtPassword.SetFocus
    Exit Sub
End If


If txtPassword.Text <> "" And txtConfirm.Text <> "" Then
    'check for password strength
    Dim l_blnPasswordCheck As Boolean
    If CheckPasswordStrength(txtPassword.Text) = False Then
        MsgBox "The password specified does not meet the strength requirements. Please try again.", vbExclamation
        txtPassword.SetFocus
        Exit Sub
    End If
End If

If txtUserName.Text = "" Then
    MsgBox "You must enter a user name", vbExclamation
    txtUserName.SetFocus
    Exit Sub
End If

If txtFullName.Text = "" Then
    MsgBox "You must enter a Full name", vbExclamation
    txtFullName.SetFocus
    Exit Sub
End If

If txtInitials.Text = "" Then
    MsgBox "You must enter some initials", vbExclamation
    txtInitials.SetFocus
    Exit Sub
End If

If cmbDepartment.Text = "" Then
    MsgBox "You must select a department", vbExclamation
    cmbDepartment.SetFocus
    Exit Sub
End If

If txtEmail.Text = "" Then
    MsgBox "You must enter an email address for this user", vbExclamation
    txtEmail.SetFocus
    Exit Sub
End If
    
    
Dim l_strSQL As String
If txtUserID.Text <> "" Then
    l_strSQL = "SELECT count(initials) FROM cetauser WHERE initials = '" & txtInitials.Text & "' AND cetauserID <> '" & txtUserID.Text & "';"
Else
    l_strSQL = "SELECT count(initials) FROM cetauser WHERE initials = '" & txtInitials.Text & "';"
End If

If GetCount(l_strSQL) <> 0 Then
    MsgBox "You already have a user in the system with those initials.", vbExclamation
    Exit Sub
End If

' Declare Variables
Dim l_strAccesscode As String
Dim l_intLoop As Integer
' Loop through all controls on form
For l_intLoop = 0 To Me.Controls.Count - 1
    'If the control is a check box
    If TypeOf Me.Controls(l_intLoop) Is CheckBox Then
        'if the checkbox is ticked
        If Me.Controls(l_intLoop).Value = 1 Then
            ' Append the cntrols tag property to the access coe variable
            l_strAccesscode = l_strAccesscode & "/" & Me.Controls(l_intLoop).Tag
        End If
    End If
Next

l_strAccesscode = txtAccessCode.Text


Dim l_lngCETAUserID As Long
l_lngCETAUserID = SaveUser(Val(txtUserID.Text), txtUserName.Text, txtPassword.Text, txtMobileLoginCode.Text, txtFullName.Text, txtInitials.Text, l_strAccesscode, txtEmail.Text, txtTelephone.Text, cmbDepartment.Text, Val(txtMaxPOValue.Text), cmbDefaultView.Text, cmbDefaultJobType.Text, cmbDefaultDisplay.Text, cmbDefaultAllocation.Text, cmbDefaultJobTab.Text, GetData("cetauser", "cetauserID", "fullname", cmbSecurityTemplate.Text), CStr(txtJobTitle.Text), chkPasswordNeverExpires.Value, chkListAsOperator.Value, chkListAsMediaServices.Value, chkInvestigateIssues.Value, chkSignoffIssues.Value)
txtUserID.Text = CStr(l_lngCETAUserID)

adoUser.Refresh
adoUser.Caption = adoUser.Recordset.RecordCount & " users"

adoUser.Recordset.Find "cetauserID = " & l_lngCETAUserID

End Sub

Private Sub Form_Load()

cmbDirection.AddItem "ASC"
cmbDirection.AddItem "DESC"
cmbDirection.ListIndex = 0


PopulateCombo "department", cmbDepartment
PopulateCombo "view", cmbDefaultView
PopulateCombo "jobtype", cmbDefaultJobType
PopulateCombo "display", cmbDefaultDisplay
PopulateCombo "allocation", cmbDefaultAllocation

PopulateCombo "users", cmbSecurityTemplate

cmbDefaultJobTab.AddItem "Schedule"
cmbDefaultJobTab.AddItem "Dubbing"
cmbDefaultJobTab.AddItem "Media"
cmbDefaultJobTab.AddItem "Costing"

Dim l_strSQL As String
l_strSQL = "SELECT cetauser.username, cetauser.fullname, cetauser.cetauserID, cetauser.email, cetauser2.username as 'securityprofile' FROM cetauser LEFT JOIN cetauser AS cetauser2 ON cetauser.securitytemplate = cetauser2.cetauserID WHERE cetauser.securitytemplate <> 120 ORDER BY cetauser.username"

adoUser.ConnectionString = g_strConnection
adoUser.RecordSource = l_strSQL
adoUser.Refresh
adoUser.Caption = adoUser.Recordset.RecordCount & " users"

CenterForm Me

End Sub

Private Sub grdUsers_HeadClick(ByVal ColIndex As Integer)

If cmbOrderBy.Text <> grdUsers.Columns(ColIndex).DataField Then
    cmbOrderBy.Text = grdUsers.Columns(ColIndex).DataField
Else
    cmbDirection.ListIndex = 1 - cmbDirection.ListIndex
End If

cmdRefresh_Click


End Sub

Private Sub grdUsers_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
ShowUser Val(grdUsers.Columns("cetauserID").Text)
End Sub

Private Sub txtUserID_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    If IsNumeric(txtUserID.Text) Then
        ShowUser Val(txtUserID.Text)
    End If
    HighLite txtUserID
End If
End Sub
