VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmSettings 
   Caption         =   "Settings"
   ClientHeight    =   10770
   ClientLeft      =   3420
   ClientTop       =   4680
   ClientWidth     =   18330
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmOptions.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10770
   ScaleWidth      =   18330
   Begin TabDlg.SSTab tabSettings 
      Height          =   10095
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   18075
      _ExtentX        =   31882
      _ExtentY        =   17806
      _Version        =   393216
      Style           =   1
      Tabs            =   12
      Tab             =   4
      TabsPerRow      =   12
      TabHeight       =   617
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "frmOptions.frx":08CA
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame11"
      Tab(0).Control(1)=   "Frame2"
      Tab(0).Control(2)=   "Frame5"
      Tab(0).Control(3)=   "Frame4"
      Tab(0).Control(4)=   "Frame3"
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "System"
      TabPicture(1)   =   "frmOptions.frx":08E6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame10"
      Tab(1).Control(1)=   "fraConnections"
      Tab(1).Control(2)=   "fraPassword"
      Tab(1).ControlCount=   3
      TabCaption(2)   =   "Sequences"
      TabPicture(2)   =   "frmOptions.frx":0902
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "adoAllocation"
      Tab(2).Control(1)=   "adoSequence"
      Tab(2).Control(2)=   "grdSequence"
      Tab(2).Control(3)=   "grdAllocation"
      Tab(2).ControlCount=   4
      TabCaption(3)   =   "Schedule"
      TabPicture(3)   =   "frmOptions.frx":091E
      Tab(3).ControlEnabled=   0   'False
      Tab(3).ControlCount=   0
      TabCaption(4)   =   "Web / Content Delivery"
      TabPicture(4)   =   "frmOptions.frx":093A
      Tab(4).ControlEnabled=   -1  'True
      Tab(4).Control(0)=   "Frame9"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).Control(1)=   "Frame13"
      Tab(4).Control(1).Enabled=   0   'False
      Tab(4).Control(2)=   "Frame14"
      Tab(4).Control(2).Enabled=   0   'False
      Tab(4).ControlCount=   3
      TabCaption(5)   =   "Departments"
      TabPicture(5)   =   "frmOptions.frx":0956
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "grdDepartments"
      Tab(5).Control(1)=   "adoDepartments"
      Tab(5).ControlCount=   2
      TabCaption(6)   =   "Quote and Purchase"
      TabPicture(6)   =   "frmOptions.frx":0972
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "chkRequireProductOnQuotes"
      Tab(6).Control(1)=   "txtMaximumQuoteDiscount"
      Tab(6).Control(2)=   "Frame7"
      Tab(6).Control(3)=   "chkRequireVideoStandardOnQuote"
      Tab(6).Control(4)=   "Label3(3)"
      Tab(6).Control(5)=   "Label3(4)"
      Tab(6).ControlCount=   6
      TabCaption(7)   =   "Costing and Invoicing"
      TabPicture(7)   =   "frmOptions.frx":098E
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "Frame17"
      Tab(7).Control(1)=   "Frame16"
      Tab(7).Control(2)=   "fraExcelOrders"
      Tab(7).Control(3)=   "fraExport"
      Tab(7).Control(4)=   "fraSundry"
      Tab(7).Control(5)=   "Frame15"
      Tab(7).ControlCount=   6
      TabCaption(8)   =   "Library and Despatch"
      TabPicture(8)   =   "frmOptions.frx":09AA
      Tab(8).ControlEnabled=   0   'False
      Tab(8).Control(0)=   "Frame19"
      Tab(8).Control(1)=   "Frame18"
      Tab(8).ControlCount=   2
      TabCaption(9)   =   "Company"
      TabPicture(9)   =   "frmOptions.frx":09C6
      Tab(9).ControlEnabled=   0   'False
      Tab(9).Control(0)=   "Frame20"
      Tab(9).ControlCount=   1
      TabCaption(10)  =   "EPG System"
      TabPicture(10)  =   "frmOptions.frx":09E2
      Tab(10).ControlEnabled=   0   'False
      Tab(10).Control(0)=   "Frame21"
      Tab(10).ControlCount=   1
      TabCaption(11)  =   "Email"
      TabPicture(11)  =   "frmOptions.frx":09FE
      Tab(11).ControlEnabled=   0   'False
      Tab(11).Control(0)=   "Frame12"
      Tab(11).ControlCount=   1
      Begin VB.Frame Frame12 
         Caption         =   "Email Details and Defaults"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   7995
         Left            =   -74820
         TabIndex        =   294
         Top             =   540
         Width           =   7275
         Begin VB.TextBox txtSMTPServerAuthorised 
            Height          =   315
            Left            =   2700
            TabIndex        =   361
            Top             =   1440
            Width           =   4395
         End
         Begin VB.TextBox txtSMTPUserNameAuthorised 
            Height          =   315
            IMEMode         =   3  'DISABLE
            Left            =   2700
            TabIndex        =   360
            Top             =   1800
            Width           =   4395
         End
         Begin VB.TextBox txtSMTPPasswordAUthorised 
            Height          =   315
            IMEMode         =   3  'DISABLE
            Left            =   2700
            PasswordChar    =   "*"
            TabIndex        =   359
            Top             =   2160
            Width           =   4395
         End
         Begin VB.TextBox txtVDMSEmailForInvoice 
            Height          =   315
            Left            =   2700
            TabIndex        =   357
            Top             =   7020
            Width           =   4395
         End
         Begin VB.TextBox txtManagerEmailAddress 
            Height          =   315
            Left            =   2700
            TabIndex        =   315
            Top             =   6660
            Width           =   4395
         End
         Begin VB.TextBox txtQuotesEmailName 
            Height          =   315
            Left            =   2700
            TabIndex        =   314
            Top             =   6300
            Width           =   4395
         End
         Begin VB.TextBox txtQuotesEmailAddress 
            Height          =   315
            Left            =   2700
            TabIndex        =   313
            Top             =   5940
            Width           =   4395
         End
         Begin VB.TextBox txtBookingsEmailName 
            Height          =   315
            Left            =   2700
            TabIndex        =   312
            Top             =   5580
            Width           =   4395
         End
         Begin VB.TextBox txtBookingsEmailAddress 
            Height          =   315
            Left            =   2700
            TabIndex        =   311
            Top             =   5220
            Width           =   4395
         End
         Begin VB.TextBox txtOperationsEmailName 
            Height          =   315
            Left            =   2700
            TabIndex        =   310
            Top             =   4860
            Width           =   4395
         End
         Begin VB.TextBox txtOperationsEmailAddress 
            Height          =   315
            Left            =   2700
            TabIndex        =   309
            Top             =   4500
            Width           =   4395
         End
         Begin VB.TextBox txtAdministratorEmailName 
            Height          =   315
            Left            =   2700
            TabIndex        =   308
            Top             =   4140
            Width           =   4395
         End
         Begin VB.TextBox txtAdministratorEmailAddress 
            Height          =   315
            Left            =   2700
            TabIndex        =   306
            Top             =   3780
            Width           =   4395
         End
         Begin VB.TextBox txtSMTPPassword 
            Height          =   315
            IMEMode         =   3  'DISABLE
            Left            =   2700
            PasswordChar    =   "*"
            TabIndex        =   300
            Top             =   1080
            Width           =   4395
         End
         Begin VB.TextBox txtSMTPUserName 
            Height          =   315
            Left            =   2700
            TabIndex        =   299
            Top             =   720
            Width           =   4395
         End
         Begin VB.TextBox txtSMTPServer 
            Height          =   315
            Left            =   2700
            TabIndex        =   298
            Top             =   360
            Width           =   4395
         End
         Begin VB.CommandButton cmdSaveSettingsAndTestEmail 
            Caption         =   "Save settings and test email"
            Height          =   375
            Left            =   3900
            TabIndex        =   297
            Top             =   2640
            Width           =   3135
         End
         Begin VB.TextBox txtCETAAdministratorEmail 
            Height          =   315
            Left            =   2700
            TabIndex        =   296
            Top             =   3060
            Width           =   4395
         End
         Begin VB.TextBox txtCetaMailFromEmailDefault 
            Height          =   315
            Left            =   2700
            TabIndex        =   295
            Top             =   3420
            Width           =   4395
         End
         Begin VB.Label lblCaption 
            Caption         =   "SMTP Authorised Server Name"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   364
            Top             =   1500
            Width           =   2475
         End
         Begin VB.Label lblCaption 
            Caption         =   "Authorised Username"
            Height          =   255
            Index           =   78
            Left            =   120
            TabIndex        =   363
            Top             =   1860
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "Authorised Password"
            Height          =   255
            Index           =   77
            Left            =   120
            TabIndex        =   362
            Top             =   2220
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "VDMS Email For Invoices"
            Height          =   255
            Index           =   31
            Left            =   120
            TabIndex        =   358
            Top             =   7080
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "Manager Email Address"
            Height          =   255
            Index           =   76
            Left            =   120
            TabIndex        =   323
            Top             =   6720
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "Quotes Email Name"
            Height          =   255
            Index           =   75
            Left            =   120
            TabIndex        =   322
            Top             =   6360
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "Quotes Email Address"
            Height          =   255
            Index           =   74
            Left            =   120
            TabIndex        =   321
            Top             =   6000
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "Bookings Email Name"
            Height          =   255
            Index           =   73
            Left            =   120
            TabIndex        =   320
            Top             =   5640
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "Bookings Email Address"
            Height          =   255
            Index           =   72
            Left            =   120
            TabIndex        =   319
            Top             =   5280
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "Operations Email Name"
            Height          =   255
            Index           =   71
            Left            =   120
            TabIndex        =   318
            Top             =   4920
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "Operations Email Address"
            Height          =   255
            Index           =   70
            Left            =   120
            TabIndex        =   317
            Top             =   4560
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "Media Window Administrator Name"
            Height          =   255
            Index           =   69
            Left            =   120
            TabIndex        =   316
            Top             =   4200
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "Media Window Administrator Email"
            Height          =   255
            Index           =   68
            Left            =   120
            TabIndex        =   307
            Top             =   3840
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "Password"
            Height          =   255
            Index           =   14
            Left            =   120
            TabIndex        =   305
            Top             =   1140
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "Username"
            Height          =   255
            Index           =   15
            Left            =   120
            TabIndex        =   304
            Top             =   780
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "SMTP Server Hostname"
            Height          =   255
            Index           =   16
            Left            =   120
            TabIndex        =   303
            Top             =   420
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "CETA Administrator Email"
            Height          =   255
            Index           =   18
            Left            =   120
            TabIndex        =   302
            Top             =   3120
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "CETA Email From Default"
            Height          =   255
            Index           =   67
            Left            =   120
            TabIndex        =   301
            Top             =   3480
            Width           =   3375
         End
      End
      Begin VB.Frame Frame21 
         Height          =   3615
         Left            =   -74820
         TabIndex        =   231
         Top             =   600
         Width           =   9255
         Begin VB.TextBox txtJCA4x3TranscodeWatchfolder 
            Height          =   315
            Left            =   2640
            TabIndex        =   245
            Top             =   3060
            Width           =   6375
         End
         Begin VB.TextBox txtJCA16x9TranscodeWatchfolder 
            Height          =   315
            Left            =   2640
            TabIndex        =   244
            Top             =   2700
            Width           =   6375
         End
         Begin VB.TextBox txtBBCAkamaiFTPaddress 
            Height          =   315
            Left            =   2640
            TabIndex        =   237
            Top             =   540
            Width           =   6375
         End
         Begin VB.TextBox txtBBCAkamaiFTPusername 
            Height          =   315
            Left            =   2640
            TabIndex        =   236
            Top             =   900
            Width           =   6375
         End
         Begin VB.TextBox txtBBCAkamaiFTPpassword 
            Height          =   315
            Left            =   2640
            TabIndex        =   235
            Top             =   1260
            Width           =   6375
         End
         Begin VB.TextBox txtBBCAkamaiFTProotdirectory 
            Height          =   315
            Left            =   2640
            TabIndex        =   234
            Top             =   1620
            Width           =   6375
         End
         Begin VB.TextBox txtBBC16x9TranscodeWatchfolder 
            Height          =   315
            Left            =   2640
            TabIndex        =   233
            Top             =   1980
            Width           =   6375
         End
         Begin VB.TextBox txtBBC4x3TranscodeWatchfolder 
            Height          =   315
            Left            =   2640
            TabIndex        =   232
            Top             =   2340
            Width           =   6375
         End
         Begin VB.Label lblCaption 
            Caption         =   "JCA 4x3 Transcode Watchfolder"
            Height          =   255
            Index           =   46
            Left            =   120
            TabIndex        =   247
            Top             =   3120
            Width           =   2475
         End
         Begin VB.Label lblCaption 
            Caption         =   "JCA 16x9 Transcode Watchfolder"
            Height          =   255
            Index           =   45
            Left            =   120
            TabIndex        =   246
            Top             =   2760
            Width           =   2475
         End
         Begin VB.Label lblCaption 
            Caption         =   "BBC Akamai FTP Address"
            Height          =   255
            Index           =   39
            Left            =   120
            TabIndex        =   243
            Top             =   600
            Width           =   2055
         End
         Begin VB.Label lblCaption 
            Caption         =   "BBC Akamai FTP Usename"
            Height          =   255
            Index           =   40
            Left            =   120
            TabIndex        =   242
            Top             =   960
            Width           =   1995
         End
         Begin VB.Label lblCaption 
            Caption         =   "BBC Akamai FTP Password"
            Height          =   255
            Index           =   41
            Left            =   120
            TabIndex        =   241
            Top             =   1320
            Width           =   2055
         End
         Begin VB.Label lblCaption 
            Caption         =   "BBC Akamai FTP Root Directory"
            Height          =   255
            Index           =   42
            Left            =   120
            TabIndex        =   240
            Top             =   1680
            Width           =   2475
         End
         Begin VB.Label lblCaption 
            Caption         =   "BBC 16x9 Transcode Watchfolder"
            Height          =   255
            Index           =   43
            Left            =   120
            TabIndex        =   239
            Top             =   2040
            Width           =   2475
         End
         Begin VB.Label lblCaption 
            Caption         =   "BBC 4x3 Transcode Watchfolder"
            Height          =   255
            Index           =   44
            Left            =   120
            TabIndex        =   238
            Top             =   2400
            Width           =   2475
         End
      End
      Begin VB.Frame Frame20 
         BorderStyle     =   0  'None
         Caption         =   "Frame20"
         Height          =   7635
         Left            =   -74880
         TabIndex        =   200
         Top             =   540
         Width           =   10575
         Begin VB.TextBox txtCompanyPrefix 
            BackColor       =   &H00C0FFC0&
            Height          =   315
            Left            =   8520
            TabIndex        =   229
            Top             =   0
            Width           =   1095
         End
         Begin VB.TextBox txtSetup 
            BackColor       =   &H00FFC0FF&
            DataField       =   "companyname"
            Height          =   315
            Index           =   0
            Left            =   2520
            TabIndex        =   214
            Top             =   0
            Width           =   3735
         End
         Begin VB.TextBox txtSetup 
            BackColor       =   &H00FFC0FF&
            DataField       =   "address"
            Height          =   1155
            Index           =   1
            Left            =   2520
            MultiLine       =   -1  'True
            TabIndex        =   213
            Top             =   360
            Width           =   3735
         End
         Begin VB.TextBox txtSetup 
            BackColor       =   &H00FFC0FF&
            DataField       =   "postcode"
            Height          =   315
            Index           =   2
            Left            =   2520
            TabIndex        =   212
            Top             =   1560
            Width           =   3735
         End
         Begin VB.TextBox txtSetup 
            BackColor       =   &H00FFC0FF&
            DataField       =   "telephone"
            Height          =   315
            Index           =   3
            Left            =   2520
            TabIndex        =   211
            Top             =   1980
            Width           =   3735
         End
         Begin VB.TextBox txtSetup 
            BackColor       =   &H00FFC0FF&
            DataField       =   "fax"
            Height          =   315
            Index           =   4
            Left            =   2520
            TabIndex        =   210
            Top             =   2340
            Width           =   3735
         End
         Begin VB.TextBox txtSetup 
            BackColor       =   &H00FFC0FF&
            DataField       =   "website"
            Height          =   315
            Index           =   5
            Left            =   2520
            TabIndex        =   209
            Top             =   2700
            Width           =   3735
         End
         Begin VB.TextBox txtSetup 
            BackColor       =   &H00FFC0FF&
            DataField       =   "email"
            Height          =   315
            Index           =   6
            Left            =   2520
            TabIndex        =   208
            Top             =   3060
            Width           =   3735
         End
         Begin VB.TextBox txtSetup 
            BackColor       =   &H00FFFFC0&
            DataField       =   "registeredofficeaddress"
            Height          =   1155
            Index           =   8
            Left            =   2520
            MultiLine       =   -1  'True
            TabIndex        =   207
            Top             =   4020
            Width           =   3735
         End
         Begin VB.TextBox txtSetup 
            BackColor       =   &H00FFFFC0&
            DataField       =   "registeredofficepostcode"
            Height          =   315
            Index           =   9
            Left            =   2520
            TabIndex        =   206
            Top             =   5220
            Width           =   3735
         End
         Begin VB.TextBox txtSetup 
            BackColor       =   &H00FFFFC0&
            DataField       =   "registeredofficenumber"
            Height          =   315
            Index           =   7
            Left            =   2520
            TabIndex        =   205
            Top             =   3660
            Width           =   3735
         End
         Begin VB.TextBox txtSetup 
            BackColor       =   &H0080FF80&
            DataField       =   "vatregistrationnumber"
            Height          =   315
            Index           =   10
            Left            =   2520
            TabIndex        =   204
            Top             =   5700
            Width           =   3735
         End
         Begin VB.TextBox txtSetup 
            BackColor       =   &H0080C0FF&
            DataField       =   "exportfield1"
            Height          =   315
            Index           =   11
            Left            =   2520
            TabIndex        =   203
            Top             =   6240
            Width           =   3735
         End
         Begin VB.TextBox txtSetup 
            BackColor       =   &H0080C0FF&
            DataField       =   "exportfield2"
            Height          =   315
            Index           =   12
            Left            =   2520
            TabIndex        =   202
            Top             =   6600
            Width           =   3735
         End
         Begin VB.TextBox txtSetup 
            BackColor       =   &H0080C0FF&
            DataField       =   "exportfield3"
            Height          =   315
            Index           =   13
            Left            =   2520
            TabIndex        =   201
            Top             =   6960
            Width           =   3735
         End
         Begin VB.Label Label1 
            Caption         =   "Prefix (for barcodes etc)"
            Height          =   255
            Index           =   25
            Left            =   6540
            TabIndex        =   230
            Top             =   60
            Width           =   2295
         End
         Begin VB.Label Label1 
            Caption         =   "Company Name"
            Height          =   255
            Index           =   9
            Left            =   0
            TabIndex        =   228
            Top             =   0
            Width           =   2295
         End
         Begin VB.Label Label1 
            Caption         =   "Address"
            Height          =   255
            Index           =   10
            Left            =   0
            TabIndex        =   227
            Top             =   360
            Width           =   2295
         End
         Begin VB.Label Label1 
            Caption         =   "Post Code"
            Height          =   255
            Index           =   11
            Left            =   0
            TabIndex        =   226
            Top             =   1500
            Width           =   2295
         End
         Begin VB.Label Label1 
            Caption         =   "Telephone"
            Height          =   255
            Index           =   12
            Left            =   0
            TabIndex        =   225
            Top             =   1980
            Width           =   2295
         End
         Begin VB.Label Label1 
            Caption         =   "Fax"
            Height          =   255
            Index           =   13
            Left            =   0
            TabIndex        =   224
            Top             =   2340
            Width           =   2295
         End
         Begin VB.Label Label1 
            Caption         =   "Website"
            Height          =   255
            Index           =   14
            Left            =   0
            TabIndex        =   223
            Top             =   2700
            Width           =   2295
         End
         Begin VB.Label Label1 
            Caption         =   "Email"
            Height          =   255
            Index           =   15
            Left            =   0
            TabIndex        =   222
            Top             =   3060
            Width           =   2295
         End
         Begin VB.Label Label1 
            Caption         =   "Registered Office Address"
            Height          =   255
            Index           =   16
            Left            =   0
            TabIndex        =   221
            Top             =   4020
            Width           =   2295
         End
         Begin VB.Label Label1 
            Caption         =   "Registered Office Postcode"
            Height          =   255
            Index           =   17
            Left            =   0
            TabIndex        =   220
            Top             =   5220
            Width           =   2295
         End
         Begin VB.Label Label1 
            Caption         =   "Registered Office Number"
            Height          =   255
            Index           =   18
            Left            =   0
            TabIndex        =   219
            Top             =   3660
            Width           =   2295
         End
         Begin VB.Label Label1 
            Caption         =   "VAT Registration Number"
            Height          =   255
            Index           =   19
            Left            =   0
            TabIndex        =   218
            Top             =   5700
            Width           =   2295
         End
         Begin VB.Label Label1 
            Caption         =   "Export Field 1"
            Height          =   255
            Index           =   21
            Left            =   0
            TabIndex        =   217
            Top             =   6240
            Width           =   2295
         End
         Begin VB.Label Label1 
            Caption         =   "Export Field 2"
            Height          =   255
            Index           =   22
            Left            =   0
            TabIndex        =   216
            Top             =   6600
            Width           =   2295
         End
         Begin VB.Label Label1 
            Caption         =   "Export Field 3"
            Height          =   255
            Index           =   23
            Left            =   0
            TabIndex        =   215
            Top             =   6960
            Width           =   2295
         End
      End
      Begin VB.Frame Frame19 
         BorderStyle     =   0  'None
         Caption         =   "Frame19"
         Height          =   9075
         Left            =   -74880
         TabIndex        =   172
         Top             =   480
         Width           =   5415
         Begin VB.CheckBox chkDoOfflineCFMRequests 
            Alignment       =   1  'Right Justify
            Caption         =   "Do Offline CETA File Manager Requests"
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   0
            TabIndex        =   345
            Top             =   8520
            Width           =   5115
         End
         Begin VB.TextBox txtTapePDFSaveLocation 
            Height          =   315
            Left            =   2760
            TabIndex        =   289
            Top             =   1020
            Width           =   2355
         End
         Begin VB.TextBox txtFilePDFSaveLocation 
            Height          =   315
            Left            =   2760
            TabIndex        =   287
            Top             =   660
            Width           =   2355
         End
         Begin VB.CheckBox chkCreateDeliveryNotesForJobsWithNoAddress 
            Alignment       =   1  'Right Justify
            Caption         =   "Dont use client address when creating delivery notes"
            Height          =   195
            Left            =   0
            TabIndex        =   192
            Top             =   3960
            Width           =   5115
         End
         Begin VB.TextBox txtNoOfCopies 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   4680
            TabIndex        =   191
            Top             =   1380
            Width           =   435
         End
         Begin VB.TextBox txtPDFSaveLocation 
            Height          =   315
            Left            =   2760
            TabIndex        =   190
            Top             =   300
            Width           =   2355
         End
         Begin VB.CheckBox chkCreatePDFFilesOfTechReviews 
            Alignment       =   1  'Right Justify
            Caption         =   "Automatically create PDF files of technical reviews on save"
            Height          =   195
            Left            =   0
            TabIndex        =   189
            Top             =   0
            Width           =   5115
         End
         Begin VB.CheckBox chkShowAdditionalEventFields 
            Alignment       =   1  'Right Justify
            Caption         =   "Show additional event fields in Library grid"
            Height          =   195
            Left            =   0
            TabIndex        =   188
            Top             =   1800
            Width           =   5115
         End
         Begin VB.CheckBox chkPromptForLateDespatchReason 
            Alignment       =   1  'Right Justify
            Caption         =   "Prompt for late despatch reason"
            Height          =   195
            Left            =   0
            TabIndex        =   187
            ToolTipText     =   "When completing jobs after there specified deadline a box is shown asking why the job was late"
            Top             =   2160
            Width           =   5115
         End
         Begin VB.ComboBox cmbDefaultLibrarySearchField 
            Height          =   315
            Left            =   2640
            TabIndex        =   186
            Top             =   2460
            Width           =   2535
         End
         Begin VB.CheckBox chkUseJobDetailsWhenAddingJobDetailLinesToDespatch 
            Alignment       =   1  'Right Justify
            Caption         =   "Use Job Details When Adding Job Detail Lines To Despatch"
            Height          =   195
            Left            =   0
            TabIndex        =   185
            ToolTipText     =   "When completing jobs after there specified deadline a box is shown asking why the job was late"
            Top             =   2880
            Width           =   5115
         End
         Begin VB.CheckBox chkDefaultEventSearchInLibrary 
            Alignment       =   1  'Right Justify
            Caption         =   "Default to searching by events in library search"
            Height          =   195
            Left            =   0
            TabIndex        =   184
            ToolTipText     =   "When completing jobs after their specified deadline a box is shown asking why the job was late"
            Top             =   3240
            Width           =   5115
         End
         Begin VB.CheckBox chkMakeEventsWide 
            Alignment       =   1  'Right Justify
            Caption         =   "Make the events grid full form width in Library form"
            Height          =   195
            Left            =   0
            TabIndex        =   183
            Top             =   3600
            Width           =   5115
         End
         Begin VB.CheckBox chkHideHireDespatchTab 
            Alignment       =   1  'Right Justify
            Caption         =   "Always Hide ""HIRE / DESPATCH"" tab in jobs"
            Height          =   195
            Left            =   0
            TabIndex        =   182
            Top             =   4320
            Width           =   5115
         End
         Begin VB.TextBox txtDefaultInternalMovementLocation 
            Height          =   315
            Left            =   1800
            TabIndex        =   181
            Top             =   4620
            Width           =   3315
         End
         Begin VB.CheckBox chkUseAudioStandardInLibraryToStoreLanguage 
            Alignment       =   1  'Right Justify
            Caption         =   "Use 'Audio Standard' in library to store Language(s)"
            Height          =   195
            Left            =   0
            TabIndex        =   180
            Top             =   6720
            Width           =   5115
         End
         Begin VB.CheckBox chkDontUseDynoBarcodeLabel 
            Alignment       =   1  'Right Justify
            Caption         =   "Dont Use Dymo Printer For Barcode Labels"
            Height          =   255
            Left            =   0
            TabIndex        =   179
            Top             =   7080
            Width           =   5115
         End
         Begin VB.CheckBox chkUseFormattedTimeCodesInEvents 
            Alignment       =   1  'Right Justify
            Caption         =   "Format time codes to 00:00:00:00 style"
            Height          =   255
            Left            =   0
            TabIndex        =   178
            Top             =   7440
            Width           =   5115
         End
         Begin VB.CheckBox chkDisableUpdateLibraryRecordWithJobDetails 
            Alignment       =   1  'Right Justify
            Caption         =   "Disable Update Library Record With Job Details"
            Height          =   255
            Left            =   0
            TabIndex        =   177
            Top             =   7800
            Width           =   5115
         End
         Begin VB.CheckBox chkPrefixLibraryBarcodesWithCompanyPrefix 
            Alignment       =   1  'Right Justify
            Caption         =   "Prefix Library Barcodes With Company Prefix"
            Height          =   255
            Left            =   0
            TabIndex        =   176
            Top             =   8160
            Width           =   5115
         End
         Begin VB.TextBox txtLocationOfRapidsLogs 
            Height          =   315
            Left            =   1800
            TabIndex        =   175
            Top             =   5460
            Width           =   3315
         End
         Begin VB.TextBox txtLocationOfOmneonXLS 
            Height          =   315
            Left            =   1800
            TabIndex        =   174
            Top             =   5880
            Width           =   3315
         End
         Begin VB.TextBox txtLocationOfWatchfolders 
            Height          =   315
            Left            =   1800
            TabIndex        =   173
            Top             =   6300
            Width           =   3315
         End
         Begin VB.Label lblCaption 
            Caption         =   "Location to save Tape Tech Reviews"
            Height          =   255
            Index           =   66
            Left            =   30
            TabIndex        =   290
            Top             =   1080
            Width           =   2715
         End
         Begin VB.Label lblCaption 
            Caption         =   "Location to save File Tech Reviews"
            Height          =   255
            Index           =   65
            Left            =   30
            TabIndex        =   288
            Top             =   720
            Width           =   2535
         End
         Begin VB.Label Label1 
            Caption         =   "Number of Despatch Note Copies to Print"
            Height          =   255
            Index           =   0
            Left            =   30
            TabIndex        =   199
            Top             =   1440
            Width           =   3495
         End
         Begin VB.Label lblCaption 
            Caption         =   "Location to save General PDF files"
            Height          =   255
            Index           =   0
            Left            =   30
            TabIndex        =   198
            Top             =   360
            Width           =   2595
         End
         Begin VB.Label lblCaption 
            Caption         =   "Default Library Search Field"
            Height          =   255
            Index           =   7
            Left            =   30
            TabIndex        =   197
            Top             =   2520
            Width           =   2415
         End
         Begin VB.Label lblCaption 
            Caption         =   "Movement Location"
            Height          =   255
            Index           =   17
            Left            =   30
            TabIndex        =   196
            Top             =   4680
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Location - Rapids Logs"
            Height          =   255
            Index           =   28
            Left            =   30
            TabIndex        =   195
            Top             =   5520
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "Location - Omneon XLS"
            Height          =   255
            Index           =   30
            Left            =   30
            TabIndex        =   194
            Top             =   5940
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "Location - WatchFolder"
            Height          =   255
            Index           =   33
            Left            =   30
            TabIndex        =   193
            Top             =   6360
            Width           =   1695
         End
      End
      Begin VB.Frame Frame18 
         BorderStyle     =   0  'None
         Caption         =   "Frame18"
         Height          =   6675
         Left            =   -69360
         TabIndex        =   159
         Top             =   480
         Width           =   5355
         Begin VB.CheckBox chkDreamworksWarnCompletedRow 
            Alignment       =   1  'Right Justify
            Caption         =   "Warn if Editing Completed Row in Dreamworks Tracker"
            Height          =   255
            Left            =   0
            TabIndex        =   329
            Top             =   5760
            Width           =   5055
         End
         Begin VB.TextBox txtJellyroll2LibraryID 
            Height          =   315
            Left            =   3360
            TabIndex        =   262
            Top             =   4920
            Width           =   1695
         End
         Begin VB.TextBox txtJellyroll1LibraryID 
            Height          =   315
            Left            =   3360
            TabIndex        =   261
            Top             =   4080
            Width           =   1695
         End
         Begin VB.TextBox txtLocationOfJellyroll2Folder 
            Height          =   315
            Left            =   1740
            TabIndex        =   260
            Top             =   5340
            Width           =   3315
         End
         Begin VB.TextBox txtLocationOfJellyroll1Folder 
            Height          =   315
            Left            =   1740
            TabIndex        =   259
            Top             =   4500
            Width           =   3315
         End
         Begin VB.TextBox txtJPEGextension 
            Height          =   315
            Left            =   3360
            TabIndex        =   248
            Top             =   3660
            Width           =   1695
         End
         Begin VB.CheckBox chkHideStandardLabelsButton 
            Alignment       =   1  'Right Justify
            Caption         =   "Hide Standard labels button in Library Print menu"
            Height          =   195
            Left            =   0
            TabIndex        =   171
            Top             =   2100
            Width           =   5055
         End
         Begin VB.CheckBox chkAddFakeJobIDToTapes 
            Alignment       =   1  'Right Justify
            Caption         =   "Add Fake Job ID To New Tapes (to remind users to correct)"
            Height          =   255
            Left            =   0
            TabIndex        =   170
            Top             =   1500
            Width           =   5055
         End
         Begin VB.CheckBox chkAllocateBarcodes 
            Alignment       =   1  'Right Justify
            Caption         =   "Allocate new barcodes"
            Height          =   255
            Left            =   0
            TabIndex        =   169
            Top             =   1800
            Width           =   5055
         End
         Begin VB.CheckBox chkDontClearShelfWhenMovingTapes 
            Alignment       =   1  'Right Justify
            Caption         =   "Don't Clear Shelf When Moving Tapes"
            Height          =   255
            Left            =   0
            TabIndex        =   168
            Top             =   3300
            Width           =   5055
         End
         Begin VB.CheckBox chkShowBillToInformationOnDespatch 
            Alignment       =   1  'Right Justify
            Caption         =   "Show ""Bill To"" information on despatch notes"
            Height          =   255
            Left            =   0
            TabIndex        =   167
            Top             =   0
            Width           =   5055
         End
         Begin VB.CheckBox chkJCADeliverylabels 
            Alignment       =   1  'Right Justify
            Caption         =   "Use JCA Style Address labels"
            Height          =   255
            Left            =   0
            TabIndex        =   166
            Top             =   300
            Width           =   5055
         End
         Begin VB.CheckBox chkDescDeadNotCompulsory 
            Alignment       =   1  'Right Justify
            Caption         =   "Description and Deadline not compulsory on Despatches"
            Height          =   255
            Left            =   0
            TabIndex        =   165
            Top             =   600
            Width           =   5055
         End
         Begin VB.CheckBox chkHideAudioChannels 
            Alignment       =   1  'Right Justify
            Caption         =   "Hide Library Audio Channels"
            Height          =   195
            Left            =   0
            TabIndex        =   164
            Top             =   900
            Width           =   5055
         End
         Begin VB.CheckBox chkDontPromptForConfirmationOnCompletionOfDespatches 
            Alignment       =   1  'Right Justify
            Caption         =   "Don't Prompt For Confirmation on Completion Of Despatches"
            Height          =   195
            Left            =   0
            TabIndex        =   163
            Top             =   1200
            Width           =   5055
         End
         Begin VB.CheckBox chkCheckAllBBCJobs 
            Alignment       =   1  'Right Justify
            Caption         =   "Check all BBC Jobs for Labelling Title Errors"
            Height          =   195
            Left            =   0
            TabIndex        =   162
            Top             =   2400
            Width           =   5055
         End
         Begin VB.CheckBox chkCompulsoryBikeRefs 
            Alignment       =   1  'Right Justify
            Caption         =   "Make Bike Courier references Compulsory"
            Height          =   195
            Left            =   0
            TabIndex        =   161
            Top             =   2700
            Width           =   5055
         End
         Begin VB.CheckBox chkEnforceRigorousLibraryChecks 
            Alignment       =   1  'Right Justify
            Caption         =   "Enforce Rigorous Library Checking"
            Height          =   195
            Left            =   0
            TabIndex        =   160
            Top             =   3000
            Width           =   5055
         End
         Begin VB.Label lblCaption 
            Caption         =   "Jellyroll 2 Folder"
            Height          =   255
            Index           =   55
            Left            =   30
            TabIndex        =   266
            Top             =   5400
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Jellyroll 2 Library ID"
            Height          =   255
            Index           =   54
            Left            =   30
            TabIndex        =   265
            Top             =   4980
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Jellyroll 1 Library ID"
            Height          =   255
            Index           =   53
            Left            =   30
            TabIndex        =   264
            Top             =   4140
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "JPG File Extension"
            Height          =   255
            Index           =   52
            Left            =   30
            TabIndex        =   263
            Top             =   3720
            Width           =   1515
         End
         Begin VB.Label lblCaption 
            Caption         =   "Jellyroll 1 Folder"
            Height          =   255
            Index           =   47
            Left            =   30
            TabIndex        =   249
            Top             =   4560
            Width           =   1515
         End
      End
      Begin VB.Frame Frame17 
         BorderStyle     =   0  'None
         Caption         =   "Frame17"
         Height          =   3555
         Left            =   -74820
         TabIndex        =   145
         Top             =   480
         Width           =   5355
         Begin VB.TextBox txtDefaultTimeWhenZero 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   4740
            TabIndex        =   157
            Top             =   2340
            Width           =   435
         End
         Begin VB.CheckBox chkCostDubbing 
            Alignment       =   1  'Right Justify
            Caption         =   "Cost Dubbing Lines Automatically"
            Height          =   195
            Left            =   60
            TabIndex        =   156
            Top             =   300
            Width           =   5115
         End
         Begin VB.CheckBox chkCostSchedule 
            Alignment       =   1  'Right Justify
            Caption         =   "Cost Scheduled Resources Automatically"
            Height          =   195
            Left            =   60
            TabIndex        =   155
            Top             =   0
            Width           =   5115
         End
         Begin VB.CheckBox chkRequireActualsBeforeCosting 
            Alignment       =   1  'Right Justify
            Caption         =   "Require actuals to be completed before costing jobs"
            Height          =   195
            Left            =   60
            TabIndex        =   154
            Top             =   1500
            Width           =   5115
         End
         Begin VB.CheckBox chkUseMasterDetailOnInvoice 
            Alignment       =   1  'Right Justify
            Caption         =   "Use dub line description for master record when costing"
            Height          =   195
            Left            =   60
            TabIndex        =   153
            Top             =   1800
            Width           =   5115
         End
         Begin VB.CheckBox chkDeductPreSetDiscountsFromUnitPrice 
            Alignment       =   1  'Right Justify
            Caption         =   "Deduct pre-set discounts from unit price"
            Height          =   195
            Left            =   60
            TabIndex        =   152
            ToolTipText     =   "Deduct pre-set discounts from unit price"
            Top             =   1200
            Width           =   5115
         End
         Begin VB.CheckBox chkUseAdditionalDescriptionsWhenCosting 
            Alignment       =   1  'Right Justify
            Caption         =   "Cost master lines using rate card description"
            Height          =   195
            Left            =   60
            TabIndex        =   151
            Top             =   2100
            Width           =   5115
         End
         Begin VB.CheckBox chkAddDurationToRateCode 
            Alignment       =   1  'Right Justify
            Caption         =   "Add duration to rate codes (ie. DIGIBETA030)"
            Height          =   195
            Left            =   60
            TabIndex        =   150
            Top             =   2700
            Width           =   5115
         End
         Begin VB.CheckBox chkAlwaysUseRateCardDescriptions 
            Alignment       =   1  'Right Justify
            Caption         =   "Always use rate card descriptions"
            Height          =   195
            Left            =   60
            TabIndex        =   149
            Top             =   3000
            Width           =   5115
         End
         Begin VB.CheckBox chkForceDealPrice 
            Alignment       =   1  'Right Justify
            Caption         =   "Force entry of deal type and price for 'auto allocated' jobs"
            Height          =   195
            Left            =   60
            TabIndex        =   148
            Top             =   3300
            Width           =   5115
         End
         Begin VB.CheckBox chkDontCostScheduledResourcesIfJobTypeIsADub 
            Alignment       =   1  'Right Justify
            Caption         =   "Dont cost scheduled resources if the job type is a dub"
            Height          =   195
            Left            =   60
            TabIndex        =   147
            Top             =   900
            Width           =   5115
         End
         Begin VB.CheckBox chkCostDespatch 
            Alignment       =   1  'Right Justify
            Caption         =   "Cost Despatch Notes Automatically"
            Height          =   195
            Left            =   60
            TabIndex        =   146
            Top             =   600
            Width           =   5115
         End
         Begin VB.Label Label1 
            Caption         =   "Default Time to use in dubbing when zero"
            Height          =   255
            Index           =   3
            Left            =   105
            TabIndex        =   158
            Top             =   2400
            Width           =   3435
         End
      End
      Begin VB.Frame Frame16 
         BorderStyle     =   0  'None
         Caption         =   "Frame16"
         Height          =   4215
         Left            =   -69240
         TabIndex        =   134
         Top             =   540
         Width           =   5295
         Begin VB.TextBox txtRoundCurrencyDecimals 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   4350
            MaxLength       =   2
            TabIndex        =   141
            Top             =   120
            Width           =   795
         End
         Begin VB.CheckBox chkApplyDealPriceToActualsRatherThanRateCard 
            Alignment       =   1  'Right Justify
            Caption         =   "Apply deal price to actuals rather than rate card prices"
            Height          =   195
            Left            =   0
            TabIndex        =   140
            Top             =   480
            Width           =   5115
         End
         Begin VB.CheckBox chkHideVATFieldsInCostings 
            Alignment       =   1  'Right Justify
            Caption         =   "Hide VAT fields in costings"
            Height          =   195
            Left            =   2700
            TabIndex        =   138
            Top             =   1140
            Width           =   2415
         End
         Begin VB.CheckBox chkLockDiscountColumnInCostings 
            Alignment       =   1  'Right Justify
            Caption         =   "Lock discount column in the costing page from manual entry"
            Height          =   255
            Left            =   0
            TabIndex        =   137
            Top             =   780
            Width           =   5115
         End
         Begin VB.TextBox txtBaseRateCard 
            Alignment       =   2  'Center
            BackColor       =   &H0080C0FF&
            Height          =   285
            Left            =   1650
            TabIndex        =   136
            Text            =   "8"
            Top             =   1140
            Width           =   435
         End
         Begin VB.TextBox txtRateCardType 
            Height          =   285
            Left            =   1410
            TabIndex        =   135
            Top             =   3840
            Width           =   1575
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdVAT 
            Bindings        =   "frmOptions.frx":0A1A
            Height          =   2175
            Left            =   30
            TabIndex        =   139
            ToolTipText     =   "Edit the Sequence Numbers"
            Top             =   1500
            Width           =   5115
            _Version        =   196617
            AllowAddNew     =   -1  'True
            AllowDelete     =   -1  'True
            BackColorOdd    =   12640511
            RowHeight       =   423
            Columns(0).Width=   3200
            _ExtentX        =   9022
            _ExtentY        =   3836
            _StockProps     =   79
            Caption         =   "VAT Rates"
         End
         Begin MSAdodcLib.Adodc adoVAT 
            Height          =   330
            Left            =   840
            Top             =   2400
            Visible         =   0   'False
            Width           =   2265
            _ExtentX        =   3995
            _ExtentY        =   582
            ConnectMode     =   0
            CursorLocation  =   3
            IsolationLevel  =   -1
            ConnectionTimeout=   15
            CommandTimeout  =   30
            CursorType      =   3
            LockType        =   3
            CommandType     =   8
            CursorOptions   =   0
            CacheSize       =   50
            MaxRecords      =   0
            BOFAction       =   0
            EOFAction       =   0
            ConnectStringType=   1
            Appearance      =   1
            BackColor       =   8421631
            ForeColor       =   -2147483640
            Orientation     =   0
            Enabled         =   -1
            Connect         =   ""
            OLEDBString     =   ""
            OLEDBFile       =   ""
            DataSourceName  =   ""
            OtherAttributes =   ""
            UserName        =   ""
            Password        =   ""
            RecordSource    =   ""
            Caption         =   "adoVAT"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            _Version        =   393216
         End
         Begin VB.Label Label3 
            Caption         =   "Round money values to how many decimal places?"
            Height          =   255
            Index           =   2
            Left            =   30
            TabIndex        =   144
            Top             =   120
            Width           =   3675
         End
         Begin VB.Label Label1 
            Caption         =   "Base Rate Card #"
            Height          =   255
            Index           =   24
            Left            =   30
            TabIndex        =   143
            Top             =   1140
            Width           =   1635
         End
         Begin VB.Label Label1 
            Caption         =   "Rate Card Type"
            Height          =   255
            Index           =   26
            Left            =   30
            TabIndex        =   142
            Top             =   3900
            Width           =   1395
         End
      End
      Begin VB.Frame Frame14 
         Caption         =   "Website Defaults"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6915
         Left            =   9900
         TabIndex        =   108
         Top             =   540
         Width           =   7815
         Begin VB.TextBox txtAdminText 
            Height          =   675
            Index           =   3
            Left            =   2460
            MultiLine       =   -1  'True
            TabIndex        =   277
            Top             =   6000
            Width           =   5115
         End
         Begin VB.TextBox txtAdminTitle 
            Height          =   315
            Index           =   3
            Left            =   2460
            TabIndex        =   276
            Top             =   5640
            Width           =   5115
         End
         Begin VB.TextBox txtAdminTitle 
            Height          =   315
            Index           =   2
            Left            =   2460
            TabIndex        =   275
            Top             =   4200
            Width           =   5115
         End
         Begin VB.TextBox txtAdminLink 
            Height          =   315
            Index           =   3
            Left            =   2460
            TabIndex        =   274
            Top             =   5280
            Width           =   5115
         End
         Begin VB.TextBox txtAdminText 
            Height          =   675
            Index           =   2
            Left            =   2460
            MultiLine       =   -1  'True
            TabIndex        =   273
            Top             =   4560
            Width           =   5115
         End
         Begin VB.TextBox txtAdminLink 
            Height          =   315
            Index           =   2
            Left            =   2460
            TabIndex        =   272
            Top             =   3840
            Width           =   5115
         End
         Begin VB.TextBox txtAdminText 
            Height          =   675
            Index           =   1
            Left            =   2460
            MultiLine       =   -1  'True
            TabIndex        =   271
            Top             =   3120
            Width           =   5115
         End
         Begin VB.TextBox txtAdminTitle 
            Height          =   315
            Index           =   1
            Left            =   2460
            TabIndex        =   270
            Top             =   2760
            Width           =   5115
         End
         Begin VB.TextBox txtAdminLink 
            Height          =   315
            Index           =   1
            Left            =   2460
            TabIndex        =   269
            Top             =   2400
            Width           =   5115
         End
         Begin VB.TextBox txtMediaWindowSystemMessage 
            Height          =   675
            Left            =   2460
            MultiLine       =   -1  'True
            TabIndex        =   257
            Top             =   1680
            Width           =   5115
         End
         Begin VB.TextBox txtDefaultMainPage 
            Height          =   315
            Left            =   2460
            TabIndex        =   115
            Top             =   1320
            Width           =   5115
         End
         Begin VB.TextBox txtDefaultPortalAddress 
            Height          =   315
            Left            =   2460
            TabIndex        =   113
            Top             =   960
            Width           =   5115
         End
         Begin VB.TextBox txtDefaultFooterPage 
            Height          =   315
            Left            =   2460
            TabIndex        =   110
            Top             =   600
            Width           =   5115
         End
         Begin VB.TextBox txtDefaultHeaderPage 
            Height          =   315
            Left            =   2460
            TabIndex        =   109
            Top             =   240
            Width           =   5115
         End
         Begin VB.Label lblCaption 
            Caption         =   "Default Text 3"
            Height          =   255
            Index           =   64
            Left            =   120
            TabIndex        =   286
            Top             =   6060
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Default Text 2"
            Height          =   255
            Index           =   63
            Left            =   120
            TabIndex        =   285
            Top             =   4620
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Default Text 1"
            Height          =   255
            Index           =   62
            Left            =   120
            TabIndex        =   284
            Top             =   3180
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Default Title 3"
            Height          =   255
            Index           =   61
            Left            =   120
            TabIndex        =   283
            Top             =   5700
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Default Title 2"
            Height          =   255
            Index           =   60
            Left            =   120
            TabIndex        =   282
            Top             =   4260
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Default Title 1"
            Height          =   255
            Index           =   59
            Left            =   120
            TabIndex        =   281
            Top             =   2820
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Default Link 3"
            Height          =   255
            Index           =   58
            Left            =   120
            TabIndex        =   280
            Top             =   5340
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Default Link 2"
            Height          =   255
            Index           =   57
            Left            =   120
            TabIndex        =   279
            Top             =   3900
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Default Link 1"
            Height          =   255
            Index           =   56
            Left            =   120
            TabIndex        =   278
            Top             =   2460
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Content Delivery System Message"
            Height          =   495
            Index           =   51
            Left            =   120
            TabIndex        =   258
            Top             =   1740
            Width           =   1575
         End
         Begin VB.Label lblCaption 
            Caption         =   "Default Main Page"
            Height          =   255
            Index           =   37
            Left            =   120
            TabIndex        =   116
            Top             =   1380
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Default BrandingFooter Page"
            Height          =   255
            Index           =   36
            Left            =   120
            TabIndex        =   114
            Top             =   660
            Width           =   2355
         End
         Begin VB.Label lblCaption 
            Caption         =   "Default Content Delivery URL"
            Height          =   255
            Index           =   35
            Left            =   120
            TabIndex        =   112
            Top             =   1020
            Width           =   2295
         End
         Begin VB.Label lblCaption 
            Caption         =   "Default Branding Header Page"
            Height          =   255
            Index           =   34
            Left            =   120
            TabIndex        =   111
            Top             =   300
            Width           =   2235
         End
      End
      Begin VB.Frame fraExcelOrders 
         Caption         =   "Excel Order Systems"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Left            =   -74880
         TabIndex        =   102
         Top             =   5520
         Width           =   8355
         Begin VB.TextBox txtExcelOrderLocation 
            Height          =   315
            Left            =   2640
            TabIndex        =   103
            Top             =   240
            Width           =   5535
         End
         Begin VB.Label lblCaption 
            Caption         =   "Excel File Locations"
            Height          =   255
            Index           =   29
            Left            =   180
            TabIndex        =   104
            Top             =   300
            Width           =   2415
         End
      End
      Begin VB.CheckBox chkRequireProductOnQuotes 
         Caption         =   "Require Product on Quotes"
         Height          =   255
         Left            =   -74880
         TabIndex        =   98
         Top             =   3660
         Width           =   3555
      End
      Begin VB.TextBox txtMaximumQuoteDiscount 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   -70860
         MaxLength       =   2
         TabIndex        =   93
         Top             =   4320
         Width           =   795
      End
      Begin VB.Frame Frame13 
         Caption         =   "Media Defaults"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   7635
         Left            =   120
         TabIndex        =   92
         Top             =   2220
         Width           =   9555
         Begin VB.TextBox txtBBCMG_Flashstore 
            Height          =   315
            Left            =   2700
            TabIndex        =   346
            Top             =   3840
            Width           =   6675
         End
         Begin VB.TextBox txtScreenGrabStore 
            Height          =   315
            Left            =   2700
            TabIndex        =   342
            Top             =   960
            Width           =   6675
         End
         Begin VB.TextBox txtBBCMG_Restore 
            Height          =   315
            Left            =   2700
            TabIndex        =   339
            Top             =   4200
            Width           =   6675
         End
         Begin VB.TextBox txtBBCMG_ProxyArchive 
            Height          =   315
            Left            =   2700
            TabIndex        =   336
            Top             =   3480
            Width           =   6675
         End
         Begin VB.TextBox txtBBCMG_ClipArchive 
            Height          =   315
            Left            =   2700
            TabIndex        =   335
            Top             =   3120
            Width           =   6675
         End
         Begin VB.TextBox txtSigniantSendPath 
            Height          =   315
            Left            =   2700
            TabIndex        =   332
            Top             =   4920
            Width           =   6675
         End
         Begin VB.TextBox txtBBCMG_Aspera 
            Height          =   315
            Left            =   2700
            TabIndex        =   330
            Top             =   2760
            Width           =   6675
         End
         Begin VB.TextBox txtTranscodeOverlayFolder 
            Height          =   315
            Left            =   2700
            TabIndex        =   327
            Top             =   4560
            Width           =   6675
         End
         Begin VB.TextBox txtBBCMG_Archive 
            Height          =   315
            Left            =   2700
            TabIndex        =   325
            Top             =   2400
            Width           =   6675
         End
         Begin VB.TextBox txtFlashstorePath 
            Height          =   315
            Left            =   2700
            TabIndex        =   255
            Top             =   1320
            Width           =   6675
         End
         Begin VB.TextBox txtAsperaServer 
            Height          =   315
            Left            =   2700
            TabIndex        =   252
            Top             =   2040
            Width           =   6675
         End
         Begin VB.TextBox txtAsperaRootDirectory 
            Height          =   315
            Left            =   2700
            TabIndex        =   250
            Top             =   1680
            Width           =   6675
         End
         Begin VB.TextBox txtFlashThumbStore 
            Height          =   315
            Left            =   2700
            TabIndex        =   105
            Top             =   240
            Width           =   6675
         End
         Begin VB.TextBox txtKeyframeStore 
            Height          =   315
            Left            =   2700
            TabIndex        =   100
            Top             =   600
            Width           =   6675
         End
         Begin VB.Label lblCaption 
            Caption         =   "BBC Motion Gallery Proxy Store"
            Height          =   255
            Index           =   19
            Left            =   120
            TabIndex        =   347
            Top             =   3900
            Width           =   2415
         End
         Begin VB.Label lblCaption 
            Caption         =   "Media Screen Grab Store"
            Height          =   255
            Index           =   26
            Left            =   120
            TabIndex        =   343
            Top             =   1020
            Width           =   2235
         End
         Begin VB.Label lblCaption 
            Caption         =   "BBC Motion Gallery Restores"
            Height          =   255
            Index           =   88
            Left            =   120
            TabIndex        =   340
            Top             =   4260
            Width           =   2595
         End
         Begin VB.Label lblCaption 
            Caption         =   "BBC Motion Gallery Proxy Archive"
            Height          =   255
            Index           =   87
            Left            =   120
            TabIndex        =   338
            Top             =   3540
            Width           =   2415
         End
         Begin VB.Label lblCaption 
            Caption         =   "BBC Motion Gallery Clip Archive"
            Height          =   255
            Index           =   86
            Left            =   120
            TabIndex        =   337
            Top             =   3180
            Width           =   2415
         End
         Begin VB.Label lblCaption 
            Caption         =   "Signiant System Barcode"
            Height          =   255
            Index           =   81
            Left            =   120
            TabIndex        =   333
            Top             =   4980
            Width           =   2415
         End
         Begin VB.Label lblCaption 
            Caption         =   "BBC Motion Gallery Aspera Store"
            Height          =   255
            Index           =   80
            Left            =   120
            TabIndex        =   331
            Top             =   2820
            Width           =   2415
         End
         Begin VB.Label lblCaption 
            Caption         =   "Transcode Overlay Folder"
            Height          =   255
            Index           =   79
            Left            =   120
            TabIndex        =   328
            Top             =   4620
            Width           =   2415
         End
         Begin VB.Label lblCaption 
            Caption         =   "BBC Motion Gallery Archive Store"
            Height          =   255
            Index           =   49
            Left            =   120
            TabIndex        =   326
            Top             =   2460
            Width           =   2415
         End
         Begin VB.Label lblCaption 
            Caption         =   "Flashstore Path"
            Height          =   255
            Index           =   50
            Left            =   120
            TabIndex        =   256
            Top             =   1380
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "Aspera Server Name"
            Height          =   255
            Index           =   48
            Left            =   120
            TabIndex        =   253
            Top             =   2100
            Width           =   2415
         End
         Begin VB.Label lblCaption 
            Caption         =   "Aspera Store "
            Height          =   255
            Index           =   38
            Left            =   120
            TabIndex        =   251
            Top             =   1740
            Width           =   2415
         End
         Begin VB.Label lblCaption 
            Caption         =   "Flash Thumbnail Store"
            Height          =   255
            Index           =   32
            Left            =   120
            TabIndex        =   106
            Top             =   300
            Width           =   2175
         End
         Begin VB.Label lblCaption 
            Caption         =   "Media Keyframe Store"
            Height          =   255
            Index           =   27
            Left            =   120
            TabIndex        =   101
            Top             =   660
            Width           =   2235
         End
      End
      Begin VB.Frame fraPassword 
         Caption         =   "Password Security"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2295
         Left            =   -65280
         TabIndex        =   80
         Top             =   540
         Width           =   4455
         Begin VB.TextBox txtPasswordChangeDays 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   2100
            TabIndex        =   87
            ToolTipText     =   "The caption for DSN cetasql"
            Top             =   1440
            Width           =   495
         End
         Begin VB.TextBox txtPasswordUppercase 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   2100
            TabIndex        =   85
            ToolTipText     =   "The caption for DSN cetasql"
            Top             =   1080
            Width           =   495
         End
         Begin VB.TextBox txtPasswordNumbers 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   2100
            TabIndex        =   83
            ToolTipText     =   "The caption for DSN cetasql"
            Top             =   720
            Width           =   495
         End
         Begin VB.TextBox txtPasswordLength 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   2100
            TabIndex        =   81
            ToolTipText     =   "The caption for DSN cetasql"
            Top             =   360
            Width           =   495
         End
         Begin VB.Label lblCaption 
            Caption         =   "Leave blank to specify not required"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   25
            Left            =   540
            TabIndex        =   91
            Top             =   1800
            Width           =   3075
         End
         Begin VB.Label lblCaption 
            Caption         =   "days"
            Height          =   255
            Index           =   24
            Left            =   2760
            TabIndex        =   89
            Top             =   1440
            Width           =   435
         End
         Begin VB.Label lblCaption 
            Caption         =   "Force change every"
            Height          =   255
            Index           =   23
            Left            =   360
            TabIndex        =   88
            Top             =   1440
            Width           =   1635
         End
         Begin VB.Label lblCaption 
            Caption         =   "Minimum Uppercase"
            Height          =   255
            Index           =   22
            Left            =   360
            TabIndex        =   86
            Top             =   1080
            Width           =   1635
         End
         Begin VB.Label lblCaption 
            Caption         =   "Minimum Numbers"
            Height          =   255
            Index           =   21
            Left            =   360
            TabIndex        =   84
            Top             =   720
            Width           =   1635
         End
         Begin VB.Label lblCaption 
            Caption         =   "Minimum Length"
            Height          =   255
            Index           =   20
            Left            =   360
            TabIndex        =   82
            Top             =   360
            Width           =   1635
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Purchase Orders"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2655
         Left            =   -74880
         TabIndex        =   70
         Top             =   540
         Width           =   5475
         Begin VB.TextBox txtAdvancedPurchaseApproveEmail 
            Height          =   315
            Left            =   2520
            TabIndex        =   77
            Top             =   2220
            Width           =   2775
         End
         Begin VB.CheckBox chkRequireBudgetCodesBeforeAuthorisation 
            Alignment       =   1  'Right Justify
            Caption         =   "Require budget codes before authorisation"
            Height          =   255
            Left            =   180
            TabIndex        =   76
            Top             =   1860
            Width           =   5115
         End
         Begin VB.CheckBox chkShowTimeColumnInPurchaseOrder 
            Alignment       =   1  'Right Justify
            Caption         =   "Show Time Column In Purchase Order Detail Grid"
            Height          =   255
            Left            =   180
            TabIndex        =   75
            Top             =   1560
            Width           =   5115
         End
         Begin VB.CheckBox chkCreateDNotesForExternalDeliveryPurchaseOrders 
            Alignment       =   1  'Right Justify
            Caption         =   "Create Delivery Notes For External Delivery Purchase Orders"
            Height          =   255
            Left            =   180
            TabIndex        =   74
            Top             =   1260
            Width           =   5115
         End
         Begin VB.CheckBox chkAllowNewPurchaseOrdersOnCostedJobs 
            Alignment       =   1  'Right Justify
            Caption         =   "Allow Purchase Orders To Be Added To Costed Jobs"
            Height          =   195
            Left            =   180
            TabIndex        =   73
            Top             =   960
            Width           =   5115
         End
         Begin VB.CheckBox chkAllowEditAuthorisedPurchaseOrders 
            Alignment       =   1  'Right Justify
            Caption         =   "Allow Editing of Authorised Purchase Orders"
            Height          =   195
            Left            =   180
            TabIndex        =   72
            ToolTipText     =   "User departmental P/O authorisation numbers"
            Top             =   660
            Width           =   5115
         End
         Begin VB.CheckBox chkUseDepartmentalAuthorisationNumbers 
            Alignment       =   1  'Right Justify
            Caption         =   "Departmental Purchase Order Authorisation Numbers"
            Height          =   195
            Left            =   180
            TabIndex        =   71
            ToolTipText     =   "User departmental P/O authorisation numbers"
            Top             =   360
            Width           =   5115
         End
         Begin VB.Label Label1 
            Caption         =   "Advanced PO auth email"
            Height          =   255
            Index           =   20
            Left            =   180
            TabIndex        =   78
            Top             =   2220
            Width           =   2115
         End
      End
      Begin VB.CheckBox chkRequireVideoStandardOnQuote 
         Caption         =   "Require Video Standard on Quotes"
         Height          =   255
         Left            =   -74880
         TabIndex        =   69
         Top             =   3360
         Width           =   3555
      End
      Begin VB.Frame fraConnections 
         Caption         =   "Database Connection Captions"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2295
         Left            =   -69900
         TabIndex        =   60
         Top             =   540
         Width           =   4455
         Begin VB.TextBox txtConnection1 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   1770
            TabIndex        =   64
            ToolTipText     =   "The caption for DSN cetasql"
            Top             =   300
            Width           =   2535
         End
         Begin VB.TextBox txtConnection2 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   1770
            TabIndex        =   63
            ToolTipText     =   "The caption for DSN cetasql2"
            Top             =   660
            Width           =   2535
         End
         Begin VB.TextBox txtConnection3 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   1770
            TabIndex        =   62
            ToolTipText     =   "The caption for DSN cetasql3"
            Top             =   1020
            Width           =   2535
         End
         Begin VB.TextBox txtConnection4 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   1770
            TabIndex        =   61
            ToolTipText     =   "The caption for DSN cetasql4"
            Top             =   1380
            Width           =   2535
         End
         Begin VB.Label Label1 
            Caption         =   "If you do not want to offer any more connections than the basic cetasql DSN, leave these blank."
            Height          =   495
            Index           =   4
            Left            =   120
            TabIndex        =   90
            Top             =   1740
            Width           =   4155
         End
         Begin VB.Label lblCaption 
            Caption         =   "Connection 1 Name"
            Height          =   255
            Index           =   8
            Left            =   180
            TabIndex        =   68
            Top             =   300
            Width           =   2415
         End
         Begin VB.Label lblCaption 
            Caption         =   "Connection 2 Name"
            Height          =   255
            Index           =   9
            Left            =   180
            TabIndex        =   67
            Top             =   660
            Width           =   2415
         End
         Begin VB.Label lblCaption 
            Caption         =   "Connection 3 Name"
            Height          =   255
            Index           =   10
            Left            =   180
            TabIndex        =   66
            Top             =   1020
            Width           =   2415
         End
         Begin VB.Label lblCaption 
            Caption         =   "Connection 4 Name"
            Height          =   255
            Index           =   11
            Left            =   180
            TabIndex        =   65
            Top             =   1380
            Width           =   2415
         End
      End
      Begin VB.Frame Frame11 
         Caption         =   "Job List"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Left            =   -69300
         TabIndex        =   52
         Top             =   2220
         Width           =   5475
         Begin VB.CheckBox chkAutoTickOurContactInJobLists 
            Alignment       =   1  'Right Justify
            Caption         =   "Show 'Our Contact' rather than 'Allocated To'"
            Height          =   255
            Left            =   240
            TabIndex        =   53
            Top             =   300
            Width           =   5055
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Management Reports"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Left            =   -69300
         TabIndex        =   50
         Top             =   1380
         Width           =   5475
         Begin VB.CheckBox chkHideZeroRateCardItemsInManagementReports 
            Alignment       =   1  'Right Justify
            Caption         =   "Dont report on items with no rate card value"
            Height          =   255
            Left            =   180
            TabIndex        =   51
            Top             =   300
            Width           =   5115
         End
      End
      Begin VB.Frame fraExport 
         Caption         =   "Export To File"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Left            =   -74880
         TabIndex        =   29
         Top             =   4800
         Width           =   8355
         Begin VB.TextBox txtAccountsExportLocation 
            Height          =   315
            Left            =   2640
            TabIndex        =   30
            Top             =   240
            Width           =   5535
         End
         Begin VB.Label lblCaption 
            Caption         =   "Accounts Export Location"
            Height          =   255
            Index           =   12
            Left            =   180
            TabIndex        =   31
            Top             =   300
            Width           =   2415
         End
      End
      Begin VB.Frame fraSundry 
         Caption         =   "Sundry Costs"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Left            =   -74880
         TabIndex        =   25
         Top             =   4080
         Width           =   5475
         Begin VB.TextBox txtSundryMarkup 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   4260
            MaxLength       =   2
            TabIndex        =   26
            Top             =   240
            Width           =   795
         End
         Begin VB.Label Label3 
            Caption         =   "Sundry cost percentage markup"
            Height          =   315
            Index           =   0
            Left            =   180
            TabIndex        =   28
            Top             =   300
            Width           =   3315
         End
         Begin VB.Label Label3 
            Caption         =   "%"
            Height          =   255
            Index           =   1
            Left            =   5160
            TabIndex        =   27
            Top             =   300
            Width           =   195
         End
      End
      Begin VB.Frame Frame10 
         Caption         =   "USE CAUTION WHEN SETTING THESE OPTIONS!"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   9255
         Left            =   -74880
         TabIndex        =   22
         Top             =   540
         Width           =   4815
         Begin VB.CheckBox chkMediaInfoLock 
            Alignment       =   1  'Right Justify
            Caption         =   "CETA MediaInfo Locked"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   180
            TabIndex        =   367
            ToolTipText     =   "Log everyone out of the system"
            Top             =   3960
            Width           =   4395
         End
         Begin VB.CheckBox chkDeliveryMonitoringLock 
            Alignment       =   1  'Right Justify
            Caption         =   "Delivery Monitoring"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   180
            TabIndex        =   366
            ToolTipText     =   "Log everyone out of the system"
            Top             =   3600
            Width           =   4395
         End
         Begin VB.CheckBox chkFFMPGExplicitLocked 
            Alignment       =   1  'Right Justify
            Caption         =   "FFMPG Explicit Transcodes Locked"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   180
            TabIndex        =   365
            ToolTipText     =   "Log everyone out of the system"
            Top             =   1440
            Width           =   4395
         End
         Begin VB.CheckBox chkChecksumHandlersLock 
            Alignment       =   1  'Right Justify
            Caption         =   "Checksum Handlers"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   180
            TabIndex        =   356
            ToolTipText     =   "Log everyone out of the system"
            Top             =   3240
            Width           =   4395
         End
         Begin VB.CheckBox chkDADCXMLLock 
            Alignment       =   1  'Right Justify
            Caption         =   "DADC XML And Send Locked"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   180
            TabIndex        =   355
            ToolTipText     =   "Log everyone out of the system"
            Top             =   2880
            Width           =   4395
         End
         Begin VB.CheckBox chkStopFFMPGServices 
            Alignment       =   1  'Right Justify
            Caption         =   "Stop FFMPG and XML Services of Locked Systems"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   180
            TabIndex        =   354
            ToolTipText     =   "Log everyone out of the system"
            Top             =   2520
            Width           =   4395
         End
         Begin VB.CheckBox chkFFMPGSpeedChangeLocked 
            Alignment       =   1  'Right Justify
            Caption         =   "FFMPG Speed Change Locked "
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   180
            TabIndex        =   353
            ToolTipText     =   "Log everyone out of the system"
            Top             =   360
            Width           =   4395
         End
         Begin VB.CheckBox chkFFMPGAudioLocked 
            Alignment       =   1  'Right Justify
            Caption         =   "FFMPG Audio Conform Loicked"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   180
            TabIndex        =   352
            ToolTipText     =   "Log everyone out of the system"
            Top             =   720
            Width           =   4395
         End
         Begin VB.CheckBox chkFFMPGGenericLocked 
            Alignment       =   1  'Right Justify
            Caption         =   "FFMPG Generic Transcodes Locked"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   180
            TabIndex        =   351
            ToolTipText     =   "Log everyone out of the system"
            Top             =   1080
            Width           =   4395
         End
         Begin VB.CheckBox chkFFMPGThumbsLocked 
            Alignment       =   1  'Right Justify
            Caption         =   "FFMPG Thumbs Transcodes Locked"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   180
            TabIndex        =   350
            ToolTipText     =   "Log everyone out of the system"
            Top             =   1800
            Width           =   4395
         End
         Begin VB.CheckBox chkVantageAPILocked 
            Alignment       =   1  'Right Justify
            Caption         =   "Vantage API Handler Locked"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   180
            TabIndex        =   349
            ToolTipText     =   "Log everyone out of the system"
            Top             =   2160
            Width           =   4395
         End
         Begin VB.CheckBox chkFileHandlersLock 
            Alignment       =   1  'Right Justify
            Caption         =   "CETA File Handlers Locked"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   180
            TabIndex        =   348
            ToolTipText     =   "Log everyone out of the system"
            Top             =   4680
            Width           =   4395
         End
         Begin VB.CheckBox chkVantageOnline 
            Alignment       =   1  'Right Justify
            Caption         =   "Vantage Transcoding Online"
            Height          =   255
            Left            =   180
            TabIndex        =   341
            Top             =   8640
            Width           =   4395
         End
         Begin VB.CheckBox chkSystemInvoiceLock 
            Alignment       =   1  'Right Justify
            Caption         =   "CETA Invoicing Locked"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   180
            TabIndex        =   334
            ToolTipText     =   "Log everyone out of the system"
            Top             =   5760
            Width           =   4395
         End
         Begin VB.CheckBox chkAsperaOnline 
            Alignment       =   1  'Right Justify
            Caption         =   "Aspera System Online"
            Height          =   255
            Left            =   180
            TabIndex        =   324
            Top             =   8280
            Width           =   4395
         End
         Begin VB.CheckBox chkSystemCETALock 
            Alignment       =   1  'Right Justify
            Caption         =   "CETA Facilities Manager Locked"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   180
            TabIndex        =   293
            ToolTipText     =   "Log everyone out of the system"
            Top             =   5400
            Width           =   4395
         End
         Begin VB.CheckBox chkSystemFileManagerLock 
            Alignment       =   1  'Right Justify
            Caption         =   "CETA File Manager Locked"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   180
            TabIndex        =   292
            ToolTipText     =   "Log everyone out of the system"
            Top             =   5040
            Width           =   4395
         End
         Begin VB.CheckBox chkSystemWebLock 
            Alignment       =   1  'Right Justify
            Caption         =   "Content Delivery Web System Locked"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   180
            TabIndex        =   291
            ToolTipText     =   "Log everyone out of the system"
            Top             =   4320
            Visible         =   0   'False
            Width           =   4395
         End
         Begin VB.CheckBox chkCloseSystemDown 
            Alignment       =   1  'Right Justify
            Caption         =   "Close the system down (logs everyone out)"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   180
            TabIndex        =   23
            ToolTipText     =   "Log everyone out of the system"
            Top             =   6120
            Width           =   4395
         End
      End
      Begin VB.Frame Frame9 
         Caption         =   "Web Server Default URL's"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1515
         Left            =   120
         TabIndex        =   17
         Top             =   540
         Width           =   9555
         Begin VB.TextBox txtWebInfo 
            Height          =   315
            Index           =   2
            Left            =   2700
            TabIndex        =   36
            Top             =   1020
            Width           =   6675
         End
         Begin VB.TextBox txtWebInfo 
            Height          =   315
            Index           =   1
            Left            =   2700
            TabIndex        =   19
            Top             =   660
            Width           =   6675
         End
         Begin VB.TextBox txtWebInfo 
            Height          =   315
            Index           =   0
            Left            =   2700
            TabIndex        =   18
            Top             =   300
            Width           =   6675
         End
         Begin VB.Label lblCaption 
            Caption         =   "Clip Store Play Path"
            Height          =   255
            Index           =   13
            Left            =   120
            TabIndex        =   37
            Top             =   1080
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "Resource information page name"
            Height          =   255
            Index           =   4
            Left            =   120
            TabIndex        =   21
            Top             =   720
            Width           =   3375
         End
         Begin VB.Label lblCaption 
            Caption         =   "Root webserver address"
            Height          =   255
            Index           =   5
            Left            =   120
            TabIndex        =   20
            Top             =   360
            Width           =   3375
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Company"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Left            =   -69300
         TabIndex        =   14
         Top             =   540
         Width           =   5475
         Begin VB.CheckBox chkShowCompanyInsuranceOutDate 
            Alignment       =   1  'Right Justify
            Caption         =   "Show ""Insurance Out"" in company details"
            Height          =   195
            Left            =   180
            TabIndex        =   15
            ToolTipText     =   "Show 'insurance out' in company details"
            Top             =   300
            Width           =   5115
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Job Defaults"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4575
         Left            =   -74880
         TabIndex        =   11
         Top             =   540
         Width           =   5475
         Begin VB.CheckBox chkUserIsNotDefaultOurContact 
            Alignment       =   1  'Right Justify
            Caption         =   "Use logged in user as default 'Our Contact'"
            Height          =   255
            Left            =   180
            TabIndex        =   79
            Top             =   2220
            Width           =   5115
         End
         Begin VB.ComboBox cmbWhenToAllocate 
            Height          =   315
            Left            =   3000
            TabIndex        =   48
            Text            =   "cmbWhenToAllocate"
            ToolTipText     =   "Select point to allocate project number"
            Top             =   1830
            Width           =   2295
         End
         Begin VB.CheckBox chkNeedAccountNumberToConfirm 
            Alignment       =   1  'Right Justify
            Caption         =   "Require company A/C number to confirm job"
            Height          =   255
            Left            =   180
            TabIndex        =   47
            Top             =   1500
            Width           =   5115
         End
         Begin VB.Frame Frame1 
            Caption         =   "Global Defaults"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1875
            Left            =   180
            TabIndex        =   38
            Top             =   2580
            Width           =   5115
            Begin VB.ComboBox cmbDefaultTabInJobDetails 
               Height          =   315
               Left            =   2520
               TabIndex        =   42
               ToolTipText     =   "Select default tab in job details"
               Top             =   300
               Width           =   2535
            End
            Begin VB.ComboBox cmbJobAllocation 
               Height          =   315
               Left            =   2520
               TabIndex        =   41
               ToolTipText     =   "Select default job allocation"
               Top             =   1020
               Width           =   2535
            End
            Begin VB.ComboBox cmbJobType 
               Height          =   315
               Left            =   2520
               TabIndex        =   40
               ToolTipText     =   "Select default job type"
               Top             =   660
               Width           =   2535
            End
            Begin VB.TextBox txtDefaultCategoryToAddResource 
               Height          =   315
               Left            =   2520
               TabIndex        =   39
               Top             =   1380
               Width           =   2475
            End
            Begin VB.Label Label1 
               Caption         =   "Show this tab when opening"
               Height          =   255
               Index           =   5
               Left            =   180
               TabIndex        =   46
               Top             =   300
               Width           =   2295
            End
            Begin VB.Label Label1 
               Caption         =   "Project Type"
               Height          =   255
               Index           =   2
               Left            =   180
               TabIndex        =   45
               Top             =   1080
               Width           =   2475
            End
            Begin VB.Label Label1 
               Caption         =   "Job Type"
               Height          =   255
               Index           =   1
               Left            =   180
               TabIndex        =   44
               Top             =   660
               Width           =   2535
            End
            Begin VB.Label Label1 
               Caption         =   "Add Resource Category"
               Height          =   255
               Index           =   6
               Left            =   180
               TabIndex        =   43
               Top             =   1440
               Width           =   1755
            End
         End
         Begin VB.CheckBox chkRequireDeliveryMethodsOnHireAndEditJobs 
            Alignment       =   1  'Right Justify
            Caption         =   "Force user to enter delivery / collection methods"
            Height          =   195
            Left            =   180
            TabIndex        =   34
            ToolTipText     =   "Product required before saving"
            Top             =   1200
            Width           =   5115
         End
         Begin VB.CheckBox chkRequireOurContactBeforeSaving 
            Alignment       =   1  'Right Justify
            Caption         =   "Force user to enter our contact"
            Height          =   195
            Left            =   180
            TabIndex        =   32
            ToolTipText     =   "Product required before saving"
            Top             =   900
            Width           =   5115
         End
         Begin VB.CheckBox chkAppendInitialsAndDateToNotes 
            Alignment       =   1  'Right Justify
            Caption         =   "Append user initials and date stamp to notes"
            Height          =   195
            Left            =   180
            TabIndex        =   13
            ToolTipText     =   "Attach user initials and date to notes"
            Top             =   300
            Width           =   5115
         End
         Begin VB.CheckBox chkRequireProductBeforeSaving 
            Alignment       =   1  'Right Justify
            Caption         =   "Force user to enter a product name"
            Height          =   195
            Left            =   180
            TabIndex        =   12
            ToolTipText     =   "Product required before saving"
            Top             =   600
            Width           =   5115
         End
         Begin VB.Label Label2 
            Caption         =   "Allocate project number to job"
            Height          =   315
            Left            =   210
            TabIndex        =   49
            Top             =   1860
            Width           =   2475
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Duplication and Dubbing"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3315
         Left            =   -74880
         TabIndex        =   6
         Top             =   5280
         Width           =   11055
         Begin VB.CheckBox chkUseAutoDeleteWhenFinalisingJobs 
            Alignment       =   1  'Right Justify
            Caption         =   "Use AutoDelete When Finalising Jobs"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   5760
            TabIndex        =   344
            ToolTipText     =   "Product required before saving"
            Top             =   2700
            Width           =   5115
         End
         Begin VB.CheckBox chkPrintCopyJobSheets 
            Alignment       =   1  'Right Justify
            Caption         =   "Print 'Copy' Job Sheets when normal Job Sheet is printed"
            Height          =   195
            Left            =   5760
            TabIndex        =   254
            ToolTipText     =   "Product required before saving"
            Top             =   2400
            Width           =   5115
         End
         Begin VB.CheckBox chkHideVideoStandardOnJobForm 
            Alignment       =   1  'Right Justify
            Caption         =   "Hide Video Standard Drop Down On Job Form"
            Height          =   195
            Left            =   5760
            TabIndex        =   107
            ToolTipText     =   "Product required before saving"
            Top             =   2100
            Width           =   5115
         End
         Begin VB.CheckBox chkUseSingleCompletionScreenForDubbing 
            Alignment       =   1  'Right Justify
            Caption         =   "Use Single Completion Screen For Dubbing"
            Height          =   195
            Left            =   180
            TabIndex        =   99
            ToolTipText     =   "Product required before saving"
            Top             =   2520
            Width           =   5115
         End
         Begin VB.CheckBox chkOrderByNumbers 
            Alignment       =   1  'Right Justify
            Caption         =   "Show Lists in Number order rather than Date"
            Height          =   195
            Left            =   5760
            TabIndex        =   97
            ToolTipText     =   "Job Listing and Despatch Listing"
            Top             =   1800
            Width           =   5115
         End
         Begin VB.CheckBox chkDeadlineTimeNotCompulsory 
            Alignment       =   1  'Right Justify
            Caption         =   "Deadline time is not compulsory on dubbing jobs"
            Height          =   195
            Left            =   5760
            TabIndex        =   96
            ToolTipText     =   "On dubbing screen, Deadline date and time are usually compulsory"
            Top             =   1500
            Width           =   5115
         End
         Begin VB.CheckBox chkBookDubsAsConfirmedStatus 
            Alignment       =   1  'Right Justify
            Caption         =   "Book Dubs as 'Confirmed' status"
            Height          =   195
            Left            =   5760
            TabIndex        =   59
            ToolTipText     =   "Request prompt fro detailed completion details"
            Top             =   1200
            Width           =   5115
         End
         Begin VB.CheckBox chkRequireCompletedDubsBeforeCosting 
            Alignment       =   1  'Right Justify
            Caption         =   "Require all dubbing requests to be completed before costing"
            Height          =   195
            Left            =   5760
            TabIndex        =   58
            ToolTipText     =   "Request prompt fro detailed completion details"
            Top             =   900
            Width           =   5115
         End
         Begin VB.CheckBox chkHideSoundChannelsOnVTDetailGrid 
            Alignment       =   1  'Right Justify
            Caption         =   "Hide Sound Channel Columns On VT Detail Grid"
            Height          =   255
            Left            =   180
            TabIndex        =   57
            Top             =   1800
            Width           =   5115
         End
         Begin VB.TextBox txtClipIDCaption 
            Height          =   285
            Left            =   4080
            TabIndex        =   55
            Top             =   2100
            Width           =   1215
         End
         Begin VB.CheckBox chkUseClipID 
            Alignment       =   1  'Right Justify
            Caption         =   "Show and generate ""Clip ID"" values"
            Height          =   195
            Left            =   180
            TabIndex        =   54
            ToolTipText     =   "Request prompt fro detailed completion details"
            Top             =   2160
            Width           =   2955
         End
         Begin VB.CheckBox chkUpperCaseDubbingDetails 
            Alignment       =   1  'Right Justify
            Caption         =   "Upper case dubbing detail lines"
            Height          =   195
            Left            =   180
            TabIndex        =   35
            ToolTipText     =   "Request prompt fro detailed completion details"
            Top             =   1500
            Width           =   5115
         End
         Begin VB.CheckBox chkHarshCheckingOnDubbingEntry 
            Alignment       =   1  'Right Justify
            Caption         =   "Force completion of Aspect and Quantity in dubbing requests"
            Height          =   195
            Left            =   180
            TabIndex        =   33
            ToolTipText     =   "Request prompt fro detailed completion details"
            Top             =   1200
            Width           =   5115
         End
         Begin VB.CheckBox chkCompleteJobsWhenVTCompleted 
            Alignment       =   1  'Right Justify
            Caption         =   "Complete whole job on VT complete"
            Height          =   195
            Left            =   5760
            TabIndex        =   24
            ToolTipText     =   "Request prompt fro detailed completion details"
            Top             =   300
            Width           =   5115
         End
         Begin VB.CheckBox chkPromptForDetailedCompletion 
            Alignment       =   1  'Right Justify
            Caption         =   "Prompt for detailed completion details"
            Height          =   195
            Left            =   5760
            TabIndex        =   10
            ToolTipText     =   "Request prompt fro detailed completion details"
            Top             =   600
            Width           =   5115
         End
         Begin VB.CheckBox chkUseTitleInMaster 
            Alignment       =   1  'Right Justify
            Caption         =   "Use Job Title in Dubbing Master Lines"
            Height          =   195
            Left            =   180
            TabIndex        =   9
            ToolTipText     =   "Use job title in dubbing master lones"
            Top             =   300
            Width           =   5115
         End
         Begin VB.CheckBox chkUseSpecialDubbingComboOptions 
            Alignment       =   1  'Right Justify
            Caption         =   "Use Special Dubbing drop down combos"
            Height          =   195
            Left            =   180
            TabIndex        =   8
            ToolTipText     =   "Use special dubbing drop down combos"
            Top             =   900
            Width           =   5115
         End
         Begin VB.CheckBox chkUseTitleInCopy 
            Alignment       =   1  'Right Justify
            Caption         =   "Use Job Title in Dubbing Copy Lines"
            Height          =   195
            Left            =   180
            TabIndex        =   7
            ToolTipText     =   "Use job title in dubbing copy lines"
            Top             =   600
            Width           =   5115
         End
         Begin VB.Label Label1 
            Caption         =   "Caption:"
            Height          =   195
            Index           =   8
            Left            =   3240
            TabIndex        =   56
            Top             =   2160
            Width           =   1095
         End
      End
      Begin MSAdodcLib.Adodc adoAllocation 
         Height          =   330
         Left            =   -70380
         Top             =   4980
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoAllocation"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoSequence 
         Height          =   330
         Left            =   -73320
         Top             =   2040
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoSequence"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdSequence 
         Bindings        =   "frmOptions.frx":0A2F
         Height          =   3255
         Left            =   -74880
         TabIndex        =   4
         ToolTipText     =   "Edit the Sequence Numbers"
         Top             =   540
         Width           =   12855
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         BackColorOdd    =   12640511
         RowHeight       =   423
         Columns(0).Width=   3200
         _ExtentX        =   22675
         _ExtentY        =   5741
         _StockProps     =   79
         Caption         =   "General Sequences"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdAllocation 
         Bindings        =   "frmOptions.frx":0A49
         Height          =   4695
         Left            =   -74880
         TabIndex        =   5
         ToolTipText     =   "Edit the Sequence Numbers"
         Top             =   3960
         Width           =   12855
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         BackColorOdd    =   12640511
         RowHeight       =   423
         Columns(0).Width=   3200
         _ExtentX        =   22675
         _ExtentY        =   8281
         _StockProps     =   79
         Caption         =   "Project Allocation"
      End
      Begin MSAdodcLib.Adodc adoDepartments 
         Height          =   330
         Left            =   -73320
         Top             =   1740
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoDepartments"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDepartments 
         Bindings        =   "frmOptions.frx":0A65
         Height          =   7035
         Left            =   -74880
         TabIndex        =   16
         ToolTipText     =   "Edit the Sequence Numbers"
         Top             =   540
         Width           =   11055
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         BackColorOdd    =   12640511
         RowHeight       =   423
         Columns(0).Width=   3200
         _ExtentX        =   19500
         _ExtentY        =   12409
         _StockProps     =   79
         Caption         =   "Departments"
      End
      Begin VB.Frame Frame15 
         BorderStyle     =   0  'None
         Caption         =   "Frame15"
         Height          =   2415
         Left            =   -74880
         TabIndex        =   117
         Top             =   6240
         Width           =   8715
         Begin VB.TextBox txtSprocketsPercentage 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   8040
            TabIndex        =   268
            Top             =   2160
            Width           =   435
         End
         Begin VB.CheckBox chkDontPromptForInvoiceAddress 
            Alignment       =   1  'Right Justify
            Caption         =   "Dont Prompt For Invoice Address"
            Height          =   195
            Left            =   0
            TabIndex        =   132
            Top             =   660
            Width           =   5115
         End
         Begin VB.CheckBox chkSendToAccountsOnFinalise 
            Alignment       =   1  'Right Justify
            Caption         =   "Send to accounts on costings finalise"
            Height          =   195
            Left            =   0
            TabIndex        =   131
            Top             =   960
            Width           =   5115
         End
         Begin VB.CheckBox chkHideTimeColumnInCostings 
            Alignment       =   1  'Right Justify
            Caption         =   "Always hide the time column in costings"
            Height          =   195
            Left            =   0
            TabIndex        =   130
            Top             =   1260
            Width           =   5115
         End
         Begin VB.CheckBox chkDontPromptForNumberOfInvoiceCopies 
            Alignment       =   1  'Right Justify
            Caption         =   "Dont Prompt For Number Of Invoice Copies (Always print default)"
            Height          =   195
            Left            =   0
            TabIndex        =   129
            Top             =   360
            Width           =   5115
         End
         Begin VB.TextBox txtDefaultCopiesToPrintOfInvoice 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   4680
            TabIndex        =   128
            Top             =   0
            Width           =   435
         End
         Begin VB.CheckBox chkHideOrderByColumnOnCostingGrid 
            Alignment       =   1  'Right Justify
            Caption         =   "Hide ""Order By"" field in costing grid"
            Height          =   195
            Left            =   0
            TabIndex        =   127
            Top             =   2220
            Width           =   5115
         End
         Begin VB.CheckBox chkDontForceUnitsChargedOnHireJobs 
            Alignment       =   1  'Right Justify
            Caption         =   "Dont force units charged on hire jobs when costing"
            Height          =   195
            Left            =   0
            TabIndex        =   126
            Top             =   1560
            Width           =   5115
         End
         Begin VB.CheckBox chkAllowCostedJobsWhenNotComplete 
            Alignment       =   1  'Right Justify
            Caption         =   "Allow jobs to change to costed, even if they are not 'Complete'"
            Height          =   255
            Left            =   0
            TabIndex        =   125
            Top             =   1860
            Width           =   5115
         End
         Begin VB.CheckBox chkUseProductRateCards 
            Alignment       =   1  'Right Justify
            Caption         =   "Use Product Rate Cards"
            Height          =   255
            Left            =   5340
            TabIndex        =   124
            Top             =   360
            Width           =   3135
         End
         Begin VB.CheckBox chkUseJCAPostingToAccounts 
            Alignment       =   1  'Right Justify
            Caption         =   "Use JCA Send To Accounts Form"
            Height          =   255
            Left            =   5340
            TabIndex        =   123
            Top             =   660
            Width           =   3135
         End
         Begin VB.CheckBox chkDontMakeMachineTimeADecimal 
            Alignment       =   1  'Right Justify
            Caption         =   "Keep machine time in minutes on dubs"
            Height          =   255
            Left            =   5340
            TabIndex        =   122
            Top             =   960
            Width           =   3135
         End
         Begin VB.CheckBox chkUseMinimumCharging 
            Alignment       =   1  'Right Justify
            Caption         =   "Use Automatic Minimum Charging"
            Height          =   255
            Left            =   5340
            TabIndex        =   121
            Top             =   1260
            Width           =   3135
         End
         Begin VB.CheckBox chkDisableCostingPopUp 
            Alignment       =   1  'Right Justify
            Caption         =   "Disable Costing Pop-Up (dbl-click)"
            Height          =   255
            Left            =   5340
            TabIndex        =   120
            Top             =   1560
            Width           =   3135
         End
         Begin VB.CheckBox chkAllowCostingEntryAtAnyStatus 
            Alignment       =   1  'Right Justify
            Caption         =   "Allow Costing Entry For All Status"
            Height          =   255
            Left            =   5340
            TabIndex        =   119
            Top             =   1860
            Width           =   3135
         End
         Begin VB.CheckBox chkAbortIfAZeroDuration 
            Alignment       =   1  'Right Justify
            Caption         =   "Abort Costing if Zero Duration Entries"
            Height          =   255
            Left            =   5340
            TabIndex        =   118
            Top             =   60
            Width           =   3135
         End
         Begin VB.Label Label1 
            Caption         =   "'Sprockets' Percentage due"
            Height          =   255
            Index           =   27
            Left            =   5370
            TabIndex        =   267
            Top             =   2220
            Width           =   3435
         End
         Begin VB.Label Label1 
            Caption         =   "Default copies to print of invoice"
            Height          =   255
            Index           =   7
            Left            =   30
            TabIndex        =   133
            Top             =   60
            Width           =   3435
         End
      End
      Begin VB.Label Label3 
         Caption         =   "Maximum discount for confirming quote"
         Height          =   255
         Index           =   3
         Left            =   -74880
         TabIndex        =   95
         Top             =   4380
         Width           =   3315
      End
      Begin VB.Label Label3 
         Caption         =   "%"
         Height          =   255
         Index           =   4
         Left            =   -69960
         TabIndex        =   94
         Top             =   4380
         Width           =   195
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   315
      Left            =   16920
      TabIndex        =   2
      ToolTipText     =   "Cancel changes made to these settings"
      Top             =   10320
      Width           =   1155
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   315
      Left            =   15600
      TabIndex        =   1
      ToolTipText     =   "Save changes to settings and close this form"
      Top             =   10320
      Width           =   1155
   End
   Begin VB.Label lblDescription 
      Caption         =   "Changes made to these settings may not take effect until a user re-starts their CETA system."
      ForeColor       =   &H00000000&
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   3
      Tag             =   "App Description"
      Top             =   10380
      Width           =   7215
   End
End
Attribute VB_Name = "frmSettings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAddLocationField_Click(Index As Integer)

On Error GoTo cmdAddLocationField_Error

Dim l_strSQL As String

Select Case Index
Case 0

    l_strSQL = "ALTER TABLE quote ADD companyname CHAR(100);ALTER TABLE quote ADD contactname CHAR(100)"

End Select


ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

Exit Sub
cmdAddLocationField_Error:
    MsgBox Err.Description

Exit Sub


End Sub

Private Sub cmdCancel_Click()

Unload Me

End Sub


Private Sub cmdOK_Click()

If Not IsNumeric(txtSundryMarkup.Text) Then
    MsgBox "Please enter a valid Sundry Cost mark-up percentage", vbExclamation
    Exit Sub
End If

Me.Hide
SaveSettingsToDatabase

Dim l_intLoop As Integer
Dim l_strSQL  As String

l_strSQL = "UPDATE setup SET "

For l_intLoop = 0 To 12
    l_strSQL = l_strSQL & txtSetup(l_intLoop).DataField & " = '" & QuoteSanitise(txtSetup(l_intLoop).Text) & "', "
    
Next

l_strSQL = Left(l_strSQL, Len(l_strSQL) - 2) & ";"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

Unload Me

End Sub

Private Sub cmdOpenDisplay_Click()

On Error GoTo cmdOpenDisplay_Click_Error
    
    Dim s As New Shell32.Shell
    s.ControlPanelItem "desk.cpl"

    Exit Sub

cmdOpenDisplay_Click_Error:

    MsgBox Err.Description, vbExclamation
    Exit Sub
    

End Sub

Private Sub cmdSaveSettingsAndTestEmail_Click()

SaveSettingsToDatabase

Dim l_strEmail As String

l_strEmail = InputBox("Please enter a valid email address", "SMTP Test", g_strCETAAdministratorEmail)

SendSMTPMail l_strEmail, "SMTP Test Recipient", "CETA SMTP Email Test", "", "If you are reading this then your SMTP email is working correctly", False, "", ""

End Sub

Private Sub Form_Load()

GetSettingsFromDatabase

PopulateCombo "jobtype", cmbJobType
PopulateCombo "allocation", cmbJobAllocation

txtVDMSEmailForInvoice.Text = g_strVDMSEmailForInvoices
chkDoOfflineCFMRequests.Value = g_optDoOfflineCFMRequests
chkUseAutoDeleteWhenFinalisingJobs.Value = g_optUseAutoDeleteWhenFinalisingJobs
chkCheckAllBBCJobs.Value = g_intCheckAllBBCJobs
chkCompulsoryBikeRefs.Value = g_intCompulsoryBikeRefs
chkEnforceRigorousLibraryChecks.Value = g_intEnforceRigorousLibraryChecks
txtDefaultPortalAddress.Text = g_strDefaultPortalAddress
txtDefaultHeaderPage.Text = g_strDefaultHeaderPage
txtDefaultFooterPage.Text = g_strDefaultFooterPage
txtDefaultMainPage.Text = g_strDefaultMainPage
txtMediaWindowSystemMessage.Text = g_strMediaWindowSystemMessage
txtAdminLink(1).Text = g_strDefaultAdminLink1
txtAdminLink(2).Text = g_strDefaultAdminLink2
txtAdminLink(3).Text = g_strDefaultAdminLink3
txtAdminTitle(1).Text = g_strDefaultAdminTitle1
txtAdminTitle(2).Text = g_strDefaultAdminTitle2
txtAdminTitle(3).Text = g_strDefaultAdminTitle3
txtAdminText(1).Text = g_strDefaultAdminText1
txtAdminText(2).Text = g_strDefaultAdminText2
txtAdminText(3).Text = g_strDefaultAdminText3

txtBBCAkamaiFTPusername.Text = g_strBBCAkamaiFTPusername
txtBBCAkamaiFTPpassword.Text = g_strBBCAkamaiFTPpassword
txtBBCAkamaiFTProotdirectory.Text = g_strBBCAkamaiFTProotdirectory
txtAsperaRootDirectory.Text = g_strAsperaRootDirectory
txtBBCAkamaiFTPaddress.Text = g_strBBCAkamaiFTPaddress
txtBBC16x9TranscodeWatchfolder.Text = g_strBBC16x9TranscodeWatchFolder
txtBBC4x3TranscodeWatchfolder.Text = g_strBBC4x3TranscodeWatchFolder
txtJCA16x9TranscodeWatchfolder.Text = g_strJCA16x9TranscodeWatchFolder
txtJCA4x3TranscodeWatchfolder.Text = g_strJCA4x3TranscodeWatchFolder
txtJPEGextension.Text = g_strJPEGextension
txtLocationOfJellyroll1Folder.Text = g_strLocationOfJellyroll1Folder
txtLocationOfJellyroll2Folder.Text = g_strLocationOfJellyroll2Folder
txtJellyroll1LibraryID.Text = g_lngJellyroll1LibraryID
txtJellyroll2LibraryID.Text = g_lngJellyroll2LibraryID
txtSprocketsPercentage.Text = g_lngSprocketsPercentage
txtBBCMG_Archive.Text = g_strBBCMG_Archive
txtBBCMG_ClipArchive.Text = g_strBBCMG_ClipArchive
txtBBCMG_ProxyArchive.Text = g_strBBCMG_ProxyArchive
txtBBCMG_Aspera.Text = g_strBBCMG_Aspera
txtBBCMG_Restore.Text = g_strBBCMG_Restore
txtBBCMG_Flashstore = g_strBBCMG_Flashstore
txtTranscodeOverlayFolder.Text = g_strTranscodeOverlayFolder
txtSigniantSendPath.Text = g_strSigniantSendPath

chkUseSingleCompletionScreenForDubbing.Value = g_optUseSingleCompletionScreenForDubbing

txtRateCardType.Text = g_strRateCardType
txtExcelOrderLocation.Text = g_strExcelOrderLocation
txtLocationOfOmneonXLS.Text = g_strLocationOfOmneonXLS
chkHideAudioChannels.Value = g_optHideAudioChannels
chkRequireProductOnQuotes.Value = g_optRequireProductOnQuotes
chkDescDeadNotCompulsory.Value = g_optDescDeadNotCompulsory
chkCostDespatch.Value = g_optCostDespatch
chkJCADeliverylabels = g_optJCADeliverylabels
chkShowBillToInformationOnDespatch.Value = g_optShowBillToInformationOnDespatch
chkDontPromptForConfirmationOnCompletionOfDespatches = g_optDontPromptForConfirmationOnCompletionOfDespatches
chkDontClearShelfWhenMovingTapes.Value = g_optDontClearShelfWhenMovingTapes
chkPrintCopyJobSheets.Value = g_intPrintCopyJobSheets

txtCETAAdministratorEmail.Text = g_strCETAAdministratorEmail
txtCetaMailFromEmailDefault.Text = g_strCetaMailFromEmailDefault
txtAdministratorEmailAddress.Text = g_strAdministratorEmailAddress
txtAdministratorEmailName.Text = g_strAdministratorEmailName
txtOperationsEmailAddress.Text = g_strOperationsEmailAddress
txtOperationsEmailName.Text = g_strOperationsEmailName
txtBookingsEmailAddress.Text = g_strBookingsEmailAddress
txtBookingsEmailName.Text = g_strBookingsEmailName
txtManagerEmailAddress.Text = g_strManagerEmailAddress
txtQuotesEmailAddress.Text = g_strQuotesEmailAddress
txtQuotesEmailName.Text = g_strQuotesEmailName

chkAbortIfAZeroDuration = g_optAbortIfAZeroDuration

chkOrderByNumbers.Value = g_optOrderByNumbers

chkDeadlineTimeNotCompulsory.Value = g_optDeadlineTimeNotCompulsory

chkAllocateBarcodes.Value = g_optAllocateBarcodes

chkAddFakeJobIDToTapes.Value = g_optAddFakeJobIDToTapes

chkPrefixLibraryBarcodesWithCompanyPrefix.Value = g_optPrefixLibraryBarcodesWithCompanyPrefix
txtCompanyPrefix.Text = g_strCompanyPrefix

chkUseFormattedTimeCodesInEvents.Value = g_optUseFormattedTimeCodesInEvents

chkAllowCostingEntryAtAnyStatus.Value = g_optAllowCostingEntryAtAnyStatus
chkDisableCostingPopUp.Value = g_optDisableCostingPopUp
chkUseMinimumCharging.Value = g_optUseMinimumCharging
txtBaseRateCard.Text = g_intBaseRateCard
chkDisableUpdateLibraryRecordWithJobDetails = g_optDisableUpdateLibraryRecordWithJobDetails

chkDontMakeMachineTimeADecimal.Value = g_optDontMakeMachineTimeADecimal

chkDontUseDynoBarcodeLabel.Value = g_optDontUseDynoBarcodeLabel

chkUseJCAPostingToAccounts.Value = g_optUseJCAPostingToAccounts

chkUseAudioStandardInLibraryToStoreLanguage.Value = g_optUseAudioStandardInLibraryToStoreLanguage

txtScreenGrabStore.Text = g_strScreenGrabStore
txtKeyframeStore = g_strKeyframeStore
txtFlashThumbStore = g_strFlashThumbStore
txtFlashstorePath.Text = g_strFlashstorePath
txtAsperaServer.Text = g_strAsperaServer
chkAsperaOnline = g_optAsperaOnline
chkVantageOnline = g_optVantageOnline
txtLocationOfRapidsLogs = g_strLocationOfRapidsLogs
txtLocationOfWatchfolders.Text = g_strLocationOfWatchfolders

chkUseProductRateCards.Value = g_optUseProductRateCards

chkAllowCostedJobsWhenNotComplete.Value = g_optAllowCostedJobsWhenNotComplete

chkDontForceUnitsChargedOnHireJobs.Value = g_optDontForceUnitsChargedOnHireJobs
chkHideVideoStandardOnJobForm.Value = g_intHideVideoStandardOnJobForm

txtPasswordChangeDays.Text = g_intPasswordChangeEvery
txtPasswordLength.Text = g_intPasswordLength
txtPasswordNumbers.Text = g_intPasswordNumbers
txtPasswordUppercase.Text = g_intPasswordUpperCase

chkLockDiscountColumnInCostings.Value = g_intLockDiscountColumnInCostings

chkUserIsNotDefaultOurContact.Value = g_optUserIsNotDefaultOurContact

chkDontCostScheduledResourcesIfJobTypeIsADub.Value = g_intDontCostScheduledResourcesIfJobTypeIsADub

txtAdvancedPurchaseApproveEmail.Text = g_strAdvancedPurchaseApproveEmail

chkRequireVideoStandardOnQuote.Value = g_optRequireVideoStandardOnQuote

chkHideStandardLabelsButton = g_optHideStandardLabelsButton

chkBookDubsAsConfirmedStatus.Value = g_optBookDubsAsConfirmedStatus

txtCETAAdministratorEmail.Text = g_strCETAAdministratorEmail

txtSMTPServer.Text = g_strSMTPServer
txtSMTPUserName.Text = g_strSMTPUserName
txtSMTPPassword.Text = g_strSMTPPassword
txtSMTPServerAuthorised.Text = g_strSMTPServerAuthorised
txtSMTPUserNameAuthorised.Text = g_strSMTPUserNameAuthorised
txtSMTPPasswordAUthorised.Text = g_strSMTPPasswordAuthorised

chkUseClipID.Value = g_optUseClipID
txtClipIDCaption.Text = g_strClipIDCaption

txtDefaultInternalMovementLocation.Text = g_strDefaultInternalMovementLocation

txtMaximumQuoteDiscount.Text = g_optMaximumQuoteDiscount

chkRequireBudgetCodesBeforeAuthorisation.Value = g_optRequireBudgetCodesBeforeAuthorisation

chkHideOrderByColumnOnCostingGrid.Value = g_optHideOrderByColumnOnCostingGrid

chkRequireCompletedDubsBeforeCosting.Value = g_optRequireCompletedDubsBeforeCosting

chkHideSoundChannelsOnVTDetailGrid.Value = g_optHideSoundChannelsOnVTDetailGrid

chkShowTimeColumnInPurchaseOrder.Value = g_optShowTimeColumnInPurchaseOrder

txtWebInfo(2).Text = g_strClipStorePlayURL

txtDefaultCopiesToPrintOfInvoice.Text = g_intDefaultCopiesToPrintOfInvoice

chkUpperCaseDubbingDetails.Value = g_optUpperCaseDubbingDetails

chkCreateDNotesForExternalDeliveryPurchaseOrders = g_optCreateDNotesForExternalDeliveryPurchaseOrders

chkApplyDealPriceToActualsRatherThanRateCard.Value = g_optApplyDealPriceToActualsRatherThanRateCard

chkRequireDeliveryMethodsOnHireAndEditJobs.Value = g_optRequireDeliveryMethodsOnHireAndEditJobs

chkDontPromptForNumberOfInvoiceCopies.Value = g_optDontPromptForNumberOfInvoiceCopies
chkNeedAccountNumberToConfirm.Value = g_optNeedAccountNumberToConfirm

chkAutoTickOurContactInJobLists.Value = g_optAutoTickOurContactInJobLists
chkHideZeroRateCardItemsInManagementReports.Value = g_optHideZeroRateCardItemsInManagementReports

chkAllowNewPurchaseOrdersOnCostedJobs.Value = g_optAllowNewPurchaseOrdersOnCostedJobs

chkHarshCheckingOnDubbingEntry.Value = g_optHarshCheckingOnDubbingEntry

txtDefaultCategoryToAddResource.Text = g_strDefaultCategoryToAddResource

chkCreateDeliveryNotesForJobsWithNoAddress.Value = g_optCreateDeliveryNotesForJobsWithNoAddress

chkHideHireDespatchTab.Value = g_optHideHireDespatchTab

chkRequireOurContactBeforeSaving.Value = g_optRequireOurContactBeforeSaving
chkHideTimeColumnInCostings.Value = g_optHideTimeColumn

txtRoundCurrencyDecimals = g_optRoundCurrencyDecimals
chkSendToAccountsOnFinalise.Value = g_optSendToAccountsOnFinalise
txtAccountsExportLocation.Text = g_strUploadInvoiceLocation

chkHideHireDespatchTab.Value = g_optHideHireDespatchTab
chkDontPromptForInvoiceAddress.Value = g_optDontPromptForInvoiceAddress

txtSundryMarkup.Text = g_intSundryMarkup

chkCompleteJobsWhenVTCompleted.Value = GetFlag(g_optCompleteJobsWhenVTCompleted)
chkForceDealPrice.Value = GetFlag(g_optForceDealPrice)

chkDefaultEventSearchInLibrary.Value = GetFlag(g_optDefaultEventSearchInLibrary)
chkHideVATFieldsInCostings = GetFlag(g_optHideVATFieldsInCostings)

chkMakeEventsWide.Value = GetFlag(g_optMakeEventsWide)

txtConnection1.Text = g_optConnection1Name
txtConnection2.Text = g_optConnection2Name
txtConnection3.Text = g_optConnection3Name
txtConnection4.Text = g_optConnection4Name

cmbWhenToAllocate.AddItem "Print Job Sheet"
cmbWhenToAllocate.AddItem "Confirm Job"
cmbWhenToAllocate.AddItem "Book Job"

cmbDefaultTabInJobDetails.AddItem "Dubbing"
cmbDefaultTabInJobDetails.AddItem "Schedule"

cmbDefaultLibrarySearchField.AddItem "Client"
cmbDefaultLibrarySearchField.AddItem "Product"
cmbDefaultLibrarySearchField.AddItem "Title"

chkAllowEditAuthorisedPurchaseOrders.Value = GetFlag(g_optAllowEditAuthorisedPurchaseOrders)
chkUseDepartmentalAuthorisationNumbers.Value = GetFlag(g_optUseDepartmentalPurchaseAuthorisationNumbers)

chkAddDurationToRateCode.Value = GetFlag(g_optAddDurationToRateCode)
chkAlwaysUseRateCardDescriptions.Value = GetFlag(g_optAlwaysUseRateCardDescriptions)
chkUseJobDetailsWhenAddingJobDetailLinesToDespatch = GetFlag(g_optUseJobDetailsWhenAddingJobDetailLinesToDespatch)

cmbDefaultLibrarySearchField.Text = g_optSearchLibraryDefaultField
cmbDefaultTabInJobDetails.Text = IIf(g_intDefaultTab = 7, "Schedule", "Dubbing")
chkPromptForLateDespatchReason.Value = GetFlag(g_optPromptForLateDespatchReason)
chkShowCompanyInsuranceOutDate.Value = GetFlag(g_optShowCompanyInsuranceOutDate)

If (g_optLockSystem And 1) = 1 Then chkSystemCETALock.Value = 1
If (g_optLockSystem And 2) = 2 Then chkSystemFileManagerLock.Value = 1
If (g_optLockSystem And 4) = 4 Then chkSystemWebLock.Value = 1
If (g_optLockSystem And 8) = 8 Then chkSystemInvoiceLock.Value = 1
If (g_optLockSystem And 16) = 16 Then chkFileHandlersLock.Value = 1
If (g_optLockSystem And 32) = 32 Then chkVantageAPILocked.Value = 1
If (g_optLockSystem And 64) = 64 Then chkFFMPGThumbsLocked.Value = 1
If (g_optLockSystem And 512) = 512 Then chkFFMPGGenericLocked.Value = 1
If (g_optLockSystem And 1024) = 1024 Then chkFFMPGAudioLocked.Value = 1
If (g_optLockSystem And 2048) = 2048 Then chkFFMPGSpeedChangeLocked.Value = 1
If (g_optLockSystem And 4096) = 4096 Then chkDADCXMLLock.Value = 1
If (g_optLockSystem And 8192) = 8192 Then chkChecksumHandlersLock.Value = 1
If (g_optLockSystem And 65536) = 65536 Then chkFFMPGExplicitLocked.Value = 1
If (g_optLockSystem And 131072) = 131072 Then chkDeliveryMonitoringLock.Value = 1
If (g_optLockSystem And 262144) = 262144 Then chkMediaInfoLock.Value = 1

chkCloseSystemDown.Value = GetFlag(g_optCloseSystemDown)
chkStopFFMPGServices.Value = GetFlag(g_optStopFFMPGServices)
chkUseAdditionalDescriptionsWhenCosting.Value = GetFlag(g_optUseAdditionalDescriptionsWhenCosting)
chkDeductPreSetDiscountsFromUnitPrice.Value = GetFlag(g_optDeductPreSetDiscountsFromUnitPrice)
chkUseMasterDetailOnInvoice.Value = GetFlag(g_optUseMasterDetailOnInvoice)
chkAppendInitialsAndDateToNotes = GetFlag(g_optAppendInitialsAndDateToNotes)
chkShowAdditionalEventFields = GetFlag(g_optShowAdditionalEventFields)
chkCreatePDFFilesOfTechReviews = GetFlag(g_optCreatePDFFilesOfTechReviews)
txtPDFSaveLocation.Text = g_strPDFLocation
txtFilePDFSaveLocation = g_strFilePDFLocation
txtTapePDFSaveLocation = g_strTapePDFLocation
txtNoOfCopies.Text = g_intNumberOfDespatchNoteCopies
chkRequireActualsBeforeCosting.Value = GetFlag(g_optRequireActualsBeforeCosting)
cmbJobType.Text = g_strDefaultJobType
cmbJobAllocation.Text = g_strDefaultJobAllocation
cmbWhenToAllocate.Text = g_strWhenToAllocate
chkCostDubbing.Value = GetFlag(g_optCostDubbing)
chkCostSchedule.Value = GetFlag(g_optCostSchedule)
chkUseTitleInMaster.Value = GetFlag(g_optUseTitleInMaster)
chkUseTitleInCopy.Value = GetFlag(g_optUseTitleInCopy)

chkUseSpecialDubbingComboOptions.Value = GetFlag(g_optUseSpecialDubbingComboOptions)
chkRequireProductBeforeSaving.Value = GetFlag(g_optRequireProductBeforeSaving)

txtWebInfo(0).Text = g_strWebBrowserHomeDirectory
txtWebInfo(1).Text = g_strWebBrowserResourceInfoPage

txtDefaultTimeWhenZero.Text = g_intDefaultTimeWhenZero


'populate the sequence grid
adoSequence.ConnectionString = g_strConnection
adoSequence.RecordSource = "SELECT * FROM sequence ORDER BY sequencename"
adoSequence.Refresh

'populate the allcation grid
adoAllocation.ConnectionString = g_strConnection
adoAllocation.RecordSource = "SELECT * FROM allocation ORDER BY name"
adoAllocation.Refresh

adoDepartments.ConnectionString = g_strConnection
adoDepartments.RecordSource = "SELECT * FROM department ORDER BY name"
adoDepartments.Refresh

adoVAT.ConnectionString = g_strConnection
adoVAT.RecordSource = "SELECT * FROM vatrate ORDER BY vatcode"
adoVAT.Refresh

grdVAT.Refresh
DoEvents

grdVAT.Columns(0).Visible = False

' load the company settings

Dim l_intLoop As Integer
Dim l_strSQL  As String

On Error Resume Next

l_strSQL = "SELECT * FROM setup"

Dim l_db As New ADODB.Connection
l_db.Open g_strConnection

Dim l_rst As New ADODB.Recordset
l_rst.Open l_strSQL, l_db, adOpenForwardOnly, adLockReadOnly

For l_intLoop = 0 To 12
    txtSetup(l_intLoop).Text = Trim(" " & l_rst(txtSetup(l_intLoop).DataField))
Next

l_rst.Close
Set l_rst = Nothing

l_db.Close
Set l_db = Nothing

CenterForm Me

End Sub
