VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmTrackeriTunes 
   Caption         =   "Platforms Tracker"
   ClientHeight    =   15615
   ClientLeft      =   3060
   ClientTop       =   3210
   ClientWidth     =   28560
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   15615
   ScaleWidth      =   28560
   WindowState     =   2  'Maximized
   Begin VB.OptionButton optComplete 
      Caption         =   "Dubcard Not Here "
      Height          =   255
      Index           =   20
      Left            =   7560
      TabIndex        =   118
      Tag             =   "NOCLEAR"
      Top             =   2640
      Width           =   2955
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Audio Not Here"
      Height          =   255
      Index           =   19
      Left            =   7560
      TabIndex        =   117
      Tag             =   "NOCLEAR"
      Top             =   2340
      Width           =   2955
   End
   Begin VB.CommandButton cmdCopyOverwriteAllDubcards 
      Caption         =   "Copy/Overwrite This Dubcard to all other Items in Grid"
      Height          =   795
      Left            =   24780
      TabIndex        =   116
      Top             =   3360
      Width           =   1695
   End
   Begin VB.CommandButton cmdSaveAndProcessDubcards 
      Caption         =   "Process All Dubcards for All Items in Grid"
      Height          =   795
      Left            =   22980
      TabIndex        =   114
      Top             =   3360
      Width           =   1695
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Ready for Jellyroll"
      Height          =   255
      Index           =   18
      Left            =   10680
      TabIndex        =   113
      Tag             =   "NOCLEAR"
      Top             =   480
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Audio & Proxy Here and Not Complete"
      Height          =   255
      Index           =   17
      Left            =   7560
      TabIndex        =   112
      Tag             =   "NOCLEAR"
      Top             =   2040
      Width           =   2955
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Dub Cards Here and Not Complete"
      Height          =   255
      Index           =   16
      Left            =   7560
      TabIndex        =   111
      Tag             =   "NOCLEAR"
      Top             =   1740
      Width           =   2955
   End
   Begin VB.ComboBox txtLanguage 
      Height          =   315
      Left            =   16020
      TabIndex        =   110
      ToolTipText     =   "The Format of this Tape"
      Top             =   1860
      Width           =   2535
   End
   Begin VB.ComboBox cmbProject 
      Height          =   315
      Left            =   16020
      TabIndex        =   107
      ToolTipText     =   "The Format of this Tape"
      Top             =   60
      Width           =   3795
   End
   Begin VB.CommandButton cmdFieldsDisneyPlus 
      Caption         =   "Disney+ Fields"
      Height          =   375
      Left            =   9300
      TabIndex        =   106
      Top             =   3360
      Width           =   2175
   End
   Begin VB.CommandButton cmdXbox 
      Caption         =   "XBox Fields"
      Height          =   375
      Left            =   7020
      TabIndex        =   102
      Top             =   3780
      Width           =   2175
   End
   Begin VB.CommandButton cmdNewPlaformOrder 
      Caption         =   "Create New Platform Order"
      Height          =   675
      Left            =   180
      TabIndex        =   97
      Top             =   1920
      Width           =   1515
   End
   Begin VB.CommandButton cmdAmazonMLFFields 
      Caption         =   "Amazon MLF Fields"
      Height          =   375
      Left            =   4740
      TabIndex        =   96
      Top             =   3780
      Width           =   2175
   End
   Begin VB.CommandButton cmdGoogleOMUFields 
      Caption         =   "Google OMU Fields"
      Height          =   375
      Left            =   2460
      TabIndex        =   95
      Top             =   3780
      Width           =   2175
   End
   Begin VB.CommandButton cmdPlatformAssets 
      Caption         =   "Platfoirm Assets Fields"
      Height          =   375
      Left            =   120
      TabIndex        =   94
      Top             =   3780
      Width           =   2175
   End
   Begin VB.CommandButton cmdAmazonFields 
      Caption         =   "Amazon Fields"
      Height          =   375
      Left            =   4740
      TabIndex        =   93
      Top             =   3360
      Width           =   2175
   End
   Begin VB.CommandButton cmdGoogleFields 
      Caption         =   "Google Fields"
      Height          =   375
      Left            =   2460
      TabIndex        =   92
      Top             =   3360
      Width           =   2175
   End
   Begin VB.CommandButton cmdiTunesTV 
      Caption         =   "iTunes TV Fields"
      Height          =   375
      Left            =   7020
      TabIndex        =   91
      Top             =   3360
      Width           =   2175
   End
   Begin VB.TextBox txtAlphaCode 
      Height          =   315
      Left            =   16020
      TabIndex        =   85
      Top             =   2940
      Width           =   2535
   End
   Begin VB.CommandButton cmdFieldsLinkedPackage 
      Caption         =   "Linked Package Fields"
      Height          =   375
      Left            =   20700
      TabIndex        =   83
      Top             =   3360
      Width           =   2175
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Completable"
      Height          =   255
      Index           =   15
      Left            =   5700
      TabIndex        =   82
      Tag             =   "NOCLEAR"
      Top             =   1440
      Width           =   1575
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Workable"
      Height          =   255
      Index           =   14
      Left            =   5700
      TabIndex        =   81
      Tag             =   "NOCLEAR"
      Top             =   1140
      Width           =   1575
   End
   Begin VB.Frame fraDeliveryTypes 
      Height          =   3135
      Left            =   3720
      TabIndex        =   76
      Top             =   60
      Width           =   1815
      Begin VB.OptionButton optDeliveryType 
         Caption         =   "Disney+"
         Height          =   195
         Index           =   11
         Left            =   120
         TabIndex        =   105
         Top             =   2340
         Width           =   1635
      End
      Begin VB.OptionButton optDeliveryType 
         Caption         =   "Netflix"
         Height          =   195
         Index           =   5
         Left            =   120
         TabIndex        =   104
         Top             =   2100
         Width           =   1275
      End
      Begin VB.OptionButton optDeliveryType 
         Caption         =   "Sony PSN"
         Height          =   195
         Index           =   4
         Left            =   120
         TabIndex        =   103
         Top             =   1860
         Width           =   1275
      End
      Begin VB.OptionButton optDeliveryType 
         Caption         =   "XBox"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   100
         Top             =   1620
         Width           =   1275
      End
      Begin VB.OptionButton optDeliveryType 
         Caption         =   "Google OMU"
         Height          =   195
         Index           =   10
         Left            =   120
         TabIndex        =   99
         Top             =   900
         Width           =   1575
      End
      Begin VB.OptionButton optDeliveryType 
         Caption         =   "Amazon MLF"
         Height          =   195
         Index           =   9
         Left            =   120
         TabIndex        =   98
         Top             =   1380
         Width           =   1575
      End
      Begin VB.OptionButton optDeliveryType 
         Caption         =   "Amazon"
         Height          =   195
         Index           =   8
         Left            =   120
         TabIndex        =   89
         Top             =   1140
         Width           =   1575
      End
      Begin VB.OptionButton optDeliveryType 
         Caption         =   "Google"
         Height          =   195
         Index           =   7
         Left            =   120
         TabIndex        =   88
         Top             =   660
         Width           =   1575
      End
      Begin VB.OptionButton optDeliveryType 
         Caption         =   "iTunes TV"
         Height          =   195
         Index           =   6
         Left            =   120
         TabIndex        =   87
         Top             =   420
         Width           =   1575
      End
      Begin VB.OptionButton optDeliveryType 
         Caption         =   "All"
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   79
         Top             =   2820
         Value           =   -1  'True
         Width           =   1275
      End
      Begin VB.OptionButton optDeliveryType 
         Caption         =   "Other"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   78
         Top             =   2580
         Width           =   1275
      End
      Begin VB.OptionButton optDeliveryType 
         Caption         =   "iTunes Film"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   77
         Top             =   180
         Width           =   1575
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnCommentTypes 
      Height          =   855
      Left            =   16140
      TabIndex        =   75
      Top             =   13620
      Width           =   2175
      DataFieldList   =   "column 0"
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).Caption=   "value"
      Columns(0).Name =   "value"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   3836
      _ExtentY        =   1508
      _StockProps     =   77
      DataFieldToDisplay=   "column 0"
   End
   Begin VB.Frame fraReport 
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   11520
      TabIndex        =   73
      Top             =   2940
      Width           =   1515
      Begin VB.CommandButton cmdReport 
         Caption         =   "Report"
         Height          =   315
         Left            =   60
         TabIndex        =   74
         Top             =   0
         Width           =   915
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnProjectManager 
      Bindings        =   "frmTrackeriTunes.frx":0000
      Height          =   1755
      Left            =   2760
      TabIndex        =   72
      Top             =   9360
      Width           =   5775
      DataFieldList   =   "name"
      ListAutoValidate=   0   'False
      MaxDropDownItems=   20
      _Version        =   196617
      ColumnHeaders   =   0   'False
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   9710
      Columns(0).Caption=   "Name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      _ExtentX        =   10186
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "name"
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "DADC Not Workable"
      Height          =   255
      Index           =   13
      Left            =   5700
      TabIndex        =   71
      Tag             =   "NOCLEAR"
      Top             =   2040
      Visible         =   0   'False
      Width           =   1755
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "DADC Workable"
      Height          =   255
      Index           =   12
      Left            =   5700
      TabIndex        =   70
      Tag             =   "NOCLEAR"
      Top             =   1740
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.CommandButton cmdFieldsSubs 
      Caption         =   "Subs Workflow Fields"
      Height          =   375
      Left            =   18420
      TabIndex        =   69
      Top             =   3780
      Width           =   2175
   End
   Begin VB.CheckBox chkHideEpStuff 
      Alignment       =   1  'Right Justify
      Caption         =   "Hide Ep Stuff"
      Height          =   255
      Left            =   180
      TabIndex        =   68
      Tag             =   "NOCLEAR"
      Top             =   900
      Width           =   1515
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Metadata Here and Not Complete"
      Height          =   255
      Index           =   11
      Left            =   7560
      TabIndex        =   67
      Tag             =   "NOCLEAR"
      Top             =   1440
      Width           =   2955
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Subs Here and Not Complete"
      Height          =   255
      Index           =   10
      Left            =   7560
      TabIndex        =   66
      Tag             =   "NOCLEAR"
      Top             =   1140
      Width           =   2955
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Artwork Here and Not Complete"
      Height          =   255
      Index           =   9
      Left            =   7560
      TabIndex        =   65
      Tag             =   "NOCLEAR"
      Top             =   840
      Width           =   2955
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Trailers Here and Not Complete"
      Height          =   255
      Index           =   8
      Left            =   7560
      TabIndex        =   64
      Tag             =   "NOCLEAR"
      Top             =   540
      Width           =   2955
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Feature Here and Not Complete"
      Height          =   255
      Index           =   7
      Left            =   7560
      TabIndex        =   63
      Tag             =   "NOCLEAR"
      Top             =   240
      Width           =   2955
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Pending"
      Height          =   255
      Index           =   6
      Left            =   5700
      TabIndex        =   62
      Tag             =   "NOCLEAR"
      Top             =   840
      Width           =   1575
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Ready to Package"
      Height          =   255
      Index           =   5
      Left            =   10680
      TabIndex        =   61
      Tag             =   "NOCLEAR"
      Top             =   240
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Billed"
      Height          =   255
      Index           =   4
      Left            =   12600
      TabIndex        =   60
      Tag             =   "NOCLEAR"
      Top             =   540
      Width           =   1575
   End
   Begin VB.TextBox txtVendorID 
      Height          =   315
      Left            =   16020
      TabIndex        =   57
      Top             =   2580
      Width           =   2535
   End
   Begin VB.TextBox txtOrderNumber 
      Height          =   315
      Left            =   16020
      TabIndex        =   56
      Top             =   2220
      Width           =   2535
   End
   Begin VB.CommandButton cmdFieldsReady 
      Caption         =   "Completed Fields"
      Height          =   375
      Left            =   9300
      TabIndex        =   55
      Top             =   3780
      Width           =   2175
   End
   Begin VB.CommandButton cmdFieldsWorkflowTrailerExtended 
      Caption         =   "Trail Extended Work Fields"
      Height          =   375
      Left            =   16140
      TabIndex        =   36
      Top             =   3780
      Width           =   2175
   End
   Begin VB.CommandButton cmdFieldsAssets 
      Caption         =   "Assets Fields"
      Height          =   375
      Left            =   18420
      TabIndex        =   35
      Top             =   3360
      Width           =   2175
   End
   Begin VB.CommandButton cmdFieldsWorkflowOthers 
      Caption         =   "Other Workflow Fields"
      Height          =   375
      Left            =   20700
      TabIndex        =   34
      Top             =   3780
      Width           =   2175
   End
   Begin VB.CommandButton cmdFieldsWorkflowTrailer 
      Caption         =   "Trail Basic Work Fields"
      Height          =   375
      Left            =   13860
      TabIndex        =   33
      Top             =   3780
      Width           =   2175
   End
   Begin VB.CommandButton cmdFieldsFullHeader 
      Caption         =   "Full Header Fields"
      Height          =   375
      Left            =   16140
      TabIndex        =   32
      Top             =   3360
      Width           =   2175
   End
   Begin VB.CommandButton cmdFieldsSummary 
      Caption         =   "iTunes Summary Fields"
      Height          =   375
      Left            =   11580
      TabIndex        =   31
      Top             =   3360
      Width           =   2175
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnChannel 
      Bindings        =   "frmTrackeriTunes.frx":001B
      Height          =   1755
      Left            =   1260
      TabIndex        =   30
      Top             =   7380
      Width           =   5775
      DataFieldList   =   "portalcompanyname"
      ListAutoValidate=   0   'False
      MaxDropDownItems=   20
      _Version        =   196617
      ColumnHeaders   =   0   'False
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   2
      Columns(0).Width=   9710
      Columns(0).Caption=   "Name"
      Columns(0).Name =   "portalcompanyname"
      Columns(0).DataField=   "portalcompanyname"
      Columns(0).FieldLen=   256
      Columns(1).Width=   8819
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   10186
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "portalcompanyname"
   End
   Begin VB.CommandButton cmdFieldsHeaders 
      Caption         =   "Normal Header Fields"
      Height          =   375
      Left            =   13860
      TabIndex        =   29
      Top             =   3360
      Width           =   2175
   End
   Begin VB.CommandButton cmdFieldsWorkflowFeature 
      Caption         =   "Feature Work Fields"
      Height          =   375
      Left            =   11580
      TabIndex        =   28
      Top             =   3780
      Width           =   2175
   End
   Begin VB.CommandButton cmdFieldsAll 
      Caption         =   "All Fields"
      Height          =   375
      Left            =   120
      TabIndex        =   27
      Top             =   3360
      Width           =   2175
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Pending"
      Height          =   255
      Index           =   3
      Left            =   5700
      TabIndex        =   25
      Tag             =   "NOCLEAR"
      Top             =   540
      Width           =   1575
   End
   Begin VB.CheckBox chkHideDemo 
      Alignment       =   1  'Right Justify
      Caption         =   "Hide Demo"
      Height          =   255
      Left            =   180
      TabIndex        =   23
      Tag             =   "NOCLEAR"
      Top             =   600
      Width           =   1515
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "All Items"
      Height          =   255
      Index           =   0
      Left            =   12600
      TabIndex        =   20
      Tag             =   "NOCLEAR"
      Top             =   840
      Width           =   1575
   End
   Begin VB.CheckBox chkLockGB 
      Alignment       =   1  'Right Justify
      Caption         =   "Lock GB size"
      Height          =   255
      Left            =   180
      TabIndex        =   19
      Tag             =   "NOCLEAR"
      Top             =   1500
      Width           =   1515
   End
   Begin VB.CheckBox chkLockDur 
      Alignment       =   1  'Right Justify
      Caption         =   "Lock Duration"
      Height          =   255
      Left            =   180
      TabIndex        =   18
      Tag             =   "NOCLEAR"
      Top             =   1200
      Width           =   1515
   End
   Begin VB.TextBox txtSeries 
      Height          =   315
      Left            =   16020
      TabIndex        =   15
      Top             =   1140
      Width           =   2535
   End
   Begin VB.TextBox txtEpisode 
      Height          =   315
      Left            =   16020
      TabIndex        =   14
      Top             =   1500
      Width           =   2535
   End
   Begin VB.TextBox txtSubtitle 
      Height          =   315
      Left            =   16020
      TabIndex        =   13
      Top             =   780
      Width           =   3795
   End
   Begin VB.TextBox txtTitle 
      Height          =   315
      Left            =   16020
      TabIndex        =   10
      Top             =   420
      Width           =   3795
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Finished"
      Height          =   255
      Index           =   2
      Left            =   12600
      TabIndex        =   7
      Tag             =   "NOCLEAR"
      Top             =   240
      Width           =   1575
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete"
      Height          =   255
      Index           =   1
      Left            =   5700
      TabIndex        =   6
      Tag             =   "NOCLEAR"
      Top             =   240
      Value           =   -1  'True
      Width           =   1575
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   5940
      TabIndex        =   1
      Top             =   14760
      Width           =   19755
      Begin VB.CommandButton cmdExportGrid 
         Caption         =   "Export Grid"
         Height          =   315
         Left            =   900
         TabIndex        =   84
         Top             =   0
         Width           =   1575
      End
      Begin VB.CommandButton cmdUnbillAll 
         Caption         =   "Unmark Billing All"
         Height          =   315
         Left            =   2580
         TabIndex        =   41
         Top             =   0
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.CommandButton cmdBillAll 
         Caption         =   "Bill All"
         Height          =   315
         Left            =   4320
         TabIndex        =   40
         Top             =   0
         Visible         =   0   'False
         Width           =   1695
      End
      Begin VB.CommandButton cmdUnbill 
         Caption         =   "Unmark as Billed"
         Height          =   315
         Left            =   6180
         TabIndex        =   39
         Top             =   0
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.CommandButton cmdManualBillItem 
         Caption         =   "Manually Mark as Billed"
         Height          =   315
         Left            =   8160
         TabIndex        =   38
         Top             =   0
         Visible         =   0   'False
         Width           =   2235
      End
      Begin VB.CommandButton cmdBillItem 
         Caption         =   "Bill Item"
         Height          =   315
         Left            =   10560
         TabIndex        =   37
         Top             =   0
         Visible         =   0   'False
         Width           =   1755
      End
      Begin VB.CommandButton cmdUpdateAll 
         Caption         =   "Update File Information"
         Height          =   315
         Left            =   12420
         TabIndex        =   26
         Top             =   0
         Width           =   2235
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   18480
         TabIndex        =   5
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   17220
         TabIndex        =   4
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   15960
         TabIndex        =   3
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   14700
         TabIndex        =   2
         Top             =   0
         Width           =   1215
      End
   End
   Begin MSAdodcLib.Adodc adoItems 
      Height          =   330
      Left            =   120
      Top             =   4200
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdItems 
      Bindings        =   "frmTrackeriTunes.frx":0036
      Height          =   5475
      Left            =   120
      TabIndex        =   0
      Top             =   4260
      Width           =   28260
      ScrollBars      =   3
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   3
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTrackeriTunes.frx":004D
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmTrackeriTunes.frx":0069
      stylesets(2).Name=   "headerfield"
      stylesets(2).ForeColor=   0
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmTrackeriTunes.frx":0085
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      StyleSet        =   "headerfield"
      RowHeight       =   450
      ExtraHeight     =   26
      Columns.Count   =   155
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "tracker_itemID"
      Columns(0).Name =   "tracker_iTunes_itemID"
      Columns(0).DataField=   "tracker_iTunes_itemID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   1588
      Columns(1).Caption=   "Job #"
      Columns(1).Name =   "jobID"
      Columns(1).DataField=   "jobID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   1773
      Columns(2).Caption=   "RR Client"
      Columns(2).Name =   "channel"
      Columns(2).FieldLen=   256
      Columns(2).StyleSet=   "headerfield"
      Columns(3).Width=   1138
      Columns(3).Caption=   "Pending?"
      Columns(3).Name =   "rejected"
      Columns(3).DataField=   "rejected"
      Columns(3).FieldLen=   256
      Columns(3).Style=   2
      Columns(3).StyleSet=   "headerfield"
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "companyID"
      Columns(4).Name =   "companyID"
      Columns(4).DataField=   "companyID"
      Columns(4).FieldLen=   256
      Columns(5).Width=   1773
      Columns(5).Caption=   "Rejected Date"
      Columns(5).Name =   "Rejected Date"
      Columns(5).DataField=   "RejectedDate"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   1773
      Columns(6).Caption=   "Off Rej Date"
      Columns(6).Name =   "OffRejectedDate"
      Columns(6).DataField=   "OffRejectedDate"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   2064
      Columns(7).Caption=   "Recent Rej Date"
      Columns(7).Name =   "MostRecentRejectedDate"
      Columns(7).DataField=   "MostRecentRejectedDate"
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   1296
      Columns(8).Caption=   "Days Rej"
      Columns(8).Name =   "DaysonRejected"
      Columns(8).DataField=   "DaysonRejected"
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(9).Width=   1667
      Columns(9).Caption=   "Fix JobID"
      Columns(9).Name =   "fixedjobID"
      Columns(9).DataField=   "fixedjobID"
      Columns(9).FieldLen=   256
      Columns(10).Width=   1058
      Columns(10).Caption=   "Priority"
      Columns(10).Name=   "priority"
      Columns(10).DataField=   "priority"
      Columns(10).FieldLen=   256
      Columns(10).Style=   2
      Columns(10).StyleSet=   "headerfield"
      Columns(11).Width=   1773
      Columns(11).Caption=   "Operator"
      Columns(11).Name=   "operator"
      Columns(11).DataField=   "operator"
      Columns(11).FieldLen=   256
      Columns(12).Width=   2302
      Columns(12).Caption=   "Order Number"
      Columns(12).Name=   "OrderNumber"
      Columns(12).DataField=   "OrderNumber"
      Columns(12).FieldLen=   256
      Columns(12).StyleSet=   "headerfield"
      Columns(13).Width=   2328
      Columns(13).Caption=   "iTunes Vendor ID"
      Columns(13).Name=   "iTunesVendorID"
      Columns(13).DataField=   "iTunesVendorID"
      Columns(13).FieldLen=   256
      Columns(13).StyleSet=   "headerfield"
      Columns(14).Width=   2117
      Columns(14).Caption=   "Project Manager"
      Columns(14).Name=   "ProjectManager"
      Columns(14).DataField=   "ProjectManager"
      Columns(14).FieldLen=   256
      Columns(14).StyleSet=   "headerfield"
      Columns(15).Width=   3175
      Columns(15).Caption=   "Order Type"
      Columns(15).Name=   "iTunesOrderType"
      Columns(15).DataField=   "iTunesOrderType"
      Columns(15).FieldLen=   256
      Columns(15).StyleSet=   "headerfield"
      Columns(16).Width=   1535
      Columns(16).Caption=   "Pack Type"
      Columns(16).Name=   "iTunesSubmissionType"
      Columns(16).DataField=   "iTunesSubmissionType"
      Columns(16).FieldLen=   256
      Columns(16).StyleSet=   "headerfield"
      Columns(17).Width=   3200
      Columns(17).Caption=   "AlphaDisplayCode"
      Columns(17).Name=   "AlphaDisplayCode"
      Columns(17).DataField=   "AlphaDisplayCode"
      Columns(17).FieldLen=   256
      Columns(17).StyleSet=   "headerfield"
      Columns(18).Width=   1058
      Columns(18).Caption=   "Project"
      Columns(18).Name=   "Project"
      Columns(18).DataField=   "Project"
      Columns(18).FieldLen=   256
      Columns(18).StyleSet=   "headerfield"
      Columns(19).Width=   3519
      Columns(19).Caption=   "Title"
      Columns(19).Name=   "title"
      Columns(19).DataField=   "title"
      Columns(19).FieldLen=   256
      Columns(19).StyleSet=   "headerfield"
      Columns(20).Width=   714
      Columns(20).Caption=   "Sr"
      Columns(20).Name=   "Series"
      Columns(20).DataField=   "Series"
      Columns(20).FieldLen=   256
      Columns(20).StyleSet=   "headerfield"
      Columns(21).Width=   714
      Columns(21).Caption=   "Vol"
      Columns(21).Name=   "Volume"
      Columns(21).DataField=   "Volume"
      Columns(21).FieldLen=   256
      Columns(21).StyleSet=   "headerfield"
      Columns(22).Width=   873
      Columns(22).Caption=   "Eps"
      Columns(22).Name=   "episode"
      Columns(22).DataField=   "episode"
      Columns(22).FieldLen=   256
      Columns(22).StyleSet=   "headerfield"
      Columns(23).Width=   3519
      Columns(23).Caption=   "Episode Title"
      Columns(23).Name=   "episodetitle"
      Columns(23).DataField=   "episodetitle"
      Columns(23).FieldLen=   256
      Columns(23).StyleSet=   "headerfield"
      Columns(24).Width=   3200
      Columns(24).Visible=   0   'False
      Columns(24).Caption=   "storagebarcode"
      Columns(24).Name=   "storagebarcode"
      Columns(24).DataField=   "storagebarcode"
      Columns(24).FieldLen=   256
      Columns(25).Width=   873
      Columns(25).Caption=   "F HD"
      Columns(25).Name=   "FeatureHD"
      Columns(25).DataField=   "FeatureHD"
      Columns(25).FieldLen=   256
      Columns(25).Style=   2
      Columns(25).StyleSet=   "headerfield"
      Columns(26).Width=   1376
      Columns(26).Caption=   "Fr Rate"
      Columns(26).Name=   "DisneyPlus_Framerate"
      Columns(26).DataField=   "DisneyPlus_Framerate"
      Columns(26).FieldLen=   256
      Columns(26).StyleSet=   "headerfield"
      Columns(27).Width=   1588
      Columns(27).Caption=   "Feat A Spec"
      Columns(27).Name=   "FeatureAudioSpec"
      Columns(27).DataField=   "FeatureAudioSpec"
      Columns(27).FieldLen=   256
      Columns(27).StyleSet=   "headerfield"
      Columns(28).Width=   1773
      Columns(28).Caption=   "Lang"
      Columns(28).Name=   "FeatureLanguage1"
      Columns(28).DataField=   "FeatureLanguage1"
      Columns(28).FieldLen=   256
      Columns(28).StyleSet=   "headerfield"
      Columns(29).Width=   1773
      Columns(29).Caption=   "Lang 2"
      Columns(29).Name=   "FeatureLanguage2"
      Columns(29).DataField=   "FeatureLanguage2"
      Columns(29).FieldLen=   256
      Columns(29).StyleSet=   "headerfield"
      Columns(30).Width=   1773
      Columns(30).Caption=   "Lang 3"
      Columns(30).Name=   "FeatureLanguage3"
      Columns(30).DataField=   "FeatureLanguage3"
      Columns(30).FieldLen=   256
      Columns(30).StyleSet=   "headerfield"
      Columns(31).Width=   1773
      Columns(31).Caption=   "Lang 4"
      Columns(31).Name=   "FeatureLanguage4"
      Columns(31).DataField=   "FeatureLanguage4"
      Columns(31).FieldLen=   256
      Columns(31).StyleSet=   "headerfield"
      Columns(32).Width=   873
      Columns(32).Caption=   "T HD"
      Columns(32).Name=   "TrailerHD"
      Columns(32).DataField=   "TrailerHD"
      Columns(32).FieldLen=   256
      Columns(32).Style=   2
      Columns(32).StyleSet=   "headerfield"
      Columns(33).Width=   1588
      Columns(33).Caption=   "Trail A Spec"
      Columns(33).Name=   "TrailerAudioSpec"
      Columns(33).DataField=   "TrailerAudioSpec"
      Columns(33).FieldLen=   256
      Columns(33).StyleSet=   "headerfield"
      Columns(34).Width=   1429
      Columns(34).Caption=   "Trail BIS"
      Columns(34).Name=   "trailerBIS"
      Columns(34).DataField=   "trailerBIS"
      Columns(34).FieldLen=   256
      Columns(34).StyleSet=   "headerfield"
      Columns(35).Width=   1588
      Columns(35).Caption=   "Trail Create"
      Columns(35).Name=   "trailercreate"
      Columns(35).DataField=   "trailercreate"
      Columns(35).FieldLen=   256
      Columns(35).StyleSet=   "headerfield"
      Columns(36).Width=   1773
      Columns(36).Caption=   "Trailer Lang 1"
      Columns(36).Name=   "TrailerLanguage1"
      Columns(36).DataField=   "TrailerLanguage1"
      Columns(36).FieldLen=   256
      Columns(36).StyleSet=   "headerfield"
      Columns(37).Width=   1773
      Columns(37).Caption=   "Trailer Lang 2"
      Columns(37).Name=   "TrailerLanguage2"
      Columns(37).DataField=   "TrailerLanguage2"
      Columns(37).FieldLen=   256
      Columns(37).StyleSet=   "headerfield"
      Columns(38).Width=   1773
      Columns(38).Caption=   "Trailer Lang 3"
      Columns(38).Name=   "TrailerLanguage3"
      Columns(38).DataField=   "TrailerLanguage3"
      Columns(38).FieldLen=   256
      Columns(38).StyleSet=   "headerfield"
      Columns(39).Width=   1773
      Columns(39).Caption=   "Trailer Lang 4"
      Columns(39).Name=   "TrailerLanguage4"
      Columns(39).DataField=   "TrailerLanguage4"
      Columns(39).FieldLen=   256
      Columns(39).StyleSet=   "headerfield"
      Columns(40).Width=   1773
      Columns(40).Caption=   "Art Lang 1"
      Columns(40).Name=   "ArtworkLanguage1"
      Columns(40).DataField=   "ArtworkLanguage1"
      Columns(40).FieldLen=   256
      Columns(40).StyleSet=   "headerfield"
      Columns(41).Width=   1773
      Columns(41).Caption=   "Art Lang 2"
      Columns(41).Name=   "ArtworkLanguage2"
      Columns(41).DataField=   "ArtworkLanguage2"
      Columns(41).FieldLen=   256
      Columns(41).StyleSet=   "headerfield"
      Columns(42).Width=   1773
      Columns(42).Caption=   "Art Lang 3"
      Columns(42).Name=   "ArtworkLanguage3"
      Columns(42).DataField=   "ArtworkLanguage3"
      Columns(42).FieldLen=   256
      Columns(42).StyleSet=   "headerfield"
      Columns(43).Width=   1773
      Columns(43).Caption=   "Art Lang 4"
      Columns(43).Name=   "ArtworkLanguage4"
      Columns(43).DataField=   "ArtworkLanguage4"
      Columns(43).FieldLen=   256
      Columns(43).StyleSet=   "headerfield"
      Columns(44).Width=   1773
      Columns(44).Caption=   "Subs Lang  1"
      Columns(44).Name=   "SubsLanguage1"
      Columns(44).DataField=   "SubsLanguage1"
      Columns(44).FieldLen=   256
      Columns(44).StyleSet=   "headerfield"
      Columns(45).Width=   1773
      Columns(45).Caption=   "Subs Lang 2"
      Columns(45).Name=   "SubsLanguage2"
      Columns(45).DataField=   "SubsLanguage2"
      Columns(45).FieldLen=   256
      Columns(45).StyleSet=   "headerfield"
      Columns(46).Width=   1773
      Columns(46).Caption=   "Subs Lang 3"
      Columns(46).Name=   "SubsLanguage3"
      Columns(46).DataField=   "SubsLanguage3"
      Columns(46).FieldLen=   256
      Columns(46).StyleSet=   "headerfield"
      Columns(47).Width=   1773
      Columns(47).Caption=   "Subs Lang 4"
      Columns(47).Name=   "SubsLanguage4"
      Columns(47).DataField=   "SubsLanguage4"
      Columns(47).FieldLen=   256
      Columns(47).StyleSet=   "headerfield"
      Columns(48).Width=   1402
      Columns(48).Caption=   "Chaptering"
      Columns(48).Name=   "Chaptering"
      Columns(48).DataField=   "Chaptering"
      Columns(48).FieldLen=   256
      Columns(48).StyleSet=   "headerfield"
      Columns(49).Width=   1773
      Columns(49).Caption=   "Meta Needed"
      Columns(49).Name=   "MetadataNeeded"
      Columns(49).DataField=   "MetadataNeeded"
      Columns(49).FieldLen=   256
      Columns(49).Style=   2
      Columns(49).StyleSet=   "headerfield"
      Columns(50).Width=   1640
      Columns(50).Caption=   "Due Date"
      Columns(50).Name=   "requiredby"
      Columns(50).DataField=   "requiredby"
      Columns(50).FieldLen=   256
      Columns(50).StyleSet=   "headerfield"
      Columns(51).Width=   1640
      Columns(51).Caption=   "Workable"
      Columns(51).Name=   "Workable"
      Columns(51).DataField=   "Workable"
      Columns(51).FieldLen=   256
      Columns(51).StyleSet=   "headerfield"
      Columns(52).Width=   1640
      Columns(52).Caption=   "Comp'le"
      Columns(52).Name=   "Completeable"
      Columns(52).DataField=   "Completeable"
      Columns(52).FieldLen=   256
      Columns(52).StyleSet=   "headerfield"
      Columns(53).Width=   1640
      Columns(53).Caption=   "1st Comp'le"
      Columns(53).Name=   "firstcompleteable"
      Columns(53).DataField=   "firstcompleteable"
      Columns(53).FieldLen=   256
      Columns(54).Width=   1640
      Columns(54).Caption=   "Target Date"
      Columns(54).Name=   "TargetDate"
      Columns(54).DataField=   "TargetDate"
      Columns(54).FieldLen=   256
      Columns(54).StyleSet=   "headerfield"
      Columns(55).Width=   1270
      Columns(55).Caption=   "Days Left"
      Columns(55).Name=   "TargetDateCountdown"
      Columns(55).DataField=   "TargetDateCountdown"
      Columns(55).FieldLen=   256
      Columns(55).StyleSet=   "headerfield"
      Columns(56).Width=   2910
      Columns(56).Caption=   "Feature Here"
      Columns(56).Name=   "mastershere"
      Columns(56).DataField=   "mastershere"
      Columns(56).FieldLen=   256
      Columns(56).Style=   1
      Columns(56).StyleSet=   "stagefield"
      Columns(57).Width=   2910
      Columns(57).Caption=   "Audio Here"
      Columns(57).Name=   "AudioHere"
      Columns(57).DataField=   "AudioHere"
      Columns(57).FieldLen=   256
      Columns(57).Style=   1
      Columns(57).StyleSet=   "stagefield"
      Columns(58).Width=   2910
      Columns(58).Caption=   "Trailer Here"
      Columns(58).Name=   "TrailerHere"
      Columns(58).DataField=   "TrailerHere"
      Columns(58).FieldLen=   256
      Columns(58).Style=   1
      Columns(58).StyleSet=   "stagefield"
      Columns(59).Width=   2910
      Columns(59).Caption=   "Artwork Here"
      Columns(59).Name=   "ArtworkHere"
      Columns(59).DataField=   "ArtworkHere"
      Columns(59).FieldLen=   256
      Columns(59).Style=   1
      Columns(59).StyleSet=   "stagefield"
      Columns(60).Width=   2910
      Columns(60).Caption=   "Subs Here"
      Columns(60).Name=   "SubsHere"
      Columns(60).DataField=   "SubsHere"
      Columns(60).FieldLen=   256
      Columns(60).Style=   1
      Columns(60).StyleSet=   "stagefield"
      Columns(61).Width=   2910
      Columns(61).Caption=   "Metadata Here"
      Columns(61).Name=   "metadatahere"
      Columns(61).DataField=   "metadatahere"
      Columns(61).FieldLen=   256
      Columns(61).Style=   1
      Columns(61).StyleSet=   "stagefield"
      Columns(62).Width=   1111
      Columns(62).Caption=   "All Here"
      Columns(62).Name=   "assetshere"
      Columns(62).DataField=   "assetshere"
      Columns(62).FieldLen=   256
      Columns(62).Style=   2
      Columns(62).StyleSet=   "conclusionfield"
      Columns(63).Width=   2910
      Columns(63).Caption=   "Feat Transcode"
      Columns(63).Name=   "FeatureTranscode"
      Columns(63).DataField=   "FeatureTranscode"
      Columns(63).FieldLen=   256
      Columns(63).Style=   1
      Columns(63).StyleSet=   "stagefield"
      Columns(64).Width=   2910
      Columns(64).Caption=   "Feat Ingest"
      Columns(64).Name=   "FeatureIngest"
      Columns(64).DataField=   "FeatureIngest"
      Columns(64).FieldLen=   256
      Columns(64).Style=   1
      Columns(64).StyleSet=   "stagefield"
      Columns(65).Width=   2117
      Columns(65).Caption=   "Feat Aud Trans"
      Columns(65).Name=   "FeatureSepAudioTranscode"
      Columns(65).DataField=   "FeatureSepAudioTranscode"
      Columns(65).FieldLen=   256
      Columns(65).StyleSet=   "stagefield"
      Columns(66).Width=   2910
      Columns(66).Caption=   "Feat Review"
      Columns(66).Name=   "FeatureReview"
      Columns(66).DataField=   "FeatureReview"
      Columns(66).FieldLen=   256
      Columns(66).Style=   1
      Columns(66).StyleSet=   "stagefield"
      Columns(67).Width=   2619
      Columns(67).Caption=   "Review Time Taken"
      Columns(67).Name=   "FeatureReviewTimeTaken"
      Columns(67).DataField=   "FeatureReviewTimeTaken"
      Columns(67).DataType=   3
      Columns(67).FieldLen=   256
      Columns(67).StyleSet=   "stagefield"
      Columns(68).Width=   979
      Columns(68).Caption=   "2Pass"
      Columns(68).Name=   "FeatureReview2Pass"
      Columns(68).DataField=   "FeatureReview2Pass"
      Columns(68).FieldLen=   256
      Columns(68).Style=   2
      Columns(68).StyleSet=   "stagefield"
      Columns(69).Width=   2117
      Columns(69).Caption=   "Feat Subs Ingest"
      Columns(69).Name=   "FeatureSubsIngest"
      Columns(69).DataField=   "FeatureSubsIngest"
      Columns(69).FieldLen=   256
      Columns(69).StyleSet=   "stagefield"
      Columns(70).Width=   2117
      Columns(70).Caption=   "Feat Vid Fixes"
      Columns(70).Name=   "FeatureVideoFixes"
      Columns(70).DataField=   "FeatureVideoFixes"
      Columns(70).FieldLen=   256
      Columns(70).StyleSet=   "stagefield"
      Columns(71).Width=   2117
      Columns(71).Caption=   "Feat Aud Fixes"
      Columns(71).Name=   "FeatureAudioFixes"
      Columns(71).DataField=   "FeatureAudioFixes"
      Columns(71).FieldLen=   256
      Columns(71).StyleSet=   "stagefield"
      Columns(72).Width=   2461
      Columns(72).Caption=   "Feat Subs Simp Fix"
      Columns(72).Name=   "FeatureSubsSimpleFix"
      Columns(72).DataField=   "FeatureSubsSimpleFix"
      Columns(72).FieldLen=   256
      Columns(72).StyleSet=   "stagefield"
      Columns(73).Width=   2646
      Columns(73).Caption=   "Feat Subs Comp Fix"
      Columns(73).Name=   "FeatureSubsComplexFix"
      Columns(73).DataField=   "FeatureSubsComplexFix"
      Columns(73).FieldLen=   256
      Columns(73).StyleSet=   "stagefield"
      Columns(74).Width=   2725
      Columns(74).Caption=   "Feat Subs Form Conv"
      Columns(74).Name=   "FeatureSubsFormatConvert"
      Columns(74).DataField=   "FeatureSubsFormatConvert"
      Columns(74).FieldLen=   256
      Columns(74).StyleSet=   "stagefield"
      Columns(75).Width=   2910
      Columns(75).Caption=   "Feat Completed"
      Columns(75).Name=   "Feature"
      Columns(75).DataField=   "Feature"
      Columns(75).FieldLen=   256
      Columns(75).Style=   1
      Columns(75).StyleSet=   "stagefield"
      Columns(76).Width=   2910
      Columns(76).Caption=   "Trail Transcode"
      Columns(76).Name=   "TrailerTranscode"
      Columns(76).DataField=   "TrailerTranscode"
      Columns(76).FieldLen=   256
      Columns(76).Style=   1
      Columns(76).StyleSet=   "stagefield"
      Columns(77).Width=   2910
      Columns(77).Caption=   "Trail Trans 2"
      Columns(77).Name=   "TrailerTranscode2"
      Columns(77).DataField=   "TrailerTranscode2"
      Columns(77).FieldLen=   256
      Columns(77).Style=   1
      Columns(77).StyleSet=   "stagefield"
      Columns(78).Width=   2910
      Columns(78).Caption=   "Trail Trans 3"
      Columns(78).Name=   "TrailerTranscode3"
      Columns(78).DataField=   "TrailerTranscode3"
      Columns(78).FieldLen=   256
      Columns(78).Style=   1
      Columns(78).StyleSet=   "stagefield"
      Columns(79).Width=   2910
      Columns(79).Caption=   "Trail Trans 4"
      Columns(79).Name=   "TrailerTranscode4"
      Columns(79).DataField=   "TrailerTranscode4"
      Columns(79).FieldLen=   256
      Columns(79).Style=   1
      Columns(79).StyleSet=   "stagefield"
      Columns(80).Width=   2910
      Columns(80).Caption=   "Trail Ingest"
      Columns(80).Name=   "TrailerIngest"
      Columns(80).DataField=   "TrailerIngest"
      Columns(80).FieldLen=   256
      Columns(80).Style=   1
      Columns(80).StyleSet=   "stagefield"
      Columns(81).Width=   2910
      Columns(81).Caption=   "Trail Ingest 2"
      Columns(81).Name=   "TrailerIngest2"
      Columns(81).DataField=   "TrailerIngest2"
      Columns(81).FieldLen=   256
      Columns(81).Style=   1
      Columns(81).StyleSet=   "stagefield"
      Columns(82).Width=   2910
      Columns(82).Caption=   "Trail Ingest 3"
      Columns(82).Name=   "TrailerIngest3"
      Columns(82).DataField=   "TrailerIngest3"
      Columns(82).FieldLen=   256
      Columns(82).Style=   1
      Columns(82).StyleSet=   "stagefield"
      Columns(83).Width=   2910
      Columns(83).Caption=   "Trail Ingest 4"
      Columns(83).Name=   "TrailerIngest4"
      Columns(83).DataField=   "TrailerIngest4"
      Columns(83).FieldLen=   256
      Columns(83).Style=   1
      Columns(83).StyleSet=   "stagefield"
      Columns(84).Width=   2910
      Columns(84).Caption=   "Trail Clip Made"
      Columns(84).Name=   "TrailerClipMade"
      Columns(84).DataField=   "TrailerClipMade"
      Columns(84).FieldLen=   256
      Columns(84).Style=   1
      Columns(84).StyleSet=   "stagefield"
      Columns(85).Width=   2910
      Columns(85).Caption=   "Trail Clip Made 2"
      Columns(85).Name=   "TrailerClipMade2"
      Columns(85).DataField=   "TrailerClipMade2"
      Columns(85).FieldLen=   256
      Columns(85).Style=   1
      Columns(85).StyleSet=   "stagefield"
      Columns(86).Width=   2910
      Columns(86).Caption=   "Trail Clip Made 3"
      Columns(86).Name=   "TrailerClipMade3"
      Columns(86).DataField=   "TrailerClipMade3"
      Columns(86).FieldLen=   256
      Columns(86).Style=   1
      Columns(86).StyleSet=   "stagefield"
      Columns(87).Width=   2910
      Columns(87).Caption=   "Trail Clip Made 4"
      Columns(87).Name=   "TrailerClipMade4"
      Columns(87).DataField=   "TrailerClipMade4"
      Columns(87).FieldLen=   256
      Columns(87).Style=   1
      Columns(87).StyleSet=   "stagefield"
      Columns(88).Width=   2910
      Columns(88).Caption=   "Trail Sub Burn"
      Columns(88).Name=   "TrailerSubsBurned"
      Columns(88).DataField=   "TrailerSubsBurned"
      Columns(88).FieldLen=   256
      Columns(88).Style=   1
      Columns(88).StyleSet=   "stagefield"
      Columns(89).Width=   2910
      Columns(89).Caption=   "Trail Sub Burn 2"
      Columns(89).Name=   "TrailerSubsBurned2"
      Columns(89).DataField=   "TrailerSubsBurned2"
      Columns(89).FieldLen=   256
      Columns(89).Style=   1
      Columns(89).StyleSet=   "stagefield"
      Columns(90).Width=   2910
      Columns(90).Caption=   "Trail Sub Burn 3"
      Columns(90).Name=   "TrailerSubsBurned3"
      Columns(90).DataField=   "TrailerSubsBurned3"
      Columns(90).FieldLen=   256
      Columns(90).Style=   1
      Columns(90).StyleSet=   "stagefield"
      Columns(91).Width=   2910
      Columns(91).Caption=   "Trail Sub Burn 4"
      Columns(91).Name=   "TrailerSubsBurned4"
      Columns(91).DataField=   "TrailerSubsBurned4"
      Columns(91).FieldLen=   256
      Columns(91).Style=   1
      Columns(91).StyleSet=   "stagefield"
      Columns(92).Width=   2910
      Columns(92).Caption=   "Trail Review"
      Columns(92).Name=   "TrailerReview"
      Columns(92).DataField=   "TrailerReview"
      Columns(92).FieldLen=   256
      Columns(92).Style=   1
      Columns(92).StyleSet=   "stagefield"
      Columns(93).Width=   979
      Columns(93).Caption=   "2Pass"
      Columns(93).Name=   "TrailerReview2Pass"
      Columns(93).DataField=   "TrailerReview2Pass"
      Columns(93).FieldLen=   256
      Columns(93).Style=   2
      Columns(93).StyleSet=   "stagefield"
      Columns(94).Width=   2910
      Columns(94).Caption=   "Trail Review 2"
      Columns(94).Name=   "TrailerReview2"
      Columns(94).DataField=   "TrailerReview2"
      Columns(94).FieldLen=   256
      Columns(94).Style=   1
      Columns(94).StyleSet=   "stagefield"
      Columns(95).Width=   979
      Columns(95).Caption=   "2Pass"
      Columns(95).Name=   "TrailerReview2Pass2"
      Columns(95).DataField=   "TrailerReview2Pass2"
      Columns(95).FieldLen=   256
      Columns(95).Style=   2
      Columns(95).StyleSet=   "stagefield"
      Columns(96).Width=   2910
      Columns(96).Caption=   "Trail Review 3"
      Columns(96).Name=   "TrailerReview3"
      Columns(96).DataField=   "TrailerReview3"
      Columns(96).FieldLen=   256
      Columns(96).Style=   1
      Columns(96).StyleSet=   "stagefield"
      Columns(97).Width=   979
      Columns(97).Caption=   "2Pass"
      Columns(97).Name=   "TrailerReview2Pass3"
      Columns(97).DataField=   "TrailerReview2Pass3"
      Columns(97).FieldLen=   256
      Columns(97).Style=   2
      Columns(97).StyleSet=   "stagefield"
      Columns(98).Width=   2910
      Columns(98).Caption=   "Trail Review 4"
      Columns(98).Name=   "TrailerReview4"
      Columns(98).DataField=   "TrailerReview4"
      Columns(98).FieldLen=   256
      Columns(98).Style=   1
      Columns(98).StyleSet=   "stagefield"
      Columns(99).Width=   979
      Columns(99).Caption=   "2Pass"
      Columns(99).Name=   "TrailerReview2Pass4"
      Columns(99).DataField=   "TrailerReview2Pass4"
      Columns(99).FieldLen=   256
      Columns(99).Style=   2
      Columns(99).StyleSet=   "stagefield"
      Columns(100).Width=   2752
      Columns(100).Caption=   "Trail Vid Fixes"
      Columns(100).Name=   "TrailerVideoFixes"
      Columns(100).DataField=   "TrailerVideoFixes"
      Columns(100).FieldLen=   256
      Columns(100).StyleSet=   "stagefield"
      Columns(101).Width=   2117
      Columns(101).Caption=   "Trail Aud Fixes"
      Columns(101).Name=   "TrailerAudioFixes"
      Columns(101).DataField=   "TrailerAudioFixes"
      Columns(101).FieldLen=   256
      Columns(101).StyleSet=   "stagefield"
      Columns(102).Width=   2910
      Columns(102).Caption=   "Trail Completed"
      Columns(102).Name=   "Trailer"
      Columns(102).DataField=   "Trailer"
      Columns(102).FieldLen=   256
      Columns(102).Style=   1
      Columns(102).StyleSet=   "stagefield"
      Columns(103).Width=   2910
      Columns(103).Caption=   "Sub1"
      Columns(103).Name=   "Sub1"
      Columns(103).DataField=   "Sub1"
      Columns(103).FieldLen=   256
      Columns(103).Style=   1
      Columns(103).StyleSet=   "stagefield"
      Columns(104).Width=   2910
      Columns(104).Caption=   "Sub2"
      Columns(104).Name=   "Sub2"
      Columns(104).DataField=   "Sub2"
      Columns(104).FieldLen=   256
      Columns(104).Style=   1
      Columns(104).StyleSet=   "stagefield"
      Columns(105).Width=   2910
      Columns(105).Caption=   "Sub3"
      Columns(105).Name=   "Sub3"
      Columns(105).DataField=   "Sub3"
      Columns(105).FieldLen=   256
      Columns(105).Style=   1
      Columns(105).StyleSet=   "stagefield"
      Columns(106).Width=   2910
      Columns(106).Caption=   "Sub4"
      Columns(106).Name=   "Sub4"
      Columns(106).DataField=   "Sub4"
      Columns(106).FieldLen=   256
      Columns(106).Style=   1
      Columns(106).StyleSet=   "stagefield"
      Columns(107).Width=   2910
      Columns(107).Caption=   "Subs Completed"
      Columns(107).Name=   "Subs"
      Columns(107).DataField=   "Subs"
      Columns(107).FieldLen=   256
      Columns(107).Style=   1
      Columns(107).StyleSet=   "stagefield"
      Columns(108).Width=   1931
      Columns(108).Caption=   "Make Package"
      Columns(108).Name=   "Make Package"
      Columns(108).DataField=   "Column 90"
      Columns(108).DataType=   8
      Columns(108).FieldLen=   256
      Columns(108).Style=   4
      Columns(108).StyleSet=   "stagefield"
      Columns(109).Width=   2910
      Columns(109).Caption=   "MD5_Done"
      Columns(109).Name=   "MD5_Done"
      Columns(109).DataField=   "MD5_Done"
      Columns(109).FieldLen=   256
      Columns(109).Style=   1
      Columns(109).StyleSet=   "stagefield"
      Columns(110).Width=   2117
      Columns(110).Caption=   "Meta Creation"
      Columns(110).Name=   "MetadataCreation"
      Columns(110).DataField=   "MetadataCreation"
      Columns(110).FieldLen=   256
      Columns(110).StyleSet=   "stagefield"
      Columns(111).Width=   2910
      Columns(111).Caption=   "Metadata Completed"
      Columns(111).Name=   "Metadata"
      Columns(111).DataField=   "Metadata"
      Columns(111).FieldLen=   256
      Columns(111).Style=   1
      Columns(111).StyleSet=   "stagefield"
      Columns(112).Width=   2117
      Columns(112).Caption=   "Chap Known"
      Columns(112).Name=   "ChaptersKnown"
      Columns(112).DataField=   "ChaptersKnown"
      Columns(112).FieldLen=   256
      Columns(112).StyleSet=   "stagefield"
      Columns(113).Width=   2117
      Columns(113).Caption=   "Chap Unknown"
      Columns(113).Name=   "ChaptersUnknown"
      Columns(113).DataField=   "ChaptersUnknown"
      Columns(113).FieldLen=   256
      Columns(113).StyleSet=   "stagefield"
      Columns(114).Width=   2910
      Columns(114).Caption=   "Chapters Completed"
      Columns(114).Name=   "Chapters"
      Columns(114).DataField=   "Chapters"
      Columns(114).FieldLen=   256
      Columns(114).Style=   1
      Columns(114).StyleSet=   "stagefield"
      Columns(115).Width=   2117
      Columns(115).Caption=   "Artwork Fixes"
      Columns(115).Name=   "ArtworkFixes"
      Columns(115).DataField=   "ArtworkFixes"
      Columns(115).FieldLen=   256
      Columns(115).StyleSet=   "stagefield"
      Columns(116).Width=   2910
      Columns(116).Caption=   "Artwork Completed"
      Columns(116).Name=   "Artwork"
      Columns(116).DataField=   "Artwork"
      Columns(116).FieldLen=   256
      Columns(116).Style=   1
      Columns(116).StyleSet=   "stagefield"
      Columns(117).Width=   979
      Columns(117).Caption=   "Ready"
      Columns(117).Name=   "readyforTX"
      Columns(117).DataField=   "ReadyToPackage"
      Columns(117).FieldLen=   256
      Columns(117).Style=   2
      Columns(117).StyleSet=   "conclusionfield"
      Columns(118).Width=   2910
      Columns(118).Caption=   "Package Made"
      Columns(118).Name=   "Package"
      Columns(118).DataField=   "Package"
      Columns(118).FieldLen=   256
      Columns(118).Style=   1
      Columns(118).StyleSet=   "stagefield"
      Columns(119).Width=   2910
      Columns(119).Caption=   "In The Queue"
      Columns(119).Name=   "InTheQueue"
      Columns(119).DataField=   "InTheQueue"
      Columns(119).FieldLen=   256
      Columns(119).Style=   1
      Columns(119).StyleSet=   "stagefield"
      Columns(120).Width=   2910
      Columns(120).Caption=   "Delivered To MDR"
      Columns(120).Name=   "DeliverToMDR"
      Columns(120).DataField=   "DeliverToMDR"
      Columns(120).FieldLen=   256
      Columns(120).Style=   1
      Columns(120).StyleSet=   "stagefield"
      Columns(121).Width=   2910
      Columns(121).Caption=   "Delivered To Modern"
      Columns(121).Name=   "DeliverToModern"
      Columns(121).DataField=   "DeliverToModern"
      Columns(121).FieldLen=   256
      Columns(121).Style=   1
      Columns(121).StyleSet=   "stagefield"
      Columns(122).Width=   2910
      Columns(122).Caption=   "Sent To Platform"
      Columns(122).Name=   "SentToiTunes"
      Columns(122).DataField=   "SentToiTunes"
      Columns(122).FieldLen=   256
      Columns(122).Style=   1
      Columns(122).StyleSet=   "stagefield"
      Columns(123).Width=   2910
      Columns(123).Caption=   "Transporter Logs Sent"
      Columns(123).Name=   "TransporterLogsSent"
      Columns(123).DataField=   "TransporterLogsSent"
      Columns(123).FieldLen=   256
      Columns(123).Style=   1
      Columns(123).StyleSet=   "stagefield"
      Columns(124).Width=   3200
      Columns(124).Visible=   0   'False
      Columns(124).Caption=   "Billed"
      Columns(124).Name=   "billed"
      Columns(124).DataField=   "billed"
      Columns(124).FieldLen=   256
      Columns(124).Style=   2
      Columns(124).StyleSet=   "conclusionfield"
      Columns(125).Width=   3360
      Columns(125).Caption=   "Audio Here"
      Columns(125).Name=   "DisneyPlus_OVHere"
      Columns(125).DataField=   "DisneyPlus_OVHere"
      Columns(125).FieldLen=   256
      Columns(125).Style=   1
      Columns(125).StyleSet=   "stagefield"
      Columns(126).Width=   3360
      Columns(126).Caption=   "Proxy Here"
      Columns(126).Name=   "DisneyPlus_ProxyHere"
      Columns(126).DataField=   "DisneyPlus_ProxyHere"
      Columns(126).FieldLen=   256
      Columns(126).Style=   1
      Columns(126).StyleSet=   "stagefield"
      Columns(127).Width=   3360
      Columns(127).Caption=   "Dubcard Doc Here"
      Columns(127).Name=   "DisneyPlus_Docs"
      Columns(127).DataField=   "DisneyPlus_Docs"
      Columns(127).FieldLen=   256
      Columns(127).Style=   1
      Columns(127).StyleSet=   "stagefield"
      Columns(128).Width=   3360
      Columns(128).Caption=   "FN Subs Here"
      Columns(128).Name=   "DisneyPlus_FNSubsHere"
      Columns(128).DataField=   "DisneyPlus_FNSubsHere"
      Columns(128).FieldLen=   256
      Columns(128).Style=   1
      Columns(128).StyleSet=   "stagefield"
      Columns(129).Width=   3360
      Columns(129).Caption=   "Dub Card Done"
      Columns(129).Name=   "DisneyPlus_Dubcard"
      Columns(129).DataField=   "DisneyPlus_Dubcard"
      Columns(129).FieldLen=   256
      Columns(129).Style=   1
      Columns(129).StyleSet=   "stagefield"
      Columns(130).Width=   3360
      Columns(130).Caption=   "Audio Done"
      Columns(130).Name=   "DisneyPlus_AudioDone"
      Columns(130).DataField=   "DisneyPlus_AudioDone"
      Columns(130).FieldLen=   256
      Columns(130).Style=   1
      Columns(130).StyleSet=   "stagefield"
      Columns(131).Width=   3360
      Columns(131).Caption=   "Forced Narratives"
      Columns(131).Name=   "DisneyPlus_ForcedNarratives"
      Columns(131).DataField=   "DisneyPlus_ForcedNarratives"
      Columns(131).FieldLen=   256
      Columns(131).Style=   1
      Columns(131).StyleSet=   "stagefield"
      Columns(132).Width=   3360
      Columns(132).Caption=   "Subtitles"
      Columns(132).Name=   "DisneyPlus_Subtitles"
      Columns(132).DataField=   "DisneyPlus_Subtitles"
      Columns(132).FieldLen=   256
      Columns(132).Style=   1
      Columns(132).StyleSet=   "stagefield"
      Columns(133).Width=   3360
      Columns(133).Caption=   "Ready to Send"
      Columns(133).Name=   "DisneyPlus_Ready"
      Columns(133).DataField=   "DisneyPlus_Ready"
      Columns(133).FieldLen=   256
      Columns(133).Style=   1
      Columns(133).StyleSet=   "stagefield"
      Columns(134).Width=   3360
      Columns(134).Caption=   "Sent to Jellyroll"
      Columns(134).Name=   "DisneyPlus_Sent"
      Columns(134).DataField=   "DisneyPlus_Sent"
      Columns(134).FieldLen=   256
      Columns(134).Style=   1
      Columns(134).StyleSet=   "stagefield"
      Columns(135).Width=   873
      Columns(135).Caption=   "Done"
      Columns(135).Name=   "readytobill"
      Columns(135).DataField=   "readytobill"
      Columns(135).FieldLen=   256
      Columns(135).Locked=   -1  'True
      Columns(135).Style=   2
      Columns(135).StyleSet=   "conclusionfield"
      Columns(136).Width=   979
      Columns(136).Caption=   "Faulty"
      Columns(136).Name=   "complainedabout"
      Columns(136).DataField=   "complainedabout"
      Columns(136).FieldLen=   256
      Columns(136).Style=   2
      Columns(136).StyleSet=   "conclusionfield"
      Columns(137).Width=   979
      Columns(137).Caption=   "Redo"
      Columns(137).Name=   "complaintredoitem"
      Columns(137).DataField=   "complaintredoitem"
      Columns(137).FieldLen=   256
      Columns(137).Style=   2
      Columns(137).StyleSet=   "conclusionfield"
      Columns(138).Width=   2910
      Columns(138).Caption=   "cdate"
      Columns(138).Name=   "cdate"
      Columns(138).DataField=   "cdate"
      Columns(138).FieldLen=   256
      Columns(138).StyleSet=   "conclusionfield"
      Columns(139).Width=   2831
      Columns(139).Caption=   "mdate"
      Columns(139).Name=   "mdate"
      Columns(139).DataField=   "mdate"
      Columns(139).FieldLen=   256
      Columns(139).StyleSet=   "conclusionfield"
      Columns(140).Width=   1773
      Columns(140).Caption=   "Package ID"
      Columns(140).Name=   "iTunes_PackageID"
      Columns(140).DataField=   "iTunes_PackageID"
      Columns(140).FieldLen=   256
      Columns(141).Width=   1773
      Columns(141).Caption=   "Master ClipID"
      Columns(141).Name=   "iTunes_Master_eventID"
      Columns(141).DataField=   "iTunes_Master_eventID"
      Columns(141).FieldLen=   256
      Columns(141).Style=   1
      Columns(141).StyleSet=   "headerfield"
      Columns(142).Width=   2990
      Columns(142).Caption=   "Ref"
      Columns(142).Name=   "itemreference"
      Columns(142).DataField=   "itemreference"
      Columns(142).FieldLen=   256
      Columns(142).StyleSet=   "headerfield"
      Columns(143).Width=   4577
      Columns(143).Caption=   "EIDR"
      Columns(143).Name=   "EIDR"
      Columns(143).DataField=   "EIDR"
      Columns(143).FieldLen=   256
      Columns(143).StyleSet=   "headerfield"
      Columns(144).Width=   714
      Columns(144).Caption=   "Dur"
      Columns(144).Name=   "duration"
      Columns(144).DataField=   "duration"
      Columns(144).FieldLen=   256
      Columns(144).StyleSet=   "headerfield"
      Columns(145).Width=   1508
      Columns(145).Caption=   "Length"
      Columns(145).Name=   "timecodeduration"
      Columns(145).DataField=   "timecodeduration"
      Columns(145).FieldLen=   256
      Columns(145).StyleSet=   "headerfield"
      Columns(146).Width=   714
      Columns(146).Caption=   "GB Sent"
      Columns(146).Name=   "gbsent"
      Columns(146).DataField=   "gbsent"
      Columns(146).FieldLen=   256
      Columns(146).StyleSet=   "headerfield"
      Columns(147).Width=   1773
      Columns(147).Caption=   "Trailer1 ClipID"
      Columns(147).Name=   "iTunes_Trailer1_eventID"
      Columns(147).DataField=   "iTunes_Trailer1_eventID"
      Columns(147).FieldLen=   256
      Columns(148).Width=   1773
      Columns(148).Caption=   "Trailer2 ClipID"
      Columns(148).Name=   "iTunes_Trailer2_eventID"
      Columns(148).DataField=   "iTunes_Trailer2_eventID"
      Columns(148).FieldLen=   256
      Columns(149).Width=   1773
      Columns(149).Caption=   "Trailer3 ClipID"
      Columns(149).Name=   "iTunes_Trailer3_eventID"
      Columns(149).DataField=   "iTunes_Trailer3_eventID"
      Columns(149).FieldLen=   256
      Columns(150).Width=   1773
      Columns(150).Caption=   "Trailer4 ClipID"
      Columns(150).Name=   "iTunes_Trailer4_eventID"
      Columns(150).DataField=   "iTunes_Trailer4_eventID"
      Columns(150).FieldLen=   256
      Columns(151).Width=   3200
      Columns(151).Visible=   0   'False
      Columns(151).Caption=   "ProjectManagerContactID"
      Columns(151).Name=   "ProjectManagerContactID"
      Columns(151).DataField=   "ProjectManagerContactID"
      Columns(151).FieldLen=   256
      Columns(152).Width=   3200
      Columns(152).Visible=   0   'False
      Columns(152).Caption=   "ReviewDuration"
      Columns(152).Name=   "ReviewDuration"
      Columns(152).DataField=   "ReviewDuration"
      Columns(152).FieldLen=   256
      Columns(153).Width=   3200
      Columns(153).Visible=   0   'False
      Columns(153).Caption=   "dat_MastersHere"
      Columns(153).Name=   "dat_MastersHere"
      Columns(153).DataField=   "dat_MastersHere"
      Columns(153).FieldLen=   256
      Columns(154).Width=   3200
      Columns(154).Visible=   0   'False
      Columns(154).Caption=   "dat_DeliverToMDR"
      Columns(154).Name=   "dat_DeliverToMDR"
      Columns(154).DataField=   "dat_DeliverToMDR"
      Columns(154).FieldLen=   256
      _ExtentX        =   49847
      _ExtentY        =   9657
      _StockProps     =   79
      Caption         =   "iTunes Tracker Items"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoComments 
      Height          =   330
      Left            =   16140
      Top             =   12660
      Visible         =   0   'False
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdComments 
      Bindings        =   "frmTrackeriTunes.frx":00A1
      Height          =   1875
      Left            =   120
      TabIndex        =   21
      Top             =   12720
      Width           =   15255
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12582847
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   9
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "trackerhistoryID"
      Columns(0).Name =   "tracker_commentID"
      Columns(0).DataField=   "tracker_commentID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "trackerprogramID"
      Columns(1).Name =   "tracker_iTunes_itemID"
      Columns(1).DataField=   "tracker_iTunes_itemID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   13070
      Columns(2).Caption=   "Comments"
      Columns(2).Name =   "comment"
      Columns(2).DataField=   "comment"
      Columns(2).FieldLen=   255
      Columns(3).Width=   3200
      Columns(3).Caption=   "Comment Type"
      Columns(3).Name =   "commenttypedecoded"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3360
      Columns(4).Caption=   "Date"
      Columns(4).Name =   "cdate"
      Columns(4).DataField=   "cdate"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   3519
      Columns(5).Caption=   "Entered By"
      Columns(5).Name =   "cuser"
      Columns(5).DataField=   "cuser"
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   1535
      Columns(6).Caption=   "Resolved?"
      Columns(6).Name =   "issueresolved"
      Columns(6).DataField=   "issueresolved"
      Columns(6).FieldLen=   256
      Columns(6).Style=   2
      Columns(7).Width=   1138
      Columns(7).Caption=   "Email?"
      Columns(7).Name =   "Email?"
      Columns(7).DataField=   "Column 5"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Style=   4
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "commenttype"
      Columns(8).Name =   "commenttype"
      Columns(8).DataField=   "commenttype"
      Columns(8).FieldLen=   256
      UseDefaults     =   0   'False
      _ExtentX        =   26908
      _ExtentY        =   3307
      _StockProps     =   79
      Caption         =   "Tracker Comments"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoCompanies 
      Height          =   330
      Left            =   16140
      Top             =   13020
      Visible         =   0   'False
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   1500
      TabIndex        =   54
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The company this job is for"
      Top             =   60
      Width           =   2175
      DataFieldList   =   "portalcompanyname"
      MaxDropDownItems=   35
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "portalcompanyname"
      Columns(0).DataField=   "portalcompanyname"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "fullname"
      Columns(2).Name =   "fullname"
      Columns(2).DataField=   "fullname"
      Columns(2).FieldLen=   256
      _ExtentX        =   3836
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "portalcompanyname"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnUsers 
      Height          =   855
      Left            =   18600
      TabIndex        =   80
      Top             =   13620
      Width           =   2175
      DataFieldList   =   "column 0"
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).Caption=   "value"
      Columns(0).Name =   "value"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   3836
      _ExtentY        =   1508
      _StockProps     =   77
      DataFieldToDisplay=   "column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnDigitalDeliveryPlatform 
      Height          =   855
      Left            =   21000
      TabIndex        =   90
      Top             =   13620
      Width           =   2175
      DataFieldList   =   "column 0"
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).Caption=   "value"
      Columns(0).Name =   "value"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   3836
      _ExtentY        =   1508
      _StockProps     =   77
      DataFieldToDisplay=   "column 0"
   End
   Begin MSAdodcLib.Adodc adoDubCards 
      Height          =   330
      Left            =   18120
      Top             =   11280
      Visible         =   0   'False
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoDubCards"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDubcards 
      Bindings        =   "frmTrackeriTunes.frx":00BB
      Height          =   1875
      Left            =   14040
      TabIndex        =   101
      Top             =   10680
      Visible         =   0   'False
      Width           =   12975
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12582847
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "tracker_DubCard_ID"
      Columns(0).Name =   "tracker_DubCard_ID"
      Columns(0).DataField=   "tracker_DubCard_ID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "tracker_itemID"
      Columns(1).Name =   "tracker_itemID"
      Columns(1).DataField=   "tracker_itemID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   5292
      Columns(2).Caption=   "Language"
      Columns(2).Name =   "Language"
      Columns(2).DataField=   "Language"
      Columns(2).FieldLen=   255
      Columns(2).Locked=   -1  'True
      Columns(2).Style=   1
      UseDefaults     =   0   'False
      _ExtentX        =   22886
      _ExtentY        =   3307
      _StockProps     =   79
      Caption         =   "Dub Cards"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnFramerate 
      Height          =   855
      Left            =   23340
      TabIndex        =   115
      Top             =   13620
      Width           =   2175
      DataFieldList   =   "column 0"
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).Caption=   "value"
      Columns(0).Name =   "value"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   3836
      _ExtentY        =   1508
      _StockProps     =   77
      DataFieldToDisplay=   "column 0"
   End
   Begin VB.Label lblCaption 
      Caption         =   "Language"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   8
      Left            =   14400
      TabIndex        =   109
      Top             =   1920
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Project"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   7
      Left            =   14400
      TabIndex        =   108
      Top             =   120
      Width           =   1515
   End
   Begin VB.Label lblCaption 
      Caption         =   "Alpha Code"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   14400
      TabIndex        =   86
      Top             =   3000
      Width           =   1575
   End
   Begin VB.Label lblCaption 
      Caption         =   "iTunes Vendor ID"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   14400
      TabIndex        =   59
      Top             =   2640
      Width           =   1575
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order Number"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   14400
      TabIndex        =   58
      Top             =   2280
      Width           =   1455
   End
   Begin VB.Label lblSearchCompanyID 
      Height          =   315
      Left            =   20160
      TabIndex        =   53
      Top             =   2100
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label2 
      Caption         =   "All other fields: Use the button to put the date done, and add ER to the end to reject item"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   9
      Left            =   21660
      TabIndex        =   52
      Top             =   3000
      Width           =   6675
   End
   Begin VB.Label Label2 
      Caption         =   "Artwork Fixes: time taken to do the fixes in minutes"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   8
      Left            =   21660
      TabIndex        =   51
      Top             =   2700
      Width           =   6675
   End
   Begin VB.Label Label2 
      Caption         =   "Chap Known: Number of Chapters Grabbed from Known Information"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   7
      Left            =   21660
      TabIndex        =   50
      Top             =   2100
      Width           =   6675
   End
   Begin VB.Label Label2 
      Caption         =   "Meta Creation: time taken making the Metadata in minutes"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   6
      Left            =   21660
      TabIndex        =   49
      Top             =   1800
      Width           =   6675
   End
   Begin VB.Label Label2 
      Caption         =   "Feat Subs Simple Fix: number of subtitle files conformed"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   5
      Left            =   21660
      TabIndex        =   48
      Top             =   1500
      Width           =   6675
   End
   Begin VB.Label Label2 
      Caption         =   "time taken in minutes to do the fixes in minutes"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   4
      Left            =   21660
      TabIndex        =   47
      Top             =   1200
      Width           =   6675
   End
   Begin VB.Label Label2 
      Caption         =   "Feat Vid Fixes, Feat Aud Fixes, Feat Subs Comp Fix, Feat Subs Form Conv, Trail Vid Fixes, Trail Aud Fixes: "
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   3
      Left            =   21660
      TabIndex        =   46
      Top             =   900
      Width           =   7815
   End
   Begin VB.Label Label2 
      Caption         =   "Chap Unknown: Number of Chapters grabbed from Unknown Information"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   2
      Left            =   21660
      TabIndex        =   45
      Top             =   2400
      Width           =   6675
   End
   Begin VB.Label Label2 
      Caption         =   "Feat Subs Ingest: Number of Subtitle Files Ingested"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   1
      Left            =   21660
      TabIndex        =   44
      Top             =   600
      Width           =   6675
   End
   Begin VB.Label Label2 
      Caption         =   "Feat Aud Transcode: Number of extra Audio Files transcoded"
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   0
      Left            =   21660
      TabIndex        =   43
      Top             =   300
      Width           =   6675
   End
   Begin VB.Label Label1 
      Caption         =   "Workflow Field Instructions:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   21660
      TabIndex        =   42
      Top             =   0
      Width           =   6675
   End
   Begin VB.Label lblLastTrackerItemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   20160
      TabIndex        =   24
      Top             =   1560
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   10
      Left            =   180
      TabIndex        =   22
      Top             =   120
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Series"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   14400
      TabIndex        =   17
      Top             =   1200
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Episode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   14400
      TabIndex        =   16
      Top             =   1560
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Ep. Title"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   14400
      TabIndex        =   12
      Top             =   840
      Width           =   1515
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   14400
      TabIndex        =   11
      Top             =   480
      Width           =   1515
   End
   Begin VB.Label lblTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   18960
      TabIndex        =   9
      Top             =   1560
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCompanyID 
      Height          =   315
      Left            =   19260
      TabIndex        =   8
      Top             =   2100
      Visible         =   0   'False
      Width           =   735
   End
End
Attribute VB_Name = "frmTrackeriTunes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strSearch As String
Dim m_strOrderby As String
Dim m_blnDelete As Boolean
Dim m_blnBilling As Boolean
Dim m_blnSilent As Boolean

Private Sub HideAllColumns()

'Headers
grdItems.Columns("iTunesVendorID").Visible = False
grdItems.Columns("operator").Visible = False
grdItems.Columns("ordernumber").Visible = False
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("Project").Visible = False
grdItems.Columns("Priority").Visible = True
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = False
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = False
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = False
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = False
grdItems.Columns("Subslanguage2").Visible = False
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = False
grdItems.Columns("trailerhere").Visible = False
grdItems.Columns("artworkhere").Visible = False
grdItems.Columns("subshere").Visible = False
grdItems.Columns("metadatahere").Visible = False
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = False
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = False
grdItems.Columns("sub1").Visible = False
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("MetadataCreation").Visible = False
grdItems.Columns("metadata").Visible = False
grdItems.Columns("MD5_Done").Visible = False
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("SentToiTunes").Visible = False
grdItems.Columns("TransporterLogsSent").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = False

'Tail
grdItems.Columns("iTunes_packageID").Visible = False
grdItems.Columns("cdate").Visible = False
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
'grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False


grdItems.Columns("Rejected").Visible = False
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False

End Sub

Private Sub chkHideDemo_Click()

Dim l_strSQL As String

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT portalcompanyname, companyID FROM company WHERE cetaclientcode like '%/iTunestracker%' and companyID > 100 AND system_active = 1 ORDER BY name;"
Else
    l_strSQL = "SELECT portalcompanyname, companyID FROM company WHERE cetaclientcode like '%/iTunestracker%' AND system_active = 1 ORDER BY name;"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset
Dim l_rstSearch2 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset
Set l_rstSearch2 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

With l_rstSearch2
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch2.ActiveConnection = Nothing

Set ddnChannel.DataSource = l_rstSearch2
grdItems.Columns("channel").DropDownHwnd = ddnChannel.hWnd

l_conSearch.Close
Set l_conSearch = Nothing

cmdSearch.Value = True

End Sub

Private Sub chkHideEpStuff_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

End Sub

Private Sub cmbCompany_Click()

lblSearchCompanyID.Caption = cmbCompany.Columns("companyID").Text
m_strSearch = " WHERE companyID = " & lblSearchCompanyID.Caption
'm_strOrderby = " ORDER BY headertext1, headerint1, headerint2, headerint3, headerint4, headerint5"

'HideAllColumns

'DoEvents

Dim l_rstChoices As ADODB.Recordset, l_blnWide As Boolean, l_strSQL As String

If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
    l_blnWide = True
Else
    l_blnWide = False
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch1 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch1 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

l_strSQL = "SELECT name FROM contact WHERE contactID IN (SELECT contactID FROM employee WHERE companyID = " & Val(lblSearchCompanyID.Caption) & ") ORDER BY name;"

With l_rstSearch1
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

Set ddnProjectManager.DataSource = l_rstSearch1

l_rstSearch1.ActiveConnection = Nothing

grdItems.Columns("projectmanager").DropDownHwnd = ddnProjectManager.hWnd

l_conSearch.Close
Set l_conSearch = Nothing

DoEvents
If InStr(GetData("company", "cetaclientcode", "companyID", lblSearchCompanyID.Caption), "/iTunesNoEpisode") > 0 Then
    chkHideEpStuff.Value = 1
    DoEvents
'    grdItems.Columns("Series").Visible = False
'    grdItems.Columns("Volume").Visible = False
'    grdItems.Columns("Episode").Visible = False
'    grdItems.Columns("EpisodeTitle").Visible = False
Else
    chkHideEpStuff.Value = 0
    DoEvents
'    grdItems.Columns("Series").Visible = True
'    grdItems.Columns("Volume").Visible = True
'    grdItems.Columns("Episode").Visible = True
'    grdItems.Columns("EpisodeTitle").Visible = True
End If

cmdSearch.Value = True
'cmdFieldsSummary.Value = True

End Sub

Private Sub cmbProject_GotFocus()
PopulateLocalCombo cmbProject, False, "Project"
End Sub

Private Sub cmdCopyOverwriteAllDubcards_Click()

Dim l_rsSourceData As ADODB.Recordset, l_rsCopyData As ADODB.Recordset
Dim l_lngTrackerID As Long
Dim l_strLanguage As String
Dim SectionNumber As Long
Dim SectionPart As Long
Dim SectionName As String
Dim l_strSQL As String

If txtLanguage.Text = "" Then
    MsgBox "A Language must have been chosen for using this button.", vbCritical, "Problem"
    Exit Sub
End If

If lblTrackeritemID.Caption = "" Or adoItems.Recordset.RecordCount = 0 Then
    MsgBox "You need to have a Grid of items, and be on the source row, to use this button.", vbCritical, "Problem"
    Exit Sub
End If

If MsgBox("You are about to overwrite all Dubcards in this grid with the information on the row you are on." & vbCrLf & "Are you sure?", vbYesNo + vbDefaultButton2, "Dubcard Copy/Overwrite") = vbNo Then
    Exit Sub
End If

l_lngTrackerID = Val(lblTrackeritemID.Caption)
l_strLanguage = txtLanguage.Text

adoItems.Recordset.MoveFirst

Do While Not adoItems.Recordset.EOF
    If adoItems.Recordset("tracker_iTunes_itemID") <> l_lngTrackerID Then
        For SectionNumber = 1 To 3
            For SectionPart = 1 To 3
                Select Case SectionPart
                    Case 1
                        SectionName = "head"
                    Case 2
                        SectionName = "left"
                    Case 3
                        SectionName = "right"
                End Select
                l_strSQL = "SELECT * from Tracker_Dubcard WHERE tracker_itemID = " & l_lngTrackerID & " AND Language = '" & l_strLanguage & "' AND SectionNumber = " & SectionNumber & " AND SectionPart = '" & SectionName & "';"
                Debug.Print l_strSQL
                Set l_rsSourceData = ExecuteSQL(l_strSQL, g_strExecuteError)
                If Not (l_rsSourceData Is Nothing) Then
                    If l_rsSourceData.RecordCount = 1 Then
                        l_strSQL = "SELECT * FROM Tracker_Dubcard WHERE tracker_itemID = " & adoItems.Recordset("Tracker_itunes_itemID") & " AND Language = '" & l_strLanguage & "' AND SectionNumber = " & SectionNumber & " AND SectionPart = '" & SectionName & "';"
                        Debug.Print l_strSQL
                        Set l_rsCopyData = ExecuteSQL(l_strSQL, g_strExecuteError)
                        If l_rsCopyData.RecordCount = 0 Then
                            l_strSQL = "INSERT INTO Tracker_Dubcard(tracker_itemID, Language, SectionNumber, Sectionpart, TextData, cdate, cuser, mdate, muser) VALUES ("
                            l_strSQL = l_strSQL & adoItems.Recordset("tracker_itunes_itemID") & ", "
                            l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
                            l_strSQL = l_strSQL & SectionNumber & ", "
                            l_strSQL = l_strSQL & "'" & SectionName & "', "
                            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rsSourceData("TextData")) & "', "
                            l_strSQL = l_strSQL & "'" & Format(l_rsSourceData("cdate"), "YYYY-MM-DD HH:NN:SS") & "', "
                            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rsSourceData("cuser")) & "', "
                            l_strSQL = l_strSQL & "'" & Format(l_rsSourceData("mdate"), "YYYY-MM-DD HH:NN:SS") & "', "
                            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rsSourceData("muser")) & "') "
                            Debug.Print l_strSQL
                            ExecuteSQL l_strSQL, g_strExecuteError
                            CheckForSQLError
                        Else
                            l_rsCopyData("TextData") = l_rsSourceData("TextData")
                            l_rsCopyData("cdate") = l_rsSourceData("cdate")
                            l_rsCopyData("cuser") = l_rsSourceData("cuser")
                            l_rsCopyData("mdate") = l_rsSourceData("mdate")
                            l_rsCopyData("muser") = l_rsSourceData("muser")
                            l_rsCopyData.Update
                            l_rsCopyData.Close
                        End If
                    End If
                    l_rsSourceData.Close
                End If
            Next
        Next
    End If
    adoItems.Recordset.MoveNext
Loop

End Sub

Private Sub cmdFieldsDisneyPlus_Click()

'Ep Stuff
grdItems.Columns("Series").Visible = False
grdItems.Columns("Volume").Visible = False
grdItems.Columns("Episode").Visible = True
grdItems.Columns("EpisodeTitle").Visible = False

'Headers
grdItems.Columns("iTunesVendorID").Visible = False
grdItems.Columns("rejected").Visible = True
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("MostRecentRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("iTunesOrderType").Visible = False
grdItems.Columns("iTunesSubmissionType").Visible = False
grdItems.Columns("operator").Visible = False
grdItems.Columns("ordernumber").Visible = True
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("Project").Visible = True
grdItems.Columns("Priority").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = True
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = False
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = False
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = False
grdItems.Columns("Subslanguage2").Visible = False
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = True
grdItems.Columns("DisneyPlus_Framerate").Visible = True
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("Due date").Visible = True
grdItems.Columns("workable").Visible = False
grdItems.Columns("targetdate").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("targetdatecountdown").Visible = False
grdItems.Columns("assetshere").Visible = False
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = False
grdItems.Columns("trailerhere").Visible = False
grdItems.Columns("audiohere").Visible = False
grdItems.Columns("artworkhere").Visible = False
grdItems.Columns("subshere").Visible = False
grdItems.Columns("metadatahere").Visible = False
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = False
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = False
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = False
grdItems.Columns("sub1").Visible = False
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("MetadataCreation").Visible = False
grdItems.Columns("metadata").Visible = False
grdItems.Columns("MD5_Done").Visible = False
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("SentToiTunes").Visible = False
grdItems.Columns("TransporterLogsSent").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = True
grdItems.Columns("DisneyPlus_ProxyHere").Visible = True
grdItems.Columns("DisneyPlus_Docs").Visible = True
grdItems.Columns("DisneyPlus_DubCard").Visible = True
grdItems.Columns("DisneyPlus_AudioDone").Visible = True
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = True
grdItems.Columns("DisneyPlus_Subtitles").Visible = True
grdItems.Columns("DisneyPlus_Ready").Visible = True
grdItems.Columns("DisneyPlus_Sent").Visible = True
grdItems.Columns("readyforTX").Visible = False

'Tails
grdItems.Columns("iTunes_packageID").Visible = False
grdItems.Columns("cdate").Visible = False
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False

End Sub

Private Sub cmdAmazonFields_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = False
grdItems.Columns("rejected").Visible = True
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("MostRecentRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("iTunesOrderType").Visible = True
grdItems.Columns("iTunesSubmissionType").Visible = False
grdItems.Columns("operator").Visible = False
grdItems.Columns("ordernumber").Visible = True
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = True
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = False
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = True
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = False
grdItems.Columns("Subslanguage2").Visible = False
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("DisneyPlus_Framerate").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("Due date").Visible = True
grdItems.Columns("workable").Visible = False
grdItems.Columns("targetdate").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("targetdatecountdown").Visible = False
grdItems.Columns("assetshere").Visible = True
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = True
grdItems.Columns("trailerhere").Visible = False
grdItems.Columns("audiohere").Visible = False
grdItems.Columns("artworkhere").Visible = True
grdItems.Columns("subshere").Visible = False
grdItems.Columns("metadatahere").Visible = True
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = False
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = False
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = False
grdItems.Columns("sub1").Visible = False
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("MetadataCreation").Visible = False
grdItems.Columns("metadata").Visible = True
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("SentToiTunes").Visible = True
grdItems.Columns("TransporterLogsSent").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = False

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = True
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False


End Sub

Private Sub cmdAmazonMLFFields_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = False
grdItems.Columns("rejected").Visible = True
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("MostRecentRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("iTunesOrderType").Visible = True
grdItems.Columns("iTunesSubmissionType").Visible = False
grdItems.Columns("operator").Visible = False
grdItems.Columns("ordernumber").Visible = True
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = True
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = False
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = True
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = True
grdItems.Columns("Subslanguage2").Visible = True
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("DisneyPlus_Framerate").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("Due date").Visible = True
grdItems.Columns("workable").Visible = False
grdItems.Columns("targetdate").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("targetdatecountdown").Visible = False
grdItems.Columns("assetshere").Visible = True
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = True
grdItems.Columns("trailerhere").Visible = False
grdItems.Columns("audiohere").Visible = False
grdItems.Columns("artworkhere").Visible = True
grdItems.Columns("subshere").Visible = True
grdItems.Columns("metadatahere").Visible = True
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = False
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = False
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = False
grdItems.Columns("sub1").Visible = True
grdItems.Columns("sub2").Visible = True
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("MetadataCreation").Visible = False
grdItems.Columns("metadata").Visible = True
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("SentToiTunes").Visible = True
grdItems.Columns("TransporterLogsSent").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = False

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = True
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False


End Sub

Private Sub cmdBillAll_Click()

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then
    
    Do While Not adoItems.Recordset.EOF
        If cmdBillItem.Visible = True Then
            cmdBillItem.Value = True
        End If
        adoItems.Recordset.MoveNext
        DoEvents
    Loop
End If

End Sub

Private Sub cmdBillItem_Click()

If Not BillItemPartOne Then

    If Not BillItemPartTwo Then
    
        BillItemPartThree
    
    Else
    
        MsgBox "Problem Billing This line"
    
    End If
    
Else

    MsgBox "Problem Billing This line"

End If

End Sub

Private Sub cmdClear_Click()

ClearFields Me
cmbCompany.Text = ""
lblSearchCompanyID.Caption = ""
m_strSearch = " WHERE 1 = 1 "
cmdSearch.Value = True

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdExportGrid_Click()

If grdItems.Rows < 1 Then Exit Sub

Dim l_strFilePath As String
l_strFilePath = fGetSpecialFolder(CSIDL_DOCUMENTS)

grdItems.Export ssExportTypeText, ssExportAllRows + ssExportFieldNames + ssExportHiddenColumns, l_strFilePath & "iTunes_Tracker.txt"
MsgBox "Exported to a text file in 'My Documents' called 'iTunes_Tracker.txt'", vbInformation

End Sub

Private Sub cmdFieldsAll_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = True
grdItems.Columns("channel").Visible = True
grdItems.Columns("Rejected").Visible = True
grdItems.Columns("Rejected Date").Visible = True
grdItems.Columns("OffRejectedDate").Visible = True
grdItems.Columns("MostRecentRejectedDate").Visible = True
grdItems.Columns("DaysOnRejected").Visible = True
grdItems.Columns("FixedJobID").Visible = True
grdItems.Columns("operator").Visible = True
grdItems.Columns("iTunesOrderType").Visible = True
grdItems.Columns("iTunesSubmissionType").Visible = True
grdItems.Columns("iTunesVendorID").Visible = True
grdItems.Columns("ordernumber").Visible = True
grdItems.Columns("projectmanager").Visible = True
grdItems.Columns("AlphaDisplayCode").Visible = True
grdItems.Columns("Project").Visible = True
grdItems.Columns("FeatureAudioSpec").Visible = True
grdItems.Columns("FeatureLanguage1").Visible = True
grdItems.Columns("FeatureLanguage2").Visible = True
grdItems.Columns("FeatureLanguage3").Visible = True
grdItems.Columns("FeatureLanguage4").Visible = True
grdItems.Columns("TrailerAudioSpec").Visible = True
grdItems.Columns("trailerbis").Visible = True
grdItems.Columns("trailercreate").Visible = True
grdItems.Columns("Trailerlanguage1").Visible = True
grdItems.Columns("Trailerlanguage2").Visible = True
grdItems.Columns("Trailerlanguage3").Visible = True
grdItems.Columns("Trailerlanguage4").Visible = True
grdItems.Columns("Artworklanguage1").Visible = True
grdItems.Columns("Artworklanguage2").Visible = True
grdItems.Columns("Artworklanguage3").Visible = True
grdItems.Columns("Artworklanguage4").Visible = True
grdItems.Columns("Subslanguage1").Visible = True
grdItems.Columns("Subslanguage2").Visible = True
grdItems.Columns("Subslanguage3").Visible = True
grdItems.Columns("Subslanguage4").Visible = True
grdItems.Columns("featureHD").Visible = True
grdItems.Columns("DisneyPlus_Framerate").Visible = True
grdItems.Columns("trailerHD").Visible = True
grdItems.Columns("chaptering").Visible = True
grdItems.Columns("metadataneeded").Visible = True
grdItems.Columns("Due date").Visible = True
grdItems.Columns("workable").Visible = True
grdItems.Columns("completeable").Visible = True
grdItems.Columns("firstcompleteable").Visible = True
grdItems.Columns("assetshere").Visible = True
grdItems.Columns("TargetDate").Visible = True
grdItems.Columns("TargetDateCountdown").Visible = True
grdItems.Columns("EIDR").Visible = True

'Stagefields
grdItems.Columns("mastershere").Visible = True
grdItems.Columns("trailerhere").Visible = True
grdItems.Columns("artworkhere").Visible = True
grdItems.Columns("subshere").Visible = True
grdItems.Columns("metadatahere").Visible = True
grdItems.Columns("FeatureTranscode").Visible = True
grdItems.Columns("FeatureIngest").Visible = True
grdItems.Columns("FeatureSepAudioTranscode").Visible = True
grdItems.Columns("FeatureReview").Visible = True
grdItems.Columns("FeatureReviewTimeTaken").Visible = True
grdItems.Columns("FeatureReview2Pass").Visible = True
grdItems.Columns("FeatureSubsIngest").Visible = True
grdItems.Columns("FeatureVideoFixes").Visible = True
grdItems.Columns("FeatureAudioFixes").Visible = True
grdItems.Columns("FeatureSubsSimpleFix").Visible = True
grdItems.Columns("FeatureSubsComplexFix").Visible = True
grdItems.Columns("FeatureSubsFormatConvert").Visible = True
grdItems.Columns("TrailerTranscode").Visible = True
grdItems.Columns("TrailerTranscode2").Visible = True
grdItems.Columns("TrailerTranscode3").Visible = True
grdItems.Columns("TrailerTranscode4").Visible = True
grdItems.Columns("TrailerIngest").Visible = True
grdItems.Columns("TrailerIngest2").Visible = True
grdItems.Columns("TrailerIngest3").Visible = True
grdItems.Columns("TrailerIngest4").Visible = True
grdItems.Columns("TrailerClipMade").Visible = True
grdItems.Columns("TrailerClipMade2").Visible = True
grdItems.Columns("TrailerClipMade3").Visible = True
grdItems.Columns("TrailerClipMade4").Visible = True
grdItems.Columns("TrailerReview").Visible = True
grdItems.Columns("TrailerReview2Pass").Visible = True
grdItems.Columns("TrailerReview2").Visible = True
grdItems.Columns("TrailerReview2Pass2").Visible = True
grdItems.Columns("TrailerReview3").Visible = True
grdItems.Columns("TrailerReview2Pass3").Visible = True
grdItems.Columns("TrailerReview4").Visible = True
grdItems.Columns("TrailerReview2Pass4").Visible = True
grdItems.Columns("TrailerSubsBurned").Visible = True
grdItems.Columns("TrailerSubsBurned2").Visible = True
grdItems.Columns("TrailerSubsBurned3").Visible = True
grdItems.Columns("TrailerSubsBurned4").Visible = True
grdItems.Columns("TrailerVideoFixes").Visible = True
grdItems.Columns("TrailerAudioFixes").Visible = True
grdItems.Columns("trailer").Visible = True
grdItems.Columns("sub1").Visible = True
grdItems.Columns("sub2").Visible = True
grdItems.Columns("sub3").Visible = True
grdItems.Columns("sub4").Visible = True
grdItems.Columns("subs").Visible = True
grdItems.Columns("Make Package").Visible = True
grdItems.Columns("MetadataCreation").Visible = True
grdItems.Columns("MD5_Done").Visible = True
grdItems.Columns("metadata").Visible = True
grdItems.Columns("ChaptersKnown").Visible = True
grdItems.Columns("ChaptersUnknown").Visible = True
grdItems.Columns("chapters").Visible = True
grdItems.Columns("ArtworkFixes").Visible = True
grdItems.Columns("artwork").Visible = True
grdItems.Columns("package").Visible = True
grdItems.Columns("InTheQueue").Visible = True
grdItems.Columns("delivertomdr").Visible = True
grdItems.Columns("delivertomodern").Visible = True
grdItems.Columns("DisneyPlus_OVHere").Visible = True
grdItems.Columns("DisneyPlus_ProxyHere").Visible = True
grdItems.Columns("DisneyPlus_Docs").Visible = True
grdItems.Columns("DisneyPlus_DubCard").Visible = True
grdItems.Columns("DisneyPlus_AudioDone").Visible = True
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = True
grdItems.Columns("DisneyPlus_Subtitles").Visible = True
grdItems.Columns("DisneyPlus_Ready").Visible = True
grdItems.Columns("DisneyPlus_Sent").Visible = True
grdItems.Columns("readyforTX").Visible = True

'Tail
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = True
grdItems.Columns("iTunes_Master_eventID").Visible = True
grdItems.Columns("iTunes_Trailer1_eventID").Visible = True
grdItems.Columns("iTunes_Trailer2_eventID").Visible = True
grdItems.Columns("iTunes_Trailer3_eventID").Visible = True
grdItems.Columns("iTunes_Trailer4_eventID").Visible = True
grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = True
grdItems.Columns("timecodeduration").Visible = True
grdItems.Columns("gbsent").Visible = True
grdItems.Columns("ReviewDuration").Visible = True
grdItems.Columns("iTunesOrderType").Visible = True


End Sub

Private Sub cmdFieldsAssets_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = False
grdItems.Columns("ordernumber").Visible = False
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = True
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = False
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = False
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = False
grdItems.Columns("Subslanguage2").Visible = False
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("DisneyPlus_Framerate").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = True
grdItems.Columns("trailerhere").Visible = True
grdItems.Columns("artworkhere").Visible = True
grdItems.Columns("subshere").Visible = True
grdItems.Columns("metadatahere").Visible = True
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = False
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = False
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = False
grdItems.Columns("sub1").Visible = False
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("MetadataCreation").Visible = False
grdItems.Columns("metadata").Visible = False
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = True

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
'grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False

grdItems.Columns("Rejected").Visible = True
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("iTunesOrderType").Visible = False
grdItems.Columns("Due date").Visible = False
grdItems.Columns("workable").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("assetshere").Visible = True
 
Conditional_Assets_Columns

End Sub

Private Sub cmdFieldsFullHeader_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = True
grdItems.Columns("ordernumber").Visible = True
grdItems.Columns("projectmanager").Visible = True
grdItems.Columns("AlphaDisplayCode").Visible = True
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = True
grdItems.Columns("FeatureLanguage1").Visible = True
grdItems.Columns("FeatureLanguage2").Visible = True
grdItems.Columns("FeatureLanguage3").Visible = True
grdItems.Columns("FeatureLanguage4").Visible = True
grdItems.Columns("TrailerAudioSpec").Visible = True
grdItems.Columns("trailerbis").Visible = True
grdItems.Columns("trailercreate").Visible = True
grdItems.Columns("Trailerlanguage1").Visible = True
grdItems.Columns("Trailerlanguage2").Visible = True
grdItems.Columns("Trailerlanguage3").Visible = True
grdItems.Columns("Trailerlanguage4").Visible = True
grdItems.Columns("Artworklanguage1").Visible = True
grdItems.Columns("Artworklanguage2").Visible = True
grdItems.Columns("Artworklanguage3").Visible = True
grdItems.Columns("Artworklanguage4").Visible = True
grdItems.Columns("Subslanguage1").Visible = True
grdItems.Columns("Subslanguage2").Visible = True
grdItems.Columns("Subslanguage3").Visible = True
grdItems.Columns("Subslanguage4").Visible = True
grdItems.Columns("featureHD").Visible = True
grdItems.Columns("DisneyPlus_Framerate").Visible = True
grdItems.Columns("trailerHD").Visible = True
grdItems.Columns("chaptering").Visible = True
grdItems.Columns("metadataneeded").Visible = True
grdItems.Columns("EIDR").Visible = True

'Stagefields
grdItems.Columns("mastershere").Visible = False
grdItems.Columns("trailerhere").Visible = False
grdItems.Columns("artworkhere").Visible = False
grdItems.Columns("subshere").Visible = False
grdItems.Columns("metadatahere").Visible = False
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = False
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = False
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = False
grdItems.Columns("sub1").Visible = False
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("MetadataCreation").Visible = False
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("metadata").Visible = False
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = True

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = True
grdItems.Columns("timecodeduration").Visible = True
grdItems.Columns("gbsent").Visible = True
grdItems.Columns("ReviewDuration").Visible = False

grdItems.Columns("Rejected").Visible = True
grdItems.Columns("Rejected Date").Visible = True
grdItems.Columns("OffRejectedDate").Visible = True
grdItems.Columns("DaysOnRejected").Visible = True
grdItems.Columns("FixedJobID").Visible = True
grdItems.Columns("iTunesOrderType").Visible = True
grdItems.Columns("Due date").Visible = True
grdItems.Columns("workable").Visible = True
grdItems.Columns("targetdate").Visible = True
grdItems.Columns("targetdatecountdown").Visible = True
grdItems.Columns("completeable").Visible = True
grdItems.Columns("firstcompleteable").Visible = True
grdItems.Columns("assetshere").Visible = True

End Sub

Private Sub cmdFieldsHeaders_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("operator").Visible = True
grdItems.Columns("ordernumber").Visible = True
grdItems.Columns("projectmanager").Visible = True
grdItems.Columns("AlphaDisplayCode").Visible = True
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = True
grdItems.Columns("FeatureLanguage1").Visible = True
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = True
grdItems.Columns("trailerbis").Visible = True
grdItems.Columns("trailercreate").Visible = True
grdItems.Columns("Trailerlanguage1").Visible = True
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = True
grdItems.Columns("Artworklanguage2").Visible = True
grdItems.Columns("Artworklanguage3").Visible = True
grdItems.Columns("Artworklanguage4").Visible = True
grdItems.Columns("Subslanguage1").Visible = True
grdItems.Columns("Subslanguage2").Visible = True
grdItems.Columns("Subslanguage3").Visible = True
grdItems.Columns("Subslanguage4").Visible = True
grdItems.Columns("featureHD").Visible = True
grdItems.Columns("DisneyPlus_Framerate").Visible = True
grdItems.Columns("trailerHD").Visible = True
grdItems.Columns("chaptering").Visible = True
grdItems.Columns("metadataneeded").Visible = True
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = False
grdItems.Columns("trailerhere").Visible = False
grdItems.Columns("artworkhere").Visible = False
grdItems.Columns("subshere").Visible = False
grdItems.Columns("metadatahere").Visible = False
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = False
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = False
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = False
grdItems.Columns("sub1").Visible = False
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("MetadataCreation").Visible = False
grdItems.Columns("metadata").Visible = False
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = True

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
'grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False

grdItems.Columns("rejected").Visible = False
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("iTunesOrderType").Visible = False
grdItems.Columns("Due date").Visible = False
grdItems.Columns("workable").Visible = False
grdItems.Columns("targetdate").Visible = False
grdItems.Columns("targetdatecountdown").Visible = True
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("assetshere").Visible = True
End Sub

Private Sub cmdFieldsLinkedPackage_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = True
grdItems.Columns("rejected").Visible = True
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("iTunesOrderType").Visible = False
grdItems.Columns("ordernumber").Visible = False
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = True
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = False
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = False
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = False
grdItems.Columns("Subslanguage2").Visible = False
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("DisneyPlus_Framerate").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("Due date").Visible = False
grdItems.Columns("workable").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("assetshere").Visible = True
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = False
grdItems.Columns("trailerhere").Visible = False
grdItems.Columns("artworkhere").Visible = False
grdItems.Columns("subshere").Visible = False
grdItems.Columns("metadatahere").Visible = False
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = False
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = False
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = False
grdItems.Columns("sub1").Visible = False
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("Make Package").Visible = True
grdItems.Columns("MetadataCreation").Visible = False
grdItems.Columns("metadata").Visible = False
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = True

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = False
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = True
grdItems.Columns("iTunes_Trailer1_eventID").Visible = True
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
'grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = True
grdItems.Columns("timecodeduration").Visible = True
grdItems.Columns("gbsent").Visible = True
grdItems.Columns("ReviewDuration").Visible = False

End Sub

Private Sub cmdFieldsReady_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = False
grdItems.Columns("rejected").Visible = False
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("iTunesOrderType").Visible = False
grdItems.Columns("ordernumber").Visible = False
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = False '
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = False
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = False
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = False
grdItems.Columns("Subslanguage2").Visible = False
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("DisneyPlus_Framerate").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("Due date").Visible = False
grdItems.Columns("workable").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("assetshere").Visible = False
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = False
grdItems.Columns("trailerhere").Visible = False
grdItems.Columns("artworkhere").Visible = False
grdItems.Columns("subshere").Visible = False
grdItems.Columns("metadatahere").Visible = False
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = False
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = True
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = True
grdItems.Columns("sub1").Visible = False
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = True
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("MetadataCreation").Visible = False
grdItems.Columns("metadata").Visible = True
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = True
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = True
grdItems.Columns("package").Visible = True
grdItems.Columns("InTheQueue").Visible = True
grdItems.Columns("delivertomdr").Visible = True
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = False

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
'grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False

End Sub

Private Sub cmdFieldsSubs_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = False
grdItems.Columns("rejected").Visible = False
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("iTunesOrderType").Visible = False
grdItems.Columns("ordernumber").Visible = False
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = False
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = False
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = False
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = True
grdItems.Columns("Subslanguage2").Visible = False
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("DisneyPlus_Framerate").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("Due date").Visible = False
grdItems.Columns("workable").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("assetshere").Visible = True
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = False
grdItems.Columns("trailerhere").Visible = False
grdItems.Columns("artworkhere").Visible = False
grdItems.Columns("subshere").Visible = True
grdItems.Columns("metadatahere").Visible = False
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = True
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = True
grdItems.Columns("FeatureSubsComplexFix").Visible = True
grdItems.Columns("FeatureSubsFormatConvert").Visible = True
grdItems.Columns("feature").Visible = False
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = False
grdItems.Columns("sub1").Visible = True
grdItems.Columns("sub2").Visible = True
grdItems.Columns("sub3").Visible = True
grdItems.Columns("sub4").Visible = True
grdItems.Columns("subs").Visible = True
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("MetadataCreation").Visible = False
grdItems.Columns("metadata").Visible = False
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = True

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
'grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False



End Sub

Private Sub cmdFieldsSummary_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = True
grdItems.Columns("Rejected").Visible = True
grdItems.Columns("Rejected Date").Visible = True
grdItems.Columns("OffRejectedDate").Visible = True
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("iTunesOrderType").Visible = True
grdItems.Columns("operator").Visible = True
grdItems.Columns("ordernumber").Visible = False
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = True
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = True
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = False
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = False
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = False
grdItems.Columns("Subslanguage2").Visible = False
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("DisneyPlus_Framerate").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("Due date").Visible = True
grdItems.Columns("workable").Visible = False
grdItems.Columns("targetdate").Visible = False
grdItems.Columns("targetdatecountdown").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("assetshere").Visible = True
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = False
grdItems.Columns("trailerhere").Visible = False
grdItems.Columns("artworkhere").Visible = False
grdItems.Columns("subshere").Visible = False
grdItems.Columns("metadatahere").Visible = False
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = False
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = False
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = False
grdItems.Columns("sub1").Visible = False
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("MetadataCreation").Visible = False
grdItems.Columns("metadata").Visible = False
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = True
grdItems.Columns("InTheQueue").Visible = True
grdItems.Columns("delivertomdr").Visible = True
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = True

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
'grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False

End Sub

Private Sub cmdFieldsWorkflow_Click()

'Headers
grdItems.Columns("ordernumber").Visible = False
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = False
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = False
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = False
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = False
grdItems.Columns("Subslanguage2").Visible = False
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = True
grdItems.Columns("feature").Visible = True
grdItems.Columns("trailer").Visible = True
grdItems.Columns("sub1").Visible = True
grdItems.Columns("sub2").Visible = True
grdItems.Columns("sub3").Visible = True
grdItems.Columns("sub4").Visible = True
grdItems.Columns("metadata").Visible = True
grdItems.Columns("chapters").Visible = True
grdItems.Columns("artwork").Visible = True
grdItems.Columns("package").Visible = True
grdItems.Columns("delivertomdr").Visible = True

'Tails
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = True
grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("gbsent").Visible = False

grdItems.Columns("Rejected").Visible = True
grdItems.Columns("Rejected Date").Visible = True
grdItems.Columns("OffRejectedDate").Visible = True
grdItems.Columns("DaysOnRejected").Visible = True
grdItems.Columns("FixedJobID").Visible = True
grdItems.Columns("iTunesOrderType").Visible = True
grdItems.Columns("Due date").Visible = True
grdItems.Columns("workable").Visible = True
grdItems.Columns("completeable").Visible = True
grdItems.Columns("firstcompleteable").Visible = True
grdItems.Columns("assetshere").Visible = True

End Sub

Private Sub cmdFieldsWorkflowFeature_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = True
grdItems.Columns("rejected").Visible = False
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("iTunesOrderType").Visible = False
grdItems.Columns("ordernumber").Visible = False
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = True
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = False
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = False
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = False
grdItems.Columns("Subslanguage2").Visible = False
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("DisneyPlus_Framerate").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("Due date").Visible = False
grdItems.Columns("workable").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("assetshere").Visible = True
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = True
grdItems.Columns("trailerhere").Visible = False
grdItems.Columns("artworkhere").Visible = False
grdItems.Columns("subshere").Visible = False
grdItems.Columns("metadatahere").Visible = False
grdItems.Columns("FeatureTranscode").Visible = True
grdItems.Columns("FeatureIngest").Visible = True
grdItems.Columns("FeatureSepAudioTranscode").Visible = True
grdItems.Columns("FeatureReview").Visible = True
grdItems.Columns("FeatureReviewTimeTaken").Visible = True
grdItems.Columns("FeatureReview2Pass").Visible = True
grdItems.Columns("FeatureSubsIngest").Visible = False
grdItems.Columns("FeatureVideoFixes").Visible = True
grdItems.Columns("FeatureAudioFixes").Visible = True
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = True
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = False
grdItems.Columns("sub1").Visible = False
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("MetadataCreation").Visible = False
grdItems.Columns("metadata").Visible = False
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = True

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = False
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
'grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False



End Sub

Private Sub cmdFieldsWorkflowOthers_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = False
grdItems.Columns("rejected").Visible = False
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("iTunesOrderType").Visible = False
grdItems.Columns("ordernumber").Visible = False
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = True
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = False
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = False
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = False
grdItems.Columns("Subslanguage2").Visible = False
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("DisneyPlus_Framerate").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("Due date").Visible = False
grdItems.Columns("workable").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("assetshere").Visible = True
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = False
grdItems.Columns("trailerhere").Visible = False
grdItems.Columns("artworkhere").Visible = True
grdItems.Columns("subshere").Visible = False
grdItems.Columns("metadatahere").Visible = True
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = True
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = False
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = False
grdItems.Columns("sub1").Visible = False
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("Make Package").Visible = True
grdItems.Columns("MetadataCreation").Visible = True
grdItems.Columns("metadata").Visible = True
grdItems.Columns("ChaptersKnown").Visible = True
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = True
grdItems.Columns("ArtworkFixes").Visible = True
grdItems.Columns("artwork").Visible = True
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = True

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
'grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False

End Sub

Private Sub cmdFieldsWorkflowTrailer_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = False
grdItems.Columns("rejected").Visible = False
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("iTunesOrderType").Visible = False
grdItems.Columns("ordernumber").Visible = False
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = False
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = True
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = False
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = False
grdItems.Columns("Subslanguage2").Visible = False
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("DisneyPlus_Framerate").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("Due date").Visible = False
grdItems.Columns("workable").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("assetshere").Visible = True
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = False
grdItems.Columns("trailerhere").Visible = True
grdItems.Columns("artworkhere").Visible = False
grdItems.Columns("subshere").Visible = False
grdItems.Columns("metadatahere").Visible = False
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = False
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = False
grdItems.Columns("TrailerTranscode").Visible = True
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = True
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = True
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = True
grdItems.Columns("TrailerReview2Pass").Visible = True
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = True
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = True
grdItems.Columns("TrailerAudioFixes").Visible = True
grdItems.Columns("trailer").Visible = True
grdItems.Columns("sub1").Visible = False
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("MetadataCreation").Visible = False
grdItems.Columns("metadata").Visible = False
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = True

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = False
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
'grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False

End Sub

Private Sub cmdFieldsWorkflowTrailerExtended_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = False
grdItems.Columns("rejected").Visible = False
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("iTunesOrderType").Visible = False
grdItems.Columns("ordernumber").Visible = False
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = False
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = True
grdItems.Columns("Trailerlanguage2").Visible = True
grdItems.Columns("Trailerlanguage3").Visible = True
grdItems.Columns("Trailerlanguage4").Visible = True
grdItems.Columns("Artworklanguage1").Visible = False
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = False
grdItems.Columns("Subslanguage2").Visible = False
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("DisneyPlus_Framerate").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("Due date").Visible = False
grdItems.Columns("workable").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("assetshere").Visible = True
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = False
grdItems.Columns("trailerhere").Visible = True
grdItems.Columns("artworkhere").Visible = False
grdItems.Columns("subshere").Visible = False
grdItems.Columns("metadatahere").Visible = False
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = False
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = False
grdItems.Columns("TrailerTranscode").Visible = True
grdItems.Columns("TrailerTranscode2").Visible = True
grdItems.Columns("TrailerTranscode3").Visible = True
grdItems.Columns("TrailerTranscode4").Visible = True
grdItems.Columns("TrailerIngest").Visible = True
grdItems.Columns("TrailerIngest2").Visible = True
grdItems.Columns("TrailerIngest3").Visible = True
grdItems.Columns("TrailerIngest4").Visible = True
grdItems.Columns("TrailerClipMade").Visible = True
grdItems.Columns("TrailerClipMade2").Visible = True
grdItems.Columns("TrailerClipMade3").Visible = True
grdItems.Columns("TrailerClipMade4").Visible = True
grdItems.Columns("TrailerReview").Visible = True
grdItems.Columns("TrailerReview2Pass").Visible = True
grdItems.Columns("TrailerReview2").Visible = True
grdItems.Columns("TrailerReview2Pass2").Visible = True
grdItems.Columns("TrailerReview3").Visible = True
grdItems.Columns("TrailerReview2Pass3").Visible = True
grdItems.Columns("TrailerReview4").Visible = True
grdItems.Columns("TrailerReview2Pass4").Visible = True
grdItems.Columns("TrailerSubsBurned").Visible = True
grdItems.Columns("TrailerSubsBurned2").Visible = True
grdItems.Columns("TrailerSubsBurned3").Visible = True
grdItems.Columns("TrailerSubsBurned4").Visible = True
grdItems.Columns("TrailerVideoFixes").Visible = True
grdItems.Columns("TrailerAudioFixes").Visible = True
grdItems.Columns("trailer").Visible = True
grdItems.Columns("sub1").Visible = False
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("MetadataCreation").Visible = False
grdItems.Columns("metadata").Visible = False
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = True

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = False
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
'grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False

End Sub

Private Sub cmdGoogleFields_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = False
grdItems.Columns("rejected").Visible = True
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("MostRecentRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("iTunesOrderType").Visible = True
grdItems.Columns("iTunesSubmissionType").Visible = False
grdItems.Columns("ordernumber").Visible = True
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = True
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = False
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = False
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = True
grdItems.Columns("Subslanguage2").Visible = False
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("DisneyPlus_Framerate").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("Due date").Visible = True
grdItems.Columns("workable").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("assetshere").Visible = True
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = True
grdItems.Columns("trailerhere").Visible = False
grdItems.Columns("audiohere").Visible = True
grdItems.Columns("artworkhere").Visible = False
grdItems.Columns("subshere").Visible = True
grdItems.Columns("metadatahere").Visible = True
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = True
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = False
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = False
grdItems.Columns("sub1").Visible = True
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("MetadataCreation").Visible = True
grdItems.Columns("metadata").Visible = True
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = False

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = True
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False


End Sub

Private Sub cmdGoogleOMUFields_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = False
grdItems.Columns("rejected").Visible = True
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("MostRecentRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("iTunesOrderType").Visible = True
grdItems.Columns("iTunesSubmissionType").Visible = False
grdItems.Columns("ordernumber").Visible = True
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = True
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = False
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = False
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = True
grdItems.Columns("Subslanguage2").Visible = False
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("DisneyPlus_Framerate").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("Due date").Visible = True
grdItems.Columns("workable").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("assetshere").Visible = True
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = True
grdItems.Columns("trailerhere").Visible = False
grdItems.Columns("audiohere").Visible = True
grdItems.Columns("artworkhere").Visible = False
grdItems.Columns("subshere").Visible = True
grdItems.Columns("metadatahere").Visible = True
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = True
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = False
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = False
grdItems.Columns("sub1").Visible = True
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("MetadataCreation").Visible = True
grdItems.Columns("metadata").Visible = True
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = False

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = True
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False


End Sub

Private Sub cmdiTunesTV_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = True
grdItems.Columns("rejected").Visible = True
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("MostRecentRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("iTunesOrderType").Visible = True
grdItems.Columns("iTunesSubmissionType").Visible = False
grdItems.Columns("operator").Visible = False
grdItems.Columns("ordernumber").Visible = True
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = True
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = False
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = False
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = False
grdItems.Columns("Subslanguage2").Visible = False
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("DisneyPlus_Framerate").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("Due date").Visible = True
grdItems.Columns("workable").Visible = False
grdItems.Columns("targetdate").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("targetdatecountdown").Visible = False
grdItems.Columns("assetshere").Visible = True
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = True
grdItems.Columns("trailerhere").Visible = False
grdItems.Columns("audiohere").Visible = False
grdItems.Columns("artworkhere").Visible = False
grdItems.Columns("subshere").Visible = False
grdItems.Columns("metadatahere").Visible = True
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = False
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = False
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = False
grdItems.Columns("sub1").Visible = False
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("MetadataCreation").Visible = False
grdItems.Columns("metadata").Visible = True
grdItems.Columns("MD5_Done").Visible = True
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("SentToiTunes").Visible = True
grdItems.Columns("TransporterLogsSent").Visible = True
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = False

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = True
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False


End Sub

Private Sub cmdManualBillItem_Click()

grdItems.Columns("billed").Text = -1
grdItems.Columns("jobid").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False

End Sub

Private Sub cmdNewPlaformOrder_Click()

'Positions for the Frame
'Top = 4440
'Left = 180

If lblSearchCompanyID.Caption = "" Then
    MsgBox "You must select a company in the drop down before you can create an order"
    Exit Sub
End If

frmDisneyOrderEntry.fraDADC.Visible = False
frmDisneyOrderEntry.fraNormal.Visible = False
frmDisneyOrderEntry.fraPlatform.Top = 4440
frmDisneyOrderEntry.fraPlatform.Left = 120
frmDisneyOrderEntry.fraPlatform.Visible = True
frmDisneyOrderEntry.cmbCompany.Columns("companyID").Text = lblSearchCompanyID.Caption
frmDisneyOrderEntry.cmbCompany.Columns("name").Text = cmbCompany.Columns("fullname").Text
frmDisneyOrderEntry.lblCompanyID.Caption = lblSearchCompanyID.Caption
frmDisneyOrderEntry.txtClientStatus.Text = ""
frmDisneyOrderEntry.cmbCompany.Columns("accountstatus").Text = ""
frmDisneyOrderEntry.cmbCompany.Visible = False
frmDisneyOrderEntry.lblCompanyID.Visible = False
frmDisneyOrderEntry.txtClientStatus.Visible = False
frmDisneyOrderEntry.lblCaption(16).Visible = False
frmDisneyOrderEntry.optOrderType(3).Value = True
'frmDisneyOrderEntry.cmbInclusive(0).Visible = False
'frmDisneyOrderEntry.cmbInclusive(1).Visible = False
'frmDisneyOrderEntry.cmbInclusive(2).Visible = False
'frmDisneyOrderEntry.cmbInclusive(3).Visible = False
'frmDisneyOrderEntry.cmbInclusive(4).Visible = False
'frmDisneyOrderEntry.cmbInclusive(5).Visible = False
'frmDisneyOrderEntry.lblCaption(9).Visible = False
'frmDisneyOrderEntry.lblCaption(10).Visible = False
'frmDisneyOrderEntry.lblCaption(11).Visible = False
'frmDisneyOrderEntry.lblCaption(12).Visible = False
'frmDisneyOrderEntry.lblCaption(13).Visible = False
'frmDisneyOrderEntry.lblCaption(14).Visible = False
frmDisneyOrderEntry.fraWorkflow.Visible = False
frmDisneyOrderEntry.Label2.Top = 60
frmDisneyOrderEntry.Label2.Left = 180
frmDisneyOrderEntry.chkMultiple.Visible = True
frmDisneyOrderEntry.cmdClear.Value = True
frmDisneyOrderEntry.Show vbModal

Unload frmDisneyOrderEntry
cmdSearch.Value = True

End Sub

Private Sub cmdPlatformAssets_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = False
grdItems.Columns("channel").Visible = False
grdItems.Columns("rejected").Visible = True
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("MostRecentRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("operator").Visible = False
grdItems.Columns("iTunesOrderType").Visible = True
grdItems.Columns("iTunesSubmissionType").Visible = False
grdItems.Columns("iTunesVendorID").Visible = False
grdItems.Columns("ordernumber").Visible = True
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = True
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = False
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = True
grdItems.Columns("Artworklanguage2").Visible = True
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = True
grdItems.Columns("Subslanguage2").Visible = True
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("DisneyPlus_Framerate").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("Due date").Visible = True
grdItems.Columns("workable").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("assetshere").Visible = True
grdItems.Columns("TargetDate").Visible = False
grdItems.Columns("TargetDateCountdown").Visible = False
grdItems.Columns("EIDR").Visible = False

'Stagefields
grdItems.Columns("mastershere").Visible = True
grdItems.Columns("trailerhere").Visible = False
grdItems.Columns("audiohere").Visible = True
grdItems.Columns("artworkhere").Visible = True
grdItems.Columns("subshere").Visible = True
grdItems.Columns("metadatahere").Visible = True
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = False
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = False
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = False
grdItems.Columns("sub1").Visible = False
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("MetadataCreation").Visible = False
grdItems.Columns("MD5_Done").Visible = False
grdItems.Columns("metadata").Visible = False
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = False

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = True
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False


End Sub

Private Sub cmdPrint_Click()

Dim l_strSQL As String

If lblCompanyID.Caption <> "" Then
    l_strSQL = adoItems.RecordSource
    PrintCrystalReportUsingCleanSQL g_strLocationOfCrystalReportFiles & "GenericTrackerReport.rpt", l_strSQL, True
End If

End Sub

Private Sub cmdReport_Click()

Dim l_strSelectionFormula As String
Dim l_strReportFile As String

'If lblSearchCompanyID.Caption = "" Then
'    MsgBox "Please Select a Company Before running the report", vbCritical, "Error..."
'    Exit Sub
'End If
'
l_strSelectionFormula = "1 = 1"

If optComplete(1).Value = True Then ' Not Complete
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.readytobill} = 0 "
ElseIf optComplete(2).Value = True Then 'Finished
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.readytobill} <> 0 AND {tracker_iTunes_item.billed} = 0 "
ElseIf optComplete(3).Value = True Then 'Pending
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.readytobill} = 0 AND {tracker_iTunes_item.rejected} <> 0"
ElseIf optComplete(4).Value = True Then 'Billed
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.billed} <> 0"
ElseIf optComplete(5).Value = True Then ' Ready to Package
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.readytobill} = 0 AND {tracker_iTunes_item.readytopackage} <> 0"
ElseIf optComplete(6).Value = True Then ' Not Pending
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.billed} = 0 AND {tracker_iTunes_item.readytobill} = 0 AND {tracker_iTunes_item.rejected} = 0 "
ElseIf optComplete(7).Value = True Then ' Feature Here
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.billed} = 0 AND {tracker_iTunes_item.readytobill} = 0 AND {tracker_iTunes_item.mastershere} <> '' AND (isnull({tracker_iTunes_item.feature}) OR {tracker_iTunes_item.feature} = '') "
ElseIf optComplete(8).Value = True Then ' Trailer Here
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.billed} = 0 AND {tracker_iTunes_item.readytobill} = 0 AND {tracker_iTunes_item.trailerhere} <> '' AND (isnull({tracker_iTunes_item.trailer}) OR {tracker_iTunes_item.trailer} = '') "
ElseIf optComplete(9).Value = True Then ' Artwork Here
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.billed} = 0 AND {tracker_iTunes_item.readytobill} = 0 AND {tracker_iTunes_item.artworkhere} <> '' AND (isnull({tracker_iTunes_item.artwork}) OR {tracker_iTunes_item.artwork} = '') "
ElseIf optComplete(10).Value = True Then ' Subs Here
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.billed} = 0 AND {tracker_iTunes_item.readytobill} = 0 AND {tracker_iTunes_item.subshere} <> '' AND (isnull({tracker_iTunes_item.subs}) OR {tracker_iTunes_item.subs} = '') "
ElseIf optComplete(11).Value = True Then ' Metadata Here
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.billed} = 0 AND {tracker_iTunes_item.readytobill} = 0 AND {tracker_iTunes_item.metadatahere} <> '' AND (isnull({tracker_iTunes_item.metadata}) OR {tracker_iTunes_item.metadata} = '') "
ElseIf optComplete(12).Value = True Then ' DADC Workable
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.billed} = 0 AND {tracker_iTunes_item.readytobill} = 0 AND {tracker_iTunes_item.rejected} = 0 and {tracker_iTunes_item.assetshere} <> 0 "
ElseIf optComplete(13).Value = True Then ' DADC Not Workable
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.billed} = 0 AND {tracker_iTunes_item.readytobill} = 0 AND ({tracker_iTunes_item.rejected} <> 0 OR {tracker_iTunes_item.assetshere} = 0) "
ElseIf optComplete(14).Value = True Then ' Workable
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.billed} = 0 AND {tracker_iTunes_item.readytobill} = 0 AND (NOT ISNULL({tracker_iTunes_item.workable})) "
ElseIf optComplete(15).Value = True Then ' Completeable
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.billed} = 0 AND {tracker_iTunes_item.readytobill} = 0 AND (NOT ISNULL({tracker_iTunes_item.completeable})) "
End If

If optDeliveryType(0).Value = True Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_itunes_item.itunesordertype} = 'iTunes Digital Delivery' "
ElseIf optDeliveryType(6).Value = True Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_itunes_item.itunesordertype} = 'iTunes TV Delivery' "
ElseIf optDeliveryType(7).Value = True Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_itunes_item.itunesordertype} = 'Google Delivery' "
ElseIf optDeliveryType(10).Value = True Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_itunes_item.itunesordertype} = 'Google OMU Delivery' "
ElseIf optDeliveryType(8).Value = True Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_itunes_item.itunesordertype} = 'Amazon Delivery' "
ElseIf optDeliveryType(9).Value = True Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_itunes_item.itunesordertype} = 'Amazon MLF Delivery' "
ElseIf optDeliveryType(10).Value = True Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_itunes_item.itunesordertype} = 'Google OMU Delivery' "
ElseIf optDeliveryType(11).Value = True Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_itunes_item.itunesordertype} = 'Disney Dub Cards' "
ElseIf optDeliveryType(2).Value = True Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_itunes_item.itunesordertype} <> 'iTunes Digital Delivery' AND {tracker_itunes_item.itunesordertype} <> 'iTunes TV Delivery' "
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_itunes_item.itunesordertype} <> 'Google Delivery' AND {tracker_itunes_item.itunesordertype} <> 'Google OMU Delivery' "
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_itunes_item.itunesordertype} <> 'Amazon Delivery' AND {tracker_itunes_item.itunesordertype} <> 'Amazon MLF Delivery' "
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_itunes_item.itunesordertype} <> 'AON Telekom digital delivery' AND {tracker_itunes_item.itunesordertype} <> 'Realeyz.TV digital delivery' "
End If

If txtVendorID.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.OrderNumber} LIKE '" & txtOrderNumber.Text & "*' "
End If

If txtOrderNumber.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.iTunesVendorID} LIKE '" & txtVendorID.Text & "*' "
End If

If txtTitle.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.title} LIKE '" & txtTitle.Text & "*' "
End If
If txtSubtitle.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.EpisodeTitle} LIKE '" & txtSubtitle.Text & "*' "
End If
If Val(txtEpisode.Text) <> 0 Then
    l_strSelectionFormula = l_strSelectionFormula & " AND not isnull({tracker_iTunes_item.episode}) AND {tracker_iTunes_item.episode} = " & Val(txtEpisode.Text) & " "
End If
If Val(txtSeries.Text) <> 0 Then
    l_strSelectionFormula = l_strSelectionFormula & " AND not isnull({tracker_iTunes_item.series}) AND {tracker_iTunes_item.series} = '" & txtSeries.Text & "' "
End If
If Val(txtOrderNumber.Text) <> 0 Then
    l_strSelectionFormula = l_strSelectionFormula & " AND not isnull({tracker_iTunes_item.ordernumber}) AND ordernumber = '" & txtOrderNumber.Text & "' "
End If
If Val(txtVendorID.Text) <> 0 Then
    l_strSelectionFormula = l_strSelectionFormula & " AND not isnull({tracker_iTunes_item.iTunesVendorID}) AND {tracker_iTunes_item.iTunesVendorID} = '" & txtVendorID.Text & "' "
End If
If cmbProject.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.Project} LIKE '" & QuoteSanitise(cmbProject.Text) & "*' "
End If
If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.companyID} = " & Val(lblSearchCompanyID.Caption) & " "
Else
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_iTunes_item.companyID} > 100 "
End If
    
Debug.Print l_strSelectionFormula
l_strReportFile = g_strLocationOfCrystalReportFiles & "iTunes_Tracker_Report.rpt"

'MsgBox l_strSelectionFormula, vbInformation
PrintCrystalReport l_strReportFile, l_strSelectionFormula, True

End Sub

Private Sub cmdSaveAndProcessDubcards_Click()

If optDeliveryType(11).Value = False Then
    MsgBox "Cannot Bulk Process Dubcards unless Disney+ selected"
    Exit Sub
End If

If adoItems.Recordset.RecordCount <= 0 Then
    MsgBox "No items in the grid"
    Exit Sub
End If

If MsgBox("You are about to open each Dubcard for each item, and press 'Process'" & vbCrLf & "Are you sure", vbYesNo + vbDefaultButton2, "Bulk Dub Card Processing") = vbNo Then Exit Sub

adoItems.Recordset.MoveFirst
DoEvents

Do While Not adoItems.Recordset.EOF
    If adoDubCards.Recordset.RecordCount > 0 Then
        adoDubCards.Recordset.MoveFirst
        Do While Not adoDubCards.Recordset.EOF
            DoEvents
            If Not (grdItems.Columns("tracker_itemID") Is Nothing) Then
                frmTrackerDubCard.lbltracker_itemID.Caption = grdItems.Columns("tracker_itemID").Text
                frmTrackerDubCard.chkAutoProcess.Value = 1
            End If
            frmTrackerDubCard.Show vbModal
            DoEvents
            adoDubCards.Recordset.MoveNext
        Loop
    End If
    adoItems.Recordset.MoveNext
Loop

End Sub

Private Sub cmdSearch_Click()

Dim l_strSQL As String

l_strSQL = ""

If chkHideDemo.Value <> 0 Then
    l_strSQL = l_strSQL & " AND companyID > 100 "
End If

'If Val(lblSearchCompanyID.Caption) <> 0 Then
'    l_strSQL = l_strSQL & " AND companyID = " & Val(lblSearchCompanyID.Caption) & " "
'End If

If optComplete(1).Value = True Then ' Not Complete
    l_strSQL = l_strSQL & " AND (readytobill IS NULL OR readytobill = 0) "
ElseIf optComplete(2).Value = True Then 'Finished
    l_strSQL = l_strSQL & " AND readytobill <> 0 AND billed = 0 "
ElseIf optComplete(3).Value = True Then 'Pending
    l_strSQL = l_strSQL & " AND (readytobill IS NULL OR readytobill = 0) AND rejected <> 0"
ElseIf optComplete(4).Value = True Then 'Billed
    l_strSQL = l_strSQL & " AND billed <> 0"
ElseIf optComplete(5).Value = True Then ' Ready to Package
    l_strSQL = l_strSQL & " AND readytobill = 0 AND readytopackage <> 0"
ElseIf optComplete(6).Value = True Then ' Not Pending
    l_strSQL = l_strSQL & " AND (billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND rejected = 0 "
ElseIf optComplete(7).Value = True Then ' Feature Here
    l_strSQL = l_strSQL & " AND (billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND mastershere <> '' AND (feature IS NULL or feature = '') "
ElseIf optComplete(8).Value = True Then ' Trailer Here
    l_strSQL = l_strSQL & " AND (billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND trailerhere <> '' AND (trailer IS NULL or trailer = '') "
ElseIf optComplete(9).Value = True Then ' Artwork Here
    l_strSQL = l_strSQL & " AND (billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND artworkhere <> '' AND (artwork IS NULL or artwork = '') "
ElseIf optComplete(10).Value = True Then ' Subs Here
    l_strSQL = l_strSQL & " AND (billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND subshere <> '' AND (subs IS NULL or subs = '') "
ElseIf optComplete(11).Value = True Then ' Metadata Here
    l_strSQL = l_strSQL & " AND (billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND metadatahere <> '' AND (metadata IS NULL or metadata = '') "
ElseIf optComplete(12).Value = True Then ' DADC Workable
    l_strSQL = l_strSQL & " AND (billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND rejected = 0 and assetshere <> 0 "
ElseIf optComplete(13).Value = True Then ' DADC Not Workable
    l_strSQL = l_strSQL & " AND (billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (rejected <> 0 or assetshere = 0) "
ElseIf optComplete(14).Value = True Then ' Workable
    l_strSQL = l_strSQL & " AND (billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (workable IS NOT NULL) "
ElseIf optComplete(15).Value = True Then ' Not Workable
    l_strSQL = l_strSQL & " AND (billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (completeable IS NOT NULL) "
ElseIf optComplete(16).Value = True Then ' Dub Cards Here and not complete
    l_strSQL = l_strSQL & " AND (billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND rejected = 0 AND Disneyplus_Docs IS NOT NULL and Disneyplus_Docs <> '' AND (Disneyplus_Dubcard IS Null OR DisneyPlus_Dubcard = '') "
ElseIf optComplete(17).Value = True Then ' Audio Proxy Here and not complete
    l_strSQL = l_strSQL & " AND (billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND rejected = 0 AND Disneyplus_OVHere IS NOT NULL and Disneyplus_OVHere <> '' AND DisneyPlus_ProxyHere IS NOT NULL AND DisneyPlus_ProxyHere <> '' AND (Disneyplus_AudioDone IS Null OR Disneyplus_AudioDone = '') "
ElseIf optComplete(18).Value = True Then ' Ready For Jellyroll
    l_strSQL = l_strSQL & " AND (billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (completeable IS NOT NULL) AND Disneyplus_Ready IS NOT NULL and Disneyplus_Ready <> '' "
ElseIf optComplete(19).Value = True Then ' Audio Not Here
    l_strSQL = l_strSQL & " AND (billed = 0) AND DisneyPlus_OVHere IS NULL AND (DisneyPlus_AudioDone IS NULL OR DisneyPlus_AudioDone = '') "
ElseIf optComplete(20).Value = True Then ' Proxy Not Here
    l_strSQL = l_strSQL & " AND (billed = 0) AND DisneyPlus_Docs IS NULL AND (DisneyPlus_Dubcard IS NULL OR DisneyPlus_Dubcard = '')  "
End If

If optDeliveryType(0).Value = True Then
    l_strSQL = l_strSQL & " AND itunesordertype = 'iTunes Film Delivery' "
ElseIf optDeliveryType(1).Value = True Then
    l_strSQL = l_strSQL & " AND itunesordertype = 'XBox' "
ElseIf optDeliveryType(4).Value = True Then
    l_strSQL = l_strSQL & " AND itunesordertype = 'Sony PSN' "
ElseIf optDeliveryType(5).Value = True Then
    l_strSQL = l_strSQL & " AND itunesordertype = 'Netflix' "
ElseIf optDeliveryType(6).Value = True Then
    l_strSQL = l_strSQL & " AND itunesordertype = 'iTunes TV Delivery' "
ElseIf optDeliveryType(7).Value = True Then
    l_strSQL = l_strSQL & " AND itunesordertype = 'Google Delivery' "
ElseIf optDeliveryType(8).Value = True Then
    l_strSQL = l_strSQL & " AND itunesordertype = 'Amazon delivery' "
ElseIf optDeliveryType(9).Value = True Then
    l_strSQL = l_strSQL & " AND itunesordertype = 'Amazon MLF delivery' "
ElseIf optDeliveryType(10).Value = True Then
    l_strSQL = l_strSQL & " AND itunesordertype = 'Google OMU Delivery' "
ElseIf optDeliveryType(11).Value = True Then
    l_strSQL = l_strSQL & " AND itunesordertype = 'Disney Dub Cards' "
ElseIf optDeliveryType(2).Value = True Then
    l_strSQL = l_strSQL & " AND itunesordertype <> 'iTunes Digital Delivery' AND itunesordertype <> 'Maxdome Digital Delivery' AND itunesordertype <> 'AON Telekom digital delivery' AND itunesordertype <> 'Realeyz.TV digital delivery' "
End If

If txtTitle.Text <> "" Then
    l_strSQL = l_strSQL & " AND title LIKE '" & QuoteSanitise(txtTitle.Text) & "%' "
End If
If txtSubtitle.Text <> "" Then
    l_strSQL = l_strSQL & " AND EpisodeTitle LIKE '" & QuoteSanitise(txtSubtitle.Text) & "%' "
End If
If Val(txtEpisode.Text) <> 0 Then
    l_strSQL = l_strSQL & " AND (episode IS NOT NULL AND episode = " & Val(txtEpisode.Text) & ") "
End If
If txtSeries.Text <> "" Then
    l_strSQL = l_strSQL & " AND (series IS NOT NULL AND series = '" & txtSeries.Text & "') "
End If
If txtOrderNumber.Text <> "" Then
    l_strSQL = l_strSQL & " AND (ordernumber IS NOT NULL AND ordernumber = '" & txtOrderNumber.Text & "') "
End If
If txtVendorID.Text <> "" Then
    l_strSQL = l_strSQL & " AND (iTunesVendorID IS NOT NULL AND iTunesVendorID = '" & txtVendorID.Text & "') "
End If
If txtAlphaCode.Text <> "" Then
    l_strSQL = l_strSQL & " AND (AlphaDisplayCode IS NOT NULL AND AlphaDisplayCode LIKE '" & txtAlphaCode.Text & "%') "
End If
If cmbProject.Text <> "" Then
    l_strSQL = l_strSQL & " AND Project LIKE '" & QuoteSanitise(cmbProject.Text) & "%' "
End If
If txtLanguage.Text <> "" Then
    l_strSQL = l_strSQL & " AND FeatureLanguage1 LIKE '" & txtLanguage.Text & "%' "
End If

Debug.Print l_strSQL

m_blnSilent = True
adoItems.ConnectionString = g_strConnection
adoItems.RecordSource = "SELECT * FROM tracker_iTunes_item WHERE 1=0;"
adoItems.Refresh
adoItems.RecordSource = "SELECT * FROM tracker_iTunes_item " & m_strSearch & l_strSQL & m_strOrderby & ";"
adoItems.Refresh

adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"
m_blnSilent = False

If Not adoItems.Recordset.EOF Then
    adoItems.Recordset.MoveLast
    adoItems.Recordset.MoveFirst
End If

'If optDeliveryType(0).Value = True Then
'    l_strSQL = l_strSQL & " AND itunesordertype = 'iTunes Film Delivery' "
''ElseIf optDeliveryType(4).Value = True Then
''    l_strSQL = l_strSQL & " AND itunesordertype = 'AON Telekom digital delivery' "
''ElseIf optDeliveryType(5).Value = True Then
''    l_strSQL = l_strSQL & " AND itunesordertype = 'Realeyz.TV digital delivery' "
'ElseIf optDeliveryType(6).Value = True Then
'    l_strSQL = l_strSQL & " AND itunesordertype = 'iTunes TV Delivery' "
'ElseIf optDeliveryType(7).Value = True Then
'    l_strSQL = l_strSQL & " AND itunesordertype = 'Google Delivery' "
'ElseIf optDeliveryType(8).Value = True Then
'    l_strSQL = l_strSQL & " AND itunesordertype = 'Amazon delivery' "
'ElseIf optDeliveryType(2).Value = True Then
'    l_strSQL = l_strSQL & " AND itunesordertype <> 'iTunes Digital Delivery' AND itunesordertype <> 'Maxdome Digital Delivery' AND itunesordertype <> 'AON Telekom digital delivery' AND itunesordertype <> 'Realeyz.TV digital delivery' "
'End If
'
End Sub

Private Sub cmdUnbill_Click()

grdItems.Columns("billed").Text = 0
grdItems.Columns("jobid").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False

End Sub

Private Sub cmdUnbillAll_Click()

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then

    Do While Not adoItems.Recordset.EOF
        adoItems.Recordset("billed") = 0
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
    Loop
End If

grdItems.Refresh

End Sub

Private Sub cmdUpdateAll_Click()

If adoItems.Recordset.RecordCount > 0 Then

    adoItems.Recordset.MoveFirst
    Do While Not adoItems.Recordset.EOF
        adoItems.Recordset("itemreference") = adoItems.Recordset("itemreference") & " "
        adoItems.Recordset("itemreference") = Trim(adoItems.Recordset("itemreference"))
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
    Loop
    adoItems.Recordset.MoveFirst
End If

End Sub

Private Sub cmdXbox_Click()

'Ep Stuff
If chkHideEpStuff.Value <> 0 Then
    grdItems.Columns("Series").Visible = False
    grdItems.Columns("Volume").Visible = False
    grdItems.Columns("Episode").Visible = False
    grdItems.Columns("EpisodeTitle").Visible = False
Else
    grdItems.Columns("Series").Visible = True
    grdItems.Columns("Volume").Visible = True
    grdItems.Columns("Episode").Visible = True
    grdItems.Columns("EpisodeTitle").Visible = True
End If

'Headers
grdItems.Columns("iTunesVendorID").Visible = False
grdItems.Columns("rejected").Visible = True
grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("OffRejectedDate").Visible = False
grdItems.Columns("MostRecentRejectedDate").Visible = False
grdItems.Columns("DaysOnRejected").Visible = False
grdItems.Columns("FixedJobID").Visible = False
grdItems.Columns("iTunesOrderType").Visible = False
grdItems.Columns("iTunesSubmissionType").Visible = False
grdItems.Columns("operator").Visible = False
grdItems.Columns("ordernumber").Visible = True
grdItems.Columns("projectmanager").Visible = False
grdItems.Columns("AlphaDisplayCode").Visible = False
grdItems.Columns("Project").Visible = False
grdItems.Columns("FeatureAudioSpec").Visible = False
grdItems.Columns("FeatureLanguage1").Visible = False
grdItems.Columns("FeatureLanguage2").Visible = False
grdItems.Columns("FeatureLanguage3").Visible = False
grdItems.Columns("FeatureLanguage4").Visible = False
grdItems.Columns("TrailerAudioSpec").Visible = False
grdItems.Columns("trailerbis").Visible = False
grdItems.Columns("trailercreate").Visible = False
grdItems.Columns("Trailerlanguage1").Visible = False
grdItems.Columns("Trailerlanguage2").Visible = False
grdItems.Columns("Trailerlanguage3").Visible = False
grdItems.Columns("Trailerlanguage4").Visible = False
grdItems.Columns("Artworklanguage1").Visible = False
grdItems.Columns("Artworklanguage2").Visible = False
grdItems.Columns("Artworklanguage3").Visible = False
grdItems.Columns("Artworklanguage4").Visible = False
grdItems.Columns("Subslanguage1").Visible = False
grdItems.Columns("Subslanguage2").Visible = False
grdItems.Columns("Subslanguage3").Visible = False
grdItems.Columns("Subslanguage4").Visible = False
grdItems.Columns("featureHD").Visible = False
grdItems.Columns("DisneyPlus_Framerate").Visible = False
grdItems.Columns("trailerHD").Visible = False
grdItems.Columns("chaptering").Visible = False
grdItems.Columns("metadataneeded").Visible = False
grdItems.Columns("Due date").Visible = True
grdItems.Columns("workable").Visible = False
grdItems.Columns("targetdate").Visible = False
grdItems.Columns("completeable").Visible = False
grdItems.Columns("firstcompleteable").Visible = False
grdItems.Columns("targetdatecountdown").Visible = False
grdItems.Columns("assetshere").Visible = True
grdItems.Columns("EIDR").Visible = True

'Stagefields
grdItems.Columns("mastershere").Visible = False
grdItems.Columns("trailerhere").Visible = False
grdItems.Columns("audiohere").Visible = False
grdItems.Columns("artworkhere").Visible = False
grdItems.Columns("subshere").Visible = False
grdItems.Columns("metadatahere").Visible = False
grdItems.Columns("FeatureTranscode").Visible = False
grdItems.Columns("FeatureIngest").Visible = False
grdItems.Columns("FeatureSepAudioTranscode").Visible = False
grdItems.Columns("FeatureReview").Visible = False
grdItems.Columns("FeatureReviewTimeTaken").Visible = False
grdItems.Columns("FeatureReview2Pass").Visible = False
grdItems.Columns("FeatureSubsIngest").Visible = False
grdItems.Columns("FeatureVideoFixes").Visible = False
grdItems.Columns("FeatureAudioFixes").Visible = False
grdItems.Columns("FeatureSubsSimpleFix").Visible = False
grdItems.Columns("FeatureSubsComplexFix").Visible = False
grdItems.Columns("FeatureSubsFormatConvert").Visible = False
grdItems.Columns("feature").Visible = False
grdItems.Columns("TrailerTranscode").Visible = False
grdItems.Columns("TrailerTranscode2").Visible = False
grdItems.Columns("TrailerTranscode3").Visible = False
grdItems.Columns("TrailerTranscode4").Visible = False
grdItems.Columns("TrailerIngest").Visible = False
grdItems.Columns("TrailerIngest2").Visible = False
grdItems.Columns("TrailerIngest3").Visible = False
grdItems.Columns("TrailerIngest4").Visible = False
grdItems.Columns("TrailerClipMade").Visible = False
grdItems.Columns("TrailerClipMade2").Visible = False
grdItems.Columns("TrailerClipMade3").Visible = False
grdItems.Columns("TrailerClipMade4").Visible = False
grdItems.Columns("TrailerReview").Visible = False
grdItems.Columns("TrailerReview2Pass").Visible = False
grdItems.Columns("TrailerReview2").Visible = False
grdItems.Columns("TrailerReview2Pass2").Visible = False
grdItems.Columns("TrailerReview3").Visible = False
grdItems.Columns("TrailerReview2Pass3").Visible = False
grdItems.Columns("TrailerReview4").Visible = False
grdItems.Columns("TrailerReview2Pass4").Visible = False
grdItems.Columns("TrailerSubsBurned").Visible = False
grdItems.Columns("TrailerSubsBurned2").Visible = False
grdItems.Columns("TrailerSubsBurned3").Visible = False
grdItems.Columns("TrailerSubsBurned4").Visible = False
grdItems.Columns("TrailerVideoFixes").Visible = False
grdItems.Columns("TrailerAudioFixes").Visible = False
grdItems.Columns("trailer").Visible = False
grdItems.Columns("sub1").Visible = False
grdItems.Columns("sub2").Visible = False
grdItems.Columns("sub3").Visible = False
grdItems.Columns("sub4").Visible = False
grdItems.Columns("subs").Visible = False
grdItems.Columns("Make Package").Visible = False
grdItems.Columns("MetadataCreation").Visible = False
grdItems.Columns("metadata").Visible = False
grdItems.Columns("MD5_Done").Visible = False
grdItems.Columns("ChaptersKnown").Visible = False
grdItems.Columns("ChaptersUnknown").Visible = False
grdItems.Columns("chapters").Visible = False
grdItems.Columns("ArtworkFixes").Visible = False
grdItems.Columns("artwork").Visible = False
grdItems.Columns("package").Visible = False
grdItems.Columns("InTheQueue").Visible = False
grdItems.Columns("delivertomdr").Visible = False
grdItems.Columns("delivertomodern").Visible = False
grdItems.Columns("SentToiTunes").Visible = True
grdItems.Columns("TransporterLogsSent").Visible = True
grdItems.Columns("DisneyPlus_OVHere").Visible = False
grdItems.Columns("DisneyPlus_ProxyHere").Visible = False
grdItems.Columns("DisneyPlus_Docs").Visible = False
grdItems.Columns("DisneyPlus_DubCard").Visible = False
grdItems.Columns("DisneyPlus_AudioDone").Visible = False
grdItems.Columns("DisneyPlus_ForcedNarratives").Visible = False
grdItems.Columns("DisneyPlus_Subtitles").Visible = False
grdItems.Columns("DisneyPlus_Ready").Visible = False
grdItems.Columns("DisneyPlus_Sent").Visible = False
grdItems.Columns("readyforTX").Visible = False

'Tails
grdItems.Columns("iTunes_packageID").Visible = True
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = False
grdItems.Columns("iTunes_Master_eventID").Visible = False
grdItems.Columns("iTunes_Trailer1_eventID").Visible = False
grdItems.Columns("iTunes_Trailer2_eventID").Visible = False
grdItems.Columns("iTunes_Trailer3_eventID").Visible = False
grdItems.Columns("iTunes_Trailer4_eventID").Visible = False
grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = True
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("ReviewDuration").Visible = False

End Sub

Private Sub ddnChannel_CloseUp()

grdItems.Columns("companyID").Text = ddnChannel.Columns("companyID").Text
grdItems.Columns("channel").Text = ddnChannel.Columns("name").Text

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

Dim l_blnWide As Boolean

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch1 As ADODB.Recordset
Dim l_rstSearch2 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch1 = New ADODB.Recordset
Set l_rstSearch2 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT portalcompanyname, companyID, name as fullname FROM company WHERE cetaclientcode like '%/iTunestracker%' and companyID > 100 ORDER BY name;"
Else
    l_strSQL = "SELECT portalcompanyname, companyID, name as fullname FROM company WHERE cetaclientcode like '%/iTunestracker%' ORDER BY name;"
End If

With l_rstSearch1
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch1.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch1

With l_rstSearch2
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch2.ActiveConnection = Nothing

Set ddnChannel.DataSource = l_rstSearch2
grdItems.Columns("channel").DropDownHwnd = ddnChannel.hWnd

l_conSearch.Close
Set l_conSearch = Nothing

adoComments.ConnectionString = g_strConnection

grdItems.StyleSets("headerfield").BackColor = &HE7FFE7
grdItems.StyleSets("stagefield").BackColor = &HE7FFFF
grdItems.StyleSets("conclusionfield").BackColor = &HFFFFE7

grdItems.StyleSets.Add "Error"
grdItems.StyleSets("Error").BackColor = &HA0A0FF

grdItems.StyleSets.Add "NotRequired"
grdItems.StyleSets("NotRequired").BackColor = &H10101

grdItems.StyleSets.Add "Priority"
grdItems.StyleSets("Priority").BackColor = &H70B0FF

optComplete(1).Value = True

PopulateCombo "trackercommenttypes", ddnCommentTypes
grdComments.Columns("commenttypedecoded").DropDownHwnd = ddnCommentTypes.hWnd

PopulateCombo "DigitalDeliveryPlatform", ddnDigitalDeliveryPlatform
grdItems.Columns("iTunesOrderType").DropDownHwnd = ddnDigitalDeliveryPlatform.hWnd

PopulateCombo "operator", ddnUsers
grdItems.Columns("operator").DropDownHwnd = ddnUsers.hWnd

PopulateCombo "framerate", ddnFramerate, "MEDIA"
grdItems.Columns("DisneyPlus_Framerate").DropDownHwnd = ddnFramerate.hWnd

m_strSearch = " WHERE 1 = 1 "
m_strOrderby = " ORDER BY Priority, requiredby, title, episode, episodetitle"
m_blnSilent = False

DoEvents
cmdSearch.Value = True
cmdFieldsSummary.Value = True

End Sub

Private Sub Form_Resize()

On Error Resume Next

grdItems.Width = Me.ScaleWidth - grdItems.Left - 120
grdItems.Height = (Me.ScaleHeight - grdItems.Top - frmButtons.Height) * 0.75 - 240
frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120
grdComments.Top = grdItems.Top + grdItems.Height + 120
grdComments.Height = frmButtons.Top - grdComments.Top - 120
grdComments.Width = grdItems.Width / 2 - 120
grdDubcards.Height = grdComments.Height
grdDubcards.Width = grdComments.Width
grdDubcards.Top = grdComments.Top
grdDubcards.Left = grdComments.Left + grdComments.Width + 120

End Sub

Private Sub grdComments_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdComments_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdComments_BeforeUpdate(Cancel As Integer)

If m_blnDelete = False Then

    If grdComments.Columns("comment").Text = "" Then
        MsgBox "Cannot Save a Comment with no actual comment", vbCritical, "Comment Not Saved"
        Cancel = True
        Exit Sub
    End If
    
    grdComments.Columns("tracker_itunes_itemID").Text = grdItems.Columns("tracker_itunes_itemID").Text
    If grdComments.Columns("cdate").Text = "" Then
        grdComments.Columns("cdate").Text = Now
    End If
    grdComments.Columns("cuser").Text = g_strFullUserName
    
    If grdComments.Columns("commenttypedecoded").Text = "" Then grdComments.Columns("commenttypedecoded").Text = "General"
    If grdComments.Columns("commenttypedecoded").Text = "Decision Tree" Then grdComments.Columns("commenttypedecoded").Text = "General"
    grdComments.Columns("commenttype").Text = GetDataSQL("SELECT TOP 1 information FROM xref WHERE category = 'trackercommenttypes' AND description = '" & grdComments.Columns("commenttypedecoded").Text & "';")
    
End If

End Sub

Private Sub grdComments_BtnClick()

Dim l_strOurEmailContact As String, l_strOurContactName As String, l_strEmailBody As String

Dim l_rstWhoToEmail As ADODB.Recordset

If MsgBox("Send Email?", vbYesNo, "Automatic Email") = vbYes Then
    
    l_strEmailBody = "A comment was created " & _
        "By: " & grdComments.Columns("cuser").Text & vbCrLf & _
        "About: " & grdItems.Columns("itemreference").Text & vbCrLf & _
        "Title: " & grdItems.Columns("title").Text & vbCrLf & _
        "Comment: " & grdComments.Columns("comment").Text & vbCrLf
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND trackermessageID = 35;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "iTunes Tracker Comment Created", "", l_strEmailBody, True, "", ""
    
            l_rstWhoToEmail.MoveNext
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
    
End If

End Sub

Private Sub grdComments_RowLoaded(ByVal Bookmark As Variant)

If grdComments.Columns("commenttype").Text = 0 Then
    grdComments.Columns("Commenttypedecoded").Text = "Pending"
ElseIf grdComments.Columns("Commenttype").Text = 2 Then
    grdComments.Columns("Commenttypedecoded").Text = "General"
Else
    grdComments.Columns("Commenttypedecoded").Text = "Decision Tree"
End If

End Sub

Private Sub grdItems_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdItems_AfterInsert(RtnDispErrMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdItems_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdItems_BeforeInsert(Cancel As Integer)
m_blnDelete = True
End Sub

Private Sub grdItems_BeforeUpdate(Cancel As Integer)

If m_blnDelete = True Then Exit Sub

Dim temp As Boolean, l_strDuration As String, l_lngRunningTime As Long, l_curFileSize As Currency, l_strFilename As String, l_rst As ADODB.Recordset, l_datNewTargetDate As Date, Count As Long
Dim l_lngClipID As Long, l_strNetworkPath As String, l_strSQL As String, l_rstWhoToEmail As ADODB.Recordset, l_strEmailBody As String, l_lngProjectManagerContactID As Long

Dim rejectdatenow As Date
Dim rejectdateCur As Date
Dim mostrecentdateCur As Date

''Check if Project Manager has changed, and if so, look up the new contactID and enter it in the hidden field
'If Val(lblTrackeritemID.Caption) <> 0 Then
'    If grdItems.Columns("projectmanager").Text <> GetData("tracker_svensk_item", "projectmanager", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption)) Then
'        l_lngProjectManagerContactID = Val(Trim(" " & GetDataSQL("SELECT contactID FROM contact WHERE name = '" & grdItems.Columns("projectmanager").Text & "' AND contactID IN (SELECT contactID FROM employee WHERE companyID = " & Val(lblCompanyID.Caption) & ");")))
'        If l_lngProjectManagerContactID <> 0 Then
'            grdItems.Columns("projectmanagercontactID").Text = l_lngProjectManagerContactID
'        Else
'            MsgBox "You have typed a Project Manager who is not listed as a contact for Svensk Ingest." & vbCrLf & "Line not saved", vbCritical, "Error..."
'            Cancel = 1
'            Exit Sub
'        End If
'    End If
'End If
'
If grdItems.Columns("companyID").Text = "" Then
    MsgBox "Please select an MX1 Client." & vbCrLf & "Row not saved", vbCritical, "Error..."
    Cancel = 1
    Exit Sub
End If

If grdItems.Columns("iTunesVendorID").Text = "" And (grdItems.Columns("iTunesOrderType").Text = "iTunes Film Delivery" Or grdItems.Columns("iTunesOrderType").Text = "AON Telekom digital delivery" _
Or grdItems.Columns("iTunesOrderType").Text = "Maxdome Digital Delivery" Or grdItems.Columns("iTunesOrderType").Text = "Realeyz.TV digital delivery") Then
    MsgBox "Please enter an iTunes Vendor ID." & vbCrLf & "Row not saved", vbCritical, "Error..."
    Cancel = 1
    Exit Sub
End If

If grdItems.Columns("cdate").Text = "" Then
    grdItems.Columns("cdate").Text = Format(Now, "YYYY-MM-DD HH:NN:SS")
End If

grdItems.Columns("mdate").Text = Format(Now, "YYYY-MM-DD HH:NN:SS")
l_strSQL = "INSERT INTO tracker_itunes_audit (tracker_iTunes_itemID, mdate, muser) VALUES (" & Val(lblTrackeritemID.Caption) & ", getdate(), '" & g_strFullUserName & "');"
Debug.Print l_strSQL
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

'Check some basic functionality...

'If the Feature has been reviewed, it must be here, and it must either have been ingested or transcoded
If grdItems.Columns("featurereview").Text <> "" Then
    If grdItems.Columns("mastershere").Text = "" Then
        grdItems.Columns("mastershere").Text = grdItems.Columns("featurereview").Text
        If IsDate(grdItems.Columns("mastershere").Text) Then
            grdItems.Columns("dat_mastershere").Text = grdItems.Columns("mastershere").Text
        End If
    End If
    If grdItems.Columns("featureingest").Text = "" And grdItems.Columns("featuretranscode").Text = "" Then
        If MsgBox("Please confirm that the feature was neither Ingested or Transcoded", vbYesNo, "Query...") = vbNo Then
            Cancel = True
            Exit Sub
        End If
    End If
End If

If grdItems.Columns("mastershere").Text <> "" Then
    If IsDate(grdItems.Columns("mastershere").Text) Then
        grdItems.Columns("dat_mastershere").Text = grdItems.Columns("mastershere").Text
    End If
End If

If grdItems.Columns("DeliverToMDR").Text <> "" Then
    If IsDate(grdItems.Columns("DeliverToMDR").Text) Then
        grdItems.Columns("dat_DeliverToMDR").Text = grdItems.Columns("DeliverToMDR").Text
    End If
End If


'If the Trailer has been reviewed, it must be here, and it must either have been ingested or transcoded
If grdItems.Columns("trailerreview").Text <> "" Then
    If grdItems.Columns("trailerhere").Text = "" Then
        If MsgBox("Please confirm that there are still some Trailers not here", vbYesNo, "Query...") = vbNo Then
            Cancel = True
            Exit Sub
        End If
        grdItems.Columns("trailerhere").Text = grdItems.Columns("trailerreview").Text
    End If
    If grdItems.Columns("traileringest").Text = "" And grdItems.Columns("trailertranscode").Text = "" And grdItems.Columns("trailerclipmade").Text = "" Then
        If MsgBox("Please confirm that the Trailer was neither Ingested or Transcoded or Made as a Clip", vbYesNo, "Query...") = vbNo Then
            Cancel = True
            Exit Sub
        End If
    End If
End If

'If the Trailer2 has been reviewed, it must be here, and it must either have been ingested or transcoded
If grdItems.Columns("trailerreview2").Text <> "" Then
    If grdItems.Columns("trailerhere").Text = "" Then
        If MsgBox("Please confirm that there are still some Trailers not here", vbYesNo, "Query...") = vbNo Then
            Cancel = True
            Exit Sub
        End If
        grdItems.Columns("trailerhere").Text = grdItems.Columns("trailerreview2").Text
    End If
    If grdItems.Columns("traileringest2").Text = "" And grdItems.Columns("trailertranscode2").Text = "" And grdItems.Columns("trailerclipmade2").Text = "" Then
        If MsgBox("Please confirm that Trailer 2 was neither Ingested or Transcoded or Made as a Clip", vbYesNo, "Query...") = vbNo Then
            Cancel = True
            Exit Sub
        End If
    End If
End If

'If the Trailer3 has been reviewed, it must be here, and it must either have been ingested or transcoded
If grdItems.Columns("trailerreview3").Text <> "" Then
    If grdItems.Columns("trailerhere").Text = "" Then
        If MsgBox("Please confirm that there are still some Trailers not here", vbYesNo, "Query...") = vbNo Then
            Cancel = True
            Exit Sub
        End If
        grdItems.Columns("trailerhere").Text = grdItems.Columns("trailerreview3").Text
    End If
    If grdItems.Columns("traileringest3").Text = "" And grdItems.Columns("trailertranscode3").Text = "" And grdItems.Columns("trailerclipmade3").Text = "" Then
        If MsgBox("Please confirm that Trailer 3 was neither Ingested or Transcoded or Made as a Clip", vbYesNo, "Query...") = vbNo Then
            Cancel = True
            Exit Sub
        End If
    End If
End If

'If the Trailer4 has been reviewed, it must be here, and it must either have been ingested or transcoded
If grdItems.Columns("trailerreview4").Text <> "" Then
    If grdItems.Columns("trailerhere").Text = "" Then
        If MsgBox("Please confirm that there are still some Trailers not here", vbYesNo, "Query...") = vbNo Then
            Cancel = True
            Exit Sub
        End If
        grdItems.Columns("trailerhere").Text = grdItems.Columns("trailerreview4").Text
    End If
    If grdItems.Columns("traileringest4").Text = "" And grdItems.Columns("trailertranscode4").Text = "" And grdItems.Columns("trailerclipmade4").Text = "" Then
        If MsgBox("Please confirm that Trailer 4 was neither Ingested or Transcoded or Made as a Clip", vbYesNo, "Query...") = vbNo Then
            Cancel = True
            Exit Sub
        End If
    End If
End If

temp = True
If grdItems.Columns("iTunesOrderType").Text = "iTunes Film Delivery" Then
    'Check ReadyforTX and ReadyToBill - setting temp to false if any items are not ready to bill
    If ((grdItems.Columns("FeatureLanguage1").Text <> "" Or grdItems.Columns("FeatureLanguage2").Text <> "" Or grdItems.Columns("FeatureLanguage3").Text <> "" Or grdItems.Columns("FeatureLanguage4").Text <> "") _
    And grdItems.Columns("feature").Text = "") Or UCase(Right(grdItems.Columns("feature").Text, 3)) = "ERR" Then temp = False
    If ((grdItems.Columns("TrailerLanguage1").Text <> "" Or grdItems.Columns("TrailerLanguage2").Text <> "" Or grdItems.Columns("TrailerLanguage3").Text <> "" Or grdItems.Columns("TrailerLanguage4").Text <> "") _
    And grdItems.Columns("trailer").Text = "") Or UCase(Right(grdItems.Columns("trailer").Text, 3)) = "ERR" Then temp = False
    If ((grdItems.Columns("subslanguage1").Text <> "" Or grdItems.Columns("subslanguage2").Text <> "" Or grdItems.Columns("subslanguage3").Text <> "" Or grdItems.Columns("subslanguage4").Text <> "") _
    And grdItems.Columns("subs").Text = "") Or UCase(Right(grdItems.Columns("subs").Text, 3)) = "ERR" Then temp = False
    If grdItems.Columns("metadata").Text = "" Or UCase(Right(grdItems.Columns("metadata").Text, 3)) = "ERR" Then temp = False
    If ((grdItems.Columns("subslanguage1").Text <> "" Or grdItems.Columns("subslanguage2").Text <> "" Or grdItems.Columns("subslanguage3").Text <> "" Or grdItems.Columns("subslanguage4").Text <> "") _
    And grdItems.Columns("artwork").Text = "") Or UCase(Right(grdItems.Columns("artwork").Text, 3)) = "ERR" Then temp = False
    If grdItems.Columns("chapters").Text = "" Or UCase(Right(grdItems.Columns("chapters").Text, 3)) = "ERR" Then temp = False
ElseIf grdItems.Columns("iTunesOrderType").Text = "iTunes TV Delivery" Then
    If grdItems.Columns("md5_done").Text = "" Or UCase(Right(grdItems.Columns("md5_done").Text, 3)) = "ERR" _
    Or grdItems.Columns("metadata").Text = "" Or UCase(Right(grdItems.Columns("metadata").Text, 3)) = "ERR" Then temp = False
Else
    If grdItems.Columns("metadata").Text = "" Or UCase(Right(grdItems.Columns("metadata").Text, 3)) = "ERR" Then temp = False
End If
If temp = True Then
    grdItems.Columns("readyforTX").Text = 1
Else
    grdItems.Columns("readyforTX").Text = 0
End If

temp = True
If grdItems.Columns("iTunesOrderType").Text = "Disney Dub Cards" Then
    If grdItems.Columns("DisneyPlus_Dubcard").Text = "" Or grdItems.Columns("DisneyPlus_AudioDone").Text = "" Or grdItems.Columns("DisneyPlus_ForcedNarratives").Text = "" Or grdItems.Columns("DisneyPlus_Subtitles").Text = "" Then
        temp = False
    End If
End If
If temp = True Then
    If grdItems.Columns("DisneyPlus_Ready").Text = "" Then grdItems.Columns("DisneyPlus_Ready").Text = Format(Now, "YYYY-MM-DD HH:NN:SS")
Else
    grdItems.Columns("DisneyPlus_Ready").Text = ""
End If

temp = True
If grdItems.Columns("iTunesOrderType").Text = "iTunes Film Delivery" Then
    If (grdItems.Columns("package").Text <> "" And UCase(Right(grdItems.Columns("package").Text, 2)) <> "ERR") _
    And (grdItems.Columns("DeliverToMDR").Text <> "" And UCase(Right(grdItems.Columns("DeliverToMDR").Text, 2)) <> "ERR") Then
        grdItems.Columns("readytobill").Text = 1
    Else
        grdItems.Columns("readytobill").Text = 0
    End If
ElseIf grdItems.Columns("iTunesOrderType").Text = "iTunes TV Delivery" Then
    If grdItems.Columns("SentToItunes").Text = "" Or UCase(Right(grdItems.Columns("SentToiTunes").Text, 3)) = "ERR" _
    Or grdItems.Columns("TransporterLogsSent").Text = "" Or UCase(Right(grdItems.Columns("TransporterLogsSent").Text, 3)) = "ERR" Then temp = False
ElseIf grdItems.Columns("iTunesOrderType").Text = "Disney Dub Cards" Then
    If grdItems.Columns("DisneyPlus_Ready").Text = "" Or UCase(Right(grdItems.Columns("DisneyPlus_Ready").Text, 3)) = "ERR" _
    Or grdItems.Columns("DisneyPlus_Sent").Text = "" Or UCase(Right(grdItems.Columns("DisneyPlus_Sent").Text, 3)) = "ERR" Then temp = False
Else
    If grdItems.Columns("SentToItunes").Text = "" Or UCase(Right(grdItems.Columns("SentToiTunes").Text, 3)) = "ERR" Then temp = False
End If
If temp = True Then
    grdItems.Columns("readytobill").Text = 1
Else
    grdItems.Columns("readytobill").Text = 0
End If

'Check the Error Status situation
temp = True
If UCase(Right(grdItems.Columns("mastershere").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("featuretranscode").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("featureingest").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("featurereview").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("feature").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailerhere").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailertranscode").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("traileringest").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailerclipmade").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailerreview").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailersubsburned").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailertranscode2").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("traileringest2").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailerclipmade2").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailerreview2").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailersubsburned2").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailertranscode3").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("traileringest3").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailerclipmade3").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailerreview3").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailersubsburned3").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailertranscode4").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("traileringest4").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailerclipmade4").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailerreview4").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailersubsburned4").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("trailer").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("subshere").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("sub1").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("sub2").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("sub3").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("sub4").Text, 3)) = "ERR" Then temp = False
'If UCase(Right(grdItems.Columns("featureaudio2").Text, 3)) = "ERR" Then temp = False
'If UCase(Right(grdItems.Columns("featureaudio3").Text, 3)) = "ERR" Then temp = False
'If UCase(Right(grdItems.Columns("featureaudio4").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("metadatahere").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("metadata").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("artworkhere").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("artwork").Text, 3)) = "ERR" Then temp = False
'If UCase(Right(grdItems.Columns("artwork2").Text, 3)) = "ERR" Then temp = False
'If UCase(Right(grdItems.Columns("artwork3").Text, 3)) = "ERR" Then temp = False
'If UCase(Right(grdItems.Columns("artwork4").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("package").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("chapters").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("DisneyPlus_OVHere").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("DisneyPlus_ProxyHere").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("DisneyPlus_FNSubsHere").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("DisneyPlus_Docs").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("DisneyPlus_DubCard").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("DisneyPlus_AudioDone").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("DisneyPlus_ForcedNarratives").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("DisneyPlus_Subtitles").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("DisneyPlus_Ready").Text, 3)) = "ERR" Then temp = False
If UCase(Right(grdItems.Columns("DisneyPlus_Sent").Text, 3)) = "ERR" Then temp = False

If temp = False Then
    grdItems.Columns("rejected").Text = 1
'Else
'    grdItems.Columns("rejected").Text = 0
End If

'Check the Pending situation
If Val(grdItems.Columns("rejected").Text) <> 0 And (IsNull(grdItems.Columns("rejected date").Text) Or grdItems.Columns("rejected date").Text = "") Then
    rejectdatenow = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("rejected date").Text = Format(rejectdatenow, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("MostRecentRejectedDate").Text = Format(rejectdatenow, "yyyy-mm-dd hh:nn:ss")
ElseIf Val(grdItems.Columns("rejected").Text) <> 0 And grdItems.Columns("rejected date").Text <> "" And adoItems.Recordset("rejected") = 0 Then
     
    grdItems.Columns("MostRecentRejectedDate").Text = Format(Now, "yyyy-mm-dd hh:nn:ss")
    
ElseIf Val(grdItems.Columns("rejected").Text) = 0 And adoItems.Recordset("rejected") <> 0 Then
    rejectdatenow = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("offRejectedDate").Text = rejectdatenow

    rejectdateCur = FormatDateTime(grdItems.Columns("rejected date").Text, vbShortDate)
    mostrecentdateCur = FormatDateTime(grdItems.Columns("MostRecentRejectedDate").Text, vbShortDate)

'    If rejectdateCur = mostrecentdateCur Then 'rejectdatenow Then
'        grdItems.Columns("daysonRejected").Text = 1
'    Else
        grdItems.Columns("daysonRejected").Text = Val(grdItems.Columns("daysonRejected").Text) + DateDiff("d", mostrecentdateCur, CDate(grdItems.Columns("offRejectedDate").Text) + 1)
'    End If
End If

If Val(grdItems.Columns("rejected").Text) <> 0 And adoItems.Recordset("rejected") = 0 Then
    If MsgBox("Send an email about this item going on Pending?", vbYesNo, "Send Email?") = vbYes Then
        Send_Email_About_Pending Val(grdItems.Columns("tracker_itunes_itemID").Text)
    End If
End If

'Get and update the master clipID from the package ID

If (UCase(grdItems.Columns("iTunesOrderType").Text) = "ITUNES DIGITAL DELIVERY") And Val(grdItems.Columns("iTunes_packageID").Text) <> 0 Then
    grdItems.Columns("iTunes_Master_eventID").Text = GetData("iTunes_package", "fullclipID", "itunes_packageID", Val(grdItems.Columns("iTunes_packageID").Text))
    grdItems.Columns("iTunes_Trailer1_eventID").Text = GetData("iTunes_package", "previewclipID", "itunes_packageID", Val(grdItems.Columns("iTunes_packageID").Text))
    grdItems.Columns("iTunes_Trailer2_eventID").Text = GetData("iTunes_package", "previewclipID2", "itunes_packageID", Val(grdItems.Columns("iTunes_packageID").Text))
    grdItems.Columns("iTunes_Trailer3_eventID").Text = GetData("iTunes_package", "previewclipID3", "itunes_packageID", Val(grdItems.Columns("iTunes_packageID").Text))
    grdItems.Columns("iTunes_Trailer4_eventID").Text = GetData("iTunes_package", "previewclipID4", "itunes_packageID", Val(grdItems.Columns("iTunes_packageID").Text))
End If

'Get and update the file related items from the master clip ID

If Val(grdItems.Columns("iTunes_Master_eventID").Text) <> 0 Then
    'see if there is a clip record to get the duration and reference from the clipID
    l_strDuration = GetData("events", "fd_length", "eventID", Val(grdItems.Columns("iTunes_Master_eventID").Text))
    grdItems.Columns("timecodeduration").Text = l_strDuration
    grdItems.Columns("itemreference").Text = GetData("events", "clipreference", "eventID", Val(grdItems.Columns("iTunes_Master_eventID").Text))
    If l_strDuration <> "" Then
        l_lngRunningTime = 60 * Val(Mid(l_strDuration, 1, 2))
        l_lngRunningTime = l_lngRunningTime + Val(Mid(l_strDuration, 4, 2))
        If Val(Mid(l_strDuration, 7, 2)) > 30 Then
            l_lngRunningTime = l_lngRunningTime + 1
        End If
        If l_lngRunningTime = 0 Then l_lngRunningTime = 1
        If (grdItems.Columns("duration").Text = "") Or (grdItems.Columns("duration").Text <> "" And chkLockDur.Value = 0) Then
            grdItems.Columns("duration").Text = l_lngRunningTime
            
            If Val(grdItems.Columns("iTunes_trailer1_eventID").Text) <> 0 Then
                l_strDuration = GetData("events", "fd_length", "eventID", Val(grdItems.Columns("iTunes_trailer1_eventID").Text))
                If l_strDuration <> "" Then
                    l_lngRunningTime = l_lngRunningTime + (60 * Val(Mid(l_strDuration, 1, 2)))
                    l_lngRunningTime = l_lngRunningTime + Val(Mid(l_strDuration, 4, 2))
                    If Val(Mid(l_strDuration, 7, 2)) > 30 Then
                        l_lngRunningTime = l_lngRunningTime + 1
                    End If
                End If
            End If
            If Val(grdItems.Columns("iTunes_trailer2_eventID").Text) <> 0 Then
                l_strDuration = GetData("events", "fd_length", "eventID", Val(grdItems.Columns("iTunes_trailer2_eventID").Text))
                If l_strDuration <> "" Then
                    l_lngRunningTime = l_lngRunningTime + (60 * Val(Mid(l_strDuration, 1, 2)))
                    l_lngRunningTime = l_lngRunningTime + Val(Mid(l_strDuration, 4, 2))
                    If Val(Mid(l_strDuration, 7, 2)) > 30 Then
                        l_lngRunningTime = l_lngRunningTime + 1
                    End If
                End If
            End If
            If Val(grdItems.Columns("iTunes_trailer3_eventID").Text) <> 0 Then
                l_strDuration = GetData("events", "fd_length", "eventID", Val(grdItems.Columns("iTunes_trailer3_eventID").Text))
                If l_strDuration <> "" Then
                    l_lngRunningTime = l_lngRunningTime + (60 * Val(Mid(l_strDuration, 1, 2)))
                    l_lngRunningTime = l_lngRunningTime + Val(Mid(l_strDuration, 4, 2))
                    If Val(Mid(l_strDuration, 7, 2)) > 30 Then
                        l_lngRunningTime = l_lngRunningTime + 1
                    End If
                End If
            End If
            If Val(grdItems.Columns("iTunes_trailer4_eventID").Text) <> 0 Then
                l_strDuration = GetData("events", "fd_length", "eventID", Val(grdItems.Columns("iTunes_trailer4_eventID").Text))
                If l_strDuration <> "" Then
                    l_lngRunningTime = l_lngRunningTime + (60 * Val(Mid(l_strDuration, 1, 2)))
                    l_lngRunningTime = l_lngRunningTime + Val(Mid(l_strDuration, 4, 2))
                    If Val(Mid(l_strDuration, 7, 2)) > 30 Then
                        l_lngRunningTime = l_lngRunningTime + 1
                    End If
                End If
            End If
            
            grdItems.Columns("ReviewDuration").Text = l_lngRunningTime

        End If
    End If
    
    'See if there is a size to go in the GB sent column
    l_curFileSize = 0
    Set l_rst = ExecuteSQL("SELECT bigfilesize FROM events WHERE eventID = " & Val(grdItems.Columns("iTunes_Master_eventID").Text) & ";", g_strExecuteError)
    CheckForSQLError
    If l_rst.RecordCount > 0 Then
        l_rst.MoveFirst
        Do While Not l_rst.EOF
            If Not IsNull(l_rst("bigfilesize")) Then
                If l_rst("bigfilesize") > l_curFileSize Then l_curFileSize = l_rst("bigfilesize")
            End If
            l_rst.MoveNext
        Loop
    End If
    l_rst.Close
    If l_curFileSize <> 0 Then
        If (grdItems.Columns("gbsent").Text = "") Or (grdItems.Columns("gbsent").Text <> "" And chkLockGB.Value = 0) Then
            grdItems.Columns("gbsent").Text = Int(l_curFileSize / 1024 / 1024 / 1024 + 0.999)
        End If
    End If
    
    Set l_rst = Nothing
Else
    grdItems.Columns("timecodeduration").Text = ""
    grdItems.Columns("itemreference").Text = ""
    grdItems.Columns("duration").Text = ""
    grdItems.Columns("gbsent").Text = ""
End If

'Check the Complaint situation
If Val(grdItems.Columns("complainedabout").Text) <> 0 And adoItems.Recordset("complainedabout") = 0 Then
    l_strSQL = "INSERT INTO complaint (companyID, contactID, complainttype, originaliTunestrackerID, cdate, cuser, mdate, muser) VALUES (" & lblCompanyID.Caption & ", 2840, 3, " & grdItems.Columns("tracker_iTunes_itemID").Text & ", getdate(), '" & g_strFullUserName & "', getdate(), '" & g_strFullUserName & "');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
End If

'Check assets here...

temp = True
Select Case grdItems.Columns("iTunesOrderType").Text

    Case "iTunes TV Delivery"
        If grdItems.Columns("mastershere").Text = "" Or grdItems.Columns("metadatahere").Text = "" Then temp = False
    Case "Google Delivery"
        If grdItems.Columns("mastershere").Text = "" Or grdItems.Columns("metadatahere").Text = "" Then temp = False
    Case "Google OMU Delivery"
        If grdItems.Columns("mastershere").Text = "" Or grdItems.Columns("metadatahere").Text = "" Or grdItems.Columns("artworkhere").Text = "" Or grdItems.Columns("subshere").Text = "" Then temp = False
    Case "Amazon MLF Delivery"
        If grdItems.Columns("mastershere").Text = "" Or grdItems.Columns("metadatahere").Text = "" Or grdItems.Columns("artworkhere").Text = "" Or grdItems.Columns("subshere").Text = "" Then temp = False
    Case "Amazon Delivery"
        If grdItems.Columns("mastershere").Text = "" Or grdItems.Columns("metadatahere").Text = "" Or grdItems.Columns("artworkhere").Text = "" Then temp = False
    Case "iTunes Film Delivery"
        If (grdItems.Columns("FeatureLanguage1").Text <> "" Or grdItems.Columns("FeatureLanguage2").Text <> "" Or grdItems.Columns("FeatureLanguage3").Text <> "" Or grdItems.Columns("FeatureLanguage4").Text <> "") And grdItems.Columns("mastershere").Text = "" Then temp = False
        If (grdItems.Columns("TrailerLanguage1").Text <> "" Or grdItems.Columns("TrailerLanguage2").Text <> "" Or grdItems.Columns("TrailerLanguage3").Text <> "" Or grdItems.Columns("TrailerLanguage4").Text <> "") And grdItems.Columns("trailerhere").Text = "" Then temp = False
        If (grdItems.Columns("ArtworkLanguage1").Text <> "" Or grdItems.Columns("ArtworkLanguage2").Text <> "" Or grdItems.Columns("ArtworkLanguage3").Text <> "" Or grdItems.Columns("ArtworkLanguage4").Text <> "") And grdItems.Columns("artworkhere").Text = "" Then temp = False
        If (grdItems.Columns("subslanguage1").Text <> "" Or grdItems.Columns("subslanguage2").Text <> "" Or grdItems.Columns("subslanguage3").Text <> "" Or grdItems.Columns("subslanguage4").Text <> "") And grdItems.Columns("subshere").Text = "" Then temp = False
        If grdItems.Columns("metadatahere").Text = "" Then temp = False
End Select
grdItems.Columns("assetshere").Text = temp

'Do Email Notifications...

'Assets Here...

temp = False

If GetData("tracker_itunes_item", "mastershere", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("mastershere").Text Then temp = True
If GetData("tracker_itunes_item", "trailerhere", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("trailerhere").Text Then temp = True
If GetData("tracker_itunes_item", "artworkhere", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("artworkhere").Text Then temp = True
If GetData("tracker_itunes_item", "subshere", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("subshere").Text Then temp = True
If GetData("tracker_itunes_item", "metadatahere", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("metadatahere").Text Then temp = True

If grdItems.Columns("iTunesOrderType").Text = "iTunes Film Delivery" Then
    If temp = True Then 'There were somne changes, so send out messages...
        If MsgBox("Should E-mail Notices be sent out?", vbYesNo, "Some 'Assets Here' fields have changed") = vbYes Then
            l_strEmailBody = "The Assets Here status has changed for item:" & vbCrLf & "Title: " & grdItems.Columns("title").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "Order Number: " & grdItems.Columns("ordernumber").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "VendorID: " & grdItems.Columns("itunesvendorID").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "For: " & GetData("company", "name", "companyID", Val(grdItems.Columns("companyID").Text)) & vbCrLf
            l_strEmailBody = l_strEmailBody & "Feature Here: " & grdItems.Columns("mastershere").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "Trailer Here: " & grdItems.Columns("trailerhere").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "Artwork Here: " & grdItems.Columns("artworkhere").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "Subs Here: " & grdItems.Columns("subshere").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "Metadata Here: " & grdItems.Columns("metadatahere").Text & vbCrLf
    
            Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND (trackermessageID = 36);", g_strExecuteError)
            CheckForSQLError
    
            If l_rstWhoToEmail.RecordCount > 0 Then
                l_rstWhoToEmail.MoveFirst
                Do While Not l_rstWhoToEmail.EOF
                    SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "iTunes Tracker Assets Here Status Changed", "", l_strEmailBody, True, "", ""
    
                    l_rstWhoToEmail.MoveNext
                Loop
            End If
            l_rstWhoToEmail.Close
            Set l_rstWhoToEmail = Nothing
        End If
    End If
End If

'Check the Rush Job hasn't been changed and adjust target dates if it is.
If grdItems.Columns("priority").Text <> 0 And adoItems.Recordset("priority") = 0 Then
    If Val(grdItems.Columns("targetdatecountdown").Text) > 5 Then
        Count = 5 - Val(grdItems.Columns("targetdatecountdown").Text)
        grdItems.Columns("targetdatecountdown").Text = Val(grdItems.Columns("targetdatecountdown").Text) + Count
        If grdItems.Columns("TargetDate").Text <> "" Then
            Set l_rst = ExecuteSQL("exec fn_getWorkingDaysBackFromDate @fromdate='" & Format(grdItems.Columns("TargetDate").Text, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & -Count, g_strExecuteError)
            l_datNewTargetDate = l_rst(0)
            l_rst.Close
            grdItems.Columns("TargetDate").Text = l_datNewTargetDate
        End If
    End If
ElseIf grdItems.Columns("priority").Text = 0 And adoItems.Recordset("priority") <> 0 Then
    grdItems.Columns("targetdatecountdown").Text = Val(grdItems.Columns("targetdatecountdown").Text) + 5
    If Val(grdItems.Columns("targetdatecountdown").Text) > 10 Then grdItems.Columns("targetdatecountdown").Text = 10
    If grdItems.Columns("TargetDate").Text <> "" Then
        Count = Val(grdItems.Columns("targetdatecountdown").Text) - Val(adoItems.Recordset("targetdatecountdown"))
        Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(grdItems.Columns("TargetDate").Text, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Count, g_strExecuteError)
        l_datNewTargetDate = l_rst(0)
        l_rst.Close
        grdItems.Columns("TargetDate").Text = l_datNewTargetDate
    End If
End If

'Content and workable fields

If (((grdItems.Columns("FeatureLanguage1").Text <> "" Or grdItems.Columns("FeatureLanguage2").Text <> "" Or grdItems.Columns("FeatureLanguage3").Text <> "" Or grdItems.Columns("FeatureLanguage4").Text <> "") And grdItems.Columns("mastershere").Text <> "") _
Or ((grdItems.Columns("TrailerLanguage1").Text <> "" Or grdItems.Columns("TrailerLanguage2").Text <> "" Or grdItems.Columns("TrailerLanguage3").Text <> "" Or grdItems.Columns("TrailerLanguage4").Text <> "") And grdItems.Columns("trailerhere").Text <> "") _
Or ((grdItems.Columns("ArtworkLanguage1").Text <> "" Or grdItems.Columns("ArtworkLanguage2").Text <> "" Or grdItems.Columns("ArtworkLanguage3").Text <> "" Or grdItems.Columns("ArtworkLanguage4").Text <> "") And grdItems.Columns("artworkhere").Text <> "") _
Or ((grdItems.Columns("subslanguage1").Text <> "" Or grdItems.Columns("subslanguage2").Text <> "" Or grdItems.Columns("subslanguage3").Text <> "" Or grdItems.Columns("subslanguage4").Text <> "") And grdItems.Columns("subshere").Text <> "") _
Or grdItems.Columns("MetadataHere").Text <> "") And Val(grdItems.Columns("rejected").Text) = 0 Then
    If Trim(" " & grdItems.Columns("Workable").Text) = "" Then grdItems.Columns("Workable").Text = Now()
Else
    grdItems.Columns("Workable").Text = ""
End If

If grdItems.Columns("AssetsHere").Text <> 0 And Val(grdItems.Columns("rejected").Text) = 0 And grdItems.Columns("readytobill").Text = 0 Then

    Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Val(grdItems.Columns("targetdatecountdown").Text), g_strExecuteError)
    l_datNewTargetDate = l_rst(0)
    l_rst.Close
    If Trim(" " & grdItems.Columns("Completeable").Text) = "" Then grdItems.Columns("Completeable").Text = Now()
    If Trim(" " & grdItems.Columns("firstcompleteable").Text) = "" Then grdItems.Columns("firstCompleteable").Text = grdItems.Columns("Completeable").Text
    If Trim(" " & grdItems.Columns("TargetDate").Text) = "" Then grdItems.Columns("TargetDate").Text = l_datNewTargetDate

ElseIf grdItems.Columns("readytobill").Text = 0 Then

    grdItems.Columns("Completeable").Text = ""
    grdItems.Columns("TargetDate").Text = ""
        
End If

'Assets Ready...

temp = False

If GetData("tracker_itunes_item", "feature", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("feature").Text Then temp = True
If GetData("tracker_itunes_item", "trailer", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("trailer").Text Then temp = True
If GetData("tracker_itunes_item", "artwork", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("artwork").Text Then temp = True
If GetData("tracker_itunes_item", "subs", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("subs").Text Then temp = True
If GetData("tracker_itunes_item", "metadata", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("metadata").Text Then temp = True
If GetData("tracker_itunes_item", "chapters", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("chapters").Text Then temp = True

If grdItems.Columns("iTunesOrderType").Text = "iTunes Film Delivery" Then
    If temp = True Then 'There were some changes, so send out messages...
        If MsgBox("Should E-mail Notices be sent out?", vbYesNo, "Some 'Ready' fields have changed state") = vbYes Then
            l_strEmailBody = "Some 'Ready' fields have changed for item:" & vbCrLf & "Title: " & grdItems.Columns("title").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "Order Number: " & grdItems.Columns("ordernumber").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "VendorID: " & grdItems.Columns("itunesvendorID").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "For: " & GetData("company", "name", "companyID", Val(grdItems.Columns("companyID").Text)) & vbCrLf
            l_strEmailBody = l_strEmailBody & "Feature Ready: " & grdItems.Columns("feature").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "Trailer Ready: " & grdItems.Columns("trailer").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "Artwork Ready: " & grdItems.Columns("artwork").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "Subs Ready: " & grdItems.Columns("subs").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "Chapters Ready: " & grdItems.Columns("chapters").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "Metadata Ready: " & grdItems.Columns("metadata").Text & vbCrLf
    
            Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND (trackermessageID = 36);", g_strExecuteError)
            CheckForSQLError
    
            If l_rstWhoToEmail.RecordCount > 0 Then
                l_rstWhoToEmail.MoveFirst
                Do While Not l_rstWhoToEmail.EOF
                    SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "iTunes Tracker Ready Status Changed", "", l_strEmailBody, True, "", ""
    
                    l_rstWhoToEmail.MoveNext
                Loop
            End If
            l_rstWhoToEmail.Close
            Set l_rstWhoToEmail = Nothing
        End If
    End If
End If

'Packaged...

If grdItems.Columns("iTunesOrderType").Text = "iTunes Film Delivery" Then
    If GetData("tracker_itunes_item", "package", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("package").Text And grdItems.Columns("package").Text <> "" Then
    
        If MsgBox("Should E-mail Notices be sent out?", vbYesNo, "Item has been Packaged") = vbYes Then
            l_strEmailBody = "Item has been Packaged:" & vbCrLf & "Title: " & grdItems.Columns("title").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "For: " & GetData("company", "name", "companyID", Val(grdItems.Columns("companyID").Text)) & vbCrLf
            l_strEmailBody = l_strEmailBody & "Package: " & grdItems.Columns("package").Text & vbCrLf
            
            Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND (trackermessageID = 36);", g_strExecuteError)
            CheckForSQLError
            
            If l_rstWhoToEmail.RecordCount > 0 Then
                l_rstWhoToEmail.MoveFirst
                Do While Not l_rstWhoToEmail.EOF
                    SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "iTunes Tracker Item has been packaged", "", l_strEmailBody, True, "", ""
            
                    l_rstWhoToEmail.MoveNext
                Loop
            End If
            l_rstWhoToEmail.Close
            Set l_rstWhoToEmail = Nothing
        End If
    
    End If
End If

'Sent to MDR...
If grdItems.Columns("iTunesORderType").Text = "iTunes Film Delivery" Then
    If GetData("tracker_itunes_item", "DeliverToMDR", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("DeliverToMDR").Text And grdItems.Columns("DeliverToMDR").Text <> "" And UCase(Right(grdItems.Columns("DeliverToMDR").Text, 2)) <> "ERR" Then
    
        If MsgBox("Should E-mail Notices be sent out?", vbYesNo, "Item has been Sent to MDR") = vbYes Then
            l_strEmailBody = "Item has been Sent to MDR:" & vbCrLf & "Title: " & grdItems.Columns("title").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "Order Number: " & grdItems.Columns("ordernumber").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "VendorID: " & grdItems.Columns("itunesvendorID").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "For: " & GetData("company", "name", "companyID", Val(grdItems.Columns("companyID").Text)) & vbCrLf
            l_strEmailBody = l_strEmailBody & "Sent to MDR: " & grdItems.Columns("DeliverToMDR").Text & vbCrLf
            
            Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND (trackermessageID = 36);", g_strExecuteError)
            CheckForSQLError
            
            If l_rstWhoToEmail.RecordCount > 0 Then
                l_rstWhoToEmail.MoveFirst
                Do While Not l_rstWhoToEmail.EOF
                    SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "iTunes Tracker Item has been sent to MDR", "", l_strEmailBody, True, "", ""
            
                    l_rstWhoToEmail.MoveNext
                Loop
            End If
            l_rstWhoToEmail.Close
            Set l_rstWhoToEmail = Nothing
        End If
        l_strEmailBody = "Item has been Sent to MDR:" & vbCrLf & "Title: " & grdItems.Columns("title").Text & vbCrLf
        l_strEmailBody = l_strEmailBody & "For: " & GetData("company", "name", "companyID", Val(grdItems.Columns("companyID").Text)) & vbCrLf
        l_strEmailBody = l_strEmailBody & "Sent to MDR: " & grdItems.Columns("DeliverToMDR").Text & vbCrLf
        
        Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND (trackermessageID = 42);", g_strExecuteError)
        CheckForSQLError
        
        If l_rstWhoToEmail.RecordCount > 0 Then
            l_rstWhoToEmail.MoveFirst
            Do While Not l_rstWhoToEmail.EOF
                SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "iTunes Tracker Item has been sent to MDR", "", l_strEmailBody, True, "", ""
        
                l_rstWhoToEmail.MoveNext
            Loop
        End If
        l_rstWhoToEmail.Close
        Set l_rstWhoToEmail = Nothing
    
    End If
End If

'Feature Transcode or Ingest

If grdItems.Columns("iTunesORderType").Text = "iTunes Film Delivery" Then
    temp = False
    
    If GetData("tracker_itunes_item", "featuretranscode", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("featuretranscode").Text Then temp = True
    If GetData("tracker_itunes_item", "featureingest", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("featureingest").Text Then temp = True
    
    If temp = True Then 'There were somne changes, so send out messages...
        If MsgBox("Should E-mail Notices be sent out?", vbYesNo, "Feature Transocde or Ingest has changed") = vbYes Then
            l_strEmailBody = "Feature Transcode or Ingest has been changed:" & vbCrLf & "Title: " & grdItems.Columns("title").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "For: " & GetData("company", "name", "companyID", Val(grdItems.Columns("companyID").Text)) & vbCrLf
            l_strEmailBody = l_strEmailBody & "Feature Transocde: " & grdItems.Columns("featuretranscode").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "Feature Ingest: " & grdItems.Columns("featureingest").Text & vbCrLf
    
            Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND trackermessageID = 37;", g_strExecuteError)
            CheckForSQLError
    
            If l_rstWhoToEmail.RecordCount > 0 Then
                l_rstWhoToEmail.MoveFirst
                Do While Not l_rstWhoToEmail.EOF
                    SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "iTunes Tracker Feature Transocde or Ingest Status Changed", "", l_strEmailBody, True, "", ""
    
                    l_rstWhoToEmail.MoveNext
                Loop
            End If
            l_rstWhoToEmail.Close
            Set l_rstWhoToEmail = Nothing
        End If
    End If
End If

'Feature Review

If grdItems.Columns("iTunesORderType").Text = "iTunes Film Delivery" Then
    If GetData("tracker_itunes_item", "featurereview", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("featurereview").Text Then
        If MsgBox("Should E-mail Notices be sent out?", vbYesNo, "Feature Review has changed") = vbYes Then
            l_strEmailBody = "Feature Review has changed for item:" & vbCrLf & "Title: " & grdItems.Columns("title").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "For: " & GetData("company", "name", "companyID", Val(grdItems.Columns("companyID").Text)) & vbCrLf
            l_strEmailBody = l_strEmailBody & "Feature Review: " & grdItems.Columns("featurereview").Text & vbCrLf
    
            Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND trackermessageID = 37;", g_strExecuteError)
            CheckForSQLError
    
            If l_rstWhoToEmail.RecordCount > 0 Then
                l_rstWhoToEmail.MoveFirst
                Do While Not l_rstWhoToEmail.EOF
                    SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "iTunes Tracker Feature Review Status Changed", "", l_strEmailBody, True, "", ""
    
                    l_rstWhoToEmail.MoveNext
                Loop
            End If
            l_rstWhoToEmail.Close
            Set l_rstWhoToEmail = Nothing
        End If
    End If
End If

'Trailer Ingest, Transocde or Make...

If grdItems.Columns("iTunesORderType").Text = "iTunes Film Delivery" Then
    temp = False
    
    If GetData("tracker_itunes_item", "trailertranscode", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("trailertranscode").Text Then temp = True
    If GetData("tracker_itunes_item", "traileringest", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("traileringest").Text Then temp = True
    If GetData("tracker_itunes_item", "trailerclipmade", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("trailerclipmade").Text Then temp = True
    
    If temp = True Then 'There were somne changes, so send out messages...
        If MsgBox("Should E-mail Notices be sent out?", vbYesNo, "Trailer Transcode, Ingest or Clip Made Status has changed") = vbYes Then
            l_strEmailBody = "Trailer Transcode, Ingest or Clip Made Status has changed:" & vbCrLf & "Title: " & grdItems.Columns("title").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "For: " & GetData("company", "name", "companyID", Val(grdItems.Columns("companyID").Text)) & vbCrLf
            l_strEmailBody = l_strEmailBody & "Trailer Transcode: " & grdItems.Columns("trailertranscode").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "Trailer Ingest: " & grdItems.Columns("traileringest").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "Trailer Clip Made: " & grdItems.Columns("trailerclipmade").Text & vbCrLf
    
            Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND trackermessageID = 37;", g_strExecuteError)
            CheckForSQLError
    
            If l_rstWhoToEmail.RecordCount > 0 Then
                l_rstWhoToEmail.MoveFirst
                Do While Not l_rstWhoToEmail.EOF
                    SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "iTunes Tracker Trailer Transcode, Ingest or Clip Made Status Changed", "", l_strEmailBody, True, "", ""
    
                    l_rstWhoToEmail.MoveNext
                Loop
            End If
            l_rstWhoToEmail.Close
            Set l_rstWhoToEmail = Nothing
        End If
    End If
End If

'Trailer Review

If grdItems.Columns("iTunesORderType").Text = "iTunes Film Delivery" Then
    If GetData("tracker_itunes_item", "trailerreview", "tracker_itunes_itemID", Val(lblTrackeritemID)) <> grdItems.Columns("trailerreview").Text Then
        If MsgBox("Should E-mail Notices be sent out?", vbYesNo, "Trailer Review has changed") = vbYes Then
            l_strEmailBody = "Trailer Review has changed for item:" & vbCrLf & "Title: " & grdItems.Columns("title").Text & vbCrLf
            l_strEmailBody = l_strEmailBody & "For: " & GetData("company", "name", "companyID", Val(grdItems.Columns("companyID").Text)) & vbCrLf
            l_strEmailBody = l_strEmailBody & "Trailer Review: " & grdItems.Columns("trailerreview").Text & vbCrLf
    
            Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND trackermessageID = 37;", g_strExecuteError)
            CheckForSQLError
    
            If l_rstWhoToEmail.RecordCount > 0 Then
                l_rstWhoToEmail.MoveFirst
                Do While Not l_rstWhoToEmail.EOF
                    SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "iTunes Tracker Trailer Review Status Changed", "", l_strEmailBody, True, "", ""
    
                    l_rstWhoToEmail.MoveNext
                Loop
            End If
            l_rstWhoToEmail.Close
            Set l_rstWhoToEmail = Nothing
        End If
    End If
End If


End Sub

Private Sub grdItems_BtnClick()

Dim tempdate As String, l_lngPackageID As Long, l_strSQL As String, l_varEventID As Variant

Select Case LCase(grdItems.Columns(grdItems.Col).Name)

Case "barcode"
    
    MakeClipFromDADCTrackerEvent grdItems.Columns("tracker_iTunes_itemID").Text

Case "make package"
    
    If Val(lblTrackeritemID.Caption) <> 0 Then
        l_lngPackageID = GetData("tracker_itunes_item", "iTunes_packageID", "tracker_itunes_itemID", Val(lblTrackeritemID.Caption))
        If l_lngPackageID <> 0 Then
            If GetData("itunes_package", "itunes_packageID", "itunes_packageID", l_lngPackageID) <> 0 Then
                ShowiTunesPackage l_lngPackageID
            End If
        Else '
            l_strSQL = "INSERT INTO itunes_package (companyID, video_type, video_vendorID, video_title) VALUES ("
            l_strSQL = l_strSQL & Val(lblCompanyID.Caption) & ", "
            If UCase(grdItems.Columns("itunesordertype").Text) = "ITUNES DIGITAL DELIVERY" Or UCase(grdItems.Columns("itunesordertype").Text) = "ITUNES FILM DELIVERY" Or UCase(grdItems.Columns("itunesordertype").Text) = "FILM" Or UCase(grdItems.Columns("itunesordertype").Text) = "FEATURE" Then
                l_strSQL = l_strSQL & "'film', "
'            ElseIf UCase(grdItems.Columns("itunesordertype").Text) = "MAXDOME DIGITAL DELIVERY" Then
'                l_strSQL = l_strSQL & "'Maxdome Non-Episodic', "
            Else
                l_strSQL = l_strSQL & "'" & grdItems.Columns("itunesordertype").Text & "', "
            End If
            l_strSQL = l_strSQL & "'" & grdItems.Columns("iTunesVendorID").Text & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("title").Text) & "');"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            l_lngPackageID = g_lngLastID
            'grdItems.Columns("iTunes_PackageID").Text = l_lngPackageID
            SetData "tracker_itunes_item", "itunes_packageID", "tracker_itunes_itemID", Val(lblTrackeritemID.Caption), l_lngPackageID
            ShowiTunesPackage l_lngPackageID
        End If
    End If
            
Case "itunes_master_eventid"
    
    l_strSQL = "SELECT TOP 1 eventID FROM events WHERE (companyID = " & Val(grdItems.Columns("companyID").Text) & ") AND (eventtitle = '" & QuoteSanitise(grdItems.Columns("title").Text) & "') "
    If Left(grdItems.Columns("iTunesOrderType").Text, 3) = "AON" Then
        l_strSQL = l_strSQL & "AND (clipfilename LIKE 'AON%') "
    ElseIf Left(grdItems.Columns("iTunesOrderType").Text, 4) = "Real" Then
        l_strSQL = l_strSQL & "AND (clipfilename LIKE 'Real%') AND (clipfilename NOT LIKE '%_clip_%') AND (clipfilename NOT LIKE '%_Trail%') "
    End If
    l_strSQL = l_strSQL & "AND (fd_length IS NOT NULL) AND (fd_length <> '');"
    l_varEventID = GetDataSQL(l_strSQL)
    If IsNumeric(l_varEventID) Then
        If l_varEventID <> 0 Then
            grdItems.Columns("iTunes_Master_eventID").Text = l_varEventID
        End If
    End If

Case Else
    
    If grdItems.ActiveCell.Text <> "" Then
        grdItems.ActiveCell.Text = ""
    Else
        If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
            grdItems.ActiveCell.Text = Format(Now, "YYYY-MM-DD HH:NN:SS")
        Else
            tempdate = FormatDateTime(Now, vbLongDate)
            grdItems.ActiveCell.Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
        End If
    End If

End Select

End Sub

Private Sub grdItems_DblClick()

If grdItems.Columns(grdItems.Col).Name = "jobID" Then
    If grdItems.Columns("jobID").Text <> "" Then
        ShowJob Val(grdItems.Columns("jobID").Text), 1, True
'        ShowJob Val(grdItems.Columns("jobID").Text), 1, True
        Exit Sub
    End If
End If

If grdItems.Columns("itemreference").Text <> "" Then
    ShowClipSearch "", grdItems.Columns("itemreference").Text
ElseIf grdItems.Columns("title").Text <> "" Then
    ShowClipSearch "", "", "", grdItems.Columns("title").Text
End If

End Sub

Private Sub grdItems_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim l_strSQL As String

If m_blnSilent = False Then
    lblTrackeritemID.Caption = grdItems.Columns("tracker_itunes_itemID").Text
    lblCompanyID.Caption = grdItems.Columns("companyID").Text

    If Val(lblTrackeritemID.Caption) <> Val(lblLastTrackerItemID.Caption) Then
        
        If Val(lblTrackeritemID.Caption) = 0 Then
            
            l_strSQL = "SELECT * FROM tracker_itunes_comment WHERE tracker_itunes_itemID = -1 ORDER BY cdate;"
            
            adoComments.RecordSource = l_strSQL
            adoComments.ConnectionString = g_strConnection
            adoComments.Refresh
            
            lblLastTrackerItemID.Caption = ""
        
            Exit Sub
        
        End If
        
        l_strSQL = "SELECT * FROM tracker_itunes_comment WHERE tracker_itunes_itemID = " & lblTrackeritemID.Caption & " ORDER BY cdate;"
        
        adoComments.RecordSource = l_strSQL
        adoComments.ConnectionString = g_strConnection
        adoComments.Refresh
    
        lblLastTrackerItemID.Caption = lblTrackeritemID.Caption
        
        If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdubcards") > 0 Then
            l_strSQL = "SELECT distinct language, tracker_itemID FROM tracker_dubcard WHERE tracker_itemID = " & lblTrackeritemID.Caption
            adoDubCards.RecordSource = l_strSQL
            adoDubCards.ConnectionString = g_strConnection
            adoDubCards.Refresh
            grdDubcards.Visible = True
        Else
            grdDubcards.Visible = False
        End If
        
    End If

End If

End Sub

Private Sub grdItems_RowLoaded(ByVal Bookmark As Variant)

'Set the error states on the individual columns that can have errors

grdItems.Columns("channel").Text = GetData("company", "portalcompanyname", "companyID", Val(grdItems.Columns("companyID").Text))

If Val(grdItems.Columns("rejected").Text) <> 0 Then
    grdItems.Columns("rejected").CellStyleSet "Error"
End If

If Val(grdItems.Columns("priority").Text) <> 0 Then
    grdItems.Columns("priority").CellStyleSet "Priority"
End If

If UCase(Right(grdItems.Columns("mastershere").Text, 3)) = "ERR" Then
    grdItems.Columns("mastershere").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("trailerhere").Text, 3)) = "ERR" Then
    grdItems.Columns("trailerhere").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("artworkhere").Text, 3)) = "ERR" Then
    grdItems.Columns("artworkhere").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("subshere").Text, 3)) = "ERR" Then
    grdItems.Columns("subshere").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("metadatahere").Text, 3)) = "ERR" Then
    grdItems.Columns("metadatahere").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("featuretranscode").Text, 3)) = "ERR" Then
    grdItems.Columns("featuretranscode").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("featureingest").Text, 3)) = "ERR" Then
    grdItems.Columns("featureingest").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("featurereview").Text, 3)) = "ERR" Then
    grdItems.Columns("featurereview").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("feature").Text, 3)) = "ERR" Then
    grdItems.Columns("feature").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("TrailerTranscode").Text, 3)) = "ERR" Then
    grdItems.Columns("TrailerTranscode").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("TrailerIngest").Text, 3)) = "ERR" Then
    grdItems.Columns("TrailerIngest").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("TrailerClipMade").Text, 3)) = "ERR" Then
    grdItems.Columns("TrailerClipMade").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("TrailerTranscode2").Text, 3)) = "ERR" Then
    grdItems.Columns("TrailerTranscode2").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("TrailerIngest2").Text, 3)) = "ERR" Then
    grdItems.Columns("TrailerIngest2").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("TrailerClipMade2").Text, 3)) = "ERR" Then
    grdItems.Columns("TrailerClipMade2").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("TrailerTranscode3").Text, 3)) = "ERR" Then
    grdItems.Columns("TrailerTranscode3").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("TrailerIngest3").Text, 3)) = "ERR" Then
    grdItems.Columns("TrailerIngest3").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("TrailerClipMade3").Text, 3)) = "ERR" Then
    grdItems.Columns("TrailerClipMade3").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("TrailerTranscode4").Text, 3)) = "ERR" Then
    grdItems.Columns("TrailerTranscode4").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("TrailerIngest4").Text, 3)) = "ERR" Then
    grdItems.Columns("TrailerIngest4").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("TrailerClipMade4").Text, 3)) = "ERR" Then
    grdItems.Columns("TrailerClipMade4").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("trailerreview").Text, 3)) = "ERR" Then
    grdItems.Columns("trailerreview").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("trailerreview2").Text, 3)) = "ERR" Then
    grdItems.Columns("trailerreview2").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("trailerreview3").Text, 3)) = "ERR" Then
    grdItems.Columns("trailerreview3").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("trailerreview4").Text, 3)) = "ERR" Then
    grdItems.Columns("trailerreview4").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("TrailerSubsBurned").Text, 3)) = "ERR" Then
    grdItems.Columns("TrailerSubsBurned").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("TrailerSubsBurned2").Text, 3)) = "ERR" Then
    grdItems.Columns("TrailerSubsBurned2").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("TrailerSubsBurned3").Text, 3)) = "ERR" Then
    grdItems.Columns("TrailerSubsBurned3").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("TrailerSubsBurned4").Text, 3)) = "ERR" Then
    grdItems.Columns("TrailerSubsBurned4").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("trailer").Text, 3)) = "ERR" Then
    grdItems.Columns("trailer").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("sub1").Text, 3)) = "ERR" Then
    grdItems.Columns("sub1").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("sub2").Text, 3)) = "ERR" Then
    grdItems.Columns("sub2").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("sub3").Text, 3)) = "ERR" Then
    grdItems.Columns("sub3").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("sub4").Text, 3)) = "ERR" Then
    grdItems.Columns("sub4").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("subs").Text, 3)) = "ERR" Then
    grdItems.Columns("subs").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("metadata").Text, 3)) = "ERR" Then
    grdItems.Columns("metadata").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("chapters").Text, 3)) = "ERR" Then
    grdItems.Columns("chapters").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("artwork").Text, 3)) = "ERR" Then
    grdItems.Columns("artwork").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("package").Text, 3)) = "ERR" Then
    grdItems.Columns("package").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("inthequeue").Text, 3)) = "ERR" Then
    grdItems.Columns("inthequeue").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("delivertomdr").Text, 3)) = "ERR" Then
    grdItems.Columns("delivertomdr").CellStyleSet "Error"
End If
If UCase(Right(grdItems.Columns("delivertomodern").Text, 3)) = "ERR" Then
    grdItems.Columns("delivertomodern").CellStyleSet "Error"
End If

If grdItems.Columns("SubsLanguage1").Text = "" And grdItems.Columns("SubsLanguage2").Text = "" And grdItems.Columns("SubsLanguage3").Text = "" And grdItems.Columns("SubsLanguage4").Text = "" Then
    grdItems.Columns("subshere").CellStyleSet "NotRequired"
    grdItems.Columns("sub1").CellStyleSet "NotRequired"
    grdItems.Columns("sub2").CellStyleSet "NotRequired"
    grdItems.Columns("sub3").CellStyleSet "NotRequired"
    grdItems.Columns("sub4").CellStyleSet "NotRequired"
    grdItems.Columns("subs").CellStyleSet "NotRequired"
End If

If grdItems.Columns("SubsLanguage1").Text = "" Then
    grdItems.Columns("sub1").CellStyleSet "NotRequired"
End If
If grdItems.Columns("SubsLanguage2").Text = "" Then
    grdItems.Columns("sub2").CellStyleSet "NotRequired"
End If
If grdItems.Columns("SubsLanguage3").Text = "" Then
    grdItems.Columns("sub3").CellStyleSet "NotRequired"
End If
If grdItems.Columns("SubsLanguage4").Text = "" Then
    grdItems.Columns("sub4").CellStyleSet "NotRequired"
End If

If grdItems.Columns("FeatureLanguage1").Text = "" And grdItems.Columns("FeatureLanguage2").Text = "" And grdItems.Columns("FeatureLanguage3").Text = "" And grdItems.Columns("FeatureLanguage4").Text = "" Then
    grdItems.Columns("mastershere").CellStyleSet "NotRequired"
    grdItems.Columns("feature").CellStyleSet "NotRequired"
End If
If grdItems.Columns("TrailerLanguage1").Text = "" And grdItems.Columns("TrailerLanguage2").Text = "" And grdItems.Columns("TrailerLanguage3").Text = "" And grdItems.Columns("TrailerLanguage4").Text = "" Then
    grdItems.Columns("trailerhere").CellStyleSet "NotRequired"
    grdItems.Columns("trailer").CellStyleSet "NotRequired"
End If
If grdItems.Columns("ArtworkLanguage1").Text = "" And grdItems.Columns("ArtworkLanguage2").Text = "" And grdItems.Columns("ArtworkLanguage3").Text = "" And grdItems.Columns("ArtworkLanguage4").Text = "" Then
    grdItems.Columns("artworkhere").CellStyleSet "NotRequired"
    grdItems.Columns("Artwork").CellStyleSet "NotRequired"
End If
If grdItems.Columns("trailercreate").Text <> "" Then
    grdItems.Columns("trailerhere").CellStyleSet "Priority"
    If grdItems.Columns("trailerlanguage1").Text <> "" Then grdItems.Columns("TrailerClipMade").CellStyleSet "Priority"
    If grdItems.Columns("trailerlanguage2").Text <> "" Then grdItems.Columns("TrailerClipMade2").CellStyleSet "Priority"
    If grdItems.Columns("trailerlanguage3").Text <> "" Then grdItems.Columns("TrailerClipMade3").CellStyleSet "Priority"
    If grdItems.Columns("trailerlanguage4").Text <> "" Then grdItems.Columns("TrailerClipMade4").CellStyleSet "Priority"
End If

End Sub

Private Sub optDeliveryType_Click(Index As Integer)

'Conditional_Assets_Columns

cmdSearch.Value = True

Select Case Index
    Case 0
        cmdFieldsSummary.Value = True
    Case 6
        cmdiTunesTV.Value = True
    Case 7
        cmdGoogleFields.Value = True
    Case 10
        cmdGoogleOMUFields.Value = True
    Case 8
        cmdAmazonFields.Value = True
    Case 9
        cmdAmazonMLFFields.Value = True
    Case 1
        cmdXbox.Value = True
    Case 11
        cmdFieldsDisneyPlus.Value = True
    Case Else
        'Do Nothing
End Select

End Sub

Private Sub optComplete_Click(Index As Integer)

cmdSearch.Value = True

End Sub

Function CheckReadyForTX() As Boolean

Dim temp As Boolean

temp = True

If grdItems.Columns("masterarrived").Text = "" Or UCase(Right(grdItems.Columns("masterarrived").Text, 3)) = "ERR" Then temp = False
If grdItems.Columns("broadcastvisionmade").Text = "" Or UCase(Right(grdItems.Columns("broadcastvisionmade").Text, 3)) = "ERR" Then temp = False
'If grdItems.Columns("senttosoundhouse").Text = "" Or UCase(Right(grdItems.Columns("senttosoundhouse").Text, 3)) = "ERR" Then temp = False
'If Val(grdItems.Columns("englishaudioneeded").Text) <> 0 Then
'    If grdItems.Columns("englishaudioarrived").Text = "" Or UCase(Right(grdItems.Columns("englishaudioarrived").Text, 3)) = "ERR" Then temp = False
'End If
'If Val(grdItems.Columns("frenchaudioneeded").Text) <> 0 Then
'    If grdItems.Columns("frenchaudioarrived").Text = "" Or UCase(Right(grdItems.Columns("frenchaudioarrived").Text, 3)) = "ERR" Then temp = False
'End If
'If Val(grdItems.Columns("brazillianportaudioneeded").Text) <> 0 Then
'    If grdItems.Columns("brazillianportaudioarrived").Text = "" Or UCase(Right(grdItems.Columns("brazillianportaudioarrived").Text, 3)) = "ERR" Then temp = False
'End If

CheckReadyForTX = temp

End Function

Function BillItemPartOne() As Boolean

Dim l_strChargeCode As String, l_rstTrackerChargeCode As ADODB.Recordset, l_strJobLine As String, l_lngJobID As Long, l_lngDuration As Long, l_lngMinuteBilling As Long
Dim l_lngQuantity As Long, l_strCode As String, l_lngFactor As Long, l_blnFirstItem As Boolean, l_blnBulkBilling As Boolean, l_lngLoggingItemCount As Long
Dim l_lngEventID As Long, l_strLastComment As String, l_strLastCommentCuser As String, l_datLastCommentDate As Date, l_blnProblemEncountered As Boolean

l_blnFirstItem = True
m_blnBilling = True
If frmJob.txtJobID.Text <> "" Then
    'A job is loaded - now check that it isn't locked.
    l_lngJobID = Val(frmJob.txtJobID.Text)
    'Check that a company has been chosen in the Drop down - we can only add to a job sheet of the correct company
    If cmbCompany.Text <> "" Then
        If frmJob.txtStatus.Text = "Confirmed" And frmJob.lblCompanyID.Caption = cmbCompany.Columns("companyID").Text Then
            'The Job is valid - go ahead and create Job lines for this item.
            l_lngQuantity = 0
            l_strJobLine = ""
            If grdItems.Columns("title").Text <> "" Then l_strJobLine = l_strJobLine & grdItems.Columns("title").Text
            If grdItems.Columns("Series").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("series").Text
            If grdItems.Columns("episode").Text <> "" Then l_strJobLine = l_strJobLine & " - Ep " & grdItems.Columns("episode").Text
            If grdItems.Columns("iTunesVendorID").Text <> "" Then l_strJobLine = l_strJobLine & " - VendorID " & grdItems.Columns("iTunesVendorID").Text
            If grdItems.Columns("readytobill").Text <> "" Then
                'DADC iTunes Tracker Billing
                'Many stagefields need checking and the appropriate job lines added if thery are set.
                If grdItems.Columns("iTunesOrderType").Text <> "AON Telekom digital delivery" And grdItems.Columns("iTunesOrderType").Text <> "Realeyz.TV digital delivery" Then
                    l_blnProblemEncountered = False
                    If grdItems.Columns("FeatureTranscode").Text <> "" Then
                        'We have a feature Transcode
                        If grdItems.Columns("duration").Text <> "" Then
                            l_strCode = "I"
                            l_lngDuration = Val(grdItems.Columns("duration").Text)
                            l_lngQuantity = 1
                            If adoComments.Recordset.RecordCount > 0 Then
                                adoComments.Recordset.MoveLast
                                l_strLastCommentCuser = adoComments.Recordset("cuser")
                                l_datLastCommentDate = adoComments.Recordset("cdate")
                                l_strLastComment = adoComments.Recordset("comment")
                            Else
                                l_strLastCommentCuser = ""
                                l_datLastCommentDate = 0
                                l_strLastComment = ""
                            End If
                            If grdItems.Columns("featureHD").Text <> 0 Then
                                l_strChargeCode = "DADCTRANBHD"
                            Else
                                l_strChargeCode = "DADCTRANBSD"
                            End If
                            If l_strLastComment <> "" Then
                                If grdItems.Columns("featureHD").Text <> 0 Then
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                                Else
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                                End If
                            Else
                                If grdItems.Columns("featureHD").Text <> 0 Then
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                                Else
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                                End If
                            End If
                        Else
                            MsgBox "The current Item must have a duration in order to bill it.", vbCritical, "Cannot Bill Item"
                            l_blnProblemEncountered = True
                        End If
                    End If
                    If grdItems.Columns("FeatureSepAudioTranscode").Text <> "" Then
                        'We have a feature separate Audio file Transcode
                        If grdItems.Columns("duration").Text <> "" Then
                            l_strCode = "I"
                            l_lngDuration = Val(grdItems.Columns("duration").Text)
                            l_lngQuantity = Val(grdItems.Columns("FeatureSepAudioTranscode").Text)
                            If adoComments.Recordset.RecordCount > 0 Then
                                adoComments.Recordset.MoveLast
                                l_strLastCommentCuser = adoComments.Recordset("cuser")
                                l_datLastCommentDate = adoComments.Recordset("cdate")
                                l_strLastComment = adoComments.Recordset("comment")
                            Else
                                l_strLastCommentCuser = ""
                                l_datLastCommentDate = 0
                                l_strLastComment = ""
                            End If
                            If l_strLastComment <> "" Then
                                If grdItems.Columns("featureHD").Text <> 0 Then
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Additional Audio Transcode", l_lngQuantity, "DADCTRANBSD", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                                Else
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Additional Audio Transcode", l_lngQuantity, "DADCTRANBSD", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                                End If
                            Else
                                If grdItems.Columns("featureHD").Text <> 0 Then
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Additional Audio Transcode", l_lngQuantity, "DADCTRANBSD", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                                Else
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Additional Audio Transcode", l_lngQuantity, "DADCTRANBSD", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                                End If
                            End If
                        Else
                            MsgBox "The current Item must have a duration in order to bill it.", vbCritical, "Cannot Bill Item"
                            l_blnProblemEncountered = True
                        End If
                    End If
                    If grdItems.Columns("FeatureReview").Text <> "" Then
                        'We have a feature review
                        If grdItems.Columns("duration").Text <> "" Then
                            l_strCode = "I"
                            l_lngDuration = Val(grdItems.Columns("duration").Text)
                            l_lngQuantity = 1
                            If grdItems.Columns("FeatureReview2Pass").Text <> 0 Then
                                l_strChargeCode = "DADCITUNESREVIEW2P"
                            Else
                                l_strChargeCode = "DADCITUNESREVIEW"
                            End If
                            If adoComments.Recordset.RecordCount > 0 Then
                                adoComments.Recordset.MoveLast
                                l_strLastCommentCuser = adoComments.Recordset("cuser")
                                l_datLastCommentDate = adoComments.Recordset("cdate")
                                l_strLastComment = adoComments.Recordset("comment")
                            Else
                                l_strLastCommentCuser = ""
                                l_datLastCommentDate = 0
                                l_strLastComment = ""
                            End If
                            If l_strLastComment <> "" Then
                                If grdItems.Columns("featureHD").Text <> 0 Then
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                                Else
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                                End If
                            Else
                                If grdItems.Columns("featureHD").Text <> 0 Then
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                                Else
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                                End If
                            End If
                        Else
                            MsgBox "The current Item must have a duration in order to bill it.", vbCritical, "Cannot Bill Item"
                            l_blnProblemEncountered = True
                        End If
                    End If
                    If grdItems.Columns("FeatureSubsIngest").Text <> "" Then
                        'We have a subs ingest and check
                        If Val(grdItems.Columns("FeatureSubsIngest").Text) <> 0 Then
                            l_strCode = "O"
                            l_lngQuantity = Val(grdItems.Columns("FeatureSubsIngest").Text)
                            l_lngDuration = 0
                            If adoComments.Recordset.RecordCount > 0 Then
                                adoComments.Recordset.MoveLast
                                l_strLastCommentCuser = adoComments.Recordset("cuser")
                                l_datLastCommentDate = adoComments.Recordset("cdate")
                                l_strLastComment = adoComments.Recordset("comment")
                            Else
                                l_strLastCommentCuser = ""
                                l_datLastCommentDate = 0
                                l_strLastComment = ""
                            End If
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Subs Ingest", 1, "DADCSUBSCHECK", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, grdItems.Columns("featurehd").Text, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            l_lngQuantity = l_lngQuantity - 1
                            If l_lngQuantity > 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Subs Ingest", l_lngQuantity, "DADCSUBSCHECK2+", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, grdItems.Columns("featurehd").Text, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            MsgBox "The We must have a quantity of Subs files Ingested in the 'Feat Subs Ingest'.", vbCritical, "Cannot Bill Item"
                            l_blnProblemEncountered = True
                        End If
                    End If
                    If grdItems.Columns("TrailerTranscode").Text <> "" Then
                        'We have a Trailer Transcode
                        l_strCode = "I"
                        l_lngDuration = 15
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("trailerHD").Text <> 0 Then
                            l_strChargeCode = "DADCTRANBHD"
                        Else
                            l_strChargeCode = "DADCTRANBSD"
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    ElseIf grdItems.Columns("TrailerClipMade").Text <> "" Then
                        'We have a Trailer Clip Made
                        l_strCode = "O"
                        l_lngDuration = 0
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("trailerHD").Text <> 0 Then
                            l_strChargeCode = "DADCITUNESCLIPHD"
                        Else
                            l_strChargeCode = "DADCITUNESCLIP"
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    ElseIf grdItems.Columns("TrailerIngest").Text <> "" Then
                        'We have a Trailer Clip Ingested
                        l_strCode = "O"
                        l_lngDuration = 0
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("trailerHD").Text <> 0 Then
                            l_strChargeCode = "DADCITUNESCLIPHD"
                        Else
                            l_strChargeCode = "DADCITUNESCLIP"
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    End If
                    If grdItems.Columns("TrailerReview").Text <> "" And grdItems.Columns("TrailerClipMade").Text = "" And grdItems.Columns("TrailerIngest").Text = "" Then
                        'We have a Trailer review
                        l_strCode = "I"
                        l_lngDuration = 15
                        l_lngQuantity = 1
                        If grdItems.Columns("TrailerReview2Pass").Value <> 0 Then
                            l_strChargeCode = "DADCITUNESREVIEW2P"
                        Else
                            l_strChargeCode = "DADCITUNESREVIEW"
                        End If
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    End If
                    If grdItems.Columns("TrailerTranscode2").Text <> "" Then
                        'We have a trailer Transcode
                        l_strCode = "I"
                        l_lngDuration = 15
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("trailerHD").Text <> 0 Then
                            l_strChargeCode = "DADCTRANBHD"
                        Else
                            l_strChargeCode = "DADCTRANBSD"
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    ElseIf grdItems.Columns("TrailerClipMade2").Text <> "" Then
                        'We have a Trailer Clip Made
                        l_strCode = "O"
                        l_lngDuration = 0
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("trailerHD").Text <> 0 Then
                            l_strChargeCode = "DADCITUNESCLIPHD"
                        Else
                            l_strChargeCode = "DADCITUNESCLIP"
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    ElseIf grdItems.Columns("TrailerIngest2").Text <> "" Then
                        'We have a Trailer Clip Ingested
                        l_strCode = "O"
                        l_lngDuration = 0
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("trailerHD").Text <> 0 Then
                            l_strChargeCode = "DADCITUNESCLIPHD"
                        Else
                            l_strChargeCode = "DADCITUNESCLIP"
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    End If
                    If grdItems.Columns("TrailerReview2").Text <> "" And grdItems.Columns("TrailerClipMade2").Text = "" And grdItems.Columns("TrailerIngest2").Text = "" Then
                        'We have a Trailer review
                        l_strCode = "I"
                        l_lngDuration = 15
                        l_lngQuantity = 1
                        If grdItems.Columns("TrailerReview2Pass2").Value <> 0 Then
                            l_strChargeCode = "DADCITUNESREVIEW2P"
                        Else
                            l_strChargeCode = "DADCITUNESREVIEW"
                        End If
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    End If
                    If grdItems.Columns("TrailerTranscode3").Text <> "" Then
                        'We have a trailer Transcode
                        l_strCode = "I"
                        l_lngDuration = 15
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("trailerHD").Text <> 0 Then
                            l_strChargeCode = "DADCTRANBHD"
                        Else
                            l_strChargeCode = "DADCTRANBSD"
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    ElseIf grdItems.Columns("TrailerClipMade3").Text <> "" Then
                        'We have a Trailer Clip Made
                        l_strCode = "O"
                        l_lngDuration = 0
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("trailerHD").Text <> 0 Then
                            l_strChargeCode = "DADCITUNESCLIPHD"
                        Else
                            l_strChargeCode = "DADCITUNESCLIP"
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    ElseIf grdItems.Columns("TrailerIngest3").Text <> "" Then
                        'We have a Trailer Clip Ingested
                        l_strCode = "O"
                        l_lngDuration = 0
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("trailerHD").Text <> 0 Then
                            l_strChargeCode = "DADCITUNESCLIPHD"
                        Else
                            l_strChargeCode = "DADCITUNESCLIP"
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    End If
                    If grdItems.Columns("TrailerReview3").Text <> "" And grdItems.Columns("TrailerClipMade3").Text = "" And grdItems.Columns("TrailerIngest3").Text = "" Then
                        'We have a Trailer review
                        l_strCode = "I"
                        l_lngDuration = 15
                        l_lngQuantity = 1
                        If grdItems.Columns("TrailerReview2Pass3").Value <> 0 Then
                            l_strChargeCode = "DADCITUNESREVIEW2P"
                        Else
                            l_strChargeCode = "DADCITUNESREVIEW"
                        End If
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    End If
                End If
            End If
        Else
            MsgBox "The current Job must belong to the correct company and be confirmed in order to bill an item.", vbCritical, "Cannot Bill Item"
        End If
    Else
        MsgBox "There must be a company chosen in order to do billing.", vbCritical, "Cannot Bill Item"
    End If
Else
    MsgBox "A Job must be created and loaded into the Jobs form in order to bill an item.", vbCritical, "Cannot Bill Item"
End If

BillItemPartOne = l_blnProblemEncountered

End Function

Function BillItemPartTwo()

Dim l_strChargeCode As String, l_rstTrackerChargeCode As ADODB.Recordset, l_strJobLine As String, l_lngJobID As Long, l_lngDuration As Long, l_lngMinuteBilling As Long
Dim l_lngQuantity As Long, l_strCode As String, l_lngFactor As Long, l_blnFirstItem As Boolean, l_blnBulkBilling As Boolean, l_lngLoggingItemCount As Long
Dim l_lngEventID As Long, l_strLastComment As String, l_strLastCommentCuser As String, l_datLastCommentDate As Date, l_blnProblemEncountered As Boolean

l_blnFirstItem = True
m_blnBilling = True
If frmJob.txtJobID.Text <> "" Then
    'A job is loaded - now check that it isn't locked.
    l_lngJobID = Val(frmJob.txtJobID.Text)
    'Check that a company has been chosen in the Drop down - we can only add to a job sheet of the correct company
    If cmbCompany.Text <> "" Then
        If frmJob.txtStatus.Text = "Confirmed" And frmJob.lblCompanyID.Caption = cmbCompany.Columns("companyID").Text Then
            'The Job is valid - go ahead and create Job lines for this item.
            l_lngQuantity = 0
            l_strJobLine = ""
            If grdItems.Columns("title").Text <> "" Then l_strJobLine = l_strJobLine & grdItems.Columns("title").Text
            If grdItems.Columns("Series").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("series").Text
            If grdItems.Columns("episode").Text <> "" Then l_strJobLine = l_strJobLine & " - Ep " & grdItems.Columns("episode").Text
            If grdItems.Columns("iTunesVendorID").Text <> "" Then l_strJobLine = l_strJobLine & " - VendorID " & grdItems.Columns("iTunesVendorID").Text
            If grdItems.Columns("readytobill").Text <> "" Then
                'DADC iTunes Tracker Billing
                'Many stagefields need checking and the appropriate job lines added if thery are set.
                If grdItems.Columns("iTunesOrderType").Text <> "AON Telekom digital delivery" And grdItems.Columns("iTunesOrderType").Text <> "Realeyz.TV digital delivery" Then
                    l_blnProblemEncountered = False
                    If grdItems.Columns("TrailerTranscode4").Text <> "" Then
                        'We have a trailer Transcode
                        l_strCode = "I"
                        l_lngDuration = 15
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("trailerHD").Text <> 0 Then
                            l_strChargeCode = "DADCTRANBHD"
                        Else
                            l_strChargeCode = "DADCTRANBSD"
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    ElseIf grdItems.Columns("TrailerClipMade4").Text <> "" Then
                        'We have a Trailer Clip Made
                        l_strCode = "O"
                        l_lngDuration = 0
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("trailerHD").Text <> 0 Then
                            l_strChargeCode = "DADCITUNESCLIPHD"
                        Else
                            l_strChargeCode = "DADCITUNESCLIP"
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    ElseIf grdItems.Columns("TrailerIngest4").Text <> "" Then
                        'We have a Trailer Clip Ingested
                        l_strCode = "O"
                        l_lngDuration = 0
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("trailerHD").Text <> 0 Then
                            l_strChargeCode = "DADCITUNESCLIPHD"
                        Else
                            l_strChargeCode = "DADCITUNESCLIP"
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    End If
                    If grdItems.Columns("TrailerReview4").Text <> "" And grdItems.Columns("TrailerClipMade4").Text = "" And grdItems.Columns("TrailerIngest4").Text = "" Then
                        'We have a Trailer review
                        l_strCode = "I"
                        l_lngDuration = 15
                        l_lngQuantity = 1
                        If grdItems.Columns("TrailerReview2Pass4").Value <> 0 Then
                            l_strChargeCode = "DADCITUNESREVIEW2P"
                        Else
                            l_strChargeCode = "DADCITUNESREVIEW"
                        End If
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Review", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    End If
                    If grdItems.Columns("TrailerSubsBurned").Text <> "" Then
                        'We have a Trailer Subs Burnin
                        l_strCode = "I"
                        l_lngDuration = 15
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Subs Burn in", l_lngQuantity, "DADCSUBSBURN", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Subs Burn in", l_lngQuantity, "DADCSUBSBURN", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Subs Burn in", l_lngQuantity, "DADCSUBSBURN", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Subs Burn in", l_lngQuantity, "DADCSUBSBURN", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    End If
                    If grdItems.Columns("TrailerSubsBurned2").Text <> "" Then
                        'We have a Trailer Subs Burnin
                        l_strCode = "I"
                        l_lngDuration = 15
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Subs Burn in", l_lngQuantity, "DADCSUBSBURN", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Subs Burn in", l_lngQuantity, "DADCSUBSBURN", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Subs Burn in", l_lngQuantity, "DADCSUBSBURN", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 2nd Language Subs Burn in", l_lngQuantity, "DADCSUBSBURN", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    End If
                    If grdItems.Columns("TrailerSubsBurned3").Text <> "" Then
                        'We have a Trailer Subs Burnin
                        l_strCode = "I"
                        l_lngDuration = 15
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Subs Burn in", l_lngQuantity, "DADCSUBSBURN", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Subs Burn in", l_lngQuantity, "DADCSUBSBURN", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Subs Burn in", l_lngQuantity, "DADCSUBSBURN", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 3rd Language Subs Burn in", l_lngQuantity, "DADCSUBSBURN", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    End If
                    If grdItems.Columns("TrailerSubsBurned4").Text <> "" Then
                        'We have a Trailer Subs Burnin
                        l_strCode = "I"
                        l_lngDuration = 15
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Subs Burn in", l_lngQuantity, "DADCSUBSBURN", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Subs Burn in", l_lngQuantity, "DADCSUBSBURN", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Subs Burn in", l_lngQuantity, "DADCSUBSBURN", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer 4th Language Subs Burn in", l_lngQuantity, "DADCSUBSBURN", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    End If
                    If grdItems.Columns("sub1").Text <> "" Then
                        'We have first Subs file in package
                        l_strCode = "O"
                        l_lngDuration = 0
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If l_strLastComment <> "" Then
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Subtitles First Language", l_lngQuantity, "DADCSUBSCHECK", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                        Else
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Subtitles First Language", l_lngQuantity, "DADCSUBSCHECK", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                        End If
                    End If
                    l_lngQuantity = 0
                    If grdItems.Columns("sub2").Text <> "" Then l_lngQuantity = l_lngQuantity + 1
                    If grdItems.Columns("sub3").Text <> "" Then l_lngQuantity = l_lngQuantity + 1
                    If grdItems.Columns("sub4").Text <> "" Then l_lngQuantity = l_lngQuantity + 1
                    If l_lngQuantity > 0 Then
                        'We have more Subs file in package
                        l_strCode = "O"
                        l_lngDuration = 0
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If l_strLastComment <> "" Then
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Subtitles Subsquent Languages", l_lngQuantity, "DADCSUBSCHECK2+", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                        Else
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Subtitles Subsquent Languages", l_lngQuantity, "DADCSUBSCHECK2+", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                        End If
                    End If
                    If grdItems.Columns("MetadataCreation").Text <> "" And (grdItems.Columns("Package").Text = "" Or Val(grdItems.Columns("MetadataCreation").Text) > 120) Then
                        'We have Metadata Creation
                        If Val(grdItems.Columns("MetadataCreation").Text) <> 0 Then
                            l_strCode = "A"
                            If grdItems.Columns("Package").Text = "" Then
                                l_lngDuration = Val(grdItems.Columns("MetadataCreation").Text)
                            Else
                                l_lngDuration = Val(grdItems.Columns("MetadataCreation").Text) - 120
                            End If
                            l_lngQuantity = 1
                            If adoComments.Recordset.RecordCount > 0 Then
                                adoComments.Recordset.MoveLast
                                l_strLastCommentCuser = adoComments.Recordset("cuser")
                                l_datLastCommentDate = adoComments.Recordset("cdate")
                                l_strLastComment = adoComments.Recordset("comment")
                            Else
                                l_strLastCommentCuser = ""
                                l_datLastCommentDate = 0
                                l_strLastComment = ""
                            End If
                            If l_strLastComment <> "" Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Metadata Creation", l_lngQuantity, "DADCMETADATA", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Metadata Creation", l_lngQuantity, "DADCMETADATA", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            MsgBox "The current Item must have a number of minutes spent doing metadata creation.", vbCritical, "Cannot Bill Item"
                            l_blnProblemEncountered = True
                        End If
                    End If
                    If grdItems.Columns("ChaptersKnown").Text <> "" Then
                        'We have chapters from known instructions
                        If Val(grdItems.Columns("ChaptersKnown").Text) <> 0 Then
                            l_strCode = "O"
                            l_lngDuration = 0
                            l_lngQuantity = Val(grdItems.Columns("ChaptersKnown").Text)
                            If l_lngQuantity > 6 Then
                                l_strChargeCode = "DADCCHAP7-12"
                            Else
                                l_strChargeCode = "DADCCHAP1-6"
                            End If
                            If adoComments.Recordset.RecordCount > 0 Then
                                adoComments.Recordset.MoveLast
                                l_strLastCommentCuser = adoComments.Recordset("cuser")
                                l_datLastCommentDate = adoComments.Recordset("cdate")
                                l_strLastComment = adoComments.Recordset("comment")
                            Else
                                l_strLastCommentCuser = ""
                                l_datLastCommentDate = 0
                                l_strLastComment = ""
                            End If
                            If l_strLastComment <> "" Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & grdItems.Columns("ChaptersKnown").Text & " Chapter Grabs", 1, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & grdItems.Columns("ChaptersKnown").Text & " Chapter Grabs", 1, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            MsgBox "The current Item must have a number of chapter points grabbed.", vbCritical, "Cannot Bill Item"
                            l_blnProblemEncountered = True
                        End If
                    End If
                    If grdItems.Columns("ChaptersUnknown").Text <> "" Then
                        'We have chapters from no known instructions
                        If Val(grdItems.Columns("ChaptersUnknown").Text) <> 0 Then
                            l_strCode = "O"
                            l_lngDuration = 0
                            l_lngQuantity = Int((Val(grdItems.Columns("ChaptersUnknown").Text) + 5) / 6)
                            If adoComments.Recordset.RecordCount > 0 Then
                                adoComments.Recordset.MoveLast
                                l_strLastCommentCuser = adoComments.Recordset("cuser")
                                l_datLastCommentDate = adoComments.Recordset("cdate")
                                l_strLastComment = adoComments.Recordset("comment")
                            Else
                                l_strLastCommentCuser = ""
                                l_datLastCommentDate = 0
                                l_strLastComment = ""
                            End If
                            If l_strLastComment <> "" Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & grdItems.Columns("ChaptersUnknown").Text & " Chapter Grabs", l_lngQuantity, "DADCCHAP2", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & grdItems.Columns("ChaptersUnknown").Text & " Chapter Grabs", l_lngQuantity, "DADCCHAP2", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            MsgBox "The current Item must have a number of chapter points grabbed.", vbCritical, "Cannot Bill Item"
                            l_blnProblemEncountered = True
                        End If
                    End If
                End If
            End If
        Else
            MsgBox "The current Job must belong to the correct company and be confirmed in order to bill an item.", vbCritical, "Cannot Bill Item"
        End If
    Else
        MsgBox "There must be a company chosen in order to do billing.", vbCritical, "Cannot Bill Item"
    End If
Else
    MsgBox "A Job must be created and loaded into the Jobs form in order to bill an item.", vbCritical, "Cannot Bill Item"
End If

BillItemPartTwo = l_blnProblemEncountered

End Function

Function BillItemPartThree() As Boolean

Dim l_strChargeCode As String, l_rstTrackerChargeCode As ADODB.Recordset, l_strJobLine As String, l_lngJobID As Long, l_lngDuration As Long, l_lngMinuteBilling As Long
Dim l_lngQuantity As Long, l_strCode As String, l_lngFactor As Long, l_blnFirstItem As Boolean, l_blnBulkBilling As Boolean, l_lngLoggingItemCount As Long
Dim l_lngEventID As Long, l_strLastComment As String, l_strLastCommentCuser As String, l_datLastCommentDate As Date, l_blnProblemEncountered As Boolean

l_blnFirstItem = True
m_blnBilling = True
If frmJob.txtJobID.Text <> "" Then
    'A job is loaded - now check that it isn't locked.
    l_lngJobID = Val(frmJob.txtJobID.Text)
    'Check that a company has been chosen in the Drop down - we can only add to a job sheet of the correct company
    If cmbCompany.Text <> "" Then
        If frmJob.txtStatus.Text = "Confirmed" And frmJob.lblCompanyID.Caption = cmbCompany.Columns("companyID").Text Then
            'The Job is valid - go ahead and create Job lines for this item.
            l_lngQuantity = 0
            l_strJobLine = ""
            If grdItems.Columns("title").Text <> "" Then l_strJobLine = l_strJobLine & grdItems.Columns("title").Text
            If grdItems.Columns("Series").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("series").Text
            If grdItems.Columns("episode").Text <> "" Then l_strJobLine = l_strJobLine & " - Ep " & grdItems.Columns("episode").Text
            If grdItems.Columns("iTunesVendorID").Text <> "" Then l_strJobLine = l_strJobLine & " - VendorID " & grdItems.Columns("iTunesVendorID").Text
            If grdItems.Columns("readytobill").Text <> "" Then
                'DADC iTunes Tracker Billing
                'Many stagefields need checking and the appropriate job lines added if thery are set.
                If grdItems.Columns("iTunesOrderType").Text <> "AON Telekom digital delivery" And grdItems.Columns("iTunesOrderType").Text <> "Realeyz.TV digital delivery" Then
                    If grdItems.Columns("Package").Text <> "" Then
                        'We have a feature Transcode
                        l_strCode = "O"
                        l_lngDuration = 0
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("featureHD").Text <> 0 Then
                            l_strChargeCode = "DADCPACKAGEITUNES-HD"
                        Else
                            l_strChargeCode = "DADCPACKAGEITUNES"
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("featureHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " iTunes Package", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " iTunes Package", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("featureHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " iTunes Package", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " iTunes Package", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    End If
                Else
                    'We have a Non-iTunes Non-Maxdome Trnascode/Package/Send single code
                    If grdItems.Columns("duration").Text <> "" Then
                        l_strCode = "I"
                        l_lngDuration = Val(grdItems.Columns("duration").Text)
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("featureHD").Text <> 0 Then
                            l_strChargeCode = "DADCTRANBPACKHD"
                        Else
                            l_strChargeCode = "DADCTRANBPACKSD"
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("featureHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Transcode and Package (non-iTunes)", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                                MakeJobDetailLine l_lngJobID, "O", l_strJobLine & " Validation of Assets", 1, "DADCFILECHECK-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Transcode and Package (non-iTunes)", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                                MakeJobDetailLine l_lngJobID, "O", l_strJobLine & "  Validation of Assets", 1, "DADCFILECHECK", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("featureHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Transcode and Package (non-iTunes)", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                                MakeJobDetailLine l_lngJobID, "O", l_strJobLine & "  Validation of Assets", 1, "DADCFILECHECK-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Transcode and Package (non-iTunes)", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                                MakeJobDetailLine l_lngJobID, "O", l_strJobLine & "  Validation of Assets", 1, "DADCFILECHECK", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    Else
                        MsgBox "The current Item must have a duration in order to bill it.", vbCritical, "Cannot Bill Item"
                        l_blnProblemEncountered = True
                    End If
                    If grdItems.Columns("TrailerTranscode").Text <> "" Then
                        'We have a Trailer Transcode
                        l_strCode = "I"
                        l_lngDuration = 15
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("trailerHD").Text <> 0 Then
                            l_strChargeCode = "DADCTRANBHD"
                        Else
                            l_strChargeCode = "DADCTRANBSD"
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Transcode", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    ElseIf grdItems.Columns("TrailerClipMade").Text <> "" Then
                        'We have a Trailer Clip Made
                        l_strCode = "O"
                        l_lngDuration = 0
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("trailerHD").Text <> 0 Then
                            l_strChargeCode = "DADCITUNESCLIPHD"
                        Else
                            l_strChargeCode = "DADCITUNESCLIP"
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Made", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    ElseIf grdItems.Columns("TrailerIngest").Text <> "" Then
                        'We have a Trailer Clip Ingested
                        l_strCode = "O"
                        l_lngDuration = 0
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("trailerHD").Text <> 0 Then
                            l_strChargeCode = "DADCITUNESCLIPHD"
                        Else
                            l_strChargeCode = "DADCITUNESCLIP"
                        End If
                        If l_strLastComment <> "" Then
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If grdItems.Columns("trailerHD").Text <> 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Ingested", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    End If
                End If
                If grdItems.Columns("FeatureVideoFixes").Text <> "" Then
                    'We have feature video fixes
                    If Val(grdItems.Columns("FeatureVideoFixes").Text) <> 0 Then
                        l_strCode = "A"
                        l_lngDuration = Val(grdItems.Columns("FeatureVideoFixes").Text)
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("featureHD").Text <> 0 Then
                            l_strChargeCode = "C-EDIT-HD"
                        Else
                            l_strChargeCode = "C-EDIT"
                        End If
                        If l_strLastComment <> "" Then
                            If l_strChargeCode = "C-EDIT-HD" Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Video Fixes", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Video Fixes", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If l_strChargeCode = "C-EDIT-HD" Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Video Fixes", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Video Fixes", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    Else
                        MsgBox "The current Item must have a time taken in the Feature Video Fixes in order to bill it.", vbCritical, "Cannot Bill Item"
                        l_blnProblemEncountered = True
                    End If
                End If
                If grdItems.Columns("FeatureAudioFixes").Text <> "" Then
                    'We have feature audio fixes
                    If Val(grdItems.Columns("FeatureAudioFixes").Text) <> 0 Then
                        l_strCode = "A"
                        l_lngDuration = Val(grdItems.Columns("FeatureAudioFixes").Text)
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("featureHD").Text <> 0 Then
                            l_strChargeCode = "PROTOOLS-HD"
                        Else
                            l_strChargeCode = "PROTOOLS"
                        End If
                        If l_strLastComment <> "" Then
                            If l_strChargeCode = "PROTOOLS-HD" Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Audio Fixes", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Audio Fixes", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If l_strChargeCode = "PROTOOLS-HD" Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Audio Fixes", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Feature Audio Fixes", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    Else
                        MsgBox "The current Item must have a time taken in the Feature Audio Fixes in order to bill it.", vbCritical, "Cannot Bill Item"
                        l_blnProblemEncountered = True
                    End If
                End If
                If grdItems.Columns("FeatureSubsSimpleFix").Text <> "" Then
                    'We have a subs simple conform situation
                    If Val(grdItems.Columns("FeatureSubsSimpleFix").Text) > 0 Then
                        l_strCode = "O"
                        l_lngQuantity = Val(grdItems.Columns("FeatureSubsSimpleFix").Text)
                        l_lngDuration = 0
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Subs Conform 1st File", 1, "DADCSUBSCONF", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, grdItems.Columns("featurehd").Text, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                        If l_lngQuantity > 1 Then
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Subs Conform 2nd and subsequent files", l_lngQuantity - 1, "DADCSUBSCONF2+", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, grdItems.Columns("featurehd").Text, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                        End If
                    Else
                        MsgBox "The We must have a quantity of Subs files Conformed in the 'Feat Subs Simple Fix'.", vbCritical, "Cannot Bill Item"
                    End If
                End If
                If grdItems.Columns("FeatureSubsFormatConvert").Text <> "" Then
                    'We have a subs ingest and check
                    If Val(grdItems.Columns("FeatureSubsFormatConvert").Text) > 0 Then
                        l_strCode = "O"
                        l_lngQuantity = Val(grdItems.Columns("FeatureSubsFormatConvert").Text)
                        l_lngDuration = 0
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Subs Format Convert", l_lngQuantity, "DADCSUBSCONV", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, grdItems.Columns("featurehd").Text, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                    Else
                        MsgBox "The We must have a quantity of Subs files Conformed in the 'Feat Subs Format Convert'.", vbCritical, "Cannot Bill Item"
                        l_blnProblemEncountered = True
                    End If
                End If
                If grdItems.Columns("FeatureSubsComplexFix").Text <> "" Then
                    'We have feature complex subs conform
                    If Val(grdItems.Columns("FeatureSubsComplexFix").Text) <> 0 Then
                        l_strCode = "I"
                        l_lngDuration = Val(grdItems.Columns("FeatureSubsComplexFix").Text)
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If l_strLastComment <> "" Then
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Subs Complex Conform", l_lngQuantity, "DADCSUBSMANCONF", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                        Else
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Subs Complex Conform", l_lngQuantity, "DADCSUBSMANCONF", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                        End If
                    Else
                        MsgBox "The current Item must have a number of minutes spent doing subtitle conforming.", vbCritical, "Cannot Bill Item"
                        l_blnProblemEncountered = True
                    End If
                End If
                If grdItems.Columns("TrailerVideoFixes").Text <> "" Then
                    'We have feature video fixes
                    If Val(grdItems.Columns("TrailerVideoFixes").Text) <> 0 Then
                        l_strCode = "A"
                        l_lngDuration = Val(grdItems.Columns("TrailerVideoFixes").Text)
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("trailerHD").Text <> 0 Then
                            l_strChargeCode = "C-EDIT-HD"
                        Else
                            l_strChargeCode = "C-EDIT"
                        End If
                        If l_strLastComment <> "" Then
                            If l_strChargeCode = "C-EDIT-HD" Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Video Fixes", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Video Fixes", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If l_strChargeCode = "C-EDIT-HD" Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Video Fixes", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Video Fixes", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    Else
                        MsgBox "The current Item must have a Time taken in the Trailer Video Fixes in order to bill it.", vbCritical, "Cannot Bill Item"
                        l_blnProblemEncountered = True
                    End If
                End If
                If grdItems.Columns("TrailerAudioFixes").Text <> "" Then
                    'We have feature audio fixes
                    If Val(grdItems.Columns("TrailerAudioFixes").Text) <> 0 Then
                        l_strCode = "A"
                        l_lngDuration = Val(grdItems.Columns("TrailerAudioFixes").Text)
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If grdItems.Columns("trailerHD").Text <> 0 Then
                            l_strChargeCode = "PROTOOLS-HD"
                        Else
                            l_strChargeCode = "PROTOOLS"
                        End If
                        If l_strLastComment <> "" Then
                            If l_strChargeCode = "PROTOOLS-HD" Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Audio Fixes", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Video Fixes", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        Else
                            If l_strChargeCode = "PROTOOLS-HD" Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Video Fixes", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 1, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Trailer Video Fixes", l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, 0, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                            End If
                        End If
                    Else
                        MsgBox "The current Item must have a time taken in the Traile Audio Fixes in order to bill it.", vbCritical, "Cannot Bill Item"
                        l_blnProblemEncountered = True
                    End If
                End If
                If grdItems.Columns("ArtworkFixes").Text <> "" Then
                    'We have Metadata Creation
                    If Val(grdItems.Columns("ArtworkFixes").Text) <> 0 Then
                        l_strCode = "A"
                        l_lngDuration = Val(grdItems.Columns("ArtworkFixes").Text)
                        l_lngQuantity = 1
                        If adoComments.Recordset.RecordCount > 0 Then
                            adoComments.Recordset.MoveLast
                            l_strLastCommentCuser = adoComments.Recordset("cuser")
                            l_datLastCommentDate = adoComments.Recordset("cdate")
                            l_strLastComment = adoComments.Recordset("comment")
                        Else
                            l_strLastCommentCuser = ""
                            l_datLastCommentDate = 0
                            l_strLastComment = ""
                        End If
                        If l_strLastComment <> "" Then
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Artwork Fixes", l_lngQuantity, "DADCPOSTER", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                        Else
                            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " Artwork Fixes", l_lngQuantity, "DADCPOSTER", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", "", "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ordernumber").Text, grdItems.Columns("series").Text, grdItems.Columns("episode").Text, grdItems.Columns("episodetitle").Text, "", grdItems.Columns("title").Text
                        End If
                    Else
                        MsgBox "The current Item must have a number of minutes spent doing artwork Fixing.", vbCritical, "Cannot Bill Item"
                        l_blnProblemEncountered = True
                    End If
                End If
            End If
            frmJob.adoJobDetail.Refresh
            frmJob.adoJobDetail.Refresh
            If Not l_blnProblemEncountered Then
                grdItems.Columns("billed").Text = -1
                grdItems.Columns("jobID").Text = l_lngJobID
                grdItems.Update
                cmdBillItem.Visible = False
                cmdManualBillItem.Visible = False
            End If
        Else
            MsgBox "The current Job must belong to the correct company and be confirmed in order to bill an item.", vbCritical, "Cannot Bill Item"
        End If
    Else
        MsgBox "There must be a company chosen in order to do billing.", vbCritical, "Cannot Bill Item"
    End If
Else
    MsgBox "A Job must be created and loaded into the Jobs form in order to bill an item.", vbCritical, "Cannot Bill Item"
End If

m_blnBilling = False

End Function

Sub Conditional_Assets_Columns()

If optDeliveryType(0).Value = True Then
    grdItems.Columns("mastershere").Visible = True
    grdItems.Columns("trailerhere").Visible = True
    grdItems.Columns("artworkhere").Visible = True
    grdItems.Columns("subshere").Visible = True
    grdItems.Columns("metadatahere").Visible = True
'ElseIf optDeliveryType(1).Value = True Then
'    grdItems.Columns("mastershere").Visible = True
'    grdItems.Columns("trailerhere").Visible = True
'    grdItems.Columns("artworkhere").Visible = True
'    grdItems.Columns("subshere").Visible = True
'    grdItems.Columns("metadatahere").Visible = True
ElseIf optDeliveryType(2).Value = True Then
    grdItems.Columns("mastershere").Visible = True
    grdItems.Columns("trailerhere").Visible = True
    grdItems.Columns("artworkhere").Visible = True
    grdItems.Columns("subshere").Visible = False
    grdItems.Columns("metadatahere").Visible = True
ElseIf optDeliveryType(3).Value = True Then
    grdItems.Columns("mastershere").Visible = True
    grdItems.Columns("trailerhere").Visible = True
    grdItems.Columns("artworkhere").Visible = True
    grdItems.Columns("subshere").Visible = True
    grdItems.Columns("metadatahere").Visible = True
'ElseIf optDeliveryType(4).Value = True Then
'    grdItems.Columns("mastershere").Visible = True
'    grdItems.Columns("trailerhere").Visible = False
'    grdItems.Columns("artworkhere").Visible = False
'    grdItems.Columns("subshere").Visible = False
'    grdItems.Columns("metadatahere").Visible = False
ElseIf optDeliveryType(5).Value = True Then
    grdItems.Columns("mastershere").Visible = True
    grdItems.Columns("trailerhere").Visible = True
    grdItems.Columns("artworkhere").Visible = True
    grdItems.Columns("subshere").Visible = False
    grdItems.Columns("metadatahere").Visible = False
End If

End Sub

Sub Send_Email_About_Pending(lp_lngTrackerItemID As Long)

Dim l_strSQL As String, l_rsDADCcomments As ADODB.Recordset, l_rsDADCTracker As ADODB.Recordset, l_strEmailBody As String, l_rsEmail As ADODB.Recordset

l_strEmailBody = ""
l_strSQL = "SELECT tracker_iTunes_itemID, title, requiredby, ordernumber, iTunesVendorID, itunesordertype, companyID FROM tracker_itunes_item WHERE tracker_itunes_itemID = " & lp_lngTrackerItemID & ";"
Set l_rsDADCTracker = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError
If l_rsDADCTracker.RecordCount > 0 Then
    l_strEmailBody = l_strEmailBody & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("title"))
    If Not IsNull(l_rsDADCTracker("requiredby")) Then
        l_strEmailBody = l_strEmailBody & ", Due Date: " & l_rsDADCTracker("requiredby")
    End If
    If Trim(" " & l_rsDADCTracker("ordernumber")) <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & "Order Number: " & l_rsDADCTracker("ordernumber")
    If Trim(" " & l_rsDADCTracker("iTunesVendorID")) <> "" Then l_strEmailBody = l_strEmailBody & " Vendor ID: " & l_rsDADCTracker("iTunesVendorID")
    If Trim(" " & l_rsDADCTracker("itunesordertype")) <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & "Delivery Platform: " & l_rsDADCTracker("itunesordertype")
    
    l_strSQL = "SELECT * FROM tracker_iTunes_comment WHERE tracker_itunes_itemID = " & lp_lngTrackerItemID & " ORDER BY cdate;"
    Set l_rsDADCcomments = ExecuteSQL(l_strSQL, g_strExecuteError)
    If l_rsDADCcomments.RecordCount > 0 Then
        l_rsDADCcomments.MoveLast
        l_strEmailBody = l_strEmailBody & vbCrLf & "Last Comment (" & l_rsDADCcomments("cdate") & "): " & l_rsDADCcomments("comment")
    End If
    l_rsDADCcomments.Close
    Set l_rsDADCcomments = Nothing
    'l_strEmailBody = l_strEmailBody & vbCrLf & "Link to Web Tracker: http://online.rrsat.tv/iTunesTracker/tracker_details.asp?trackerID=" & l_rsDADCTracker("tracker_iTunes_itemID") & "&goback=tracker" & vbCrLf & vbCrLf
    
    If l_strEmailBody <> "" Then

        l_strEmailBody = "The following item has just been put on 'Pending':" & vbCrLf & vbCrLf & l_strEmailBody
        'l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
        l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", Val(l_rsDADCTracker("companyID"))) & "' AND (trackermessageID = 40 or trackermessageID = 41);"
        Set l_rsEmail = ExecuteSQL(l_strSQL, g_strExecuteError)
        If l_rsEmail.RecordCount > 0 Then
            l_rsEmail.MoveFirst
            Do While Not l_rsEmail.EOF
                SendSMTPMail l_rsEmail("email"), l_rsEmail("fullname"), "Platform Tracker Pending Item - " & GetData("company", "portalcompanyname", "companyID", Val(l_rsDADCTracker("companyID"))), "", l_strEmailBody, True, "", "", g_strAdministratorEmailAddress
                l_rsEmail.MoveNext
            Loop
        End If
        l_rsEmail.Close
        Set l_rsEmail = Nothing
    
    End If
    
End If

l_rsDADCTracker.Close
Set l_rsDADCTracker = Nothing

End Sub

Private Sub grdDubcards_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdDubcards_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdDubcards_BeforeUpdate(Cancel As Integer)

If m_blnDelete = False Then

    If grdDubcards.Columns("Language").Text = "" Then
        MsgBox "Cannot save a Dubcard with no Language", vbCritical, "Comment Not Saved"
        Cancel = True
        Exit Sub
    End If
       
    grdDubcards.Columns("tracker_itemID").Text = lblTrackeritemID.Caption
    If grdDubcards.Columns("cdate").Text = "" Then
        grdDubcards.Columns("cdate").Text = Now
        grdDubcards.Columns("cuser").Text = g_strFullUserName
    End If
    grdDubcards.Columns("mdate").Text = Now
    grdDubcards.Columns("muser").Text = g_strFullUserName

End If

End Sub

Private Sub grdDubcards_BtnClick()

Dim l_varBookmark As Variant
'Open the Dub Card Edit Form

If Not (grdItems.Columns("tracker_itemID") Is Nothing) Then
    frmTrackerDubCard.lbltracker_itemID.Caption = grdItems.Columns("tracker_itemID").Text
End If
frmTrackerDubCard.Show vbModal

'Then Refresh
adoDubCards.Refresh

End Sub

Private Sub PopulateLocalCombo(lp_conControl As Control, lp_blnClearFirst As Boolean, lp_strType As String)

Dim Count As Long

On Error GoTo PROC_CheckListError
    
    If lp_conControl.ListCount > 0 Then
        If lp_blnClearFirst = False Then
            Exit Sub
        Else
            For Count = 0 To lp_conControl.ListCount - 1
                lp_conControl.RemoveItem (0)
            Next
        End If
    End If
    GoTo PROC_Continue
    
PROC_TryRowCount:
    If lp_conControl.Rows > 0 Then
        If lp_blnClearFirst = False Then
            Exit Sub
        Else
            For Count = 0 To lp_conControl.Rows - 1
                lp_conControl.RemoveItem (0)
            Next
        End If
    End If
    GoTo PROC_Continue

PROC_CheckListError:
    If Err.Number = 438 Then
        GoTo PROC_TryRowCount
    End If
    
PROC_Continue:
    
    LockControl lp_conControl, True
    
    Dim l_strSQL As String
    Dim l_rstItems As New ADODB.Recordset
    Select Case lp_strType
        Case "Project"
            l_strSQL = "SELECT DISTINCT Project FROM tracker_iTunes_item WHERE iTunesOrderType = 'Disney Dub Cards' ORDER BY Project;"
        Case "FeatureLanguage1"
            l_strSQL = "SELECT DISTINCT FeatureLanguage1 FROM tracker_iTunes_item WHERE iTunesOrderType = 'Disney Dub Cards' ORDER BY FeatureLanguage1;"
    End Select
    Set l_rstItems = ExecuteSQL(l_strSQL, g_strExecuteError)
    If l_rstItems.RecordCount > 0 Then
        l_rstItems.MoveFirst
        Do While Not l_rstItems.EOF
            lp_conControl.AddItem Trim(" " & l_rstItems(0))
            l_rstItems.MoveNext
        Loop
    End If
    l_rstItems.Close
    
    LockControl lp_conControl, False

End Sub

Private Sub txtLanguage_GotFocus()
PopulateLocalCombo txtLanguage, False, "FeatureLanguage1"
End Sub
