VERSION 5.00
Begin VB.Form frmSQLAdmin 
   Caption         =   "SQL Administration"
   ClientHeight    =   3180
   ClientLeft      =   5610
   ClientTop       =   5085
   ClientWidth     =   4800
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   3180
   ScaleWidth      =   4800
   Begin VB.TextBox txtDNSSuffix 
      Height          =   285
      Left            =   120
      TabIndex        =   9
      Text            =   ".ad.mpc.local"
      Top             =   480
      Width           =   1455
   End
   Begin VB.TextBox txtSuffix 
      Height          =   285
      Left            =   2040
      TabIndex        =   8
      Text            =   "SUFFIX"
      Top             =   1320
      Width           =   855
   End
   Begin VB.TextBox txtPrefix 
      Height          =   285
      Left            =   120
      TabIndex        =   7
      Text            =   "PREFIX"
      Top             =   1320
      Width           =   855
   End
   Begin VB.CommandButton cmdAddSequence 
      Caption         =   "Add Sequence"
      Height          =   315
      Left            =   3000
      TabIndex        =   6
      Top             =   1320
      Width           =   1695
   End
   Begin VB.TextBox txtNumberTo 
      Height          =   285
      Left            =   1080
      TabIndex        =   5
      Text            =   "300"
      Top             =   1680
      Width           =   855
   End
   Begin VB.TextBox txtNumberFrom 
      Height          =   285
      Left            =   1080
      TabIndex        =   4
      Text            =   "100"
      Top             =   1320
      Width           =   855
   End
   Begin VB.CommandButton cmdRemoveUser 
      Caption         =   "Remove"
      Height          =   315
      Left            =   3540
      TabIndex        =   1
      ToolTipText     =   "Removes User"
      Top             =   2760
      Width           =   1155
   End
   Begin VB.CommandButton cmdAddUser 
      Caption         =   "Add User"
      Height          =   315
      Left            =   3540
      TabIndex        =   2
      ToolTipText     =   "Creates a new user with the default settings and passwords"
      Top             =   2340
      Width           =   1155
   End
   Begin VB.TextBox txtUserName 
      Height          =   315
      Left            =   120
      TabIndex        =   0
      ToolTipText     =   "Computer Name"
      Top             =   2520
      Width           =   3135
   End
   Begin VB.Label Label1 
      Caption         =   "Suffix"
      Height          =   195
      Index           =   4
      Left            =   2040
      TabIndex        =   13
      Top             =   1020
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Number"
      Height          =   195
      Index           =   3
      Left            =   1080
      TabIndex        =   12
      Top             =   1020
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Prefix"
      Height          =   195
      Index           =   2
      Left            =   120
      TabIndex        =   11
      Top             =   1020
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "DNS Suffix"
      Height          =   195
      Index           =   1
      Left            =   120
      TabIndex        =   10
      Top             =   180
      Width           =   1395
   End
   Begin VB.Label Label1 
      Caption         =   "Add Single"
      Height          =   195
      Index           =   0
      Left            =   120
      TabIndex        =   3
      Top             =   2220
      Width           =   1455
   End
End
Attribute VB_Name = "frmSQLAdmin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdAddSequence_Click()
Dim l_intLoop As Integer

For l_intLoop = Val(txtNumberFrom.Text) To Val(txtNumberTo.Text)
    txtUserName.Text = txtPrefix.Text & Format(l_intLoop) & txtSuffix.Text
    cmdAddUser.Value = True
    DoEvents
    
Next

MsgBox "All records have now been added.", vbExclamation


End Sub

Private Sub cmdAddUser_Click()

Me.MousePointer = vbHourglass
Me.Caption = "Adding user"

Dim l_strSQL As String
l_strSQL = "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE ON cetasoft2.* TO '" & Trim(UCase(txtUserName.Text)) & "'@'" & Trim(LCase(txtUserName.Text)) & txtDNSSuffix.Text & "' IDENTIFIED BY 'ce7a50ft';"

'l_strSQL = "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE ON cetasoft2.* TO '" & Trim(UCase(txtUserName.Text)) & "'@'%' IDENTIFIED BY '4remetro73a';"

Dim l_Con As New ADODB.Connection
l_Con.Open g_strConnection

l_Con.Execute l_strSQL

l_Con.Close
Set l_Con = Nothing

Me.Caption = "Done"
Me.MousePointer = vbDefault

DoEvents

End Sub

Private Sub cmdImport_Click(Index As Integer)
On Error GoTo cmdImport_Error

Dim db As dao.Database
Set db = OpenDatabase("\\Narsil\CETASQL\junk_to_be_deleted\pcname.mdb")
Dim rs As dao.Recordset
Set rs = db.OpenRecordset("pcname")

If Not rs.EOF Then rs.MoveFirst

Do While Not rs.EOF
    If Not IsNull(rs("pcname")) Then
        txtUserName.Text = Format(rs("pcname"))
        Select Case Index
        Case 0
            cmdAddUser.Value = True
        Case 1
            cmdRemoveUser.Value = True
        End Select
    End If
    rs.MoveNext
Loop

rs.Close
Set rs = Nothing
db.Close
Set db = Nothing

MsgBox "Completed import of machine name list", vbInformation

Exit Sub
cmdImport_Error:

Beep
Resume Next

End Sub

Private Sub cmdRemoveUser_Click()

On Error GoTo cndRemoveUser_Error
Me.MousePointer = vbHourglass
Me.Caption = "Adding user"

Dim l_strSQL As String
l_strSQL = "REVOKE ALL PRIVILEGES ON cetasoft2.* FROM '" & Trim(UCase(txtUserName.Text)) & "'@'" & Trim(LCase(txtUserName.Text)) & "mpc.local';"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

Me.Caption = "Done"
Me.MousePointer = vbDefault

DoEvents

Exit Sub

cndRemoveUser_Error:
Beep
Resume Next


End Sub

Private Sub Command1_Click()

End Sub

