VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmSendToAccounts 
   Caption         =   "Authorisation and Send To Accounts"
   ClientHeight    =   11190
   ClientLeft      =   1260
   ClientTop       =   2550
   ClientWidth     =   16860
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSendToAccounts.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   11190
   ScaleWidth      =   16860
   WindowState     =   2  'Maximized
   Begin VB.OptionButton optShow 
      Caption         =   "Credit Note PDFs for ESI"
      Height          =   195
      Index           =   21
      Left            =   12660
      TabIndex        =   60
      Top             =   7500
      Width           =   2955
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Invoices to Sony DADC Ltd (Scanbox)"
      Enabled         =   0   'False
      Height          =   195
      Index           =   20
      Left            =   12660
      TabIndex        =   59
      Top             =   10500
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Invoices to Sony DADC Ltd (BBC HDD Deliveries)"
      Height          =   195
      Index           =   19
      Left            =   12660
      TabIndex        =   58
      Top             =   8940
      Width           =   3975
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Invoices to Sony DADC Ltd (Svensk Compilations)"
      Enabled         =   0   'False
      Height          =   195
      Index           =   18
      Left            =   3660
      TabIndex        =   57
      Top             =   10680
      Visible         =   0   'False
      Width           =   3975
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Invoices to Sony DADC Ltd (Bulk Audio Ingest)"
      Height          =   195
      Index           =   17
      Left            =   12660
      TabIndex        =   56
      Top             =   9180
      Width           =   3915
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   315
      Left            =   240
      TabIndex        =   55
      Top             =   9240
      Visible         =   0   'False
      Width           =   1635
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Invoices to Sony DADC Ltd (Hoanzl)"
      Enabled         =   0   'False
      Height          =   195
      Index           =   16
      Left            =   12660
      TabIndex        =   54
      Top             =   10020
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Invoices to Sony DADC Ltd (Svensk iTunes)"
      Enabled         =   0   'False
      Height          =   195
      Index           =   15
      Left            =   3660
      TabIndex        =   53
      Top             =   10440
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Invoices to Sony DADC Ltd (UK Playouts)"
      Height          =   195
      Index           =   14
      Left            =   12660
      TabIndex        =   51
      Top             =   8460
      Width           =   3615
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Invoices to Sony DADC Ltd (Creative)"
      Height          =   195
      Index           =   13
      Left            =   12660
      TabIndex        =   50
      Top             =   8700
      Width           =   3615
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Invoices to Sony DADC Ltd (UK Fixes)"
      Height          =   195
      Index           =   12
      Left            =   12660
      TabIndex        =   49
      Top             =   8220
      Width           =   3615
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Invoices to Sony DADC Ltd (Nordisk)"
      Enabled         =   0   'False
      Height          =   195
      Index           =   11
      Left            =   12660
      TabIndex        =   48
      Top             =   10260
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Invoices to Sony DADC Ltd (Svensk Fixes)"
      Enabled         =   0   'False
      Height          =   195
      Index           =   10
      Left            =   3660
      TabIndex        =   47
      Top             =   10200
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Invoices to MX1 Priority Accounts"
      Enabled         =   0   'False
      Height          =   195
      Index           =   0
      Left            =   12660
      TabIndex        =   45
      Top             =   5820
      Width           =   3735
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Credit Notes to MX1 Prioity Accounts"
      Enabled         =   0   'False
      Height          =   195
      Index           =   1
      Left            =   12660
      TabIndex        =   44
      Top             =   6060
      Width           =   3795
   End
   Begin VB.OptionButton optShow 
      Caption         =   "BBC W/W Int Ops Star Orders"
      Height          =   195
      Index           =   2
      Left            =   12660
      TabIndex        =   43
      Top             =   6300
      Width           =   2835
   End
   Begin VB.OptionButton optShow 
      Caption         =   "BBC W/W Int Ops Non-Star Orders"
      Height          =   195
      Index           =   3
      Left            =   12660
      TabIndex        =   42
      Top             =   6540
      Width           =   2895
   End
   Begin VB.OptionButton optShow 
      Caption         =   "BBC W/W Eastenders Project"
      Height          =   195
      Index           =   4
      Left            =   12660
      TabIndex        =   41
      Top             =   6780
      Width           =   2895
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Invoices for BFI"
      Height          =   195
      Index           =   5
      Left            =   12660
      TabIndex        =   40
      Top             =   7740
      Width           =   4035
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Generate A Spockets Batch"
      Height          =   195
      Index           =   6
      Left            =   12660
      TabIndex        =   39
      Top             =   7020
      Width           =   2955
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Invoices PDFs for ESI"
      Height          =   195
      Index           =   7
      Left            =   12660
      TabIndex        =   38
      Top             =   7260
      Width           =   2955
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Invoices to Sony DADC Ltd (Manual Ingest)"
      Height          =   195
      Index           =   9
      Left            =   12660
      TabIndex        =   37
      Top             =   7980
      Width           =   3615
   End
   Begin VB.OptionButton optShow 
      Caption         =   "Invoices to Sony DADC Ltd (Svensk Ingest)"
      Enabled         =   0   'False
      Height          =   195
      Index           =   8
      Left            =   3660
      TabIndex        =   36
      Top             =   9960
      Visible         =   0   'False
      Width           =   3675
   End
   Begin VB.ComboBox cmbOrderBy 
      Height          =   315
      Left            =   6120
      TabIndex        =   34
      Tag             =   "NOCLEAR"
      Text            =   "cmbOrderBy"
      ToolTipText     =   "Which data field to search against"
      Top             =   9240
      Visible         =   0   'False
      Width           =   2235
   End
   Begin VB.ComboBox cmbDirection 
      Height          =   315
      ItemData        =   "frmSendToAccounts.frx":08CA
      Left            =   8460
      List            =   "frmSendToAccounts.frx":08D4
      Style           =   2  'Dropdown List
      TabIndex        =   33
      ToolTipText     =   "Which data field to search against"
      Top             =   9240
      Visible         =   0   'False
      Width           =   1095
   End
   Begin MSComCtl2.DTPicker datDateToShow 
      Height          =   315
      Left            =   13440
      TabIndex        =   14
      ToolTipText     =   "Date to be shown on from"
      Top             =   5400
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   556
      _Version        =   393216
      Format          =   31784961
      CurrentDate     =   38408
   End
   Begin MSAdodcLib.Adodc adoCosting 
      Height          =   330
      Left            =   8820
      Top             =   5100
      Visible         =   0   'False
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCosting"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoJobListing 
      Height          =   330
      Left            =   7740
      Top             =   300
      Visible         =   0   'False
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   582
      ConnectMode     =   3
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   2
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoJobListing"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.PictureBox picTextBoxes 
      BorderStyle     =   0  'None
      Height          =   5175
      Left            =   11640
      ScaleHeight     =   5175
      ScaleWidth      =   5055
      TabIndex        =   17
      Top             =   60
      Width           =   5055
      Begin VB.CommandButton cmdPostCustomersOnly 
         Caption         =   "Just Send New Customers to Accounts"
         Height          =   315
         Left            =   780
         TabIndex        =   61
         Top             =   4380
         Visible         =   0   'False
         Width           =   4155
      End
      Begin VB.CommandButton cmdUnbatch 
         Caption         =   "BBC Unbatch"
         Height          =   315
         Left            =   780
         TabIndex        =   46
         Top             =   4020
         Visible         =   0   'False
         Width           =   4155
      End
      Begin VB.TextBox txtProjectNumber 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   780
         Locked          =   -1  'True
         TabIndex        =   0
         ToolTipText     =   "Unique job ID number"
         Top             =   0
         Width           =   1275
      End
      Begin VB.CommandButton cmdSendToAccounts 
         Caption         =   "Send To Accounts MX1"
         Height          =   315
         Left            =   780
         TabIndex        =   12
         ToolTipText     =   "Send information to JCA accounts"
         Top             =   3660
         Visible         =   0   'False
         Width           =   4155
      End
      Begin VB.TextBox txtSelectedCount 
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   780
         TabIndex        =   13
         ToolTipText     =   "Number of items selected"
         Top             =   4740
         Width           =   1275
      End
      Begin VB.TextBox txtCreatedOn 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   780
         Locked          =   -1  'True
         TabIndex        =   8
         ToolTipText     =   "Job created date"
         Top             =   2880
         Width           =   1275
      End
      Begin VB.TextBox txtCreatedBy 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   9
         ToolTipText     =   "Job created by"
         Top             =   2880
         Width           =   1215
      End
      Begin VB.TextBox txtSubTitle 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   780
         Locked          =   -1  'True
         TabIndex        =   7
         ToolTipText     =   "Job Subtitle (if available)"
         Top             =   2520
         Width           =   4155
      End
      Begin VB.TextBox txtTitle 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   780
         Locked          =   -1  'True
         TabIndex        =   6
         ToolTipText     =   "Job Title"
         Top             =   2160
         Width           =   4155
      End
      Begin VB.TextBox txtDeadlineTime 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   11
         ToolTipText     =   "Job deadline time"
         Top             =   3240
         Width           =   1215
      End
      Begin VB.TextBox txtDeadlineDate 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   780
         Locked          =   -1  'True
         TabIndex        =   10
         ToolTipText     =   "Job deadline date"
         Top             =   3240
         Width           =   1275
      End
      Begin VB.TextBox txtContact 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   780
         Locked          =   -1  'True
         TabIndex        =   3
         ToolTipText     =   "Contact Name"
         Top             =   1080
         Width           =   4155
      End
      Begin VB.TextBox txtCompany 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   780
         Locked          =   -1  'True
         TabIndex        =   2
         ToolTipText     =   "Company Name"
         Top             =   720
         Width           =   4155
      End
      Begin VB.TextBox txtJobID 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   780
         Locked          =   -1  'True
         TabIndex        =   1
         ToolTipText     =   "Unique job ID number"
         Top             =   360
         Width           =   1275
      End
      Begin VB.TextBox txtTelephone 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   780
         Locked          =   -1  'True
         TabIndex        =   4
         ToolTipText     =   "The job specific telephone number"
         Top             =   1440
         Width           =   4155
      End
      Begin VB.TextBox txtOrderReference 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   255
         Left            =   780
         Locked          =   -1  'True
         TabIndex        =   5
         ToolTipText     =   "The clients order reference number"
         Top             =   1800
         Width           =   4155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Project #"
         Height          =   255
         Index           =   12
         Left            =   0
         TabIndex        =   30
         Top             =   0
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Selected"
         Height          =   255
         Index           =   11
         Left            =   0
         TabIndex        =   29
         Top             =   4740
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "By"
         Height          =   255
         Index           =   10
         Left            =   3000
         TabIndex        =   28
         Top             =   2880
         Width           =   495
      End
      Begin VB.Label lblCaption 
         Caption         =   "Created"
         Height          =   255
         Index           =   9
         Left            =   0
         TabIndex        =   27
         Top             =   2880
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Sub Title"
         Height          =   255
         Index           =   8
         Left            =   0
         TabIndex        =   26
         Top             =   2520
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Title"
         Height          =   255
         Index           =   1
         Left            =   0
         TabIndex        =   25
         Top             =   2160
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Job ID"
         Height          =   255
         Index           =   0
         Left            =   0
         TabIndex        =   24
         Top             =   360
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Company"
         Height          =   255
         Index           =   2
         Left            =   0
         TabIndex        =   23
         Top             =   720
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Telephone"
         Height          =   255
         Index           =   3
         Left            =   0
         TabIndex        =   22
         Top             =   1440
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Contact"
         Height          =   255
         Index           =   5
         Left            =   0
         TabIndex        =   21
         Top             =   1080
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Order Ref"
         Height          =   255
         Index           =   4
         Left            =   0
         TabIndex        =   20
         Top             =   1800
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Deadline"
         Height          =   255
         Index           =   6
         Left            =   0
         TabIndex        =   19
         Top             =   3240
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Time"
         Height          =   255
         Index           =   7
         Left            =   3000
         TabIndex        =   18
         Top             =   3240
         Width           =   495
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdJobListing 
      Bindings        =   "frmSendToAccounts.frx":08E3
      Height          =   5115
      Left            =   60
      TabIndex        =   15
      Top             =   240
      Width           =   11415
      _Version        =   196617
      ColumnHeaders   =   0   'False
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   1
      ForeColorEven   =   -2147483630
      ForeColorOdd    =   -2147483630
      BackColorOdd    =   16761024
      RowHeight       =   370
      ExtraHeight     =   79
      Columns.Count   =   13
      Columns(0).Width=   1323
      Columns(0).Caption=   "Job #"
      Columns(0).Name =   "jobID"
      Columns(0).DataField=   "jobID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   1773
      Columns(1).Caption=   "Invoice #"
      Columns(1).Name =   "invoicenumber"
      Columns(1).DataField=   "invoicenumber"
      Columns(1).FieldLen=   256
      Columns(2).Width=   847
      Columns(2).Caption=   "User"
      Columns(2).Name =   "cuser"
      Columns(2).DataField=   "cuser"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2646
      Columns(3).Caption=   "Invoice Date"
      Columns(3).Name =   "invoiceddate"
      Columns(3).DataField=   "invoiceddate"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "Project #"
      Columns(4).Name =   "projectnumber"
      Columns(4).DataField=   "projectID"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "Job Start Date"
      Columns(5).Name =   "startdate"
      Columns(5).DataField=   "startdate"
      Columns(5).DataType=   7
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "A/C #"
      Columns(6).Name =   "accountnumber"
      Columns(6).DataField=   "accountcode"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   2858
      Columns(7).Caption=   "Company"
      Columns(7).Name =   "companyname"
      Columns(7).DataField=   "companyname"
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Caption=   "Contact"
      Columns(8).Name =   "contactname"
      Columns(8).DataField=   "contactname"
      Columns(8).FieldLen=   256
      Columns(9).Width=   3201
      Columns(9).Caption=   "Job Title"
      Columns(9).Name =   "title"
      Columns(9).DataField=   "title1"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   2461
      Columns(10).Caption=   "Sent To Accounts Date"
      Columns(10).Name=   "senttoaccountsdate"
      Columns(10).DataField=   "senttoaccountsdate"
      Columns(10).DataType=   7
      Columns(10).FieldLen=   256
      Columns(10).Style=   1
      Columns(10).ButtonsAlways=   -1  'True
      Columns(11).Width=   714
      Columns(11).Caption=   "By"
      Columns(11).Name=   "senttoaccountsuser"
      Columns(11).DataField=   "senttoaccountsuser"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "companyID"
      Columns(12).Name=   "companyID"
      Columns(12).DataField=   "companyID"
      Columns(12).FieldLen=   256
      _ExtentX        =   20135
      _ExtentY        =   9022
      _StockProps     =   79
      Caption         =   "Jobs To Be Sent To Accounts"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdCosting 
      Bindings        =   "frmSendToAccounts.frx":08FF
      Height          =   3675
      Left            =   60
      TabIndex        =   16
      ToolTipText     =   "Details of the Job"
      Top             =   5460
      Width           =   11415
      _Version        =   196617
      AllowUpdate     =   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   1
      ForeColorEven   =   -2147483630
      ForeColorOdd    =   -2147483630
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   2064
      Columns(0).Caption=   "Account Code"
      Columns(0).Name =   "accountcode"
      Columns(0).DataField=   "accountcode"
      Columns(0).FieldLen=   256
      Columns(1).Width=   6271
      Columns(1).Caption=   "Description"
      Columns(1).Name =   "description"
      Columns(1).DataField=   "description"
      Columns(1).FieldLen=   256
      Columns(2).Width=   1640
      Columns(2).Caption=   "Quantity"
      Columns(2).Name =   "quantity"
      Columns(2).DataField=   "quantity"
      Columns(2).FieldLen=   256
      Columns(3).Width=   1244
      Columns(3).Caption=   "Time"
      Columns(3).Name =   "time"
      Columns(3).DataField=   "time"
      Columns(3).FieldLen=   256
      Columns(4).Width=   2170
      Columns(4).Caption=   "Unit Charge"
      Columns(4).Name =   "unitcharge"
      Columns(4).Alignment=   1
      Columns(4).DataField=   "unitcharge"
      Columns(4).DataType=   6
      Columns(4).NumberFormat=   "�#0.00"
      Columns(4).FieldLen=   256
      Columns(5).Width=   1455
      Columns(5).Caption=   "Disc %"
      Columns(5).Name =   "discount"
      Columns(5).DataField=   "discount"
      Columns(5).FieldLen=   256
      Columns(6).Width=   2196
      Columns(6).Caption=   "Total"
      Columns(6).Name =   "total"
      Columns(6).Alignment=   1
      Columns(6).DataField=   "total"
      Columns(6).DataType=   6
      Columns(6).NumberFormat=   "�#0.00"
      Columns(6).FieldLen=   256
      _ExtentX        =   20135
      _ExtentY        =   6482
      _StockProps     =   79
      Caption         =   "Costs"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdNewJobListing 
      Bindings        =   "frmSendToAccounts.frx":0918
      Height          =   5115
      Left            =   60
      TabIndex        =   52
      Top             =   240
      Width           =   11415
      _Version        =   196617
      ColumnHeaders   =   0   'False
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   1
      ForeColorEven   =   -2147483630
      ForeColorOdd    =   -2147483630
      BackColorOdd    =   16761024
      RowHeight       =   370
      ExtraHeight     =   26
      Columns.Count   =   12
      Columns(0).Width=   1323
      Columns(0).Caption=   "Trans ID"
      Columns(0).Name =   "jobID"
      Columns(0).DataField=   "accountstransactionID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   1773
      Columns(1).Caption=   "Invoice #"
      Columns(1).Name =   "invoicenumber"
      Columns(1).DataField=   "reference"
      Columns(1).FieldLen=   256
      Columns(2).Width=   847
      Columns(2).Caption=   "User"
      Columns(2).Name =   "cuser"
      Columns(2).DataField=   "cuser"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2117
      Columns(3).Caption=   "Invoice Date"
      Columns(3).Name =   "invoiceddate"
      Columns(3).DataField=   "transactiondate"
      Columns(3).FieldLen=   256
      Columns(4).Width=   1455
      Columns(4).Caption=   "Project #"
      Columns(4).Name =   "projectnumber"
      Columns(4).DataField=   "projectID"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "Job Start Date"
      Columns(5).Name =   "startdate"
      Columns(5).DataField=   "startdate"
      Columns(5).DataType=   7
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "A/C #"
      Columns(6).Name =   "accountnumber"
      Columns(6).DataField=   "accountcode"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   2858
      Columns(7).Caption=   "Company"
      Columns(7).Name =   "companyname"
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "Job Title"
      Columns(8).Name =   "title"
      Columns(8).DataField=   "title1"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   2461
      Columns(9).Caption=   "Sent To Accounts Date"
      Columns(9).Name =   "senttoaccountsdate"
      Columns(9).DataField=   "senttoaccountsdate"
      Columns(9).DataType=   7
      Columns(9).FieldLen=   256
      Columns(9).Style=   1
      Columns(9).ButtonsAlways=   -1  'True
      Columns(10).Width=   714
      Columns(10).Caption=   "By"
      Columns(10).Name=   "senttoaccountsuser"
      Columns(10).DataField=   "senttoaccountsuser"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "companyID"
      Columns(11).Name=   "companyID"
      Columns(11).DataField=   "companyID"
      Columns(11).FieldLen=   256
      _ExtentX        =   20135
      _ExtentY        =   9022
      _StockProps     =   79
      Caption         =   "Jobs To Be Sent To Accounts"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order By"
      Height          =   255
      Index           =   24
      Left            =   4980
      TabIndex        =   35
      Top             =   9240
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblProgress 
      Alignment       =   2  'Center
      Caption         =   "INFORMATION"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   255
      Left            =   60
      TabIndex        =   32
      Top             =   0
      Width           =   11415
   End
   Begin VB.Label Label1 
      Caption         =   "Date To Show"
      Height          =   195
      Left            =   12060
      TabIndex        =   31
      Top             =   5460
      Width           =   1275
   End
End
Attribute VB_Name = "frmSendToAccounts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_blnPostingToAccounts As Boolean
Const g_strUploadPDFLocation = "\\jcaserver\ceta\Invoices\"

Private Sub adoJobListing_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

If adoJobListing.Recordset.EOF Or adoJobListing.Recordset.BOF Then Exit Sub

Dim l_strSQL As String

'If g_intWebInvoicing = 1 Then
'
'    With adoJobListing.Recordset
'        'no project number at MX1
'        If g_optUseJCAPostingToAccounts = 0 Then txtProjectNumber.Text = Format(.Fields("projectnumber"))
'        'everything else should load
'        txtJobID.Text = .Fields("accountstransactionID")
'        'txtCompany.Text = Format(.Fields("companyname"))
'        'txtContact.Text = Format(.Fields("contactname"))
'        'txtTelephone.Text = Format(.Fields("telephone"))
'        txtOrderReference.Text = Format(.Fields("clientponumber"))
'        'txtDeadlineDate.Text = Format(.Fields("deadlinedate"), vbShortDateFormat)
'        'txtDeadlineTime.Text = Format(.Fields("deadlinedate"), vbTimeFormat)
'        'txtTitle.Text = Format(.Fields("title1"))
'        'txtSubtitle.Text = Format(.Fields("title2"))
'        'txtCreatedOn.Text = Format(.Fields("createddate"), vbShortDateFormat)
'        'txtCreatedBy.Text = Format(.Fields("createduser"))
'
'    End With
'
'    l_strSQL = "SELECT * FROM costing WHERE accountstransactionID = '" & adoJobListing.Recordset("accountstransactionID") & "' ORDER BY costingID"
'
'Else

    With adoJobListing.Recordset
        'no project number at MX1
        If g_optUseJCAPostingToAccounts = 0 Then txtProjectNumber.Text = Format(.Fields("projectnumber"))
        'everything else should load
        txtJobID.Text = .Fields("jobID")
        txtCompany.Text = Format(.Fields("companyname"))
        txtContact.Text = Format(.Fields("contactname"))
        txtTelephone.Text = Format(.Fields("telephone"))
        txtOrderReference.Text = Format(.Fields("orderreference"))
        txtDeadlineDate.Text = Format(.Fields("deadlinedate"), vbShortDateFormat)
        txtDeadlineTime.Text = Format(.Fields("deadlinedate"), vbTimeFormat)
        txtTitle.Text = Format(.Fields("title1"))
        txtSubtitle.Text = Format(.Fields("title2"))
        txtCreatedOn.Text = Format(.Fields("createddate"), vbShortDateFormat)
        txtCreatedBy.Text = Format(.Fields("createduser"))
    
    End With
    
    If optShow(1).Value = True Then
        l_strSQL = "SELECT * FROM costing WHERE jobID = '" & adoJobListing.Recordset("jobID") & "' AND (creditnotenumber IS NOT NULL) AND (creditnotenumber <> 0) ORDER BY originalcostingID"
    Else
        l_strSQL = "SELECT * FROM costing WHERE ((creditnotenumber IS NULL) OR (creditnotenumber = 0)) AND (jobID = '" & adoJobListing.Recordset("jobID") & "') ORDER BY costingID"
    End If
    
'End If

adoCosting.ConnectionString = g_strConnection
adoCosting.RecordSource = l_strSQL
adoCosting.Refresh

End Sub

Private Sub cmdPostCustomersOnly_Click()
PostNewCustomersToAccounts
End Sub

Private Sub cmdSendToAccounts_Click()

Dim l_blnPostToTextFile As Boolean

If Not CheckAccess("/sendtoaccounts") Then Exit Sub

'prepare the SQL for the jobs to be sent
Dim l_strSQL As String
Dim l_strType As String

If optShow(0).Value = True Then 'Priority Invoices
    l_strSQL = "SELECT jobID, invoicenumber FROM job WHERE jobtype <> 'CREDIT' AND senttoaccountsdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoaccountsbatchnumber IS NULL OR senttoaccountsbatchnumber = 0) ORDER BY invoicenumber"
    l_strType = "INVOICE"
    l_blnPostToTextFile = False
ElseIf optShow(1).Value = True Then 'Priority Credit Notes
    l_strSQL = "SELECT jobID, creditnotenumber FROM job WHERE creditsenttoaccountsdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (creditnotebatchnumber IS NULL OR creditnotebatchnumber = 0) ORDER BY creditnotenumber"
    l_strType = "CREDIT"
    l_blnPostToTextFile = False
ElseIf optShow(2).Value = True Or optShow(3).Value = True Then 'BBCWW
    l_strSQL = "SELECT jobID FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0) ORDER BY contactname, invoicenumber"
    l_strType = "BBCWW"
    l_blnPostToTextFile = True
ElseIf optShow(4).Value = True Then
    l_strSQL = "SELECT jobID FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0) ORDER BY contactname, invoicenumber"
    l_strType = "EASTENDERS"
    l_blnPostToTextFile = True
ElseIf optShow(5).Value = True Then 'BFI
    l_strSQL = "SELECT jobID FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0)"
    l_strType = "BFI"
    l_blnPostToTextFile = True
ElseIf optShow(6).Value = True Then 'Sprockets
    l_strSQL = "SELECT jobID FROM job WHERE senttoSprocketsdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoSprocketsbatchnumber IS NULL OR senttoSprocketsbatchnumber = 0)"
    l_strType = "Sprockets"
    l_blnPostToTextFile = True
ElseIf optShow(7).Value = True Then 'ESI Invoices
    l_strSQL = "SELECT jobID, invoicenumber FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0)"
    l_strType = "SHINE"
    l_blnPostToTextFile = True
'ElseIf optShow(8).Value = True Then 'DADC Svensk
'    l_strSQL = "SELECT jobID FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0)"
'    l_strType = "Svensk"
'    l_blnPostToTextFile = True
ElseIf optShow(9).Value = True Then 'DADC Manual BBC
    l_strSQL = "SELECT jobID FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0)"
    l_strType = "SONYDADC"
    l_blnPostToTextFile = True
'ElseIf optShow(10).Value = True Then 'DADC Svensk Fixes
'    l_strSQL = "SELECT jobID FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0)"
'    l_strType = "Svensk"
'    l_blnPostToTextFile = True
'ElseIf optShow(11).Value = True Then 'DADC Nordisk
'    l_strSQL = "SELECT jobID FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0)"
'    l_strType = "DADC"
'    l_blnPostToTextFile = True
ElseIf optShow(12).Value = True Then 'DADC BBC Fixes
    l_strSQL = "SELECT jobID FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0)"
    l_strType = "DADC"
    l_blnPostToTextFile = True
ElseIf optShow(13).Value = True Then 'DADC BBC Creative Services
    l_strSQL = "SELECT jobID FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0)"
    l_strType = "DADC"
    l_blnPostToTextFile = True
ElseIf optShow(14).Value = True Then 'DADC BBC Playouts
    l_strSQL = "SELECT jobID FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0)"
    l_strType = "DADC"
    l_blnPostToTextFile = True
'ElseIf optShow(15).Value = True Then 'DADC Svensk iTunes
'    l_strSQL = "SELECT jobID FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0)"
'    l_strType = "Svensk"
'    l_blnPostToTextFile = True
'ElseIf optShow(16).Value = True Then 'DADC Hoanzl
'    l_strSQL = "SELECT jobID FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0)"
'    l_strType = "DADC"
'    l_blnPostToTextFile = True
ElseIf optShow(17).Value = True Then 'DADC Manual BBC
    l_strSQL = "SELECT jobID FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0)"
    l_strType = "SONYDADC"
    l_blnPostToTextFile = True
'ElseIf optShow(18).Value = True Then 'DADC Svensk Compilations
'    l_strSQL = "SELECT jobID FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0)"
'    l_strType = "Svensk"
'    l_blnPostToTextFile = True
ElseIf optShow(19).Value = True Then 'DADC BBC HDD
    l_strSQL = "SELECT jobID FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0)"
    l_strType = "DADC"
    l_blnPostToTextFile = True
'ElseIf optShow(20).Value = True Then 'DADC Scanbox
'    l_strSQL = "SELECT jobID FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0)"
'    l_strType = "DADC"
'    l_blnPostToTextFile = True
ElseIf optShow(21).Value = True Then 'ESI Credits
    l_strSQL = "SELECT jobID, creditnotenumber as invoicenumber FROM job WHERE senttoBBCdate = '" & Format(Date, "mm/dd/yyyy") & "' AND (senttoBBCbatchnumber IS NULL OR senttoBBCbatchnumber = 0)"
    l_strType = "SHINE"
    l_blnPostToTextFile = True
End If

'open the recordset of jobs prepared to be sent
Dim l_rstJobsToSend As New ADODB.Recordset
Set l_rstJobsToSend = ExecuteSQL(l_strSQL, g_strExecuteError)


CheckForSQLError

'if there is a record to send
If Not l_rstJobsToSend.EOF Then

    frmSendToAccounts.MousePointer = vbHourglass
    
    Dim l_lngBatchNumber As Long, l_lngCreditBatchNumber As Long, l_lngCurrentJobID As Long, l_strNotPostMsg As String
    
    'If we are sending to accounts and there are jobs to send, then send any new customers first.
    If l_blnPostToTextFile = False Then
        If PostNewCustomersToAccounts = False Then
            l_rstJobsToSend.Close
            Set l_rstJobsToSend = Nothing
            Exit Sub
        End If
    End If
    
    'get the next batch number
    l_lngBatchNumber = GetNextSequence("batchnumber")
    
    l_rstJobsToSend.MoveFirst
        
    If l_strType = "SHINE" Then
    
        Dim l_strEmailToSend As String, l_strAttachmentsToSend As String, l_strContactName As String
        l_strEmailToSend = GetData("Company", "invoiceemail", "companyID", 769)
        l_strContactName = GetData("job", "contactname", "jobID", l_rstJobsToSend("jobID"))
        l_strAttachmentsToSend = ""
        'Building a Batch Email to Send to Shine.
        'g_strPDFLocation & "\Invoices\" & l_lngItemID & ".pdf"
        Do While Not l_rstJobsToSend.EOF
            lblProgress.Caption = "Processing Job: " & l_rstJobsToSend("jobID")
            DoEvents
            'check to see if there are any problems with this jobs costing records
            Select Case JobSafeForPosting(l_rstJobsToSend("jobID"))
            Case vbuJobSafe
                'add the job to the email batch
                If Len(l_strAttachmentsToSend) <= 0 Then
                    l_strAttachmentsToSend = g_strPDFLocation & "\Invoices\" & l_rstJobsToSend("invoicenumber") & ".pdf"
                Else
                    l_strAttachmentsToSend = l_strAttachmentsToSend & ", " & g_strPDFLocation & "\Invoices\" & l_rstJobsToSend("invoicenumber") & ".pdf"
                End If
                SetData "job", "senttoBBCbatchnumber", "jobID", l_rstJobsToSend("jobID"), l_lngBatchNumber

            Case vbuJobNotSafeNoPONumber
                'no PO number on the invoice
                MsgBox l_strNotPostMsg & " there is no PO number on the invoice", vbExclamation
                ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
            
            Case vbuJobNotSafeCompanyAccountCode
                'no company account code
                MsgBox l_strNotPostMsg & " there is no company account code", vbExclamation
                ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
                
            Case vbuJobNotSafeCostingRows
                'problem with costing lines
                MsgBox l_strNotPostMsg & " there is at least one costing line without an account code", vbExclamation
                ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
                
            Case vbuJobNotSafePOTooLong
                MsgBox l_strNotPostMsg & " The PO number on the invoice has more than 50 characters", vbExclamation
                ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
            
            End Select
            
            
            'move to the next job
            l_rstJobsToSend.MoveNext
        Loop
        If optShow(21).Value = True Then
            SendOutlookEmail l_strEmailToSend, "Credit Notes raised for " & l_strContactName, "The PDF Credit Note(s) are attached.", l_strAttachmentsToSend, g_strUserEmailAddress, g_strAdministratorEmailAddress
        Else
            SendOutlookEmail l_strEmailToSend, "Invoices for jobs raised by " & l_strContactName, "The PDF Invoice(s) are attached.", l_strAttachmentsToSend, g_strUserEmailAddress, g_strAdministratorEmailAddress
        End If
    
    Else
    
        On Error GoTo OPENERROR
        'Open text file for ouput if not batching to accounts.
        If l_blnPostToTextFile = True Then
            Open g_strUploadInvoiceLocation & l_lngBatchNumber & ".txt" For Output As 1
        End If
        On Error GoTo 0
        
        'loop through each record and post to batch
        Do While Not l_rstJobsToSend.EOF
            
            'pick up the current job ID
            l_lngCurrentJobID = l_rstJobsToSend("jobID")
            l_strNotPostMsg = "The job with ID " & l_lngCurrentJobID & " can not be posted as "
            lblProgress.Caption = "Processing Job: " & l_lngCurrentJobID
            DoEvents
                    
            'check to see if there are any problems with this jobs costing records
            Select Case JobSafeForPosting(l_lngCurrentJobID)
            Case vbuJobSafe
                'post the job to accounts
                If l_blnPostToTextFile = True Then
                    SendJobToCustomer l_lngCurrentJobID, l_strType, l_lngBatchNumber
                Else
                    SendJobToAccounts l_lngCurrentJobID, l_strType, l_lngBatchNumber
                    
                    'update the job status, but use the parameter to stop the sent to accounts date being re-set
                    UpdateJobStatus l_lngCurrentJobID, "Sent To Accounts", , True, True
                    
                End If
            
            Case vbuJobNotSafeNoPONumber
                'no PO number on the invoice
                MsgBox l_strNotPostMsg & " there is no PO number on the invoice", vbExclamation
                ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
            
            Case vbuJobNotSafeCompanyAccountCode
                'no company account code
                MsgBox l_strNotPostMsg & " there is no company account code", vbExclamation
                ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
                
            Case vbuJobNotSafeCostingRows
                'problem with costing lines
                MsgBox l_strNotPostMsg & " there is at least one costing line without an account code", vbExclamation
                ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
                
            Case vbuJobNotSafePOTooLong
                MsgBox l_strNotPostMsg & " The PO number on the invoice has more than 50 characters", vbExclamation
                ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
            
            Case vbuJobNotSafeSAPcode
                MsgBox l_strNotPostMsg & " There is no SAP code against the current customer", vbExclamation
                ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
            
            End Select
            
            'move to the next job
            l_rstJobsToSend.MoveNext
        Loop
        lblProgress.Caption = ""
        DoEvents
        
        If l_blnPostToTextFile = True Then
            Close 1
        End If
        
        frmSendToAccounts.MousePointer = vbDefault
        MsgBox "Batch " & g_strBatchPrefix & l_lngBatchNumber & " has been successfully posted", vbInformation
    End If
Else
    MsgBox "There is nothing to send", vbExclamation
End If

l_rstJobsToSend.Close
Set l_rstJobsToSend = Nothing

'We need to also send any new customers at this point, and print the batch report, if its an acounts run, and not a BBC run.
If l_blnPostToTextFile = False Then
    PrintCrystalReport g_strLocationOfCrystalReportFiles & "Batchreport.rpt", "{batchjob.batch_number} = '" & g_strBatchPrefix & l_lngBatchNumber & "'", False
    ExportCrystalReport g_strLocationOfCrystalReportFiles & "Batchreport.rpt", "{batchjob.batch_number} = '" & g_strBatchPrefix & l_lngBatchNumber & "'", g_strUploadInvoiceLocation & l_lngBatchNumber & ".xls"
    ExportCrystalReport g_strLocationOfCrystalReportFiles & "Batchsagereport.rpt", "{batchjob.batch_number} = '" & g_strBatchPrefix & l_lngBatchNumber & "'", g_strUploadInvoiceLocation & l_lngBatchNumber & "_sage.xls"
    ExportCrystalReportAsPDF "MonthlyActForFinance.rpt", g_strUploadPDFLocation & "MonthlyActivityReoprt.pdf"
    NotifyByEMail g_strUploadInvoiceLocation & l_lngBatchNumber & ".xls"
    NotifyByEMail g_strUploadInvoiceLocation & l_lngBatchNumber & "_sage.xls"
    NotifyByEMail g_strUploadPDFLocation & "MonthlyActivityReoprt.pdf"
ElseIf l_strType = "BBCWW" Then
    PrintCrystalReport g_strLocationOfCrystalReportFiles & "BBCBatchreport.rpt", "{job.senttobbcbatchnumber} = " & l_lngBatchNumber, False
ElseIf l_strType = "BBCMG" Then
    PrintCrystalReport g_strLocationOfCrystalReportFiles & "BBCBatchreport.rpt", "{job.senttobbcbatchnumber} = " & l_lngBatchNumber, False
ElseIf l_strType = "Sprockets" Then
    PrintCrystalReport g_strLocationOfCrystalReportFiles & "batchreportsprockets.rpt", "{batchsprockets.batchnumber} = " & l_lngBatchNumber, False
ElseIf l_strType = "SONYDADC" Then
    PrintCrystalReport g_strLocationOfCrystalReportFiles & "DADCBatchreport.rpt", "{job.senttobbcbatchnumber} = " & l_lngBatchNumber, False
ElseIf l_strType = "SHINE" Then
'    PrintCrystalReport g_strLocationOfCrystalReportFiles & "ShineBatchreport.rpt", "{job.senttobbcbatchnumber} = " & l_lngBatchNumber, False
ElseIf l_strType = "Svensk" Then
    PrintCrystalReport g_strLocationOfCrystalReportFiles & "SvenskBatchreport.rpt", "{job.senttobbcbatchnumber} = " & l_lngBatchNumber, False
ElseIf l_strType = "DADC" Then
    PrintCrystalReport g_strLocationOfCrystalReportFiles & "DADCBatchreport.rpt", "{job.senttobbcbatchnumber} = " & l_lngBatchNumber, False
End If

adoJobListing.Refresh
txtSelectedCount.Text = Val(0)

Exit Sub

OPENERROR:

MsgBox "Failure to open Output file. Please verify that the folder" & vbCrLf & g_strUploadInvoiceLocation & " exists.", vbCritical, "Error opening Batch Output"
l_rstJobsToSend.Close
Set l_rstJobsToSend = Nothing

End Sub

Private Sub cmdUnbatch_Click()

Dim l_lngBatchNumber As Long, l_strSQL As String

l_lngBatchNumber = Val(InputBox("Please give the batch number be cleared.", "BBC Unbatch", 0))
If l_lngBatchNumber <> 0 Then
'    If g_intWebInvoicing = 1 Then
'        l_strSQL = "UPDATE accountstransaction SET senttoBBCdate = NULL, senttoBBCbatchnumber = NULL WHERE senttoBBCbatchnumber = " & l_lngBatchNumber & ";"
'    Else
        l_strSQL = "UPDATE job SET senttoBBCdate = NULL, senttoBBCbatchnumber = NULL WHERE senttoBBCbatchnumber = " & l_lngBatchNumber & ";"
'    End If
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
End If

End Sub

Private Sub Command1_Click()

Dim l_strSQL As String, l_rst As ADODB.Recordset

l_strSQL = "SELECT * FROM batchjob WHERE invoice_date >= '05-01-2014' AND invoice_date < '06-01-2014' AND customer_account_code <> 'SHI 03' ORDER BY invoice_number ASC;"
Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError
If l_rst.RecordCount > 0 Then
    l_rst.MoveFirst
    Do While Not l_rst.EOF
        l_rst("Sagecode") = GetData("ratecard", "VDMS_Charge_Code", "nominalcode", l_rst("account_code"))
        l_rst.Update
        l_rst.MoveNext
    Loop
End If
l_rst.Close

l_strSQL = "SELECT * FROM batchjob WHERE invoice_date >= '05-01-2014' AND invoice_date < '06-01-2014' AND customer_account_code = 'SHI 03' ORDER BY invoice_number ASC;"
Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError
If l_rst.RecordCount > 0 Then
    l_rst.MoveFirst
    Do While Not l_rst.EOF
        l_rst("PriorityCode") = "1464002"
        l_rst.Update
        l_rst.MoveNext
    Loop
End If
l_rst.Close

Set l_rst = Nothing

End Sub

Private Sub datDateToShow_Change()
optShow_Click 8
End Sub

Private Sub Form_Load()

cmbOrderBy.Text = "jobID"
cmbDirection.ListIndex = 1

datDateToShow.Value = Date

MakeLookLikeOffice Me
CenterForm Me

'Dim l_intLoop As Integer
'For l_intLoop = 0 To 6
'    optShow(l_intLoop).Visible = 0 - g_optUseJCAPostingToAccounts
'Next
'
cmdSendToAccounts.Visible = True
cmdUnbatch.Left = cmdSendToAccounts.Left
cmdUnbatch.Visible = True
    
grdJobListing.Refresh

If CheckAccess("/ManualPostToAccounts", True) Then
    optShow(0).Enabled = True
    optShow(1).Enabled = True
End If

End Sub

Private Sub Form_Resize()
On Error Resume Next

grdJobListing.Height = (Me.ScaleHeight - 3000) - grdJobListing.Top
grdNewJobListing.Height = (Me.ScaleHeight - 3000) - grdJobListing.Top
grdCosting.Top = grdJobListing.Top + grdJobListing.Height + 120
grdCosting.Height = Me.ScaleHeight - grdCosting.Top - 120

End Sub


Private Sub grdJobListing_BtnClick()

Dim l_strSQL As String

If grdJobListing.Row = grdJobListing.Rows Then Exit Sub

If txtCompany.Text = "Unattached Contacts" Then
    MsgBox "Cannot post jobs for 'Unattached Contacts'.", vbCritical, "Posting Error"
    Exit Sub
End If

l_strSQL = "UPDATE job SET "

If grdJobListing.Columns("senttoaccountsdate").Text <> "" Then
    grdJobListing.Columns("senttoaccountsdate").Text = ""
    grdJobListing.Columns("senttoaccountsuser").Text = ""
    If m_blnPostingToAccounts = True Then
        If GetData("job", "jobtype", "jobid", grdJobListing.Columns("jobid").Text) = "CREDIT" Then
            l_strSQL = l_strSQL & " creditsenttoaccountsdate = NULL, creditsenttoaccountsuser = NULL"
        Else
            l_strSQL = l_strSQL & " senttoaccountsdate = NULL, senttoaccountsuser = NULL"
        End If
    Else
        If cmdSendToAccounts.Caption = "Send to Sprockets" Then
            l_strSQL = l_strSQL & " senttosprocketsdate = NULL"
        Else
            l_strSQL = l_strSQL & " senttoBBCdate = NULL"
        End If
    End If
Else
    grdJobListing.Columns("senttoaccountsdate").Text = Date
    grdJobListing.Columns("senttoaccountsuser").Text = g_strUserInitials
    If m_blnPostingToAccounts = True Then
        If GetData("job", "jobtype", "jobid", grdJobListing.Columns("jobid").Text) = "CREDIT" Then
            l_strSQL = l_strSQL & " creditsenttoaccountsdate = '" & FormatSQLDate(Date) & "', creditsenttoaccountsuser = '" & g_strUserInitials & "'"
        Else
            l_strSQL = l_strSQL & " senttoaccountsdate = '" & FormatSQLDate(Date) & "', senttoaccountsuser = '" & g_strUserInitials & "'"
        End If
    Else
        If cmdSendToAccounts.Caption = "Send to Sprockets" Then
            l_strSQL = l_strSQL & " senttosprocketsdate = '" & FormatSQLDate(Date) & "'"
        Else
            l_strSQL = l_strSQL & " senttoBBCdate = '" & FormatSQLDate(Date) & "'"
        End If
    End If
End If

DoEvents

grdJobListing.Update

l_strSQL = l_strSQL & " WHERE jobID = '" & grdJobListing.Columns("jobID").Text & "';"
ExecuteSQL l_strSQL, g_strExecuteError

CheckForSQLError

End Sub

Private Sub grdJobListing_HeadClick(ByVal ColIndex As Integer)

If cmbOrderBy.Text <> grdJobListing.Columns(ColIndex).DataField Then
    cmbOrderBy.Text = grdJobListing.Columns(ColIndex).DataField
Else
    cmbDirection.ListIndex = 1 - cmbDirection.ListIndex
End If

datDateToShow_Change


End Sub

Private Sub grdJobListing_InitColumnProps()

If g_optUseJCAPostingToAccounts = 1 Then
    grdJobListing.Columns("accountnumber").Visible = False
    grdJobListing.Columns("projectnumber").Visible = False
    grdJobListing.Columns("cuser").Visible = False
    grdJobListing.Columns("startdate").Visible = False
    grdJobListing.Columns("title").Visible = False
    grdJobListing.Columns("companyname").Width = 3000
    
End If

grdJobListing.ColumnHeaders = True

End Sub

Private Sub grdNewJobListing_BtnClick()

Dim l_strSQL As String

If grdNewJobListing.Row = grdNewJobListing.Rows Then Exit Sub

If txtCompany.Text = "Unattached Contacts" Then
    MsgBox "Cannot post jobs for 'Unattached Contacts'.", vbCritical, "Posting Error"
    Exit Sub
End If

l_strSQL = "UPDATE accountstransaction SET "

If grdNewJobListing.Columns("senttoaccountsdate").Text <> "" Then
    grdNewJobListing.Columns("senttoaccountsdate").Text = ""
    grdNewJobListing.Columns("senttoaccountsuser").Text = ""
    If m_blnPostingToAccounts = True Then
        l_strSQL = l_strSQL & " senttoaccountsdate = NULL, senttoaccountsuser = NULL"
    Else
        If cmdSendToAccounts.Caption = "Send to Sprockets" Then
            l_strSQL = l_strSQL & " senttosprocketsdate = NULL"
        Else
            l_strSQL = l_strSQL & " senttoBBCdate = NULL"
        End If
    End If
Else
    grdNewJobListing.Columns("senttoaccountsdate").Text = Date
    grdNewJobListing.Columns("senttoaccountsuser").Text = g_strUserInitials
    If m_blnPostingToAccounts = True Then
        l_strSQL = l_strSQL & " senttoaccountsdate = '" & FormatSQLDate(Date) & "', senttoaccountsuser = '" & g_strUserInitials & "'"
    Else
        If cmdSendToAccounts.Caption = "Send to Sprockets" Then
            l_strSQL = l_strSQL & " senttosprocketsdate = '" & FormatSQLDate(Date) & "'"
        Else
            l_strSQL = l_strSQL & " senttoBBCdate = '" & FormatSQLDate(Date) & "'"
        End If
    End If
End If

DoEvents

grdNewJobListing.Update

l_strSQL = l_strSQL & " WHERE accountstransactionID = '" & grdNewJobListing.Columns("jobID").Text & "';"
ExecuteSQL l_strSQL, g_strExecuteError

CheckForSQLError

End Sub

Private Sub grdNewJobListing_HeadClick(ByVal ColIndex As Integer)

If cmbOrderBy.Text <> grdNewJobListing.Columns(ColIndex).DataField Then
    cmbOrderBy.Text = grdNewJobListing.Columns(ColIndex).DataField
Else
    cmbDirection.ListIndex = 1 - cmbDirection.ListIndex
End If

datDateToShow_Change

End Sub

Private Sub grdNewJobListing_InitColumnProps()

If g_optUseJCAPostingToAccounts = 1 Then
    grdNewJobListing.Columns("accountnumber").Visible = False
    grdNewJobListing.Columns("projectnumber").Visible = False
    grdNewJobListing.Columns("cuser").Visible = False
    grdNewJobListing.Columns("startdate").Visible = False
    grdNewJobListing.Columns("title").Visible = False
    grdNewJobListing.Columns("companyname").Width = 3000
    
End If

grdNewJobListing.ColumnHeaders = True

End Sub

Private Sub grdNewJobListing_RowLoaded(ByVal Bookmark As Variant)

grdNewJobListing.Columns("company").Text = GetData("company", "name", "companyID", Val(grdNewJobListing.Columns("companyID").Text))

End Sub

Private Sub optShow_Click(Index As Integer)

Dim l_strWhereSQL As String, l_strSQL As String

Select Case Index

Case 0
    'invoices
    l_strWhereSQL = " (job.invoiceddate Is Not null) and (job.jobtype <> 'CREDIT') AND (job.senttoaccountsdate Is null) AND (job.cancelleddate Is null) AND (job.flagquote <> " & GetFlag(1) & ")"
    l_strSQL = "SELECT * FROM job WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, jobID"
    optShow(1).Enabled = False
    optShow(2).Enabled = False
    optShow(3).Enabled = False
    optShow(4).Enabled = False
    optShow(5).Enabled = False
    optShow(6).Enabled = False
    optShow(7).Enabled = False
    optShow(8).Enabled = False
    optShow(9).Enabled = False
    optShow(10).Enabled = False
    optShow(11).Enabled = False
    optShow(12).Enabled = False
    optShow(13).Enabled = False
    optShow(14).Enabled = False
    optShow(15).Enabled = False
    optShow(16).Enabled = False
    optShow(17).Enabled = False
    optShow(18).Enabled = False
    optShow(19).Enabled = False
    optShow(20).Enabled = False
    optShow(21).Enabled = False
    cmdSendToAccounts.Caption = "Send to Accounts"
    m_blnPostingToAccounts = True

Case 1
    'credit notes
    l_strWhereSQL = "(jobtype='CREDIT' AND job.creditnotedate IS NOT NULL AND creditsenttoaccountsdate Is Null AND cancelleddate Is null AND flagquote <> " & GetFlag(1) & ")"
    l_strSQL = "SELECT jobID, creditnotenumber as invoicenumber, creditsenttoaccountsdate as senttoaccountsdate, creditsenttoaccountsuser as senttoaccountsuser, companyname, creditnotedate as invoiceddate, deadlinedate, createddate, createduser, companyID, contactname, contactID, telephone, email, fax, orderreference, title1, title2, startdate FROM job WHERE " & l_strWhereSQL & " ORDER BY creditnotenumber, jobID"
    optShow(0).Enabled = False
    optShow(2).Enabled = False
    optShow(3).Enabled = False
    optShow(4).Enabled = False
    optShow(5).Enabled = False
    optShow(6).Enabled = False
    optShow(7).Enabled = False
    optShow(8).Enabled = False
    optShow(9).Enabled = False
    optShow(10).Enabled = False
    optShow(11).Enabled = False
    optShow(12).Enabled = False
    optShow(13).Enabled = False
    optShow(14).Enabled = False
    optShow(15).Enabled = False
    optShow(16).Enabled = False
    optShow(17).Enabled = False
    optShow(18).Enabled = False
    optShow(19).Enabled = False
    optShow(20).Enabled = False
    optShow(21).Enabled = False
    cmdSendToAccounts.Caption = "Send to Accounts"
    m_blnPostingToAccounts = True

Case 2
    'BBC W/W Int Ops ordered through their STAR order system
    l_strWhereSQL = " ((job.fd_status = 'Costed') OR (job.fd_status = 'Sent To Accounts')) AND (job.companyID=570) and (job.jobtype <> 'CREDIT') AND (job.invoiceddate IS NOT Null) AND (job.cancelleddate Is null) AND (job.senttoBBCdate Is Null) AND (job.flagquote <> " & GetFlag(1) & ")"
    l_strSQL = "SELECT job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate, extendedjob.BBCnominalcode FROM job INNER JOIN extendedjob ON job.jobid = extendedjob.jobID WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, job.jobID"
    optShow(0).Enabled = False
    optShow(1).Enabled = False
    optShow(3).Enabled = False
    optShow(4).Enabled = False
    optShow(5).Enabled = False
    optShow(6).Enabled = False
    optShow(7).Enabled = False
    optShow(8).Enabled = False
    optShow(9).Enabled = False
    optShow(10).Enabled = False
    optShow(11).Enabled = False
    optShow(12).Enabled = False
    optShow(13).Enabled = False
    optShow(14).Enabled = False
    optShow(15).Enabled = False
    optShow(16).Enabled = False
    optShow(17).Enabled = False
    optShow(18).Enabled = False
    optShow(19).Enabled = False
    optShow(20).Enabled = False
    optShow(21).Enabled = False
    cmdSendToAccounts.Caption = "Send to BBC"
    m_blnPostingToAccounts = False

Case 3
    'BBC W/W Int Ops Not ordered through their STAR system
    l_strWhereSQL = " ((job.fd_status = 'Costed') OR (job.fd_status = 'Sent To Accounts')) AND (job.companyID=919) and (job.jobtype <> 'CREDIT') AND (job.invoiceddate IS NOT Null) AND (job.cancelleddate Is null) AND (job.senttoBBCdate Is Null) AND (job.flagquote <> " & GetFlag(1) & ")"
    l_strSQL = "SELECT job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate FROM job WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, job.jobID"
    optShow(0).Enabled = False
    optShow(1).Enabled = False
    optShow(2).Enabled = False
    optShow(4).Enabled = False
    optShow(5).Enabled = False
    optShow(6).Enabled = False
    optShow(7).Enabled = False
    optShow(8).Enabled = False
    optShow(9).Enabled = False
    optShow(10).Enabled = False
    optShow(11).Enabled = False
    optShow(12).Enabled = False
    optShow(13).Enabled = False
    optShow(14).Enabled = False
    optShow(15).Enabled = False
    optShow(16).Enabled = False
    optShow(17).Enabled = False
    optShow(18).Enabled = False
    optShow(19).Enabled = False
    optShow(20).Enabled = False
    optShow(21).Enabled = False
    cmdSendToAccounts.Caption = "Send to BBC"
    m_blnPostingToAccounts = False

Case 4
    'BBC W/W Eastenders Project
    l_strWhereSQL = " (job.companyID=1574) and (job.jobtype <> 'CREDIT') AND (fd_status = 'Hold Cost') AND (job.invoiceddate IS Null) AND (job.cancelleddate Is null) AND (job.senttoBBCdate Is Null) AND (job.flagquote <> " & GetFlag(1) & ")"
    l_strSQL = "SELECT job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate FROM job WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, job.jobID"
    optShow(0).Enabled = False
    optShow(1).Enabled = False
    optShow(2).Enabled = False
    optShow(3).Enabled = False
    optShow(5).Enabled = False
    optShow(6).Enabled = False
    optShow(7).Enabled = False
    optShow(8).Enabled = False
    optShow(9).Enabled = False
    optShow(10).Enabled = False
    optShow(11).Enabled = False
    optShow(12).Enabled = False
    optShow(13).Enabled = False
    optShow(14).Enabled = False
    optShow(15).Enabled = False
    optShow(16).Enabled = False
    optShow(17).Enabled = False
    optShow(18).Enabled = False
    optShow(19).Enabled = False
    optShow(20).Enabled = False
    optShow(21).Enabled = False
    cmdSendToAccounts.Caption = "Send to BBC"
    m_blnPostingToAccounts = False

Case 5
    'BFI Invoices for sending to them.
    l_strWhereSQL = "(job.companyID = 1244) and (job.jobtype <> 'CREDIT') AND (invoiceddate IS NOT Null) AND (cancelleddate Is null) AND (senttoBBCdate Is Null) AND (flagquote <> " & GetFlag(1) & ")"
    l_strSQL = "SELECT company.salesledgercode, job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate FROM job INNER JOIN company ON job.companyID = company.companyID WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, jobID"
    optShow(0).Enabled = False
    optShow(1).Enabled = False
    optShow(2).Enabled = False
    optShow(3).Enabled = False
    optShow(4).Enabled = False
    optShow(6).Enabled = False
    optShow(7).Enabled = False
    optShow(8).Enabled = False
    optShow(9).Enabled = False
    optShow(10).Enabled = False
    optShow(11).Enabled = False
    optShow(12).Enabled = False
    optShow(13).Enabled = False
    optShow(14).Enabled = False
    optShow(15).Enabled = False
    optShow(16).Enabled = False
    optShow(17).Enabled = False
    optShow(18).Enabled = False
    optShow(19).Enabled = False
    optShow(20).Enabled = False
    optShow(21).Enabled = False
    cmdSendToAccounts.Caption = "Send to BFI"
    m_blnPostingToAccounts = False

Case 6
    'Sprockets Batch System
    l_strWhereSQL = " (job.fd_status = 'Sent To Accounts') AND (job.jobtype <> 'CREDIT') AND (job.invoiceddate IS NOT Null) AND (job.cancelleddate Is null) AND "
    l_strWhereSQL = l_strWhereSQL & "(costing.costcode IN (SELECT description FROM xref WHERE category = 'dformat' AND videostandard = 'SPROCKETS') OR "
    l_strWhereSQL = l_strWhereSQL & "costing.costcode IN (SELECT format FROM xref WHERE category = 'OTHER' AND videostandard = 'SPROCKETS')) "
'        l_strWhereSQL = l_strWhereSQL & "AND job.stillowedtojca = 0 "
    l_strWhereSQL = l_strWhereSQL & "AND (job.senttoSprocketsdate Is Null) AND (job.flagquote <> " & GetFlag(1) & ")"
    l_strSQL = "SELECT DISTINCT job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoSprocketsdate as senttoaccountsdate FROM job INNER JOIN costing ON job.jobid = costing.jobID WHERE " & l_strWhereSQL & " ORDER BY job.invoicenumber, job.jobID"
    optShow(0).Enabled = False
    optShow(1).Enabled = False
    optShow(2).Enabled = False
    optShow(3).Enabled = False
    optShow(4).Enabled = False
    optShow(5).Enabled = False
    optShow(7).Enabled = False
    optShow(8).Enabled = False
    optShow(9).Enabled = False
    optShow(10).Enabled = False
    optShow(11).Enabled = False
    optShow(12).Enabled = False
    optShow(13).Enabled = False
    optShow(14).Enabled = False
    optShow(15).Enabled = False
    optShow(16).Enabled = False
    optShow(17).Enabled = False
    optShow(18).Enabled = False
    optShow(19).Enabled = False
    optShow(20).Enabled = False
    optShow(21).Enabled = False
    cmdSendToAccounts.Caption = "Send to Sprockets"
    m_blnPostingToAccounts = False

Case 7
    'Shine International Invoices for sending to them.
    l_strWhereSQL = "(job.companyID = 769 OR job.companyID = 1313) and (job.jobtype <> 'CREDIT') AND (invoiceddate IS NOT Null) AND (cancelleddate Is null) AND (senttoBBCdate Is Null) AND (flagquote <> " & GetFlag(1) & ")"
    l_strSQL = "SELECT company.salesledgercode, job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate FROM job INNER JOIN company ON job.companyID = company.companyID WHERE " & l_strWhereSQL & " ORDER BY contactname, invoicenumber, jobID"
    optShow(0).Enabled = False
    optShow(1).Enabled = False
    optShow(2).Enabled = False
    optShow(3).Enabled = False
    optShow(4).Enabled = False
    optShow(5).Enabled = False
    optShow(6).Enabled = False
    optShow(8).Enabled = False
    optShow(9).Enabled = False
    optShow(10).Enabled = False
    optShow(11).Enabled = False
    optShow(12).Enabled = False
    optShow(13).Enabled = False
    optShow(14).Enabled = False
    optShow(15).Enabled = False
    optShow(16).Enabled = False
    optShow(17).Enabled = False
    optShow(18).Enabled = False
    optShow(19).Enabled = False
    optShow(20).Enabled = False
    optShow(21).Enabled = False
    cmdSendToAccounts.Caption = "Build Email for ESI"
    m_blnPostingToAccounts = False
    
'Case 8
'
'    'Svensk Ingest Jobs Posting to audit text file for contract audits.
'    l_strWhereSQL = "(job.companyID = 1415) and (job.jobtype <> 'CREDIT') AND (invoiceddate IS NOT Null) AND (cancelleddate Is null) AND (senttoBBCdate Is Null) AND (flagquote <> " & GetFlag(1) & ")"
'    l_strSQL = "SELECT company.salesledgercode, job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate FROM job INNER JOIN company ON job.companyID = company.companyID WHERE " & l_strWhereSQL & " ORDER BY jobID"
'    optShow(0).Enabled = False
'    optShow(1).Enabled = False
'    optShow(2).Enabled = False
'    optShow(3).Enabled = False
'    optShow(4).Enabled = False
'    optShow(5).Enabled = False
'    optShow(6).Enabled = False
'    optShow(7).Enabled = False
'    optShow(9).Enabled = False
'    optShow(10).Enabled = False
'    optShow(11).Enabled = False
'    optShow(12).Enabled = False
'    optShow(13).Enabled = False
'    optShow(14).Enabled = False
'    optShow(15).Enabled = False
'    optShow(16).Enabled = False
'    optShow(17).Enabled = False
'    optShow(18).Enabled = False
'    optShow(19).Enabled = False
'    optShow(20).Enabled = False
'    optShow(21).Enabled = False
'    cmdSendToAccounts.Caption = "Send to Sony DADC Ltd. (Svensk Ingest)"
'    m_blnPostingToAccounts = False
'
'
Case 9
    
    'Sony DADC Ltd (Manual) Invoices for sending to them.
    l_strWhereSQL = "(job.companyID = 1261) and (job.jobtype <> 'CREDIT') AND (fd_status = 'Hold Cost') AND (cancelleddate Is null) AND (senttoBBCdate Is Null) AND (flagquote <> " & GetFlag(1) & ")"
    l_strSQL = "SELECT company.salesledgercode, job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate FROM job INNER JOIN company ON job.companyID = company.companyID WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, jobID"
    optShow(0).Enabled = False
    optShow(1).Enabled = False
    optShow(2).Enabled = False
    optShow(3).Enabled = False
    optShow(4).Enabled = False
    optShow(5).Enabled = False
    optShow(6).Enabled = False
    optShow(7).Enabled = False
    optShow(8).Enabled = False
    optShow(10).Enabled = False
    optShow(11).Enabled = False
    optShow(12).Enabled = False
    optShow(13).Enabled = False
    optShow(14).Enabled = False
    optShow(15).Enabled = False
    optShow(16).Enabled = False
    optShow(17).Enabled = False
    optShow(18).Enabled = False
    optShow(19).Enabled = False
    optShow(20).Enabled = False
    optShow(21).Enabled = False
    cmdSendToAccounts.Caption = "Send to Sony DADC Ltd. (Manual)"
    m_blnPostingToAccounts = False
    
'Case 10
'
'    'Sony DADC Ltd (Svensk Fixes) Invoices for sending to them.
'    l_strWhereSQL = "(job.companyID = 1330) and (job.jobtype <> 'CREDIT') AND (invoiceddate IS NOT Null) AND (cancelleddate Is null) AND (senttoBBCdate Is Null) AND (flagquote <> " & GetFlag(1) & ")"
'    l_strSQL = "SELECT company.salesledgercode, job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate FROM job INNER JOIN company ON job.companyID = company.companyID WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, jobID"
'    optShow(0).Enabled = False
'    optShow(1).Enabled = False
'    optShow(2).Enabled = False
'    optShow(3).Enabled = False
'    optShow(4).Enabled = False
'    optShow(5).Enabled = False
'    optShow(6).Enabled = False
'    optShow(7).Enabled = False
'    optShow(8).Enabled = False
'    optShow(9).Enabled = False
'    optShow(11).Enabled = False
'    optShow(12).Enabled = False
'    optShow(13).Enabled = False
'    optShow(14).Enabled = False
'    optShow(15).Enabled = False
'    optShow(16).Enabled = False
'    optShow(17).Enabled = False
'    optShow(18).Enabled = False
'    optShow(19).Enabled = False
'    optShow(20).Enabled = False
'    optShow(21).Enabled = False
'    cmdSendToAccounts.Caption = "Send to Sony DADC Ltd. (Svensk Fixes)"
'    m_blnPostingToAccounts = False
'
'Case 11
'
'    'Sony DADC Ltd (Nordic) Invoices for sending to them.
'    l_strWhereSQL = "(job.companyID = 1419) and (job.jobtype <> 'CREDIT') AND (invoiceddate IS NOT Null) AND (cancelleddate Is null) AND (senttoBBCdate Is Null) AND (flagquote <> " & GetFlag(1) & ")"
'    l_strSQL = "SELECT company.salesledgercode, job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate FROM job INNER JOIN company ON job.companyID = company.companyID WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, jobID"
'    optShow(0).Enabled = False
'    optShow(1).Enabled = False
'    optShow(2).Enabled = False
'    optShow(3).Enabled = False
'    optShow(4).Enabled = False
'    optShow(5).Enabled = False
'    optShow(6).Enabled = False
'    optShow(7).Enabled = False
'    optShow(8).Enabled = False
'    optShow(9).Enabled = False
'    optShow(10).Enabled = False
'    optShow(12).Enabled = False
'    optShow(13).Enabled = False
'    optShow(14).Enabled = False
'    optShow(15).Enabled = False
'    optShow(16).Enabled = False
'    optShow(17).Enabled = False
'    optShow(18).Enabled = False
'    optShow(19).Enabled = False
'    optShow(20).Enabled = False
'    optShow(21).Enabled = False
'    cmdSendToAccounts.Caption = "Send to Sony DADC Ltd. (Nordisk)"
'    m_blnPostingToAccounts = False
'
Case 12
    
    'Sony DADC Ltd (UK Fixes) Invoices for sending to them.
    l_strWhereSQL = "(job.companyID = 1292) and (job.jobtype <> 'CREDIT') AND (fd_status = 'Hold Cost') AND (cancelleddate Is null) AND (senttoBBCdate Is Null) AND (flagquote <> " & GetFlag(1) & ")"
    l_strSQL = "SELECT company.salesledgercode, job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate FROM job INNER JOIN company ON job.companyID = company.companyID WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, jobID"
    optShow(0).Enabled = False
    optShow(1).Enabled = False
    optShow(2).Enabled = False
    optShow(3).Enabled = False
    optShow(4).Enabled = False
    optShow(5).Enabled = False
    optShow(6).Enabled = False
    optShow(7).Enabled = False
    optShow(8).Enabled = False
    optShow(9).Enabled = False
    optShow(10).Enabled = False
    optShow(11).Enabled = False
    optShow(13).Enabled = False
    optShow(14).Enabled = False
    optShow(15).Enabled = False
    optShow(16).Enabled = False
    optShow(17).Enabled = False
    optShow(18).Enabled = False
    optShow(19).Enabled = False
    optShow(20).Enabled = False
    optShow(21).Enabled = False
    cmdSendToAccounts.Caption = "Send to Sony DADC Ltd. (UK Fixes)"
    m_blnPostingToAccounts = False
    
Case 13
    
    'Sony DADC Ltd (Creative) Invoices for sending to them.
    l_strWhereSQL = "(job.companyID = 1291) and (job.jobtype <> 'CREDIT') AND (fd_status = 'Hold Cost') AND (cancelleddate Is null) AND (senttoBBCdate Is Null) AND (flagquote <> " & GetFlag(1) & ")"
    l_strSQL = "SELECT company.salesledgercode, job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate FROM job INNER JOIN company ON job.companyID = company.companyID WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, jobID"
    optShow(0).Enabled = False
    optShow(1).Enabled = False
    optShow(2).Enabled = False
    optShow(3).Enabled = False
    optShow(4).Enabled = False
    optShow(5).Enabled = False
    optShow(6).Enabled = False
    optShow(7).Enabled = False
    optShow(8).Enabled = False
    optShow(9).Enabled = False
    optShow(10).Enabled = False
    optShow(11).Enabled = False
    optShow(12).Enabled = False
    optShow(14).Enabled = False
    optShow(15).Enabled = False
    optShow(16).Enabled = False
    optShow(17).Enabled = False
    optShow(18).Enabled = False
    optShow(19).Enabled = False
    optShow(20).Enabled = False
    optShow(21).Enabled = False
    cmdSendToAccounts.Caption = "Send to Sony DADC Ltd. (Creative)"
    m_blnPostingToAccounts = False
    
Case 14
    
    'Sony DADC Ltd (UK Playouts) Invoices for sending to them.
    l_strWhereSQL = "(job.companyID = 1370) and (job.jobtype <> 'CREDIT') AND (fd_status = 'Hold Cost') AND (cancelleddate Is null) AND (senttoBBCdate Is Null) AND (flagquote <> " & GetFlag(1) & ")"
    l_strSQL = "SELECT company.salesledgercode, job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate FROM job INNER JOIN company ON job.companyID = company.companyID WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, jobID"
    optShow(0).Enabled = False
    optShow(1).Enabled = False
    optShow(2).Enabled = False
    optShow(3).Enabled = False
    optShow(4).Enabled = False
    optShow(5).Enabled = False
    optShow(6).Enabled = False
    optShow(7).Enabled = False
    optShow(8).Enabled = False
    optShow(9).Enabled = False
    optShow(10).Enabled = False
    optShow(11).Enabled = False
    optShow(12).Enabled = False
    optShow(13).Enabled = False
    optShow(15).Enabled = False
    optShow(16).Enabled = False
    optShow(17).Enabled = False
    optShow(18).Enabled = False
    optShow(19).Enabled = False
    optShow(20).Enabled = False
    optShow(21).Enabled = False
    cmdSendToAccounts.Caption = "Send to Sony DADC Ltd. (UK)"
    m_blnPostingToAccounts = False
    
'Case 15
'
'    'Sony DADC Ltd (Svensk iTunes) Invoices for sending to them.
'    l_strWhereSQL = "(job.companyID = 1417) and (job.jobtype <> 'CREDIT') AND (invoiceddate IS NOT Null) AND (cancelleddate Is null) AND (senttoBBCdate Is Null) AND (flagquote <> " & GetFlag(1) & ")"
'    l_strSQL = "SELECT company.salesledgercode, job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate FROM job INNER JOIN company ON job.companyID = company.companyID WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, jobID"
'    optShow(0).Enabled = False
'    optShow(1).Enabled = False
'    optShow(2).Enabled = False
'    optShow(3).Enabled = False
'    optShow(4).Enabled = False
'    optShow(5).Enabled = False
'    optShow(6).Enabled = False
'    optShow(7).Enabled = False
'    optShow(8).Enabled = False
'    optShow(9).Enabled = False
'    optShow(10).Enabled = False
'    optShow(11).Enabled = False
'    optShow(12).Enabled = False
'    optShow(13).Enabled = False
'    optShow(14).Enabled = False
'    optShow(16).Enabled = False
'    optShow(17).Enabled = False
'    optShow(18).Enabled = False
'    optShow(19).Enabled = False
'    optShow(20).Enabled = False
'    optShow(21).Enabled = False
'    cmdSendToAccounts.Caption = "Send to Sony DADC Ltd. (Svensk iTunes)"
'    m_blnPostingToAccounts = False
'
'Case 16
'
'    'Sony DADC Ltd (Hoanzl) Invoices for sending to them.
'    l_strWhereSQL = "(job.companyID = 1416) and (job.jobtype <> 'CREDIT') AND (invoiceddate IS NOT Null) AND (cancelleddate Is null) AND (senttoBBCdate Is Null) AND (flagquote <> " & GetFlag(1) & ")"
'    l_strSQL = "SELECT company.salesledgercode, job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate FROM job INNER JOIN company ON job.companyID = company.companyID WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, jobID"
'    optShow(0).Enabled = False
'    optShow(1).Enabled = False
'    optShow(2).Enabled = False
'    optShow(3).Enabled = False
'    optShow(4).Enabled = False
'    optShow(5).Enabled = False
'    optShow(6).Enabled = False
'    optShow(7).Enabled = False
'    optShow(8).Enabled = False
'    optShow(9).Enabled = False
'    optShow(10).Enabled = False
'    optShow(11).Enabled = False
'    optShow(12).Enabled = False
'    optShow(13).Enabled = False
'    optShow(14).Enabled = False
'    optShow(15).Enabled = False
'    optShow(17).Enabled = False
'    optShow(18).Enabled = False
'    optShow(19).Enabled = False
'    optShow(20).Enabled = False
'    optShow(21).Enabled = False
'    cmdSendToAccounts.Caption = "Send to Sony DADC Ltd. (Hoanzl)"
'    m_blnPostingToAccounts = False
'
Case 17

    'Sony DADC Ltd (Manual) Invoices for sending to them.
    l_strWhereSQL = "(job.companyID = 1239) and (job.jobtype <> 'CREDIT') AND (fd_status = 'Hold Cost') AND (cancelleddate Is null) AND (senttoBBCdate Is Null) AND (flagquote <> " & GetFlag(1) & ")"
    l_strSQL = "SELECT company.salesledgercode, job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate FROM job INNER JOIN company ON job.companyID = company.companyID WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, jobID"
    optShow(0).Enabled = False
    optShow(1).Enabled = False
    optShow(2).Enabled = False
    optShow(3).Enabled = False
    optShow(4).Enabled = False
    optShow(5).Enabled = False
    optShow(6).Enabled = False
    optShow(7).Enabled = False
    optShow(8).Enabled = False
    optShow(9).Enabled = False
    optShow(10).Enabled = False
    optShow(11).Enabled = False
    optShow(12).Enabled = False
    optShow(13).Enabled = False
    optShow(14).Enabled = False
    optShow(15).Enabled = False
    optShow(16).Enabled = False
    optShow(18).Enabled = False
    optShow(19).Enabled = False
    optShow(20).Enabled = False
    optShow(21).Enabled = False
    cmdSendToAccounts.Caption = "Send to Sony DADC Ltd. (Bulk Audio)"
    m_blnPostingToAccounts = False
    
'Case 18
'
'    'Sony DADC Ltd (Svensk Compilations) Invoices for sending to them.
'    l_strWhereSQL = "(job.companyID = 1434) and (job.jobtype <> 'CREDIT') AND (invoiceddate IS NOT Null) AND (cancelleddate Is null) AND (senttoBBCdate Is Null) AND (flagquote <> " & GetFlag(1) & ")"
'    l_strSQL = "SELECT company.salesledgercode, job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate FROM job INNER JOIN company ON job.companyID = company.companyID WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, jobID"
'    optShow(0).Enabled = False
'    optShow(1).Enabled = False
'    optShow(2).Enabled = False
'    optShow(3).Enabled = False
'    optShow(4).Enabled = False
'    optShow(5).Enabled = False
'    optShow(6).Enabled = False
'    optShow(7).Enabled = False
'    optShow(8).Enabled = False
'    optShow(9).Enabled = False
'    optShow(10).Enabled = False
'    optShow(11).Enabled = False
'    optShow(12).Enabled = False
'    optShow(13).Enabled = False
'    optShow(14).Enabled = False
'    optShow(15).Enabled = False
'    optShow(16).Enabled = False
'    optShow(17).Enabled = False
'    optShow(19).Enabled = False
'    optShow(20).Enabled = False
'    optShow(21).Enabled = False
'    cmdSendToAccounts.Caption = "Send to Sony DADC Ltd. (Svensk Compilations)"
'    m_blnPostingToAccounts = False
'
Case 19
    
    'Sony DADC Ltd (BBC HDD Project) Invoices for sending to them.
    l_strWhereSQL = "(job.companyID = 1418) and (job.jobtype <> 'CREDIT') AND (fd_status = 'Hold Cost') AND (cancelleddate Is null) AND (senttoBBCdate Is Null) AND (flagquote <> " & GetFlag(1) & ")"
    l_strSQL = "SELECT company.salesledgercode, job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate FROM job INNER JOIN company ON job.companyID = company.companyID WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, jobID"
    optShow(0).Enabled = False
    optShow(1).Enabled = False
    optShow(2).Enabled = False
    optShow(3).Enabled = False
    optShow(4).Enabled = False
    optShow(5).Enabled = False
    optShow(6).Enabled = False
    optShow(7).Enabled = False
    optShow(8).Enabled = False
    optShow(9).Enabled = False
    optShow(10).Enabled = False
    optShow(11).Enabled = False
    optShow(12).Enabled = False
    optShow(13).Enabled = False
    optShow(14).Enabled = False
    optShow(15).Enabled = False
    optShow(16).Enabled = False
    optShow(17).Enabled = False
    optShow(18).Enabled = False
    optShow(20).Enabled = False
    optShow(21).Enabled = False
    cmdSendToAccounts.Caption = "Send to Sony DADC Ltd. (BBC HDD Deliveries)"
    m_blnPostingToAccounts = False

'Case 20
'
'    'Sony DADC Ltd (Scanbox) Invoices for sending to them.
'    l_strWhereSQL = "(job.companyID = 1435) and (job.jobtype <> 'CREDIT') AND (invoiceddate IS NOT Null) AND (cancelleddate Is null) AND (senttoBBCdate Is Null) AND (flagquote <> " & GetFlag(1) & ")"
'    l_strSQL = "SELECT company.salesledgercode, job.jobID, job.companyID, job.contactname, job.orderreference, job.telephone, job.deadlinedate, job.title1, job.title2, job.createddate, job.createduser, job.invoicenumber, job.companyname, job.invoiceddate, job.senttoBBCdate as senttoaccountsdate FROM job INNER JOIN company ON job.companyID = company.companyID WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, jobID"
'    optShow(0).Enabled = False
'    optShow(1).Enabled = False
'    optShow(2).Enabled = False
'    optShow(3).Enabled = False
'    optShow(4).Enabled = False
'    optShow(5).Enabled = False
'    optShow(6).Enabled = False
'    optShow(7).Enabled = False
'    optShow(8).Enabled = False
'    optShow(9).Enabled = False
'    optShow(10).Enabled = False
'    optShow(11).Enabled = False
'    optShow(12).Enabled = False
'    optShow(13).Enabled = False
'    optShow(14).Enabled = False
'    optShow(15).Enabled = False
'    optShow(16).Enabled = False
'    optShow(17).Enabled = False
'    optShow(18).Enabled = False
'    optShow(19).Enabled = False
'    optShow(21).Enabled = False
'    cmdSendToAccounts.Caption = "Send to Sony DADC Ltd. (BBC HDD Deliveries)"
'    m_blnPostingToAccounts = False
'
Case 21
    'Shine International credit notes for sending to them.
    l_strWhereSQL = "(job.companyID = 769 OR job.companyID = 1313) and (jobtype='CREDIT' AND job.creditnotedate IS NOT NULL AND senttoBBCdate Is Null AND cancelleddate Is null AND flagquote <> " & GetFlag(1) & ")"
    l_strSQL = "SELECT company.salesledgercode, job.jobID, creditnotenumber as invoicenumber, job.senttoBBCdate as senttoaccountsdate, companyname, creditnotedate as invoiceddate, deadlinedate, job.createddate, job.createduser, job.companyID, contactname, contactID, job.telephone, job.email, job.fax, orderreference, title1, title2, startdate FROM job INNER JOIN company ON job.companyID = company.companyID WHERE " & l_strWhereSQL & " ORDER BY contactname, creditnotenumber, jobID"
    optShow(0).Enabled = False
    optShow(1).Enabled = False
    optShow(2).Enabled = False
    optShow(3).Enabled = False
    optShow(4).Enabled = False
    optShow(5).Enabled = False
    optShow(6).Enabled = False
    optShow(7).Enabled = False
    optShow(8).Enabled = False
    optShow(9).Enabled = False
    optShow(10).Enabled = False
    optShow(11).Enabled = False
    optShow(12).Enabled = False
    optShow(13).Enabled = False
    optShow(14).Enabled = False
    optShow(15).Enabled = False
    optShow(16).Enabled = False
    optShow(17).Enabled = False
    optShow(18).Enabled = False
    optShow(19).Enabled = False
    optShow(20).Enabled = False
    cmdSendToAccounts.Caption = "Build Email for ESI"
    m_blnPostingToAccounts = False

End Select

adoJobListing.ConnectionString = g_strConnection
adoJobListing.RecordSource = l_strSQL
adoJobListing.Refresh

 'grdJobListing.Columns(3).Style = ssStyleEditButton
 'grdJobListing.Columns(3).DataType = vbDate
 'grdJobListing.ReBind

End Sub

