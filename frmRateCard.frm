VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmRateCard 
   Caption         =   "Rate Card"
   ClientHeight    =   8130
   ClientLeft      =   3045
   ClientTop       =   4185
   ClientWidth     =   27495
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRateCard.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8130
   ScaleWidth      =   27495
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdHireRates 
      Caption         =   "Hire Rates"
      Height          =   315
      Left            =   16260
      TabIndex        =   30
      Top             =   5400
      Visible         =   0   'False
      Width           =   1275
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdRateCard 
      Align           =   1  'Align Top
      Bindings        =   "frmRateCard.frx":08CA
      Height          =   1695
      Left            =   0
      TabIndex        =   14
      Top             =   615
      Width           =   27495
      _Version        =   196617
      SelectTypeRow   =   3
      BackColorOdd    =   16707070
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   48498
      _ExtentY        =   2990
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.ComboBox cmbRateCard 
      Height          =   315
      Left            =   7380
      TabIndex        =   4
      Top             =   6540
      Visible         =   0   'False
      Width           =   2235
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnUnit 
      Height          =   1755
      Left            =   8880
      TabIndex        =   13
      Top             =   3300
      Width           =   1695
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   3200
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "Description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   2990
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print"
      Height          =   315
      Left            =   14940
      TabIndex        =   12
      Top             =   4200
      Visible         =   0   'False
      Width           =   1515
   End
   Begin VB.ComboBox cmbDirection 
      Height          =   315
      ItemData        =   "frmRateCard.frx":08E4
      Left            =   3780
      List            =   "frmRateCard.frx":08EE
      Style           =   2  'Dropdown List
      TabIndex        =   10
      ToolTipText     =   "Ascending/Descending"
      Top             =   7380
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ComboBox cmbOrderBy 
      Height          =   315
      Left            =   1500
      TabIndex        =   9
      Text            =   "cmbOrderBy"
      ToolTipText     =   "Order data by "
      Top             =   7380
      Visible         =   0   'False
      Width           =   2235
   End
   Begin MSAdodcLib.Adodc adoDefaultRatecard 
      Height          =   330
      Left            =   3840
      Top             =   4380
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoRateCard"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnRateCard 
      Bindings        =   "frmRateCard.frx":08FD
      Height          =   1755
      Left            =   1980
      TabIndex        =   8
      Top             =   3300
      Width           =   6735
      DataFieldList   =   "cetacode"
      _Version        =   196617
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   5
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ratecardID"
      Columns(0).Name =   "ratecardID"
      Columns(0).Alignment=   1
      Columns(0).CaptionAlignment=   1
      Columns(0).DataField=   "ratecardID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   2831
      Columns(1).Caption=   "Code"
      Columns(1).Name =   "cetacode"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "cetacode"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   2011
      Columns(2).Caption=   "Account Code"
      Columns(2).Name =   "nominalcode"
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "nominalcode"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   6429
      Columns(3).Caption=   "Description"
      Columns(3).Name =   "description"
      Columns(3).CaptionAlignment=   0
      Columns(3).DataField=   "description"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "companyID"
      Columns(4).Name =   "companyID"
      Columns(4).Alignment=   1
      Columns(4).CaptionAlignment=   1
      Columns(4).DataField=   "companyID"
      Columns(4).DataType=   3
      Columns(4).FieldLen=   256
      _ExtentX        =   11880
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "cetacode"
   End
   Begin VB.PictureBox picFooterRight 
      Align           =   1  'Align Top
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   27495
      TabIndex        =   7
      Top             =   2310
      Width           =   27495
      Begin VB.OptionButton optPrinting 
         Caption         =   "DADC Codes"
         Height          =   195
         Index           =   6
         Left            =   18960
         TabIndex        =   32
         Top             =   120
         Width           =   1335
      End
      Begin VB.CommandButton cmdRefresh 
         Caption         =   "Refresh"
         Height          =   315
         Left            =   6180
         TabIndex        =   31
         Top             =   60
         Width           =   1155
      End
      Begin VB.OptionButton optPrinting 
         Caption         =   "No Clip Codes"
         Height          =   195
         Index           =   5
         Left            =   17520
         TabIndex        =   29
         Top             =   120
         Width           =   1335
      End
      Begin VB.OptionButton optPrinting 
         Caption         =   "Just Finishing Post Codes"
         Height          =   195
         Index           =   4
         Left            =   15240
         TabIndex        =   28
         Top             =   120
         Width           =   2235
      End
      Begin VB.OptionButton optPrinting 
         Caption         =   "Just Skibbly Codes"
         Height          =   195
         Index           =   3
         Left            =   13500
         TabIndex        =   27
         Top             =   120
         Width           =   1935
      End
      Begin VB.CommandButton cmdAll 
         Caption         =   "All Rates"
         Height          =   315
         Left            =   4980
         TabIndex        =   25
         ToolTipText     =   "Return to the Default Rates"
         Top             =   60
         Width           =   1155
      End
      Begin VB.OptionButton optPrinting 
         Caption         =   "Just BBC WW Codes"
         Height          =   195
         Index           =   2
         Left            =   11580
         TabIndex        =   24
         Top             =   120
         Width           =   1935
      End
      Begin VB.OptionButton optPrinting 
         Caption         =   "No Company Codes"
         Height          =   195
         Index           =   1
         Left            =   9720
         TabIndex        =   23
         Top             =   120
         Width           =   1875
      End
      Begin VB.OptionButton optPrinting 
         Caption         =   "All Codes"
         Height          =   195
         Index           =   0
         Left            =   8700
         TabIndex        =   22
         Top             =   120
         Width           =   1035
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   7380
         TabIndex        =   6
         ToolTipText     =   "Close this form"
         Top             =   60
         Width           =   1155
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Default"
         Height          =   315
         Left            =   3780
         TabIndex        =   5
         ToolTipText     =   "Return to the Default Rates"
         Top             =   60
         Width           =   1155
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
         Bindings        =   "frmRateCard.frx":091E
         Height          =   315
         Left            =   840
         TabIndex        =   20
         ToolTipText     =   "Company for Specific Rates"
         Top             =   60
         Width           =   2895
         DataFieldList   =   "name"
         _Version        =   196617
         ColumnHeaders   =   0   'False
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   5609
         Columns(0).Caption=   "Company Name"
         Columns(0).Name =   "name"
         Columns(0).DataField=   "name"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "companyID"
         Columns(1).Name =   "companyID"
         Columns(1).DataField=   "companyID"
         Columns(1).FieldLen=   256
         _ExtentX        =   5106
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "name"
      End
      Begin VB.Label lblCompanyID 
         BackColor       =   &H0000FFFF&
         Height          =   195
         Left            =   20580
         TabIndex        =   26
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Company"
         Height          =   255
         Index           =   2
         Left            =   60
         TabIndex        =   21
         Top             =   120
         Width           =   1035
      End
   End
   Begin MSAdodcLib.Adodc adoCompany 
      Height          =   390
      Left            =   5400
      Top             =   7560
      Visible         =   0   'False
      Width           =   2340
      _ExtentX        =   4128
      _ExtentY        =   688
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCompany"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.PictureBox picHeader 
      Align           =   1  'Align Top
      Height          =   615
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   27435
      TabIndex        =   15
      Top             =   0
      Width           =   27495
      Begin VB.TextBox txtFilter 
         DataField       =   "nominalcode"
         Height          =   345
         Index           =   5
         Left            =   11580
         TabIndex        =   34
         Top             =   120
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox txtFilter 
         DataField       =   "MPServiceProfileNo"
         Height          =   345
         Index           =   4
         Left            =   9060
         TabIndex        =   33
         Top             =   120
         Width           =   1515
      End
      Begin VB.TextBox txtFilter 
         DataField       =   "description"
         Height          =   345
         Index           =   3
         Left            =   13620
         TabIndex        =   3
         Top             =   120
         Width           =   3615
      End
      Begin VB.TextBox txtFilter 
         DataField       =   "nominalcode"
         Height          =   345
         Index           =   2
         Left            =   6120
         TabIndex        =   2
         Top             =   120
         Width           =   1515
      End
      Begin VB.TextBox txtFilter 
         DataField       =   "cetacode"
         Height          =   345
         Index           =   1
         Left            =   3180
         TabIndex        =   1
         Top             =   120
         Width           =   1515
      End
      Begin VB.TextBox txtFilter 
         DataField       =   "category"
         Height          =   345
         Index           =   0
         Left            =   960
         TabIndex        =   0
         Top             =   120
         Width           =   1515
      End
      Begin MSAdodcLib.Adodc adoRateCard 
         Height          =   330
         Left            =   17580
         Top             =   120
         Width           =   2655
         _ExtentX        =   4683
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoRateCard"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.Label Label1 
         Caption         =   "MP Srv #"
         Height          =   255
         Index           =   4
         Left            =   8280
         TabIndex        =   35
         Top             =   180
         Width           =   1275
      End
      Begin VB.Label Label1 
         Caption         =   "Description"
         Height          =   255
         Index           =   3
         Left            =   12660
         TabIndex        =   19
         Top             =   180
         Width           =   1275
      End
      Begin VB.Label Label1 
         Caption         =   "Nominal"
         Height          =   255
         Index           =   2
         Left            =   5220
         TabIndex        =   18
         Top             =   180
         Width           =   1275
      End
      Begin VB.Label Label1 
         Caption         =   "Code"
         Height          =   255
         Index           =   1
         Left            =   2640
         TabIndex        =   17
         Top             =   180
         Width           =   1275
      End
      Begin VB.Label Label1 
         Caption         =   "Category"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   16
         Top             =   180
         Width           =   1275
      End
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order By"
      Height          =   255
      Index           =   20
      Left            =   360
      TabIndex        =   11
      Top             =   7380
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "frmRateCard"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmbCompany_Click()

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

grdRateCard.Columns("companyID").Visible = False
ShowRateCard g_strRateCardType, Val(lblCompanyID.Caption)

End Sub

Private Sub cmbRateCard_Click()

ShowRateCard g_strRateCardType, 0

End Sub

Private Sub cmdAll_Click()

lblCompanyID.Caption = ""
cmbCompany.Text = "ALL RATES"
cmbRateCard.Text = "Basic Rates"
grdRateCard.Columns("companyID").Visible = True
cmdRefresh_Click

End Sub

Private Sub cmdClear_Click()
lblCompanyID.Caption = "0"
cmbCompany.Text = ""
grdRateCard.Columns("companyID").Visible = False
cmbRateCard.Text = "Basic Rates"

cmdRefresh_Click

End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub


Private Sub cmdHireRates_Click()
ShowRateCard g_strRateCardType, Val(lblCompanyID.Caption)
End Sub

Private Sub cmdPrint_Click()
If Not CheckAccess("/printratecard") Then Exit Sub

Dim l_strReportFileName As String, l_strSelection As String
l_strReportFileName = "ratecard.rpt"

l_strSelection = "{ratecard.cetacode} <> ''"

If txtFilter(0).Text <> "" Then l_strSelection = l_strSelection & " AND uppercase({ratecard.category}) like '" & UCase(txtFilter(0).Text) & "*'"
If txtFilter(1).Text <> "" Then l_strSelection = l_strSelection & " AND uppercase({ratecard.cetacode}) LIKE '" & UCase(txtFilter(1).Text) & "*'"
If txtFilter(2).Text <> "" Then l_strSelection = l_strSelection & " AND uppercase({ratecard.nominalcode}) LIKE '" & UCase(txtFilter(2).Text) & "*'"
If txtFilter(3).Text <> "" Then l_strSelection = l_strSelection & " AND uppercase({ratecard.description}) LIKE '" & UCase(txtFilter(3).Text) & "*'"

If lblCompanyID.Caption <> "" Then l_strSelection = l_strSelection & " AND {ratecard.companyID} = " & lblCompanyID.Caption

If optPrinting(1).Value = True Then
    l_strSelection = l_strSelection & " AND {ratecard.cetacode} <> '' AND not ({ratecard.cetacode} like 'WW*' OR {ratecard.cetacode} like 'CS*' OR {ratecard.cetacode} like 'FP*')"
ElseIf optPrinting(2).Value = True Then
    l_strSelection = l_strSelection & " AND {ratecard.cetacode} <> '' AND {ratecard.cetacode} like 'WW*'"
ElseIf optPrinting(3).Value = True Then
    l_strSelection = l_strSelection & " AND {ratecard.cetacode} <> '' AND {ratecard.cetacode} like 'CS*'"
ElseIf optPrinting(4).Value = True Then
    l_strSelection = l_strSelection & " AND {ratecard.cetacode} <> '' AND {ratecard.cetacode} like 'FP*'"
End If

PrintCrystalReport g_strLocationOfCrystalReportFiles & l_strReportFileName, l_strSelection, g_blnPreviewReport

End Sub

Private Sub cmdRefresh_Click()

ShowRateCard g_strRateCardType, lblCompanyID.Caption

End Sub

Private Sub ddnRateCard_Click()

grdRateCard.Columns("nominalcode").Text = ddnRateCard.Columns("nominalCode").Text
grdRateCard.Columns("description").Text = ddnRateCard.Columns("description").Text

End Sub

Private Sub Form_Load()

Dim l_strSQL As String
l_strSQL = "SELECT name, companyID FROM company WHERE system_active = 1 ORDER BY name"

adoCompany.ConnectionString = g_strConnection
adoCompany.RecordSource = l_strSQL
adoCompany.Refresh

cmbOrderBy.Text = "fd_orderby,nominalcode,cetacode"
cmbDirection.ListIndex = 0


PopulateCombo "ratecard", cmbRateCard

cmbRateCard.AddItem "Basic Rates"
cmbRateCard.ListIndex = GetListIndex(cmbRateCard, "Basic Rates")

'cmdRefresh.Value = True

If CheckAccess("/editratecard", True) Then
    grdRateCard.AllowAddNew = True
    grdRateCard.AllowDelete = True
    grdRateCard.AllowUpdate = True
Else
    grdRateCard.AllowAddNew = False
    grdRateCard.AllowDelete = False
    grdRateCard.AllowUpdate = False
End If

PopulateCombo "unit", ddnUnit

CenterForm Me

End Sub

Private Sub Form_Resize()

On Error Resume Next

'picFooterRight.Left = Me.ScaleWidth - picFooterRight.Width - 120
picFooterRight.Top = Me.ScaleHeight - picFooterRight.Height - grdRateCard.Top - 120
'cmbRateCard.Top = Me.ScaleHeight - picFooterLeft.Height - grdRateCard.Top - 120

grdRateCard.Height = Me.ScaleHeight - picFooterRight.Height - grdRateCard.Top - 240 - 120
grdRateCard.Width = Me.ScaleWidth - 120 - 120


End Sub

Private Sub grdRateCard_BeforeUpdate(Cancel As Integer)

If lblCompanyID.Caption <> "" Then grdRateCard.Columns("companyID").Text = lblCompanyID.Caption

Dim l_strOriginal As String, l_strNew As String, l_intLoop As Integer
Dim l_lngRateCardID As Long
 
l_lngRateCardID = Val(grdRateCard.Columns("ratecardID").Text)

If l_lngRateCardID = 0 Then Exit Sub

'collect the data from the record in the DB before it is updated
Dim l_rstOriginal As New ADODB.Recordset
Set l_rstOriginal = ExecuteSQL("SELECT * FROM ratecard WHERE ratecardID = '" & l_lngRateCardID & "';", g_strExecuteError)

If Not l_rstOriginal.EOF Then
    'pick up all the values in the grid, and the original database values
    For l_intLoop = 0 To grdRateCard.Columns.Count - 1
        If grdRateCard.Columns(l_intLoop).DataField <> "discount" Then
            l_strOriginal = l_strOriginal & grdRateCard.Columns(l_intLoop).DataField & " = " & l_rstOriginal(grdRateCard.Columns(l_intLoop).DataField) & ", "
            l_strNew = l_strNew & grdRateCard.Columns(l_intLoop).DataField & " = " & grdRateCard.Columns(l_intLoop).Text & ", "
        End If
    Next
    
    'chop the extra comma etc off the end
    l_strOriginal = Left(l_strOriginal, Len(l_strOriginal) - 3)
    l_strNew = Left(l_strNew, Len(l_strNew) - 3)
    
    'add history items for it
    AddHistoryLog "ratecard", l_lngRateCardID, l_strOriginal
    AddHistoryLog "ratecard", l_lngRateCardID, l_strNew
    
    'update the modified details
    Dim l_strSQL As String
    l_strSQL = "UPDATE ratecard SET mdate = '" & FormatSQLDate(Now) & "', muser = '" & g_strUserInitials & "', muserID = '" & g_lngUserID & "' WHERE ratecardID = '" & l_lngRateCardID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
Else
    MsgBox "There was a problem locating the record for this rate card item. Please refresh and try again.", vbExclamation
End If

l_rstOriginal.Close
Set l_rstOriginal = Nothing

End Sub

Private Sub grdRateCard_BtnClick()

Select Case grdRateCard.Columns(grdRateCard.Col).DataField


Case "discount"
    
    'add or change a discount for this company
    If Val(lblCompanyID.Caption) = 0 Then
        MsgBox "You can not apply discounts without first selecting a company.", vbInformation
        Exit Sub
    End If
    
    Dim l_sngDiscount As Single
    
    l_sngDiscount = Val(grdRateCard.Columns("discount").Text)
    l_sngDiscount = Val(InputBox("Please enter the new discount percentage for " & grdRateCard.Columns("cetacode").Text & ".", "Set company discount", l_sngDiscount))
    
    If l_sngDiscount = 0 Then
        Dim l_intResult
        l_intResult = MsgBox("Set discount to 0 (zero)?", 4 + 32)
        If l_intResult = vbNo Then Exit Sub
    End If
    
    SetDiscount lblCompanyID.Caption, grdRateCard.Columns("cetacode").Text, l_sngDiscount
    
    grdRateCard.Columns("discount").Text = l_sngDiscount

Case "noratecardprice"
    If Val(grdRateCard.Columns("noratecardprice").Text) = 0 Then
        grdRateCard.Columns("noratecardprice").Text = 1
    Else
        grdRateCard.Columns("noratecardprice").Text = 0
    End If
    
    grdRateCard.Update


Case "cetacode"
    Dim l_strNewCode As String
    l_strNewCode = InputBox("Please enter the new code that you want to change " & grdRateCard.Columns("cetacode").Text & " to.", "Change Rate Card Code", grdRateCard.Columns("cetacode").Text)
    
    If l_strNewCode = grdRateCard.Columns("cetacode").Text Then Exit Sub
    If l_strNewCode = "" Then Exit Sub
    
    Me.MousePointer = vbHourglass
    Me.Caption = "Rate Card... Please wait... Updating..."
    DoEvents
    
    Dim l_strSQL As String
    l_strSQL = "UPDATE costing SET costcode = '" & QuoteSanitise(l_strNewCode) & "' WHERE costcode = '" & QuoteSanitise(grdRateCard.Columns("cetacode").Text) & "';"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError

    grdRateCard.Columns("cetacode").Text = l_strNewCode
    grdRateCard.Update
    
    Me.Caption = "Rate Card"
    
    DoEvents
    
    Me.MousePointer = vbDefault

End Select
End Sub

Private Sub grdRateCard_HeadClick(ByVal ColIndex As Integer)

If cmbOrderBy.Text <> grdRateCard.Columns(ColIndex).DataField Then
    cmbOrderBy.Text = grdRateCard.Columns(ColIndex).DataField
Else
    cmbDirection.ListIndex = 1 - cmbDirection.ListIndex
End If

cmdRefresh_Click

End Sub

Private Sub grdRateCard_InitColumnProps()

'Exit Sub

If Val(lblCompanyID.Caption) <> 0 Then
    grdRateCard.Columns("cetacode").DropDownHwnd = ddnRateCard.hWnd
Else
    grdRateCard.Columns("cetacode").DropDownHwnd = 0
End If


'grdRateCard.Columns("companyID").Visible = True
grdRateCard.Columns("ratecardID").Visible = False

grdRateCard.Columns("MPServiceProfileNo").Width = 800
grdRateCard.Columns("MPServiceProfileNo").Caption = "MP Srv #"

grdRateCard.Columns("category").Width = 1200
grdRateCard.Columns("category").Caption = "Category"

grdRateCard.Columns("VDMS_Charge_Code").Width = 800
grdRateCard.Columns("VDMS_Charge_Code").Caption = "VDMS Code"

grdRateCard.Columns("cetacode").Width = 2300
grdRateCard.Columns("cetacode").Caption = "Code"
grdRateCard.Columns("cetacode").Style = ssStyleEditButton


grdRateCard.Columns("fd_orderby").Width = 400
grdRateCard.Columns("fd_orderby").Caption = "Order"

grdRateCard.Columns("nominalcode").Width = 800
grdRateCard.Columns("nominalcode").Caption = "Nominal"

grdRateCard.Columns("includedstocknominalcode").Width = 1200
grdRateCard.Columns("includedstocknominalcode").Caption = "Inc Stock Nom"
grdRateCard.Columns("includedstockprice").Width = 1200
grdRateCard.Columns("includedstockprice").Caption = "Inc Stock Pr."

grdRateCard.Columns("description").Width = 4000
grdRateCard.Columns("description").Caption = "Description"

grdRateCard.Columns("shinecode").Width = 600
grdRateCard.Columns("shinecode").Caption = "Shine"

grdRateCard.Columns("DisneyNumber").Width = 600
grdRateCard.Columns("DisneyNumber").Caption = "Disney"

grdRateCard.Columns("MinimumCharge").Caption = "Min Ch."

grdRateCard.Columns("unit").DropDownHwnd = ddnUnit.hWnd
grdRateCard.Columns("unit").Width = 600
grdRateCard.Columns("unit").Caption = "Unit"

'---
grdRateCard.Columns("noratecardprice").Width = 700
grdRateCard.Columns("noratecardprice").Style = ssStyleEditButton
grdRateCard.Columns("noratecardprice").Caption = "Virtual"
grdRateCard.Columns("noratecardprice").Visible = False

grdRateCard.Columns("discount").Width = 800
grdRateCard.Columns("discount").Caption = "Discount"
grdRateCard.Columns("discount").Style = ssStyleEditButton

'Dim l_intLoop As Integer
'For l_intLoop = 7 To grdRateCard.Columns.Count - 1
'    grdRateCard.Columns(l_intLoop).NumberFormat = g_strCurrency & "###,###,##0.00"
'    grdRateCard.Columns(l_intLoop).Width = 1100

'Next

End Sub

Private Sub grdRateCard_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

On Error GoTo SKIP
If grdRateCard.Columns(LastCol).Name = "nominalcode" Then

    If grdRateCard.Columns("VDMS_Charge_Code").Text <> GetData("Profitcentre5", "VDMS_Charge_Code", "Profitcentre5ID", grdRateCard.Columns("nominalcode").Text) Then
        grdRateCard.Columns("VDMS_CHarge_Code").Text = GetData("Profitcentre5", "VDMS_Charge_Code", "Profitcentre5ID", grdRateCard.Columns("nominalcode").Text)
    End If

End If
SKIP:
End Sub

Private Sub grdRateCard_RowLoaded(ByVal Bookmark As Variant)

If Val(lblCompanyID.Caption) <> 0 Then
    Dim l_sngDiscount As Single
    l_sngDiscount = GetCompanyDiscount(CLng(lblCompanyID.Caption), CStr(grdRateCard.Columns("cetacode").Text))
    If l_sngDiscount <> 0 Then
        grdRateCard.Columns("discount").Text = l_sngDiscount
    Else
        grdRateCard.Columns("discount").Text = 0
    End If
End If
    
End Sub

Private Sub optPrinting_Click(Index As Integer)

ShowRateCard g_strRateCardType, Val(lblCompanyID.Caption)

End Sub

Private Sub txtFilter_GotFocus(Index As Integer)
txtFilter(Index).SelStart = 0
txtFilter(Index).SelLength = Len(txtFilter(Index).Text)
End Sub
