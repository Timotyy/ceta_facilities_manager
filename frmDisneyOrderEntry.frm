VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmDisneyOrderEntry 
   Caption         =   $"frmDisneyOrderEntry.frx":0000
   ClientHeight    =   8775
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8130
   LinkTopic       =   "Form1"
   ScaleHeight     =   8775
   ScaleWidth      =   8130
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraGeneric 
      BorderStyle     =   0  'None
      Height          =   1395
      Left            =   15120
      TabIndex        =   66
      Top             =   1740
      Width           =   7935
      Begin VB.TextBox txtGenericVersion 
         Height          =   315
         Left            =   1440
         TabIndex        =   73
         ToolTipText     =   "The title for the job"
         Top             =   1080
         Width           =   2415
      End
      Begin VB.TextBox txtGenericElements 
         Height          =   315
         Left            =   1440
         TabIndex        =   71
         ToolTipText     =   "The title for the job"
         Top             =   720
         Width           =   2415
      End
      Begin VB.TextBox txtGenericSeries 
         Height          =   315
         Left            =   1440
         TabIndex        =   69
         ToolTipText     =   "The title for the job"
         Top             =   360
         Width           =   2415
      End
      Begin VB.ComboBox cmbGenericLanguage 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   1440
         TabIndex        =   67
         ToolTipText     =   "The Clip Codec"
         Top             =   0
         Width           =   2415
      End
      Begin VB.Label lblCaption 
         Caption         =   "Version"
         Height          =   315
         Index           =   23
         Left            =   0
         TabIndex        =   74
         Top             =   1140
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Elements"
         Height          =   315
         Index           =   22
         Left            =   0
         TabIndex        =   72
         Top             =   780
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Series"
         Height          =   315
         Index           =   21
         Left            =   0
         TabIndex        =   70
         Top             =   420
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Language"
         Height          =   315
         Index           =   20
         Left            =   0
         TabIndex        =   68
         Top             =   60
         Width           =   1095
      End
   End
   Begin VB.Frame fraWorkflow 
      BorderStyle     =   0  'None
      Height          =   2175
      Left            =   180
      TabIndex        =   53
      Top             =   4800
      Width           =   7815
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbInclusive 
         Height          =   315
         Index           =   0
         Left            =   1440
         TabIndex        =   54
         ToolTipText     =   "The contact for this job"
         Top             =   0
         Width           =   6375
         DataFieldList   =   "Description"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BorderStyle     =   0
         ForeColorEven   =   -2147483640
         ForeColorOdd    =   -2147483640
         BackColorEven   =   -2147483643
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   7435
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Description"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Format"
         Columns(1).Name =   "Format"
         Columns(1).DataField=   "format"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   979
         Columns(2).Caption=   "Type"
         Columns(2).Name =   "copytype"
         Columns(2).DataField=   "copytype"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   11245
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16761087
         DataFieldToDisplay=   "Description"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbInclusive 
         Height          =   315
         Index           =   1
         Left            =   1440
         TabIndex        =   55
         ToolTipText     =   "The contact for this job"
         Top             =   360
         Width           =   6375
         DataFieldList   =   "Description"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BorderStyle     =   0
         ForeColorEven   =   -2147483640
         ForeColorOdd    =   -2147483640
         BackColorEven   =   -2147483643
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   7435
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Description"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Format"
         Columns(1).Name =   "Format"
         Columns(1).DataField=   "format"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   979
         Columns(2).Caption=   "Type"
         Columns(2).Name =   "copytype"
         Columns(2).DataField=   "copytype"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   11245
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16761087
         DataFieldToDisplay=   "Description"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbInclusive 
         Height          =   315
         Index           =   2
         Left            =   1440
         TabIndex        =   56
         ToolTipText     =   "The contact for this job"
         Top             =   720
         Width           =   6375
         DataFieldList   =   "Description"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BorderStyle     =   0
         ForeColorEven   =   -2147483640
         ForeColorOdd    =   -2147483640
         BackColorEven   =   -2147483643
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   7435
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Description"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Format"
         Columns(1).Name =   "Format"
         Columns(1).DataField=   "format"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   979
         Columns(2).Caption=   "Type"
         Columns(2).Name =   "copytype"
         Columns(2).DataField=   "copytype"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   11245
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16761087
         DataFieldToDisplay=   "Description"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbInclusive 
         Height          =   315
         Index           =   3
         Left            =   1440
         TabIndex        =   57
         ToolTipText     =   "The contact for this job"
         Top             =   1080
         Width           =   6375
         DataFieldList   =   "Description"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BorderStyle     =   0
         ForeColorEven   =   -2147483640
         ForeColorOdd    =   -2147483640
         BackColorEven   =   -2147483643
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   7435
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Description"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Format"
         Columns(1).Name =   "Format"
         Columns(1).DataField=   "format"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   979
         Columns(2).Caption=   "Type"
         Columns(2).Name =   "copytype"
         Columns(2).DataField=   "copytype"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   11245
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16761087
         DataFieldToDisplay=   "Description"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbInclusive 
         Height          =   315
         Index           =   4
         Left            =   1440
         TabIndex        =   58
         ToolTipText     =   "The contact for this job"
         Top             =   1440
         Width           =   6375
         DataFieldList   =   "Description"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BorderStyle     =   0
         ForeColorEven   =   -2147483640
         ForeColorOdd    =   -2147483640
         BackColorEven   =   -2147483643
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   7435
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Description"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Format"
         Columns(1).Name =   "Format"
         Columns(1).DataField=   "format"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   979
         Columns(2).Caption=   "Type"
         Columns(2).Name =   "copytype"
         Columns(2).DataField=   "copytype"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   11245
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16761087
         DataFieldToDisplay=   "Description"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbInclusive 
         Height          =   315
         Index           =   5
         Left            =   1440
         TabIndex        =   59
         ToolTipText     =   "The contact for this job"
         Top             =   1800
         Width           =   6375
         DataFieldList   =   "Description"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BorderStyle     =   0
         ForeColorEven   =   -2147483640
         ForeColorOdd    =   -2147483640
         BackColorEven   =   -2147483643
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   7435
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Description"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Format"
         Columns(1).Name =   "Format"
         Columns(1).DataField=   "format"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   979
         Columns(2).Caption=   "Type"
         Columns(2).Name =   "copytype"
         Columns(2).DataField=   "copytype"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   11245
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16761087
         DataFieldToDisplay=   "Description"
      End
      Begin VB.Label lblCaption 
         Caption         =   "Workflow Stage 1"
         Height          =   195
         Index           =   9
         Left            =   0
         TabIndex        =   65
         Top             =   60
         Width           =   1455
      End
      Begin VB.Label lblCaption 
         Caption         =   "Workflow Stage 2"
         Height          =   195
         Index           =   10
         Left            =   0
         TabIndex        =   64
         Top             =   420
         Width           =   1455
      End
      Begin VB.Label lblCaption 
         Caption         =   "Workflow Stage 3"
         Height          =   195
         Index           =   11
         Left            =   0
         TabIndex        =   63
         Top             =   780
         Width           =   1455
      End
      Begin VB.Label lblCaption 
         Caption         =   "Workflow Stage 4"
         Height          =   195
         Index           =   12
         Left            =   0
         TabIndex        =   62
         Top             =   1140
         Width           =   1455
      End
      Begin VB.Label lblCaption 
         Caption         =   "Workflow Stage 5"
         Height          =   195
         Index           =   13
         Left            =   0
         TabIndex        =   61
         Top             =   1500
         Width           =   1455
      End
      Begin VB.Label lblCaption 
         Caption         =   "Workflow Stage 6"
         Height          =   195
         Index           =   14
         Left            =   0
         TabIndex        =   60
         Top             =   1860
         Width           =   1455
      End
   End
   Begin VB.TextBox txtDuration 
      Height          =   315
      Left            =   1620
      TabIndex        =   48
      ToolTipText     =   "The title for the job"
      Top             =   4080
      Width           =   2415
   End
   Begin VB.Frame fraPlatform 
      BorderStyle     =   0  'None
      Caption         =   "Title"
      Height          =   3795
      Left            =   8820
      TabIndex        =   40
      Top             =   1680
      Width           =   3975
      Begin VB.ComboBox cmbFrameRate 
         Height          =   315
         Left            =   1500
         TabIndex        =   79
         ToolTipText     =   "The Clip Codec"
         Top             =   3420
         Width           =   1575
      End
      Begin VB.OptionButton optPlatformOrderType 
         Alignment       =   1  'Right Justify
         Caption         =   "Disney+"
         Height          =   255
         Index           =   8
         Left            =   90
         TabIndex        =   78
         Top             =   2820
         Width           =   1635
      End
      Begin VB.OptionButton optPlatformOrderType 
         Alignment       =   1  'Right Justify
         Caption         =   "Netflix"
         Height          =   255
         Index           =   7
         Left            =   90
         TabIndex        =   77
         Top             =   2520
         Width           =   1635
      End
      Begin VB.OptionButton optPlatformOrderType 
         Alignment       =   1  'Right Justify
         Caption         =   "Sony PSN"
         Height          =   255
         Index           =   6
         Left            =   90
         TabIndex        =   76
         Top             =   2220
         Width           =   1635
      End
      Begin VB.OptionButton optPlatformOrderType 
         Alignment       =   1  'Right Justify
         Caption         =   "XBox"
         Height          =   255
         Index           =   5
         Left            =   90
         TabIndex        =   75
         Top             =   1920
         Width           =   1635
      End
      Begin VB.ComboBox cmbLanguage 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   1500
         TabIndex        =   50
         ToolTipText     =   "The Clip Codec"
         Top             =   0
         Width           =   2415
      End
      Begin VB.CheckBox chkPlatformHD 
         Alignment       =   1  'Right Justify
         Caption         =   "HD"
         Height          =   315
         Left            =   90
         TabIndex        =   47
         Top             =   3120
         Value           =   1  'Checked
         Width           =   1635
      End
      Begin VB.OptionButton optPlatformOrderType 
         Alignment       =   1  'Right Justify
         Caption         =   "Amazon MLF"
         Height          =   255
         Index           =   4
         Left            =   90
         TabIndex        =   46
         Top             =   1620
         Width           =   1635
      End
      Begin VB.OptionButton optPlatformOrderType 
         Alignment       =   1  'Right Justify
         Caption         =   "Amazon"
         Height          =   255
         Index           =   3
         Left            =   90
         TabIndex        =   45
         Top             =   1320
         Width           =   1635
      End
      Begin VB.OptionButton optPlatformOrderType 
         Alignment       =   1  'Right Justify
         Caption         =   "Google OMU"
         Height          =   255
         Index           =   2
         Left            =   90
         TabIndex        =   44
         Top             =   1020
         Width           =   1635
      End
      Begin VB.OptionButton optPlatformOrderType 
         Alignment       =   1  'Right Justify
         Caption         =   "Google"
         Height          =   255
         Index           =   1
         Left            =   90
         TabIndex        =   43
         Top             =   720
         Width           =   1635
      End
      Begin VB.OptionButton optPlatformOrderType 
         Alignment       =   1  'Right Justify
         Caption         =   "iTunes TV"
         Height          =   255
         Index           =   0
         Left            =   90
         TabIndex        =   42
         Top             =   420
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Frame Rate"
         Height          =   255
         Index           =   39
         Left            =   120
         TabIndex        =   80
         Top             =   3480
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Language"
         Height          =   315
         Index           =   19
         Left            =   90
         TabIndex        =   51
         Top             =   60
         Width           =   1095
      End
   End
   Begin VB.Frame fraOrderType 
      Caption         =   "Order Type"
      Height          =   3075
      Left            =   13140
      TabIndex        =   21
      Top             =   1260
      Width           =   1815
      Begin VB.OptionButton optOrderType 
         Caption         =   "Generic Tracker"
         Height          =   315
         Index           =   4
         Left            =   180
         TabIndex        =   52
         Top             =   1740
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.OptionButton optOrderType 
         Caption         =   "Platform Tracker"
         Height          =   315
         Index           =   3
         Left            =   180
         TabIndex        =   39
         Top             =   1380
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.OptionButton optOrderType 
         Caption         =   "DADC Playout"
         Height          =   315
         Index           =   2
         Left            =   180
         TabIndex        =   30
         Top             =   1020
         Width           =   1335
      End
      Begin VB.OptionButton optOrderType 
         Caption         =   "Other"
         Height          =   315
         Index           =   1
         Left            =   180
         TabIndex        =   23
         Top             =   660
         Width           =   855
      End
      Begin VB.OptionButton optOrderType 
         Caption         =   "Disney"
         Height          =   315
         Index           =   0
         Left            =   180
         TabIndex        =   22
         Top             =   300
         Width           =   855
      End
   End
   Begin VB.Frame fraDADC 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   8700
      TabIndex        =   34
      Top             =   660
      Width           =   7875
      Begin VB.TextBox txtDeliveryDetails 
         Height          =   315
         Left            =   5400
         TabIndex        =   37
         ToolTipText     =   "The title for the job"
         Top             =   0
         Width           =   2415
      End
      Begin VB.TextBox txtBBCCustomer 
         Height          =   315
         Left            =   1440
         TabIndex        =   35
         ToolTipText     =   "The title for the job"
         Top             =   0
         Width           =   2415
      End
      Begin VB.Label lblCaption 
         Caption         =   "Delivery Details"
         Height          =   195
         Index           =   17
         Left            =   3960
         TabIndex        =   38
         Top             =   60
         Width           =   1275
      End
      Begin VB.Label lblCaption 
         Caption         =   "BBC Customer"
         Height          =   195
         Index           =   18
         Left            =   0
         TabIndex        =   36
         Top             =   60
         Width           =   1275
      End
   End
   Begin VB.Frame fraNormal 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   8760
      TabIndex        =   31
      Top             =   1200
      Width           =   4095
      Begin VB.TextBox txtElementNumber 
         Height          =   315
         Left            =   1440
         TabIndex        =   32
         ToolTipText     =   "The title for the job"
         Top             =   0
         Width           =   1455
      End
      Begin VB.Label lblCaption 
         Caption         =   "Element Number"
         Height          =   195
         Index           =   15
         Left            =   0
         TabIndex        =   33
         Top             =   60
         Width           =   1275
      End
   End
   Begin VB.TextBox txtClientStatus 
      Alignment       =   2  'Center
      Height          =   315
      Left            =   5040
      Locked          =   -1  'True
      TabIndex        =   27
      ToolTipText     =   "Current client's status (On hold, cash, ok etc)"
      Top             =   1200
      Width           =   915
   End
   Begin VB.ComboBox cmbDeadlineTime 
      Height          =   315
      Left            =   3240
      TabIndex        =   20
      Text            =   "12:00"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   3360
      Width           =   795
   End
   Begin VB.CheckBox chkMultiple 
      Alignment       =   1  'Right Justify
      Caption         =   "Multiple Item Job"
      Height          =   195
      Left            =   180
      TabIndex        =   19
      Top             =   8340
      Width           =   1635
   End
   Begin VB.TextBox txtEpisodeEnd 
      Height          =   315
      Left            =   3000
      TabIndex        =   10
      ToolTipText     =   "The title for the job"
      Top             =   3720
      Width           =   1035
   End
   Begin VB.TextBox txtEpisodeStart 
      Height          =   315
      Left            =   1620
      TabIndex        =   9
      ToolTipText     =   "The title for the job"
      Top             =   3720
      Width           =   1035
   End
   Begin VB.ComboBox cmbJCAContact 
      Height          =   315
      ItemData        =   "frmDisneyOrderEntry.frx":00F1
      Left            =   1620
      List            =   "frmDisneyOrderEntry.frx":00F3
      TabIndex        =   7
      ToolTipText     =   "The Version for the Tape"
      Top             =   3000
      Width           =   2415
   End
   Begin VB.TextBox txtOrderNumber 
      Height          =   315
      Left            =   1620
      TabIndex        =   5
      ToolTipText     =   "The title for the job"
      Top             =   2640
      Width           =   2415
   End
   Begin VB.TextBox txtRequirement 
      Height          =   315
      Left            =   1620
      TabIndex        =   4
      ToolTipText     =   "The title for the job"
      Top             =   2280
      Width           =   4335
   End
   Begin VB.TextBox txtTitle 
      Height          =   315
      Left            =   1620
      TabIndex        =   3
      ToolTipText     =   "The title for the job"
      Top             =   1920
      Width           =   4335
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save"
      Height          =   315
      Left            =   3420
      TabIndex        =   2
      Top             =   8280
      Width           =   1215
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   4740
      TabIndex        =   1
      Top             =   8280
      Width           =   1215
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear"
      Height          =   315
      Left            =   2100
      TabIndex        =   0
      Top             =   8280
      Width           =   1215
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbContact 
      Height          =   315
      Left            =   1620
      TabIndex        =   6
      ToolTipText     =   "The contact for this job"
      Top             =   1560
      Width           =   2535
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      HeadFont3D      =   3
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5424
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "contactID"
      Columns(1).Name =   "contactID"
      Columns(1).DataField=   "contactID"
      Columns(1).FieldLen=   256
      _ExtentX        =   4471
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin MSComCtl2.DTPicker datDueDate 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   1620
      TabIndex        =   8
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   3360
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   124649473
      CurrentDate     =   40934.5
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   1620
      TabIndex        =   24
      ToolTipText     =   "The company this job is for"
      Top             =   1200
      Width           =   2535
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   8
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "accountcode"
      Columns(2).Name =   "accountcode"
      Columns(2).DataField=   "accountcode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "telephone"
      Columns(3).Name =   "telephone"
      Columns(3).DataField=   "telephone"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "cetaclientcode"
      Columns(4).Name =   "cetaclientcode"
      Columns(4).DataField=   "cetaclientcode"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "accountstatus"
      Columns(5).Name =   "accountstatus"
      Columns(5).DataField=   "accountstatus"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "fax"
      Columns(6).Name =   "fax"
      Columns(6).DataField=   "fax"
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Caption=   "email"
      Columns(7).Name =   "email"
      Columns(7).DataField=   "email"
      Columns(7).FieldLen=   256
      _ExtentX        =   4471
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin VB.Label lblCaption 
      Caption         =   "Dur (per Episode)"
      Height          =   195
      Index           =   7
      Left            =   180
      TabIndex        =   49
      Top             =   4140
      Width           =   1275
   End
   Begin VB.Label Label2 
      Caption         =   $"frmDisneyOrderEntry.frx":00F5
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1095
      Left            =   8820
      TabIndex        =   41
      Top             =   6180
      Width           =   5895
   End
   Begin VB.Label Label1 
      Caption         =   $"frmDisneyOrderEntry.frx":01FC
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1035
      Left            =   180
      TabIndex        =   29
      Top             =   60
      Width           =   5895
   End
   Begin VB.Label lblJobID 
      Height          =   315
      Left            =   6420
      TabIndex        =   28
      Top             =   8280
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.Label lblCompanyID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   4260
      TabIndex        =   26
      Tag             =   "CLEARFIELDS"
      Top             =   1200
      Width           =   675
   End
   Begin VB.Label lblCaption 
      Caption         =   "Customer"
      Height          =   195
      Index           =   16
      Left            =   180
      TabIndex        =   25
      Top             =   1260
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "to"
      Height          =   195
      Index           =   6
      Left            =   2760
      TabIndex        =   18
      Top             =   3780
      Width           =   315
   End
   Begin VB.Label lblCaption 
      Caption         =   "Episodes"
      Height          =   195
      Index           =   5
      Left            =   180
      TabIndex        =   17
      Top             =   3780
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Due Date"
      Height          =   195
      Index           =   4
      Left            =   180
      TabIndex        =   16
      Top             =   3420
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "JCA Contact"
      Height          =   195
      Index           =   3
      Left            =   180
      TabIndex        =   15
      Top             =   3060
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Customer Contact"
      Height          =   195
      Index           =   2
      Left            =   180
      TabIndex        =   14
      Top             =   1620
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order Number"
      Height          =   195
      Index           =   1
      Left            =   180
      TabIndex        =   13
      Top             =   2700
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Requirement"
      Height          =   195
      Index           =   0
      Left            =   180
      TabIndex        =   12
      Top             =   2340
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      Height          =   195
      Index           =   8
      Left            =   180
      TabIndex        =   11
      Top             =   1980
      Width           =   1035
   End
End
Attribute VB_Name = "frmDisneyOrderEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmbCompany_CloseUp()

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text
txtClientStatus.Text = Trim(" " & cmbCompany.Columns("accountstatus").Text)
DoCompanyMessages

End Sub

Private Sub cmbCompany_DropDown()

Dim l_strSQL As String
l_strSQL = "SELECT name, accountcode, telephone, companyID, fax, accountstatus, email FROM company WHERE name LIKE '" & QuoteSanitise(cmbCompany.Text) & "%' AND (iscustomer = 1 OR isprospective = 1) AND system_active = 1 ORDER BY name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmbContact_DropDown()

If lblCompanyID.Caption = "" Then
    NoCompanySelectedMessage
    Exit Sub
End If

Dim l_strSQL As String

l_strSQL = "SELECT company.companyID, contact.contactID, contact.name, contact.telephone, employee.jobtitle, contact.email FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((company.companyID)=" & lblCompanyID.Caption & ")) ORDER BY contact.name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbContact.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmbDeadlineTime_DropDown()
PopulateCombo "times", cmbDeadlineTime
End Sub

Private Sub cmbFrameRate_GotFocus()

PopulateCombo "framerate", cmbFrameRate, "MEDIA"
HighLite cmbFrameRate

End Sub

Private Sub cmbInclusive_DropDown(Index As Integer)

Dim l_strSQL As String, l_strCompanyGroup As String

If optOrderType(0).Value = True Then
    l_strSQL = "SELECT forder, description, format, 'I' AS copytype FROM xref WHERE category = 'inclusive' AND description LIKE 'DIS%' UNION SELECT forder, description, format, 'O' AS copytype FROM xref WHERE category = 'other' AND description LIKE 'DISNEY%' ORDER BY copytype, forder, format;"
ElseIf optOrderType(2).Value = True Then
    l_strCompanyGroup = GetData("company", "source", "companyID", lblCompanyID.Caption)
    If l_strCompanyGroup <> "" Then
        l_strSQL = "SELECT forder, description, format, 'IS' AS copytype FROM xref WHERE category = 'inclusive' AND (descriptionalias = '" & l_strCompanyGroup & "' OR descriptionalias IS NULL) UNION SELECT forder, description, format, 'O' AS copytype FROM xref WHERE category = 'other' AND (descriptionalias = '" & l_strCompanyGroup & "' OR descriptionalias IS NULL) UNION SELECT DISTINCT forder, description, description AS format, 'M' AS copytype FROM xref WHERE category = 'dformat' ORDER BY copytype, forder, format;"
    Else
        l_strSQL = "SELECT forder, description, format, 'IS' AS copytype FROM xref WHERE category = 'inclusive' UNION SELECT forder, description, format, 'O' AS copytype FROM xref WHERE category = 'other' UNION SELECT DISTINCT forder, description, description AS format, 'M' AS copytype FROM xref WHERE category = 'dformat' ORDER BY copytype, forder, format;"
    End If
Else
    If lblCompanyID.Caption <> "" Then
        l_strCompanyGroup = GetData("company", "source", "companyID", lblCompanyID.Caption)
        If l_strCompanyGroup <> "" Then
            l_strSQL = "SELECT forder, description, format, 'I' AS copytype FROM xref WHERE category = 'inclusive' AND (descriptionalias = '" & l_strCompanyGroup & "' OR descriptionalias IS NULL) UNION SELECT forder, description, format, 'O' AS copytype FROM xref WHERE category = 'other' AND (descriptionalias = '" & l_strCompanyGroup & "' OR descriptionalias IS NULL) UNION SELECT DISTINCT forder, description, description AS format, 'M' AS copytype FROM xref WHERE category = 'dformat' ORDER BY copytype, forder, format;"
        Else
            l_strSQL = "SELECT forder, description, format, 'I' AS copytype FROM xref WHERE category = 'inclusive' UNION SELECT forder, description, format, 'O' AS copytype FROM xref WHERE category = 'other' UNION SELECT DISTINCT forder, description, description AS format, 'M' AS copytype FROM xref WHERE category = 'dformat' ORDER BY copytype, forder, format;"
        End If
    Else
        l_strSQL = "SELECT forder, description, format, 'I' AS copytype FROM xref WHERE category = 'inclusive' UNION SELECT forder, description, format, 'O' AS copytype FROM xref WHERE category = 'other' UNION SELECT DISTINCT forder, description, description AS format, 'M' AS copytype FROM xref WHERE category = 'dformat' ORDER BY copytype, forder, format;"
    End If
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbInclusive(Index).DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmbJCAContact_DropDown()

PopulateCombo "Users", cmbJCAContact

End Sub

Private Sub cmdClear_Click()

txtTitle.Text = ""
txtRequirement.Text = ""
txtOrderNumber.Text = ""
cmbContact.Text = ""
cmbJCAContact.Text = ""
datDueDate.Value = Null
cmbDeadlineTime.Text = "12:00"
txtEpisodeStart.Text = ""
txtEpisodeEnd.Text = ""
txtDuration.Text = ""

End Sub

Private Sub cmdClose_Click()

Me.Hide

End Sub

Private Sub cmdSave_Click()

Dim l_lngJobID As Long, l_lngJobDetailID As Long, temp As String
Dim l_intEpStart As Integer, l_intEpEnd As Integer, Count As Integer, Count2 As Integer, l_strElementNumber As String
Dim l_strSQL As String, l_rsJobs As ADODB.Recordset, l_rsDetails As ADODB.Recordset
Dim l_rsDisneyTracker As ADODB.Recordset, l_rsPlayoutTracker As ADODB.Recordset, l_rsPlatformTracker As ADODB.Recordset, l_rsGenericTracker As ADODB.Recordset
Dim l_strTitleField As String, l_strEpisodeField As String, l_strSeriesField As String, l_strElementsField As String
Dim l_strVersionField As String, l_strLanguageField As String, l_strOrderField As String
Dim l_rsTemp As ADODB.Recordset
Dim l_strEpisodePrefix As String

If optPlatformOrderType(8).Value = True And cmbFrameRate.Text = "" Then
    MsgBox "Essential information was not all completed." & vbCrLf & "Frame Rate must be SPecified for DisneyPLus Dub Card Orders", vbCritical, "Cannot Create New Order"
    Exit Sub
End If

If txtTitle.Text <> "" And Not IsNull(datDueDate) And cmbContact.Text <> "" And cmbJCAContact.Text <> "" Then

    If txtRequirement.Text = "" And optOrderType(3).Value = 0 Then
        MsgBox "Essential information was not all completed.", vbCritical, "Cannot Create New Order"
        Exit Sub
    End If
    
    l_lngJobID = 0
    
    If txtOrderNumber.Text <> "" Then
        Set l_rsJobs = ExecuteSQL("SELECT * FROM job WHERE companyID = " & lblCompanyID.Caption & " AND orderreference = '" & txtOrderNumber.Text & "' AND fd_status <> 'Sent To Accounts' AND fd_status <> 'Costed' AND fd_status <> 'Cancelled';", g_strExecuteError)
        CheckForSQLError
        
        If l_rsJobs.RecordCount > 0 Then
            
            If chkMultiple.Value = 0 Then
                If MsgBox("Do you wish to proceed and add these details to existing Job?", vbYesNo + vbDefaultButton2, "Existing job found with this order number") = vbYes Then
                    l_rsJobs.MoveFirst
                    l_lngJobID = l_rsJobs("JobID")
                ElseIf MsgBox("Do you wish to make a new job for these items?", vbYesNo + vbDefaultButton2, "Existing job found with this order number") = vbNo Then
                    l_rsJobs.Close
                    Set l_rsJobs = Nothing
                    Exit Sub
                End If
            Else
                l_rsJobs.MoveFirst
                l_lngJobID = l_rsJobs("JobID")
            End If
        
        End If
    Else
        Set l_rsJobs = ExecuteSQL("SELECT * FROM job WHERE jobID = -1;", g_strExecuteError)
        CheckForSQLError
    End If
    
    If l_lngJobID = 0 Then
        
        l_strSQL = "INSERT INTO job (createddate, createduser, createduserID, fd_status) VALUES ('" & FormatSQLDate(Now) & "', '" & g_strUserInitials & "', " & g_lngUserID & ", 'Submitted');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
            
        l_lngJobID = g_lngLastID
            
        l_strSQL = "UPDATE job SET "
        l_strSQL = l_strSQL & "modifieddate = '" & Format(Now, "YYYY-MM-DD HH:NN:SS") & "', "
        l_strSQL = l_strSQL & "modifieduserID = " & g_lngUserID & ", "
        l_strSQL = l_strSQL & "modifieduser = '" & g_strUserInitials & "', "
        l_strSQL = l_strSQL & "flagemail = 0, "
        l_strSQL = l_strSQL & "jobtype = 'Dubbing', "
        l_strSQL = l_strSQL & "joballocation = 'Regular', "
        l_strSQL = l_strSQL & "deadlinedate = '" & Format(datDueDate.Value, "YYYY-MM-DD") & " " & Format(cmbDeadlineTime, "HH:NN") & "', "
        l_strSQL = l_strSQL & "despatchdate = '" & Format(datDueDate.Value, "YYYY-MM-DD") & " " & Format(cmbDeadlineTime, "HH:NN") & "', "
        l_strSQL = l_strSQL & "projectID = 0, "
        l_strSQL = l_strSQL & "productID = 0, "
        If optOrderType(3).Value = False Then
            l_strSQL = l_strSQL & "companyID = " & lblCompanyID.Caption & ", "
            l_strSQL = l_strSQL & "companyname = '" & cmbCompany.Columns("name").Text & "', "
            l_strSQL = l_strSQL & "contactname = '" & cmbContact.Columns("name").Text & "', "
            l_strSQL = l_strSQL & "contactID = '" & cmbContact.Columns("contactID").Text & "', "
        Else
            If optPlatformOrderType(1).Value = True Or optPlatformOrderType(2).Value = True Then
                l_strSQL = l_strSQL & "companyID = 1489, "
                l_strSQL = l_strSQL & "companyname = 'Google', "
                l_strSQL = l_strSQL & "contactname = 'Melanie Griffiths', "
                l_strSQL = l_strSQL & "contactID = 3679, "
            Else
'                l_strSQL = l_strSQL & "companyID") = 1163
'                l_strSQL = l_strSQL & "companyname") = "The Walt Disney Company Ltd."
'                l_strSQL = l_strSQL & "contactname") = cmbContact.Columns("name").Text
'                l_strSQL = l_strSQL & "contactID") = cmbContact.Columns("contactID").Text
                l_strSQL = l_strSQL & "companyID = " & lblCompanyID.Caption & ", "
                l_strSQL = l_strSQL & "companyname = '" & QuoteSanitise(cmbCompany.Columns("name").Text) & "', "
                l_strSQL = l_strSQL & "contactname = '" & QuoteSanitise(cmbContact.Columns("name").Text) & "', "
                l_strSQL = l_strSQL & "contactID = " & cmbContact.Columns("contactID").Text & ", "
            End If
        End If
        l_strSQL = l_strSQL & "title1 = '" & QuoteSanitise(Left(txtTitle.Text, 50)) & "', "
        l_strSQL = l_strSQL & "notes2 = '" & QuoteSanitise(txtRequirement.Text) & "', "
        l_strSQL = l_strSQL & "orderreference = ' " & QuoteSanitise(txtOrderNumber.Text) & "'"
        l_strSQL = l_strSQL & " WHERE jobID = " & l_lngJobID
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        lblJobID.Caption = l_lngJobID
    
    End If
    l_rsJobs.Close
    Set l_rsJobs = Nothing
    
    If UCase(Left(txtEpisodeStart.Text, 1)) = "F" Then l_strEpisodePrefix = "F" Else l_strEpisodePrefix = ""
    l_intEpStart = IIf(l_strEpisodePrefix = "F", Val(Mid(txtEpisodeStart.Text, 2)), Val(txtEpisodeStart.Text))
    l_intEpEnd = IIf(l_strEpisodePrefix = "F", Val(Mid(txtEpisodeEnd.Text, 2)), Val(txtEpisodeEnd.Text))
    
    Set l_rsDetails = ExecuteSQL("SELECT * FROM jobdetail WHERE jobID = " & l_lngJobID, g_strExecuteError)
    CheckForSQLError
    
    If optOrderType(0).Value = True Then
        Set l_rsDisneyTracker = ExecuteSQL("SELECT * FROM disneytracker WHERE jobID = -1", g_strExecuteError)
        CheckForSQLError
    End If

    If optOrderType(2).Value = True Then
        Set l_rsPlayoutTracker = ExecuteSQL("SELECT * FROM trackerplayout WHERE jobID = -1", g_strExecuteError)
        CheckForSQLError
    End If

    If optOrderType(3).Value = True Then
        Set l_rsPlatformTracker = ExecuteSQL("SELECT * FROM tracker_itunes_item WHERE jobID = -1", g_strExecuteError)
        CheckForSQLError
    End If

    If optOrderType(4).Value = True Then
        Set l_rsGenericTracker = ExecuteSQL("SELECT * FROM tracker_item WHERE jobID = -1", g_strExecuteError)
        CheckForSQLError
    End If

    If l_intEpEnd > 0 And l_intEpStart > 0 And l_intEpEnd > l_intEpStart Then
        If optOrderType(0).Value = True And txtElementNumber.Text <> "" Then
            MsgBox "Element numbers will be requested as we go through - please enter them as prompted.", vbInformation, "Element Numbers"
            txtElementNumber.Text = ""
        End If
        For Count = l_intEpStart To l_intEpEnd
            If optOrderType(3).Value = False Then
                'Not a Platform order, so make a job detail line based on the chosen workflow stages and if it is a DADC playout order make a playout tracker line
                For Count2 = 0 To 5
                    If cmbInclusive(Count2).Text <> "" Then
                        l_rsDetails.AddNew
                        l_rsDetails("jobID") = l_lngJobID
                        l_rsDetails("description") = txtTitle.Text & " - Ep." & IIf(l_strEpisodePrefix = "F", l_strEpisodePrefix & Format(Count, "000"), Count)
                        l_rsDetails("workflowdescription") = cmbInclusive(Count2).Columns("description").Text
                        l_rsDetails("format") = cmbInclusive(Count2).Columns("format").Text
                        l_rsDetails("copytype") = cmbInclusive(Count2).Columns("copytype").Text
                        l_rsDetails("quantity") = 1
                        If cmbInclusive(Count2).Columns("copytype").Text = "O" Then
                            l_rsDetails("runningtime") = 0
                        Else
                            l_rsDetails("runningtime") = Val(txtDuration)
                        End If
                        l_rsDetails.Update
                        l_rsDetails.Bookmark = l_rsDetails.Bookmark
                        l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                        l_rsDetails.Update
                        l_rsDetails.Bookmark = l_rsDetails.Bookmark
                        l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                        l_rsDetails.Update
                        
                        l_rsDetails.Bookmark = l_rsDetails.Bookmark
                        l_lngJobDetailID = l_rsDetails("jobdetailID")
                        
                        If optOrderType(2).Value = True Then
                            'DADC playout tracker order
                            l_rsPlayoutTracker.AddNew
                            l_rsPlayoutTracker("companyID") = Val(lblCompanyID.Caption)
                            l_rsPlayoutTracker("title") = txtTitle.Text
                            l_rsPlayoutTracker("episodenumber") = Format(Count, "00")
                            l_rsPlayoutTracker("specialinstructions") = txtRequirement.Text
                            l_rsPlayoutTracker("bbcorderfor") = txtBBCCustomer.Text
                            l_rsPlayoutTracker("deliverydetails") = txtDeliveryDetails.Text
                            l_rsPlayoutTracker("bbcordernumber") = txtOrderNumber.Text
                            l_rsPlayoutTracker("jobID") = l_lngJobID
                            l_rsPlayoutTracker("duedate") = datDueDate.Value
                            l_rsPlayoutTracker("jobdetailID" & Count2 + 1) = l_lngJobDetailID
                            l_rsPlayoutTracker("tapeformat" & Count2 + 1) = cmbInclusive(Count2).Columns("format").Text
                            l_rsPlayoutTracker.Update
                        End If
                        If optOrderType(4).Value = True Then
                            'Generic Tracker Order -
                            Set l_rsTemp = ExecuteSQL("SELECT * FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption, g_strExecuteError)
                            If l_rsTemp.RecordCount > 0 Then
                                Do While Not l_rsTemp.EOF
                                    If LCase(l_rsTemp("itemheading")) = "title" Then l_strTitleField = l_rsTemp("itemfield")
                                    If LCase(l_rsTemp("itemheading")) = "episode" Then l_strEpisodeField = l_rsTemp("itemfield")
                                    If LCase(l_rsTemp("itemheading")) = "series" Then l_strSeriesField = l_rsTemp("itemfield")
                                    If LCase(l_rsTemp("itemheading")) = "elements" Then l_strElementsField = l_rsTemp("itemfield")
                                    If LCase(l_rsTemp("itemheading")) = "version" Then l_strVersionField = l_rsTemp("itemfield")
                                    If LCase(l_rsTemp("itemheading")) = "language" Then l_strLanguageField = l_rsTemp("itemfield")
                                    If LCase(l_rsTemp("itemheading")) = "order #" Then l_strOrderField = l_rsTemp("itemfield")
                                    l_rsTemp.MoveNext
                                Loop
                            End If
                            l_rsTemp.Close
                            Set l_rsTemp = Nothing
                            l_strElementNumber = InputBox("Episode: " & Count, "Please enter the Item Reference")
                            l_rsGenericTracker.AddNew
                            l_rsGenericTracker("companyID") = Val(lblCompanyID.Caption)
                            If l_strTitleField <> "" Then l_rsGenericTracker(l_strTitleField) = txtTitle.Text
                            If l_strEpisodeField <> "" Then l_rsGenericTracker(l_strEpisodeField) = l_strEpisodePrefix & Format(Count, "000")
                            If l_strSeriesField <> "" Then l_rsGenericTracker(l_strSeriesField) = txtGenericSeries.Text
                            If l_strElementsField <> "" Then l_rsGenericTracker(l_strElementsField) = txtGenericElements.Text
                            If l_strVersionField <> "" Then l_rsGenericTracker(l_strVersionField) = IIf(IsNumeric(txtGenericVersion.Text), txtGenericVersion.Text, Null)
                            If l_strLanguageField <> "" Then l_rsGenericTracker(l_strLanguageField) = cmbGenericLanguage.Text
                            If l_strOrderField <> "" Then l_rsGenericTracker(l_strOrderField) = txtOrderNumber.Text
                            l_rsGenericTracker("jobID") = l_lngJobID
                            l_rsGenericTracker("requiredby") = Format(datDueDate.Value, "YYYY-MM-DD HH:NN:SS")
                            l_rsGenericTracker("itemreference") = l_strElementNumber
                            temp = GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption))
                            If InStr(temp, "/trackercontactfield") > 0 Then
                                temp = Mid(temp, InStr(temp, "/trackercontactfield") + 21)
                                temp = "headertext" & Val(temp)
                                l_rsGenericTracker(temp) = cmbContact.Text
                            End If
                            l_rsGenericTracker.Update
                        End If
                    End If
                Next
            Else
                'Platform order so make the necessary job detail lines for the chosen platform and make a Platform tracker line
                If optPlatformOrderType(0).Value = False And optPlatformOrderType(1).Value = False And optPlatformOrderType(2).Value = False And _
                optPlatformOrderType(3).Value = False And optPlatformOrderType(4).Value = False And optPlatformOrderType(5).Value = False And optPlatformOrderType(6).Value = False _
                And optPlatformOrderType(7).Value = False And optPlatformOrderType(8).Value = False Then
                    MsgBox "You must chose a platform Order Type.", vbCritical, "Error..."
                    l_rsPlatformTracker.Close
                    l_rsDetails.Close
                    Exit Sub
                End If
                 
                If optPlatformOrderType(0).Value = True Or optPlatformOrderType(3).Value = True Or optPlatformOrderType(4).Value = True Then
                    'iTunes TV, Amazon or XBox
                    l_rsDetails.AddNew
                    l_rsDetails("jobID") = l_lngJobID
                    l_rsDetails("description") = txtTitle.Text & " - Ep." & l_strEpisodePrefix & Format(Count, "000")
                    
                    If optPlatformOrderType(0).Value = True Then
                        If lblCompanyID.Caption = "1163" Then
                            'Disney iTunes TV
                            If chkPlatformHD.Value = 0 Then
                                l_rsDetails("workflowdescription") = "DISNEY Transcode any SD file format to any file format - EPISODIC"
                                l_rsDetails("format") = "DISTRANSDEP"
                                l_rsDetails("copytype") = "O"
                            Else
                                l_rsDetails("workflowdescription") = "DISNEY Transcode any HD file format to any file format - EPISODIC"
                                l_rsDetails("format") = "DISTRANHDEP"
                                l_rsDetails("copytype") = "O"
                            End If
                        ElseIf lblCompanyID.Caption = "310" Then
                            'Other people's iTunes...
                            If chkPlatformHD.Value <> 0 Then
                                l_rsDetails("workflowdescription") = "iTunes HD from HDCAM-SR source"
                                l_rsDetails("format") = "ITUNESHDHDCAMSR"
                                l_rsDetails("copytype") = "I"
                            Else
                                l_rsDetails("workflowdescription") = "iTunes SD from D-BETA source"
                                l_rsDetails("format") = "ITUNESSDDB"
                                l_rsDetails("copytype") = "I"
                                l_rsDetails("quantity") = 1
                                l_rsDetails("runningtime") = Val(txtDuration)
                                l_rsDetails.Update
                                l_rsDetails.AddNew
                                l_rsDetails("jobID") = l_lngJobID
                                l_rsDetails("description") = txtTitle.Text & " - Ep." & Count
                                l_rsDetails("workflowdescription") = "iTunes Delivery"
                                l_rsDetails("format") = "MEDDEL"
                                l_rsDetails("copytype") = "O"
                            End If
                        ElseIf lblCompanyID.Caption = "305" Then
                            If chkPlatformHD.Value <> 0 Then
                                l_rsDetails("workflowdescription") = "Transcoding HD"
                                l_rsDetails("format") = "HATTRANHD"
                                l_rsDetails("copytype") = "I"
                            Else
                                l_rsDetails("workflowdescription") = "Transcoding SD"
                                l_rsDetails("format") = "HATTRANSD"
                                l_rsDetails("copytype") = "I"
                            End If
                        Else
                            If chkPlatformHD.Value <> 0 Then
                                l_rsDetails("workflowdescription") = "Transcoding HD"
                                l_rsDetails("format") = "TRAN-HD"
                                l_rsDetails("copytype") = "M"
                            Else
                                l_rsDetails("workflowdescription") = "Transcoding SD"
                                l_rsDetails("format") = "TRAN-B"
                                l_rsDetails("copytype") = "M"
                            End If
                        End If
                    ElseIf optPlatformOrderType(3).Value = True Then
                        l_rsDetails("workflowdescription") = "DISNEY Transcode any HD file format to any file format - EPISODIC"
                        l_rsDetails("format") = "DISTRANHDEP"
                        l_rsDetails("copytype") = "O"
                    ElseIf optPlatformOrderType(4).Value = True Then
                        l_rsDetails("workflowdescription") = "DIS Bundle Amazon MLF"
                        l_rsDetails("format") = "DISBUNAMAMLF"
                        l_rsDetails("copytype") = "I"
                    End If
                    l_rsDetails("quantity") = 1
                    If l_rsDetails("copytype") = "O" Then
                        l_rsDetails("runningtime") = 0
                    Else
                        l_rsDetails("runningtime") = Val(txtDuration)
                    End If
                    l_rsDetails.Update
                    l_rsDetails.Bookmark = l_rsDetails.Bookmark
                    l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                    l_rsDetails.Update
                    l_rsDetails.Bookmark = l_rsDetails.Bookmark
                    l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                    l_rsDetails.Update
                ElseIf optPlatformOrderType(1).Value = True Then
                    'Google Item
                    l_rsDetails.AddNew
                    l_rsDetails("jobID") = l_lngJobID
                    l_rsDetails("description") = txtTitle.Text & " - Ep." & Count
                    If chkPlatformHD.Value <> 0 Then
                        l_rsDetails("workflowdescription") = "Digital File Delivery or Download HD"
                        l_rsDetails("format") = "GOOGLEDELHD"
                    Else
                        l_rsDetails("workflowdescription") = "Digital File Delivery or Download SD"
                        l_rsDetails("format") = "GOOGLEDELSD"
                    End If
                    l_rsDetails("copytype") = "I"
                    l_rsDetails("quantity") = 1
                    l_rsDetails("runningtime") = Val(txtDuration)
                    l_rsDetails.Update
                    l_rsDetails.Bookmark = l_rsDetails.Bookmark
                    l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                    l_rsDetails.Update
                    l_rsDetails.Bookmark = l_rsDetails.Bookmark
                    l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                    l_rsDetails.Update
                    'Transcode and Package
                    l_rsDetails.AddNew
                    l_rsDetails("jobID") = l_lngJobID
                    l_rsDetails("description") = txtTitle.Text & " - Ep." & Count
                    If chkPlatformHD.Value <> 0 Then
                        l_rsDetails("workflowdescription") = "Transcode to Google Spec from HD source"
                        l_rsDetails("format") = "GOOGLETRHD"
                    Else
                        l_rsDetails("workflowdescription") = "Transcode to Google Spec from SD source"
                        l_rsDetails("format") = "GOOGLETRSD"
                    End If
                    l_rsDetails("copytype") = "I"
                    l_rsDetails("quantity") = 1
                    l_rsDetails("runningtime") = Val(txtDuration)
                    l_rsDetails.Update
                    l_rsDetails.Bookmark = l_rsDetails.Bookmark
                    l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                    l_rsDetails.Update
                    l_rsDetails.Bookmark = l_rsDetails.Bookmark
                    l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                    l_rsDetails.Update
                    'Outgoing Delivery
                    l_rsDetails.AddNew
                    l_rsDetails("jobID") = l_lngJobID
                    l_rsDetails("description") = txtTitle.Text & " - Ep." & Count
                    If chkPlatformHD.Value <> 0 Then
                        l_rsDetails("workflowdescription") = "Digital File Delivery or Download HD"
                        l_rsDetails("format") = "GOOGLEDELHD"
                    Else
                        l_rsDetails("workflowdescription") = "Digital File Delivery or Download SD"
                        l_rsDetails("format") = "GOOGLEDELSD"
                    End If
                    l_rsDetails("copytype") = "I"
                    l_rsDetails("quantity") = 1
                    l_rsDetails("runningtime") = Val(txtDuration)
                    l_rsDetails.Update
                    l_rsDetails.Bookmark = l_rsDetails.Bookmark
                    l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                    l_rsDetails.Update
                    l_rsDetails.Bookmark = l_rsDetails.Bookmark
                    l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                    l_rsDetails.Update
                ElseIf optPlatformOrderType(2).Value = True Then
                    'Google OMU
                    l_rsDetails.AddNew
                    l_rsDetails("jobID") = l_lngJobID
                    l_rsDetails("description") = txtTitle.Text & " - Ep." & Count
                    If chkPlatformHD.Value <> 0 Then
                        l_rsDetails("workflowdescription") = "Digital File Delivery or Download HD"
                        l_rsDetails("format") = "GOOGLEDELHD"
                    Else
                        l_rsDetails("workflowdescription") = "Digital File Delivery or Download SD"
                        l_rsDetails("format") = "GOOGLEDELSD"
                    End If
                    l_rsDetails("copytype") = "I"
                    l_rsDetails("quantity") = 1
                    l_rsDetails("runningtime") = Val(txtDuration)
                    l_rsDetails.Update
                    l_rsDetails.Bookmark = l_rsDetails.Bookmark
                    l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                    l_rsDetails.Update
                    l_rsDetails.Bookmark = l_rsDetails.Bookmark
                    l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                    l_rsDetails.Update
                    'Transcode and Package
                    l_rsDetails.AddNew
                    l_rsDetails("jobID") = l_lngJobID
                    l_rsDetails("description") = txtTitle.Text & " - Ep." & Count
                    If chkPlatformHD.Value <> 0 Then
                        l_rsDetails("workflowdescription") = "Transcode to Google Spec from HD source"
                        l_rsDetails("format") = "GOOGLETRHD"
                    Else
                        l_rsDetails("workflowdescription") = "Transcode to Google Spec from SD source"
                        l_rsDetails("format") = "GOOGLETRSD"
                    End If
                    l_rsDetails("copytype") = "I"
                    l_rsDetails("quantity") = 1
                    l_rsDetails("runningtime") = Val(txtDuration)
                    l_rsDetails.Update
                    l_rsDetails.Bookmark = l_rsDetails.Bookmark
                    l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                    l_rsDetails.Update
                    l_rsDetails.Bookmark = l_rsDetails.Bookmark
                    l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                    l_rsDetails.Update
                    'Outgoing Delivery
                    l_rsDetails.AddNew
                    l_rsDetails("jobID") = l_lngJobID
                    l_rsDetails("description") = txtTitle.Text & " - Ep." & Count
                    If chkPlatformHD.Value <> 0 Then
                        l_rsDetails("workflowdescription") = "Digital File Delivery or Download HD"
                        l_rsDetails("format") = "GOOGLEDELHD"
                    Else
                        l_rsDetails("workflowdescription") = "Digital File Delivery or Download SD"
                        l_rsDetails("format") = "GOOGLEDELSD"
                    End If
                    l_rsDetails("copytype") = "I"
                    l_rsDetails("quantity") = 1
                    l_rsDetails("runningtime") = Val(txtDuration)
                    l_rsDetails.Update
                    l_rsDetails.Bookmark = l_rsDetails.Bookmark
                    l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                    l_rsDetails.Update
                    l_rsDetails.Bookmark = l_rsDetails.Bookmark
                    l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                    l_rsDetails.Update
                ElseIf optPlatformOrderType(5).Value = True Then
                    'XBox
                ElseIf optPlatformOrderType(6).Value = True Then
                    'Sony PSN
                ElseIf optPlatformOrderType(7).Value = True Then
                    'Netflix
                ElseIf optPlatformOrderType(8).Value = True Then
                    'Disney Dub Cards
                End If
                l_rsPlatformTracker.AddNew
                If optPlatformOrderType(1).Value = True Or optPlatformOrderType(2).Value = True Then
                    l_rsPlatformTracker("companyID") = 1489
                Else
                    l_rsPlatformTracker("companyID") = Val(lblCompanyID.Caption)
                End If
                l_rsPlatformTracker("title") = txtTitle.Text
                l_rsPlatformTracker("episode") = IIf(l_strEpisodePrefix = "F", l_strEpisodePrefix & Format(Count, "000"), Count)
                l_rsPlatformTracker("jobID") = l_lngJobID
                l_rsPlatformTracker("requiredby") = datDueDate.Value
                l_rsPlatformTracker("FeatureLanguage1") = cmbLanguage.Text
                l_rsPlatformTracker("OrderNumber") = txtOrderNumber.Text
                If optPlatformOrderType(0).Value = True Then
                    l_rsPlatformTracker("iTunesOrderType") = "iTunes TV Delivery"
                ElseIf optPlatformOrderType(1).Value = True Then
                    l_rsPlatformTracker("iTunesOrderType") = "Google Delivery"
                ElseIf optPlatformOrderType(2).Value = True Then
                    l_rsPlatformTracker("iTunesOrderType") = "Google OMU Delivery"
                ElseIf optPlatformOrderType(3).Value = True Then
                    l_rsPlatformTracker("iTunesOrderType") = "Amazon Delivery"
                ElseIf optPlatformOrderType(4).Value = True Then
                    l_rsPlatformTracker("iTunesOrderType") = "Amazon MLF Delivery"
                    l_rsPlatformTracker("subslanguage1") = "German"
                    l_rsPlatformTracker("subslanguage2") = "English"
                ElseIf optPlatformOrderType(5).Value = True Then
                    l_rsPlatformTracker("iTunesOrderType") = "XBox"
                    l_rsPlatformTracker("FeatureLanguage1") = "English"
                    l_rsPlatformTracker("FeatureAudio2") = "French"
                    l_rsPlatformTracker("FeatureAudio3") = "German"
                ElseIf optPlatformOrderType(6).Value = True Then
                    l_rsPlatformTracker("iTunesOrderType") = "Sony PSN"
                ElseIf optPlatformOrderType(7).Value = True Then
                    l_rsPlatformTracker("iTunesOrderType") = "Netflix"
                ElseIf optPlatformOrderType(8).Value = True Then
                    l_rsPlatformTracker("iTunesOrderType") = "Disney Dub Cards"
                    If chkPlatformHD.Value <> 0 Then
                        l_rsPlatformTracker("FeatureHD") = 1
                    Else
                        l_rsPlatformTracker("FeatureHD") = 0
                    End If
                    If cmbFrameRate.Text <> "" Then
                        l_rsPlatformTracker("DisneyPlus_Framerate") = cmbFrameRate.Text
                    Else
                        l_rsPlatformTracker("DisneyPlus_Framerate") = Null
                    End If
                End If
                l_rsPlatformTracker.Update
            End If
            If optOrderType(0).Value = True Then
                'Disney tracker order, so make a Disney Tracker entry
'                l_strElementNumber = InputBox("Episode: " & Count, "Please enter the Element Number")
                l_rsDisneyTracker.AddNew
                l_rsDisneyTracker("companyID") = 1163
                l_rsDisneyTracker("title") = txtTitle.Text
                l_rsDisneyTracker("episode") = Format(Count, "000")
                l_rsDisneyTracker("requirement") = Left(txtRequirement.Text, 250)
                l_rsDisneyTracker("disneycontactID") = cmbContact.Columns("contactID").Text
                l_rsDisneyTracker("ordernumber") = txtOrderNumber.Text
                l_rsDisneyTracker("jobID") = l_lngJobID
                l_rsDisneyTracker("jcacontact") = cmbJCAContact.Text
                l_rsDisneyTracker("duedate") = datDueDate.Value
'                l_rsDisneyTracker("elementnumber") = l_strElementNumber
                l_rsDisneyTracker("jobdetailID") = l_lngJobDetailID
                l_rsDisneyTracker.Update
            End If
        Next
    Else
        'Single Episode or no episode
        If optOrderType(3).Value = False Then
            'Not a Platform order, so make a job detail line based on the chosen workflow stages and if it is a DADC playout order make a playout tracker line
    
            For Count2 = 0 To 5
                If cmbInclusive(Count2).Text <> "" Then
                    l_rsDetails.AddNew
                    l_rsDetails("jobID") = l_lngJobID
                    If l_intEpStart > 0 Then
                        l_rsDetails("description") = txtTitle.Text & " - Ep." & IIf(l_strEpisodePrefix = "F", l_strEpisodePrefix & Format(l_intEpStart, "000"), l_intEpStart)
                    Else
                        l_rsDetails("description") = txtTitle.Text
                    End If
                    l_rsDetails("workflowdescription") = cmbInclusive(Count2).Columns("description").Text
                    l_rsDetails("format") = cmbInclusive(Count2).Columns("format").Text
                    l_rsDetails("copytype") = cmbInclusive(Count2).Columns("copytype").Text
                    l_rsDetails("quantity") = 1
                    If cmbInclusive(Count2).Columns("copytype").Text = "O" Then
                        l_rsDetails("runningtime") = 0
                    Else
                        l_rsDetails("runningtime") = Val(txtDuration)
                    End If
                    l_rsDetails.Update
                    l_rsDetails.Bookmark = l_rsDetails.Bookmark
                    l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                    l_rsDetails.Update
                    
                    l_rsDetails.Bookmark = l_rsDetails.Bookmark
                    l_lngJobDetailID = l_rsDetails("jobdetailID")
                    
                    If optOrderType(2).Value = True Then
                        l_rsPlayoutTracker.AddNew
                        l_rsPlayoutTracker("companyID") = Val(lblCompanyID.Caption)
                        l_rsPlayoutTracker("title") = txtTitle.Text
                        l_rsPlayoutTracker("episodenumber") = IIf(l_intEpStart > 0, l_intEpStart, Null)
                        l_rsPlayoutTracker("specialinstructions") = txtRequirement.Text
                        l_rsPlayoutTracker("bbcorderfor") = txtBBCCustomer.Text
                        l_rsPlayoutTracker("deliverydetails") = txtDeliveryDetails.Text
                        l_rsPlayoutTracker("bbcordernumber") = txtOrderNumber.Text
                        l_rsPlayoutTracker("jobID") = l_lngJobID
                        l_rsPlayoutTracker("duedate") = datDueDate.Value
                        l_rsPlayoutTracker("jobdetailID" & Count2 + 1) = l_lngJobDetailID
                        l_rsPlayoutTracker("tapeformat" & Count2 + 1) = cmbInclusive(Count2).Columns("format").Text
                        l_rsPlayoutTracker.Update
                    End If
                End If
            Next
            If optOrderType(4).Value = True Then
                'Generic Tracker Order -
                l_strElementNumber = InputBox("Episode: " & Count, "Please enter the Item Reference")
                Set l_rsTemp = ExecuteSQL("SELECT * FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption, g_strExecuteError)
                If l_rsTemp.RecordCount > 0 Then
                    Do While Not l_rsTemp.EOF
                        If LCase(l_rsTemp("itemheading")) = "title" Then l_strTitleField = l_rsTemp("itemfield")
                        If LCase(l_rsTemp("itemheading")) = "episode" Then l_strEpisodeField = l_rsTemp("itemfield")
                        If LCase(l_rsTemp("itemheading")) = "series" Then l_strSeriesField = l_rsTemp("itemfield")
                        If LCase(l_rsTemp("itemheading")) = "elements" Then l_strElementsField = l_rsTemp("itemfield")
                        If LCase(l_rsTemp("itemheading")) = "version" Then l_strVersionField = l_rsTemp("itemfield")
                        If LCase(l_rsTemp("itemheading")) = "language" Then l_strLanguageField = l_rsTemp("itemfield")
                        If LCase(l_rsTemp("itemheading")) = "order #" Then l_strOrderField = l_rsTemp("itemfield")
                        l_rsTemp.MoveNext
                    Loop
                End If
                l_rsTemp.Close
                Set l_rsTemp = Nothing
                l_rsGenericTracker.AddNew
                l_rsGenericTracker("companyID") = Val(lblCompanyID.Caption)
                l_rsGenericTracker(l_strTitleField) = txtTitle.Text
                If l_strEpisodeField <> "" Then l_rsGenericTracker(l_strEpisodeField) = IIf(l_intEpStart > 0, IIf(l_strEpisodePrefix = "F", l_strEpisodePrefix & Format(l_intEpStart, "000"), l_intEpStart), Null)
                If l_strSeriesField <> "" Then If l_strSeriesField <> "" Then l_rsGenericTracker(l_strSeriesField) = txtGenericSeries.Text
                If l_strElementsField <> "" Then l_rsGenericTracker(l_strElementsField) = txtGenericElements.Text
                If l_strVersionField <> "" Then l_rsGenericTracker(l_strVersionField) = IIf(IsNumeric(txtGenericVersion.Text), txtGenericVersion.Text, Null)
                If l_strLanguageField <> "" Then l_rsGenericTracker(l_strLanguageField) = cmbGenericLanguage.Text
                If l_strOrderField <> "" Then l_rsGenericTracker(l_strOrderField) = txtOrderNumber.Text
                l_rsGenericTracker("jobID") = l_lngJobID
                l_rsGenericTracker("itemreference") = l_strElementNumber
                l_rsGenericTracker("requiredby") = Format(datDueDate.Value, "YYYY-MM-DD HH:NN:SS")
                temp = GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption))
                If InStr(temp, "/trackercontactfield") > 0 Then
                    temp = Mid(temp, InStr(temp, "/trackercontactfield") + 21)
                    temp = "headertext" & Val(temp)
                    l_rsGenericTracker(temp) = cmbContact.Text
                End If
                l_rsGenericTracker.Update
            End If
        Else
            'Platform order so make the necessary job detail lines fior the chosen platform and make a Platform tracker line
            If optPlatformOrderType(0).Value = False And optPlatformOrderType(1).Value = False And optPlatformOrderType(2).Value = False And _
            optPlatformOrderType(3).Value = False And optPlatformOrderType(4).Value = False And optPlatformOrderType(5).Value = False And optPlatformOrderType(6).Value = False _
            And optPlatformOrderType(7).Value = False And optPlatformOrderType(8).Value = False Then
                MsgBox "You must chose a platform Order Type.", vbCritical, "Error..."
                l_rsPlatformTracker.Close
                l_rsDetails.Close
                Exit Sub
            End If
            
            If optPlatformOrderType(0).Value = True Or optPlatformOrderType(3).Value = True Or optPlatformOrderType(4).Value = True Then
                'TunesTV, Amazon or Amazon MLF
                l_rsDetails.AddNew
                l_rsDetails("jobID") = l_lngJobID
                If l_intEpStart > 0 Then
                    l_rsDetails("description") = txtTitle.Text & " - Ep." & l_intEpStart
                Else
                    l_rsDetails("description") = txtTitle.Text
                End If
                If optPlatformOrderType(0).Value = True Then
                    If lblCompanyID.Caption = "1163" Then
                        'Disney iTunes TV
                        If chkPlatformHD.Value = 0 Then
                            l_rsDetails("workflowdescription") = "DISNEY Transcode any SD file format to any file format - EPISODIC"
                            l_rsDetails("format") = "DISTRANSDEP"
                            l_rsDetails("copytype") = "O"
                        Else
                            l_rsDetails("workflowdescription") = "DISNEY Transcode any HD file format to any file format - EPISODIC"
                            l_rsDetails("format") = "DISTRANHDEP"
                            l_rsDetails("copytype") = "O"
                        End If
                    ElseIf lblCompanyID.Caption = "310" Then
                        'Channel Four iTunes
                        If chkPlatformHD.Value <> 0 Then
                            l_rsDetails("workflowdescription") = "iTunes HD from HDCAM-SR source"
                            l_rsDetails("format") = "ITUNESHDHDCAMSR"
                            l_rsDetails("copytype") = "I"
                        Else
                            l_rsDetails("workflowdescription") = "iTunes SD from D-BETA source"
                            l_rsDetails("format") = "ITUNESSDDB"
                            l_rsDetails("copytype") = "I"
                            l_rsDetails("quantity") = 1
                            l_rsDetails("runningtime") = Val(txtDuration)
                            l_rsDetails.Update
                            l_rsDetails.AddNew
                            l_rsDetails("jobID") = l_lngJobID
                            If l_intEpStart > 0 Then
                                l_rsDetails("description") = txtTitle.Text & " - Ep." & l_intEpStart
                            Else
                                l_rsDetails("description") = txtTitle.Text
                            End If
                            l_rsDetails("workflowdescription") = "iTunes Delivery"
                            l_rsDetails("format") = "MEDDEL"
                            l_rsDetails("copytype") = "O"
                        End If
                    ElseIf lblCompanyID = "305" Then
                        'Other people's iTunes
                        If chkPlatformHD.Value <> 0 Then
                            l_rsDetails("workflowdescription") = "Transcoding HD"
                            l_rsDetails("format") = "HATTRANHD"
                            l_rsDetails("copytype") = "I"
                        Else
                            l_rsDetails("workflowdescription") = "Transcoding SD"
                            l_rsDetails("format") = "HATTRANSD"
                            l_rsDetails("copytype") = "I"
                        End If
                    Else
                        'Other people's iTunes
                        If chkPlatformHD.Value <> 0 Then
                            l_rsDetails("workflowdescription") = "Transcoding HD"
                            l_rsDetails("format") = "TRAN-HD"
                            l_rsDetails("copytype") = "M"
                        Else
                            l_rsDetails("workflowdescription") = "Transcoding SD"
                            l_rsDetails("format") = "TRAN-B"
                            l_rsDetails("copytype") = "M"
                        End If
                    End If
                ElseIf optPlatformOrderType(3).Value = True Then
                    l_rsDetails("workflowdescription") = "DISNEY Transcode any HD file format to any file format - EPISODIC"
                    l_rsDetails("format") = "DISTRANHDEP"
                    l_rsDetails("copytype") = "O"
                ElseIf optPlatformOrderType(4).Value = True Then
                    l_rsDetails("workflowdescription") = "DIS Bundle Amazon MLF"
                    l_rsDetails("format") = "DISBUNAMAMLF"
                    l_rsDetails("copytype") = "I"
                End If
                l_rsDetails("quantity") = 1
                If l_rsDetails("copytype") = "O" Then
                    l_rsDetails("runningtime") = 0
                Else
                    l_rsDetails("runningtime") = Val(txtDuration)
                End If
                l_rsDetails.Update
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
            ElseIf optPlatformOrderType(1).Value = True Then
                'Google
                'Incoming Delivery first
                l_rsDetails.AddNew
                l_rsDetails("jobID") = l_lngJobID
                If l_intEpStart > 0 Then
                    l_rsDetails("description") = txtTitle.Text & " - Ep." & l_intEpStart
                Else
                    l_rsDetails("description") = txtTitle.Text
                End If
                If chkPlatformHD.Value <> 0 Then
                    l_rsDetails("workflowdescription") = "Digital File Delivery or Download HD"
                    l_rsDetails("format") = "GOOGLEDELHD"
                Else
                    l_rsDetails("workflowdescription") = "Digital File Delivery or Download SD"
                    l_rsDetails("format") = "GOOGLEDELSD"
                End If
                l_rsDetails("copytype") = "I"
                l_rsDetails("quantity") = 1
                l_rsDetails("runningtime") = Val(txtDuration)
                l_rsDetails.Update
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                'Then Transcode and Package
                l_rsDetails.AddNew
                l_rsDetails("jobID") = l_lngJobID
                If l_intEpStart > 0 Then
                    l_rsDetails("description") = txtTitle.Text & " - Ep." & l_intEpStart
                Else
                    l_rsDetails("description") = txtTitle.Text
                End If
                If chkPlatformHD.Value <> 0 Then
                    l_rsDetails("workflowdescription") = "Transcode to Google Spec from HD source"
                    l_rsDetails("format") = "GOOGLETRHD"
                Else
                    l_rsDetails("workflowdescription") = "Transcode to Google Spec from SD source"
                    l_rsDetails("format") = "GOOGLETRSD"
                End If
                l_rsDetails("copytype") = "I"
                l_rsDetails("quantity") = 1
                l_rsDetails("runningtime") = Val(txtDuration)
                l_rsDetails.Update
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                'Then Outgoing Delivery
                l_rsDetails.AddNew
                l_rsDetails("jobID") = l_lngJobID
                If l_intEpStart > 0 Then
                    l_rsDetails("description") = txtTitle.Text & " - Ep." & l_intEpStart
                Else
                    l_rsDetails("description") = txtTitle.Text
                End If
                If chkPlatformHD.Value <> 0 Then
                    l_rsDetails("workflowdescription") = "Digital File Delivery or Download HD"
                    l_rsDetails("format") = "GOOGLEDELHD"
                Else
                    l_rsDetails("workflowdescription") = "Digital File Delivery or Download SD"
                    l_rsDetails("format") = "GOOGLEDELSD"
                End If
                l_rsDetails("copytype") = "I"
                l_rsDetails("quantity") = 1
                l_rsDetails("runningtime") = Val(txtDuration)
                l_rsDetails.Update
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
            ElseIf optPlatformOrderType(2).Value = True Then
                'Goggle OMU
                'Incoming Delivery first
                l_rsDetails.AddNew
                l_rsDetails("jobID") = l_lngJobID
                If l_intEpStart > 0 Then
                    l_rsDetails("description") = txtTitle.Text & " - Ep." & l_intEpStart
                Else
                    l_rsDetails("description") = txtTitle.Text
                End If
                If chkPlatformHD.Value <> 0 Then
                    l_rsDetails("workflowdescription") = "Digital File Delivery or Download HD"
                    l_rsDetails("format") = "GOOGLEDELHD"
                Else
                    l_rsDetails("workflowdescription") = "Digital File Delivery or Download SD"
                    l_rsDetails("format") = "GOOGLEDELSD"
                End If
                l_rsDetails("copytype") = "I"
                l_rsDetails("quantity") = 1
                l_rsDetails("runningtime") = Val(txtDuration)
                l_rsDetails.Update
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                'Then Transcode and Package
                l_rsDetails.AddNew
                l_rsDetails("jobID") = l_lngJobID
                If l_intEpStart > 0 Then
                    l_rsDetails("description") = txtTitle.Text & " - Ep." & l_intEpStart
                Else
                    l_rsDetails("description") = txtTitle.Text
                End If
                If chkPlatformHD.Value <> 0 Then
                    l_rsDetails("workflowdescription") = "Transcode to Google Spec from HD source"
                    l_rsDetails("format") = "GOOGLETRHD"
                Else
                    l_rsDetails("workflowdescription") = "Transcode to Google Spec from SD source"
                    l_rsDetails("format") = "GOOGLETRSD"
                End If
                l_rsDetails("copytype") = "I"
                l_rsDetails("quantity") = 1
                l_rsDetails("runningtime") = Val(txtDuration)
                l_rsDetails.Update
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                'Then Outgoing Delivery
                l_rsDetails.AddNew
                l_rsDetails("jobID") = l_lngJobID
                If l_intEpStart > 0 Then
                    l_rsDetails("description") = txtTitle.Text & " - Ep." & l_intEpStart
                Else
                    l_rsDetails("description") = txtTitle.Text
                End If
                If chkPlatformHD.Value <> 0 Then
                    l_rsDetails("workflowdescription") = "Digital File Delivery or Download HD"
                    l_rsDetails("format") = "GOOGLEDELHD"
                Else
                    l_rsDetails("workflowdescription") = "Digital File Delivery or Download SD"
                    l_rsDetails("format") = "GOOGLEDELSD"
                End If
                l_rsDetails("copytype") = "I"
                l_rsDetails("quantity") = 1
                l_rsDetails("runningtime") = Val(txtDuration)
                l_rsDetails.Update
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
            ElseIf optPlatformOrderType(5).Value = True Then
                'Xbox
            ElseIf optPlatformOrderType(6).Value = True Then
                'Sony PSN
            ElseIf optPlatformOrderType(7).Value = True Then
                'Netflix
            ElseIf optPlatformOrderType(8).Value = True Then
                'Disney Dub Cards
            End If
            l_rsPlatformTracker.AddNew
            If optPlatformOrderType(1).Value = True Or optPlatformOrderType(2).Value = True Then
                l_rsPlatformTracker("companyID") = 1489
            Else
                l_rsPlatformTracker("companyID") = Val(lblCompanyID.Caption)
            End If
            l_rsPlatformTracker("title") = txtTitle.Text
            l_rsPlatformTracker("episode") = IIf(l_intEpStart > 0, l_intEpStart, "")
            l_rsPlatformTracker("jobID") = l_lngJobID
            l_rsPlatformTracker("requiredby") = datDueDate.Value
            l_rsPlatformTracker("FeatureLanguage1") = cmbLanguage.Text
            l_rsPlatformTracker("OrderNumber") = txtOrderNumber.Text
            If optPlatformOrderType(0).Value = True Then
                l_rsPlatformTracker("iTunesOrderType") = "iTunes TV Delivery"
            ElseIf optPlatformOrderType(1).Value = True Then
                l_rsPlatformTracker("iTunesOrderType") = "Google Delivery"
            ElseIf optPlatformOrderType(2).Value = True Then
                l_rsPlatformTracker("iTunesOrderType") = "Google OMU Delivery"
            ElseIf optPlatformOrderType(3).Value = True Then
                l_rsPlatformTracker("iTunesOrderType") = "Amazon Delivery"
            ElseIf optPlatformOrderType(4).Value = True Then
                l_rsPlatformTracker("iTunesOrderType") = "Amazon MLF Delivery"
                l_rsPlatformTracker("subslanguage1") = "German"
                l_rsPlatformTracker("subslanguage2") = "English"
            ElseIf optPlatformOrderType(5).Value = True Then
                l_rsPlatformTracker("iTunesOrderType") = "XBox"
                l_rsPlatformTracker("FeatureLanguage1") = "English"
                l_rsPlatformTracker("FeatureAudio2") = "French"
                l_rsPlatformTracker("FeatureAudio3") = "German"
            ElseIf optPlatformOrderType(6).Value = True Then
                l_rsPlatformTracker("iTunesOrderType") = "Sony PSN"
            ElseIf optPlatformOrderType(7).Value = True Then
                l_rsPlatformTracker("iTunesOrderType") = "Netflix"
            ElseIf optPlatformOrderType(8).Value = True Then
                l_rsPlatformTracker("iTunesOrderType") = "Disney Dub Cards"
                If chkPlatformHD.Value <> 0 Then
                    l_rsPlatformTracker("FeatureHD") = 1
                Else
                    l_rsPlatformTracker("FeatureHD") = 0
                End If
                If cmbFrameRate.Text <> "" Then
                    l_rsPlatformTracker("DisneyPlus_Framerate") = cmbFrameRate.Text
                Else
                    l_rsPlatformTracker("DisneyPlus_Framerate") = Null
                End If
            End If
            l_rsPlatformTracker.Update
        End If
        
        If optOrderType(0).Value = True Then
            l_rsDisneyTracker.AddNew
            l_rsDisneyTracker("companyID") = 1163
            If l_intEpStart > 0 Then
                l_rsDisneyTracker("title") = txtTitle.Text
                l_rsDisneyTracker("episode") = l_intEpStart
            Else
                l_rsDisneyTracker("title") = txtTitle.Text
            End If
            l_rsDisneyTracker("requirement") = Left(txtRequirement.Text, 250)
            l_rsDisneyTracker("disneycontactID") = cmbContact.Columns("contactID").Text
            l_rsDisneyTracker("ordernumber") = txtOrderNumber.Text
            l_rsDisneyTracker("jobID") = l_lngJobID
            l_rsDisneyTracker("jcacontact") = cmbJCAContact.Text
            l_rsDisneyTracker("duedate") = datDueDate.Value
            l_rsDisneyTracker("elementnumber") = txtElementNumber.Text
            l_rsDisneyTracker("jobdetailID") = l_lngJobDetailID
            l_rsDisneyTracker.Update
        End If
    End If

    l_rsDetails.Close
    Set l_rsDetails = Nothing

    If optOrderType(0).Value = True Then
        l_rsDisneyTracker.Close
        Set l_rsDisneyTracker = Nothing
    End If
    If optOrderType(2).Value = True Then
        l_rsPlayoutTracker.Close
        Set l_rsPlayoutTracker = Nothing
    End If
    If optOrderType(3).Value = True Then
        l_rsPlatformTracker.Close
        Set l_rsPlatformTracker = Nothing
    End If
Else
    MsgBox "Essential information was not all completed.", vbCritical, "Cannot Create New Order"
    Exit Sub
End If

MsgBox "Job and Tracker Entires sucessfully created.", vbInformation, "Success"

End Sub

Private Sub DoCompanyMessages()

If UCase(txtClientStatus.Text) = "HOLD" Or UCase(txtClientStatus.Text) = "CASH" Then
    txtClientStatus.BackColor = vbRed
    MsgBox "This account is currently set to " & txtClientStatus.Text & ". You will not be able to confirm this job.", vbExclamation
Else
    txtClientStatus.BackColor = vbWindowBackground
End If

Dim l_strBookingsMessage As String
l_strBookingsMessage = GetData("company", "messagebookings", "companyID", lblCompanyID.Caption)

If LCase(l_strBookingsMessage) = "false" Then l_strBookingsMessage = ""

If l_strBookingsMessage <> "" Then MsgBox l_strBookingsMessage, vbInformation, "Bookings Alert"

l_strBookingsMessage = GetData("company", "messagecostings", "companyID", lblCompanyID.Caption)
If LCase(l_strBookingsMessage) = "false" Then l_strBookingsMessage = ""
If l_strBookingsMessage <> "" Then MsgBox l_strBookingsMessage, vbInformation, "Costings Alert"

End Sub

Private Sub Form_Load()

PopulateCombo "Language", cmbLanguage
PopulateCombo "Language", cmbGenericLanguage

End Sub

