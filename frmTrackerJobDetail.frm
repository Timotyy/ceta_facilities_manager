VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTrackerJobDetail 
   Caption         =   "Job Detail Tracker"
   ClientHeight    =   15735
   ClientLeft      =   3060
   ClientTop       =   3210
   ClientWidth     =   28725
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   15735
   ScaleWidth      =   28725
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdSomeFields 
      Caption         =   "Operational Fields Only"
      Height          =   315
      Left            =   13920
      TabIndex        =   37
      Top             =   120
      Width           =   1875
   End
   Begin VB.CommandButton cmdAllFields 
      Caption         =   "All Fields"
      Height          =   315
      Left            =   15900
      TabIndex        =   36
      Top             =   120
      Width           =   1095
   End
   Begin VB.TextBox txtBarcode 
      Height          =   315
      Left            =   1620
      TabIndex        =   34
      Top             =   840
      Width           =   2595
   End
   Begin VB.Frame fraSortFilters 
      Caption         =   "Sort Order"
      Height          =   1935
      Left            =   10140
      TabIndex        =   28
      Top             =   60
      Width           =   3735
      Begin VB.OptionButton optSorting 
         Caption         =   "Workable Order (Target Dates)"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   30
         Tag             =   "NOCLEAR"
         Top             =   360
         Value           =   -1  'True
         Width           =   2955
      End
      Begin VB.OptionButton optSorting 
         Caption         =   "Job Order (as on Job Sheet, by Deadlines)"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   29
         Tag             =   "NOCLEAR"
         Top             =   660
         Width           =   3495
      End
   End
   Begin VB.Frame fraFilters 
      Caption         =   "Row Filters"
      Height          =   2115
      Left            =   4320
      TabIndex        =   20
      Top             =   60
      Width           =   5655
      Begin VB.OptionButton optComplete 
         Caption         =   "Completed (Not Despatched)"
         Height          =   255
         Index           =   10
         Left            =   2460
         TabIndex        =   39
         Tag             =   "NOCLEAR"
         Top             =   840
         Width           =   2955
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "Finished (No Charge)"
         Height          =   255
         Index           =   9
         Left            =   2460
         TabIndex        =   38
         Tag             =   "NOCLEAR"
         Top             =   1440
         Width           =   3075
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "Completed (Not Invoiced)"
         Height          =   255
         Index           =   8
         Left            =   2460
         TabIndex        =   32
         Tag             =   "NOCLEAR"
         Top             =   1140
         Width           =   2295
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "Not Complete Rush Jobs"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   31
         Tag             =   "NOCLEAR"
         Top             =   540
         Width           =   2355
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "Not Complete"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   27
         Tag             =   "NOCLEAR"
         Top             =   240
         Value           =   -1  'True
         Width           =   1395
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "Finished (Invoiced)"
         Height          =   255
         Index           =   2
         Left            =   2460
         TabIndex        =   26
         Tag             =   "NOCLEAR"
         Top             =   1740
         Width           =   3075
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "Master Not Here"
         Height          =   255
         Index           =   5
         Left            =   2460
         TabIndex        =   25
         Tag             =   "NOCLEAR"
         Top             =   240
         Width           =   1575
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "All Items"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   24
         Tag             =   "NOCLEAR"
         Top             =   1440
         Width           =   1455
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "Pending"
         ForeColor       =   &H00000000&
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   23
         Tag             =   "NOCLEAR"
         Top             =   840
         Width           =   1395
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "Master Here"
         Height          =   255
         Index           =   3
         Left            =   2460
         TabIndex        =   22
         Tag             =   "NOCLEAR"
         Top             =   540
         Width           =   1575
      End
      Begin VB.OptionButton optComplete 
         Caption         =   "Cancelled"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   21
         Tag             =   "NOCLEAR"
         Top             =   1140
         Width           =   1575
      End
   End
   Begin VB.TextBox txtTitle 
      Height          =   315
      Left            =   1620
      TabIndex        =   17
      Top             =   120
      Width           =   2595
   End
   Begin VB.TextBox txtJobID 
      Height          =   315
      Left            =   1620
      TabIndex        =   15
      Top             =   1200
      Width           =   2595
   End
   Begin VB.CheckBox chkHideDemo 
      Alignment       =   1  'Right Justify
      Caption         =   "Hide Demo"
      Height          =   255
      Left            =   1620
      TabIndex        =   11
      Tag             =   "NOCLEAR"
      Top             =   1920
      Value           =   1  'Checked
      Width           =   1215
   End
   Begin VB.TextBox txtSubtitle 
      Height          =   315
      Left            =   1620
      TabIndex        =   9
      Top             =   480
      Width           =   2595
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   4920
      TabIndex        =   1
      Top             =   14880
      Width           =   20775
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Report"
         Height          =   315
         Left            =   14700
         TabIndex        =   33
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   18480
         TabIndex        =   4
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   17220
         TabIndex        =   3
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   15960
         TabIndex        =   2
         Top             =   0
         Width           =   1215
      End
   End
   Begin MSAdodcLib.Adodc adoItems 
      Height          =   330
      Left            =   120
      Top             =   2460
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdItems 
      Bindings        =   "frmTrackerJobDetail.frx":0000
      Height          =   8715
      Left            =   120
      TabIndex        =   0
      Top             =   2460
      Width           =   28530
      ScrollBars      =   3
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   3
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTrackerJobDetail.frx":0017
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmTrackerJobDetail.frx":0033
      stylesets(2).Name=   "headerfield"
      stylesets(2).ForeColor=   0
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmTrackerJobDetail.frx":004F
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   3
      StyleSet        =   "headerfield"
      RowHeight       =   450
      ExtraHeight     =   26
      Columns.Count   =   49
      Columns(0).Width=   1402
      Columns(0).Caption=   "Jobdetail#"
      Columns(0).Name =   "jobdetailID"
      Columns(0).DataField=   "jobdetailID"
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).StyleSet=   "headerfield"
      Columns(1).Width=   1191
      Columns(1).Caption=   "Job #"
      Columns(1).Name =   "jobID"
      Columns(1).DataField=   "jobID"
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).StyleSet=   "headerfield"
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "companyID"
      Columns(2).Name =   "companyID"
      Columns(2).DataField=   "companyID"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2778
      Columns(3).Caption=   "Contact Name"
      Columns(3).Name =   "contactname"
      Columns(3).DataField=   "contactname"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   873
      Columns(4).Caption=   "Rush"
      Columns(4).Name =   "priority"
      Columns(4).DataField=   "priority"
      Columns(4).FieldLen=   256
      Columns(4).Style=   2
      Columns(4).StyleSet=   "headerfield"
      Columns(5).Width=   529
      Columns(5).Caption=   "Ty"
      Columns(5).Name =   "copytype"
      Columns(5).DataField=   "copytype"
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(5).StyleSet=   "headerfield"
      Columns(6).Width=   450
      Columns(6).Caption=   "Qu"
      Columns(6).Name =   "quantity"
      Columns(6).DataField=   "quantity"
      Columns(6).FieldLen=   256
      Columns(6).StyleSet=   "headerfield"
      Columns(7).Width=   4948
      Columns(7).Caption=   "Description"
      Columns(7).Name =   "description"
      Columns(7).DataField=   "description"
      Columns(7).FieldLen=   256
      Columns(7).StyleSet=   "headerfield"
      Columns(8).Width=   2461
      Columns(8).Caption=   "Format"
      Columns(8).Name =   "format"
      Columns(8).DataField=   "format"
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(8).StyleSet=   "headerfield"
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "fd_orderby"
      Columns(9).Name =   "fd_orderby"
      Columns(9).DataField=   "fd_orderby"
      Columns(9).FieldLen=   256
      Columns(9).StyleSet=   "headerfield"
      Columns(10).Width=   3200
      Columns(10).Caption=   "Workflow Description"
      Columns(10).Name=   "workflowdescription"
      Columns(10).DataField=   "workflowdescription"
      Columns(10).FieldLen=   256
      Columns(10).StyleSet=   "headerfield"
      Columns(11).Width=   714
      Columns(11).Caption=   "Dur"
      Columns(11).Name=   "runningtime"
      Columns(11).DataField=   "runningtime"
      Columns(11).FieldLen=   256
      Columns(11).StyleSet=   "headerfield"
      Columns(12).Width=   2461
      Columns(12).Caption=   "Order #"
      Columns(12).Name=   "orderreference"
      Columns(12).DataField=   "orderreference"
      Columns(12).FieldLen=   256
      Columns(12).Locked=   -1  'True
      Columns(12).StyleSet=   "headerfield"
      Columns(13).Width=   2461
      Columns(13).Caption=   "BBC Prog ID"
      Columns(13).Name=   "bbcprogrammeID"
      Columns(13).DataField=   "bbcprogrammeID"
      Columns(13).FieldLen=   256
      Columns(13).StyleSet=   "headerfield"
      Columns(14).Width=   2037
      Columns(14).Caption=   "Deadline"
      Columns(14).Name=   "deadlinedate"
      Columns(14).DataField=   "deadlinedate"
      Columns(14).FieldLen=   256
      Columns(14).Locked=   -1  'True
      Columns(14).StyleSet=   "headerfield"
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "jobdetailbbcbookingnumber"
      Columns(15).Name=   "jobdetailbbcbookingnumber"
      Columns(15).DataField=   "jobdetailbbcbookingnumber"
      Columns(15).FieldLen=   256
      Columns(15).Locked=   -1  'True
      Columns(15).StyleSet=   "headerfield"
      Columns(15).PromptInclude=   -1  'True
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "jobbbcbookingnumber"
      Columns(16).Name=   "jobbbcbookingnumber"
      Columns(16).DataField=   "jobbbcbookingnumber"
      Columns(16).FieldLen=   256
      Columns(16).Locked=   -1  'True
      Columns(16).StyleSet=   "headerfield"
      Columns(16).PromptInclude=   -1  'True
      Columns(17).Width=   1138
      Columns(17).Caption=   "M. Late"
      Columns(17).Name=   "MasterIsLateFlag"
      Columns(17).DataField=   "MasterIsLateFlag"
      Columns(17).FieldLen=   256
      Columns(17).Style=   2
      Columns(17).StyleSet=   "headerfield"
      Columns(18).Width=   2117
      Columns(18).Caption=   "Special Project"
      Columns(18).Name=   "SpecialProject"
      Columns(18).DataField=   "SpecialProject"
      Columns(18).FieldLen=   256
      Columns(18).StyleSet=   "headerfield"
      Columns(19).Width=   2117
      Columns(19).Caption=   "Job Status"
      Columns(19).Name=   "jobstatus"
      Columns(19).DataField=   "jobstatus"
      Columns(19).FieldLen=   256
      Columns(19).Locked=   -1  'True
      Columns(19).StyleSet=   "headerfield"
      Columns(20).Width=   2117
      Columns(20).Caption=   "Original Barcode"
      Columns(20).Name=   "OriginalBarcode"
      Columns(20).DataField=   "OriginalBarcode"
      Columns(20).FieldLen=   256
      Columns(20).StyleSet=   "headerfield"
      Columns(21).Width=   2117
      Columns(21).Caption=   "Master Barcode"
      Columns(21).Name=   "MasterBarcode"
      Columns(21).DataField=   "MasterBarcode"
      Columns(21).FieldLen=   256
      Columns(21).StyleSet=   "headerfield"
      Columns(22).Width=   3200
      Columns(22).Visible=   0   'False
      Columns(22).Caption=   "MasterClipFilename"
      Columns(22).Name=   "MasterClipFilename"
      Columns(22).DataField=   "MasterClipFilename"
      Columns(22).FieldLen=   256
      Columns(22).StyleSet=   "headerfield"
      Columns(23).Width=   2117
      Columns(23).Caption=   "Master ClipID"
      Columns(23).Name=   "MastereventID"
      Columns(23).DataField=   "MastereventID"
      Columns(23).FieldLen=   256
      Columns(24).Width=   2117
      Columns(24).Caption=   "Target Date"
      Columns(24).Name=   "TargetDate"
      Columns(24).DataField=   "TargetDate"
      Columns(24).FieldLen=   256
      Columns(24).StyleSet=   "headerfield"
      Columns(25).Width=   873
      Columns(25).Caption=   "Days"
      Columns(25).Name=   "TargetDateCountdown"
      Columns(25).DataField=   "TargetDateCountdown"
      Columns(25).FieldLen=   256
      Columns(25).StyleSet=   "headerfield"
      Columns(26).Width=   3200
      Columns(26).Visible=   0   'False
      Columns(26).Caption=   "Completeable"
      Columns(26).Name=   "Completeable"
      Columns(26).DataField=   "Completeable"
      Columns(26).FieldLen=   256
      Columns(26).StyleSet=   "headerfield"
      Columns(27).Width=   2117
      Columns(27).Caption=   "Master Arrived"
      Columns(27).Name=   "DateMasterArrived"
      Columns(27).DataField=   "DateMasterArrived"
      Columns(27).FieldLen=   256
      Columns(27).Style=   1
      Columns(27).StyleSet=   "stagefield"
      Columns(28).Width=   2117
      Columns(28).Caption=   "location"
      Columns(28).Name=   "location"
      Columns(28).DataField=   "location"
      Columns(28).FieldLen=   256
      Columns(28).StyleSet=   "headerfield"
      Columns(29).Width=   2117
      Columns(29).Caption=   "shelf"
      Columns(29).Name=   "shelf"
      Columns(29).DataField=   "shelf"
      Columns(29).FieldLen=   256
      Columns(29).StyleSet=   "headerfield"
      Columns(30).Width=   2117
      Columns(30).Caption=   "Copy Made"
      Columns(30).Name=   "DateCopyMade"
      Columns(30).DataField=   "DateCopyMade"
      Columns(30).FieldLen=   256
      Columns(30).Style=   1
      Columns(30).StyleSet=   "stagefield"
      Columns(31).Width=   2117
      Columns(31).Caption=   "Despatch Created"
      Columns(31).Name=   "DateDelivered"
      Columns(31).DataField=   "DateDelivered"
      Columns(31).FieldLen=   256
      Columns(31).Style=   1
      Columns(31).StyleSet=   "stagefield"
      Columns(32).Width=   3200
      Columns(32).Visible=   0   'False
      Columns(32).Caption=   "Completed Date"
      Columns(32).Name=   "completeddate"
      Columns(32).DataField=   "completeddate"
      Columns(32).FieldLen=   256
      Columns(32).Style=   1
      Columns(32).StyleSet=   "stagefield"
      Columns(33).Width=   2117
      Columns(33).Caption=   "Waybill Number"
      Columns(33).Name=   "WaybillNumber"
      Columns(33).DataField=   "WaybillNumber"
      Columns(33).FieldLen=   256
      Columns(33).StyleSet=   "stagefield"
      Columns(34).Width=   2117
      Columns(34).Caption=   "Tape Sent Back"
      Columns(34).Name=   "DateTapeSentBack"
      Columns(34).DataField=   "DateTapeSentBack"
      Columns(34).FieldLen=   256
      Columns(34).Style=   1
      Columns(34).StyleSet=   "headerfield"
      Columns(35).Width=   2117
      Columns(35).Caption=   "Pending Date"
      Columns(35).Name=   "rejecteddate"
      Columns(35).DataField=   "rejecteddate"
      Columns(35).FieldLen=   256
      Columns(35).Style=   1
      Columns(35).StyleSet=   "stagefield"
      Columns(36).Width=   2117
      Columns(36).Caption=   "Off Pending"
      Columns(36).Name=   "OffRejectedDate"
      Columns(36).DataField=   "OffRejectedDate"
      Columns(36).FieldLen=   256
      Columns(36).Style=   1
      Columns(36).StyleSet=   "stagefield"
      Columns(37).Width=   3200
      Columns(37).Visible=   0   'False
      Columns(37).Caption=   "CancelledDate"
      Columns(37).Name=   "cancelleddate"
      Columns(37).DataField=   "cancelleddate"
      Columns(37).FieldLen=   256
      Columns(37).Locked=   -1  'True
      Columns(37).StyleSet=   "headerfield"
      Columns(38).Width=   3200
      Columns(38).Visible=   0   'False
      Columns(38).Caption=   "InvoicedDate"
      Columns(38).Name=   "invoiceddate"
      Columns(38).DataField=   "invoiceddate"
      Columns(38).FieldLen=   256
      Columns(38).Locked=   -1  'True
      Columns(38).StyleSet=   "headerfield"
      Columns(39).Width=   3200
      Columns(39).Visible=   0   'False
      Columns(39).Caption=   "FirstRejectedDate"
      Columns(39).Name=   "FirstRejectedDate"
      Columns(39).DataField=   "FirstRejectedDate"
      Columns(39).FieldLen=   256
      Columns(40).Width=   3200
      Columns(40).Visible=   0   'False
      Columns(40).Caption=   "DaysOnRejected"
      Columns(40).Name=   "DaysOnRejected"
      Columns(40).DataField=   "DaysOnRejected"
      Columns(40).FieldLen=   256
      Columns(41).Width=   1773
      Columns(41).Caption=   "Pending User"
      Columns(41).Name=   "RejectedUser"
      Columns(41).DataField=   "RejectedUser"
      Columns(41).FieldLen=   256
      Columns(41).Locked=   -1  'True
      Columns(41).StyleSet=   "stagefield"
      Columns(42).Width=   3200
      Columns(42).Visible=   0   'False
      Columns(42).Caption=   "DT"
      Columns(42).Name=   "DecisionTree"
      Columns(42).DataField=   "DecisionTree"
      Columns(42).FieldLen=   256
      Columns(42).Style=   2
      Columns(42).StyleSet=   "stagefield"
      Columns(43).Width=   3200
      Columns(43).Visible=   0   'False
      Columns(43).Caption=   "DecisionTreeDate"
      Columns(43).Name=   "DecisionTreeDate"
      Columns(43).DataField=   "DecisionTreeDate"
      Columns(43).FieldLen=   256
      Columns(43).Locked=   -1  'True
      Columns(43).StyleSet=   "stagefield"
      Columns(44).Width=   3200
      Columns(44).Visible=   0   'False
      Columns(44).Caption=   "OffDecisionTreeDate"
      Columns(44).Name=   "OffDecisionTreeDate"
      Columns(44).DataField=   "OffDecisionTreeDate"
      Columns(44).FieldLen=   256
      Columns(44).Locked=   -1  'True
      Columns(44).StyleSet=   "stagefield"
      Columns(45).Width=   3200
      Columns(45).Visible=   0   'False
      Columns(45).Caption=   "MostRecentDecisionTreeDate"
      Columns(45).Name=   "MostRecentDecisionTreeDate"
      Columns(45).DataField=   "MostRecentDecisionTreeDate"
      Columns(45).FieldLen=   256
      Columns(45).Locked=   -1  'True
      Columns(45).StyleSet=   "stagefield"
      Columns(46).Width=   3200
      Columns(46).Visible=   0   'False
      Columns(46).Caption=   "Days DT"
      Columns(46).Name=   "DaysOnDecisionTree"
      Columns(46).DataField=   "DaysOnDecisionTree"
      Columns(46).FieldLen=   256
      Columns(46).Locked=   -1  'True
      Columns(46).StyleSet=   "stagefield"
      Columns(47).Width=   3200
      Columns(47).Caption=   "Op. Notes"
      Columns(47).Name=   "notes2"
      Columns(47).DataField=   "notes2"
      Columns(47).FieldLen=   256
      Columns(48).Width=   979
      Columns(48).Caption=   "Faulty"
      Columns(48).Name=   "complainedabout"
      Columns(48).DataField=   "complainedabout"
      Columns(48).FieldLen=   256
      Columns(48).Style=   2
      Columns(48).StyleSet=   "conclusionfield"
      _ExtentX        =   50324
      _ExtentY        =   15372
      _StockProps     =   79
      Caption         =   "Tracker Items"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   1620
      TabIndex        =   13
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The company this job is for"
      Top             =   1560
      Width           =   2595
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   4577
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   315
      Left            =   13920
      TabIndex        =   18
      Top             =   1680
      Visible         =   0   'False
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   556
      _Version        =   393216
      Appearance      =   1
   End
   Begin MSAdodcLib.Adodc adoComments 
      Height          =   330
      Left            =   1860
      Top             =   12600
      Visible         =   0   'False
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoComments"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdComments 
      Bindings        =   "frmTrackerJobDetail.frx":006B
      Height          =   1875
      Left            =   120
      TabIndex        =   19
      Top             =   11280
      Width           =   12555
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12582847
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   6
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "trackerhistoryID"
      Columns(0).Name =   "JobDetailCommentID"
      Columns(0).DataField=   "JobDetailCommentID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "trackerprogramID"
      Columns(1).Name =   "jobdetailID"
      Columns(1).DataField=   "jobdetailID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   13070
      Columns(2).Caption=   "Comments"
      Columns(2).Name =   "comment"
      Columns(2).DataField=   "comment"
      Columns(2).FieldLen=   255
      Columns(3).Width=   3360
      Columns(3).Caption=   "Date"
      Columns(3).Name =   "cdate"
      Columns(3).DataField=   "cdate"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).Style=   1
      Columns(4).Width=   3519
      Columns(4).Caption=   "Entered By"
      Columns(4).Name =   "cuser"
      Columns(4).DataField=   "cuser"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   1164
      Columns(5).Caption=   "Email"
      Columns(5).Name =   "Email"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   4
      UseDefaults     =   0   'False
      _ExtentX        =   22146
      _ExtentY        =   3307
      _StockProps     =   79
      Caption         =   "Tracker Comments"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblCaption 
      Caption         =   "Barcode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   35
      Top             =   900
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job #"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   50
      Left            =   120
      TabIndex        =   16
      Top             =   1260
      Width           =   1455
   End
   Begin VB.Label lblSearchCompanyID 
      Height          =   315
      Left            =   3240
      TabIndex        =   14
      Top             =   14820
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblLastTrackerItemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   1260
      TabIndex        =   12
      Top             =   14820
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   10
      Left            =   120
      TabIndex        =   10
      Top             =   1620
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order Number"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   8
      Top             =   540
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   7
      Top             =   180
      Width           =   1395
   End
   Begin VB.Label lblTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   180
      TabIndex        =   6
      Top             =   14820
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCompanyID 
      Height          =   315
      Left            =   2340
      TabIndex        =   5
      Top             =   14820
      Visible         =   0   'False
      Width           =   735
   End
End
Attribute VB_Name = "frmTrackerJobDetail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strSearch As String
Dim msp_strSearch As String
Dim msp_strCrystal As String
Dim m_strOrderby As String
Dim m_blnDelete As Boolean
Dim m_blnDontVerifyXML As Boolean
Dim m_blnBilling As Boolean
Dim m_blnSilent As Boolean
Dim l_strDateSearch As String
'Things for the Billing

Dim m_blnBillAll As Boolean
Dim m_strLastUniqueID As String
Dim m_strUniqueID As String
Dim m_strLastComponent As String
Dim m_datLastCompleteDate As Date
Dim m_lngAudioValidateCount As Long
Dim m_lngAudioConformCount As Long
Dim m_lngSubsValidateCount As Long
Dim m_lngSubsConformCount As Long
Dim m_strLastProjectManager As String, m_lngLastProjectNumber As Long, m_strLastSeries As String, m_strLastEpisodeNo As String, m_strLastEpisodeTitle As String, m_strLastTitle As String, m_strLastRightsOwner As String

Private Sub Hide_All_Fields()

grdItems.Columns("contactname").Visible = False
grdItems.Columns("format").Visible = False
grdItems.Columns("originalBarcode").Visible = False
grdItems.Columns("jobstatus").Visible = False
grdItems.Columns("DateTapeSentBack").Visible = False

End Sub

Private Sub cmdSomeFields_Click()

grdItems.Columns("contactname").Visible = False
grdItems.Columns("format").Visible = False
grdItems.Columns("originalBarcode").Visible = False
grdItems.Columns("jobstatus").Visible = False
grdItems.Columns("DateTapeSentBack").Visible = False

End Sub

Private Sub cmdAllFields_Click()

grdItems.Columns("contactname").Visible = True
grdItems.Columns("format").Visible = True
grdItems.Columns("originalBarcode").Visible = True
grdItems.Columns("jobstatus").Visible = True
grdItems.Columns("DateTapeSentBack").Visible = True

End Sub

Private Sub chkHideDemo_Click()

Dim l_strSQL As String

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/WWtracker%' and companyID > 100 AND system_active = 1 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/WWtracker%' AND system_active = 1 ORDER BY name;"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

cmdSearch.Value = True

End Sub

Private Sub cmbCompany_Click()

lblSearchCompanyID.Caption = cmbCompany.Columns("companyID").Text
m_strSearch = " WHERE companyID = " & lblSearchCompanyID.Caption

DoEvents

Dim l_rstChoices As ADODB.Recordset, l_blnWide As Boolean, l_strSQL As String

If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
    l_blnWide = True
Else
    l_blnWide = False
End If

'Dim l_conSearch As ADODB.Connection
'Dim l_rstSearch1 As ADODB.Recordset
'
'Set l_conSearch = New ADODB.Connection
'Set l_rstSearch1 = New ADODB.Recordset
'
'l_conSearch.ConnectionString = g_strConnection
'l_conSearch.Open
'
'l_strSQL = "SELECT name, contactID FROM contact WHERE contactID IN (SELECT contactID FROM employee WHERE companyID = " & Val(lblSearchCompanyID.Caption) & ") ORDER BY name;"
'
'With l_rstSearch1
'     .CursorLocation = adUseClient
'     .LockType = adLockBatchOptimistic
'     .CursorType = adOpenDynamic
'     .Open l_strSQL, l_conSearch, adOpenDynamic
'End With
'
'Set ddnProjectManager.DataSource = l_rstSearch1
'
'l_rstSearch1.ActiveConnection = Nothing
'
'grdItems.Columns("projectmanager").DropDownHwnd = ddnProjectManager.hWnd
'grdItems.Columns("projectmanager").Locked = False
'
'l_conSearch.Close
'Set l_conSearch = Nothing
'
cmdSearch.Value = True

End Sub

Private Sub cmdClear_Click()

ClearFields Me
cmbCompany.Text = ""
lblCompanyID.Caption = ""
lblSearchCompanyID.Caption = ""
m_strSearch = " WHERE 1 = 1 "
cmdSearch.Value = True

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdPrint_Click()

Dim l_strSelectionFormula As String
Dim l_strReportFile As String
    
l_strReportFile = g_strLocationOfCrystalReportFiles & "BBCWW_Tracker_report.rpt"

Debug.Print msp_strCrystal

PrintCrystalReport l_strReportFile, msp_strCrystal, True
    
End Sub

Private Sub cmdSearch_Click()

'Dim l_strDateSearch As String

If Val(lblSearchCompanyID.Caption) = 0 Then Exit Sub

l_strDateSearch = ""
m_blnSilent = True
adoItems.ConnectionString = g_strConnection
SearchSQLstr
adoItems.RecordSource = "SELECT * FROM vw_Tracker_BBCWW " & m_strSearch & l_strDateSearch & msp_strSearch & m_strOrderby & ";"
Debug.Print adoItems.RecordSource

adoItems.Refresh

adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"
m_blnSilent = False

If Not adoItems.Recordset.EOF Then
    adoItems.Recordset.MoveLast
    adoItems.Recordset.MoveFirst
End If

End Sub

Sub SearchSQLstr()

msp_strSearch = ""
msp_strCrystal = ""

If chkHideDemo.Value <> 0 Then
    msp_strSearch = msp_strSearch & " AND companyID > 100 "
    msp_strCrystal = msp_strCrystal & "{vw_Tracker_BBCWW.companyID} > 100"
End If

If optComplete(0).Value = True Then 'Not Complete
    msp_strSearch = msp_strSearch & " AND cancelleddate IS NULL AND invoiceddate IS NULL AND completeddate IS NULL AND jobstatus NOT IN ('Pencil', 'Submitted', 'Completed', 'Hold Cost', 'Costed', 'Sent To Accounts', 'No Charge') "
    msp_strCrystal = msp_strCrystal & " AND isnull({vw_Tracker_BBCWW.cancelleddate}) AND isnull({vw_Tracker_BBCWW.invoiceddate}) AND isnull({vw_Tracker_BBCWW.completeddate}) AND {vw_Tracker_BBCWW.jobstatus} <> ""Pencil"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Submitted"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Completed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Hold Cost"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Costed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Sent To Accounts"" AND {vw_Tracker_BBCWW.jobstatus} <> ""No Charge"" "
ElseIf optComplete(1).Value = True Then 'Pending
    msp_strSearch = msp_strSearch & " AND cancelleddate IS NULL AND invoiceddate IS NULL AND completeddate IS NULL AND rejecteddate IS NOT NULL AND jobstatus NOT IN ('Pencil', 'Submitted', 'Completed', 'Hold Cost', 'Costed', 'Sent To Accounts', 'No Charge')"
    msp_strCrystal = msp_strCrystal & " AND isnull({vw_Tracker_BBCWW.cancelleddate}) AND isnull({vw_Tracker_BBCWW.invoiceddate}) AND isnull({vw_Tracker_BBCWW.completeddate}) AND not(isnull({vw_Tracker_BBCWW.rejecteddate})) AND {vw_Tracker_BBCWW.jobstatus} <> ""Pencil"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Submitted"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Completed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Hold Cost"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Costed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Sent To Accounts"" AND {vw_Tracker_BBCWW.jobstatus} <> ""No Charge"" "
ElseIf optComplete(2).Value = True Then 'Finished (incl Invoiced)
    msp_strSearch = msp_strSearch & " AND cancelleddate IS NULL AND completeddate IS NOT NULL AND jobstatus NOT IN ('Pencil', 'Submitted', 'Confirmed', 'Rejected', 'Masters Here', 'In Progress', 'On Hold', 'VT Done', 'Completed', 'Hold Cost', 'No Charge')"
    msp_strCrystal = msp_strCrystal & "AND isnull({vw_Tracker_BBCWW.cancelleddate}) AND not(isnull({vw_Tracker_BBCWW.completeddate})) AND {vw_Tracker_BBCWW.jobstatus} <> ""Pencil"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Submitted"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Confirmed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Rejected"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Masters Here"" AND {vw_Tracker_BBCWW.jobstatus} <> ""In Progress"" AND {vw_Tracker_BBCWW.jobstatus} <> ""On Hold"" AND {vw_Tracker_BBCWW.jobstatus} <> ""VT Done"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Completed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Hold Cost"" "
ElseIf optComplete(3).Value = True Then 'Master Arrived
    msp_strSearch = msp_strSearch & "AND cancelleddate IS NULL AND invoiceddate IS NULL AND completeddate IS NULL AND datemasterarrived IS NOT NULL AND DateTapeSentBack IS NULL AND jobstatus NOT IN ('Pencil', 'Submitted', 'Completed', 'Hold Cost', 'Costed', 'Sent To Accounts', 'No Charge')"
    msp_strCrystal = msp_strCrystal & "AND isnull({vw_Tracker_BBCWW.cancelleddate}) AND isnull({vw_Tracker_BBCWW.invoiceddate}) AND isnull({vw_Tracker_BBCWW.completeddate}) AND not(isnull({vw_Tracker_BBCWW.datemasterarrived})) AND isnull({vw_Tracker_BBCWW.DateTapeSentBack}) AND {vw_Tracker_BBCWW.jobstatus} <> ""Pencil"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Submitted"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Completed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Hold Cost"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Costed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Sent To Accounts"" AND {vw_Tracker_BBCWW.jobstatus} <> ""No Charge"" "
ElseIf optComplete(4).Value = True Then 'All items
    msp_strSearch = msp_strSearch & "AND 1=1 "
    msp_strCrystal = msp_strCrystal & "AND 1=1 "
ElseIf optComplete(5).Value = True Then 'Master Not Here
    msp_strSearch = msp_strSearch & " AND cancelleddate IS NULL AND invoiceddate IS NULL AND completeddate IS NULL AND (datemasterarrived IS NULL OR (datemasterarrived IS NOT NULL AND DateTapeSentBack IS NOT NULL)) AND jobstatus NOT IN ('Pencil', 'Submitted', 'Completed', 'Hold Cost', 'Costed', 'Sent To Accounts', 'No Charge') "
    msp_strCrystal = msp_strCrystal & "AND isnull({vw_Tracker_BBCWW.cancelleddate}) AND isnull({vw_Tracker_BBCWW.invoiceddate}) AND isnull({vw_Tracker_BBCWW.completeddate}) AND (isnull({vw_Tracker_BBCWW.datemasterarrived}) OR (not(isnull({vw_Tracker_BBCWW.datemasterarrived})) AND not(isnull({vw_Tracker_BBCWW.DateTapeSentBack})))) AND {vw_Tracker_BBCWW.jobstatus} <> ""Pencil"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Submitted"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Completed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Hold Cost"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Costed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Sent To Accounts"" AND {vw_Tracker_BBCWW.jobstatus} <> ""No Charge"" "
ElseIf optComplete(6).Value = True Then 'Cancelled
    msp_strSearch = msp_strSearch & " AND (cancelleddate IS NOT NULL) "
    msp_strCrystal = msp_strCrystal & "AND (not(isnull({vw_Tracker_BBCWW.cancelleddate}))) "
ElseIf optComplete(7).Value = True Then 'Not Complete Rush Jobs
    msp_strSearch = msp_strSearch & " AND cancelleddate IS NULL AND invoiceddate IS NULL AND completeddate IS NULL AND Priority <> 0 AND jobstatus NOT IN ('Pencil', 'Submitted', 'Completed', 'Hold Cost', 'Costed', 'Sent To Accounts', 'No Charge') "
    msp_strCrystal = msp_strCrystal & " AND isnull({vw_Tracker_BBCWW.cancelleddate}) AND isnull({vw_Tracker_BBCWW.invoiceddate}) AND isnull({vw_Tracker_BBCWW.completeddate}) AND {vw_Tracker_BBCWW.Priority} <> 0 AND {vw_Tracker_BBCWW.jobstatus} <> ""Pencil"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Submitted"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Completed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Hold Cost"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Costed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Sent To Accounts"" AND {vw_Tracker_BBCWW.jobstatus} <> ""No Charge"" "
ElseIf optComplete(8).Value = True Then 'Completed, Not Invoiced
    msp_strSearch = msp_strSearch & " AND cancelleddate IS NULL AND invoiceddate IS NULL AND jobstatus <> 'Pencil' AND completeddate IS NOT NULL AND jobstatus NOT IN ('Submitted', 'Confirmed', 'Rejected', 'Masters Here', 'In Progress', 'VT Done', 'On Hold', 'Costed', 'Sent To Accounts', 'No Charge') "
    msp_strCrystal = msp_strCrystal & " AND isnull({vw_Tracker_BBCWW.cancelleddate}) AND isnull({vw_Tracker_BBCWW.invoiceddate}) AND {vw_Tracker_BBCWW.jobstatus} <> 'Pencil' AND not(isnull({vw_Tracker_BBCWW.completeddate})) AND ({vw_Tracker_BBCWW.jobstatus} <> ""Submitted"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Confirmed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Rejected"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Masters Here"" AND {vw_Tracker_BBCWW.jobstatus} <> ""In Progress"" AND {vw_Tracker_BBCWW.jobstatus} <> ""VT Done"" AND {vw_Tracker_BBCWW.jobstatus} <> ""On Hold"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Costed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Sent To Accounts"" AND {vw_Tracker_BBCWW.jobstatus} <> ""No Charge"")"
ElseIf optComplete(9).Value = True Then 'Finished (No Charge)
    msp_strSearch = msp_strSearch & " AND cancelleddate IS NULL AND completeddate IS NOT NULL AND jobstatus NOT IN ('Pencil', 'Submitted', 'Confirmed', 'Rejected', 'Masters Here', 'In Progress', 'On Hold', 'VT Done', 'Completed', 'Hold Cost', 'Costed', 'Sent To Accounts')"
    msp_strCrystal = msp_strCrystal & "AND isnull({vw_Tracker_BBCWW.cancelleddate}) AND not(isnull({vw_Tracker_BBCWW.completeddate})) AND {vw_Tracker_BBCWW.jobstatus} <> ""Pencil"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Submitted"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Confirmed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Rejected"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Masters Here"" AND {vw_Tracker_BBCWW.jobstatus} <> ""In Progress"" AND {vw_Tracker_BBCWW.jobstatus} <> ""On Hold"" AND {vw_Tracker_BBCWW.jobstatus} <> ""VT Done"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Completed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Hold Cost"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Costed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Sent To Accounts"" "
ElseIf optComplete(10).Value = True Then 'Finished (Not Despatched)
    msp_strSearch = msp_strSearch & " AND cancelleddate IS NULL AND datecopymade IS NOT NULL AND datedelivered IS NULL AND jobstatus NOT IN ('Pencil', 'Submitted', 'Hold Cost', 'Costed', 'Sent To Accounts')"
    msp_strCrystal = msp_strCrystal & "AND isnull({vw_Tracker_BBCWW.cancelleddate}) AND not(isnull({vw_Tracker_BBCWW.datecopymade})) AND isnull({vw_Tracker_BBCWW.datedelivered}) AND {vw_Tracker_BBCWW.jobstatus} <> ""Pencil"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Submitted"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Confirmed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Rejected"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Masters Here"" AND {vw_Tracker_BBCWW.jobstatus} <> ""In Progress"" AND {vw_Tracker_BBCWW.jobstatus} <> ""On Hold"" AND {vw_Tracker_BBCWW.jobstatus} <> ""VT Done"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Completed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Hold Cost"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Costed"" AND {vw_Tracker_BBCWW.jobstatus} <> ""Sent To Accounts"" "
End If

If txtTitle.Text <> "" Then
    msp_strSearch = msp_strSearch & " AND Description like '" & QuoteSanitise(txtTitle.Text) & "%' "
    msp_strCrystal = msp_strCrystal & " AND {vw_Tracker_BBCWW.Description} like '" & QuoteSanitise(txtTitle.Text) & "*' "
End If
If txtSubtitle.Text <> "" Then
    msp_strSearch = msp_strSearch & " AND orderreference LIKE '" & QuoteSanitise(txtSubtitle.Text) & "%' "
    msp_strCrystal = msp_strCrystal & " AND {vw_Tracker_BBCWW.orderreference} LIKE '" & QuoteSanitise(txtSubtitle.Text) & "*' "
End If
If txtJobID.Text <> "" Then
    msp_strSearch = msp_strSearch & " AND jobID = " & Val(txtJobID.Text) & " "
    msp_strCrystal = msp_strCrystal & " AND {vw_Tracker_BBCWW.jobID} = " & Val(txtJobID.Text) & " "
End If
If txtBarcode.Text <> "" Then
    msp_strSearch = msp_strSearch & " AND MasterBarcode Like '" & txtBarcode.Text & "%' "
    msp_strCrystal = msp_strCrystal & " AND {vw_Tracker_BBCWW.MasterBarcode} = " & Val(txtJobID.Text) & " "
End If

If Val(lblSearchCompanyID.Caption) <> 0 Then
    msp_strSearch = msp_strSearch & " AND companyID = " & Val(lblSearchCompanyID.Caption) & " "
    msp_strCrystal = msp_strCrystal & " AND {vw_Tracker_BBCWW.companyID} = " & Val(lblSearchCompanyID.Caption) & " "
End If

End Sub

Private Sub Form_Activate()

cmdSomeFields.Value = True

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

Dim l_blnWide As Boolean

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch1 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch1 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/WWtracker%' and companyID > 100 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/WWtracker%' ORDER BY name;"
End If

With l_rstSearch1
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch1.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch1

grdItems.StyleSets("headerfield").BackColor = &HE7FFE7
grdItems.StyleSets("stagefield").BackColor = &HE7FFFF
grdItems.StyleSets("conclusionfield").BackColor = &HFFFFE7

grdItems.StyleSets.Add "Error"
grdItems.StyleSets("Error").BackColor = &HA0A0FF

grdItems.StyleSets.Add "NotRequired"
grdItems.StyleSets("NotRequired").BackColor = &H10101

grdItems.StyleSets.Add "Priority"
grdItems.StyleSets("Priority").BackColor = &H70B0FF

grdItems.StyleSets.Add "Fixed"
grdItems.StyleSets("Fixed").BackColor = &HA0FFA0
grdItems.StyleSets.Add "Notify"
grdItems.StyleSets("Notify").BackColor = &HFFFF40

grdItems.StyleSets.Add "Internal"
grdItems.StyleSets("Internal").BackColor = &HCCCC99

optComplete(0).Value = True

m_strSearch = " WHERE 1 = 1 "
m_strOrderby = " ORDER BY CASE WHEN priority <> 0 THEN 0 ELSE 1 END, CASE WHEN targetdate IS NULL THEN 1 ELSE 0 END, cast (targetdate as date), cast (deadlinedate as date), jobid, fd_orderby"
m_blnSilent = False

Me.Visible = True
Me.ZOrder 0
DoEvents
'DoEvents

'MsgBox "Bing"
'
'DoEvents
'DoEvents

Hide_All_Fields

End Sub

Private Sub Form_Resize()

On Error Resume Next

grdItems.Width = Me.ScaleWidth - grdItems.Left - 120
grdItems.Height = (Me.ScaleHeight - grdItems.Top - frmButtons.Height) * 0.75 - 240
grdComments.Top = grdItems.Top + grdItems.Height + 120
grdComments.Height = (Me.ScaleHeight - grdComments.Top - frmButtons.Height) - 240
frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120

End Sub

Private Sub grdComments_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdComments_AfterUpdate(RtnDispErrMsg As Integer)

If m_blnDelete = False Then
    adoComments.Refresh
End If

End Sub

Private Sub grdComments_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdComments_BeforeUpdate(Cancel As Integer)

If m_blnDelete = False Then

    If Val(lblTrackeritemID.Caption) = 0 Then
        Cancel = 1
        Exit Sub
    End If
    
    If grdComments.Columns("comment").Text = "" Then
        MsgBox "Cannot save a Comment with no actual comment", vbCritical, "Comment Not Saved"
        Cancel = True
        Exit Sub
    End If
           
    grdComments.Columns("jobdetailID").Text = lblTrackeritemID.Caption
    grdComments.Columns("cuser").Text = g_strFullUserName
    grdComments.Columns("cdate").Text = Format(Now, "yyyy-mm-dd")
    
End If

End Sub

Private Sub grdComments_BtnClick()

Dim l_strOurEmailContact As String, l_strOurContactName As String, l_strEmailBody As String

Dim l_rstWhoToEmail As ADODB.Recordset

If MsgBox("Send Email?", vbYesNo, "Automatic Email") = vbYes Then
    
    l_strEmailBody = "A BBCWW Item was put on pending " & _
        "By: " & grdComments.Columns("cuser").Text & vbCrLf & _
        "Job # : " & grdItems.Columns("jobID").Text & ", Tape: " & grdItems.Columns("MasterBarcode").Text & vbCrLf & _
        "Title: " & grdItems.Columns("descripiton").Text & vbCrLf & _
        "Comment: " & grdComments.Columns("comment").Text & vbCrLf
    
    Debug.Print l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND trackermessageID = 56;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
    
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "BBCWW Tracker Item Comment Entered", "", l_strEmailBody, True, "", ""
            l_rstWhoToEmail.MoveNext
        
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
    
End If

End Sub

Private Sub grdItems_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdItems_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdItems_BeforeUpdate(Cancel As Integer)

If m_blnDelete = True Then Exit Sub

Dim l_rst As ADODB.Recordset, l_datDate As Date, l_datDate2 As Date, l_strRejectionReason As String
Dim l_strEmailBody As String
Dim l_rstWhoToEmail As ADODB.Recordset


If Val(lblSearchCompanyID.Caption) = 0 Then
    MsgBox "Please select an MX1 Client." & vbCrLf & "Row not saved", vbCritical, "Error..."
    Cancel = 1
    Exit Sub
End If

If grdItems.Columns("companyID").Text = "" Then grdItems.Columns("companyID").Text = lblSearchCompanyID.Caption

Dim temp As Boolean, Count As Long, l_strDuration As String, l_strFrameRate As String, l_lngRunningTime As Long, l_curFileSize As Currency, l_strFilename As String
Dim l_lngClipID As Long, l_strNetworkPath As String, l_strSQL As String, l_lngProjectManagerContactID As Long, l_datNewTargetDate As Date, l_datNewTargetDate2 As Date

'Check Completedness of whole job...
If grdItems.Columns("completeddate").Text <> "" Then
    If GetCount("SELECT count(jobdetailID) FROM jobdetail WHERE jobID = " & grdItems.Columns("jobID").Text & " AND completeddate IS NOT NULL") <= 0 Then
        If GetStatusNumber(grdItems.Columns("jobstatus").Text) < GetStatusNumber("VT Done") Then grdItems.Columns("jobstatus").Text = "VT Done"
    End If
End If

'Check Pending Status
If grdItems.Columns("rejecteddate").Text <> "" And IsNull(adoItems.Recordset("RejectedDate")) Then
    If grdItems.Columns("FirstRejectedDate").Text = "" Then grdItems.Columns("FirstRejectedDate").Text = Format(grdItems.Columns("rejecteddate").Text, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("RejectedUser").Text = g_strUserInitials
    grdItems.Columns("targetdate").Text = ""
    grdItems.Columns("offrejecteddate").Text = ""
    Do While l_strRejectionReason = ""
        l_strRejectionReason = InputBox("Please give the Pending Reason")
        If l_strRejectionReason = "" Then MsgBox "You must provide the pending reason"
    Loop
    l_strSQL = "INSERT INTO jobdetailcomment (jobdetailID, comment, cdate, cuser) VALUES ("
    l_strSQL = l_strSQL & Val(grdItems.Columns("jobdetailID").Text) & ", "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strRejectionReason) & "', "
    l_strSQL = l_strSQL & "'" & Format(Now, "yyyy-mm-dd hh:nn:ss") & "', "
    l_strSQL = l_strSQL & "'" & g_strUserInitials & "')"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_strEmailBody = "A BBCWW Item was put on pending " & _
        "By: " & grdComments.Columns("cuser").Text & vbCrLf & _
        "Job # : " & grdItems.Columns("jobID").Text & ", Tape: " & grdItems.Columns("MasterBarcode").Text & vbCrLf & _
        "Title: " & grdItems.Columns("description").Text & vbCrLf & _
        "Comment: " & QuoteSanitise(l_strRejectionReason) & vbCrLf
    
    Debug.Print l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND trackermessageID = 50;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
    
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "BBCWW Tracker Item put on Pending", "", l_strEmailBody, True, "", ""
            l_rstWhoToEmail.MoveNext
        
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
ElseIf grdItems.Columns("rejecteddate").Text = "" And Not IsNull(adoItems.Recordset("rejecteddate")) Then
    grdItems.Columns("offRejectedDate").Text = Now
    'Need to put something in here to use working day calculations of how long it was on pending, not raw days like is is now.
    grdItems.Columns("daysonRejected").Text = Val(grdItems.Columns("daysonRejected").Text) + DateDiff("d", CDate(adoItems.Recordset("rejecteddate")), CDate(grdItems.Columns("offRejectedDate").Text) + 1)
    grdItems.Columns("RejectedUser").Text = ""
    If grdItems.Columns("DateMasterArrived").Text <> "" And grdItems.Columns("DateTapeSentBack").Text = "" Then
        Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & GetData("jobdetail", "TargetDateCountdown", "jobdetailID", Val(grdItems.Columns("jobdetailID").Text)), g_strExecuteError)
        grdItems.Columns("Targetdate").Text = l_rst(0)
        l_rst.Close
        Set l_rst = Nothing
    End If
    Do While l_strRejectionReason = ""
        l_strRejectionReason = InputBox("Please issue a comment about this item coming off pending")
        If l_strRejectionReason = "" Then MsgBox "You must provide a comment about this item coming off pending"
    Loop
    l_strSQL = "INSERT INTO jobdetailcomment (jobdetailID, comment, cdate, cuser) VALUES ("
    l_strSQL = l_strSQL & Val(grdItems.Columns("jobdetailID").Text) & ", "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strRejectionReason) & "', "
    l_strSQL = l_strSQL & "'" & Format(Now, "yyyy-mm-dd hh:nn:ss") & "', "
    l_strSQL = l_strSQL & "'" & g_strUserInitials & "')"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_strEmailBody = "A BBCWW Item was brought off pending " & _
        "By: " & grdComments.Columns("cuser").Text & vbCrLf & _
        "Job # : " & grdItems.Columns("jobID").Text & ", Tape: " & grdItems.Columns("MasterBarcode").Text & vbCrLf & _
        "Title: " & grdItems.Columns("description").Text & vbCrLf & _
        "Comment: " & QuoteSanitise(l_strRejectionReason) & vbCrLf
    
    Debug.Print l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND trackermessageID = 50;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
    
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "BBCWW Tracker Item brought off Pending", "", l_strEmailBody, True, "", ""
            l_rstWhoToEmail.MoveNext
        
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

'Check the Decisiontree situation
If Val(grdItems.Columns("decisiontree").Text) <> 0 And (IsNull(grdItems.Columns("decisiontreedate").Text) Or grdItems.Columns("decisiontreedate").Text = "") Then
    l_datDate = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("decisiontreedate").Text = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("MostRecentDecisionTreeDate").Text = Format(l_datDate, "yyyy-mm-dd hh:nn:ss")

ElseIf Val(grdItems.Columns("decisiontree").Text) <> 0 And adoItems.Recordset("decisiontree") = 0 Then
     
    grdItems.Columns("MostRecentDecisionTreeDate").Text = Format(Now, "yyyy-mm-dd hh:nn:ss")
    
ElseIf Val(grdItems.Columns("decisiontree").Text) = 0 And adoItems.Recordset("decisiontree") <> 0 Then
    l_datDate = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("OffDecisionTreeDate").Text = l_datDate
    'Need to put something in here to use working day calculations of how long it was on decisiontree, not raw days like is is now.
    grdItems.Columns("daysondecisiontree").Text = Val(grdItems.Columns("daysondecisiontree").Text) + _
    DateDiff("d", FormatDateTime(grdItems.Columns("mostrecentdecisiontreedate").Text, vbShortDate), CDate(grdItems.Columns("OffDecisionTreeDate").Text) + 1)

End If

'Check if priority status changes...
If grdItems.Columns("priority").Text <> 0 And adoItems.Recordset("priority") = 0 Then
    If Val(grdItems.Columns("targetdatecountdown").Text) > 1 Then
        Count = 1 - Val(grdItems.Columns("targetdatecountdown").Text)
        grdItems.Columns("targetdatecountdown").Text = Val(grdItems.Columns("targetdatecountdown").Text) + Count
        If grdItems.Columns("TargetDate").Text <> "" Then
            Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Val(grdItems.Columns("targetdatecountdown").Text), g_strExecuteError)
            l_datNewTargetDate = l_rst(0)
            l_rst.Close
            grdItems.Columns("TargetDate").Text = l_datNewTargetDate
        End If
    End If
ElseIf grdItems.Columns("priority").Text = 0 And adoItems.Recordset("priority") <> 0 Then
    grdItems.Columns("targetdatecountdown").Text = Val(grdItems.Columns("targetdatecountdown").Text) + 2
    If Val(grdItems.Columns("targetdatecountdown").Text) > 3 Then grdItems.Columns("targetdatecountdown").Text = 3
    If grdItems.Columns("TargetDate").Text <> "" Then
        Count = Val(grdItems.Columns("targetdatecountdown").Text) - Val(adoItems.Recordset("targetdatecountdown"))
        Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(grdItems.Columns("TargetDate").Text, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & Count, g_strExecuteError)
        l_datNewTargetDate = l_rst(0)
        l_rst.Close
        If IsDate(grdItems.Columns("deadlinedate").Text) Then l_datNewTargetDate2 = DateAdd("h", -12, grdItems.Columns("deadlinedate").Text)
        If l_datNewTargetDate2 > l_datNewTargetDate Then l_datNewTargetDate = l_datNewTargetDate2
        grdItems.Columns("TargetDate").Text = l_datNewTargetDate
    End If
End If

'Check Lateflag
If IsDate(grdItems.Columns("deadlinedate").Text) And IsDate(grdItems.Columns("targetdate").Text) Then
    l_datDate = grdItems.Columns("deadlinedate").Text
    l_datDate2 = grdItems.Columns("targetdate").Text
    If l_datDate2 > l_datDate Then
        grdItems.Columns("MasterIsLateflag").Text = 1
    Else
        grdItems.Columns("MasterIsLateflag").Text = 0
    End If
End If

'Check the Complaint situation
If Val(grdItems.Columns("complainedabout").Text) <> 0 And adoItems.Recordset("complainedabout") = 0 Then
    l_strSQL = "INSERT INTO complaint (companyID, contactID, complainttype, originaljobdetailID, title, originaljobID, orderreference, ordercontactID, originaloperator, jobcompleteddate, cdate, cuser, mdate, muser) VALUES ("
    l_strSQL = l_strSQL & lblCompanyID.Caption & ", 2840, 5, "
    l_strSQL = l_strSQL & grdItems.Columns("jobdetailID").Text & ", "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("description").Text) & "', "
    l_strSQL = l_strSQL & grdItems.Columns("jobID").Text & ", "
    l_strSQL = l_strSQL & "'" & grdItems.Columns("orderreference").Text & "', "
    l_strSQL = l_strSQL & GetData("job", "contactID", "jobID", grdItems.Columns("jobID").Text) & ", "
    l_strSQL = l_strSQL & "'" & GetData("jobdetail", "completeduser", "jobdetailID", grdItems.Columns("jobdetailID").Text) & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(GetData("jobdetail", "completeddate", "jobdetailID", grdItems.Columns("jobdetailID").Text)) & "', "
    l_strSQL = l_strSQL & "getdate(), "
    l_strSQL = l_strSQL & "'" & g_strFullUserName & "', "
    l_strSQL = l_strSQL & "getdate(), "
    l_strSQL = l_strSQL & "'" & g_strFullUserName & "');"
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
End If

End Sub

Private Sub grdItems_BtnClick()

Dim tempdate As String

Select Case LCase(grdItems.Columns(grdItems.Col).Name)

Case Else
    If grdItems.ActiveCell.Text <> "" Then
        grdItems.ActiveCell.Text = ""
    Else
        If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
            grdItems.ActiveCell.Text = Now
        Else
            tempdate = FormatDateTime(Now, vbLongDate)
            grdItems.ActiveCell.Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
        End If
    End If

End Select

End Sub

Private Sub grdItems_DblClick()

ShowJob Val(grdItems.Columns("jobID").Text), 1, True
'ShowJob Val(grdItems.Columns("jobID").Text), 1, True

End Sub

Private Sub grdItems_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim l_strSQL As String

If m_blnSilent = False Then
    lblTrackeritemID.Caption = grdItems.Columns("jobdetailID").Text
    lblCompanyID.Caption = grdItems.Columns("companyID").Text
    
    If Val(lblTrackeritemID.Caption) <> Val(lblLastTrackeritemID.Caption) Then
        
        If Val(lblTrackeritemID.Caption) = 0 Then
            
            l_strSQL = "SELECT * FROM JobDetailComment WHERE jobdetailID = -1 ORDER BY cdate;"
            
            adoComments.RecordSource = l_strSQL
            adoComments.ConnectionString = g_strConnection
            adoComments.Refresh
            
            lblLastTrackeritemID.Caption = ""
        
            Exit Sub
        
        End If
        
        l_strSQL = "SELECT * FROM JobDetailComment WHERE jobdetailID = " & lblTrackeritemID.Caption & " ORDER BY cdate;"
        
        adoComments.RecordSource = l_strSQL
        adoComments.ConnectionString = g_strConnection
        adoComments.Refresh
    
        lblLastTrackeritemID.Caption = lblTrackeritemID.Caption
        
    End If

End If

End Sub

Private Sub grdItems_RowLoaded(ByVal Bookmark As Variant)

If grdItems.Columns("rejecteddate").Text <> "" Then
    grdItems.Columns("rejecteddate").CellStyleSet "Error"
    grdItems.Columns("rejecteduser").CellStyleSet "Error"
    grdItems.Columns("description").CellStyleSet "Error"
End If

If grdItems.Columns("MasterIsLateFlag").Text <> 0 Then
    grdItems.Columns("MasterIsLateFlag").CellStyleSet "Error"
End If

grdItems.Columns("location").Text = GetData("library", "location", "barcode", grdItems.Columns("masterBarcode").Text)
grdItems.Columns("shelf").Text = GetData("library", "shelf", "barcode", grdItems.Columns("masterBarcode").Text)

End Sub

Private Sub optComplete_Click(Index As Integer)

cmdSearch.Value = True

End Sub

Private Sub optSorting_Click(Index As Integer)

If optSorting(0).Value = True Then
    m_strOrderby = " ORDER BY deadlinedate, jobid, fd_orderby"
Else
    m_strOrderby = " ORDER BY CASE WHEN priority <> 0 THEN 0 ELSE 1 END, CASE WHEN targetdate IS NULL THEN 1 ELSE 0 END, cast (targetdate as date), cast (deadlinedate as date), jobid, fd_orderby"
End If

cmdSearch.Value = True

End Sub
