VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmTrackerHDD 
   Caption         =   "HDD Tracker"
   ClientHeight    =   12975
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   27045
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   12975
   ScaleWidth      =   27045
   WindowState     =   2  'Maximized
   Begin VB.ComboBox txtSeriesTitle 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6960
      TabIndex        =   37
      ToolTipText     =   "What Copy Type is the tape?"
      Top             =   780
      Width           =   3135
   End
   Begin VB.ComboBox cmbComponentsAvailable 
      Height          =   315
      ItemData        =   "frmTrackerHDD.frx":0000
      Left            =   12240
      List            =   "frmTrackerHDD.frx":000A
      TabIndex        =   33
      Top             =   420
      Width           =   975
   End
   Begin VB.CheckBox chkHideDemo 
      Alignment       =   1  'Right Justify
      Caption         =   "Hide Demo"
      Height          =   255
      Left            =   120
      TabIndex        =   20
      Tag             =   "NOCLEAR"
      Top             =   480
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   7620
      TabIndex        =   9
      Top             =   12420
      Width           =   17835
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   12780
         TabIndex        =   18
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   14040
         TabIndex        =   17
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   15300
         TabIndex        =   16
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   16560
         TabIndex        =   15
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdBillItem 
         Caption         =   "Bill Item"
         Height          =   315
         Left            =   10920
         TabIndex        =   14
         Top             =   0
         Visible         =   0   'False
         Width           =   1755
      End
      Begin VB.CommandButton cmdManualBillItem 
         Caption         =   "Manually Mark as Billed"
         Height          =   315
         Left            =   8580
         TabIndex        =   13
         Top             =   0
         Visible         =   0   'False
         Width           =   2235
      End
      Begin VB.CommandButton cmdUnbill 
         Caption         =   "Unmark as Billed"
         Height          =   315
         Left            =   6180
         TabIndex        =   12
         Top             =   0
         Visible         =   0   'False
         Width           =   2235
      End
      Begin VB.CommandButton cmdBillAll 
         Caption         =   "Bill All"
         Height          =   315
         Left            =   3780
         TabIndex        =   11
         Top             =   0
         Width           =   2235
      End
      Begin VB.CommandButton cmdUnbillAll 
         Caption         =   "Unmark Billing All"
         Height          =   315
         Left            =   1380
         TabIndex        =   10
         Top             =   0
         Width           =   2235
      End
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete"
      Height          =   255
      Index           =   0
      Left            =   13680
      TabIndex        =   8
      Tag             =   "NOCLEAR"
      Top             =   60
      Width           =   2175
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Finished"
      Height          =   255
      Index           =   1
      Left            =   13680
      TabIndex        =   7
      Tag             =   "NOCLEAR"
      Top             =   660
      Width           =   2175
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Billed"
      Height          =   255
      Index           =   2
      Left            =   13680
      TabIndex        =   6
      Tag             =   "NOCLEAR"
      Top             =   960
      Width           =   2175
   End
   Begin VB.TextBox txtClient 
      Height          =   315
      Left            =   6960
      TabIndex        =   5
      Top             =   60
      Width           =   3075
   End
   Begin VB.TextBox txtTerritory 
      Height          =   315
      Left            =   6960
      TabIndex        =   4
      Top             =   420
      Width           =   3075
   End
   Begin VB.TextBox txtRequestID 
      Height          =   315
      Left            =   12240
      TabIndex        =   3
      Top             =   60
      Width           =   1275
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "All Items"
      Height          =   255
      Index           =   3
      Left            =   13680
      TabIndex        =   2
      Tag             =   "NOCLEAR"
      Top             =   1260
      Width           =   2175
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Pending"
      Height          =   255
      Index           =   4
      Left            =   13680
      TabIndex        =   1
      Tag             =   "NOCLEAR"
      Top             =   360
      Width           =   2175
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnPortalUser 
      Bindings        =   "frmTrackerHDD.frx":0017
      Height          =   1755
      Left            =   300
      TabIndex        =   0
      Top             =   3000
      Width           =   7875
      DataFieldList   =   "fullname"
      ListAutoValidate=   0   'False
      MaxDropDownItems=   20
      _Version        =   196617
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   2
      Columns(0).Width=   8819
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "portaluserID"
      Columns(0).Name =   "portaluserID"
      Columns(0).DataField=   "portaluserID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   4551
      Columns(1).Caption=   "fullname"
      Columns(1).Name =   "fullname"
      Columns(1).DataField=   "fullname"
      Columns(1).FieldLen=   256
      _ExtentX        =   13891
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "fullname"
   End
   Begin MSAdodcLib.Adodc adoItems 
      Height          =   330
      Left            =   60
      Top             =   1620
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdItems 
      Bindings        =   "frmTrackerHDD.frx":0032
      Height          =   8715
      Left            =   60
      TabIndex        =   19
      Top             =   1620
      Width           =   26865
      _Version        =   196617
      stylesets.count =   3
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTrackerHDD.frx":0049
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmTrackerHDD.frx":0065
      stylesets(2).Name=   "headerfield"
      stylesets(2).ForeColor=   0
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmTrackerHDD.frx":0081
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   40
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "tracker_hdd_tasksID"
      Columns(0).Name =   "tracker_hdd_itemID"
      Columns(0).DataField=   "tracker_hdd_itemID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "Client"
      Columns(1).Name =   "Client"
      Columns(1).DataField=   "Client"
      Columns(1).FieldLen=   256
      Columns(1).StyleSet=   "headerfield"
      Columns(2).Width=   1905
      Columns(2).Caption=   "Territory"
      Columns(2).Name =   "Territory"
      Columns(2).DataField=   "Territory"
      Columns(2).FieldLen=   256
      Columns(2).StyleSet=   "headerfield"
      Columns(3).Width=   1508
      Columns(3).Caption=   "Req ID"
      Columns(3).Name =   "RequestID"
      Columns(3).DataField=   "RequestID"
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "headerfield"
      Columns(4).Width=   1508
      Columns(4).Caption=   "Line ID"
      Columns(4).Name =   "LineItemID"
      Columns(4).DataField=   "LineItemID"
      Columns(4).FieldLen=   256
      Columns(4).StyleSet=   "headerfield"
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "date arrived"
      Columns(5).Name =   "datearrived"
      Columns(5).DataField=   "datearrived"
      Columns(5).FieldLen=   256
      Columns(5).Style=   1
      Columns(6).Width=   1773
      Columns(6).Caption=   "Due Date"
      Columns(6).Name =   "RequestItemDueDate"
      Columns(6).DataField=   "DueDate"
      Columns(6).FieldLen=   256
      Columns(6).StyleSet=   "headerfield"
      Columns(7).Width=   1773
      Columns(7).Caption=   "Before Date"
      Columns(7).Name =   "DoNotDeliverBeforeDate"
      Columns(7).DataField=   "DoNotDeliverBeforeDate"
      Columns(7).FieldLen=   256
      Columns(7).StyleSet=   "headerfield"
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "Request Name"
      Columns(8).Name =   "RequestName"
      Columns(8).DataField=   "RequestName"
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "Scheduled By"
      Columns(9).Name =   "ScheduledBy"
      Columns(9).DataField=   "ScheduledBy"
      Columns(9).FieldLen=   256
      Columns(10).Width=   873
      Columns(10).Caption=   "Avail"
      Columns(10).Name=   "ComponentsAvailable"
      Columns(10).DataField=   "ComponentsAvailable"
      Columns(10).FieldLen=   256
      Columns(10).StyleSet=   "headerfield"
      Columns(11).Width=   3704
      Columns(11).Caption=   "Series Title"
      Columns(11).Name=   "SeriesTitle"
      Columns(11).DataField=   "SeriesTitle"
      Columns(11).FieldLen=   256
      Columns(11).StyleSet=   "headerfield"
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "Episode Title"
      Columns(12).Name=   "EpisodeTitle"
      Columns(12).DataField=   "EpisodeTitle"
      Columns(12).FieldLen=   256
      Columns(13).Width=   794
      Columns(13).Caption=   "Eps"
      Columns(13).Name=   "EpisodeNumber"
      Columns(13).DataField=   "EpisodeNumber"
      Columns(13).FieldLen=   256
      Columns(13).StyleSet=   "headerfield"
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "CV Disp Name"
      Columns(14).Name=   "CVDisplayName"
      Columns(14).DataField=   "CVDisplayName"
      Columns(14).FieldLen=   256
      Columns(15).Width=   4048
      Columns(15).Caption=   "File Location"
      Columns(15).Name=   "FileLocation"
      Columns(15).Alignment=   1
      Columns(15).DataField=   "FileLocation"
      Columns(15).FieldLen=   256
      Columns(15).StyleSet=   "headerfield"
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "File Reference"
      Columns(16).Name=   "FileReference"
      Columns(16).DataField=   "FileReference"
      Columns(16).FieldLen=   256
      Columns(17).Width=   5292
      Columns(17).Caption=   "Actual File Name"
      Columns(17).Name=   "ActualFileName"
      Columns(17).DataField=   "ActualFileName"
      Columns(17).FieldLen=   256
      Columns(17).StyleSet=   "headerfield"
      Columns(18).Width=   1773
      Columns(18).Caption=   "File Size GB"
      Columns(18).Name=   "FileSizeGB"
      Columns(18).DataField=   "ActualFileSize"
      Columns(18).FieldLen=   256
      Columns(18).StyleSet=   "headerfield"
      Columns(19).Width=   1773
      Columns(19).Caption=   "Status"
      Columns(19).Name=   "Status"
      Columns(19).DataField=   "Status"
      Columns(19).FieldLen=   256
      Columns(19).StyleSet=   "headerfield"
      Columns(20).Width=   1720
      Columns(20).Caption=   "Format Req"
      Columns(20).Name=   "FormatRequiredType"
      Columns(20).DataField=   "FormatRequiredType"
      Columns(20).FieldLen=   256
      Columns(20).StyleSet=   "headerfield"
      Columns(21).Width=   3200
      Columns(21).Visible=   0   'False
      Columns(21).Caption=   "Ship Method Type ID"
      Columns(21).Name=   "ShippingMethodTypeID"
      Columns(21).DataField=   "ShippingMethodTypeID"
      Columns(21).FieldLen=   256
      Columns(22).Width=   1640
      Columns(22).Caption=   "Media Req"
      Columns(22).Name=   "MediaRequiredType"
      Columns(22).DataField=   "MediaRequiredType"
      Columns(22).FieldLen=   256
      Columns(22).StyleSet=   "headerfield"
      Columns(23).Width=   2646
      Columns(23).Caption=   "Med Type"
      Columns(23).Name=   "MediaType"
      Columns(23).DataField=   "MediaType"
      Columns(23).FieldLen=   256
      Columns(23).StyleSet=   "stagefield"
      Columns(24).Width=   2646
      Columns(24).Caption=   "Media Barcode"
      Columns(24).Name=   "MediaBarcode"
      Columns(24).DataField=   "MediaBarcode"
      Columns(24).FieldLen=   256
      Columns(24).StyleSet=   "stagefield"
      Columns(25).Width=   2646
      Columns(25).Caption=   "Destination"
      Columns(25).Name=   "Destination"
      Columns(25).DataField=   "Destination"
      Columns(25).FieldLen=   256
      Columns(25).StyleSet=   "headerfield"
      Columns(26).Width=   2223
      Columns(26).Caption=   "Client Contact"
      Columns(26).Name=   "ClientContact"
      Columns(26).DataField=   "ClientContact"
      Columns(26).FieldLen=   256
      Columns(26).StyleSet=   "headerfield"
      Columns(27).Width=   2805
      Columns(27).Caption=   "Track No"
      Columns(27).Name=   "TrackingNumber"
      Columns(27).DataField=   "TrackingNumber"
      Columns(27).FieldLen=   256
      Columns(27).StyleSet=   "stagefield"
      Columns(28).Width=   3200
      Columns(28).Visible=   0   'False
      Columns(28).Caption=   "Ship Date"
      Columns(28).Name=   "ShipDate"
      Columns(28).DataField=   "ShipDate"
      Columns(28).FieldLen=   256
      Columns(28).Style=   1
      Columns(29).Width=   3200
      Columns(29).Visible=   0   'False
      Columns(29).Caption=   "Dept Charge Code"
      Columns(29).Name=   "DepartmentChargeCode"
      Columns(29).DataField=   "DepartmentChargeCode"
      Columns(29).FieldLen=   256
      Columns(30).Width=   3200
      Columns(30).Visible=   0   'False
      Columns(30).Caption=   "cdate"
      Columns(30).Name=   "cdate"
      Columns(30).DataField=   "cdate"
      Columns(30).FieldLen=   256
      Columns(30).Style=   1
      Columns(31).Width=   3200
      Columns(31).Visible=   0   'False
      Columns(31).Caption=   "mdate"
      Columns(31).Name=   "mdate"
      Columns(31).DataField=   "mdate"
      Columns(31).FieldLen=   256
      Columns(31).Style=   1
      Columns(32).Width=   926
      Columns(32).Caption=   "Pend"
      Columns(32).Name=   "rejected"
      Columns(32).DataField=   "rejected"
      Columns(32).FieldLen=   256
      Columns(32).Style=   2
      Columns(32).StyleSet=   "conclusionfield"
      Columns(33).Width=   979
      Columns(33).Caption=   "Comp"
      Columns(33).Name=   "readytobill"
      Columns(33).DataField=   "readytobill"
      Columns(33).FieldLen=   256
      Columns(33).Locked=   -1  'True
      Columns(33).Style=   2
      Columns(33).StyleSet=   "conclusionfield"
      Columns(34).Width=   926
      Columns(34).Caption=   "Billed"
      Columns(34).Name=   "billed"
      Columns(34).DataField=   "billed"
      Columns(34).FieldLen=   256
      Columns(34).Locked=   -1  'True
      Columns(34).Style=   2
      Columns(34).StyleSet=   "conclusionfield"
      Columns(35).Width=   3200
      Columns(35).Visible=   0   'False
      Columns(35).Caption=   "job ID"
      Columns(35).Name=   "jobID"
      Columns(35).DataField=   "jobID"
      Columns(35).FieldLen=   256
      Columns(36).Width=   3200
      Columns(36).Visible=   0   'False
      Columns(36).Caption=   "companyID"
      Columns(36).Name=   "companyID"
      Columns(36).DataField=   "companyID"
      Columns(36).FieldLen=   256
      Columns(37).Width=   3200
      Columns(37).Visible=   0   'False
      Columns(37).Caption=   "fileCopied"
      Columns(37).Name=   "fileCopied"
      Columns(37).DataField=   "fileCopied"
      Columns(37).FieldLen=   256
      Columns(37).Style=   1
      Columns(38).Width=   3200
      Columns(38).Visible=   0   'False
      Columns(38).Caption=   "ActualFileSize"
      Columns(38).Name=   "ActualFileSize"
      Columns(38).DataField=   "ActualFileSize"
      Columns(38).FieldLen=   256
      Columns(39).Width=   3200
      Columns(39).Visible=   0   'False
      Columns(39).Caption=   "fileCopiedBy"
      Columns(39).Name=   "fileCopiedBy"
      Columns(39).DataField=   "fileCopiedBy"
      Columns(39).FieldLen=   256
      _ExtentX        =   47387
      _ExtentY        =   15372
      _StockProps     =   79
      Caption         =   "Tracker Items"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   1740
      TabIndex        =   21
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The company this job is for"
      Top             =   60
      Width           =   2835
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   5001
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin MSAdodcLib.Adodc adoComments 
      Height          =   330
      Left            =   14820
      Top             =   10380
      Visible         =   0   'False
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoComments"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdComments 
      Bindings        =   "frmTrackerHDD.frx":009D
      Height          =   1875
      Left            =   60
      TabIndex        =   22
      Top             =   10380
      Visible         =   0   'False
      Width           =   12015
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12582847
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   5
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "trackerhistoryID"
      Columns(0).Name =   "tracker_commentID"
      Columns(0).DataField=   "tracker_hdd_commentID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "trackerprogramID"
      Columns(1).Name =   "tracker_hdd_itemID"
      Columns(1).DataField=   "tracker_hdd_itemID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   12144
      Columns(2).Caption=   "Comments"
      Columns(2).Name =   "comment"
      Columns(2).DataField=   "comment"
      Columns(2).FieldLen=   255
      Columns(3).Width=   3360
      Columns(3).Caption=   "Date"
      Columns(3).Name =   "cdate"
      Columns(3).DataField=   "cdate"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).Style=   1
      Columns(4).Width=   3519
      Columns(4).Caption=   "Entered By"
      Columns(4).Name =   "cuser"
      Columns(4).DataField=   "cuser"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      UseDefaults     =   0   'False
      _ExtentX        =   21193
      _ExtentY        =   3307
      _StockProps     =   79
      Caption         =   "Tracker Comments"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbOrderBy 
      Height          =   315
      Left            =   1740
      TabIndex        =   23
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The company this job is for"
      Top             =   780
      Width           =   2835
      DataFieldList   =   "label"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "fieldname"
      Columns(0).Name =   "fieldname"
      Columns(0).DataField=   "fieldname"
      Columns(0).FieldLen=   256
      Columns(1).Width=   4498
      Columns(1).Caption=   "label"
      Columns(1).Name =   "label"
      Columns(1).DataField=   "label"
      Columns(1).FieldLen=   256
      _ExtentX        =   5001
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "label"
   End
   Begin MSAdodcLib.Adodc adoCustomers 
      Height          =   330
      Left            =   12180
      Top             =   10380
      Visible         =   0   'False
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCustomers"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label lblCaption 
      Caption         =   "Total Size"
      Height          =   255
      Index           =   4
      Left            =   15960
      TabIndex        =   36
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label lblTotalFileSize 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   17520
      TabIndex        =   35
      Tag             =   "CLEARFIELDS"
      Top             =   60
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Components Available"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   10140
      TabIndex        =   34
      Top             =   480
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   10
      Left            =   150
      TabIndex        =   32
      Top             =   120
      Width           =   915
   End
   Begin VB.Label lblCompanyID 
      Height          =   195
      Left            =   3300
      TabIndex        =   31
      Top             =   480
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   255
      Left            =   12420
      TabIndex        =   30
      Top             =   10920
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Client"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   4740
      TabIndex        =   29
      Top             =   120
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Territory"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   4740
      TabIndex        =   28
      Top             =   480
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Series Title"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   4740
      TabIndex        =   27
      Top             =   840
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Request ID"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   10140
      TabIndex        =   26
      Top             =   120
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Display Sort Order"
      Height          =   255
      Index           =   14
      Left            =   120
      TabIndex        =   25
      Top             =   840
      Width           =   1575
   End
   Begin VB.Label lblLastTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   255
      Left            =   12420
      TabIndex        =   24
      Top             =   11280
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "frmTrackerHDD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strSearch As String
Dim m_strOrderby As String
Dim m_blnDelete As Boolean

Private Sub chkHideDemo_Click()

Dim l_strSQL As String

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/hddtracker %' and companyID > 100 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/hddtracker %' ORDER BY name;"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

cmdSearch.Value = True

End Sub

Private Sub cmbCompany_Click()

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text
m_strSearch = " WHERE companyID = " & lblCompanyID.Caption
m_strOrderby = " ORDER BY componentsavailable DESC, duedate, LineItemID "

cmdSearch.Value = True

End Sub

Private Sub cmbComponentsAvailable_Click()
cmdSearch_Click
End Sub

Private Sub cmbComponentsAvailable_DropDown()
Dim l_strTemp As String

cmbComponentsAvailable.Clear
cmbComponentsAvailable.AddItem ""
cmbComponentsAvailable.ItemData(cmbComponentsAvailable.NewIndex) = 0
cmbComponentsAvailable.AddItem "Yes"
cmbComponentsAvailable.ItemData(cmbComponentsAvailable.NewIndex) = 1
cmbComponentsAvailable.AddItem "No"
cmbComponentsAvailable.ItemData(cmbComponentsAvailable.NewIndex) = 2

End Sub

Private Sub cmdBillAll_Click2()

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then
    
    Do While Not adoItems.Recordset.EOF
        If cmdBillItem.Visible = True Then
            cmdBillItem.Value = True
        End If
        adoItems.Recordset.MoveNext
        DoEvents
    Loop
End If

End Sub
Private Sub cmdBillAll_Click()

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then
    
    Do While Not adoItems.Recordset.EOF
        If cmdBillItem.Visible = True Then
            cmdBillItem.Value = True
        End If
        adoItems.Recordset.MoveNext
        DoEvents
    Loop
End If

End Sub

Private Sub cmdBillItem_Click()

Dim l_strChargeCode As String, l_rstTrackerChargeCode As ADODB.Recordset, l_strJobLine As String, l_lngJobID As Long, l_lngDuration As Long, l_strLanguage As String, l_rst As ADODB.Recordset
Dim l_lngQuantity As Long, l_strCode As String, l_lngFactor As Long, l_strLastComment As String, l_strLastCommentCuser As String, l_datLastCommentDate As Date, l_blnFirstLine As Boolean
Dim l_blnError As Boolean, l_lngEventID As Long, l_varBookmark As Variant, l_datFileCopied As Date, l_lngLibraryID As Long, l_strSQL As String

If frmJob.txtJobID.Text <> "" Then
    'A job is loaded - now check that it isn't locked.
    l_lngJobID = Val(frmJob.txtJobID.Text)
    If frmJob.txtStatus.Text = "Confirmed" And frmJob.lblCompanyID.Caption = lblCompanyID.Caption Then
     '   If grdItems.Columns("tracker_hdd_itemID").Text <> "" And AdoSource.Recordset.RecordCount > 0 Then
            'The Job is valid - go ahead and create Job lines for this item.
            l_strJobLine = QuoteSanitise(grdItems.Columns("CVDisplayName").Text)
            l_blnError = False
            l_datFileCopied = grdItems.Columns("filecopied").Text
                

            l_lngDuration = 0
            l_strCode = "O"
            l_lngQuantity = 1
            MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, 1, "DADCHDD", 0, l_datFileCopied, grdItems.Columns("fileCopiedBy").Text, frmJob.adoJobDetail.Recordset.RecordCount, 0

            frmJob.adoJobDetail.Refresh
            grdItems.Columns("jobID").Text = l_lngJobID
            grdItems.Columns("billed").Text = -1
            grdItems.Update
            cmdBillItem.Visible = False
            cmdManualBillItem.Visible = False
            cmdUnbill.Visible = True
            l_lngLibraryID = GetData("library", "libraryID", "Barcode", grdItems.Columns("MediaBarcode").Text)
            l_strSQL = "UPDATE events SET jobID = " & l_lngJobID & " WHERE libraryID = " & l_lngLibraryID & " AND clipfilename = '" & grdItems.Columns("ActualFileName").Text & "' AND system_deleted = 0;"
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
       ' Else
          '  MsgBox "The current Item must have a duration in order to bill it.", vbCritical, "Cannot Bill Item"
       ' End If
    Else
        MsgBox "The current Job must belong to the correct company and be confirmed in order to bill an item.", vbCritical, "Cannot Bill Item"
    End If
Else
    MsgBox "A Job must be created and loaded into the Jobs form in order to bill an item.", vbCritical, "Cannot Bill Item"
End If

End Sub

Private Sub cmdBillItem_Click2()

Dim l_strChargeCode As String, l_rstTrackerChargeCode As ADODB.Recordset, l_strJobLine As String, l_lngJobID As Long, l_lngDuration As Long, l_lngMinuteBilling As Long
Dim l_lngQuantity As Long, l_strCode As String, l_lngFactor As Long, l_strLastComment As String, l_strLastCommentCuser As String, l_datLastCommentDate As Date, l_blnFirstLine As Boolean
Dim l_lngCount As Long

If frmJob.txtJobID.Text <> "" Then
    'A job is loaded - now check that it isn't locked.
    l_lngJobID = Val(frmJob.txtJobID.Text)
    If frmJob.txtStatus.Text = "Confirmed" And frmJob.lblCompanyID.Caption = lblCompanyID.Caption Then
        If grdItems.Columns("duration").Text <> "" Then
            'The Job is valid - go ahead and create Job lines for this item.
            l_lngQuantity = 0
            l_strJobLine = QuoteSanitise(grdItems.Columns("headertext1").Text)
            If grdItems.Columns("headerint1").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("headerint1").Text
            If grdItems.Columns("headerint2").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("headerint2").Text
            If grdItems.Columns("headertext2").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("headertext2").Text
            If grdItems.Columns("itemreference").Text <> "" Then
                If l_strJobLine <> "" Then
                    l_strJobLine = l_strJobLine & " - " & grdItems.Columns("itemreference").Text
                Else
                    l_strJobLine = grdItems.Columns("itemreference").Text
                End If
            End If
            If adoComments.Recordset.RecordCount > 0 Then
                adoComments.Recordset.MoveLast
                l_strLastCommentCuser = adoComments.Recordset("cuser")
                l_datLastCommentDate = adoComments.Recordset("cdate")
                l_strLastComment = adoComments.Recordset("comment")
                If adoComments.Recordset("emailclock") <> 0 Then l_strLastComment = l_strLastComment & ", MX1 to Replace Clock"
                If adoComments.Recordset("emailbarsandtone") <> 0 Then l_strLastComment = l_strLastComment & ", MX1 to Replace Bars and Tone"
                If adoComments.Recordset("emailtimecode") <> 0 Then l_strLastComment = l_strLastComment & ", MX1 to Correct Timecode"
                If adoComments.Recordset("emailblackatend") <> 0 Then l_strLastComment = l_strLastComment & ", MX1 to correct Black at End"
            Else
                l_strLastCommentCuser = ""
                l_datLastCommentDate = 0
                l_strLastComment = ""
            End If
            l_blnFirstLine = True
            For l_lngCount = 1 To 25
            
            If grdItems.Columns("stagefield" & l_lngCount).Text <> "" And LCase(grdItems.Columns("stagefield" & l_lngCount).Text) <> "n/a" Then
                If l_blnFirstLine = True Then
                    l_strCode = "M"
                    l_blnFirstLine = False
                Else
                    l_strCode = "A"
                End If
                l_lngDuration = Val(grdItems.Columns("duration").Text)
                Set l_rstTrackerChargeCode = ExecuteSQL("SELECT billingcode, minutebilling FROM tracker_itemchoice WHERE companyID = " & lblCompanyID.Caption & " AND itemfield = 'stagefield1" & l_lngCount & "';", g_strExecuteError)
                CheckForSQLError
                If l_rstTrackerChargeCode.RecordCount > 0 Then
                    l_strChargeCode = Trim(" " & l_rstTrackerChargeCode("billingcode"))
                    Select Case Left(l_rstTrackerChargeCode("minutebilling"), 1)
                        Case "N"
                            l_lngQuantity = Val(grdItems.Columns("stagefield" & l_lngCount).Text)
                            l_lngDuration = 0
                            l_strCode = "O"
                        Case "T"
                            l_lngQuantity = 1
                            l_lngDuration = Val(grdItems.Columns("stagefield" & l_lngCount).Text)
                        Case "M"
                            l_lngMinuteBilling = 1
                        Case "S"
                            l_lngFactor = 0
                            If Len(l_rstTrackerChargeCode("minutebilling")) > 1 Then l_lngFactor = Val(Mid(l_rstTrackerChargeCode("minutebilling"), 3))
                            If l_lngFactor <> 0 Then
                                If Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "/" Then
                                    l_lngQuantity = Int((Val(grdItems.Columns("gbsent").Text) + 0.99) / l_lngFactor)
                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "*" Then
                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) * l_lngFactor)
                                ElseIf Mid(l_rstTrackerChargeCode("minutebilling"), 2, 1) = "+" Then
                                    l_lngQuantity = Int(Val(grdItems.Columns("gbsent").Text) + l_lngFactor)
                                Else
                                    l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                                End If
                            Else
                                l_lngQuantity = Val(grdItems.Columns("gbsent").Text)
                            End If
                            l_lngDuration = 0
                            l_strCode = "O"
                        Case "1"
                            l_lngQuantity = 1
                            l_lngDuration = 0
                            l_strCode = "O"
                        Case "I"
                            l_strCode = "I"
                        Case Else
                            l_lngMinuteBilling = 0
                    End Select
                    If l_lngQuantity = 0 Then l_lngQuantity = 1
                End If
                l_rstTrackerChargeCode.Close
                If l_strChargeCode <> "" Then
                    If l_strLastComment <> "" Then
                        MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser
                    Else
                        MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, l_lngMinuteBilling
                    End If
                    l_lngQuantity = 0
                End If
            End If
            frmJob.adoJobDetail.Refresh
            Next
            grdItems.Columns("billed").Text = -1
            grdItems.Columns("jobID").Text = l_lngJobID
            grdItems.Update
            cmdBillItem.Visible = False
            cmdManualBillItem.Visible = False
        Else
            MsgBox "The current Item must have a duration in order to bill it.", vbCritical, "Cannot Bill Item"
        End If
    Else
        MsgBox "The current Job must belong to the correct company and be confirmed in order to bill an item.", vbCritical, "Cannot Bill Item"
    End If
Else
    MsgBox "A Job must be created and loaded into the Jobs form in order to bill an item.", vbCritical, "Cannot Bill Item"
End If

End Sub

Private Sub cmdClear_Click()

ClearFields Me

End Sub

Private Sub cmdClearComments_Click()

'optComments(0).Value = False
'optComments(1).Value = False

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdManualBillItem_Click()

grdItems.Columns("billed").Text = -1
grdItems.Columns("jobid").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False

End Sub

Private Sub cmdPrint_Click()

Dim l_strSQL As String

If lblCompanyID.Caption <> "" Then
    l_strSQL = adoItems.RecordSource
    PrintCrystalReportUsingCleanSQL g_strLocationOfCrystalReportFiles & "GenericTrackerReport.rpt", l_strSQL, True
End If

End Sub

Private Sub cmdSearch_Click()

Dim l_rstTotal As ADODB.Recordset, l_lngVideoTotal As Long, l_strSQL As String, l_curTotalSize As Currency

If lblCompanyID.Caption <> "" Then

    If optComplete(0).Value = True Then
        'Not Complete
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) AND (rejected IS NULL OR rejected = 0) "
    ElseIf optComplete(1).Value = True Then
        'Finished
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND readytobill <> 0 "
    ElseIf optComplete(2).Value = True Then
        'Billed
        l_strSQL = " AND billed IS NOT NULL AND billed <> 0 "
    ElseIf optComplete(4).Value = True Then
        'Pending
        l_strSQL = " AND rejected <> 0 "
    Else

    End If
    
    Select Case cmbComponentsAvailable.Text
    Case "Yes"
            l_strSQL = " AND ComponentsAvailable = 'YES'  "
    Case "No"
            l_strSQL = " AND ComponentsAvailable = 'NO'  "
    Case Else
    
    End Select
    
    If txtClient.Text <> "" Then
        l_strSQL = l_strSQL & " AND Client LIKE '" & QuoteSanitise(txtClient.Text) & "%' "
    End If
    
    If txtTerritory.Text <> "" Then
        l_strSQL = l_strSQL & " AND Territory LIKE '" & QuoteSanitise(txtTerritory.Text) & "%' "
    End If
    
    If txtSeriesTitle.Text <> "" Then
        l_strSQL = l_strSQL & " AND SeriesTitle LIKE '" & QuoteSanitise(txtSeriesTitle.Text) & "%' "
    End If
    
    If Val(txtRequestID.Text) <> 0 Then
        l_strSQL = l_strSQL & " AND RequestID = " & Val(txtRequestID.Text) & " "
    End If
    
    Debug.Print l_strSQL
    adoItems.ConnectionString = g_strConnection
    adoItems.RecordSource = "SELECT * FROM tracker_hdd_item " & m_strSearch & l_strSQL & m_strOrderby & ";"
    
    Debug.Print adoItems.RecordSource
    adoItems.Refresh
    
    adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"
    
    Set l_rstTotal = ExecuteSQL("SELECT Sum(ActualFileSize) FROM tracker_hdd_item " & m_strSearch & l_strSQL, g_strExecuteError)
    
    If l_rstTotal.RecordCount > 0 Then
        If Not IsNull(l_rstTotal(0)) Then
            l_curTotalSize = l_rstTotal(0)
            lblTotalFileSize.Caption = Format(Int((l_curTotalSize / 1024 / 1024 / 1024) + 0.99999999), "#,##0") & " GB"
        Else
            lblTotalFileSize.Caption = ""
        End If
    Else
        lblTotalFileSize.Caption = ""
    End If
    
    l_rstTotal.Close
    Set l_rstTotal = Nothing

End If

End Sub

Private Sub cmdUnbill_Click()

grdItems.Columns("billed").Text = 0
grdItems.Columns("jobid").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False

End Sub

Private Sub cmdUnbillAll_Click()

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then

    Do While Not adoItems.Recordset.EOF
        adoItems.Recordset("billed") = 0
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
    Loop
End If

grdItems.Refresh

End Sub

Private Sub ddnPortalUser_CloseUp()

grdItems.Columns("portaluserID").Text = ddnPortalUser.Columns("portaluserID").Text

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

grdItems.StyleSets("headerfield").BackColor = &HE7FFE7
grdItems.StyleSets("stagefield").BackColor = &HE7FFFF
grdItems.StyleSets("conclusionfield").BackColor = &HFFFFE7

grdItems.StyleSets.Add "Error"
grdItems.StyleSets("Error").BackColor = &HA0A0FF

optComplete(0).Value = True

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE (cetaclientcode like '%/hddtracker %') AND companyID > 100 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE (cetaclientcode like '%/hddtracker %') ORDER BY name;"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub Form_Resize()

On Error Resume Next

grdItems.Width = Me.ScaleWidth - grdItems.Left - 120
'grdItems.height = (Me.ScaleHeight - grdItems.Top - frmButtons.height) * 0.75 - 240
grdItems.Height = (Me.ScaleHeight - grdItems.Top - frmButtons.Height) - 240
frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120
grdComments.Top = grdItems.Top + grdItems.Height + 120
grdComments.Height = frmButtons.Top - grdComments.Top - 120
grdComments.Width = grdItems.Width

End Sub

Private Sub grdComments_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdComments_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdComments_BeforeUpdate(Cancel As Integer)

If m_blnDelete = False Then

    If grdComments.Columns("comment").Text = "" Then
        MsgBox "Cannot save a Comment with no actual comment", vbCritical, "Comment Not Saved"
        Cancel = True
        Exit Sub
    End If
       
    grdComments.Columns("tracker_hdd_itemID").Text = lblTrackeritemID.Caption
    If grdComments.Columns("cdate").Text = "" Then
        grdComments.Columns("cdate").Text = Now
    End If
    grdComments.Columns("cuser").Text = g_strFullUserName

End If

End Sub

Private Sub grdItems_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdItems_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdItems_BeforeUpdate(Cancel As Integer)

If m_blnDelete = True Then Exit Sub

If grdItems.Columns("MediaBarcode").Text <> "" And Trim(" " & adoItems.Recordset("MediaBarcode")) = "" Then
    grdItems.Columns("filecopied").Text = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("fileCopiedBy").Text = g_strFullUserName
    grdItems.Columns("MediaBarcode").Text = UCase(grdItems.Columns("Mediabarcode").Text)
    grdItems.Columns("MediaType").Text = GetData("library", "stocktype", "barcode", UCase(grdItems.Columns("Mediabarcode").Text))
ElseIf grdItems.Columns("MediaBarcode").Text = "" And Trim(" " & adoItems.Recordset("MediaBarcode")) <> "" Then
    grdItems.Columns("Filecopied").Text = ""
    grdItems.Columns("fileCopiedBy").Text = ""
    grdItems.Columns("MediaType").Text = ""
End If

If grdItems.Columns("TrackingNumber").Text <> "" And Trim(" " & adoItems.Recordset("TrackingNumber")) = "" Then
    grdItems.Columns("ShipDate").Text = Format(Now, "yyyy-mm-dd hh:nn:ss")
ElseIf grdItems.Columns("TrackingNumber").Text = "" And Trim(" " & adoItems.Recordset("TrackingNumber")) <> "" Then
    grdItems.Columns("ShipDate").Text = ""
End If

If grdItems.Columns("MediaBarcode").Text <> "" And grdItems.Columns("TrackingNumber").Text <> "" Then
    grdItems.Columns("readytobill").Text = 1
Else
    grdItems.Columns("readytobill").Text = 0
End If

End Sub

Private Sub grdItems_BtnClick()

Dim tempdate As String

Select Case LCase(grdItems.Columns(grdItems.Col).Name)

Case "itemreference"
    
    MakeClipFromGenericTrackerEvent

Case Else

    If grdItems.ActiveCell.Text <> "" Then
        grdItems.ActiveCell.Text = ""
    Else
        If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
            grdItems.ActiveCell.Text = Now
        Else
            tempdate = FormatDateTime(Now, vbLongDate)
            grdItems.ActiveCell.Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
        End If
    End If

End Select

End Sub

Private Sub grdItems_DblClick()

'ShowClipSearch "", grdItems.Columns("itemreference").Text

End Sub

Private Sub grdItems_HeadClick(ByVal ColIndex As Integer)

If (grdItems.Columns(ColIndex).Name <> "Customer" And grdItems.Columns(ColIndex).Name <> "Stored On") Then
    m_strOrderby = " ORDER BY " & grdItems.Columns(ColIndex).Name
    adoItems.RecordSource = "SELECT * FROM tracker_hdd_item " & m_strSearch & m_strOrderby
    adoItems.Refresh
End If

End Sub

Private Sub grdItems_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim l_strSQL As String

lblTrackeritemID.Caption = grdItems.Columns("tracker_hdd_itemID").Text

If Val(lblTrackeritemID.Caption) <> Val(lblLastTrackerItemID.Caption) Then
    
    If Val(lblTrackeritemID.Caption) = 0 Then
        cmdBillItem.Visible = False
    
'        l_strSQL = "SELECT * FROM tracker_hdd_comment WHERE tracker_hdd_itemID = -1 ORDER BY cdate; "
'        adoComments.RecordSource = l_strSQL
'        adoComments.ConnectionString = g_strConnection
'        adoComments.Refresh
'
        lblLastTrackerItemID.Caption = ""
        
        Exit Sub
        
    End If
    
    l_strSQL = "SELECT * FROM tracker_hdd_comment WHERE tracker_hdd_itemID = " & lblTrackeritemID.Caption & " ORDER BY cdate;"

    adoComments.RecordSource = l_strSQL
    adoComments.ConnectionString = g_strConnection
    adoComments.Refresh

    If grdItems.Columns("readytobill").Text <> 0 And grdItems.Columns("billed").Text = 0 Then
        cmdBillItem.Visible = True
        cmdManualBillItem.Visible = True
    Else
        cmdBillItem.Visible = False
        cmdManualBillItem.Visible = False
    End If

    If grdItems.Columns("billed").Text <> 0 Then
        cmdUnbill.Visible = True
    Else
        cmdUnbill.Visible = False
    End If

    lblLastTrackerItemID.Caption = lblTrackeritemID.Caption
    
End If
    
End Sub

Private Sub grdItems_RowLoaded(ByVal Bookmark As Variant)

Dim l_curFileSize As Currency

If grdItems.Columns("actualfilesize").Text <> "" Then
    l_curFileSize = grdItems.Columns("actualfilesize").Text
    grdItems.Columns("FilesizeGB").Text = Format(Int((l_curFileSize / 1024 / 1024 / 1024) + 0.99999999), "#,##0") & " GB"
End If

End Sub

Private Sub optComplete_Click(Index As Integer)

cmdSearch_Click

End Sub

Private Sub txtSeriesTitle_DropDown()

Dim l_strTemp As String
If Val(lblCompanyID.Caption) <> 0 Then
    l_strTemp = txtSeriesTitle.Text
    txtSeriesTitle.Clear
    PopulateCombo "svenskhddtitle", txtSeriesTitle, "companyID = " & Val(lblCompanyID.Caption) & " "
    txtSeriesTitle.Text = l_strTemp
End If
HighLite txtSeriesTitle
End Sub
