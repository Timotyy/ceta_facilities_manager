VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmClipSearch 
   Caption         =   "Media File Search"
   ClientHeight    =   13080
   ClientLeft      =   60
   ClientTop       =   570
   ClientWidth     =   24030
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmClipSearch.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   13080
   ScaleWidth      =   24030
   Visible         =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.Frame fraPatheOrder 
      Caption         =   "Pathe Order Processing"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   10935
      Left            =   2880
      TabIndex        =   285
      Top             =   480
      Visible         =   0   'False
      Width           =   17355
      Begin VB.CommandButton cmdClearPathe 
         Caption         =   "Clear"
         Height          =   555
         Left            =   14520
         TabIndex        =   299
         ToolTipText     =   "Clear the form"
         Top             =   10020
         Width           =   1215
      End
      Begin VB.CommandButton cmdProcessPatheTextFilmID 
         Caption         =   "Process FilmID Order Email"
         Height          =   555
         Left            =   10680
         TabIndex        =   298
         ToolTipText     =   "Read the FilmID Email and extract media Items"
         Top             =   10020
         Width           =   1395
      End
      Begin VB.CommandButton cmdClosePathe 
         Caption         =   "Close"
         Height          =   555
         Left            =   15900
         TabIndex        =   296
         ToolTipText     =   "Close the form"
         Top             =   10020
         Width           =   1215
      End
      Begin VB.CommandButton cmdTransferToGrid 
         Caption         =   "Transfer Pathe Clips to Grid"
         Height          =   555
         Left            =   12780
         TabIndex        =   295
         ToolTipText     =   "Transfer the extracted Clips to the Media Search Grid"
         Top             =   10020
         Width           =   1215
      End
      Begin VB.CommandButton cmdProcessPatheTextVLVA 
         Caption         =   "Process VLVA Order Email"
         Height          =   555
         Left            =   9120
         TabIndex        =   294
         ToolTipText     =   "Read the VLVA Email and extract media Items"
         Top             =   10020
         Width           =   1395
      End
      Begin VB.CommandButton cmdPatheOrder 
         Caption         =   "Locate or Create Order"
         Height          =   555
         Left            =   2400
         TabIndex        =   293
         ToolTipText     =   "Clear the form"
         Top             =   9960
         Width           =   1215
      End
      Begin MSAdodcLib.Adodc adoPatheClips 
         Height          =   330
         Left            =   9180
         Top             =   3780
         Width           =   3000
         _ExtentX        =   5292
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "0 Records Found"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdPatheClips 
         Bindings        =   "frmClipSearch.frx":08CA
         Height          =   5895
         Left            =   9180
         TabIndex        =   292
         Top             =   3780
         Width           =   7935
         _Version        =   196617
         AllowDelete     =   -1  'True
         AllowUpdate     =   0   'False
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Order_Detail_ID"
         Columns(0).Name =   "Order_Detail_ID"
         Columns(0).DataField=   "Order_Detail_ID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "eventID"
         Columns(1).Name =   "eventID"
         Columns(1).DataField=   "eventID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   6482
         Columns(2).Caption=   "File"
         Columns(2).Name =   "clipfilename"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "Stored On"
         Columns(3).Name =   "Barcode"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         _ExtentX        =   13996
         _ExtentY        =   10398
         _StockProps     =   79
         Caption         =   "Pathe Clips on Order"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox txtPatheOrderNumber 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   240
         TabIndex        =   290
         Tag             =   "NOCLEAR"
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   10200
         Width           =   1935
      End
      Begin VB.TextBox txtOriginalOrderEmail 
         Appearance      =   0  'Flat
         Height          =   5895
         Left            =   240
         MultiLine       =   -1  'True
         TabIndex        =   287
         Tag             =   "NOCLEAR"
         Top             =   3780
         Width           =   8715
      End
      Begin MSComCtl2.DTPicker datPatheOrderCreated 
         Height          =   315
         Left            =   4380
         TabIndex        =   300
         Tag             =   "NOCLEAR"
         ToolTipText     =   "The date the tape was created"
         Top             =   10200
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         _Version        =   393216
         CalendarBackColor=   12632319
         CalendarTitleBackColor=   12632319
         CheckBox        =   -1  'True
         CustomFormat    =   "dd/MM/yy HH:mm:ss"
         DateIsNull      =   -1  'True
         Format          =   108724225
         CurrentDate     =   40429
      End
      Begin MSComCtl2.DTPicker datPatheOrderProcessed 
         Height          =   315
         Left            =   6660
         TabIndex        =   301
         Tag             =   "NOCLEAR"
         ToolTipText     =   "The date the tape was created"
         Top             =   10200
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         _Version        =   393216
         CalendarBackColor=   12632319
         CalendarTitleBackColor=   12632319
         CheckBox        =   -1  'True
         CustomFormat    =   "dd/MM/yy HH:mm:ss"
         DateIsNull      =   -1  'True
         Format          =   108724225
         CurrentDate     =   40429
      End
      Begin VB.Label lblPatheInstructions 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   1755
         Left            =   240
         TabIndex        =   304
         Top             =   1320
         Width           =   16935
      End
      Begin VB.Label lblCaption 
         Caption         =   "Date Processed"
         Height          =   255
         Index           =   60
         Left            =   6660
         TabIndex        =   303
         Top             =   9960
         Width           =   1515
      End
      Begin VB.Label lblCaption 
         Caption         =   "Date Created"
         Height          =   255
         Index           =   59
         Left            =   4380
         TabIndex        =   302
         Top             =   9960
         Width           =   1515
      End
      Begin VB.Label lblCaption 
         Caption         =   "Pathe Order Number"
         Height          =   255
         Index           =   58
         Left            =   300
         TabIndex        =   291
         Top             =   9960
         Width           =   1515
      End
      Begin VB.Label lblOrderID 
         BackColor       =   &H00C0FFFF&
         Height          =   315
         Left            =   6060
         TabIndex        =   289
         Top             =   3480
         Visible         =   0   'False
         Width           =   1755
      End
      Begin VB.Label lblCaption 
         Caption         =   "Original Order Email Text"
         Height          =   255
         Index           =   57
         Left            =   300
         TabIndex        =   288
         Top             =   3480
         Width           =   3015
      End
      Begin VB.Label Label7 
         Caption         =   $"frmClipSearch.frx":08E6
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   3855
         Left            =   120
         TabIndex        =   286
         Top             =   420
         Width           =   16935
      End
   End
   Begin TabDlg.SSTab TabMediaSearching 
      Height          =   8175
      Left            =   0
      TabIndex        =   98
      Top             =   3240
      Width           =   25515
      _ExtentX        =   45006
      _ExtentY        =   14420
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Media Search Results"
      TabPicture(0)   =   "frmClipSearch.frx":09B7
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblTotalFileSize"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblCaption(22)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "ProgressBar1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "adoClip"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "cmdClearSelectedRows"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "chkTimecodeColumns"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "chkHideVersionAndLanguage"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "chkHideEpisodic"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "chkExtraFields"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "chkLongSearch"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "chkUnlockGrid"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "chkSeriesIds"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "chkDates"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "chkShowEventID"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "grdClips"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "chkShowMD5"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).ControlCount=   16
      TabCaption(1)   =   "Additional Search and Update Fields"
      TabPicture(1)   =   "frmClipSearch.frx":09D3
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Label2(3)"
      Tab(1).Control(1)=   "Label2(2)"
      Tab(1).Control(2)=   "Label2(1)"
      Tab(1).Control(3)=   "Label5"
      Tab(1).Control(4)=   "Label4"
      Tab(1).Control(5)=   "Label3"
      Tab(1).Control(6)=   "Label2(0)"
      Tab(1).Control(7)=   "Label1"
      Tab(1).Control(8)=   "lblCaption(30)"
      Tab(1).Control(9)=   "lblField6"
      Tab(1).Control(10)=   "lblField5"
      Tab(1).Control(11)=   "lblField4"
      Tab(1).Control(12)=   "lblField3"
      Tab(1).Control(13)=   "lblField2"
      Tab(1).Control(14)=   "lblField1"
      Tab(1).Control(15)=   "lblCaption(49)"
      Tab(1).Control(16)=   "lblCaption(43)"
      Tab(1).Control(17)=   "lblCaption(47)"
      Tab(1).Control(18)=   "lblCaption(21)"
      Tab(1).Control(19)=   "lblCaption(46)"
      Tab(1).Control(20)=   "lblCaption(31)"
      Tab(1).Control(21)=   "lblCaption(29)"
      Tab(1).Control(22)=   "lblCaption(28)"
      Tab(1).Control(23)=   "lblCaption(26)"
      Tab(1).Control(24)=   "lblCaption(20)"
      Tab(1).Control(25)=   "lblCaption(19)"
      Tab(1).Control(26)=   "lblCaption(18)"
      Tab(1).Control(27)=   "lblCaption(17)"
      Tab(1).Control(28)=   "lblCaption(0)"
      Tab(1).Control(29)=   "lblCaption(41)"
      Tab(1).Control(30)=   "lblCaption(40)"
      Tab(1).Control(31)=   "lblCaption(39)"
      Tab(1).Control(32)=   "lblCaption(36)"
      Tab(1).Control(33)=   "lblCaption(33)"
      Tab(1).Control(34)=   "lblCaption(32)"
      Tab(1).Control(35)=   "lblCaption(12)"
      Tab(1).Control(36)=   "lblCaption(11)"
      Tab(1).Control(37)=   "lblCaption(10)"
      Tab(1).Control(38)=   "lblCaption(7)"
      Tab(1).Control(39)=   "lblCaption(6)"
      Tab(1).Control(40)=   "lblCaption(35)"
      Tab(1).Control(41)=   "lblSourceFormat"
      Tab(1).Control(42)=   "lblCaption(16)"
      Tab(1).Control(43)=   "lblSourceLibraryID"
      Tab(1).Control(44)=   "lblCaption(15)"
      Tab(1).Control(45)=   "lblCaption(1)"
      Tab(1).Control(46)=   "lblCaption(56)"
      Tab(1).Control(47)=   "lblCaption(44)"
      Tab(1).Control(48)=   "cmbMediaSpecs"
      Tab(1).Control(49)=   "frmDangerous"
      Tab(1).Control(50)=   "Frame1"
      Tab(1).Control(51)=   "frmStorageOwnership"
      Tab(1).Control(52)=   "chkCustomFieldInclusive"
      Tab(1).Control(53)=   "chkExplicitText"
      Tab(1).Control(54)=   "chkBulkUpdate(24)"
      Tab(1).Control(55)=   "chkBulkUpdate(23)"
      Tab(1).Control(56)=   "chkBulkUpdate(22)"
      Tab(1).Control(57)=   "chkBulkUpdate(21)"
      Tab(1).Control(58)=   "chkBulkUpdate(20)"
      Tab(1).Control(59)=   "chkBulkUpdate(19)"
      Tab(1).Control(60)=   "cmbField6"
      Tab(1).Control(61)=   "cmbField5"
      Tab(1).Control(62)=   "cmbField4"
      Tab(1).Control(63)=   "cmbField3"
      Tab(1).Control(64)=   "cmbField2"
      Tab(1).Control(65)=   "cmbField1"
      Tab(1).Control(66)=   "chkBulkUpdate(53)"
      Tab(1).Control(67)=   "cmbProfileGroup"
      Tab(1).Control(68)=   "chkBulkUpdate(38)"
      Tab(1).Control(69)=   "cmbFileVersion"
      Tab(1).Control(70)=   "txtSQLQuery"
      Tab(1).Control(71)=   "chkBulkUpdate(48)"
      Tab(1).Control(72)=   "cmbVodPlatform"
      Tab(1).Control(73)=   "cmbStreamType"
      Tab(1).Control(74)=   "chkBulkUpdate(35)"
      Tab(1).Control(75)=   "chkBulkUpdate(34)"
      Tab(1).Control(76)=   "chkBulkUpdate(33)"
      Tab(1).Control(77)=   "chkBulkUpdate(32)"
      Tab(1).Control(78)=   "cmbInterlace"
      Tab(1).Control(79)=   "cmbPasses"
      Tab(1).Control(80)=   "cmbCbrVbr"
      Tab(1).Control(81)=   "chkBulkUpdate(28)"
      Tab(1).Control(82)=   "chkBulkUpdate(27)"
      Tab(1).Control(83)=   "chkBulkUpdate(26)"
      Tab(1).Control(84)=   "chkBulkUpdate(16)"
      Tab(1).Control(85)=   "chkBulkUpdate(15)"
      Tab(1).Control(86)=   "chkBulkUpdate(14)"
      Tab(1).Control(87)=   "chkBulkUpdate(8)"
      Tab(1).Control(88)=   "chkBulkUpdate(7)"
      Tab(1).Control(89)=   "chkBulkUpdate(6)"
      Tab(1).Control(90)=   "chkBulkUpdate(5)"
      Tab(1).Control(91)=   "chkBulkUpdate(4)"
      Tab(1).Control(92)=   "chkBulkUpdate(3)"
      Tab(1).Control(93)=   "chkBulkUpdate(2)"
      Tab(1).Control(94)=   "chkBulkUpdate(1)"
      Tab(1).Control(95)=   "txtVideoBitrate"
      Tab(1).Control(96)=   "txtAudioBitrate"
      Tab(1).Control(97)=   "cmbAudioCodec"
      Tab(1).Control(98)=   "txtNotes"
      Tab(1).Control(99)=   "cmbKeyword"
      Tab(1).Control(100)=   "cmbClipType"
      Tab(1).Control(101)=   "cmbGenre"
      Tab(1).Control(102)=   "cmbFrameRate"
      Tab(1).Control(103)=   "cmbPurpose"
      Tab(1).Control(104)=   "cmbGeometry"
      Tab(1).Control(105)=   "cmbAspect"
      Tab(1).Control(106)=   "txtBitrate"
      Tab(1).Control(107)=   "txtVertpixels"
      Tab(1).Control(108)=   "txtHorizpixels"
      Tab(1).Control(109)=   "cmbClipcodec"
      Tab(1).Control(110)=   "cmbClipformat"
      Tab(1).Control(111)=   "txtSourcebarcode"
      Tab(1).Control(112)=   "chkBulkUpdate(25)"
      Tab(1).Control(113)=   "txtInternalReference"
      Tab(1).Control(114)=   "txtSelectTop"
      Tab(1).Control(115)=   "txtMD5"
      Tab(1).ControlCount=   116
      Begin VB.TextBox txtMD5 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -73620
         TabIndex        =   330
         ToolTipText     =   "The clip filename"
         Top             =   4800
         Width           =   5055
      End
      Begin VB.CheckBox chkShowMD5 
         Alignment       =   1  'Right Justify
         Caption         =   "Show MD5"
         Height          =   195
         Left            =   15660
         TabIndex        =   314
         Top             =   720
         Width           =   1395
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdClips 
         Bindings        =   "frmClipSearch.frx":09EF
         Height          =   6975
         Left            =   60
         TabIndex        =   99
         Top             =   1140
         Width           =   25380
         _Version        =   196617
         stylesets.count =   5
         stylesets(0).Name=   "EncryptedFile"
         stylesets(0).BackColor=   12632319
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmClipSearch.frx":0A05
         stylesets(1).Name=   "OriginalMasterfile"
         stylesets(1).BackColor=   6487829
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmClipSearch.frx":0A21
         stylesets(2).Name=   "iTunesMasterfile"
         stylesets(2).BackColor=   14614478
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmClipSearch.frx":0A3D
         stylesets(3).Name=   "Masterfile"
         stylesets(3).BackColor=   11927441
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmClipSearch.frx":0A59
         stylesets(4).Name=   "EditedMasterfile"
         stylesets(4).BackColor=   13303728
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmClipSearch.frx":0A75
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowColumnSwapping=   2
         AllowGroupShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeRow   =   3
         StyleSet        =   "OriginalMasterfile"
         ForeColorEven   =   0
         BackColorOdd    =   12648447
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   50
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Clip Use"
         Columns(0).Name =   "purpose"
         Columns(0).DataField=   "eventtype"
         Columns(0).FieldLen=   256
         Columns(1).Width=   1588
         Columns(1).Caption=   "Series ID"
         Columns(1).Name =   "seriesID"
         Columns(1).DataField=   "seriesID"
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   1244
         Columns(2).Caption=   "Serial #"
         Columns(2).Name =   "serialnumber"
         Columns(2).DataField=   "serialnumber"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3704
         Columns(3).Caption=   "Clip Title"
         Columns(3).Name =   "eventtitle"
         Columns(3).DataField=   "eventtitle"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   1270
         Columns(4).Caption=   "Series"
         Columns(4).Name =   "eventseries"
         Columns(4).DataField=   "eventseries"
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   635
         Columns(5).Caption=   "Set"
         Columns(5).Name =   "eventset"
         Columns(5).DataField=   "eventset"
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   1270
         Columns(6).Caption=   "Episode"
         Columns(6).Name =   "eventepisode"
         Columns(6).DataField=   "eventepisode"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(7).Width=   2831
         Columns(7).Caption=   "Subtitle"
         Columns(7).Name =   "eventsubtitle"
         Columns(7).DataField=   "eventsubtitle"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(8).Width=   2117
         Columns(8).Caption=   "Version"
         Columns(8).Name =   "eventversion"
         Columns(8).DataField=   "eventversion"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).Locked=   -1  'True
         Columns(9).Width=   1931
         Columns(9).Caption=   "Language"
         Columns(9).Name =   "language"
         Columns(9).DataField=   "language"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(9).Locked=   -1  'True
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "resourceID"
         Columns(10).Name=   "resourceID"
         Columns(10).DataField=   "resourceID"
         Columns(10).FieldLen=   256
         Columns(10).Locked=   -1  'True
         Columns(11).Width=   3201
         Columns(11).Caption=   "Sales Delivery Profile"
         Columns(11).Name=   "portalpurchaseprofilegroup"
         Columns(11).FieldLen=   256
         Columns(11).Locked=   -1  'True
         Columns(12).Width=   2646
         Columns(12).Caption=   "Custom Field 1"
         Columns(12).Name=   "customfield1"
         Columns(12).DataField=   "customfield1"
         Columns(12).FieldLen=   256
         Columns(12).Locked=   -1  'True
         Columns(13).Width=   2646
         Columns(13).Caption=   "Custom Field 2"
         Columns(13).Name=   "customfield2"
         Columns(13).DataField=   "customfield2"
         Columns(13).FieldLen=   256
         Columns(13).Locked=   -1  'True
         Columns(14).Width=   2646
         Columns(14).Caption=   "customfield3"
         Columns(14).Name=   "customfield3"
         Columns(14).DataField=   "customfield3"
         Columns(14).FieldLen=   256
         Columns(14).Locked=   -1  'True
         Columns(15).Width=   2646
         Columns(15).Caption=   "customfield4"
         Columns(15).Name=   "customfield4"
         Columns(15).DataField=   "customfield4"
         Columns(15).FieldLen=   256
         Columns(15).Locked=   -1  'True
         Columns(16).Width=   2646
         Columns(16).Caption=   "customfield5"
         Columns(16).Name=   "customfield5"
         Columns(16).DataField=   "customfield5"
         Columns(16).FieldLen=   256
         Columns(16).Locked=   -1  'True
         Columns(17).Width=   2646
         Columns(17).Caption=   "customfield6"
         Columns(17).Name=   "customfield6"
         Columns(17).DataField=   "customfield6"
         Columns(17).FieldLen=   256
         Columns(17).Locked=   -1  'True
         Columns(18).Width=   1931
         Columns(18).Caption=   "Start Time"
         Columns(18).Name=   "timecodestart"
         Columns(18).DataField=   "timecodestart"
         Columns(18).FieldLen=   256
         Columns(18).Locked=   -1  'True
         Columns(19).Width=   1931
         Columns(19).Caption=   "Stop Time"
         Columns(19).Name=   "timecodestop"
         Columns(19).DataField=   "timecodestop"
         Columns(19).FieldLen=   256
         Columns(19).Locked=   -1  'True
         Columns(20).Width=   1931
         Columns(20).Caption=   "Duration"
         Columns(20).Name=   "fd_length"
         Columns(20).DataField=   "fd_length"
         Columns(20).FieldLen=   256
         Columns(20).Locked=   -1  'True
         Columns(21).Width=   1508
         Columns(21).Caption=   "Clip ID"
         Columns(21).Name=   "eventID"
         Columns(21).DataField=   "eventID"
         Columns(21).DataType=   17
         Columns(21).FieldLen=   256
         Columns(21).Locked=   -1  'True
         Columns(22).Width=   2646
         Columns(22).Caption=   "Tape_Barcode"
         Columns(22).Name=   "Tape_Barcode"
         Columns(22).DataField=   "Tape_Barcode"
         Columns(22).FieldLen=   256
         Columns(22).Locked=   -1  'True
         Columns(23).Width=   2117
         Columns(23).Caption=   "Format"
         Columns(23).Name=   "clipformat"
         Columns(23).DataField=   "clipformat"
         Columns(23).FieldLen=   256
         Columns(23).Locked=   -1  'True
         Columns(24).Width=   1931
         Columns(24).Caption=   "Codec"
         Columns(24).Name=   "clipcodec"
         Columns(24).DataField=   "clipcodec"
         Columns(24).FieldLen=   256
         Columns(24).Locked=   -1  'True
         Columns(25).Width=   873
         Columns(25).Caption=   "Hor"
         Columns(25).Name=   "horiz"
         Columns(25).DataField=   "cliphorizontalpixels"
         Columns(25).FieldLen=   256
         Columns(25).Locked=   -1  'True
         Columns(26).Width=   873
         Columns(26).Caption=   "Ver"
         Columns(26).Name=   "vert"
         Columns(26).DataField=   "clipverticalpixels"
         Columns(26).FieldLen=   256
         Columns(26).Locked=   -1  'True
         Columns(27).Width=   1323
         Columns(27).Caption=   "Bit Rate"
         Columns(27).Name=   "clipbitrate"
         Columns(27).DataField=   "clipbitrate"
         Columns(27).FieldLen=   256
         Columns(27).Locked=   -1  'True
         Columns(28).Width=   979
         Columns(28).Caption=   "Fr R"
         Columns(28).Name=   "clipframerate"
         Columns(28).DataField=   "clipframerate"
         Columns(28).FieldLen=   256
         Columns(28).Locked=   -1  'True
         Columns(29).Width=   4948
         Columns(29).Caption=   "Reference"
         Columns(29).Name=   "clipreference"
         Columns(29).DataField=   "clipreference"
         Columns(29).FieldLen=   256
         Columns(29).Locked=   -1  'True
         Columns(30).Width=   4577
         Columns(30).Caption=   "Alt Folder"
         Columns(30).Name=   "altlocation"
         Columns(30).DataField=   "altlocation"
         Columns(30).FieldLen=   256
         Columns(30).Locked=   -1  'True
         Columns(31).Width=   5106
         Columns(31).Caption=   "Clip Filename"
         Columns(31).Name=   "clipfilename"
         Columns(31).DataField=   "clipfilename"
         Columns(31).FieldLen=   256
         Columns(31).Locked=   -1  'True
         Columns(32).Width=   2858
         Columns(32).Caption=   "Clip Store"
         Columns(32).Name=   "barcode"
         Columns(32).FieldLen=   256
         Columns(32).Locked=   -1  'True
         Columns(33).Width=   2646
         Columns(33).Caption=   "File Size (Bytes)"
         Columns(33).Name=   "filesize"
         Columns(33).Alignment=   1
         Columns(33).DataField=   "bigfilesize"
         Columns(33).DataType=   6
         Columns(33).NumberFormat=   "#,##0"
         Columns(33).FieldLen=   256
         Columns(33).Locked=   -1  'True
         Columns(34).Width=   3175
         Columns(34).Caption=   "Date Created"
         Columns(34).Name=   "cdate"
         Columns(34).DataField=   "cdate"
         Columns(34).DataType=   7
         Columns(34).FieldLen=   256
         Columns(35).Width=   2566
         Columns(35).Caption=   "Company Name"
         Columns(35).Name=   "companyname"
         Columns(35).DataField=   "companyname"
         Columns(35).DataType=   8
         Columns(35).FieldLen=   256
         Columns(35).Locked=   -1  'True
         Columns(36).Width=   794
         Columns(36).Caption=   "Hide"
         Columns(36).Name=   "hidefromweb"
         Columns(36).Alignment=   2
         Columns(36).DataField=   "hidefromweb"
         Columns(36).DataType=   11
         Columns(36).FieldLen=   256
         Columns(36).Locked=   -1  'True
         Columns(36).Style=   2
         Columns(37).Width=   3200
         Columns(37).Visible=   0   'False
         Columns(37).Caption=   "libraryID"
         Columns(37).Name=   "libraryID"
         Columns(37).DataField=   "libraryID"
         Columns(37).FieldLen=   256
         Columns(38).Width=   794
         Columns(38).Caption=   "Lock"
         Columns(38).Name=   "FileLocked"
         Columns(38).DataField=   "sound_stereo_russian"
         Columns(38).FieldLen=   256
         Columns(38).Style=   2
         Columns(39).Width=   794
         Columns(39).Caption=   "md5"
         Columns(39).Name=   "MD5"
         Columns(39).Alignment=   2
         Columns(39).DataField=   "MD5present"
         Columns(39).FieldLen=   256
         Columns(39).Locked=   -1  'True
         Columns(39).Style=   2
         Columns(40).Width=   3200
         Columns(40).Visible=   0   'False
         Columns(40).Caption=   "AuA"
         Columns(40).Name=   "AudioAnalysis"
         Columns(40).Alignment=   2
         Columns(40).FieldLen=   256
         Columns(40).Locked=   -1  'True
         Columns(40).Style=   2
         Columns(41).Width=   794
         Columns(41).Caption=   "sha1"
         Columns(41).Name=   "SHA1"
         Columns(41).DataField=   "sha1present"
         Columns(41).FieldLen=   256
         Columns(41).Style=   2
         Columns(42).Width=   794
         Columns(42).Caption=   "TR"
         Columns(42).Name=   "techrev"
         Columns(42).Alignment=   2
         Columns(42).DataField=   "Column 23"
         Columns(42).DataType=   8
         Columns(42).FieldLen=   256
         Columns(42).Locked=   -1  'True
         Columns(42).Style=   2
         Columns(43).Width=   794
         Columns(43).Caption=   "MW"
         Columns(43).Name=   "MW"
         Columns(43).DataField=   "MW"
         Columns(43).FieldLen=   256
         Columns(43).Locked=   -1  'True
         Columns(43).Style=   2
         Columns(44).Width=   3200
         Columns(44).Visible=   0   'False
         Columns(44).Caption=   "fileversion"
         Columns(44).Name=   "fileversion"
         Columns(44).DataField=   "fileversion"
         Columns(44).FieldLen=   256
         Columns(45).Width=   3200
         Columns(45).Visible=   0   'False
         Columns(45).Caption=   "DW"
         Columns(45).Name=   "DW"
         Columns(45).DataField=   "DW"
         Columns(45).FieldLen=   256
         Columns(45).Locked=   -1  'True
         Columns(45).Style=   2
         Columns(46).Width=   3200
         Columns(46).Visible=   0   'False
         Columns(46).Caption=   "companyID"
         Columns(46).Name=   "companyID"
         Columns(46).DataField=   "companyID"
         Columns(46).FieldLen=   256
         Columns(47).Width=   3200
         Columns(47).Visible=   0   'False
         Columns(47).Caption=   "Del"
         Columns(47).Name=   "system_deleted"
         Columns(47).DataField=   "system_deleted"
         Columns(47).FieldLen=   256
         Columns(47).Style=   2
         Columns(48).Width=   5477
         Columns(48).Caption=   "md5 checksum"
         Columns(48).Name=   "md5checksum"
         Columns(48).DataField=   "md5checksum"
         Columns(48).FieldLen=   256
         Columns(49).Width=   3175
         Columns(49).Caption=   "AutoDeleteDate"
         Columns(49).Name=   "AutoDeleteDate"
         Columns(49).DataField=   "AutoDeleteDate"
         Columns(49).DataType=   7
         Columns(49).FieldLen=   256
         _ExtentX        =   44767
         _ExtentY        =   12303
         _StockProps     =   79
         Caption         =   "Clip List"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox txtSelectTop 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -52800
         TabIndex        =   281
         ToolTipText     =   "Barcode of the source library item for the clip"
         Top             =   2100
         Width           =   1695
      End
      Begin VB.CheckBox chkShowEventID 
         Alignment       =   1  'Right Justify
         Caption         =   "Show Clip IDs"
         Height          =   195
         Left            =   15660
         TabIndex        =   270
         Tag             =   "NOCLEAR"
         Top             =   420
         Value           =   1  'Checked
         Width           =   1395
      End
      Begin VB.CheckBox chkDates 
         Alignment       =   1  'Right Justify
         Caption         =   "Show Dates"
         Height          =   195
         Left            =   8400
         TabIndex        =   250
         Top             =   720
         Width           =   1575
      End
      Begin VB.CheckBox chkSeriesIds 
         Alignment       =   1  'Right Justify
         Caption         =   "Show Series IDs"
         Height          =   195
         Left            =   8400
         TabIndex        =   249
         Top             =   420
         Width           =   1575
      End
      Begin VB.TextBox txtInternalReference 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -66240
         TabIndex        =   238
         ToolTipText     =   "Job number for which the clip was made"
         Top             =   1560
         Width           =   1695
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   25
         Left            =   -64500
         TabIndex        =   237
         Top             =   1620
         Width           =   195
      End
      Begin VB.TextBox txtSourcebarcode 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -66240
         TabIndex        =   231
         ToolTipText     =   "Barcode of the source library item for the clip"
         Top             =   480
         Width           =   1695
      End
      Begin VB.ComboBox cmbClipformat 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -73620
         TabIndex        =   209
         ToolTipText     =   "The Clip Format"
         Top             =   1560
         Width           =   1695
      End
      Begin VB.ComboBox cmbClipcodec 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -73620
         TabIndex        =   208
         ToolTipText     =   "The Clip Codec"
         Top             =   1920
         Width           =   1695
      End
      Begin VB.TextBox txtHorizpixels 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -73620
         TabIndex        =   207
         ToolTipText     =   "The number of horizontal pixels"
         Top             =   3000
         Width           =   1695
      End
      Begin VB.TextBox txtVertpixels 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -73620
         TabIndex        =   206
         ToolTipText     =   "The number of vertical pixels"
         Top             =   3360
         Width           =   1695
      End
      Begin VB.TextBox txtBitrate 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -70260
         TabIndex        =   205
         ToolTipText     =   "The clip bitrate"
         Top             =   3720
         Width           =   1695
      End
      Begin VB.ComboBox cmbAspect 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -70260
         TabIndex        =   204
         ToolTipText     =   "The Clip Codec"
         Top             =   840
         Width           =   1695
      End
      Begin VB.ComboBox cmbGeometry 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -70260
         TabIndex        =   203
         ToolTipText     =   "The Clip Codec"
         Top             =   1200
         Width           =   1695
      End
      Begin VB.ComboBox cmbPurpose 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -73620
         TabIndex        =   202
         ToolTipText     =   "The Clip Codec"
         Top             =   1200
         Width           =   1695
      End
      Begin VB.ComboBox cmbFrameRate 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -73620
         TabIndex        =   201
         ToolTipText     =   "The Clip Codec"
         Top             =   3720
         Width           =   1695
      End
      Begin VB.ComboBox cmbGenre 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -73620
         TabIndex        =   200
         ToolTipText     =   "The Version for the Tape"
         Top             =   480
         Width           =   1695
      End
      Begin VB.ComboBox cmbClipType 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -73620
         TabIndex        =   199
         ToolTipText     =   "The Version for the Tape"
         Top             =   840
         Width           =   1695
      End
      Begin VB.ComboBox cmbKeyword 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -73620
         TabIndex        =   198
         ToolTipText     =   "The Version for the Tape"
         Top             =   4440
         Width           =   5055
      End
      Begin VB.TextBox txtNotes 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -73620
         TabIndex        =   197
         ToolTipText     =   "The clip filename"
         Top             =   4080
         Width           =   5055
      End
      Begin VB.ComboBox cmbAudioCodec 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -70260
         TabIndex        =   196
         ToolTipText     =   "The Clip Codec"
         Top             =   3000
         Width           =   1695
      End
      Begin VB.TextBox txtAudioBitrate 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -70260
         TabIndex        =   195
         ToolTipText     =   "The clip bitrate"
         Top             =   3360
         Width           =   1695
      End
      Begin VB.TextBox txtVideoBitrate 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -70260
         TabIndex        =   194
         ToolTipText     =   "The clip bitrate"
         Top             =   1560
         Width           =   1695
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   1
         Left            =   -71880
         TabIndex        =   193
         Top             =   1620
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   2
         Left            =   -71880
         TabIndex        =   192
         Top             =   1980
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   3
         Left            =   -71880
         TabIndex        =   191
         Top             =   1260
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   4
         Left            =   -71880
         TabIndex        =   190
         Top             =   3060
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   5
         Left            =   -71880
         TabIndex        =   189
         Top             =   3420
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   6
         Left            =   -68520
         TabIndex        =   188
         Top             =   3780
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   7
         Left            =   -68520
         TabIndex        =   187
         Top             =   900
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   8
         Left            =   -68520
         TabIndex        =   186
         Top             =   1260
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   14
         Left            =   -71880
         TabIndex        =   185
         Top             =   3780
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   15
         Left            =   -71880
         TabIndex        =   184
         Top             =   540
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   16
         Left            =   -71880
         TabIndex        =   183
         Top             =   900
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   26
         Left            =   -68520
         TabIndex        =   182
         Top             =   3060
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   27
         Left            =   -68520
         TabIndex        =   181
         Top             =   1620
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   28
         Left            =   -68520
         TabIndex        =   180
         Top             =   3420
         Width           =   195
      End
      Begin VB.ComboBox cmbCbrVbr 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -73620
         TabIndex        =   179
         ToolTipText     =   "The Clip Codec"
         Top             =   2640
         Width           =   1695
      End
      Begin VB.ComboBox cmbPasses 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -73620
         TabIndex        =   178
         ToolTipText     =   "The Clip Codec"
         Top             =   2280
         Width           =   1695
      End
      Begin VB.ComboBox cmbInterlace 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -70260
         TabIndex        =   177
         ToolTipText     =   "The Clip Codec"
         Top             =   480
         Width           =   1695
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   32
         Left            =   -71880
         TabIndex        =   176
         Top             =   2340
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   33
         Left            =   -71880
         TabIndex        =   175
         Top             =   2700
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   34
         Left            =   -68520
         TabIndex        =   174
         Top             =   540
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   35
         Left            =   -68520
         TabIndex        =   173
         Top             =   1980
         Width           =   195
      End
      Begin VB.TextBox cmbStreamType 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -70260
         TabIndex        =   172
         ToolTipText     =   "The clip bitrate"
         Top             =   1920
         Width           =   1695
      End
      Begin VB.ComboBox cmbVodPlatform 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -70260
         TabIndex        =   171
         ToolTipText     =   "The Clip Codec"
         Top             =   2280
         Width           =   1695
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   48
         Left            =   -68520
         TabIndex        =   170
         Top             =   2340
         Width           =   195
      End
      Begin VB.TextBox txtSQLQuery 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -58200
         MultiLine       =   -1  'True
         TabIndex        =   165
         ToolTipText     =   "The clip filename"
         Top             =   2100
         Width           =   3675
      End
      Begin VB.ComboBox cmbFileVersion 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -66240
         TabIndex        =   162
         ToolTipText     =   "The Clip Codec"
         Top             =   1920
         Width           =   3495
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   38
         Left            =   -62700
         TabIndex        =   161
         Top             =   1980
         Width           =   195
      End
      Begin VB.ComboBox cmbProfileGroup 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -66240
         TabIndex        =   160
         ToolTipText     =   "The Clip Codec"
         Top             =   2280
         Width           =   3495
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   255
         Index           =   53
         Left            =   -62700
         TabIndex        =   159
         Top             =   2310
         Width           =   195
      End
      Begin VB.ComboBox cmbField1 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -66240
         TabIndex        =   143
         ToolTipText     =   "The Clip Codec"
         Top             =   2640
         Width           =   3495
      End
      Begin VB.ComboBox cmbField2 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -66240
         TabIndex        =   142
         ToolTipText     =   "The Clip Codec"
         Top             =   3000
         Width           =   3495
      End
      Begin VB.ComboBox cmbField3 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -66240
         TabIndex        =   141
         ToolTipText     =   "The Clip Codec"
         Top             =   3360
         Width           =   3495
      End
      Begin VB.ComboBox cmbField4 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -66240
         TabIndex        =   140
         ToolTipText     =   "The Clip Codec"
         Top             =   3720
         Width           =   3495
      End
      Begin VB.ComboBox cmbField5 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -66240
         TabIndex        =   139
         ToolTipText     =   "The Clip Codec"
         Top             =   4080
         Width           =   3495
      End
      Begin VB.ComboBox cmbField6 
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   -66240
         TabIndex        =   138
         ToolTipText     =   "The Clip Codec"
         Top             =   4440
         Width           =   3495
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   19
         Left            =   -62700
         TabIndex        =   137
         Top             =   2700
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   20
         Left            =   -62700
         TabIndex        =   136
         Top             =   3060
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   21
         Left            =   -62700
         TabIndex        =   135
         Top             =   3420
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   22
         Left            =   -62700
         TabIndex        =   134
         Top             =   3780
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   23
         Left            =   -62700
         TabIndex        =   133
         Top             =   4140
         Width           =   195
      End
      Begin VB.CheckBox chkBulkUpdate 
         Caption         =   "Check1"
         Height          =   195
         Index           =   24
         Left            =   -62700
         TabIndex        =   132
         Top             =   4500
         Width           =   195
      End
      Begin VB.CheckBox chkExplicitText 
         Alignment       =   1  'Right Justify
         Caption         =   "Cus Fld Exact Matches"
         Height          =   255
         Left            =   -66240
         TabIndex        =   131
         Top             =   4800
         Width           =   1935
      End
      Begin VB.CheckBox chkCustomFieldInclusive 
         Alignment       =   1  'Right Justify
         Caption         =   "Inclusive Searches"
         Height          =   255
         Left            =   -64200
         TabIndex        =   130
         Top             =   4800
         Width           =   1695
      End
      Begin VB.Frame frmStorageOwnership 
         Caption         =   "Storage Ownership"
         Height          =   1155
         Left            =   -59460
         TabIndex        =   126
         Top             =   420
         Width           =   3855
         Begin VB.OptionButton optStorageOwner 
            Caption         =   "Not Relevant"
            Height          =   255
            Index           =   0
            Left            =   180
            TabIndex        =   129
            Top             =   240
            Value           =   -1  'True
            Width           =   2235
         End
         Begin VB.OptionButton optStorageOwner 
            Caption         =   "Owned by JCA"
            Height          =   255
            Index           =   1
            Left            =   180
            TabIndex        =   128
            Top             =   540
            Width           =   2235
         End
         Begin VB.OptionButton optStorageOwner 
            Caption         =   "Not Owned by JCA"
            Height          =   255
            Index           =   2
            Left            =   180
            TabIndex        =   127
            Top             =   840
            Width           =   2235
         End
      End
      Begin VB.Frame Frame1 
         Height          =   2055
         Left            =   -61680
         TabIndex        =   119
         Top             =   420
         Width           =   2115
         Begin VB.OptionButton optHidden 
            Caption         =   "All Clips"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   125
            Top             =   180
            Value           =   -1  'True
            Width           =   1635
         End
         Begin VB.OptionButton optHidden 
            Caption         =   "Hidden From Web"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   124
            Top             =   480
            Width           =   1635
         End
         Begin VB.OptionButton optHidden 
            Caption         =   "Visible On Web"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   123
            Top             =   780
            Width           =   1635
         End
         Begin VB.OptionButton optHidden 
            Caption         =   "Assigned On MW"
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   122
            Top             =   1080
            Width           =   1635
         End
         Begin VB.OptionButton optHidden 
            Caption         =   "Not Assigned On MW"
            Height          =   255
            Index           =   4
            Left            =   120
            TabIndex        =   121
            Top             =   1380
            Width           =   1935
         End
         Begin VB.OptionButton optHidden 
            Caption         =   "Not On SMVSTORE"
            ForeColor       =   &H000000FF&
            Height          =   255
            Index           =   5
            Left            =   120
            TabIndex        =   120
            Top             =   1680
            Width           =   1935
         End
      End
      Begin VB.Frame frmDangerous 
         Caption         =   "Dangerous Stuff - Does not Search - only Bulk Updates"
         Height          =   4935
         Left            =   -61680
         TabIndex        =   108
         Top             =   2640
         Visible         =   0   'False
         Width           =   5955
         Begin VB.CheckBox chkSupressNewReference 
            Alignment       =   1  'Right Justify
            Caption         =   "Supress Regenerating of References after Renames"
            Height          =   195
            Left            =   90
            TabIndex        =   280
            Top             =   3420
            Width           =   4695
         End
         Begin VB.CommandButton cmdClearRadioButtons 
            Caption         =   "Clear Submits"
            Height          =   555
            Left            =   4860
            TabIndex        =   279
            Top             =   2760
            Width           =   915
         End
         Begin VB.OptionButton optSubmitMoveRequests 
            Alignment       =   1  'Right Justify
            Caption         =   "Submit Folder Changes as Requests (discard NameChanges)"
            Height          =   195
            Left            =   90
            TabIndex        =   278
            Top             =   3120
            Width           =   4695
         End
         Begin VB.OptionButton optSubmitRenameRequests 
            Alignment       =   1  'Right Justify
            Caption         =   "Submit Name Changes as Requests (discard Folder Changes)"
            Height          =   195
            Left            =   90
            TabIndex        =   277
            Top             =   2820
            Width           =   4695
         End
         Begin VB.CommandButton cmdRelinkTechReviews 
            Caption         =   "Relink Tech Reviews based on filename and filesize only."
            Height          =   495
            Left            =   120
            TabIndex        =   246
            Top             =   3900
            Width           =   4695
         End
         Begin VB.TextBox txtThumbnail 
            BackColor       =   &H00C0C0FF&
            Height          =   315
            Left            =   1320
            TabIndex        =   116
            ToolTipText     =   "Alternative file location if not in client number folder"
            Top             =   300
            Width           =   3315
         End
         Begin VB.CheckBox chkBulkUpdate 
            Caption         =   "Check1"
            Height          =   195
            Index           =   36
            Left            =   4680
            TabIndex        =   115
            Top             =   360
            Width           =   195
         End
         Begin VB.CheckBox chkFourByThree 
            Alignment       =   1  'Right Justify
            Caption         =   "4x3 Flag"
            Height          =   315
            Left            =   90
            TabIndex        =   114
            Top             =   660
            Width           =   1455
         End
         Begin VB.CheckBox chkBulkUpdate 
            Caption         =   "Check1"
            Height          =   195
            Index           =   37
            Left            =   4680
            TabIndex        =   113
            Top             =   720
            Width           =   195
         End
         Begin VB.CommandButton cmdBulkAssignment 
            Caption         =   "Bulk Assignment"
            Height          =   495
            Left            =   3780
            TabIndex        =   112
            ToolTipText     =   "Clear the form"
            Top             =   1920
            Width           =   1035
         End
         Begin VB.CheckBox chkUnlockFolderAndFile 
            Alignment       =   1  'Right Justify
            Caption         =   "Unlock Folder and Filename"
            Height          =   195
            Left            =   90
            TabIndex        =   110
            Top             =   2520
            Width           =   4695
         End
         Begin VB.CommandButton cmdExpireUnassignedClips 
            Caption         =   "Grid - Expire all clips not assigned to Media Window"
            Height          =   495
            Left            =   120
            TabIndex        =   109
            Top             =   1320
            Width           =   4695
         End
         Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPortalUser 
            Height          =   315
            Left            =   1320
            TabIndex        =   111
            Top             =   2040
            Width           =   2415
            DataFieldList   =   "fullname"
            _Version        =   196617
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4419
            Columns(0).Caption=   "fullname"
            Columns(0).Name =   "fullname"
            Columns(0).DataField=   "fullname"
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "portaluserID"
            Columns(1).Name =   "portaluserID"
            Columns(1).DataField=   "portaluserID"
            Columns(1).FieldLen=   256
            _ExtentX        =   4260
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   12632319
            DataFieldToDisplay=   "fullname"
         End
         Begin VB.Label lblCaption 
            Caption         =   "Thumbnail File"
            Height          =   255
            Index           =   37
            Left            =   120
            TabIndex        =   118
            Top             =   360
            Width           =   1155
         End
         Begin VB.Label lblCaption 
            Caption         =   "Portal User"
            Height          =   255
            Index           =   38
            Left            =   120
            TabIndex        =   117
            Top             =   2100
            Width           =   1155
         End
         Begin VB.Line Line1 
            X1              =   120
            X2              =   4800
            Y1              =   1860
            Y2              =   1860
         End
      End
      Begin VB.CheckBox chkUnlockGrid 
         Alignment       =   1  'Right Justify
         Caption         =   "Unlock Some Columns"
         Height          =   195
         Left            =   5820
         TabIndex        =   107
         Top             =   720
         Width           =   1935
      End
      Begin VB.CheckBox chkLongSearch 
         Alignment       =   1  'Right Justify
         Caption         =   "Allow Long Searches"
         Height          =   195
         Left            =   5880
         TabIndex        =   106
         Top             =   420
         Width           =   1875
      End
      Begin VB.CheckBox chkExtraFields 
         Alignment       =   1  'Right Justify
         Caption         =   "Show Extra Columns"
         Height          =   195
         Left            =   10740
         TabIndex        =   105
         Top             =   720
         Width           =   1815
      End
      Begin VB.CheckBox chkHideEpisodic 
         Alignment       =   1  'Right Justify
         Caption         =   "Hide Episodic Fields"
         Height          =   195
         Left            =   13740
         TabIndex        =   104
         Top             =   720
         Width           =   1695
      End
      Begin VB.CheckBox chkHideVersionAndLanguage 
         Alignment       =   1  'Right Justify
         Caption         =   "Hide Version and Language Fields"
         Height          =   195
         Left            =   12720
         TabIndex        =   103
         Top             =   420
         Width           =   2715
      End
      Begin VB.CheckBox chkTimecodeColumns 
         Alignment       =   1  'Right Justify
         Caption         =   "Show Timecode Columns"
         Height          =   195
         Left            =   10440
         TabIndex        =   102
         Top             =   420
         Width           =   2115
      End
      Begin VB.CommandButton cmdClearSelectedRows 
         Caption         =   "Clear Row Selections"
         Height          =   315
         Left            =   3120
         TabIndex        =   101
         Top             =   720
         Width           =   1995
      End
      Begin MSAdodcLib.Adodc adoClip 
         Height          =   330
         Left            =   60
         Top             =   720
         Width           =   3000
         _ExtentX        =   5292
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "0 Records Found"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSComctlLib.ProgressBar ProgressBar1 
         Height          =   315
         Left            =   17520
         TabIndex        =   100
         Top             =   420
         Visible         =   0   'False
         Width           =   7875
         _ExtentX        =   13891
         _ExtentY        =   556
         _Version        =   393216
         Appearance      =   1
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbMediaSpecs 
         Height          =   315
         Left            =   -58200
         TabIndex        =   166
         ToolTipText     =   "The company this tape belongs to"
         Top             =   1740
         Width           =   3705
         DataFieldList   =   "mediaspecname"
         MaxDropDownItems=   24
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "companyID"
         Columns(0).Name =   "mediaspecID"
         Columns(0).DataField=   "mediaspecID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   7938
         Columns(1).Caption=   "mediaspecname"
         Columns(1).Name =   "mediaspecname"
         Columns(1).DataField=   "mediaspecname"
         Columns(1).FieldLen=   256
         _ExtentX        =   6535
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "mediaspecname"
      End
      Begin VB.Label lblCaption 
         Caption         =   "MD5"
         Height          =   255
         Index           =   44
         Left            =   -74820
         TabIndex        =   329
         Top             =   4860
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "SQL Return Top"
         Height          =   255
         Index           =   56
         Left            =   -54120
         TabIndex        =   282
         Top             =   2130
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Total Filesize"
         Height          =   195
         Index           =   22
         Left            =   120
         TabIndex        =   247
         Top             =   120
         Width           =   975
      End
      Begin VB.Label lblCaption 
         Caption         =   "Internal Ref"
         Height          =   255
         Index           =   1
         Left            =   -67920
         TabIndex        =   239
         Top             =   1620
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Source Barcode"
         Height          =   255
         Index           =   15
         Left            =   -67920
         TabIndex        =   236
         Top             =   540
         Width           =   1155
      End
      Begin VB.Label lblSourceLibraryID 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   -66240
         TabIndex        =   235
         Tag             =   "CLEARFIELDS"
         Top             =   840
         Width           =   1695
      End
      Begin VB.Label lblCaption 
         Caption         =   "Source Lib.ID"
         Height          =   255
         Index           =   16
         Left            =   -67920
         TabIndex        =   234
         Top             =   900
         Width           =   1155
      End
      Begin VB.Label lblSourceFormat 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   -66240
         TabIndex        =   233
         Tag             =   "CLEARFIELDS"
         Top             =   1200
         Width           =   1695
      End
      Begin VB.Label lblCaption 
         Caption         =   "Source Format"
         Height          =   255
         Index           =   35
         Left            =   -67920
         TabIndex        =   232
         Top             =   1260
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Clip Format"
         Height          =   255
         Index           =   6
         Left            =   -74820
         TabIndex        =   230
         Top             =   1620
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Video Codec"
         Height          =   255
         Index           =   7
         Left            =   -74820
         TabIndex        =   229
         Top             =   1980
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Horiz Pixels"
         Height          =   255
         Index           =   10
         Left            =   -74820
         TabIndex        =   228
         Top             =   3060
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Vert Pixels"
         Height          =   255
         Index           =   11
         Left            =   -74820
         TabIndex        =   227
         Top             =   3420
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Bitrate (kbps)"
         Height          =   255
         Index           =   12
         Left            =   -71520
         TabIndex        =   226
         Top             =   3780
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Aspect Ratio"
         Height          =   255
         Index           =   32
         Left            =   -71520
         TabIndex        =   225
         Top             =   900
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Geometry"
         Height          =   255
         Index           =   33
         Left            =   -71520
         TabIndex        =   224
         Top             =   1260
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Clip Purpose"
         Height          =   255
         Index           =   36
         Left            =   -74820
         TabIndex        =   223
         Top             =   1260
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Frame Rate"
         Height          =   255
         Index           =   39
         Left            =   -74820
         TabIndex        =   222
         Top             =   3780
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Genre"
         Height          =   255
         Index           =   40
         Left            =   -74820
         TabIndex        =   221
         Top             =   540
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Clip Type"
         Height          =   255
         Index           =   41
         Left            =   -74820
         TabIndex        =   220
         Top             =   900
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Keywords"
         Height          =   255
         Index           =   0
         Left            =   -74820
         TabIndex        =   219
         Top             =   4500
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Notes"
         Height          =   255
         Index           =   17
         Left            =   -74820
         TabIndex        =   218
         Top             =   4140
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Video Bitrate"
         Height          =   255
         Index           =   18
         Left            =   -71520
         TabIndex        =   217
         Top             =   1620
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Audio Codec"
         Height          =   255
         Index           =   19
         Left            =   -71520
         TabIndex        =   216
         Top             =   3060
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Audio Bitrate"
         Height          =   255
         Index           =   20
         Left            =   -71520
         TabIndex        =   215
         Top             =   3420
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "No. of Passes"
         Height          =   255
         Index           =   26
         Left            =   -74820
         TabIndex        =   214
         Top             =   2340
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "CBR / VBR"
         Height          =   255
         Index           =   28
         Left            =   -74820
         TabIndex        =   213
         Top             =   2700
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "Interlace"
         Height          =   255
         Index           =   29
         Left            =   -71520
         TabIndex        =   212
         Top             =   540
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Stream Type"
         Height          =   255
         Index           =   31
         Left            =   -71520
         TabIndex        =   211
         Top             =   1980
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "VOD Platform"
         Height          =   255
         Index           =   46
         Left            =   -71520
         TabIndex        =   210
         Top             =   2340
         Width           =   1095
      End
      Begin VB.Label lblTotalFileSize 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   60
         TabIndex        =   169
         Tag             =   "CLEARFIELDS"
         Top             =   360
         Width           =   5535
      End
      Begin VB.Label lblCaption 
         Caption         =   "Direct SQL Qry"
         Height          =   255
         Index           =   21
         Left            =   -59400
         TabIndex        =   168
         Top             =   2130
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Media Specs"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   47
         Left            =   -59400
         TabIndex        =   167
         Top             =   1800
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "File Version"
         Height          =   255
         Index           =   43
         Left            =   -67920
         TabIndex        =   164
         Top             =   1980
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Sales Delivery Profile"
         Height          =   255
         Index           =   49
         Left            =   -67920
         TabIndex        =   163
         Top             =   2340
         Width           =   1695
      End
      Begin VB.Label lblField1 
         Caption         =   "Custom Field 1"
         Height          =   255
         Left            =   -67920
         TabIndex        =   158
         Tag             =   "1"
         Top             =   2700
         Width           =   1635
      End
      Begin VB.Label lblField2 
         Caption         =   "Custom Field 2"
         Height          =   255
         Left            =   -67920
         TabIndex        =   157
         Tag             =   "2"
         Top             =   3060
         Width           =   1635
      End
      Begin VB.Label lblField3 
         Caption         =   "Custom Field 3"
         Height          =   255
         Left            =   -67920
         TabIndex        =   156
         Tag             =   "3"
         Top             =   3420
         Width           =   1695
      End
      Begin VB.Label lblField4 
         Caption         =   "Custom Field 4"
         Height          =   255
         Left            =   -67920
         TabIndex        =   155
         Tag             =   "4"
         Top             =   3780
         Width           =   1695
      End
      Begin VB.Label lblField5 
         Caption         =   "Custom Field 5"
         Height          =   255
         Left            =   -67920
         TabIndex        =   154
         Tag             =   "5"
         Top             =   4140
         Width           =   1695
      End
      Begin VB.Label lblField6 
         Caption         =   "Custom Field 6"
         Height          =   255
         Left            =   -67920
         TabIndex        =   153
         Tag             =   "6"
         Top             =   4500
         Width           =   1695
      End
      Begin VB.Label lblCaption 
         Caption         =   "--"
         Height          =   255
         Index           =   30
         Left            =   -62400
         TabIndex        =   152
         Top             =   2700
         Width           =   255
      End
      Begin VB.Label Label1 
         Caption         =   "--"
         Height          =   255
         Left            =   -62400
         TabIndex        =   151
         Top             =   4500
         Width           =   255
      End
      Begin VB.Label Label2 
         Caption         =   "|"
         Height          =   195
         Index           =   0
         Left            =   -62280
         TabIndex        =   150
         Top             =   2880
         Width           =   195
      End
      Begin VB.Label Label3 
         Caption         =   "|"
         Height          =   195
         Left            =   -62280
         TabIndex        =   149
         Top             =   3120
         Width           =   195
      End
      Begin VB.Label Label4 
         Caption         =   "|"
         Height          =   195
         Left            =   -62280
         TabIndex        =   148
         Top             =   3360
         Width           =   195
      End
      Begin VB.Label Label5 
         Caption         =   "|"
         Height          =   195
         Left            =   -62280
         TabIndex        =   147
         Top             =   3600
         Width           =   195
      End
      Begin VB.Label Label2 
         Caption         =   "|"
         Height          =   195
         Index           =   1
         Left            =   -62280
         TabIndex        =   146
         Top             =   3840
         Width           =   195
      End
      Begin VB.Label Label2 
         Caption         =   "|"
         Height          =   195
         Index           =   2
         Left            =   -62280
         TabIndex        =   145
         Top             =   4080
         Width           =   195
      End
      Begin VB.Label Label2 
         Caption         =   "|"
         Height          =   195
         Index           =   3
         Left            =   -62280
         TabIndex        =   144
         Top             =   4320
         Width           =   195
      End
   End
   Begin VB.CommandButton cmdPrepareYouTubeSend 
      Caption         =   "Prepare YouTube Send"
      Height          =   375
      Left            =   20925
      TabIndex        =   336
      ToolTipText     =   "Send items in grid to Latimer Road"
      Top             =   1740
      Visible         =   0   'False
      Width           =   2000
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   47
      Left            =   8400
      TabIndex        =   332
      Top             =   2220
      Width           =   195
   End
   Begin VB.ComboBox cmbLanguage 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   4860
      TabIndex        =   331
      ToolTipText     =   "The Clip Codec"
      Top             =   2160
      Width           =   3495
   End
   Begin VB.CheckBox chkBulkUpdate 
      Alignment       =   1  'Right Justify
      Caption         =   "(Bulk Update Locks)"
      Height          =   195
      Index           =   39
      Left            =   6780
      TabIndex        =   328
      Top             =   2580
      Width           =   1815
   End
   Begin VB.CheckBox chkFileLock 
      Alignment       =   1  'Right Justify
      Caption         =   "File Locks"
      Height          =   255
      Left            =   3960
      TabIndex        =   327
      Top             =   2580
      Width           =   1095
   End
   Begin VB.CommandButton cmdExportToLatimer 
      Caption         =   "Export To S3 STORAGE"
      Height          =   375
      Left            =   20925
      TabIndex        =   324
      ToolTipText     =   "Send items in grid to S3 / VIDA"
      Top             =   1320
      Visible         =   0   'False
      Width           =   2000
   End
   Begin VB.CommandButton cmdMakeGenericTrackerLines 
      Caption         =   "Generic Tracker"
      Height          =   375
      Left            =   17700
      TabIndex        =   323
      ToolTipText     =   "Duplicate the Tape info into a new Tape"
      Top             =   2160
      Width           =   1335
   End
   Begin VB.CommandButton cmdMakeThumbnails 
      Caption         =   "Make Image Thumbs"
      Height          =   375
      Left            =   19095
      TabIndex        =   97
      ToolTipText     =   "Submit Thumbnail Requests for all items in the Grid"
      Top             =   1320
      Width           =   1755
   End
   Begin VB.CommandButton cmdInitiateWorkflow 
      Caption         =   "Initiate Workflow"
      Height          =   375
      Left            =   19095
      TabIndex        =   264
      ToolTipText     =   "Duplicate the Tape info into a new Tape"
      Top             =   2160
      Width           =   1755
   End
   Begin VB.CommandButton cmdBatonReports 
      Caption         =   "Send for Baton QC"
      Height          =   375
      Left            =   19095
      TabIndex        =   320
      ToolTipText     =   "Submit Thumbnail Requests for all items in the Grid"
      Top             =   1740
      Width           =   1755
   End
   Begin VB.CommandButton cmdBulkTranscodeMOConvert 
      Caption         =   "MO Bulk Transcode"
      Height          =   375
      Left            =   19095
      TabIndex        =   319
      ToolTipText     =   "Make Transcode requests for all items in the Grid"
      Top             =   900
      Width           =   1755
   End
   Begin VB.CommandButton cmdMd5GridWhenNoMD5 
      Caption         =   "Make new MD5"
      Height          =   375
      Left            =   17700
      TabIndex        =   317
      ToolTipText     =   "Make MD5 Requests for items in the Grid"
      Top             =   900
      Width           =   1335
   End
   Begin VB.CommandButton cmdReleaseBBCMGClips 
      Caption         =   "Release BBCMG Clips"
      Height          =   795
      Left            =   20580
      TabIndex        =   313
      Top             =   13860
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Frame fraYouTubeWarning 
      Height          =   1035
      Left            =   240
      TabIndex        =   88
      Top             =   14460
      Visible         =   0   'False
      Width           =   4455
      Begin VB.Label Label8 
         Caption         =   "YouTube XML files in Progress Please Do Not Use"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   795
         Left            =   120
         TabIndex        =   89
         Top             =   180
         Width           =   4275
      End
   End
   Begin VB.Frame FraMediaInfoWarning 
      Height          =   1035
      Left            =   4740
      TabIndex        =   84
      Top             =   14460
      Visible         =   0   'False
      Width           =   4395
      Begin VB.Label Label6 
         Caption         =   "MediaInfo Query in Progress Please Do Not Use"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   795
         Left            =   60
         TabIndex        =   85
         Top             =   180
         Width           =   4275
      End
   End
   Begin VB.CommandButton cmdSpecialCommand 
      Caption         =   "Special Command"
      Height          =   375
      Left            =   17640
      TabIndex        =   272
      Top             =   14280
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.CommandButton cmdSendVia360 
      Caption         =   "Send Via Aspera 360"
      Height          =   375
      Left            =   12720
      TabIndex        =   271
      ToolTipText     =   "Send items in the Grid via Aspera 360 P2P"
      Top             =   15060
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.TextBox txtAssignedJobID 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   1260
      TabIndex        =   268
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   2160
      Width           =   2220
   End
   Begin VB.CommandButton cmdFindLongClips 
      Caption         =   "Find Long Clips"
      Height          =   375
      Left            =   25260
      TabIndex        =   267
      Top             =   60
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CommandButton cmdSendWithContentDelivery 
      Caption         =   "Send by Content Delivery"
      Height          =   375
      Left            =   20925
      TabIndex        =   266
      ToolTipText     =   "Send items in the Grid via Content Delivery"
      Top             =   480
      Visible         =   0   'False
      Width           =   2000
   End
   Begin VB.CommandButton cmdFTPToSmartjog 
      Caption         =   "Send to Smartjog "
      Height          =   375
      Left            =   20925
      TabIndex        =   265
      ToolTipText     =   "Send items in the Grid via ftp to Smartjog System"
      Top             =   900
      Visible         =   0   'False
      Width           =   2000
   End
   Begin VB.CommandButton cmdAssignTape_Barcode 
      Caption         =   "Assign Tape_Barcode"
      Height          =   555
      Left            =   9720
      TabIndex        =   263
      Top             =   14580
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.CommandButton cmdSendGridViaAspera 
      Caption         =   "Send Via Aspera P2P"
      Height          =   375
      Left            =   12600
      TabIndex        =   262
      ToolTipText     =   "Send items in the Grid via Aspera P2P"
      Top             =   14640
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CheckBox chkAssignedButNotClearedMasters 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      Caption         =   "Assigned not Cleared Masters"
      Height          =   195
      Left            =   12540
      TabIndex        =   261
      Top             =   1740
      Width           =   2535
   End
   Begin VB.CheckBox chkAssignedAndClearedProducts 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      Caption         =   "Assigned and Cleared Products"
      Height          =   195
      Left            =   12540
      TabIndex        =   260
      Top             =   2340
      Width           =   2535
   End
   Begin VB.CheckBox chkAssignedAndClearedMasters 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      Caption         =   "Assigned and Cleared Masters"
      Height          =   195
      Left            =   12540
      TabIndex        =   258
      Top             =   2040
      Width           =   2535
   End
   Begin VB.CheckBox chkUnassignedMasters 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      Caption         =   "Unassigned Masters"
      Height          =   195
      Left            =   12540
      TabIndex        =   257
      Top             =   1440
      Width           =   2535
   End
   Begin VB.CheckBox chkJobIDisNull 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      Caption         =   "JobsID is Null"
      Height          =   195
      Left            =   1260
      TabIndex        =   256
      Top             =   1860
      Width           =   1395
   End
   Begin VB.CommandButton cmdSendGridViaDeliverySystem 
      Caption         =   "Send Via Delivery System"
      Height          =   375
      Left            =   20925
      TabIndex        =   255
      ToolTipText     =   "Send items in the Grid via a Delivery System"
      Top             =   60
      Visible         =   0   'False
      Width           =   2000
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Set Title from reference where title is blank for Grid"
      Height          =   795
      Left            =   25260
      TabIndex        =   252
      Top             =   480
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   45
      Left            =   8400
      TabIndex        =   244
      Top             =   1140
      Width           =   195
   End
   Begin VB.TextBox txtSerialNumber 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   7200
      TabIndex        =   243
      ToolTipText     =   "The sub title (if known)"
      Top             =   1080
      Width           =   1155
   End
   Begin VB.CommandButton cmdDreamWorksAudioDeliveries 
      Caption         =   "Dreamworks Audio Deliveries"
      Height          =   795
      Left            =   15300
      TabIndex        =   96
      Top             =   14340
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.CommandButton cmdKidscoBulkTranscode 
      Caption         =   "Kidsco Proxy Trans"
      Height          =   375
      Left            =   15300
      TabIndex        =   95
      Top             =   15240
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   54
      Left            =   3600
      TabIndex        =   94
      Top             =   1140
      Width           =   195
   End
   Begin VB.TextBox txtSvenskProjectNumber 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   1260
      TabIndex        =   92
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   1080
      Width           =   2220
   End
   Begin VB.CommandButton cmdBulkTranscodeProgOnly 
      Caption         =   "Prog Only Bulk Trans"
      Height          =   375
      Left            =   19095
      TabIndex        =   91
      ToolTipText     =   "Make Prog Only  Transcode requests for all items in the Grid (Program Item timecodes must be logged)"
      Top             =   480
      Width           =   1755
   End
   Begin VB.CommandButton cmdMediaInfoPreserveTimecode 
      Caption         =   "Mediaifo No TC"
      Height          =   375
      Left            =   17700
      TabIndex        =   90
      ToolTipText     =   "Do MediaInfo of items in the Grid without Timecode Overwrite"
      Top             =   1740
      Width           =   1335
   End
   Begin VB.CommandButton cmdYouTubeXML 
      Caption         =   "Make Pathe YouTube XMLs"
      Height          =   375
      Left            =   9900
      TabIndex        =   87
      Top             =   15420
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.CommandButton cmdMediaInfo 
      Caption         =   "Mediaifo inc TC"
      Height          =   375
      Left            =   17700
      TabIndex        =   83
      ToolTipText     =   "Do MediaInfo of items in the Grid with Timecode Overwrite"
      Top             =   1320
      Width           =   1335
   End
   Begin VB.CheckBox chkSerNull 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      Caption         =   "SeriesID Null"
      Height          =   195
      Left            =   8760
      TabIndex        =   82
      Top             =   1140
      Width           =   1200
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   49
      Left            =   6000
      TabIndex        =   80
      Top             =   1140
      Width           =   195
   End
   Begin VB.TextBox txtSeriesID 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   4860
      TabIndex        =   78
      ToolTipText     =   "The sub title (if known)"
      Top             =   1080
      Width           =   1035
   End
   Begin VB.CommandButton cmdExport 
      Caption         =   "Export Grid"
      Height          =   375
      Left            =   20925
      TabIndex        =   77
      Top             =   2160
      Width           =   2000
   End
   Begin VB.CommandButton cmdMd5Grid 
      Caption         =   "Delete MD5"
      Height          =   375
      Index           =   1
      Left            =   17700
      TabIndex        =   76
      ToolTipText     =   "Delet existing MD5 records for items in the Grid"
      Top             =   480
      Width           =   1335
   End
   Begin VB.CommandButton cmdBulkTranscode 
      Caption         =   "Bulk Transcode"
      Height          =   375
      Left            =   19095
      TabIndex        =   75
      ToolTipText     =   "Make Transcode requests for all items in the Grid"
      Top             =   60
      Width           =   1755
   End
   Begin VB.CommandButton cmdMd5Grid 
      Caption         =   "Make MD5"
      Height          =   375
      Index           =   0
      Left            =   17700
      TabIndex        =   74
      ToolTipText     =   "Make MD5 Requests for items in the Grid"
      Top             =   60
      Width           =   1335
   End
   Begin VB.CheckBox chkNoMD5 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      Caption         =   "MD5 Not Made"
      Height          =   195
      Left            =   15900
      TabIndex        =   73
      Top             =   1140
      Width           =   1635
   End
   Begin VB.CommandButton cmdZeroUsage 
      Caption         =   "Make Zero date Event Usage Entries for items in the grid that don't have one"
      Height          =   1035
      Left            =   18060
      TabIndex        =   69
      Top             =   14820
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.CheckBox chkTitleNull 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      Caption         =   "Title is Null"
      Height          =   195
      Left            =   8760
      TabIndex        =   68
      Top             =   420
      Width           =   1635
   End
   Begin VB.CheckBox chkCompanyNull 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      Caption         =   "Company Null"
      Height          =   195
      Left            =   10020
      TabIndex        =   67
      Top             =   60
      Width           =   1335
   End
   Begin VB.CheckBox chkKeyframeNull 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      Caption         =   "Keyframe is Null"
      Height          =   195
      Left            =   15900
      TabIndex        =   64
      Top             =   420
      Width           =   1635
   End
   Begin VB.CommandButton cmdCompile 
      Caption         =   "Add To Compilation"
      Height          =   375
      Left            =   21420
      TabIndex        =   62
      Top             =   15360
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.CommandButton cmdClearCompany 
      Caption         =   "Clr"
      Height          =   315
      Left            =   8700
      TabIndex        =   59
      Top             =   0
      Width           =   375
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   31
      Left            =   8400
      TabIndex        =   55
      Top             =   780
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   30
      Left            =   8400
      TabIndex        =   54
      Top             =   1500
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   29
      Left            =   8400
      TabIndex        =   53
      Top             =   420
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   18
      Left            =   5580
      TabIndex        =   52
      Top             =   780
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   17
      Left            =   7020
      TabIndex        =   51
      Top             =   780
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   13
      Left            =   15540
      TabIndex        =   50
      Top             =   780
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   12
      Left            =   15540
      TabIndex        =   49
      Top             =   60
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   11
      Left            =   15540
      TabIndex        =   48
      Top             =   1140
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   10
      Left            =   8400
      TabIndex        =   47
      Top             =   1860
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   9
      Left            =   8400
      TabIndex        =   46
      Top             =   60
      Width           =   195
   End
   Begin VB.CheckBox chkBulkUpdate 
      Caption         =   "Check1"
      Height          =   195
      Index           =   0
      Left            =   3600
      TabIndex        =   45
      Top             =   1500
      Width           =   195
   End
   Begin VB.CheckBox chkLocIsNull 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      Caption         =   "Location is Null"
      Height          =   195
      Left            =   15900
      TabIndex        =   41
      Top             =   780
      Width           =   1635
   End
   Begin VB.CheckBox chkRefNull 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      Caption         =   "Reference is Null"
      Height          =   195
      Left            =   15900
      TabIndex        =   38
      Top             =   60
      Width           =   1635
   End
   Begin VB.TextBox txtClockNumber 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   12525
      TabIndex        =   30
      ToolTipText     =   "A reference for the clip (readers digest)"
      Top             =   1080
      Width           =   3000
   End
   Begin VB.TextBox txtReference 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   12525
      TabIndex        =   27
      ToolTipText     =   "A reference for the clip (readers digest)"
      Top             =   0
      Width           =   3000
   End
   Begin VB.TextBox txtJobID 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   1260
      TabIndex        =   23
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   1440
      Width           =   2220
   End
   Begin VB.TextBox txtAltFolder 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   12525
      TabIndex        =   29
      ToolTipText     =   "Alternative file location if not in client number folder"
      Top             =   720
      Width           =   3000
   End
   Begin VB.TextBox txtClipfilename 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   12525
      TabIndex        =   28
      ToolTipText     =   "The clip filename"
      Top             =   360
      Width           =   3000
   End
   Begin VB.PictureBox picFooter 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Left            =   4320
      ScaleHeight     =   2175
      ScaleWidth      =   19035
      TabIndex        =   13
      Top             =   11520
      Width           =   19035
      Begin VB.CheckBox chkMoveThenForget 
         Alignment       =   1  'Right Justify
         Caption         =   "Move Then Forget"
         Height          =   255
         Left            =   13320
         TabIndex        =   345
         Top             =   1920
         Visible         =   0   'False
         Width           =   2025
      End
      Begin VB.CommandButton cmdDEleteEmptyFolders 
         Caption         =   "Clear Empty Folders"
         Height          =   555
         Left            =   16740
         TabIndex        =   344
         ToolTipText     =   "Delete all the items in the Grid"
         Top             =   1200
         Width           =   1095
      End
      Begin VB.CheckBox chkUnverifiedMedia 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0FF&
         Caption         =   "Unverified"
         Height          =   195
         Left            =   2160
         TabIndex        =   326
         Top             =   1440
         Width           =   2355
      End
      Begin VB.CheckBox chkSearchArchive 
         Alignment       =   1  'Right Justify
         Caption         =   "Search Deleted Clips"
         Height          =   195
         Left            =   2160
         TabIndex        =   325
         Top             =   1200
         Width           =   2355
      End
      Begin VB.CommandButton cmdBulkAddAudioData 
         Caption         =   "Bulk Add Prog Audio Data"
         Height          =   555
         Left            =   11280
         TabIndex        =   322
         ToolTipText     =   "Bulk Update the Chosen Changes to all items in the Grid"
         Top             =   0
         Width           =   1395
      End
      Begin VB.CommandButton cmdBulkAddProgData 
         Caption         =   "Bulk Add Prog Logging Data"
         Height          =   555
         Left            =   9840
         TabIndex        =   321
         ToolTipText     =   "Bulk Update the Chosen Changes to all items in the Grid"
         Top             =   0
         Width           =   1395
      End
      Begin VB.CommandButton cmdMoveAndChangeOwner 
         Caption         =   "Move Grid and Change Owner"
         Height          =   555
         Left            =   14160
         TabIndex        =   316
         ToolTipText     =   "Move all items in the Grid to another Store"
         Top             =   1200
         Width           =   1275
      End
      Begin VB.CommandButton cmdCopyGridAndChangeOwner 
         Caption         =   "Copy Grid and Change Owner"
         Height          =   555
         Left            =   12780
         TabIndex        =   315
         ToolTipText     =   "Copy all items in the Grid to another Store"
         Top             =   1200
         Width           =   1335
      End
      Begin VB.CommandButton cmdDeleteSelected 
         Caption         =   "Delete Selected"
         Height          =   555
         Left            =   16740
         TabIndex        =   312
         ToolTipText     =   "Delete all the items in the Grid"
         Top             =   0
         Width           =   1095
      End
      Begin VB.CommandButton cmdMoveToPrivate 
         Height          =   555
         Left            =   4620
         Picture         =   "frmClipSearch.frx":0A91
         Style           =   1  'Graphical
         TabIndex        =   311
         ToolTipText     =   "Bulk Update the Chosen Changes to all items in the Grid"
         Top             =   1200
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.CommandButton cmdMoveToOps 
         Height          =   555
         Left            =   5880
         Picture         =   "frmClipSearch.frx":10C7
         Style           =   1  'Graphical
         TabIndex        =   310
         ToolTipText     =   "Bulk Update the Chosen Changes to all items in the Grid"
         Top             =   1200
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.CommandButton cmdMoveToEdit 
         Height          =   555
         Left            =   7140
         Picture         =   "frmClipSearch.frx":16F2
         Style           =   1  'Graphical
         TabIndex        =   309
         ToolTipText     =   "Bulk Update the Chosen Changes to all items in the Grid"
         Top             =   1200
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.CommandButton cmdMoveToBVOD 
         Height          =   555
         Left            =   8400
         Picture         =   "frmClipSearch.frx":1D11
         Style           =   1  'Graphical
         TabIndex        =   308
         ToolTipText     =   "Bulk Update the Chosen Changes to all items in the Grid"
         Top             =   1200
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.CheckBox chkRecordUpdateOnly 
         Alignment       =   1  'Right Justify
         Caption         =   "Only Update File Records"
         Height          =   255
         Left            =   5940
         TabIndex        =   307
         Top             =   1920
         Width           =   2265
      End
      Begin VB.CheckBox chkFutureRequest 
         Alignment       =   1  'Right Justify
         Caption         =   "Future Request"
         Height          =   255
         Left            =   9900
         TabIndex        =   305
         Top             =   1920
         Width           =   1545
      End
      Begin VB.CommandButton cmdPatheOrders 
         Caption         =   "Pathe Orders"
         Height          =   555
         Left            =   3660
         TabIndex        =   297
         ToolTipText     =   "Open the Pathe sub-form"
         Top             =   0
         Width           =   855
      End
      Begin VB.CommandButton cmdMoveSelectedToAutoDelete 
         Caption         =   "Move Sel to AutoDelete"
         Enabled         =   0   'False
         Height          =   555
         Left            =   15480
         TabIndex        =   284
         ToolTipText     =   "Move Selected items in the Grid to an AutoDelete Store"
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdMoveGridToAutoDelete 
         Caption         =   "Move Grid to AutoDelete"
         Enabled         =   0   'False
         Height          =   555
         Left            =   15480
         TabIndex        =   283
         ToolTipText     =   "Move all items in the Grid to an AutoDelete Store"
         Top             =   600
         Width           =   1215
      End
      Begin VB.CommandButton cmdMoveSelected 
         Caption         =   "Move Selected"
         Height          =   555
         Left            =   14160
         TabIndex        =   276
         ToolTipText     =   "Move Selected items in the Grid to another Store"
         Top             =   0
         Width           =   1275
      End
      Begin VB.CommandButton cmdCopySelected 
         Caption         =   "Copy Selected"
         Height          =   555
         Left            =   12780
         TabIndex        =   275
         ToolTipText     =   "Copy Selected items in the Grid to another Store"
         Top             =   0
         Width           =   1335
      End
      Begin VB.CommandButton cmdAssignToJob 
         Caption         =   "Assign to Job as Masters"
         Height          =   555
         Left            =   9840
         TabIndex        =   259
         ToolTipText     =   "Assign the grid to a Job as Required Masters"
         Top             =   600
         Width           =   1395
      End
      Begin VB.CheckBox chkSupressValidateMessages 
         Alignment       =   1  'Right Justify
         Caption         =   "Supress Filename Validation Messages"
         Height          =   1155
         Left            =   120
         TabIndex        =   251
         Top             =   300
         Width           =   1125
      End
      Begin VB.CommandButton cmdBulkUpdateSelected 
         Caption         =   "Bulk Update Selected Rows"
         Height          =   555
         Left            =   8400
         TabIndex        =   86
         ToolTipText     =   "Bulk Update the Chosen Changes to the Selected Items in the Grid"
         Top             =   0
         Width           =   1395
      End
      Begin VB.CommandButton cmdShowDangerous 
         Caption         =   "Show Dangerous"
         Height          =   555
         Left            =   1380
         TabIndex        =   63
         ToolTipText     =   "Show the Additional Dangerous Controls"
         Top             =   600
         Width           =   1215
      End
      Begin VB.CommandButton cmdHideGrid 
         Caption         =   "Hide/Unhide Grid from Web"
         Height          =   555
         Left            =   11280
         TabIndex        =   57
         ToolTipText     =   "Set the 'Hidden from Web' flags"
         Top             =   600
         Width           =   1395
      End
      Begin VB.CommandButton cmdVerifyGrid 
         Caption         =   "Verify Media Grid"
         Height          =   555
         Left            =   4620
         TabIndex        =   56
         ToolTipText     =   "Verify the items in the Grid"
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdPortalSanitise 
         Caption         =   "Sanitise Portal Access"
         Height          =   555
         Left            =   4620
         TabIndex        =   40
         ToolTipText     =   "Sanitise COntent Delivery Permissions from all items in the Grid"
         Top             =   600
         Width           =   1215
      End
      Begin VB.CommandButton cmdSanitiseOnlineStatus 
         Caption         =   "Sanitise Online Status"
         Height          =   555
         Left            =   5880
         TabIndex        =   39
         ToolTipText     =   "Sanitise the Online Status of all Iitems in the Grid"
         Top             =   600
         Width           =   1215
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   555
         Left            =   1380
         TabIndex        =   37
         ToolTipText     =   "Print the Report of the Grid Items"
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdBulkUpdate 
         Caption         =   "Bulk Update Grid"
         Height          =   555
         Left            =   8400
         TabIndex        =   36
         ToolTipText     =   "Bulk Update the Chosen Changes to all items in the Grid"
         Top             =   600
         Width           =   1395
      End
      Begin VB.CommandButton cmdDeleteGrid 
         Caption         =   "Delete Grid"
         Height          =   555
         Left            =   16740
         TabIndex        =   32
         ToolTipText     =   "Delete all the items in the Grid"
         Top             =   600
         Width           =   1095
      End
      Begin VB.CommandButton cmdMoveGrid 
         Caption         =   "Move Grid"
         Height          =   555
         Left            =   14160
         TabIndex        =   31
         ToolTipText     =   "Move all items in the Grid to another Store"
         Top             =   600
         Width           =   1275
      End
      Begin VB.CommandButton cmdCopyGrid 
         Caption         =   "Copy Grid"
         Height          =   555
         Left            =   12780
         TabIndex        =   26
         ToolTipText     =   "Copy all items in the Grid to another Store"
         Top             =   600
         Width           =   1335
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search"
         Height          =   555
         Left            =   3660
         TabIndex        =   21
         ToolTipText     =   "Search for Media Items"
         Top             =   600
         Width           =   855
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   555
         Left            =   17880
         TabIndex        =   15
         ToolTipText     =   "Close the Form"
         Top             =   600
         Width           =   1095
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   555
         Left            =   2700
         TabIndex        =   14
         ToolTipText     =   "Clear the form"
         Top             =   600
         Width           =   855
      End
   End
   Begin VB.ComboBox cmbVersion 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   4860
      TabIndex        =   6
      ToolTipText     =   "The Version for the Tape"
      Top             =   1800
      Width           =   3495
   End
   Begin VB.TextBox txtSubTitle 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   4860
      TabIndex        =   5
      ToolTipText     =   "The sub title (if known)"
      Top             =   1440
      Width           =   3495
   End
   Begin VB.ComboBox cmbEpisode 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   7680
      TabIndex        =   4
      ToolTipText     =   "The Episode for the Tape"
      Top             =   720
      Width           =   675
   End
   Begin VB.ComboBox cmbSeries 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   4860
      TabIndex        =   3
      ToolTipText     =   "The Series for the Tape"
      Top             =   720
      Width           =   675
   End
   Begin VB.ComboBox cmbSet 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   6300
      TabIndex        =   2
      ToolTipText     =   "The Series for the Tape"
      Top             =   720
      Width           =   675
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   4860
      TabIndex        =   0
      ToolTipText     =   "The company this tape belongs to"
      Top             =   0
      Width           =   3495
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "accountcode"
      Columns(2).Name =   "accountcode"
      Columns(2).DataField=   "accountcode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "telephone"
      Columns(3).Name =   "telephone"
      Columns(3).DataField=   "telephone"
      Columns(3).FieldLen=   256
      _ExtentX        =   6165
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   12632319
      DataFieldToDisplay=   "name"
   End
   Begin VB.Frame frmStorageType 
      BorderStyle     =   0  'None
      Caption         =   "JCA201110173"
      Height          =   2115
      Left            =   180
      TabIndex        =   42
      Top             =   11580
      Width           =   3975
      Begin VB.CheckBox chkNotOnLTO 
         Caption         =   "not on LTO Tape"
         Height          =   195
         Left            =   60
         TabIndex        =   343
         Top             =   840
         Width           =   1695
      End
      Begin VB.CheckBox chkNotOnUSBDisk 
         Caption         =   "not on USB Disk"
         Height          =   195
         Left            =   60
         TabIndex        =   342
         Top             =   360
         Width           =   1695
      End
      Begin VB.CheckBox chkAlsoOnLTO 
         Caption         =   "also on LTO Tape"
         Height          =   195
         Left            =   60
         TabIndex        =   341
         Top             =   600
         Width           =   1695
      End
      Begin VB.CheckBox chkAlsoOnUSBDisk 
         Caption         =   "also on USB Disk"
         Height          =   195
         Left            =   60
         TabIndex        =   340
         Top             =   120
         Width           =   1695
      End
      Begin VB.CheckBox chkConfirmedAtLatimer 
         Caption         =   "Confirmed on S3-STORAGE"
         Height          =   195
         Left            =   1980
         TabIndex        =   339
         Top             =   1320
         Visible         =   0   'False
         Width           =   1995
      End
      Begin VB.CheckBox chkNotOnOtherDISC 
         Caption         =   "not on other DISC"
         Height          =   195
         Left            =   1980
         TabIndex        =   338
         Top             =   1800
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.CheckBox chkAlsoOnOtherDISC 
         Caption         =   "also on other DISC"
         Height          =   195
         Left            =   1980
         TabIndex        =   337
         Top             =   1560
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.CheckBox chkAlsoAtLatimer 
         Caption         =   "also on S3-STORAGE"
         Height          =   195
         Left            =   1980
         TabIndex        =   335
         Top             =   120
         Width           =   1875
      End
      Begin VB.CheckBox chkNotAtLatimer 
         Caption         =   "not on S3-STORAGE"
         Height          =   195
         Left            =   1980
         TabIndex        =   334
         Top             =   360
         Width           =   1795
      End
      Begin VB.OptionButton optStorageType 
         Caption         =   "LTO 7"
         Height          =   255
         Index           =   6
         Left            =   1980
         TabIndex        =   274
         Tag             =   "SET"
         Top             =   1080
         Width           =   795
      End
      Begin VB.OptionButton optStorageType 
         Caption         =   "LTO 6"
         Height          =   255
         Index           =   2
         Left            =   1980
         TabIndex        =   273
         Tag             =   "SET"
         Top             =   840
         Width           =   795
      End
      Begin VB.OptionButton optStorageType 
         Caption         =   "DIVA"
         Height          =   255
         Index           =   4
         Left            =   60
         TabIndex        =   254
         Top             =   1320
         Width           =   1695
      End
      Begin VB.OptionButton optStorageType 
         Caption         =   "LTO 5"
         Height          =   255
         Index           =   3
         Left            =   1980
         TabIndex        =   72
         Tag             =   "SET"
         Top             =   600
         Width           =   795
      End
      Begin VB.OptionButton optStorageType 
         Caption         =   "MOBILEDISC"
         Height          =   255
         Index           =   1
         Left            =   60
         TabIndex        =   65
         Tag             =   "SET"
         Top             =   1560
         Width           =   1695
      End
      Begin VB.CommandButton cmdClearOnline 
         Caption         =   "Clr"
         Height          =   375
         Left            =   3000
         TabIndex        =   60
         Top             =   780
         Width           =   375
      End
      Begin VB.OptionButton optStorageType 
         Caption         =   "DISCSTORE"
         Height          =   255
         Index           =   0
         Left            =   60
         TabIndex        =   44
         Tag             =   "SET"
         Top             =   1080
         Value           =   -1  'True
         Width           =   1695
      End
      Begin VB.OptionButton optStorageType 
         Caption         =   "Other Storage"
         Height          =   255
         Index           =   5
         Left            =   60
         TabIndex        =   43
         Top             =   1800
         Width           =   1695
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbBarcode 
      Height          =   315
      Left            =   1260
      TabIndex        =   58
      Top             =   0
      Width           =   2220
      DataFieldList   =   "Column 0"
      MaxDropDownItems=   56
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      HeadLines       =   0
      BackColorOdd    =   12632319
      RowHeight       =   423
      Columns(0).Width=   4233
      Columns(0).Caption=   "barcode"
      Columns(0).Name =   "barcode"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   3916
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   12632319
      DataFieldToDisplay=   "Column 0"
   End
   Begin MSComCtl2.DTPicker datLastUsageBefore 
      Height          =   315
      Left            =   9840
      TabIndex        =   70
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The date the tape was created"
      Top             =   720
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CalendarBackColor=   12632319
      CalendarTitleBackColor=   12632319
      CheckBox        =   -1  'True
      CustomFormat    =   "dd/MM/yy HH:mm:ss"
      DateIsNull      =   -1  'True
      Format          =   108724225
      CurrentDate     =   40429
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo txtTitle 
      Height          =   315
      Left            =   4860
      TabIndex        =   81
      ToolTipText     =   "The company this tape belongs to"
      Top             =   360
      Width           =   3495
      DataFieldList   =   "title"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "masterseriestitleID"
      Columns(0).Name =   "masterseriestitleID"
      Columns(0).DataField=   "masterseriestitleID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   1773
      Columns(1).Caption=   "seriesID"
      Columns(1).Name =   "seriesID"
      Columns(1).DataField=   "seriesID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   8149
      Columns(2).Caption=   "title"
      Columns(2).Name =   "title"
      Columns(2).DataField=   "title"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "companyID"
      Columns(3).Name =   "companyID"
      Columns(3).DataField=   "companyID"
      Columns(3).FieldLen=   256
      _ExtentX        =   6165
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   12632319
      DataFieldToDisplay=   "title"
   End
   Begin VB.CheckBox chkSerialNoIsNull 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      Caption         =   "Serial # Null"
      Height          =   195
      Left            =   10095
      TabIndex        =   245
      Top             =   1140
      Width           =   1200
   End
   Begin VB.CommandButton cmdReport 
      Caption         =   "Output Report"
      Height          =   375
      Left            =   23000
      TabIndex        =   66
      Top             =   1740
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CommandButton cmdDreamWorks 
      Caption         =   "Make DreamWorks xls"
      Height          =   375
      Left            =   23000
      TabIndex        =   240
      Top             =   60
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CommandButton cmdBreakpointReport 
      Caption         =   "Make DRG Breakpoint xls"
      Height          =   375
      Left            =   23000
      TabIndex        =   241
      Top             =   480
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CommandButton cmdMakeBulkGoogleXMLs 
      Caption         =   "Make Google Episode XMLs"
      Height          =   375
      Left            =   23000
      TabIndex        =   253
      Top             =   1320
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CommandButton cmdOutputGridAsXML 
      Caption         =   "Output Grid as XML"
      Height          =   375
      Left            =   23000
      TabIndex        =   306
      Top             =   900
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CommandButton cmdSendViaFaspex 
      Caption         =   "Send via FaspEx"
      Height          =   375
      Left            =   23000
      TabIndex        =   318
      ToolTipText     =   "Send items in the Grid via FaspEx"
      Top             =   2160
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label lblCaption 
      Caption         =   "Language"
      Height          =   255
      Index           =   45
      Left            =   3960
      TabIndex        =   333
      Top             =   2220
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Assigned Job #"
      Height          =   255
      Index           =   55
      Left            =   60
      TabIndex        =   269
      Top             =   2160
      Width           =   1215
   End
   Begin VB.Label lblCurrentFilename 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   2460
      TabIndex        =   248
      Tag             =   "CLEARFIELDS"
      Top             =   3000
      Width           =   17775
   End
   Begin VB.Label lblCaption 
      Caption         =   "Serial #"
      Height          =   255
      Index           =   5
      Left            =   6540
      TabIndex        =   242
      Top             =   1140
      Width           =   615
   End
   Begin VB.Label lblCaption 
      Caption         =   "Sv.Pr #"
      Height          =   255
      Index           =   50
      Left            =   60
      TabIndex        =   93
      Top             =   1140
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Series ID"
      Height          =   255
      Index           =   48
      Left            =   3960
      TabIndex        =   79
      Top             =   1140
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Last Used Bef"
      Height          =   255
      Index           =   42
      Left            =   8760
      TabIndex        =   71
      Top             =   780
      Width           =   1035
   End
   Begin VB.Label lblSearchStatus 
      Appearance      =   0  'Flat
      Caption         =   "Searching Live Clips"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   120
      TabIndex        =   61
      Top             =   3000
      Width           =   2895
   End
   Begin VB.Label lblCaption 
      Caption         =   "Format"
      Height          =   255
      Index           =   34
      Left            =   60
      TabIndex        =   35
      Top             =   780
      Width           =   1095
   End
   Begin VB.Label lblFormat 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   1260
      TabIndex        =   34
      Tag             =   "CLEARFIELDS"
      Top             =   720
      Width           =   2220
   End
   Begin VB.Label lblCaption 
      Caption         =   "Clock No"
      Height          =   255
      Index           =   27
      Left            =   11670
      TabIndex        =   33
      Top             =   1140
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Reference"
      Height          =   255
      Index           =   25
      Left            =   11670
      TabIndex        =   25
      Top             =   60
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "CETA Job No"
      Height          =   255
      Index           =   24
      Left            =   60
      TabIndex        =   24
      Top             =   1500
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Folder"
      Height          =   255
      Index           =   23
      Left            =   11670
      TabIndex        =   22
      Top             =   780
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Library ID"
      Height          =   255
      Index           =   14
      Left            =   60
      TabIndex        =   20
      Top             =   420
      Width           =   1095
   End
   Begin VB.Label lblLibraryID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   1260
      TabIndex        =   19
      Tag             =   "CLEARFIELDS"
      Top             =   360
      Width           =   2220
   End
   Begin VB.Label lblCaption 
      Caption         =   "Filename"
      Height          =   255
      Index           =   13
      Left            =   11670
      TabIndex        =   18
      Top             =   420
      Width           =   795
   End
   Begin VB.Label lblCompanyID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   9180
      TabIndex        =   17
      Tag             =   "CLEARFIELDS"
      Top             =   0
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Barcode"
      Height          =   255
      Index           =   2
      Left            =   60
      TabIndex        =   16
      Top             =   60
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Ep"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   86
      Left            =   7380
      TabIndex        =   12
      Top             =   780
      Width           =   315
   End
   Begin VB.Label lblCaption 
      Caption         =   "Set"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   87
      Left            =   5940
      TabIndex        =   11
      Top             =   780
      Width           =   315
   End
   Begin VB.Label lblCaption 
      Caption         =   "Sub Title"
      Height          =   255
      Index           =   9
      Left            =   3960
      TabIndex        =   10
      Top             =   1500
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      Height          =   255
      Index           =   8
      Left            =   3960
      TabIndex        =   9
      Top             =   420
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Series"
      Height          =   255
      Index           =   74
      Left            =   3960
      TabIndex        =   8
      Top             =   780
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Version"
      Height          =   255
      Index           =   4
      Left            =   3960
      TabIndex        =   7
      Top             =   1860
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      Height          =   255
      Index           =   3
      Left            =   3960
      TabIndex        =   1
      Top             =   60
      Width           =   795
   End
End
Attribute VB_Name = "frmClipSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'variable to store the clip search conditions, so that several buttons can perform actions on the contents of the search grid.
Dim m_strSearchConditions As String, m_strCrystalSearchConditions As String
Dim m_blnSilentSanitise As Boolean
Dim m_Framerate As Integer
Dim m_MinWidth As Long
Dim m_MinHeight As Long

Sub VisibleColumns()

If chkExtraFields.Value <> 0 Then
    grdClips.Columns("eventset").Visible = True
    grdClips.Columns("portalpurchaseprofilegroup").Visible = True
    If lblField1.Visible = True Then grdClips.Columns("customfield1").Visible = True
    If lblField2.Visible = True Then grdClips.Columns("customfield2").Visible = True
    If lblField3.Visible = True Then grdClips.Columns("customfield3").Visible = True
    If lblField4.Visible = True Then grdClips.Columns("customfield4").Visible = True
    If lblField5.Visible = True Then grdClips.Columns("customfield5").Visible = True
    If lblField6.Visible = True Then grdClips.Columns("customfield6").Visible = True
    grdClips.Columns("Tape_Barcode").Visible = True
'    grdClips.Columns("system_deleted").Visible = True
'    grdClips.Columns("DW").Visible = True
    grdClips.Columns("sha1").Visible = True
    grdClips.Columns("eventID").Visible = True
Else
    grdClips.Columns("eventset").Visible = False
    grdClips.Columns("portalpurchaseprofilegroup").Visible = False
    If lblField1.Visible = True Then grdClips.Columns("customfield1").Visible = False
    If lblField2.Visible = True Then grdClips.Columns("customfield2").Visible = False
    If lblField3.Visible = True Then grdClips.Columns("customfield3").Visible = False
    If lblField4.Visible = True Then grdClips.Columns("customfield4").Visible = False
    If lblField5.Visible = True Then grdClips.Columns("customfield5").Visible = False
    If lblField6.Visible = True Then grdClips.Columns("customfield6").Visible = False
    grdClips.Columns("Tape_Barcode").Visible = False
'    grdClips.Columns("system_deleted").Visible = False
'    grdClips.Columns("DW").Visible = False
    grdClips.Columns("sha1").Visible = False
End If

If chkShowEventID.Value = 0 Then
    grdClips.Columns("eventID").Visible = False
Else
    grdClips.Columns("eventID").Visible = True
End If

If chkShowMD5.Value = 0 Then
    grdClips.Columns("md5checksum").Visible = False
Else
    grdClips.Columns("md5checksum").Visible = True
End If

If chkHideEpisodic.Value = 0 Then
    grdClips.Columns("eventSeries").Visible = True
    'grdClips.Columns("eventset").Visible = True
    grdClips.Columns("eventepisode").Visible = True
    'grdClips.Columns("eventsubtitle").Visible = True
Else
    grdClips.Columns("eventSeries").Visible = False
    'grdClips.Columns("eventset").Visible = False
    grdClips.Columns("eventepisode").Visible = False
    'grdClips.Columns("eventsubtitle").Visible = False
End If

If chkHideVersionAndLanguage.Value = 0 Then
    grdClips.Columns("eventversion").Visible = True
    grdClips.Columns("language").Visible = True
Else
    grdClips.Columns("eventversion").Visible = False
    grdClips.Columns("language").Visible = False
End If

If chkSeriesIds.Value <> 0 Then
    grdClips.Columns("seriesID").Visible = True
    grdClips.Columns("serialnumber").Visible = True
Else
    grdClips.Columns("seriesID").Visible = False
    grdClips.Columns("serialnumber").Visible = False
End If

If chkTimecodeColumns.Value <> 0 Then
    grdClips.Columns("timecodestart").Visible = True
    grdClips.Columns("timecodestop").Visible = True
    grdClips.Columns("fd_length").Visible = True
Else
    grdClips.Columns("timecodestart").Visible = False
    grdClips.Columns("timecodestop").Visible = False
    grdClips.Columns("fd_length").Visible = False
End If

If chkDates.Value <> 0 Then
    grdClips.Columns("Cdate").Visible = True
    grdClips.Columns("AutoDeleteDate").Visible = True
Else
    grdClips.Columns("Cdate").Visible = False
    grdClips.Columns("AutoDeleteDate").Visible = False
End If

If chkSeriesIds.Value <> 0 Then
    grdClips.Columns("seriesID").Visible = True
    grdClips.Columns("serialnumber").Visible = True
Else
    grdClips.Columns("seriesID").Visible = False
    grdClips.Columns("serialnumber").Visible = False
End If

End Sub

Private Sub chkAssignedAndClearedMasters_Click()

If chkAssignedAndClearedMasters.Value <> 0 Then
    chkAssignedButNotClearedMasters.Value = 0
    chkUnassignedMasters.Value = 0
End If
    
End Sub

Private Sub chkAssignedButNotClearedMasters_Click()

If chkAssignedButNotClearedMasters.Value <> 0 Then
    chkAssignedAndClearedMasters.Value = 0
    chkUnassignedMasters.Value = 0
End If
    
End Sub

Private Sub chkDates_Click()

VisibleColumns

End Sub

Private Sub chkExtraFields_Click()

VisibleColumns

End Sub

Private Sub chkHideEpisodic_Click()

VisibleColumns

End Sub

Private Sub chkHideVersionAndLanguage_Click()

VisibleColumns

End Sub

Private Sub chkSearchArchive_Click()

If chkSearchArchive.Value <> 0 Then

    cmdMediaInfo.Enabled = False
    cmdMediaInfoPreserveTimecode.Enabled = False
    cmdVerifyGrid.Enabled = False
    cmdMd5Grid(0).Enabled = False
    cmdMd5Grid(1).Enabled = False
    cmdBulkTranscode.Enabled = False
    cmdBulkTranscodeProgOnly.Enabled = False
    cmdYouTubeXML.Enabled = False
'    cmdPortalSanitise.Enabled = False
'    cmdSanitiseOnlineStatus.Enabled = False
    cmdPrint.Enabled = False
    cmdReport.Enabled = False
'    cmdBulkUpdate.Enabled = False
'    cmdHideGrid.Enabled = False
    cmdCopyGrid.Enabled = False
    cmdMoveGrid.Enabled = False
'    cmdArchiveClips.Caption = "Unarchive Clips"
    lblSearchStatus.Caption = "Searching Deleted Clips"
    cmdDeleteGrid.Caption = "Undelete Grid"
    cmdDeleteSelected.Caption = "Undelete Selected"
    cmdClearOnline.Value = True
    cmdMakeThumbnails.Enabled = False
    cmdCompile.Enabled = False
    cmdDreamWorks.Enabled = False
    cmdBreakpointReport.Enabled = False

Else
    
    cmdMediaInfo.Enabled = True
    cmdMediaInfoPreserveTimecode.Enabled = True
    cmdVerifyGrid.Enabled = True
    cmdMd5Grid(0).Enabled = True
    cmdMd5Grid(1).Enabled = True
    cmdBulkTranscode.Enabled = True
    cmdBulkTranscodeProgOnly.Enabled = True
    cmdYouTubeXML.Enabled = True
'    cmdPortalSanitise.Enabled = True
'    cmdSanitiseOnlineStatus.Enabled = True
    cmdPrint.Enabled = True
    cmdReport.Enabled = True
'    cmdBulkUpdate.Enabled = True
'    cmdHideGrid.Enabled = True
    cmdCopyGrid.Enabled = True
    cmdMoveGrid.Enabled = True
'    cmdArchiveClips.Caption = "Archive Clips"
    lblSearchStatus.Caption = "Searching Live Clips"
    cmdDeleteGrid.Caption = "Delete Grid"
    cmdDeleteSelected.Caption = "Delete Selected"
    cmdMakeThumbnails.Enabled = True
    cmdCompile.Enabled = True
    cmdDreamWorks.Enabled = True
    cmdBreakpointReport.Enabled = True

End If

End Sub

Private Sub chkSeriesIds_Click()

VisibleColumns

End Sub

Private Sub chkShowEventID_Click()

VisibleColumns

End Sub

Private Sub chkShowMD5_Click()

VisibleColumns

End Sub

Private Sub chkTimecodeColumns_Click()

VisibleColumns

End Sub

Private Sub chkUnassignedMasters_Click()

If chkUnassignedMasters.Value <> 0 Then
    chkAssignedButNotClearedMasters.Value = 0
    chkAssignedAndClearedMasters.Value = 0
End If
    
End Sub

Private Sub chkUnlockFolderAndFile_Click()

If chkUnlockFolderAndFile.Value <> 0 Then
    grdClips.Columns("altlocation").Locked = False
    grdClips.Columns("clipfilename").Locked = False
    chkUnlockGrid.Value = 0
    chkUnlockGrid_Click
Else
    grdClips.Columns("altlocation").Locked = True
    grdClips.Columns("clipfilename").Locked = True
End If

End Sub

Private Sub chkUnlockGrid_Click()

If chkUnlockGrid.Value <> 0 Then
    If chkUnlockFolderAndFile.Value <> 0 Then
        MsgBox "Cannot unlock these columns if Filename and Folder are unlocked"
        chkUnlockGrid.Value = 0
        Exit Sub
    End If
    grdClips.Columns("seriesID").Locked = False
    grdClips.Columns("eventseries").Locked = False
    grdClips.Columns("eventset").Locked = False
    grdClips.Columns("eventepisode").Locked = False
    grdClips.Columns("clipreference").Locked = False
    grdClips.Columns("hidefromweb").Locked = False
    grdClips.Columns("eventtitle").Locked = False
    grdClips.Columns("eventsubtitle").Locked = False
    grdClips.Columns("eventversion").Locked = False
    grdClips.Columns("Language").Locked = False
    grdClips.Columns("customfield1").Locked = False
    grdClips.Columns("customfield2").Locked = False
    grdClips.Columns("customfield3").Locked = False
    grdClips.Columns("customfield4").Locked = False
    grdClips.Columns("customfield5").Locked = False
    grdClips.Columns("customfield6").Locked = False
Else
    grdClips.Columns("seriesID").Locked = True
    grdClips.Columns("eventseries").Locked = True
    grdClips.Columns("eventset").Locked = True
    grdClips.Columns("eventepisode").Locked = True
    grdClips.Columns("clipreference").Locked = True
    grdClips.Columns("hidefromweb").Locked = True
    grdClips.Columns("eventtitle").Locked = True
    grdClips.Columns("eventsubtitle").Locked = True
    grdClips.Columns("eventversion").Locked = True
    grdClips.Columns("language").Locked = True
    grdClips.Columns("customfield1").Locked = True
    grdClips.Columns("customfield2").Locked = True
    grdClips.Columns("customfield3").Locked = True
    grdClips.Columns("customfield4").Locked = True
    grdClips.Columns("customfield5").Locked = True
    grdClips.Columns("customfield6").Locked = True
End If

End Sub

'routines go here
Private Sub cmbAspect_Click()
    lblLibraryID.Caption = GetData("library", "libraryID", "barcode", cmbBarcode.Text)
    If lblLibraryID.Caption = "0" Then Beep: lblLibraryID.Caption = ""
End Sub

Private Sub cmbAspect_GotFocus()
PopulateCombo "aspectratio", cmbAspect
HighLite cmbAspect
End Sub

Private Sub cmbAudioCodec_GotFocus()
PopulateCombo "audiocodec", cmbAudioCodec
HighLite cmbAudioCodec
End Sub

Private Sub cmbBarcode_Click()
    
    If cmbBarcode.Text <> "" Then
        lblLibraryID.Caption = Trim(" " & GetDataSQL("SELECT TOP 1 libraryID FROM library WHERE barcode = '" & cmbBarcode.Text & "' AND system_deleted = 0"))
        lblFormat.Caption = Trim(" " & GetDataSQL("SELECT TOP 1 format FROM library WHERE barcode = '" & cmbBarcode.Text & "' and system_deleted = 0"))
        Select Case lblFormat.Caption
            Case "DISCSTORE"
                optStorageType(0).Value = True
                optStorageType_Click 0
            Case "MOBILEDISC"
                optStorageType(1).Value = True
                optStorageType_Click 1
            Case "LTO1", "LTO3"
                optStorageType(5).Value = True
                optStorageType_Click 5
            Case "LTO5"
                optStorageType(3).Value = True
                optStorageType_Click 3
            Case "LTO6"
                optStorageType(2).Value = True
                optStorageType_Click 2
            Case "DIVA"
                optStorageType(4).Value = True
                optStorageType_Click 4
            Case "LTO7"
                optStorageType(6).Value = True
                optStorageType_Click 6
            Case Else
                optStorageType(5).Value = True
                optStorageType_Click 5
        End Select
    Else
        lblLibraryID.Caption = 0
    End If
    If lblLibraryID.Caption = "0" Then
        Beep
        lblLibraryID.Caption = ""
        lblFormat.Caption = ""
    End If

End Sub

Private Sub cmbBarcode_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    cmbBarcode.Text = UCase(cmbBarcode.Text)
    If cmbBarcode.Text <> "" Then
        lblLibraryID.Caption = Trim(" " & GetDataSQL("SELECT TOP 1 libraryID FROM library WHERE barcode = '" & cmbBarcode.Text & "' AND system_deleted = 0"))
        lblFormat.Caption = Trim(" " & GetDataSQL("SELECT TOP 1 format FROM library WHERE barcode = '" & cmbBarcode.Text & "' and system_deleted = 0"))
        Select Case lblFormat.Caption
            Case "DISCSTORE"
                optStorageType(0).Value = True
                optStorageType_Click 0
            Case "MOBILEDISC"
                optStorageType(1).Value = True
                optStorageType_Click 1
            Case "LTO1", "LTO3"
                optStorageType(5).Value = True
                optStorageType_Click 5
            Case "LTO5"
                optStorageType(3).Value = True
                optStorageType_Click 3
            Case "LTO6"
                optStorageType(2).Value = True
                optStorageType_Click 2
            Case "DIVA"
                optStorageType(4).Value = True
                optStorageType_Click 4
            Case "LTO7"
                optStorageType(6).Value = True
                optStorageType_Click 6
            Case Else
                optStorageType(5).Value = True
                optStorageType_Click 5
        End Select
    Else
        lblLibraryID.Caption = "0"
    End If
    If lblLibraryID.Caption = "0" Then
        Beep
        lblLibraryID.Caption = ""
        lblFormat.Caption = ""
    End If
End If
End Sub

Private Sub cmbCbrVbr_GotFocus()
PopulateCombo "cbrvbr", cmbCbrVbr
HighLite cmbCbrVbr
End Sub

Private Sub cmbClipcodec_Click()
If cmbPurpose = "" Then cmbPurpose.Text = GetData("xref", "format", "description", cmbClipcodec.Text)
End Sub

Private Sub cmbClipcodec_GotFocus()
PopulateCombo "clipcodec", cmbClipcodec
HighLite cmbClipcodec
End Sub

Private Sub cmbClipformat_Click()
'cmbPurpose = GetData("xref", "videostandard", "description", cmbClipformat)
End Sub

Private Sub cmbClipformat_GotFocus()
PopulateCombo "clipformat", cmbClipformat
HighLite cmbClipformat
End Sub

Private Sub cmbClipType_GotFocus()
PopulateCombo "cliptype", cmbClipType
HighLite cmbClipType
End Sub

Private Sub cmbCompany_Click()

Dim l_strCurrentKeyword As String, TempStr As String

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/SPEUploadPage") > 0 Then
    lblCaption(48).Caption = "Alpha ID"
    grdClips.Columns("seriesID").Caption = "Alpha ID"
ElseIf InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/shineversion") > 0 Then
    lblCaption(48).Caption = "Title Code"
    grdClips.Columns("seriesID").Caption = "Title Code"
Else
    lblCaption(48).Caption = "Series ID"
    grdClips.Columns("seriesID").Caption = "Series ID"
End If

lblField1.Caption = Trim(" " & GetData("company", "customfield1label", "companyID", Val(lblCompanyID.Caption)))
If lblField1.Caption = "" Then
    lblField1.Visible = False
    cmbField1.Visible = False
    chkBulkUpdate(19).Value = 0
    chkBulkUpdate(19).Visible = False
Else
    lblField1.Visible = True
    cmbField1.Visible = True
    PopulateCustomFieldCombo Val(lblCompanyID.Caption), 1, cmbField1
    chkBulkUpdate(19).Visible = True
    grdClips.Columns("customfield1").Caption = lblField1.Caption
End If
lblField2.Caption = Trim(" " & GetData("company", "customfield2label", "companyID", Val(lblCompanyID.Caption)))
If lblField2.Caption = "" Then
    lblField2.Visible = False
    cmbField2.Visible = False
    chkBulkUpdate(20).Value = 0
    chkBulkUpdate(20).Visible = False
Else
    lblField2.Visible = True
    cmbField2.Visible = True
    PopulateCustomFieldCombo Val(lblCompanyID.Caption), 2, cmbField2
    chkBulkUpdate(20).Visible = True
    grdClips.Columns("customfield2").Caption = lblField2.Caption
End If
lblField3.Caption = Trim(" " & GetData("company", "customfield3label", "companyID", Val(lblCompanyID.Caption)))
If lblField3.Caption = "" Then
    lblField3.Visible = False
    cmbField3.Visible = False
    chkBulkUpdate(21).Value = 0
    chkBulkUpdate(21).Visible = False
Else
    lblField3.Visible = True
    cmbField3.Visible = True
    PopulateCustomFieldCombo Val(lblCompanyID.Caption), 3, cmbField3
    chkBulkUpdate(21).Visible = True
    grdClips.Columns("customfield3").Caption = lblField3.Caption
End If
lblField4.Caption = Trim(" " & GetData("company", "customfield4label", "companyID", Val(lblCompanyID.Caption)))
If lblField4.Caption = "" Then
    lblField4.Visible = False
    cmbField4.Visible = False
    chkBulkUpdate(22).Value = 0
    chkBulkUpdate(22).Visible = False
Else
    lblField4.Visible = True
    cmbField4.Visible = True
    PopulateCustomFieldCombo Val(lblCompanyID.Caption), 4, cmbField4
    chkBulkUpdate(22).Visible = True
    grdClips.Columns("customfield4").Caption = lblField4.Caption
End If
lblField5.Caption = Trim(" " & GetData("company", "customfield5label", "companyID", Val(lblCompanyID.Caption)))
If lblField5.Caption = "" Then
    lblField5.Visible = False
    cmbField5.Visible = False
    chkBulkUpdate(23).Value = 0
    chkBulkUpdate(23).Visible = False
Else
    lblField5.Visible = True
    cmbField5.Visible = True
    PopulateCustomFieldCombo Val(lblCompanyID.Caption), 5, cmbField5
    chkBulkUpdate(23).Visible = True
    grdClips.Columns("customfield5").Caption = lblField5.Caption
End If
lblField6.Caption = Trim(" " & GetData("company", "customfield6label", "companyID", Val(lblCompanyID.Caption)))
If lblField6.Caption = "" Then
    lblField6.Visible = False
    cmbField6.Visible = False
    chkBulkUpdate(24).Value = 0
    chkBulkUpdate(24).Visible = False
Else
    lblField6.Visible = True
    cmbField6.Visible = True
    PopulateCustomFieldCombo Val(lblCompanyID.Caption), 6, cmbField6
    chkBulkUpdate(24).Visible = True
    grdClips.Columns("customfield6").Caption = lblField6.Caption
End If

l_strCurrentKeyword = cmbKeyword.Text
cmbKeyword.Clear

Dim l_rstKeyword As ADODB.Recordset, l_strSQL As String

l_strSQL = "SELECT keywordtext FROM keyword WHERE companyID = " & Val(lblCompanyID.Caption) & "ORDER BY keywordtext;"
Set l_rstKeyword = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If Not l_rstKeyword.EOF Then
    l_rstKeyword.MoveFirst
    Do While Not l_rstKeyword.EOF
        cmbKeyword.AddItem Trim(" " & l_rstKeyword("keywordtext"))
        l_rstKeyword.MoveNext
    Loop
End If

l_rstKeyword.Close
Set l_rstKeyword = Nothing

cmbKeyword.Text = l_strCurrentKeyword

l_strSQL = "SELECT fullname, portaluserID FROM portaluser WHERE activeuser > 0 and companyID = " & lblCompanyID.Caption & " AND allclips = 0 ORDER BY fullname;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch1 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch1 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch1
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch1.ActiveConnection = Nothing

Set cmbPortalUser.DataSourceList = l_rstSearch1

Dim l_rstSearch2 As ADODB.Recordset

Set l_rstSearch2 = New ADODB.Recordset

l_strSQL = "SELECT * FROM masterseriestitle WHERE companyID = " & Val(lblCompanyID.Caption) & " ORDER BY title;"

With l_rstSearch2
    .CursorLocation = adUseClient
    .LockType = adLockBatchOptimistic
    .CursorType = adOpenDynamic
    .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch2.ActiveConnection = Nothing

Set txtTitle.DataSourceList = l_rstSearch2

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmbEpisode_GotFocus()
PopulateCombo "episode", cmbEpisode
HighLite cmbEpisode
End Sub

Private Sub cmbField1_GotFocus()
HighLite cmbField1
End Sub

Private Sub cmbField2_GotFocus()
HighLite cmbField2
End Sub

Private Sub cmbField3_GotFocus()
HighLite cmbField3
End Sub

Private Sub cmbField4_GotFocus()
HighLite cmbField4
End Sub

Private Sub cmbField5_GotFocus()
HighLite cmbField5
End Sub

Private Sub cmbField6_GotFocus()
HighLite cmbField6
End Sub

Private Sub cmbFileVersion_GotFocus()
PopulateCombo "fileversion", cmbFileVersion
HighLite cmbFileVersion
End Sub

Private Sub cmbFrameRate_Click()
    lblLibraryID.Caption = GetData("library", "libraryID", "barcode", cmbBarcode.Text)
    If lblLibraryID.Caption = "0" Then Beep: lblLibraryID.Caption = ""
End Sub

Private Sub cmbFrameRate_GotFocus()
PopulateCombo "framerate", cmbFrameRate, "MEDIA"
HighLite cmbFrameRate
End Sub

Private Sub cmbGenre_GotFocus()
PopulateCombo "genre", cmbGenre
HighLite cmbGenre
End Sub

Private Sub cmbGeometry_Click()
    lblLibraryID.Caption = GetData("library", "libraryID", "barcode", cmbBarcode.Text)
    If lblLibraryID.Caption = "0" Then Beep: lblLibraryID.Caption = ""
End Sub

Private Sub cmbGeometry_GotFocus()
PopulateCombo "geometry", cmbGeometry
HighLite cmbGeometry
End Sub

Private Sub cmbKeyword_DropDown()
HighLite cmbKeyword
End Sub

Private Sub cmbLanguage_GotFocus()
PopulateCombo "language", cmbLanguage
HighLite cmbLanguage

End Sub

Private Sub cmbMediaSpecs_Click()

Dim l_lngMediaSpecID As Long, l_strSQL As String, l_rstSpec As ADODB.Recordset

l_lngMediaSpecID = Val(cmbMediaSpecs.Columns("mediaspecID").Text)

If l_lngMediaSpecID <> 0 Then
    l_strSQL = "SELECT * FROM mediaspec WHERE mediaspecID = '" & l_lngMediaSpecID & "';"
    
    Set l_rstSpec = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If Not l_rstSpec.EOF Then
        cmbClipformat.Text = Trim(" " & l_rstSpec("mediaformat"))
        cmbClipcodec.Text = Trim(" " & l_rstSpec("videocodec"))
        cmbStreamType.Text = Trim(" " & l_rstSpec("mediastreamtype"))
        cmbAudioCodec.Text = Trim(" " & l_rstSpec("audiocodec"))
        txtHorizpixels.Text = Trim(" " & l_rstSpec("horiz"))
        txtVertpixels.Text = Trim(" " & l_rstSpec("vert"))
        cmbFrameRate.Text = Trim(" " & l_rstSpec("framerate"))
        txtBitrate.Text = Trim(" " & l_rstSpec("totalbitrate"))
        txtVideoBitrate.Text = Trim(" " & l_rstSpec("videobitrate"))
        txtAudioBitrate.Text = Trim(" " & l_rstSpec("audiobitrate"))
        cmbAspect.Text = Trim(" " & l_rstSpec("aspectratio"))
        cmbGeometry.Text = Trim(" " & l_rstSpec("geometry"))
        If Trim(" " & l_rstSpec("altlocation")) <> "" Then
            txtAltFolder.Text = Trim(" " & l_rstSpec("altlocation"))
        End If
        cmbInterlace.Text = Trim(" " & l_rstSpec("interlace"))
        cmbVodPlatform.Text = Trim(" " & l_rstSpec("vodplatform"))
    End If
    l_rstSpec.Close
    Set l_rstSpec = Nothing

End If

cmbMediaSpecs.Text = ""

End Sub

Private Sub cmbPasses_GotFocus()
PopulateCombo "encodepasses", cmbPasses
HighLite cmbPasses
End Sub

Private Sub cmbPortalUser_GotFocus()
HighLite cmbPortalUser
End Sub

Private Sub cmbPurpose_GotFocus()
PopulateCombo "clippurpose", cmbPurpose
HighLite cmbPurpose
End Sub

Private Sub cmbSeries_GotFocus()
PopulateCombo "series", cmbSeries
HighLite cmbSeries
End Sub
Private Sub cmbSet_GotFocus()
PopulateCombo "series", cmbSet
HighLite cmbSet
End Sub

Private Sub cmbStreamType_GotFocus()
HighLite cmbStreamType
End Sub

Private Sub cmbVersion_DropDown()

Dim l_strTemp As String
l_strTemp = cmbVersion.Text
If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/shineversion") > 0 Then
    PopulateCombo "shineversion", cmbVersion, "", True
Else
    PopulateCombo "version", cmbVersion, "", True
End If
cmbVersion.Text = l_strTemp

End Sub

Private Sub cmbVersion_GotFocus()
HighLite cmbVersion
End Sub

Private Sub cmbVodPlatform_GotFocus()

PopulateCombo "vodplatform", cmbVodPlatform
HighLite cmbVodPlatform

End Sub

Private Sub cmdAssignTape_Barcode_Click()

adoClip.Recordset.MoveFirst

Do While Not adoClip.Recordset.EOF
    If Trim(" " & adoClip.Recordset("clipreference")) <> "" Then
        adoClip.Recordset("Tape_Barcode") = GetData("BBCMG_Media_Key", "Media_ID", "Media_Key", adoClip.Recordset("clipreference"))
        adoClip.Recordset.Update
    End If
    adoClip.Recordset.MoveNext
Loop

End Sub

Private Sub cmdAssignToJob_Click()

Dim l_strSQL As String, Count As Long, l_lngJobID As Long
Dim l_rstClips As ADODB.Recordset

l_lngJobID = Val(InputBox("Please input the JobID to which these files should be assigned as Masters", "JobID to Assign"))
If l_lngJobID = 0 Then Exit Sub

Set l_rstClips = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
CheckForSQLError
If l_rstClips.RecordCount > 0 Then
    l_rstClips.MoveFirst
    Count = 0
    ProgressBar1.Max = l_rstClips.RecordCount
    ProgressBar1.Value = 0
    ProgressBar1.Visible = True
    Do While Not l_rstClips.EOF
        
        'Update the progress label
        ProgressBar1.Value = Count
        lblCurrentFilename.Caption = l_rstClips("clipfilename")
        DoEvents
        
        l_strSQL = "INSERT INTO requiredmedia (jobID, eventID, CDATE, CUSER) VALUES ("
        l_strSQL = l_strSQL & l_lngJobID & ", "
        l_strSQL = l_strSQL & l_rstClips("eventID") & ", "
        l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
        l_strSQL = l_strSQL & "'" & g_strUserInitials & "')"
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        Count = Count + 1
        l_rstClips.MoveNext
        
    Loop
End If
l_rstClips.Close
Set l_rstClips = Nothing
ProgressBar1.Visible = False
lblCurrentFilename.Caption = ""
DoEvents
MsgBox "These files were assigned as masters to jobb " & l_lngJobID

adoClip.Refresh
adoClip.Caption = grdClips.Rows & " Clips Found"

Exit Sub

End Sub

Private Sub cmdBatonReports_Click()

Dim l_strSQL As String, l_strReport As String, l_rstTemp As ADODB.Recordset, Count As Long
    
'Has to be some records int he grid
If adoClip.Recordset.RecordCount <= 0 Then Exit Sub

'Has to be from either a DISCSTORE or the DIVA
If lblFormat.Caption <> "DISCSTORE" Then
    MsgBox "We can only do Baton QC on items that are on a DISCSTORE.", vbCritical
    Exit Sub
End If

frmGetATSProfile.chkBatonProfile.Value = 1
frmGetATSProfile.Show vbModal
If frmGetATSProfile.cmbProfile.Text = "" Then
    MsgBox "No Baton Profile Selected", vbInformation
    Unload frmGetATSProfile
    Exit Sub
End If

l_strReport = ""

If MsgBox("Do you want to preserve XMLs of the Reports?", vbYesNo + vbDefaultButton2, "Baton Report") = vbYes Then l_strReport = l_strReport & "1"
If MsgBox("Do you want to preserve PDFs of the Reports?", vbYesNo + vbDefaultButton2, "Baton Report") = vbYes Then l_strReport = l_strReport & "2"

Set l_rstTemp = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
CheckForSQLError
l_rstTemp.MoveFirst
Count = 0
ProgressBar1.Max = l_rstTemp.RecordCount
ProgressBar1.Value = 0
ProgressBar1.Visible = True
Do While Not l_rstTemp.EOF
    ProgressBar1.Value = Count
    DoEvents
    
    l_strSQL = "INSERT INTO event_file_request (eventID, event_file_Request_typeID, NewFileName, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
    l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Baton QC") & ", "
    l_strSQL = l_strSQL & "'" & frmGetATSProfile.cmbProfile.Text & "', "
    l_strSQL = l_strSQL & IIf(l_strReport <> "", Val(l_strReport), "0") & ", "
    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    Count = Count + 1
    l_rstTemp.MoveNext
Loop
l_rstTemp.Close
Set l_rstTemp = Nothing

Unload frmGetATSProfile

ProgressBar1.Visible = False

adoClip.Refresh
adoClip.Caption = grdClips.Rows & " Clips Found"

Exit Sub

End Sub

Private Sub cmdBreakpointReport_Click()

Dim l_strFilename As String, oExecl As Excel.Application, oWorkbook As Excel.Workbook, l_intLinecount As Integer
Dim l_rstClip As ADODB.Recordset

If adoClip.Recordset.RecordCount > 20 Then
    If MsgBox("Outputting Breakpoint Report for " & adoClip.Recordset.RecordCount & vbCrLf & "Are you sure?", vbYesNo, "There are more than 20 items in the grid.") = vbNo Then
        Exit Sub
    End If
End If

l_strFilename = g_strLocationOfOmneonXLS & "\BreakpointTemplate.xls"
Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strFilename)
l_strFilename = g_strLocationOfOmneonXLS & "\" & Format(Now, "yyyy-mm-dd-hh-mm-ss") & ".xls"
On Error GoTo EXIT_FUNCTION
oWorkbook.SaveAs l_strFilename
Set oWorksheet = oWorkbook.Worksheets("Sheet1")

adoClip.Recordset.MoveFirst
l_intLinecount = 5

Do While Not adoClip.Recordset.EOF

    Set l_rstClip = ExecuteSQL("SELECT * FROM events WHERE eventID = " & adoClip.Recordset("eventID"), g_strExecuteError)
    oWorksheet.Cells(l_intLinecount, 1) = l_rstClip("clipfilename")
    oWorksheet.Cells(l_intLinecount, 2) = l_rstClip("eventepisode")
    oWorksheet.Cells(l_intLinecount, 3) = l_rstClip("break1elapsed")
    oWorksheet.Cells(l_intLinecount, 4) = l_rstClip("break2elapsed")
    oWorksheet.Cells(l_intLinecount, 5) = l_rstClip("break3elapsed")
    oWorksheet.Cells(l_intLinecount, 6) = l_rstClip("break4elapsed")
    oWorksheet.Cells(l_intLinecount, 7) = l_rstClip("break5elapsed")
    oWorksheet.Cells(l_intLinecount, 8) = l_rstClip("break6elapsed")
    oWorksheet.Cells(l_intLinecount, 9) = l_rstClip("break7elapsed")
    oWorksheet.Cells(l_intLinecount, 10) = l_rstClip("break8elapsed")
    oWorksheet.Cells(l_intLinecount, 11) = l_rstClip("break9elapsed")
    oWorksheet.Cells(l_intLinecount, 12) = l_rstClip("break10elapsed")
    oWorksheet.Cells(l_intLinecount, 13) = l_rstClip("break11elapsed")
    oWorksheet.Cells(l_intLinecount, 14) = l_rstClip("break12elapsed")
    oWorksheet.Cells(l_intLinecount, 15) = l_rstClip("break13elapsed")
    oWorksheet.Cells(l_intLinecount, 16) = l_rstClip("break14elapsed")
    oWorksheet.Cells(l_intLinecount, 17) = l_rstClip("break15elapsed")
    oWorksheet.Cells(l_intLinecount, 18) = l_rstClip("break16elapsed")
    oWorksheet.Cells(l_intLinecount, 19) = l_rstClip("break17elapsed")
    oWorksheet.Cells(l_intLinecount, 20) = l_rstClip("break18elapsed")
    l_rstClip.Close
    Set l_rstClip = Nothing

    l_intLinecount = l_intLinecount + 1
    adoClip.Recordset.MoveNext
    
Loop

oWorkbook.Save
Set oWorksheet = Nothing

EXIT_FUNCTION:

oWorkbook.Close vbNo
Set oWorkbook = Nothing
oExcel.Quit

MsgBox "Done"

End Sub

Private Sub cmdBulkAddAudioData_Click()

Dim l_strSQL As String, l_blnFoldersFound As Boolean, i As Long

If MsgBox("About to Bulk Update Audio Data" & vbCrLf & "Are Your SUre", vbYesNo + vbDefaultButton2, "Sanity Check") = vbNo Then Exit Sub

If Val(frmClipControl.txtClipID.Text) <> 0 Then
    If frmClipControl.cmbAudioType1.Text <> "" Then
        If adoClip.Recordset.RecordCount > 0 Then
            adoClip.Recordset.MoveFirst
            Do While Not adoClip.Recordset.EOF
                If adoClip.Recordset("eventID") <> Val(frmClipControl.txtClipID.Text) Then
                    If Trim(" " & adoClip.Recordset("AudioTypegroup1")) <> "" Then
                        If MsgBox("Audio Track Data aloready exists for " & adoClip.Recordset("clipfilename") & vbCrLf & "DO you wish to Overwrite it", vbYesNo + vbDefaultButton2, "Existing Data") = vbYes Then
                            adoClip.Recordset("AudioTypegroup1") = frmClipControl.cmbAudioType1.Text
                            adoClip.Recordset("AudioTypegroup2") = frmClipControl.cmbAudioType2.Text
                            adoClip.Recordset("AudioTypegroup3") = frmClipControl.cmbAudioType3.Text
                            adoClip.Recordset("AudioTypegroup4") = frmClipControl.cmbAudioType4.Text
                            adoClip.Recordset("AudioTypegroup5") = frmClipControl.cmbAudioType5.Text
                            adoClip.Recordset("AudioTypegroup6") = frmClipControl.cmbAudioType6.Text
                            adoClip.Recordset("AudioTypegroup7") = frmClipControl.cmbAudioType7.Text
                            adoClip.Recordset("AudioTypegroup8") = frmClipControl.cmbAudioType8.Text
                            adoClip.Recordset("AudioContentgroup1") = frmClipControl.cmbAudioContent1.Text
                            adoClip.Recordset("AudioContentgroup2") = frmClipControl.cmbAudioContent2.Text
                            adoClip.Recordset("AudioContentgroup3") = frmClipControl.cmbAudioContent3.Text
                            adoClip.Recordset("AudioContentgroup4") = frmClipControl.cmbAudioContent4.Text
                            adoClip.Recordset("AudioContentgroup5") = frmClipControl.cmbAudioContent5.Text
                            adoClip.Recordset("AudioContentgroup6") = frmClipControl.cmbAudioContent6.Text
                            adoClip.Recordset("AudioContentgroup7") = frmClipControl.cmbAudioContent7.Text
                            adoClip.Recordset("AudioContentgroup8") = frmClipControl.cmbAudioContent8.Text
                            adoClip.Recordset("AudioLanguagegroup1") = frmClipControl.cmbAudioLanguage1.Text
                            adoClip.Recordset("AudioLanguagegroup2") = frmClipControl.cmbAudioLanguage2.Text
                            adoClip.Recordset("AudioLanguagegroup3") = frmClipControl.cmbAudioLanguage3.Text
                            adoClip.Recordset("AudioLanguagegroup4") = frmClipControl.cmbAudioLanguage4.Text
                            adoClip.Recordset("AudioLanguagegroup5") = frmClipControl.cmbAudioLanguage5.Text
                            adoClip.Recordset("AudioLanguagegroup6") = frmClipControl.cmbAudioLanguage6.Text
                            adoClip.Recordset("AudioLanguagegroup7") = frmClipControl.cmbAudioLanguage7.Text
                            adoClip.Recordset("AudioLanguagegroup8") = frmClipControl.cmbAudioLanguage8.Text
                            adoClip.Recordset.Update
                        End If
                    Else
                        adoClip.Recordset("AudioTypegroup1") = frmClipControl.cmbAudioType1.Text
                        adoClip.Recordset("AudioTypegroup2") = frmClipControl.cmbAudioType2.Text
                        adoClip.Recordset("AudioTypegroup3") = frmClipControl.cmbAudioType3.Text
                        adoClip.Recordset("AudioTypegroup4") = frmClipControl.cmbAudioType4.Text
                        adoClip.Recordset("AudioTypegroup5") = frmClipControl.cmbAudioType5.Text
                        adoClip.Recordset("AudioTypegroup6") = frmClipControl.cmbAudioType6.Text
                        adoClip.Recordset("AudioTypegroup7") = frmClipControl.cmbAudioType7.Text
                        adoClip.Recordset("AudioTypegroup8") = frmClipControl.cmbAudioType8.Text
                        adoClip.Recordset("AudioContentgroup1") = frmClipControl.cmbAudioContent1.Text
                        adoClip.Recordset("AudioContentgroup2") = frmClipControl.cmbAudioContent2.Text
                        adoClip.Recordset("AudioContentgroup3") = frmClipControl.cmbAudioContent3.Text
                        adoClip.Recordset("AudioContentgroup4") = frmClipControl.cmbAudioContent4.Text
                        adoClip.Recordset("AudioContentgroup5") = frmClipControl.cmbAudioContent5.Text
                        adoClip.Recordset("AudioContentgroup6") = frmClipControl.cmbAudioContent6.Text
                        adoClip.Recordset("AudioContentgroup7") = frmClipControl.cmbAudioContent7.Text
                        adoClip.Recordset("AudioContentgroup8") = frmClipControl.cmbAudioContent8.Text
                        adoClip.Recordset("AudioLanguagegroup1") = frmClipControl.cmbAudioLanguage1.Text
                        adoClip.Recordset("AudioLanguagegroup2") = frmClipControl.cmbAudioLanguage2.Text
                        adoClip.Recordset("AudioLanguagegroup3") = frmClipControl.cmbAudioLanguage3.Text
                        adoClip.Recordset("AudioLanguagegroup4") = frmClipControl.cmbAudioLanguage4.Text
                        adoClip.Recordset("AudioLanguagegroup5") = frmClipControl.cmbAudioLanguage5.Text
                        adoClip.Recordset("AudioLanguagegroup6") = frmClipControl.cmbAudioLanguage6.Text
                        adoClip.Recordset("AudioLanguagegroup7") = frmClipControl.cmbAudioLanguage7.Text
                        adoClip.Recordset("AudioLanguagegroup8") = frmClipControl.cmbAudioLanguage8.Text
                        adoClip.Recordset.Update
                    End If
                End If
                adoClip.Recordset.MoveNext
            Loop
            MsgBox "Done"
        End If
        adoClip.Refresh
    End If
End If

End Sub

Private Sub cmdBulkAddProgData_Click()

Dim SQL As String

If Val(frmClipControl.txtClipID.Text) <> 0 Then
    If frmClipControl.adoLogging.Recordset.RecordCount <> 0 Then
        frmClipControl.adoLogging.Recordset.MoveFirst
        Do While Not frmClipControl.adoLogging.Recordset.EOF
            If frmClipControl.adoLogging.Recordset("segmentreference") = "Programme" Then
                Exit Do
            End If
            frmClipControl.adoLogging.Recordset.MoveNext
        Loop
        If Not frmClipControl.adoLogging.Recordset.EOF Then
            If adoClip.Recordset.RecordCount > 0 Then
                adoClip.Recordset.MoveFirst
                Do While Not adoClip.Recordset.EOF
                    If adoClip.Recordset("eventID") <> Val(frmClipControl.txtClipID.Text) Then
                        If GetDataSQL("SELECT eventloggingID FROM eventlogging WHERE segmentreference = 'Programme' AND eventID = " & adoClip.Recordset("eventID")) <> "" Then
                            If MsgBox("A Programme item already exists for " & adoClip.Recordset("clipfilename") & vbCrLf & "Do you wish to Overwrite it", vbYesNo + vbDefaultButton2, "Existing Data") = vbYes Then
                                SQL = "DELETE FROM eventlogging WHERE eventID = " & adoClip.Recordset("eventID") & " AND segmentreference = 'Programme';"
                                Debug.Print SQL
                                ExecuteSQL SQL, g_strExecuteError
                                CheckForSQLError
                                SQL = "INSERT INTO eventlogging (eventID, segmentreference, timecodestart, timecodestop) VALUES ("
                                SQL = SQL & adoClip.Recordset("eventID") & ", "
                                SQL = SQL & "'Programme', "
                                SQL = SQL & "'" & frmClipControl.adoLogging.Recordset("timecodestart") & "', "
                                SQL = SQL & "'" & frmClipControl.adoLogging.Recordset("timecodestop") & "');"
                                Debug.Print SQL
                                ExecuteSQL SQL, g_strExecuteError
                                CheckForSQLError
                            End If
                        Else
                            SQL = "INSERT INTO eventlogging (eventID, segmentreference, timecodestart, timecodestop) VALUES ("
                            SQL = SQL & adoClip.Recordset("eventID") & ", "
                            SQL = SQL & "'Programme', "
                            SQL = SQL & "'" & frmClipControl.adoLogging.Recordset("timecodestart") & "', "
                            SQL = SQL & "'" & frmClipControl.adoLogging.Recordset("timecodestop") & "');"
                            Debug.Print SQL
                            ExecuteSQL SQL, g_strExecuteError
                            CheckForSQLError
                        End If
                    End If
                    adoClip.Recordset.MoveNext
                Loop
                MsgBox "Done"
            End If
            adoClip.Refresh
        End If
    End If
End If

End Sub

Private Sub cmdBulkAssignment_Click()

Dim l_strSQL As String, l_rstAssignment As ADODB.Recordset, l_rstClips As ADODB.Recordset
Dim Count As Long, l_strUpdateSet As String

If lblCompanyID.Caption = "" Then
    MsgBox "Company must be set before using Bulk Assignment", vbCritical, "Error"
    Exit Sub
End If

If cmbPortalUser.Text = "" Then
    MsgBox "A user must be selected before using Bulk Assignment", vbCritical, "Error"
    Exit Sub
End If

If adoClip.Recordset.RecordCount <= 0 Then Exit Sub

If MsgBox("You are about to assign the " & adoClip.Recordset.RecordCount & " clips in the grid to user: " & cmbPortalUser.Text & "." & vbCrLf & "Is this correct?", vbYesNo, "Bulk media Window Assignment") = vbNo Then Exit Sub

'Check all the checkboxes, and for any that are true include their field in the update set
l_strUpdateSet = "mdate = '" & FormatSQLDate(Now) & "', muser = '" & g_strUserInitials & "'"
l_strUpdateSet = l_strUpdateSet & ", hidefromweb = 0"

'If there were any fields to update, then complete the SQL statement and execute it
Set l_rstClips = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
CheckForSQLError

If l_rstClips.RecordCount > 0 Then
    l_rstClips.MoveFirst
    Count = 0
    ProgressBar1.Max = l_rstClips.RecordCount
    ProgressBar1.Value = 0
    ProgressBar1.Visible = True
    DoEvents
    
    Do While Not l_rstClips.EOF
        ProgressBar1.Value = Count
        lblCurrentFilename.Caption = l_rstClips("clipfilename")
        DoEvents
        l_strSQL = "UPDATE events SET " & l_strUpdateSet & " WHERE eventID = " & l_rstClips("eventID") & ";"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        Count = Count + 1
        l_rstClips.MoveNext
    Loop
    lblCurrentFilename.Caption = ""
    DoEvents
End If
l_rstClips.Close

adoClip.Refresh

Set l_rstClips = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
CheckForSQLError

If l_rstClips.RecordCount > 0 Then
    l_rstClips.MoveFirst
    Count = 0
    ProgressBar1.Max = l_rstClips.RecordCount
    ProgressBar1.Value = 0
    ProgressBar1.Visible = True
    DoEvents
    
    Do While Not l_rstClips.EOF
        ProgressBar1.Value = Count
        lblCurrentFilename.Caption = l_rstClips("clipfilename")
        DoEvents
    
        If VerifyClip(l_rstClips("eventID"), l_rstClips("altlocation"), l_rstClips("clipfilename"), l_rstClips("LibraryID"), True, True) = True Then
            If l_rstClips("libraryID") = 772744 Or l_rstClips("libraryID") = 772745 Then
'                ExecuteSQL "UPDATE events SET hidefromweb = 0 WHERE eventID = " & l_rstClips("eventID") & ";", g_strExecuteError
'                CheckForSQLError
                l_strSQL = "SELECT * FROM portalpermission WHERE eventID = " & l_rstClips("eventID") & " AND portaluserID = " & cmbPortalUser.Columns("portaluserID").Text & ";"
                Set l_rstAssignment = ExecuteSQL(l_strSQL, g_strExecuteError)
                CheckForSQLError
                If l_rstAssignment.RecordCount > 0 Then
                    'Already exists
                    l_rstAssignment("dateassigned") = Now
                    l_rstAssignment.Update
                Else
                    l_rstAssignment.AddNew
                    l_rstAssignment("eventID") = l_rstClips("eventID")
                    l_rstAssignment("portaluserID") = cmbPortalUser.Columns("portaluserID").Text
                    l_rstAssignment("fullname") = cmbPortalUser.Columns("fullname").Text
                    l_rstAssignment("mediareference") = l_rstClips("clipreference")
                    l_rstAssignment("dateassigned") = Now
                    l_rstAssignment("assignedby") = "MX1 - " & g_strFullUserName
                    l_rstAssignment.Update
                End If
                l_rstAssignment.Close
            End If
        End If
        
        'move to the next record
        Count = Count + 1
        
        l_rstClips.MoveNext
    
    Loop
    lblCurrentFilename.Caption = ""
    DoEvents
End If

l_rstClips.Close
Set l_rstClips = Nothing

ProgressBar1.Visible = False

adoClip.Recordset.Requery

End Sub

Private Sub cmdBulkTranscode_Click()

If MsgBox("Are you sure?", vbYesNo, "Submitting the entire grid for transcoding.") = vbNo Then Exit Sub

frmXMLTranscode.chkBulkTranscoding.Value = 1
frmXMLTranscode.chkTimecodesFromCaller.Value = 0
frmXMLTranscode.chkMOConvert.Value = 0

frmXMLTranscode.Show vbModal

Unload frmXMLTranscode

End Sub

Private Sub cmdBulkTranscodeMOConvert_Click()

If MsgBox("Are you sure?", vbYesNo, "Submitting the entire grid for MO Convert transcoding.") = vbNo Then Exit Sub

frmXMLTranscode.chkBulkTranscoding.Value = 1
frmXMLTranscode.chkTimecodesFromCaller.Value = 0
frmXMLTranscode.chkMOConvert.Value = 1

frmXMLTranscode.Show vbModal

Unload frmXMLTranscode

End Sub

Private Sub cmdBulkTranscodeProgOnly_Click()

If MsgBox("Are you sure?", vbYesNo, "Submitting the entire grid for transcoding.") = vbNo Then Exit Sub

frmXMLTranscode.chkBulkTranscoding.Value = 1
frmXMLTranscode.chkTimecodesFromCaller.Value = 1
frmXMLTranscode.chkSkipLogging.Value = 1
frmXMLTranscode.chkProgramOnly.Value = 1
frmXMLTranscode.chkMOConvert.Value = 0

frmXMLTranscode.Show vbModal

Unload frmXMLTranscode

End Sub

Private Sub cmdBulkUpdate_Click()

Dim l_strSQL As String, l_lngCount As Long
Dim l_strUpdateSet As String
Dim l_rstTestCount As ADODB.Recordset, l_rstData As ADODB.Recordset

If adoClip.Recordset.RecordCount = 0 Then Beep: Exit Sub

On Error GoTo BULK_UPDATE_ERROR

Set l_rstTestCount = ExecuteSQL("SELECT eventID FROM events WHERE 1=1 " & m_strSearchConditions & ";", g_strExecuteError)
CheckForSQLError

l_lngCount = l_rstTestCount.RecordCount

If l_lngCount <= 0 Then
    MsgBox "No selection criteria in place. Please do a search.", vbCritical, "Error with Bulk Update"
    Exit Sub
End If

If MsgBox("Updating " & l_lngCount & " records - Are you sure", vbYesNo + vbDefaultButton2, "Bulk Update") = vbNo Then Exit Sub

If l_lngCount > 10 Then
    If MsgBox("Updating more than 10 records - Are you sure", vbYesNo + vbDefaultButton2, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 50 Then
    If MsgBox("Updating more than 50 records - Are you sure", vbYesNo + vbDefaultButton2, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 100 Then
    If Not CheckAccess("/bulkupdatelots") Then Exit Sub
End If


'Check all the checkboxes, and for any that are true include their field in the update set
l_strUpdateSet = "mdate = '" & FormatSQLDate(Now) & "', muser = '" & g_strUserInitials & "'"

If chkBulkUpdate(0).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", jobID = '" & txtJobID.Text & "'"
If chkBulkUpdate(1).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clipformat = '" & cmbClipformat.Text & "'"
If chkBulkUpdate(2).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clipcodec = '" & cmbClipcodec.Text & "'"
If chkBulkUpdate(3).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", eventtype = '" & cmbPurpose.Text & "'"
If chkBulkUpdate(4).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", cliphorizontalpixels = '" & txtHorizpixels.Text & "'"
If chkBulkUpdate(5).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clipverticalpixels = '" & txtVertpixels.Text & "'"
If chkBulkUpdate(6).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clipbitrate = '" & txtBitrate.Text & "'"
If chkBulkUpdate(7).Value <> 0 Then
    l_strUpdateSet = l_strUpdateSet & ", aspectratio = '" & cmbAspect.Text & "'"
    If cmbAspect.Text = "4:3" Then
        l_strUpdateSet = l_strUpdateSet & ", fourbythreeflag = 1"
    Else
        l_strUpdateSet = l_strUpdateSet & ", fourbythreeflag = 0"
    End If
End If
If chkBulkUpdate(8).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", geometriclinearity = '" & cmbGeometry.Text & "'"
If chkBulkUpdate(9).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", companyname = '" & QuoteSanitise(cmbCompany.Text) & "', companyID = '" & lblCompanyID.Caption & "'"
If chkBulkUpdate(10).Value <> 0 Then If cmbVersion.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", eventversion = '" & QuoteSanitise(cmbVersion.Text) & "'" Else l_strUpdateSet = l_strUpdateSet & ", eventversion = NULL"
If chkBulkUpdate(11).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clocknumber = '" & txtClockNumber.Text & "'"
If chkBulkUpdate(12).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clipreference = '" & QuoteSanitise(txtReference.Text) & "'"
If chkBulkUpdate(13).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", altlocation = '" & QuoteSanitise(SlashForwardToBack(txtAltFolder.Text)) & "'"
If chkBulkUpdate(14).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clipframerate = '" & cmbFrameRate.Text & "'"
If chkBulkUpdate(15).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clipgenre = '" & cmbGenre.Text & "'"
If chkBulkUpdate(16).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", cliptype = '" & cmbClipType.Text & "'"
If chkBulkUpdate(17).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", eventset = '" & cmbSet.Text & "'"
If chkBulkUpdate(18).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", eventseries = '" & cmbSeries.Text & "'"
If chkBulkUpdate(19).Value <> 0 Then If cmbField1.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", customfield1 = '" & cmbField1.Text & "'" Else l_strUpdateSet = l_strUpdateSet & ", customfield1 = NULL"
If chkBulkUpdate(20).Value <> 0 Then If cmbField2.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", customfield2 = '" & cmbField2.Text & "'" Else l_strUpdateSet = l_strUpdateSet & ", customfield2 = NULL"
If chkBulkUpdate(21).Value <> 0 Then If cmbField3.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", customfield3 = '" & cmbField3.Text & "'" Else l_strUpdateSet = l_strUpdateSet & ", customfield3 = NULL"
If chkBulkUpdate(22).Value <> 0 Then If cmbField4.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", customfield4 = '" & cmbField4.Text & "'" Else l_strUpdateSet = l_strUpdateSet & ", customfield4 = NULL"
If chkBulkUpdate(23).Value <> 0 Then If cmbField5.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", customfield5 = '" & cmbField5.Text & "'" Else l_strUpdateSet = l_strUpdateSet & ", customfield5 = NULL"
If chkBulkUpdate(24).Value <> 0 Then If cmbField6.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", customfield6 = '" & cmbField6.Text & "'" Else l_strUpdateSet = l_strUpdateSet & ", customfield6 = NULL"
If chkBulkUpdate(25).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", internalreference = '" & txtInternalReference.Text & "'"
If chkBulkUpdate(26).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clipaudiocodec = '" & cmbAudioCodec.Text & "'"
If chkBulkUpdate(27).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", videobitrate = '" & txtVideoBitrate.Text & "'"
If chkBulkUpdate(28).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", audiobitrate = '" & txtAudioBitrate.Text & "'"
If chkBulkUpdate(29).Value <> 0 Then If txtTitle.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", eventtitle = '" & QuoteSanitise(Trim(Replace(txtTitle.Text, vbCrLf, ""))) & "'" Else l_strUpdateSet = l_strUpdateSet & ", eventtitle = NULL"
If chkBulkUpdate(30).Value <> 0 Then If txtSubtitle.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", eventsubtitle = '" & QuoteSanitise(txtSubtitle.Text) & "'" Else l_strUpdateSet = l_strUpdateSet & ", eventsubtitle = NULL"
If chkBulkUpdate(31).Value <> 0 Then If cmbEpisode.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", eventepisode = '" & cmbEpisode.Text & "'" Else l_strUpdateSet = l_strUpdateSet & ", eventepisode = NULL"
If chkBulkUpdate(32).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", encodepasses = '" & cmbPasses.Text & "'"
If chkBulkUpdate(33).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", cbrvbr = '" & cmbCbrVbr.Text & "'"
If chkBulkUpdate(34).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", interlace = '" & cmbInterlace.Text & "'"
If chkBulkUpdate(35).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", mediastreamtype = '" & cmbStreamType.Text & "'"
If chkBulkUpdate(36).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", eventkeyframefilename = '" & txtThumbnail.Text & "'"
If chkBulkUpdate(37).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", fourbythreeflag = '" & chkFourByThree.Value & "'"
If chkBulkUpdate(38).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", fileversion = '" & cmbFileVersion.Text & "'"
If chkBulkUpdate(39).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_stereo_russian = '" & chkFileLock.Value & "'"
If chkBulkUpdate(45).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", serialnumber = " & IIf(txtSerialNumber.Text <> "", "'" & Trim(Replace(txtSerialNumber.Text, vbCrLf, "")) & "'", "NULL")
If chkBulkUpdate(47).Value <> 0 Then If cmbLanguage.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", language = '" & cmbLanguage.Text & "'" Else l_strUpdateSet = l_strUpdateSet & ", language = NULL"
If chkBulkUpdate(48).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", vodplatform = '" & cmbVodPlatform.Text & "'"
If chkBulkUpdate(49).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", seriesID = " & IIf(txtSeriesID.Text <> "", "'" & Trim(Replace(txtSeriesID.Text, vbCrLf, "")) & "'", "NULL")
'If chkBulkUpdate(52).Value <> 0 Then
If chkBulkUpdate(53).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", events.resourceID = " & Val(GetDataSQL("SELECT TOP 1 description FROM xref WHERE category = 'portalpurchaseprofilegroups' AND information = '" & cmbProfileGroup.Text & "';"))
If chkBulkUpdate(54).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", events.svenskprojectnumber = '" & txtSvenskProjectNumber.Text & "'"
'If there were any fields to update, then complete the SQL statement and execute it

If chkBulkUpdate(9).Value <> 0 Or chkBulkUpdate(3).Value <> 0 Then Set l_rstTestCount = ExecuteSQL("SELECT eventID FROM events WHERE 1=1 " & m_strSearchConditions & ";", g_strExecuteError)

l_strSQL = "UPDATE events SET " & l_strUpdateSet & " WHERE 1=1 " & m_strSearchConditions & ";"
Debug.Print l_strSQL

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

'If chkBulkUpdate(9).Value <> 0 Or chkBulkUpdate(3).Value <> 0 Then
'    If l_rstTestCount.RecordCount > 0 Then
'        l_rstTestCount.MoveFirst
'        Do While Not l_rstTestCount.EOF
'            'Display the DIVA Category
'            Set l_rstData = ExecuteSQL("exec cet_Get_DIVA_Category @EventID=" & l_rstTestCount("eventID"), g_strExecuteError)
'            CheckForSQLError
'            SetData "events", "clipsoundformat", "eventID", l_rstTestCount("eventID"), l_rstData(0)
'            l_rstData.Close
'            l_rstTestCount.MoveNext
'        Loop
'    End If
'    l_rstTestCount.Close
'End If
'
cmdSearch.Value = True

BULK_UPDATE_ERROR:

Exit Sub

End Sub

Private Sub cmdBulkUpdateSelected_Click()

Dim l_strSQL As String, l_lngCount As Long
Dim l_strUpdateSet As String
Dim bkmrk As Variant
Dim l_rstData As ADODB.Recordset

If adoClip.Recordset.RecordCount = 0 Then Beep: Exit Sub

On Error GoTo BULK_UPDATE_ERROR

l_lngCount = grdClips.SelBookmarks.Count

If l_lngCount <= 0 Then
    MsgBox "No rows selected.", vbCritical, "Error with Bulk Update"
    Exit Sub
End If

If MsgBox("Updating " & l_lngCount & " records - Are you sure", vbYesNo + vbDefaultButton2, "Bulk Update") = vbNo Then Exit Sub

If l_lngCount > 10 Then
    If MsgBox("Updating more than 10 records - Are you sure", vbYesNo + vbDefaultButton2, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 50 Then
    If MsgBox("Updating more than 50 records - Are you sure", vbYesNo + vbDefaultButton2, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 100 Then
    If Not CheckAccess("/bulkupdatelots") Then Exit Sub
End If

'Check all the checkboxes, and for any that are true include their field in the update set
l_strUpdateSet = "mdate = '" & FormatSQLDate(Now) & "', muser = '" & g_strUserInitials & "'"

If chkBulkUpdate(0).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", jobID = '" & txtJobID.Text & "'"
If chkBulkUpdate(1).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clipformat = '" & cmbClipformat.Text & "'"
If chkBulkUpdate(2).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clipcodec = '" & cmbClipcodec.Text & "'"
If chkBulkUpdate(3).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", eventtype = '" & cmbPurpose.Text & "'"
If chkBulkUpdate(4).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", cliphorizontalpixels = '" & txtHorizpixels.Text & "'"
If chkBulkUpdate(5).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clipverticalpixels = '" & txtVertpixels.Text & "'"
If chkBulkUpdate(6).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clipbitrate = '" & txtBitrate.Text & "'"
If chkBulkUpdate(7).Value <> 0 Then
    l_strUpdateSet = l_strUpdateSet & ", aspectratio = '" & cmbAspect.Text & "'"
    If cmbAspect.Text = "4:3" Then
        l_strUpdateSet = l_strUpdateSet & ", fourbythreeflag = 1"
    Else
        l_strUpdateSet = l_strUpdateSet & ", fourbythreeflag = 0"
    End If
End If
If chkBulkUpdate(8).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", geometriclinearity = '" & cmbGeometry.Text & "'"
If chkBulkUpdate(9).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", companyname = '" & QuoteSanitise(cmbCompany.Text) & "', companyID = '" & lblCompanyID.Caption & "'"
If chkBulkUpdate(10).Value <> 0 Then If cmbVersion.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", eventversion = '" & QuoteSanitise(cmbVersion.Text) & "'" Else l_strUpdateSet = l_strUpdateSet & ", eventversion = NULL"
If chkBulkUpdate(11).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clocknumber = '" & txtClockNumber.Text & "'"
If chkBulkUpdate(12).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clipreference = '" & QuoteSanitise(txtReference.Text) & "'"
If chkBulkUpdate(13).Value <> 0 Then
    'Check the altlocation doesn't have any forward slashes in it...
    If InStr(txtAltFolder.Text, "/") > 0 Then
        MsgBox "Alt Location has forward slashes in. Please fix this - This field not updated...", vbOKOnly, "Bulk Update Problem..."
    Else
        l_strUpdateSet = l_strUpdateSet & ", altlocation = '" & QuoteSanitise(txtAltFolder.Text) & "'"
    End If
End If
If chkBulkUpdate(14).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clipframerate = '" & cmbFrameRate.Text & "'"
If chkBulkUpdate(15).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clipgenre = '" & cmbGenre.Text & "'"
If chkBulkUpdate(16).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", cliptype = '" & cmbClipType.Text & "'"
If chkBulkUpdate(17).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", eventset = '" & cmbSet.Text & "'"
If chkBulkUpdate(18).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", eventseries = '" & cmbSeries.Text & "'"
If chkBulkUpdate(19).Value <> 0 Then If cmbField1.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", customfield1 = '" & cmbField1.Text & "'" Else l_strUpdateSet = l_strUpdateSet & ", customfield1 = NULL"
If chkBulkUpdate(20).Value <> 0 Then If cmbField2.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", customfield2 = '" & cmbField2.Text & "'" Else l_strUpdateSet = l_strUpdateSet & ", customfield2 = NULL"
If chkBulkUpdate(21).Value <> 0 Then If cmbField3.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", customfield3 = '" & cmbField3.Text & "'" Else l_strUpdateSet = l_strUpdateSet & ", customfield3 = NULL"
If chkBulkUpdate(22).Value <> 0 Then If cmbField4.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", customfield4 = '" & cmbField4.Text & "'" Else l_strUpdateSet = l_strUpdateSet & ", customfield4 = NULL"
If chkBulkUpdate(23).Value <> 0 Then If cmbField5.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", customfield5 = '" & cmbField5.Text & "'" Else l_strUpdateSet = l_strUpdateSet & ", customfield5 = NULL"
If chkBulkUpdate(24).Value <> 0 Then If cmbField6.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", customfield6 = '" & cmbField6.Text & "'" Else l_strUpdateSet = l_strUpdateSet & ", customfield6 = NULL"
If chkBulkUpdate(25).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", internalreference = '" & txtInternalReference.Text & "'"
If chkBulkUpdate(26).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", clipaudiocodec = '" & cmbAudioCodec.Text & "'"
If chkBulkUpdate(27).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", videobitrate = '" & txtVideoBitrate.Text & "'"
If chkBulkUpdate(28).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", audiobitrate = '" & txtAudioBitrate.Text & "'"
If chkBulkUpdate(29).Value <> 0 Then If txtTitle.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", eventtitle = '" & QuoteSanitise(Trim(Replace(txtTitle.Text, vbCrLf, ""))) & "'" Else l_strUpdateSet = l_strUpdateSet & ", eventtitle = NULL"
If chkBulkUpdate(30).Value <> 0 Then If txtSubtitle.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", eventsubtitle = '" & QuoteSanitise(txtSubtitle.Text) & "'" Else l_strUpdateSet = l_strUpdateSet & ", eventsubtitle = NULL"
If chkBulkUpdate(31).Value <> 0 Then If cmbEpisode.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", eventepisode = '" & cmbEpisode.Text & "'" Else l_strUpdateSet = l_strUpdateSet & ", eventepisode = NULL"
If chkBulkUpdate(32).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", encodepasses = '" & cmbPasses.Text & "'"
If chkBulkUpdate(33).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", cbrvbr = '" & cmbCbrVbr.Text & "'"
If chkBulkUpdate(34).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", interlace = '" & cmbInterlace.Text & "'"
If chkBulkUpdate(35).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", mediastreamtype = '" & cmbStreamType.Text & "'"
If chkBulkUpdate(36).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", eventkeyframefilename = '" & txtThumbnail.Text & "'"
If chkBulkUpdate(37).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", fourbythreeflag = '" & chkFourByThree.Value & "'"
If chkBulkUpdate(38).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", fileversion = '" & cmbFileVersion.Text & "'"
If chkBulkUpdate(39).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", sound_stereo_russian = '" & chkFileLock.Value & "'"
If chkBulkUpdate(45).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", serialnumber = " & IIf(txtSerialNumber.Text <> "", "'" & Trim(Replace(txtSerialNumber.Text, vbCrLf, "")) & "'", "NULL")
If chkBulkUpdate(47).Value <> 0 Then If cmbLanguage.Text <> "" Then l_strUpdateSet = l_strUpdateSet & ", language = '" & cmbLanguage.Text & "'" Else l_strUpdateSet = l_strUpdateSet & ", language = NULL"
If chkBulkUpdate(48).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", vodplatform = '" & cmbVodPlatform.Text & "'"
If chkBulkUpdate(49).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", seriesID = " & IIf(txtSeriesID.Text <> "", "'" & Trim(Replace(txtSeriesID.Text, vbCrLf, "")) & "'", "NULL")
'If chkBulkUpdate(52).Value <> 0 Then Do Nothing
If chkBulkUpdate(53).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", events.resourceID = " & Val(GetDataSQL("SELECT TOP 1 description FROM xref WHERE category = 'portalpurchaseprofilegroups' AND information = '" & cmbProfileGroup.Text & "';"))
If chkBulkUpdate(54).Value <> 0 Then l_strUpdateSet = l_strUpdateSet & ", events.svenskprojectnumber = '" & txtSvenskProjectNumber.Text & "'"

'If there were any fields to update, then complete the SQL statement and execute it

For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1

    bkmrk = grdClips.SelBookmarks(l_lngCount)
    l_strSQL = "UPDATE events SET " & l_strUpdateSet & " WHERE eventID = " & Val(grdClips.Columns("eventID").CellText(bkmrk)) & ";"
    Debug.Print l_strSQL
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
Next

'If chkBulkUpdate(9).Value <> 0 Or chkBulkUpdate(3).Value <> 0 Then
'    For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1
'        'Display the DIVA Category
'        bkmrk = grdClips.SelBookmarks(l_lngCount)
'        Set l_rstData = ExecuteSQL("exec cet_Get_DIVA_Category @EventID=" & Val(grdClips.Columns("eventID").CellText(bkmrk)), g_strExecuteError)
'        CheckForSQLError
'        SetData "events", "clipsoundformat", "eventID", Val(grdClips.Columns("eventID").CellText(bkmrk)), l_rstData(0)
'        l_rstData.Close
'    Next
'End If
'
cmdSearch.Value = True

BULK_UPDATE_ERROR:

Exit Sub

End Sub

Private Sub cmdClear_Click()

ClearFields Me

lblField1.Visible = True
lblField2.Visible = True
lblField3.Visible = True
lblField4.Visible = True
lblField5.Visible = True
lblField6.Visible = True
cmbField1.Visible = True
cmbField2.Visible = True
cmbField3.Visible = True
cmbField4.Visible = True
cmbField5.Visible = True
cmbField6.Visible = True

End Sub

Private Sub cmdClearCompany_Click()

cmbCompany.Text = ""
lblCompanyID.Caption = ""
lblField1.Caption = "Custom Fld 1"
lblField1.Visible = True
lblField2.Caption = "Custom Fld 2"
lblField2.Visible = True
lblField3.Caption = "Custom Fld 3"
lblField3.Visible = True
lblField4.Caption = "Custom Fld 4"
lblField4.Visible = True
lblField5.Caption = "Custom Fld 5"
lblField5.Visible = True
lblField6.Caption = "Custom Fld 6"
lblField6.Visible = True
cmbField1.Visible = True
cmbField2.Visible = True
cmbField3.Visible = True
cmbField4.Visible = True
cmbField5.Visible = True
cmbField6.Visible = True

End Sub

Private Sub cmdClearOnline_Click()

Dim i As Integer
For i = 0 To 6
    optStorageType(i).Value = False
Next

End Sub

Private Sub cmdClearPathe_Click()

lblOrderID.Caption = ""
txtOriginalOrderEmail.Text = ""
txtPatheOrderNumber.Text = ""
adoPatheClips.RecordSource = "Select * from BP_Order_detail where Order_detail_ID = -1"
adoPatheClips.Refresh
adoPatheClips.Caption = "0 Clips"

End Sub

Private Sub cmdClearRadioButtons_Click()

optSubmitRenameRequests.Value = 0
optSubmitMoveRequests.Value = 0

End Sub

Private Sub cmdClearSelectedRows_Click()

grdClips.SelBookmarks.RemoveAll

End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdClosePathe_Click()

fraPatheOrder.Visible = False

End Sub

Private Sub cmdCompile_Click()

Dim l_lngMasterClipID As Long, l_strSQL  As String

On Error Resume Next
l_lngMasterClipID = Val(frmClipCompile.lblClipID.Caption)
On Error GoTo 0

If l_lngMasterClipID <> 0 Then

    l_strSQL = "INSERT INTO eventcompilation (eventID, sourceclipID, forder) VALUES ("
    l_strSQL = l_strSQL & l_lngMasterClipID & ", "
    l_strSQL = l_strSQL & grdClips.Columns("eventID").Text & ", "
    l_strSQL = l_strSQL & Int(GetCount("SELECT count(sourceclipID) FROM eventcompilation WHERE eventID = " & l_lngMasterClipID & ";"))
    l_strSQL = l_strSQL & ");"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    frmClipCompile.adoSegment.Refresh

End If

End Sub

Private Sub cmdCopyGridAndChangeOwner_Click()

Dim l_lngNewCompanyID As Long, l_lngDestinationLibraryID As Long
Dim l_strNewLibraryBarcode As String, l_strNewFolder As String, l_blnPreserve_Masterfile_Status As Boolean, l_lngNewLibraryID As Long, l_strSQL As String, Count As Long, Sub_Path As String, Rebuilt_Path As String, l_blnActuallyMove As Boolean
Dim l_rstExtras As ADODB.Recordset, l_lngNewClipID As Long, l_lngClipID As Long, l_rstTemp As ADODB.Recordset, l_rstClips As ADODB.Recordset, FSO As Scripting.FileSystemObject, l_blnDuplicatesFound As Boolean
Dim PreserveFolderStructure As Boolean, TempStr As String

If chkRecordUpdateOnly.Value <> 0 Then
    MsgBox "Copy Grid and Change Owner can only be used to actually copy files."
    Exit Sub
End If

If optStorageType(0).Value = False Then
    MsgBox "You haven't selected DISCSTORES for this search." & vbCrLf & "You must select DISCSTORES to perform actual File Requests.", vbCritical
    optStorageType(0).Value = True
    cmdSearch.Value = True
End If

frmGetNewFileDetails.Caption = "Please give the details for the Copy..."
frmGetNewFileDetails.chkPreserveFolderStructure.Visible = True
frmGetNewFileDetails.Show vbModal

If Left(l_strNewLibraryBarcode, 3) = "BFS" Then
    If Not CheckAccess("/MoveToBFS") Then
        MsgBox "You do not have permission to move or copy to BFS", vbCritical
        Exit Sub
    End If
End If

l_strNewLibraryBarcode = UCase(frmGetNewFileDetails.txtBarcode.Text)
l_strNewFolder = frmGetNewFileDetails.txtAltLocation.Text
PreserveFolderStructure = frmGetNewFileDetails.chkPreserveFolderStructure.Value
If frmGetNewFileDetails.chkPreserveMasterfile.Value <> 0 Then l_blnPreserve_Masterfile_Status = True Else l_blnPreserve_Masterfile_Status = False
Unload frmGetNewFileDetails

l_lngDestinationLibraryID = Val(GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode))
If CheckDriveWritePermissions(l_lngDestinationLibraryID) = False Then
    MsgBox "You do not have permissinos to send file to that DISCSTORE", vbCritical
    Exit Sub
End If

If l_strNewLibraryBarcode <> "" Then

    l_lngNewLibraryID = GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode)
    If l_lngNewLibraryID <> 0 Then
    
        Set l_rstClips = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
        CheckForSQLError
        If l_rstClips.RecordCount > 0 Then
            frmGetNewCompany.Show vbModal
            If frmGetNewCompany.lblCompanyID.Caption <> "" Then
                l_lngNewCompanyID = Val(frmGetNewCompany.lblCompanyID.Caption)
            Else
                MsgBox "No new Owner - Selected - Operation Aborted"
                l_rstClips.Close
                Set l_rstClips = Nothing
                Exit Sub
            End If
            Unload frmGetNewCompany
            
            l_rstClips.MoveFirst
            Count = 0
            ProgressBar1.Max = l_rstClips.RecordCount
            ProgressBar1.Value = 0
            ProgressBar1.Visible = True
            Do While Not l_rstClips.EOF
                Set l_rstTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & l_rstClips("eventID"), g_strExecuteError)
                CheckForSQLError
                
                'Update the progress label
                ProgressBar1.Value = Count
                lblCurrentFilename.Caption = l_rstTemp("clipfilename")
                DoEvents
                
                l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & l_lngNewLibraryID
                If l_strNewFolder <> "" Then
                    l_strSQL = l_strSQL & " AND altlocation = '" & l_strNewFolder & "' "
                Else
                    l_strSQL = l_strSQL & " AND altlocation = '" & l_rstTemp("altlocation") & "' "
                End If
                l_strSQL = l_strSQL & " AND clipfilename = '" & l_rstTemp("clipfilename") & "'"
                l_strSQL = l_strSQL & " AND system_deleted = 0"
                If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
                    l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, NewCompanyID, PreserveMasterFileStatus, RequestDate, bigfilesize, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                    l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
                    l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
                    If PreserveFolderStructure = True Then
                        If Val(l_rstTemp("altlocation")) = l_rstTemp("companyID") Then
                            TempStr = Mid(l_rstTemp("altlocation"), Len(Trim(" " & l_rstTemp("companyID"))) + 2)
                        Else
                            TempStr = l_rstTemp("altlocation")
                        End If
                        If l_strNewFolder <> "" Then
                            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder & IIf(TempStr <> "", "\" & TempStr, "")) & "', "
                        Else
                            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstTemp("altlocation")) & "', "
                        End If
                    Else
                        If l_strNewFolder <> "" Then
                            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & "', "
                        Else
                            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstTemp("altlocation")) & "', "
                        End If
                    End If
                    If l_lngNewCompanyID <> 0 Then
                        l_strSQL = l_strSQL & l_lngNewCompanyID & ", "
                    Else
                        l_strSQL = l_strSQL & "Null, "
                    End If
                    If l_blnPreserve_Masterfile_Status = True Then
                        l_strSQL = l_strSQL & "1, "
                    Else
                        l_strSQL = l_strSQL & "0, "
                    End If
                    l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
                    l_strSQL = l_strSQL & l_rstTemp("bigfilesize") & ", "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                Else
                    l_blnDuplicatesFound = True
                End If
                    
                l_rstTemp.Close
                Set l_rstTemp = Nothing
                
                Count = Count + 1
                l_rstClips.MoveNext
                
            Loop
        End If
        l_rstClips.Close
        Set l_rstClips = Nothing
        ProgressBar1.Visible = False
        lblCurrentFilename.Caption = ""
        DoEvents
        If l_blnDuplicatesFound = True Then
            MsgBox "Some of the files already existed in the destination folder." & vbCrLf & "File requests were not made for those files", vbCritical
        End If
    Else
        MsgBox "That Store does not yet exist!", vbOKOnly, "Problem..."
    End If

End If

adoClip.Refresh
adoClip.Caption = grdClips.Rows & " Clips Found"

Exit Sub

End Sub

Private Sub cmdCopySelected_Click()

Dim l_strNewLibraryBarcode As String, l_strNewFolder As String, l_blnPreserve_Masterfile_Status As Boolean, l_lngNewLibraryID As Long, l_strSQL As String, Count As Long, Sub_Path As String, Rebuilt_Path As String, l_blnActuallyMove As Boolean
Dim l_rstExtras As ADODB.Recordset, l_lngNewClipID As Long, l_lngClipID As Long, l_rstTemp As ADODB.Recordset, FSO As Scripting.FileSystemObject, l_blnDuplicatesFound As Boolean
Dim l_lngCount As Long, bkmrk As Variant, l_lngDestinationLibraryID As Long, l_blnUnVerifiedFound As Boolean
Dim PreserveFolderStructure As Boolean, TempStr As String

If chkRecordUpdateOnly.Value <> 0 Then
    If MsgBox("Are you sure you wish to only Move File Records, rather than actually Moving Files?", vbYesNo, "Move Check") = vbNo Then
        Exit Sub
    End If
End If

'If lblLibraryID.Caption = "" Then
'    If MsgBox("You haven't selected a source drive for this search." & vbCrLf & "You must select a source drive to perform actual File Requests." & vbCrLf & "Do you wish to continue?", vbYesNo, "Question...") = vbNo Then
'        Exit Sub
'    End If
'End If
'
If optStorageType(0).Value = False And chkRecordUpdateOnly.Value = 0 Then
    MsgBox "You haven't selected DISCSTORES for this search." & vbCrLf & "You must select DISCSTORES to perform actual File Requests.", vbCritical
    optStorageType(0).Value = True
    cmdSearch.Value = True
    Exit Sub
End If

l_lngCount = grdClips.SelBookmarks.Count

If l_lngCount <= 0 Then
    MsgBox "No rows selected.", vbCritical, "Error with Copy Selected Rows"
    Exit Sub
End If

If MsgBox("Copy " & l_lngCount & " items - Are you sure", vbYesNo + vbDefaultButton2, "Copy Selected") = vbNo Then Exit Sub

frmGetNewFileDetails.Caption = "Please give the details for the Copy..."
frmGetNewFileDetails.chkPreserveFolderStructure.Visible = True
frmGetNewFileDetails.Show vbModal
l_strNewLibraryBarcode = UCase(frmGetNewFileDetails.txtBarcode.Text)

If Left(l_strNewLibraryBarcode, 3) = "BFS" Then
    If Not CheckAccess("/MoveToBFS") Then
        MsgBox "You do not have permission to move or copy to BFS", vbCritical
        Exit Sub
    End If
End If

l_strNewFolder = frmGetNewFileDetails.txtAltLocation.Text
PreserveFolderStructure = frmGetNewFileDetails.chkPreserveFolderStructure.Value
If frmGetNewFileDetails.chkPreserveMasterfile.Value <> 0 Then l_blnPreserve_Masterfile_Status = True Else l_blnPreserve_Masterfile_Status = False
Unload frmGetNewFileDetails
l_blnActuallyMove = False

l_lngDestinationLibraryID = Val(GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode))
If CheckDriveWritePermissions(l_lngDestinationLibraryID) = False Then
    MsgBox "You do not have permissinos to send file to that DISCSTORE", vbCritical
    Exit Sub
End If

If l_strNewLibraryBarcode <> "" Then

    l_lngNewLibraryID = GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode)
    If l_lngNewLibraryID <> 0 Then
    
        If GetData("library", "format", "libraryID", l_lngNewLibraryID) = "DISCSTORE" And chkRecordUpdateOnly.Value = 0 Then
            If MsgBox("Do you wish to actually copy the files, as well as change the CETA records", vbYesNo, "Question...") = vbYes Then l_blnActuallyMove = True Else l_blnActuallyMove = False
        End If
        
        ProgressBar1.Max = grdClips.SelBookmarks.Count
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        Count = 1
        
        For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1
        
            
            bkmrk = grdClips.SelBookmarks(l_lngCount)
            Set l_rstTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & Val(grdClips.Columns("eventID").CellText(bkmrk)) & ";", g_strExecuteError)
            CheckForSQLError
            
            'Update the progress label
            ProgressBar1.Value = Count
            lblCurrentFilename.Caption = l_rstTemp("clipfilename")
            DoEvents
            
            If l_blnActuallyMove = True Then
            
                l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & l_lngNewLibraryID
                If l_strNewFolder <> "" Then
                    l_strSQL = l_strSQL & " AND altlocation = '" & l_strNewFolder & "' "
                Else
                    l_strSQL = l_strSQL & " AND altlocation = '" & l_rstTemp("altlocation") & "' "
                End If
                l_strSQL = l_strSQL & " AND clipfilename = '" & l_rstTemp("clipfilename") & "'"
                l_strSQL = l_strSQL & " AND system_deleted = 0"
                If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
                    If Not IsNull(l_rstTemp("bigfilesize")) Then
                        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, PreserveMasterFileStatus, RequestDate, bigfilesize, RequestName, RequesterEmail) VALUES ("
                        l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                        l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
                        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
                        l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
                        If PreserveFolderStructure = True Then
                            If Val(l_rstTemp("altlocation")) = l_rstTemp("companyID") Then
                                TempStr = Mid(l_rstTemp("altlocation"), Len(Trim(" " & l_rstTemp("companyID"))) + 2)
                            Else
                                TempStr = l_rstTemp("altlocation")
                            End If
                            If l_strNewFolder <> "" Then
                                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & IIf(TempStr <> "", "\" & TempStr, "") & "', "
                            Else
                                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstTemp("altlocation")) & "', "
                            End If
                        Else
                            If l_strNewFolder <> "" Then
                                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & "', "
                            Else
                                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstTemp("altlocation")) & "', "
                            End If
                        End If
                        If l_blnPreserve_Masterfile_Status = True Then
                            l_strSQL = l_strSQL & "1, "
                        Else
                            l_strSQL = l_strSQL & "0, "
                        End If
                        l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
                        l_strSQL = l_strSQL & l_rstTemp("bigfilesize") & ", "
                        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                        Debug.Print l_strSQL
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                    Else
                        l_blnUnVerifiedFound = True
                    End If
                Else
                    l_blnDuplicatesFound = True
                End If
                
            Else
            
                'First copy the actual clip data.
                l_lngNewClipID = CopyFileEventToLibraryID(l_rstTemp("eventID"), l_lngNewLibraryID)
                If GetData("library", "format", "libraryID", l_lngNewLibraryID) = "DISCSTORE" Then
                    SetData "events", "online", "eventID", l_lngNewClipID, 1
                Else
                    SetData "events", "online", "eventID", l_lngNewClipID, 0
                End If
                If l_lngNewLibraryID = 284349 Or l_lngNewLibraryID = 248095 Or l_lngNewLibraryID = 276356 Or l_lngNewLibraryID = 441212 Or l_lngNewLibraryID = 623206 Or l_lngNewLibraryID = 650950 Then
                    SetData "events", "hidefromweb", "eventID", l_lngNewClipID, 0
                End If
                
                If l_strNewFolder <> "" Then
                    SetData "events", "altlocation", "eventID", l_lngNewClipID, l_strNewFolder
                End If
                
                If l_blnPreserve_Masterfile_Status = False Then
                    SetData "events", "fileversion", "eventID", l_lngNewClipID, "File Copy"
                End If
                
                l_lngClipID = l_rstTemp("eventID")
                
                'Duplicate any Segment Information
                l_strSQL = "SELECT * FROM eventsegment WHERE eventID = '" & l_lngClipID & "';"
                Set l_rstExtras = ExecuteSQL(l_strSQL, g_strExecuteError)
                CheckForSQLError
                
                If Not l_rstExtras.EOF Then
                    l_rstExtras.MoveFirst
                    
                    Do While Not l_rstExtras.EOF
                        l_strSQL = "INSERT INTO eventsegment (eventID, segmentreference, timecodestart, timecodestop, customfield1) VALUES ('"
                        l_strSQL = l_strSQL & l_lngNewClipID & "', '"
                        l_strSQL = l_strSQL & QuoteSanitise(l_rstExtras("segmentreference")) & "', '"
                        l_strSQL = l_strSQL & l_rstExtras("timecodestart") & "', '"
                        l_strSQL = l_strSQL & l_rstExtras("timecodestop") & "', '"
                        l_strSQL = l_strSQL & l_rstExtras("customfield1") & "');"
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                        l_rstExtras.MoveNext
                    Loop
                End If
                l_rstExtras.Close
                
                'Duplicate any Logging Information
                l_strSQL = "SELECT * FROM eventlogging WHERE eventID = '" & l_lngClipID & "';"
                Set l_rstExtras = ExecuteSQL(l_strSQL, g_strExecuteError)
                CheckForSQLError
                
                If Not l_rstExtras.EOF Then
                    l_rstExtras.MoveFirst
                    
                    Do While Not l_rstExtras.EOF
                        l_strSQL = "INSERT INTO eventlogging (eventID, segmentreference, timecodestart, timecodestop, note) VALUES ('"
                        l_strSQL = l_strSQL & l_lngNewClipID & "', '"
                        l_strSQL = l_strSQL & QuoteSanitise(l_rstExtras("segmentreference")) & "', '"
                        l_strSQL = l_strSQL & l_rstExtras("timecodestart") & "', '"
                        l_strSQL = l_strSQL & l_rstExtras("timecodestop") & "', '"
                        l_strSQL = l_strSQL & l_rstExtras("note") & "');"
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                        l_rstExtras.MoveNext
                    Loop
                End If
                l_rstExtras.Close
                
                'Duplicate any Ratings Advisory Information
                l_strSQL = "SELECT * FROM iTunes_Clip_Advisory WHERE eventID = '" & l_lngClipID & "';"
                Set l_rstExtras = ExecuteSQL(l_strSQL, g_strExecuteError)
                CheckForSQLError
                
                If Not l_rstExtras.EOF Then
                    l_rstExtras.MoveFirst
                    
                    Do While Not l_rstExtras.EOF
                        l_strSQL = "INSERT INTO iTunes_Clip_Advisory (eventID, advisorysystem, advisory) VALUES ('"
                        l_strSQL = l_strSQL & l_lngNewClipID & "', '"
                        l_strSQL = l_strSQL & l_rstExtras("advisorysystem") & "', '"
                        l_strSQL = l_strSQL & l_rstExtras("advisory") & "');"
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                        l_rstExtras.MoveNext
                    Loop
                End If
                l_rstExtras.Close
                
                'Duplicate any Products Information
                l_strSQL = "SELECT * FROM iTunes_product WHERE eventID = '" & l_lngClipID & "';"
                Set l_rstExtras = ExecuteSQL(l_strSQL, g_strExecuteError)
                CheckForSQLError
                
                If Not l_rstExtras.EOF Then
                    l_rstExtras.MoveFirst
                    
                    Do While Not l_rstExtras.EOF
                        l_strSQL = "INSERT INTO iTunes_product (eventID, territory, salesstartdate, clearedforsale, wholesalepricetier, preordersalesstartdate, "
                        l_strSQL = l_strSQL & "clearedforvod, salesenddate, vodtype, availableforvoddate, unavailableforvoddate, physicalreleasedate) VALUES ('"
                        l_strSQL = l_strSQL & l_lngNewClipID & "', '"
                        l_strSQL = l_strSQL & l_rstExtras("territory") & "', '"
                        l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("salesstartdate")) & "', '"
                        l_strSQL = l_strSQL & l_rstExtras("clearedforsale") & "', '"
                        l_strSQL = l_strSQL & l_rstExtras("wholesalepricetier") & "', '"
                        l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("preordersalesstartdate")) & "', '"
                        l_strSQL = l_strSQL & l_rstExtras("clearedforvod") & "', '"
                        l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("salesenddate")) & "', '"
                        l_strSQL = l_strSQL & l_rstExtras("vodtype") & "', '"
                        l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("availableforvoddate")) & "', '"
                        l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("unavailableforvoddate")) & "', '"
                        l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("physicalreleasedate")) & "');"
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                        l_rstExtras.MoveNext
                    Loop
                End If
                l_rstExtras.Close
                Set l_rstExtras = Nothing
                
                'Record the original clip usage
                l_strSQL = "INSERT INTO eventusage ("
                l_strSQL = l_strSQL & "eventID, dateused) VALUES ("
                l_strSQL = l_strSQL & "'" & l_lngClipID & "', "
                l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "'"
                l_strSQL = l_strSQL & ");"
                
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                
                'Record a History event
                l_strSQL = "INSERT INTO eventhistory ("
                l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                l_strSQL = l_strSQL & "'" & l_lngNewClipID & "', "
                l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                l_strSQL = l_strSQL & "'Created' "
                l_strSQL = l_strSQL & ");"
                
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            
            End If
            l_rstTemp.Close
            Set l_rstTemp = Nothing
            
            Count = Count + 1
                    
        Next
        ProgressBar1.Visible = False
        lblCurrentFilename.Caption = ""
        DoEvents
        If l_blnDuplicatesFound = True Then
            MsgBox "Some of the files already existed in the destination folder." & vbCrLf & "File requests were not made for those files", vbCritical
        End If
        If l_blnUnVerifiedFound = True Then
            MsgBox "Some of the files were unverified." & vbCrLf & "File requests were not made for those files", vbCritical
        End If
    Else
        MsgBox "That Store does not yet exist!", vbOKOnly, "Problem..."
    End If

End If

adoClip.Refresh
adoClip.Caption = grdClips.Rows & " Clips Found"

Exit Sub

End Sub

Private Sub cmdCopyGrid_Click()

Dim l_strNewLibraryBarcode As String, l_strNewFolder As String, l_blnPreserve_Masterfile_Status As Boolean, l_lngNewLibraryID As Long, l_strSQL As String, Count As Long, Sub_Path As String, Rebuilt_Path As String, l_blnActuallyMove As Boolean
Dim l_rstExtras As ADODB.Recordset, l_lngNewClipID As Long, l_lngClipID As Long, l_rstTemp As ADODB.Recordset, l_rstClips As ADODB.Recordset, FSO As Scripting.FileSystemObject, l_blnDuplicatesFound As Boolean, l_lngDestinationLibraryID As Long
Dim l_blnUnVerifiedFound As Boolean
Dim PreserveFolderStructure As Boolean, TempStr As String

If chkRecordUpdateOnly.Value <> 0 Then
    If MsgBox("Are you sure you wish to only Move File Records, rather than actually Moving Files?", vbYesNo, "Move Check") = vbNo Then
        Exit Sub
    End If
End If

'If lblLibraryID.Caption = "" Then
'    If MsgBox("You haven't selected a source drive for this search." & vbCrLf & "You must select a source drive to perform actual File Requests." & vbCrLf & "Do you wish to continue?", vbYesNo, "Question...") = vbNo Then
'        Exit Sub
'    End If
'End If
'
If optStorageType(0).Value = False And chkRecordUpdateOnly.Value = 0 Then
    MsgBox "You haven't selected DISCSTORES for this search." & vbCrLf & "You must select DISCSTORES to perform actual File Requests.", vbCritical
    optStorageType(0).Value = True
    cmdSearch.Value = True
    Exit Sub
End If

frmGetNewFileDetails.Caption = "Please give the details for the Copy..."
frmGetNewFileDetails.chkPreserveFolderStructure.Visible = True
frmGetNewFileDetails.Show vbModal
l_strNewLibraryBarcode = UCase(frmGetNewFileDetails.txtBarcode.Text)

If Left(l_strNewLibraryBarcode, 3) = "BFS" Then
    If Not CheckAccess("/MoveToBFS") Then
        MsgBox "You do not have permission to move or copy to BFS", vbCritical
        Exit Sub
    End If
End If

l_strNewFolder = frmGetNewFileDetails.txtAltLocation.Text
PreserveFolderStructure = frmGetNewFileDetails.chkPreserveFolderStructure.Value
If frmGetNewFileDetails.chkPreserveMasterfile.Value <> 0 Then l_blnPreserve_Masterfile_Status = True Else l_blnPreserve_Masterfile_Status = False
Unload frmGetNewFileDetails
l_blnActuallyMove = False

l_lngDestinationLibraryID = Val(GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode))
If CheckDriveWritePermissions(l_lngDestinationLibraryID) = False Then
    MsgBox "You do not have permissions to send file to that DISCSTORE", vbCritical
    Exit Sub
End If

If l_strNewLibraryBarcode <> "" Then

    l_lngNewLibraryID = GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode)
    If l_lngNewLibraryID <> 0 Then
    
        If GetData("library", "format", "libraryID", l_lngNewLibraryID) = "DISCSTORE" And chkRecordUpdateOnly.Value = 0 Then
            If MsgBox("Do you wish to actually copy the files, as well as change the CETA records", vbYesNo, "Question...") = vbYes Then l_blnActuallyMove = True Else l_blnActuallyMove = False
        End If
        
        Set l_rstClips = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
        CheckForSQLError
        If l_rstClips.RecordCount > 0 Then
            l_rstClips.MoveFirst
            Count = 0
            ProgressBar1.Max = l_rstClips.RecordCount
            ProgressBar1.Value = 0
            ProgressBar1.Visible = True
            Do While Not l_rstClips.EOF
                Set l_rstTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & l_rstClips("eventID"), g_strExecuteError)
                CheckForSQLError
                
                'Update the progress label
                ProgressBar1.Value = Count
                lblCurrentFilename.Caption = l_rstTemp("clipfilename")
                DoEvents
                
                If l_blnActuallyMove = True Then
                
                    l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & l_lngNewLibraryID
                    If l_strNewFolder <> "" Then
                        l_strSQL = l_strSQL & " AND altlocation = '" & l_strNewFolder & "' "
                    Else
                        l_strSQL = l_strSQL & " AND altlocation = '" & l_rstTemp("altlocation") & "' "
                    End If
                    l_strSQL = l_strSQL & " AND clipfilename = '" & l_rstTemp("clipfilename") & "'"
                    l_strSQL = l_strSQL & " AND system_deleted = 0"
                    If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
                        If Not IsNull(l_rstTemp("bigfilesize")) Then
                            l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, PreserveMasterFileStatus, RequestDate, bigfilesize, RequestName, RequesterEmail) VALUES ("
                            l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                            l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
                            l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
                            l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
                            If PreserveFolderStructure = True Then
                                If Val(l_rstTemp("altlocation")) = l_rstTemp("companyID") Then
                                    TempStr = Mid(l_rstTemp("altlocation"), Len(Trim(" " & l_rstTemp("companyID"))) + 2)
                                Else
                                    TempStr = l_rstTemp("altlocation")
                                End If
                                If l_strNewFolder <> "" Then
                                    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder & IIf(TempStr <> "", "\" & TempStr, "")) & "', "
                                Else
                                    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstTemp("altlocation")) & "', "
                                End If
                            Else
                                If l_strNewFolder <> "" Then
                                    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & "', "
                                Else
                                    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstTemp("altlocation")) & "', "
                                End If
                            End If
                            If l_blnPreserve_Masterfile_Status = True Then
                                l_strSQL = l_strSQL & "1, "
                            Else
                                l_strSQL = l_strSQL & "0, "
                            End If
                            l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
                            l_strSQL = l_strSQL & l_rstTemp("bigfilesize") & ", "
                            l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                            ExecuteSQL l_strSQL, g_strExecuteError
                            CheckForSQLError
                        Else
                            l_blnUnVerifiedFound = True
                        End If
                    Else
                        l_blnDuplicatesFound = True
                    End If
                    
                Else
                
                    'First copy the actual clip data.
                    l_lngNewClipID = CopyFileEventToLibraryID(l_rstClips("eventID"), l_lngNewLibraryID)
                    If GetData("library", "format", "libraryID", l_lngNewLibraryID) = "DISCSTORE" Then
                        SetData "events", "online", "eventID", l_lngNewClipID, 1
                    Else
                        SetData "events", "online", "eventID", l_lngNewClipID, 0
                    End If
                    If l_lngNewLibraryID = 284349 Or l_lngNewLibraryID = 248095 Or l_lngNewLibraryID = 276356 Or l_lngNewLibraryID = 441212 Or l_lngNewLibraryID = 623206 Or l_lngNewLibraryID = 650950 Then
                        SetData "events", "hidefromweb", "eventID", l_lngNewClipID, 0
                    End If
                    
                    If l_strNewFolder <> "" Then
                        SetData "events", "altlocation", "eventID", l_lngNewClipID, l_strNewFolder
                    End If
                    
                    If l_blnPreserve_Masterfile_Status = False Then
                        SetData "events", "fileversion", "eventID", l_lngNewClipID, "File Copy"
                    End If
                    
                    l_lngClipID = l_rstTemp("eventID")
                    
                    'Duplicate any Segment Information
                    l_strSQL = "SELECT * FROM eventsegment WHERE eventID = '" & l_lngClipID & "';"
                    Set l_rstExtras = ExecuteSQL(l_strSQL, g_strExecuteError)
                    CheckForSQLError
                    
                    If Not l_rstExtras.EOF Then
                        l_rstExtras.MoveFirst
                        
                        Do While Not l_rstExtras.EOF
                            l_strSQL = "INSERT INTO eventsegment (eventID, segmentreference, timecodestart, timecodestop, customfield1) VALUES ('"
                            l_strSQL = l_strSQL & l_lngNewClipID & "', '"
                            l_strSQL = l_strSQL & QuoteSanitise(l_rstExtras("segmentreference")) & "', '"
                            l_strSQL = l_strSQL & l_rstExtras("timecodestart") & "', '"
                            l_strSQL = l_strSQL & l_rstExtras("timecodestop") & "', '"
                            l_strSQL = l_strSQL & l_rstExtras("customfield1") & "');"
                            ExecuteSQL l_strSQL, g_strExecuteError
                            CheckForSQLError
                            l_rstExtras.MoveNext
                        Loop
                    End If
                    l_rstExtras.Close
                    
                    'Duplicate any Logging Information
                    l_strSQL = "SELECT * FROM eventlogging WHERE eventID = '" & l_lngClipID & "';"
                    Set l_rstExtras = ExecuteSQL(l_strSQL, g_strExecuteError)
                    CheckForSQLError
                    
                    If Not l_rstExtras.EOF Then
                        l_rstExtras.MoveFirst
                        
                        Do While Not l_rstExtras.EOF
                            l_strSQL = "INSERT INTO eventlogging (eventID, segmentreference, timecodestart, timecodestop, note) VALUES ('"
                            l_strSQL = l_strSQL & l_lngNewClipID & "', '"
                            l_strSQL = l_strSQL & QuoteSanitise(l_rstExtras("segmentreference")) & "', '"
                            l_strSQL = l_strSQL & l_rstExtras("timecodestart") & "', '"
                            l_strSQL = l_strSQL & l_rstExtras("timecodestop") & "', '"
                            l_strSQL = l_strSQL & l_rstExtras("note") & "');"
                            ExecuteSQL l_strSQL, g_strExecuteError
                            CheckForSQLError
                            l_rstExtras.MoveNext
                        Loop
                    End If
                    l_rstExtras.Close
                    
                    'Duplicate any Ratings Advisory Information
                    l_strSQL = "SELECT * FROM iTunes_Clip_Advisory WHERE eventID = '" & l_lngClipID & "';"
                    Set l_rstExtras = ExecuteSQL(l_strSQL, g_strExecuteError)
                    CheckForSQLError
                    
                    If Not l_rstExtras.EOF Then
                        l_rstExtras.MoveFirst
                        
                        Do While Not l_rstExtras.EOF
                            l_strSQL = "INSERT INTO iTunes_Clip_Advisory (eventID, advisorysystem, advisory) VALUES ('"
                            l_strSQL = l_strSQL & l_lngNewClipID & "', '"
                            l_strSQL = l_strSQL & l_rstExtras("advisorysystem") & "', '"
                            l_strSQL = l_strSQL & l_rstExtras("advisory") & "');"
                            ExecuteSQL l_strSQL, g_strExecuteError
                            CheckForSQLError
                            l_rstExtras.MoveNext
                        Loop
                    End If
                    l_rstExtras.Close
                    
                    'Duplicate any Products Information
                    l_strSQL = "SELECT * FROM iTunes_product WHERE eventID = '" & l_lngClipID & "';"
                    Set l_rstExtras = ExecuteSQL(l_strSQL, g_strExecuteError)
                    CheckForSQLError
                    
                    If Not l_rstExtras.EOF Then
                        l_rstExtras.MoveFirst
                        
                        Do While Not l_rstExtras.EOF
                            l_strSQL = "INSERT INTO iTunes_product (eventID, territory, salesstartdate, clearedforsale, wholesalepricetier, preordersalesstartdate, "
                            l_strSQL = l_strSQL & "clearedforvod, salesenddate, vodtype, availableforvoddate, unavailableforvoddate, physicalreleasedate) VALUES ('"
                            l_strSQL = l_strSQL & l_lngNewClipID & "', '"
                            l_strSQL = l_strSQL & l_rstExtras("territory") & "', '"
                            l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("salesstartdate")) & "', '"
                            l_strSQL = l_strSQL & l_rstExtras("clearedforsale") & "', '"
                            l_strSQL = l_strSQL & l_rstExtras("wholesalepricetier") & "', '"
                            l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("preordersalesstartdate")) & "', '"
                            l_strSQL = l_strSQL & l_rstExtras("clearedforvod") & "', '"
                            l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("salesenddate")) & "', '"
                            l_strSQL = l_strSQL & l_rstExtras("vodtype") & "', '"
                            l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("availableforvoddate")) & "', '"
                            l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("unavailableforvoddate")) & "', '"
                            l_strSQL = l_strSQL & FormatSQLDate(l_rstExtras("physicalreleasedate")) & "');"
                            ExecuteSQL l_strSQL, g_strExecuteError
                            CheckForSQLError
                            l_rstExtras.MoveNext
                        Loop
                    End If
                    l_rstExtras.Close
                    Set l_rstExtras = Nothing
                    
                    'Record the original clip usage
                    l_strSQL = "INSERT INTO eventusage ("
                    l_strSQL = l_strSQL & "eventID, dateused) VALUES ("
                    l_strSQL = l_strSQL & "'" & l_lngClipID & "', "
                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "'"
                    l_strSQL = l_strSQL & ");"
                    
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    
                    'Record a History event
                    l_strSQL = "INSERT INTO eventhistory ("
                    l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                    l_strSQL = l_strSQL & "'" & l_lngNewClipID & "', "
                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                    l_strSQL = l_strSQL & "'Created' "
                    l_strSQL = l_strSQL & ");"
                    
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                
                End If
                l_rstTemp.Close
                Set l_rstTemp = Nothing
                
                Count = Count + 1
                l_rstClips.MoveNext
                
            Loop
        End If
        l_rstClips.Close
        Set l_rstClips = Nothing
        ProgressBar1.Visible = False
        lblCurrentFilename.Caption = ""
        DoEvents
        If l_blnDuplicatesFound = True Then
            MsgBox "Some of the files already existed in the destination folder." & vbCrLf & "File requests were not made for those files", vbCritical
        End If
        If l_blnUnVerifiedFound = True Then
            MsgBox "Some of the files were unverified." & vbCrLf & "File requests were not made for those files", vbCritical
        End If
    Else
        MsgBox "That Store does not yet exist!", vbOKOnly, "Problem..."
    End If

End If

adoClip.Refresh
adoClip.Caption = grdClips.Rows & " Clips Found"

Exit Sub

End Sub

Private Sub cmdDeleteEmptyFolders_Click()

Dim l_strNewLibraryBarcode As String, l_strNewFolder As String, l_strSQL As String, l_lngNewLibraryID As Long

frmGetNewFileDetails.Caption = "Please give the Detailds of the Folder to do this on..."
frmGetNewFileDetails.optDeleteEmptyFolders = True
frmGetNewFileDetails.Show vbModal

l_strNewLibraryBarcode = UCase(frmGetNewFileDetails.txtBarcode.Text)
l_lngNewLibraryID = GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode)
l_strNewFolder = frmGetNewFileDetails.txtAltLocation.Text
Unload frmGetNewFileDetails

l_strSQL = "INSERT INTO event_file_request (event_file_request_TypeID, NewLibraryID, NewAltLocation, RequestDate, RequestName, RequesterEmail) VALUES ("
l_strSQL = l_strSQL & "41, "
l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
l_strSQL = l_strSQL & "'" & l_strNewFolder & "', "
l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Sub

Private Sub cmdDeleteGrid_Click()

Dim l_strSQL As String, l_YesNo As VbMsgBoxResult
Dim l_rstClips As ADODB.Recordset, l_lngClipID As Long, Count As Long, l_blnPurgeFiles As Boolean, l_strFullFilePath As String, FSO As FileSystemObject

If cmdDeleteGrid.Caption = "Delete Grid" Then
    
    If chkRecordUpdateOnly.Value <> 0 Then
        If MsgBox("Are you sure you wish to only Delete File Records, rather than actually Deleteing Files?", vbYesNo, "Delete Check") = vbNo Then
            Exit Sub
        End If
    End If
    
    If optStorageType(0).Value = False And chkRecordUpdateOnly.Value = 0 Then
        MsgBox "You haven't selected DISCSTORES for this search." & vbCrLf & "You must select DISCSTORES to perform actual File Requests.", vbCritical
        optStorageType(0).Value = True
        cmdSearch.Value = True
        Exit Sub
    End If
    
    l_YesNo = MsgBox("Are You Sure?", vbYesNo + vbDefaultButton2, "Delete Entire Grid")
    If l_YesNo = vbYes Then
    
        'Sanitise the portal access table to remove all entries for the clips that are about to be deleted.
        If chkSearchArchive.Value = 0 Then
'            m_blnSilentSanitise = True
'            lblCurrentFilename.Caption = "Removing the Clips"
'            DoEvents
'            cmdPortalSanitise.Value = True
'            m_blnSilentSanitise = False
            Set l_rstClips = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
            CheckForSQLError
            l_rstClips.MoveFirst
            Count = 1
            ProgressBar1.Max = l_rstClips.RecordCount
            ProgressBar1.Value = 0
            ProgressBar1.Visible = True
            
            If chkRecordUpdateOnly.Value = 0 Then
                If CheckAccess("/PurgeFilesFromDisk", True) Then
                    If MsgBox("Do you wish to attempt to delete the files from disk, if they are on a DISCSTORE?", vbYesNo + vbDefaultButton2, "Purge from disk") = vbYes Then
                        If MsgBox("Are you sure?", vbYesNo + vbDefaultButton2, "Purge from Disk") = vbYes Then
                            l_blnPurgeFiles = True
                        Else
                            MsgBox "Aborted"
                            Exit Sub
                        End If
                    Else
                        MsgBox "Aborted"
                        Exit Sub
                    End If
                End If
            End If
            Do While Not l_rstClips.EOF
            
                ProgressBar1.Value = Count
                DoEvents
                
                If l_blnPurgeFiles = True Then
                    If Trim(" " & l_rstClips("bigfilesize")) <> "" And Trim(" " & l_rstClips("clipformat")) <> "Folder" And UCase(GetData("library", "format", "libraryID", l_rstClips("libraryID"))) = "DISCSTORE" Then
                        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, RequestDate, RequestName, RequesterEmail) VALUES ("
                        l_strSQL = l_strSQL & l_rstClips("eventID") & ", "
                        l_strSQL = l_strSQL & l_rstClips("libraryID") & ", "
                        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Delete") & ", "
                        l_strSQL = l_strSQL & GetData("events", "libraryID", "eventID", l_rstClips("eventID")) & ", "
                        l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
                        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                    Else
                        l_strSQL = "UPDATE events SET system_deleted = 1, hidefromweb = -1, online = 0 WHERE eventID = " & l_rstClips("eventID") & ";"
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                        'Record a History event
                        l_strSQL = "INSERT INTO eventhistory ("
                        l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                        l_strSQL = l_strSQL & "'" & l_rstClips("eventID") & "', "
                        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                        l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                        l_strSQL = l_strSQL & "'Clip Record Deleted' "
                        l_strSQL = l_strSQL & ");"
                        
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                    End If
                Else
                    l_strSQL = "UPDATE events SET system_deleted = 1, hidefromweb = -1, online = 0 WHERE eventID = " & l_rstClips("eventID") & ";"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                
                    'Record a History event
                    l_strSQL = "INSERT INTO eventhistory ("
                    l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                    l_strSQL = l_strSQL & "'" & l_rstClips("eventID") & "', "
                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                    l_strSQL = l_strSQL & "'Clip Record Deleted' "
                    l_strSQL = l_strSQL & ");"
                    
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                End If
                
                Count = Count + 1
                l_rstClips.MoveNext
                
            Loop
            l_rstClips.Close
            Set l_rstClips = Nothing
            ProgressBar1.Visible = False
            lblCurrentFilename.Caption = ""
    
        End If
    
    End If
    
Else

    l_YesNo = MsgBox("Records will be restored to where they were before being deleted." & vbCrLf & "Are You Sure?", vbYesNo, "UnDelete Entire Grid")
    If l_YesNo = vbYes Then
    
        'Sanitise the portal access table to remove all entries for the clips that are about to be deleted.
        If chkSearchArchive.Value = 1 Then
            lblCurrentFilename.Caption = "UnDeleting the Clip Records"
            DoEvents
            Set l_rstClips = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
            CheckForSQLError
            l_rstClips.MoveFirst
            Count = 1
            ProgressBar1.Max = l_rstClips.RecordCount
            ProgressBar1.Value = 0
            ProgressBar1.Visible = True
            Do While Not l_rstClips.EOF
            
                ProgressBar1.Value = Count
                DoEvents
                l_strSQL = "UPDATE events SET system_deleted = 0, hidefromweb = -1, online = 0 WHERE eventID = " & l_rstClips("eventID") & ";"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            
                'Record a History event
                l_strSQL = "INSERT INTO eventhistory ("
                l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                l_strSQL = l_strSQL & "'" & l_rstClips("eventID") & "', "
                l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                l_strSQL = l_strSQL & "'Clip Record UnDeleted' "
                l_strSQL = l_strSQL & ");"
                
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                
                Count = Count + 1
                l_rstClips.MoveNext
            
            Loop
            l_rstClips.Close
            Set l_rstClips = Nothing
            ProgressBar1.Visible = False
            lblCurrentFilename.Caption = ""
    
        End If
    
    End If
    
End If

cmdSearch.Value = True
Exit Sub

End Sub

Private Sub cmdDeleteSelected_Click()

Dim l_strSQL As String, l_YesNo As VbMsgBoxResult
Dim l_rstTemp As ADODB.Recordset, l_lngClipID As Long, Count As Long, l_blnPurgeFiles As Boolean, l_strFullFilePath As String, FSO As FileSystemObject
Dim l_lngCount As Long, bkmrk As Variant

If cmdDeleteSelected.Caption = "Delete Selected" Then
    If chkRecordUpdateOnly.Value <> 0 Then
        If MsgBox("Are you sure you wish to only Delete File Records, rather than actually Deleteing Files?", vbYesNo, "Delete Check") = vbNo Then
            Exit Sub
        End If
    End If
    
    If optStorageType(0).Value = False And chkRecordUpdateOnly.Value = 0 Then
        MsgBox "You haven't selected DISCSTORES for this search." & vbCrLf & "You must select DISCSTORES to perform actual File Requests.", vbCritical
        optStorageType(0).Value = True
        cmdSearch.Value = True
        Exit Sub
    End If
    
    l_lngCount = grdClips.SelBookmarks.Count
    
    If l_lngCount <= 0 Then
        MsgBox "No rows selected.", vbCritical, "Error with Delete Selected Rows"
        Exit Sub
    End If
    
    If MsgBox("Delete " & l_lngCount & " items - Are you sure", vbYesNo + vbDefaultButton2, "Delete Selected") = vbNo Then Exit Sub
    
    If chkRecordUpdateOnly.Value = 0 Then
        If CheckAccess("/PurgeFilesFromDisk", True) Then
            If MsgBox("Do you wish to attempt to delete the files from disk, if they are on a DISCSTORE?", vbYesNo + vbDefaultButton2, "Purge from disk") = vbYes Then
                If MsgBox("Are you sure?", vbYesNo + vbDefaultButton2, "Purge from Disk") = vbYes Then
                    l_blnPurgeFiles = True
                Else
                    MsgBox "Aborted"
                    Exit Sub
                End If
            Else
                MsgBox "Aborted"
                Exit Sub
            End If
        End If
    End If
    
    'Sanitise the portal access table to remove all entries for the clips that are about to be deleted.
    If chkSearchArchive.Value = 0 Then
        m_blnSilentSanitise = True
        lblCurrentFilename.Caption = "Removing the Clips"
        DoEvents
        ProgressBar1.Max = grdClips.SelBookmarks.Count
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        Count = 1
        
        For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1
        
            
            bkmrk = grdClips.SelBookmarks(l_lngCount)
            Set l_rstTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & Val(grdClips.Columns("eventID").CellText(bkmrk)) & ";", g_strExecuteError)
            CheckForSQLError
            
            'Update the progress label
            ProgressBar1.Value = Count
            lblCurrentFilename.Caption = l_rstTemp("clipfilename")
            DoEvents
            
            ProgressBar1.Value = Count
            DoEvents
            
            If l_blnPurgeFiles = True Then
                If Trim(" " & l_rstTemp("bigfilesize")) <> "" And Trim(" " & l_rstTemp("clipformat")) <> "Folder" And UCase(GetData("library", "format", "libraryID", l_rstTemp("libraryID"))) = "DISCSTORE" Then
                    l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, RequestDate, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                    l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Delete") & ", "
                    l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
                    l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                Else
                    l_strSQL = "UPDATE events SET system_deleted = 1, hidefromweb = -1, online = 0 WHERE eventID = " & l_rstTemp("eventID") & ";"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    'Record a History event
                    l_strSQL = "INSERT INTO eventhistory ("
                    l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                    l_strSQL = l_strSQL & "'" & l_rstTemp("eventID") & "', "
                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                    l_strSQL = l_strSQL & "'Clip Record Deleted' "
                    l_strSQL = l_strSQL & ");"
                    
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                End If
            Else
                l_strSQL = "UPDATE events SET system_deleted = 1, hidefromweb = -1, online = 0 WHERE eventID = " & l_rstTemp("eventID") & ";"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            
                'Record a History event
                l_strSQL = "INSERT INTO eventhistory ("
                l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                l_strSQL = l_strSQL & "'" & l_rstTemp("eventID") & "', "
                l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                l_strSQL = l_strSQL & "'Clip Record Deleted' "
                l_strSQL = l_strSQL & ");"
                
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            End If
            
            Count = Count + 1
            l_rstTemp.Close
            Set l_rstTemp = Nothing
            
        Next
        Set l_rstTemp = Nothing
        ProgressBar1.Visible = False
        lblCurrentFilename.Caption = ""
        DoEvents
    
    End If
Else
    l_YesNo = MsgBox("Records will be restored to where they were before being deleted." & vbCrLf & "Are You Sure?", vbYesNo, "UnDelete Selected Items")
    If l_YesNo = vbYes Then
    
        ProgressBar1.Max = grdClips.SelBookmarks.Count
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        Count = 1
        
        For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1
        
            
            bkmrk = grdClips.SelBookmarks(l_lngCount)
            Set l_rstTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & Val(grdClips.Columns("eventID").CellText(bkmrk)) & ";", g_strExecuteError)
            CheckForSQLError
            
            'Update the progress label
            ProgressBar1.Value = Count
            lblCurrentFilename.Caption = l_rstTemp("clipfilename")
            DoEvents
            
            ProgressBar1.Value = Count
            DoEvents
            
            l_strSQL = "UPDATE events SET system_deleted = 0, hidefromweb = -1, online = 0 WHERE eventID = " & l_rstTemp("eventID") & ";"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
        
            'Record a History event
            l_strSQL = "INSERT INTO eventhistory ("
            l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
            l_strSQL = l_strSQL & "'" & l_rstTemp("eventID") & "', "
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
            l_strSQL = l_strSQL & "'Clip Record UnDeleted' "
            l_strSQL = l_strSQL & ");"
            
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            
            Count = Count + 1
        Next
        ProgressBar1.Visible = False
        lblCurrentFilename.Caption = ""
    
    End If
    
End If

cmdSearch.Value = True
Exit Sub

End Sub

Private Sub cmdDreamWorks_Click()

Dim l_strFilename As String, oExecl As Excel.Application, oWorkbook As Excel.Workbook, l_intLinecount As Integer
Dim l_rstClip As ADODB.Recordset

If adoClip.Recordset.RecordCount <= 0 Then Exit Sub
If Val(lblLibraryID.Caption) = 0 Then Exit Sub
If lblFormat.Caption <> "DISCSTORE" Then Exit Sub

If adoClip.Recordset.RecordCount > 20 Then
    If MsgBox("Outputting Dreamworks File Reports for " & adoClip.Recordset.RecordCount & vbCrLf & "Are you sure?", vbYesNo, "There are more than 20 items in the grid.") = vbNo Then
        Exit Sub
    End If
End If

adoClip.Recordset.MoveFirst

l_intLinecount = 0
ProgressBar1.Max = adoClip.Recordset.RecordCount
ProgressBar1.Value = 0
ProgressBar1.Visible = True
DoEvents
Set oExcel = CreateObject("Excel.Application")

'loop through all the records
Do While Not adoClip.Recordset.EOF
    ProgressBar1.Value = l_intLinecount
    DoEvents
    l_strFilename = GetData("library", "subtitle", "libraryID", Val(lblLibraryID.Caption))
    l_strFilename = l_strFilename & "\" & adoClip.Recordset("altlocation")
    l_strFilename = l_strFilename & "\" & adoClip.Recordset("clipreference") & ".xls"
    Set oWorkbook = oExcel.Workbooks.Open(g_strLocationOfOmneonXLS & "\DreamworksTemplate.xls")
    On Error GoTo TryAgain
TryAgain:
    oWorkbook.SaveAs l_strFilename
    Set oWorksheet = oWorkbook.Worksheets("TX METADATA")
    On Error GoTo 0

    Set l_rstClip = ExecuteSQL("SELECT * FROM events WHERE eventID = " & adoClip.Recordset("eventID"), g_strExecuteError)
    oWorksheet.Cells(3, 1).Value = 1
    oWorksheet.Cells(3, 2).Value = l_rstClip("clipreference")
    oWorksheet.Cells(3, 3).Value = l_rstClip("eventtitle")
    oWorksheet.Cells(3, 3).Value = l_rstClip("eventepisode")
    oWorksheet.Cells(3, 5).Value = l_rstClip("eventsubtitle")
    oWorksheet.Cells(3, 6).Value = 1
    oWorksheet.Cells(3, 7).Value = l_rstClip("timecodestart")
    oWorksheet.Cells(3, 8).Value = l_rstClip("timecodestop")
    oWorksheet.Cells(3, 9).Value = l_rstClip("fd_length")
    If Val(l_rstClip("cliphorizontalpixels")) > 720 Then
        oWorksheet.Cells(3, 10).Value = "HD"
    Else
        If l_rstClip("geometriclinearity") = "ANAMORPHIC" Then
            oWorksheet.Cells(3, 10).Value = "SDW"
        Else
            oWorksheet.Cells(3, 10).Value = "SD"
        End If
    End If
    oWorksheet.Cells(3, 11).Value = l_rstClip("audiolanguagegroup1")
    
    l_rstClip.Close
    On Error GoTo ANDAGAIN
ANDAGAIN:
    oWorkbook.Save
    On Error GoTo 0
    Set oWorksheet = Nothing
    oWorkbook.Close vbNo
    Set oWorkbook = Nothing

    l_intLinecount = l_intLinecount + 1
    adoClip.Recordset.MoveNext
    
Loop

EXIT_FUNCTION:

ProgressBar1.Visible = False
DoEvents

Set l_rstClip = Nothing
oExcel.Quit

MsgBox "Complete"

End Sub

Private Sub cmdDreamWorksAudioDeliveries_Click()

Dim l_rst As ADODB.Recordset, l_strBarcode As String, l_strClipfilename As String, l_lngClipID As Long, l_strSQL As String, FSO As Scripting.FileSystemObject, l_lngLibraryID As Long, l_strPath As String
Dim l_lngCount As Long

Set FSO = New Scripting.FileSystemObject

l_strSQL = "SELECT * FROM DWTitlesCETADelivered WHERE ProcessedDate IS NULL"
Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
If l_rst.RecordCount > 0 Then
    l_rst.MoveFirst
    l_lngCount = 0
    ProgressBar1.Max = l_rst.RecordCount
    ProgressBar1.Visible = True
    ProgressBar1.Value = l_lngCount
    DoEvents
    Do While Not l_rst.EOF
        lblCurrentFilename.Caption = l_lngCount & ": " & l_rst("Filename")
        DoEvents
        ' Find a file
        l_lngClipID = Val(Trim(" " & GetDataSQL("SELECT eventID FROM vw_Events_on_DISCSTORE WHERE clipfilename = '" & l_rst("Filename") & "' AND system_deleted = 0 AND companyID = 1455")))
        If l_lngClipID <> 0 Then
            ' Verify the File
            l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngClipID)
            l_strPath = GetData("library", "subtitle", "libraryID", l_lngLibraryID)
            If Trim(" " & GetData("events", "altlocation", "eventID", l_lngClipID)) <> "" Then
                l_strPath = l_strPath & "\" & GetData("events", "altlocation", "eventID", l_lngClipID)
            End If
            l_strPath = l_strPath & "\" & l_rst("Filename")
            If FSO.FileExists(l_strPath) Then
                ' Move Request to BFS-PRIVATE 1455\Dreamworks_Audio_Deliveries
                l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, RequestDate, RequestName, RequesterEmail) VALUES ("
                l_strSQL = l_strSQL & l_lngClipID & ", "
                l_strSQL = l_strSQL & l_lngLibraryID & ", "
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
                l_strSQL = l_strSQL & "719952, "
                l_strSQL = l_strSQL & "'1455\Dreamworks_Audio_Deliveries', "
                l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
                l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                'Mark the row as processed
                l_rst("ProcessedDate") = Now
                l_rst.Update
            End If
        End If
        l_rst.MoveNext
        l_lngCount = l_lngCount + 1
        ProgressBar1.Value = l_lngCount
        DoEvents
    Loop
End If
l_rst.Close
Set l_rst = Nothing
ProgressBar1.Visible = False
lblCurrentFilename.Caption = ""

End Sub

Private Sub cmdExpireUnassignedClips_Click()

Dim l_lngClipID As Long, rstClip As ADODB.Recordset, l_lngOldLibraryID As Long, cnn2 As ADODB.Connection, SQL As String
Dim l_strFilePath As String, l_strClipfilename As String, FSO As Scripting.FileSystemObject, l_strEmail As String

If adoClip.Recordset.RecordCount = 0 Then Beep: Exit Sub

lblCurrentFilename.Caption = "Expiring all usassigned clips ofr Grid"
DoEvents

Set FSO = New Scripting.FileSystemObject

adoClip.Recordset.MoveFirst
Do While Not adoClip.Recordset.EOF
    If adoClip.Recordset("MW") = 0 Then
        l_lngClipID = adoClip.Recordset("eventID")
        Set rstClip = ExecuteSQL("SELECT * FROM events WHERE eventID = " & l_lngClipID & ";", g_strExecuteError)
        CheckForSQLError
        If rstClip.RecordCount = 1 Then
            l_lngOldLibraryID = rstClip("libraryID")
            l_strClipfilename = rstClip("clipfilename")
            If l_lngOldLibraryID = 248095 Or l_lngOldLibraryID = 284349 Or l_lngOldLibraryID = 441212 Or l_lngOldLibraryID = 623206 Then
                
                rstClip("system_deleted") = 1
                rstClip("hidefromweb") = -1
                rstClip.Update
                
                Select Case l_lngOldLibraryID
                    Case 248095
                        l_strFilePath = GetData("library", "subtitle", "barcode", "STREAMSTORE")
                    Case 284349
                        l_strFilePath = GetData("library", "subtitle", "barcode", "ASPERASTORE")
                    Case 441212
                        l_strFilePath = GetData("library", "subtitle", "barcode", "ASPERA-2")
                    Case 623206
                        l_strFilePath = GetData("library", "subtitle", "barcode", "ASPERA-3")
                End Select
                If Trim(" " & rstClip("altlocation")) <> "" Then
                    l_strFilePath = l_strFilePath & "\" & rstClip("altlocation")
                End If
                l_strFilePath = l_strFilePath & "\" & l_strClipfilename
                On Error Resume Next
                FSO.DeleteFile l_strFilePath, True
                On Error GoTo 0
                l_strEmail = "ClipID: " & l_lngClipID & ", Full Filename: " & l_strFilePath
                SendSMTPMail g_strAdministratorEmailAddress, g_strAdministratorEmailName, "Portal Clip Permission Expiry", "", "Portal Clip Permission Expiry: Clip Physically Deleted" & l_strEmail, True, "", "", g_strAdministratorEmailAddress
                
            End If
        End If
        rstClip.Close
    End If
    adoClip.Recordset.MoveNext
Loop

Set rstClip = Nothing
Set FSO = Nothing

grdClips.Refresh
lblCurrentFilename.Caption = ""

MsgBox "Done"
Exit Sub

End Sub

Private Sub cmdExport_Click()

If grdClips.Rows < 1 Then Exit Sub

Dim l_strFilePath As String
l_strFilePath = fGetSpecialFolder(CSIDL_DOCUMENTS)

On Error GoTo PROBLEM
grdClips.Export ssExportTypeText, ssExportColumnHeaders + ssExportAllRows, l_strFilePath & "clipsearch.txt"
MsgBox "Exported to a text file in 'My Documents' called 'clipsearch.txt'", vbInformation

ENDING:
Exit Sub

PROBLEM:
MsgBox "Could not export the grid - perhaps clipsearch.txt is already open?"
GoTo ENDING

End Sub

Private Sub cmdExportToLatimer_Click()

'Has to be some records int he grid
If adoClip.Recordset.RecordCount <= 0 Then Exit Sub

'Has to be a companyID
If Val(lblCompanyID.Caption) = 0 Then
    MsgBox "A Single client must be selected for File Sends.", vbCritical
    Exit Sub
End If

''Has to be from either a DISCSTORE or the DIVA
'If (optStorageType(0).Value = False And lblFormat.Caption <> "DISCSTORE") And (optStorageType(4).Value = False And lblFormat.Caption <> "DIVA") Then
'    MsgBox "We can only Delivery items that are on a DISCSTORE or the DIVA at this time.", vbCritical
'    Exit Sub
'End If

Dim l_strSQL As String, Count As Long
Dim l_lngCount As Long, bkmrk As Variant, l_rstTemp As ADODB.Recordset
Dim l_lngNewLibraryID As Long, l_lngNewEventID As Long
Dim l_blnAlreadySent As Boolean

'Give the fitst 'Are You Sure?'
If MsgBox("You are about to Permanently Export these files to Latimer Road as part of Client Migration." & vbCrLf & "Are you Sure", vbYesNo) = vbNo Then Exit Sub

If MsgBox("Just Export selected files (instead of whole grid)", vbYesNo + vbDefaultButton2, "Exporting Grid") = vbYes Then
    
    l_lngCount = grdClips.SelBookmarks.Count
    
    If l_lngCount <= 0 Then
        MsgBox "No rows selected.", vbCritical, "Error with Send via Aspera"
        Exit Sub
    End If
    
    If MsgBox("Export " & l_lngCount & " items - Are you sure", vbYesNo + vbDefaultButton2, "Exporting Grid") = vbNo Then Exit Sub
    
    ProgressBar1.Max = grdClips.SelBookmarks.Count
    ProgressBar1.Value = 0
    ProgressBar1.Visible = True
    Count = 1
    
    For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1
        bkmrk = grdClips.SelBookmarks(l_lngCount)
        Set l_rstTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & Val(grdClips.Columns("eventID").CellText(bkmrk)) & ";", g_strExecuteError)
        CheckForSQLError
        
        ProgressBar1.Value = Count
        DoEvents
        
        If l_rstTemp("clipfilename") <> ".DS_Store" Then
            'Invoke an 'Export to Latimer' file request
            
            If GetDataSQL("SELECT eventID FROM events WHERE clipfilename = '" & QuoteSanitise(l_rstTemp("clipfilename")) & "' AND bigfilesize = '" & l_rstTemp("bigfilesize") & "' AND system_deleted = 0 AND libraryID = 744815") <> "" Then
                l_blnAlreadySent = True
            Else
                l_blnAlreadySent = False
            End If
            If l_blnAlreadySent = False Or GetData("setting", "value", "name", "RigorousLatimerCheck") = "OFF" Then
                l_lngNewLibraryID = 744815 'GetData("library", "libraryID", "barcode", "LATIMER")
                l_lngNewEventID = CopyFileEventToLibraryID(Val(l_rstTemp("eventID")), l_lngNewLibraryID, True)
                SetData "events", "sourceEventID", "eventID", l_lngNewEventID, l_rstTemp("eventID")
                SetData "events", "sourcefrommedia", "eventID", l_lngNewEventID, 1
                SetData "events", "online", "eventID", l_lngNewEventID, 0
                If l_lngNewEventID <> 0 Then
                    If Not IsNull(l_rstTemp("bigfilesize")) Then
                        SetData "events", "sourceeventID", "eventID", l_lngNewEventID, l_rstTemp("eventID")
                    
                        l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewFileID, NewLibraryID, bigfilesize, RequestDate, RequestName, RequesterEmail) VALUES ("
                        l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                        l_strSQL = l_strSQL & l_rstTemp("LibraryID") & ", "
                        l_strSQL = l_strSQL & 38 'GetData("event_file_request_type", "event_file_request_typeID", "description", "Export to S3") & ", "
                        l_strSQL = l_strSQL & l_lngNewEventID & ", " & l_lngNewLibraryID & ", "
                        l_strSQL = l_strSQL & l_rstTemp("bigfilesize") & ", "
                        l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
                        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                        Debug.Print l_strSQL
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                        
                        l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, bigfilesize, accesstype, finalstatus) VALUES ("
                        l_strSQL = l_strSQL & "3684, "
                        l_strSQL = l_strSQL & l_rstTemp("companyID") & ", "
                        l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                        l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                        l_strSQL = l_strSQL & l_rstTemp("bigfilesize") & ", "
                        l_strSQL = l_strSQL & "'Export to Latimer Rd.', "
                        l_strSQL = l_strSQL & "'Completed')"
                        Debug.Print l_strSQL
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                    Else
                        If MsgBox("Unverified file " & vbCrLf & l_rstTemp("clipfilename") & vbCrLf & "Abort Operation", vbYesNo + vbDefaultButton2) = vbYes Then
                            l_lngCount = grdClips.SelBookmarks.Count - 1
                        Else
                            MsgBox "Unverified File Ignored."
                        End If
                    End If
                End If
            ElseIf l_blnAlreadySent = True And GetData("setting", "value", "name", "RigorousLatimerCheck") <> "OFF" Then
                If MsgBox("Item " & l_rstTemp("clipfilename") & " perviously sent to Latimer" & vbCrLf & "Abort Operation", vbYesNo + vbDefaultButton2) = vbYes Then
                    l_lngCount = grdClips.SelBookmarks.Count - 1
                Else
                    MsgBox "Already Sent Item Skipped."
                End If
            End If
        End If
        Count = Count + 1
    Next
Else
    If MsgBox("You are about to Export the whole grid" & vbCrLf & "Are you Sure", vbYesNo + vbDefaultButton2, "Final Confirmation") = vbNo Then
        Exit Sub
    End If
            
    Count = 1
    ProgressBar1.Max = adoClip.Recordset.RecordCount
    ProgressBar1.Value = 0
    ProgressBar1.Visible = True
    adoClip.Recordset.MoveFirst
    Do While Not adoClip.Recordset.EOF
        ProgressBar1.Value = Count
        DoEvents
        If GetDataSQL("SELECT eventID FROM events WHERE clipfilename = '" & QuoteSanitise(adoClip.Recordset("clipfilename")) & "' AND bigfilesize = '" & adoClip.Recordset("bigfilesize") & "' AND system_deleted = 0 AND libraryID = 744815") <> "" Then
            l_blnAlreadySent = True
        Else
            l_blnAlreadySent = False
        End If
        If l_blnAlreadySent = False Or GetData("setting", "value", "name", "RigorousLatimerCheck") = "OFF" Then
            l_lngNewLibraryID = 744815 'GetData("library", "libraryID", "barcode", "LATIMER")
            l_lngNewEventID = CopyFileEventToLibraryID(Val(adoClip.Recordset("eventID")), l_lngNewLibraryID, True)
            SetData "events", "sourceEventID", "eventID", l_lngNewEventID, adoClip.Recordset("eventID")
            SetData "events", "sourcefrommedia", "eventID", l_lngNewEventID, 1
            SetData "events", "online", "eventID", l_lngNewEventID, 0
            If l_lngNewEventID <> 0 Then
        
                If Not IsNull(adoClip.Recordset("bigfilesize")) Then
                     SetData "events", "sourceeventID", "eventID", l_lngNewEventID, adoClip.Recordset("eventID")
                
                    'Invoke a copy to the hotfolder location
                    l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewFileID, NewLibraryID, bigfilesize, RequestDate, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                    l_strSQL = l_strSQL & adoClip.Recordset("LibraryID") & ", "
                    l_strSQL = l_strSQL & 38 & ", " 'GetData("event_file_request_type", "event_file_request_typeID", "description", "Export to S3")
                    l_strSQL = l_strSQL & l_lngNewEventID & ", " & l_lngNewLibraryID & ", "
                    l_strSQL = l_strSQL & adoClip.Recordset("bigfilesize") & ", "
                    l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    
                    l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, bigfilesize, accesstype, finalstatus) VALUES ("
                    l_strSQL = l_strSQL & "3684, "
                    l_strSQL = l_strSQL & adoClip.Recordset("companyID") & ", "
                    l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                    l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                    l_strSQL = l_strSQL & adoClip.Recordset("bigfilesize") & ", "
                    l_strSQL = l_strSQL & "'Export to Latimer Rd.', "
                    l_strSQL = l_strSQL & "'Completed')"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                Else
                    If MsgBox("Unverified file " & vbCrLf & adoClip.Recordset("clipfilename") & vbCrLf & "Abort Operation", vbYesNo + vbDefaultButton2) = vbYes Then
                        Exit Do
                    Else
                        MsgBox "Unverified File Ignored."
                    End If
                End If
            End If
        ElseIf l_blnAlreadySent = True And GetData("setting", "value", "name", "RigorousLatimerCheck") <> "OFF" Then
            If MsgBox("Item " & adoClip.Recordset("clipfilename") & " perviously sent to Latimer" & vbCrLf & "Abort Operation", vbYesNo + vbDefaultButton2) = vbYes Then
                l_lngCount = grdClips.SelBookmarks.Count - 1
            Else
                MsgBox "Already Sent Item Skipped."
            End If
        End If
        Count = Count + 1
        adoClip.Recordset.MoveNext
    Loop
End If
ProgressBar1.Visible = False

MsgBox "Done"

End Sub

Private Sub cmdFindLongClips_Click()

txtSQLQuery.Text = " dbo.Timecode_To_Framenumber (fd_length,'25') >= 7500 "
cmdSearch.Value = True

End Sub

Private Sub cmdHideGrid_Click()

Dim l_strSQL As String, l_intResponse As Integer, Count As Long
Dim l_strUpdateSet As String, l_rstTemp As ADODB.Recordset

On Error GoTo BULK_UPDATE_ERROR

'Check all the checkboxes, and for any that are true include their field in the update set
l_strUpdateSet = "mdate = '" & FormatSQLDate(Now) & "', muser = '" & g_strUserInitials & "'"

l_intResponse = MsgBox("Do you wish to Hide the Clips?", vbYesNoCancel, "Hide / Unhide Grid")

If l_intResponse = vbYes Then
    l_strUpdateSet = l_strUpdateSet & ", hidefromweb = 1"
ElseIf l_intResponse = vbNo Then
    l_strUpdateSet = l_strUpdateSet & ", hidefromweb = 0, MediaWindowDeleteDate = Null"
Else
    Exit Sub
End If

'If there were any fields to update, then complete the SQL statement and execute it
Set l_rstTemp = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
CheckForSQLError

If l_rstTemp.RecordCount > 0 Then
    l_rstTemp.MoveFirst
    Count = 0
    ProgressBar1.Max = l_rstTemp.RecordCount
    ProgressBar1.Value = 0
    ProgressBar1.Visible = True
    DoEvents
    
    Do While Not l_rstTemp.EOF
        ProgressBar1.Value = Count
        lblCurrentFilename.Caption = l_rstTemp("clipfilename")
        DoEvents
        l_strSQL = "UPDATE events SET " & l_strUpdateSet & " WHERE eventID = " & l_rstTemp("eventID") & ";"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        Count = Count + 1
        l_rstTemp.MoveNext
    Loop
    lblCurrentFilename.Caption = ""
    DoEvents
End If
l_rstTemp.Close
Set l_rstTemp = Nothing
ProgressBar1.Visible = False
adoClip.Refresh

BULK_UPDATE_ERROR:

Exit Sub

End Sub

Private Sub cmdKidscoBulkTranscode_Click()

If MsgBox("Are you sure?", vbYesNo, "Submitting the entire grid for Kidsco Proxy Files transcoding.") = vbNo Then Exit Sub

frmXMLTranscode.chkKidscoBulkTranscoding.Value = 1
frmXMLTranscode.lblCaption(1).Visible = False
frmXMLTranscode.lblCaption(3).Visible = False

frmXMLTranscode.Show vbModal

Unload frmXMLTranscode

End Sub

Private Sub cmdMakeBulkGoogleXMLs_Click()

adoClip.Recordset.MoveFirst
Do While Not adoClip.Recordset.EOF
    If Val(Trim(" " & adoClip.Recordset("companyID"))) <> 1489 Then
        MsgBox "To use this function, all the files in the grid must belong to Google.", vbCritical
        Exit Sub
    End If
    adoClip.Recordset.MoveNext
Loop

adoClip.Recordset.MoveFirst
Do While Not adoClip.Recordset.EOF
    LoadClipControl adoClip.Recordset("eventID")
    frmClipControl.cmdGoogleEpisodeXML.Value = True
    adoClip.Recordset.MoveNext
Loop
MsgBox "Done"
Unload frmClipControl

End Sub

Private Sub cmdMakeGenericTrackerLines_Click()

'Has to be some records int he grid
If adoClip.Recordset.RecordCount <= 0 Then Exit Sub

'Has to be a companyID
If Val(lblCompanyID.Caption) = 0 Then
    MsgBox "A Single client must be selected for Workflow Variants.", vbCritical
    Exit Sub
End If

'Has to be from either a DISCSTORE or the DIVA
If (optStorageType(0).Value = False And lblFormat.Caption <> "DISCSTORE") And (optStorageType(4).Value = False And lblFormat.Caption <> "DIVA") Then
    MsgBox "We can only do Workflows on items that are on a DISCSTORE or the DIVA at this time.", vbCritical
    Exit Sub
End If

If MsgBox("This action will create Generic Tracker lines for all the files in the grid, including the reference, fillename, and putting the clipID into {originaleventID} of the tracker." & vbCrLf & _
"It will NOT include a Workflow Variant ID." & vbCrLf & "Continue", vbYesNo + vbDefaultButton2, "Make Tracker Lines") = vbNo Then
    Exit Sub
End If

Dim l_strSQL As String, l_rstClips As ADODB.Recordset, Count As Long
Dim Duration_Timecode As String, Duration As Long

Set l_rstClips = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
CheckForSQLError
If l_rstClips.RecordCount > 0 Then
    l_rstClips.MoveFirst
    Count = 0
    ProgressBar1.Max = l_rstClips.RecordCount
    ProgressBar1.Value = 0
    ProgressBar1.Visible = True
    Do While Not l_rstClips.EOF
        'Update the progress label
        ProgressBar1.Value = Count
        lblCurrentFilename.Caption = l_rstClips("clipfilename")
        DoEvents
        
        Duration_Timecode = Trim(" " & l_rstClips("fd_length"))
        If Duration_Timecode <> "" Then
            Duration = 60 * Val(Left(Duration_Timecode, 2)) + Val(Mid(Duration_Timecode, 4, 2))
            If Val(Mid(Duration_Timecode, 7, 2)) > 0 Then Duration = Duration + 1
        Else
            Duration = 0
        End If
        l_strSQL = "INSERT INTO tracker_item (companyID, itemreference, itemfilename, originaleventID, timecodeduration, duration) VALUES ("
        l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstClips("clipreference")) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstClips("clipfilename")) & "', "
        l_strSQL = l_strSQL & l_rstClips("eventID") & ", "
        l_strSQL = l_strSQL & "'" & Duration_Timecode & "', "
        l_strSQL = l_strSQL & Duration & ");"
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        Count = Count + 1
        l_rstClips.MoveNext
        
    Loop
End If

l_rstClips.Close
Set l_rstClips = Nothing
ProgressBar1.Visible = False
lblCurrentFilename.Caption = ""
DoEvents

MsgBox "Done!"

adoClip.Refresh
adoClip.Caption = grdClips.Rows & " Clips Found"

Exit Sub

End Sub

Private Sub cmdMakeThumbnails_Click()

'declare what's needed
Dim r As ADODB.Recordset, rsKeyword As ADODB.Recordset
Dim s As String
Dim F As String
Dim Count As Long, l_blnSkip As Boolean
Dim l_lngOriginalImageHandle As Long, l_lngNewImageHandle As Long, l_lngCount As Long
Dim l_lngWidth As Long, l_lngHeight As Long, l_dblScaleWidth As Double, l_dblScaleHeight As Double

If adoClip.Recordset.RecordCount <= 0 Then Exit Sub

'build the SQL
s = "SELECT eventID, libraryID, subtitle, format, altlocation, clipfilename, clipreference, companyID "
s = s & " FROM vwEventsWithLibraryFormat WHERE 1 = 1 " & m_strSearchConditions
s = s & " AND (clipfilename <> 'ZZZZZZ' )"
s = s & " ORDER BY events.eventID DESC;"

'open the connection
Set r = ExecuteSQL(s, g_strExecuteError)
CheckForSQLError

If r.RecordCount > 0 Then
    fraYouTubeWarning.Top = frmDangerous.Top
    fraYouTubeWarning.Left = frmDangerous.Left
    fraYouTubeWarning.Visible = True
    
    r.MoveFirst
    Count = 0
    ProgressBar1.Max = r.RecordCount
    ProgressBar1.Value = 0
    ProgressBar1.Visible = True
    DoEvents
    
    'loop through all the records
    Do While Not r.EOF
        'build the full file name
        F = r("subtitle") & "\" & r("altlocation") & "\" & r("clipfilename")
        lblCurrentFilename.Caption = F
        l_blnSkip = False
        DoEvents
        
        'check if the file exists if it's not on an SMV discstore
        If r("format") = "DISCSTORE" And r("libraryID") <> 445810 Then
            If FileExists(F, r("clipfilename")) Then
                'Load the original image
                Select Case UCase(Right(r("clipfilename"), 4))
                
                    Case ".JPG"
                        l_lngOriginalImageHandle = FreeImage_Load(FIF_JPEG, F, 0)
                    
                    Case ".BMP"
                        l_lngOriginalImageHandle = FreeImage_Load(FIF_BMP, F, 0)
                    
                    Case ".GIF"
                        l_lngOriginalImageHandle = FreeImage_Load(FIF_GIF, F, 0)
                    
                    Case ".PNG"
                        l_lngOriginalImageHandle = FreeImage_Load(FIF_PNG, F, 0)
                    
                    Case ".TIF"
                        l_lngOriginalImageHandle = FreeImage_Load(FIF_TIFF, F, 0)
                    
                    Case ".TGA"
                        l_lngOriginalImageHandle = FreeImage_Load(FIF_TARGA, F, 0)
                    
                    Case Else
                        l_blnSkip = True
                        
                End Select
                    
                If Not l_blnSkip Then
                    ' Scale the original image
                    l_lngWidth = FreeImage_GetWidth(l_lngOriginalImageHandle)
                    l_lngHeight = FreeImage_GetHeight(l_lngOriginalImageHandle)
                    
                    l_dblScaleWidth = 320 / l_lngWidth
                    l_dblScaleHeight = 180 / l_lngHeight
                    
                    If l_dblScaleWidth < l_dblScaleHeight Then
                        l_lngNewImageHandle = FreeImage_RescaleByFactor(l_lngOriginalImageHandle, l_dblScaleWidth, l_dblScaleWidth, True)
                    Else
                        l_lngNewImageHandle = FreeImage_RescaleByFactor(l_lngOriginalImageHandle, l_dblScaleHeight, l_dblScaleHeight, True)
                    End If
                    
                    l_lngCount = InStr(r("clipfilename"), ".")
                    Do While InStr(l_lngCount + 1, r("clipfilename"), ".") > 0
                        l_lngCount = InStr(l_lngCount, r("clipfilename"), ".")
                    Loop
                    'MsgBox Count & " - " & txtClipfilename.Text & " - " & Left(txtClipfilename.Text, l_lngCount - 1)
                    ' Save this image as JPEG in the Grabs folder
                    If FreeImage_Save(FIF_JPEG, l_lngNewImageHandle, "\\jcaweb\Keyframes\" & r("companyID") & "\" & Left(r("clipfilename"), l_lngCount - 1) & ".jpg", JPEG_BASELINE) Then
                        SetData "events", "eventkeyframefilename", "eventID", r("eventID"), Left(r("clipfilename"), l_lngCount - 1) & ".jpg"
                    End If
                                        
                    ' Unload the new image
                    FreeImage_Unload (l_lngNewImageHandle)
                End If
                
            End If
        End If
        
        'move to the next record
        Count = Count + 1
        r.MoveNext
        ProgressBar1.Value = Count
        DoEvents
    
    Loop
End If

'close the recordset
r.Close
Set r = Nothing

'close the connection
lblCurrentFilename.Caption = ""
MsgBox "Finished.", vbInformation
ProgressBar1.Visible = False
fraYouTubeWarning.Visible = False

cmdSearch.Value = True

End Sub

Private Sub cmdMd5Grid_Click(Index As Integer)

Dim l_strSQL As String, l_blnFoldersFound As Boolean

If adoClip.Recordset.RecordCount > 0 Then
    
    If Index = 0 Then
        l_blnFoldersFound = False
        adoClip.Recordset.MoveFirst
        If adoClip.Recordset.RecordCount > 20 Then chkFutureRequest.Value = 1
        Do While Not adoClip.Recordset.EOF
            
            If Not IsNull(adoClip.Recordset("bigfilesize")) Then
                If adoClip.Recordset("clipformat") <> "Folder" Then
                    l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, RequestDate, bigfilesize, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                    l_strSQL = l_strSQL & adoClip.Recordset("libraryID") & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "MD5 Checksum") & ", "
                    l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
                    l_strSQL = l_strSQL & adoClip.Recordset("bigfilesize") & ", "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                Else
                    l_blnFoldersFound = True
                End If
            End If
            adoClip.Recordset.MoveNext
        Loop
        
    ElseIf Index = 1 Then
    
        If MsgBox("Are you sure", vbYesNo, "Deleting MD5 Checksums for all clips in Grid") = vbYes Then
            adoClip.Recordset.MoveFirst
            Do While Not adoClip.Recordset.EOF
                adoClip.Recordset("md5checksum") = ""
                adoClip.Recordset.MoveNext
            Loop
        End If
    End If
    
End If

If l_blnFoldersFound = True Then
    MsgBox "Done." & vbCrLf & "There were some items in the grid that were Folders." & vbCrLf & "MD5s cannot be made for folders, so those items were skipped"
Else
    MsgBox "Done"
End If

adoClip.Refresh

End Sub

Private Sub cmdMd5GridWhenNoMD5_Click()

Dim l_strSQL As String, l_blnFoldersFound As Boolean

If adoClip.Recordset.RecordCount > 0 Then
    
    l_blnFoldersFound = False
    adoClip.Recordset.MoveFirst
    If adoClip.Recordset.RecordCount > 20 Then chkFutureRequest.Value = 1
    Do While Not adoClip.Recordset.EOF
        
        If Not IsNull(adoClip.Recordset("bigfilesize")) And (IsNull(adoClip.Recordset("md5checksum")) Or adoClip.Recordset("md5checksum") = "") Then
            If adoClip.Recordset("clipformat") <> "Folder" Then
                l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, RequestDate, bigfilesize, RequestName, RequesterEmail) VALUES ("
                l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                l_strSQL = l_strSQL & adoClip.Recordset("libraryID") & ", "
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "MD5 Checksum") & ", "
                l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
                l_strSQL = l_strSQL & adoClip.Recordset("bigfilesize") & ", "
                l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            Else
                l_blnFoldersFound = True
            End If
        End If
        adoClip.Recordset.MoveNext
        DoEvents
    Loop
    
End If

If l_blnFoldersFound = True Then
    MsgBox "Done." & vbCrLf & "There were some items in the grid that were Folders." & vbCrLf & "MD5s cannot be made for folders, so those items were skipped"
Else
    MsgBox "Done"
End If

adoClip.Refresh
End Sub

Private Sub cmdMediaInfo_Click()

'declare what's needed
Dim c As ADODB.Connection, c2 As ADODB.Connection
Dim r As ADODB.Recordset
Dim s As String
Dim F As String
Dim FileNumber As Long, FileSize As Currency
Dim Count As Long
Dim MediaData As MediaInfoData
Dim l_ImageDimensions As ImgDimType, temp As String
Dim l_strRootPath As String
Dim FSO As Scripting.FileSystemObject

Set FSO = New Scripting.FileSystemObject
If adoClip.Recordset.RecordCount <= 0 Then Exit Sub

'build the SQL
s = "SELECT eventID, libraryID, altlocation, clipfilename "
s = s & " FROM vwEventsWithLibraryFormat WHERE 1 = 1 " & m_strSearchConditions
s = s & " AND (format = 'DISCSTORE') "
s = s & " AND (clipfilename <> 'ZZZZZZ' )"
s = s & " ORDER BY eventID DESC;"

'open the connection
Set c = New ADODB.Connection
c.Open g_strConnection
Set c2 = New ADODB.Connection
c2.Open g_strConnection
Set r = New ADODB.Recordset
r.Open s, c, 3, 3

If r.RecordCount > 0 Then
    FraMediaInfoWarning.Top = 840
    FraMediaInfoWarning.Left = 8640
    FraMediaInfoWarning.Visible = True
    
    r.MoveFirst
    Count = 1
    ProgressBar1.Max = r.RecordCount
    ProgressBar1.Value = 0
    ProgressBar1.Visible = True
    DoEvents
    
    'loop through all the records
    Do While Not r.EOF
        ProgressBar1.Value = Count
        'build the full file name
        l_strRootPath = GetData("library", "subtitle", "libraryID", Val(r("libraryid")))
        F = l_strRootPath & "\" & r("altlocation") & "\" & r("clipfilename")
        lblCurrentFilename.Caption = F
        DoEvents
        
        'check if the file exists if it's not on an SMV discstore
        If r("libraryID") <> 445810 Then
            temp = Replace(F, "\\", "\\?\UNC\")
            If FSO.FileExists(temp) Then
            
                'if it does, then set the sound lay to be the date/time
                API_OpenFile temp, FileNumber, FileSize
                API_CloseFile FileNumber
                c2.Execute "UPDATE events SET soundlay = '" & Month(Now) & "/" & Day(Now) & "/" & Year(Now) & " " & Hour(Now) & ":" & Minute(Now) & ":" & Second(Now) & "' WHERE eventID = " & r("eventID") & ";"
                c2.Execute "UPDATE events SET lastmediainfoquery = '" & Month(Now) & "/" & Day(Now) & "/" & Year(Now) & " " & Hour(Now) & ":" & Minute(Now) & ":" & Second(Now) & "' WHERE eventID = " & r("eventID") & ";"
                c2.Execute "UPDATE events SET bigfilesize = " & FileSize & " WHERE eventID = '" & r("eventID") & "';"
                MediaData = GetMediaInfoOnFile(F)
                With MediaData
                    If .txtFormat = "TTML" Then
                        c2.Execute "UPDATE events SET clipformat = 'ITT Subtitles' WHERE eventID = '" & r("eventID") & "';"
'                    ElseIf .txtFormat = "JPEG Image" Or .txtFormat = "PNG Image" Or .txtFormat = "BMP Image" Or .txtFormat = "GIF Image" Then
'                        c2.Execute "UPDATE events SET clipformat = '" & .txtFormat & "' WHERE eventID = '" & r("eventID") & "';"
'                        If getImgDim(F, l_ImageDimensions, temp) = True Then
'                            c2.Execute "UPDATE events SET clipverticalpixels = " & l_ImageDimensions.Height & " WHERE eventID = " & r("eventID") & ";"
'                            c2.Execute "UPDATE events SET cliphorizontalpixels = " & l_ImageDimensions.Width & " WHERE eventID = " & r("eventID") & ";"
'                        End If
                    ElseIf .txtFormat <> "" Then
                        c2.Execute "UPDATE events SET clipformat = '" & .txtFormat & "' WHERE eventID = '" & r("eventID") & "';"
                        c2.Execute "UPDATE events SET clipaudiocodec = '" & .txtAudioCodec & "' WHERE eventID = " & r("eventID") & ";"
                        c2.Execute "UPDATE events SET clipframeRate = '" & .txtFrameRate & "' WHERE eventID = " & r("eventID") & ";"
                        c2.Execute "UPDATE events SET cbrvbr = '" & .txtCBRVBR & "' WHERE eventID = " & r("eventID") & ";"
                        If .txtWidth <> "" Then c2.Execute "UPDATE events SET cliphorizontalpixels = " & Val(.txtWidth) & " WHERE eventID = " & r("eventID") & ";"
                        If .txtHeight <> "" Then c2.Execute "UPDATE events SET clipverticalpixels = " & Val(.txtHeight) & " WHERE eventID = " & r("eventID") & ";"
                        If .txtVideoBitrate <> "" Then c2.Execute "UPDATE events SET videobitrate = " & .txtVideoBitrate & " WHERE eventID = " & r("eventID") & ";"
                        c2.Execute "UPDATE events SET interlace = '" & .txtInterlace & "' WHERE eventID = " & r("eventID") & ";"
                        c2.Execute "UPDATE events SET timecodestart = '" & .txtTimecodeStart & "' WHERE eventID = " & r("eventID") & ";"
                        c2.Execute "UPDATE events SET timecodestop = '" & .txtTimecodeStop & "' WHERE eventID = " & r("eventID") & ";"
                        c2.Execute "UPDATE events SET fd_length = '" & .txtDuration & "' WHERE eventID = " & r("eventID") & ";"
                        If .txtAudioBitrate <> "" Then c2.Execute "UPDATE events SET audiobitrate = " & .txtAudioBitrate & " WHERE eventID = " & r("eventID") & ";"
                        If .txtTrackCount <> "" Then c2.Execute "UPDATE events SET trackcount = " & .txtTrackCount & " WHERE eventID = " & r("eventID") & ";"
                        If .txtChannelCount <> "" Then c2.Execute "UPDATE events SET channelcount = " & .txtChannelCount & " WHERE eventID = " & r("eventID") & ";"
                        If Val(.txtVideoBitrate) + Val(.txtAudioBitrate) <> 0 Then
                            c2.Execute "UPDATE events SET clipbitrate = " & Val(.txtVideoBitrate) + Val(.txtAudioBitrate) & " WHERE eventID = " & r("eventID") & ";"
                        Else
                            c2.Execute "UPDATE events SET clipbitrate = " & Int(Val(.txtOverallBitrate) / 1000) & " WHERE eventID = " & r("eventID") & ";"
                        End If
                        c2.Execute "UPDATE events SET aspectratio = '" & .txtAspect & "' WHERE eventID = " & r("eventID") & ";"
                        If .txtAspect = "4:3" Then
                            c2.Execute "UPDATE events SET fourbythreeflag = 1 WHERE eventID = " & r("eventID") & ";"
                        Else
                            c2.Execute "UPDATE events SET fourbythreeflag = 0 WHERE eventID = " & r("eventID") & ";"
                        End If
                        c2.Execute "UPDATE events SET geometriclinearity = '" & .txtGeometry & "' WHERE eventID = " & r("eventID") & ";"
                        c2.Execute "UPDATE events SET clipcodec = '" & .txtVideoCodec & "' WHERE eventID = " & r("eventID") & ";"
                        If Trim(" " & GetData("events", "eventtype", "eventID", r("eventID"))) = "" Then c2.Execute "UPDATE events SET eventtype = '" & GetData("xref", "format", "description", .txtVideoCodec) & "' WHERE eventID = " & r("eventID") & ";"
                        c2.Execute "UPDATE events SET mediastreamtype = '" & .txtStreamType & "' WHERE eventID = " & r("eventID") & ";"
                        If .txtSeriesTitle <> "" Then
                            c2.Execute "UPDATE events SET eventtitle = '" & .txtSeriesTitle & "' WHERE eventID = " & r("eventID") & ";"
                            c2.Execute "UPDATE events SET eventseries = '" & .txtSeriesNumber & "' WHERE eventID = " & r("eventID") & ";"
                            c2.Execute "UPDATE events SET eventsubtitle = '" & .txtProgrammeTitle & "' WHERE eventID = " & r("eventID") & ";"
                            c2.Execute "UPDATE events SET eventepisode = '" & .txtEpisodeNumber & "' WHERE eventID = " & r("eventID") & ";"
                            c2.Execute "UPDATE events SET notes = '" & .txtSynopsis & "' WHERE eventID = " & r("eventID") & ";"
                            Select Case .txtAudioLayout
                                Case "EBU R 48: 2a"
                                    c2.Execute "UPDATE events SET AudioTypeGroup1 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup1 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup1 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup2 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup2 = 'MOS' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup2 = 'None' WHERE eventID = " & r("eventID") & ";"
                                Case "EBU R 123: 4b"
                                    c2.Execute "UPDATE events SET AudioTypeGroup1 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup1 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup1 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup2 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup2 = 'M&E' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup2 = 'None' WHERE eventID = " & r("eventID") & ";"
                                Case "EBU R 123: 4c"
                                    c2.Execute "UPDATE events SET AudioTypeGroup1 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup1 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup1 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup2 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup2 = 'Audio Description' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup2 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                Case "EBU R 123: 16c (5.1 with M&E)"
                                    c2.Execute "UPDATE events SET AudioTypeGroup1 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup1 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup1 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup2 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup2 = 'M&E' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup2 = 'None' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup3 = '5.1' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup3 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup3 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup4 = '5.1' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup4 = 'M&E' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup4 = 'None' WHERE eventID = " & r("eventID") & ";"
                                Case "EBU R 123: 16c (5.1 with AD)"
                                    c2.Execute "UPDATE events SET AudioTypeGroup1 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup1 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup1 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup2 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup2 = 'Audio Description' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup2 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup3 = '5.1' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup3 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup3 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup4 = '5.1' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup4 = 'M&E' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup4 = 'None' WHERE eventID = " & r("eventID") & ";"
                                Case "EBU R 123: 16d"
                                    c2.Execute "UPDATE events SET AudioTypeGroup1 = '5.1' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup1 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup1 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup2 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup2 = 'MOS' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup2 = 'None' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup3 = '5.1' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup3 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup3 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtSecondaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup4 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup4 = 'MOS' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup4 = 'None' WHERE eventID = " & r("eventID") & ";"
                                Case "EBU R 123: 16f"
                                    c2.Execute "UPDATE events SET AudioTypeGroup1 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup1 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup1 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup2 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup2 = 'MOS' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup2 = 'None' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup3 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup3 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup3 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtSecondaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup4 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup4 = 'MOS' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup4 = 'None' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup5 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup5 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup5 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtTertiaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup6 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup6 = 'MOS' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup6 = 'None' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup7 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup7 = 'MOS' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup7 = 'None' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup8 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup8 = 'MOS' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup8 = 'None' WHERE eventID = " & r("eventID") & ";"
                            End Select
                        End If
                        c2.Execute "UPDATE events SET lastmediainfoquery = '" & Month(Now) & "/" & Day(Now) & "/" & Year(Now) & " " & Hour(Now) & ":" & Minute(Now) & ":" & Second(Now) & "' WHERE eventID = " & r("eventID") & ";"
                    Else
                        Select Case UCase(Right(F, 3))
                            Case "ISO"
                                c2.Execute "UPDATE events SET clipformat = 'DVD ISO Image' WHERE eventID = '" & r("eventID") & "';"
                            Case "PDF"
                                c2.Execute "UPDATE events SET clipformat = 'PDF File' WHERE eventID = '" & r("eventID") & "';"
                            Case "PSD"
                                c2.Execute "UPDATE events SET clipformat = 'PSD Still Image' WHERE eventID = '" & r("eventID") & "';"
                            Case "SCC"
                                c2.Execute "UPDATE events SET clipformat = 'SCC File (EIA 608)' WHERE eventID = '" & r("eventID") & "';"
                            Case "STL"
                                c2.Execute "UPDATE events SET clipformat = 'STL Subtitles' WHERE eventID = '" & r("eventID") & "';"
                            Case "TIF"
                                c2.Execute "UPDATE events SET clipformat = 'TIF Still Image' WHERE eventID = '" & r("eventID") & "';"
                            Case "DOC", "DOCX"
                                c2.Execute "UPDATE events SET clipformat = 'Word File' WHERE eventID = '" & r("eventID") & "';"
                            Case "XLS", "XLSX"
                                c2.Execute "UPDATE events SET clipformat = 'XLS File' WHERE eventID = '" & r("eventID") & "';"
                            Case "XML"
                                c2.Execute "UPDATE events SET clipformat = 'XML File' WHERE eventID = '" & r("eventID") & "';"
                            Case "RAR"
                                c2.Execute "UPDATE events SET clipformat = 'RAR Archive' WHERE eventID = '" & r("eventID") & "';"
                            Case "ZIP"
                                c2.Execute "UPDATE events SET clipformat = 'ZIP Archive' WHERE eventID = '" & r("eventID") & "';"
                            Case Else
                                c2.Execute "UPDATE events SET clipformat = 'Other' WHERE eventID = '" & r("eventID") & "';"
                        End Select
                    End If
                End With
                
                DoEvents
            Else
                c2.Execute "UPDATE events SET soundlay = NULL, bigfilesize = NULL, lastmediainfoquery = NULL WHERE eventID = " & r("eventID") & ";"
                DoEvents
            End If
        End If
        
        'move to the next record
        Count = Count + 1
        r.MoveNext
    
    Loop
End If

'close the recordset
r.Close
Set r = Nothing

'close the connection
c.Close
Set c = Nothing
c2.Close
Set c2 = Nothing
lblCurrentFilename.Caption = ""
MsgBox "Finished.", vbInformation
ProgressBar1.Visible = False
FraMediaInfoWarning.Visible = False

cmdSearch.Value = True

End Sub

Private Sub cmdMediaInfoPreserveTimecode_Click()

'declare what's needed
Dim c As ADODB.Connection, c2 As ADODB.Connection
Dim r As ADODB.Recordset
Dim s As String
Dim F As String
Dim FileNumber As Long, FileSize As Currency
Dim Count As Long
Dim MediaData As MediaInfoData
Dim l_ImageDimensions As ImgDimType, temp As String
Dim l_strRootPath As String
Dim FSO As Scripting.FileSystemObject

Set FSO = New Scripting.FileSystemObject
If adoClip.Recordset.RecordCount <= 0 Then Exit Sub

'build the SQL
s = "SELECT eventID, libraryID, altlocation, clipfilename "
s = s & " FROM vwEventsWithLibraryFormat WHERE 1 = 1 " & m_strSearchConditions
s = s & " AND (format = 'DISCSTORE') "
s = s & " AND (clipfilename <> 'ZZZZZZ' )"
s = s & " ORDER BY eventID DESC;"

'open the connection
Set c = New ADODB.Connection
c.Open g_strConnection
Set c2 = New ADODB.Connection
c2.Open g_strConnection
Set r = New ADODB.Recordset
r.Open s, c, 3, 3

If r.RecordCount > 0 Then
    FraMediaInfoWarning.Top = 840
    FraMediaInfoWarning.Left = 8640
    FraMediaInfoWarning.Visible = True
    
    r.MoveFirst
    Count = 1
    ProgressBar1.Max = r.RecordCount
    ProgressBar1.Value = 0
    ProgressBar1.Visible = True
    DoEvents
    
    'loop through all the records
    Do While Not r.EOF
        ProgressBar1.Value = Count
        'build the full file name
        l_strRootPath = GetData("library", "subtitle", "libraryID", Val(r("libraryid")))
        F = l_strRootPath & "\" & r("altlocation") & "\" & r("clipfilename")
        lblCurrentFilename.Caption = F
        DoEvents
        
        'check if the file exists if it's not on an SMV discstore
        If r("libraryID") <> 445810 Then
            temp = Replace(F, "\\", "\\?\UNC\")
            If FSO.FileExists(temp) Then
            
                'if it does, then set the sound lay to be the date/time
                API_OpenFile temp, FileNumber, FileSize
                API_CloseFile FileNumber
                c2.Execute "UPDATE events SET soundlay = '" & Month(Now) & "/" & Day(Now) & "/" & Year(Now) & " " & Hour(Now) & ":" & Minute(Now) & ":" & Second(Now) & "' WHERE eventID = " & r("eventID") & ";"
                c2.Execute "UPDATE events SET lastmediainfoquery = '" & Month(Now) & "/" & Day(Now) & "/" & Year(Now) & " " & Hour(Now) & ":" & Minute(Now) & ":" & Second(Now) & "' WHERE eventID = " & r("eventID") & ";"
                c2.Execute "UPDATE events SET bigfilesize = " & FileSize & " WHERE eventID = '" & r("eventID") & "';"
                MediaData = GetMediaInfoOnFile(F)
                With MediaData
                    If .txtFormat = "TTML" Then
                        c2.Execute "UPDATE events SET clipformat = 'ITT Subtitles' WHERE eventID = '" & r("eventID") & "';"
'                    ElseIf .txtFormat = "JPEG Image" Or .txtFormat = "PNG Image" Or .txtFormat = "BMP Image" Or .txtFormat = "GIF Image" Then
'                        c2.Execute "UPDATE events SET clipformat = '" & .txtFormat & "' WHERE eventID = '" & r("eventID") & "';"
'                        If getImgDim(F, l_ImageDimensions, temp) = True Then
'                            c2.Execute "UPDATE events SET clipverticalpixels = " & l_ImageDimensions.Height & " WHERE eventID = " & r("eventID") & ";"
'                            c2.Execute "UPDATE events SET cliphorizontalpixels = " & l_ImageDimensions.Width & " WHERE eventID = " & r("eventID") & ";"
'                        End If
                    ElseIf .txtFormat <> "" Then
                        c2.Execute "UPDATE events SET clipformat = '" & .txtFormat & "' WHERE eventID = '" & r("eventID") & "';"
                        c2.Execute "UPDATE events SET clipaudiocodec = '" & .txtAudioCodec & "' WHERE eventID = " & r("eventID") & ";"
                        c2.Execute "UPDATE events SET clipframeRate = '" & .txtFrameRate & "' WHERE eventID = " & r("eventID") & ";"
                        c2.Execute "UPDATE events SET cbrvbr = '" & .txtCBRVBR & "' WHERE eventID = " & r("eventID") & ";"
                        If .txtWidth <> "" Then c2.Execute "UPDATE events SET cliphorizontalpixels = " & Val(.txtWidth) & " WHERE eventID = " & r("eventID") & ";"
                        If .txtHeight <> "" Then c2.Execute "UPDATE events SET clipverticalpixels = " & Val(.txtHeight) & " WHERE eventID = " & r("eventID") & ";"
                        If .txtVideoBitrate <> "" Then c2.Execute "UPDATE events SET videobitrate = " & .txtVideoBitrate & " WHERE eventID = " & r("eventID") & ";"
                        c2.Execute "UPDATE events SET interlace = '" & .txtInterlace & "' WHERE eventID = " & r("eventID") & ";"
                        If .txtAudioBitrate <> "" Then c2.Execute "UPDATE events SET audiobitrate = " & .txtAudioBitrate & " WHERE eventID = " & r("eventID") & ";"
                        If .txtTrackCount <> "" Then c2.Execute "UPDATE events SET trackcount = " & .txtTrackCount & " WHERE eventID = " & r("eventID") & ";"
                        If .txtChannelCount <> "" Then c2.Execute "UPDATE events SET channelcount = " & .txtChannelCount & " WHERE eventID = " & r("eventID") & ";"
                        If Val(.txtVideoBitrate) + Val(.txtAudioBitrate) <> 0 Then
                            c2.Execute "UPDATE events SET clipbitrate = " & Val(.txtVideoBitrate) + Val(.txtAudioBitrate) & " WHERE eventID = " & r("eventID") & ";"
                        Else
                            c2.Execute "UPDATE events SET clipbitrate = " & Int(Val(.txtOverallBitrate) / 1000) & " WHERE eventID = " & r("eventID") & ";"
                        End If
                        c2.Execute "UPDATE events SET aspectratio = '" & .txtAspect & "' WHERE eventID = " & r("eventID") & ";"
                        If .txtAspect = "4:3" Then
                            c2.Execute "UPDATE events SET fourbythreeflag = 1 WHERE eventID = " & r("eventID") & ";"
                        Else
                            c2.Execute "UPDATE events SET fourbythreeflag = 0 WHERE eventID = " & r("eventID") & ";"
                        End If
                        c2.Execute "UPDATE events SET geometriclinearity = '" & .txtGeometry & "' WHERE eventID = " & r("eventID") & ";"
                        c2.Execute "UPDATE events SET clipcodec = '" & .txtVideoCodec & "' WHERE eventID = " & r("eventID") & ";"
                        If Trim(" " & GetData("events", "eventtype", "eventID", r("eventID"))) = "" Then c2.Execute "UPDATE events SET eventtype = '" & GetData("xref", "format", "description", .txtVideoCodec) & "' WHERE eventID = " & r("eventID") & ";"
                        c2.Execute "UPDATE events SET mediastreamtype = '" & .txtStreamType & "' WHERE eventID = " & r("eventID") & ";"
                        If .txtSeriesTitle <> "" Then
                            c2.Execute "UPDATE events SET eventtitle = '" & .txtSeriesTitle & "' WHERE eventID = " & r("eventID") & ";"
                            c2.Execute "UPDATE events SET eventseries = '" & .txtSeriesNumber & "' WHERE eventID = " & r("eventID") & ";"
                            c2.Execute "UPDATE events SET eventsubtitle = '" & .txtProgrammeTitle & "' WHERE eventID = " & r("eventID") & ";"
                            c2.Execute "UPDATE events SET eventepisode = '" & .txtEpisodeNumber & "' WHERE eventID = " & r("eventID") & ";"
                            c2.Execute "UPDATE events SET notes = '" & .txtSynopsis & "' WHERE eventID = " & r("eventID") & ";"
                            Select Case .txtAudioLayout
                                Case "EBU R 48: 2a"
                                    c2.Execute "UPDATE events SET AudioTypeGroup1 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup1 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup1 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup2 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup2 = 'MOS' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup2 = 'None' WHERE eventID = " & r("eventID") & ";"
                                Case "EBU R 123: 4b"
                                    c2.Execute "UPDATE events SET AudioTypeGroup1 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup1 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup1 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup2 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup2 = 'M&E' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup2 = 'None' WHERE eventID = " & r("eventID") & ";"
                                Case "EBU R 123: 4c"
                                    c2.Execute "UPDATE events SET AudioTypeGroup1 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup1 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup1 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup2 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup2 = 'Audio Description' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup2 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                Case "EBU R 123: 16c (5.1 with M&E)"
                                    c2.Execute "UPDATE events SET AudioTypeGroup1 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup1 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup1 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup2 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup2 = 'M&E' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup2 = 'None' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup3 = '5.1' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup3 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup3 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup4 = '5.1' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup4 = 'M&E' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup4 = 'None' WHERE eventID = " & r("eventID") & ";"
                                Case "EBU R 123: 16c (5.1 with AD)"
                                    c2.Execute "UPDATE events SET AudioTypeGroup1 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup1 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup1 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup2 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup2 = 'Audio Description' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup2 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup3 = '5.1' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup3 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup3 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup4 = '5.1' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup4 = 'M&E' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup4 = 'None' WHERE eventID = " & r("eventID") & ";"
                                Case "EBU R 123: 16d"
                                    c2.Execute "UPDATE events SET AudioTypeGroup1 = '5.1' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup1 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup1 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup2 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup2 = 'MOS' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup2 = 'None' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup3 = '5.1' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup3 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup3 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtSecondaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup4 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup4 = 'MOS' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup4 = 'None' WHERE eventID = " & r("eventID") & ";"
                                Case "EBU R 123: 16f"
                                    c2.Execute "UPDATE events SET AudioTypeGroup1 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup1 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup1 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup2 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup2 = 'MOS' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup2 = 'None' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup3 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup3 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup3 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtSecondaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup4 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup4 = 'MOS' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup4 = 'None' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup5 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup5 = 'Composite' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup5 = '" & GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtTertiaryAudioLanguage) & "' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup6 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup6 = 'MOS' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup6 = 'None' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup7 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup7 = 'MOS' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup7 = 'None' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioTypeGroup8 = 'Standard Stereo' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioContentGroup8 = 'MOS' WHERE eventID = " & r("eventID") & ";"
                                    c2.Execute "UPDATE events SET AudioLanguageGroup8 = 'None' WHERE eventID = " & r("eventID") & ";"
                            End Select
                        End If
                        c2.Execute "UPDATE events SET lastmediainfoquery = '" & Month(Now) & "/" & Day(Now) & "/" & Year(Now) & " " & Hour(Now) & ":" & Minute(Now) & ":" & Second(Now) & "' WHERE eventID = " & r("eventID") & ";"
                    Else
                        Select Case UCase(Right(F, 3))
                            Case "ISO"
                                c2.Execute "UPDATE events SET clipformat = 'DVD ISO Image' WHERE eventID = '" & r("eventID") & "';"
                            Case "PDF"
                                c2.Execute "UPDATE events SET clipformat = 'PDF File' WHERE eventID = '" & r("eventID") & "';"
                            Case "PSD"
                                c2.Execute "UPDATE events SET clipformat = 'PSD Still Image' WHERE eventID = '" & r("eventID") & "';"
                            Case "SCC"
                                c2.Execute "UPDATE events SET clipformat = 'SCC File (EIA 608)' WHERE eventID = '" & r("eventID") & "';"
                            Case "STL"
                                c2.Execute "UPDATE events SET clipformat = 'STL Subtitles' WHERE eventID = '" & r("eventID") & "';"
                            Case "TIF"
                                c2.Execute "UPDATE events SET clipformat = 'TIF Still Image' WHERE eventID = '" & r("eventID") & "';"
                            Case "DOC", "DOCX"
                                c2.Execute "UPDATE events SET clipformat = 'Word File' WHERE eventID = '" & r("eventID") & "';"
                            Case "XLS", "XLSX"
                                c2.Execute "UPDATE events SET clipformat = 'XLS File' WHERE eventID = '" & r("eventID") & "';"
                            Case "XML"
                                c2.Execute "UPDATE events SET clipformat = 'XML File' WHERE eventID = '" & r("eventID") & "';"
                            Case "RAR"
                                c2.Execute "UPDATE events SET clipformat = 'RAR Archive' WHERE eventID = '" & r("eventID") & "';"
                            Case "ZIP"
                                c2.Execute "UPDATE events SET clipformat = 'ZIP Archive' WHERE eventID = '" & r("eventID") & "';"
                            Case Else
                                c2.Execute "UPDATE events SET clipformat = 'Other' WHERE eventID = '" & r("eventID") & "';"
                        End Select
                    End If
                End With
                
                DoEvents
            Else
                c2.Execute "UPDATE events SET soundlay = NULL, bigfilesize = NULL, lastmediainfoquery = NULL WHERE eventID = " & r("eventID") & ";"
                DoEvents
            End If
        End If
        
        'move to the next record
        Count = Count + 1
        r.MoveNext
    
    Loop
End If

'close the recordset
r.Close
Set r = Nothing

'close the connection
c.Close
Set c = Nothing
c2.Close
Set c2 = Nothing
lblCurrentFilename.Caption = ""
MsgBox "Finished.", vbInformation
ProgressBar1.Visible = False
FraMediaInfoWarning.Visible = False

cmdSearch.Value = True

End Sub

Private Sub cmdMoveAndChangeOwner_Click()

Dim l_lngNewCompanyID As Long, l_lngDestinationLibraryID As Long
Dim l_strNewLibraryBarcode As String, l_lngNewLibraryID As Long, l_strNewFolder As String, l_strSQL As String, l_rstTemp As ADODB.Recordset, Sub_Path As String, Rebuilt_Path As String
Dim Count As Long, FSO As Scripting.FileSystemObject, l_blnActuallyMove As Boolean, l_blnDuplicatesFound As Boolean
Dim PreserveFolderStructure As Boolean, TempStr As String

If chkRecordUpdateOnly.Value <> 0 Then
    MsgBox "Copy Grid and Change Owner can only be used to actually copy files."
    Exit Sub
End If

If optStorageType(0).Value = False And chkRecordUpdateOnly.Value = 0 Then
    MsgBox "You haven't selected DISCSTORES for this search." & vbCrLf & "You must select DISCSTORES to perform actual File Requests.", vbCritical
    optStorageType(0).Value = True
    cmdSearch.Value = True
End If

frmGetNewFileDetails.Caption = "Please give the Barcode to move to..."
frmGetNewFileDetails.chkPreserveFolderStructure.Visible = True
frmGetNewFileDetails.Show vbModal

If Left(l_strNewLibraryBarcode, 3) = "BFS" Then
    If Not CheckAccess("/MoveToBFS") Then
        MsgBox "You do not have permission to move or copy to BFS", vbCritical
        Exit Sub
    End If
End If

l_strNewLibraryBarcode = UCase(frmGetNewFileDetails.txtBarcode.Text)
l_strNewFolder = frmGetNewFileDetails.txtAltLocation.Text
PreserveFolderStructure = frmGetNewFileDetails.chkPreserveFolderStructure.Value
Unload frmGetNewFileDetails

l_lngDestinationLibraryID = Val(GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode))
If CheckDriveWritePermissions(l_lngDestinationLibraryID) = False Then
    MsgBox "You do not have permissinos to send file to that DISCSTORE", vbCritical
    Exit Sub
End If

If l_strNewLibraryBarcode <> "" Then

    l_lngNewLibraryID = GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode)
    If l_lngNewLibraryID <> 0 Then
        
        Set l_rstTemp = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
        CheckForSQLError
        If l_rstTemp.RecordCount > 0 Then
            frmGetNewCompany.Show vbModal
            If frmGetNewCompany.lblCompanyID.Caption <> "" Then
                l_lngNewCompanyID = Val(frmGetNewCompany.lblCompanyID.Caption)
            Else
                MsgBox "No new Owner - Selected - Operation Aborted"
                l_rstTemp.Close
                Set l_rstTemp = Nothing
                Exit Sub
            End If
            Unload frmGetNewCompany
            
            l_rstTemp.MoveFirst
            Count = 0
            ProgressBar1.Max = l_rstTemp.RecordCount
            ProgressBar1.Value = 0
            ProgressBar1.Visible = True
            Do While Not l_rstTemp.EOF
                ProgressBar1.Value = Count
                DoEvents
                l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & l_lngNewLibraryID
                If l_strNewFolder <> "" Then
                    l_strSQL = l_strSQL & " AND altlocation = '" & l_strNewFolder & "' "
                Else
                    l_strSQL = l_strSQL & " AND altlocation = '" & l_rstTemp("altlocation") & "' "
                End If
                l_strSQL = l_strSQL & " AND clipfilename = '" & l_rstTemp("clipfilename") & "'"
                l_strSQL = l_strSQL & " AND system_deleted = 0"
                If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
                    l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, NewCompanyID, RequestDate, bigfilesize, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                    l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
                    l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
                    If PreserveFolderStructure = True Then
                        If Val(l_rstTemp("altlocation")) = l_rstTemp("companyID") Then
                            TempStr = Mid(l_rstTemp("altlocation"), Len(Trim(" " & l_rstTemp("companyID"))) + 2)
                        Else
                            TempStr = l_rstTemp("altlocation")
                        End If
                        If l_strNewFolder <> "" Then
                            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder & IIf(TempStr <> "", "\" & TempStr, "")) & "', "
                        Else
                            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstTemp("altlocation")) & "', "
                        End If
                    Else
                        If l_strNewFolder <> "" Then
                            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & "', "
                        Else
                            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstTemp("altlocation")) & "', "
                        End If
                    End If
                    If l_lngNewCompanyID <> 0 Then
                        l_strSQL = l_strSQL & l_lngNewCompanyID & ", "
                    Else
                        l_strSQL = l_strSQL & "Null, "
                    End If
                    l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
                    l_strSQL = l_strSQL & l_rstTemp("bigfilesize") & ", "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                Else
                    l_blnDuplicatesFound = True
                End If
                Count = Count + 1
                l_rstTemp.MoveNext
            Loop
        End If
        l_rstTemp.Close
        Set l_rstTemp = Nothing
        If l_blnDuplicatesFound = True Then
            MsgBox "Some of the files already existed in the destination folder." & vbCrLf & "File requests were not made for those files", vbCritical
        End If
    Else
        MsgBox "That Store does not yet exist!", vbOKOnly, "Problem..."
    End If

End If

ProgressBar1.Visible = False

adoClip.Refresh
adoClip.Caption = grdClips.Rows & " Clips Found"

Exit Sub

End Sub

Private Sub cmdMoveGrid_Click()

Dim l_strNewLibraryBarcode As String, l_lngNewLibraryID As Long, l_strNewFolder As String, l_strSQL As String, l_rstTemp As ADODB.Recordset, Sub_Path As String, Rebuilt_Path As String
Dim Count As Long, FSO As Scripting.FileSystemObject, l_blnActuallyMove As Boolean, l_blnDuplicatesFound As Boolean, l_lngDestinationLibraryID As Long, l_blnUnVerifiedFound As Boolean
Dim PreserveFolderStructure As Boolean, TempStr As String, ForgetAfterMove As Boolean

If chkRecordUpdateOnly.Value <> 0 Then
    If MsgBox("Are you sure you wish to only Move File Records, rather than actually Moving Files?", vbYesNo, "Move Check") = vbNo Then
        Exit Sub
    End If
End If

If optStorageType(0).Value = False And chkRecordUpdateOnly.Value = 0 Then
    MsgBox "You haven't selected DISCSTORES for this search." & vbCrLf & "You must select DISCSTORES to perform actual File Requests.", vbCritical
    optStorageType(0).Value = True
    cmdSearch.Value = True
    Exit Sub
End If

frmGetNewFileDetails.Caption = "Please give the Barcode to move to..."
frmGetNewFileDetails.chkPreserveFolderStructure.Visible = True
frmGetNewFileDetails.Show vbModal

If Left(l_strNewLibraryBarcode, 3) = "BFS" Then
    If Not CheckAccess("/MoveToBFS") Then
        MsgBox "You do not have permission to move or copy to BFS", vbCritical
        Exit Sub
    End If
End If

l_strNewLibraryBarcode = UCase(frmGetNewFileDetails.txtBarcode.Text)
l_strNewFolder = frmGetNewFileDetails.txtAltLocation.Text
PreserveFolderStructure = frmGetNewFileDetails.chkPreserveFolderStructure.Value
Unload frmGetNewFileDetails
l_blnActuallyMove = False

l_lngDestinationLibraryID = Val(GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode))
If CheckDriveWritePermissions(l_lngDestinationLibraryID) = False Then
    MsgBox "You do not have permissinos to send file to that DISCSTORE", vbCritical
    Exit Sub
End If

ForgetAfterMove = False

If l_strNewLibraryBarcode <> "" Then

    
    If CheckAccess("Superuser", True) Then
        If chkMoveThenForget.Value <> 0 Then
            ForgetAfterMove = True
        End If
    End If
    
    l_lngNewLibraryID = GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode)
    If l_lngNewLibraryID <> 0 Then
        
        If GetData("library", "format", "libraryID", l_lngNewLibraryID) = "DISCSTORE" And chkRecordUpdateOnly.Value = 0 Then
            If MsgBox("Do you wish to actually move the files, as well as change the CETA records", vbYesNo, "Question...") = vbYes Then l_blnActuallyMove = True Else l_blnActuallyMove = False
        End If
        
        Set l_rstTemp = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
        CheckForSQLError
        l_rstTemp.MoveFirst
        Count = 0
        ProgressBar1.Max = l_rstTemp.RecordCount
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        If l_blnActuallyMove = True Then Set FSO = New Scripting.FileSystemObject
        Do While Not l_rstTemp.EOF
            ProgressBar1.Value = Count
            DoEvents
            If l_blnActuallyMove = True Then
                l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & l_lngNewLibraryID
                If l_strNewFolder <> "" Then
                    l_strSQL = l_strSQL & " AND altlocation = '" & l_strNewFolder & "' "
                Else
                    l_strSQL = l_strSQL & " AND altlocation = '" & l_rstTemp("altlocation") & "' "
                End If
                l_strSQL = l_strSQL & " AND clipfilename = '" & l_rstTemp("clipfilename") & "'"
                l_strSQL = l_strSQL & " AND system_deleted = 0"
                If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
                    If Not IsNull(l_rstTemp("bigfilesize")) Then
                        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, RequestDate, bigfilesize, RequestName, RequesterEmail) VALUES ("
                        l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                        l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
                        If ForgetAfterMove Then
                            l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move then Forget") & ", "
                        Else
                            l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
                        End If
                        l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
                        If PreserveFolderStructure = True Then
                            If Val(l_rstTemp("altlocation")) = l_rstTemp("companyID") Then
                                TempStr = Mid(l_rstTemp("altlocation"), Len(Trim(" " & l_rstTemp("companyID"))) + 2)
                            Else
                                TempStr = l_rstTemp("altlocation")
                            End If
                            If l_strNewFolder <> "" Then
                                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder & IIf(TempStr <> "", "\" & TempStr, "")) & "', "
                            Else
                                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstTemp("altlocation")) & "', "
                            End If
                        Else
                            If l_strNewFolder <> "" Then
                                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & "', "
                            Else
                                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstTemp("altlocation")) & "', "
                            End If
                        End If
                        l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
                        l_strSQL = l_strSQL & l_rstTemp("bigfilesize") & ", "
                        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                    Else
                        l_blnUnVerifiedFound = True
                    End If
                Else
                    l_blnDuplicatesFound = True
                End If
            Else
                l_strSQL = "UPDATE events SET "
                l_strSQL = l_strSQL & "libraryID = '" & l_lngNewLibraryID & "', "
                If l_lngNewLibraryID = 284349 Or l_lngNewLibraryID = 248095 Or l_lngNewLibraryID = 276356 Or l_lngNewLibraryID = 441212 Or l_lngNewLibraryID = 623206 Then
                    l_strSQL = l_strSQL & "hidefromweb = 0, "
                End If
                If GetData("library", "format", "libraryID", l_lngNewLibraryID) = "DISCSTORE" Then
                    l_strSQL = l_strSQL & "online = '1'"
                Else
                    l_strSQL = l_strSQL & "online = '0'"
                End If
                If l_strNewFolder <> "" Then
                    l_strSQL = l_strSQL & ", altlocation = '" & QuoteSanitise(l_strNewFolder) & "'"
                End If
                l_strSQL = l_strSQL & " WHERE eventID = " & l_rstTemp("eventID") & ";"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            End If
            If l_blnActuallyMove = False Then
                'Record a History event
                l_strSQL = "INSERT INTO eventhistory ("
                l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                l_strSQL = l_strSQL & "'" & l_rstTemp("eventID") & "', "
                l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                l_strSQL = l_strSQL & "'Move Grid' "
                l_strSQL = l_strSQL & ");"
                
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            End If
            Count = Count + 1
            l_rstTemp.MoveNext
        Loop
        l_rstTemp.Close
        Set l_rstTemp = Nothing
        If l_blnActuallyMove = True Then Set FSO = Nothing
        If l_blnDuplicatesFound = True Then
            MsgBox "Some of the files already existed in the destination folder." & vbCrLf & "File requests were not made for those files", vbCritical
        End If
        If l_blnUnVerifiedFound = True Then
            MsgBox "Some of the files were unverified." & vbCrLf & "File requests were not made for those files", vbCritical
        End If
    Else
        MsgBox "That Store does not yet exist!", vbOKOnly, "Problem..."
    End If

End If

ProgressBar1.Visible = False

adoClip.Refresh
adoClip.Caption = grdClips.Rows & " Clips Found"

Exit Sub

End Sub

Private Sub cmdMoveGridToAutoDelete_Click()

Dim l_strSQL As String, l_rstTemp As ADODB.Recordset
Dim Count As Long

If optStorageType(0).Value = False Then
    MsgBox "You haven't selected DISCSTORES for this search." & vbCrLf & "You must select DISCSTORES to perform actual File Requests.", vbCritical
    optStorageType(0).Value = True
    cmdSearch.Value = True
    Exit Sub
End If

Set l_rstTemp = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
CheckForSQLError
l_rstTemp.MoveFirst
Count = 0
ProgressBar1.Max = l_rstTemp.RecordCount
ProgressBar1.Value = 0
ProgressBar1.Visible = True

Do While Not l_rstTemp.EOF
    ProgressBar1.Value = Count
    DoEvents
    
    If l_rstTemp("clipformat") <> "Folder" Then
        
        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, RequestDate, bigfilesize, RequestName, RequesterEmail) VALUES ("
        l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
        l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move to AutoDelete") & ", "
        l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
        l_strSQL = l_strSQL & l_rstTemp("bigfilesize") & ", "
        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    Else
        MsgBox "Folders Cannot be handled in this way - item skipped"
    End If
        
    Count = Count + 1
    l_rstTemp.MoveNext
Loop
l_rstTemp.Close
Set l_rstTemp = Nothing

ProgressBar1.Visible = False

MsgBox "Done"

adoClip.Refresh
adoClip.Caption = grdClips.Rows & " Clips Found"

Exit Sub

End Sub

Private Sub cmdMoveSelected_Click()

Dim l_strNewLibraryBarcode As String, l_lngNewLibraryID As Long, l_strNewFolder As String, l_strSQL As String, l_rstTemp As ADODB.Recordset, Sub_Path As String, Rebuilt_Path As String
Dim Count As Long, l_blnActuallyMove As Boolean, l_blnDuplicatesFound As Boolean, l_lngDestinationLibraryID As Long, l_blnUnVerifiedFound As Boolean
Dim l_lngCount As Long, bkmrk As Variant
Dim PreserveFolderStructure As Boolean, TempStr As String, ForgetAfterMove As Boolean

If chkRecordUpdateOnly.Value <> 0 Then
    If MsgBox("Are you sure you wish to only Move File Records, rather than actually Moving Files?", vbYesNo, "Move Check") = vbNo Then
        Exit Sub
    End If
End If

If optStorageType(0).Value = False And chkRecordUpdateOnly.Value = 0 Then
    MsgBox "You haven't selected DISCSTORES for this search." & vbCrLf & "You must select DISCSTORES to perform actual File Requests.", vbCritical
    optStorageType(0).Value = True
    cmdSearch.Value = True
    Exit Sub
End If

l_lngCount = grdClips.SelBookmarks.Count

If l_lngCount <= 0 Then
    MsgBox "No rows selected.", vbCritical, "Error with Move Selected Rows"
    Exit Sub
End If

If MsgBox("Move " & l_lngCount & " items - Are you sure", vbYesNo + vbDefaultButton2, "Move Selected") = vbNo Then Exit Sub

frmGetNewFileDetails.Caption = "Please give the Barcode to move to..."
frmGetNewFileDetails.chkPreserveFolderStructure.Visible = True
frmGetNewFileDetails.Show vbModal
l_strNewLibraryBarcode = UCase(frmGetNewFileDetails.txtBarcode.Text)

If Left(l_strNewLibraryBarcode, 3) = "BFS" Then
    If Not CheckAccess("/MoveToBFS") Then
        MsgBox "You do not have permission to move or copy to BFS", vbCritical
        Exit Sub
    End If
End If

If CheckAccess("Superuser", True) Then

End If

l_strNewFolder = frmGetNewFileDetails.txtAltLocation.Text
PreserveFolderStructure = frmGetNewFileDetails.chkPreserveFolderStructure.Value
Unload frmGetNewFileDetails
l_blnActuallyMove = False

l_lngDestinationLibraryID = Val(GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode))
If CheckDriveWritePermissions(l_lngDestinationLibraryID) = False Then
    MsgBox "You do not have permissinos to send file to that DISCSTORE", vbCritical
    Exit Sub
End If

If l_strNewLibraryBarcode <> "" Then

    If CheckAccess("Superuser", True) Then
        If chkMoveThenForget.Value <> 0 Then
            ForgetAfterMove = True
        End If
    End If

    l_lngNewLibraryID = GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode)
    If l_lngNewLibraryID <> 0 Then
        
        If GetData("library", "format", "libraryID", l_lngNewLibraryID) = "DISCSTORE" And chkRecordUpdateOnly.Value = 0 Then
            If MsgBox("Do you wish to actually move the files, as well as change the CETA records", vbYesNo, "Question...") = vbYes Then l_blnActuallyMove = True Else l_blnActuallyMove = False
        End If
        
        Count = 1
        ProgressBar1.Max = l_lngCount
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        
        For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1
        
            
            bkmrk = grdClips.SelBookmarks(l_lngCount)
            Set l_rstTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & Val(grdClips.Columns("eventID").CellText(bkmrk)) & ";", g_strExecuteError)
            CheckForSQLError
            
            ProgressBar1.Value = Count
            DoEvents
            If l_blnActuallyMove = True Then
                l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & l_lngNewLibraryID
                If l_strNewFolder <> "" Then
                    l_strSQL = l_strSQL & " AND altlocation = '" & l_strNewFolder & "' "
                Else
                    l_strSQL = l_strSQL & " AND altlocation = '" & l_rstTemp("altlocation") & "' "
                End If
                l_strSQL = l_strSQL & " AND clipfilename = '" & l_rstTemp("clipfilename") & "'"
                l_strSQL = l_strSQL & " AND system_deleted = 0"
                If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
                    If Not IsNull(l_rstTemp("bigfilesize")) Then
                        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, RequestDate, bigfilesize, RequestName, RequesterEmail) VALUES ("
                        l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                        l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
                        If ForgetAfterMove Then
                            l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move then Forget") & ", "
                        Else
                            l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
                        End If
                        l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
                        If PreserveFolderStructure = True Then
                            If Val(l_rstTemp("altlocation")) = l_rstTemp("companyID") Then
                                TempStr = Mid(l_rstTemp("altlocation"), Len(Trim(" " & l_rstTemp("companyID"))) + 2)
                            Else
                                TempStr = l_rstTemp("altlocation")
                            End If
                            If l_strNewFolder <> "" Then
                                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & IIf(TempStr <> "", "\" & TempStr, "") & "', "
                            Else
                                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstTemp("altlocation")) & "', "
                            End If
                        Else
                            If l_strNewFolder <> "" Then
                                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & "', "
                            Else
                                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstTemp("altlocation")) & "', "
                            End If
                        End If
                        l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
                        l_strSQL = l_strSQL & l_rstTemp("bigfilesize") & ", "
                        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                    Else
                        l_blnUnVerifiedFound = True
                    End If
                Else
                    l_blnDuplicatesFound = True
                End If
            Else
                l_strSQL = "UPDATE events SET "
                l_strSQL = l_strSQL & "libraryID = '" & l_lngNewLibraryID & "', "
                If l_lngNewLibraryID = 284349 Or l_lngNewLibraryID = 248095 Or l_lngNewLibraryID = 276356 Or l_lngNewLibraryID = 441212 Or l_lngNewLibraryID = 623206 Then
                    l_strSQL = l_strSQL & "hidefromweb = 0, "
                End If
                If GetData("library", "format", "libraryID", l_lngNewLibraryID) = "DISCSTORE" Then
                    l_strSQL = l_strSQL & "online = '1'"
                Else
                    l_strSQL = l_strSQL & "online = '0'"
                End If
                If l_strNewFolder <> "" Then
                    l_strSQL = l_strSQL & ", altlocation = '" & QuoteSanitise(l_strNewFolder) & "'"
                End If
                l_strSQL = l_strSQL & " WHERE eventID = " & l_rstTemp("eventID") & ";"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            End If
            If l_blnActuallyMove = False Then
                'Record a History event
                l_strSQL = "INSERT INTO eventhistory ("
                l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                l_strSQL = l_strSQL & "'" & l_rstTemp("eventID") & "', "
                l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                l_strSQL = l_strSQL & "'Move Grid' "
                l_strSQL = l_strSQL & ");"
                
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            End If
            
            l_rstTemp.Close
            Set l_rstTemp = Nothing
            
            Count = Count + 1
        Next
        
        ProgressBar1.Visible = False
        lblCurrentFilename.Caption = ""
        DoEvents
        If l_blnDuplicatesFound = True Then
            MsgBox "Some of the files already existed in the destination folder." & vbCrLf & "File requests were not made for those files", vbCritical
        End If
        If l_blnUnVerifiedFound = True Then
            MsgBox "Some of the files were unverified." & vbCrLf & "File requests were not made for those files", vbCritical
        End If
    Else
        MsgBox "That Store does not yet exist!", vbOKOnly, "Problem..."
    End If

End If

adoClip.Refresh
adoClip.Caption = grdClips.Rows & " Clips Found"

Exit Sub

End Sub

Private Sub cmdMoveSelectedToAutoDelete_Click()

Dim l_strSQL As String, l_rstTemp As ADODB.Recordset
Dim Count As Long, l_lngCount As Long, bkmrk As Variant

If optStorageType(0).Value = False Then
    MsgBox "You haven't selected DISCSTORES for this search." & vbCrLf & "You must select DISCSTORES to perform actual File Requests.", vbCritical
    optStorageType(0).Value = True
    cmdSearch.Value = True
    Exit Sub
End If

l_lngCount = grdClips.SelBookmarks.Count

If l_lngCount <= 0 Then
    MsgBox "No rows selected.", vbCritical, "Error with Move Selected Rows"
    Exit Sub
End If

If MsgBox("Move " & l_lngCount & " items to AutoDelete - Are you sure", vbYesNo + vbDefaultButton2, "Move Selected to AutoDelete") = vbNo Then Exit Sub

Count = 1
ProgressBar1.Max = l_lngCount
ProgressBar1.Value = 0
ProgressBar1.Visible = True

For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1

    bkmrk = grdClips.SelBookmarks(l_lngCount)
    Set l_rstTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & Val(grdClips.Columns("eventID").CellText(bkmrk)) & ";", g_strExecuteError)
    CheckForSQLError
    
    ProgressBar1.Value = Count
    DoEvents

    If l_rstTemp("clipformat") <> "Folder" Then
        
        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, RequestDate, bigfilesize, RequestName, RequesterEmail) VALUES ("
        l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
        l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move to AutoDelete") & ", "
        l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
        l_strSQL = l_strSQL & l_rstTemp("bigfilesize") & ", "
        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    Else
        MsgBox "Folders Cannot be handled in this way - item skipped"
    End If
        
    Count = Count + 1
    l_rstTemp.Close

Next

Set l_rstTemp = Nothing
ProgressBar1.Visible = False

MsgBox "Done"

adoClip.Refresh
adoClip.Caption = grdClips.Rows & " Clips Found"

Exit Sub

End Sub

Private Sub cmdMoveToBVOD_Click()

Dim l_strNewLibraryBarcode As String, l_lngNewLibraryID As Long, l_strNewFolder As String, l_strSQL As String, l_rstTemp As ADODB.Recordset, Sub_Path As String, Rebuilt_Path As String
Dim Count As Long, FSO As Scripting.FileSystemObject, l_blnActuallyMove As Boolean, l_blnDuplicatesFound As Boolean

If optStorageType(0).Value = False And chkRecordUpdateOnly.Value = 0 Then
    MsgBox "You haven't selected DISCSTORES for this search." & vbCrLf & "You must select DISCSTORES to perform actual File Requests.", vbCritical
    optStorageType(0).Value = True
    cmdSearch.Value = True
    Exit Sub
End If

If MsgBox("This will put in move requests to move these items to LAT-PLATFORMS." & vbCrLf & "Are you Sure", vbYesNo + vbDefaultButton2, "Moving to LAT-PLATFORMS") = vbNo Then Exit Sub

l_lngNewLibraryID = 770135

Set l_rstTemp = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
CheckForSQLError
l_rstTemp.MoveFirst
Count = 0
ProgressBar1.Max = l_rstTemp.RecordCount
ProgressBar1.Value = 0
ProgressBar1.Visible = True
If l_blnActuallyMove = True Then Set FSO = New Scripting.FileSystemObject
Do While Not l_rstTemp.EOF
    ProgressBar1.Value = Count
    DoEvents
    l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & l_lngNewLibraryID
    If l_strNewFolder <> "" Then
        l_strSQL = l_strSQL & " AND altlocation = '" & l_strNewFolder & "' "
    Else
        l_strSQL = l_strSQL & " AND altlocation = '" & l_rstTemp("altlocation") & "' "
    End If
    l_strSQL = l_strSQL & " AND clipfilename = '" & l_rstTemp("clipfilename") & "'"
    l_strSQL = l_strSQL & " AND system_deleted = 0"
    If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, RequestDate, bigfilesize, RequestName, RequesterEmail) VALUES ("
        l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
        l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
        l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
        If l_strNewFolder <> "" Then
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & "', "
        Else
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstTemp("altlocation")) & "', "
        End If
        l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
        l_strSQL = l_strSQL & l_rstTemp("bigfilesize") & ", "
        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    Else
        l_blnDuplicatesFound = True
    End If
    Count = Count + 1
    l_rstTemp.MoveNext
Loop
l_rstTemp.Close
Set l_rstTemp = Nothing
If l_blnDuplicatesFound = True Then
    MsgBox "Some of the files already existed in the destination folder." & vbCrLf & "File requests were not made for those files", vbCritical
End If

ProgressBar1.Visible = False

adoClip.Refresh
adoClip.Caption = grdClips.Rows & " Clips Found"

Exit Sub

End Sub

Private Sub cmdMoveToEdit_Click()

Dim l_strNewLibraryBarcode As String, l_lngNewLibraryID As Long, l_strNewFolder As String, l_strSQL As String, l_rstTemp As ADODB.Recordset, Sub_Path As String, Rebuilt_Path As String
Dim Count As Long, FSO As Scripting.FileSystemObject, l_blnActuallyMove As Boolean, l_blnDuplicatesFound As Boolean

If optStorageType(0).Value = False And chkRecordUpdateOnly.Value = 0 Then
    MsgBox "You haven't selected DISCSTORES for this search." & vbCrLf & "You must select DISCSTORES to perform actual File Requests.", vbCritical
    optStorageType(0).Value = True
    cmdSearch.Value = True
    Exit Sub
End If

If MsgBox("This will put in move requests to move these items to LAT-EDIT." & vbCrLf & "Are you Sure", vbYesNo + vbDefaultButton2, "Moving to LAT-EDIT") = vbNo Then Exit Sub

l_lngNewLibraryID = 770133

Set l_rstTemp = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
CheckForSQLError
l_rstTemp.MoveFirst
Count = 0
ProgressBar1.Max = l_rstTemp.RecordCount
ProgressBar1.Value = 0
ProgressBar1.Visible = True
If l_blnActuallyMove = True Then Set FSO = New Scripting.FileSystemObject
Do While Not l_rstTemp.EOF
    ProgressBar1.Value = Count
    DoEvents
    l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & l_lngNewLibraryID
    If l_strNewFolder <> "" Then
        l_strSQL = l_strSQL & " AND altlocation = '" & l_strNewFolder & "' "
    Else
        l_strSQL = l_strSQL & " AND altlocation = '" & l_rstTemp("altlocation") & "' "
    End If
    l_strSQL = l_strSQL & " AND clipfilename = '" & l_rstTemp("clipfilename") & "'"
    l_strSQL = l_strSQL & " AND system_deleted = 0"
    If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, RequestDate, bigfilesize, RequestName, RequesterEmail) VALUES ("
        l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
        l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
        l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
        If l_strNewFolder <> "" Then
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & "', "
        Else
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstTemp("altlocation")) & "', "
        End If
        l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
        l_strSQL = l_strSQL & l_rstTemp("bigfilesize") & ", "
        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    Else
        l_blnDuplicatesFound = True
    End If
    Count = Count + 1
    l_rstTemp.MoveNext
Loop
l_rstTemp.Close
Set l_rstTemp = Nothing
If l_blnDuplicatesFound = True Then
    MsgBox "Some of the files already existed in the destination folder." & vbCrLf & "File requests were not made for those files", vbCritical
End If

ProgressBar1.Visible = False

adoClip.Refresh
adoClip.Caption = grdClips.Rows & " Clips Found"

Exit Sub

End Sub

Private Sub cmdMoveToOps_Click()

Dim l_strNewLibraryBarcode As String, l_lngNewLibraryID As Long, l_strNewFolder As String, l_strSQL As String, l_rstTemp As ADODB.Recordset, Sub_Path As String, Rebuilt_Path As String
Dim Count As Long, FSO As Scripting.FileSystemObject, l_blnActuallyMove As Boolean, l_blnDuplicatesFound As Boolean

If optStorageType(0).Value = False And chkRecordUpdateOnly.Value = 0 Then
    MsgBox "You haven't selected DISCSTORES for this search." & vbCrLf & "You must select DISCSTORES to perform actual File Requests.", vbCritical
    optStorageType(0).Value = True
    cmdSearch.Value = True
    Exit Sub
End If

If MsgBox("This will put in move requests to move these items to LAT-OPS." & vbCrLf & "Are you Sure", vbYesNo + vbDefaultButton2, "Moving to LAT-OPS") = vbNo Then Exit Sub

l_lngNewLibraryID = 770134

Set l_rstTemp = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
CheckForSQLError
l_rstTemp.MoveFirst
Count = 0
ProgressBar1.Max = l_rstTemp.RecordCount
ProgressBar1.Value = 0
ProgressBar1.Visible = True
If l_blnActuallyMove = True Then Set FSO = New Scripting.FileSystemObject
Do While Not l_rstTemp.EOF
    ProgressBar1.Value = Count
    DoEvents
    l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & l_lngNewLibraryID
    If l_strNewFolder <> "" Then
        l_strSQL = l_strSQL & " AND altlocation = '" & l_strNewFolder & "' "
    Else
        l_strSQL = l_strSQL & " AND altlocation = '" & l_rstTemp("altlocation") & "' "
    End If
    l_strSQL = l_strSQL & " AND clipfilename = '" & l_rstTemp("clipfilename") & "'"
    l_strSQL = l_strSQL & " AND system_deleted = 0"
    If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, RequestDate, bigfilesize, RequestName, RequesterEmail) VALUES ("
        l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
        l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
        l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
        If l_strNewFolder <> "" Then
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & "', "
        Else
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstTemp("altlocation")) & "', "
        End If
        l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
        l_strSQL = l_strSQL & l_rstTemp("bigfilesize") & ", "
        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    Else
        l_blnDuplicatesFound = True
    End If
    Count = Count + 1
    l_rstTemp.MoveNext
Loop
l_rstTemp.Close
Set l_rstTemp = Nothing
If l_blnDuplicatesFound = True Then
    MsgBox "Some of the files already existed in the destination folder." & vbCrLf & "File requests were not made for those files", vbCritical
End If

ProgressBar1.Visible = False

adoClip.Refresh
adoClip.Caption = grdClips.Rows & " Clips Found"

Exit Sub

End Sub

Private Sub cmdMoveToPrivate_Click()

Dim l_strNewLibraryBarcode As String, l_lngNewLibraryID As Long, l_strNewFolder As String, l_strSQL As String, l_rstTemp As ADODB.Recordset, Sub_Path As String, Rebuilt_Path As String
Dim Count As Long, FSO As Scripting.FileSystemObject, l_blnActuallyMove As Boolean, l_blnDuplicatesFound As Boolean

If optStorageType(0).Value = False And chkRecordUpdateOnly.Value = 0 Then
    MsgBox "You haven't selected DISCSTORES for this search." & vbCrLf & "You must select DISCSTORES to perform actual File Requests.", vbCritical
    optStorageType(0).Value = True
    cmdSearch.Value = True
    Exit Sub
End If

If MsgBox("This will put in move requests to move these items to LAT-CETA." & vbCrLf & "Are you Sure", vbYesNo + vbDefaultButton2, "Moving to LAT-CETA") = vbNo Then Exit Sub

l_lngNewLibraryID = 770131

Set l_rstTemp = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
CheckForSQLError
l_rstTemp.MoveFirst
Count = 0
ProgressBar1.Max = l_rstTemp.RecordCount
ProgressBar1.Value = 0
ProgressBar1.Visible = True
If l_blnActuallyMove = True Then Set FSO = New Scripting.FileSystemObject
Do While Not l_rstTemp.EOF
    ProgressBar1.Value = Count
    DoEvents
    l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & l_lngNewLibraryID
    If l_strNewFolder <> "" Then
        l_strSQL = l_strSQL & " AND altlocation = '" & l_strNewFolder & "' "
    Else
        l_strSQL = l_strSQL & " AND altlocation = '" & l_rstTemp("altlocation") & "' "
    End If
    l_strSQL = l_strSQL & " AND clipfilename = '" & l_rstTemp("clipfilename") & "'"
    l_strSQL = l_strSQL & " AND system_deleted = 0"
    If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, RequestDate, bigfilesize, RequestName, RequesterEmail) VALUES ("
        l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
        l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
        l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
        If l_strNewFolder <> "" Then
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & "', "
        Else
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstTemp("altlocation")) & "', "
        End If
        l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
        l_strSQL = l_strSQL & l_rstTemp("bigfilesize") & ", "
        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    Else
        l_blnDuplicatesFound = True
    End If
    Count = Count + 1
    l_rstTemp.MoveNext
Loop
l_rstTemp.Close
Set l_rstTemp = Nothing
If l_blnDuplicatesFound = True Then
    MsgBox "Some of the files already existed in the destination folder." & vbCrLf & "File requests were not made for those files", vbCritical
End If

ProgressBar1.Visible = False

adoClip.Refresh
adoClip.Caption = grdClips.Rows & " Clips Found"

Exit Sub

End Sub

Private Sub cmdOutputGridAsXML_Click()

If adoClip.Recordset.RecordCount <= 0 Then
    MsgBox "No clips files in the grid to output"
    Exit Sub
End If

Dim l_rst As ADODB.Recordset
Dim l_strSQL As String, l_strFileNameToSave As String

l_strSQL = adoClip.RecordSource

Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
If l_rst.RecordCount = adoClip.Recordset.RecordCount Then
    
    l_strFileNameToSave = fGetSpecialFolder(CSIDL_DOCUMENTS) & "\MediaFiles.xml"
    
    If l_strFileNameToSave <> "" And Right(l_strFileNameToSave, 5) <> "*.xml" Then
        
        Open l_strFileNameToSave For Output As 1
        Print #1, "<?xml version=""1.0"" encoding=""utf-8""?>"
        Print #1, "<MediaFiles>"
        l_rst.MoveFirst
        Do While Not l_rst.EOF
            Print #1, Chr(9) & "<MediaFile>"
            Print #1, Chr(9) & Chr(9) & "<MediaFileOwner>" & XMLSanitise(l_rst("companyname")) & "</MediaFileOwner>"
            Print #1, Chr(9) & Chr(9) & "<MediaFileTitle>";
            Print #1, XMLSanitise(l_rst("eventtitle"));
            If Not IsNull(l_rst("eventseries")) Then Print #1, " Sr. " & l_rst("eventseries");
            If Not IsNull(l_rst("eventepisode")) Then Print #1, " Ep. " & l_rst("eventepisode");
            If Not IsNull(l_rst("eventsubtitle")) Then Print #1, ": " & l_rst("eventsubtitle");
            Print #1, "</MediaFileTitle>"
            Print #1, Chr(9) & Chr(9) & "<MediaFileDuration>" & l_rst("fd_length") & "</MediaFileDuration>"
            Print #1, Chr(9) & Chr(9) & "<MediaFileFormat>" & XMLSanitise(l_rst("clipformat")) & "</MediaFileFormat>"
            Print #1, Chr(9) & Chr(9) & "<MediaFileBitrate>" & XMLSanitise(l_rst("clipbitrate")) & "</MediaFileBitrate>"
            Print #1, Chr(9) & Chr(9) & "<MediaFileName>" & XMLSanitise(l_rst("clipfilename")) & "</MediaFileName>"
            Print #1, Chr(9) & Chr(9) & "<MediaFileSize>" & XMLSanitise(l_rst("bigfilesize")) & "</MediaFileSize>"
            Print #1, Chr(9) & "</MediaFile>"
            l_rst.MoveNext
        Loop
    End If
    l_rst.Close
    Set l_rst = Nothing
    Print #1, "</MediaFiles>"
    Close #1
    MsgBox "Done"
Else
    MsgBox "Problem: The search has changed sionce you last refreshed the grid. Output not done"
    l_rst.Close
    Set l_rst = Nothing
End If

End Sub

Private Sub cmdPatheOrder_Click()

If txtPatheOrderNumber.Text = "" Then
    MsgBox "Cannot find an Order with no Pathe Order Number"
    Exit Sub
End If
    
Dim l_strSQL As String, l_rstPatheOrder As ADODB.Recordset
l_strSQL = "SELECT * from BP_Orders WHERE Order_Number = '" & txtPatheOrderNumber.Text & "'"
Debug.Print l_strSQL

Set l_rstPatheOrder = ExecuteSQL(l_strSQL, g_strExecuteError)
If l_rstPatheOrder.RecordCount < 1 Then
    If MsgBox("Order not found - Create a New Order", vbYesNo, "Pathe Order Processing") = vbNo Then
        Exit Sub
    Else
        l_strSQL = "INSERT INTO BP_Orders (Order_Number, Order_Email_Text) VALUES("
        l_strSQL = l_strSQL & "'" & txtPatheOrderNumber.Text & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(txtOriginalOrderEmail.Text) & "')"
        l_rstPatheOrder.Close
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_strSQL = "SELECT * from BP_Orders WHERE Order_Number = '" & txtPatheOrderNumber.Text & "'"
        Set l_rstPatheOrder = ExecuteSQL(l_strSQL, g_strExecuteError)
    End If
End If

If l_rstPatheOrder.RecordCount = 1 Then
    txtOriginalOrderEmail.Text = l_rstPatheOrder("Order_Email_Text")
    txtPatheOrderNumber.Text = l_rstPatheOrder("Order_number")
    lblOrderID.Caption = l_rstPatheOrder("Order_ID")
    If Not IsNull(l_rstPatheOrder("Order_Date")) Then datPatheOrderCreated.Value = l_rstPatheOrder("Order_Date") Else datPatheOrderCreated.Value = Null
    If Not IsNull(l_rstPatheOrder("Order_Processed")) Then datPatheOrderProcessed.Value = l_rstPatheOrder("Order_Processed") Else datPatheOrderProcessed.Value = Null
    adoPatheClips.RecordSource = "SELECT * FROM BP_Order_Detail WHERE Order_ID = " & lblOrderID.Caption
    adoPatheClips.ConnectionString = g_strConnection
    adoPatheClips.Refresh
    adoPatheClips.Caption = adoPatheClips.Recordset.RecordCount & " Clip(s)"
End If

End Sub

Private Sub cmdPatheOrders_Click()

fraPatheOrder.Visible = True

End Sub

Private Sub cmdPortalSanitise_Click()

If MsgBox("Are you sure?", vbYesNo, "Sanitise...") = vbNo Then Exit Sub

Dim l_strSQL As String, l_rstTemp As ADODB.Recordset, Count As Long

If adoClip.Recordset.RecordCount <> 0 Then
    
    If m_blnSilentSanitise <> True Then
        If MsgBox("This will remove all the listed clips from the portals" & vbCrLf & "Are you Sure?", vbYesNo, "Sanitise Portal Access") = vbNo Then
            Exit Sub
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    DoEvents

    Set l_rstTemp = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
    CheckForSQLError
    l_rstTemp.MoveFirst
    ProgressBar1.Max = l_rstTemp.RecordCount
    ProgressBar1.Value = 0
    Count = 0
    ProgressBar1.Visible = True
    DoEvents
    
    Do While Not l_rstTemp.EOF
        ProgressBar1.Value = Count
        lblCurrentFilename.Caption = "Sanitising Clip " & l_rstTemp("eventID")
        DoEvents
        l_strSQL = "DELETE FROM portalpermission WHERE eventID = '" & l_rstTemp("eventID") & "';"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        Count = Count + 1
        l_rstTemp.MoveNext
    Loop
    lblCurrentFilename.Caption = ""
    l_rstTemp.Close
    Set l_rstTemp = Nothing
    
    ProgressBar1.Visible = False
    Screen.MousePointer = vbDefault

End If

adoClip.Recordset.Requery

End Sub

Private Sub cmdPrepareYouTubeSend_Click()

'Has to be some records int he grid
If adoClip.Recordset.RecordCount <= 0 Then Exit Sub

'Has to be a companyID
If Val(lblCompanyID.Caption) = 0 Then
    MsgBox "A Single client must be selected for File Sends.", vbCritical
    Exit Sub
End If

''Has to be from either a DISCSTORE or the DIVA
'If (optStorageType(0).Value = False And lblFormat.Caption <> "DISCSTORE") And (optStorageType(4).Value = False And lblFormat.Caption <> "DIVA") Then
'    MsgBox "We can only Delivery items that are on a DISCSTORE or the DIVA at this time.", vbCritical
'    Exit Sub
'End If

Dim l_strSQL As String, Count As Long, FSO As Scripting.FileSystemObject
Dim l_lngCount As Long, bkmrk As Variant, l_rstTemp As ADODB.Recordset
Dim l_lngNewLibraryID As Long, l_lngNewEventID As Long
Dim l_strBatchName As String, l_strBatchFolder As String, l_strBatchFilename As String
Dim l_blnTestRun As Boolean

'Give the fitst 'Are You Sure?'
If MsgBox("You are about to Prepare YouTube Metadata for these " & adoClip.Recordset.RecordCount & " Files." & vbCrLf & "Are you Sure", vbYesNo) = vbNo Then Exit Sub

l_strBatchName = InputBox("Give the BatchName", "YouTube Metadata Batch", "Batch_0")

If l_strBatchName = "" Then Exit Sub
        
l_lngNewLibraryID = GetData("library", "libraryID", "barcode", "DMZ-IMPEX")
l_strBatchFolder = "905\Reuters_Project_Batch_Sends\" & l_strBatchName
l_strBatchFilename = GetData("library", "subtitle", "libraryID", l_lngNewLibraryID) & "\" & l_strBatchFolder
Set FSO = New Scripting.FileSystemObject
If Not FSO.FolderExists(l_strBatchFilename) Then FSO.CreateFolder (l_strBatchFilename)
l_strBatchFilename = l_strBatchFilename & "\" & l_strBatchName & ".csv"

Open l_strBatchFilename For Output As 1
If MsgBox("Yes or no", vbYesNo, "Should the Clips be marked Private") = vbNo Then
    l_blnTestRun = False
Else
    l_blnTestRun = True
End If

Print #1, "filename,channel,custom_id,add_asset_labels,title,description,keywords,spoken_language,caption_file,caption_language,category,privacy,notify_subscribers,start_time,end_time,custom_thumbnail,ownership,block_outside_ownership,usage_policy,enable_content_id,reference_exclusions,match_policy,ad_types,ad_break_times,playlist_id,is_made_for_kids"
Count = 1
ProgressBar1.Max = adoClip.Recordset.RecordCount
ProgressBar1.Value = 0
ProgressBar1.Visible = True
adoClip.Recordset.MoveFirst
Do While Not adoClip.Recordset.EOF
    ProgressBar1.Value = Count
    DoEvents

    If Not IsNull(adoClip.Recordset("bigfilesize")) Then
    
        Print #1, """" & adoClip.Recordset("clipfilename") & """,";
        Print #1, """britishpathe"",";
        Print #1, """" & adoClip.Recordset("clipreference") & """,""Reuters"",";
        Print #1, """" & UTF8_Encode(DoubleQuoteSanitise(adoClip.Recordset("eventtitle"))) & """,";
        Print #1, """" & UTF8_Encode(DoubleQuoteSanitise(adoClip.Recordset("notes"))) & """,";
        Print #1, ",""EN"",,,,";
        If l_blnTestRun = True Then
            Print #1, """Private"",""no"",";
        Else
            Print #1, """Public"",""no"",";
        End If
        Print #1, ",,,,,,""yes"",,,,,,,"
        
        If adoClip.Recordset("libraryID") <> l_lngNewLibraryID Or adoClip.Recordset("altlocation") <> l_strBatchFolder Then
            'Invoke a copy to the hotfolder location
            l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, RequestDate, bigfilesize, RequestName, RequesterEmail) VALUES ("
            l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
            l_strSQL = l_strSQL & adoClip.Recordset("LibraryID") & ", "
            l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
            l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strBatchFolder) & "', "
            l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
            l_strSQL = l_strSQL & adoClip.Recordset("bigfilesize") & ", "
            l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
    
            l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, bigfilesize, accesstype, finalstatus) VALUES ("
            l_strSQL = l_strSQL & "3684, "
            l_strSQL = l_strSQL & adoClip.Recordset("companyID") & ", "
            l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
            l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
            l_strSQL = l_strSQL & adoClip.Recordset("bigfilesize") & ", "
            l_strSQL = l_strSQL & "'Send to YouTube', "
            l_strSQL = l_strSQL & "'Completed')"
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
        End If
    Else
        If MsgBox("Unverified file " & vbCrLf & adoClip.Recordset("clipfilename") & vbCrLf & "Abort Operation", vbYesNo + vbDefaultButton2) = vbYes Then
            Exit Do
        Else
            MsgBox "Unverified File Ignored."
        End If
    End If
    
    Count = Count + 1
    adoClip.Recordset.MoveNext
Loop

Close #1

ProgressBar1.Visible = False

MsgBox "Done"

End Sub

Private Sub cmdProcessPatheTextFilmID_Click()

If lblOrderID.Caption = "" Then Exit Sub

Dim myRegExp As RegExp
Dim myMatches As MatchCollection
Dim myMatch As Match
Dim l_rst As ADODB.Recordset, l_strSQL As String

'This function uses Regular expressions to find all the Pathe style Film.ID text sections in the Order email text, and returns MyMatches as a collection of these.
Set myRegExp = New RegExp
myRegExp.IgnoreCase = True
myRegExp.Global = True
myRegExp.Pattern = "([0-9]+\.[0-9][0-9])"
Set myMatches = myRegExp.Execute(txtOriginalOrderEmail.Text)
For Each myMatch In myMatches
    'This loop performs a search of our MAM for a file that has the Film.ID corresponding to the matched piece of text, and then adds that Media File to a BP_Order_Detail table.
    'From there, the list of media files can be used to initiate work or a delivery.
    'There are criteria on the Media Serach, to make sure we get the right version of the clip, stored on our spinning disk master store, rather than any other versions of the file that happen to exist.
    l_strSQL = "SELECT eventID FROM events WHERE customfield1 = '" & myMatch & "' AND system_deleted = 0 AND libraryID IN (SELECT intvalue FROM setting WHERE name = 'PatheDiskID') AND altlocation LIKE '905\00%'"
    Debug.Print l_strSQL
    Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
    If l_rst.RecordCount = 1 Then
        l_strSQL = "INSERT INTO BP_Order_Detail (Order_ID, eventID) VALUES ("
        l_strSQL = l_strSQL & lblOrderID.Caption & ", "
        l_strSQL = l_strSQL & l_rst("eventID") & ")"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    End If
    l_rst.Close
Next

'Final stage is to refresh the screen that this routing was called from, so that it now shows the items that have been found.
SetData "BP_Orders", "Order_Processed", "Order_ID", lblOrderID.Caption, Format(Now, "YYYY-MM-DD HH:NN:SS")
cmdPatheOrder.Value = True

End Sub

Private Sub cmdProcessPatheTextVLVA_Click()

If lblOrderID.Caption = "" Then Exit Sub

Dim myRegExp As RegExp
Dim myMatches As MatchCollection
Dim myMatch As Match
Dim l_rst As ADODB.Recordset, l_strSQL As String

Set myRegExp = New RegExp
myRegExp.IgnoreCase = True
myRegExp.Global = True
myRegExp.Pattern = "(V[A-Z0-9]{24,28})"
Set myMatches = myRegExp.Execute(txtOriginalOrderEmail.Text)
For Each myMatch In myMatches
    'This loop performs a search of our MAM for a file that has the Film.ID corresponding to the matched piece of text, and then adds that Media File to a BP_Order_Detail table.
    'From there, the list of media files can be used to initiate work or a delivery.
    'There are criteria on the Media Serach, to make sure we get the right version of the clip, stored on our spinning disk master store, rather than any other versions of th file that happen to exist.
    l_strSQL = "SELECT eventID FROM events WHERE customfield1 = '" & myMatch & "' AND companyID = 905 AND system_deleted = 0 AND libraryID IN (SELECT intvalue FROM setting WHERE name = 'PatheDiskID') AND altlocation LIKE '905\Pathe%'"
    Debug.Print l_strSQL
    Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
    If l_rst.RecordCount = 1 Then
        l_strSQL = "INSERT INTO BP_Order_Detail (Order_ID, eventID) VALUES ("
        l_strSQL = l_strSQL & lblOrderID.Caption & ", "
        l_strSQL = l_strSQL & l_rst("eventID") & ")"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    End If
    l_rst.Close
Next

'Final stage is to refresh thet screen that this routing was called from, so that it now shows the items that have been found.
SetData "BP_Orders", "Order_Processed", "Order_ID", lblOrderID.Caption, Format(Now, "YYYY-MM-DD HH:NN:SS")
cmdPatheOrder.Value = True

End Sub

Private Sub cmdReleaseBBCMGClips_Click()

Dim l_rst As ADODB.Recordset, l_strSQL As String, l_lngCount As Long

l_strSQL = "SELECT * FROM BBCMG_Clip_Order WHERE Request_Number = 'REQ-00261292' AND Order_Status_ID = 8"

Exit Sub

Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
If l_rst.RecordCount > 0 Then
    l_rst.MoveFirst
    Do While Not l_rst.EOF
    
        lblCurrentFilename.Caption = l_rst("Clip_Order_ID")
        DoEvents
        l_rst("Order_status_ID") = 9
        l_rst.Update
        l_rst.MoveNext
    
    Loop
    lblCurrentFilename.Caption = ""
    DoEvents
End If
l_rst.Close

End Sub

Private Sub cmdRelinkTechReviews_Click()

If adoClip.Recordset.RecordCount <= 0 Then Exit Sub

Dim l_strSQL As String, l_rstClip As ADODB.Recordset, Count As Long, l_lngtechrevID As Long

TabMediaSearching.Tab = 0

If MsgBox("Are you sure?", vbYesNo, "Relinking Tech Reviews") = vbNo Then Exit Sub

adoClip.Recordset.MoveFirst
Count = 0
ProgressBar1.Max = adoClip.Recordset.RecordCount
ProgressBar1.Value = 0
Count = 0
ProgressBar1.Visible = True
DoEvents

Do While Not adoClip.Recordset.EOF
    lblCurrentFilename.Caption = adoClip.Recordset("eventID")
    ProgressBar1.Value = Count
    DoEvents
    If GetFlag(grdClips.Columns("techrev").Text) = 0 Then
        Set l_rstClip = ExecuteSQL("SELECT techrevID, filename, bigfilesize FROM techrev WHERE filename = '" & adoClip.Recordset("clipfilename") & "'", g_strExecuteError)
        If l_rstClip.RecordCount > 0 Then
            If MsgBox("Filesize: " & adoClip.Recordset("bigfilesize") & " Clip" & vbCrLf & "Filesize: " & GetData("techrev", "bigfilesize", "techrevID", l_rstClip("techrevID")) & " Techrev", vbYesNo, adoClip.Recordset("clipfilename")) = vbYes Then
                SetData "techrev", "eventID", "techrevID", l_rstClip("techrevID"), adoClip.Recordset("eventID")
            End If
        Else
            MsgBox "Could not find a techrev for:" & vbCrLf & adoClip.Recordset("clipfilename"), vbInformation
        End If
    End If
    Count = Count + 1
    adoClip.Recordset.MoveNext
Loop

ProgressBar1.Visible = False

lblCurrentFilename.Caption = ""
adoClip.Recordset.Requery
MsgBox "Done"

End Sub

Private Sub cmdReport_Click()

Dim l_strSelectionFormula As String
Dim l_strReportFile As String

If chkSearchArchive.Value = 0 Then
    
    If txtSQLQuery.Text <> "" Then
        If MsgBox("Direct SQL Query and Last Usage Before will not be involved the report" & vbCrLf & "Do you wish to continue?", vbYesNo, "Report Search is Restricted.") = vbNo Then
            Exit Sub
        End If
    End If
    
    If Not CheckAccess("/printrecordreport") Then Exit Sub
    
    l_strSelectionFormula = m_strCrystalSearchConditions
        
    l_strReportFile = g_strLocationOfCrystalReportFiles & "MediaContentDetail.rpt"
    Debug.Print m_strCrystalSearchConditions
    PrintCrystalReport l_strReportFile, l_strSelectionFormula, True
    
Else
    MsgBox "Cannot print report of archived clips.", vbCritical, "Error..."
End If

End Sub

Private Sub cmdSanitiseOnlineStatus_Click()

If adoClip.Recordset.RecordCount <= 0 Then Exit Sub

If MsgBox("Are you sure?", vbYesNo, "Sanitise Grid") = vbNo Then Exit Sub

Dim l_strSQL As String, l_rstClip As ADODB.Recordset, Count As Long

Set l_rstClip = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
CheckForSQLError

l_rstClip.MoveFirst
Count = 0
ProgressBar1.Max = l_rstClip.RecordCount
ProgressBar1.Value = 0
Count = 0
ProgressBar1.Visible = True
DoEvents

Do While Not l_rstClip.EOF
    lblCurrentFilename.Caption = l_rstClip("eventID")
    ProgressBar1.Value = Count
    DoEvents
    If GetData("Library", "format", "libraryID", l_rstClip("libraryID")) = "DISCSTORE" Then
        SetData "events", "online", "eventID", l_rstClip("eventID"), 1
    Else
        SetData "events", "online", "eventID", l_rstClip("eventID"), 0
    End If
    Count = Count + 1
    l_rstClip.MoveNext
Loop
l_rstClip.Close
Set l_rstClip = Nothing
ProgressBar1.Visible = False

lblCurrentFilename.Caption = ""
adoClip.Recordset.Requery
MsgBox "Done"

End Sub

Private Sub cmdSearch_Click()

Dim l_strSQL As String, l_blnflag As Boolean, l_lngCount As Long
Dim l_rstTest As ADODB.Recordset, l_curTotalSize As Currency, l_strTotalSize As String

Screen.MousePointer = vbHourglass

On Error Resume Next
For l_lngCount = 1 To 54
    If l_lngCount <> 52 Then chkBulkUpdate(l_lngCount).Value = 0
Next
On Error GoTo 0

l_blnflag = False

l_strSQL = "SELECT "
If Val(txtSelectTop.Text) <> 0 Then l_strSQL = l_strSQL & "TOP " & Val(txtSelectTop.Text) & " "
l_strSQL = l_strSQL & "events.*, (SELECT CASE WHEN md5checksum IS NULL OR md5checksum = '' THEN 0 ELSE 1 END AS Expr1) AS MD5present, (SELECT CASE WHEN sha1checksum IS NULL OR sha1checksum = '' THEN 0 ELSE 1 END AS Expr2) AS SHA1present FROM events WHERE 1=1 "
m_strSearchConditions = ""
m_strCrystalSearchConditions = "1=1 "

If chkSearchArchive.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND system_deleted <> 0 "
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.system_deleted} <> 0 "
Else
    m_strSearchConditions = m_strSearchConditions & " AND system_deleted = 0 "
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.system_deleted} = 0 "
End If

If lblCompanyID.Caption <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND companyID = '" & lblCompanyID.Caption & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.companyID} = " & Val(lblCompanyID.Caption) & " "
ElseIf chkCompanyNull.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND (companyID = 0 OR companyID IS NULL) "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.companyID} = 0 OR Isnull({events.companyID})) "
End If

If lblLibraryID.Caption <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND libraryID = '" & lblLibraryID.Caption & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.libraryID} = " & Val(lblLibraryID.Caption) & " "
End If

If txtJobID.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND jobID = '" & txtJobID.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.jobID} = " & Val(txtJobID.Text) & " "
ElseIf chkJobIDisNull.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND (jobID = 0 OR jobID IS NULL) "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.jobID} = 0 OR Isnull({events.jobID})) "
End If
If txtAssignedJobID.Text <> "" And Val(txtAssignedJobID.Text) <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND events.eventID IN (SELECT eventID FROM requiredmedia WHERE jobID = " & Val(txtAssignedJobID.Text) & ") "
    l_blnflag = True
End If

If txtSvenskProjectNumber.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND svenskprojectnumber = '" & txtSvenskProjectNumber.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.svenskprojectnumber} '= " & txtSvenskProjectNumber.Text & "' "
End If
If txtSeriesID.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND seriesID LIKE '" & txtSeriesID.Text & "%' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.seriesID} Like '" & WildcardChange(txtSeriesID.Text, "%", "*") & "*' "
ElseIf chkSerNull.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND (seriesID IS NULL OR seriesID = '') "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND (Isnull({events.seriesID}) OR  {events.seriesID} = '') "
End If
If txtSerialNumber.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND serialnumber = '" & txtSerialNumber.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.serialnumber} = '" & txtSerialNumber.Text & "' "
ElseIf chkSerialNoIsNull.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND (serialnumber IS NULL OR serialnumber = '') "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND (Isnull({events.serialnumber}) OR  {events.serialnumber} = '') "
End If
If lblSourceLibraryID.Caption <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND sourcelibraryID = '" & lblSourceLibraryID.Caption & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.sourcelibraryID} = " & Val(lblSourceLibraryID.Caption) & " "
End If
If cmbAspect.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND aspectratio LIKE '" & QuoteSanitise(cmbAspect.Text) & "%' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.aspectratio} LIKE '" & WildcardChange(cmbAspect.Text, "%", "*") & "*' "
End If
If cmbGeometry.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND geometriclinearity LIKE '" & QuoteSanitise(cmbGeometry.Text) & "%' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.geometriclinearity} LIKE '" & WildcardChange(cmbGeometry.Text, "%", "*") & "*' "
End If
If cmbLanguage.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND language LIKE '" & QuoteSanitise(cmbLanguage.Text) & "%' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.language} LIKE '" & WildcardChange(cmbLanguage.Text, "%", "*") & "*' "
End If
If cmbVodPlatform.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND vodplatform = '" & QuoteSanitise(cmbVodPlatform.Text) & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.vodplatform} = '" & cmbVodPlatform.Text & "' "
End If
If txtInternalReference.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND internalreference = '" & txtInternalReference.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.internalreference} = '" & txtInternalReference.Text & "' "
End If
If txtNotes.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND (notes LIKE '" & QuoteSanitise(txtNotes.Text) & "%' OR notes LIKE '%" & QuoteSanitise(txtNotes.Text) & "%') "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.notes} LIKE '" & WildcardChange(txtNotes.Text, "%", "*") & "*' OR {events.notes LIKE '*" & WildcardChange(txtNotes.Text, "%", "*") & "*') "
End If
If chkFileLock.Value = 1 Then
    m_strSearchConditions = m_strSearchConditions & " AND (events.sound_stereo_russian = 1) "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & " AND ({events.sound_stereo_russian} = 1) "
End If
If txtMD5.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND (md5checksum = '" & QuoteSanitise(txtMD5.Text) & "') "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.md5checksum} = '" & txtMD5.Text & "') "
End If
On Error Resume Next
grdClips.Columns("md5").Visible = True
'grdClips.Columns("audioanalysis").Visible = True
grdClips.Columns("MW").Visible = True
On Error GoTo 0
If optHidden(1).Value = True Then
    'Hidden from Web
    m_strSearchConditions = m_strSearchConditions & " AND (hidefromweb <> 0) "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.hidefromweb} <> 0 "
ElseIf optHidden(2).Value = True Then
    'Visible on Web
    m_strSearchConditions = m_strSearchConditions & " AND (hidefromweb IS NULL OR hidefromweb = 0) "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND (Isnull({events.hidefromweb}) or {events.hidefromweb} = 0) "
ElseIf optHidden(3).Value = True Then
    'Assigned on Web
    m_strSearchConditions = m_strSearchConditions & " AND (events.eventID IN (SELECT eventID FROM portalpermission)) "
ElseIf optHidden(4).Value = True Then
    'Not Assigned on Web
    m_strSearchConditions = m_strSearchConditions & " AND (events.eventID NOT IN (SELECT portalpermissioneventID FROM vweventonmediawindow WHERE eventID = portalpermissioneventID)) "
ElseIf optHidden(5).Value = True Then
    'Not on SMVSTORE
    m_strSearchConditions = m_strSearchConditions & " AND (libraryID <> 445810) "
End If

If optStorageOwner(1).Value = True Then
    m_strSearchConditions = m_strSearchConditions & " AND library.companyID = 501 "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {library.companyID} = 501 "
End If
If optStorageOwner(2).Value = True Then
    m_strSearchConditions = m_strSearchConditions & " AND library.companyID <> 501 "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {library.companyID} <> 501 "
End If

If cmbClipformat.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND clipformat = '" & cmbClipformat.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.clipformat} = '" & cmbClipformat.Text & "' "
End If
If cmbClipcodec.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND clipcodec = '" & cmbClipcodec.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.clipcodec} = '" & cmbClipcodec.Text & "' "
End If
If cmbStreamType.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND mediastreamtype = '" & cmbStreamType.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.mediastreamtype} = '" & cmbStreamType.Text & "' "
End If
If txtSubtitle.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND eventsubtitle LIKE '" & QuoteSanitise(txtSubtitle.Text) & "%' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.eventsubtitle} LIKE '" & WildcardChange(txtSubtitle.Text, "%", "*") & "*' "
End If
If cmbVersion.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND eventversion LIKE '" & QuoteSanitise(cmbVersion.Text) & "%' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.eventversion} LIKE '" & WildcardChange(cmbVersion.Text, "%", "*") & "*' "
End If
If cmbSeries.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND eventseries LIKE '" & QuoteSanitise(cmbSeries.Text) & "%' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.eventseries} LIKE '" & WildcardChange(cmbSeries.Text, "%", "*") & "*' "
End If
If cmbSet.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND eventset LIKE '" & QuoteSanitise(cmbSet.Text) & "%' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.eventset} LIKE '" & WildcardChange(cmbSet.Text, "%", "*") & "*' "
End If
If cmbEpisode.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND eventepisode LIKE '" & QuoteSanitise(cmbEpisode.Text) & "%' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.eventepisode} LIKE '" & WildcardChange(cmbEpisode.Text, "%", "*") & "*' "
End If
If txtBitrate.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND clipbitrate = '" & txtBitrate.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.clipbitrate} = '" & txtBitrate.Text & "' "
End If
If txtVideoBitrate.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND videobitrate = '" & txtVideoBitrate.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.videobitrate} = '" & txtVideoBitrate.Text & "' "
End If
If txtAudioBitrate.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND audiobitrate = '" & txtAudioBitrate.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.audiobitrate} = '" & txtAudioBitrate.Text & "' "
End If
If txtClipfilename.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND clipfilename LIKE '" & QuoteSanitise(txtClipfilename.Text) & "%' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.clipfilename} LIKE '" & WildcardChange(txtClipfilename.Text, "%", "*") & "*' "
End If
If txtClockNumber.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND clocknumber LIKE '" & QuoteSanitise(txtClockNumber.Text) & "%' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.clocknumber} LIKE '" & WildcardChange(txtClockNumber.Text, "%", "*") & "*' "
End If
If cmbPurpose.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND eventtype LIKE '" & QuoteSanitise(cmbPurpose.Text) & "%' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.eventtype} LIKE '" & WildcardChange(cmbPurpose.Text, "%", "*") & "*' "
End If
If cmbFrameRate.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND clipframerate = '" & cmbFrameRate.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.clipframerate} = '" & cmbFrameRate.Text & "' "
End If
If cmbInterlace.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND interlace = '" & cmbInterlace.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.interlace} = '" & cmbInterlace.Text & "' "
End If
If cmbPasses.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND encodepasses = '" & cmbPasses.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.encodepasses} = '" & cmbPasses.Text & "' "
End If
If cmbCbrVbr.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND cbrvbr = '" & cmbCbrVbr.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.cbrvbr} = '" & cmbCbrVbr.Text & "' "
End If
If txtHorizpixels.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND cliphorizontalpixels = '" & txtHorizpixels.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.cliphorizontalpixels} = " & Val(txtHorizpixels.Text) & " "
End If
If txtVertpixels.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND clipverticalpixels = '" & txtVertpixels.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.clipverticalpixels} = " & Val(txtVertpixels.Text) & " "
End If
If cmbClipType.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND cliptype = '" & cmbClipType.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.cliptype} = '" & cmbClipType.Text & "' "
End If
If cmbGenre.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND clipgenre = '" & cmbGenre.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.clipgenre} = " & cmbGenre.Text & "' "
End If
If cmbAudioCodec.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND clipaudiocodec = '" & cmbAudioCodec.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.clipaudiocodec} = '" & cmbAudioCodec.Text & "' "
End If
If cmbFileVersion.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND fileversion = '" & cmbFileVersion.Text & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.fileversion} = '" & cmbFileVersion.Text & "' "
End If

If cmbProfileGroup.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND resourceID = " & Val(GetDataSQL("SELECT TOP 1 description FROM xref WHERE category = 'portalpurchaseprofilegroups' AND information = '" & cmbProfileGroup.Text & "';")) & " "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.resourceID} = " & Val(GetDataSQL("SELECT TOP 1 description FROM xref WHERE category = 'portalpurchaseprofilegroups' AND information = '" & cmbProfileGroup.Text & "';")) & " "
End If

If chkExplicitText.Value = 0 Then
    If cmbField1.Text <> "" Then
        l_blnflag = True
        If chkCustomFieldInclusive.Value <> 0 Then
            m_strSearchConditions = m_strSearchConditions & " AND (customfield1 LIKE '" & QuoteSanitise(cmbField1.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield1} LIKE '" & WildcardChange(cmbField1.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield2 LIKE '" & QuoteSanitise(cmbField1.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield2} LIKE '" & WildcardChange(cmbField1.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield3 LIKE '" & QuoteSanitise(cmbField1.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield3} LIKE '" & WildcardChange(cmbField1.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield4 LIKE '" & QuoteSanitise(cmbField1.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield4} LIKE '" & WildcardChange(cmbField1.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield5 LIKE '" & QuoteSanitise(cmbField1.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield5} LIKE '" & WildcardChange(cmbField1.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield6 LIKE '" & QuoteSanitise(cmbField1.Text) & "%') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield6} LIKE '" & WildcardChange(cmbField1.Text, "%", "*") & "*') "
        Else
            m_strSearchConditions = m_strSearchConditions & " AND (customfield1 LIKE '" & QuoteSanitise(cmbField1.Text) & "%') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield1} LIKE '" & WildcardChange(cmbField1.Text, "%", "*") & "*') "
        End If
    End If
    If cmbField2.Text <> "" Then
        l_blnflag = True
        If chkCustomFieldInclusive.Value <> 0 Then
            m_strSearchConditions = m_strSearchConditions & " AND (customfield1 LIKE '" & QuoteSanitise(cmbField2.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield1} LIKE '" & WildcardChange(cmbField2.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield2 LIKE '" & QuoteSanitise(cmbField2.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield2} LIKE '" & WildcardChange(cmbField2.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield3 LIKE '" & QuoteSanitise(cmbField2.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield3} LIKE '" & WildcardChange(cmbField2.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield4 LIKE '" & QuoteSanitise(cmbField2.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield4} LIKE '" & WildcardChange(cmbField2.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield5 LIKE '" & QuoteSanitise(cmbField2.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield5} LIKE '" & WildcardChange(cmbField2.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield6 LIKE '" & QuoteSanitise(cmbField2.Text) & "%') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield6} LIKE '" & WildcardChange(cmbField2.Text, "%", "*") & "*') "
        Else
            m_strSearchConditions = m_strSearchConditions & " AND (customfield2 LIKE '" & QuoteSanitise(cmbField2.Text) & "%') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield2} LIKE '" & WildcardChange(cmbField2.Text, "%", "*") & "*') "
        End If
    End If
    If cmbField3.Text <> "" Then
        l_blnflag = True
        If chkCustomFieldInclusive.Value <> 0 Then
            m_strSearchConditions = m_strSearchConditions & " AND (customfield1 LIKE '" & QuoteSanitise(cmbField3.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield1} LIKE '" & WildcardChange(cmbField3.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield2 LIKE '" & QuoteSanitise(cmbField3.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield2} LIKE '" & WildcardChange(cmbField3.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield3 LIKE '" & QuoteSanitise(cmbField3.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield3} LIKE '" & WildcardChange(cmbField3.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield4 LIKE '" & QuoteSanitise(cmbField3.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield4} LIKE '" & WildcardChange(cmbField3.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield5 LIKE '" & QuoteSanitise(cmbField3.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield5} LIKE '" & WildcardChange(cmbField3.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield6 LIKE '" & QuoteSanitise(cmbField3.Text) & "%') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield6} LIKE '" & WildcardChange(cmbField3.Text, "%", "*") & "*') "
        Else
            m_strSearchConditions = m_strSearchConditions & " AND (customfield3 LIKE '" & QuoteSanitise(cmbField3.Text) & "%') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield3} LIKE '" & WildcardChange(cmbField3.Text, "%", "*") & "*') "
        End If
    End If
    If cmbField4.Text <> "" Then
        l_blnflag = True
        If chkCustomFieldInclusive.Value <> 0 Then
            m_strSearchConditions = m_strSearchConditions & " AND (customfield1 LIKE '" & QuoteSanitise(cmbField4.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield1} LIKE '" & WildcardChange(cmbField4.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield2 LIKE '" & QuoteSanitise(cmbField4.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield2} LIKE '" & WildcardChange(cmbField4.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield3 LIKE '" & QuoteSanitise(cmbField4.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield3} LIKE '" & WildcardChange(cmbField4.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield4 LIKE '" & QuoteSanitise(cmbField4.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield4} LIKE '" & WildcardChange(cmbField4.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield5 LIKE '" & QuoteSanitise(cmbField4.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield5} LIKE '" & WildcardChange(cmbField4.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield6 LIKE '" & QuoteSanitise(cmbField4.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield6} LIKE '" & WildcardChange(cmbField4.Text, "%", "*") & "*') "
        Else
            m_strSearchConditions = m_strSearchConditions & " AND (customfield4 LIKE '" & QuoteSanitise(cmbField4.Text) & "%') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield4} LIKE '" & WildcardChange(cmbField4.Text, "%", "*") & "*') "
        End If
    End If
    If cmbField5.Text <> "" Then
        l_blnflag = True
        If chkCustomFieldInclusive.Value <> 0 Then
            m_strSearchConditions = m_strSearchConditions & " AND (customfield1 LIKE '" & QuoteSanitise(cmbField5.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield1} LIKE '" & WildcardChange(cmbField5.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield2 LIKE '" & QuoteSanitise(cmbField5.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield2} LIKE '" & WildcardChange(cmbField5.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield3 LIKE '" & QuoteSanitise(cmbField5.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield3} LIKE '" & WildcardChange(cmbField5.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield4 LIKE '" & QuoteSanitise(cmbField5.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield4} LIKE '" & WildcardChange(cmbField5.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield5 LIKE '" & QuoteSanitise(cmbField5.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield5} LIKE '" & WildcardChange(cmbField5.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield6 LIKE '" & QuoteSanitise(cmbField5.Text) & "%') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield6} LIKE '" & WildcardChange(cmbField5.Text, "%", "*") & "*') "
        Else
            m_strSearchConditions = m_strSearchConditions & " AND (customfield5 LIKE '" & QuoteSanitise(cmbField5.Text) & "%') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield5} LIKE '" & WildcardChange(cmbField5.Text, "%", "*") & "*') "
        End If
    End If
    If cmbField6.Text <> "" Then
        l_blnflag = True
        If chkCustomFieldInclusive.Value <> 0 Then
            m_strSearchConditions = m_strSearchConditions & " AND (customfield1 LIKE '" & QuoteSanitise(cmbField6.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield1} LIKE '" & WildcardChange(cmbField6.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield2 LIKE '" & QuoteSanitise(cmbField6.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield2} LIKE '" & WildcardChange(cmbField6.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield3 LIKE '" & QuoteSanitise(cmbField6.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield3} LIKE '" & WildcardChange(cmbField6.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield4 LIKE '" & QuoteSanitise(cmbField6.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield4} LIKE '" & WildcardChange(cmbField6.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield5 LIKE '" & QuoteSanitise(cmbField6.Text) & "%' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield5} LIKE '" & WildcardChange(cmbField6.Text, "%", "*") & "*' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield6 LIKE '" & QuoteSanitise(cmbField6.Text) & "%') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield6} LIKE '" & WildcardChange(cmbField6.Text, "%", "*") & "*') "
        Else
            m_strSearchConditions = m_strSearchConditions & " AND (customfield6 LIKE '" & QuoteSanitise(cmbField6.Text) & "%') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield6} LIKE '" & WildcardChange(cmbField6.Text, "%", "*") & "*') "
        End If
    End If
Else
    If cmbField1.Text <> "" Then
        l_blnflag = True
        If chkCustomFieldInclusive.Value <> 0 Then
            m_strSearchConditions = m_strSearchConditions & " AND (customfield1 = '" & QuoteSanitise(cmbField1.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield1} = '" & cmbField1.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield2 = '" & QuoteSanitise(cmbField1.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield2} = '" & cmbField1.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield3 = '" & QuoteSanitise(cmbField1.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield3} = '" & cmbField1.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield4 = '" & QuoteSanitise(cmbField1.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield4} = '" & cmbField1.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield5 = '" & QuoteSanitise(cmbField1.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield5} = '" & cmbField1.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield6 = '" & QuoteSanitise(cmbField1.Text) & "') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield6} = '" & cmbField1.Text & "') "
        Else
            m_strSearchConditions = m_strSearchConditions & " AND (customfield1 = '" & QuoteSanitise(cmbField1.Text) & "') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield1} = '" & cmbField1.Text & "') "
        End If
    End If
    If cmbField2.Text <> "" Then
        l_blnflag = True
        If chkCustomFieldInclusive.Value <> 0 Then
            m_strSearchConditions = m_strSearchConditions & " AND (customfield1 = '" & QuoteSanitise(cmbField2.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield1} = '" & cmbField2.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield2 = '" & QuoteSanitise(cmbField2.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield2} = '" & cmbField2.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield3 = '" & QuoteSanitise(cmbField2.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield3} = '" & cmbField2.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield4 = '" & QuoteSanitise(cmbField2.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield4} = '" & cmbField2.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield5 = '" & QuoteSanitise(cmbField2.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield5} = '" & cmbField2.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield6 = '" & QuoteSanitise(cmbField2.Text) & "') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield6} = '" & cmbField2.Text & "') "
        Else
            m_strSearchConditions = m_strSearchConditions & " AND (customfield2 = '" & QuoteSanitise(cmbField2.Text) & "') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield2} = '" & cmbField2.Text & "') "
        End If
    End If
    If cmbField3.Text <> "" Then
        l_blnflag = True
        If chkCustomFieldInclusive.Value <> 0 Then
            m_strSearchConditions = m_strSearchConditions & " AND (customfield1 = '" & QuoteSanitise(cmbField3.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield1} = '" & cmbField3.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield2 = '" & QuoteSanitise(cmbField3.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield2} = '" & cmbField3.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield3 = '" & QuoteSanitise(cmbField3.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield3} = '" & cmbField3.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield4 = '" & QuoteSanitise(cmbField3.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield4} = '" & cmbField3.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield5 = '" & QuoteSanitise(cmbField3.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield5} = '" & cmbField3.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield6 = '" & QuoteSanitise(cmbField3.Text) & "') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield6} = '" & cmbField3.Text & "') "
        Else
            m_strSearchConditions = m_strSearchConditions & " AND (customfield3 = '" & QuoteSanitise(cmbField3.Text) & "') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield3} = '" & cmbField3.Text & "') "
        End If
    End If
    If cmbField4.Text <> "" Then
        l_blnflag = True
        If chkCustomFieldInclusive.Value <> 0 Then
            m_strSearchConditions = m_strSearchConditions & " AND (customfield1 = '" & QuoteSanitise(cmbField4.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield1} = '" & cmbField4.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield2 = '" & QuoteSanitise(cmbField4.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield2} = '" & cmbField4.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield3 = '" & QuoteSanitise(cmbField4.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield3} = '" & cmbField4.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield4 = '" & QuoteSanitise(cmbField4.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield4} = '" & cmbField4.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield5 = '" & QuoteSanitise(cmbField4.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield5} = '" & cmbField4.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield6 = '" & QuoteSanitise(cmbField4.Text) & "') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield6} = '" & cmbField4.Text & "') "
        Else
            m_strSearchConditions = m_strSearchConditions & " AND (customfield4 = '" & QuoteSanitise(cmbField4.Text) & "') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.customfield4} = '" & cmbField4.Text & "') "
        End If
    End If
    If cmbField5.Text <> "" Then
        l_blnflag = True
        If chkCustomFieldInclusive.Value <> 0 Then
            m_strSearchConditions = m_strSearchConditions & " AND (customfield1 = '" & QuoteSanitise(cmbField5.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield1} = '" & cmbField5.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield2 = '" & QuoteSanitise(cmbField5.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield2} = '" & cmbField5.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield3 = '" & QuoteSanitise(cmbField5.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield3} = '" & cmbField5.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield4 = '" & QuoteSanitise(cmbField5.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield4} = '" & cmbField5.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield5 = '" & QuoteSanitise(cmbField5.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield5} = '" & cmbField5.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield6 = '" & QuoteSanitise(cmbField5.Text) & "') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield6} = '" & cmbField5.Text & "') "
        Else
            m_strSearchConditions = m_strSearchConditions & " AND (customfield5 = '" & QuoteSanitise(cmbField5.Text) & "') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.customfield5} = '" & cmbField5.Text & "') "
        End If
    End If
    If cmbField6.Text <> "" Then
        l_blnflag = True
        If chkCustomFieldInclusive.Value <> 0 Then
            m_strSearchConditions = m_strSearchConditions & " AND (customfield1 = '" & QuoteSanitise(cmbField6.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.customfield1} = '" & cmbField6.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield2 = '" & QuoteSanitise(cmbField6.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield2} = '" & cmbField6.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield3 = '" & QuoteSanitise(cmbField6.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield3} = '" & cmbField6.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield4 = '" & QuoteSanitise(cmbField6.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield4} = '" & cmbField6.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield5 = '" & QuoteSanitise(cmbField6.Text) & "' "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield5} = '" & cmbField6.Text & "' "
            m_strSearchConditions = m_strSearchConditions & " OR customfield6 = '" & QuoteSanitise(cmbField6.Text) & "') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "OR {events.customfield6} = '" & cmbField6.Text & "') "
        Else
            m_strSearchConditions = m_strSearchConditions & " AND (customfield6 = '" & QuoteSanitise(cmbField6.Text) & "') "
            m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.customfield6} = '" & cmbField6.Text & "') "
        End If
    End If
End If

If txtTitle.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND eventtitle LIKE '" & QuoteSanitise(txtTitle.Text) & "%' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.eventtitle} LIKE '" & WildcardChange(txtTitle.Text, "%", "*") & "*' "
ElseIf chkTitleNull.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND (eventtitle ='' OR eventtitle IS NULL) "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.eventtitle} = '' OR Isnull({events.eventtitle})) "
End If
If txtReference.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND clipreference LIKE '" & QuoteSanitise(txtReference.Text) & "%' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.clipreference} LIKE '" & WildcardChange(txtReference.Text, "%", "*") & "*' "
ElseIf chkRefNull.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND (clipreference = '' OR clipreference IS NULL) "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.clipreference} = '' OR Isnull({events.clipreference})) "
End If
If txtAltFolder.Text <> "" Then
    m_strSearchConditions = m_strSearchConditions & " AND altlocation LIKE '" & QuoteSanitise(txtAltFolder.Text) & "' "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND {events.altlocation} LIKE '" & WildcardChange(txtAltFolder.Text, "%", "*") & "' "
ElseIf chkLocIsNull.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND (altlocation = '' OR altlocation IS NULL) "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.altlocation} = '' OR Isnull({events.altlocation})) "
End If
If chkKeyframeNull.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND (eventkeyframefilename IS NULL OR eventkeyframefilename = '') "
    l_blnflag = True
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.eventkeyframefilename} = '' OR Isnull({events.eventkeyframefilename})) "
End If

If cmbKeyword.Text <> "" Then
    l_blnflag = True
    'First search through all the actual keyword entries for any that match the typed string, and add all the clips found to the search
    Dim l_keySQL As String, l_rstKeys As ADODB.Recordset, l_strKeySearchCriteria As String, l_strCrystalKeySearchCriteria As String
    l_keySQL = "SELECT * FROM eventkeyword WHERE (keywordtext LIKE '" & cmbKeyword.Text & "%') OR (keywordtext LIKE '%" & cmbKeyword.Text & "%');"
    Set l_rstKeys = ExecuteSQL(l_keySQL, g_strExecuteError)
    CheckForSQLError
    l_strKeySearchCriteria = " AND (events.eventID = '0'"
    l_strCrystalKeySearchCriteria = "AND ({events.eventID} = 0 "
    If Not l_rstKeys.EOF Then
        l_rstKeys.MoveFirst
        Do While Not l_rstKeys.EOF
            If chkSearchArchive.Value <> 0 Then
                l_strKeySearchCriteria = l_strKeySearchCriteria & " OR eventsarchive.clipreference = '" & l_rstKeys("clipreference") & "' "
            Else
                l_strKeySearchCriteria = l_strKeySearchCriteria & " OR clipreference = '" & l_rstKeys("clipreference") & "' "
                l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & " OR {events.clipreference} = """ & l_rstKeys("clipreference") & """ "
            End If
            l_rstKeys.MoveNext
        Loop
    End If
    l_rstKeys.Close
    Set l_rstKeys = Nothing
        
    'Then go through and search all the vaguely applicable fields for the types text too.
    
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR clipformat LIKE '" & cmbKeyword.Text & "%' OR clipformat LIKE '%" & cmbKeyword.Text & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR eventtitle LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR eventtitle LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR eventsubtitle LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR eventsubtitle LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR eventversion LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR eventversion LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR eventseries LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR eventseries LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR eventepisode LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR eventepisode LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR eventset LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR eventset LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR altlocation = '" & QuoteSanitise(cmbKeyword.Text) & "' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR clipfilename LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR clipfilename LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR clocknumber LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR clocknumber LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR eventtype LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR eventtype LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR clipgenre LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR clipgenre LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR notes LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR notes LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR clipreference LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR clipreference LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR customfield1 LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR customfield1 LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR customfield2 LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR customfield2 LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR customfield3 LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR customfield3 LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR customfield4 LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR customfield4 LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR customfield5 LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR customfield5 LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & " OR customfield6 LIKE '" & QuoteSanitise(cmbKeyword.Text) & "%' OR customfield6 LIKE '%" & QuoteSanitise(cmbKeyword.Text) & "%' "
    l_strKeySearchCriteria = l_strKeySearchCriteria & ") "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.clipformat} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.clipformat} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.eventtitle} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.eventtitle} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.eventsubtitle} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.eventsubtitle} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.eventversion} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.eventversion LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.eventseries} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.eventseries} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.eventepisode} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.eventepisode} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.eventset} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.eventset} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.altlocation} = '" & cmbKeyword.Text & "' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.clipfilename} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.clipfilename} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.clocknumber} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.clocknumber} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.eventtype} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.eventtype} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.clipgenre} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.clipgenre} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.notes} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.notes} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.clipreference} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.clipreference} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.customfield1} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.customfield1} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.customfield2} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.customfield2} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.customfield3} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.customfield3} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.customfield4} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.customfield4} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.customfield5} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.customfield5} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & "OR {events.customfield6} LIKE '" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' OR {events.customfield6} LIKE '*" & WildcardChange(cmbKeyword.Text, "%", "*") & "*' "
    l_strCrystalKeySearchCriteria = l_strCrystalKeySearchCriteria & ") "
    
    m_strSearchConditions = m_strSearchConditions & l_strKeySearchCriteria
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & l_strCrystalKeySearchCriteria
    
End If

If chkUnverifiedMedia.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND (bigfilesize is null)"
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & " AND Isnull({events.bigfilesize}) "
    If cmbBarcode.Text = "" Then optStorageType(0).Value = True
    l_blnflag = True
End If

If chkNotAtLatimer.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND eventID NOT IN (Select distinct DISCSTORE.eventID as [Disk Event]" & _
    "FROM (Select eventID, companyID, clipfilename, bigfilesize, md5checksum from vw_Events_on_DISCSTORE where vw_Events_on_DISCSTORE.system_deleted = 0) " & _
    "As DISCSTORE " & _
    "JOIN vw_Events_at_Latimer on vw_Events_at_Latimer.system_deleted = 0 " & _
    "AND vw_Events_at_Latimer.companyID = DISCSTORE.companyID " & _
    "AND vw_Events_at_Latimer.md5checksum = DISCSTORE.md5checksum " & _
    "Union " & _
    "Select distinct DIVASTORE.eventID as [Disk Event] " & _
    "from (Select eventID, companyID, clipfilename, bigfilesize, md5checksum from vw_Events_on_DIVA where vw_Events_on_DIVA.system_deleted = 0) " & _
    "As DIVASTORE " & _
    "Join vw_Events_at_Latimer on vw_Events_at_Latimer.system_deleted = 0 " & _
    "AND vw_Events_at_Latimer.companyID = DIVASTORE.companyID " & _
    "AND vw_Events_at_Latimer.md5checksum = DIVASTORE.md5checksum) "
ElseIf chkAlsoAtLatimer.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND eventID IN (Select distinct DISCSTORE.eventID as [Disk Event]" & _
    "FROM (Select eventID, companyID, clipfilename, bigfilesize, md5checksum from vw_Events_on_DISCSTORE where vw_Events_on_DISCSTORE.system_deleted = 0) " & _
    "As DISCSTORE " & _
    "JOIN vw_Events_at_Latimer on vw_Events_at_Latimer.system_deleted = 0 " & _
    "AND vw_Events_at_Latimer.companyID = DISCSTORE.companyID " & _
    "AND vw_Events_at_Latimer.md5checksum = DISCSTORE.md5checksum " & _
    "Union " & _
    "Select distinct DIVASTORE.eventID as [Disk Event] " & _
    "from (Select eventID, companyID, clipfilename, bigfilesize, md5checksum from vw_Events_on_DIVA where vw_Events_on_DIVA.system_deleted = 0) " & _
    "As DIVASTORE " & _
    "Join vw_Events_at_Latimer on vw_Events_at_Latimer.system_deleted = 0 " & _
    "AND vw_Events_at_Latimer.companyID = DIVASTORE.companyID " & _
    "AND vw_Events_at_Latimer.md5checksum = DIVASTORE.md5checksum) "
End If

'If chkAlsoOnDIVA.Value <> 0 Then
'    m_strSearchConditions = m_strSearchConditions & "AND (eventid in (Select distinct DISCSTORE.eventID as [Disk Event] " & _
'    "from (Select eventID, companyID, clipfilename, bigfilesize from vw_Events_on_DISCSTORE where vw_Events_on_DISCSTORE.system_deleted = 0" & _
'    IIf(cmbCompany.Text <> "", " AND companyID = " & cmbCompany.Columns("companyID").Text, "") & _
'    IIf(lblLibraryID.Caption <> "", " AND libraryID = " & lblLibraryID.Caption, "") & _
'    ") As DISCSTORE Join vw_Events_on_DIVA on vw_Events_on_DIVA.system_deleted = 0 " & _
'    "AND vw_Events_on_DIVA.companyID = DISCSTORE.companyID AND vw_Events_on_DIVA.clipfilename = DISCSTORE.clipfilename AND vw_Events_on_DIVA.bigfilesize = DISCSTORE.bigfilesize " & _
'    "AND vw_Events_on_DIVA.bigfilesize IS NOT NULL AND vw_Events_on_DIVA.DIVA_ID IS NOT NULL))"
'ElseIf chkNotOnDIVA.Value <> 0 Then
'    m_strSearchConditions = m_strSearchConditions & "AND (eventid not in (Select distinct DISCSTORE.eventID as [Disk Event] " & _
'    "from (Select eventID, companyID, clipfilename, bigfilesize from vw_Events_on_DISCSTORE where vw_Events_on_DISCSTORE.system_deleted = 0" & _
'    IIf(cmbCompany.Text <> "", " AND companyID = " & cmbCompany.Columns("companyID").Text, "") & _
'    IIf(lblLibraryID.Caption <> "", " AND libraryID = " & lblLibraryID.Caption, "") & _
'    ") As DISCSTORE Join vw_Events_on_DIVA on vw_Events_on_DIVA.system_deleted = 0 " & _
'    "AND vw_Events_on_DIVA.companyID = DISCSTORE.companyID AND vw_Events_on_DIVA.clipfilename = DISCSTORE.clipfilename AND vw_Events_on_DIVA.bigfilesize = DISCSTORE.bigfilesize " & _
'    "AND vw_Events_on_DIVA.bigfilesize IS NOT NULL AND vw_Events_on_DIVA.DIVA_ID IS NOT NULL))"
'ElseIf chkAlsoOnUSBDisk.Value <> 0 Then
If chkAlsoOnUSBDisk.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & "AND (eventid in (Select distinct DISCSTORE.eventID as [Disk Event] " & _
    "from (Select eventID, companyID, clipfilename, bigfilesize from vw_Events_on_DISCSTORE where vw_Events_on_DISCSTORE.system_deleted = 0" & _
    IIf(cmbCompany.Text <> "", " AND companyID = " & cmbCompany.Columns("companyID").Text, "") & _
    IIf(lblLibraryID.Caption <> "", " AND libraryID = " & lblLibraryID.Caption, "") & _
    ") As DISCSTORE Join vw_Events_on_MOBILEDISC on vw_Events_on_MOBILEDISC.system_deleted = 0 " & _
    "AND vw_Events_on_MOBILEDISC.companyID = DISCSTORE.companyID AND vw_Events_on_MOBILEDISC.clipfilename = DISCSTORE.clipfilename AND vw_Events_on_MOBILEDISC.bigfilesize = DISCSTORE.bigfilesize " & _
    "AND vw_Events_on_MOBILEDISC.bigfilesize IS NOT NULL))"
ElseIf chkNotOnUSBDisk.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & "AND (eventid not in (Select distinct DISCSTORE.eventID as [Disk Event] " & _
    "from (Select eventID, companyID, clipfilename, bigfilesize from vw_Events_on_DISCSTORE where vw_Events_on_DISCSTORE.system_deleted = 0" & _
    IIf(cmbCompany.Text <> "", " AND companyID = " & cmbCompany.Columns("companyID").Text, "") & _
    IIf(lblLibraryID.Caption <> "", " AND libraryID = " & lblLibraryID.Caption, "") & _
    ") As DISCSTORE Join vw_Events_on_MOBILEDISC on vw_Events_on_MOBILEDISC.system_deleted = 0 " & _
    "AND vw_Events_on_MOBILEDISC.companyID = DISCSTORE.companyID AND vw_Events_on_MOBILEDISC.clipfilename = DISCSTORE.clipfilename AND vw_Events_on_MOBILEDISC.bigfilesize = DISCSTORE.bigfilesize " & _
    "AND vw_Events_on_MOBILEDISC.bigfilesize IS NOT NULL))"
ElseIf chkAlsoOnLTO.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & "AND (eventid in (Select distinct DISCSTORE.eventID as [Disk Event] " & _
    "from (Select eventID, companyID, clipfilename, bigfilesize from vw_Events_on_DISCSTORE where vw_Events_on_DISCSTORE.system_deleted = 0" & _
    IIf(cmbCompany.Text <> "", " AND companyID = " & cmbCompany.Columns("companyID").Text, "") & _
    IIf(lblLibraryID.Caption <> "", " AND libraryID = " & lblLibraryID.Caption, "") & _
    ") As DISCSTORE Join vw_Events_on_LTO on vw_Events_on_LTO.system_deleted = 0 " & _
    "AND vw_Events_on_LTO.companyID = DISCSTORE.companyID AND vw_Events_on_LTO.clipfilename = DISCSTORE.clipfilename AND vw_Events_on_LTO.bigfilesize = DISCSTORE.bigfilesize " & _
    "AND vw_Events_on_LTO.bigfilesize IS NOT NULL))"
ElseIf chkNotOnLTO.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & "AND (eventid not in (Select distinct DISCSTORE.eventID as [Disk Event] " & _
    "from (Select eventID, companyID, clipfilename, bigfilesize from vw_Events_on_DISCSTORE where vw_Events_on_DISCSTORE.system_deleted = 0" & _
    IIf(cmbCompany.Text <> "", " AND companyID = " & cmbCompany.Columns("companyID").Text, "") & _
    IIf(lblLibraryID.Caption <> "", " AND libraryID = " & lblLibraryID.Caption, "") & _
    ") As DISCSTORE Join vw_Events_on_LTO on vw_Events_on_LTO.system_deleted = 0 " & _
    "AND vw_Events_on_LTO.companyID = DISCSTORE.companyID AND vw_Events_on_LTO.clipfilename = DISCSTORE.clipfilename AND vw_Events_on_LTO.bigfilesize = DISCSTORE.bigfilesize " & _
    "AND vw_Events_on_LTO.bigfilesize IS NOT NULL))"
End If

If optStorageType(0).Value = True Then 'DISCSTORE
    m_strSearchConditions = m_strSearchConditions & " AND (online <> 0) "
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.online} <> 0) "
Else
    If optStorageType(1).Value = True Then 'MOBILEDISC
        m_strSearchConditions = m_strSearchConditions & " AND (online = 0 AND libraryID IN (SELECT libraryID FROM library WHERE format = 'MOBILEDISC')) "
        m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.online} = 0 AND {library.format} = 'MOBILEDISC') "
    ElseIf optStorageType(3).Value = True Then 'LTO5
        m_strSearchConditions = m_strSearchConditions & " AND (online = 0 AND libraryID IN (SELECT libraryID FROM library WHERE format = 'LTO5')) "
        m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.online} = 0 AND {library.format} = 'LTO5') "
    ElseIf optStorageType(2).Value = True Then 'LTO6
        m_strSearchConditions = m_strSearchConditions & " AND (online = 0 AND libraryID IN (SELECT libraryID FROM library WHERE format = 'LTO6')) "
        m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.online} = 0 AND {library.format} = 'LTO6') "
    ElseIf optStorageType(6).Value = True Then 'LTO7
        m_strSearchConditions = m_strSearchConditions & " AND (online = 0 AND libraryID IN (SELECT libraryID FROM library WHERE format = 'LTO7')) "
        m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.online} = 0 AND {library.format} = 'LTO7') "
    ElseIf optStorageType(4).Value = True Then 'DIVA
        m_strSearchConditions = m_strSearchConditions & " AND (online = 0 AND libraryID IN (SELECT libraryID FROM library WHERE format = 'DIVA')) "
        m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.online} = 0 AND {library.format} = 'DIVA') "
    ElseIf optStorageType(5).Value = True Then 'Other
        m_strSearchConditions = m_strSearchConditions & " AND (online = 0 AND NOT (libraryID IN (SELECT libraryID FROM library WHERE format = 'LTO5' OR format = 'LTO6' OR format = 'LTO7' OR format = 'MOBILEDISC' OR format = 'DIVA'))) "
        m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND ({events.online} = 0 AND {library.format} <> 'MOBILEDISC' AND {library.format} <> 'LTO5'AND {library.format} <> 'LTO6' AND {library.format} <> 'LTO7' AND {library.format} <> 'DIVA') "
    End If
End If

If chkNoMD5.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND (MD5checksum IS NULL OR md5checksum = '') "
    m_strCrystalSearchConditions = m_strCrystalSearchConditions & "AND (IsNull({events.MD5checksum}) OR {events.md5checksum} = '') "
End If

'Direct SQL Query added to the end of all the other search criteria
If txtSQLQuery.Text <> "" Then
    If InStr(txtSQLQuery.Text, "library.") Then
        MsgBox "Cannot include library table in SQL search criteria - SQL query not included.", vbInformation, "Problem..."
    Else
        m_strSearchConditions = m_strSearchConditions & " AND " & txtSQLQuery.Text: l_blnflag = True
    End If
End If

If chkUnassignedMasters.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND (events.eventID IN (SELECT eventID FROM vw_File_Masters_Unassigned))"
    l_blnflag = True
End If

If chkAssignedAndClearedMasters.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND (events.eventID IN (SELECT eventID FROM vw_File_Masters_Assigned_and_Cleared))"
    l_blnflag = True
End If

If chkAssignedButNotClearedMasters.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND (events.eventID IN (SELECT eventID FROM vw_File_Masters_Assigned_but_not_Cleared))"
    l_blnflag = True
End If

If chkAssignedAndClearedProducts.Value <> 0 Then
    m_strSearchConditions = m_strSearchConditions & " AND (events.eventID IN (SELECT eventID FROM vw_File_Products_Assigned_and_Cleared))"
    l_blnflag = True
End If

If Not IsNull(datLastUsageBefore.Value) Then
    m_strSearchConditions = m_strSearchConditions & " AND (events.eventID NOT IN (SELECT eventusage.eventID FROM eventusage WHERE dateused >= '" & FormatSQLDate(datLastUsageBefore.Value) & "')) ": l_blnflag = True
End If

If l_blnflag = False Then
    If MsgBox("Are You Sure?", vbYesNo, "Empty Search requested....") = vbNo Then Exit Sub
End If

l_strSQL = l_strSQL & m_strSearchConditions

l_strSQL = l_strSQL & " ORDER BY libraryID, altlocation, clipfilename;"

Debug.Print l_strSQL

Dim l_conTest As ADODB.Connection
Set l_conTest = New ADODB.Connection
l_conTest.Open g_strConnection

If chkLongSearch.Value = 0 Then
    
    Set l_rstTest = ExecuteSQL(l_strSQL, g_strExecuteError)
    
Else

    l_conTest.CommandTimeout = 60
    
    Set l_rstTest = New ADODB.Recordset
    
    On Error GoTo TESTFAIL
    l_rstTest.Open l_strSQL, l_conTest, 3, 3
    
RESUMESEARCH:
    On Error GoTo 0

End If

If InStr(UCase(g_strExecuteError), "ERROR") = 0 Then
    adoClip.RecordSource = "SELECT events.*, (SELECT CASE WHEN md5checksum IS NULL OR md5checksum = '' THEN 0 ELSE 1 END AS Expr1) AS MD5present, (SELECT CASE WHEN sha1checksum IS NULL OR sha1checksum = '' THEN 0 ELSE 1 END AS Expr2) AS SHA1present FROM events WHERE 1 = 0 "
    adoClip.ConnectionString = g_strConnection
    adoClip.Refresh
    adoClip.RecordSource = l_strSQL
    If chkLongSearch.Value <> 0 Then adoClip.CommandTimeout = 60
    On Error GoTo REDOSEARCH
REDOSEARCH:
    adoClip.Refresh
    DoEvents
    adoClip.Caption = grdClips.Rows & " Clips Found"
    DoEvents
    If Val(txtSelectTop.Text) <> 0 Then
        l_curTotalSize = 0
        If Not l_rstTest.EOF Then
            l_rstTest.MoveFirst
            Do While Not l_rstTest.EOF
                If Not IsNull(l_rstTest("bigfilesize")) Then l_curTotalSize = l_curTotalSize + l_rstTest("bigfilesize")
                l_rstTest.MoveNext
            Loop
        End If
        l_rstTest.Close
        If l_curTotalSize <> 0 Then
            lblTotalFileSize.Caption = Format(Int((l_curTotalSize / 1024) + 0.9999), "#,##0") & " KB    " & Format(Int((l_curTotalSize / 1024 / 1024) + 0.999999), "#,##0") & " MB    " & Format(Int((l_curTotalSize / 1024 / 1024 / 1024) + 0.99999999), "#,##0") & " GB    " & Format(Int((l_curTotalSize / 1024 / 1024 / 1024 / 1024) + 0.999999999), "#,##0") & " TB"
        Else
            lblTotalFileSize.Caption = ""
        End If
    Else
        On Error Resume Next
        l_rstTest.Close
        On Error GoTo 0
        l_strSQL = "SELECT Sum(bigfilesize) FROM events WHERE 1=1 " & m_strSearchConditions
        Debug.Print l_strSQL
        l_rstTest.Open l_strSQL, l_conTest, 3, 3
        l_curTotalSize = 0
        If Not l_rstTest.EOF Then
            If Not IsNull(l_rstTest(0)) Then l_strTotalSize = l_rstTest(0)
        End If
        l_rstTest.Close
        On Error Resume Next
        If l_strTotalSize <> "" Then
            lblTotalFileSize.Caption = Format(Int((l_strTotalSize / 1024) + 0.9999), "#,##0") & " KB    " & Format(Int((l_strTotalSize / 1024 / 1024) + 0.999999), "#,##0") & " MB    " & Format(Int((l_strTotalSize / 1024 / 1024 / 1024) + 0.99999999), "#,##0") & " GB    " & Format(Int((l_strTotalSize / 1024 / 1024 / 1024 / 1024) + 0.999999999), "#,##0") & " TB"
        Else
            lblTotalFileSize.Caption = ""
        End If
        On Error GoTo 0
    End If
    On Error GoTo 0
    DoEvents
Else
    DoEvents
    MsgBox "The Search as specified is invalid - please correct." & vbCrLf & g_strExecuteError, vbCritical, "Error..."
End If

Set l_rstTest = Nothing
l_conTest.Close
Set l_conTest = Nothing

Screen.MousePointer = vbDefault
TabMediaSearching.Tab = 0

Exit Sub

TESTFAIL:

g_strExecuteError = "ExecuteSQL *ERROR* " & Err.Description
GoTo RESUMESEARCH

End Sub

Private Sub cmdPrint_Click()

Dim l_strSelectionFormula As String
Dim l_strReportFile As String

If Val(lblLibraryID.Caption) = 0 Then
    NoLibraryItemSelectedMessage
    Exit Sub
End If

If Not CheckAccess("/printrecordreport") Then Exit Sub

If m_strCrystalSearchConditions <> "" Then
    l_strSelectionFormula = m_strCrystalSearchConditions
Else
    l_strSelectionFormula = "{events.libraryID} = " & lblLibraryID.Caption & " AND {events.system_deleted} = 0"
End If
If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/mediacontentDADC") > 0 Then
    l_strReportFile = g_strLocationOfCrystalReportFiles & "MediaContentDADC.rpt"
Else
    l_strReportFile = g_strLocationOfCrystalReportFiles & "MediaContent.rpt"
End If
PrintCrystalReport l_strReportFile, l_strSelectionFormula, g_blnPreviewReport

End Sub

Private Sub cmdSendGridViaAspera_Click()

'Has to be some records int he grid
If adoClip.Recordset.RecordCount <= 0 Then Exit Sub

'Has to be a companyID
If Val(lblCompanyID.Caption) = 0 Then
    MsgBox "A Single client must be selected for File Sends.", vbCritical
    Exit Sub
End If

'Has to be from either a DISCSTORE or the DIVA
If (optStorageType(0).Value = False And lblFormat.Caption <> "DISCSTORE") And (optStorageType(4).Value = False And lblFormat.Caption <> "DIVA") Then
    MsgBox "We can only Delivery items that are on a DISCSTORE or the DIVA at this time.", vbCritical
    Exit Sub
End If

'Give the fitst 'Are You Sure?'
If MsgBox("You are about to send these files to an Aspera Recipient." & vbCrLf & "Are you Sure", vbYesNo) = vbNo Then Exit Sub

Dim l_strDestinationID As String, l_strDestinationDropfolder As String, l_lngDestinationLibraryID As Long, l_strSQL As String, Count As Long, l_strDestinationSubfolder As String, l_strDestinationSecondaryFolder As String
Dim l_lngCount As Long, bkmrk As Variant, l_rstTemp As ADODB.Recordset, l_strNewRestorePath As String, l_lngNewEventID As Long, l_strTempDestinationFolder As String, l_strTempRestorePath As String

frmGetDestination.lblDestinationType.Caption = "Aspera1"
frmGetDestination.Show vbModal
l_strDestinationID = frmGetDestination.cmbDestination.Columns("DestinationName").Text
Unload frmGetDestination

'If a destination was chosen...
If l_strDestinationID <> "" Then
    If MsgBox("You are about to send these files to " & l_strDestinationID & "." & vbCrLf & "Are you Sure", vbYesNo) = vbNo Then Exit Sub
    l_strDestinationDropfolder = GetDataSQL("SELECT TOP 1 DestinationDropfolder FROM DigitalDestination WHERE DestinationType = 'Aspera1' and DestinationName = '" & l_strDestinationID & "'")
    If Trim(" " & GetDataSQL("SELECT TOP 1 DestinationSubfolder FROM DigitalDestination WHERE DestinationType = 'Aspera1' and DestinationName = '" & l_strDestinationID & "'")) <> "" Then
        l_strDestinationSubfolder = GetDataSQL("SELECT TOP 1 DestinationSubfolder FROM DigitalDestination WHERE DestinationType = 'Aspera1' and DestinationName = '" & l_strDestinationID & "'")
        
        Select Case True
        
            Case InStr(l_strDestinationSubfolder, "{Date}") > 0
                l_strDestinationDropfolder = l_strDestinationDropfolder & "\" & Format(Now, "YYYY-MM-DD")
            Case InStr(l_strDestinationSubfolder, "{DateTime}") > 0
                l_strDestinationDropfolder = l_strDestinationDropfolder & "\" & Format(Now, "YYYY-MM-DD") & "_" & Format(Now, "HH-NN-SS")
            Case InStr(l_strDestinationSubfolder, "{") <= 0
                l_strDestinationDropfolder = l_strDestinationDropfolder & "\" & l_strDestinationSubfolder
            Case Else
                'Do Nothing - we don't process that key here
        End Select
        
    End If
    
    If Trim(" " & GetDataSQL("SELECT TOP 1 DestinationSecondaryFolder FROM DigitalDestination WHERE DestinationType = 'Aspera1' and DestinationName = '" & l_strDestinationID & "'")) <> "" Then
        l_strDestinationSecondaryFolder = GetDataSQL("SELECT TOP 1 DestinationSecondaryFolder FROM DigitalDestination WHERE DestinationType = 'Aspera1' and DestinationName = '" & l_strDestinationID & "'")
    End If
    
    l_lngDestinationLibraryID = GetData("library", "libraryID", "barcode", g_strAsperaClient1SendPath)
    l_strNewRestorePath = GetData("library", "version", "libraryID", l_lngDestinationLibraryID) & "/" & SlashBackToForward(l_strDestinationDropfolder)
    
    If MsgBox("Just send selected files (instead of whole grid)", vbYesNo + vbDefaultButton2, "Sending Grid") = vbYes Then
        
        l_lngCount = grdClips.SelBookmarks.Count
        
        If l_lngCount <= 0 Then
            MsgBox "No rows selected.", vbCritical, "Error with Send via Aspera"
            Exit Sub
        End If
        
        If MsgBox("Send " & l_lngCount & " items - Are you sure", vbYesNo + vbDefaultButton2, "Sending Grid") = vbNo Then Exit Sub
        
        ProgressBar1.Max = grdClips.SelBookmarks.Count
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        Count = 1
        
        If (optStorageType(0).Value = True Or lblFormat.Caption = "DISCSTORE") Then
            For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1
                bkmrk = grdClips.SelBookmarks(l_lngCount)
                Set l_rstTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & Val(grdClips.Columns("eventID").CellText(bkmrk)) & ";", g_strExecuteError)
                CheckForSQLError
                
                ProgressBar1.Value = Count
                DoEvents
                'Invoke a copy to the hotfolder location
                l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
                l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                l_strSQL = l_strSQL & l_rstTemp("LibraryID") & ", "
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
                l_strSQL = l_strSQL & l_lngDestinationLibraryID & ", "
                l_strSQL = l_strSQL & "'"
                l_strSQL = l_strSQL & l_strDestinationDropfolder
                If InStr(l_strDestinationSubfolder, "{clipreference}") > 0 Then
                    l_strSQL = l_strSQL & "\" & QuoteSanitise(l_rstTemp("clipreference"))
                End If
                If InStr(l_strDestinationSecondaryFolder, "{movielabs_key}") > 0 And Right(l_rstTemp("clipfilename"), 4) <> ".xml" Then
                    l_strSQL = l_strSQL & "\resources"
                End If
                l_strSQL = l_strSQL & "', "
                l_strSQL = l_strSQL & "1, "
                l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                
                l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, finalstatus) VALUES ("
                l_strSQL = l_strSQL & "2753, "
                l_strSQL = l_strSQL & l_rstTemp("companyID") & ", "
                l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                l_strSQL = l_strSQL & "'Aspera to " & l_strDestinationID & "', "
                l_strSQL = l_strSQL & "'Completed')"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                Count = Count + 1
            Next
        ElseIf (optStorageType(4).Value = True Or lblFormat.Caption = "DIVA") Then
            MsgBox "Direct Restores into Aspera Hotfolders has become unreliable." & vbCrLf & "Please Restore to a DISCSTORE somewhere and then deliver the items from there." & vbCrLf & "We will re-enable this feature as soon as we can.", vbCritical
            Exit Sub
'            For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1
'                bkmrk = grdClips.SelBookmarks(l_lngCount)
'                Set l_rstTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & Val(grdClips.Columns("eventID").CellText(bkmrk)) & ";", g_strExecuteError)
'                CheckForSQLError
'
'                ProgressBar1.Value = Count
'                DoEvents
'
'                'Invoke a restore to the hotfolder location
'                l_lngNewEventID = CopyFileEventToLibraryID(l_rstTemp("eventID"), l_lngDestinationLibraryID, True)
'                l_strTempDestinationFolder = l_strDestinationDropfolder
'                l_strTempRestorePath = l_strNewRestorePath
'                If InStr(l_strDestinationSubfolder, "{clipreference}") > 0 Then
'                    l_strTempDestinationFolder = l_strTempDestinationFolder & "\" & QuoteSanitise(l_rstTemp("clipreference"))
'                    l_strTempRestorePath = l_strTempRestorePath & "/" & l_rstTemp("clipreference")
'                End If
'                If InStr(l_strDestinationSecondaryFolder, "{movielabs_key}") > 0 And Right(l_rstTemp("clipfilename"), 4) <> ".xml" Then
'                    l_strTempDestinationFolder = l_strTempDestinationFolder & "\resources"
'                    l_strTempRestorePath = l_strTempRestorePath & "/resources"
'                End If
'                SetData "events", "altlocation", "eventID", l_lngNewEventID, l_strTempDestinationFolder
'                SetData "events", "bigfilesize", "eventID", l_lngNewEventID, Null
'                SetData "events", "soundlay", "eventID", l_lngNewEventID, Null
'                SetData "events", "online", "eventID", l_lngNewEventID, 1
'                l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewFileID, FullPathToSourceFile, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
'                l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
'                l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
'                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "DIVA To CETA") & ", "
'                l_strSQL = l_strSQL & l_lngNewEventID & ", "
'                l_strSQL = l_strSQL & "'" & l_strTempRestorePath & "/" & l_rstTemp("clipfilename") & "', "
'                l_strSQL = l_strSQL & "1, "
'                l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
'                Debug.Print l_strSQL
'                ExecuteSQL l_strSQL, g_strExecuteError
'                CheckForSQLError
'
'                'Then make a placeholder delivery record
'                l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, finalstatus) VALUES ("
'                l_strSQL = l_strSQL & "2753, "
'                l_strSQL = l_strSQL & l_rstTemp("companyID") & ", "
'                l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
'                l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
'                l_strSQL = l_strSQL & "'Aspera to " & l_strDestinationID & "', "
'                l_strSQL = l_strSQL & "'Completed')"
'                Debug.Print l_strSQL
'                ExecuteSQL l_strSQL, g_strExecuteError
'                CheckForSQLError
'                Count = Count + 1
'            Next
        End If
    Else
        If MsgBox("You are about to send the whole grid" & vbCrLf & "Are you Sure", vbYesNo + vbDefaultButton2, "Final Confirmation") = vbNo Then
            Exit Sub
        End If
                
        Count = 1
        ProgressBar1.Max = adoClip.Recordset.RecordCount
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        adoClip.Recordset.MoveFirst
        If (optStorageType(0).Value = True Or lblFormat.Caption = "DISCSTORE") Then
            Do While Not adoClip.Recordset.EOF
                ProgressBar1.Value = Count
                DoEvents
                'Invoke a copy to the hotfolder location
                l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
                l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                l_strSQL = l_strSQL & adoClip.Recordset("LibraryID") & ", "
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
                l_strSQL = l_strSQL & l_lngDestinationLibraryID & ", "
                l_strSQL = l_strSQL & "'"
                l_strSQL = l_strSQL & l_strDestinationDropfolder
                If InStr(l_strDestinationSubfolder, "{clipreference}") > 0 Then
                    l_strSQL = l_strSQL & "\" & QuoteSanitise(adoClip.Recordset("clipreference"))
                End If
                If InStr(l_strDestinationSecondaryFolder, "{movielabs_key}") > 0 And Right(adoClip.Recordset("clipfilename"), 4) <> ".xml" Then
                    l_strSQL = l_strSQL & "\resources"
                End If
                l_strSQL = l_strSQL & "', "
                l_strSQL = l_strSQL & "1, "
                l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                
                l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, finalstatus) VALUES ("
                l_strSQL = l_strSQL & "2753, "
                l_strSQL = l_strSQL & adoClip.Recordset("companyID") & ", "
                l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                l_strSQL = l_strSQL & "'Aspera to " & l_strDestinationID & "', "
                l_strSQL = l_strSQL & "'Completed')"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                Count = Count + 1
                adoClip.Recordset.MoveNext
            Loop
        ElseIf (optStorageType(4).Value = True Or lblFormat.Caption = "DIVA") Then
            MsgBox "Direct Restores into Aspera Hotfolders has become unreliable." & vbCrLf & "Please Restore to a DISCSTORE somewhere and then deliver the items from there." & vbCrLf & "We will re-enable this feature as soon as we can.", vbCritical
            Exit Sub
'            Do While Not adoClip.Recordset.EOF
'                ProgressBar1.Value = Count
'                DoEvents
'
'                'Invoke a restore to the hotfolder location
'                l_lngNewEventID = CopyFileEventToLibraryID(adoClip.Recordset("eventID"), l_lngDestinationLibraryID, True)
'                l_strTempDestinationFolder = l_strDestinationDropfolder
'                l_strTempRestorePath = l_strNewRestorePath
'                If InStr(l_strDestinationSubfolder, "{clipreference}") > 0 Then
'                    l_strTempDestinationFolder = l_strTempDestinationFolder & "\" & QuoteSanitise(adoClip.Recordset("clipreference"))
'                    l_strTempRestorePath = l_strTempRestorePath & "/" & adoClip.Recordset("clipreference")
'                End If
'                If InStr(l_strDestinationSecondaryFolder, "{movielabs_key}") > 0 And Right(adoClip.Recordset("clipfilename"), 4) <> ".xml" Then
'                    l_strTempDestinationFolder = l_strTempDestinationFolder & "\resources"
'                    l_strTempRestorePath = l_strTempRestorePath & "/resources"
'                End If
'                SetData "events", "altlocation", "eventID", l_lngNewEventID, l_strTempDestinationFolder
'                SetData "events", "bigfilesize", "eventID", l_lngNewEventID, Null
'                SetData "events", "soundlay", "eventID", l_lngNewEventID, Null
'                SetData "events", "online", "eventID", l_lngNewEventID, 1
'                l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewFileID, FullPathToSourceFile, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
'                l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
'                l_strSQL = l_strSQL & adoClip.Recordset("libraryID") & ", "
'                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "DIVA To CETA") & ", "
'                l_strSQL = l_strSQL & l_lngNewEventID & ", "
'                l_strSQL = l_strSQL & "'" & l_strTempRestorePath & "/" & adoClip.Recordset("clipfilename") & "', "
'                l_strSQL = l_strSQL & "1, "
'                l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
'                Debug.Print l_strSQL
'                ExecuteSQL l_strSQL, g_strExecuteError
'                CheckForSQLError
'
'                'Then make a placeholder delivery record
'                l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, finalstatus) VALUES ("
'                l_strSQL = l_strSQL & "2753, "
'                l_strSQL = l_strSQL & adoClip.Recordset("companyID") & ", "
'                l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
'                l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
'                l_strSQL = l_strSQL & "'Aspera to " & l_strDestinationID & "', "
'                l_strSQL = l_strSQL & "'Completed')"
'                Debug.Print l_strSQL
'                ExecuteSQL l_strSQL, g_strExecuteError
'                CheckForSQLError
'                Count = Count + 1
'                adoClip.Recordset.MoveNext
'            Loop
        End If
    End If
    ProgressBar1.Visible = False

    MsgBox "Done"
    
Else

    MsgBox "Cancelled"

End If

End Sub

Private Sub cmdSendGridViaDeliverySystem_Click()

'Has to be some records int he grid
If adoClip.Recordset.RecordCount <= 0 Then Exit Sub

'Has to be a companyID
If Val(lblCompanyID.Caption) = 0 Then
    MsgBox "A Single client must be selected for File Sends.", vbCritical
    Exit Sub
End If

'Has to be from either a DISCSTORE or the DIVA
If (optStorageType(0).Value = False And lblFormat.Caption <> "DISCSTORE") And (optStorageType(4).Value = False And lblFormat.Caption <> "DIVA") Then
    MsgBox "We can only Delivery items that are on a DISCSTORE or the DIVA at this time.", vbCritical
    Exit Sub
End If

Dim l_lngDestinationID As Long, l_strDestinationName As String, l_strDestinationType As String, l_strDestinationDropfolder As String, l_strDestinationSubfolder As String, l_lngDestinationLibraryID As Long, l_strSQL As String, Count As Long
Dim l_lngCount As Long, bkmrk As Variant, l_rstTemp As ADODB.Recordset, l_strNewRestorePath As String, l_lngNewEventID As Long, l_strDestinationSecondaryFolder As String, l_lngSlashCounter As Long
Dim l_blnNullFlag As Boolean

l_blnNullFlag = False

frmGetDestination.Show vbModal
l_lngDestinationID = Val(frmGetDestination.cmbDestination.Columns("DigitalDestinationID").Text)
l_strDestinationName = frmGetDestination.cmbDestination.Columns("DestinationName").Text
l_strDestinationType = frmGetDestination.cmbDestination.Columns("DestinationType").Text
Unload frmGetDestination

'Give the fitst 'Are You Sure?'
If MsgBox("You are about to send these files to a " & l_strDestinationType & " Recipient." & vbCrLf & "Are you Sure", vbYesNo) = vbNo Then Exit Sub

'If a destination was chosen...
If l_lngDestinationID <> 0 Then
    If MsgBox("You are about to send these files to " & l_strDestinationName & "." & vbCrLf & "Are you Sure", vbYesNo) = vbNo Then Exit Sub
    l_strDestinationDropfolder = GetDataSQL("SELECT DestinationDropfolder FROM DigitalDestination WHERE DigitalDestinationID = " & l_lngDestinationID)
    If Trim(" " & GetDataSQL("SELECT DestinationSubfolder FROM DigitalDestination WHERE DigitalDestinationID = " & l_lngDestinationID)) <> "" Then
        l_strDestinationSubfolder = GetDataSQL("SELECT DestinationSubfolder FROM DigitalDestination WHERE DigitalDestinationID = " & l_lngDestinationID)
        Select Case l_strDestinationSubfolder
            Case "{Date}"
                l_strDestinationDropfolder = l_strDestinationDropfolder & "\" & Format(Now, "YYYY-MM-DD")
            Case "{Date2}"
                l_strDestinationDropfolder = l_strDestinationDropfolder & "\" & Format(Now, "YYYY_MM_DD")
            Case "{DateTime}"
                l_strDestinationDropfolder = l_strDestinationDropfolder & "\" & Format(Now, "YYYY-MM-DD") & "_" & Format(Now, "HH-NN-SS")
            Case InStr(l_strDestinationSubfolder, "{") <= 0
                l_strDestinationDropfolder = l_strDestinationDropfolder & "\" & l_strDestinationSubfolder
            Case Else
                'Do Nothing - we don't process that key here
        End Select
    End If
    
    If Trim(" " & GetDataSQL("SELECT DestinationSecondaryFolder FROM DigitalDestination WHERE DigitalDestinationID " & l_lngDestinationID)) <> "" Then
        l_strDestinationSecondaryFolder = GetDataSQL("SELECT DestinationSecondaryFolder FROM DigitalDestination WHERE DigitalDestinationID " & l_lngDestinationID)
    End If
    
    Select Case l_strDestinationType
        Case "Signiant"
            l_lngDestinationLibraryID = GetData("library", "libraryID", "barcode", g_strSigniantSendPath)
        Case "Aspera1"
            l_lngDestinationLibraryID = GetData("library", "libraryID", "barcode", g_strAsperaClient1SendPath)
        Case "Aspera360"
            l_lngDestinationLibraryID = GetData("library", "libraryID", "barcode", g_strAsperaClient360SendPath)
        Case Else
            l_lngDestinationLibraryID = GetData("library", "libraryID", "barcode", l_strDestinationType)
    End Select
    
    l_strNewRestorePath = GetData("library", "version", "libraryID", l_lngDestinationLibraryID) & "/" & SlashBackToForward(l_strDestinationDropfolder)
    
    If MsgBox("Just send selected files (instead of whole grid)", vbYesNo + vbDefaultButton2, "Sending Grid") = vbYes Then
        
        l_lngCount = grdClips.SelBookmarks.Count
        
        If l_lngCount <= 0 Then
            MsgBox "No rows selected.", vbCritical, "Error with Send via Aspera"
            Exit Sub
        End If
        
        If l_lngCount > 20 Then chkFutureRequest.Value = 1
        
        If MsgBox("Send " & l_lngCount & " items - Are you sure", vbYesNo + vbDefaultButton2, "Sending Grid") = vbNo Then Exit Sub
        
        ProgressBar1.Max = grdClips.SelBookmarks.Count
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        Count = 1
        
        If (optStorageType(0).Value = True Or lblFormat.Caption = "DISCSTORE") Then
            For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1
                bkmrk = grdClips.SelBookmarks(l_lngCount)
                Set l_rstTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & Val(grdClips.Columns("eventID").CellText(bkmrk)) & ";", g_strExecuteError)
                CheckForSQLError
                
                ProgressBar1.Value = Count
                DoEvents
                
                If l_rstTemp("clipfilename") <> ".DS_Store" And Not IsNull(l_rstTemp("bigfilesize")) Then
                    'Invoke a copy to the hotfolder location
                    l_strSQL = "INSERT INTO Event_File_Request (RequestDate, eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, DoNotRecordCopy, bigfilesize, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
                    l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                    l_strSQL = l_strSQL & l_rstTemp("LibraryID") & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
                    l_strSQL = l_strSQL & l_lngDestinationLibraryID & ", "
                    l_strSQL = l_strSQL & "'"
                    l_strSQL = l_strSQL & l_strDestinationDropfolder
                    If InStr(l_strDestinationSubfolder, "{clipreference}") > 0 Then
                        l_strSQL = l_strSQL & "\" & QuoteSanitise(l_rstTemp("clipreference"))
                    End If
                    If InStr(l_strDestinationSecondaryFolder, "{movielabs_key}") > 0 And Right(l_rstTemp("clipfilename"), 4) <> ".xml" Then
                        l_strSQL = l_strSQL & "\resources"
                    End If
                    If InStr(l_strDestinationSubfolder, "{iTunes}") Then
                        l_lngSlashCounter = InStr(adoClip.Recordset("altfolder"), "\")
                        Do While InStr(Mid(adoClip.Recordset("altfolder"), l_lngSlashCounter), "\") > 0
                            l_lngSlashCounter = l_lngSlashCounter + InStr(Mid(adoClip.Recordset("altfolder"), l_lngSlashCounter), "\")
                        Loop
                        l_strSQL = l_strSQL & "\" & Mid(adoClip.Recordset("altfolder"), l_lngSlashCounter)
                    End If
                    l_strSQL = l_strSQL & "', "
                    l_strSQL = l_strSQL & "1, "
                    l_strSQL = l_strSQL & l_rstTemp("bigfilesize") & ", "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    
                    l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, bigfilesize, accesstype, finalstatus) VALUES ("
                    l_strSQL = l_strSQL & "3684, "
                    l_strSQL = l_strSQL & l_rstTemp("companyID") & ", "
                    l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD HH:NN:SS") & "', "
                    l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                    l_strSQL = l_strSQL & l_rstTemp("bigfilesize") & ", "
                    l_strSQL = l_strSQL & "'" & l_strDestinationType & " to " & l_strDestinationName & "', "
                    l_strSQL = l_strSQL & "'Completed')"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                ElseIf IsNull(l_rstTemp("bigfilesize")) Then
                    l_blnNullFlag = True
                End If
                Count = Count + 1
            Next
        ElseIf (optStorageType(4).Value = True Or lblFormat.Caption = "DIVA") Then
            For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1
                bkmrk = grdClips.SelBookmarks(l_lngCount)
                Set l_rstTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & Val(grdClips.Columns("eventID").CellText(bkmrk)) & ";", g_strExecuteError)
                CheckForSQLError
                
                ProgressBar1.Value = Count
                DoEvents
                
                If l_rstTemp("clipfilename") <> ".DS_Store" And Not IsNull(l_rstTemp("bigfilesize")) Then
                    'Invoke a restore to the hotfolder location
                    l_lngNewEventID = CopyFileEventToLibraryID(l_rstTemp("eventID"), l_lngDestinationLibraryID, True)
                    SetData "events", "altlocation", "eventID", l_lngNewEventID, l_strDestinationDropfolder
                    SetData "events", "bigfilesize", "eventID", l_lngNewEventID, Null
                    SetData "events", "soundlay", "eventID", l_lngNewEventID, Null
                    SetData "events", "online", "eventID", l_lngNewEventID, 1
                    l_strSQL = "INSERT INTO event_file_request (RequestDate, eventID, SourceLibraryID, event_file_Request_typeID, NewFileID, FullPathToSourceFile, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
                    l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                    l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "DIVA To CETA") & ", "
                    l_strSQL = l_strSQL & l_lngNewEventID & ", "
                    l_strSQL = l_strSQL & "'" & l_strNewRestorePath & "/" & l_rstTemp("clipfilename") & "', "
                    l_strSQL = l_strSQL & "1, "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    
                    'Then make a placeholder delivery record
                    l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, bigfilesize, accesstype, finalstatus) VALUES ("
                    l_strSQL = l_strSQL & "2753, "
                    l_strSQL = l_strSQL & l_rstTemp("companyID") & ", "
                    l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD HH:NN:SS") & "', "
                    l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                    l_strSQL = l_strSQL & l_rstTemp("bigfilesize") & ", "
                    l_strSQL = l_strSQL & "'" & l_strDestinationType & " to " & l_strDestinationName & "', "
                    l_strSQL = l_strSQL & "'Completed')"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                ElseIf IsNull(l_rstTemp("bigfilesize")) Then
                    l_blnNullFlag = True
                End If
                Count = Count + 1
            Next
        End If
    Else
        If MsgBox("You are about to send the whole grid" & vbCrLf & "Are you Sure", vbYesNo + vbDefaultButton2, "Final Confirmation") = vbNo Then
            Exit Sub
        End If
                
        Count = 1
        ProgressBar1.Max = adoClip.Recordset.RecordCount
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        adoClip.Recordset.MoveFirst
        If adoClip.Recordset.RecordCount > 20 Then chkFutureRequest.Value = 1
        If (optStorageType(0).Value = True Or lblFormat.Caption = "DISCSTORE") Then
            Do While Not adoClip.Recordset.EOF
                ProgressBar1.Value = Count
                DoEvents
                If Not IsNull(adoClip.Recordset("bigfilesize")) Then
                    'Invoke a copy to the hotfolder location
                    l_strSQL = "INSERT INTO Event_File_Request (RequestDate, eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, DoNotRecordCopy, bigfilesize, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
                    l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                    l_strSQL = l_strSQL & adoClip.Recordset("LibraryID") & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
                    l_strSQL = l_strSQL & l_lngDestinationLibraryID & ", "
                    l_strSQL = l_strSQL & "'"
                    l_strSQL = l_strSQL & l_strDestinationDropfolder
                    If InStr(l_strDestinationSubfolder, "{clipreference}") > 0 Then
                        l_strSQL = l_strSQL & "\" & QuoteSanitise(adoClip.Recordset("clipreference"))
                    End If
                    If InStr(l_strDestinationSecondaryFolder, "{movielabs_key}") > 0 And Right(adoClip.Recordset("clipfilename"), 4) <> ".xml" Then
                        l_strSQL = l_strSQL & "\resources"
                    End If
                    If InStr(l_strDestinationSubfolder, "{iTunes}") Then
                        l_lngSlashCounter = InStr(adoClip.Recordset("altlocation"), "\")
                        Do While InStr(Mid(adoClip.Recordset("altlocation"), l_lngSlashCounter + 1), "\") > 0
                            l_lngSlashCounter = l_lngSlashCounter + InStr(Mid(adoClip.Recordset("altlocation"), l_lngSlashCounter + 1), "\")
                        Loop
                        l_strSQL = l_strSQL & "\" & Mid(adoClip.Recordset("altlocation"), l_lngSlashCounter + 1)
                    End If
                    l_strSQL = l_strSQL & "', "
                    l_strSQL = l_strSQL & "1, "
                    l_strSQL = l_strSQL & adoClip.Recordset("bigfilesize") & ", "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    
                    l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, bigfilesize, accesstype, finalstatus) VALUES ("
                    l_strSQL = l_strSQL & "3684, "
                    l_strSQL = l_strSQL & adoClip.Recordset("companyID") & ", "
                    l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD HH:NN:SS") & "', "
                    l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                    l_strSQL = l_strSQL & adoClip.Recordset("bigfilesize") & ", "
                    l_strSQL = l_strSQL & "'" & l_strDestinationType & " to " & l_strDestinationName & "', "
                    l_strSQL = l_strSQL & "'Completed')"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                ElseIf IsNull(adoClip.Recordset("bigfilesize")) Then
                    l_blnNullFlag = True
                End If
                Count = Count + 1
                adoClip.Recordset.MoveNext
            Loop
        ElseIf (optStorageType(4).Value = True Or lblFormat.Caption = "DIVA") Then
            Do While Not adoClip.Recordset.EOF
                ProgressBar1.Value = Count
                DoEvents
                If Not IsNull(adoClip.Recordset("bigfilesize")) Then
                    'Invoke a restore to the hotfolder location
                    l_lngNewEventID = CopyFileEventToLibraryID(adoClip.Recordset("eventID"), l_lngDestinationLibraryID, True)
                    SetData "events", "altlocation", "eventID", l_lngNewEventID, l_strDestinationDropfolder
                    SetData "events", "bigfilesize", "eventID", l_lngNewEventID, Null
                    SetData "events", "soundlay", "eventID", l_lngNewEventID, Null
                    SetData "events", "online", "eventID", l_lngNewEventID, 1
                    l_strSQL = "INSERT INTO event_file_request (RequestDate, eventID, SourceLibraryID, event_file_Request_typeID, NewFileID, FullPathToSourceFile, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & "'" & Format(IIf(chkFutureRequest.Value <> 0, DateAdd("D", 30, Now), Now), "YYYY-MM-DD HH:NN:SS") & "', "
                    l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                    l_strSQL = l_strSQL & adoClip.Recordset("libraryID") & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "DIVA To CETA") & ", "
                    l_strSQL = l_strSQL & l_lngNewEventID & ", "
                    l_strSQL = l_strSQL & "'" & l_strNewRestorePath & "/" & adoClip.Recordset("clipfilename") & "', "
                    l_strSQL = l_strSQL & "1, "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    
                    'Then make a placeholder delivery record
                    l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, bigfilesize, accesstype, finalstatus) VALUES ("
                    l_strSQL = l_strSQL & "2753, "
                    l_strSQL = l_strSQL & adoClip.Recordset("companyID") & ", "
                    l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD HH:NN:SS") & "', "
                    l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                    l_strSQL = l_strSQL & adoClip.Recordset("bigfilesize") & ", "
                    l_strSQL = l_strSQL & "'" & l_strDestinationType & " to " & l_strDestinationName & "', "
                    l_strSQL = l_strSQL & "'Completed')"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                ElseIf IsNull(adoClip.Recordset("bigfilesize")) Then
                    l_blnNullFlag = True
                End If
                Count = Count + 1
                adoClip.Recordset.MoveNext
            Loop
        End If
    End If
    ProgressBar1.Visible = False

    MsgBox "Done" & IIf(l_blnNullFlag = True, vbCrLf & "Unverified items were skipped", "")

Else

    MsgBox "Cancelled"

End If

End Sub

Private Sub cmdInitiateWorkflow_Click()

'Has to be some records int he grid
If adoClip.Recordset.RecordCount <= 0 Then Exit Sub

'Has to be a companyID
If Val(lblCompanyID.Caption) = 0 Then
    MsgBox "A Single client must be selected for Workflow Variants.", vbCritical
    Exit Sub
End If

'Has to be from either a DISCSTORE or the DIVA
If (optStorageType(0).Value = False And lblFormat.Caption <> "DISCSTORE") And (optStorageType(4).Value = False And lblFormat.Caption <> "DIVA") Then
    MsgBox "We can only do Workflows on items that are on a DISCSTORE or the DIVA at this time.", vbCritical
    Exit Sub
End If

MsgBox "This action will create Generic Tracker lines for all the files in the grid, including the reference, fillename, and putting the clipID into {originaleventID} of the tracker." & vbCrLf & _
"It will include a Workflow Variant ID from the Workflow that you chose in the next box. Leave the text blank to cancel this action."

Dim l_strSQL As String, l_strVariantName As String, l_lngVariantID As Long, l_rstClips As ADODB.Recordset, Count As Long
Dim Duration_Timecode As String, Duration As Long

frmGetATSProfile.chkWorkflowVariant.Value = 1
frmGetATSProfile.lblCompanyID.Caption = lblCompanyID.Caption
frmGetATSProfile.Show vbModal
l_strVariantName = frmGetATSProfile.cmbProfile.Text
If l_strVariantName = "" Then
    MsgBox "Action Cancelled"
    Exit Sub
End If
l_lngVariantID = frmGetATSProfile.cmbProfile.Columns("ATS_Profile_ID").Text
Unload frmGetNewFileDetails

Set l_rstClips = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
CheckForSQLError
If l_rstClips.RecordCount > 0 Then
    l_rstClips.MoveFirst
    Count = 0
    ProgressBar1.Max = l_rstClips.RecordCount
    ProgressBar1.Value = 0
    ProgressBar1.Visible = True
    Do While Not l_rstClips.EOF
        'Update the progress label
        ProgressBar1.Value = Count
        lblCurrentFilename.Caption = l_rstClips("clipfilename")
        DoEvents
        
        Duration_Timecode = Trim(" " & l_rstClips("fd_length"))
        If Duration_Timecode <> "" Then
            Duration = 60 * Val(Left(Duration_Timecode, 2)) + Val(Mid(Duration_Timecode, 4, 2))
            If Val(Mid(Duration_Timecode, 7, 2)) > 0 Then Duration = Duration + 1
        Else
            Duration = 0
        End If
        l_strSQL = "INSERT INTO tracker_item (companyID, itemreference, itemfilename, originaleventID, timecodeduration, duration, WorkflowVariantID) VALUES ("
        l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstClips("clipreference")) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstClips("clipfilename")) & "', "
        l_strSQL = l_strSQL & l_rstClips("eventID") & ", "
        l_strSQL = l_strSQL & "'" & Duration_Timecode & "', "
        l_strSQL = l_strSQL & Duration & ", "
        l_strSQL = l_strSQL & l_lngVariantID & ");"
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        Count = Count + 1
        l_rstClips.MoveNext
        
    Loop
End If

l_rstClips.Close
Set l_rstClips = Nothing
ProgressBar1.Visible = False
lblCurrentFilename.Caption = ""
DoEvents

MsgBox "Done!"

adoClip.Refresh
adoClip.Caption = grdClips.Rows & " Clips Found"

Exit Sub

End Sub

Private Sub cmdSendViaFaspex_Click()

If Not CheckAccess("/SendViaContentDelivery") Then Exit Sub

'Has to be some records int he grid
If adoClip.Recordset.RecordCount <= 0 Then
    MsgBox "There must be some files in order to send"
    Exit Sub
End If

'Has to be a companyID
If Val(lblCompanyID.Caption) = 0 Then
    MsgBox "A Single client must be selected for File Sends.", vbCritical
    Exit Sub
End If

If (optStorageType(0).Value = False And lblFormat.Caption <> "DISCSTORE") And (optStorageType(4).Value = False And lblFormat.Caption <> "DIVA") Then
    MsgBox "We can only Delivery items that are on a DISCSTORE or the DIVA at this time.", vbCritical
    Exit Sub
End If

'Give the first 'Are You Sure?'
If MsgBox("You are about to send files to FaspEx Recipients, based on the contacts in a Job sheet." & vbCrLf & "Are you Sure", vbYesNo) = vbNo Then Exit Sub

Dim l_lngDestinationJobID As Long, l_blnNotifyUser As Boolean, l_blnMarkJobComplete As Boolean
Dim l_strSQL As String, Count As Long, l_strMessage As String
Dim l_rstJobLines As ADODB.Recordset, l_rstUsers As ADODB.Recordset, l_lngLastClipID As Long, l_lngCDDeliveryNumber As Long
Dim l_strNewLibraryBarcode As String, l_strNewFolder As String, l_lngNewLibraryID As Long, l_strSubjectLine As String, l_strMessageBody As String
Dim l_strCETACompanyCodes As String
Dim l_lngAsperaLibraryID As Long, l_strAsperaFolder As String, l_lngNewEventID As Long, l_strNewPath As String
Dim l_rsTemp As ADODB.Recordset, bkmrk As Variant, l_lngCount As Long

l_lngDestinationJobID = Val(InputBox("Give the JobID for this send", "Send Via Content Delivery", 0))
If l_lngDestinationJobID = 0 Then
    MsgBox "No jobID provided - aborting the send"
    Exit Sub
End If

If Val(Trim(" " & GetData("job", "companyID", "jobID", l_lngDestinationJobID))) <> Val(lblCompanyID.Caption) Then
    MsgBox "Job does not belong to the company selected for the Clip Search"
    Exit Sub
End If

l_blnNotifyUser = True
l_strSubjectLine = GetData("job", "EmailNotificationSubjectLine", "jobID", l_lngDestinationJobID)
Do
    If Len(l_strSubjectLine) > 42 Then
        l_strSubjectLine = InputBox("The Notification Subject Line is too long. PLease edit to 42 characters max", "Notifiaction Subject Line", l_strSubjectLine)
    Else
        l_strSubjectLine = InputBox("Please confirm the email notification Subject Line", "Notifiaction Subject Line", l_strSubjectLine)
    End If
Loop Until Len(l_strSubjectLine) <= 42

frmTextEdit.txtTextToEdit.Text = GetData("job", "EmailNotificationMessageBody", "jobID", l_lngDestinationJobID)
frmTextEdit.Caption = "Please confirm any additional text that needs to be in the notification message"
frmTextEdit.Show vbModal
If frmTextEdit.Tag <> "CANCELLED" Then
    l_strMessageBody = frmTextEdit.txtTextToEdit.Text
End If
Unload frmTextEdit

If MsgBox("Should the system mark this job as VT Done once these sends are queued?", vbYesNo + vbDefaultButton2) = vbYes Then l_blnMarkJobComplete = True

l_lngNewLibraryID = 0

If (optStorageType(0).Value = True Or lblFormat.Caption = "DISCSTORE") Then
    If Val(lblLibraryID.Caption) <> 284349 And Val(lblLibraryID.Caption) <> 441212 And Val(lblLibraryID.Caption) <> 623206 And Val(lblLibraryID.Caption) <> 650950 And Val(lblLibraryID.Caption) <> 722000 Then
        If MsgBox("Do you wish to move these files somewhere after they are sent?", vbYesNo, "Stick the originals somwehere") = vbYes Then
            frmGetNewFileDetails.Caption = "Please give the Barcode to move to..."
            frmGetNewFileDetails.chkPreserveMasterfile.Visible = False
            frmGetNewFileDetails.txtAltLocation.Text = "AutoDelete30Days\" & l_lngDestinationJobID
            frmGetNewFileDetails.Show vbModal
            l_strNewLibraryBarcode = UCase(frmGetNewFileDetails.txtBarcode.Text)
            l_strNewFolder = frmGetNewFileDetails.txtAltLocation.Text
            Unload frmGetNewFileDetails
            If l_strNewLibraryBarcode = "" Then
                MsgBox "No Destination Provided = Aborting operation"
                Exit Sub
            End If
            l_lngNewLibraryID = GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode)
        Else
            l_lngNewLibraryID = 0
        End If
    End If
End If

l_strSQL = "SELECT * FROM jobcontact WHERE jobID = " & l_lngDestinationJobID & " AND CCnotification = 0;"
Set l_rstUsers = ExecuteSQL(l_strSQL, g_strExecuteError)
If l_rstUsers.RecordCount <> 0 Then

    l_strMessage = ""
    l_rstUsers.MoveFirst
    Do While Not l_rstUsers.EOF
        l_strMessage = l_strMessage & l_rstUsers("name") & ", Email: " & l_rstUsers("email") & vbCrLf
        l_rstUsers.MoveNext
    Loop
    
    l_strMessage = l_strMessage & vbCrLf & vbCrLf & "A notification will be sent to all the recipients, with the subject line:" & vbCrLf & l_strSubjectLine & vbCrLf
    If l_strMessageBody <> "" Then
        l_strMessage = l_strMessage & vbCrLf & vbCrLf & "And the following text will be at the start of the Notification message:" & vbCrLf & l_strMessageBody & vbCrLf
    End If
    
    If l_lngNewLibraryID <> 0 Then
        l_strMessage = l_strMessage & "And after delivery, the originals will all be moved to " & l_strNewLibraryBarcode & " in folder " & l_strNewFolder & vbCrLf
    End If
    
    l_lngCDDeliveryNumber = GetNextSequence("CDDeliveryNumber")
    
    l_lngAsperaLibraryID = GetCETASetting("FaspexSendLibraryID")
    l_strAsperaFolder = GetCETASetting("FaspexSendAltlocation") & "\" & l_lngCDDeliveryNumber
    
    If MsgBox("Just send selected files (instead of whole grid)", vbYesNo + vbDefaultButton2, "Sending Grid") = vbYes Then
        
        If MsgBox("You are about to send the selected files to:" & vbCrLf & l_strMessage & vbCrLf & "Are you Sure", vbYesNo + vbDefaultButton2, "Final Confirmation") = vbNo Then
            l_rstUsers.Close
            Set l_rstUsers = Nothing
            Exit Sub
        End If
        
        l_lngCount = grdClips.SelBookmarks.Count
        
        If l_lngCount <= 0 Then
            MsgBox "No rows selected.", vbCritical, "Error with Send via FaspEx"
            l_rstUsers.Close
            Set l_rstUsers = Nothing
            Exit Sub
        End If
        
        If MsgBox("Send " & l_lngCount & " items - Are you sure", vbYesNo + vbDefaultButton2, "Send Via FaspEx") = vbNo Then Exit Sub
        
        ProgressBar1.Max = grdClips.SelBookmarks.Count
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        Count = 1
        
        For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1
            
            bkmrk = grdClips.SelBookmarks(l_lngCount)
            Set l_rsTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & Val(grdClips.Columns("eventID").CellText(bkmrk)) & ";", g_strExecuteError)
            CheckForSQLError
            
            ProgressBar1.Value = Count
            DoEvents
            
            If l_rsTemp("companyID") = Val(lblCompanyID.Caption) Then
                'Make a Content Delivery Send Request for this clip with this DestinationJobID
                If (optStorageType(0).Value = True Or lblFormat.Caption = "DISCSTORE") Then
                    l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, CDDeliveryNumber, NewLibraryID, NewAltlocation, NewFilename, Bigfilesize, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & l_rsTemp("eventID") & ", "
                    l_strSQL = l_strSQL & l_rsTemp("libraryID") & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
                    l_strSQL = l_strSQL & l_lngCDDeliveryNumber & ", "
                    l_strSQL = l_strSQL & l_lngAsperaLibraryID & ", "
                    l_strSQL = l_strSQL & "'" & l_strAsperaFolder & "', "
                    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rsTemp("clipfilename")) & "', "
                    l_strSQL = l_strSQL & l_rsTemp("Bigfilesize") & ", "
                    l_strSQL = l_strSQL & "1, "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                ElseIf (optStorageType(4).Value = True Or lblFormat.Caption = "DIVA") Then
                    l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, CDDeliveryNumber, NewFilename, NewFileID, NewLibraryID, FullPathToSourceFile, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & l_rsTemp("eventID") & ", "
                    l_strSQL = l_strSQL & l_rsTemp("libraryID") & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "DIVA to CETA") & ", "
                    l_lngNewEventID = CopyFileEventToLibraryID(l_rsTemp("eventID"), l_lngAsperaLibraryID, True)
                    l_strNewPath = GetData("library", "version", "libraryID", l_lngAsperaLibraryID) & "/" & Replace(l_strAsperaFolder, "\", "/") & "/" & l_rsTemp("clipfilename")
                    SetData "events", "altlocation", "eventID", l_lngNewEventID, l_strAsperaFolder
                    SetData "events", "bigfilesize", "eventID", l_lngNewEventID, Null
                    SetData "events", "soundlay", "eventID", l_lngNewEventID, Null
                    SetData "events", "online", "eventID", l_lngNewEventID, 1
                
                    l_strSQL = l_strSQL & l_lngCDDeliveryNumber & ", "
                    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rsTemp("clipfilename")) & "', "
                    l_strSQL = l_strSQL & l_lngNewEventID & ", "
                    l_strSQL = l_strSQL & IIf(l_lngAsperaLibraryID <> 0, l_lngAsperaLibraryID & ", '" & l_strNewPath & "', ", "Null, Null, ")
                    l_strSQL = l_strSQL & "1, "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                End If
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                
                If (optStorageType(0).Value = True Or lblFormat.Caption = "DISCSTORE") And l_lngNewLibraryID <> 0 Then
                    l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltlocation, Bigfilesize, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & l_rsTemp("eventID") & ", "
                    l_strSQL = l_strSQL & l_rsTemp("libraryID") & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
                    l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
                    l_strSQL = l_strSQL & "'" & l_strNewFolder & "', "
                    l_strSQL = l_strSQL & l_rsTemp("Bigfilesize") & ", "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                End If
                
                l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, bigfilesize, finalstatus) VALUES ("
                l_strSQL = l_strSQL & "1448, "
                l_strSQL = l_strSQL & l_rsTemp("companyID") & ", "
                l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                l_strSQL = l_strSQL & l_rsTemp("eventID") & ", "
                l_strSQL = l_strSQL & "'FaspexDelivery for Job " & l_lngDestinationJobID & "', "
                l_strSQL = l_strSQL & l_rsTemp("Bigfilesize") & ", "
                l_strSQL = l_strSQL & "'Completed')"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            End If
            Count = Count + 1
        Next
        ProgressBar1.Visible = False
    Else
        
        If MsgBox("You are about to send the whole grid to:" & vbCrLf & l_strMessage & vbCrLf & "Are you Sure", vbYesNo + vbDefaultButton2, "Final Confirmation") = vbNo Then
            l_rstUsers.Close
            Set l_rstUsers = Nothing
            Exit Sub
        End If
                
        Count = 1
        ProgressBar1.Max = adoClip.Recordset.RecordCount
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        adoClip.Recordset.MoveLast
        l_lngLastClipID = adoClip.Recordset("eventID")
        adoClip.Recordset.MoveFirst
        Do While Not adoClip.Recordset.EOF
            ProgressBar1.Value = Count
            DoEvents
            If adoClip.Recordset("companyID") = Val(lblCompanyID.Caption) Then
                'Make a Content Delivery Send Request for this clip with this DestinationJobID
                If (optStorageType(0).Value = True Or lblFormat.Caption = "DISCSTORE") Then
                    l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, CDDeliveryNumber, NewLibraryID, NewAltlocation, Newfilename, BigFileSize, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                    l_strSQL = l_strSQL & adoClip.Recordset("libraryID") & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
                    l_strSQL = l_strSQL & l_lngCDDeliveryNumber & ", "
                    l_strSQL = l_strSQL & l_lngAsperaLibraryID & ", "
                    l_strSQL = l_strSQL & "'" & l_strAsperaFolder & "', "
                    l_strSQL = l_strSQL & "'" & QuoteSanitise(adoClip.Recordset("clipfilename")) & "', "
                    l_strSQL = l_strSQL & adoClip.Recordset("Bigfilesize") & ", "
                    l_strSQL = l_strSQL & "1, "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                ElseIf (optStorageType(4).Value = True Or lblFormat.Caption = "DIVA") Then
                    l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, CDDeliveryNumber, Newfilename, NewFileID, NewLibraryID, FullPathToSourceFile, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                    l_strSQL = l_strSQL & adoClip.Recordset("libraryID") & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "DIVA to CETA") & ", "
                    l_lngNewEventID = CopyFileEventToLibraryID(adoClip.Recordset("eventID"), l_lngAsperaLibraryID, True)
                    l_strNewPath = GetData("library", "version", "libraryID", l_lngAsperaLibraryID) & "/" & Replace(l_strAsperaFolder, "\", "/") & "/" & adoClip.Recordset("clipfilename")
                    SetData "events", "altlocation", "eventID", l_lngNewEventID, l_strAsperaFolder
                    SetData "events", "bigfilesize", "eventID", l_lngNewEventID, Null
                    SetData "events", "soundlay", "eventID", l_lngNewEventID, Null
                    SetData "events", "online", "eventID", l_lngNewEventID, 1
                
                    l_strSQL = l_strSQL & l_lngCDDeliveryNumber & ", "
                    l_strSQL = l_strSQL & "'" & QuoteSanitise(adoClip.Recordset("clipfilename")) & "', "
                    l_strSQL = l_strSQL & l_lngNewEventID & ", "
                    l_strSQL = l_strSQL & IIf(l_lngAsperaLibraryID <> 0, l_lngAsperaLibraryID & ", '" & l_strNewPath & "', ", "Null, Null, ")
                    l_strSQL = l_strSQL & "1, "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                End If
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                
                If (optStorageType(0).Value = True Or lblFormat.Caption = "DISCSTORE") And l_lngNewLibraryID <> 0 Then
                    l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltlocation, Bigfilesize, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                    l_strSQL = l_strSQL & adoClip.Recordset("libraryID") & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
                    l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
                    l_strSQL = l_strSQL & "'" & l_strNewFolder & "', "
                    l_strSQL = l_strSQL & adoClip.Recordset("Bigfilesize") & ", "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                End If
                
                l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, BigFilesize, finalstatus) VALUES ("
                l_strSQL = l_strSQL & "1448, "
                l_strSQL = l_strSQL & adoClip.Recordset("companyID") & ", "
                l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                l_strSQL = l_strSQL & "'FaspexDelivery for Job " & l_lngDestinationJobID & "', "
                l_strSQL = l_strSQL & adoClip.Recordset("Bigfilesize") & ", "
                l_strSQL = l_strSQL & "'Completed')"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            End If
            Count = Count + 1
            adoClip.Recordset.MoveNext
        Loop
        ProgressBar1.Visible = False
    End If
    
    'Make the FaspEx Delivery request
    l_strSQL = "INSERT INTO Event_File_Request (event_File_Request_TypeID, JobID, CompanyID, NotifyRecipient, NotifySubjectLine, NotifyMessageBody, CDDeliveryNumber, RequestName, RequesterEmail) VALUES ("
    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Faspex") & ", "
    l_strSQL = l_strSQL & l_lngDestinationJobID & ", "
    l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
    l_strSQL = l_strSQL & CLng(l_blnNotifyUser) & ", "
    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strSubjectLine) & "'", "Null") & ", "
    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strMessageBody) & "'", "Null") & ", "
    l_strSQL = l_strSQL & l_lngCDDeliveryNumber & ", "
    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    If l_blnMarkJobComplete = True Then
        l_strSQL = "SELECT jobdetailID, rejecteddate, completeddate FROM jobdetail WHERE jobID = " & l_lngDestinationJobID
        Set l_rstJobLines = ExecuteSQL(l_strSQL, g_strExecuteError)
        If l_rstJobLines.RecordCount > 0 Then
            l_rstJobLines.MoveFirst
            Do While Not l_rstJobLines.EOF
                If IsNull(l_rstJobLines("rejecteddate")) And IsNull(l_rstJobLines("completeddate")) Then
                    If g_intUseTerseJobDetailCompletion = 1 Then
                        TerseCompleteJobDetailItem l_rstJobLines("jobdetailID")
                    Else
                        CompleteJobDetailItem l_rstJobLines("jobdetailID")
                    End If
                End If
                l_rstJobLines.MoveNext
            Loop
        End If
        l_rstJobLines.Close
        Set l_rstJobLines = Nothing
        UpdateJobStatus Val(l_lngDestinationJobID), "VT Done"
    End If
    MsgBox "Done"
    
    l_rstUsers.Close
    Set l_rstUsers = Nothing
    
Else

    MsgBox "No contacts specified for that job - cannot send files."
    l_rstUsers.Close
    Set l_rstUsers = Nothing

End If

End Sub

Private Sub cmdSendWithContentDelivery_Click()

If Not CheckAccess("/SendViaContentDelivery") Then Exit Sub

'Has to be some records int he grid
If adoClip.Recordset.RecordCount <= 0 Then
    MsgBox "There must be some files in order to send"
    Exit Sub
End If

'Has to be a companyID
If Val(lblCompanyID.Caption) = 0 Then
    MsgBox "A Single client must be selected for File Sends.", vbCritical
    Exit Sub
End If

If (optStorageType(0).Value = False And lblFormat.Caption <> "DISCSTORE") And (optStorageType(4).Value = False And lblFormat.Caption <> "DIVA") Then
    MsgBox "We can only Delivery items that are on a DISCSTORE or the DIVA at this time.", vbCritical
    Exit Sub
End If



'Give the first 'Are You Sure?'
If MsgBox("You are about to send files to Content Delivery Recipients, based on the contacts in a Job sheet." & vbCrLf & "Are you Sure", vbYesNo) = vbNo Then Exit Sub

Dim l_lngDestinationJobID As Long, l_blnNotifyUser As Boolean, l_blnMarkJobComplete As Boolean
Dim l_strSQL As String, Count As Long, l_strMessage As String
Dim l_rstJobLines As ADODB.Recordset, l_rstUsers As ADODB.Recordset, l_lngLastClipID As Long, l_lngCDDeliveryNumber As Long
Dim l_strNewLibraryBarcode As String, l_strNewFolder As String, l_lngNewLibraryID As Long, l_strSubjectLine As String, l_strMessageBody As String
Dim l_strCETACompanyCodes As String
Dim l_lngAsperaLibraryID As Long, l_strAsperaFolder As String, l_lngNewEventID As Long, l_strNewPath As String, l_lngEventID As Long, l_lngLibraryID As Long
Dim l_rsTemp As ADODB.Recordset, bkmrk As Variant, l_lngCount As Long

l_lngDestinationJobID = Val(InputBox("Give the JobID for this send", "Send Via Content Delivery", 0))
If l_lngDestinationJobID = 0 Then
    MsgBox "No jobID provided - aborting the send"
    Exit Sub
End If

If Val(Trim(" " & GetData("job", "companyID", "jobID", l_lngDestinationJobID))) <> Val(lblCompanyID.Caption) Then
    MsgBox "Job does not belong to the company selected for the Clip Search"
    Exit Sub
End If

If Trim(" " & GetDataSQL("SELECT TOP 1 jobcontactID FROM jobcontact WHERE jobID = " & l_lngDestinationJobID & " AND CCNotification = 0 AND portaluserID = 0")) <> "" Then
    MsgBox "There are recipients on this job sheet that have no valid portaluserID. Re-enter these recipients before continuing." & vbCrLf & "Aborting the send"
    Exit Sub
End If

If MsgBox("Should the system notify the recipient users of this delivery?", vbYesNo) = vbYes Then
    l_blnNotifyUser = True
    l_strSubjectLine = InputBox("Please confirm the email notification Subject Line", "Notifiaction Subject Line", GetData("job", "EmailNotificationSubjectLine", "jobID", l_lngDestinationJobID))
    frmTextEdit.txtTextToEdit.Text = GetData("job", "EmailNotificationMessageBody", "jobID", l_lngDestinationJobID)
    frmTextEdit.Caption = "Please confirm any additional text that needs to be in the notification message"
    frmTextEdit.Show vbModal
    If frmTextEdit.Tag <> "CANCELLED" Then
        l_strMessageBody = frmTextEdit.txtTextToEdit.Text
    End If
    Unload frmTextEdit
End If
If MsgBox("Should the system mark this job as VT Done once these sends are queued?", vbYesNo + vbDefaultButton2) = vbYes Then l_blnMarkJobComplete = True

l_lngNewLibraryID = 0

If (optStorageType(0).Value = True Or lblFormat.Caption = "DISCSTORE") Then
    If Val(lblLibraryID.Caption) <> 722000 Then
        If MsgBox("Do you wish to move these files somewhere after they are sent?", vbYesNo, "Stick the originals somwehere") = vbYes Then
            frmGetNewFileDetails.Caption = "Please give the Barcode to move to..."
            frmGetNewFileDetails.chkPreserveMasterfile.Visible = False
            frmGetNewFileDetails.txtAltLocation.Text = "AutoDelete30Days\" & l_lngDestinationJobID
            frmGetNewFileDetails.optAutoDeleteDiscstores.Value = True
            frmGetNewFileDetails.Show vbModal
            l_strNewLibraryBarcode = UCase(frmGetNewFileDetails.txtBarcode.Text)
            l_strNewFolder = frmGetNewFileDetails.txtAltLocation.Text
            Unload frmGetNewFileDetails
            If l_strNewLibraryBarcode = "" Then
                MsgBox "No Destination Provided = Aborting operation"
                Exit Sub
            End If
            l_lngNewLibraryID = GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode)
        End If
    End If
End If

l_strSQL = "SELECT * FROM jobcontact WHERE jobID = " & l_lngDestinationJobID & " AND CCnotification = 0;"
Set l_rstUsers = ExecuteSQL(l_strSQL, g_strExecuteError)
If l_rstUsers.RecordCount <> 0 Then

    l_strMessage = ""
    l_rstUsers.MoveFirst
    Do While Not l_rstUsers.EOF
        l_strMessage = l_strMessage & l_rstUsers("name") & ", PortaluserID: " & l_rstUsers("portaluserID") & IIf(l_rstUsers("portaluserID") = 0, vbCrLf & "If the PortaluserID is zero, please abort the send and re-enter the recipient in the Job Delivery Contacts Tab", "") & vbCrLf
        l_rstUsers.MoveNext
    Loop
    
    If l_blnNotifyUser = True Then
        l_strMessage = l_strMessage & vbCrLf & vbCrLf & "A notification will be sent to all the recipients, with the subject line:" & vbCrLf & l_strSubjectLine & vbCrLf
        If l_strMessageBody <> "" Then
            l_strMessage = l_strMessage & vbCrLf & vbCrLf & "And the following text will be at the start of the Notification message:" & vbCrLf & l_strMessageBody & vbCrLf
        End If
    End If
    
    If l_lngNewLibraryID <> 0 Then
        l_strMessage = l_strMessage & "And after delivery, the originals will all be moved to " & l_strNewLibraryBarcode & " in folder " & l_strNewFolder & vbCrLf
    End If
    
    l_strCETACompanyCodes = GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption)
    If InStr(l_strCETACompanyCodes, "/oneshot") <= 0 And InStr(l_strCETACompanyCodes, "/hours") <= 0 And InStr(l_strCETACompanyCodes, "/nooneshot") <= 0 Then
        SetData "company", "cetaclientcode", "companyID", lblCompanyID.Caption, l_strCETACompanyCodes & " /oneshotkill"
    End If
    
    l_lngCDDeliveryNumber = GetNextSequence("CDDeliveryNumber")
    If (optStorageType(4).Value = True Or lblFormat.Caption = "DIVA") Then
        l_lngAsperaLibraryID = GetDataSQL("SELECT TOP 1 libraryID FROM StorageUsage_Stores WHERE libraryID IN (441212, 623206, 722000) ORDER BY cdate DESC, FreeSpace DESC;")
        l_strAsperaFolder = lblCompanyID.Caption & "\" & l_lngDestinationJobID
    End If
    
    If MsgBox("Just send selected files (instead of whole grid)", vbYesNo + vbDefaultButton2, "Sending Grid") = vbYes Then
        
        If MsgBox("You are about to send the selected files to:" & vbCrLf & l_strMessage & vbCrLf & "Are you Sure", vbYesNo + vbDefaultButton2, "Final Confirmation") = vbNo Then
            l_rstUsers.Close
            Set l_rstUsers = Nothing
            Exit Sub
        End If
        
        l_lngCount = grdClips.SelBookmarks.Count
        
        If l_lngCount <= 0 Then
            MsgBox "No rows selected.", vbCritical, "Error with Send via Content Delivery"
            l_rstUsers.Close
            Set l_rstUsers = Nothing
            Exit Sub
        End If
        
        Count = 1
        
        For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1
            
            bkmrk = grdClips.SelBookmarks(l_lngCount)
            Set l_rsTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & Val(grdClips.Columns("eventID").CellText(bkmrk)) & ";", g_strExecuteError)
            CheckForSQLError
            If IsNull(l_rsTemp("eventtitle")) Or l_rsTemp("eventtitle") = "" Then
                MsgBox "Some of the select rows have not had titles details." & vbCrLf & "Files must have titles to send via Content Delivery." & vbCrLf & "Please fix this and then re-do.", vbCritical, "Error with Send via Content Delivery"
                Exit Sub
            End If
            Count = Count + 1
        Next
        
        If MsgBox("Send " & l_lngCount & " items - Are you sure", vbYesNo + vbDefaultButton2, "Send Via Content Delivery") = vbNo Then Exit Sub
        
        ProgressBar1.Max = grdClips.SelBookmarks.Count
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        Count = 1
        
        For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1
            
            bkmrk = grdClips.SelBookmarks(l_lngCount)
            Set l_rsTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & Val(grdClips.Columns("eventID").CellText(bkmrk)) & ";", g_strExecuteError)
            CheckForSQLError
            
            ProgressBar1.Value = Count
            DoEvents
            
            'Make a Content Delivery Send Request for this clip with this DestinationJobID
            If (optStorageType(0).Value = True Or lblFormat.Caption = "DISCSTORE") And l_rsTemp("libraryID") <> 659178 Then
                If l_rsTemp("libraryID") <> 722000 And l_rsTemp("libraryID") <> 722004 Then
                    If GetDataSQL("SELECT eventID FROM events WHERE system_deleted = 0 AND clipfilename = '" & QuoteSanitise(l_rsTemp("clipfilename")) & "' AND bigfilesize = " & l_rsTemp("bigfilesize") & _
                    " AND companyID = " & l_rsTemp("companyID") & "AND libraryID IN (722000, 722004);") <> "" Then
                        l_lngEventID = GetDataSQL("SELECT eventID FROM events WHERE system_deleted = 0 AND clipfilename = '" & QuoteSanitise(l_rsTemp("clipfilename")) & "' AND bigfilesize = " & l_rsTemp("bigfilesize") & _
                        " AND companyID = " & l_rsTemp("companyID") & "AND libraryID IN (722000, 722004);")
                        l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngEventID)
                    Else
                        l_lngEventID = l_rsTemp("eventID")
                        l_lngLibraryID = l_rsTemp("libraryID")
                    End If
                Else
                    l_lngEventID = l_rsTemp("eventID")
                    l_lngLibraryID = l_rsTemp("libraryID")
                End If
                l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, JobID, CompanyID, NotifyRecipient, NotifySubjectLine, NotifyMessageBody, CDDeliveryNumber, NewLibraryID, NewAltlocation, RequestName, RequesterEmail) VALUES ("
                l_strSQL = l_strSQL & l_lngEventID & ", "
                l_strSQL = l_strSQL & l_lngLibraryID & ", "
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Content Delivery") & ", "
                l_strSQL = l_strSQL & l_lngDestinationJobID & ", "
                l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
                l_strSQL = l_strSQL & IIf(l_lngCount = grdClips.SelBookmarks.Count - 1, CLng(l_blnNotifyUser), 0) & ", "
                l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strSubjectLine) & "'", "Null") & ", "
                l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strMessageBody) & "'", "Null") & ", "
                l_strSQL = l_strSQL & l_lngCDDeliveryNumber & ", "
                l_strSQL = l_strSQL & IIf(l_lngNewLibraryID <> 0, l_lngNewLibraryID & ", '" & l_strNewFolder & "', ", "Null, Null, ")
                l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, finalstatus) VALUES ("
                l_strSQL = l_strSQL & "1448, "
                l_strSQL = l_strSQL & l_rsTemp("companyID") & ", "
                l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                l_strSQL = l_strSQL & l_lngEventID & ", "
                l_strSQL = l_strSQL & "'Content Delivery to Job " & l_lngDestinationJobID & "', "
                l_strSQL = l_strSQL & "'Completed')"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            ElseIf (optStorageType(4).Value = True Or lblFormat.Caption = "DIVA") Then
                If GetDataSQL("SELECT eventID FROM events WHERE system_deleted = 0 AND clipfilename = '" & QuoteSanitise(l_rsTemp("clipfilename")) & "' AND bigfilesize = " & l_rsTemp("bigfilesize") & _
                " AND companyID = " & l_rsTemp("companyID") & "AND libraryID IN (722000, 722004);") <> "" Then
                    l_lngEventID = GetDataSQL("SELECT eventID FROM events WHERE system_deleted = 0 AND clipfilename = '" & QuoteSanitise(l_rsTemp("clipfilename")) & "' AND bigfilesize = " & l_rsTemp("bigfilesize") & _
                    " AND companyID = " & l_rsTemp("companyID") & "AND libraryID IN (722000, 722004);")
                    l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngEventID)
                    l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, JobID, CompanyID, NotifyRecipient, NotifySubjectLine, NotifyMessageBody, CDDeliveryNumber, NewLibraryID, NewAltlocation, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & l_lngEventID & ", "
                    l_strSQL = l_strSQL & l_lngLibraryID & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Content Delivery") & ", "
                    l_strSQL = l_strSQL & l_lngDestinationJobID & ", "
                    l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
                    l_strSQL = l_strSQL & IIf(l_lngCount = grdClips.SelBookmarks.Count - 1, CLng(l_blnNotifyUser), 0) & ", "
                    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strSubjectLine) & "'", "Null") & ", "
                    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strMessageBody) & "'", "Null") & ", "
                    l_strSQL = l_strSQL & l_lngCDDeliveryNumber & ", "
                    l_strSQL = l_strSQL & IIf(l_lngNewLibraryID <> 0, l_lngNewLibraryID & ", '" & l_strNewFolder & "', ", "Null, Null, ")
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, finalstatus) VALUES ("
                    l_strSQL = l_strSQL & "1448, "
                    l_strSQL = l_strSQL & l_rsTemp("companyID") & ", "
                    l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                    l_strSQL = l_strSQL & l_lngEventID & ", "
                    l_strSQL = l_strSQL & "'Content Delivery to Job " & l_lngDestinationJobID & "', "
                    l_strSQL = l_strSQL & "'Completed')"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                ElseIf GetDataSQL("SELECT eventID FROM vw_Events_on_DISCSTORE WHERE system_deleted = 0 AND clipfilename = '" & QuoteSanitise(l_rsTemp("clipfilename")) & "' AND bigfilesize = " & l_rsTemp("bigfilesize") & _
                " AND companyID = " & l_rsTemp("companyID") & "AND libraryID IN (719952, 719953, 719954, 719955, 740691, 447261, 770131, 770132, 770133, 770134, 770135, 770653);") <> "" Then
                    l_lngEventID = GetDataSQL("SELECT eventID FROM events WHERE system_deleted = 0 AND clipfilename = '" & QuoteSanitise(l_rsTemp("clipfilename")) & "' AND bigfilesize = " & l_rsTemp("bigfilesize") & _
                    " AND companyID = " & l_rsTemp("companyID") & "AND libraryID IN (719952, 719953, 719954, 719955, 740691, 447261, 770131, 770132, 770133, 770134, 770135, 770653);")
                    l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngEventID)
                    
                    l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, JobID, CompanyID, NotifyRecipient, NotifySubjectLine, NotifyMessageBody, CDDeliveryNumber, NewLibraryID, NewAltlocation, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & l_lngEventID & ", "
                    l_strSQL = l_strSQL & l_lngLibraryID & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Content Delivery") & ", "
                    l_strSQL = l_strSQL & l_lngDestinationJobID & ", "
                    l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
                    l_strSQL = l_strSQL & IIf(l_lngCount = grdClips.SelBookmarks.Count - 1, CLng(l_blnNotifyUser), 0) & ", "
                    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strSubjectLine) & "'", "Null") & ", "
                    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strMessageBody) & "'", "Null") & ", "
                    l_strSQL = l_strSQL & l_lngCDDeliveryNumber & ", "
                    l_strSQL = l_strSQL & IIf(l_lngNewLibraryID <> 0, l_lngNewLibraryID & ", '" & l_strNewFolder & "', ", "Null, Null, ")
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, finalstatus) VALUES ("
                    l_strSQL = l_strSQL & "1448, "
                    l_strSQL = l_strSQL & l_rsTemp("companyID") & ", "
                    l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                    l_strSQL = l_strSQL & l_lngEventID & ", "
                    l_strSQL = l_strSQL & "'Content Delivery to Job " & l_lngDestinationJobID & "', "
                    l_strSQL = l_strSQL & "'Completed')"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                Else
                    l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, JobID, CompanyID, NotifyRecipient, NotifySubjectLine, NotifyMessageBody, CDDeliveryNumber, NewFileID, NewLibraryID, FullPathToSourceFile, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & l_rsTemp("eventID") & ", "
                    l_strSQL = l_strSQL & l_rsTemp("libraryID") & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Content Delivery From DIVA") & ", "
                    l_lngNewEventID = CopyFileEventToLibraryID(l_rsTemp("eventID"), l_lngAsperaLibraryID, True)
                    l_strNewPath = GetData("library", "version", "libraryID", l_lngAsperaLibraryID) & "/" & Replace(l_strAsperaFolder, "\", "/") & "/" & l_rsTemp("clipfilename")
                    SetData "events", "altlocation", "eventID", l_lngNewEventID, l_strAsperaFolder
                    SetData "events", "bigfilesize", "eventID", l_lngNewEventID, Null
                    SetData "events", "soundlay", "eventID", l_lngNewEventID, Null
                    SetData "events", "online", "eventID", l_lngNewEventID, 1
                
                    l_strSQL = l_strSQL & l_lngDestinationJobID & ", "
                    l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
                    l_strSQL = l_strSQL & IIf(l_lngCount = grdClips.SelBookmarks.Count - 1, CLng(l_blnNotifyUser), 0) & ", "
                    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strSubjectLine) & "'", "Null") & ", "
                    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strMessageBody) & "'", "Null") & ", "
                    l_strSQL = l_strSQL & l_lngCDDeliveryNumber & ", "
                    l_strSQL = l_strSQL & l_lngNewEventID & ", "
                    l_strSQL = l_strSQL & IIf(l_lngAsperaLibraryID <> 0, l_lngAsperaLibraryID & ", '" & l_strNewPath & "', ", "Null, Null, ")
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, JobID, CompanyID, NotifyRecipient, NotifySubjectLine, NotifyMessageBody, CDDeliveryNumber, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & l_lngNewEventID & ", "
                    l_strSQL = l_strSQL & l_lngAsperaLibraryID & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Content Delivery") & ", "
                    l_strSQL = l_strSQL & l_lngDestinationJobID & ", "
                    l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
                    l_strSQL = l_strSQL & IIf(l_lngCount = grdClips.SelBookmarks.Count - 1, CLng(l_blnNotifyUser), 0) & ", "
                    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strSubjectLine) & "'", "Null") & ", "
                    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strMessageBody) & "'", "Null") & ", "
                    l_strSQL = l_strSQL & l_lngCDDeliveryNumber & ", "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, finalstatus) VALUES ("
                    l_strSQL = l_strSQL & "1448, "
                    l_strSQL = l_strSQL & l_rsTemp("companyID") & ", "
                    l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                    l_strSQL = l_strSQL & l_lngNewEventID & ", "
                    l_strSQL = l_strSQL & "'Content Delivery to Job " & l_lngDestinationJobID & "', "
                    l_strSQL = l_strSQL & "'Completed')"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                End If
            End If
            
            Count = Count + 1
        Next
        ProgressBar1.Visible = False
    Else
        
        If MsgBox("You are about to send the whole grid to:" & vbCrLf & l_strMessage & vbCrLf & "Are you Sure", vbYesNo + vbDefaultButton2, "Final Confirmation") = vbNo Then
            l_rstUsers.Close
            Set l_rstUsers = Nothing
            Exit Sub
        End If
                
        adoClip.Recordset.MoveFirst
        Do While Not adoClip.Recordset.EOF
            If IsNull(adoClip.Recordset("eventtitle") = "") Or adoClip.Recordset("eventtitle") = "" Then
                MsgBox "Some of the select rows have not had titles details." & vbCrLf & "Files must have titles to send via Content Delivery." & vbCrLf & "Please fix this and then re-do.", vbCritical, "Error with Send via Content Delivery"
                Exit Sub
            End If
            adoClip.Recordset.MoveNext
        Loop
        
        Count = 1
        ProgressBar1.Max = adoClip.Recordset.RecordCount
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        adoClip.Recordset.MoveLast
        l_lngLastClipID = adoClip.Recordset("eventID")
        adoClip.Recordset.MoveFirst
        Do While Not adoClip.Recordset.EOF
            ProgressBar1.Value = Count
            DoEvents
            'Make a Content Delivery Send Request for this clip with this DestinationJobID
            If (optStorageType(0).Value = True Or lblFormat.Caption = "DISCSTORE") And adoClip.Recordset("libraryID") <> 659178 Then
                If adoClip.Recordset("libraryID") <> 722000 And adoClip.Recordset("libraryID") <> 722004 Then
                    If GetDataSQL("SELECT eventID FROM events WHERE system_deleted = 0 AND clipfilename = '" & QuoteSanitise(adoClip.Recordset("clipfilename")) & "' AND bigfilesize = " & adoClip.Recordset("bigfilesize") & _
                    " AND companyID = " & adoClip.Recordset("companyID") & "AND libraryID IN (722000, 722004);") <> "" Then
                        l_lngEventID = GetDataSQL("SELECT eventID FROM events WHERE system_deleted = 0 AND clipfilename = '" & QuoteSanitise(adoClip.Recordset("clipfilename")) & "' AND bigfilesize = " & adoClip.Recordset("bigfilesize") & _
                        " AND companyID = " & adoClip.Recordset("companyID") & "AND libraryID IN (722000, 722004);")
                        l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngEventID)
                    Else
                        l_lngEventID = adoClip.Recordset("eventID")
                        l_lngLibraryID = adoClip.Recordset("libraryID")
                    End If
                End If
                l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, JobID, CompanyID, NotifyRecipient, NotifySubjectLine, NotifyMessageBody, CDDeliveryNumber, NewLibraryID, NewAltlocation, RequestName, RequesterEmail) VALUES ("
                l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                l_strSQL = l_strSQL & adoClip.Recordset("libraryID") & ", "
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Content Delivery") & ", "
                l_strSQL = l_strSQL & l_lngDestinationJobID & ", "
                l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
                l_strSQL = l_strSQL & IIf(adoClip.Recordset("eventID") = l_lngLastClipID, CLng(l_blnNotifyUser), 0) & ", "
                l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strSubjectLine) & "'", "Null") & ", "
                l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strMessageBody) & "'", "Null") & ", "
                l_strSQL = l_strSQL & l_lngCDDeliveryNumber & ", "
                l_strSQL = l_strSQL & IIf(l_lngNewLibraryID <> 0, l_lngNewLibraryID & ", '" & l_strNewFolder & "', ", "Null, Null, ")
                l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, finalstatus) VALUES ("
                l_strSQL = l_strSQL & "1448, "
                l_strSQL = l_strSQL & adoClip.Recordset("companyID") & ", "
                l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                l_strSQL = l_strSQL & "'Content Delivery to Job " & l_lngDestinationJobID & "', "
                l_strSQL = l_strSQL & "'Completed')"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            ElseIf (optStorageType(4).Value = True Or lblFormat.Caption = "DIVA") Then
                If GetDataSQL("SELECT eventID FROM events WHERE system_deleted = 0 AND clipfilename = '" & QuoteSanitise(adoClip.Recordset("clipfilename")) & "' AND bigfilesize = " & adoClip.Recordset("bigfilesize") & _
                " AND companyID = " & adoClip.Recordset("companyID") & "AND libraryID IN (722000, 722004);") <> "" Then
                    l_lngEventID = GetDataSQL("SELECT eventID FROM events WHERE system_deleted = 0 AND clipfilename = '" & QuoteSanitise(adoClip.Recordset("clipfilename")) & "' AND bigfilesize = " & adoClip.Recordset("bigfilesize") & _
                    " AND companyID = " & adoClip.Recordset("companyID") & "AND libraryID IN (722000, 722004);")
                    l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngEventID)
                    l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, JobID, CompanyID, NotifyRecipient, NotifySubjectLine, NotifyMessageBody, CDDeliveryNumber, NewLibraryID, NewAltlocation, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & l_lngEventID & ", "
                    l_strSQL = l_strSQL & l_lngLibraryID & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Content Delivery") & ", "
                    l_strSQL = l_strSQL & l_lngDestinationJobID & ", "
                    l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
                    l_strSQL = l_strSQL & IIf(adoClip.Recordset("eventID") = l_lngLastClipID, CLng(l_blnNotifyUser), 0) & ", "
                    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strSubjectLine) & "'", "Null") & ", "
                    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strMessageBody) & "'", "Null") & ", "
                    l_strSQL = l_strSQL & l_lngCDDeliveryNumber & ", "
                    l_strSQL = l_strSQL & IIf(l_lngNewLibraryID <> 0, l_lngNewLibraryID & ", '" & l_strNewFolder & "', ", "Null, Null, ")
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, finalstatus) VALUES ("
                    l_strSQL = l_strSQL & "1448, "
                    l_strSQL = l_strSQL & adoClip.Recordset("companyID") & ", "
                    l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                    l_strSQL = l_strSQL & l_lngEventID & ", "
                    l_strSQL = l_strSQL & "'Content Delivery to Job " & l_lngDestinationJobID & "', "
                    l_strSQL = l_strSQL & "'Completed')"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                ElseIf GetDataSQL("SELECT eventID FROM vw_Events_on_DISCSTORE WHERE system_deleted = 0 AND clipfilename = '" & QuoteSanitise(adoClip.Recordset("clipfilename")) & "' AND bigfilesize = " & adoClip.Recordset("bigfilesize") & _
                " AND companyID = " & adoClip.Recordset("companyID") & "AND libraryID IN (719952, 719953, 719954, 719955, 740691, 447261, 770131, 770132, 770133, 770134, 770135, 770653);") <> "" Then
                    l_lngEventID = GetDataSQL("SELECT eventID FROM events WHERE system_deleted = 0 AND clipfilename = '" & QuoteSanitise(adoClip.Recordset("clipfilename")) & "' AND bigfilesize = " & adoClip.Recordset("bigfilesize") & _
                    " AND companyID = " & adoClip.Recordset("companyID") & "AND libraryID IN (719952, 719953, 719954, 719955, 740691, 447261, 770131, 770132, 770133, 770134, 770135, 770653);")
                    l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngEventID)
                    
                    l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, JobID, CompanyID, NotifyRecipient, NotifySubjectLine, NotifyMessageBody, CDDeliveryNumber, NewLibraryID, NewAltlocation, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & l_lngEventID & ", "
                    l_strSQL = l_strSQL & l_lngLibraryID & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Content Delivery") & ", "
                    l_strSQL = l_strSQL & l_lngDestinationJobID & ", "
                    l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
                    l_strSQL = l_strSQL & IIf(adoClip.Recordset("eventID") = l_lngLastClipID, CLng(l_blnNotifyUser), 0) & ", "
                    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strSubjectLine) & "'", "Null") & ", "
                    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strMessageBody) & "'", "Null") & ", "
                    l_strSQL = l_strSQL & l_lngCDDeliveryNumber & ", "
                    l_strSQL = l_strSQL & IIf(l_lngNewLibraryID <> 0, l_lngNewLibraryID & ", '" & l_strNewFolder & "', ", "Null, Null, ")
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, finalstatus) VALUES ("
                    l_strSQL = l_strSQL & "1448, "
                    l_strSQL = l_strSQL & adoClip.Recordset("companyID") & ", "
                    l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                    l_strSQL = l_strSQL & l_lngEventID & ", "
                    l_strSQL = l_strSQL & "'Content Delivery to Job " & l_lngDestinationJobID & "', "
                    l_strSQL = l_strSQL & "'Completed')"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                Else
                    l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, JobID, CompanyID, NotifyRecipient, NotifySubjectLine, NotifyMessageBody, CDDeliveryNumber, NewFileID, NewLibraryID, FullPathToSourceFile, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                    l_strSQL = l_strSQL & adoClip.Recordset("libraryID") & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Content Delivery From DIVA") & ", "
                    l_lngNewEventID = CopyFileEventToLibraryID(adoClip.Recordset("eventID"), l_lngAsperaLibraryID, True)
                    l_strNewPath = GetData("library", "version", "libraryID", l_lngAsperaLibraryID) & "/" & Replace(l_strAsperaFolder, "\", "/") & "/" & adoClip.Recordset("clipfilename")
                    SetData "events", "altlocation", "eventID", l_lngNewEventID, l_strAsperaFolder
                    SetData "events", "bigfilesize", "eventID", l_lngNewEventID, Null
                    SetData "events", "soundlay", "eventID", l_lngNewEventID, Null
                    SetData "events", "online", "eventID", l_lngNewEventID, 1
                
                    l_strSQL = l_strSQL & l_lngDestinationJobID & ", "
                    l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
                    l_strSQL = l_strSQL & IIf(adoClip.Recordset("eventID") = l_lngLastClipID, CLng(l_blnNotifyUser), 0) & ", "
                    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strSubjectLine) & "'", "Null") & ", "
                    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strMessageBody) & "'", "Null") & ", "
                    l_strSQL = l_strSQL & l_lngCDDeliveryNumber & ", "
                    l_strSQL = l_strSQL & l_lngNewEventID & ", "
                    l_strSQL = l_strSQL & IIf(l_lngAsperaLibraryID <> 0, l_lngAsperaLibraryID & ", '" & l_strNewPath & "', ", "Null, Null, ")
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, JobID, CompanyID, NotifyRecipient, NotifySubjectLine, NotifyMessageBody, CDDeliveryNumber, RequestName, RequesterEmail) VALUES ("
                    l_strSQL = l_strSQL & l_lngNewEventID & ", "
                    l_strSQL = l_strSQL & l_lngAsperaLibraryID & ", "
                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Content Delivery") & ", "
                    l_strSQL = l_strSQL & l_lngDestinationJobID & ", "
                    l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
                    l_strSQL = l_strSQL & IIf(adoClip.Recordset("eventID") = l_lngLastClipID, CLng(l_blnNotifyUser), 0) & ", "
                    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strSubjectLine) & "'", "Null") & ", "
                    l_strSQL = l_strSQL & IIf(l_blnNotifyUser = True, "'" & QuoteSanitise(l_strMessageBody) & "'", "Null") & ", "
                    l_strSQL = l_strSQL & l_lngCDDeliveryNumber & ", "
                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, finalstatus) VALUES ("
                    l_strSQL = l_strSQL & "1448, "
                    l_strSQL = l_strSQL & adoClip.Recordset("companyID") & ", "
                    l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                    l_strSQL = l_strSQL & l_lngNewEventID & ", "
                    l_strSQL = l_strSQL & "'Content Delivery to Job " & l_lngDestinationJobID & "', "
                    l_strSQL = l_strSQL & "'Completed')"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                End If
            End If
            
            Count = Count + 1
            adoClip.Recordset.MoveNext
        Loop
        ProgressBar1.Visible = False
    End If
    If l_blnMarkJobComplete = True Then
        l_strSQL = "SELECT jobdetailID, rejecteddate, completeddate FROM jobdetail WHERE jobID = " & l_lngDestinationJobID
        Set l_rstJobLines = ExecuteSQL(l_strSQL, g_strExecuteError)
        If l_rstJobLines.RecordCount > 0 Then
            l_rstJobLines.MoveFirst
            Do While Not l_rstJobLines.EOF
                If IsNull(l_rstJobLines("rejecteddate")) And IsNull(l_rstJobLines("completeddate")) Then
                    If g_intUseTerseJobDetailCompletion = 1 Then
                        TerseCompleteJobDetailItem l_rstJobLines("jobdetailID")
                    Else
                        CompleteJobDetailItem l_rstJobLines("jobdetailID")
                    End If
                End If
                l_rstJobLines.MoveNext
            Loop
        End If
        l_rstJobLines.Close
        Set l_rstJobLines = Nothing
        UpdateJobStatus Val(l_lngDestinationJobID), "VT Done"
    End If
    MsgBox "Done"
    
    l_rstUsers.Close
    Set l_rstUsers = Nothing
    
Else

    MsgBox "No contacts specified for that job - cannot send files."
    l_rstUsers.Close
    Set l_rstUsers = Nothing

End If

End Sub

Private Sub cmdShowDangerous_Click()

If cmdShowDangerous.Caption = "Show Dangerous" Then
    frmDangerous.Visible = True
    cmdShowDangerous.Caption = "Hide Dangerous"
Else
    chkBulkUpdate(36).Value = 0
    chkBulkUpdate(37).Value = 0
    frmDangerous.Visible = False
    cmdShowDangerous.Caption = "Show Dangerous"
End If

End Sub

Private Sub cmdSpecialCommand_Click()

'declare what's needed
Dim c As ADODB.Connection, c2 As ADODB.Connection
Dim r As ADODB.Recordset, Message As String
Dim s As String
Dim Count As Long, RecordCount As Double
Dim l_lngClipID, l_strNewTimecodeStart, l_strNewTimecodeStop, l_strNewFD_Length

'If adoClip.Recordset.RecordCount <= 0 Then Exit Sub
If g_blnRedNetwork = False Then Exit Sub
If MsgBox("Special Command - currently hard deleting event records to a specific search criteria", vbYesNo + vbDefaultButton2) = vbNo Then Exit Sub

'build the SQL

s = "SELECT eventID FROM events_deleted_archive WHERE eventID IN (SELECT eventID FROM events WHERE system_deleted <> 0)"

'open the connection
Set c = New ADODB.Connection
c.Open g_strConnection
Set r = New ADODB.Recordset
r.Open s, c, 3, 3

If r.RecordCount > 0 Then
    r.MoveFirst
    Count = 1
    ProgressBar1.Max = r.RecordCount
    ProgressBar1.Value = 0
    ProgressBar1.Visible = True
    DoEvents
    
    'loop through all the records
    Do While Not r.EOF
        ProgressBar1.Value = Count
        Message = r.RecordCount & " Records total, Processing: " & Count & ", eventID: " & r("eventID")
        lblCurrentFilename.Caption = Message
        DoEvents
        
        If GetData("events", "eventID", "eventID", r("eventID")) = r("eventID") Then
            RecordCount = GetCount("SELECT Count(eventID) FROM events_iTunes WHERE eventID = " & r("eventID"))
            If RecordCount > 0 Then
                lblCurrentFilename.Caption = Message & ", " & RecordCount & " records in events_iTunes"
                DoEvents
                s = "DELETE FROM events_iTunes WHERE eventID = " & r("eventID")
                Debug.Print s
                c.Execute s
            End If
            RecordCount = GetCount("SELECT Count(eventID) FROM events_breaks WHERE eventID = " & r("eventID"))
            If RecordCount > 0 Then
                lblCurrentFilename.Caption = Message & ", " & RecordCount & " records in events_breaks"
                DoEvents
                s = "DELETE FROM events_breaks WHERE eventID = " & r("eventID")
                Debug.Print s
                c.Execute s
            End If
            RecordCount = GetCount("SELECT Count(eventID) FROM BP_Order_Detail WHERE eventID = " & r("eventID"))
            If RecordCount > 0 Then
                lblCurrentFilename.Caption = Message & ", " & RecordCount & " records in BP_Order_Detail"
                DoEvents
                s = "DELETE FROM BP_Order_Detail WHERE eventID = " & r("eventID")
                Debug.Print s
                c.Execute s
            End If
            RecordCount = GetCount("SELECT Count(eventID) FROM Event_File_Request WHERE eventID = " & r("eventID"))
            If RecordCount > 0 Then
                lblCurrentFilename.Caption = Message & ", " & RecordCount & " records in Event_File_Request"
                DoEvents
                s = "DELETE FROM Event_File_Request WHERE eventID = " & r("eventID")
                Debug.Print s
                c.Execute s
            End If
            RecordCount = GetCount("SELECT Count(eventID) FROM eventChecksumHistory WHERE eventID = " & r("eventID"))
            If RecordCount > 0 Then
                lblCurrentFilename.Caption = Message & ", " & RecordCount & " records in eventChecksumHistory"
                DoEvents
                s = "DELETE FROM eventChecksumHistory WHERE eventID = " & r("eventID")
                Debug.Print s
                c.Execute s
            End If
            RecordCount = GetCount("SELECT Count(eventID) FROM eventcompilation WHERE eventID = " & r("eventID"))
            If RecordCount > 0 Then
                lblCurrentFilename.Caption = Message & ", " & RecordCount & " records in eventcompilation"
                DoEvents
                s = "DELETE FROM eventcompilation WHERE eventID = " & r("eventID")
                Debug.Print s
                c.Execute s
            End If
            RecordCount = GetCount("SELECT Count(eventID) FROM eventcompilation WHERE eventID = " & r("eventID"))
            If RecordCount > 0 Then
                lblCurrentFilename.Caption = Message & ", " & RecordCount & " records with sourceclipID in eventcompilation"
                DoEvents
                s = "DELETE FROM eventcompilation WHERE sourceclipID = " & r("eventID")
                Debug.Print s
                c.Execute s
            End If
            RecordCount = GetCount("SELECT Count(eventID) FROM eventhistory WHERE eventID = " & r("eventID"))
            If RecordCount > 0 Then
                lblCurrentFilename.Caption = Message & ", " & RecordCount & " records in eventhistory"
                DoEvents
                s = "DELETE FROM eventhistory WHERE eventID = " & r("eventID")
                Debug.Print s
                c.Execute s
            End If
            RecordCount = GetCount("SELECT Count(eventID) FROM eventlogging WHERE eventID = " & r("eventID"))
            If RecordCount > 0 Then
                lblCurrentFilename.Caption = Message & ", " & RecordCount & " records in eventlogging"
                DoEvents
                s = "DELETE FROM eventlogging WHERE eventID = " & r("eventID")
                Debug.Print s
                c.Execute s
            End If
            RecordCount = GetCount("SELECT Count(eventID) FROM eventsegment WHERE eventID = " & r("eventID"))
            If RecordCount > 0 Then
                lblCurrentFilename.Caption = Message & ", " & RecordCount & " records in eventsegment"
                DoEvents
                s = "DELETE FROM eventsegment WHERE eventID = " & r("eventID")
                Debug.Print s
                c.Execute s
            End If
            RecordCount = GetCount("SELECT Count(eventID) FROM eventusage WHERE eventID = " & r("eventID"))
            If RecordCount > 0 Then
                lblCurrentFilename.Caption = Message & ", " & RecordCount & " records in eventusage"
                DoEvents
                s = "DELETE FROM eventusage WHERE eventID = " & r("eventID")
                Debug.Print s
                c.Execute s
            End If
            RecordCount = GetCount("SELECT Count(eventID) FROM Excel_Techrev_Output_Request WHERE eventID = " & r("eventID"))
            If RecordCount > 0 Then
                lblCurrentFilename.Caption = Message & ", " & RecordCount & " records in Excel_Techrev_Output_Request"
                DoEvents
                s = "DELETE FROM Excel_Techrev_Output_Request WHERE eventID = " & r("eventID")
                Debug.Print s
                c.Execute s
            End If
            RecordCount = GetCount("SELECT Count(eventID) FROM techrev WHERE eventID = " & r("eventID"))
            If RecordCount > 0 Then
                lblCurrentFilename.Caption = Message & ", " & RecordCount & " records in techrev"
                DoEvents
                s = "UPDATE techrev SET eventID = NULL WHERE eventID = " & r("eventID")
                Debug.Print s
                c.Execute s
            End If
            lblCurrentFilename.Caption = Message & " deleting event record"
            DoEvents
            s = "DELETE FROM events WHERE eventID = " & r("eventID")
            Debug.Print s
            c.Execute s
        End If
        
        'move to the next record
        Count = Count + 1
        r.MoveNext

    Loop

End If

'close the recordset
r.Close
Set r = Nothing

'close the connection
c.Close
Set c = Nothing
lblCurrentFilename.Caption = ""
ProgressBar1.Visible = False

cmdSearch.Value = True

End Sub

Private Sub cmdTransferToGrid_Click()

If adoPatheClips.Recordset.RecordCount < 1 Then
    MsgBox "Cannot transfer clips if there aren't any"
    Exit Sub
End If

ClearFields Me

txtSQLQuery.Text = "eventID in (SELECT eventID FROM BP_Order_Detail WHERE Order_ID = " & lblOrderID.Caption & ")"
lblCompanyID.Caption = "905"
cmdSearch_Click
fraPatheOrder.Visible = False

End Sub

Private Sub cmdVerifyGrid_Click()

'declare what's needed
Dim c As ADODB.Connection, c2 As ADODB.Connection
Dim r As ADODB.Recordset
Dim s As String
Dim F As String
Dim FileNumber As Long, FileSize As Currency
Dim Count As Long
Dim l_strRootPath As String
Dim FSO As FileSystemObject, fil As File, FileModifiedDate As Date

If adoClip.Recordset.RecordCount <= 0 Then Exit Sub

'build the SQL

s = "SELECT "
If Val(txtSelectTop.Text) <> 0 Then s = s & "TOP " & Val(txtSelectTop.Text)
s = s & " events.eventID, events.libraryID, events.altlocation, events.clipfilename "
s = s & " FROM events WHERE 1=1 " & m_strSearchConditions
s = s & " AND (events.libraryID in (select libraryID from library WHERE format = 'DISCSTORE' and system_deleted = 0))"
s = s & " AND (events.clipfilename <> 'ZZZZZZ' )"
s = s & " ORDER BY events.eventID DESC;"

'open the connection
Set c = New ADODB.Connection
c.Open g_strConnection
Set c2 = New ADODB.Connection
c2.Open g_strConnection
Set r = New ADODB.Recordset
r.Open s, c, 3, 3

If r.RecordCount > 0 Then
    r.MoveFirst
    Count = 1
    ProgressBar1.Max = r.RecordCount
    ProgressBar1.Value = 0
    ProgressBar1.Visible = True
    Set FSO = New FileSystemObject
    DoEvents
    
    'loop through all the records
    Do While Not r.EOF
        ProgressBar1.Value = Count
        
        If chkSupressValidateMessages.Value = 0 Then
            If Not ValidateFilename(r("clipfilename")) Then
                If MsgBox("Illegal characters in filename: " & r("clipFileName") & vbCrLf & "Abort Verify?", vbYesNo) = vbYes Then Exit Do
            End If
            
            If Not ValidateFoldername(r("altlocation")) Then
                If MsgBox("Illegal characters somewhere in path folder names: " & r("altlocation") & vbCrLf & "Abort Verify?", vbYesNo) = vbYes Then Exit Do
            End If
        End If
        
        'build the full file name
        l_strRootPath = GetData("library", "subtitle", "libraryID", Val(r("libraryid")))
        F = l_strRootPath & "\" & r("altlocation") & "\" & r("clipfilename")
        lblCurrentFilename.Caption = F
        DoEvents
        
        'check if the file exists if it's not on an SMV discstore
        If r("libraryID") <> 445810 Then
'            If FileExists(F, r("clipfilename")) Then
'
'                'if it does, then set the sound lay to be the date/time
'                On Error GoTo VERIFY_FAILED
'                API_OpenFile F, FileNumber, FileSize
'                API_CloseFile FileNumber
'                If FileNumber <> -1 Then
'                    Set fil = FSO.GetFile(F)
'                    FileModifiedDate = fil.DateLastModified
'                    Set fil = Nothing
'                End If
'                c2.Execute "UPDATE events SET soundlay = '" & Month(Now) & "/" & Day(Now) & "/" & Year(Now) & " " & Hour(Now) & ":" & Minute(Now) & ":" & Second(Now) & "' WHERE eventID = '" & r("eventID") & "';"
'                c2.Execute "UPDATE events SET bigfilesize = " & FileSize & " WHERE eventID = '" & r("eventID") & "';"
'                c2.Execute "UPDATE events SET FileModifiedDate = '" & FormatSQLDate(FileModifiedDate) & "' WHERE eventID = '" & r("eventID") & "';"
'                DoEvents
'            Else
'VERIFY_FAILED:
'                c2.Execute "UPDATE events SET soundlay = NULL, bigfilesize = NULL WHERE eventID = '" & r("eventID") & "';"
'                DoEvents
'            End If
            VerifyClip r("eventID"), Trim(" " & r("altlocation")), Trim(" " & r("clipfilename")), r("libraryID"), True, True
        End If
        
        'move to the next record
        Count = Count + 1
        r.MoveNext
    
    Loop
    Set FSO = Nothing
End If

'close the recordset
r.Close
Set r = Nothing

'close the connection
c.Close
Set c = Nothing
c2.Close
Set c2 = Nothing
lblCurrentFilename.Caption = ""
If g_blnRedNetwork = True Then
    MsgBox "Finished.", vbInformation
Else
    MsgBox "Verification Requests Submitted.", vbInformation
End If
ProgressBar1.Visible = False

cmdSearch.Value = True

End Sub

Private Sub cmbInterlace_GotFocus()
PopulateCombo "interlace", cmbInterlace
HighLite cmbInterlace
End Sub

Private Sub cmdYouTubeXML_Click()

'declare what's needed
Dim r As ADODB.Recordset, rsKeyword As ADODB.Recordset
Dim s As String
Dim F As String
Dim Count As Long
Dim XMLfile As String

Dim XMLcomplex As VbMsgBoxResult

XMLcomplex = MsgBox("Should they be Complex Style (Fingerprinting and Usage)", vbYesNo, "Generating YouTubeXMLs")

If adoClip.Recordset.RecordCount <= 0 Then Exit Sub

'build the SQL
s = "SELECT events.eventID, events.libraryID, library.subtitle, library.format, events.altlocation, events.clipfilename, events.clipreference, events.eventtitle, events.notes, "
s = s & " events.customfield1, events.customfield4, events.companyID "
s = s & " FROM library INNER JOIN events ON library.libraryID = events.libraryID WHERE 1 = 1 " & m_strSearchConditions
s = s & " AND (events.clipfilename <> 'ZZZZZZ' )"
s = s & " ORDER BY events.eventID DESC;"

'open the connection
Set r = ExecuteSQL(s, g_strExecuteError)
CheckForSQLError

If r.RecordCount > 0 Then
    fraYouTubeWarning.Top = 840
    fraYouTubeWarning.Left = 8640
    fraYouTubeWarning.Visible = True
    
    r.MoveFirst
    Count = 0
    ProgressBar1.Max = r.RecordCount
    ProgressBar1.Value = 0
    ProgressBar1.Visible = True
    DoEvents
    
    'loop through all the records
    Do While Not r.EOF
        'build the full file name
        F = r("subtitle") & "\" & r("altlocation") & "\" & r("clipfilename")
        lblCurrentFilename.Caption = F
        DoEvents
        
        'check if the file exists if it's not on an SMV discstore
        If r("format") = "DISCSTORE" And r("libraryID") <> 445810 Then
            If FileExists(F, r("clipfilename")) Then
            
                If XMLcomplex = vbYes Then
                    
                    XMLfile = r("subtitle") & IIf(Trim(r("altlocation")) <> "", "\" & r("altlocation"), "") & "\" & r("clipreference") & ".xml"
                    Open XMLfile For Output As 1
                    
                    Print #1, "<?xml version=""1.0"" encoding=""UTF-8""?>"
                    Print #1, "<feed xmlns=""http://www.youtube.com/schemas/cms/2.0"" notification_email=""" & g_strOperationsEmailAddress & """>"
                    WriteTabs 1
                    Print #1, "<asset type=""web"">"
                    WriteTabs 2
                    Print #1, "<custom_id>" & r("clipreference") & "</custom_id>"
                    WriteTabs 2
                    Print #1, "<description>" & UTF8_Encode(XMLSanitise(r("notes"))) & "</description>"
                    WriteTabs 2
                    Print #1, "<notes></notes>"
                    WriteTabs 2
                    Print #1, "<title>" & UTF8_Encode(XMLSanitise(r("eventtitle"))) & "</title>"
                    WriteTabs 2
                    Print #1, "<url>www.britishpathe.com</url>"
                    WriteTabs 1
                    Print #1, "</asset>"
                    
                    WriteTabs 1
                    Print #1, "<file type=""video"">"
                    WriteTabs 2
                    Print #1, "<filename>" & r("clipfilename") & "</filename>"
                    WriteTabs 1
                    Print #1, "</file>"
                    
                    WriteTabs 1
                    Print #1, "<video>"
                    WriteTabs 2
                    Print #1, "<channel>britishpathe</channel>"
                    WriteTabs 2
                    Print #1, "<notify_subscribers>false</notify_subscribers>"
                    WriteTabs 2
                    Print #1, "<title>" & UTF8_Encode(XMLSanitise(r("eventtitle"))) & "</title>"
                    WriteTabs 2
                    Print #1, "<description>" & UTF8_Encode(XMLSanitise(r("notes"))) & "</description>"
                    WriteTabs 2
                    Print #1, "<keyword>" & r("customfield1") & "</keyword>"
                    WriteTabs 2
                    Print #1, "<keyword>" & r("customfield4") & "</keyword>"
                    
                    'add other keywords
                    Set rsKeyword = ExecuteSQL("SELECT * FROM eventkeyword WHERE companyID = " & r("companyID") & " AND clipreference = '" & r("clipreference") & "' ORDER BY eventkeywordID DESC;", g_strExecuteError)
                    CheckForSQLError
                    If rsKeyword.RecordCount > 0 Then
                        Do While Not rsKeyword.EOF
                            WriteTabs 2
                            Print #1, "<keyword>" & UTF8_Encode(XMLSanitise(rsKeyword("keywordtext"))) & "</keyword>"
                            rsKeyword.MoveNext
                        Loop
                    End If
                    rsKeyword.Close
                    Set rsKeyword = Nothing
                    
                    WriteTabs 2
                    Print #1, "<allow_comments>Approve</allow_comments>"
                    WriteTabs 2
                    Print #1, "<allow_embedding>True</allow_embedding>"
                    WriteTabs 2
                    Print #1, "<allow_ratings>True</allow_ratings>"
                    WriteTabs 2
                    Print #1, "<allow_responses>Approve</allow_responses>"
                    WriteTabs 2
                    Print #1, "<genre>Entertainment</genre>"
                    WriteTabs 2
                    Print #1, "<public>False</public>"
                    WriteTabs 1
                    Print #1, "</video>"
                    
'                    WriteTabs 1
'                    Print #1, "<relationship>"
'                    WriteTabs 2
'                    Print #1, "<item path=""/feed/asset[1]""/>"
'                    WriteTabs 2
'                    Print #1, "<related_item path=""/feed/file[1]""/>"
'                    WriteTabs 1
'                    Print #1, "</relationship>"
'
                    WriteTabs 1
                    Print #1, "<relationship>"
                    WriteTabs 2
                    Print #1, "<item path=""/feed/video[1]""/>"
                    WriteTabs 2
                    Print #1, "<related_item path=""/feed/file[1]""/>"
                    WriteTabs 1
                    Print #1, "</relationship>"
                    
                    WriteTabs 1
                    Print #1, "<ownership/>"
                    
                    WriteTabs 1
                    Print #1, "<relationship>"
                    WriteTabs 2
                    Print #1, "<item path=""/feed/asset[1]""/>"
                    WriteTabs 2
                    Print #1, "<related_item path=""/feed/ownership[1]""/>"
                    WriteTabs 1
                    Print #1, "</relationship>"
    
                    WriteTabs 1
                    Print #1, "<rights_admin type=""usage"" owner=""True""/>"
'                    WriteTabs 1
'                    Print #1, "<rights_admin type=""match"" owner=""True""/>"
    
                    WriteTabs 1
                    Print #1, "<claim type=""audiovisual"" asset=""/feed/asset[1]"" video=""/feed/video[1]"" rights_admin=""/feed/rights_admin[@type='usage']"" rights_policy=""/external/rights_policy[@name='Block in all countries']""/>"
                    
'                    WriteTabs 1
'                    Print #1, "<relationship>"
'                    WriteTabs 2
'                    Print #1, "<item path=""/feed/rights_admin[@type='match']""/>"
'                    WriteTabs 2
'                    Print #1, "<item path=""/external/rights_policy[@name='Monetize in all countries']""/>"
'                    WriteTabs 2
'                    Print #1, "<related_item path=""/feed/asset[1]""/>"
'                    WriteTabs 1
'                    Print #1, "</relationship>"
'
                    Print #1, "</feed>"
                    Close #1
                
                Else
                    
                    XMLfile = r("subtitle") & IIf(Trim(r("altlocation")) <> "", "\" & r("altlocation"), "") & "\" & r("clipreference") & ".xml"
                    Open XMLfile For Output As 1
                    
                    Print #1, "<?xml version=""1.0""?>"
                    Print #1, "<feed xmlns=""http://www.youtube.com/schemas/cms/2.0"" notification_email=""" & g_strOperationsEmailAddress & """>"
                    WriteTabs 1
                    Print #1, "<asset type=""web"">"
                    WriteTabs 2
                    Print #1, "<custom_id>" & r("clipreference") & "</custom_id>"
                    WriteTabs 2
                    Print #1, "<description>" & UTF8_Encode(XMLSanitise(r("notes"))) & "</description>"
                    WriteTabs 2
                    Print #1, "<notes></notes>"
                    WriteTabs 2
                    Print #1, "<title>" & UTF8_Encode(XMLSanitise(r("eventtitle"))) & "</title>"
                    WriteTabs 2
                    Print #1, "<url>www.britishpathe.com</url>"
                    WriteTabs 2
                    Print #1, "</asset>"
                    
                    WriteTabs 1
                    Print #1, "<file type=""video"">"
                    WriteTabs 2
                    Print #1, "<filename>" & r("clipfilename") & "</filename>"
                    WriteTabs 1
                    Print #1, "</file>"
                    
                    WriteTabs 1
                    Print #1, "<relationship>"
                    WriteTabs 2
                    Print #1, "<item path=""/feed/asset[1]""/>"
                    WriteTabs 2
                    Print #1, "<related_item path=""/feed/file[1]""/>"
                    WriteTabs 1
                    Print #1, "</relationship>"
                    
                    WriteTabs 1
                    Print #1, "<ownership/>"
                    
                    WriteTabs 1
                    Print #1, "<relationship>"
                    WriteTabs 2
                    Print #1, "<item path=""/feed/asset[1]""/>"
                    WriteTabs 2
                    Print #1, "<related_item path=""/feed/ownership[1]""/>"
                    WriteTabs 1
                    Print #1, "</relationship>"
    
                    WriteTabs 1
                    Print #1, "<rights_admin type=""match"" owner=""True""/>"
    
                    WriteTabs 1
                    Print #1, "<relationship>"
                    WriteTabs 2
                    Print #1, "<item path=""/feed/rights_admin[@type='match']""/>"
                    WriteTabs 2
                    Print #1, "<item path=""/external/rights_policy[@name='Monetize in all countries']""/>"
                    WriteTabs 2
                    Print #1, "<related_item path=""/feed/asset[1]""/>"
                    WriteTabs 1
                    Print #1, "</relationship>"
                    
                    Print #1, "</feed>"
                    Close #1

                End If
            
            End If
        
        End If
        
        'move to the next record
        Count = Count + 1
        r.MoveNext
        ProgressBar1.Value = Count
        DoEvents
    
    Loop
End If

'close the recordset
r.Close
Set r = Nothing

'close the connection
lblCurrentFilename.Caption = ""
MsgBox "Finished.", vbInformation
ProgressBar1.Visible = False
fraYouTubeWarning.Visible = False

cmdSearch.Value = True

End Sub

Private Sub cmdZeroUsage_Click()

Dim l_rstUsage As ADODB.Recordset, l_rstSource As ADODB.Recordset, l_strSearch As String

Set l_rstSource = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
CheckForSQLError

If l_rstSource.RecordCount > 0 Then
    l_rstSource.MoveFirst
    Do While Not l_rstSource.EOF
        Set l_rstUsage = ExecuteSQL("SELECT * FROM eventusage WHERE eventID = " & l_rstSource("eventID") & ";", g_strExecuteError)
        CheckForSQLError
        If l_rstUsage.RecordCount <= 0 Then
            lblCurrentFilename.Caption = "Needs Zero Date Usage - " & l_rstSource("clipfilename")
            DoEvents
            ExecuteSQL "INSERT INTO eventusage (eventID, dateused) VALUES (" & l_rstSource("eventID") & ", 0);", g_strExecuteError
            CheckForSQLError
        Else
            lblCurrentFilename.Caption = "Has Zero Date Usage - " & l_rstSource("clipfilename")
            DoEvents
        End If
        l_rstUsage.Close
        l_rstSource.MoveNext
    Loop
End If

Set l_rstUsage = Nothing
l_rstSource.Close
Set l_rstSource = Nothing

lblCurrentFilename.Caption = ""
DoEvents
MsgBox "Done"

End Sub

Private Sub cmdFTPToSmartjog_Click()

Dim l_strSQL As String, Count As Long, l_blnRTE As Boolean
Dim l_lngClipID As Long, l_rstTemp As ADODB.Recordset, l_rstClips As ADODB.Recordset, FSO As Scripting.FileSystemObject, l_lngCount As Long, bkmrk As Variant

If lblLibraryID.Caption = "" Then
    If MsgBox("You haven't selected a source drive for this search." & vbCrLf & "You must select a source drive to perform actual File Requests." & vbCrLf & "Do you wish to continue?", vbYesNo, "Question...") = vbNo Then
        Exit Sub
    End If
End If

If MsgBox("Is this an Eastenders RTE Send?", vbYesNo + vbDefaultButton2, "Which Type of Smartjog Send") = vbYes Then
    l_blnRTE = True
End If

If MsgBox("Just send selected files (instead of whole grid)", vbYesNo + vbDefaultButton2, "Sending Grid") = vbYes Then
    
    l_lngCount = grdClips.SelBookmarks.Count
    
    If l_lngCount <= 0 Then
        MsgBox "No rows selected.", vbCritical, "Error with Send via Smartjog"
        Exit Sub
    End If
    
    If MsgBox("Send " & l_lngCount & " items - Are you sure", vbYesNo + vbDefaultButton2, "FTP to Smartjog") = vbNo Then Exit Sub
    
    ProgressBar1.Max = grdClips.SelBookmarks.Count
    ProgressBar1.Value = 0
    ProgressBar1.Visible = True
    Count = 1
    
    For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1
        
        bkmrk = grdClips.SelBookmarks(l_lngCount)
        Set l_rstTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & Val(grdClips.Columns("eventID").CellText(bkmrk)) & ";", g_strExecuteError)
        CheckForSQLError
        
        ProgressBar1.Value = Count
        DoEvents
            
        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, RequestName, RequesterEmail) VALUES ("
        l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
        l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
        If l_blnRTE = True Then
            l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "FTP to RTE Smartjog") & ", "
        Else
            l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "FTP to Smartjog") & ", "
        End If
        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
            
        l_rstTemp.Close
        Set l_rstTemp = Nothing
        
        Count = Count + 1
    
    Next

Else

    If MsgBox("You are about to send the whole grid" & vbCrLf & "Are you Sure", vbYesNo + vbDefaultButton2, "Final Confirmation") = vbNo Then
        Exit Sub
    End If
            
    Set l_rstClips = ExecuteSQL(adoClip.RecordSource, g_strExecuteError)
    CheckForSQLError
    If l_rstClips.RecordCount > 0 Then
        l_rstClips.MoveFirst
        Count = 0
        ProgressBar1.Max = l_rstClips.RecordCount
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        Do While Not l_rstClips.EOF
            Set l_rstTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & l_rstClips("eventID"), g_strExecuteError)
            CheckForSQLError
            
            'Update the progress label
            ProgressBar1.Value = Count
            lblCurrentFilename.Caption = l_rstTemp("clipfilename")
            DoEvents
            
            l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, RequestName, RequesterEmail) VALUES ("
            l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
            l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
            If l_blnRTE = True Then
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "FTP to RTE Smartjog") & ", "
            Else
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "FTP to Smartjog") & ", "
            End If
            l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
                
            l_rstTemp.Close
            Set l_rstTemp = Nothing
            
            Count = Count + 1
            l_rstClips.MoveNext
            
        Loop
    End If
    
    l_rstClips.Close
    Set l_rstClips = Nothing
End If
ProgressBar1.Visible = False
lblCurrentFilename.Caption = ""
DoEvents

MsgBox "Done!"

End Sub

Private Sub cmdSendVia360_Click()

'Has to be some records int he grid
If adoClip.Recordset.RecordCount <= 0 Then Exit Sub

'Has to be a companyID
If Val(lblCompanyID.Caption) = 0 Then
    MsgBox "A Single client must be selected for File Sends.", vbCritical
    Exit Sub
End If

'Has to be from either a DISCSTORE or the DIVA
If (optStorageType(0).Value = False And lblFormat.Caption <> "DISCSTORE") And (optStorageType(4).Value = False And lblFormat.Caption <> "DIVA") Then
    MsgBox "We can only Delivery items that are on a DISCSTORE or the DIVA at this time.", vbCritical
    Exit Sub
End If

'Give the fitst 'Are You Sure?'
If MsgBox("You are about to send these files to an Aspera Recipient." & vbCrLf & "Are you Sure", vbYesNo) = vbNo Then Exit Sub

Dim l_strDestinationID As String, l_strDestinationDropfolder As String, l_lngDestinationLibraryID As Long, l_strSQL As String, Count As Long, l_strDestinationSubfolder As String
Dim l_rstTemp As ADODB.Recordset, bkmrk As Variant, l_lngCount As Long, l_strNewRestorePath As String, l_lngNewEventID As Long, l_strDestinationSecondaryFolder As String

frmGetDestination.lblDestinationType.Caption = "Aspera360"
frmGetDestination.Show vbModal
l_strDestinationID = frmGetDestination.cmbDestination.Columns("DestinationName").Text
Unload frmGetDestination

'If a destination was chosen...
If l_strDestinationID <> "" Then
    If MsgBox("You are about to send these files to " & l_strDestinationID & "." & vbCrLf & "Are you Sure", vbYesNo) = vbNo Then Exit Sub
    l_strDestinationDropfolder = GetDataSQL("SELECT TOP 1 DestinationDropfolder FROM DigitalDestination WHERE DestinationType = 'Aspera360' and DestinationName = '" & l_strDestinationID & "'")
    If Trim(" " & GetDataSQL("SELECT TOP 1 DestinationSubfolder FROM DigitalDestination WHERE DestinationType = 'Aspera360' and DestinationName = '" & l_strDestinationID & "'")) <> "" Then
        l_strDestinationSubfolder = GetDataSQL("SELECT TOP 1 DestinationSubfolder FROM DigitalDestination WHERE DestinationType = 'Aspera360' and DestinationName = '" & l_strDestinationID & "'")
        Select Case True
        
            Case InStr(l_strDestinationSubfolder, "{Date}") > 0
                l_strDestinationDropfolder = l_strDestinationDropfolder & "\" & Format(Now, "YYYY-MM-DD")
            Case InStr(l_strDestinationSubfolder, "{DateTime}") > 0
                l_strDestinationDropfolder = l_strDestinationDropfolder & "\" & Format(Now, "YYYY-MM-DD") & "_" & Format(Now, "HH-NN-SS")
            Case InStr(l_strDestinationSubfolder, "{") <= 0
                l_strDestinationDropfolder = l_strDestinationDropfolder & "\" & l_strDestinationSubfolder
            Case Else
                'Do Nothing - we don't process that key here
        End Select
        
    End If
    
    If Trim(" " & GetDataSQL("SELECT TOP 1 DestinationSecondaryFolder FROM DigitalDestination WHERE DestinationType = 'Aspera360' and DestinationName = '" & l_strDestinationID & "'")) <> "" Then
        l_strDestinationSecondaryFolder = GetDataSQL("SELECT TOP 1 DestinationSecondaryFolder FROM DigitalDestination WHERE DestinationType = 'Aspera1360 and DestinationName = '" & l_strDestinationID & "'")
    End If
    
    l_lngDestinationLibraryID = GetData("library", "libraryID", "barcode", g_strAsperaClient360SendPath)
    l_strNewRestorePath = GetData("library", "version", "libraryID", l_lngDestinationLibraryID) & "/" & SlashBackToForward(l_strDestinationDropfolder)
    
    If MsgBox("Just send selected files (instead of whole grid)", vbYesNo + vbDefaultButton2, "Sending Grid") = vbYes Then
        
        l_lngCount = grdClips.SelBookmarks.Count
        
        If l_lngCount <= 0 Then
            MsgBox "No rows selected.", vbCritical, "Error with Send via Aspera"
            Exit Sub
        End If
        
        If MsgBox("Send " & l_lngCount & " items - Are you sure", vbYesNo + vbDefaultButton2) = vbNo Then Exit Sub
        
        ProgressBar1.Max = grdClips.SelBookmarks.Count
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        Count = 1
        
        If (optStorageType(0).Value = True Or lblFormat.Caption = "DISCSTORE") Then
            For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1
                bkmrk = grdClips.SelBookmarks(l_lngCount)
                Set l_rstTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & Val(grdClips.Columns("eventID").CellText(bkmrk)) & ";", g_strExecuteError)
                CheckForSQLError
                
                ProgressBar1.Value = Count
                DoEvents
                    
                'Invoke a copy to the hotfolder location
                l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
                l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                l_strSQL = l_strSQL & l_rstTemp("LibraryID") & ", "
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
                l_strSQL = l_strSQL & l_lngDestinationLibraryID & ", "
                l_strSQL = l_strSQL & "'"
                l_strSQL = l_strSQL & l_strDestinationDropfolder
                If InStr(l_strDestinationSubfolder, "{clipreference}") > 0 Then
                    l_strSQL = l_strSQL & "\" & QuoteSanitise(l_rstTemp("clipreference"))
                End If
                If InStr(l_strDestinationSecondaryFolder, "{movielabs_key}") > 0 And Right(l_rstTemp("clipfilename"), 4) <> ".xml" Then
                    l_strSQL = l_strSQL & "\resources"
                End If
                l_strSQL = l_strSQL & "', "
                l_strSQL = l_strSQL & "1, "
                l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                
                'Then make a placeholder delivery record
                l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, finalstatus) VALUES ("
                l_strSQL = l_strSQL & "2753, "
                l_strSQL = l_strSQL & l_rstTemp("companyID") & ", "
                l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                l_strSQL = l_strSQL & "'Aspera to " & l_strDestinationID & "', "
                l_strSQL = l_strSQL & "'Completed')"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                Count = Count + 1
                
            Next
        ElseIf (optStorageType(4).Value = True Or lblFormat.Caption = "DIVA") Then
            For l_lngCount = 0 To grdClips.SelBookmarks.Count - 1
                bkmrk = grdClips.SelBookmarks(l_lngCount)
                Set l_rstTemp = ExecuteSQL("SELECT * FROM events WHERE eventID = " & Val(grdClips.Columns("eventID").CellText(bkmrk)) & ";", g_strExecuteError)
                CheckForSQLError
                
                ProgressBar1.Value = Count
                DoEvents
                
                'Invoke a restore to the hotfolder location
                l_lngNewEventID = CopyFileEventToLibraryID(l_rstTemp("eventID"), l_lngDestinationLibraryID, True)
                SetData "events", "altlocation", "eventID", l_lngNewEventID, l_strDestinationDropfolder
                SetData "events", "bigfilesize", "eventID", l_lngNewEventID, Null
                SetData "events", "soundlay", "eventID", l_lngNewEventID, Null
                SetData "events", "online", "eventID", l_lngNewEventID, 1
                l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewFileID, FullPathToSourceFile, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
                l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                l_strSQL = l_strSQL & l_rstTemp("libraryID") & ", "
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "DIVA To CETA") & ", "
                l_strSQL = l_strSQL & l_lngNewEventID & ", "
                l_strSQL = l_strSQL & "'" & l_strNewRestorePath & "/" & l_rstTemp("clipfilename") & "', "
                l_strSQL = l_strSQL & "1, "
                l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                
                'Then make a placeholder delivery record
                l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, finalstatus) VALUES ("
                l_strSQL = l_strSQL & "2753, "
                l_strSQL = l_strSQL & l_rstTemp("companyID") & ", "
                l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                l_strSQL = l_strSQL & l_rstTemp("eventID") & ", "
                l_strSQL = l_strSQL & "'Aspera to " & l_strDestinationID & "', "
                l_strSQL = l_strSQL & "'Completed')"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                Count = Count + 1
            Next
        End If
    Else
        If MsgBox("You are about to send the whole grid" & vbCrLf & "Are you Sure", vbYesNo + vbDefaultButton2, "Final Confirmation") = vbNo Then
            Exit Sub
        End If
        Count = 1
        ProgressBar1.Max = adoClip.Recordset.RecordCount
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        adoClip.Recordset.MoveFirst
        If (optStorageType(0).Value = True Or lblFormat.Caption = "DISCSTORE") Then
            Do While Not adoClip.Recordset.EOF
                ProgressBar1.Value = Count
                DoEvents
                'Invoke a copy to the hotfolder location
                l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
                l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                l_strSQL = l_strSQL & adoClip.Recordset("LibraryID") & ", "
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
                l_strSQL = l_strSQL & l_lngDestinationLibraryID & ", "
                l_strSQL = l_strSQL & "'" & l_strDestinationDropfolder & "', "
                l_strSQL = l_strSQL & "1, "
                l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                
                l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, finalstatus) VALUES ("
                l_strSQL = l_strSQL & "2753, "
                l_strSQL = l_strSQL & adoClip.Recordset("companyID") & ", "
                l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                l_strSQL = l_strSQL & "'Aspera to " & l_strDestinationID & "', "
                l_strSQL = l_strSQL & "'Completed')"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                Count = Count + 1
                adoClip.Recordset.MoveNext
            Loop
        ElseIf (optStorageType(4).Value = True Or lblFormat.Caption = "DIVA") Then
            Do While Not adoClip.Recordset.EOF
                ProgressBar1.Value = Count
                DoEvents
        
                'Invoke a restore to the hotfolder location
                l_lngNewEventID = CopyFileEventToLibraryID(adoClip.Recordset("eventID"), l_lngDestinationLibraryID, True)
                SetData "events", "altlocation", "eventID", l_lngNewEventID, l_strDestinationDropfolder
                SetData "events", "bigfilesize", "eventID", l_lngNewEventID, Null
                SetData "events", "soundlay", "eventID", l_lngNewEventID, Null
                SetData "events", "online", "eventID", l_lngNewEventID, 1
                l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewFileID, FullPathToSourceFile, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
                l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                l_strSQL = l_strSQL & adoClip.Recordset("libraryID") & ", "
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "DIVA To CETA") & ", "
                l_strSQL = l_strSQL & l_lngNewEventID & ", "
                l_strSQL = l_strSQL & "'" & l_strNewRestorePath & "/" & adoClip.Recordset("clipfilename") & "', "
                l_strSQL = l_strSQL & "1, "
                l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                
                'Then make a placeholder delivery record
                l_strSQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, accesstype, finalstatus) VALUES ("
                l_strSQL = l_strSQL & "2753, "
                l_strSQL = l_strSQL & adoClip.Recordset("companyID") & ", "
                l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                l_strSQL = l_strSQL & adoClip.Recordset("eventID") & ", "
                l_strSQL = l_strSQL & "'Aspera to " & l_strDestinationID & "', "
                l_strSQL = l_strSQL & "'Completed')"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                Count = Count + 1
                adoClip.Recordset.MoveNext
            Loop
        End If
    End If
    
    ProgressBar1.Visible = False

    MsgBox "Done"
    
Else

    MsgBox "Cancelled"

End If

End Sub

Private Sub Command2_Click()

'Set title from reference where title is blank for grid.
Dim l_strSQL As String, l_lngCount As Long
Dim l_strUpdateSet As String
Dim l_rstTestCount As ADODB.Recordset, l_rstData As ADODB.Recordset

If adoClip.Recordset.RecordCount = 0 Then Beep: Exit Sub

On Error GoTo BULK_UPDATE_ERROR

Set l_rstTestCount = ExecuteSQL("SELECT eventID FROM events WHERE 1=1 " & m_strSearchConditions & ";", g_strExecuteError)
CheckForSQLError

l_lngCount = l_rstTestCount.RecordCount

If l_lngCount <= 0 Then
    MsgBox "No selection criteria in place. Please do a search.", vbCritical, "Error with Bulk Update"
    Exit Sub
End If

If MsgBox("Updating " & l_lngCount & " records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub

If l_lngCount > 10 Then
    If MsgBox("Updating more than 10 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 50 Then
    If MsgBox("Updating more than 50 records - Are you sure", vbYesNo, "Bulk Update") = vbNo Then Exit Sub
End If

If l_lngCount > 100 Then
    If Not CheckAccess("/bulkupdatelots") Then Exit Sub
End If

l_strSQL = "UPDATE events SET eventtitle = clipreference WHERE (eventtitle IS NULL or eventtitle = '') " & m_strSearchConditions & ";"
Debug.Print l_strSQL

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

cmdSearch.Value = True

BULK_UPDATE_ERROR:

Exit Sub

End Sub

Private Sub Form_Activate()

If Not CheckAccess("/mediainfo", True) Then
    cmdMediaInfo.Visible = False
End If

If Not CheckAccess("/mediainfo", True) Then
    cmdMediaInfoPreserveTimecode.Visible = False
End If

If g_blnRedNetwork = False Then
    
    'Disable the functionality that is not possible on Blue Network
    cmdMediaInfo.Enabled = False
    cmdMediaInfoPreserveTimecode.Enabled = False
    cmdYouTubeXML.Enabled = False
    cmdExpireUnassignedClips.Enabled = False
    cmdMakeBulkGoogleXMLs.Enabled = False
End If

If CheckAccess("/BulkUpdateProgAndAudio", True) Then
    cmdBulkAddProgData.Visible = True
    cmdBulkAddAudioData.Visible = True
Else
    cmdBulkAddProgData.Visible = False
    cmdBulkAddAudioData.Visible = False
End If

If CheckAccess("/superuser", True) Or CheckAccess("/FutureRequests", True) Then
    'cmdReleaseBBCMGClips.Visible = True
    'cmdDreamWorksAudioDeliveries.Visible = True
    'cmdFindLongClips.Visible = True
    chkFutureRequest.Visible = True
    'cmdMd5GridWhenNoMD5.Visible = True
    'Command2.Visible = True
    'cmdPrepareYouTubeSend.Visible = True
Else
    'cmdReleaseBBCMGClips.Visible = False
    'cmdDreamWorksAudioDeliveries.Visible = False
    'cmdFindLongClips.Visible = False
    chkFutureRequest.Visible = False
    'cmdMd5GridWhenNoMD5.Visible = False
    'Command2.Visible = False
    'cmdPrepareYouTubeSend.Visible = False
End If
    
VisibleColumns

End Sub

Private Sub Form_Load()

'Load and store minimum screen height and width
m_MinWidth = 28080
m_MinHeight = 14670

'populate the company drop down (data bound)
Dim l_strSQL As String
l_strSQL = "SELECT name, accountcode, telephone, companyID FROM company WHERE (iscustomer = 1 OR isprospective = 1) AND system_active = 1 ORDER BY name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch1 As ADODB.Recordset
Dim l_rstSearch2 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch1 = New ADODB.Recordset
Set l_rstSearch2 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch1
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch1.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch1

m_strSearchConditions = ""
m_blnSilentSanitise = False

l_strSQL = "SELECT * FROM events WHERE eventID = -1;"

adoClip.ConnectionString = g_strConnection
adoClip.RecordSource = l_strSQL
adoClip.Refresh

If Not CheckAccess("/usedirectSQLquery", True) Then
    lblCaption(21).Visible = False
    txtSQLQuery.Visible = False
Else
    lblCaption(21).Visible = True
    txtSQLQuery.Visible = True
End If

l_strSQL = "SELECT mediaspecID, mediaspecname FROM mediaspec WHERE companyID = 0 ORDER BY fd_order, mediaspecname;"

With l_rstSearch2
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch2.ActiveConnection = Nothing

Set cmbMediaSpecs.DataSourceList = l_rstSearch2

PopulateCombo "discstores", cmbBarcode
PopulateCombo "portalpurchaseprofilegroups", cmbProfileGroup

l_conSearch.Close
Set l_conSearch = Nothing

'Set a flag for double clicking the grid
g_intClipGridClick = 0

lblPatheInstructions.Caption = "1) Paste the Email text into the box" & vbCrLf
lblPatheInstructions.Caption = lblPatheInstructions.Caption & "2) Find the Pathe Order, Select it in the text and Ctrl-Copy" & vbCrLf
lblPatheInstructions.Caption = lblPatheInstructions.Caption & "3) Paste the Order number into the box and hit 'Locate or Create Order" & vbCrLf
lblPatheInstructions.Caption = lblPatheInstructions.Caption & "4) If it finds an existing order, decide if you need to wipe the clips and redo the order." & vbCrLf
lblPatheInstructions.Caption = lblPatheInstructions.Caption & "5) If it doesn't find an order, let it make a new one." & vbCrLf
lblPatheInstructions.Caption = lblPatheInstructions.Caption & "6) Process the Order, so that it can locate the clips." & vbCrLf
lblPatheInstructions.Caption = lblPatheInstructions.Caption & "7) Transfer the clips back to the Search Grid, and do whatever it is you need to do with them."

If CheckAccess("Superuser", True) Then
    chkMoveThenForget.Visible = True
Else
    chkMoveThenForget.Visible = False
End If

End Sub

Private Sub Form_Resize()

On Error Resume Next

'If Me.Height < m_MinHeight Then Me.Height = m_MinHeight
'If Me.Width < m_MinWidth Then Me.Width = m_MinWidth

picFooter.Top = Me.ScaleHeight - picFooter.ScaleHeight - 120
picFooter.Left = Me.ScaleWidth - picFooter.ScaleWidth - 120
TabMediaSearching.Height = Me.ScaleHeight - TabMediaSearching.Top - picFooter.ScaleHeight - 240
TabMediaSearching.Width = Me.ScaleWidth - 30
grdClips.Height = TabMediaSearching.Height - grdClips.Top - 120
grdClips.Width = TabMediaSearching.Width - 120
frmStorageType.Top = picFooter.Top
'frmStorageType.Left = picFooter.Left - frmStorageType.Width - 120

lblCurrentFilename.Width = Me.ScaleWidth - lblCurrentFilename.Left - 240

End Sub

Private Sub grdClips_BeforeUpdate(Cancel As Integer)

Dim l_strSQL As String, Bookmark As Variant

If optSubmitRenameRequests.Value = True And grdClips.Columns("clipfilename").Text <> adoClip.Recordset("clipfilename") Then
    If GetData("vw_Events_On_DISCSTORE", "eventID", "eventID", grdClips.Columns("eventID").Text) = grdClips.Columns("eventID").Text Then
        Cancel = 1
        l_strSQL = "INSERT INTO event_file_request (eventID, sourcelibraryID, event_file_Request_typeID, NewLibraryID, NewFileName, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
        l_strSQL = l_strSQL & grdClips.Columns("eventID").Text & ", "
        l_strSQL = l_strSQL & grdClips.Columns("libraryID").Text & ", "
        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "File Rename") & ", "
        l_strSQL = l_strSQL & grdClips.Columns("libraryID").Text & ", "
        l_strSQL = l_strSQL & "'" & grdClips.Columns("clipfilename").Text & "', "
        If chkSupressNewReference.Value <> 0 Then l_strSQL = l_strSQL & "0, " Else l_strSQL = l_strSQL & "1, "
        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        adoClip.Refresh
        Exit Sub
    End If
End If

If optSubmitMoveRequests.Value = True And grdClips.Columns("altlocation").Text <> adoClip.Recordset("altlocation") Then
    If GetData("vw_Events_On_DISCSTORE", "eventID", "eventID", grdClips.Columns("eventID").Text) = grdClips.Columns("eventID").Text Then
        Cancel = 1
        l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & grdClips.Columns("libraryID").Text
        l_strSQL = l_strSQL & " AND altlocation = '" & grdClips.Columns("altlocation").Text & "' "
        l_strSQL = l_strSQL & " AND clipfilename = '" & grdClips.Columns("clipfilename").Text & "'"
        l_strSQL = l_strSQL & " AND system_deleted = 0"
        If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
            l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, RequestName, RequesterEmail) VALUES ("
            l_strSQL = l_strSQL & grdClips.Columns("eventID").Text & ", "
            l_strSQL = l_strSQL & grdClips.Columns("libraryID").Text & ", "
            l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
            l_strSQL = l_strSQL & grdClips.Columns("libraryID").Text & ", "
            l_strSQL = l_strSQL & "'" & grdClips.Columns("altlocation").Text & "', "
            l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
        Else
            MsgBox "A File of this name already exists in the destination folder", vbCritical, "Error"
        End If
        adoClip.Refresh
        Exit Sub
    End If
End If

If grdClips.Columns("eventEpisode").Text <> "" Then
    If grdClips.Columns("companyID").Text <> "" Then
        If InStr(GetData("company", "cetaclientcode", "companyID", grdClips.Columns("companyID").Text), "/2digitepnumbers") > 0 Then
            grdClips.Columns("eventepisode").Text = Format(Val(grdClips.Columns("eventepisode").Text), "00")
        End If
    End If
End If

'l_strSQL = "UPDATE events SET "
grdClips.Columns("altlocation").Text = SlashForwardToBack(grdClips.Columns("altlocation").Text)
'If grdClips.Columns("altlocation").Text <> adoClip.Recordset("altlocation") Then l_strSQL = l_strSQL & "altlocation = '" & grdClips.Columns("altlocation").Text & "', "
'If grdClips.Columns("clipfilename").Text <> adoClip.Recordset("clipfilename") Then l_strSQL = l_strSQL & "clipfilename = '" & grdClips.Columns("clipfilename").Text & "', "

grdClips.Columns("seriesID").Text = Trim(grdClips.Columns("seriesID").Text)
grdClips.Columns("seriesID").Text = Replace(grdClips.Columns("seriesID").Text, vbCrLf, "")
'If grdClips.Columns("seriesID").Text <> Trim(" " & adoClip.Recordset("seriesID")) Then l_strSQL = l_strSQL & "seriesID = '" & grdClips.Columns("seriesID").Text & "', "
'If grdClips.Columns("eventseries").Text <> Trim(" " & adoClip.Recordset("eventseries")) Then l_strSQL = l_strSQL & "eventseries = '" & grdClips.Columns("eventseries").Text & "', "
'If grdClips.Columns("eventset").Text <> Trim(" " & adoClip.Recordset("eventset")) Then l_strSQL = l_strSQL & "eventset = '" & grdClips.Columns("eventset").Text & "', "
'If grdClips.Columns("eventepisode").Text <> Trim(" " & adoClip.Recordset("eventepisode")) Then l_strSQL = l_strSQL & "eventepisode = '" & grdClips.Columns("eventepisode").Text & "', "
grdClips.Columns("clipreference").Text = Trim(grdClips.Columns("clipreference").Text)
grdClips.Columns("clipreference").Text = Replace(grdClips.Columns("clipreference").Text, vbCrLf, "")
'If grdClips.Columns("clipreference").Text <> Trim(" " & adoClip.Recordset("clipreference")) Then l_strSQL = l_strSQL & "clipreference = '" & grdClips.Columns("clipreference").Text & "', "
'If grdClips.Columns("hidefromweb").Text <> Trim(" " & adoClip.Recordset("hidefromweb")) Then l_strSQL = l_strSQL & "hidefromweb = '" & grdClips.Columns("hidefromweb").Text & "', "
grdClips.Columns("eventtitle").Text = Trim(grdClips.Columns("eventtitle").Text)
grdClips.Columns("eventtitle").Text = Replace(grdClips.Columns("eventtitle").Text, vbCrLf, "")
'If grdClips.Columns("eventtitle").Text <> Trim(" " & adoClip.Recordset("eventtitle")) Then l_strSQL = l_strSQL & "eventtitle = '" & grdClips.Columns("eventtitle").Text & "', "
'If grdClips.Columns("eventsubtitle").Text <> Trim(" " & adoClip.Recordset("eventsubtitle")) Then l_strSQL = l_strSQL & "eventsubtitle = '" & grdClips.Columns("eventsubtitle").Text & "', "
'If grdClips.Columns("eventversion").Text <> Trim(" " & adoClip.Recordset("eventversion")) Then l_strSQL = l_strSQL & "eventversion = '" & grdClips.Columns("eventversion").Text & "', "
'If grdClips.Columns("language").Text <> Trim(" " & adoClip.Recordset("language")) Then l_strSQL = l_strSQL & "language = '" & grdClips.Columns("language").Text & "', "
'If grdClips.Columns("customfield1").Text <> Trim(" " & adoClip.Recordset("customfield1")) Then l_strSQL = l_strSQL & "customfield1 = '" & grdClips.Columns("customfield1").Text & "', "
'If grdClips.Columns("customfield2").Text <> Trim(" " & adoClip.Recordset("customfield2")) Then l_strSQL = l_strSQL & "customfield2 = '" & grdClips.Columns("customfield2").Text & "', "
'If grdClips.Columns("customfield3").Text <> Trim(" " & adoClip.Recordset("customfield3")) Then l_strSQL = l_strSQL & "customfield3 = '" & grdClips.Columns("customfield3").Text & "', "
'If grdClips.Columns("customfield4").Text <> Trim(" " & adoClip.Recordset("customfield4")) Then l_strSQL = l_strSQL & "customfield4 = '" & grdClips.Columns("customfield4").Text & "', "
'If grdClips.Columns("customfield5").Text <> Trim(" " & adoClip.Recordset("customfield5")) Then l_strSQL = l_strSQL & "customfield5 = '" & grdClips.Columns("customfield5").Text & "', "
'If grdClips.Columns("customfield6").Text <> Trim(" " & adoClip.Recordset("customfield6")) Then l_strSQL = l_strSQL & "customfield6 = '" & grdClips.Columns("customfield6").Text & "', "
'
'If l_strSQL <> "UPDATE events SET " Then
'    If Right(l_strSQL, 2) = ", " Then
'        Bookmark = grdClips.Bookmark
'        l_strSQL = Left(l_strSQL, Len(l_strSQL) - 2)
'        l_strSQL = l_strSQL & " WHERE eventID = " & grdClips.Columns("eventID").Text & ";"
'        ExecuteSQL l_strSQL, g_strExecuteError
'        CheckForSQLError
'        Cancel = 1
'        adoClip.Refresh
'        grdClips.Bookmark = Bookmark
'    End If
'End If

End Sub

Private Sub grdclips_DblClick()

If g_intClipGridClick <> 0 Then Exit Sub

g_intClipGridClick = 1
ShowClipControl Val(grdClips.Columns("eventID").Text)
'Me.Hide

End Sub

Private Sub grdClips_RowLoaded(ByVal Bookmark As Variant)

Dim l_strMD5  As String, Count As Long, Exists As Long

On Error GoTo ENDPROC

If GetData("techrev", "techrevID", "eventID", grdClips.Columns("eventID").Text) <> 0 Then
    grdClips.Columns("techrev").Text = 1
ElseIf Trim(" " & grdClips.Columns("Clipfilename").Text) <> "" And Trim(" " & grdClips.Columns("filesize").Text) <> "" And grdClips.Columns("md5").Text = 1 Then
    l_strMD5 = GetData("events", "md5checksum", "eventID", Val(grdClips.Columns("eventID").Text))
    If GetDataSQL("SELECT TOP 1 techrevID FROM techrev WHERE system_deleted = 0 AND companyID = " & Val(grdClips.Columns("companyID").Text) & " AND filename = '" & grdClips.Columns("clipfilename").Text & "' AND bigfilesize = " & grdClips.Columns("filesize").Text & " AND md5checksum = '" & l_strMD5 & "';") <> 0 Then
        grdClips.Columns("techrev").Text = 1
    End If
End If
'    If GetDataSQL("SELECT status_name FROM JelimediaAnalysis WHERE companyID = " & Val(grdClips.Columns("companyID").Text) & " AND clipreference = '" & grdClips.Columns("clipreference").Text & "';") = "COMPLETE" Then
'        grdClips.Columns("audioanalysis").Text = 1
'    End If

Exists = Val(GetDataSQL("SELECT TOP 1 portalpermissionID FROM portalpermission where eventID = " & grdClips.Columns("eventID").Text & ";"))
If Exists <> 0 Then grdClips.Columns("MW").Text = -1 Else grdClips.Columns("MW").Text = 0
Exists = Val(GetDataSQL("SELECT TOP 1 distributionpermissionID FROM distributionpermission where eventID = " & grdClips.Columns("eventID").Text & ";"))
If Exists <> 0 Then grdClips.Columns("DW").Text = -1 Else grdClips.Columns("DW").Text = 0

grdClips.Columns("barcode").Text = GetData("library", "barcode", "libraryID", Val(grdClips.Columns("libraryID").Text))

If grdClips.Columns("fileversion").Text = "Masterfile" Then
    For Count = 0 To grdClips.Cols - 1
        grdClips.Columns(Count).CellStyleSet "Masterfile"
    Next
ElseIf grdClips.Columns("fileversion").Text = "Original Masterfile" Then
    For Count = 0 To grdClips.Cols - 1
        grdClips.Columns(Count).CellStyleSet "OriginalMasterfile"
    Next
ElseIf grdClips.Columns("fileversion").Text = "Edited Masterfile" Then
    For Count = 0 To grdClips.Cols - 1
        grdClips.Columns(Count).CellStyleSet "EditedMasterfile"
    Next
ElseIf grdClips.Columns("fileversion").Text = "iTunes Masterfile" Then
    For Count = 0 To grdClips.Cols - 1
        grdClips.Columns(Count).CellStyleSet "iTunesMasterfile"
    Next
ElseIf grdClips.Columns("fileversion").Text = "Encrypted File" Then
    For Count = 0 To grdClips.Cols - 1
        grdClips.Columns(Count).CellStyleSet "EncryptedFile"
    Next
End If

grdClips.Columns("portalpurchaseprofilegroup").Text = GetDataSQL("SELECT TOP 1 information FROM xref WHERE category = 'portalpurchaseprofilegroups' AND description = '" & grdClips.Columns("resourceID").Text & "';")

ENDPROC:
Exit Sub

End Sub

Private Sub SSTab1_DblClick()

End Sub

Private Sub grdPatheClips_RowLoaded(ByVal Bookmark As Variant)

grdPatheClips.Columns("clipfilename").Text = GetData("events", "clipfilename", "eventID", grdPatheClips.Columns("eventID").Text)
grdPatheClips.Columns("Barcode").Text = GetData("library", "barcode", "LibraryID", GetData("events", "libraryID", "eventID", grdPatheClips.Columns("eventID").Text))

End Sub

Private Sub optStorageType_Click(Index As Integer)

'If optStorageType(Index).Value = True Then
'    Select Case Index
'    Case 4 'DIVA
'        chkAlsoOnDIVA.Value = 0
'        chkAlsoOnDIVA.Enabled = False
'
'    Case 0 'DISCSTORE
'        chkAlsoOnDIVA.Enabled = True
'
'    Case 1 'MOBILEDISC
'        chkAlsoOnDIVA.Enabled = True
'
'    Case 3, 2, 6, 5 'LTO
'        chkAlsoOnDIVA.Enabled = True
'
'    Case Else 'Other
'        chkAlsoOnDIVA.Enabled = True
'    End Select
'
'End If
'
End Sub

Private Sub txtAltFolder_GotFocus()
HighLite txtAltFolder
End Sub

Private Sub txtAudioBitrate_GotFocus()
HighLite txtAudioBitrate
End Sub

Private Sub txtBitrate_GotFocus()
HighLite txtBitrate
End Sub

Private Sub txtClipfilename_GotFocus()
HighLite txtClipfilename
End Sub

Private Sub txtClockNumber_GotFocus()
HighLite txtClockNumber
End Sub

Private Sub txtHorizpixels_GotFocus()
HighLite txtHorizpixels
End Sub

Private Sub txtInternalReference_GotFocus()
HighLite txtInternalReference
End Sub

Private Sub txtJobID_GotFocus()
HighLite txtJobID
End Sub

Private Sub txtNotes_GotFocus()
HighLite txtNotes
End Sub

Private Sub txtReference_GotFocus()
HighLite txtReference
End Sub

Private Sub txtSeriesID_GotFocus()
HighLite txtSeriesID
End Sub

Private Sub txtSourcebarcode_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    If txtSourcebarcode.Text <> "" Then
        KeyAscii = 0
        txtSourcebarcode.Text = UCase(txtSourcebarcode.Text)
        lblSourceLibraryID.Caption = GetData("library", "libraryID", "barcode", txtSourcebarcode.Text)
        lblSourceFormat.Caption = GetData("library", "format", "barcode", txtSourcebarcode.Text)
    Else
        lblSourceLibraryID.Caption = ""
        lblSourceFormat.Caption = ""
    End If
End If
End Sub

Private Sub txtSQLQuery_GotFocus()
HighLite txtSQLQuery
End Sub

Private Sub txtSubTitle_GotFocus()
HighLite txtSubtitle
End Sub

Private Sub txtThumbnail_GotFocus()
HighLite txtThumbnail
End Sub

Private Sub txtTitle_Click()

txtSeriesID.Text = txtTitle.Columns("SeriesID").Text

End Sub

Private Sub txtTitle_GotFocus()
HighLite txtTitle
End Sub

Private Sub txtVertpixels_GotFocus()
HighLite txtVertpixels
End Sub

Private Sub txtVideoBitrate_GotFocus()
HighLite txtVideoBitrate
End Sub

Sub WriteTabs(lp_intTabCount As Integer)

Dim Count As Integer

For Count = 1 To lp_intTabCount

    Print #1, Chr(9);

Next

End Sub

Private Sub Local_Set_Framerate()

Select Case frmClipControl.cmbFrameRate.Text

    Case "25"
        m_Framerate = TC_25
    Case "29.97"
        m_Framerate = TC_29
    Case "30"
        m_Framerate = TC_30
    Case "24", "23.98"
        m_Framerate = TC_24
    Case "50"
        m_Framerate = TC_50
    Case "60"
        m_Framerate = TC_60
    Case "59.94"
        m_Framerate = TC_59
    Case Else
        m_Framerate = TC_UN

End Select

End Sub

