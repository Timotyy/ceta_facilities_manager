Attribute VB_Name = "modFolderSize"
Option Explicit

Private Declare Function FindFirstFile Lib "kernel32.dll" Alias "FindFirstFileA" _
                         (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
                         
Private Declare Function FindNextFile Lib "kernel32.dll" Alias "FindNextFileA" _
                         (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long
                         
Private Declare Function FindClose Lib "kernel32.dll" (ByVal hFindFile As Long) As Long

Private Const FILE_ATTRIBUTE_DIRECTORY = &H10
Private Const MAX_PATH = 260

Private Type FILETIME
        dwLowDateTime As Long
        dwHighDateTime As Long
End Type

Public Type WIN32_FIND_DATA
        dwFileAttributes As Long
        ftCreationTime As FILETIME
        ftLastAccessTime As FILETIME
        ftLastWriteTime As FILETIME
        nFileSizeHigh As Long
        nFileSizeLow As Long
        dwReserved0 As Long
        dwReserved1 As Long
        cFileName As String * MAX_PATH
        cAlternate As String * 14
End Type

Public Function GetDirectorySize(fld As String, Optional silent As Boolean) As Currency
    'useage: cSize = GetDirectorySize(Foldername) Note cSize needs to be Dimmed As Currency
    
    Dim fHandle As Long
    
    Dim Filename As String
    
    Dim bRet As Boolean
    
    Dim findData As WIN32_FIND_DATA
    
    Dim l_lngFilenumber As Long
    
    Dim l_curFileSize As Currency
    
    On Error Resume Next
    
    SetPath fld 'add a trailing \ if there isn't one
    
    fHandle = FindFirstFile(fld & "*", findData) 'find the first file/folder in the root path
    
    'get rid of the nulls
    Filename = findData.cFileName
    Filename = StripNulls(Filename)
    
    If silent <> True Then
        frmClipControl.MousePointer = vbHourglass
    End If
    
    If IsMissing(silent) Then silent = False

    'loop until there's nothing left
    Do While Len(Filename) <> 0
        'if this is a subfolder, drop into it
        If (findData.dwFileAttributes And FILE_ATTRIBUTE_DIRECTORY) = FILE_ATTRIBUTE_DIRECTORY _
                                      And Filename <> "." And Filename <> ".." Then
           'look ma, we're recursing!
           GetDirectorySize = GetDirectorySize + GetDirectorySize(fld & "\" & Filename, silent)
        End If
        
        'add the bytes to the total
        API_OpenFile fld & Filename, l_lngFilenumber, l_curFileSize
        API_CloseFile l_lngFilenumber
        
        GetDirectorySize = GetDirectorySize + l_curFileSize
        If silent <> True Then
            frmClipControl.lblFileSize.Caption = Format(GetDirectorySize, "#,#")
        End If
        DoEvents
        
        'get the next one
        bRet = FindNextFile(fHandle, findData)
        
        'nothing left in this folder so get out
        If bRet = False Then
           Exit Do
        End If
        
        'get rid of the nulls
        Filename = findData.cFileName
        Filename = StripNulls(Filename)
        
        'it runs a bit faster without this DoEvents, but if you anticipate using
        'it on folders with tons (thousands and thousands) of files and subfolders,
        'you might want to uncomment it so your app doesn't appear to be 'frozen'
        
        'DoEvents
    Loop
    
    If silent <> True Then
        frmClipControl.MousePointer = vbDefault
    End If
       
    'clean up the file handle
    bRet = FindClose(fHandle)
   
End Function

Private Function SetPath(instring As String) As String
   'appends a back slash to a path if needed

   If Right$(instring, 1) <> "\" Then
      instring = instring & "\"
   End If
         
   SetPath = instring
   
End Function

Private Function StripNulls(OriginalStr As String) As String
   'strip nulls from a string
   
   If (InStr(OriginalStr, Chr(0)) > 0) Then
      OriginalStr = Left$(OriginalStr, InStr(OriginalStr, Chr(0)) - 1)
   End If
   
   StripNulls = OriginalStr
   
End Function

