VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmAddMasters 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Add Masters"
   ClientHeight    =   5895
   ClientLeft      =   4020
   ClientTop       =   2955
   ClientWidth     =   10635
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5895
   ScaleWidth      =   10635
   Begin VB.TextBox txtBarcode 
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   5100
      TabIndex        =   9
      ToolTipText     =   "The title for the job"
      Top             =   480
      Width           =   2835
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   375
      Left            =   9000
      TabIndex        =   8
      Top             =   5460
      Width           =   1515
   End
   Begin VB.TextBox txtClockNumber 
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   5100
      TabIndex        =   6
      ToolTipText     =   "The title for the job"
      Top             =   120
      Width           =   2835
   End
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "Refresh"
      Height          =   375
      Left            =   60
      TabIndex        =   5
      Top             =   5460
      Width           =   1515
   End
   Begin MSAdodcLib.Adodc adoMasters 
      Height          =   330
      Left            =   8400
      Top             =   60
      Visible         =   0   'False
      Width           =   2115
      _ExtentX        =   3731
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoMasters"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdMasters 
      Bindings        =   "frmAddMasters.frx":0000
      Height          =   4515
      Left            =   60
      TabIndex        =   4
      Top             =   840
      Width           =   10455
      _Version        =   196617
      AllowUpdate     =   0   'False
      BackColorOdd    =   12648384
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   18441
      _ExtentY        =   7964
      _StockProps     =   79
   End
   Begin VB.TextBox txtTitle 
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1320
      TabIndex        =   0
      ToolTipText     =   "The title for the job"
      Top             =   480
      Width           =   2895
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbProduct 
      Height          =   255
      Left            =   1320
      TabIndex        =   1
      ToolTipText     =   "The product on this job"
      Top             =   120
      Width           =   2895
      BevelWidth      =   0
      DataFieldList   =   "name"
      BevelType       =   0
      _Version        =   196617
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      CheckBox3D      =   0   'False
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   8438015
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "productID"
      Columns(0).Name =   "productID"
      Columns(0).DataField=   "productID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   5212
      Columns(1).Caption=   "Name"
      Columns(1).Name =   "name"
      Columns(1).DataField=   "name"
      Columns(1).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   8438015
      DataFieldToDisplay=   "name"
   End
   Begin VB.Label lblCaption 
      Caption         =   "Double click on a row in the grid to add it to your job as a master"
      Height          =   255
      Index           =   2
      Left            =   2100
      TabIndex        =   11
      Top             =   5460
      Width           =   6555
   End
   Begin VB.Label lblCaption 
      Caption         =   "Barcode"
      Height          =   255
      Index           =   1
      Left            =   4320
      TabIndex        =   10
      Top             =   480
      Width           =   675
   End
   Begin VB.Label lblCaption 
      Caption         =   "Clock"
      Height          =   255
      Index           =   0
      Left            =   4320
      TabIndex        =   7
      Top             =   120
      Width           =   675
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      Height          =   255
      Index           =   8
      Left            =   60
      TabIndex        =   3
      Top             =   480
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Product"
      Height          =   255
      Index           =   46
      Left            =   60
      TabIndex        =   2
      Top             =   120
      Width           =   1035
   End
End
Attribute VB_Name = "frmAddMasters"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmbProduct_DropDown()

Dim l_strSQL As String
l_strSQL = "SELECT name, productID FROM product WHERE name LIKE '" & QuoteSanitise(cmbProduct.Text) & "%' ORDER BY name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbProduct.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmbProduct_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF5 Then cmdRefresh.Value = True
End Sub


Private Sub cmdClose_Click()
Me.Hide

End Sub

Private Sub cmdRefresh_Click()

Me.MousePointer = vbHourglass
adoMasters.ConnectionString = g_strConnection
adoMasters.RecordSource = "SELECT library.barcode AS 'Barcode', library.location AS 'Location', clocknumber AS 'Clock Number', eventtitle AS 'Title', timecodestart AS 'Time Code', tapeevents.eventID FROM tapeevents INNER JOIN library ON library.libraryID = tapeevents.libraryID WHERE library.productname LIKE '" & QuoteSanitise(cmbProduct.Text) & "%' AND tapeevents.eventtitle LIKE '" & QuoteSanitise(txtTitle.Text) & "%' AND library.barcode LIKE '" & QuoteSanitise(txtBarcode.Text) & "%' AND tapeevents.clocknumber LIKE '" & QuoteSanitise(txtClockNumber.Text) & "%' ORDER BY tapeevents.clocknumber DESC;"
adoMasters.Refresh

Me.MousePointer = vbDefault


End Sub

Private Sub Form_Load()
CenterForm Me
End Sub

Private Sub grdMasters_DblClick()
If grdMasters.Rows = 0 Then Exit Sub

g_CurrentlySelectedEventID = Val(grdMasters.Columns(5).Text)
DoEvents
AddEventToJobAsMaster
frmJob.adoJobDetail.Refresh
End Sub


Private Sub Text1_Change()

End Sub

Private Sub txtClockNumber_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF5 Then cmdRefresh.Value = True
End Sub


Private Sub txtTitle_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF5 Then cmdRefresh.Value = True
End Sub


