VERSION 5.00
Begin VB.Form frmSystemSettings 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "System Settings"
   ClientHeight    =   5730
   ClientLeft      =   5160
   ClientTop       =   3930
   ClientWidth     =   5220
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5730
   ScaleWidth      =   5220
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdEnableControls 
      Caption         =   "Enable Controls"
      Height          =   315
      Left            =   3360
      TabIndex        =   15
      Top             =   180
      Width           =   1755
   End
   Begin VB.Frame fraDisable 
      Caption         =   "Disable..."
      Enabled         =   0   'False
      Height          =   5415
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3075
      Begin VB.CheckBox Check9 
         Caption         =   "Purchase Orders"
         Height          =   195
         Left            =   180
         TabIndex        =   14
         Top             =   720
         Width           =   2715
      End
      Begin VB.CheckBox chkDisableEditViews 
         Caption         =   "Edit Views"
         Height          =   195
         Left            =   180
         TabIndex        =   13
         Top             =   5040
         Width           =   2715
      End
      Begin VB.CheckBox chkDisableResourceList 
         Caption         =   "Resource List"
         Height          =   195
         Left            =   180
         TabIndex        =   12
         Top             =   4680
         Width           =   2715
      End
      Begin VB.CheckBox chkDisableKitDefinition 
         Caption         =   "Kit Definition"
         Height          =   195
         Left            =   180
         TabIndex        =   11
         Top             =   4320
         Width           =   2715
      End
      Begin VB.CheckBox chkDisableImport 
         Caption         =   "Import"
         Height          =   195
         Left            =   180
         TabIndex        =   10
         Top             =   3960
         Width           =   2715
      End
      Begin VB.CheckBox chkDisableToolbarMenu 
         Caption         =   "Toolbar Menu"
         Height          =   195
         Left            =   180
         TabIndex        =   9
         Top             =   3600
         Width           =   2715
      End
      Begin VB.CheckBox chkDisableSchedule 
         Caption         =   "Schedule"
         Height          =   195
         Left            =   180
         TabIndex        =   8
         Top             =   2520
         Width           =   2715
      End
      Begin VB.CheckBox chkDisableLibraryTransfer 
         Caption         =   "Library Transfer"
         Height          =   195
         Left            =   180
         TabIndex        =   7
         Top             =   3240
         Width           =   2715
      End
      Begin VB.CheckBox chkDisableEquipmentManagement 
         Caption         =   "Equipment Management"
         Height          =   195
         Left            =   180
         TabIndex        =   6
         Top             =   2880
         Width           =   2715
      End
      Begin VB.CheckBox chkDisableTasks 
         Caption         =   "Tasks"
         Height          =   195
         Left            =   180
         TabIndex        =   5
         Top             =   2160
         Width           =   2715
      End
      Begin VB.CheckBox chkDisableCostingSheets 
         Caption         =   "Costing Sheets"
         Height          =   195
         Left            =   180
         TabIndex        =   4
         Top             =   1800
         Width           =   2715
      End
      Begin VB.CheckBox chkDisableProducts 
         Caption         =   "Products"
         Height          =   195
         Left            =   180
         TabIndex        =   3
         Top             =   1440
         Width           =   2715
      End
      Begin VB.CheckBox chkDisableProjects 
         Caption         =   "Projects"
         Height          =   195
         Left            =   180
         TabIndex        =   2
         Top             =   1080
         Width           =   2715
      End
      Begin VB.CheckBox chkDisableQuotes 
         Caption         =   "Quotes"
         Height          =   195
         Left            =   180
         TabIndex        =   1
         Top             =   360
         Width           =   2715
      End
   End
End
Attribute VB_Name = "frmSystemSettings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdEnableControls_Click()
fraDisable.Enabled = True

End Sub

