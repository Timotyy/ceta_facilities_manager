VERSION 5.00
Begin VB.Form frmResourceDefaults 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   990
   ClientLeft      =   6645
   ClientTop       =   4425
   ClientWidth     =   7650
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   990
   ScaleWidth      =   7650
   Begin VB.CommandButton cmdCancelResourceDefaults 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   315
      Left            =   6300
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Forward one page"
      Top             =   540
      UseMaskColor    =   -1  'True
      Width           =   1155
   End
   Begin VB.CommandButton cmdAcceptResourceDefaults 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   315
      Left            =   6300
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Forward one page"
      Top             =   120
      UseMaskColor    =   -1  'True
      Width           =   1155
   End
   Begin VB.OptionButton optScanPattern 
      Caption         =   "Unknown"
      ForeColor       =   &H000000FF&
      Height          =   195
      Index           =   3
      Left            =   3060
      TabIndex        =   6
      Top             =   720
      Width           =   975
   End
   Begin VB.OptionButton optScanPattern 
      Caption         =   "Psf"
      Height          =   195
      Index           =   2
      Left            =   2400
      TabIndex        =   5
      Top             =   720
      Width           =   675
   End
   Begin VB.OptionButton optScanPattern 
      Caption         =   "P"
      Height          =   195
      Index           =   1
      Left            =   1860
      TabIndex        =   4
      Top             =   720
      Width           =   435
   End
   Begin VB.OptionButton optScanPattern 
      Caption         =   "I"
      Height          =   195
      Index           =   0
      Left            =   1260
      TabIndex        =   3
      Top             =   720
      Width           =   435
   End
   Begin VB.ComboBox cmbAspect 
      Height          =   315
      Left            =   3060
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   300
      Width           =   3015
   End
   Begin VB.ComboBox cmbFrameRate 
      Height          =   315
      Left            =   1560
      TabIndex        =   1
      Top             =   300
      Width           =   1455
   End
   Begin VB.ComboBox cmbLineStandard 
      Height          =   315
      Left            =   60
      TabIndex        =   0
      Top             =   300
      Width           =   1455
   End
   Begin VB.Label lblLegend 
      BackStyle       =   0  'Transparent
      Caption         =   "Scan Pattern"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   22
      Left            =   60
      TabIndex        =   12
      Top             =   720
      Width           =   1275
   End
   Begin VB.Label lblLegend 
      BackStyle       =   0  'Transparent
      Caption         =   "Aspect Ratio"
      Height          =   195
      Index           =   21
      Left            =   3060
      TabIndex        =   11
      Top             =   60
      Width           =   1275
   End
   Begin VB.Label lblLegend 
      BackStyle       =   0  'Transparent
      Caption         =   "Frame Rate"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   20
      Left            =   1560
      TabIndex        =   10
      Top             =   60
      Width           =   1275
   End
   Begin VB.Label lblLegend 
      BackStyle       =   0  'Transparent
      Caption         =   "Line Standard"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   19
      Left            =   60
      TabIndex        =   9
      Top             =   60
      Width           =   1275
   End
End
Attribute VB_Name = "frmResourceDefaults"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub cmbAspect_DropDown()
PopulateCombo "aspectratio", cmbAspect
End Sub

Private Sub cmbFrameRate_DropDown()
cmbFrameRate.Clear
PopulateCombo "framerate", cmbFrameRate, cmbLineStandard.Text
End Sub

Private Sub cmbLineStandard_DropDown()
PopulateCombo "linestandard", cmbLineStandard
End Sub

Private Sub cmdAcceptResourceDefaults_Click()
Me.Hide
DoEvents

End Sub

Private Sub cmdCancelResourceDefaults_Click()
Me.Tag = "CANCELLED"
Me.Hide

End Sub

Private Sub Form_Load()
Me.Left = 2500
Me.Top = Screen.Height / 2
End Sub
