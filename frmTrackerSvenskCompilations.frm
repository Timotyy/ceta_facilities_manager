VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmTrackerSvenskCompilations 
   Caption         =   "Tracker Svensk Compilations"
   ClientHeight    =   12420
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   28650
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   12420
   ScaleMode       =   0  'User
   ScaleWidth      =   28969.66
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtAlphaCode 
      Height          =   315
      Left            =   6960
      TabIndex        =   63
      Top             =   720
      Width           =   3075
   End
   Begin VB.TextBox txtSpecialProject 
      Height          =   315
      Left            =   12240
      TabIndex        =   60
      Top             =   360
      Width           =   1275
   End
   Begin VB.CommandButton cmdGetCompilationTitle 
      Caption         =   "Copy Compilation Title"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2880
      TabIndex        =   58
      Top             =   1200
      Width           =   2655
   End
   Begin VB.CommandButton cmdUpXMLSent 
      Caption         =   "Update"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   12660
      TabIndex        =   55
      Top             =   1200
      Width           =   975
   End
   Begin VB.CommandButton cmdUpXMLMade 
      Caption         =   "Update"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   11640
      TabIndex        =   54
      Top             =   1200
      Width           =   975
   End
   Begin VB.PictureBox fraUpdates 
      Height          =   5235
      Left            =   6480
      ScaleHeight     =   5175
      ScaleWidth      =   8055
      TabIndex        =   37
      Top             =   2400
      Visible         =   0   'False
      Width           =   8115
      Begin VB.CheckBox chkUpdate 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Check1"
         Height          =   195
         Index           =   9
         Left            =   3540
         TabIndex        =   76
         Top             =   3420
         Width           =   195
      End
      Begin VB.CheckBox chkUpdate 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Check1"
         Height          =   195
         Index           =   8
         Left            =   5760
         TabIndex        =   75
         Top             =   3060
         Width           =   195
      End
      Begin VB.CheckBox chkUpdate 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Check1"
         Height          =   195
         Index           =   7
         Left            =   3540
         TabIndex        =   74
         Top             =   2640
         Width           =   195
      End
      Begin VB.CheckBox chkUpdate 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Check1"
         Height          =   195
         Index           =   6
         Left            =   3540
         TabIndex        =   73
         Top             =   2340
         Width           =   195
      End
      Begin VB.CheckBox chkUpdate 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Check1"
         Height          =   195
         Index           =   5
         Left            =   3540
         TabIndex        =   72
         Top             =   2040
         Width           =   195
      End
      Begin VB.CheckBox chkUpdate 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Check1"
         Height          =   195
         Index           =   4
         Left            =   3540
         TabIndex        =   71
         Top             =   1680
         Width           =   195
      End
      Begin VB.CheckBox chkUpdate 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Check1"
         Height          =   195
         Index           =   3
         Left            =   3540
         TabIndex        =   70
         Top             =   1320
         Width           =   195
      End
      Begin VB.CheckBox chkUpdate 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Check1"
         Height          =   195
         Index           =   2
         Left            =   3540
         TabIndex        =   69
         Top             =   960
         Width           =   195
      End
      Begin VB.CheckBox chkUpdate 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Check1"
         Height          =   195
         Index           =   1
         Left            =   3540
         TabIndex        =   68
         Top             =   600
         Width           =   195
      End
      Begin VB.CheckBox chkUpdate 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Check1"
         Height          =   195
         Index           =   0
         Left            =   7140
         TabIndex        =   67
         Top             =   240
         Width           =   195
      End
      Begin VB.TextBox txtKitID 
         Height          =   315
         Left            =   1980
         TabIndex        =   65
         Top             =   1260
         Width           =   1455
      End
      Begin VB.TextBox txtEditSpecialProject 
         Height          =   315
         Left            =   1980
         TabIndex        =   61
         Top             =   1980
         Width           =   1455
      End
      Begin VB.TextBox txtAlphaDisplayCode 
         Height          =   315
         Left            =   1980
         TabIndex        =   56
         Top             =   900
         Width           =   1455
      End
      Begin VB.TextBox txtUpCompilation 
         Height          =   315
         Left            =   1980
         TabIndex        =   45
         Top             =   180
         Width           =   5055
      End
      Begin VB.TextBox txtUpProjectNumber 
         Height          =   315
         Left            =   1980
         TabIndex        =   44
         Top             =   540
         Width           =   1455
      End
      Begin VB.CheckBox chkUpPriority 
         Alignment       =   1  'Right Justify
         Height          =   255
         Left            =   1920
         TabIndex        =   43
         Tag             =   "NOCLEAR"
         Top             =   2310
         Value           =   1  'Checked
         Width           =   255
      End
      Begin VB.CheckBox chkUpPend 
         Alignment       =   1  'Right Justify
         Height          =   255
         Left            =   1920
         TabIndex        =   42
         Tag             =   "NOCLEAR"
         Top             =   2640
         Value           =   1  'Checked
         Width           =   255
      End
      Begin VB.TextBox txtUpProjectManager 
         Height          =   315
         Left            =   1980
         TabIndex        =   41
         Top             =   3000
         Width           =   3675
      End
      Begin VB.TextBox txtUpFrameRate 
         Height          =   315
         Left            =   1980
         TabIndex        =   40
         Top             =   3360
         Width           =   1455
      End
      Begin VB.CommandButton cmdSaveUpdate 
         Caption         =   "Save Update"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         TabIndex        =   39
         Top             =   4020
         Width           =   1455
      End
      Begin VB.CommandButton cmdCancelUpdate 
         Caption         =   "Cancel"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3540
         TabIndex        =   38
         Top             =   4020
         Width           =   1395
      End
      Begin MSComCtl2.DTPicker datDeadlineDate 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Left            =   1980
         TabIndex        =   46
         ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
         Top             =   1620
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   166985729
         CurrentDate     =   37870
      End
      Begin VB.Label Label11 
         Caption         =   "Select the Tick boxes next to each item for you wish to save a change. Unticked items will be left unchanged."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   615
         Left            =   120
         TabIndex        =   77
         Top             =   4440
         Width           =   7515
      End
      Begin VB.Label Label10 
         Caption         =   "DBB Kit ID"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   66
         Top             =   1320
         Width           =   1875
      End
      Begin VB.Label Label9 
         Caption         =   "Special Project"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   62
         Top             =   1980
         Width           =   1575
      End
      Begin VB.Label Label8 
         Caption         =   "DBB Alpha Display Code"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   57
         Top             =   960
         Width           =   1875
      End
      Begin VB.Label Label1 
         Caption         =   "Priority"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   53
         Top             =   2310
         Width           =   1575
      End
      Begin VB.Label Label2 
         Caption         =   "Pending"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   52
         Top             =   2640
         Width           =   1575
      End
      Begin VB.Label Label3 
         Caption         =   "Project Manager"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   51
         Top             =   3000
         Width           =   1575
      End
      Begin VB.Label Label4 
         Caption         =   "Frame Rate"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   50
         Top             =   3360
         Width           =   1575
      End
      Begin VB.Label Label5 
         Caption         =   "Due Date"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   49
         Top             =   1680
         Width           =   1575
      End
      Begin VB.Label Label6 
         Caption         =   "Project Number"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   48
         Top             =   600
         Width           =   1575
      End
      Begin VB.Label Label7 
         Caption         =   "Compilation Title"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   47
         Top             =   240
         Width           =   1575
      End
   End
   Begin VB.CommandButton cmdUpComp 
      Caption         =   "Update"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   14700
      TabIndex        =   36
      Top             =   1200
      Width           =   975
   End
   Begin VB.CommandButton cmdUpSent 
      Caption         =   "Update"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   13680
      TabIndex        =   35
      Top             =   1200
      Width           =   975
   End
   Begin VB.CommandButton cmdUpMade 
      Caption         =   "Update"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10560
      TabIndex        =   34
      Top             =   1200
      Width           =   1035
   End
   Begin VB.CommandButton cmdSaveRow 
      Caption         =   "Edit Selected Row"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   33
      Top             =   1200
      Width           =   2655
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "All Assets Here"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   13680
      TabIndex        =   32
      Tag             =   "NOCLEAR"
      Top             =   480
      Width           =   2175
   End
   Begin MSAdodcLib.Adodc adoSource 
      Height          =   330
      Left            =   17040
      Top             =   1500
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Pending"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   13680
      TabIndex        =   18
      Tag             =   "NOCLEAR"
      Top             =   240
      Width           =   2175
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "All Items"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   15960
      TabIndex        =   17
      Tag             =   "NOCLEAR"
      Top             =   480
      Width           =   2175
   End
   Begin VB.TextBox txtSeriesTitle 
      Height          =   315
      Left            =   6960
      TabIndex        =   16
      Top             =   360
      Width           =   3075
   End
   Begin VB.TextBox txtRequestID 
      Height          =   315
      Left            =   12240
      TabIndex        =   15
      Top             =   0
      Width           =   1275
   End
   Begin VB.TextBox txtProjectManager 
      Height          =   315
      Left            =   6960
      TabIndex        =   14
      Top             =   0
      Width           =   3075
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Billed"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   15960
      TabIndex        =   13
      Tag             =   "NOCLEAR"
      Top             =   240
      Width           =   2175
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Finished"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   15960
      TabIndex        =   12
      Tag             =   "NOCLEAR"
      Top             =   0
      Width           =   2175
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   13680
      TabIndex        =   11
      Tag             =   "NOCLEAR"
      Top             =   0
      Width           =   2175
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   7620
      TabIndex        =   1
      Top             =   11880
      Width           =   17835
      Begin VB.CommandButton cmdUnbillAll 
         Caption         =   "Unmark Billing All"
         Height          =   315
         Left            =   1380
         TabIndex        =   10
         Top             =   0
         Width           =   2235
      End
      Begin VB.CommandButton cmdBillAll 
         Caption         =   "Bill All"
         Height          =   315
         Left            =   3780
         TabIndex        =   9
         Top             =   0
         Width           =   2235
      End
      Begin VB.CommandButton cmdUnbill 
         Caption         =   "Unmark as Billed"
         Height          =   315
         Left            =   6180
         TabIndex        =   8
         Top             =   0
         Visible         =   0   'False
         Width           =   2235
      End
      Begin VB.CommandButton cmdManualBillItem 
         Caption         =   "Manually Mark as Billed"
         Height          =   315
         Left            =   8580
         TabIndex        =   7
         Top             =   0
         Visible         =   0   'False
         Width           =   2235
      End
      Begin VB.CommandButton cmdBillItem 
         Caption         =   "Bill Item"
         Height          =   315
         Left            =   10920
         TabIndex        =   6
         Top             =   0
         Visible         =   0   'False
         Width           =   1755
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   16560
         TabIndex        =   5
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   15300
         TabIndex        =   4
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   14040
         TabIndex        =   3
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   12780
         TabIndex        =   2
         Top             =   0
         Width           =   1215
      End
   End
   Begin VB.CheckBox chkHideDemo 
      Alignment       =   1  'Right Justify
      Caption         =   "Hide Demo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Tag             =   "NOCLEAR"
      Top             =   420
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnPortalUser 
      Bindings        =   "frmTrackerSvenskCompilations.frx":0000
      Height          =   1755
      Left            =   360
      TabIndex        =   19
      Top             =   3120
      Width           =   7875
      DataFieldList   =   "fullname"
      ListAutoValidate=   0   'False
      MaxDropDownItems=   20
      _Version        =   196617
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   2
      Columns(0).Width=   8819
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "portaluserID"
      Columns(0).Name =   "portaluserID"
      Columns(0).DataField=   "portaluserID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   4551
      Columns(1).Caption=   "fullname"
      Columns(1).Name =   "fullname"
      Columns(1).DataField=   "fullname"
      Columns(1).FieldLen=   256
      _ExtentX        =   13891
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "fullname"
   End
   Begin MSAdodcLib.Adodc adoItems 
      Height          =   330
      Left            =   120
      Top             =   1500
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdCompilations 
      Bindings        =   "frmTrackerSvenskCompilations.frx":001B
      Height          =   8295
      Left            =   120
      TabIndex        =   20
      Top             =   1500
      Width           =   16845
      _Version        =   196617
      stylesets.count =   3
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTrackerSvenskCompilations.frx":0032
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmTrackerSvenskCompilations.frx":004E
      stylesets(2).Name=   "headerfield"
      stylesets(2).ForeColor=   0
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmTrackerSvenskCompilations.frx":006A
      AllowUpdate     =   0   'False
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   24
      Columns(0).Width=   5477
      Columns(0).Caption=   "Compilation Title"
      Columns(0).Name =   "CompilationTitle"
      Columns(0).DataField=   "CompilationTitle"
      Columns(0).FieldLen=   256
      Columns(0).StyleSet=   "headerfield"
      Columns(1).Width=   926
      Columns(1).Caption=   "Pend"
      Columns(1).Name =   "rejected"
      Columns(1).DataField=   "rejected"
      Columns(1).FieldLen=   256
      Columns(1).Style=   2
      Columns(1).StyleSet=   "headerfield"
      Columns(2).Width=   1402
      Columns(2).Caption=   "Project #"
      Columns(2).Name =   "ProjectNo"
      Columns(2).DataField=   "ProjectNo"
      Columns(2).FieldLen=   256
      Columns(2).StyleSet=   "headerfield"
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "Rights Owner"
      Columns(3).Name =   "RightsOwner"
      Columns(3).DataField=   "RightsOwner"
      Columns(3).FieldLen=   256
      Columns(4).Width=   1693
      Columns(4).Caption=   "Alpha Code"
      Columns(4).Name =   "DBBAlphaDisplayCode"
      Columns(4).DataField=   "DBBAlphaDisplayCode"
      Columns(4).FieldLen=   256
      Columns(4).StyleSet=   "headerfield"
      Columns(5).Width=   1058
      Columns(5).Caption=   "Urgent"
      Columns(5).Name =   "urgent"
      Columns(5).DataField=   "urgent"
      Columns(5).FieldLen=   256
      Columns(5).Style=   2
      Columns(5).StyleSet=   "headerfield"
      Columns(6).Width=   2328
      Columns(6).Caption=   "Project Manager"
      Columns(6).Name =   "ProjectManager"
      Columns(6).DataField=   "ProjectManager"
      Columns(6).FieldLen=   256
      Columns(6).StyleSet=   "headerfield"
      Columns(7).Width=   1085
      Columns(7).Caption=   "Sp. Pr."
      Columns(7).Name =   "SpecialProject"
      Columns(7).DataField=   "SpecialProject"
      Columns(7).FieldLen=   256
      Columns(7).StyleSet=   "headerfield"
      Columns(8).Width=   1799
      Columns(8).Caption=   "Due Date"
      Columns(8).Name =   "DueDate"
      Columns(8).DataField=   "DueDate"
      Columns(8).FieldLen=   256
      Columns(8).StyleSet=   "headerfield"
      Columns(9).Width=   1217
      Columns(9).Caption=   "Fr Rate"
      Columns(9).Name =   "framerate"
      Columns(9).DataField=   "framerate"
      Columns(9).FieldLen=   256
      Columns(9).StyleSet=   "headerfield"
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "jobID"
      Columns(10).Name=   "jobID"
      Columns(10).DataField=   "jobID"
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "Launch Date"
      Columns(11).Name=   "LaunchDateforEndCustomer"
      Columns(11).DataField=   "LaunchDateforEndCustomer"
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "Title Series"
      Columns(12).Name=   "TitleSeries"
      Columns(12).DataField=   "TitleSeries"
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "Season"
      Columns(13).Name=   "SeasonNo"
      Columns(13).DataField=   "SeasonNo"
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "Vol No"
      Columns(14).Name=   "VolNo"
      Columns(14).DataField=   "VolNo"
      Columns(14).FieldLen=   256
      Columns(15).Width=   900
      Columns(15).Caption=   "Here"
      Columns(15).Name=   "Allassetshere"
      Columns(15).DataField=   "Allassetshere"
      Columns(15).FieldLen=   256
      Columns(15).Style=   2
      Columns(15).StyleSet=   "stagefield"
      Columns(16).Width=   1799
      Columns(16).Caption=   "Comp Made"
      Columns(16).Name=   "CompilationMade"
      Columns(16).DataField=   "CompilationMade"
      Columns(16).FieldLen=   256
      Columns(16).StyleSet=   "stagefield"
      Columns(17).Width=   1799
      Columns(17).Caption=   "XML Made"
      Columns(17).Name=   "XMLMade"
      Columns(17).DataField=   "XMLMade"
      Columns(17).FieldLen=   256
      Columns(17).StyleSet=   "stagefield"
      Columns(18).Width=   1799
      Columns(18).Caption=   "XML Sent"
      Columns(18).Name=   "XMLSent"
      Columns(18).DataField=   "XMLSent"
      Columns(18).FieldLen=   256
      Columns(18).StyleSet=   "stagefield"
      Columns(19).Width=   1799
      Columns(19).Caption=   "Comp Sent"
      Columns(19).Name=   "CompilationSent"
      Columns(19).DataField=   "CompilationSent"
      Columns(19).FieldLen=   256
      Columns(19).StyleSet=   "stagefield"
      Columns(20).Width=   1799
      Columns(20).Caption=   "Completed"
      Columns(20).Name=   "Completed"
      Columns(20).DataField=   "Completed"
      Columns(20).FieldLen=   256
      Columns(20).StyleSet=   "stagefield"
      Columns(21).Width=   926
      Columns(21).Caption=   "Done"
      Columns(21).Name=   "readytobill"
      Columns(21).DataField=   "readytobill"
      Columns(21).FieldLen=   256
      Columns(21).Style=   2
      Columns(21).StyleSet=   "conclusionfield"
      Columns(22).Width=   926
      Columns(22).Caption=   "Billed"
      Columns(22).Name=   "billed"
      Columns(22).DataField=   "billed"
      Columns(22).FieldLen=   256
      Columns(22).Style=   2
      Columns(22).StyleSet=   "conclusionfield"
      Columns(23).Width=   3200
      Columns(23).Visible=   0   'False
      Columns(23).Caption=   "KitID"
      Columns(23).Name=   "KitID"
      Columns(23).DataField=   "KitID"
      Columns(23).FieldLen=   256
      _ExtentX        =   29713
      _ExtentY        =   14631
      _StockProps     =   79
      Caption         =   "Tracker Compilation"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   1740
      TabIndex        =   21
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The company this job is for"
      Top             =   0
      Width           =   2835
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   5001
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin MSAdodcLib.Adodc adoComments 
      Height          =   330
      Left            =   1800
      Top             =   11880
      Visible         =   0   'False
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoComments"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdComments 
      Bindings        =   "frmTrackerSvenskCompilations.frx":0086
      Height          =   1875
      Left            =   120
      TabIndex        =   22
      Top             =   9960
      Width           =   14175
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12582847
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   5
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "trackerhistoryID"
      Columns(0).Name =   "tracker_commentID"
      Columns(0).DataField=   "tracker_hdd_commentID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "CompilationTitle"
      Columns(1).Name =   "CompilationTitle"
      Columns(1).DataField=   "CompilationTitle"
      Columns(1).FieldLen=   256
      Columns(2).Width=   16722
      Columns(2).Caption=   "Comments"
      Columns(2).Name =   "comment"
      Columns(2).DataField=   "comment"
      Columns(2).FieldLen=   255
      Columns(3).Width=   3360
      Columns(3).Caption=   "Date"
      Columns(3).Name =   "cdate"
      Columns(3).DataField=   "cdate"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).Style=   1
      Columns(4).Width=   3519
      Columns(4).Caption=   "Entered By"
      Columns(4).Name =   "cuser"
      Columns(4).DataField=   "cuser"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      UseDefaults     =   0   'False
      _ExtentX        =   25003
      _ExtentY        =   3307
      _StockProps     =   79
      Caption         =   "Tracker Comments"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoCustomers 
      Height          =   330
      Left            =   14520
      Top             =   10740
      Visible         =   0   'False
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCustomers"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdSource 
      Bindings        =   "frmTrackerSvenskCompilations.frx":00A0
      Height          =   8295
      Left            =   17040
      TabIndex        =   31
      Top             =   1500
      Width           =   11490
      _Version        =   196617
      stylesets.count =   3
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTrackerSvenskCompilations.frx":00B8
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmTrackerSvenskCompilations.frx":00D4
      stylesets(2).Name=   "headerfield"
      stylesets(2).ForeColor=   0
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmTrackerSvenskCompilations.frx":00F0
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   28
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Compilation Title"
      Columns(0).Name =   "CompilationTitle"
      Columns(0).DataField=   "CompilationTitle"
      Columns(0).FieldLen=   256
      Columns(0).Nullable=   2
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "tracker_svensk_compilations_itemID"
      Columns(1).Name =   "tracker_svensk_compilations_itemID"
      Columns(1).DataField=   "tracker_svensk_compilations_itemID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   1984
      Columns(2).Caption=   "Source Alpha"
      Columns(2).Name =   "DBBAlphaDisplayCode"
      Columns(2).DataField=   "SourceDBBAlphaDisplayCode"
      Columns(2).FieldLen=   256
      Columns(2).StyleSet=   "headerfield"
      Columns(3).Width=   5212
      Columns(3).Caption=   "Episode Name"
      Columns(3).Name =   "EpisodeName"
      Columns(3).DataField=   "EpisodeName"
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "headerfield"
      Columns(4).Width=   1138
      Columns(4).Caption=   "Seas #"
      Columns(4).Name =   "SeasonNo"
      Columns(4).DataField=   "SeasonNo"
      Columns(4).FieldLen=   256
      Columns(4).StyleSet=   "headerfield"
      Columns(5).Width=   714
      Columns(5).Caption=   "Eps"
      Columns(5).Name =   "EpisodeNo"
      Columns(5).DataField=   "EpisodeNo"
      Columns(5).FieldLen=   256
      Columns(5).StyleSet=   "headerfield"
      Columns(6).Width=   2408
      Columns(6).Caption=   "Component Type"
      Columns(6).Name =   "ComponentType"
      Columns(6).DataField=   "ComponentType"
      Columns(6).FieldLen=   256
      Columns(6).StyleSet=   "headerfield"
      Columns(7).Width=   1588
      Columns(7).Caption=   "Language"
      Columns(7).Name =   "Language"
      Columns(7).DataField=   "Language"
      Columns(7).FieldLen=   256
      Columns(7).StyleSet=   "headerfield"
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "Conf"
      Columns(8).Name =   "SonytoSync"
      Columns(8).DataField=   "SonytoSync"
      Columns(8).FieldLen=   256
      Columns(8).Style=   2
      Columns(8).StyleSet=   "headerfield"
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "Textless Rq?"
      Columns(9).Name =   "TextlessRequired"
      Columns(9).DataField=   "TextlessRequired"
      Columns(9).FieldLen=   256
      Columns(9).Style=   2
      Columns(9).StyleSet=   "headerfield"
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "ME Req"
      Columns(10).Name=   "MERequired"
      Columns(10).DataField=   "MERequired"
      Columns(10).FieldLen=   256
      Columns(10).Style=   2
      Columns(10).StyleSet=   "headerfield"
      Columns(11).Width=   1958
      Columns(11).Caption=   "Audio Config"
      Columns(11).Name=   "AudioConfig"
      Columns(11).DataField=   "AudioConfig"
      Columns(11).FieldLen=   256
      Columns(11).StyleSet=   "headerfield"
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "cdate"
      Columns(12).Name=   "cdate"
      Columns(12).DataField=   "cdate"
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "mdate"
      Columns(13).Name=   "mdate"
      Columns(13).DataField=   "mdate"
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "cuser"
      Columns(14).Name=   "cuser"
      Columns(14).DataField=   "cuser"
      Columns(14).FieldLen=   256
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "muser"
      Columns(15).Name=   "muser"
      Columns(15).DataField=   "muser"
      Columns(15).FieldLen=   256
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "companyID"
      Columns(16).Name=   "companyID"
      Columns(16).DataField=   "companyID"
      Columns(16).FieldLen=   256
      Columns(17).Width=   2117
      Columns(17).Caption=   "Arrived"
      Columns(17).Name=   "DateArrived"
      Columns(17).DataField=   "DateArrived"
      Columns(17).FieldLen=   256
      Columns(17).Style=   1
      Columns(17).StyleSet=   "stagefield"
      Columns(18).Width=   3200
      Columns(18).Visible=   0   'False
      Columns(18).Caption=   "ProjectNo"
      Columns(18).Name=   "ProjectNo"
      Columns(18).DataField=   "ProjectNo"
      Columns(18).FieldLen=   256
      Columns(19).Width=   3200
      Columns(19).Visible=   0   'False
      Columns(19).Caption=   "RightsOwner"
      Columns(19).Name=   "RightsOwner"
      Columns(19).DataField=   "RightsOwner"
      Columns(19).FieldLen=   256
      Columns(20).Width=   3200
      Columns(20).Visible=   0   'False
      Columns(20).Caption=   "DueDate"
      Columns(20).Name=   "DueDate"
      Columns(20).DataField=   "DueDate"
      Columns(20).FieldLen=   256
      Columns(21).Width=   3200
      Columns(21).Visible=   0   'False
      Columns(21).Caption=   "ProjectManager"
      Columns(21).Name=   "ProjectManager"
      Columns(21).DataField=   "ProjectManager"
      Columns(21).FieldLen=   256
      Columns(22).Width=   3200
      Columns(22).Visible=   0   'False
      Columns(22).Caption=   "TitleSeries"
      Columns(22).Name=   "TitleSeries"
      Columns(22).DataField=   "TitleSeries"
      Columns(22).FieldLen=   256
      Columns(23).Width=   3200
      Columns(23).Visible=   0   'False
      Columns(23).Caption=   "VolNo"
      Columns(23).Name=   "VolNo"
      Columns(23).DataField=   "VolNo"
      Columns(23).FieldLen=   256
      Columns(24).Width=   3200
      Columns(24).Visible=   0   'False
      Columns(24).Caption=   "framerate"
      Columns(24).Name=   "framerate"
      Columns(24).DataField=   "framerate"
      Columns(24).FieldLen=   256
      Columns(25).Width=   3200
      Columns(25).Visible=   0   'False
      Columns(25).Caption=   "LaunchDateforEndCustomer"
      Columns(25).Name=   "LaunchDateforEndCustomer"
      Columns(25).DataField=   "LaunchDateforEndCustomer"
      Columns(25).FieldLen=   256
      Columns(26).Width=   926
      Columns(26).Caption=   "Done"
      Columns(26).Name=   "Completed"
      Columns(26).DataField=   "readytobill"
      Columns(26).FieldLen=   256
      Columns(26).Locked=   -1  'True
      Columns(26).Style=   2
      Columns(26).StyleSet=   "conclusionfield"
      Columns(27).Width=   926
      Columns(27).Caption=   "Billed"
      Columns(27).Name=   "billed"
      Columns(27).DataField=   "Billed"
      Columns(27).FieldLen=   256
      Columns(27).Style=   2
      Columns(27).StyleSet=   "conclusionfield"
      _ExtentX        =   20267
      _ExtentY        =   14631
      _StockProps     =   79
      Caption         =   "Tracker Source"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblCaption 
      Caption         =   "Alpha Code"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   78
      Left            =   4740
      TabIndex        =   64
      Top             =   780
      Width           =   2115
   End
   Begin VB.Label lblCaption 
      Caption         =   "Special Project"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   10140
      TabIndex        =   59
      Top             =   420
      Width           =   2055
   End
   Begin VB.Image Image1 
      Height          =   1575
      Left            =   8880
      Top             =   3000
      Width           =   2535
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   18
      Left            =   4740
      TabIndex        =   30
      Top             =   2940
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblLastTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   255
      Left            =   16500
      TabIndex        =   29
      Top             =   11520
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Project Number"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   10140
      TabIndex        =   28
      Top             =   60
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Compilation Title"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   4740
      TabIndex        =   27
      Top             =   420
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Project Manager"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   4740
      TabIndex        =   26
      Top             =   60
      Width           =   2055
   End
   Begin VB.Label lblTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   255
      Left            =   16500
      TabIndex        =   25
      Top             =   11160
      Visible         =   0   'False
      Width           =   4455
   End
   Begin VB.Label lblCompanyID 
      Height          =   195
      Left            =   3300
      TabIndex        =   24
      Top             =   420
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   10
      Left            =   150
      TabIndex        =   23
      Top             =   60
      Width           =   915
   End
End
Attribute VB_Name = "frmTrackerSvenskCompilations"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strSearch As String
Dim m_strOrderby As String
Dim s_strOrderby As String
Dim m_blnDelete As Boolean
Dim m_blnUpdate As Boolean
Dim m_blnBillAll As Boolean

Private Sub chkHideDemo_Click()

Dim l_strSQL As String

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/compilationtracker %' and companyID > 100 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/compilationtracker %' ORDER BY name;"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

cmdSearch.Value = True

End Sub

Private Sub cmbCompany_Click()

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text
m_strSearch = " WHERE companyID = " & lblCompanyID.Caption
m_strOrderby = " ORDER BY duedate, ProjectNo, CompilationTitle "
s_strOrderby = " ORDER BY tracker_svensk_compilations_itemID "
'm_strOrderby = " ORDER BY componentsavailable DESC, duedate, LineItemID "
cmdSearch.Value = True

End Sub

Private Sub cmbComponentsAvailable_Click()
cmdSearch_Click
End Sub

Private Sub cmdBillAll_Click()

m_blnBillAll = True

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then
    
    Do While Not adoItems.Recordset.EOF
        If cmdBillItem.Visible = True Then
            cmdBillItem.Value = True
        End If
        adoItems.Recordset.MoveNext
        DoEvents
    Loop
End If

m_blnBillAll = False
adoItems.Refresh
If adoItems.Recordset.RecordCount > 0 Then
    MsgBox "Some items did not bill - check and if necessary the ownership of the clips - this is often the reason.", vbInformation
End If
End Sub

Private Sub cmdCancelUpdate_Click()
fraUpdates.Visible = False
End Sub

Private Sub cmdBillItem_Click()

Dim l_strChargeCode As String, l_rstTrackerChargeCode As ADODB.Recordset, l_strJobLine As String, l_lngJobID As Long, l_lngDuration As Long, l_strLanguage As String, l_rst As ADODB.Recordset
Dim l_lngQuantity As Long, l_strCode As String, l_lngFactor As Long, l_strLastComment As String, l_strLastCommentCuser As String, l_datLastCommentDate As Date, l_blnFirstLine As Boolean
Dim l_blnError As Boolean, l_lngEventID As Long, l_varBookmark As Variant, l_strSeasonNo As String, l_strVolNo As String, l_strAudioLanguageExclude As String

If frmJob.txtJobID.Text <> "" Then
    'A job is loaded - now check that it isn't locked.
    l_lngJobID = Val(frmJob.txtJobID.Text)
    If frmJob.txtStatus.Text = "Confirmed" And frmJob.lblCompanyID.Caption = lblCompanyID.Caption Then
        If grdCompilations.Columns("CompilationTitle").Text <> "" And adoSource.Recordset.RecordCount > 0 And Val(GetDataSQL("SELECT eventID FROM events WHERE eventtitle = '" & QuoteSanitise(grdCompilations.Columns("CompilationTitle").Text) & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Quicktime';")) <> 0 Then
            'The Job is valid - go ahead and create Job lines for this item.
            l_strJobLine = grdCompilations.Columns("CompilationTitle").Text
            l_blnError = False
            If adoComments.Recordset.RecordCount > 0 Then
                adoComments.Recordset.MoveLast
                l_strLastCommentCuser = adoComments.Recordset("cuser")
                l_datLastCommentDate = adoComments.Recordset("cdate")
                l_strLastComment = adoComments.Recordset("comment")
                If adoComments.Recordset("emailclock") <> 0 Then l_strLastComment = l_strLastComment & ", MX1 to Replace Clock"
                If adoComments.Recordset("emailbarsandtone") <> 0 Then l_strLastComment = l_strLastComment & ", MX1 to Replace Bars and Tone"
                If adoComments.Recordset("emailtimecode") <> 0 Then l_strLastComment = l_strLastComment & ", MX1 to Correct Timecode"
                If adoComments.Recordset("emailblackatend") <> 0 Then l_strLastComment = l_strLastComment & ", MX1 to correct Black at End"
            Else
                l_strLastCommentCuser = ""
                l_datLastCommentDate = 0
                l_strLastComment = ""
            End If
            If grdCompilations.Columns("completed").Text <> "" Then
                l_lngDuration = 0
                l_strCode = "O"
                Set l_rst = ExecuteSQL("SELECT DISTINCT language FROM tracker_svensk_compilations_item WHERE CompilationTitle = '" & QuoteSanitise(grdCompilations.Columns("CompilationTitle").Text) & "' AND componenttype = 'Video' AND readytobill <> 0 and billed = 0;", g_strExecuteError)
                CheckForSQLError
                If l_rst.RecordCount > 0 Then
                    l_rst.MoveFirst
                    l_strAudioLanguageExclude = ""
                    Do While Not l_rst.EOF
                        l_strLanguage = l_rst("language")
                        l_strAudioLanguageExclude = l_strAudioLanguageExclude & " AND language <> '" & l_strLanguage & "'"
                        l_lngQuantity = GetDataSQL("SELECT COUNT(tracker_svensk_compilations_itemID) FROM tracker_svensk_compilations_item WHERE CompilationTitle = '" & QuoteSanitise(grdCompilations.Columns("CompilationTitle").Text) & "' AND componenttype = 'Video' AND language = '" & l_strLanguage & "' AND readytobill <> 0 and billed = 0;")
                        l_strSeasonNo = Trim(" " & GetDataSQL("SELECT SeasonNo FROM tracker_svensk_compilations_item WHERE CompilationTitle = '" & QuoteSanitise(grdCompilations.Columns("CompilationTitle").Text) & "' AND componenttype = 'Video' AND readytobill <> 0 and billed = 0;"))
                        l_strVolNo = Trim(" " & GetDataSQL("SELECT VolNo FROM tracker_svensk_compilations_item WHERE CompilationTitle = '" & QuoteSanitise(grdCompilations.Columns("CompilationTitle").Text) & "' AND componenttype = 'Video' AND readytobill <> 0 and billed = 0;"))
                        If l_lngQuantity > 10 And l_lngQuantity <= 13 Then
                            MakeJobDetailLine l_lngJobID, l_strCode, Trim(l_strJobLine & " Compilation Video " & l_strLanguage), 1, "DADCCOMP013", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdCompilations.Columns("ProjectManager").Text, grdCompilations.Columns("projectno").Text, l_strSeasonNo, l_strVolNo, "", grdCompilations.Columns("RightsOwner").Text, l_strJobLine
                        ElseIf l_lngQuantity > 7 And l_lngQuantity <= 10 Then
                            MakeJobDetailLine l_lngJobID, l_strCode, Trim(l_strJobLine & " Compilation Video " & l_strLanguage), 1, "DADCCOMP010", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdCompilations.Columns("ProjectManager").Text, grdCompilations.Columns("projectno").Text, l_strSeasonNo, l_strVolNo, "", grdCompilations.Columns("RightsOwner").Text, l_strJobLine
                        ElseIf l_lngQuantity > 4 And l_lngQuantity <= 7 Then
                            MakeJobDetailLine l_lngJobID, l_strCode, Trim(l_strJobLine & " Compilation Video " & l_strLanguage), 1, "DADCCOMP007", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdCompilations.Columns("ProjectManager").Text, grdCompilations.Columns("projectno").Text, l_strSeasonNo, l_strVolNo, "", grdCompilations.Columns("RightsOwner").Text, l_strJobLine
                        ElseIf l_lngQuantity <= 4 Then
                            MakeJobDetailLine l_lngJobID, l_strCode, Trim(l_strJobLine & " Compilation Video " & l_strLanguage), 1, "DADCCOMP004", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdCompilations.Columns("ProjectManager").Text, grdCompilations.Columns("projectno").Text, l_strSeasonNo, l_strVolNo, "", grdCompilations.Columns("RightsOwner").Text, l_strJobLine
                        Else
                            l_blnError = True
                        End If
                        l_rst.MoveNext
                    Loop
                End If
                l_rst.Close
                Set l_rst = ExecuteSQL("SELECT DISTINCT language FROM tracker_svensk_compilations_item WHERE CompilationTitle = '" & QuoteSanitise(grdCompilations.Columns("CompilationTitle").Text) & "' AND componenttype = 'Audio'" & l_strAudioLanguageExclude & " AND readytobill <> 0 and billed = 0;", g_strExecuteError)
                CheckForSQLError
                If l_rst.RecordCount > 0 Then
                    l_rst.MoveFirst
                    Do While Not l_rst.EOF
                        l_lngQuantity = GetDataSQL("SELECT COUNT(tracker_svensk_compilations_itemID) FROM tracker_svensk_compilations_item WHERE CompilationTitle = '" & QuoteSanitise(grdCompilations.Columns("CompilationTitle").Text) & "' AND componenttype = 'Audio' AND language = '" & l_rst("language") & "' AND readytobill <> 0 and billed = 0;")
                        If l_lngQuantity > 10 And l_lngQuantity <= 13 Then
                            MakeJobDetailLine l_lngJobID, l_strCode, Trim(l_strJobLine & " Compilation Audio " & l_rst("language")), 1, "DADCCOMP013", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdCompilations.Columns("ProjectManager").Text, grdCompilations.Columns("projectno").Text, l_strSeasonNo, l_strVolNo, "", grdCompilations.Columns("RightsOwner").Text, l_strJobLine
                        ElseIf l_lngQuantity > 7 And l_lngQuantity <= 10 Then
                            MakeJobDetailLine l_lngJobID, l_strCode, Trim(l_strJobLine & " Compilation Audio " & l_rst("language")), 1, "DADCCOMP010", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdCompilations.Columns("ProjectManager").Text, grdCompilations.Columns("projectno").Text, l_strSeasonNo, l_strVolNo, "", grdCompilations.Columns("RightsOwner").Text, l_strJobLine
                        ElseIf l_lngQuantity > 4 And l_lngQuantity <= 7 Then
                            MakeJobDetailLine l_lngJobID, l_strCode, Trim(l_strJobLine & " Compilation Audio " & l_rst("language")), 1, "DADCCOMP007", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdCompilations.Columns("ProjectManager").Text, grdCompilations.Columns("projectno").Text, l_strSeasonNo, l_strVolNo, "", grdCompilations.Columns("RightsOwner").Text, l_strJobLine
                        ElseIf l_lngQuantity <= 4 Then
                            MakeJobDetailLine l_lngJobID, l_strCode, Trim(l_strJobLine & " Compilation Audio " & l_rst("language")), 1, "DADCCOMP004", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdCompilations.Columns("ProjectManager").Text, grdCompilations.Columns("projectno").Text, l_strSeasonNo, l_strVolNo, "", grdCompilations.Columns("RightsOwner").Text, l_strJobLine
                        Else
                            MsgBox "The current item has more source files than we currently can Bill", vbCritical, "Error..."
                            l_blnError = True
                        End If
                        l_rst.MoveNext
                    Loop
                End If
                l_rst.Close
                If grdCompilations.Columns("xmlmade").Text <> "" Then
                    l_lngQuantity = 0
                    l_lngEventID = Val(GetDataSQL("SELECT eventID FROM events WHERE eventtitle = '" & QuoteSanitise(grdCompilations.Columns("CompilationTitle").Text) & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Quicktime';"))
                    If l_lngEventID <> 0 Then
                        Set l_rst = ExecuteSQL("SELECT Count(eventlogging.eventloggingID) FROM eventlogging WHERE eventID = " & l_lngEventID & ";", g_strExecuteError)
                        CheckForSQLError
                        If l_rst.RecordCount >= 0 Then
                            l_lngQuantity = l_rst(0)
                        End If
                        l_rst.Close
                    End If
                    If l_lngQuantity > 15 Then
                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING25", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdCompilations.Columns("ProjectManager").Text, grdCompilations.Columns("projectno").Text, l_strSeasonNo, l_strVolNo, "", grdCompilations.Columns("RightsOwner").Text, l_strJobLine
                    ElseIf l_lngQuantity > 4 Then
                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING15", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdCompilations.Columns("ProjectManager").Text, grdCompilations.Columns("projectno").Text, l_strSeasonNo, l_strVolNo, "", grdCompilations.Columns("RightsOwner").Text, l_strJobLine
                    ElseIf l_lngQuantity >= 0 Then
                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING04", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdCompilations.Columns("ProjectManager").Text, grdCompilations.Columns("projectno").Text, l_strSeasonNo, l_strVolNo, "", grdCompilations.Columns("RightsOwner").Text, l_strJobLine
                    Else
                        MsgBox "There was a problem counting the logging items for this Compilation.", vbCritical, "Error..."
                        l_blnError = True
                    End If
                End If
                Set l_rst = Nothing
            End If
            frmJob.adoJobDetail.Refresh
            If l_blnError = False Then
                ExecuteSQL "UPDATE tracker_svensk_compilations_item SET billed = -1, jobID = " & l_lngJobID & " WHERE CompilationTitle = '" & QuoteSanitise(grdCompilations.Columns("CompilationTitle").Text) & "' AND readytobill <> 0 and billed = 0;", g_strExecuteError
                cmdBillItem.Visible = False
                cmdManualBillItem.Visible = False
                cmdUnbill.Visible = True
                If m_blnBillAll = False Then
                    l_varBookmark = grdCompilations.Bookmark
                    adoItems.Refresh
                    grdCompilations.Bookmark = l_varBookmark
                    lblTrackeritemID.Caption = Trim(grdCompilations.Columns("CompilationTitle").Text)
                    grdSource.Caption = lblTrackeritemID.Caption
                    adoSource.ConnectionString = g_strConnection
                    adoSource.RecordSource = "SELECT * FROM tracker_svensk_compilations_item " & m_strSearch & " And CompilationTitle = '" & QuoteSanitise(lblTrackeritemID.Caption) & "' " & s_strOrderby & ";"
                    Debug.Print adoSource.RecordSource
                    adoSource.Refresh
                    adoSource.Caption = adoSource.Recordset.RecordCount & " item(s)"
                End If
            End If
'        ElseIf Val(GetDataSQL("SELECT eventID FROM events WHERE eventtitle = '" & QuoteSanitise(grdCompilations.Columns("CompilationTitle").Text) & "' AND companyID = " & lblCompanyID.Caption & " AND clipformat = 'Quicktime' AND clipcodec = 'ProRes HQ';")) = 0 Then
'            MsgBox "There was a problem counting the logging items for this Compilation.", vbCritical, "Cannot Bill Item"
'        Else
'            MsgBox "There was an unspecified problem billing this line.", vbCritical, "Cannot Bill Item"
        End If
    Else
        MsgBox "The current Job must belong to the correct company and be confirmed in order to bill an item.", vbCritical, "Cannot Bill Item"
    End If
Else
    MsgBox "A Job must be created and loaded into the Jobs form in order to bill an item.", vbCritical, "Cannot Bill Item"
End If

End Sub

Private Sub cmdClear_Click()

ClearFields Me

End Sub

Private Sub cmdClearComments_Click()

'optComments(0).Value = False
'optComments(1).Value = False

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdGetCompilationTitle_Click()

Clipboard.Clear
Clipboard.SetText grdCompilations.Columns("CompilationTitle").Text

End Sub

Private Sub cmdManualBillItem_Click()

Dim l_strSQL As String, l_lngBookmark As Long

l_strSQL = "UPDATE tracker_svensk_compilations_item SET billed = 1 WHERE CompilationTitle = '" & QuoteSanitise(lblTrackeritemID.Caption) & "';"

Debug.Print l_strSQL

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_lngBookmark = grdCompilations.Bookmark

adoItems.Refresh

grdCompilations.Bookmark = l_lngBookmark

adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"

End Sub

Private Sub cmdPrint_Click()

Dim l_strSQL As String

If lblCompanyID.Caption <> "" Then
    l_strSQL = adoItems.RecordSource
    PrintCrystalReportUsingCleanSQL g_strLocationOfCrystalReportFiles & "GenericTrackerReport.rpt", l_strSQL, True
End If

End Sub

Private Sub cmdSaveRow_Click()

If grdCompilations.Columns("billed").Text <> 0 Then
    If MsgBox("Are you sure?", vbYesNo, "You are about to edit details of Finished Items") = vbNo Then
        Exit Sub
    End If
End If

Dim l_lngCount As Long
For l_lngCount = 0 To 9
    chkUpdate(l_lngCount).Value = 0
Next

fraUpdates.Visible = True
txtUpCompilation.Text = grdCompilations.Columns("CompilationTitle").Text
txtUpProjectNumber.Text = grdCompilations.Columns("ProjectNo").Text
datDeadlineDate.Value = Format(grdCompilations.Columns("duedate").Text, vbShortDateFormat)
txtAlphaDisplayCode.Text = grdCompilations.Columns("DBBAlphaDisplayCode").Text
txtEditSpecialProject.Text = grdCompilations.Columns("SpecialProject").Text
txtKitID.Text = grdCompilations.Columns("KitID").Text
If grdCompilations.Columns("urgent").Text <> 0 Then
    chkUpPriority.Value = vbChecked
Else
    chkUpPriority.Value = False
End If
If grdCompilations.Columns("rejected").Text <> 0 Then
    chkUpPend.Value = vbChecked
Else
    chkUpPend.Value = False
End If
txtUpProjectManager.Text = grdCompilations.Columns("ProjectManager").Text
txtUpFrameRate.Text = grdCompilations.Columns("framerate").Text

End Sub

Private Sub cmdSearch_Click()

Dim l_rstTotal As ADODB.Recordset, l_lngVideoTotal As Long, l_strSQL As String

If lblCompanyID.Caption <> "" Then

    If optComplete(0).Value = True Then
        'Not Complete
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) "
    ElseIf optComplete(1).Value = True Then
        'Finished
        l_strSQL = " AND (billed IS NULL OR billed = 0) AND readytobill <> 0 "
    ElseIf optComplete(2).Value = True Then
        'Billed
        l_strSQL = " AND billed IS NOT NULL AND billed <> 0 "
    ElseIf optComplete(4).Value = True Then
        'Pending
        l_strSQL = " AND rejected <> 0 "
    ElseIf optComplete(5).Value = True Then
        'All Assets Here
        l_strSQL = " AND Allassetshere <> 0 "
    Else

    End If
    
    If txtProjectManager.Text <> "" Then
        l_strSQL = l_strSQL & " AND ProjectManager LIKE '%" & QuoteSanitise(txtProjectManager.Text) & "%' "
    End If
    
    If txtSeriesTitle.Text <> "" Then
        l_strSQL = l_strSQL & " AND CompilationTitle LIKE '%" & QuoteSanitise(txtSeriesTitle.Text) & "%' "
    End If
    
    If txtSpecialProject.Text <> "" Then
        l_strSQL = l_strSQL & " AND SpecialProject LIKE '%" & QuoteSanitise(txtSpecialProject.Text) & "%' "
    End If
    
    If txtRequestID.Text <> "" Then
        l_strSQL = l_strSQL & " AND ProjectNo LIKE '%" & txtRequestID.Text & "%' "
    End If
        
    If txtAlphaCode.Text <> "" Then
        l_strSQL = l_strSQL & " AND DBBAlphaDisplayCode LIKE '%" & txtAlphaCode.Text & "%' "
    End If
        
    Debug.Print l_strSQL
    adoItems.ConnectionString = g_strConnection
    adoItems.RecordSource = "SELECT DISTINCT companyID, ProjectNo, RightsOwner, readytobill, jobID, billed, Allassetshere, DueDate, rejected, " & _
    "LaunchDateforEndCustomer, ProjectManager, urgent, TitleSeries, CompilationTitle, VolNo, framerate, CompilationMade, DBBAlphaDisplayCode, KitID, " & _
    "CompilationSent, XMLMade, XMLSent, Completed, SpecialProject FROM tracker_svensk_compilations_item " & m_strSearch & l_strSQL & m_strOrderby & ";"
    
    Debug.Print adoItems.RecordSource
    adoItems.Refresh
    adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"
    If adoItems.Recordset.RecordCount > 0 Then
        adoItems.Recordset.MoveNext
        adoItems.Recordset.MovePrevious
    End If

End If

End Sub

Private Sub cmdUnbill_Click()

Dim l_varBookmark As Variant

ExecuteSQL "UPDATE tracker_svensk_compilations_item SET billed = 0, jobID = 0 WHERE CompilationTitle = '" & QuoteSanitise(grdCompilations.Columns("CompilationTitle").Text) & "' AND readytobill <> 0 and billed <> 0;", g_strExecuteError
CheckForSQLError
If m_blnBillAll = False Then
    l_varBookmark = grdCompilations.Bookmark
    adoItems.Refresh
    grdCompilations.Bookmark = l_varBookmark
End If
cmdBillItem.Visible = True
cmdManualBillItem.Visible = True

End Sub

Private Sub cmdUnbillAll_Click()

m_blnBillAll = True

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then

    While Not adoItems.Recordset.EOF
        If cmdUnbill.Visible = True Then
            cmdUnbill.Value = True
        End If
        adoItems.Recordset.MoveNext
    Wend
End If

m_blnBillAll = False

grdCompilations.Refresh

End Sub

Private Sub cmdUpComp_Click()

If grdCompilations.Columns("billed").Text <> 0 Then Exit Sub

Dim l_strSQL As String
Dim l_lngBookmark As String
If grdCompilations.Columns("Completed").Text = "" Then
    l_strSQL = "UPDATE tracker_svensk_compilations_item SET Completed = '" & Format(Now, "yyyy-mm-dd hh:mm:ss") & "', readytobill = -1, mdate = getdate(), muser = '" & g_strUserInitials & "' WHERE (CompilationTitle = '" & QuoteSanitise(lblTrackeritemID.Caption) & "')"
Else
    l_strSQL = "UPDATE tracker_svensk_compilations_item SET Completed = NULL, readytobill = 0, mdate = getdate(), muser = '" & g_strUserInitials & "' WHERE (CompilationTitle = '" & QuoteSanitise(lblTrackeritemID.Caption) & "')"
End If

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_lngBookmark = grdCompilations.Bookmark
grdCompilations.Refresh

adoItems.Refresh

grdCompilations.Bookmark = l_lngBookmark

End Sub

Private Sub cmdUpMade_Click()

If grdCompilations.Columns("billed").Text <> 0 Then Exit Sub

Dim l_strSQL As String
Dim l_lngBookmark As Variant
'CompilationMade

If grdCompilations.Columns("CompilationMade").Text = "" Then
    l_strSQL = "UPDATE tracker_svensk_compilations_item SET CompilationMade = '" & Format(Now, "yyyy-mm-dd hh:mm:ss") & "', mdate = getdate(), muser = '" & g_strUserInitials & "' WHERE (CompilationTitle = '" & QuoteSanitise(lblTrackeritemID.Caption) & "')"
Else
    l_strSQL = "UPDATE tracker_svensk_compilations_item SET CompilationMade = NULL, mdate = getdate(), muser = '" & g_strUserInitials & "' WHERE (CompilationTitle = '" & QuoteSanitise(lblTrackeritemID.Caption) & "')"
End If
If optComplete(0).Value = True Then
    'Not Complete
    l_strSQL = " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) "
ElseIf optComplete(1).Value = True Then
    'Finished
    l_strSQL = " AND (billed IS NULL OR billed = 0) AND readytobill <> 0 "
ElseIf optComplete(2).Value = True Then
    'Billed
    l_strSQL = " AND billed IS NOT NULL AND billed <> 0 "
ElseIf optComplete(4).Value = True Then
    'Pending
    l_strSQL = " AND rejected <> 0 "
ElseIf optComplete(5).Value = True Then
    'All Assets Here
    l_strSQL = " AND Allassetshere <> 0 "
Else

End If

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_lngBookmark = grdCompilations.Bookmark

adoItems.Refresh

grdCompilations.Bookmark = l_lngBookmark

End Sub

Private Sub cmdUpSent_Click()

If grdCompilations.Columns("billed").Text <> 0 Then Exit Sub

Dim l_strSQL As String
Dim l_lngBookmark As Variant

If grdCompilations.Columns("CompilationSent").Text = "" Then
    l_strSQL = "UPDATE tracker_svensk_compilations_item SET  CompilationSent = '" & Format(Now, "yyyy-mm-dd hh:mm:ss") & "', mdate = getdate(), muser = '" & g_strUserInitials & "' WHERE (CompilationTitle = '" & QuoteSanitise(lblTrackeritemID.Caption) & "')"
Else
    l_strSQL = "UPDATE tracker_svensk_compilations_item SET  CompilationSent = NULL, mdate = getdate(), muser = '" & g_strUserInitials & "' WHERE (CompilationTitle = '" & QuoteSanitise(lblTrackeritemID.Caption) & "')"
End If

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_lngBookmark = grdCompilations.Bookmark

adoItems.Refresh

grdCompilations.Bookmark = l_lngBookmark

End Sub

Private Sub cmdSaveUpdate_Click()

If CheckBilledItems(lblTrackeritemID.Caption) = True Then
    If MsgBox("Are you sure?", vbYesNo, "You are about to edit details of Finished Items") = vbNo Then
        Exit Sub
    End If
End If

Dim l_strSQL As String
Dim l_lngBookmark As Variant
Dim FoundOne As Boolean

FoundOne = False

l_strSQL = "UPDATE tracker_svensk_compilations_item SET "

If chkUpdate(0).Value <> 0 Then
    If lblTrackeritemID.Caption <> txtUpCompilation.Text Then
        If MsgBox("Are you sure you want change the Compilation Title?", vbYesNo, "Question") = vbYes Then
            l_strSQL = l_strSQL & "CompilationTitle = '" & QuoteSanitise(txtUpCompilation.Text) & "', "
            FoundOne = True
        End If
    End If
End If
If chkUpdate(1).Value <> 0 Then l_strSQL = l_strSQL & "ProjectNo = '" & txtUpProjectNumber.Text & "', ": FoundOne = True
If chkUpdate(7).Value <> 0 Then l_strSQL = l_strSQL & "rejected = '" & chkUpPend.Value & "', ": FoundOne = True
If chkUpdate(4).Value <> 0 Then l_strSQL = l_strSQL & "DueDate = '" & Format(datDeadlineDate.Value, "yyyy-mm-dd hh:nn:ss") & "', ": FoundOne = True
If chkUpdate(8).Value <> 0 Then l_strSQL = l_strSQL & "ProjectManager = '" & txtUpProjectManager.Text & "', ": FoundOne = True
If chkUpdate(6).Value <> 0 Then l_strSQL = l_strSQL & "urgent = '" & chkUpPriority.Value & "', ": FoundOne = True
If chkUpdate(9).Value <> 0 Then l_strSQL = l_strSQL & "framerate = '" & txtUpFrameRate.Text & "', ": FoundOne = True
If chkUpdate(2).Value <> 0 Then l_strSQL = l_strSQL & "DBBAlphaDisplayCode = '" & txtAlphaDisplayCode.Text & "', ": FoundOne = True
If chkUpdate(3).Value <> 0 Then l_strSQL = l_strSQL & "KitID = '" & txtKitID.Text & "', ": FoundOne = True
If chkUpdate(5).Value <> 0 Then l_strSQL = l_strSQL & "SpecialProject = '" & QuoteSanitise(txtEditSpecialProject.Text) & "', ": FoundOne = True
l_strSQL = l_strSQL & "mdate = getdate(), "
l_strSQL = l_strSQL & "muser = '" & g_strUserInitials & "' "
l_strSQL = l_strSQL & "WHERE (CompilationTitle = '" & QuoteSanitise(lblTrackeritemID.Caption) & "')"
If optComplete(0).Value = True Then
    'Not Complete
    l_strSQL = l_strSQL & " AND (billed IS NULL OR billed = 0) AND (readytobill IS NULL OR readytobill = 0) "
ElseIf optComplete(1).Value = True Then
    'Finished
    l_strSQL = l_strSQL & " AND (billed IS NULL OR billed = 0) AND readytobill <> 0 "
ElseIf optComplete(2).Value = True Then
    'Billed
    l_strSQL = l_strSQL & " AND billed IS NOT NULL AND billed <> 0 "
ElseIf optComplete(4).Value = True Then
    'Pending
    l_strSQL = l_strSQL & " AND rejected <> 0 "
ElseIf optComplete(5).Value = True Then
    'All Assets Here
    l_strSQL = l_strSQL & " AND Allassetshere <> 0 "
Else

End If

If FoundOne = False Then
    MsgBox "No items selected to be updated", vbInformation, "Nothing Saved"
    Exit Sub
End If

Debug.Print l_strSQL
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_lngBookmark = grdCompilations.Bookmark

adoItems.Refresh

grdCompilations.Bookmark = l_lngBookmark

adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"

fraUpdates.Visible = False

End Sub

Private Sub cmdUpXMLMade_Click()

If grdCompilations.Columns("billed").Text <> 0 Then Exit Sub

Dim l_strSQL As String
Dim l_lngBookmark As Variant

If grdCompilations.Columns("xmlmade").Text = "" Then
    l_strSQL = "UPDATE tracker_svensk_compilations_item SET  xmlmade = '" & Format(Now, "yyyy-mm-dd hh:mm:ss") & "', mdate = getdate(), muser = '" & g_strUserInitials & "' WHERE (CompilationTitle = '" & QuoteSanitise(lblTrackeritemID.Caption) & "')"
Else
    l_strSQL = "UPDATE tracker_svensk_compilations_item SET  xmlmade = NULL, mdate = getdate(), muser = '" & g_strUserInitials & "' WHERE (CompilationTitle = '" & QuoteSanitise(lblTrackeritemID.Caption) & "')"
End If

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_lngBookmark = grdCompilations.Bookmark

adoItems.Refresh

grdCompilations.Bookmark = l_lngBookmark

End Sub

Private Sub cmdUpXMLSent_Click()

If grdCompilations.Columns("billed").Text <> 0 Then Exit Sub

Dim l_strSQL As String
Dim l_lngBookmark As Variant

If grdCompilations.Columns("xmlsent").Text = "" Then
    l_strSQL = "UPDATE tracker_svensk_compilations_item SET xmlsent = '" & Format(Now, "yyyy-mm-dd hh:mm:ss") & "', mdate = getdate(), muser = '" & g_strUserInitials & "' WHERE (CompilationTitle = '" & QuoteSanitise(lblTrackeritemID.Caption) & "')"
Else
    l_strSQL = "UPDATE tracker_svensk_compilations_item SET xmlsent = NULL, mdate = getdate(), muser = '" & g_strUserInitials & "' WHERE (CompilationTitle = '" & QuoteSanitise(lblTrackeritemID.Caption) & "')"
End If

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_lngBookmark = grdCompilations.Bookmark

adoItems.Refresh

grdCompilations.Bookmark = l_lngBookmark

End Sub

Private Sub ddnPortalUser_CloseUp()

grdCompilations.Columns("portaluserID").Text = ddnPortalUser.Columns("portaluserID").Text

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

grdCompilations.StyleSets("headerfield").BackColor = &HE7FFE7
grdCompilations.StyleSets("stagefield").BackColor = &HE7FFFF
grdCompilations.StyleSets("conclusionfield").BackColor = &HFFFFE7

grdSource.StyleSets("headerfield").BackColor = &HE7FFE7
grdSource.StyleSets("stagefield").BackColor = &HE7FFFF
grdSource.StyleSets("conclusionfield").BackColor = &HFFFFE7

grdCompilations.StyleSets.Add "Error"
grdCompilations.StyleSets("Error").BackColor = &HA0A0FF

grdSource.StyleSets.Add "Error"
grdSource.StyleSets("Error").BackColor = &HA0A0FF

optComplete(0).Value = True

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE (cetaclientcode like '%/compilationtracker %') AND companyID > 100 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE (cetaclientcode like '%/compilationtracker %') ORDER BY name;"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub Form_Resize()

On Error Resume Next

grdCompilations.Width = Me.ScaleWidth * 0.6 - grdCompilations.Left - 120
grdSource.Width = Me.ScaleWidth - grdCompilations.Width - grdCompilations.Left - 240
grdSource.Left = grdCompilations.Left + grdCompilations.Width + 120
grdCompilations.Height = (Me.ScaleHeight - grdCompilations.Top - frmButtons.Height) * 0.75 - 240
grdSource.Height = grdCompilations.Height
frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120
grdComments.Top = grdCompilations.Top + grdCompilations.Height + 120
grdComments.Height = frmButtons.Top - grdComments.Top - 120
grdComments.Width = grdCompilations.Width
adoSource.Left = grdSource.Left

End Sub

Private Sub grdComments_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdComments_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdComments_BeforeUpdate(Cancel As Integer)

If m_blnDelete = False Then

    If grdComments.Columns("comment").Text = "" Then
        MsgBox "Cannot save a Comment with no actual comment", vbCritical, "Comment Not Saved"
        Cancel = True
        Exit Sub
    End If
       
    grdComments.Columns("CompilationTitle").Text = lblTrackeritemID.Caption
    If grdComments.Columns("cdate").Text = "" Then
        grdComments.Columns("cdate").Text = Now
    End If
    grdComments.Columns("cuser").Text = g_strFullUserName

End If

End Sub

Private Sub grdCompilations_DblClick()

ShowClipSearch "", "", "", grdCompilations.Columns("CompilationTitle").Text

End Sub

Private Sub grdCompilations_RowLoaded(ByVal Bookmark As Variant)

If Val(grdCompilations.Columns("rejected").Text) <> 0 Then
    grdCompilations.Columns("rejected").CellStyleSet "Error"
End If

If Val(grdCompilations.Columns("urgent").Text) <> 0 Then
    grdCompilations.Columns("urgent").CellStyleSet "Error"
End If

End Sub

Private Sub grdSource_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub
Private Sub grdSource_AfterUpdate(RtnDispErrMsg As Integer)

m_blnUpdate = True

Dim l_strSQL As String
Dim l_varBookmark As Variant
Dim l_rstSave As ADODB.Recordset
Dim l_rstSave1 As ADODB.Recordset
Dim l_strCompilationTitle As String

DoEvents

l_strCompilationTitle = Trim(grdCompilations.Columns("CompilationTitle").Text)

If grdSource.Columns("datearrived").Text <> "" Then

    l_strSQL = "SELECT datearrived FROM tracker_svensk_compilations_item WHERE CompilationTitle = '" & QuoteSanitise(l_strCompilationTitle) & "' and (datearrived IS NULL) ;"
    
    Set l_rstSave = ExecuteSQL(l_strSQL, g_strExecuteError)
    If l_rstSave.RecordCount <= 0 Then
    
        'All items for this comp are here.
        l_strSQL = "UPDATE tracker_svensk_compilations_item SET Allassetshere = -1 WHERE (CompilationTitle = '" & QuoteSanitise(l_strCompilationTitle) & "')"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_varBookmark = grdCompilations.Bookmark
        'grdCompilations.Refresh
        adoItems.Refresh
        grdCompilations.Bookmark = l_varBookmark
        adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"
        DoEvents
        
    Else
    
        'Some other items are still not here.
        l_strSQL = "UPDATE tracker_svensk_compilations_item SET Allassetshere = 0 WHERE (CompilationTitle = '" & QuoteSanitise(l_strCompilationTitle) & "')"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        l_varBookmark = grdCompilations.Bookmark
        'grdCompilations.Refresh
        adoItems.Refresh
        grdCompilations.Bookmark = l_varBookmark
        adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"
        DoEvents
        
    End If
    l_rstSave.Close
    Set l_rstSave = Nothing

Else

    l_strSQL = "UPDATE tracker_svensk_compilations_item SET Allassetshere = 0 WHERE (CompilationTitle = '" & QuoteSanitise(lblTrackeritemID.Caption) & "')"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    l_varBookmark = grdCompilations.Bookmark
    'grdCompilations.Refresh
    adoItems.Refresh
    grdCompilations.Bookmark = l_varBookmark
    adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"
    DoEvents
    
End If

m_blnUpdate = False

End Sub

Private Sub grdSource_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub
Private Sub grdSource_BeforeUpdate(Cancel As Integer)

If m_blnDelete = True Then Exit Sub
If grdCompilations.Columns("done").Text <> 0 Then
    If MsgBox("Are you sure?", vbYesNo, "Updating a supposedly finished item?") = vbNo Then
        Cancel = 1
        Exit Sub
    End If
End If

If lblCompanyID.Caption = "" Or grdSource.Caption = "" Then
    Cancel = 1
    Exit Sub
End If

grdSource.Columns("CompilationTitle").Text = grdSource.Caption
grdSource.Columns("companyID").Text = lblCompanyID.Caption
grdSource.Columns("ProjectNo").Text = grdCompilations.Columns("projectNo").Text
grdSource.Columns("RightsOwner").Text = grdCompilations.Columns("RightsOwner").Text
grdSource.Columns("DueDate").Text = grdCompilations.Columns("DueDate").Text
grdSource.Columns("ProjectManager").Text = grdCompilations.Columns("ProjectManager").Text
grdSource.Columns("TitleSeries").Text = grdCompilations.Columns("TitleSeries").Text
grdSource.Columns("SeasonNo").Text = grdCompilations.Columns("SeasonNo").Text
grdSource.Columns("VolNo").Text = grdCompilations.Columns("VolNo").Text
grdSource.Columns("framerate").Text = grdCompilations.Columns("framerate").Text
grdSource.Columns("LaunchDateForendCustomer").Text = grdCompilations.Columns("LaunchDateForendCustomer").Text
grdSource.Columns("mdate").Text = Now
grdSource.Columns("Muser").Text = g_strUserInitials

DoEvents

End Sub

Private Sub grdCompilations_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim l_strSQL As String

If m_blnUpdate = True Then Exit Sub

lblTrackeritemID.Caption = Trim(grdCompilations.Columns("CompilationTitle").Text)
grdSource.Caption = lblTrackeritemID.Caption

adoSource.ConnectionString = g_strConnection
adoSource.RecordSource = "SELECT * FROM tracker_svensk_compilations_item " & m_strSearch & " And CompilationTitle = '" & QuoteSanitise(lblTrackeritemID.Caption) & "' " & s_strOrderby & ";"
Debug.Print adoSource.RecordSource

'If grdCompilations.Columns("CompilationMade").Text = "" Then
'    cmdUpMade.Caption = "Set"
'Else
'    cmdUpMade.Caption = "Clear"
'End If
'
'If grdCompilations.Columns("CompilationSent").Text = "" Then
'    cmdUpSent.Caption = "Set"
'Else
'    cmdUpSent.Caption = "Clear"
'End If
'
'If grdCompilations.Columns("Completed").Text = "" Then
'    cmdUpComp.Caption = "Set"
'Else
'    cmdUpComp.Caption = "Clear"
'End If
'
'If grdCompilations.Columns("XMLmade").Text = "" Then
'    cmdUpXMLMade.Caption = "Set"
'Else
'    cmdUpXMLMade.Caption = "Clear"
'End If
'
'If grdCompilations.Columns("XMLSent").Text = "" Then
'    cmdUpXMLSent.Caption = "Set"
'Else
'    cmdUpXMLSent.Caption = "Clear"
'End If
'
adoSource.Refresh
adoSource.Caption = adoSource.Recordset.RecordCount & " item(s)"

l_strSQL = "SELECT * FROM tracker_svensk_compilations_comment WHERE CompilationTitle = '" & QuoteSanitise(Trim(lblTrackeritemID.Caption)) & "' ORDER BY cdate;"

adoComments.RecordSource = l_strSQL
adoComments.ConnectionString = g_strConnection
Debug.Print
adoComments.Refresh
    
On Error Resume Next

If grdCompilations.Columns("readytobill").Text <> 0 And grdCompilations.Columns("billed").Text = 0 Then
    cmdBillItem.Visible = True
    cmdManualBillItem.Visible = True
'ElseIf grdCompilations.Columns("complaintredoitem").Text <> 0 Then
'    cmdBillItem.Visible = False
'    cmdManualBillItem.Visible = True
Else
    cmdBillItem.Visible = False
    cmdManualBillItem.Visible = False
End If

If grdCompilations.Columns("billed").Text <> 0 Then
    cmdUnbill.Visible = True
Else
    cmdUnbill.Visible = False
End If

End Sub

Private Sub grdSource_BtnClick()

Dim tempdate As Date

If grdSource.ActiveCell.Text <> "" Then
    grdSource.ActiveCell.Text = ""
Else
    If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
        grdSource.ActiveCell.Text = Now
    Else
        tempdate = FormatDateTime(Now, vbLongDate)
        grdSource.ActiveCell.Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
End If

End Sub

Private Sub grdSource_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim l_strSQL As String

lblTrackeritemID.Caption = Trim(grdSource.Columns("CompilationTitle").Text)

If Val(lblTrackeritemID.Caption) <> Val(lblLastTrackeritemID.Caption) Then
    
    If Val(lblTrackeritemID.Caption) = 0 Then
    
        cmdBillItem.Visible = False
        lblLastTrackeritemID.Caption = ""
        
        Exit Sub
        
    End If
    
    lblLastTrackeritemID.Caption = lblTrackeritemID.Caption
    
End If
    
End Sub

Private Sub optComplete_Click(Index As Integer)
cmdSearch_Click
End Sub

Private Sub txtProjectManager_Change()
'cmdSearch_Click
End Sub

Private Sub txtSeriesTitle_Change()
'cmdSearch_Click
End Sub

Function CheckBilledItems(lp_strCompilationTitle As String) As Boolean

If GetCount("SELECT tracker_svensk_compilations_itemID FROM tracker_svensk_compilations_item WHERE CompilationTitle = '" & QuoteSanitise(lp_strCompilationTitle) & "' AND readytobill <> 0") > 0 Then
    CheckBilledItems = True
Else
    CheckBilledItems = False
End If

End Function
