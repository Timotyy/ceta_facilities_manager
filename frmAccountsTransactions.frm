VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmAccountsTransactions 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Accounts Transactions"
   ClientHeight    =   5265
   ClientLeft      =   1965
   ClientTop       =   4530
   ClientWidth     =   13575
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5265
   ScaleWidth      =   13575
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Preview"
      Height          =   315
      Index           =   1
      Left            =   3000
      TabIndex        =   4
      Top             =   4860
      Width           =   1335
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print"
      Height          =   315
      Index           =   0
      Left            =   1560
      TabIndex        =   3
      Top             =   4860
      Width           =   1335
   End
   Begin VB.CommandButton cmdNew 
      Caption         =   "New"
      Height          =   315
      Left            =   120
      TabIndex        =   1
      Top             =   4860
      Width           =   1335
   End
   Begin MSAdodcLib.Adodc adoTransactions 
      Height          =   330
      Left            =   2820
      Top             =   3540
      Visible         =   0   'False
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoTransactions"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdTransactions 
      Align           =   1  'Align Top
      Bindings        =   "frmAccountsTransactions.frx":0000
      Height          =   4755
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   13575
      _Version        =   196617
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   8421631
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   23945
      _ExtentY        =   8387
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblProjectNumber 
      BackColor       =   &H0000FFFF&
      Caption         =   "PROJECT NUMBER"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7320
      TabIndex        =   2
      Top             =   4860
      Visible         =   0   'False
      Width           =   1815
   End
End
Attribute VB_Name = "frmAccountsTransactions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()



End Sub

Private Sub cmdNew_Click()

NewAccountsTransaction Val(lblProjectNumber.Caption)

adoTransactions.Refresh

End Sub

Private Sub cmdPreview_Click()

End Sub

Private Sub cmdPrint_Click(Index As Integer)

If Index = 1 Then g_blnPreviewReport = True

If grdTransactions.Rows < 1 Then Exit Sub

If Not CheckAccess("/printaccountstransaction") Then Exit Sub

Dim l_lngAccountsTransactionID As Long
l_lngAccountsTransactionID = Val(grdTransactions.Columns("accountstransactionID").Text)
If l_lngAccountsTransactionID = 0 Then Exit Sub

g_blnPreviewReport = True

PrintCrystalReport App.Path & "/reports/accountstransaction.rpt", "{accountstransaction.accountstransactionID} = " & l_lngAccountsTransactionID, g_blnPreviewReport

End Sub

Private Sub Form_Load()
CenterForm Me

End Sub

Private Sub grdTransactions_DblClick()

'load this transaction
ShowAccountsTransaction grdTransactions.Columns("accountstransactionID").Text
adoTransactions.Refresh

End Sub

