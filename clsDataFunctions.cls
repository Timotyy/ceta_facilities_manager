VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsDataFunctions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Function ExecuteSQL(ByVal SQL As String, msgString As String) As ADODB.Recordset
Dim l_varLastID As Variant
g_lngLastID = 0

'Debug.Print SQL

'executes SQL and returns Recordset
Dim cnn As ADODB.Connection, rst As ADODB.Recordset, l_strSQL As String

'Dim sTokens() As String

On Error GoTo ExecuteSQL_Error

'split the SQL up in to individual SQL statements, seperated by ; (semicolon)
'sTokens = Split(SQL, ";")
Set cnn = New ADODB.Connection
cnn.Open g_strConnection
    
    'if this is an action query, and there will be no results returned
    If Left(UCase(SQL), 6) = "INSERT" Or Left(UCase(SQL), 6) = "DELETE" Or Left(UCase(SQL), 6) = "UPDATE" Or Left(UCase(SQL), 5) = "ALTER" Then
        
        'remove a preleading semicolon if there
        If Left(SQL, 1) = ";" Then SQL = Right(SQL, Len(SQL) - 1)
        If Left(UCase(SQL), 6) = "INSERT" Then
            'if insert query we need to get the ID
            If Right(SQL, 1) <> ";" Then SQL = SQL & ";"
            SQL = SQL & "SELECT SCOPE_IDENTITY();"
            Dim l_rstID As New ADODB.Recordset
            Set l_rstID = cnn.Execute(SQL)
            l_varLastID = l_rstID.NextRecordset().Fields(0).Value
            If Not IsNull(l_varLastID) Then g_lngLastID = l_varLastID Else g_lngLastID = 0
        Else
            cnn.Execute SQL
        End If
        
        msgString = SQL & " query successful"
        
    Else
        'this was a select query, so just get back the results
    
        Set rst = New ADODB.Recordset
        rst.Open Trim$(SQL), cnn, adOpenKeyset, adLockOptimistic
        If rst.EOF = False Then
            'rst.MoveLast 'get approx RecordCount
            'rst.MoveFirst
        End If
        Set ExecuteSQL = rst
        msgString = rst.RecordCount & " records found from SQL"
        
    End If


ExecuteSQL_Exit:

Set rst = Nothing
Set cnn = Nothing
Exit Function

ExecuteSQL_Error:
    msgString = "ExecuteSQL *ERROR* " & _
    Err.Description
    g_strDebugSQLString = SQL
    'Resume ExecuteSQL_Exit
End Function

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, Optional lp_ReturnTrueNulls As Boolean) As Variant

On Error GoTo Proc_GetData_Error

Dim l_strSQL As String
l_strSQL = "SELECT TOP 1 " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & QuoteSanitise(lp_varCriterea) & "'"

Dim l_rstGetData As New ADODB.Recordset
Dim l_con As New ADODB.Connection

l_con.Open g_strConnection
l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

If Not l_rstGetData.EOF Then
    
    If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
        GetData = l_rstGetData(lp_strFieldToReturn)
        GoTo Proc_CloseDB
    End If

Else
    GoTo Proc_CloseDB

End If

If lp_ReturnTrueNulls = True Then
    GetData = Null
Else
    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select
End If

Proc_CloseDB:

On Error Resume Next

l_rstGetData.Close
Set l_rstGetData = Nothing

l_con.Close
Set l_con = Nothing

Exit Function

Proc_GetData_Error:

MsgBox "Error in GET-DATA: " & Err.Description


GetData = ""

'GoTo Proc_CloseDB

End Function
Function GetDataSQL(lp_strSQL As String)

On Error GoTo Proc_GetData_Error

Dim l_rstGetData As New ADODB.Recordset
Dim l_conGetData As New ADODB.Connection

l_conGetData.Open g_strConnection
l_rstGetData.Open lp_strSQL, l_conGetData, 3, 3

If Not l_rstGetData.EOF Then
    
    If Not IsNull(l_rstGetData(0)) Then
        GetDataSQL = l_rstGetData(0)
    Else
        Select Case l_rstGetData.Fields(0).Type
        Case adChar, adVarChar, adVarWChar, 201
            GetDataSQL = ""
        Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
            GetDataSQL = 0
        Case adDate, adDBDate, adDBTime, adDBTimeStamp
            GetDataSQL = 0
        Case Else
            GetDataSQL = False
        End Select
    End If

End If

l_rstGetData.Close
Set l_rstGetData = Nothing

l_conGetData.Close
Set l_conGetData = Nothing

Exit Function

Proc_GetData_Error:

GetDataSQL = ""

End Function

Function GetCount(lp_strSQL As String) As Double

Screen.MousePointer = vbHourglass

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open lp_strSQL, l_conSearch, adOpenStatic, adLockReadOnly
End With

l_rstSearch.ActiveConnection = Nothing

If l_rstSearch.EOF Then
    GetCount = 0
Else

    If Not IsNull(l_rstSearch(0)) Then
        GetCount = Val(l_rstSearch(0))
    Else
        GetCount = 0
    End If
End If

l_rstSearch.Close
Set l_rstSearch = Nothing

l_conSearch.Close
Set l_conSearch = Nothing

Screen.MousePointer = vbDefault

End Function


Function RecordExists(lp_strTable As String, lp_strFieldToSearch As String, lp_varCriteria As Variant) As Boolean

Dim l_strSQL As String
l_strSQL = "SELECT count(" & lp_strFieldToSearch & ") FROM " & lp_strTable & " WHERE " & lp_strFieldToSearch & " = '" & QuoteSanitise(lp_varCriteria) & "'"

Dim l_rstExists As New ADODB.Recordset
Set l_rstExists = ExecuteSQL(l_strSQL, g_strExecuteError)

If l_rstExists(0) = 0 Then
    RecordExists = False
Else
    RecordExists = True
End If

l_rstExists.Close
Set l_rstExists = Nothing

End Function

Function SetData(lp_strTableName As String, lp_strFieldToEdit As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, lp_varValue As Variant) As Variant

Dim l_strSQL As String, l_UpdateValue As String

If IsNull(lp_varValue) Or lp_varValue = "" Or UCase(lp_varValue) = "NULL" Then
    l_UpdateValue = "NULL"
Else
    l_UpdateValue = "'" & QuoteSanitise(lp_varValue) & "'"
End If

l_strSQL = "UPDATE " & lp_strTableName & " SET " & lp_strFieldToEdit & " = " & l_UpdateValue & " WHERE " & lp_strFieldToSearch & " = '" & QuoteSanitise(lp_varCriterea) & "'"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Function

