VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Begin VB.Form frmGetATSProfile 
   Caption         =   "ATS Profile Selection"
   ClientHeight    =   1200
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5385
   Icon            =   "frmGetATSProfile.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1200
   ScaleWidth      =   5385
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkWorkflowVariant 
      Caption         =   "Workflow Variant"
      Height          =   255
      Left            =   180
      TabIndex        =   5
      Top             =   900
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.CheckBox chkBatonProfile 
      Caption         =   "Baton Profile"
      Height          =   255
      Left            =   180
      TabIndex        =   4
      Top             =   600
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.CommandButton cmbOK 
      Caption         =   "OK"
      Height          =   315
      Left            =   2040
      TabIndex        =   2
      ToolTipText     =   "Accept the offered Barcode"
      Top             =   720
      Width           =   1275
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbProfile 
      Height          =   315
      Left            =   2040
      TabIndex        =   0
      Top             =   180
      Width           =   3015
      DataFieldList   =   "ATS_Profile_Name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "DigitalDestinationID"
      Columns(0).Name =   "ATS_Profile_ID"
      Columns(0).DataField=   "ATS_Profile_ID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   9578
      Columns(1).Caption=   "Profile Name"
      Columns(1).Name =   "ATS_Profile_Name"
      Columns(1).DataField=   "ATS_Profile_Name"
      Columns(1).FieldLen=   256
      _ExtentX        =   5318
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "ATS_Profile_Name"
   End
   Begin VB.Label lblCompanyID 
      Height          =   195
      Left            =   3480
      TabIndex        =   6
      Top             =   900
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label lblDestinationType 
      Height          =   195
      Left            =   3480
      TabIndex        =   3
      Top             =   600
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Choose Profile"
      Height          =   255
      Left            =   300
      TabIndex        =   1
      Top             =   240
      Width           =   1635
   End
End
Attribute VB_Name = "frmGetATSProfile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmbOK_Click()
Me.Hide
End Sub

Private Sub Form_Activate()

Dim l_strSQL As String

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch1 As ADODB.Recordset

If chkBatonProfile.Value <> 0 Then

    Me.Caption = "Baton Profile Selection"
    
    l_strSQL = "SELECT 0 AS ATS_Profile_ID, description as ATS_Profile_Name FROM xref WHERE category = 'BatonProfileName' ORDER BY forder, description;"
    
    Set l_conSearch = New ADODB.Connection
    Set l_rstSearch1 = New ADODB.Recordset
    
    l_conSearch.ConnectionString = g_strConnection
    l_conSearch.Open
    
    With l_rstSearch1
         .CursorLocation = adUseClient
         .LockType = adLockBatchOptimistic
         .CursorType = adOpenDynamic
         .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch1.ActiveConnection = Nothing
    
    Set cmbProfile.DataSourceList = l_rstSearch1

ElseIf chkWorkflowVariant.Value <> 0 Then

    Me.Caption = "Workflow Variant Selection"
    Label1.Caption = "Choose Variant"
    
    l_strSQL = "SELECT VariantID AS ATS_Profile_ID, VariantName as ATS_Profile_Name FROM Workflow_Variant WHERE companyID = " & Val(lblCompanyID.Caption) & " ORDER BY VariantName;"
    
    Set l_conSearch = New ADODB.Connection
    Set l_rstSearch1 = New ADODB.Recordset
    
    l_conSearch.ConnectionString = g_strConnection
    l_conSearch.Open
    
    With l_rstSearch1
         .CursorLocation = adUseClient
         .LockType = adLockBatchOptimistic
         .CursorType = adOpenDynamic
         .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch1.ActiveConnection = Nothing
    
    Set cmbProfile.DataSourceList = l_rstSearch1

Else
    
    Me.Caption = "ATS Profile Selection"
    
    l_strSQL = "SELECT ATS_Profile_ID, ATS_Profile_Name FROM ATS_Profile;"
    
    Set l_conSearch = New ADODB.Connection
    Set l_rstSearch1 = New ADODB.Recordset
    
    l_conSearch.ConnectionString = g_strConnection
    l_conSearch.Open
    
    With l_rstSearch1
         .CursorLocation = adUseClient
         .LockType = adLockBatchOptimistic
         .CursorType = adOpenDynamic
         .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch1.ActiveConnection = Nothing
    
    Set cmbProfile.DataSourceList = l_rstSearch1

End If
cmbProfile.Text = ""

End Sub

Private Sub Form_Load()

DoEvents

End Sub
