VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmOffAirPortal 
   Caption         =   "Off Air Schedule"
   ClientHeight    =   14760
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   28440
   Icon            =   "frmOffAirPortal.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   14760
   ScaleWidth      =   28440
   WindowState     =   2  'Maximized
   Begin VB.CheckBox chkUnlockOLCColumn 
      Caption         =   "Unlock the OLC column"
      Height          =   255
      Left            =   9240
      TabIndex        =   10
      Top             =   480
      Width           =   2055
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDVDRequests 
      Bindings        =   "frmOffAirPortal.frx":08CA
      Height          =   4815
      Left            =   180
      TabIndex        =   5
      Top             =   9360
      Width           =   20010
      _Version        =   196617
      AllowDelete     =   -1  'True
      RowHeight       =   423
      Columns.Count   =   10
      Columns(0).Width=   1852
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "epg_dvd_requestID"
      Columns(0).DataField=   "epg_dvd_requestID"
      Columns(0).DataType=   2
      Columns(0).FieldLen=   256
      Columns(1).Width=   3731
      Columns(1).Caption=   "Date Requested"
      Columns(1).Name =   "cdate"
      Columns(1).DataField=   "cdate"
      Columns(1).DataType=   7
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "epg_userID"
      Columns(2).Name =   "epg_userID"
      Columns(2).DataField=   "epg_userID"
      Columns(2).FieldLen=   256
      Columns(3).Width=   4551
      Columns(3).Caption=   "Requested By"
      Columns(3).Name =   "requestedby"
      Columns(3).DataField=   "Column 6"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   8149
      Columns(4).Caption=   "Title"
      Columns(4).Name =   "maintitle"
      Columns(4).DataField=   "maintitle"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   6509
      Columns(5).Caption=   "Episode Title"
      Columns(5).Name =   "episodetitle"
      Columns(5).DataField=   "episodetitle"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3704
      Columns(6).Caption=   "Prog Reference"
      Columns(6).Name =   "programmereference"
      Columns(6).DataField=   "programmereference"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "status"
      Columns(7).Name =   "status"
      Columns(7).DataField=   "status"
      Columns(7).FieldLen=   256
      Columns(8).Width=   688
      Columns(8).Caption=   "All"
      Columns(8).Name =   "alluser"
      Columns(8).DataField=   "alluser"
      Columns(8).FieldLen=   256
      Columns(8).Style=   2
      Columns(9).Width=   4604
      Columns(9).Caption=   "Recording Status"
      Columns(9).Name =   "Recording Status"
      Columns(9).DataField=   "Column 8"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      _ExtentX        =   35295
      _ExtentY        =   8493
      _StockProps     =   79
      Caption         =   "DVD Rip Requests"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdDuplicateRequest 
      Caption         =   "Duplicate EPG Request"
      Height          =   315
      Left            =   2280
      TabIndex        =   9
      ToolTipText     =   "Close this form"
      Top             =   780
      Width           =   2235
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnDVDStatus 
      Bindings        =   "frmOffAirPortal.frx":08E7
      Height          =   1095
      Left            =   2340
      TabIndex        =   8
      Top             =   10740
      Width           =   6555
      DataFieldList   =   "statusmessage"
      _Version        =   196617
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3201
      Columns(0).Caption=   "epg_recording_statusID"
      Columns(0).Name =   "epg_recording_statusID"
      Columns(0).DataField=   "epg_recording_statusID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   4577
      Columns(1).Caption=   "statusmessage"
      Columns(1).Name =   "statusmessage"
      Columns(1).DataField=   "statusmessage"
      Columns(1).FieldLen=   256
      _ExtentX        =   11562
      _ExtentY        =   1931
      _StockProps     =   77
      DataFieldToDisplay=   "statusmessage"
   End
   Begin VB.CheckBox chkShowAll 
      Caption         =   "Show All Requests"
      Height          =   255
      Left            =   9240
      TabIndex        =   6
      Top             =   180
      Width           =   1935
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print"
      Height          =   315
      Left            =   11340
      TabIndex        =   4
      ToolTipText     =   "Close this form"
      Top             =   120
      Width           =   1335
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "Search"
      Height          =   315
      Left            =   12780
      TabIndex        =   3
      ToolTipText     =   "Close this form"
      Top             =   120
      Width           =   1335
   End
   Begin MSAdodcLib.Adodc adoStatus 
      Height          =   330
      Left            =   1440
      Top             =   2460
      Visible         =   0   'False
      Width           =   2100
      _ExtentX        =   3704
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoStatus"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnRecordingStatus 
      Bindings        =   "frmOffAirPortal.frx":08FF
      Height          =   1095
      Left            =   1440
      TabIndex        =   2
      Top             =   2760
      Width           =   6555
      DataFieldList   =   "statusmessage"
      _Version        =   196617
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3201
      Columns(0).Caption=   "epg_recording_statusID"
      Columns(0).Name =   "epg_recording_statusID"
      Columns(0).DataField=   "epg_recording_statusID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   4577
      Columns(1).Caption=   "statusmessage"
      Columns(1).Name =   "statusmessage"
      Columns(1).DataField=   "statusmessage"
      Columns(1).FieldLen=   256
      _ExtentX        =   11562
      _ExtentY        =   1931
      _StockProps     =   77
      DataFieldToDisplay=   "statusmessage"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdOffAirs 
      Bindings        =   "frmOffAirPortal.frx":0917
      Height          =   7935
      Left            =   180
      TabIndex        =   1
      Top             =   1080
      Width           =   28095
      _Version        =   196617
      AllowDelete     =   -1  'True
      AllowColumnMoving=   2
      AllowColumnSwapping=   0
      RowHeight       =   423
      Columns.Count   =   24
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "epg_recording_requestID"
      Columns(0).DataField=   "epg_recording_requestID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3149
      Columns(1).Caption=   "Start Time"
      Columns(1).Name =   "publishedstarttime"
      Columns(1).DataField=   "publishedstarttime"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3149
      Columns(2).Caption=   "End Time"
      Columns(2).Name =   "publishedendtime"
      Columns(2).DataField=   "publishedendtime"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3149
      Columns(3).Caption=   "Actual Start Time"
      Columns(3).Name =   "actualstarttime"
      Columns(3).DataField=   "actualstarttime"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3149
      Columns(4).Caption=   "Actual End Time"
      Columns(4).Name =   "actualendtime"
      Columns(4).DataField=   "actualendtime"
      Columns(4).FieldLen=   256
      Columns(5).Width=   1931
      Columns(5).Caption=   "Channel"
      Columns(5).Name =   "channelname"
      Columns(5).DataField=   "channelname"
      Columns(5).FieldLen=   256
      Columns(6).Width=   4577
      Columns(6).Caption=   "Programme Title"
      Columns(6).Name =   "maintitle"
      Columns(6).DataField=   "maintitle"
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "recording_status_id"
      Columns(7).Name =   "recording_status_id"
      Columns(7).DataField=   "recording_statusID"
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Caption=   "Programme ID"
      Columns(8).Name =   "progrefID"
      Columns(8).DataField=   "progrefID"
      Columns(8).FieldLen=   256
      Columns(9).Width=   1931
      Columns(9).Caption=   "Requested"
      Columns(9).Name =   "daterequested"
      Columns(9).DataField=   "daterequested"
      Columns(9).FieldLen=   256
      Columns(10).Width=   1931
      Columns(10).Caption=   "Confirmed"
      Columns(10).Name=   "dateconfirmed"
      Columns(10).DataField=   "dateconfirmed"
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "portaluserID"
      Columns(11).Name=   "userID"
      Columns(11).DataField=   "userID"
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Caption=   "Requested By"
      Columns(12).Name=   "requestedby"
      Columns(12).FieldLen=   256
      Columns(12).Locked=   -1  'True
      Columns(13).Width=   3200
      Columns(13).Caption=   "Company"
      Columns(13).Name=   "Company"
      Columns(13).FieldLen=   256
      Columns(14).Width=   2831
      Columns(14).Caption=   "Star Order Number"
      Columns(14).Name=   "starordernumber"
      Columns(14).DataField=   "starordernumber"
      Columns(14).FieldLen=   256
      Columns(15).Width=   609
      Columns(15).Caption=   "All"
      Columns(15).Name=   "alluser"
      Columns(15).DataField=   "alluser"
      Columns(15).FieldLen=   256
      Columns(15).Style=   2
      Columns(16).Width=   1244
      Columns(16).Caption=   "B. Area"
      Columns(16).Name=   "businessarea"
      Columns(16).DataField=   "businessarea"
      Columns(16).FieldLen=   256
      Columns(17).Width=   1588
      Columns(17).Caption=   "PAL DVD"
      Columns(17).Name=   "DVDPALrequested"
      Columns(17).DataField=   "DVDPALrequested"
      Columns(17).FieldLen=   256
      Columns(18).Width=   1773
      Columns(18).Caption=   "NTSC DVD"
      Columns(18).Name=   "DVDNTSCrequested"
      Columns(18).DataField=   "DVDNTSCrequested"
      Columns(18).FieldLen=   256
      Columns(19).Width=   794
      Columns(19).Caption=   "OLC"
      Columns(19).Name=   "OLC"
      Columns(19).DataField=   "OLCrequested"
      Columns(19).FieldLen=   256
      Columns(19).Locked=   -1  'True
      Columns(19).Style=   2
      Columns(20).Width=   3519
      Columns(20).Caption=   "Recording Reference"
      Columns(20).Name=   "recordingreference"
      Columns(20).DataField=   "recordingreference"
      Columns(20).FieldLen=   256
      Columns(21).Width=   3519
      Columns(21).Caption=   "Recording Status"
      Columns(21).Name=   "statusmessage"
      Columns(21).FieldLen=   256
      Columns(22).Width=   3200
      Columns(22).Visible=   0   'False
      Columns(22).Caption=   "MWClientNumber"
      Columns(22).Name=   "companyID"
      Columns(22).DataField=   "companyID"
      Columns(22).FieldLen=   256
      Columns(23).Width=   3200
      Columns(23).Visible=   0   'False
      Columns(23).Caption=   "contactID"
      Columns(23).Name=   "contactID"
      Columns(23).DataField=   "contactID"
      Columns(23).FieldLen=   256
      _ExtentX        =   49556
      _ExtentY        =   13996
      _StockProps     =   79
      Caption         =   "Off-Air Recording Requests"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   14220
      TabIndex        =   0
      ToolTipText     =   "Close this form"
      Top             =   120
      Width           =   1335
   End
   Begin MSAdodcLib.Adodc adoOffAirs 
      Height          =   330
      Left            =   180
      Top             =   780
      Width           =   2100
      _ExtentX        =   3704
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Off Airs"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoDVDRequests 
      Height          =   330
      Left            =   180
      Top             =   9060
      Width           =   2340
      _ExtentX        =   4128
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "DVD Requests"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label Label1 
      Caption         =   "Please Note: Recording times are in GMT, regardless of local UK time. Normal list shows between 2 days ahead  and 5 days behind."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   615
      Left            =   180
      TabIndex        =   7
      Top             =   60
      Width           =   8895
   End
End
Attribute VB_Name = "frmOffAirPortal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strSearch_Fields As String
Dim m_strSearch_Status As String
Dim m_strSearch_Period As String
Dim m_strSearch_Orderby As String

Private Function Refresh_Search_Grid()

Dim l_strSQL As String

If chkShowAll.Value = 0 Then
    m_strSearch_Period = "AND ((publishedstarttime > '" & FormatSQLDate(DateAdd("d", -5, Date)) & "' "
    m_strSearch_Period = m_strSearch_Period & " AND publishedstarttime < '" & FormatSQLDate(DateAdd("d", 3, Date)) & "') "
    m_strSearch_Period = m_strSearch_Period & " OR recording_statusID = 5 OR recording_statusID = 6 OR recording_statusID = 7 OR recording_statusID = 8)"
Else
    m_strSearch_Period = " "
End If

If m_strSearch_Fields <> "" Then
    
    l_strSQL = m_strSearch_Fields & m_strSearch_Status & m_strSearch_Period & m_strSearch_Orderby

    adoOffAirs.RecordSource = l_strSQL
    adoOffAirs.ConnectionString = g_strConnection
    adoOffAirs.Refresh
    
    adoOffAirs.Caption = adoOffAirs.Recordset.RecordCount & " Items"

End If

End Function

Private Sub chkUnlockOLCColumn_Click()

If chkUnlockOLCColumn.Value = 0 Then
    grdOffAirs.Columns("OLC").Locked = True
Else
    grdOffAirs.Columns("OLC").Locked = False
End If

End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdPrintReport_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "OffAirs.rpt", "", True
End Sub

Private Sub cmdDuplicateRequest_Click()

Dim l_strSQL As String, cnn As ADODB.Connection

If Not adoOffAirs.Recordset.EOF Then
    
    Set cnn = New ADODB.Connection
    cnn.Open g_strConnection

    l_strSQL = "INSERT INTO epg_recording_request (userID, recording_statusID, progrefID, publishedstarttime, publishedendtime, channelname, daterequested, dateconfirmed, maintitle, businessarea, "
    l_strSQL = l_strSQL & "recordingreference, actualstarttime, actualendtime, DVDPALrequested, DVDNTSCrequested, starordernumber, OLCrequested, alluser) "
    l_strSQL = l_strSQL & "SELECT userID, recording_statusID, progrefID, publishedstarttime, publishedendtime, channelname, daterequested, dateconfirmed, maintitle, businessarea, "
    l_strSQL = l_strSQL & "recordingreference, actualstarttime, actualendtime, DVDPALrequested, DVDNTSCrequested, starordernumber, OLCrequested, alluser FROM epg_recording_request "
    l_strSQL = l_strSQL & "WHERE epg_recording_requestID = " & Val(Trim(" " & adoOffAirs.Recordset("epg_recording_requestID"))) & ";"
    
    Debug.Print l_strSQL
    cnn.Execute l_strSQL
    
    cnn.Close
    Set cnn = Nothing
    
    adoOffAirs.Refresh
    
End If

End Sub

Private Sub cmdPrint_Click()

PrintCrystalReport "reports\EPGRequests.rpt", "", g_blnPreviewReport

End Sub

Private Sub cmdSearch_Click()

Refresh_Search_Grid
adoDVDRequests.Refresh
adoDVDRequests.Caption = adoDVDRequests.Recordset.RecordCount & " Requests"
DoEvents

End Sub

Private Sub ddnDVDStatus_CloseUp()

grdDVDRequests.Columns("status").Text = ddnDVDStatus.Columns("epg_recording_statusID").Text

End Sub

Private Sub ddnRecordingStatus_CloseUp()

grdOffAirs.Columns("recording_status_ID").Text = ddnRecordingStatus.Columns("epg_recording_statusID").Text

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

grdOffAirs.StyleSets.Add "pending"
grdOffAirs.StyleSets("pending").BackColor = &HFFC0C0
grdOffAirs.StyleSets("pending").ForeColor = &H80000012
grdOffAirs.StyleSets.Add "complete"
grdOffAirs.StyleSets("complete").BackColor = &HFF00&
grdOffAirs.StyleSets("complete").ForeColor = &H80000012
grdOffAirs.StyleSets.Add "working"
grdOffAirs.StyleSets("working").BackColor = &H80FFFF
grdOffAirs.StyleSets("working").ForeColor = &H80000012
grdOffAirs.StyleSets.Add "Error"
grdOffAirs.StyleSets("Error").BackColor = &HA0A0FF
grdOffAirs.StyleSets("Error").ForeColor = &H80000012
grdOffAirs.StyleSets.Add "Nothing"
grdOffAirs.StyleSets("Nothing").BackColor = &HFFFFFF
grdOffAirs.StyleSets("Nothing").ForeColor = &H80000012

grdDVDRequests.StyleSets.Add "pending"
grdDVDRequests.StyleSets("pending").BackColor = &HFFC0C0
grdDVDRequests.StyleSets("pending").ForeColor = &H80000012
grdDVDRequests.StyleSets.Add "complete"
grdDVDRequests.StyleSets("complete").BackColor = &HFF00&
grdDVDRequests.StyleSets("complete").ForeColor = &H80000012
grdDVDRequests.StyleSets.Add "working"
grdDVDRequests.StyleSets("working").BackColor = &H80FFFF
grdDVDRequests.StyleSets("working").ForeColor = &H80000012
grdDVDRequests.StyleSets.Add "Error"
grdDVDRequests.StyleSets("Error").BackColor = &HA0A0FF
grdDVDRequests.StyleSets("Error").ForeColor = &H80000012
grdDVDRequests.StyleSets.Add "Nothing"
grdDVDRequests.StyleSets("Nothing").BackColor = &HFFFFFF
grdDVDRequests.StyleSets("Nothing").ForeColor = &H80000012

m_strSearch_Fields = "SELECT * FROM epg_recording_request "

m_strSearch_Status = "WHERE epg_recording_request.recording_statusID > 2 "

m_strSearch_Period = "AND publishedstarttime > '" & FormatSQLDate(DateAdd("d", -5, Date)) & "' "

m_strSearch_Orderby = "ORDER BY publishedstarttime DESC, channelname;"

l_strSQL = "SELECT * FROM epg_recording_status ORDER BY epg_recording_statusID;"
adoStatus.RecordSource = l_strSQL
adoStatus.ConnectionString = g_strConnection
adoStatus.Refresh

grdOffAirs.Columns("statusmessage").DropDownHwnd = ddnRecordingStatus.hWnd
grdDVDRequests.Columns("recording status").DropDownHwnd = ddnDVDStatus.hWnd

l_strSQL = "SELECT * FROM epg_dvd_request ORDER BY cdate DESC;"
adoDVDRequests.RecordSource = l_strSQL
adoDVDRequests.ConnectionString = g_strConnection

cmdSearch.Value = True

End Sub

Private Sub Form_Resize()

On Error Resume Next

grdOffAirs.Height = (Me.ScaleHeight - grdOffAirs.Top) * 2 / 3 - 240
grdDVDRequests.Top = grdOffAirs.Top + grdOffAirs.Height + 120
grdDVDRequests.Height = Me.ScaleHeight - grdDVDRequests.Top - 120
If Me.ScaleWidth <= 28095 + 120 Then
    grdOffAirs.Width = Me.ScaleWidth - 240
Else
    grdOffAirs.Width = 28095
End If

If Me.ScaleWidth <= 20010 + 120 Then
    grdDVDRequests.Width = Me.ScaleWidth - 240
Else
    grdDVDRequests.Width = 20010
End If

adoDVDRequests.Top = grdDVDRequests.Top

End Sub

Private Sub grdDVDRequests_DblClick()

ShowClipSearch "", grdDVDRequests.Columns("programmereference").Text

End Sub

Private Sub grdDVDRequests_RowLoaded(ByVal Bookmark As Variant)

grdDVDRequests.Columns("Recording Status").Text = GetData("epg_recording_status", "statusmessage", "epg_recording_statusID", Val(grdDVDRequests.Columns("status").Text))
grdDVDRequests.Columns("requestedby").Text = GetData("epg_user", "fullname", "epg_userID", Val(grdDVDRequests.Columns("epg_userID").Text))

Select Case Val(grdDVDRequests.Columns("status").Text)

    Case 1, 2, 4
        grdDVDRequests.Columns("Recording Status").CellStyleSet "Nothing"
    Case 3
        grdDVDRequests.Columns("Recording Status").CellStyleSet "pending"
    Case 5, 6, 8, 14
        grdDVDRequests.Columns("Recording Status").CellStyleSet "working"
    Case 9, 10, 13, 12
        grdDVDRequests.Columns("Recording Status").CellStyleSet "complete"
    Case 7, 11
        grdDVDRequests.Columns("Recording Status").CellStyleSet "Error"

End Select

End Sub

Private Sub grdOffAirs_DblClick()

ShowClipSearch grdOffAirs.Columns("progrefID").Text

End Sub

Private Sub grdOffAirs_RowLoaded(ByVal Bookmark As Variant)

grdOffAirs.Columns("statusmessage").Text = GetData("epg_recording_status", "statusmessage", "epg_recording_statusID", Val(grdOffAirs.Columns("recording_status_id").Text))
If Val(grdOffAirs.Columns("contactID").Text) <> 0 Then
    grdOffAirs.Columns("requestedby").Text = GetData("contact", "name", "contactID", Val(grdOffAirs.Columns("contactID").Text))
    grdOffAirs.Columns("Company").Text = GetData("company", "name", "companyID", Val(grdOffAirs.Columns("companyID").Text))
Else
    grdOffAirs.Columns("requestedby").Text = GetData("epg_user", "fullname", "epg_userID", Val(grdOffAirs.Columns("userID").Text))
End If

Select Case Val(grdOffAirs.Columns("recording_status_id").Text)

    Case 1, 2, 4
        grdOffAirs.Columns("statusmessage").CellStyleSet "Nothing"
    Case 3
        grdOffAirs.Columns("statusmessage").CellStyleSet "pending"
    Case 5, 6, 8, 14
        grdOffAirs.Columns("statusmessage").CellStyleSet "working"
    Case 9, 10, 13, 12
        grdOffAirs.Columns("statusmessage").CellStyleSet "complete"
    Case 7, 11
        grdOffAirs.Columns("statusmessage").CellStyleSet "Error"

End Select

End Sub
