VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmAccountsTransaction 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Accounts Transaction"
   ClientHeight    =   5040
   ClientLeft      =   1755
   ClientTop       =   3705
   ClientWidth     =   14805
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5040
   ScaleWidth      =   14805
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.Frame Frame1 
      Caption         =   "Costing Summary By Job Status"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2475
      Left            =   11580
      TabIndex        =   34
      Top             =   60
      Width           =   3135
      Begin VB.Label lblCaption 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         Height          =   255
         Index           =   25
         Left            =   1560
         TabIndex        =   46
         Top             =   2100
         Width           =   1455
      End
      Begin VB.Label lblCaption 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         Height          =   255
         Index           =   24
         Left            =   1560
         TabIndex        =   45
         Top             =   1740
         Width           =   1455
      End
      Begin VB.Label lblCaption 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         Height          =   255
         Index           =   23
         Left            =   1560
         TabIndex        =   44
         Top             =   1380
         Width           =   1455
      End
      Begin VB.Label lblCaption 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         Height          =   255
         Index           =   22
         Left            =   1560
         TabIndex        =   43
         Top             =   1020
         Width           =   1455
      End
      Begin VB.Label lblCaption 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         Height          =   255
         Index           =   21
         Left            =   1560
         TabIndex        =   42
         Top             =   660
         Width           =   1455
      End
      Begin VB.Label lblCaption 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         Height          =   255
         Index           =   20
         Left            =   1560
         TabIndex        =   41
         Top             =   300
         Width           =   1455
      End
      Begin VB.Label lblCaption 
         Caption         =   "Sent To Accounts"
         Height          =   255
         Index           =   19
         Left            =   120
         TabIndex        =   40
         Top             =   2100
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Costed"
         Height          =   255
         Index           =   18
         Left            =   120
         TabIndex        =   39
         Top             =   1740
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Hold Cost"
         Height          =   255
         Index           =   14
         Left            =   120
         TabIndex        =   38
         Top             =   1380
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Completed"
         Height          =   255
         Index           =   13
         Left            =   120
         TabIndex        =   37
         Top             =   1020
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Confirmed"
         Height          =   255
         Index           =   12
         Left            =   120
         TabIndex        =   36
         Top             =   660
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Pencil / 2nd Pencil"
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   35
         Top             =   300
         Width           =   1335
      End
   End
   Begin VB.TextBox txtBalance 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   13140
      Locked          =   -1  'True
      TabIndex        =   32
      ToolTipText     =   "The total cost for the job"
      Top             =   3840
      Width           =   1455
   End
   Begin VB.TextBox txtDifferencePercent 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   13140
      Locked          =   -1  'True
      TabIndex        =   28
      ToolTipText     =   "The total cost for the job"
      Top             =   3360
      Width           =   1095
   End
   Begin VB.TextBox txtRateCardTotal 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   13140
      Locked          =   -1  'True
      TabIndex        =   27
      ToolTipText     =   "The total cost for the job"
      Top             =   3000
      Width           =   1455
   End
   Begin VB.TextBox txtTotal 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   13140
      Locked          =   -1  'True
      TabIndex        =   26
      ToolTipText     =   "The total cost for the job"
      Top             =   2640
      Width           =   1455
   End
   Begin VB.CommandButton cmdEditTotal 
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   14340
      TabIndex        =   25
      ToolTipText     =   "Set a discount globally for this costing sheet"
      Top             =   3360
      Width           =   255
   End
   Begin MSAdodcLib.Adodc adoCostingSheets 
      Height          =   330
      Left            =   60
      Top             =   4620
      Visible         =   0   'False
      Width           =   2580
      _ExtentX        =   4551
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCostingSheets"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo ddnCostingSheets 
      Bindings        =   "frmAccTransaction.frx":0000
      Height          =   315
      Left            =   4080
      TabIndex        =   7
      ToolTipText     =   "Select the MCS (costing sheet) number that this should be included on"
      Top             =   4140
      Width           =   2895
      DataFieldList   =   "MCS ID"
      _Version        =   196617
      BackColorOdd    =   16777215
      Columns(0).Width=   3200
      _ExtentX        =   5106
      _ExtentY        =   556
      _StockProps     =   93
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      DataFieldToDisplay=   "MCS ID"
   End
   Begin VB.TextBox txtInput 
      Height          =   1275
      Index           =   6
      Left            =   1440
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      ToolTipText     =   "Detailed description of what this transaction covered. Also serves as the description on the printout"
      Top             =   2340
      Width           =   5535
   End
   Begin VB.TextBox txtInput 
      Height          =   315
      Index           =   5
      Left            =   1440
      TabIndex        =   3
      ToolTipText     =   "Your clients purchase order reference"
      Top             =   1500
      Width           =   2895
   End
   Begin VB.ComboBox cmbTransactionType 
      Height          =   315
      Left            =   1440
      Style           =   2  'Dropdown List
      TabIndex        =   0
      ToolTipText     =   "The type of transaction (Credit / Invoice etc)"
      Top             =   660
      Width           =   2895
   End
   Begin VB.TextBox txtInput 
      Height          =   315
      Index           =   4
      Left            =   4080
      TabIndex        =   6
      ToolTipText     =   "The total amount this transaction was for (Excluding any tax)"
      Top             =   3720
      Width           =   2895
   End
   Begin VB.TextBox txtInput 
      Height          =   315
      Index           =   3
      Left            =   1440
      TabIndex        =   1
      ToolTipText     =   "Your reference number. Ie. Invoice number / Credit note number etc"
      Top             =   1080
      Width           =   2895
   End
   Begin VB.TextBox txtInput 
      Height          =   315
      Index           =   2
      Left            =   1440
      TabIndex        =   4
      ToolTipText     =   "Short description of this transaction. ie. ""First 50% invoice"""
      Top             =   1920
      Width           =   5535
   End
   Begin VB.TextBox txtInput 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   315
      Index           =   1
      Left            =   3600
      Locked          =   -1  'True
      TabIndex        =   16
      Top             =   120
      Width           =   735
   End
   Begin VB.TextBox txtInput 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   315
      Index           =   0
      Left            =   1440
      Locked          =   -1  'True
      TabIndex        =   15
      Top             =   120
      Width           =   1815
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   315
      Left            =   5760
      TabIndex        =   9
      ToolTipText     =   "Cancel this transaction"
      Top             =   4620
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   315
      Left            =   4440
      TabIndex        =   8
      ToolTipText     =   "Save this transaction"
      Top             =   4620
      Width           =   1215
   End
   Begin MSComCtl2.DTPicker datTransactionDate 
      Height          =   315
      Left            =   5520
      TabIndex        =   2
      ToolTipText     =   "The accounts date of this transaction"
      Top             =   1080
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   77660161
      CurrentDate     =   37870
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdNominalTotals 
      Height          =   4815
      Left            =   7080
      TabIndex        =   24
      Top             =   120
      Width           =   4410
      _Version        =   196617
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   12632319
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   7779
      _ExtentY        =   8493
      _StockProps     =   79
      Caption         =   "Nominal Codes"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker datStartDate 
      Height          =   315
      Left            =   5760
      TabIndex        =   47
      ToolTipText     =   "The accounts date of this transaction"
      Top             =   1500
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      _Version        =   393216
      Format          =   77660161
      CurrentDate     =   37870
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job Start Date"
      Height          =   255
      Index           =   26
      Left            =   4440
      TabIndex        =   48
      Top             =   1500
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Balance"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   10
      Left            =   11700
      TabIndex        =   33
      Top             =   3840
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Discount %"
      Height          =   255
      Index           =   17
      Left            =   11700
      TabIndex        =   31
      Top             =   3360
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Rate Card"
      Height          =   255
      Index           =   16
      Left            =   11700
      TabIndex        =   30
      Top             =   3000
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   15
      Left            =   11700
      TabIndex        =   29
      Top             =   2640
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Costing Sheet ID"
      Height          =   255
      Index           =   9
      Left            =   2760
      TabIndex        =   23
      Top             =   4140
      Width           =   1215
   End
   Begin VB.Label lblAccountsTransactionID 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   4620
      TabIndex        =   22
      Top             =   180
      Width           =   2355
   End
   Begin VB.Label lblCaption 
      Caption         =   "Invoice Details"
      Height          =   255
      Index           =   8
      Left            =   120
      TabIndex        =   21
      Top             =   2340
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Client Order #"
      Height          =   255
      Index           =   7
      Left            =   120
      TabIndex        =   20
      Top             =   1500
      Width           =   1215
   End
   Begin VB.Label lblProjectNumber 
      BackColor       =   &H0000FFFF&
      Height          =   195
      Left            =   180
      TabIndex        =   19
      Top             =   3600
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Type"
      Height          =   255
      Index           =   6
      Left            =   120
      TabIndex        =   18
      Top             =   660
      Width           =   1215
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   4620
      Y1              =   540
      Y2              =   540
   End
   Begin VB.Label lblCaption 
      Caption         =   "Trans Date"
      Height          =   255
      Index           =   5
      Left            =   4440
      TabIndex        =   17
      Top             =   1080
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Amount (Net)"
      Height          =   255
      Index           =   4
      Left            =   2760
      TabIndex        =   14
      Top             =   3720
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Our Ref #"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   13
      Top             =   1080
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Description"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   12
      Top             =   1920
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "By"
      Height          =   255
      Index           =   1
      Left            =   3300
      TabIndex        =   11
      Top             =   120
      Width           =   375
   End
   Begin VB.Label lblCaption 
      Caption         =   "Created"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   10
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmAccountsTransaction"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmbTransactionType_Click()
Select Case LCase(cmbTransactionType.Text)
Case "invoice"
    lblCaption(3).Caption = "Invoice No."
    lblCaption(5).Caption = "Invoice Date"
Case "credit note"
    lblCaption(3).Caption = "Credit No."
    lblCaption(5).Caption = "Credit Date"
Case "payment"
    lblCaption(3).Caption = "Invoice No."
    lblCaption(5).Caption = "Payment Date"
Case Else
    lblCaption(3).Caption = "Our Ref #"
    lblCaption(5).Caption = "Trans Date"
End Select
End Sub

Private Sub cmdCancel_Click()
Me.Tag = "CANCELLED"
Me.Hide

End Sub

Private Sub cmdEditTotal_Click()

If Val(lblAccountsTransactionID.Caption) = 0 Then
    MsgBox "You can not apply a deal without selecting a valid accounts transaction ID.", vbExclamation
    Exit Sub
End If

Dim I As Integer
I = MsgBox("Are you REALLY SURE you want to do this? You could mess up your costing data unless you know what you are doing?", vbExclamation + vbYesNo)
If I = vbNo Then Exit Sub
    

Dim l_sngDiscount As Single, l_strCategory As String
l_sngDiscount = UpdateTotal(txtRateCardTotal.Text)

If l_sngDiscount = -999 Then Exit Sub

Dim l_strSQL As String

If g_dbtype = "mysql" Then
    l_strSQL = "UPDATE costing SET costing.discount = ROUND(" & l_sngDiscount & "," & g_optRoundCurrencyDecimals & "), unitcharge = ROUND(((costing.ratecardprice / 100) * " & 100 - l_sngDiscount & ")," & g_optRoundCurrencyDecimals & ") , total = ROUND(quantity * fd_time * ((ratecardprice / 100) * " & 100 - l_sngDiscount & ")," & g_optRoundCurrencyDecimals & ") WHERE costing.accountstransactionID = '" & lblAccountsTransactionID.Caption & "';"
Else
    l_strSQL = "UPDATE costing SET costing.discount = ROUND(" & l_sngDiscount & "," & g_optRoundCurrencyDecimals & "), unitcharge = ROUND(((costing.ratecardprice / 100) * " & 100 - l_sngDiscount & ")," & g_optRoundCurrencyDecimals & ") , total = ROUND(quantity * fd_time * ((ratecardprice / 100) * " & 100 - l_sngDiscount & ")," & g_optRoundCurrencyDecimals & ") FROM costing WHERE costing.accountstransactionID = '" & lblAccountsTransactionID.Caption & "';"
End If
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

If g_dbtype = "mysql" Then
    l_strSQL = "UPDATE costing SET costing.vat = ROUND(total * 0.175," & g_optRoundCurrencyDecimals & "), totalincludingvat = ROUND(total * 1.175," & g_optRoundCurrencyDecimals & ") WHERE costing.accountstransactionID = '" & lblAccountsTransactionID.Caption & "';"
Else
    l_strSQL = "UPDATE costing SET costing.vat = ROUND(total * 0.175," & g_optRoundCurrencyDecimals & "), totalincludingvat = ROUND(total * 1.175," & g_optRoundCurrencyDecimals & ") FROM costing WHERE costing.accountstransactionID = '" & lblAccountsTransactionID.Caption & "';"
End If

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

ShowAccountsTransaction Val(lblAccountsTransactionID.Caption)


End Sub

Private Sub cmdOK_Click()

'type
If cmbTransactionType.Text = "" Then
    MsgBox "Please enter a transaction type before saving", vbExclamation
    cmbTransactionType.SetFocus
    Exit Sub
End If

'description
If txtInput(2).Text = "" Then
    MsgBox "Please enter a description before saving", vbExclamation
    txtInput(2).SetFocus
    Exit Sub
End If

'reference
If txtInput(3).Text = "" Then
    MsgBox "Please enter a reference before saving", vbExclamation
    txtInput(3).SetFocus
    Exit Sub
End If

'total
If txtInput(4).Text = "" Then
    MsgBox "Please enter an amount (net) before saving", vbExclamation
    txtInput(4).SetFocus
    Exit Sub
End If

'total
If Not IsNumeric(txtInput(4).Text) Then
    MsgBox "Please enter a valid amount (net) before saving", vbExclamation
    txtInput(4).SetFocus
    Exit Sub
End If

'client order number
If txtInput(5).Text = "" Then
    MsgBox "Please enter a clients order number before saving", vbExclamation
    txtInput(5).SetFocus
    Exit Sub
End If

'detail
If txtInput(6).Text = "" Then
    MsgBox "Please enter the details of this transaction before saving", vbExclamation
    txtInput(6).SetFocus
    Exit Sub
End If

Dim l_int As Integer

'costing sheet ID
If ddnCostingSheets.Text = "" Then
    l_int = MsgBox("You have not selected a costing sheet ID. Are you sure you want to save without one?", vbYesNo + vbExclamation)
    If l_int = vbNo Then Exit Sub
End If


l_int = MsgBox("Are you sure you want to " & IIf(Val(lblAccountsTransactionID.Caption) = 0, "add a new", "edit this") & " transaction for this project?", vbQuestion + vbYesNo)
If l_int = vbNo Then Exit Sub

AddAccountsTransaction Val(lblProjectNumber.Caption), CStr(cmbTransactionType.Text), datTransactionDate.Value, CStr(txtInput(2).Text), CStr(txtInput(3).Text), Val(txtInput(4).Text), GetData("project", "companyID", "projectnumber", lblProjectNumber.Caption), txtInput(5).Text, txtInput(6).Text, Val(lblAccountsTransactionID.Caption), Val(ddnCostingSheets.Text), datStartDate.Value

Me.Hide

End Sub

Private Sub ddnCostingSheets_DropDown()

Dim l_strSQL As String
l_strSQL = "SELECT costingsheetID as 'MCS ID', title AS 'Title', clientordernumber AS 'Order Ref', createduser AS 'User', createddate AS 'Created', printeddate AS 'Printed', invoiceddate AS 'Invoiced', fd_status AS 'Status', invoicenumber AS 'Invoice #' FROM costingsheet WHERE (costingsheet.projectnumber = '" & Val(lblProjectNumber.Caption) & "') ORDER BY costingsheetID; "

adoCostingSheets.RecordSource = l_strSQL
adoCostingSheets.ConnectionString = g_strConnection
adoCostingSheets.Refresh

Me.MousePointer = vbDefault
End Sub

Private Sub Form_Load()

PopulateCombo "transactiontype", cmbTransactionType

CenterForm Me

lblCaption(4).Caption = "Amount (Net) " & g_strCurrency

'txtInput(0).Text = Now
'txtInput(1).Text = g_strUserInitials

End Sub


