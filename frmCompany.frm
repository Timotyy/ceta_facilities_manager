VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCompany 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Company Information"
   ClientHeight    =   10875
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   28860
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCompany.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   10875
   ScaleWidth      =   28860
   WindowState     =   2  'Maximized
   Begin VB.Frame fraDetails 
      Height          =   10755
      Left            =   23640
      TabIndex        =   175
      Top             =   -60
      Width           =   4935
      Begin VB.TextBox txtCompanyID 
         BackColor       =   &H00FFC0FF&
         Height          =   285
         Left            =   1260
         TabIndex        =   200
         ToolTipText     =   "The unique identifier for this company"
         Top             =   240
         Width           =   1395
      End
      Begin VB.TextBox txtAddress 
         BackColor       =   &H00FFC0C0&
         DataField       =   "address"
         Height          =   855
         Left            =   1260
         MultiLine       =   -1  'True
         TabIndex        =   199
         ToolTipText     =   "Company Address"
         Top             =   1320
         Width           =   3315
      End
      Begin VB.TextBox txtPostCode 
         BackColor       =   &H00FFC0C0&
         DataField       =   "postcode"
         Height          =   285
         Left            =   1260
         TabIndex        =   198
         ToolTipText     =   "Company Postcode"
         Top             =   2280
         Width           =   3315
      End
      Begin VB.TextBox txtTelephone 
         BackColor       =   &H00FFC0C0&
         DataField       =   "telephone"
         Height          =   285
         Left            =   1260
         TabIndex        =   197
         ToolTipText     =   "Company Telephone"
         Top             =   3000
         Width           =   3315
      End
      Begin VB.TextBox txtFax 
         DataField       =   "fax"
         Height          =   285
         Left            =   1260
         TabIndex        =   196
         ToolTipText     =   "Company Fax"
         Top             =   3360
         Width           =   3315
      End
      Begin VB.TextBox txtEmail 
         DataField       =   "email"
         Height          =   285
         Left            =   1260
         TabIndex        =   195
         ToolTipText     =   "Company Email Address"
         Top             =   3720
         Width           =   3315
      End
      Begin VB.TextBox txtWebsite 
         DataField       =   "website"
         Height          =   285
         Left            =   1260
         TabIndex        =   194
         ToolTipText     =   "Company Website"
         Top             =   4080
         Width           =   3315
      End
      Begin VB.TextBox txtMarketCode 
         BackColor       =   &H00FFC0C0&
         DataField       =   "marketcode"
         Height          =   285
         Left            =   1260
         TabIndex        =   193
         Top             =   960
         Width           =   3315
      End
      Begin VB.TextBox txtSource 
         DataField       =   "source"
         Height          =   285
         Left            =   1260
         TabIndex        =   192
         Top             =   4440
         Width           =   3315
      End
      Begin VB.TextBox txtTradeCode 
         DataField       =   "tradecode"
         Height          =   285
         Left            =   1260
         TabIndex        =   191
         ToolTipText     =   "Company Trade Code"
         Top             =   4800
         Width           =   3315
      End
      Begin VB.TextBox txtComments 
         DataField       =   "notes1"
         Height          =   285
         Left            =   1260
         MultiLine       =   -1  'True
         TabIndex        =   190
         ToolTipText     =   "Notes for this Company"
         Top             =   5160
         Width           =   3315
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   2160
         TabIndex        =   189
         ToolTipText     =   "Close this form"
         Top             =   9600
         Width           =   1815
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         Height          =   315
         Left            =   2160
         TabIndex        =   188
         ToolTipText     =   "Save changes to this job"
         Top             =   9180
         Width           =   1815
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   120
         TabIndex        =   187
         ToolTipText     =   "Clear the form"
         Top             =   9180
         Width           =   1815
      End
      Begin VB.CommandButton cmdRateCard 
         Caption         =   "Rate Card"
         Height          =   315
         Left            =   2160
         TabIndex        =   186
         ToolTipText     =   "Enter Specific Ratecard entries for this company"
         Top             =   8760
         Width           =   1815
      End
      Begin VB.TextBox txtInvoiceCompany 
         BackColor       =   &H00FFC0FF&
         DataField       =   "invoicecompanyname"
         Height          =   285
         Left            =   1260
         TabIndex        =   185
         ToolTipText     =   "Company Name for Invoices, if different"
         Top             =   5880
         Width           =   3315
      End
      Begin VB.TextBox txtInvoiceCountry 
         BackColor       =   &H00FFC0FF&
         DataField       =   "invoicecountry"
         Height          =   285
         Left            =   1260
         TabIndex        =   184
         ToolTipText     =   "Country for Invoices if different"
         Top             =   7560
         Width           =   3315
      End
      Begin VB.TextBox txtInvoicePostCode 
         BackColor       =   &H00FFC0FF&
         DataField       =   "invoicepostcode"
         Height          =   285
         Left            =   1260
         TabIndex        =   183
         ToolTipText     =   "Postcode for Invoices if different"
         Top             =   7200
         Width           =   3315
      End
      Begin VB.TextBox txtInvoiceAddress 
         BackColor       =   &H00FFC0FF&
         DataField       =   "invoiceaddress"
         Height          =   885
         Left            =   1260
         MultiLine       =   -1  'True
         TabIndex        =   182
         ToolTipText     =   "Address for Invoices if different"
         Top             =   6240
         Width           =   3315
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "Deactivate Company"
         Height          =   315
         Left            =   120
         TabIndex        =   181
         ToolTipText     =   "Enter Specifi Ratecard entries for this company"
         Top             =   8760
         Width           =   1815
      End
      Begin VB.CommandButton cmdTakeSpaces 
         Caption         =   "Take Spaces"
         Height          =   255
         Left            =   2760
         TabIndex        =   180
         Top             =   240
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.ComboBox txtCountry 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFC0C0&
         Height          =   315
         Left            =   1260
         TabIndex        =   179
         ToolTipText     =   "What Ratecard for this Company"
         Top             =   2640
         Width           =   3315
      End
      Begin VB.TextBox txtInvoiceEmail 
         BackColor       =   &H00FFC0FF&
         DataField       =   "invoicecountry"
         Height          =   285
         Left            =   1260
         TabIndex        =   178
         ToolTipText     =   "Country for Invoices if different"
         Top             =   7920
         Width           =   3315
      End
      Begin VB.CheckBox chkInactiveCompany 
         Alignment       =   1  'Right Justify
         Caption         =   "Active ?"
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   177
         Top             =   8280
         Width           =   1335
      End
      Begin VB.ComboBox cmbBusinessUnit 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFC0C0&
         Height          =   315
         Left            =   1260
         TabIndex        =   176
         ToolTipText     =   "What Ratecard for this Company"
         Top             =   5520
         Width           =   3315
      End
      Begin MSAdodcLib.Adodc adoCompany 
         Height          =   330
         Left            =   2220
         Top             =   1320
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   "cetasoft"
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoCompany"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
         Bindings        =   "frmCompany.frx":08CA
         Height          =   285
         Left            =   1260
         TabIndex        =   201
         ToolTipText     =   "The company name"
         Top             =   600
         Width           =   3315
         DataFieldList   =   "name"
         _Version        =   196617
         BorderStyle     =   0
         BeveColorScheme =   1
         ForeColorEven   =   -2147483640
         ForeColorOdd    =   -2147483640
         BackColorEven   =   -2147483643
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   6376
         Columns(0).Caption=   "Name"
         Columns(0).Name =   "name"
         Columns(0).DataField=   "name"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "companyID"
         Columns(1).Name =   "companyID"
         Columns(1).DataField=   "companyID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Post Code"
         Columns(2).Name =   "postcode"
         Columns(2).DataField=   "postcode"
         Columns(2).FieldLen=   256
         _ExtentX        =   5847
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16761087
         DataFieldToDisplay=   "name"
      End
      Begin VB.Label lblCaption 
         Caption         =   "Company ID"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   221
         Top             =   240
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Name"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   220
         Top             =   600
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Address"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   219
         Top             =   1320
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Post Code"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   218
         Top             =   2340
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Country"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   217
         Top             =   2700
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Telephone"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   216
         Top             =   3060
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Fax"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   215
         Top             =   3420
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Email"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   214
         Top             =   3780
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Website"
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   213
         Top             =   4140
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Short Name"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   212
         Top             =   960
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Source"
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   211
         Top             =   4500
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Trade Code"
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   210
         Top             =   4860
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Comments"
         Height          =   255
         Index           =   12
         Left            =   120
         TabIndex        =   209
         Top             =   5220
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Invoice Co"
         Height          =   255
         Index           =   25
         Left            =   120
         TabIndex        =   208
         Top             =   5940
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Country"
         Height          =   255
         Index           =   26
         Left            =   120
         TabIndex        =   207
         Top             =   7620
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Post Code"
         Height          =   255
         Index           =   27
         Left            =   120
         TabIndex        =   206
         Top             =   7260
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Invoice Address"
         Height          =   495
         Index           =   28
         Left            =   120
         TabIndex        =   205
         Top             =   6300
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Invoice Email"
         Height          =   255
         Index           =   82
         Left            =   120
         TabIndex        =   204
         Top             =   7980
         Width           =   1035
      End
      Begin VB.Label Label1 
         Caption         =   "Please use Ratecard 8 for new Customers unless using non-standard rates"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   555
         Left            =   120
         TabIndex        =   203
         Top             =   9960
         Width           =   4755
      End
      Begin VB.Label lblCaption 
         Caption         =   "Business Unit"
         Height          =   255
         Index           =   84
         Left            =   120
         TabIndex        =   202
         Top             =   5580
         Width           =   1035
      End
   End
   Begin TabDlg.SSTab tabDetails 
      Height          =   9975
      Left            =   3780
      TabIndex        =   19
      Top             =   60
      Width           =   19755
      _ExtentX        =   34846
      _ExtentY        =   17595
      _Version        =   393216
      Style           =   1
      Tabs            =   11
      TabsPerRow      =   11
      TabHeight       =   520
      TabCaption(0)   =   "Details"
      TabPicture(0)   =   "frmCompany.frx":08E3
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblCaption(24)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblCaption(22)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblCaption(21)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblCaption(20)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblCaption(19)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblCaption(18)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "lblCaption(17)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "lblCaption(16)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "lblCaption(15)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "lblCaption(14)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "lblCaption(13)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "lblCaption(35)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "lblCaption(36)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "lblCaption(37)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "lblCaption(41)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "lblCaption(42)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "lblCaption(23)"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "lblCaption(92)"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "lblCaption(86)"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "grdContacts"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "datPostedToAccounts"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "datAccountStart"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "adoContact"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "txtSalesLedgerCode"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "chkProspective"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "chkSupplier"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "chkClient"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "cmbRateCard"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).Control(28)=   "txtRegisteredOffice"
      Tab(0).Control(28).Enabled=   0   'False
      Tab(0).Control(29)=   "cmbAccountStatus"
      Tab(0).Control(29).Enabled=   0   'False
      Tab(0).Control(30)=   "txtAccountCode"
      Tab(0).Control(30).Enabled=   0   'False
      Tab(0).Control(31)=   "txtClientCode"
      Tab(0).Control(31).Enabled=   0   'False
      Tab(0).Control(32)=   "txtCreatedDate"
      Tab(0).Control(32).Enabled=   0   'False
      Tab(0).Control(33)=   "txtVATNumber"
      Tab(0).Control(33).Enabled=   0   'False
      Tab(0).Control(34)=   "txtVATCode"
      Tab(0).Control(34).Enabled=   0   'False
      Tab(0).Control(35)=   "cmdAddNewContact"
      Tab(0).Control(35).Enabled=   0   'False
      Tab(0).Control(36)=   "txtCreatedBy"
      Tab(0).Control(36).Enabled=   0   'False
      Tab(0).Control(37)=   "txtModifiedDate"
      Tab(0).Control(37).Enabled=   0   'False
      Tab(0).Control(38)=   "txtModifiedBy"
      Tab(0).Control(38).Enabled=   0   'False
      Tab(0).Control(39)=   "txtCreditLimit"
      Tab(0).Control(39).Enabled=   0   'False
      Tab(0).Control(40)=   "txtDiscount"
      Tab(0).Control(40).Enabled=   0   'False
      Tab(0).Control(41)=   "chkCPGlobal"
      Tab(0).Control(41).Enabled=   0   'False
      Tab(0).Control(42)=   "fraClientServicesTeams"
      Tab(0).Control(42).Enabled=   0   'False
      Tab(0).Control(43)=   "txtSageCustomerReference"
      Tab(0).Control(43).Enabled=   0   'False
      Tab(0).Control(44)=   "chkLatimerLock"
      Tab(0).Control(44).Enabled=   0   'False
      Tab(0).Control(45)=   "txtMediaPulseCompanyID"
      Tab(0).Control(45).Enabled=   0   'False
      Tab(0).Control(46)=   "txtMediaPulseJobID"
      Tab(0).Control(46).Enabled=   0   'False
      Tab(0).ControlCount=   47
      TabCaption(1)   =   "Notes / Labels"
      TabPicture(1)   =   "frmCompany.frx":08FF
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lblCaption(34)"
      Tab(1).Control(1)=   "lblCaption(33)"
      Tab(1).Control(2)=   "lblCaption(32)"
      Tab(1).Control(3)=   "lblCaption(31)"
      Tab(1).Control(4)=   "lblCaption(30)"
      Tab(1).Control(5)=   "lblCaption(29)"
      Tab(1).Control(6)=   "txtAccountsNotes"
      Tab(1).Control(7)=   "txtOperatorNotes"
      Tab(1).Control(8)=   "txtOfficeClientNotes"
      Tab(1).Control(9)=   "txtDespatchNotes"
      Tab(1).Control(10)=   "txtProducerNotes"
      Tab(1).ControlCount=   11
      TabCaption(2)   =   "Marketing"
      TabPicture(2)   =   "frmCompany.frx":091B
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "lblCaption(40)"
      Tab(2).Control(1)=   "grdMarketingHistory"
      Tab(2).Control(2)=   "fraCreateNew"
      Tab(2).ControlCount=   3
      TabCaption(3)   =   "Rate Card"
      TabPicture(3)   =   "frmCompany.frx":0937
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "lblCaption(43)"
      Tab(3).Control(1)=   "grdSupplierRates"
      Tab(3).Control(2)=   "adoSupplierRates"
      Tab(3).ControlCount=   3
      TabCaption(4)   =   "Messages"
      TabPicture(4)   =   "frmCompany.frx":0953
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "lblCaption(39)"
      Tab(4).Control(1)=   "lblCaption(38)"
      Tab(4).Control(2)=   "txtCostingsAlert"
      Tab(4).Control(3)=   "txtBookingsAlert"
      Tab(4).ControlCount=   4
      TabCaption(5)   =   "Associations"
      TabPicture(5)   =   "frmCompany.frx":096F
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "lblCaption(50)"
      Tab(5).Control(1)=   "lblCaption(49)"
      Tab(5).Control(2)=   "lblCaption(48)"
      Tab(5).Control(3)=   "lblCaption(47)"
      Tab(5).Control(4)=   "lblAssociationID"
      Tab(5).Control(5)=   "lblAssociationCompanyID"
      Tab(5).Control(6)=   "lblCaption(46)"
      Tab(5).Control(7)=   "lblCaption(45)"
      Tab(5).Control(8)=   "lblCaption(44)"
      Tab(5).Control(9)=   "adoCompany2"
      Tab(5).Control(10)=   "cmbCompany2"
      Tab(5).Control(11)=   "grdAssociatedCompanies"
      Tab(5).Control(12)=   "adoAssociatedCompanies"
      Tab(5).Control(13)=   "cmdNewAssociation"
      Tab(5).Control(14)=   "chkCompanyPermission(7)"
      Tab(5).Control(15)=   "chkCompanyPermission(6)"
      Tab(5).Control(16)=   "chkCompanyPermission(5)"
      Tab(5).Control(17)=   "chkCompanyPermission(4)"
      Tab(5).Control(18)=   "chkCompanyPermission(3)"
      Tab(5).Control(19)=   "chkCompanyPermission(2)"
      Tab(5).Control(20)=   "chkCompanyPermission(1)"
      Tab(5).Control(21)=   "chkCompanyPermission(0)"
      Tab(5).Control(22)=   "cmdRemoveAssociation"
      Tab(5).Control(23)=   "cmdAddAssociation"
      Tab(5).Control(24)=   "cmbAssociationType"
      Tab(5).Control(25)=   "txtAssociationComment"
      Tab(5).ControlCount=   26
      TabCaption(6)   =   "Trackers"
      TabPicture(6)   =   "frmCompany.frx":098B
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "lblCaption(64)"
      Tab(6).Control(1)=   "lblCaption(65)"
      Tab(6).Control(2)=   "lblCaption(83)"
      Tab(6).Control(3)=   "lblCaption(91)"
      Tab(6).Control(4)=   "grdTrackerNotifications"
      Tab(6).Control(5)=   "adoTrackerNotifications"
      Tab(6).Control(6)=   "grdTrackerChoices"
      Tab(6).Control(7)=   "adoTrackerChoices"
      Tab(6).Control(8)=   "ddnTrackerFields"
      Tab(6).Control(9)=   "ddnMessages"
      Tab(6).Control(10)=   "adoMessages"
      Tab(6).Control(11)=   "txtProgressRefName"
      Tab(6).Control(12)=   "txtTrackerTitle"
      Tab(6).Control(13)=   "txtDTBillingCompanyID"
      Tab(6).Control(14)=   "ddnTrackerDataFormat"
      Tab(6).Control(15)=   "txtAS11BillingCompanyID"
      Tab(6).ControlCount=   16
      TabCaption(7)   =   "Content Delivery"
      TabPicture(7)   =   "frmCompany.frx":09A7
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "txtUploadCode"
      Tab(7).Control(1)=   "txtDownloadCode"
      Tab(7).Control(2)=   "txtPasswordLifetime"
      Tab(7).Control(3)=   "txtportalpasswordqualitystring"
      Tab(7).Control(4)=   "txtportalpasswordqualityexplanation"
      Tab(7).Control(5)=   "txtAdminText(3)"
      Tab(7).Control(6)=   "txtAdminTitle(3)"
      Tab(7).Control(7)=   "txtAdminLink(3)"
      Tab(7).Control(8)=   "txtAdminText(2)"
      Tab(7).Control(9)=   "txtAdminTitle(2)"
      Tab(7).Control(10)=   "txtAdminLink(2)"
      Tab(7).Control(11)=   "txtAdminText(1)"
      Tab(7).Control(12)=   "txtAdminTitle(1)"
      Tab(7).Control(13)=   "txtAdminLink(1)"
      Tab(7).Control(14)=   "txtPortalCompanyName"
      Tab(7).Control(15)=   "cmdMediaPortalUsers"
      Tab(7).Control(16)=   "txtMainPage"
      Tab(7).Control(17)=   "txtFooterPage"
      Tab(7).Control(18)=   "txtJCAContactEmail"
      Tab(7).Control(19)=   "txtEmailSubject"
      Tab(7).Control(20)=   "txtPortalURL"
      Tab(7).Control(21)=   "txtHeaderPage"
      Tab(7).Control(22)=   "txtlabel6"
      Tab(7).Control(23)=   "txtlabel5"
      Tab(7).Control(24)=   "txtlabel4"
      Tab(7).Control(25)=   "txtlabel3"
      Tab(7).Control(26)=   "txtlabel2"
      Tab(7).Control(27)=   "txtlabel1"
      Tab(7).Control(28)=   "txtEmailBody"
      Tab(7).Control(29)=   "chkUpdate"
      Tab(7).Control(30)=   "txtWelcomeMessage"
      Tab(7).Control(31)=   "txtOrderNumberFieldName"
      Tab(7).Control(32)=   "adoPortalTitleStubs"
      Tab(7).Control(33)=   "grdPortalTitleStubs"
      Tab(7).Control(34)=   "lblCaption(85)"
      Tab(7).Control(35)=   "lblCaption(57)"
      Tab(7).Control(36)=   "lblCaption(81)"
      Tab(7).Control(37)=   "lblCaption(79)"
      Tab(7).Control(38)=   "lblCaption(80)"
      Tab(7).Control(39)=   "lblCaption(78)"
      Tab(7).Control(40)=   "lblCaption(77)"
      Tab(7).Control(41)=   "lblCaption(76)"
      Tab(7).Control(42)=   "lblCaption(75)"
      Tab(7).Control(43)=   "lblCaption(74)"
      Tab(7).Control(44)=   "lblCaption(73)"
      Tab(7).Control(45)=   "lblCaption(72)"
      Tab(7).Control(46)=   "lblCaption(71)"
      Tab(7).Control(47)=   "lblCaption(70)"
      Tab(7).Control(48)=   "lblCaption(69)"
      Tab(7).Control(49)=   "lblCaption(62)"
      Tab(7).Control(50)=   "lblCaption(63)"
      Tab(7).Control(51)=   "lblCaption(61)"
      Tab(7).Control(52)=   "lblCaption(60)"
      Tab(7).Control(53)=   "lblCaption(59)"
      Tab(7).Control(54)=   "lblCaption(58)"
      Tab(7).Control(55)=   "lblCaption(56)"
      Tab(7).Control(56)=   "lblCaption(55)"
      Tab(7).Control(57)=   "lblCaption(54)"
      Tab(7).Control(58)=   "lblCaption(53)"
      Tab(7).Control(59)=   "lblCaption(52)"
      Tab(7).Control(60)=   "lblCaption(51)"
      Tab(7).Control(61)=   "lblCaption(66)"
      Tab(7).Control(62)=   "lblCaption(67)"
      Tab(7).Control(63)=   "lblCaption(68)"
      Tab(7).ControlCount=   64
      TabCaption(8)   =   "Customer Contacts"
      TabPicture(8)   =   "frmCompany.frx":09C3
      Tab(8).ControlEnabled=   0   'False
      Tab(8).Control(0)=   "adoCompanyContact"
      Tab(8).Control(1)=   "grdCompanyContacts"
      Tab(8).ControlCount=   2
      TabCaption(9)   =   "Material Tracker"
      TabPicture(9)   =   "frmCompany.frx":09DF
      Tab(9).ControlEnabled=   0   'False
      Tab(9).Control(0)=   "adoMaterialTrackerChoices"
      Tab(9).Control(1)=   "ddnMaterialTrackerFields"
      Tab(9).Control(2)=   "grdMaterialTrackerChoices"
      Tab(9).ControlCount=   3
      TabCaption(10)  =   "MOM / Diva"
      TabPicture(10)  =   "frmCompany.frx":09FB
      Tab(10).ControlEnabled=   0   'False
      Tab(10).Control(0)=   "lblCaption(87)"
      Tab(10).Control(1)=   "lblCaption(88)"
      Tab(10).Control(2)=   "lblCaption(89)"
      Tab(10).Control(3)=   "lblCaption(90)"
      Tab(10).Control(4)=   "txtMX1360CustomerID"
      Tab(10).Control(5)=   "txtMX1360Username"
      Tab(10).Control(6)=   "txtMX1360password"
      Tab(10).Control(7)=   "txtMX1360ProxyTranscodeID"
      Tab(10).ControlCount=   8
      Begin VB.TextBox txtMediaPulseJobID 
         Height          =   285
         Left            =   5280
         TabIndex        =   240
         ToolTipText     =   "Company Code for Sales Ledger"
         Top             =   4500
         Width           =   1815
      End
      Begin VB.TextBox txtMediaPulseCompanyID 
         Height          =   285
         Left            =   5280
         TabIndex        =   238
         ToolTipText     =   "Company Code for Sales Ledger"
         Top             =   4140
         Width           =   1815
      End
      Begin VB.CheckBox chkLatimerLock 
         Caption         =   "Latimer MediaPulse Customer"
         Height          =   195
         Left            =   7560
         TabIndex        =   237
         ToolTipText     =   "This Company is a Marketing Prospect"
         Top             =   3180
         Width           =   2955
      End
      Begin VB.TextBox txtAS11BillingCompanyID 
         DataField       =   "source"
         Height          =   345
         Left            =   -64320
         TabIndex        =   235
         Top             =   420
         Width           =   675
      End
      Begin VB.TextBox txtSageCustomerReference 
         Height          =   285
         Left            =   5280
         TabIndex        =   233
         ToolTipText     =   "Company Code for Sales Ledger"
         Top             =   3780
         Width           =   1815
      End
      Begin VB.TextBox txtMX1360ProxyTranscodeID 
         DataField       =   "fax"
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   -72600
         TabIndex        =   228
         ToolTipText     =   "Company Fax"
         Top             =   1620
         Width           =   3315
      End
      Begin VB.TextBox txtMX1360password 
         DataField       =   "fax"
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   -72600
         PasswordChar    =   "*"
         TabIndex        =   227
         ToolTipText     =   "Company Fax"
         Top             =   1260
         Width           =   3315
      End
      Begin VB.TextBox txtMX1360Username 
         DataField       =   "fax"
         Height          =   285
         Left            =   -72600
         TabIndex        =   226
         ToolTipText     =   "Company Fax"
         Top             =   900
         Width           =   3315
      End
      Begin VB.TextBox txtMX1360CustomerID 
         DataField       =   "fax"
         Height          =   285
         Left            =   -72600
         TabIndex        =   225
         ToolTipText     =   "Company Fax"
         Top             =   540
         Width           =   3315
      End
      Begin VB.TextBox txtUploadCode 
         DataField       =   "accountcode"
         Height          =   285
         Left            =   -65460
         TabIndex        =   223
         Top             =   2040
         Width           =   1815
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnTrackerDataFormat 
         Height          =   1335
         Left            =   -68580
         TabIndex        =   222
         Top             =   2520
         Width           =   3735
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   3200
         Columns(0).Caption=   "Column 0 "
         Columns(0).Name =   "Column 0 "
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   6588
         _ExtentY        =   2355
         _StockProps     =   77
      End
      Begin VB.Frame fraClientServicesTeams 
         Caption         =   "Client Services Teams"
         Height          =   1335
         Left            =   7560
         TabIndex        =   171
         Top             =   3840
         Width           =   2055
         Begin VB.CheckBox chkTVIndependents 
            Caption         =   "Independents Team"
            Height          =   255
            Left            =   120
            TabIndex        =   174
            ToolTipText     =   "This Company is a Client"
            Top             =   960
            Width           =   1875
         End
         Begin VB.CheckBox chkTVStudios 
            Caption         =   "Studios Team"
            Height          =   195
            Left            =   120
            TabIndex        =   173
            ToolTipText     =   "This Company is a Client"
            Top             =   660
            Width           =   1575
         End
         Begin VB.CheckBox chkTVDistribution 
            Caption         =   "Distribution Team"
            Height          =   195
            Left            =   120
            TabIndex        =   172
            ToolTipText     =   "This Company is a Client"
            Top             =   300
            Width           =   1695
         End
      End
      Begin VB.TextBox txtDTBillingCompanyID 
         DataField       =   "source"
         Height          =   345
         Left            =   -67200
         TabIndex        =   169
         Top             =   420
         Width           =   675
      End
      Begin VB.CheckBox chkCPGlobal 
         Caption         =   "CP Global Client"
         Height          =   195
         Left            =   7560
         TabIndex        =   168
         ToolTipText     =   "This Company is a Marketing Prospect"
         Top             =   3480
         Width           =   1815
      End
      Begin MSAdodcLib.Adodc adoMaterialTrackerChoices 
         Height          =   330
         Left            =   -74820
         Top             =   840
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   "cetasoft"
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoMaterialTrackerChoices"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnMaterialTrackerFields 
         Height          =   1335
         Left            =   -74460
         TabIndex        =   167
         Top             =   2040
         Width           =   3735
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   3200
         Columns(0).Caption=   "Column 0 "
         Columns(0).Name =   "Column 0 "
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   6588
         _ExtentY        =   2355
         _StockProps     =   77
      End
      Begin VB.TextBox txtDownloadCode 
         DataField       =   "accountcode"
         Height          =   285
         Left            =   -65460
         TabIndex        =   164
         Top             =   1740
         Width           =   1815
      End
      Begin VB.TextBox txtPasswordLifetime 
         DataField       =   "salesledgercode"
         Height          =   285
         Left            =   -66660
         TabIndex        =   162
         ToolTipText     =   "Company Code for Sales Ledger"
         Top             =   720
         Width           =   735
      End
      Begin VB.TextBox txtportalpasswordqualitystring 
         Height          =   945
         Left            =   -74880
         MultiLine       =   -1  'True
         TabIndex        =   159
         ToolTipText     =   "Ceta Client Code"
         Top             =   720
         Width           =   3195
      End
      Begin VB.TextBox txtportalpasswordqualityexplanation 
         Height          =   945
         Left            =   -71580
         MultiLine       =   -1  'True
         TabIndex        =   158
         ToolTipText     =   "Ceta Client Code"
         Top             =   720
         Width           =   3195
      End
      Begin VB.TextBox txtAdminText 
         DataField       =   "accountcode"
         Height          =   585
         Index           =   3
         Left            =   -71580
         MultiLine       =   -1  'True
         TabIndex        =   148
         Top             =   7800
         Width           =   4695
      End
      Begin VB.TextBox txtAdminTitle 
         DataField       =   "accountcode"
         Height          =   285
         Index           =   3
         Left            =   -71580
         TabIndex        =   147
         Top             =   7500
         Width           =   4695
      End
      Begin VB.TextBox txtAdminLink 
         DataField       =   "accountcode"
         Height          =   285
         Index           =   3
         Left            =   -71580
         TabIndex        =   146
         Top             =   7200
         Width           =   4695
      End
      Begin VB.TextBox txtAdminText 
         DataField       =   "accountcode"
         Height          =   585
         Index           =   2
         Left            =   -71580
         MultiLine       =   -1  'True
         TabIndex        =   145
         Top             =   6600
         Width           =   4695
      End
      Begin VB.TextBox txtAdminTitle 
         DataField       =   "accountcode"
         Height          =   285
         Index           =   2
         Left            =   -71580
         TabIndex        =   144
         Top             =   6300
         Width           =   4695
      End
      Begin VB.TextBox txtAdminLink 
         DataField       =   "accountcode"
         Height          =   285
         Index           =   2
         Left            =   -71580
         TabIndex        =   143
         Top             =   6000
         Width           =   4695
      End
      Begin VB.TextBox txtAdminText 
         DataField       =   "accountcode"
         Height          =   585
         Index           =   1
         Left            =   -71580
         MultiLine       =   -1  'True
         TabIndex        =   142
         Top             =   5400
         Width           =   4695
      End
      Begin VB.TextBox txtAdminTitle 
         DataField       =   "accountcode"
         Height          =   285
         Index           =   1
         Left            =   -71580
         TabIndex        =   141
         Top             =   5100
         Width           =   4695
      End
      Begin VB.TextBox txtAdminLink 
         DataField       =   "accountcode"
         Height          =   285
         Index           =   1
         Left            =   -71580
         TabIndex        =   140
         Top             =   4800
         Width           =   4695
      End
      Begin MSAdodcLib.Adodc adoCompanyContact 
         Height          =   330
         Left            =   -74880
         Top             =   540
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoCompanyContact"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.TextBox txtPortalCompanyName 
         DataField       =   "accountcode"
         Height          =   285
         Left            =   -71580
         TabIndex        =   137
         Top             =   4500
         Width           =   4695
      End
      Begin VB.CommandButton cmdMediaPortalUsers 
         Caption         =   "Content Delivery Users"
         Height          =   315
         Left            =   -65700
         TabIndex        =   121
         ToolTipText     =   "Enter Specific Ratecard entries for this company"
         Top             =   720
         Width           =   1935
      End
      Begin VB.ComboBox txtMainPage 
         Height          =   315
         Left            =   -71580
         TabIndex        =   120
         Top             =   8400
         Width           =   4695
      End
      Begin VB.TextBox txtFooterPage 
         DataField       =   "accountcode"
         Height          =   285
         Left            =   -71580
         TabIndex        =   119
         Top             =   2040
         Width           =   4695
      End
      Begin VB.TextBox txtJCAContactEmail 
         DataField       =   "accountcode"
         Height          =   285
         Left            =   -71580
         TabIndex        =   118
         Top             =   4200
         Width           =   4695
      End
      Begin VB.TextBox txtEmailSubject 
         DataField       =   "accountcode"
         Height          =   285
         Left            =   -71580
         TabIndex        =   117
         Top             =   2940
         Width           =   4695
      End
      Begin VB.TextBox txtPortalURL 
         DataField       =   "accountcode"
         Height          =   285
         Left            =   -71580
         TabIndex        =   116
         Top             =   2340
         Width           =   4695
      End
      Begin VB.TextBox txtHeaderPage 
         DataField       =   "accountcode"
         Height          =   285
         Left            =   -71580
         TabIndex        =   115
         Top             =   1740
         Width           =   4695
      End
      Begin VB.TextBox txtlabel6 
         DataField       =   "accountcode"
         Height          =   285
         Left            =   -67380
         TabIndex        =   114
         Top             =   9540
         Width           =   1995
      End
      Begin VB.TextBox txtlabel5 
         DataField       =   "accountcode"
         Height          =   285
         Left            =   -70260
         TabIndex        =   113
         Top             =   9540
         Width           =   1815
      End
      Begin VB.TextBox txtlabel4 
         DataField       =   "accountcode"
         Height          =   285
         Left            =   -67380
         TabIndex        =   112
         Top             =   9240
         Width           =   1995
      End
      Begin VB.TextBox txtlabel3 
         DataField       =   "accountcode"
         Height          =   285
         Left            =   -70260
         TabIndex        =   111
         Top             =   9240
         Width           =   1815
      End
      Begin VB.TextBox txtlabel2 
         DataField       =   "accountcode"
         Height          =   285
         Left            =   -67380
         TabIndex        =   110
         Top             =   8940
         Width           =   1995
      End
      Begin VB.TextBox txtlabel1 
         DataField       =   "accountcode"
         Height          =   285
         Left            =   -70260
         TabIndex        =   109
         Top             =   8940
         Width           =   1815
      End
      Begin VB.TextBox txtEmailBody 
         DataField       =   "accountcode"
         Height          =   465
         Left            =   -71580
         MultiLine       =   -1  'True
         TabIndex        =   107
         Top             =   3240
         Width           =   4695
      End
      Begin VB.CheckBox chkUpdate 
         Height          =   255
         Left            =   -66840
         TabIndex        =   106
         Top             =   8460
         Width           =   195
      End
      Begin VB.TextBox txtWelcomeMessage 
         DataField       =   "accountcode"
         Height          =   465
         Left            =   -71580
         MultiLine       =   -1  'True
         TabIndex        =   105
         Top             =   3720
         Width           =   4695
      End
      Begin VB.TextBox txtOrderNumberFieldName 
         DataField       =   "accountcode"
         Height          =   285
         Left            =   -71580
         TabIndex        =   104
         Top             =   2640
         Width           =   4695
      End
      Begin VB.TextBox txtTrackerTitle 
         DataField       =   "source"
         Height          =   345
         Left            =   -72780
         TabIndex        =   98
         Top             =   420
         Width           =   3315
      End
      Begin VB.TextBox txtProgressRefName 
         DataField       =   "accountcode"
         Height          =   345
         Left            =   -60180
         TabIndex        =   97
         Top             =   420
         Width           =   3615
      End
      Begin VB.TextBox txtAssociationComment 
         Height          =   525
         Left            =   -73680
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   84
         Text            =   "frmCompany.frx":0A17
         Top             =   5520
         Width           =   3075
      End
      Begin VB.ComboBox cmbAssociationType 
         Height          =   315
         Left            =   -73680
         TabIndex        =   83
         Top             =   5100
         Width           =   3075
      End
      Begin VB.CommandButton cmdAddAssociation 
         Caption         =   "Add / Save"
         Height          =   315
         Left            =   -72900
         TabIndex        =   82
         Top             =   7740
         Width           =   1095
      End
      Begin VB.CommandButton cmdRemoveAssociation 
         Caption         =   "Remove"
         Enabled         =   0   'False
         Height          =   315
         Left            =   -71700
         TabIndex        =   81
         Top             =   7740
         Width           =   1095
      End
      Begin VB.CheckBox chkCompanyPermission 
         Caption         =   "Read"
         Enabled         =   0   'False
         Height          =   195
         Index           =   0
         Left            =   -73680
         TabIndex        =   80
         Top             =   6180
         Width           =   795
      End
      Begin VB.CheckBox chkCompanyPermission 
         Caption         =   "Write"
         Enabled         =   0   'False
         Height          =   195
         Index           =   1
         Left            =   -72840
         TabIndex        =   79
         Top             =   6180
         Width           =   795
      End
      Begin VB.CheckBox chkCompanyPermission 
         Caption         =   "Read"
         Enabled         =   0   'False
         Height          =   195
         Index           =   2
         Left            =   -73680
         TabIndex        =   78
         Top             =   6540
         Width           =   795
      End
      Begin VB.CheckBox chkCompanyPermission 
         Caption         =   "Write"
         Enabled         =   0   'False
         Height          =   195
         Index           =   3
         Left            =   -72840
         TabIndex        =   77
         Top             =   6540
         Width           =   795
      End
      Begin VB.CheckBox chkCompanyPermission 
         Caption         =   "Read"
         Enabled         =   0   'False
         Height          =   195
         Index           =   4
         Left            =   -73680
         TabIndex        =   76
         Top             =   6900
         Width           =   795
      End
      Begin VB.CheckBox chkCompanyPermission 
         Caption         =   "Write"
         Enabled         =   0   'False
         Height          =   195
         Index           =   5
         Left            =   -72840
         TabIndex        =   75
         Top             =   6900
         Width           =   795
      End
      Begin VB.CheckBox chkCompanyPermission 
         Caption         =   "Read"
         Enabled         =   0   'False
         Height          =   195
         Index           =   6
         Left            =   -73680
         TabIndex        =   74
         Top             =   7260
         Width           =   795
      End
      Begin VB.CheckBox chkCompanyPermission 
         Caption         =   "Write"
         Enabled         =   0   'False
         Height          =   195
         Index           =   7
         Left            =   -72840
         TabIndex        =   73
         Top             =   7260
         Width           =   795
      End
      Begin VB.CommandButton cmdNewAssociation 
         Caption         =   "New"
         Height          =   315
         Left            =   -74100
         TabIndex        =   72
         Top             =   7740
         Width           =   1095
      End
      Begin VB.TextBox txtBookingsAlert 
         Height          =   1995
         Left            =   -74880
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   69
         Top             =   720
         Width           =   9435
      End
      Begin VB.TextBox txtCostingsAlert 
         Height          =   1995
         Left            =   -74880
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   68
         Top             =   3060
         Width           =   9435
      End
      Begin VB.Frame fraCreateNew 
         Caption         =   "Create New..."
         Height          =   1635
         Left            =   -74760
         TabIndex        =   60
         Top             =   7260
         Width           =   1515
         Begin VB.CommandButton cmdCreateMerge 
            Caption         =   "Word"
            Height          =   315
            Left            =   180
            TabIndex        =   63
            ToolTipText     =   "Enter Specific Ratecard entries for this company"
            Top             =   300
            Width           =   1155
         End
         Begin VB.CommandButton cmdCreateEmail 
            Caption         =   "Email"
            Height          =   315
            Left            =   180
            TabIndex        =   62
            ToolTipText     =   "Enter Specific Ratecard entries for this company"
            Top             =   720
            Width           =   1155
         End
         Begin VB.CommandButton cmdCreateNote 
            Caption         =   "Note"
            Height          =   315
            Left            =   180
            TabIndex        =   61
            ToolTipText     =   "Enter Specific Ratecard entries for this company"
            Top             =   1140
            Width           =   1155
         End
      End
      Begin VB.TextBox txtProducerNotes 
         BackColor       =   &H00FFC0FF&
         Height          =   975
         Left            =   -74820
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   53
         Top             =   4920
         Width           =   9315
      End
      Begin VB.TextBox txtDespatchNotes 
         BackColor       =   &H00C0FFC0&
         Height          =   975
         Left            =   -74820
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   52
         Top             =   3600
         Width           =   9315
      End
      Begin VB.TextBox txtOfficeClientNotes 
         BackColor       =   &H00FFC0C0&
         Height          =   1035
         Left            =   -74820
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   51
         Top             =   1020
         Width           =   9375
      End
      Begin VB.TextBox txtOperatorNotes 
         BackColor       =   &H00C0E0FF&
         Height          =   975
         Left            =   -74820
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   50
         Top             =   2340
         Width           =   9315
      End
      Begin VB.TextBox txtAccountsNotes 
         BackColor       =   &H00C0C0FF&
         Height          =   975
         Left            =   -74820
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   49
         Top             =   6240
         Width           =   9315
      End
      Begin VB.TextBox txtDiscount 
         BackColor       =   &H00C0C0FF&
         DataField       =   "vatcode"
         Height          =   285
         Left            =   1320
         TabIndex        =   47
         ToolTipText     =   "Client's VAT code"
         Top             =   5220
         Width           =   1815
      End
      Begin VB.TextBox txtCreditLimit 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0FF&
         Height          =   285
         Left            =   1320
         TabIndex        =   45
         ToolTipText     =   "Ceta Client Code"
         Top             =   5580
         Width           =   1815
      End
      Begin VB.TextBox txtModifiedBy 
         Height          =   285
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   41
         TabStop         =   0   'False
         ToolTipText     =   "Date Company was Created"
         Top             =   3060
         Width           =   555
      End
      Begin VB.TextBox txtModifiedDate 
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   39
         TabStop         =   0   'False
         ToolTipText     =   "Date Company was Created"
         Top             =   3060
         Width           =   1815
      End
      Begin VB.TextBox txtCreatedBy 
         Height          =   285
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   37
         TabStop         =   0   'False
         ToolTipText     =   "Date Company was Created"
         Top             =   2700
         Width           =   555
      End
      Begin VB.CommandButton cmdAddNewContact 
         Caption         =   "Add New Contact"
         Height          =   315
         Left            =   7620
         TabIndex        =   13
         ToolTipText     =   "Enter Specifi Ratecard entries for this company"
         Top             =   5580
         Width           =   1995
      End
      Begin VB.TextBox txtVATCode 
         BackColor       =   &H00FFC0C0&
         DataField       =   "vatcode"
         Height          =   285
         Left            =   5280
         TabIndex        =   11
         ToolTipText     =   "Client's VAT code"
         Top             =   5220
         Width           =   1815
      End
      Begin VB.TextBox txtVATNumber 
         BackColor       =   &H00FFC0C0&
         DataField       =   "vatnumber"
         Height          =   285
         Left            =   5280
         TabIndex        =   12
         ToolTipText     =   "VAT Number for this Company including EU Letters"
         Top             =   5580
         Width           =   1815
      End
      Begin VB.TextBox txtCreatedDate 
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   20
         TabStop         =   0   'False
         ToolTipText     =   "Date Company was Created"
         Top             =   2700
         Width           =   1815
      End
      Begin VB.TextBox txtClientCode 
         Height          =   2205
         Left            =   1320
         MultiLine       =   -1  'True
         TabIndex        =   3
         ToolTipText     =   "Ceta Client Code"
         Top             =   420
         Width           =   18015
      End
      Begin VB.TextBox txtAccountCode 
         DataField       =   "accountcode"
         Height          =   285
         Left            =   1320
         TabIndex        =   4
         Top             =   3420
         Width           =   1815
      End
      Begin VB.ComboBox cmbAccountStatus 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1320
         TabIndex        =   7
         Top             =   4140
         Width           =   1815
      End
      Begin VB.TextBox txtRegisteredOffice 
         BackColor       =   &H00FFC0C0&
         DataField       =   "registeredoffice"
         Height          =   285
         Left            =   5280
         TabIndex        =   10
         ToolTipText     =   "Client's Registered Office number"
         Top             =   4860
         Width           =   1815
      End
      Begin VB.ComboBox cmbRateCard 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0FF&
         Height          =   315
         Left            =   1320
         TabIndex        =   8
         ToolTipText     =   "What Ratecard for this Company"
         Top             =   4860
         Width           =   1815
      End
      Begin VB.CheckBox chkClient 
         Alignment       =   1  'Right Justify
         Caption         =   "Client"
         Height          =   195
         Left            =   5040
         TabIndex        =   0
         ToolTipText     =   "This Company is a Client"
         Top             =   2700
         Width           =   1335
      End
      Begin VB.CheckBox chkSupplier 
         Alignment       =   1  'Right Justify
         Caption         =   "Supplier"
         Height          =   195
         Left            =   5040
         TabIndex        =   1
         ToolTipText     =   "This Company is a Supplier"
         Top             =   2940
         Width           =   1335
      End
      Begin VB.CheckBox chkProspective 
         Alignment       =   1  'Right Justify
         Caption         =   "Prospective"
         Height          =   195
         Left            =   5040
         TabIndex        =   2
         ToolTipText     =   "This Company is a Marketing Prospect"
         Top             =   3180
         Width           =   1335
      End
      Begin VB.TextBox txtSalesLedgerCode 
         DataField       =   "salesledgercode"
         Height          =   285
         Left            =   5280
         TabIndex        =   5
         ToolTipText     =   "Company Code for Sales Ledger"
         Top             =   3420
         Width           =   1815
      End
      Begin MSAdodcLib.Adodc adoContact 
         Height          =   330
         Left            =   120
         Top             =   6000
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoContact"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSComCtl2.DTPicker datAccountStart 
         Height          =   315
         Left            =   1320
         TabIndex        =   6
         Top             =   3780
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         Format          =   122355713
         CurrentDate     =   37870
      End
      Begin MSComCtl2.DTPicker datPostedToAccounts 
         Height          =   315
         Left            =   1320
         TabIndex        =   9
         Tag             =   "NOCHECK"
         Top             =   4500
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   556
         _Version        =   393216
         CheckBox        =   -1  'True
         Format          =   122355713
         CurrentDate     =   37870
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdContacts 
         Bindings        =   "frmCompany.frx":0A1D
         Height          =   3435
         Left            =   120
         TabIndex        =   21
         ToolTipText     =   "People who currently work for this company. Click the button in the email column to create a new email."
         Top             =   6000
         Width           =   9495
         _Version        =   196617
         BeveColorScheme =   1
         ForeColorEven   =   0
         BackColorOdd    =   16772351
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   2963
         Columns(0).Caption=   "Full Name"
         Columns(0).Name =   "name"
         Columns(0).DataField=   "name"
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   2408
         Columns(1).Caption=   "Telephone"
         Columns(1).Name =   "telephone"
         Columns(1).DataField=   "telephone"
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "contactID"
         Columns(2).Name =   "contactID"
         Columns(2).DataField=   "contactID"
         Columns(2).FieldLen=   256
         Columns(3).Width=   4233
         Columns(3).Caption=   "Email"
         Columns(3).Name =   "email"
         Columns(3).DataField=   "email"
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).Style=   1
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "employeeID"
         Columns(4).Name =   "employeeID"
         Columns(4).DataField=   "employeeID"
         Columns(4).FieldLen=   256
         _ExtentX        =   16748
         _ExtentY        =   6059
         _StockProps     =   79
         Caption         =   "Contact Information (Double-click to view/edit/delete)"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdMarketingHistory 
         Height          =   5775
         Left            =   -74820
         TabIndex        =   64
         ToolTipText     =   "People who currently work for this company"
         Top             =   1200
         Width           =   9435
         _Version        =   196617
         BeveColorScheme =   1
         AllowUpdate     =   0   'False
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   5
         Columns(0).Width=   1032
         Columns(0).Caption=   "markethistoryID"
         Columns(0).Name =   "markethistoryID"
         Columns(0).DataField=   "markethistoryID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   1535
         Columns(1).Caption=   "cdate"
         Columns(1).Name =   "cdate"
         Columns(1).DataField=   "cdate"
         Columns(1).FieldLen=   256
         Columns(2).Width=   1561
         Columns(2).Caption=   "cuser"
         Columns(2).Name =   "cuser"
         Columns(2).DataField=   "cuser"
         Columns(2).FieldLen=   256
         Columns(3).Width=   4710
         Columns(3).Caption=   "description"
         Columns(3).Name =   "description"
         Columns(3).DataField=   "description"
         Columns(3).FieldLen=   256
         Columns(4).Width=   820
         Columns(4).Caption=   "requiresattention"
         Columns(4).Name =   "requiresattention"
         Columns(4).DataField=   "requiresattention"
         Columns(4).FieldLen=   256
         _ExtentX        =   16642
         _ExtentY        =   10186
         _StockProps     =   79
         Caption         =   "Marketing History"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSAdodcLib.Adodc adoSupplierRates 
         Height          =   330
         Left            =   -74940
         Top             =   9360
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoSupplierRates"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdSupplierRates 
         Bindings        =   "frmCompany.frx":0A36
         Height          =   8775
         Left            =   -74880
         TabIndex        =   66
         ToolTipText     =   "Items which can later be selected from a drop down in the purchase order grid"
         Top             =   840
         Width           =   9435
         _Version        =   196617
         BeveColorScheme =   1
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   8
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "supplierrateID"
         Columns(0).Name =   "supplierrateID"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "supplierrateID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "companyID"
         Columns(1).Name =   "companyID"
         Columns(1).DataField=   "companyID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   2170
         Columns(2).Caption=   "Part Reference"
         Columns(2).Name =   "partreference"
         Columns(2).DataField=   "partreference"
         Columns(2).FieldLen=   256
         Columns(3).Width=   9551
         Columns(3).Caption=   "Description"
         Columns(3).Name =   "description"
         Columns(3).DataField=   "description"
         Columns(3).FieldLen=   256
         Columns(4).Width=   1270
         Columns(4).Caption=   "Price"
         Columns(4).Name =   "price0"
         Columns(4).Alignment=   1
         Columns(4).DataField=   "price0"
         Columns(4).DataType=   6
         Columns(4).NumberFormat=   "CURRENCY"
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "muser"
         Columns(5).Name =   "muser"
         Columns(5).DataField=   "muser"
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "mdate"
         Columns(6).Name =   "mdate"
         Columns(6).DataField=   "mdate"
         Columns(6).FieldLen=   256
         Columns(7).Width=   2170
         Columns(7).Caption=   "Nominal Code"
         Columns(7).Name =   "nominalcode"
         Columns(7).DataField=   "nominalcode"
         Columns(7).FieldLen=   256
         _ExtentX        =   16642
         _ExtentY        =   15478
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSAdodcLib.Adodc adoAssociatedCompanies 
         Height          =   330
         Left            =   -70320
         Top             =   1200
         Visible         =   0   'False
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoAssociatedCompanies"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdAssociatedCompanies 
         Bindings        =   "frmCompany.frx":0A55
         Height          =   4035
         Left            =   -74880
         TabIndex        =   85
         Top             =   540
         Width           =   9495
         _Version        =   196617
         AllowUpdate     =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   12648384
         RowHeight       =   423
         Columns(0).Width=   3200
         _ExtentX        =   16748
         _ExtentY        =   7117
         _StockProps     =   79
         Caption         =   "Associated Companies"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany2 
         Bindings        =   "frmCompany.frx":0A7A
         Height          =   315
         Left            =   -73680
         TabIndex        =   86
         ToolTipText     =   "Company Name"
         Top             =   4680
         Width           =   3075
         DataFieldList   =   "name"
         BevelType       =   0
         _Version        =   196617
         BorderStyle     =   0
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   5609
         Columns(0).Caption=   "Company Name"
         Columns(0).Name =   "name"
         Columns(0).DataField=   "name"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "companyID"
         Columns(1).Name =   "companyID"
         Columns(1).DataField=   "companyID"
         Columns(1).FieldLen=   256
         _ExtentX        =   5424
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "name"
      End
      Begin MSAdodcLib.Adodc adoCompany2 
         Height          =   330
         Left            =   -70860
         Top             =   5640
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   "cetasoft"
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoCompany2"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSAdodcLib.Adodc adoMessages 
         Height          =   330
         Left            =   -72600
         Top             =   8100
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoMessages"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnMessages 
         Bindings        =   "frmCompany.frx":0A94
         Height          =   735
         Left            =   -72060
         TabIndex        =   96
         Top             =   7620
         Width           =   2775
         DataFieldList   =   "trackermessage"
         _Version        =   196617
         ColumnHeaders   =   0   'False
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "trackermessageID"
         Columns(0).Name =   "trackermessageID"
         Columns(0).DataField=   "trackermessageID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   4419
         Columns(1).Caption=   "trackermessage"
         Columns(1).Name =   "trackermessage"
         Columns(1).DataField=   "trackermessage"
         Columns(1).FieldLen=   256
         _ExtentX        =   4895
         _ExtentY        =   1296
         _StockProps     =   77
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnTrackerFields 
         Height          =   1335
         Left            =   -74220
         TabIndex        =   99
         Top             =   2040
         Width           =   3735
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   3200
         Columns(0).Caption=   "Column 0 "
         Columns(0).Name =   "Column 0 "
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   6588
         _ExtentY        =   2355
         _StockProps     =   77
      End
      Begin MSAdodcLib.Adodc adoTrackerChoices 
         Height          =   330
         Left            =   -74580
         Top             =   840
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   "cetasoft"
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoTrackerChoices"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdTrackerChoices 
         Bindings        =   "frmCompany.frx":0AAE
         Height          =   4275
         Left            =   -74820
         TabIndex        =   100
         Top             =   840
         Width           =   19395
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         SelectTypeRow   =   3
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   32
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "trackeritemchoiceID"
         Columns(0).Name =   "trackeritemchoiceID"
         Columns(0).DataField=   "trackeritemchoiceID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "companyID"
         Columns(1).Name =   "companyID"
         Columns(1).DataField=   "companyID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3519
         Columns(2).Caption=   "Field"
         Columns(2).Name =   "itemfield"
         Columns(2).DataField=   "itemfield"
         Columns(2).FieldLen=   256
         Columns(3).Width=   4948
         Columns(3).Caption=   "Heading"
         Columns(3).Name =   "itemheading"
         Columns(3).DataField=   "itemheading"
         Columns(3).FieldLen=   256
         Columns(4).Width=   873
         Columns(4).Caption=   "St1?"
         Columns(4).Name =   "stage1conclusion"
         Columns(4).DataField=   "stage1conclusion"
         Columns(4).FieldLen=   256
         Columns(4).Style=   2
         Columns(5).Width=   873
         Columns(5).Caption=   "!St1?"
         Columns(5).Name =   "stage1NOTconclusion"
         Columns(5).DataField=   "stage1NOTconclusion"
         Columns(5).FieldLen=   256
         Columns(5).Style=   2
         Columns(6).Width=   873
         Columns(6).Caption=   "St2?"
         Columns(6).Name =   "stage2conclusion"
         Columns(6).DataField=   "stage2conclusion"
         Columns(6).FieldLen=   256
         Columns(6).Style=   2
         Columns(7).Width=   873
         Columns(7).Caption=   "!St2?"
         Columns(7).Name =   "stage2NOTconclusion"
         Columns(7).DataField=   "stage2NOTconclusion"
         Columns(7).FieldLen=   256
         Columns(7).Style=   2
         Columns(8).Width=   873
         Columns(8).Caption=   "St3?"
         Columns(8).Name =   "stage3conclusion"
         Columns(8).DataField=   "stage3conclusion"
         Columns(8).FieldLen=   256
         Columns(8).Style=   2
         Columns(9).Width=   873
         Columns(9).Caption=   "!St3?"
         Columns(9).Name =   "stage3NOTconclusion"
         Columns(9).DataField=   "stage3NOTconclusion"
         Columns(9).FieldLen=   256
         Columns(9).Style=   2
         Columns(10).Width=   873
         Columns(10).Caption=   "St4?"
         Columns(10).Name=   "stage4conclusion"
         Columns(10).DataField=   "stage4conclusion"
         Columns(10).FieldLen=   256
         Columns(10).Style=   2
         Columns(11).Width=   873
         Columns(11).Caption=   "!St4?"
         Columns(11).Name=   "stage4NOTconclusion"
         Columns(11).DataField=   "stage4NOTconclusion"
         Columns(11).FieldLen=   256
         Columns(11).Style=   2
         Columns(12).Width=   873
         Columns(12).Caption=   "St5?"
         Columns(12).Name=   "stage5conclusion"
         Columns(12).DataField=   "stage5conclusion"
         Columns(12).FieldLen=   256
         Columns(12).Style=   2
         Columns(13).Width=   873
         Columns(13).Caption=   "!St5?"
         Columns(13).Name=   "stage5NOTconclusion"
         Columns(13).DataField=   "stage5NOTconclusion"
         Columns(13).FieldLen=   256
         Columns(13).Style=   2
         Columns(14).Width=   1323
         Columns(14).Caption=   "Finshed?"
         Columns(14).Name=   "conclusion"
         Columns(14).DataField=   "conclusion"
         Columns(14).FieldLen=   256
         Columns(14).Style=   2
         Columns(15).Width=   1984
         Columns(15).Caption=   "Charge Code"
         Columns(15).Name=   "billingcode"
         Columns(15).DataField=   "billingcode"
         Columns(15).FieldLen=   256
         Columns(16).Width=   820
         Columns(16).Caption=   "Unit"
         Columns(16).Name=   "minutebilling"
         Columns(16).DataField=   "minutebilling"
         Columns(16).FieldLen=   256
         Columns(17).Width=   661
         Columns(17).Caption=   "Col"
         Columns(17).Name=   "field_order"
         Columns(17).DataField=   "field_order"
         Columns(17).FieldLen=   256
         Columns(18).Width=   2011
         Columns(18).Caption=   "Column Width"
         Columns(18).Name=   "itemwidth"
         Columns(18).DataField=   "itemwidth"
         Columns(18).FieldLen=   256
         Columns(19).Width=   1508
         Columns(19).Caption=   "dash grid"
         Columns(19).Name=   "dash_grid"
         Columns(19).DataField=   "dash_grid"
         Columns(19).FieldLen=   256
         Columns(19).Style=   2
         Columns(20).Width=   1588
         Columns(20).Caption=   "dash excel"
         Columns(20).Name=   "dash_excel"
         Columns(20).DataField=   "dash_excel"
         Columns(20).FieldLen=   256
         Columns(20).Style=   2
         Columns(21).Width=   1667
         Columns(21).Caption=   "dash_chart"
         Columns(21).Name=   "dash_chart"
         Columns(21).DataField=   "dash_chart"
         Columns(21).DataType=   17
         Columns(21).FieldLen=   256
         Columns(21).Style=   2
         Columns(22).Width=   1588
         Columns(22).Caption=   "dash_filter"
         Columns(22).Name=   "dash_filter"
         Columns(22).DataField=   "dash_filter"
         Columns(22).FieldLen=   256
         Columns(22).Style=   2
         Columns(23).Width=   1588
         Columns(23).Caption=   "dash_total"
         Columns(23).Name=   "dash_total"
         Columns(23).DataField=   "dash_total"
         Columns(23).FieldLen=   256
         Columns(23).Style=   2
         Columns(24).Width=   2143
         Columns(24).Caption=   "dash_pctdone"
         Columns(24).Name=   "dash_pctdone"
         Columns(24).DataField=   "dash_pctdone"
         Columns(24).FieldLen=   256
         Columns(25).Width=   1931
         Columns(25).Caption=   "dash_format"
         Columns(25).Name=   "dash_format"
         Columns(25).DataField=   "dash_format"
         Columns(25).FieldLen=   256
         Columns(26).Width=   1323
         Columns(26).Caption=   "mw_grid"
         Columns(26).Name=   "mw_grid"
         Columns(26).DataField=   "mw_grid"
         Columns(26).FieldLen=   256
         Columns(26).Style=   2
         Columns(27).Width=   1508
         Columns(27).Caption=   "mw_excel"
         Columns(27).Name=   "mw_excel"
         Columns(27).DataField=   "mw_excel"
         Columns(27).FieldLen=   256
         Columns(27).Style=   2
         Columns(28).Width=   1508
         Columns(28).Caption=   "mw_chart"
         Columns(28).Name=   "mw_chart"
         Columns(28).DataField=   "mw_chart"
         Columns(28).FieldLen=   256
         Columns(28).Style=   2
         Columns(29).Width=   1402
         Columns(29).Caption=   "mw_filter"
         Columns(29).Name=   "mw_filter"
         Columns(29).DataField=   "mw_filter"
         Columns(29).FieldLen=   256
         Columns(29).Style=   2
         Columns(30).Width=   1402
         Columns(30).Caption=   "mw_total"
         Columns(30).Name=   "mw_total"
         Columns(30).DataField=   "mw_total"
         Columns(30).FieldLen=   256
         Columns(30).Style=   2
         Columns(31).Width=   1349
         Columns(31).Caption=   "inc_notif"
         Columns(31).Name=   "use_in_notifications"
         Columns(31).DataField=   "use_in_notifications"
         Columns(31).FieldLen=   256
         Columns(31).Style=   2
         _ExtentX        =   34211
         _ExtentY        =   7541
         _StockProps     =   79
         Caption         =   "Generic Tracker Configurations"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSAdodcLib.Adodc adoTrackerNotifications 
         Height          =   330
         Left            =   -74640
         Top             =   6600
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoTrackerNotifications"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdTrackerNotifications 
         Bindings        =   "frmCompany.frx":0ACE
         Height          =   3435
         Left            =   -74820
         TabIndex        =   101
         Top             =   6180
         Width           =   10875
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   6
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "trackernotificationID"
         Columns(0).Name =   "trackernotificationID"
         Columns(0).DataField=   "trackernotificationID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "contractgroup"
         Columns(1).Name =   "contractgroup"
         Columns(1).DataField=   "contractgroup"
         Columns(1).FieldLen=   256
         Columns(2).Width=   5292
         Columns(2).Caption=   "E-mail Address"
         Columns(2).Name =   "email"
         Columns(2).DataField=   "email"
         Columns(2).FieldLen=   256
         Columns(3).Width=   5292
         Columns(3).Caption=   "Full Name"
         Columns(3).Name =   "fullname"
         Columns(3).DataField=   "fullname"
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "ID"
         Columns(4).Name =   "trackermessageID"
         Columns(4).DataField=   "trackermessageID"
         Columns(4).FieldLen=   256
         Columns(5).Width=   7408
         Columns(5).Caption=   "Message"
         Columns(5).Name =   "Message"
         Columns(5).FieldLen=   256
         _ExtentX        =   19182
         _ExtentY        =   6059
         _StockProps     =   79
         Caption         =   "Contract Work - Tracker Email Notifications"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSAdodcLib.Adodc adoPortalTitleStubs 
         Height          =   330
         Left            =   -74760
         Top             =   8880
         Visible         =   0   'False
         Width           =   2685
         _ExtentX        =   4736
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   "cetasoft"
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoPortalTitleStubs"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdPortalTitleStubs 
         Bindings        =   "frmCompany.frx":0AF4
         Height          =   1035
         Left            =   -74760
         TabIndex        =   108
         Top             =   8880
         Width           =   3405
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   4
         Columns(0).Width=   3757
         Columns(0).Caption=   "Filename Stub"
         Columns(0).Name =   "description"
         Columns(0).DataField=   "description"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "information"
         Columns(1).Name =   "information"
         Columns(1).DataField=   "information"
         Columns(1).FieldLen=   256
         Columns(2).Width=   1111
         Columns(2).Caption=   "forder"
         Columns(2).Name =   "forder"
         Columns(2).DataField=   "forder"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "category"
         Columns(3).Name =   "category"
         Columns(3).DataField=   "category"
         Columns(3).FieldLen=   256
         _ExtentX        =   6006
         _ExtentY        =   1826
         _StockProps     =   79
         Caption         =   "Portal Title Stubs"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdCompanyContacts 
         Bindings        =   "frmCompany.frx":0B16
         Height          =   9255
         Left            =   -74880
         TabIndex        =   139
         Top             =   540
         Width           =   9495
         _Version        =   196617
         BeveColorScheme =   1
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeRow   =   3
         ForeColorEven   =   0
         BackColorOdd    =   16772351
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "companycontactID"
         Columns(0).Name =   "companycontactID"
         Columns(0).DataField=   "companycontactID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   7646
         Columns(1).Caption=   "Full Name"
         Columns(1).Name =   "name"
         Columns(1).DataField=   "name"
         Columns(1).FieldLen=   256
         Columns(2).Width=   7938
         Columns(2).Caption=   "Email"
         Columns(2).Name =   "email"
         Columns(2).DataField=   "email"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "companyID"
         Columns(3).Name =   "companyID"
         Columns(3).DataField=   "companyID"
         Columns(3).FieldLen=   256
         _ExtentX        =   16748
         _ExtentY        =   16325
         _StockProps     =   79
         Caption         =   "Customer Contact Information (for delivery notifications etc.)"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdMaterialTrackerChoices 
         Bindings        =   "frmCompany.frx":0B36
         Height          =   8835
         Left            =   -74820
         TabIndex        =   166
         Top             =   960
         Width           =   9375
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         SelectTypeRow   =   3
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   10
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "trackeritemchoiceID"
         Columns(0).Name =   "trackeritemchoiceID"
         Columns(0).DataField=   "trackeritemchoiceID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "companyID"
         Columns(1).Name =   "companyID"
         Columns(1).DataField=   "companyID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3519
         Columns(2).Caption=   "Field"
         Columns(2).Name =   "itemfield"
         Columns(2).DataField=   "itemfield"
         Columns(2).FieldLen=   256
         Columns(3).Width=   4948
         Columns(3).Caption=   "Heading"
         Columns(3).Name =   "itemheading"
         Columns(3).DataField=   "itemheading"
         Columns(3).FieldLen=   256
         Columns(4).Width=   873
         Columns(4).Caption=   "St1?"
         Columns(4).Name =   "stage1conclusion"
         Columns(4).DataField=   "stage1conclusion"
         Columns(4).FieldLen=   256
         Columns(4).Style=   2
         Columns(5).Width=   873
         Columns(5).Caption=   "St2?"
         Columns(5).Name =   "stage2conclusion"
         Columns(5).DataField=   "stage2conclusion"
         Columns(5).FieldLen=   256
         Columns(5).Style=   2
         Columns(6).Width=   873
         Columns(6).Caption=   "St3?"
         Columns(6).Name =   "stage3conclusion"
         Columns(6).DataField=   "stage3conclusion"
         Columns(6).FieldLen=   256
         Columns(6).Style=   2
         Columns(7).Width=   1323
         Columns(7).Caption=   "Finshed?"
         Columns(7).Name =   "conclusion"
         Columns(7).DataField=   "conclusion"
         Columns(7).FieldLen=   256
         Columns(7).Style=   2
         Columns(8).Width=   2170
         Columns(8).Caption=   "Charge Code"
         Columns(8).Name =   "billingcode"
         Columns(8).DataField=   "billingcode"
         Columns(8).FieldLen=   256
         Columns(9).Width=   820
         Columns(9).Caption=   "Unit"
         Columns(9).Name =   "minutebilling"
         Columns(9).DataField=   "minutebilling"
         Columns(9).FieldLen=   256
         _ExtentX        =   16536
         _ExtentY        =   15584
         _StockProps     =   79
         Caption         =   "Material Tracker Configurations"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblCaption 
         Caption         =   "MediaPulse JobID"
         Height          =   255
         Index           =   86
         Left            =   3420
         TabIndex        =   241
         Top             =   4560
         Width           =   1695
      End
      Begin VB.Label lblCaption 
         Caption         =   "MediaPulse CompanyID"
         Height          =   255
         Index           =   92
         Left            =   3420
         TabIndex        =   239
         Top             =   4200
         Width           =   1695
      End
      Begin VB.Label lblCaption 
         Caption         =   "AS11  Billing CompanyID"
         Height          =   255
         Index           =   91
         Left            =   -66240
         TabIndex        =   236
         Top             =   480
         Width           =   1935
      End
      Begin VB.Label lblCaption 
         Caption         =   "Sage Code"
         Height          =   255
         Index           =   23
         Left            =   3420
         TabIndex        =   234
         Top             =   3840
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "MX1360 Proxy Transcode ID"
         Height          =   255
         Index           =   90
         Left            =   -74820
         TabIndex        =   232
         Top             =   1680
         Width           =   2115
      End
      Begin VB.Label lblCaption 
         Caption         =   "MX1360 password"
         Height          =   255
         Index           =   89
         Left            =   -74820
         TabIndex        =   231
         Top             =   1320
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "MX1360 Username"
         Height          =   255
         Index           =   88
         Left            =   -74820
         TabIndex        =   230
         Top             =   960
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "MX1360 Customer ID"
         Height          =   255
         Index           =   87
         Left            =   -74820
         TabIndex        =   229
         Top             =   600
         Width           =   1695
      End
      Begin VB.Label lblCaption 
         Caption         =   "Upload Code"
         Height          =   255
         Index           =   85
         Left            =   -66660
         TabIndex        =   224
         Top             =   2100
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "DT Billing CompanyID"
         Height          =   255
         Index           =   83
         Left            =   -68820
         TabIndex        =   170
         Top             =   480
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Download Code"
         Height          =   255
         Index           =   57
         Left            =   -66660
         TabIndex        =   165
         Top             =   1800
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "Content Delivery Password Lifetime"
         Height          =   255
         Index           =   81
         Left            =   -66660
         TabIndex        =   163
         Top             =   420
         Width           =   3135
      End
      Begin VB.Label lblCaption 
         Caption         =   "Content Delivery Pwd Regular Expression"
         Height          =   255
         Index           =   79
         Left            =   -74880
         TabIndex        =   161
         Top             =   420
         Width           =   3195
      End
      Begin VB.Label lblCaption 
         Caption         =   "Content Delivery Pwd Security Explanation"
         Height          =   255
         Index           =   80
         Left            =   -71580
         TabIndex        =   160
         Top             =   420
         Width           =   3495
      End
      Begin VB.Label lblCaption 
         Caption         =   "Content Delivery Admin Text 3"
         Height          =   255
         Index           =   78
         Left            =   -74820
         TabIndex        =   157
         Top             =   7860
         Width           =   2355
      End
      Begin VB.Label lblCaption 
         Caption         =   "Content Delivery Admin Title 3"
         Height          =   255
         Index           =   77
         Left            =   -74820
         TabIndex        =   156
         Top             =   7560
         Width           =   2355
      End
      Begin VB.Label lblCaption 
         Caption         =   "Content Delivery Admin Link 3"
         Height          =   255
         Index           =   76
         Left            =   -74820
         TabIndex        =   155
         Top             =   7260
         Width           =   2355
      End
      Begin VB.Label lblCaption 
         Caption         =   "Content Delivery Admin Text 2"
         Height          =   255
         Index           =   75
         Left            =   -74820
         TabIndex        =   154
         Top             =   6660
         Width           =   2355
      End
      Begin VB.Label lblCaption 
         Caption         =   "Content Delivery Admin Title 2"
         Height          =   255
         Index           =   74
         Left            =   -74820
         TabIndex        =   153
         Top             =   6360
         Width           =   2355
      End
      Begin VB.Label lblCaption 
         Caption         =   "Content Delivery Admin Link 2"
         Height          =   255
         Index           =   73
         Left            =   -74820
         TabIndex        =   152
         Top             =   6060
         Width           =   2355
      End
      Begin VB.Label lblCaption 
         Caption         =   "Content Delivery Admin Text 1"
         Height          =   255
         Index           =   72
         Left            =   -74820
         TabIndex        =   151
         Top             =   5460
         Width           =   2355
      End
      Begin VB.Label lblCaption 
         Caption         =   "Content Delivery Admin Title 1"
         Height          =   255
         Index           =   71
         Left            =   -74820
         TabIndex        =   150
         Top             =   5160
         Width           =   2355
      End
      Begin VB.Label lblCaption 
         Caption         =   "Content Delivery Admin Link 1"
         Height          =   255
         Index           =   70
         Left            =   -74820
         TabIndex        =   149
         Top             =   4860
         Width           =   2355
      End
      Begin VB.Label lblCaption 
         Caption         =   "Company Name on Content Delivery Site"
         Height          =   255
         Index           =   69
         Left            =   -74820
         TabIndex        =   138
         Top             =   4560
         Width           =   3195
      End
      Begin VB.Label lblCaption 
         Caption         =   "MX1 Contact Email (group)"
         Height          =   255
         Index           =   62
         Left            =   -74820
         TabIndex        =   136
         Top             =   4260
         Width           =   2355
      End
      Begin VB.Label lblCaption 
         Caption         =   "Branding Footer"
         Height          =   255
         Index           =   63
         Left            =   -74820
         TabIndex        =   135
         Top             =   2100
         Width           =   2055
      End
      Begin VB.Label lblCaption 
         Caption         =   "Email Intr Subj"
         Height          =   255
         Index           =   61
         Left            =   -74820
         TabIndex        =   134
         Top             =   3000
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Content Delivery Users Main Page"
         Height          =   255
         Index           =   60
         Left            =   -74820
         TabIndex        =   133
         Top             =   8460
         Width           =   3135
      End
      Begin VB.Label lblCaption 
         Caption         =   "User Site URL"
         Height          =   255
         Index           =   59
         Left            =   -74820
         TabIndex        =   132
         Top             =   2400
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Branding Header"
         Height          =   315
         Index           =   58
         Left            =   -74820
         TabIndex        =   131
         Top             =   1800
         Width           =   1755
      End
      Begin VB.Label lblCaption 
         Caption         =   "Field 6"
         Height          =   255
         Index           =   56
         Left            =   -68100
         TabIndex        =   130
         Top             =   9600
         Width           =   555
      End
      Begin VB.Label lblCaption 
         Caption         =   "Field 4"
         Height          =   255
         Index           =   55
         Left            =   -68100
         TabIndex        =   129
         Top             =   9300
         Width           =   675
      End
      Begin VB.Label lblCaption 
         Caption         =   "Field 2"
         Height          =   255
         Index           =   54
         Left            =   -68100
         TabIndex        =   128
         Top             =   9000
         Width           =   615
      End
      Begin VB.Label lblCaption 
         Caption         =   "Field 5"
         Height          =   255
         Index           =   53
         Left            =   -70980
         TabIndex        =   127
         Top             =   9600
         Width           =   555
      End
      Begin VB.Label lblCaption 
         Caption         =   "Field 3"
         Height          =   255
         Index           =   52
         Left            =   -70980
         TabIndex        =   126
         Top             =   9300
         Width           =   555
      End
      Begin VB.Label lblCaption 
         Caption         =   "Field 1"
         Height          =   255
         Index           =   51
         Left            =   -70980
         TabIndex        =   125
         Top             =   9000
         Width           =   555
      End
      Begin VB.Label lblCaption 
         Caption         =   "Email Intr Body"
         Height          =   255
         Index           =   66
         Left            =   -74820
         TabIndex        =   124
         Top             =   3300
         Width           =   1515
      End
      Begin VB.Label lblCaption 
         Caption         =   "Welcome Message"
         Height          =   255
         Index           =   67
         Left            =   -74820
         TabIndex        =   123
         Top             =   3780
         Width           =   1515
      End
      Begin VB.Label lblCaption 
         Caption         =   "Order Number Field Name"
         Height          =   255
         Index           =   68
         Left            =   -74820
         TabIndex        =   122
         Top             =   2700
         Width           =   1935
      End
      Begin VB.Label lblCaption 
         Caption         =   "Generic Tracker Title"
         Height          =   255
         Index           =   65
         Left            =   -74820
         TabIndex        =   103
         Top             =   480
         Width           =   1935
      End
      Begin VB.Label lblCaption 
         Caption         =   "Progress Tracker Ref Field Name"
         Height          =   255
         Index           =   64
         Left            =   -62700
         TabIndex        =   102
         Top             =   480
         Width           =   2355
      End
      Begin VB.Label lblCaption 
         Caption         =   "Association"
         Height          =   255
         Index           =   44
         Left            =   -74880
         TabIndex        =   95
         Top             =   5160
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Comments"
         Height          =   255
         Index           =   45
         Left            =   -74880
         TabIndex        =   94
         Top             =   5580
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Company"
         Height          =   255
         Index           =   46
         Left            =   -74880
         TabIndex        =   93
         Top             =   4740
         Width           =   1035
      End
      Begin VB.Label lblAssociationCompanyID 
         Appearance      =   0  'Flat
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   315
         Left            =   -70500
         TabIndex        =   92
         Top             =   6840
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.Label lblAssociationID 
         Appearance      =   0  'Flat
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   315
         Left            =   -70500
         TabIndex        =   91
         Top             =   6360
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Quotes"
         Enabled         =   0   'False
         Height          =   255
         Index           =   47
         Left            =   -74880
         TabIndex        =   90
         Top             =   6180
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Projects"
         Enabled         =   0   'False
         Height          =   255
         Index           =   48
         Left            =   -74880
         TabIndex        =   89
         Top             =   6540
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Jobs"
         Enabled         =   0   'False
         Height          =   255
         Index           =   49
         Left            =   -74880
         TabIndex        =   88
         Top             =   6900
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Costs"
         Enabled         =   0   'False
         Height          =   255
         Index           =   50
         Left            =   -74880
         TabIndex        =   87
         Top             =   7260
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Bookings Alert"
         Height          =   255
         Index           =   38
         Left            =   -74880
         TabIndex        =   71
         Top             =   420
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Costing Alert"
         Height          =   255
         Index           =   39
         Left            =   -74880
         TabIndex        =   70
         Top             =   2760
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "These rates are for suppliers - not for your own sales rates for this customer."
         Height          =   255
         Index           =   43
         Left            =   -74880
         TabIndex        =   67
         Top             =   540
         Width           =   6135
      End
      Begin VB.Label lblCaption 
         Caption         =   $"frmCompany.frx":0B5E
         Height          =   615
         Index           =   40
         Left            =   -74820
         TabIndex        =   65
         Top             =   480
         Width           =   6135
      End
      Begin VB.Label lblCaption 
         Caption         =   "Producers"
         Height          =   255
         Index           =   29
         Left            =   -74820
         TabIndex        =   59
         Top             =   4680
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Despatch"
         Height          =   255
         Index           =   30
         Left            =   -74820
         TabIndex        =   58
         Top             =   3360
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Office / Client"
         Height          =   255
         Index           =   31
         Left            =   -74820
         TabIndex        =   57
         Top             =   780
         Width           =   5235
      End
      Begin VB.Label lblCaption 
         Caption         =   "Operator"
         Height          =   255
         Index           =   32
         Left            =   -74820
         TabIndex        =   56
         Top             =   2100
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Accounts Notes"
         Height          =   195
         Index           =   33
         Left            =   -74820
         TabIndex        =   55
         Top             =   6000
         Width           =   1995
      End
      Begin VB.Label lblCaption 
         Caption         =   "These notes are copied to the relevant sections when new jobs are created."
         Height          =   255
         Index           =   34
         Left            =   -74820
         TabIndex        =   54
         Top             =   480
         Width           =   6135
      End
      Begin VB.Label lblCaption 
         Caption         =   "Discount %"
         Height          =   255
         Index           =   42
         Left            =   120
         TabIndex        =   48
         Top             =   5280
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Credit Limit"
         Height          =   255
         Index           =   41
         Left            =   120
         TabIndex        =   46
         Top             =   5640
         Width           =   1095
      End
      Begin VB.Label lblCaption 
         Caption         =   "By"
         Height          =   255
         Index           =   37
         Left            =   3420
         TabIndex        =   42
         Top             =   3120
         Width           =   255
      End
      Begin VB.Label lblCaption 
         Caption         =   "Modified Date"
         Height          =   255
         Index           =   36
         Left            =   120
         TabIndex        =   40
         Top             =   3120
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "By"
         Height          =   255
         Index           =   35
         Left            =   3420
         TabIndex        =   38
         Top             =   2760
         Width           =   255
      End
      Begin VB.Label lblCaption 
         Caption         =   "VAT Code"
         Height          =   255
         Index           =   13
         Left            =   3420
         TabIndex        =   32
         Top             =   5280
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "VAT Number"
         Height          =   255
         Index           =   14
         Left            =   3420
         TabIndex        =   31
         Top             =   5640
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Created Date"
         Height          =   255
         Index           =   15
         Left            =   120
         TabIndex        =   30
         Top             =   2760
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "CETA Code"
         Height          =   255
         Index           =   16
         Left            =   120
         TabIndex        =   29
         Top             =   420
         Width           =   1695
      End
      Begin VB.Label lblCaption 
         Caption         =   "A/C Code"
         Height          =   255
         Index           =   17
         Left            =   120
         TabIndex        =   28
         Top             =   3480
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "A/C Start"
         Height          =   255
         Index           =   18
         Left            =   120
         TabIndex        =   27
         Top             =   3840
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "A/C Status"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   19
         Left            =   120
         TabIndex        =   26
         Top             =   4200
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "A/C Posted"
         Height          =   255
         Index           =   20
         Left            =   120
         TabIndex        =   25
         Top             =   4560
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Reg Office"
         Height          =   255
         Index           =   21
         Left            =   3420
         TabIndex        =   24
         Top             =   4920
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Ratecard"
         Height          =   255
         Index           =   22
         Left            =   120
         TabIndex        =   23
         Top             =   4920
         Width           =   675
      End
      Begin VB.Label lblCaption 
         Caption         =   "S/L Code"
         Height          =   255
         Index           =   24
         Left            =   3420
         TabIndex        =   22
         Top             =   3480
         Width           =   1035
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filter search..."
      Height          =   1035
      Left            =   60
      TabIndex        =   33
      Top             =   60
      Width           =   3615
      Begin VB.ComboBox cmbShowAccountStatus 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         CausesValidation=   0   'False
         Height          =   315
         ItemData        =   "frmCompany.frx":0C19
         Left            =   180
         List            =   "frmCompany.frx":0C1B
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   43
         Tag             =   "NOCLEAR"
         Top             =   600
         Width           =   1335
      End
      Begin VB.CheckBox chkShow 
         Caption         =   "Prospects"
         Height          =   195
         Index           =   2
         Left            =   2400
         TabIndex        =   36
         ToolTipText     =   "This Company is a Marketing Prospect"
         Top             =   300
         Width           =   1185
      End
      Begin VB.CheckBox chkShow 
         Caption         =   "Suppliers"
         Height          =   195
         Index           =   1
         Left            =   1200
         TabIndex        =   35
         ToolTipText     =   "This Company is a Supplier"
         Top             =   300
         Width           =   1020
      End
      Begin VB.CheckBox chkShow 
         Caption         =   "Clients"
         Height          =   195
         Index           =   0
         Left            =   180
         TabIndex        =   34
         ToolTipText     =   "This Company is a Client"
         Top             =   300
         Width           =   960
      End
   End
   Begin VB.CommandButton cmdLaunch 
      Appearance      =   0  'Flat
      Height          =   300
      Index           =   2
      Left            =   7920
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmCompany.frx":0C1D
      Style           =   1  'Graphical
      TabIndex        =   15
      ToolTipText     =   "Show on-line map (using multimap)"
      Top             =   1800
      UseMaskColor    =   -1  'True
      Width           =   300
   End
   Begin VB.CommandButton cmdLaunch 
      Appearance      =   0  'Flat
      Height          =   300
      Index           =   0
      Left            =   7920
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmCompany.frx":102F
      Style           =   1  'Graphical
      TabIndex        =   16
      ToolTipText     =   "Send email"
      Top             =   3240
      UseMaskColor    =   -1  'True
      Width           =   300
   End
   Begin VB.CommandButton cmdLaunch 
      Appearance      =   0  'Flat
      Height          =   300
      Index           =   1
      Left            =   7920
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmCompany.frx":1467
      Style           =   1  'Graphical
      TabIndex        =   17
      ToolTipText     =   "Visit website"
      Top             =   3600
      UseMaskColor    =   -1  'True
      Width           =   300
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdCompanyList 
      Bindings        =   "frmCompany.frx":189A
      Height          =   8715
      Left            =   480
      TabIndex        =   14
      Top             =   1140
      Width           =   3195
      _Version        =   196617
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      SelectByCell    =   -1  'True
      ForeColorEven   =   -2147483630
      ForeColorOdd    =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "companyID"
      Columns(0).Name =   "companyID"
      Columns(0).DataField=   "companyID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   5847
      Columns(1).Caption=   "name"
      Columns(1).Name =   "name"
      Columns(1).DataField=   "name"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "postcode"
      Columns(2).Name =   "postcode"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   5636
      _ExtentY        =   15372
      _StockProps     =   79
      BackColor       =   -2147483643
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.TabStrip tabCompanySearch 
      Height          =   7935
      Left            =   60
      TabIndex        =   18
      Top             =   1140
      Width           =   435
      _ExtentX        =   767
      _ExtentY        =   13996
      MultiRow        =   -1  'True
      Placement       =   2
      TabMinWidth     =   353
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   28
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "A"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "B"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "C"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab4 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "D"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab5 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "E"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab6 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "F"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab7 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "G"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab8 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "H"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab9 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "I"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab10 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "J"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab11 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "K"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab12 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "L"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab13 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "M"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab14 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "N"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab15 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "O"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab16 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "P"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab17 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Q"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab18 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "R"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab19 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "S"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab20 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "T"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab21 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "U"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab22 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "V"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab23 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "W"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab24 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "X"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab25 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Y"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab26 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Z"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab27 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "#"
            Object.ToolTipText     =   "Show items which begin with numbers"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab28 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "*"
            Object.ToolTipText     =   "Show all items"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.Label lblEscape 
      Height          =   195
      Left            =   11820
      TabIndex        =   44
      Top             =   1380
      Width           =   375
   End
End
Attribute VB_Name = "frmCompany"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Function SearchForCompany(lp_strField As String, lp_strCriterea As String)
adoCompany.RecordSource = "SELECT * FROM company WHERE " & lp_strField & " LIKE '" & lp_strCriterea & "%' ORDER BY " & lp_strField
adoCompany.Refresh
cmbCompany.DroppedDown = True
End Function

Private Sub chkShow_Click(Index As Integer)

If tabCompanySearch.SelectedItem.Caption = "#" Then Exit Sub

If adoCompany.Recordset.Filter = 0 Or adoCompany.Recordset.Filter = "" Then
    adoCompany.Recordset.Filter = "companyID > 0"
End If

Dim l_intfilter As Integer
If chkShow(0).Value = 1 Then adoCompany.Recordset.Filter = adoCompany.Recordset.Filter & " AND iscustomer = 1": l_intfilter = 1
If chkShow(1).Value = 1 Then adoCompany.Recordset.Filter = adoCompany.Recordset.Filter & " AND issupplier = 1": l_intfilter = 1
If chkShow(2).Value = 1 Then adoCompany.Recordset.Filter = adoCompany.Recordset.Filter & " AND isprospective = 1": l_intfilter = 1

If l_intfilter = 0 Then tabCompanySearch_Click

End Sub

Private Sub cmbAccountStatus_KeyPress(KeyAscii As Integer)
AutoMatch cmbAccountStatus, KeyAscii
End Sub

Private Sub cmbAssociationType_GotFocus()
PopulateCombo "association", cmbAssociationType
End Sub

Private Sub cmbCompany_CloseUp()

ShowCompany Val(cmbCompany.Columns(1).Text)

End Sub

Private Sub cmbCompany2_Click()
lblAssociationCompanyID.Caption = cmbCompany2.Columns("companyID").Text
End Sub

Private Sub cmbShowAccountStatus_Click()
If adoCompany.Recordset.Filter = 0 Or adoCompany.Recordset.Filter = "" Then
    adoCompany.Recordset.Filter = "companyID > 0"
End If

If cmbShowAccountStatus.Text = "ALL" Then
    adoCompany.Recordset.Filter = ""
    chkShow(0).Value = 0
    chkShow(1).Value = 0
    chkShow(2).Value = 0
Else
    On Error Resume Next
    adoCompany.Recordset.Filter = adoCompany.Recordset.Filter & " AND accountstatus = '" & cmbShowAccountStatus.Text & "'"
End If

End Sub

Private Sub cmdAddAssociation_Click()

Dim l_lngAssociationID As Long
l_lngAssociationID = SaveAssociation(Val(lblAssociationID.Caption), "company", Val(txtCompanyID.Text), CStr(cmbAssociationType.Text), Val(lblAssociationCompanyID.Caption), CStr(txtAssociationComment.Text))

lblAssociationID.Caption = l_lngAssociationID

If l_lngAssociationID <> 0 Then adoAssociatedCompanies.Refresh

End Sub

Private Sub cmdAddNewContact_Click()

If Val(txtCompanyID.Text) = 0 Then
    NoCompanySelectedMessage
    Exit Sub
End If

ShowContact -1, Val(txtCompanyID.Text)
End Sub

Private Sub cmdClear_Click()
ShowCompany -1

'check to see if a search has been applied

Dim l_strSQL As String
l_strSQL = "SELECT name, companyID, postcode, iscustomer, issupplier, isprospective, accountstatus, system_active FROM company ORDER BY name"

If adoCompany.RecordSource = l_strSQL Then Exit Sub

adoCompany.ConnectionString = g_strConnection
adoCompany.RecordSource = l_strSQL
adoCompany.Refresh

End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdDelete_Click()
If DeleteCompany(Val(txtCompanyID.Text)) Then cmdClear_Click
End Sub

Private Sub cmdLaunch_Click(Index As Integer)
Select Case Index
Case 1
    OpenBrowser txtWebsite.Text

Case 0
    OpenBrowser "mailto:" & txtEmail.Text
    
Case 2
    Dim l_strSearch  As String
    l_strSearch = "http://www.multimap.com/map/places.cgi?db=pc&place=" & Replace(txtPostCode.Text, " ", "")
    OpenBrowser l_strSearch

End Select
End Sub

Private Sub cmdMediaPortalUsers_Click()

If txtCompanyID.Text <> "" Then

    frmClipPortalUsers.cmbCompany.Text = cmbCompany.Text
    frmClipPortalUsers.lblCompanyID.Caption = txtCompanyID.Text
    
    frmClipPortalUsers.Show
    frmClipPortalUsers.ZOrder 0
    
End If

End Sub

Private Sub cmdNewAssociation_Click()

cmbAssociationType.Text = ""
cmbCompany2.Text = ""
lblAssociationCompanyID.Caption = ""
lblAssociationID.Caption = ""
txtAssociationComment.Text = ""

End Sub

Private Sub cmdRateCard_Click()
ShowRateCard g_strRateCardType, Val(txtCompanyID.Text)
End Sub

Private Sub cmdSave_Click()

If chkClient.Value = 0 And chkProspective.Value = 0 And chkSupplier.Value = 0 Then
    MsgBox "You must tick at least one box to specify which type of company this is (Client, Supplier or Prospective)", vbInformation
    Exit Sub
End If

If cmbCompany.Text = "" Then
    MsgBox "Please enter a company name", vbInformation
    cmbCompany.SetFocus
    Exit Sub
End If

If chkTVDistribution.Value = 0 And chkTVStudios.Value = 0 And chkTVIndependents.Value = 0 Then
    MsgBox "Please select one of the client services teams to manage this company"
    Exit Sub
End If

If cmbBusinessUnit.Text = "" Then
    MsgBox "Please select a Business Unit from the dropdown. Please do not free type.", vbInformation
    cmbBusinessUnit.SetFocus
    Exit Sub
End If

If txtMarketCode.Text = "" Then
    MsgBox "Please enter a short company name", vbInformation
    cmbCompany.SetFocus
    Exit Sub
ElseIf Len(txtMarketCode.Text) > 8 Then
    txtMarketCode.Text = Left(txtMarketCode.Text, 8)
End If

If InStr(txtMarketCode.Text, " ") > 0 Then
    MsgBox "Please short company name cannot have spaces in.", vbInformation
    cmbCompany.SetFocus
    Exit Sub
End If
txtMarketCode.Text = UCase(txtMarketCode.Text)

If g_intNoCompulsoryPostcodeAndCountry = 0 Then

    If txtAddress.Text = "" Then
        MsgBox "Please complete the address section", vbInformation
        txtAddress.SetFocus
        Exit Sub
    End If
    
    If txtPostCode.Text = "" Then
        MsgBox "Please complete the postcode section", vbInformation
        txtPostCode.SetFocus
        Exit Sub
    End If
    
    If txtTelephone.Text = "" Then
        MsgBox "Please complete the telephone section", vbInformation
        txtTelephone.SetFocus
        Exit Sub
    End If

End If

If txtCountry.Text = "" Then
    MsgBox "Please complete the country section", vbInformation
    txtCountry.SetFocus
    Exit Sub
End If

If UCase(txtCountry.Text) <> "UK" And UCase(txtCountry.Text) <> "UNITED KINGDOM" Then
    If txtVATCode.Text = "" Then
        If MsgBox("You have said that this is a non-UK company and have not completed the VAT information." & vbCrLf & "Please confirm this is correct, or please add the VAT information ", vbYesNo + vbDefaultButton2) = vbNo Then
            txtVATCode.SetFocus
            Exit Sub
        End If
    End If
End If

If txtSalesLedgerCode.Text = "" Then
    MsgBox "Please provide a Sales Ledger code", vbInformation
    txtSalesLedgerCode.SetFocus
    Exit Sub
End If

If txtCreditLimit.Text <> "" Then
    If Not IsNumeric(txtCreditLimit.Text) Then
        MsgBox "Credit Limit must be numeric!", vbExclamation
        txtCreditLimit.SetFocus
        Exit Sub
    End If
End If

If txtDiscount.Text <> "" Then
    If Not IsNumeric(txtDiscount.Text) Then
        MsgBox "Fixed Discount must be numeric!", vbExclamation
        txtDiscount.SetFocus
        Exit Sub
    Else
        If Val(txtDiscount.Text) > 100 Then
            MsgBox "You can not discount a company more than 100%!", vbExclamation
            txtDiscount.SetFocus
            Exit Sub
        End If
    End If
End If


If cmbAccountStatus.Text = "" Then
    
    If txtCompanyID.Text = "" Then
        cmbAccountStatus.Text = "CASH"
    Else
        MsgBox "The account status has not been set for this client. You must enter one before contimuing." & vbCrLf & vbCrLf & "Please check with your credit control / accounts department if you are unsure.", vbExclamation
        cmbAccountStatus.SetFocus
        Exit Sub
    End If
    
End If

'If txtEmail.Text = "" Then
'    MsgBox "Please complete the email section", vbInformation
'    Exit Sub
'End If

Dim l_strOldName As String

l_strOldName = GetData("company", "name", "accountcode", txtAccountCode.Text)

If l_strOldName <> "" And l_strOldName <> cmbCompany.Text Then
    If MsgBox("Should that company be overwritten with these details?", vbYesNo, "A Company already exists for this account code with name " & l_strOldName) = vbYes Then
        SaveCompany
    End If
Else
    SaveCompany
End If

End Sub

Private Sub cmdTakeSpaces_Click()

Dim l_rst As New ADODB.Recordset
Dim l_con As New ADODB.Connection

l_con.Open g_strConnection
l_rst.Open "SELECT companyID, accountcode FROM company", l_con, adOpenDynamic, adLockOptimistic

l_rst.MoveFirst

Do While Not l_rst.EOF
    l_con.Execute "UPDATE company SET accountcode = '" & QuoteSanitise(Replace(l_rst("accountcode"), " ", "")) & "' WHERE companyID = '" & l_rst("companyID") & "';"
    l_rst.MoveNext
Loop

l_rst.Close
Set l_rst = Nothing
l_con.Close
Set l_con = Nothing


End Sub

Private Sub ddnMessages_CloseUp()

grdTrackerNotifications.Columns("trackermessageID").Text = ddnMessages.Columns("trackermessageID").Text

End Sub

Private Sub Form_Activate()

If txtCompanyID.Text <> "" Then ShowCompany (Val(txtCompanyID.Text))
If CheckAccess("/reactivatecompany", True) Then
    chkInactiveCompany.Enabled = True
Else
    chkInactiveCompany.Enabled = False
End If

Me.WindowState = vbMaximized

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF5 Then
    If Me.ActiveControl.DataField <> "" Then
        SearchForCompany Me.ActiveControl.DataField, Me.ActiveControl.Text
        KeyCode = 0
    End If
End If
End Sub

Private Sub Form_Load()

'MakeLookLikeOffice Me

Dim l_strSQL As String
l_strSQL = "SELECT name, companyID, postcode, iscustomer, issupplier, isprospective, accountstatus, system_active FROM company ORDER BY name"

adoCompany.ConnectionString = g_strConnection
adoCompany.RecordSource = l_strSQL
adoCompany.Refresh

adoCompany2.ConnectionString = g_strConnection
adoCompany2.RecordSource = l_strSQL
adoCompany2.Refresh

adoMessages.ConnectionString = g_strConnection
adoMessages.RecordSource = "SELECT * FROM trackermessage ORDER BY trackermessageID;"
adoMessages.Refresh

grdTrackerNotifications.Columns("message").DropDownHwnd = ddnMessages.hWnd

PopulateCombo "trackerfields", ddnTrackerFields
PopulateCombo "trackerfields", ddnMaterialTrackerFields
grdTrackerChoices.Columns("itemfield").DropDownHwnd = ddnTrackerFields.hWnd
grdMaterialTrackerChoices.Columns("itemfield").DropDownHwnd = ddnMaterialTrackerFields.hWnd

PopulateCombo "trackerdataformats", ddnTrackerDataFormat
grdTrackerChoices.Columns("dash_format").DropDownHwnd = ddnTrackerDataFormat.hWnd

PopulateCombo "countries", txtCountry
PopulateCombo "ratecard", cmbRateCard
PopulateCombo "accountstatus", cmbAccountStatus

PopulateCombo "accountstatus", cmbShowAccountStatus

PopulateCombo "portalmainpages", txtMainPage

PopulateCombo "companyassociation", cmbAssociationType
PopulateCombo "BusinessUnit", cmbBusinessUnit

cmbShowAccountStatus.AddItem "ALL"
cmbShowAccountStatus.ListIndex = 1

tabCompanySearch.Tabs.Item(28).Selected = True

If CheckAccess("/changecompanyaccountstatus", True) Then
    cmbAccountStatus.Enabled = True
Else
    cmbAccountStatus.Enabled = False
End If

If g_optJCASystemVariables <> 0 Then
    tabDetails.TabVisible(5) = False
End If

'CenterForm Me

End Sub

Private Sub Form_Resize()

On Error Resume Next

grdCompanyList.Height = Me.ScaleHeight - grdCompanyList.Top - 180
fraDetails.Left = Me.ScaleWidth - fraDetails.Width - 60
tabDetails.Width = Me.ScaleWidth - tabDetails.Left - fraDetails.Width - 120
tabDetails.Height = Me.ScaleHeight - tabDetails.Top - 60
tabDetails.Tab = 6
grdTrackerChoices.Width = tabDetails.Width - grdTrackerChoices.Left - 180
grdTrackerChoices.Height = (tabDetails.Height - grdTrackerChoices.Top) / 2 - 120
grdTrackerNotifications.Height = grdTrackerChoices.Height
grdTrackerNotifications.Top = grdTrackerChoices.Top + grdTrackerChoices.Height + 120
tabDetails.Tab = 0
grdContacts.Height = Me.ScaleHeight - grdContacts.Top - 120
txtClientCode.Width = tabDetails.Width - txtClientCode.Left - 180

End Sub

Private Sub Frame2_DragDrop(Source As Control, X As Single, Y As Single)

End Sub

Private Sub grdAssociatedCompanies_Click()
If grdAssociatedCompanies.Rows = 0 Then Exit Sub
Dim l_lngLinkedCompanyID As Long

'collect the ID of the linked company
l_lngLinkedCompanyID = grdAssociatedCompanies.Columns("associateID").Text
If l_lngLinkedCompanyID = Val(txtCompanyID.Text) Then l_lngLinkedCompanyID = grdAssociatedCompanies.Columns("masterID").Text

adoCompany2.Recordset.Find "companyID = " & l_lngLinkedCompanyID
lblAssociationCompanyID.Caption = l_lngLinkedCompanyID
lblAssociationID.Caption = grdAssociatedCompanies.Columns("associationID").Text
cmbCompany2.Text = GetData("company", "name", "companyID", l_lngLinkedCompanyID)
cmbAssociationType.Text = grdAssociatedCompanies.Columns("association").Text
End Sub

Private Sub grdCompanyContacts_BeforeUpdate(Cancel As Integer)

If Val(txtCompanyID.Text) = 0 Then
    NoCompanySelectedMessage
    Exit Sub
End If

If grdCompanyContacts.Columns("email").Text <> "" Then
    If ValidateEmail(grdCompanyContacts.Columns("email").Text) = False Then
        MsgBox "Invalid Email address provided"
        Cancel = 1
    End If
Else
    MsgBox "No Email address provided"
    Cancel = 1
End If

grdCompanyContacts.Columns("companyID").Text = txtCompanyID.Text

End Sub

Private Sub grdCompanyList_Click()
If grdCompanyList.Rows < 1 Then Exit Sub
ShowCompany grdCompanyList.Columns("companyID").Text
End Sub

Private Sub grdContacts_BtnClick()
EmailContact grdContacts.Columns("contactID").Text
End Sub

Private Sub grdContacts_DblClick()

If Val(txtCompanyID.Text) = 0 Then
    NoCompanySelectedMessage
    Exit Sub
End If

frmContact.lblCompanyID.Caption = txtCompanyID.Text
frmContact.lblEmployeeID.Caption = grdContacts.Columns("employeeID").Text
frmContact.txtUserName.Text = GetData("employee", "username", "employeeID", grdContacts.Columns("employeeID").Text)
frmContact.txtPassword.Text = GetData("employee", "password", "employeeID", grdContacts.Columns("employeeID").Text)
frmContact.txtJobTitle.Text = GetData("employee", "jobtitle", "employeeID", grdContacts.Columns("employeeID").Text)
frmContact.txtCetaCode.Text = GetData("employee", "cetacode", "employeeID", grdContacts.Columns("employeeID").Text)
frmContact.cmdLoadContactsForCompany.Value = True

If grdContacts.Rows = 0 Then
    ShowContact 0
Else
    ShowContact grdContacts.Columns("contactID").Text
End If

End Sub

Private Sub grdPortalTitleStubs_BeforeUpdate(Cancel As Integer)

If Val(txtCompanyID.Text) = 0 Then Cancel = 1

grdPortalTitleStubs.Columns("category").Text = "portaltitlestub"
grdPortalTitleStubs.Columns("information").Text = txtCompanyID.Text

End Sub

Private Sub grdSupplierRates_BeforeUpdate(Cancel As Integer)

grdSupplierRates.Columns("companyID").Text = Val(txtCompanyID.Text)
grdSupplierRates.Columns("muser").Text = g_strUserInitials
grdSupplierRates.Columns("mdate").Text = Now

End Sub

Private Sub grdTrackerChoices_BeforeUpdate(Cancel As Integer)

If Val(txtCompanyID.Text) = 0 Then
    Cancel = 1
    Exit Sub
End If

grdTrackerChoices.Columns("companyID").Text = txtCompanyID.Text

End Sub

Private Sub grdTrackerNotifications_BeforeUpdate(Cancel As Integer)

If ValidateEmail(grdTrackerNotifications.Columns("email").Text) = False Then
    InvalidEmailAddressMessage
    Cancel = True
End If
If grdTrackerNotifications.Columns("fullname").Text = "" Then Cancel = True
If txtSource.Text <> "" Then
    grdTrackerNotifications.Columns("contractgroup").Text = txtSource.Text
Else
    grdTrackerNotifications.Columns("contractgroup").Text = "NOGROUPSPECIFIED"
End If

End Sub

Private Sub grdTrackerNotifications_RowLoaded(ByVal Bookmark As Variant)

grdTrackerNotifications.Columns("message").Text = GetData("trackermessage", "trackermessage", "trackermessageID", Val(grdTrackerNotifications.Columns("trackermessageID").Text))

End Sub

Private Sub SSOleDBGrid1_InitColumnProps()

End Sub

Public Sub tabCompanySearch_Click()

Dim l_strSearch As String

If tabCompanySearch.SelectedItem.Caption = "*" Then
    adoCompany.Recordset.Filter = "system_active = 1"
ElseIf tabCompanySearch.SelectedItem.Caption = "#" Then
    adoCompany.Recordset.Filter = "(name LIKE '0%' OR name LIKE '1%' OR name LIKE '2%'  OR name LIKE '3%' OR name LIKE '4%' OR name LIKE '5%' OR name LIKE '6%' OR name LIKE '7%' OR name LIKE '8%' OR name LIKE '9%')"
    chkShow(0).Value = False
    chkShow(1).Value = False
    chkShow(2).Value = False
    cmbShowAccountStatus.ListIndex = 1
    Exit Sub
Else
    l_strSearch = tabCompanySearch.SelectedItem.Caption
    adoCompany.Recordset.Filter = "name LIKE '" & l_strSearch & "%' AND system_active = 1"
End If

If UCase(cmbShowAccountStatus.Text) <> "ALL" Then
    If adoCompany.Recordset.Filter <> "" Then
        adoCompany.Recordset.Filter = CStr(adoCompany.Recordset.Filter) & " AND accountstatus = '" & cmbShowAccountStatus.Text & "'"
    Else
        adoCompany.Recordset.Filter = "accountstatus = '" & cmbShowAccountStatus.Text & "'"
    End If
End If

If adoCompany.Recordset.Filter = "" Or adoCompany.Recordset.Filter = "0" Then adoCompany.Recordset.Filter = "companyID <> 0"

If chkShow(0).Value = 1 Then adoCompany.Recordset.Filter = adoCompany.Recordset.Filter & " AND iscustomer = 1" 'l_intfilter = 1
If chkShow(1).Value = 1 Then adoCompany.Recordset.Filter = adoCompany.Recordset.Filter & " AND issupplier = 1" 'l_intfilter = 1
If chkShow(2).Value = 1 Then adoCompany.Recordset.Filter = adoCompany.Recordset.Filter & " AND isprospective = 1" ': l_intfilter = 1


End Sub

Public Sub tabDetails_Click(PreviousTab As Integer)

Select Case tabDetails.Caption

Case "Rate Card"

    adoSupplierRates.RecordSource = "SELECT * FROM supplierrate WHERE companyID = '" & txtCompanyID.Text & "' ORDER BY partreference;"
    adoSupplierRates.ConnectionString = g_strConnection
    adoSupplierRates.Refresh
    
'Case "Associations"
'
'    adoAssociatedCompanies.RecordSource = "(SELECT company.name, association.association, masterID, associateID, associationID FROM association INNER JOIN company ON association.associateID = company.companyID WHERE associationtype = 'company' AND (masterID = '" & Val(txtCompanyID.Text) & "') ORDER BY company.name) UNION (SELECT company.name, association.association, masterID, associateID, associationID FROM association INNER JOIN company ON association.masterID = company.companyID WHERE associationtype = 'company' AND (associateID = '" & Val(txtCompanyID.Text) & "') ORDER BY company.name);"
'    adoAssociatedCompanies.ConnectionString = g_strConnection
'    adoAssociatedCompanies.Refresh
'
'    grdAssociatedCompanies.Columns("associateID").Visible = False
'    grdAssociatedCompanies.Columns("masterID").Visible = False
'    grdAssociatedCompanies.Columns("associationID").Visible = False
'
End Select

End Sub

Private Sub txtAccountCode_GotFocus()
HighLite txtAccountCode
End Sub


Private Sub txtAddress_GotFocus()
HighLite txtAddress
End Sub

Private Sub txtClientCode_GotFocus()
HighLite txtClientCode
End Sub


Private Sub txtComments_GotFocus()
HighLite txtComments
End Sub


Private Sub txtCompanyID_GotFocus()
HighLite txtCompanyID
End Sub

Private Sub txtCompanyID_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    If IsNumeric(txtCompanyID.Text) Then
        ShowCompany Val(txtCompanyID.Text)
    End If
    HighLite txtCompanyID
End If
End Sub
Private Sub txtCountry_GotFocus()
HighLite txtCountry
End Sub

Private Sub txtCreatedDate_GotFocus()
HighLite txtCreatedDate
End Sub

Private Sub txtEmail_GotFocus()
HighLite txtEmail
End Sub

Private Sub txtFax_GotFocus()
HighLite txtFax
End Sub

Private Sub txtMarketCode_GotFocus()
HighLite txtMarketCode
End Sub

Private Sub txtPostCode_GotFocus()
HighLite txtPostCode
End Sub

Private Sub txtRegisteredOffice_GotFocus()
HighLite txtRegisteredOffice
End Sub

Private Sub txtSource_GotFocus()
HighLite txtSource
End Sub

Private Sub txtTelephone_GotFocus()
HighLite txtTelephone
End Sub

Private Sub txtTradeCode_GotFocus()
HighLite txtTradeCode
End Sub

Private Sub txtVATCode_GotFocus()
HighLite txtVATCode
End Sub

Private Sub txtVATNumber_GotFocus()
HighLite txtVATNumber
End Sub

Private Sub txtWebsite_GotFocus()
HighLite txtWebsite
End Sub
