VERSION 5.00
Begin VB.Form frmLabelsVHS 
   Caption         =   "VHS Labels"
   ClientHeight    =   4350
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7260
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLabelsVHS.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4350
   ScaleWidth      =   7260
   StartUpPosition =   1  'CenterOwner
   Begin VB.Timer timCloseForm 
      Interval        =   20000
      Left            =   540
      Top             =   3780
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   10
      Left            =   2940
      TabIndex        =   27
      ToolTipText     =   "SubTitle for VHS Labels"
      Top             =   960
      Width           =   1095
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   9
      Left            =   4740
      TabIndex        =   24
      ToolTipText     =   "SubTitle for VHS Labels"
      Top             =   960
      Width           =   1095
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   8
      Left            =   1140
      TabIndex        =   23
      ToolTipText     =   "SubTitle for VHS Labels"
      Top             =   960
      Width           =   1095
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print Labels"
      Height          =   315
      Left            =   4740
      TabIndex        =   13
      ToolTipText     =   "Print the VHS Labels"
      Top             =   3840
      Width           =   1155
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Index           =   0
      Left            =   6000
      TabIndex        =   12
      ToolTipText     =   "Close this form"
      Top             =   3840
      Width           =   1155
   End
   Begin VB.Frame Frame1 
      Caption         =   "Label Layouts"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   180
      TabIndex        =   9
      Top             =   2700
      Width           =   6975
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         Height          =   315
         Index           =   7
         Left            =   6180
         TabIndex        =   21
         Text            =   "1"
         ToolTipText     =   "Number of individual VHS Label sets to be printed"
         Top             =   420
         Width           =   435
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Reduced Layout"
         Height          =   195
         Index           =   1
         Left            =   300
         TabIndex        =   11
         ToolTipText     =   "Use the Reduced VHS Label Layout"
         Top             =   660
         Width           =   2295
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Standard Layout"
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   10
         ToolTipText     =   "Use the Standard VHS Label Layout"
         Top             =   300
         Width           =   1875
      End
      Begin VB.Label Label1 
         Caption         =   "Number of copies to Print"
         Height          =   195
         Index           =   7
         Left            =   4200
         TabIndex        =   22
         Top             =   480
         Width           =   1935
      End
   End
   Begin VB.OptionButton Option1 
      Caption         =   "SVHS"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   5940
      TabIndex        =   8
      Top             =   1200
      Width           =   1140
   End
   Begin VB.OptionButton Option1 
      Caption         =   "VHS"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   5940
      TabIndex        =   7
      Top             =   780
      Width           =   1140
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   6
      Left            =   4740
      TabIndex        =   6
      ToolTipText     =   "Duration for VHS Labels"
      Top             =   2220
      Width           =   1095
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   5
      Left            =   1140
      TabIndex        =   5
      ToolTipText     =   "Standard for VHS Labels"
      Top             =   2220
      Width           =   1095
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   3
      Left            =   1140
      TabIndex        =   3
      ToolTipText     =   "SubTitle for VHS Labels"
      Top             =   1380
      Width           =   4695
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   2
      Left            =   1140
      TabIndex        =   2
      ToolTipText     =   "Title for VHS Labels"
      Top             =   540
      Width           =   4695
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   1
      Left            =   5820
      TabIndex        =   1
      ToolTipText     =   "Master Number for VHS Labels"
      Top             =   120
      Width           =   1275
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   0
      Left            =   1140
      TabIndex        =   0
      ToolTipText     =   "Job Number for VHS Labels"
      Top             =   120
      Width           =   1095
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   4
      Left            =   1140
      TabIndex        =   4
      ToolTipText     =   "Version for VHS Labels"
      Top             =   1800
      Width           =   4695
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Set"
      Height          =   195
      Index           =   10
      Left            =   2400
      TabIndex        =   28
      Top             =   1020
      Width           =   495
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Episode"
      Height          =   195
      Index           =   9
      Left            =   3900
      TabIndex        =   26
      Top             =   1020
      Width           =   795
   End
   Begin VB.Label Label1 
      Caption         =   "Series"
      Height          =   195
      Index           =   8
      Left            =   120
      TabIndex        =   25
      Top             =   1020
      Width           =   915
   End
   Begin VB.Label Label1 
      Caption         =   "Master Number"
      Height          =   195
      Index           =   6
      Left            =   4620
      TabIndex        =   20
      Top             =   180
      Width           =   1155
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Duration"
      Height          =   195
      Index           =   5
      Left            =   3720
      TabIndex        =   19
      Top             =   2280
      Width           =   915
   End
   Begin VB.Label Label1 
      Caption         =   "Standard"
      Height          =   195
      Index           =   4
      Left            =   120
      TabIndex        =   18
      Top             =   2280
      Width           =   915
   End
   Begin VB.Label Label1 
      Caption         =   "Version"
      Height          =   195
      Index           =   3
      Left            =   120
      TabIndex        =   17
      Top             =   1860
      Width           =   915
   End
   Begin VB.Label Label1 
      Caption         =   "Subtitle"
      Height          =   195
      Index           =   2
      Left            =   120
      TabIndex        =   16
      Top             =   1440
      Width           =   915
   End
   Begin VB.Label Label1 
      Caption         =   "Title"
      Height          =   195
      Index           =   1
      Left            =   120
      TabIndex        =   15
      Top             =   600
      Width           =   915
   End
   Begin VB.Label Label1 
      Caption         =   "Job Number"
      Height          =   195
      Index           =   0
      Left            =   120
      TabIndex        =   14
      Top             =   180
      Width           =   915
   End
End
Attribute VB_Name = "frmLabelsVHS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_intMinutesToCloseForm As Integer

Private Sub cmdClose_Click(Index As Integer)
Me.Hide
End Sub

Private Sub cmdPrint_Click()

Dim l_strReportToPrint As String, l_strSQL As String, l_lngID As Long

If Option2(0).Value = True Then
    l_strReportToPrint = g_strLocationOfCrystalReportFiles & "VHSlabel.rpt"
Else
    l_strReportToPrint = g_strLocationOfCrystalReportFiles & "VHSlabl2.rpt"
End If

'load up the labelprint table with the values from the form

l_strSQL = "INSERT INTO labelprinting ("
l_strSQL = l_strSQL & "cuser, cdate, field0, field1, field2, field3, field4, field5, field6, "
l_strSQL = l_strSQL & "field7, field8, field10, field9) VALUES ("

l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'" & Text1(0).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(1).Text & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(2).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(3).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(4).Text) & "', "
l_strSQL = l_strSQL & "'" & Text1(5).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(6).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(8).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(9).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(10).Text & "', "
If Option1(0).Value = True Then
    l_strSQL = l_strSQL & "'VHS'"
Else
    l_strSQL = l_strSQL & "'SVHS'"
End If
l_strSQL = l_strSQL & ")"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_lngID = g_lngLastID

'print the report
PrintCrystalReport l_strReportToPrint, "{labelprinting.labelprintingID} =" & Str(l_lngID), g_blnPreviewReport, Val(Text1(7).Text)

'get rid of that labelprint record again.

l_strSQL = "DELETE FROM labelprinting WHERE labelprintingID = " & Str(l_lngID)
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Sub

Private Sub timCloseForm_Timer()

g_optCloseSystemDown = GetFlag(GetData("setting", "value", "name", "CloseSystemDown"))

If g_optCloseSystemDown <> 0 Then Unload Me

End Sub
