VERSION 5.00
Begin VB.Form frmBarcodePrint 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Barcode Label Printing"
   ClientHeight    =   2805
   ClientLeft      =   5310
   ClientTop       =   2535
   ClientWidth     =   3690
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "frmBarcodePrint.frx":0000
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2805
   ScaleWidth      =   3690
   Begin VB.CommandButton cmdPrint 
      Appearance      =   0  'Flat
      Caption         =   "&Print"
      Height          =   315
      Left            =   1500
      TabIndex        =   5
      Top             =   2400
      Width           =   1035
   End
   Begin VB.PictureBox pnlProgress 
      Height          =   315
      Left            =   60
      ScaleHeight     =   255
      ScaleWidth      =   3495
      TabIndex        =   4
      Top             =   1980
      Width           =   3555
   End
   Begin VB.Frame fraInput 
      Caption         =   "Printing Options"
      Height          =   1875
      Index           =   1
      Left            =   60
      TabIndex        =   3
      Top             =   60
      Width           =   3555
      Begin VB.ComboBox cmbStockManufacturer 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   2160
         TabIndex        =   10
         ToolTipText     =   "The Tape Type"
         Top             =   1380
         Width           =   1275
      End
      Begin VB.ComboBox cmbStockCode 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   2160
         TabIndex        =   8
         ToolTipText     =   "The Tape Type"
         Top             =   1020
         Width           =   1275
      End
      Begin VB.ComboBox cmbRecordFormat 
         Height          =   315
         Left            =   2160
         TabIndex        =   6
         ToolTipText     =   "The Format of this Tape"
         Top             =   660
         Width           =   1275
      End
      Begin VB.TextBox txtNumberOfBarcodes 
         Height          =   285
         Left            =   2160
         MaxLength       =   3
         TabIndex        =   1
         Text            =   "0"
         Top             =   300
         Width           =   1275
      End
      Begin VB.Label lblCaption 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Stock Manufacturer"
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   11
         Top             =   1440
         Width           =   1410
      End
      Begin VB.Label lblCaption 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Stock Code"
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   9
         Top             =   1080
         Width           =   810
      End
      Begin VB.Label lblCaption 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Format"
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   7
         Top             =   720
         Width           =   510
      End
      Begin VB.Label lblCaption 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Number of labels to print"
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   0
         Top             =   360
         Width           =   1770
      End
   End
   Begin VB.CommandButton cmdClose 
      Appearance      =   0  'Flat
      Cancel          =   -1  'True
      Caption         =   "Close"
      Height          =   315
      Left            =   2580
      TabIndex        =   2
      Top             =   2400
      Width           =   1035
   End
End
Attribute VB_Name = "frmBarcodePrint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmbRecordFormat_GotFocus()
PopulateCombo "format", cmbRecordFormat
End Sub

Private Sub cmbRecordFormat_KeyPress(KeyAscii As Integer)
AutoMatch cmbRecordFormat, KeyAscii
End Sub

Private Sub cmbStockCode_GotFocus()
Dim l_strStockCode As String
l_strStockCode = cmbStockCode.Text
cmbStockCode.Clear
PopulateCombo "stockcodesforformat" & cmbRecordFormat.Text, cmbStockCode
cmbStockCode.Text = l_strStockCode
End Sub

Private Sub cmbStockManufacturer_GotFocus()
Dim l_strStockManufacturer As String
l_strStockManufacturer = cmbStockManufacturer.Text
cmbStockManufacturer.Clear
PopulateCombo "stockmanufacturer", cmbStockManufacturer
cmbStockManufacturer.Text = l_strStockManufacturer
End Sub

Private Sub cmdClose_Click()

On Error GoTo cmdClose_ClickError

'unload this form
Unload Me

Exit Sub

cmdClose_ClickError:
'ErrorHandler "frmBarcodePrint", "cmdClose_Click", Err, Error: Exit Sub

End Sub

Private Sub cmdPrint_Click()

Dim l_strNextBarcode As String, l_lngCount As Long, l_lngLibraryID As Long
Dim l_strReportToPrint As String

If cmbRecordFormat.Text = "" Or cmbStockCode.Text = "" Or cmbStockManufacturer.Text = "" Then Exit Sub

l_strReportToPrint = g_strLocationOfCrystalReportFiles & GetData("xref", "information", "description", CurrentMachineName)
        
If Val(txtNumberOfBarcodes) > 0 Then
    
    For l_lngCount = 1 To Val(txtNumberOfBarcodes)
        l_lngLibraryID = 0
        Do While l_lngLibraryID = 0
            l_strNextBarcode = g_strCompanyPrefix & GetNextSequence("barcodenumber")
            If GetData("library", "libraryID", "barcode", l_strNextBarcode) = 0 Then
                l_lngLibraryID = CreateLibraryItem(l_strNextBarcode, "Blank Stock", 0, 0, 501, True)
            End If
        Loop
        SetData "library", "format", "libraryID", l_lngLibraryID, cmbRecordFormat.Text
        SetData "library", "stocktype", "libraryID", l_lngLibraryID, cmbStockCode.Text
        SetData "library", "stockmanufacturer", "libraryID", l_lngLibraryID, cmbStockManufacturer.Text
        
        If GetData("Xref", "descriptionalias", "description", cmbStockCode.Text) = "COMP" Then
            l_strReportToPrint = g_strLocationOfCrystalReportFiles & "DLT-LTO" & Mid(GetData("xref", "information", "description", CurrentMachineName), 8, 1) & ".rpt"
        End If
    
        PrintCrystalReport l_strReportToPrint, "{library.libraryID} = " & l_lngLibraryID, False

    Next
    
End If

End Sub

Private Sub Form_Load()

On Error GoTo Form_LoadError

'centre the form
CenterForm Me

Screen.MousePointer = vbDefault

Exit Sub

Form_LoadError:
MsgBox Err.Description, vbExclamation, "Error: " & Err.Number
Exit Sub

End Sub

Private Sub txtNumberOfBarcodes_GotFocus()
HighLite txtNumberOfBarcodes
End Sub
