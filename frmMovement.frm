VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmMovement 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Library Internal Movement"
   ClientHeight    =   8850
   ClientLeft      =   2715
   ClientTop       =   4740
   ClientWidth     =   15705
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8850
   ScaleWidth      =   15705
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox cmbBox 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   7920
      TabIndex        =   21
      ToolTipText     =   "The New Location?"
      Top             =   120
      Width           =   2355
   End
   Begin MSAdodcLib.Adodc adoMovement 
      Height          =   330
      Left            =   300
      Top             =   1200
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoDelivery"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox txtForJobID 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   4560
      TabIndex        =   19
      ToolTipText     =   "Scan the Barcodes of the items on the Despatch"
      Top             =   480
      Width           =   2355
   End
   Begin VB.CommandButton cmdProcessMovement 
      Caption         =   "Process Movement"
      Height          =   315
      Left            =   4560
      TabIndex        =   18
      Top             =   900
      Visible         =   0   'False
      Width           =   2355
   End
   Begin VB.ComboBox cmbShelf 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   4560
      TabIndex        =   15
      ToolTipText     =   "The New Location?"
      Top             =   120
      Width           =   2355
   End
   Begin VB.TextBox txtItemCount 
      Alignment       =   2  'Center
      Height          =   345
      Left            =   13500
      Locked          =   -1  'True
      TabIndex        =   13
      Top             =   900
      Width           =   1155
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print"
      Height          =   315
      Left            =   11640
      TabIndex        =   12
      Top             =   60
      Width           =   1155
   End
   Begin VB.TextBox txtBatchNumber 
      BackColor       =   &H00FFC0C0&
      Height          =   315
      Left            =   8340
      TabIndex        =   11
      Top             =   900
      Width           =   1155
   End
   Begin VB.CommandButton cmdLoadBatch 
      Caption         =   "Load Batch"
      Height          =   315
      Left            =   7080
      TabIndex        =   10
      Top             =   900
      Width           =   1155
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   14160
      TabIndex        =   5
      Top             =   60
      Width           =   1155
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdMovements 
      Bindings        =   "frmMovement.frx":0000
      Height          =   7455
      Left            =   120
      TabIndex        =   9
      Top             =   1320
      Width           =   14535
      _Version        =   196617
      AllowDelete     =   -1  'True
      AllowUpdate     =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   12648384
      RowHeight       =   370
      ExtraHeight     =   53
      Columns(0).Width=   3200
      _ExtentX        =   25638
      _ExtentY        =   13150
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdDespatch 
      Caption         =   "Despatch"
      Height          =   315
      Left            =   10380
      TabIndex        =   3
      Top             =   60
      Visible         =   0   'False
      Width           =   1155
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save && Close"
      Height          =   315
      Left            =   12900
      TabIndex        =   4
      Top             =   60
      Width           =   1155
   End
   Begin VB.ComboBox cmbDeliveredBy 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1260
      TabIndex        =   1
      ToolTipText     =   "Who is this despatch for or delivered by"
      Top             =   480
      Width           =   2235
   End
   Begin VB.ComboBox cmbLocation 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1260
      TabIndex        =   0
      ToolTipText     =   "The New Location?"
      Top             =   120
      Width           =   2235
   End
   Begin VB.TextBox txtBarcode 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   1260
      TabIndex        =   2
      ToolTipText     =   "Scan the Barcodes of the items on the Despatch"
      Top             =   840
      Width           =   2235
   End
   Begin VB.Label lblCaption 
      Caption         =   "Detail / Box"
      Height          =   255
      Index           =   2
      Left            =   7020
      TabIndex        =   22
      Top             =   180
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "For JobID"
      Height          =   195
      Index           =   1
      Left            =   3600
      TabIndex        =   20
      Top             =   540
      Width           =   1035
   End
   Begin VB.Label lblStatus 
      Height          =   255
      Left            =   7020
      TabIndex        =   17
      Top             =   540
      Width           =   5835
   End
   Begin VB.Label lblCaption 
      Caption         =   "Sub Location"
      Height          =   255
      Index           =   0
      Left            =   3600
      TabIndex        =   16
      Top             =   180
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Item Count:"
      Height          =   255
      Left            =   12420
      TabIndex        =   14
      Top             =   960
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Taken By"
      Height          =   255
      Index           =   21
      Left            =   120
      TabIndex        =   8
      Top             =   540
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Destination"
      Height          =   255
      Index           =   18
      Left            =   120
      TabIndex        =   7
      Top             =   180
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Barcode"
      Height          =   195
      Index           =   11
      Left            =   120
      TabIndex        =   6
      Top             =   900
      Width           =   855
   End
End
Attribute VB_Name = "frmMovement"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const m_strFields = " library.barcode as 'Barcode', library.productname AS 'Product', library.format AS 'Format', library.companyname AS 'Client', movement.cuser as 'User', destination AS 'Destination', movement.shelf AS 'Sub Location', movement.box AS 'Detail / Box', takenby as 'Taken By', movement.cdate AS 'Date Moved', movement.forjobID, movementID FROM movement INNER JOIN library ON library.libraryID = movement.libraryID "

Private Sub cmbDeliveredBy_Click()

'refresh the list of records
adoMovement.ConnectionString = g_strConnection
adoMovement.RecordSource = "SELECT " & m_strFields & " WHERE movement.batchnumber = '" & txtBatchNumber.Text & "' ORDER BY movement.cdate DESC"
adoMovement.Refresh

cmdSave.Enabled = True

End Sub

Private Sub cmdClose_Click()

If cmdSave.Enabled = True Then

    If grdMovements.Rows > 0 Then
        Dim i As Integer
        i = MsgBox("There are still some un-saved movements. Do you want to save your changes before you close?", vbYesNoCancel + vbQuestion)
        
        Select Case i
        Case vbYes
            cmdSave_Click
            DoEvents
        Case vbNo
            'do nothing, just close
        Case vbCancel
            Exit Sub
        End Select
    
    End If
End If

If Me.Tag = "AssignFromLibrary" Then
    If IsFormLoaded("frmLibrary") Then
        frmLibrary.Tag = "ReturnFromAssign"
    End If
End If

'close the form
Unload Me

End Sub

Private Sub cmdLoadBatch_Click()

Dim l_strSQL As String
l_strSQL = "SELECT " & m_strFields & " WHERE batchnumber = '" & Val(txtBatchNumber.Text) & "' ORDER BY movement.cdate DESC"

adoMovement.ConnectionString = g_strConnection
adoMovement.RecordSource = l_strSQL
adoMovement.Refresh

cmdSave.Enabled = False

End Sub

Private Sub cmdPrint_Click()

If Val(txtBatchNumber.Text) = 0 Then Exit Sub

PrintCrystalReport App.Path & "/reports/batchmovement.rpt", "{movement.batchnumber} = " & Val(txtBatchNumber.Text), True

End Sub

Private Sub cmdProcessMovement_Click()

txtBarcode_KeyPress 13

End Sub

Private Sub cmdSave_Click()

Dim i As Integer, AssignFromLibrary As Boolean, l_blnQuestionAsked As Boolean, l_blnQuestionAnswer As Boolean
i = MsgBox("Are you sure you want to save this movement?", vbYesNo + vbQuestion)
If i = vbNo Then Exit Sub

'if this is a movement from the library form
If Me.Tag = "AssignFromLibrary" Then
    If txtBarcode.Text <> "" Then
        txtBarcode_KeyPress vbKeyReturn
        txtBarcode.Text = ""
        AssignFromLibrary = True
    End If
End If

Dim l_lngBatch As Long
l_lngBatch = Val(txtBatchNumber.Text)

Dim l_strSQL As String
Dim l_con As New ADODB.Connection
Dim l_rstMovements As New ADODB.Recordset
Dim l_strOldLocation As String

l_con.Open g_strConnection

'now update them all
l_strSQL = "SELECT * FROM movement WHERE batchnumber = '" & l_lngBatch & "' ORDER BY cdate"

l_rstMovements.Open l_strSQL, l_con, adOpenDynamic, adLockOptimistic

If Not l_rstMovements.EOF Then l_rstMovements.MoveFirst

Do While Not l_rstMovements.EOF
    lblStatus.Caption = "Moving " & GetData("library", "barcode", "libraryID", l_rstMovements("libraryID"))
    DoEvents
    l_strOldLocation = GetData("library", "location", "libraryID", l_rstMovements("libraryID"))
    MoveLibraryItem l_rstMovements("libraryID"), l_rstMovements("destination"), 0, l_lngBatch, l_rstMovements("shelf"), l_rstMovements("box"), Val(Trim(" " & l_rstMovements("forjobID")))
    If (GetData("library", "location", "libraryID", l_rstMovements("libraryID")) <> l_strOldLocation) And LCase(l_strOldLocation) = "library" And (AssignFromLibrary = False) Then
        If l_blnQuestionAsked = False Then
            If MsgBox("Is this a permanent removal from Library?", vbYesNo, "Removing Tape from Library") = vbYes Then
                l_blnQuestionAsked = True
                SetData "library", "inlibrary", "libraryID", l_rstMovements("libraryID"), "NO"
                l_blnQuestionAnswer = True
            Else
                l_blnQuestionAsked = True
                l_blnQuestionAnswer = False
            End If
        Else
            If l_blnQuestionAnswer = True Then
                SetData "library", "inlibrary", "libraryID", l_rstMovements("libraryID"), "NO"
            End If
        End If
    End If
    l_rstMovements.MoveNext
Loop

lblStatus.Caption = ""
DoEvents

l_rstMovements.Close
Set l_rstMovements = Nothing

l_con.Close
Set l_con = Nothing

On Error Resume Next
txtBatchNumber.Text = ""

cmdLoadBatch_Click

cmdClose.Value = True

'MsgBox "Movements complete", vbExclamation

End Sub

Private Sub Form_Load()

CenterForm Me

DoEvents

PopulateCombo "location", cmbLocation
PopulateCombo "users", cmbDeliveredBy
PopulateCombo "shelf", cmbShelf
PopulateCombo "box", cmbBox

'cmbDeliveredBy.Text = g_strFullUSerName 'dont default - ASHOK REQUEST

cmbLocation.Text = g_strDefaultInternalMovementLocation

End Sub

Private Sub grdMovements_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

'variable
Dim l_intBookmark As Integer, l_intTotalBookmarks As Integer, l_varCurrentBookmark As Variant
Dim l_lngMovementID As Long, l_strInsertSQL As String


'store the bookmark count
l_intTotalBookmarks = grdMovements.SelBookmarks.Count


Dim l_intMsg As Integer
l_intMsg = MsgBox("Are you sure you want to delete these " & l_intTotalBookmarks & " movement lines?", vbQuestion + vbYesNo)
If l_intMsg = vbNo Then Exit Sub


'loop through the grid bookmarks
For l_intBookmark = 0 To l_intTotalBookmarks - 1

    'get the bookmark
    l_varCurrentBookmark = grdMovements.SelBookmarks(l_intBookmark)
    
    'pick up the costing ID
    l_lngMovementID = grdMovements.Columns("movementID").CellText(l_varCurrentBookmark)
        
    'build it (its only easy, even a monkey could do it)
    l_strInsertSQL = "DELETE FROM movement WHERE movementID = '" & l_lngMovementID & "';"
        
    'then we need to run it
    ExecuteSQL l_strInsertSQL, g_strExecuteError
    
    CheckForSQLError

Next

Cancel = True

adoMovement.Refresh
grdMovements.Refresh

End Sub

Private Sub grdMovements_RowLoaded(ByVal Bookmark As Variant)
txtItemCount.Text = grdMovements.Rows
End Sub

Private Sub txtBarcode_GotFocus()
HighLite txtBarcode
End Sub

Private Sub txtBarcode_KeyPress(KeyAscii As Integer)
If KeyAscii = vbKeyReturn Then
    KeyAscii = 0
    
    Dim l_lngLibraryID  As Long
    Dim l_strBarcode    As String
    Dim l_strLocation   As String
    Dim l_strTakenBy    As String
    Dim l_strShelf      As String
    Dim l_strBox        As String
    Dim l_lngBatchNumber As String
    Dim l_lngForJobID   As Long
    
    
    l_strBarcode = txtBarcode.Text
    l_strLocation = cmbLocation.Text
    l_strTakenBy = cmbDeliveredBy.Text
    l_strShelf = cmbShelf.Text
    l_strBox = cmbBox.Text
    l_lngForJobID = Val(txtForJobID.Text)
    
    
    If txtBatchNumber.Text = "" Then
        txtBatchNumber.Text = GetNextSequence("librarymovementbatch")
    End If
    
    l_lngBatchNumber = Val(txtBatchNumber.Text)
    
    'check for a valid location
    If l_strLocation = "" Then
        MsgBox "You must specify a location before scanning a barcode", vbExclamation
        HighLite txtBarcode
        Exit Sub
    End If
    
    If UCase(l_strLocation) = "LIBRARY" And (l_strShelf = "" And l_strBox = "") Then
        MsgBox "You must specify a shelf or a box for all materials moving to the Library", vbExclamation
        HighLite txtBarcode
        Exit Sub
    End If
    
    If UCase(l_strLocation) = "OPERATIONS" And l_lngForJobID = 0 Then
        MsgBox "You must specify a JobID for all materials moving into Operations", vbExclamation
        HighLite txtBarcode
        Exit Sub
    End If
    
    'check for a valid user name
    If l_strTakenBy = "" Then
        MsgBox "You must specify a taken by name before scanning a barcode", vbExclamation
        HighLite txtBarcode
        Exit Sub
    End If
    
    'check for a valid barcode
    If l_strBarcode <> "" Then
        
        'pick up the library ID
        l_lngLibraryID = GetData("library", "libraryID", "barcode", l_strBarcode)
        
        'if there is a library item for this barcode
        If l_lngLibraryID <> 0 Then
            
            'move the library item
            MoveLibraryItemInternally l_lngLibraryID, l_strLocation, l_strTakenBy, l_lngBatchNumber, l_strShelf, l_strBox, l_lngForJobID
        
        
            adoMovement.ConnectionString = g_strConnection
            adoMovement.RecordSource = "SELECT " & m_strFields & " WHERE movement.batchnumber = '" & txtBatchNumber.Text & "' ORDER BY movement.cdate DESC"
            adoMovement.Refresh
            
            HighLite txtBarcode

        Else
            
            MsgBox "Could not locate barcode", vbExclamation
            HighLite txtBarcode
            Exit Sub
            
        End If
    
    Else
        MsgBox "You must specify a barcode", vbExclamation
        HighLite txtBarcode
        Exit Sub
    End If

End If

End Sub

