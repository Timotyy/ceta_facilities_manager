VERSION 5.00
Begin VB.Form frmStartVTJob 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Start VT Job"
   ClientHeight    =   4335
   ClientLeft      =   4260
   ClientTop       =   3960
   ClientWidth     =   5070
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4335
   ScaleWidth      =   5070
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdStartVTJob 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   495
      Index           =   2
      Left            =   2460
      TabIndex        =   2
      ToolTipText     =   "Cancel without saving"
      Top             =   3720
      Width           =   2475
   End
   Begin VB.CommandButton cmdStartVTJob 
      Caption         =   "Start VT, Print and Close"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   495
      Index           =   1
      Left            =   2460
      TabIndex        =   1
      ToolTipText     =   "Start VT job, print & close"
      Top             =   1080
      Width           =   2475
   End
   Begin VB.CommandButton cmdStartVTJob 
      Caption         =   "Start VT and Close"
      Enabled         =   0   'False
      Height          =   495
      Index           =   0
      Left            =   2460
      TabIndex        =   0
      ToolTipText     =   "Start VT job & Close"
      Top             =   480
      Width           =   2475
   End
   Begin VB.ListBox lstOperator 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      Height          =   3735
      Left            =   120
      TabIndex        =   3
      Top             =   480
      Width           =   2175
   End
   Begin VB.Label lblCaption 
      Caption         =   "Please highlight your name on the list below..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   60
      TabIndex        =   4
      Top             =   120
      Width           =   3915
   End
End
Attribute VB_Name = "frmStartVTJob"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdStartVTJob_Click(Index As Integer)

Select Case Index
Case 0
    Me.Tag = "Start"
Case 1
    Me.Tag = "Print"

Case 2
    Me.Tag = "Cancel"
End Select

Me.Hide


End Sub

Private Sub Form_Load()

PopulateCombo "operator", lstOperator

CenterForm Me


End Sub

Private Sub lstOperator_Click()

'enable the start buttons.
'as you cant start a job without saying who you are

cmdStartVTJob(0).Enabled = True
cmdStartVTJob(1).Enabled = True

End Sub
