VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmTrackerMediaServices 
   Caption         =   "Media Services Tracker"
   ClientHeight    =   17220
   ClientLeft      =   3060
   ClientTop       =   3210
   ClientWidth     =   28695
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   17220
   ScaleWidth      =   28695
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdGotoLink 
      Caption         =   "Goto"
      Height          =   315
      Left            =   27780
      TabIndex        =   62
      Top             =   11430
      Width           =   735
   End
   Begin VB.TextBox txtLinkForRequest 
      Height          =   2115
      Left            =   16980
      MultiLine       =   -1  'True
      TabIndex        =   61
      Top             =   11400
      Width           =   10695
   End
   Begin VB.CommandButton cmdClearSearchCompany 
      Caption         =   "Clear"
      Height          =   315
      Left            =   4740
      TabIndex        =   59
      Top             =   1140
      Width           =   615
   End
   Begin VB.ComboBox cmbSearchDueTime 
      Height          =   315
      Left            =   3180
      TabIndex        =   51
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   2580
      Width           =   1035
   End
   Begin VB.ComboBox cmbRequestTime 
      Height          =   315
      Left            =   3180
      TabIndex        =   50
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   2220
      Width           =   1035
   End
   Begin VB.ComboBox cmbSearchRequestType 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      ItemData        =   "frmTrackerMediaServices.frx":0000
      Left            =   1680
      List            =   "frmTrackerMediaServices.frx":0002
      TabIndex        =   48
      ToolTipText     =   "The Version for the Tape"
      Top             =   420
      Width           =   2955
   End
   Begin VB.ComboBox txtAssignedTo 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      ItemData        =   "frmTrackerMediaServices.frx":0004
      Left            =   1680
      List            =   "frmTrackerMediaServices.frx":0006
      TabIndex        =   47
      ToolTipText     =   "The Version for the Tape"
      Top             =   1860
      Width           =   2955
   End
   Begin VB.ComboBox cmbAssignedTo 
      Height          =   315
      ItemData        =   "frmTrackerMediaServices.frx":0008
      Left            =   16980
      List            =   "frmTrackerMediaServices.frx":000A
      TabIndex        =   36
      ToolTipText     =   "The Version for the Tape"
      Top             =   13920
      Width           =   3015
   End
   Begin VB.TextBox txtLocation 
      Height          =   315
      Left            =   16980
      TabIndex        =   35
      Top             =   13560
      Width           =   3015
   End
   Begin VB.TextBox txtJobID 
      Height          =   315
      Left            =   16980
      TabIndex        =   34
      Top             =   1740
      Width           =   3015
   End
   Begin VB.ComboBox cmbPriority 
      Height          =   315
      ItemData        =   "frmTrackerMediaServices.frx":000C
      Left            =   16980
      List            =   "frmTrackerMediaServices.frx":000E
      TabIndex        =   33
      ToolTipText     =   "The Version for the Tape"
      Top             =   660
      Width           =   3015
   End
   Begin VB.ComboBox cmbRequestType 
      Height          =   315
      ItemData        =   "frmTrackerMediaServices.frx":0010
      Left            =   16980
      List            =   "frmTrackerMediaServices.frx":0012
      TabIndex        =   32
      ToolTipText     =   "The Version for the Tape"
      Top             =   300
      Width           =   3015
   End
   Begin VB.TextBox txtTitle 
      Height          =   315
      Left            =   16980
      TabIndex        =   30
      Top             =   1020
      Width           =   6015
   End
   Begin VB.TextBox txtLongTask 
      Height          =   9195
      Left            =   16980
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   28
      Top             =   2100
      Width           =   11475
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete Courier"
      Height          =   255
      Index           =   7
      Left            =   5760
      TabIndex        =   27
      Tag             =   "NOCLEAR"
      Top             =   1680
      Width           =   2415
   End
   Begin VB.CommandButton cmdFixCdates 
      Caption         =   "Fix Dates"
      Height          =   615
      Left            =   8340
      TabIndex        =   26
      Top             =   120
      Visible         =   0   'False
      Width           =   2655
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete Van"
      Height          =   255
      Index           =   6
      Left            =   5760
      TabIndex        =   24
      Tag             =   "NOCLEAR"
      Top             =   1020
      Width           =   2415
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete Digital"
      Height          =   255
      Index           =   5
      Left            =   5760
      TabIndex        =   23
      Tag             =   "NOCLEAR"
      Top             =   720
      Width           =   2295
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete Physical"
      Height          =   255
      Index           =   3
      Left            =   5760
      TabIndex        =   22
      Tag             =   "NOCLEAR"
      Top             =   420
      Width           =   2235
   End
   Begin VB.TextBox txtSearchTitle 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   1680
      TabIndex        =   19
      Top             =   1500
      Width           =   2955
   End
   Begin VB.TextBox txtSearchJobID 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   1680
      TabIndex        =   17
      Top             =   780
      Width           =   2955
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Pending"
      ForeColor       =   &H00C00000&
      Height          =   255
      Index           =   1
      Left            =   5760
      TabIndex        =   12
      Tag             =   "NOCLEAR"
      Top             =   1980
      Visible         =   0   'False
      Width           =   1395
   End
   Begin VB.TextBox txtSearchRequestBy 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   1680
      TabIndex        =   10
      Top             =   60
      Width           =   2955
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "All Items"
      Height          =   255
      Index           =   4
      Left            =   5760
      TabIndex        =   8
      Tag             =   "NOCLEAR"
      Top             =   2580
      Width           =   1455
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Finished"
      Height          =   255
      Index           =   2
      Left            =   5760
      TabIndex        =   6
      Tag             =   "NOCLEAR"
      Top             =   2280
      Width           =   1395
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete"
      Height          =   255
      Index           =   0
      Left            =   5760
      TabIndex        =   5
      Tag             =   "NOCLEAR"
      Top             =   120
      Value           =   -1  'True
      Width           =   1395
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   5940
      TabIndex        =   1
      Top             =   15600
      Width           =   20775
      Begin VB.CommandButton cmdMarkComplete 
         Caption         =   "Mark Item as Complete"
         Height          =   315
         Left            =   10260
         TabIndex        =   52
         Top             =   0
         Width           =   2995
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         Height          =   315
         Left            =   18180
         TabIndex        =   31
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   15420
         TabIndex        =   21
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   19560
         TabIndex        =   4
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   16800
         TabIndex        =   3
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "New Request"
         Height          =   315
         Left            =   13740
         TabIndex        =   2
         Top             =   0
         Width           =   1515
      End
   End
   Begin MSAdodcLib.Adodc adoItems 
      Height          =   330
      Left            =   60
      Top             =   2940
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdItems 
      Bindings        =   "frmTrackerMediaServices.frx":0014
      Height          =   8355
      Left            =   60
      TabIndex        =   0
      Top             =   2940
      Width           =   15525
      ScrollBars      =   3
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   3
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTrackerMediaServices.frx":002B
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmTrackerMediaServices.frx":0047
      stylesets(2).Name=   "headerfield"
      stylesets(2).ForeColor=   0
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmTrackerMediaServices.frx":0063
      AllowDelete     =   -1  'True
      AllowUpdate     =   0   'False
      SelectTypeRow   =   3
      StyleSet        =   "headerfield"
      RowHeight       =   450
      ExtraHeight     =   26
      Columns.Count   =   20
      Columns(0).Width=   1164
      Columns(0).Caption=   "Item ID"
      Columns(0).Name =   "tracker_ms_itemID"
      Columns(0).DataField=   "tracker_ms_itemID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   2117
      Columns(1).Caption=   "Request Type"
      Columns(1).Name =   "MS_Request_Type"
      Columns(1).DataField=   "MS_Request_Type"
      Columns(1).FieldLen=   256
      Columns(2).Width=   2884
      Columns(2).Caption=   "Request Date"
      Columns(2).Name =   "Request_Date"
      Columns(2).DataField=   "Request_Date"
      Columns(2).FieldLen=   256
      Columns(2).StyleSet=   "headerfield"
      Columns(3).Width=   3200
      Columns(3).Caption=   "Request By"
      Columns(3).Name =   "Request_By"
      Columns(3).DataField=   "Request_By"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "Requester Email"
      Columns(4).Name =   "Requester_Email"
      Columns(4).DataField=   "Requester_Email"
      Columns(4).FieldLen=   256
      Columns(5).Width=   900
      Columns(5).Caption=   "For"
      Columns(5).Name =   "CompanyID"
      Columns(5).DataField=   "CompanyID"
      Columns(5).FieldLen=   256
      Columns(6).Width=   2064
      Columns(6).Caption=   "Prioritty"
      Columns(6).Name =   "Priority"
      Columns(6).DataField=   "Priority"
      Columns(6).FieldLen=   256
      Columns(6).StyleSet=   "headerfield"
      Columns(7).Width=   3889
      Columns(7).Caption=   "Title"
      Columns(7).Name =   "Title"
      Columns(7).DataField=   "Title"
      Columns(7).FieldLen=   256
      Columns(8).Width=   2884
      Columns(8).Caption=   "Due By"
      Columns(8).Name =   "DueBy"
      Columns(8).DataField=   "DueBy"
      Columns(8).FieldLen=   256
      Columns(8).StyleSet=   "headerfield"
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "Request"
      Columns(9).Name =   "Request"
      Columns(9).DataField=   "Request"
      Columns(9).FieldLen=   256
      Columns(9).StyleSet=   "headerfield"
      Columns(10).Width=   1323
      Columns(10).Caption=   "Job #"
      Columns(10).Name=   "JobID"
      Columns(10).DataField=   "JobID"
      Columns(10).FieldLen=   256
      Columns(10).StyleSet=   "headerfield"
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "Location"
      Columns(11).Name=   "Location"
      Columns(11).DataField=   "Location"
      Columns(11).FieldLen=   256
      Columns(11).StyleSet=   "headerfield"
      Columns(12).Width=   1984
      Columns(12).Caption=   "AssignedTo"
      Columns(12).Name=   "AssignedTo"
      Columns(12).DataField=   "AssignedTo"
      Columns(12).FieldLen=   256
      Columns(12).Style=   1
      Columns(12).StyleSet=   "stagefield"
      Columns(13).Width=   1879
      Columns(13).Caption=   "Complete"
      Columns(13).Name=   "Completed_Date"
      Columns(13).DataField=   "Completed_Date"
      Columns(13).DataType=   17
      Columns(13).FieldLen=   256
      Columns(13).StyleSet=   "stagefield"
      Columns(14).Width=   1984
      Columns(14).Caption=   "By"
      Columns(14).Name=   "Completed_By"
      Columns(14).DataField=   "Completed_By"
      Columns(14).FieldLen=   256
      Columns(14).Style=   1
      Columns(14).StyleSet=   "conclusionfield"
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "cdate"
      Columns(15).Name=   "cdate"
      Columns(15).DataField=   "cdate"
      Columns(15).FieldLen=   256
      Columns(15).StyleSet=   "conclusionfield"
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "cuser"
      Columns(16).Name=   "cuser"
      Columns(16).DataField=   "cuser"
      Columns(16).FieldLen=   256
      Columns(16).Locked=   -1  'True
      Columns(16).StyleSet=   "conclusionfield"
      Columns(17).Width=   3200
      Columns(17).Visible=   0   'False
      Columns(17).Caption=   "Date Mod"
      Columns(17).Name=   "mdate"
      Columns(17).DataField=   "mdate"
      Columns(17).FieldLen=   256
      Columns(17).VertScrollBar=   -1  'True
      Columns(17).Locked=   -1  'True
      Columns(17).StyleSet=   "conclusionfield"
      Columns(18).Width=   3200
      Columns(18).Visible=   0   'False
      Columns(18).Caption=   "muser"
      Columns(18).Name=   "muser"
      Columns(18).DataField=   "muser"
      Columns(18).FieldLen=   256
      Columns(18).Locked=   -1  'True
      Columns(18).StyleSet=   "conclusionfield"
      Columns(19).Width=   3200
      Columns(19).Visible=   0   'False
      Columns(19).Caption=   "LinkForRequest"
      Columns(19).Name=   "LinkForRequest"
      Columns(19).DataField=   "LinkForRequest"
      Columns(19).FieldLen=   256
      _ExtentX        =   27384
      _ExtentY        =   14737
      _StockProps     =   79
      Caption         =   "Tracker Items"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoComments 
      Height          =   330
      Left            =   2520
      Top             =   14580
      Visible         =   0   'False
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoComments"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdComments 
      Bindings        =   "frmTrackerMediaServices.frx":007F
      Height          =   1875
      Left            =   60
      TabIndex        =   11
      Top             =   11340
      Width           =   12855
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12582847
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   6
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "trackerhistoryID"
      Columns(0).Name =   "tracker_ms_commentID"
      Columns(0).DataField=   "tracker_ms_commentID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "trackerprogramID"
      Columns(1).Name =   "tracker_ms_itemID"
      Columns(1).DataField=   "tracker_ms_itemID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   13070
      Columns(2).Caption=   "Comments"
      Columns(2).Name =   "comment"
      Columns(2).DataField=   "comment"
      Columns(2).FieldLen=   255
      Columns(3).Width=   3360
      Columns(3).Caption=   "Date"
      Columns(3).Name =   "cdate"
      Columns(3).DataField=   "cdate"
      Columns(3).DataType=   7
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   3519
      Columns(4).Caption=   "Entered By"
      Columns(4).Name =   "cuser"
      Columns(4).DataField=   "cuser"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   1640
      Columns(5).Caption=   "Send Email"
      Columns(5).Name =   "Send Email"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   4
      UseDefaults     =   0   'False
      _ExtentX        =   22675
      _ExtentY        =   3307
      _StockProps     =   79
      Caption         =   "Tracker Comments"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker datRequestDate 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   1680
      TabIndex        =   13
      Tag             =   "NOCHECK"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   2220
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CalendarBackColor=   12632319
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   128843777
      CurrentDate     =   39580
   End
   Begin MSComCtl2.DTPicker datSearchDueDate 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   1680
      TabIndex        =   15
      Tag             =   "NOCHECK"
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   2580
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CalendarBackColor=   12632319
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   128843777
      CurrentDate     =   42765.3994212963
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   16980
      TabIndex        =   53
      ToolTipText     =   "The company this tape belongs to"
      Top             =   1380
      Width           =   4575
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "accountcode"
      Columns(2).Name =   "accountcode"
      Columns(2).DataField=   "accountcode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "telephone"
      Columns(3).Name =   "telephone"
      Columns(3).DataField=   "telephone"
      Columns(3).FieldLen=   256
      _ExtentX        =   8070
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16777215
      DataFieldToDisplay=   "name"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbSearchCompany 
      Height          =   315
      Left            =   1680
      TabIndex        =   56
      ToolTipText     =   "The company this tape belongs to"
      Top             =   1140
      Width           =   2955
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "accountcode"
      Columns(2).Name =   "accountcode"
      Columns(2).DataField=   "accountcode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "telephone"
      Columns(3).Name =   "telephone"
      Columns(3).DataField=   "telephone"
      Columns(3).FieldLen=   256
      _ExtentX        =   5212
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   12632319
      DataFieldToDisplay=   "name"
   End
   Begin VB.Label lblCaption 
      Caption         =   "Request Link"
      Height          =   255
      Index           =   17
      Left            =   15720
      TabIndex        =   60
      Top             =   11460
      Width           =   1095
   End
   Begin VB.Label lblSearchCompanyID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   9900
      TabIndex        =   58
      Tag             =   "CLEARFIELDS"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   16
      Left            =   120
      TabIndex        =   57
      Top             =   1200
      Width           =   1395
   End
   Begin VB.Label lblCompanyID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   21600
      TabIndex        =   55
      Tag             =   "CLEARFIELDS"
      Top             =   1380
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      Height          =   255
      Index           =   10
      Left            =   15720
      TabIndex        =   54
      Top             =   1440
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Request Type"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   120
      TabIndex        =   49
      Top             =   480
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Last Mod By"
      Height          =   255
      Index           =   15
      Left            =   15720
      TabIndex        =   46
      Top             =   15060
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Last Modified"
      Height          =   255
      Index           =   14
      Left            =   15720
      TabIndex        =   45
      Top             =   14700
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Assigned To"
      Height          =   255
      Index           =   13
      Left            =   15720
      TabIndex        =   44
      Top             =   13980
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Location"
      Height          =   255
      Index           =   12
      Left            =   15720
      TabIndex        =   43
      Top             =   13620
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job #"
      Height          =   255
      Index           =   11
      Left            =   15720
      TabIndex        =   42
      Top             =   1800
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Request Title"
      Height          =   255
      Index           =   9
      Left            =   15720
      TabIndex        =   41
      Top             =   1080
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Priority"
      Height          =   255
      Index           =   8
      Left            =   15720
      TabIndex        =   40
      Top             =   720
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Request Type"
      Height          =   255
      Index           =   7
      Left            =   15720
      TabIndex        =   39
      Top             =   360
      Width           =   1095
   End
   Begin VB.Label lblLastModifiedBy 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   16980
      TabIndex        =   38
      Tag             =   "CLEARFIELDS"
      Top             =   15000
      Width           =   3015
   End
   Begin VB.Label lblLastModifiedDate 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   16980
      TabIndex        =   37
      Tag             =   "CLEARFIELDS"
      Top             =   14640
      Width           =   3015
   End
   Begin VB.Label lblCaption 
      Caption         =   "Task Details"
      Height          =   255
      Index           =   6
      Left            =   15720
      TabIndex        =   29
      Top             =   2160
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Assigned To"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   25
      Top             =   1920
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Request Title"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   20
      Top             =   1560
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job #"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   18
      Top             =   840
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Due Date"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   16
      Top             =   2640
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Request Date"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   14
      Top             =   2280
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Request By"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   78
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   1395
   End
   Begin VB.Label lblTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   8820
      TabIndex        =   7
      Tag             =   "CLEARFIELDS"
      Top             =   1440
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "frmTrackerMediaServices"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strSearch As String
Dim msp_strSearch As String
Dim m_strOrderby As String
Dim m_blnDelete As Boolean
Dim m_blnDontVerifyXML As Boolean
Dim m_blnBilling As Boolean
Dim l_strDateSearch As String
Dim m_blnTimerPaused As Boolean

Private Sub cmbCompany_Click()

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

End Sub

Private Sub cmbPriority_Click()

    Select Case cmbPriority.Text
        Case "Low", "Low (Week)", "Low (Month)"
            cmbPriority.BackColor = &H70FFFF
        Case "Medium"
            cmbPriority.BackColor = &H70B0FF
        Case "High"
            cmbPriority.BackColor = &HA0A0FF
        Case "On Hold"
            cmbPriority.BackColor = &H7070FF
        Case Else
            cmbPriority.BackColor = &HFFFFFF
    End Select

End Sub

Private Sub cmbRequestTime_DropDown()
PopulateCombo "times", cmbRequestTime
End Sub

Private Sub cmbSearchCompany_Click()

lblSearchCompanyID.Caption = cmbSearchCompany.Columns("companyID").Text

End Sub

Private Sub cmbSearchDueTime_DropDown()
PopulateCombo "times", cmbSearchDueTime
End Sub

Private Sub cmdClear_Click()

ClearFields Me

End Sub

Private Sub cmdClearSearchCompany_Click()
lblSearchCompanyID.Caption = ""
cmbSearchCompany.Text = ""
End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdGotoLink_Click()

Dim l_strCommand As String

l_strCommand = txtLinkForRequest.Text

If l_strCommand <> "" Then
    ShellExecute 0&, "open", l_strCommand, vbNullString, vbNullString, vbNormalFocus
End If
    
End Sub

Private Sub cmdMarkComplete_Click()

Dim l_strSQL As String, l_strEmail As String, l_strEmailTo As String

If Val(lblTrackeritemID.Caption) = 0 Then Exit Sub
If cmbPriority.Text = "On Hold" Then
    MsgBox "Cannot mark an 'On Hold' item as complete"
    Exit Sub
End If

If cmdMarkComplete.Caption = "Mark Item as Complete" Then
    SetData "tracker_ms_item", "Completed_Date", "tracker_ms_itemID", Val(lblTrackeritemID.Caption), Format(Now, "YYYY-MM-DD HH:NN:SS")
    SetData "tracker_ms_item", "Completed_By", "tracker_ms_itemID", Val(lblTrackeritemID.Caption), g_strUserName
    If MsgBox("Send out Email notification about this change / update", vbYesNo + vbDefaultButton2, "Email Notification") = vbYes Then
        l_strEmail = "Request ID # " & lblTrackeritemID.Caption & " for: " & cmbRequestType.Text & ", Due by: " & GetData("tracker_ms_item", "DueBy", "tracker_ms_itemID", Val(lblTrackeritemID.Caption)) & vbCrLf
        l_strEmail = l_strEmail & "Request: " & txtLongTask.Text & vbCrLf
        If txtJobID.Text <> "" Then l_strEmail = l_strEmail & "Related Job #: " & txtJobID.Text & vbCrLf
        If txtLocation.Text <> "" Then l_strEmail = l_strEmail & "Location: " & txtLocation.Text & vbCrLf
        l_strEmail = l_strEmail & "Assigned to: " & cmbAssignedTo.Text & vbCrLf
        l_strEmail = l_strEmail & "has been marked complete by " & g_strFullUserName
        
        l_strEmailTo = GetData("tracker_ms_item", "Requester_email", "tracker_ms_itemID", lblTrackeritemID.Caption)
        SendSMTPMail l_strEmailTo, "", "MX1 Media Services Tracker Update", "", l_strEmail, True, "uk-mediaservices-VIE@visualdatamedia.com", "", g_strAdministratorEmailAddress
    End If
Else
    SetData "tracker_ms_item", "Completed_Date", "tracker_ms_itemID", Val(lblTrackeritemID.Caption), "Null"
    SetData "tracker_ms_item", "Completed_By", "tracker_ms_itemID", Val(lblTrackeritemID.Caption), "Null"
    If MsgBox("Send out Email notification about this change / update", vbYesNo + vbDefaultButton2, "Email Notification") = vbYes Then
        l_strEmail = "Request ID # " & lblTrackeritemID.Caption & " for: " & cmbRequestType.Text & ", Due by: " & GetData("tracker_ms_item", "DueBy", "tracker_ms_itemID", Val(lblTrackeritemID.Caption)) & vbCrLf
        l_strEmail = l_strEmail & "Request: " & txtLongTask.Text & vbCrLf
        If txtJobID.Text <> "" Then l_strEmail = l_strEmail & "Related Job #: " & txtJobID.Text & vbCrLf
        If txtLocation.Text <> "" Then l_strEmail = l_strEmail & "Location: " & txtLocation.Text & vbCrLf
        l_strEmail = l_strEmail & "Assigned to: " & cmbAssignedTo.Text & vbCrLf
        l_strEmail = l_strEmail & "has been marked as not complete by " & g_strFullUserName
        
        l_strEmailTo = GetData("tracker_ms_item", "Requester_email", "tracker_ms_itemID", lblTrackeritemID.Caption)
        SendSMTPMail l_strEmailTo, "", "MX1 Media Services Tracker Update", "", l_strEmail, True, "uk-mediaservices-VIE@visualdatamedia.com", "", g_strAdministratorEmailAddress
    End If
End If

'MsgBox "pressing search button"
cmdSearch.Value = True

End Sub

Private Sub cmdPrint_Click()

If lblTrackeritemID.Caption = "" Then Exit Sub
PrintCrystalReport g_strLocationOfCrystalReportFiles & "MediaServicesTrackerReport.rpt", "{tracker_ms_item.tracker_ms_itemID} = " & lblTrackeritemID.Caption, g_blnPreviewReport

End Sub

Private Sub cmdSave_Click()

Dim l_strSQL As String, Bookmark As Variant, l_strEmailTo As String, l_strEmail As String, l_blnCompulsoryComment As Boolean, l_DatDueBy As Date, l_strComment As String, l_blnRejected As Boolean
Dim l_lngRequestID As Long, l_strRequestType As String, l_strRequestTitle As String, l_strRequest As String, l_strRequesterEmail As String

If lblCompanyID.Caption = "" Then
    MsgBox "A Client must be selected for this request", vbCritical, "Request Not Saved"
    Exit Sub
End If

If Val(lblTrackeritemID.Caption) = 0 Then

    Select Case cmbPriority.Text
        Case "Pathe"
            l_DatDueBy = DateAdd("h", 2, Now)
        Case "High"
            l_DatDueBy = DateAdd("h", 12, Now)
        Case "Medium"
            l_DatDueBy = DateAdd("h", 24, Now)
        Case "Low"
            l_DatDueBy = DateAdd("h", 48, Now)
        Case "Low (Week)"
            l_DatDueBy = DateAdd("h", 168, Now)
        Case "Low (Month)"
            l_DatDueBy = DateAdd("h", 672, Now)
        Case Else
            l_DatDueBy = 0
    End Select
    
    'New item - Insert statement
    l_strSQL = "INSERT INTO tracker_ms_item (MS_Request_Type, CompanyID, Request_By, Request_Date, Requester_Email, Priority, Title, DueBy, Request, LinkForRequest, JobID, Location, AssignedTo, cdate, cuser, mdate, muser) VALUES ("
    l_strSQL = l_strSQL & "'" & cmbRequestType.Text & "', "
    l_strSQL = l_strSQL & "'" & lblCompanyID.Caption & "', "
    l_strSQL = l_strSQL & "'" & g_strFullUserName & "', "
    l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD HH:NN:SS") & "', "
    l_strSQL = l_strSQL & "'" & g_strUserEmailAddress & "', "
    l_strSQL = l_strSQL & "'" & cmbPriority.Text & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtTitle.Text) & "', "
    Select Case l_DatDueBy
        Case 0
            l_strSQL = l_strSQL & "Null, "
        Case Else
            l_strSQL = l_strSQL & "'" & Format(l_DatDueBy, "YYYY-MM-DD HH:NN:SS") & "', "
    End Select
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtLongTask.Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtLinkForRequest.Text) & "', "
    l_strSQL = l_strSQL & IIf(Val(txtJobID.Text) <> 0, Val(txtJobID.Text), "Null") & ", "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtLocation.Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbAssignedTo.Text) & "', "
    l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD HH:NN:SS") & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strUserName) & "', "
    l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD HH:NN:SS") & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strUserName) & "'); "
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
'    If MsgBox("Send out Email notification about this change / update", vbYesNo + vbDefaultButton2, "Email Notification") = vbYes Then
'        l_strEmail = "Request ID # " & g_lngLastID & " for: " & cmbRequestType.Text & ", Due by: " & IIf(l_DatDueBy <> 0, Format(l_DatDueBy, "YYYY-MM-DD HH:NN:SS"), "Null") & vbCrLf
'        l_strEmail = l_strEmail & "Title: " & grdItems.Columns("title").Text & vbCrLf
'        l_strEmail = l_strEmail & "Request: " & grdItems.Columns("Request").Text & vbCrLf
'        If txtJobID.Text <> "" Then l_strEmail = l_strEmail & "Related Job #: " & txtJobID.Text & vbCrLf
'        If txtLocation.Text <> "" Then l_strEmail = l_strEmail & "Location: " & txtLocation.Text & vbCrLf
'        l_strEmail = l_strEmail & "Assigned to: " & cmbAssignedTo.Text & vbCrLf
'        l_strEmail = l_strEmail & "has been added by " & g_strFullUserName
'
'        l_strEmailTo = g_strUserEmailAddress
'        SendSMTPMail l_strEmailTo, "", "MX1 Media Services Tracker Update", "", l_strEmail, True, "uk-mediaservices-VIE@visualdatamedia.com", "", g_strAdministratorEmailAddress
'    End If
    
    adoItems.Refresh
    adoItems.Recordset.MoveLast
        
Else

    If cmbPriority.Text <> GetData("tracker_ms_item", "Priority", "tracker_ms_itemID", Val(lblTrackeritemID.Caption)) Then
        Select Case cmbPriority.Text
            Case "On Hold"
                l_blnCompulsoryComment = True
                l_DatDueBy = 0
            Case "Rejected"
                l_blnCompulsoryComment = True
                l_DatDueBy = 0
                l_blnRejected = True
            Case Else
                If GetData("tracker_ms_item", "Priority", "tracker_ms_itemID", Val(lblTrackeritemID.Caption)) <> "On Hold" And GetData("tracker_ms_item", "Priority", "tracker_ms_itemID", Val(lblTrackeritemID.Caption)) <> "Rejected" Then
                    MsgBox "Cannot Change Priority between diffewrent time periods" & vbCrLf & "Item Not Saved"
                    Exit Sub
                End If
                Select Case cmbPriority.Text
                    Case "Pathe"
                        l_DatDueBy = DateAdd("h", 2, Now)
                    Case "High"
                        l_DatDueBy = DateAdd("h", 12, Now)
                    Case "Medium"
                        l_DatDueBy = DateAdd("h", 24, Now)
                    Case "Low"
                        l_DatDueBy = DateAdd("h", 48, Now)
                    Case "Low (Week)"
                        l_DatDueBy = DateAdd("h", 168, Now)
                    Case "Low (Month)"
                        l_DatDueBy = DateAdd("h", 672, Now)
                End Select
        End Select
    End If
    
    'Old item - update statement
    l_strSQL = "UPDATE tracker_ms_item SET "
    l_strSQL = l_strSQL & "MS_Request_Type = '" & cmbRequestType.Text & "', "
    l_strSQL = l_strSQL & "CompanyID = " & lblCompanyID.Caption & ", "
    l_strSQL = l_strSQL & "Priority = '" & cmbPriority.Text & "', "
    l_strSQL = l_strSQL & "Title = '" & QuoteSanitise(txtTitle.Text) & "', "
    If cmbPriority.Text <> GetData("tracker_ms_item", "Priority", "tracker_ms_itemID", Val(lblTrackeritemID.Caption)) Then
        Select Case l_DatDueBy
            Case 0
                l_strSQL = l_strSQL & "DueBy = Null, "
            Case Else
                l_strSQL = l_strSQL & "Dueby = '" & Format(l_DatDueBy, "YYYY-MM-DD HH:NN:SS") & "', "
        End Select
    End If
    l_strSQL = l_strSQL & "Request = '" & QuoteSanitise(txtLongTask.Text) & "', "
    l_strSQL = l_strSQL & "LinkForRequest = '" & QuoteSanitise(txtLinkForRequest.Text) & "', "
    l_strSQL = l_strSQL & "JobID = " & IIf(Val(txtJobID.Text) <> 0, Val(txtJobID.Text), "Null") & ", "
    l_strSQL = l_strSQL & "Location = '" & QuoteSanitise(txtLocation.Text) & "', "
    l_strSQL = l_strSQL & "AssignedTo = '" & QuoteSanitise(cmbAssignedTo.Text) & "', "
    l_strSQL = l_strSQL & "mdate = '" & Format(Now, "YYYY-MM-DD HH:NN:SS") & "', "
    l_strSQL = l_strSQL & "muser = '" & QuoteSanitise(g_strUserName) & "'"
    If l_blnRejected = True Then
        l_strSQL = l_strSQL & ", Completed_Date = getdate(), Completed_By = '" & QuoteSanitise(g_strUserName) & "'"
        l_lngRequestID = grdItems.Columns("tracker_ms_itemID").Text
        l_strRequestType = grdItems.Columns("MS_Request_Type").Text
        l_strRequestTitle = grdItems.Columns("title").Text
        l_strRequest = grdItems.Columns("Request").Text
        l_strRequesterEmail = grdItems.Columns("Requester_email").Text
    End If
    l_strSQL = l_strSQL & " WHERE tracker_ms_itemID = " & Val(lblTrackeritemID.Caption) & ";"
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    On Error Resume Next
    If l_blnRejected = False Then
        Bookmark = adoItems.Recordset.Bookmark
        adoItems.Refresh
        adoItems.Recordset.Bookmark = Bookmark
    End If
    On Error GoTo 0
    If l_blnRejected = True Then
        Do
            l_strComment = InputBox("Please give a reason for this item being Rejected", "Compulsory Comment Required")
        Loop Until l_strComment <> ""
        
        If l_strComment <> "" Then
            l_strSQL = "INSERT INTO tracker_ms_comment (tracker_ms_itemID, cuser, comment) VALUES ("
            l_strSQL = l_strSQL & l_lngRequestID & ", "
            l_strSQL = l_strSQL & "'" & g_strUserName & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strComment) & "');"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            adoComments.Refresh
        End If
        l_strEmail = "Request ID # " & l_lngRequestID & " for: " & l_strRequestType & " & vbCrLf"
        l_strEmail = l_strEmail & "Title: " & l_strRequestTitle & vbCrLf
        l_strEmail = l_strEmail & "Request: " & l_strRequest & vbCrLf & vbCrLf
        adoComments.Recordset.MoveLast
        l_strEmail = l_strEmail & "Last Comment: " & adoComments.Recordset("comment") & vbCrLf
        l_strEmailTo = l_strRequesterEmail
        SendSMTPMail l_strEmailTo, "", "MX1 Media Services Tracker Item Rejected", "", l_strEmail, True, "uk-mediaservices-VIE@visualdatamedia.com", "", g_strAdministratorEmailAddress
    ElseIf l_blnCompulsoryComment = True Then
        Do
            l_strComment = InputBox("Please give a reason for this item being put to " & cmbPriority.Text, "Compulsory Comment Required")
        Loop Until l_strComment <> ""
        
        If l_strComment <> "" Then
            l_strSQL = "INSERT INTO tracker_ms_comment (tracker_ms_itemID, cuser, comment) VALUES ("
            l_strSQL = l_strSQL & lblTrackeritemID.Caption & ", "
            l_strSQL = l_strSQL & "'" & g_strUserName & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strComment) & "');"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            adoComments.Refresh
        End If
    End If
    If l_blnRejected = False Then
        If MsgBox("Send out Email notification about this change / update", vbYesNo + vbDefaultButton2, "Email Notification") = vbYes Then
            l_strEmail = "Request ID # " & grdItems.Columns("tracker_ms_itemID").Text & " for: " & grdItems.Columns("MS_Request_Type").Text & ", Due by: " & grdItems.Columns("Dueby").Text & vbCrLf
            l_strEmail = l_strEmail & "Title: " & grdItems.Columns("title").Text & vbCrLf
            l_strEmail = l_strEmail & "Request: " & grdItems.Columns("Request").Text & vbCrLf
            If grdItems.Columns("jobID").Text <> "" Then l_strEmail = l_strEmail & "Related Job #: " & grdItems.Columns("jobID").Text & vbCrLf
            If grdItems.Columns("location").Text <> "" Then l_strEmail = l_strEmail & "Location: " & grdItems.Columns("location").Text & vbCrLf
            l_strEmail = l_strEmail & "Assigned to: " & grdItems.Columns("AssignedTo").Text & vbCrLf
            If grdItems.Columns("Completed_Date").Text <> "" Then
                l_strEmail = l_strEmail & "has been marked complete by " & grdItems.Columns("Completed_By").Text
            ElseIf grdItems.Columns("mdate").Text <> "" Then
                l_strEmail = l_strEmail & "has been updated by " & grdItems.Columns("muser").Text
            ElseIf grdItems.Columns("cdate").Text = "" Then
                l_strEmail = l_strEmail & "has been added by " & grdItems.Columns("cuser").Text
            End If
            
            If adoComments.Recordset.RecordCount > 0 Then
                adoComments.Recordset.MoveFirst
                l_strEmail = l_strEmail & vbCrLf & vbCrLf & "Comments about this Request:" & vbCrLf
                Do While Not adoComments.Recordset.EOF
                    l_strEmail = l_strEmail & Format(adoComments.Recordset("cdate"), "YYYY-MM-DD HH:NN:SS") & Chr(9) & adoComments.Recordset("cuser") & Chr(9) & adoComments.Recordset("comment") & vbCrLf
                    adoComments.Recordset.MoveNext
                Loop
            End If
            
            l_strEmailTo = grdItems.Columns("Requester_email").Text
            SendSMTPMail l_strEmailTo, "", "MX1 Media Services Tracker Update", "", l_strEmail, True, "uk-mediaservices-VIE@visualdatamedia.com", "", g_strAdministratorEmailAddress
        End If
    End If
End If

End Sub

Private Sub cmdSearch_Click()

Dim l_strDateSearch As String, l_strRequestDate As String, l_strDueDate As String, l_strSQL As String

If Not IsNull(datRequestDate.Value) Then
    l_strRequestDate = Format(datRequestDate.Value, "YYYY-MM-DD")
    If cmbRequestTime.Text <> "" Then
        l_strRequestDate = l_strRequestDate & " " & cmbRequestTime.Text
    End If
    l_strDateSearch = " AND (Request_Date >= '" & l_strRequestDate & "' AND Request_Date < '" & Format(DateAdd("D", 1, CDate(l_strRequestDate)), "YYYY-MM-DD HH:NN:SS") & "') "
Else
    l_strDateSearch = ""
End If
If Not IsNull(datSearchDueDate.Value) Then
    l_strDueDate = Format(datSearchDueDate.Value, "YYYY-MM-DD")
    If cmbSearchDueTime.Text <> "" Then
        l_strDueDate = l_strDueDate & " " & cmbSearchDueTime.Text
    End If
    l_strDateSearch = l_strDateSearch & " AND (DueBy >= '" & l_strDueDate & "' AND DueBy < '" & Format(DateAdd("D", 1, CDate(l_strDueDate)), "YYYY-MM-DD HH:NN:SS") & "') "
End If
adoItems.ConnectionString = g_strConnection
msp_strSearch = SearchSQLstr()
adoItems.RecordSource = "SELECT * FROM tracker_ms_item " & m_strSearch & l_strDateSearch & msp_strSearch & m_strOrderby & ";"
Debug.Print adoItems.RecordSource
adoItems.Refresh

adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"

If Not adoItems.Recordset.EOF Then
    If adoItems.Recordset.RecordCount > 1 Then
        adoItems.Recordset.MoveLast
        adoItems.Recordset.MoveFirst
    Else
        lblTrackeritemID.Caption = grdItems.Columns("tracker_ms_itemID").Text
        cmbRequestType.Text = grdItems.Columns("MS_Request_Type").Text
        cmbPriority.Text = grdItems.Columns("Priority").Text
        Select Case cmbPriority.Text
            Case "Low"
                cmbPriority.BackColor = &H70FFFF
            Case "Medium"
                cmbPriority.BackColor = &H70B0FF
            Case "High"
                cmbPriority.BackColor = &HA0A0FF
            Case "On Hold"
                cmbPriority.BackColor = &H7070FF
            Case Else
                cmbPriority.BackColor = &HFFFFFF
        End Select
        txtTitle.Text = grdItems.Columns("Title").Text
        txtLongTask.Text = grdItems.Columns("Request").Text
        txtJobID.Text = grdItems.Columns("jobID").Text
        cmbAssignedTo.Text = grdItems.Columns("AssignedTo").Text
        lblLastModifiedDate.Caption = grdItems.Columns("mdate").Text
        lblLastModifiedBy.Caption = grdItems.Columns("muser").Text
        If grdItems.Columns("completed_date").Text <> "" Then
            cmdMarkComplete.Caption = "Un-Mark Item as Complete"
        Else
            cmdMarkComplete.Caption = "Mark Item as Complete"
        End If
        If Val(lblTrackeritemID.Caption) = 0 Then
            
            l_strSQL = "SELECT * FROM tracker_ms_comment WHERE tracker_ms_itemID = -1 ORDER BY cdate;"
            
            adoComments.RecordSource = l_strSQL
            adoComments.ConnectionString = g_strConnection
            adoComments.Refresh
            
            Exit Sub
        
        End If
        
        l_strSQL = "SELECT * FROM tracker_ms_comment WHERE tracker_ms_itemID = " & lblTrackeritemID.Caption & " ORDER BY cdate;"
        
        adoComments.RecordSource = l_strSQL
        adoComments.ConnectionString = g_strConnection
        adoComments.Refresh
    End If
Else
    ClearFields Me
End If

End Sub

Function SearchSQLstr() As String

Dim l_strSQL As String

l_strSQL = ""

If optComplete(0).Value = True Then 'Not Complete
    l_strSQL = l_strSQL & " AND (completed_date IS NULL) "
'ElseIf optComplete(1).Value = True Then 'Pending
'    l_strSQL = l_strSQL & " AND (readytobill IS NULL OR readytobill = 0) AND rejected <> 0 "
ElseIf optComplete(2).Value = True Then 'Complete
    l_strSQL = l_strSQL & "AND (completed_date IS NOT NULL) "
ElseIf optComplete(3).Value = True Then 'Not Complete Physical
    l_strSQL = l_strSQL & " AND (completed_date IS NULL) AND (MS_Request_Type LIKE 'Physical%')"
ElseIf optComplete(4).Value = True Then
    l_strSQL = l_strSQL & "AND 1=1 "
ElseIf optComplete(5).Value = True Then 'Not Complete Digital
    l_strSQL = l_strSQL & " AND (completed_date IS NULL) AND (MS_Request_Type LIKE 'Digital%')"
ElseIf optComplete(6).Value = True Then 'Not Complete Van
    l_strSQL = l_strSQL & " AND (completed_date IS NULL) AND (MS_Request_Type = 'Van')"
ElseIf optComplete(7).Value = True Then 'Not Complete Courier
    l_strSQL = l_strSQL & " AND (completed_date IS NULL) AND (MS_Request_Type = 'Courier')"
End If

If txtSearchRequestBy.Text <> "" Then
    l_strSQL = l_strSQL & " AND Request_BY LIKE '" & QuoteSanitise(txtSearchRequestBy.Text) & "%' "
End If

If cmbSearchRequestType.Text <> "" Then
    l_strSQL = l_strSQL & " AND MS_Request_Type = '" & QuoteSanitise(cmbSearchRequestType.Text) & "%' "
End If

If txtSearchJobID.Text <> "" Then
    l_strSQL = l_strSQL & " AND JobID = " & Val(txtSearchJobID.Text) & " "
End If

If txtAssignedTo.Text <> "" Then
    l_strSQL = l_strSQL & " AND Assignedto LIKE '" & QuoteSanitise(txtAssignedTo.Text) & "%' "
End If

If txtSearchTitle.Text <> "" Then
    l_strSQL = l_strSQL & " AND Title LIKE '" & QuoteSanitise(txtSearchTitle.Text) & "%' "
End If

If lblSearchCompanyID.Caption <> "" Then
    l_strSQL = l_strSQL & " AND companyID = " & Val(lblSearchCompanyID.Caption) & " "
End If


SearchSQLstr = l_strSQL

End Function

Private Sub cmdFixCdates_Click()

Dim rs As ADODB.Recordset

Set rs = ExecuteSQL("SELECT * FROM tracker_ms_item", g_strExecuteError)

If rs.RecordCount > 0 Then
    rs.MoveFirst
    Do While Not rs.EOF
        rs("cdate") = Format(rs("cdate"), "YYYY-MM-DD HH:NN:SS")
        rs.Update
        rs.MoveNext
    Loop
End If
rs.Close
Set rs = Nothing

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

l_strSQL = "SELECT name, accountcode, telephone, companyID FROM company WHERE (iscustomer = 1 OR isprospective = 1) AND system_active = 1 ORDER BY name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset
Dim l_rstSearch2 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset
Set l_rstSearch2 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

With l_rstSearch2
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch2.ActiveConnection = Nothing

Set cmbSearchCompany.DataSourceList = l_rstSearch2

l_conSearch.Close
Set l_conSearch = Nothing

grdItems.StyleSets("headerfield").BackColor = &HE7FFE7
grdItems.StyleSets("stagefield").BackColor = &HE7FFFF
grdItems.StyleSets("conclusionfield").BackColor = &HFFFFE7

grdItems.StyleSets.Add "Error"
grdItems.StyleSets("Error").BackColor = &H7070FF

grdItems.StyleSets.Add "PriorityLow"
grdItems.StyleSets("PriorityLow").BackColor = &H70FFFF

grdItems.StyleSets.Add "PriorityMedium"
grdItems.StyleSets("PriorityMedium").BackColor = &H70B0FF

grdItems.StyleSets.Add "PriorityHigh"
grdItems.StyleSets("PriorityHigh").BackColor = &HA0A0FF

PopulateCombo "priority", cmbPriority
PopulateCombo "MSRequestType", cmbRequestType
PopulateCombo "MSRequestType", cmbSearchRequestType
PopulateCombo "mediaservices", cmbAssignedTo
PopulateCombo "mediaservices", txtAssignedTo

optComplete(0).Value = True

m_strSearch = " WHERE 1 = 1 "
'm_strOrderby = " ORDER BY urgent, CASE WHEN newworkontoday IS NULL THEN 1 ELSE 0 END, newworkontoday, CASE WHEN targetdate IS NULL THEN 1 ELSE 0 END, targetdate, CASE WHEN duedate IS NULL THEN 1 ELSE 0 END, duedate, uniqueID, title, componenttype DESC, complete;"

cmdSearch.Value = True

End Sub

Private Sub Form_Resize()

On Error Resume Next

'grdItems.Width = Me.ScaleWidth - grdItems.Left - 120
grdItems.Height = (Me.ScaleHeight - grdItems.Top - frmButtons.Height) * 0.6 - 240
grdComments.Top = grdItems.Top + grdItems.Height + 120
grdComments.Height = (Me.ScaleHeight - grdComments.Top - frmButtons.Height) - 240
'txtLongTask.Top = grdComments.Top
'txtLongTask.Height = grdComments.Height
'lblCaption(6).Top = grdComments.Top
frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120

End Sub

Private Sub grdComments_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdComments_AfterUpdate(RtnDispErrMsg As Integer)

If m_blnDelete = False Then
    adoComments.Refresh
End If

End Sub

Private Sub grdComments_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdComments_BeforeUpdate(Cancel As Integer)

If m_blnDelete = False Then

    If Val(lblTrackeritemID.Caption) = 0 Then
        Cancel = 1
        Exit Sub
    End If
    
    If grdComments.Columns("comment").Text = "" Then
        MsgBox "Cannot save a Comment with no actual comment", vbCritical, "Comment Not Saved"
        Cancel = True
        Exit Sub
    End If
           
    grdComments.Columns("tracker_ms_itemID").Text = lblTrackeritemID.Caption
    grdComments.Columns("cuser").Text = g_strFullUserName
    
End If

End Sub

Private Sub grdComments_BtnClick()

Dim l_strEmail As String, l_strEmailTo As String

If MsgBox("Send out Email notification about this comment", vbYesNo + vbDefaultButton2, "Email Notification") = vbYes Then
    l_strEmail = "Request for: " & grdItems.Columns("MS_Request_Type").Text & ", Due by: " & grdItems.Columns("Dueby").Text & vbCrLf
    l_strEmail = l_strEmail & "Title: " & grdItems.Columns("title").Text & vbCrLf
    l_strEmail = l_strEmail & "Request: " & grdItems.Columns("Request").Text & vbCrLf
    If grdItems.Columns("jobID").Text <> "" Then l_strEmail = l_strEmail & "Related Job #: " & grdItems.Columns("jobID").Text & vbCrLf
    If grdItems.Columns("location").Text <> "" Then l_strEmail = l_strEmail & "Location: " & grdItems.Columns("location").Text & vbCrLf
    l_strEmail = l_strEmail & "Assigned to: " & grdItems.Columns("AssignedTo").Text & vbCrLf
    l_strEmail = l_strEmail & "Has a new / changed comment by " & grdComments.Columns("cuser").Text
    l_strEmail = l_strEmail & " on " & grdComments.Columns("cdate").Text & vbCrLf
    l_strEmail = l_strEmail & "Comment: " & grdComments.Columns("comment").Text
    
    l_strEmailTo = grdItems.Columns("Requester_email").Text
    SendSMTPMail l_strEmailTo, "", "MX1 Media Services Tracker Comment Update", "", l_strEmail, True, "uk-mediaservices-VIE@visualdatamedia.com", "", g_strAdministratorEmailAddress
End If

End Sub

Private Sub grdItems_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
adoItems.Refresh
adoItems.Recordset.MoveLast
adoItems.Recordset.MoveFirst
End Sub

Private Sub grdItems_AfterInsert(RtnDispErrMsg As Integer)

m_blnTimerPaused = False

End Sub

Private Sub grdItems_AfterUpdate(RtnDispErrMsg As Integer)

m_blnTimerPaused = False

End Sub

Private Sub grdItems_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

If Not CheckAccess("/DeleteMediaServices") Then
    Cancel = 1
    Exit Sub
End If

m_blnDelete = True

End Sub

Private Sub grdItems_BeforeInsert(Cancel As Integer)

m_blnTimerPaused = True

End Sub

Private Sub grdItems_BeforeUpdate(Cancel As Integer)

Dim l_strEmail As String, l_strEmailTo As String

If m_blnDelete = True Then Exit Sub

If grdItems.Columns("Request").Text = "" Then
    MsgBox "Cannot save a Request without Requesting something."
    Cancel = True
    Exit Sub
End If

If grdItems.Columns("MS_Request_Type").Text = "" Then
    MsgBox "Please give the Request Type"
    Cancel = True
    Exit Sub
End If

If grdItems.Columns("Request_By").Text = "" Then grdItems.Columns("Request_By").Text = g_strFullUserName
If grdItems.Columns("Request_Date").Text = "" Then grdItems.Columns("Request_Date").Text = Now

If grdItems.Columns("Requester_Email").Text <> "" Then
    If ValidateEmail(grdItems.Columns("Requester_Email").Text) = False Then
        MsgBox "Invalid Email address provided"
        Cancel = True
        Exit Sub
    End If
Else
    grdItems.Columns("Requester_Email").Text = g_strUserEmailAddress
End If

If grdItems.Columns("Completed_Date").Text <> "" And IsNull(adoItems.Recordset("Completed_Date")) Then
    grdItems.Columns("Completed_By").Text = g_strFullUserName
End If

grdItems.Columns("mdate").Text = Now
grdItems.Columns("muser").Text = g_strFullUserName
If grdItems.Columns("cuser").Text = "" Then grdItems.Columns("cuser").Text = g_strFullUserName
If grdItems.Columns("cdate").Text = "" Then grdItems.Columns("cdate").Text = Now

If txtLongTask.Text <> "" Then grdItems.Columns("Request").Text = txtLongTask.Text

If MsgBox("Send out Email notification about this change / update", vbYesNo + vbDefaultButton2, "Email Notification") = vbYes Then
    l_strEmail = "Request ID # " & grdItems.Columns("tracker_ms_itemID").Text & " for: " & grdItems.Columns("MS_Request_Type").Text & ", Due by: " & grdItems.Columns("Dueby").Text & vbCrLf
    l_strEmail = l_strEmail & "Title: " & grdItems.Columns("title").Text & vbCrLf
    l_strEmail = l_strEmail & "Request: " & grdItems.Columns("Request").Text & vbCrLf
    If grdItems.Columns("jobID").Text <> "" Then l_strEmail = l_strEmail & "Related Job #: " & grdItems.Columns("jobID").Text & vbCrLf
    If grdItems.Columns("location").Text <> "" Then l_strEmail = l_strEmail & "Location: " & grdItems.Columns("location").Text & vbCrLf
    l_strEmail = l_strEmail & "Assigned to: " & grdItems.Columns("AssignedTo").Text & vbCrLf
    If grdItems.Columns("Completed_Date").Text <> "" Then
        l_strEmail = l_strEmail & "has been marked complete by " & grdItems.Columns("Completed_By").Text
    ElseIf grdItems.Columns("cdate").Text <> "" Then
        l_strEmail = l_strEmail & "has been updated by " & grdItems.Columns("muser").Text
    ElseIf grdItems.Columns("cdate").Text = "" Then
        l_strEmail = l_strEmail & "has been added by " & grdItems.Columns("muser").Text
    End If
    
    l_strEmailTo = grdItems.Columns("Requester_email").Text
    SendSMTPMail l_strEmailTo, "", "MX1 Media Services Tracker Update", "", l_strEmail, True, "uk-mediaservices-VIE@visualdatamedia.com", "", g_strAdministratorEmailAddress
End If

End Sub

Private Sub grdItems_BtnClick()

Dim tempdate As String

Select Case LCase(grdItems.Columns(grdItems.Col).Name)

Case "assignedto", "completed_by"

    If grdItems.ActiveCell.Text <> "" Then
        grdItems.ActiveCell.Text = ""
    Else
        grdItems.ActiveCell.Text = g_strFullUserName
    End If

Case Else
    
    If grdItems.ActiveCell.Text <> "" Then
        grdItems.ActiveCell.Text = ""
    Else
        grdItems.ActiveCell.Text = Now
    End If

End Select

End Sub

Private Sub grdItems_GotFocus()

'MsgBox "Grid GotFocus"
m_blnTimerPaused = True

End Sub

Private Sub grdItems_LostFocus()

'If grdItems.DataChanged = False Then
'    m_blnTimerPaused = False
'    MsgBox "Grid LostFocus"
'End If

End Sub

Private Sub grdItems_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim l_strSQL As String

lblTrackeritemID.Caption = grdItems.Columns("tracker_ms_itemID").Text
cmbRequestType.Text = grdItems.Columns("MS_Request_Type").Text
cmbPriority.Text = grdItems.Columns("Priority").Text
Select Case cmbPriority.Text
    Case "Low"
        cmbPriority.BackColor = &H70FFFF
    Case "Medium"
        cmbPriority.BackColor = &H70B0FF
    Case "High", "Pathe"
        cmbPriority.BackColor = &HA0A0FF
    Case "On Hold"
        cmbPriority.BackColor = &H7070FF
    Case "Rejected"
        cmbPriority.BackColor = &H7070FF
    Case Else
        cmbPriority.BackColor = &HFFFFFF
End Select
txtTitle.Text = grdItems.Columns("Title").Text
txtLongTask.Text = grdItems.Columns("Request").Text
txtLinkForRequest.Text = grdItems.Columns("LinkForRequest").Text
txtJobID.Text = grdItems.Columns("jobID").Text
txtLocation.Text = grdItems.Columns("location").Text
cmbAssignedTo.Text = grdItems.Columns("AssignedTo").Text
lblLastModifiedDate.Caption = grdItems.Columns("mdate").Text
lblLastModifiedBy.Caption = grdItems.Columns("muser").Text
lblCompanyID.Caption = grdItems.Columns("companyID").Text
If Val(lblCompanyID.Caption) <> 0 Then
    cmbCompany.Text = GetData("company", "name", "companyID", Val(lblCompanyID.Caption))
Else
    cmbCompany.Text = ""
End If
If grdItems.Columns("completed_date").Text <> "" Then
    cmdMarkComplete.Caption = "Un-Mark Item as Complete"
Else
    cmdMarkComplete.Caption = "Mark Item as Complete"
End If
If Val(lblTrackeritemID.Caption) = 0 Then
    
    l_strSQL = "SELECT * FROM tracker_ms_comment WHERE tracker_ms_itemID = -1 ORDER BY cdate;"
    
    adoComments.RecordSource = l_strSQL
    adoComments.ConnectionString = g_strConnection
    adoComments.Refresh
    
    Exit Sub

End If

l_strSQL = "SELECT * FROM tracker_ms_comment WHERE tracker_ms_itemID = " & lblTrackeritemID.Caption & " ORDER BY cdate;"

adoComments.RecordSource = l_strSQL
adoComments.ConnectionString = g_strConnection
adoComments.Refresh

End Sub

Private Sub grdItems_RowLoaded(ByVal Bookmark As Variant)

Select Case grdItems.Columns("Priority").Text

    Case "Low", "Low (Week)", "Low (Month)"
        grdItems.Columns("Priority").CellStyleSet "PriorityLow"
    Case "Medium"
        grdItems.Columns("Priority").CellStyleSet "PriorityMedium"
    Case "High", "Pathe"
        grdItems.Columns("Priority").CellStyleSet "PriorityHigh"
    Case "On Hold", "Rejected"
        grdItems.Columns("Priority").CellStyleSet "Error"

End Select

End Sub

Private Sub optComplete_Click(Index As Integer)

cmdSearch.Value = True

End Sub

