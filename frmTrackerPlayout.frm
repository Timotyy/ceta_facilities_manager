VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmTrackerPlayout 
   Caption         =   "Playout Tracker"
   ClientHeight    =   15795
   ClientLeft      =   3060
   ClientTop       =   3210
   ClientWidth     =   28560
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   15795
   ScaleWidth      =   28560
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.CheckBox chkPlayout 
      Alignment       =   1  'Right Justify
      Caption         =   "Playout 6"
      Height          =   315
      Index           =   6
      Left            =   4860
      TabIndex        =   32
      Top             =   2700
      Width           =   2175
   End
   Begin VB.CommandButton cmdMakeOrder 
      Caption         =   "Make New Order"
      Height          =   375
      Left            =   120
      TabIndex        =   31
      Top             =   900
      Width           =   2655
   End
   Begin VB.CheckBox chkPlayout 
      Alignment       =   1  'Right Justify
      Caption         =   "Playout 4"
      Height          =   315
      Index           =   4
      Left            =   4860
      TabIndex        =   30
      Top             =   1980
      Width           =   2175
   End
   Begin VB.CheckBox chkPlayout 
      Alignment       =   1  'Right Justify
      Caption         =   "Playout 3"
      Height          =   315
      Index           =   3
      Left            =   4860
      TabIndex        =   29
      Top             =   1620
      Width           =   2175
   End
   Begin VB.CheckBox chkPlayout 
      Alignment       =   1  'Right Justify
      Caption         =   "Playout 5"
      Height          =   315
      Index           =   5
      Left            =   4860
      TabIndex        =   28
      Top             =   2340
      Width           =   2175
   End
   Begin VB.CheckBox chkPlayout 
      Alignment       =   1  'Right Justify
      Caption         =   "Playout 2"
      Height          =   315
      Index           =   2
      Left            =   4860
      TabIndex        =   27
      Top             =   1260
      Width           =   2175
   End
   Begin VB.CheckBox chkPlayout 
      Alignment       =   1  'Right Justify
      Caption         =   "Playout 1"
      Height          =   315
      Index           =   1
      Left            =   4860
      TabIndex        =   26
      Tag             =   "SET"
      Top             =   900
      Value           =   1  'Checked
      Width           =   2175
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Job Complete"
      Height          =   255
      Index           =   3
      Left            =   13200
      TabIndex        =   25
      Tag             =   "NOCLEAR"
      Top             =   480
      Width           =   2355
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnChannel 
      Height          =   1755
      Left            =   540
      TabIndex        =   24
      Top             =   5100
      Width           =   6915
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   8819
      Columns(0).Caption=   "Supplier"
      Columns(0).Name =   "Supplier"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   12197
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Rejected"
      Height          =   255
      Index           =   4
      Left            =   13200
      TabIndex        =   23
      Tag             =   "NOCLEAR"
      Top             =   1080
      Width           =   2355
   End
   Begin VB.CheckBox chkHideDemo 
      Alignment       =   1  'Right Justify
      Caption         =   "Hide Demo"
      Height          =   255
      Left            =   180
      TabIndex        =   21
      Tag             =   "NOCLEAR"
      Top             =   480
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.TextBox txtBBCOrderNumber 
      Height          =   315
      Left            =   6840
      TabIndex        =   17
      Top             =   480
      Width           =   3315
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "All Items"
      Height          =   255
      Index           =   0
      Left            =   13200
      TabIndex        =   15
      Tag             =   "NOCLEAR"
      Top             =   1380
      Width           =   2355
   End
   Begin VB.TextBox txtSeries 
      Height          =   315
      Left            =   11400
      TabIndex        =   12
      Top             =   120
      Width           =   1455
   End
   Begin VB.TextBox txtEpisode 
      Height          =   315
      Left            =   11400
      TabIndex        =   11
      Top             =   480
      Width           =   1455
   End
   Begin VB.TextBox txtTitle 
      Height          =   315
      Left            =   6840
      TabIndex        =   9
      Top             =   120
      Width           =   3315
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Job Billed"
      Height          =   255
      Index           =   2
      Left            =   13200
      TabIndex        =   6
      Tag             =   "NOCLEAR"
      Top             =   780
      Width           =   2355
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Job Not Complete"
      Height          =   255
      Index           =   1
      Left            =   13200
      TabIndex        =   5
      Tag             =   "NOCLEAR"
      Top             =   180
      Value           =   -1  'True
      Width           =   2355
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   5940
      TabIndex        =   1
      Top             =   14760
      Width           =   19755
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   18480
         TabIndex        =   4
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   17220
         TabIndex        =   3
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   15960
         TabIndex        =   2
         Top             =   0
         Width           =   1215
      End
   End
   Begin MSAdodcLib.Adodc adoItems 
      Height          =   330
      Left            =   120
      Top             =   2880
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdItems 
      Bindings        =   "frmTrackerPlayout.frx":0000
      Height          =   9375
      Left            =   180
      TabIndex        =   0
      Top             =   3180
      Width           =   25020
      ScrollBars      =   3
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   3
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTrackerPlayout.frx":0017
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmTrackerPlayout.frx":0033
      stylesets(2).Name=   "headerfield"
      stylesets(2).ForeColor=   0
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmTrackerPlayout.frx":004F
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      StyleSet        =   "headerfield"
      RowHeight       =   476
      ExtraHeight     =   26
      Columns.Count   =   44
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "trackerplayoutID"
      Columns(0).Name =   "trackerplayoutID"
      Columns(0).DataField=   "trackerplayoutID"
      Columns(0).FieldLen=   256
      Columns(0).StyleSet=   "headerfield"
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(1).StyleSet=   "headerfield"
      Columns(2).Width=   2990
      Columns(2).Caption=   "Alpha Code"
      Columns(2).Name =   "masterfilereference"
      Columns(2).DataField=   "masterfilereference"
      Columns(2).FieldLen=   256
      Columns(2).StyleSet=   "headerfield"
      Columns(3).Width=   2461
      Columns(3).Caption=   "BBC Ordernumber"
      Columns(3).Name =   "bbcordernumber"
      Columns(3).DataField=   "bbcordernumber"
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "headerfield"
      Columns(4).Width=   3200
      Columns(4).Caption=   "BBC Order For"
      Columns(4).Name =   "bbcorderfor"
      Columns(4).DataField=   "bbcorderfor"
      Columns(4).FieldLen=   256
      Columns(4).StyleSet=   "headerfield"
      Columns(5).Width=   1508
      Columns(5).Caption=   "DADC ID"
      Columns(5).Name =   "dadcID"
      Columns(5).DataField=   "dadcID"
      Columns(5).FieldLen=   256
      Columns(5).StyleSet=   "headerfield"
      Columns(6).Width=   3519
      Columns(6).Caption=   "Title"
      Columns(6).Name =   "title"
      Columns(6).DataField=   "title"
      Columns(6).FieldLen=   256
      Columns(6).StyleSet=   "headerfield"
      Columns(7).Width=   714
      Columns(7).Caption=   "Sr."
      Columns(7).Name =   "seriesnumber"
      Columns(7).DataField=   "seriesnumber"
      Columns(7).FieldLen=   256
      Columns(7).StyleSet=   "headerfield"
      Columns(8).Width=   794
      Columns(8).Caption=   "Eps"
      Columns(8).Name =   "episodenumber"
      Columns(8).DataField=   "episodenumber"
      Columns(8).FieldLen=   256
      Columns(8).StyleSet=   "headerfield"
      Columns(9).Width=   3200
      Columns(9).Caption=   "Delivery Details"
      Columns(9).Name =   "deliverydetails"
      Columns(9).DataField=   "deliverydetails"
      Columns(9).FieldLen=   256
      Columns(9).StyleSet=   "headerfield"
      Columns(10).Width=   3200
      Columns(10).Caption=   "Special Instructions"
      Columns(10).Name=   "specialinstructions"
      Columns(10).DataField=   "specialinstructions"
      Columns(10).FieldLen=   256
      Columns(10).StyleSet=   "headerfield"
      Columns(11).Width=   1773
      Columns(11).Caption=   "jobID"
      Columns(11).Name=   "jobID"
      Columns(11).DataField=   "jobID"
      Columns(11).FieldLen=   256
      Columns(11).StyleSet=   "headerfield"
      Columns(12).Width=   3200
      Columns(12).Caption=   "jobstatus"
      Columns(12).Name=   "jobstatus"
      Columns(12).DataField=   "jobstatus"
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Caption=   "Due Date"
      Columns(13).Name=   "duedate"
      Columns(13).DataField=   "duedate"
      Columns(13).FieldLen=   256
      Columns(13).StyleSet=   "headerfield"
      Columns(14).Width=   1773
      Columns(14).Caption=   "Master ClipID"
      Columns(14).Name=   "masterclipID"
      Columns(14).DataField=   "masterclipID"
      Columns(14).FieldLen=   256
      Columns(14).StyleSet=   "headerfield"
      Columns(15).Width=   1508
      Columns(15).Caption=   "Length"
      Columns(15).Name=   "duration"
      Columns(15).DataField=   "duration"
      Columns(15).FieldLen=   256
      Columns(15).StyleSet=   "headerfield"
      Columns(16).Width=   714
      Columns(16).Caption=   "Dur."
      Columns(16).Name=   "durationinmins"
      Columns(16).DataField=   "durationinmins"
      Columns(16).FieldLen=   256
      Columns(16).StyleSet=   "headerfield"
      Columns(17).Width=   2117
      Columns(17).Caption=   "Master Here"
      Columns(17).Name=   "masterarrived"
      Columns(17).DataField=   "masterarrived"
      Columns(17).FieldLen=   256
      Columns(17).Style=   1
      Columns(17).StyleSet=   "stagefield"
      Columns(18).Width=   1773
      Columns(18).Caption=   "tapeformat1"
      Columns(18).Name=   "tapeformat1"
      Columns(18).DataField=   "tapeformat1"
      Columns(18).FieldLen=   256
      Columns(18).StyleSet=   "headerfield"
      Columns(19).Width=   1773
      Columns(19).Caption=   "jobdetailID1"
      Columns(19).Name=   "jobdetailID1"
      Columns(19).DataField=   "jobdetailID1"
      Columns(19).FieldLen=   256
      Columns(19).StyleSet=   "headerfield"
      Columns(20).Width=   1773
      Columns(20).Caption=   "playoutdone1"
      Columns(20).Name=   "playoutdone1"
      Columns(20).DataField=   "playoutdone1"
      Columns(20).FieldLen=   256
      Columns(20).StyleSet=   "stagefield"
      Columns(21).Width=   1773
      Columns(21).Caption=   "tapesentout1"
      Columns(21).Name=   "tapesentout1"
      Columns(21).DataField=   "tapesentout1"
      Columns(21).FieldLen=   256
      Columns(21).Style=   1
      Columns(21).StyleSet=   "stagefield"
      Columns(22).Width=   1773
      Columns(22).Caption=   "tapeformat2"
      Columns(22).Name=   "tapeformat2"
      Columns(22).DataField=   "tapeformat2"
      Columns(22).FieldLen=   256
      Columns(22).StyleSet=   "headerfield"
      Columns(23).Width=   1773
      Columns(23).Caption=   "jobdetailID2"
      Columns(23).Name=   "jobdetailID2"
      Columns(23).DataField=   "jobdetailID2"
      Columns(23).FieldLen=   256
      Columns(23).StyleSet=   "headerfield"
      Columns(24).Width=   1773
      Columns(24).Caption=   "playoutdone2"
      Columns(24).Name=   "playoutdone2"
      Columns(24).DataField=   "playoutdone2"
      Columns(24).FieldLen=   256
      Columns(24).StyleSet=   "stagefield"
      Columns(25).Width=   1773
      Columns(25).Caption=   "tapesentout2"
      Columns(25).Name=   "tapesentout2"
      Columns(25).DataField=   "tapesentout2"
      Columns(25).FieldLen=   256
      Columns(25).Style=   1
      Columns(25).StyleSet=   "stagefield"
      Columns(26).Width=   1773
      Columns(26).Caption=   "tapeformat3"
      Columns(26).Name=   "tapeformat3"
      Columns(26).DataField=   "tapeformat3"
      Columns(26).FieldLen=   256
      Columns(26).StyleSet=   "headerfield"
      Columns(27).Width=   1773
      Columns(27).Caption=   "jobdetailID3"
      Columns(27).Name=   "jobdetailID3"
      Columns(27).DataField=   "jobdetailID3"
      Columns(27).FieldLen=   256
      Columns(27).StyleSet=   "headerfield"
      Columns(28).Width=   1773
      Columns(28).Caption=   "playoutdone3"
      Columns(28).Name=   "playoutdone3"
      Columns(28).DataField=   "playoutdone3"
      Columns(28).FieldLen=   256
      Columns(28).StyleSet=   "stagefield"
      Columns(29).Width=   1773
      Columns(29).Caption=   "tapesentout3"
      Columns(29).Name=   "tapesentout3"
      Columns(29).DataField=   "tapesentout3"
      Columns(29).FieldLen=   256
      Columns(29).Style=   1
      Columns(29).StyleSet=   "stagefield"
      Columns(30).Width=   1773
      Columns(30).Caption=   "tapeformat4"
      Columns(30).Name=   "tapeformat4"
      Columns(30).DataField=   "tapeformat4"
      Columns(30).FieldLen=   256
      Columns(30).StyleSet=   "headerfield"
      Columns(31).Width=   1773
      Columns(31).Caption=   "jobdetailID4"
      Columns(31).Name=   "jobdetailID4"
      Columns(31).DataField=   "jobdetailID4"
      Columns(31).FieldLen=   256
      Columns(31).StyleSet=   "headerfield"
      Columns(32).Width=   1773
      Columns(32).Caption=   "playoutdone4"
      Columns(32).Name=   "playoutdone4"
      Columns(32).DataField=   "playoutdone4"
      Columns(32).FieldLen=   256
      Columns(32).StyleSet=   "stagefield"
      Columns(33).Width=   1773
      Columns(33).Caption=   "tapesentout4"
      Columns(33).Name=   "tapesentout4"
      Columns(33).DataField=   "tapesentout4"
      Columns(33).FieldLen=   256
      Columns(33).Style=   1
      Columns(33).StyleSet=   "stagefield"
      Columns(34).Width=   1773
      Columns(34).Caption=   "tapeformat5"
      Columns(34).Name=   "tapeformat5"
      Columns(34).DataField=   "tapeformat5"
      Columns(34).FieldLen=   256
      Columns(34).StyleSet=   "headerfield"
      Columns(35).Width=   1773
      Columns(35).Caption=   "jobdetailID5"
      Columns(35).Name=   "jobdetailID5"
      Columns(35).DataField=   "jobdetailID5"
      Columns(35).FieldLen=   256
      Columns(35).StyleSet=   "headerfield"
      Columns(36).Width=   1773
      Columns(36).Caption=   "playoutdone5"
      Columns(36).Name=   "playoutdone5"
      Columns(36).DataField=   "playoutdone5"
      Columns(36).FieldLen=   256
      Columns(36).StyleSet=   "stagefield"
      Columns(37).Width=   1773
      Columns(37).Caption=   "tapesentout5"
      Columns(37).Name=   "tapesentout5"
      Columns(37).DataField=   "tapesentout5"
      Columns(37).FieldLen=   256
      Columns(37).Style=   1
      Columns(37).StyleSet=   "stagefield"
      Columns(38).Width=   1773
      Columns(38).Caption=   "tapeformat6"
      Columns(38).Name=   "tapeformat6"
      Columns(38).DataField=   "tapeformat6"
      Columns(38).FieldLen=   256
      Columns(38).StyleSet=   "headerfield"
      Columns(39).Width=   1773
      Columns(39).Caption=   "jobdetailID6"
      Columns(39).Name=   "jobdetailID6"
      Columns(39).DataField=   "jobdetailID6"
      Columns(39).FieldLen=   256
      Columns(39).StyleSet=   "headerfield"
      Columns(40).Width=   1773
      Columns(40).Caption=   "playoutdone6"
      Columns(40).Name=   "playoutdone6"
      Columns(40).DataField=   "playoutdone6"
      Columns(40).FieldLen=   256
      Columns(40).StyleSet=   "stagefield"
      Columns(41).Width=   1773
      Columns(41).Caption=   "tapesentout6"
      Columns(41).Name=   "tapesentout6"
      Columns(41).DataField=   "tapesentout6"
      Columns(41).FieldLen=   256
      Columns(41).Style=   1
      Columns(41).StyleSet=   "stagefield"
      Columns(42).Width=   2117
      Columns(42).Caption=   "Master Deleted"
      Columns(42).Name=   "masterfiledeleted"
      Columns(42).DataField=   "masterfiledeleted"
      Columns(42).FieldLen=   256
      Columns(42).StyleSet=   "stagefield"
      Columns(43).Width=   1773
      Columns(43).Caption=   "playoutformatsneeded"
      Columns(43).Name=   "playoutformatsneeded"
      Columns(43).DataField=   "playoutformatsneeded"
      Columns(43).FieldLen=   256
      _ExtentX        =   44124
      _ExtentY        =   16536
      _StockProps     =   79
      Caption         =   "Tracker Items"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoComments 
      Height          =   330
      Left            =   13500
      Top             =   12660
      Visible         =   0   'False
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdComments 
      Bindings        =   "frmTrackerPlayout.frx":006B
      Height          =   1875
      Left            =   120
      TabIndex        =   18
      Top             =   12660
      Width           =   12555
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12582847
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   6
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "trackerhistoryID"
      Columns(0).Name =   "trackerplayout_commentID"
      Columns(0).DataField=   "trackerplayout_commentID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "trackerprogramID"
      Columns(1).Name =   "trackerplayoutID"
      Columns(1).DataField=   "trackerplayoutID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   13070
      Columns(2).Caption=   "Comments"
      Columns(2).Name =   "comment"
      Columns(2).DataField=   "comment"
      Columns(2).FieldLen=   255
      Columns(3).Width=   3360
      Columns(3).Caption=   "Date"
      Columns(3).Name =   "cdate"
      Columns(3).DataField=   "cdate"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).Style=   1
      Columns(4).Width=   3519
      Columns(4).Caption=   "Entered By"
      Columns(4).Name =   "cuser"
      Columns(4).DataField=   "cuser"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   1138
      Columns(5).Caption=   "Email?"
      Columns(5).Name =   "Email?"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   4
      UseDefaults     =   0   'False
      _ExtentX        =   22146
      _ExtentY        =   3307
      _StockProps     =   79
      Caption         =   "Tracker Comments"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   1770
      TabIndex        =   19
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The company this job is for"
      Top             =   120
      Width           =   2835
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   5001
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin VB.Label lblLastTrackerItemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   1620
      TabIndex        =   22
      Top             =   2040
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   10
      Left            =   180
      TabIndex        =   20
      Top             =   180
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "BBC Order Number"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   13
      Left            =   4860
      TabIndex        =   16
      Top             =   540
      Width           =   1755
   End
   Begin VB.Label lblCaption 
      Caption         =   "Series"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   10500
      TabIndex        =   14
      Top             =   180
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Episode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   10500
      TabIndex        =   13
      Top             =   540
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   4860
      TabIndex        =   10
      Top             =   180
      Width           =   1395
   End
   Begin VB.Label lblTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   420
      TabIndex        =   8
      Top             =   2040
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCompanyID 
      Height          =   255
      Left            =   23520
      TabIndex        =   7
      Top             =   12960
      Visible         =   0   'False
      Width           =   735
   End
End
Attribute VB_Name = "frmTrackerPlayout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strSearch As String
Dim m_strOrderby As String
Dim m_blnDelete As Boolean

Private Sub HideAllColumns()

'Eventually Hide Items from different buttons
If chkPlayout(1).Value = 1 Then
    grdItems.Columns("tapeformat1").Visible = True
    grdItems.Columns("jobdetailID1").Visible = True
    grdItems.Columns("playoutdone1").Visible = True
    grdItems.Columns("tapesentout1").Visible = True
Else
    grdItems.Columns("tapeformat1").Visible = False
    grdItems.Columns("jobdetailID1").Visible = False
    grdItems.Columns("playoutdone1").Visible = False
    grdItems.Columns("tapesentout1").Visible = False
End If

If chkPlayout(2).Value = 1 Then
    grdItems.Columns("tapeformat2").Visible = True
    grdItems.Columns("jobdetailID2").Visible = True
    grdItems.Columns("playoutdone2").Visible = True
    grdItems.Columns("tapesentout2").Visible = True
Else
    grdItems.Columns("tapeformat2").Visible = False
    grdItems.Columns("jobdetailID2").Visible = False
    grdItems.Columns("playoutdone2").Visible = False
    grdItems.Columns("tapesentout2").Visible = False
End If

If chkPlayout(3).Value = 1 Then
    grdItems.Columns("tapeformat3").Visible = True
    grdItems.Columns("jobdetailID3").Visible = True
    grdItems.Columns("playoutdone3").Visible = True
    grdItems.Columns("tapesentout3").Visible = True
Else
    grdItems.Columns("tapeformat3").Visible = False
    grdItems.Columns("jobdetailID3").Visible = False
    grdItems.Columns("playoutdone3").Visible = False
    grdItems.Columns("tapesentout3").Visible = False
End If

If chkPlayout(4).Value = 1 Then
    grdItems.Columns("tapeformat4").Visible = True
    grdItems.Columns("jobdetailID4").Visible = True
    grdItems.Columns("playoutdone4").Visible = True
    grdItems.Columns("tapesentout4").Visible = True
Else
    grdItems.Columns("tapeformat4").Visible = False
    grdItems.Columns("jobdetailID4").Visible = False
    grdItems.Columns("playoutdone4").Visible = False
    grdItems.Columns("tapesentout4").Visible = False
End If

If chkPlayout(5).Value = 1 Then
    grdItems.Columns("tapeformat5").Visible = True
    grdItems.Columns("jobdetailID5").Visible = True
    grdItems.Columns("playoutdone5").Visible = True
    grdItems.Columns("tapesentout5").Visible = True
Else
    grdItems.Columns("tapeformat5").Visible = False
    grdItems.Columns("jobdetailID5").Visible = False
    grdItems.Columns("playoutdone5").Visible = False
    grdItems.Columns("tapesentout5").Visible = False
End If

If chkPlayout(6).Value = 1 Then
    grdItems.Columns("tapeformat6").Visible = True
    grdItems.Columns("jobdetailID6").Visible = True
    grdItems.Columns("playoutdone6").Visible = True
    grdItems.Columns("tapesentout6").Visible = True
Else
    grdItems.Columns("tapeformat6").Visible = False
    grdItems.Columns("jobdetailID6").Visible = False
    grdItems.Columns("playoutdone6").Visible = False
    grdItems.Columns("tapesentout6").Visible = False
End If

End Sub

Private Sub chkHideDemo_Click()

Dim l_strSQL As String

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/playouttracker%' and companyID > 100 AND system_active = 1 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/playouttracker%' AND system_active = 1 ORDER BY name;"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

cmdSearch.Value = True

End Sub

Private Sub chkPlayout_Click(Index As Integer)

If chkPlayout(Index).Value = 1 Then
    grdItems.Columns("tapeformat" & Index).Visible = True
    grdItems.Columns("jobdetailID" & Index).Visible = True
    grdItems.Columns("playoutdone" & Index).Visible = True
    grdItems.Columns("tapesentout" & Index).Visible = True
Else
    grdItems.Columns("tapeformat" & Index).Visible = False
    grdItems.Columns("jobdetailID" & Index).Visible = False
    grdItems.Columns("playoutdone" & Index).Visible = False
    grdItems.Columns("tapesentout" & Index).Visible = False
End If

End Sub

Private Sub cmbCompany_Click()

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text
m_strSearch = " WHERE companyID = " & lblCompanyID.Caption
'm_strOrderby = " ORDER BY headertext1, headerint1, headerint2, headerint3, headerint4, headerint5"

HideAllColumns

DoEvents

Dim l_rstChoices As ADODB.Recordset, l_blnWide As Boolean

If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
    l_blnWide = True
Else
    l_blnWide = False
End If

cmdSearch.Value = True

End Sub

Private Sub cmdClear_Click()

ClearFields Me

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdFieldsAll_Click()

grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = True
grdItems.Columns("timecodeduration").Visible = True
grdItems.Columns("gbsent").Visible = True
grdItems.Columns("englishsubsneeded").Visible = True
grdItems.Columns("frenchsubsneeded").Visible = True
grdItems.Columns("brazillianportsubsneeded").Visible = True
grdItems.Columns("englishaudioneeded").Visible = True
grdItems.Columns("frenchaudioneeded").Visible = True
grdItems.Columns("brazillianportaudioneeded").Visible = True
grdItems.Columns("masterarrived").Visible = True
grdItems.Columns("englishsubsarrived").Visible = True
grdItems.Columns("frenchsubsarrived").Visible = True
grdItems.Columns("brazillianportsubsarrived").Visible = True
grdItems.Columns("englishaudioarrived").Visible = True
grdItems.Columns("frenchaudioarrived").Visible = True
grdItems.Columns("brazillianportaudioarrived").Visible = True
grdItems.Columns("proresmasterfile").Visible = True
grdItems.Columns("broadcastvisionmade").Visible = True
grdItems.Columns("buggedmpeg2made").Visible = True
grdItems.Columns("portalflashmade").Visible = True
grdItems.Columns("cleanbuggedportalflashmade").Visible = True
grdItems.Columns("senttoTX").Visible = True
grdItems.Columns("txpassed").Visible = True
grdItems.Columns("txfailed").Visible = True
grdItems.Columns("txnotreceived").Visible = True
grdItems.Columns("txcomments").Visible = True

End Sub

Private Sub cmdFieldsTX_Click()

grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = True
grdItems.Columns("timecodeduration").Visible = True
grdItems.Columns("gbsent").Visible = True
grdItems.Columns("englishsubsneeded").Visible = False
grdItems.Columns("frenchsubsneeded").Visible = False
grdItems.Columns("brazillianportsubsneeded").Visible = False
grdItems.Columns("englishaudioneeded").Visible = False
grdItems.Columns("frenchaudioneeded").Visible = False
grdItems.Columns("brazillianportaudioneeded").Visible = False
grdItems.Columns("masterarrived").Visible = False
grdItems.Columns("englishsubsarrived").Visible = False
grdItems.Columns("frenchsubsarrived").Visible = False
grdItems.Columns("brazillianportsubsarrived").Visible = False
grdItems.Columns("englishaudioarrived").Visible = False
grdItems.Columns("frenchaudioarrived").Visible = False
grdItems.Columns("brazillianportaudioarrived").Visible = False
grdItems.Columns("proresmasterfile").Visible = False
grdItems.Columns("broadcastvisionmade").Visible = False
grdItems.Columns("buggedmpeg2made").Visible = False
grdItems.Columns("portalflashmade").Visible = False
grdItems.Columns("cleanbuggedportalflashmade").Visible = False
grdItems.Columns("senttoTX").Visible = True
grdItems.Columns("txpassed").Visible = True
grdItems.Columns("txfailed").Visible = True
grdItems.Columns("txnotreceived").Visible = True
grdItems.Columns("txcomments").Visible = True

End Sub

Private Sub cmdFieldsWorkflow_Click()

grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("gbsent").Visible = False
grdItems.Columns("englishsubsneeded").Visible = False
grdItems.Columns("frenchsubsneeded").Visible = False
grdItems.Columns("brazillianportsubsneeded").Visible = False
grdItems.Columns("englishaudioneeded").Visible = False
grdItems.Columns("frenchaudioneeded").Visible = False
grdItems.Columns("brazillianportaudioneeded").Visible = False
grdItems.Columns("masterarrived").Visible = True
grdItems.Columns("englishsubsarrived").Visible = True
grdItems.Columns("frenchsubsarrived").Visible = True
grdItems.Columns("brazillianportsubsarrived").Visible = True
grdItems.Columns("englishaudioarrived").Visible = True
grdItems.Columns("frenchaudioarrived").Visible = True
grdItems.Columns("brazillianportaudioarrived").Visible = True
grdItems.Columns("proresmasterfile").Visible = True
grdItems.Columns("broadcastvisionmade").Visible = True
grdItems.Columns("buggedmpeg2made").Visible = True
grdItems.Columns("portalflashmade").Visible = True
grdItems.Columns("cleanbuggedportalflashmade").Visible = True
grdItems.Columns("senttoTX").Visible = False
grdItems.Columns("txpassed").Visible = False
grdItems.Columns("txfailed").Visible = False
grdItems.Columns("txnotreceived").Visible = False
grdItems.Columns("txcomments").Visible = False

End Sub

Private Sub cmdMakeOrder_Click()

'Positions for the Frame
'Top = 4440
'Left = 180

If Val(lblCompanyID.Caption) <> 0 Then
'    frmDisneyOrderEntry.fraNormal.Visible = False
    frmDisneyOrderEntry.fraDADC.Top = 4440
    frmDisneyOrderEntry.fraDADC.Left = 180
    frmDisneyOrderEntry.fraDADC.Visible = True
    frmDisneyOrderEntry.cmbCompany.Columns("companyID").Text = Val(lblCompanyID.Caption)
    frmDisneyOrderEntry.cmbCompany.Columns("name").Text = GetData("company", "name", "companyID", Val(lblCompanyID.Caption))
    frmDisneyOrderEntry.lblCompanyID.Caption = Val(lblCompanyID.Caption)
    frmDisneyOrderEntry.txtClientStatus.Text = GetData("company", "accountstatus", "companyID", Val(lblCompanyID.Caption))
    frmDisneyOrderEntry.cmbCompany.Columns("accountstatus").Text = frmDisneyOrderEntry.txtClientStatus.Text
    frmDisneyOrderEntry.cmbCompany.Visible = False
    frmDisneyOrderEntry.lblCompanyID.Visible = False
    frmDisneyOrderEntry.txtClientStatus.Visible = False
    frmDisneyOrderEntry.lblCaption(16).Visible = False
    frmDisneyOrderEntry.txtElementNumber.Visible = False
    frmDisneyOrderEntry.lblCaption(15).Visible = False
    frmDisneyOrderEntry.txtDuration.Visible = True
    frmDisneyOrderEntry.lblCaption(7).Visible = True
    frmDisneyOrderEntry.optOrderType(2).Value = True
    frmDisneyOrderEntry.Show vbModal
    adoItems.Refresh
End If

End Sub

Private Sub cmdSearch_Click()

Dim l_lngCount As Long

If lblCompanyID.Caption <> "" Then
    
    Dim l_strSQL As String
    If optComplete(1).Value = True Then
        l_strSQL = " AND (jobstatus IS NULL OR (jobstatus <> 'Completed' AND jobstatus <> 'Hold Cost' AND jobstatus <> 'Costed' AND jobstatus <> 'Sent To Accounts' AND jobstatus <> 'Cancelled')) "
    ElseIf optComplete(2).Value = True Then
        l_strSQL = "AND (jobstatus = 'Costed' OR jobstatus = 'Sent To Accounts') "
    ElseIf optComplete(3).Value = True Then
        l_strSQL = "AND (jobstatus = 'Completed' OR jobstatus = 'Hold Cost') "
    ElseIf optComplete(4).Value = True Then
        l_strSQL = "AND (jobstatus = 'Rejected' OR jobstatus = 'On Hold') "
    End If
    
    If txtBBCOrderNumber.Text <> "" Then
        l_strSQL = l_strSQL & " AND bbcordernumber LIKE '" & txtBBCOrderNumber.Text & "%' "
    End If
    
    If txtTitle.Text <> "" Then
        l_strSQL = l_strSQL & " AND title LIKE '" & QuoteSanitise(txtTitle.Text) & "%' "
    End If
    If Val(txtEpisode.Text) <> 0 Then
        l_strSQL = l_strSQL & " AND (episodenumber IS NOT NULL AND episodenumber = " & Val(txtEpisode.Text) & ") "
    End If
    If Val(txtSeries.Text) <> 0 Then
        l_strSQL = l_strSQL & " AND (seriesnumber IS NOT NULL AND seriesnumber = " & Val(txtSeries.Text) & ") "
    End If

    adoItems.ConnectionString = g_strConnection
    adoItems.RecordSource = "SELECT * FROM trackerplayout " & m_strSearch & l_strSQL & m_strOrderby & ";"
    adoItems.Refresh
    
    adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"
    
End If

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

Dim l_blnWide As Boolean

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/playouttracker%' and companyID > 100 ORDER BY name;"
Else
    l_strSQL = "SELECT name, companyID FROM company WHERE cetaclientcode like '%/playouttracker%' ORDER BY name;"
End If

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

adoComments.ConnectionString = g_strConnection

grdItems.StyleSets("headerfield").BackColor = &HE7FFE7
grdItems.StyleSets("stagefield").BackColor = &HE7FFFF
grdItems.StyleSets("conclusionfield").BackColor = &HFFFFE7

grdItems.StyleSets.Add "Error"
grdItems.StyleSets("Error").BackColor = &HA0A0FF

grdItems.StyleSets.Add "Priority"
grdItems.StyleSets("Priority").BackColor = &H70B0FF

optComplete(1).Value = True

m_strSearch = " WHERE companyID = 0"
m_strOrderby = " ORDER BY duedate, masterfilereference, title, episodenumber"

HideAllColumns

DoEvents

cmdSearch.Value = True

End Sub

Private Sub Form_Resize()

On Error Resume Next

grdItems.Width = Me.ScaleWidth - grdItems.Left - 120
grdItems.Height = (Me.ScaleHeight - grdItems.Top - frmButtons.Height) * 0.75 - 240
frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120
grdComments.Top = grdItems.Top + grdItems.Height + 120
grdComments.Height = frmButtons.Top - grdComments.Top - 120
grdComments.Width = grdItems.Width

End Sub

Private Sub grdComments_AfterDelete(RtnDispErrMsg As Integer)

m_blnDelete = False

End Sub

Private Sub grdComments_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

m_blnDelete = True

End Sub

Private Sub grdComments_BeforeUpdate(Cancel As Integer)

If m_blnDelete = False Then

    If grdComments.Columns("comment").Text = "" Then
        MsgBox "Cannot Save a Comment with no actual comment", vbCritical, "Comment Not Saved"
        Cancel = True
        Exit Sub
    End If
    
    grdComments.Columns("trackerplayoutID").Text = lblTrackeritemID.Caption
    If grdComments.Columns("cdate").Text = "" Then
        grdComments.Columns("cdate").Text = Now
    End If
    grdComments.Columns("cuser").Text = g_strFullUserName

End If

End Sub

Private Sub grdComments_BtnClick()

Dim l_strOurEmailContact As String, l_strOurContactName As String, l_strEmailBody As String

Dim l_rstWhoToEmail As ADODB.Recordset

If MsgBox("Send Email?", vbYesNo, "Automatic Email") = vbYes Then
    
    l_strEmailBody = "A comment was created " & _
        "By: " & grdComments.Columns("cuser").Text & vbCrLf & _
        "About PLayout Item: " & grdItems.Columns("dadcID").Text & vbCrLf & _
        "Title: " & grdItems.Columns("title").Text & vbCrLf & _
        "Comment: " & grdComments.Columns("comment").Text & vbCrLf
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND trackermessageID = 13;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        Do While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "DADC Playout Tracker Comment Created", "", l_strEmailBody, True, "", ""
    
            l_rstWhoToEmail.MoveNext
        Loop
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
    
End If

End Sub

Private Sub grdItems_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdItems_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdItems_BeforeUpdate(Cancel As Integer)

If m_blnDelete = True Then Exit Sub

Dim temp As Boolean, l_strDuration As String, l_lngRunningTime As Long, l_curFileSize As Currency, l_strFilename As String, l_rst As ADODB.Recordset
Dim l_lngClipID As Long, l_strNetworkPath As String

'Check ReadytoBill - setting temp to false if any items are not ready to bill
'If grdItems.Columns("txpassed").Text <> "" _
'And (grdItems.Columns("buggedmpeg2made").Text <> "" And UCase(Right(grdItems.Columns("buggedmpeg2made").Text, 2)) <> "ER") _
'And (grdItems.Columns("cleanbuggedportalflashmade").Text <> "" And UCase(Right(grdItems.Columns("cleanbuggedportalflashmade").Text, 2)) <> "ER") _
'Then
'    grdItems.Columns("readytobill").Text = 1
'Else
'    grdItems.Columns("readytobill").Text = 0
'End If

grdItems.Columns("companyID").Text = lblCompanyID.Caption

'Get and update the file related items from the reference field.

If grdItems.Columns("masterclipID").Text <> "" Then
    'see if there is a clip record to get the duration from reference
    l_strDuration = GetDataSQL("SELECT TOP 1 fd_length FROM events WHERE eventID = " & grdItems.Columns("masterclipID").Text & ";")
    grdItems.Columns("duration").Text = l_strDuration
    If l_strDuration <> "" Then
        l_lngRunningTime = 60 * Val(Mid(l_strDuration, 1, 2))
        l_lngRunningTime = l_lngRunningTime + Val(Mid(l_strDuration, 4, 2))
        If Val(Mid(l_strDuration, 7, 2)) > 30 Then
            l_lngRunningTime = l_lngRunningTime + 1
        End If
        If l_lngRunningTime = 0 Then l_lngRunningTime = 1
        If (grdItems.Columns("durationinmins").Text = "") Or (grdItems.Columns("durationinmins").Text <> "") Then
            grdItems.Columns("durationinmins").Text = l_lngRunningTime
        End If
    End If
    
    Set l_rst = Nothing

End If

End Sub

Private Sub grdItems_BtnClick()

Dim tempdate As String

Select Case LCase(grdItems.Columns(grdItems.Col).Name)

Case "barcode"
    
    MakeClipFromDADCTrackerEvent grdItems.Columns("tracker_Svensk_itemID").Text

Case Else
    
    If grdItems.ActiveCell.Text <> "" Then
        grdItems.ActiveCell.Text = ""
    Else
        If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
            grdItems.ActiveCell.Text = Now
        Else
            tempdate = FormatDateTime(Now, vbLongDate)
            grdItems.ActiveCell.Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
        End If
    End If

End Select

End Sub

Private Sub grdItems_DblClick()

'ShowClipSearch "", grdItems.Columns("itemreference").Text

Select Case LCase(grdItems.Columns(grdItems.Col).Name)

Case "jobid"
    If Val(grdItems.Columns("jobID").Text) <> 0 Then
        ShowJob Val(grdItems.Columns("jobID").Text), 1, True
'        ShowJob Val(grdItems.Columns("jobID").Text), 1, True
    End If
Case "masterclipid"
    If Val(grdItems.Columns("masterclipID").Text) <> 0 Then ShowClipControl Val(grdItems.Columns("masterclipID").Text)
    
End Select

End Sub

Private Sub grdItems_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim l_strSQL As String, l_lngCount As Long, l_lngMaxCount As Long

lblTrackeritemID.Caption = grdItems.Columns("trackerplayoutID").Text

If Val(lblTrackeritemID.Caption) <> Val(lblLastTrackeritemID.Caption) Then
    
    If Val(lblTrackeritemID.Caption) = 0 Then
        
        l_strSQL = "SELECT * FROM trackerplayout_comment WHERE trackerplayoutID = -1 ORDER BY cdate;"
        
        adoComments.RecordSource = l_strSQL
        adoComments.ConnectionString = g_strConnection
        adoComments.Refresh
        
        lblLastTrackeritemID.Caption = ""
    
        Exit Sub
    
    End If
    
    l_strSQL = "SELECT * FROM trackerplayout_comment WHERE trackerplayoutID = " & lblTrackeritemID.Caption & " ORDER BY cdate;"
    
    adoComments.RecordSource = l_strSQL
    adoComments.ConnectionString = g_strConnection
    adoComments.Refresh
    
    If Val(Trim(" " & grdItems.Columns("playoutformatsneeded").Text)) <> 0 Then
        l_lngMaxCount = Val(grdItems.Columns("playoutformatsneeded").Text)
        For l_lngCount = 1 To l_lngMaxCount
            chkPlayout(l_lngCount).Value = 1
        Next
        For l_lngCount = l_lngMaxCount To 5
            chkPlayout(l_lngCount).Value = 0
        Next
        HideAllColumns
    End If

    lblLastTrackeritemID.Caption = lblTrackeritemID.Caption
    
End If

End Sub

Private Sub grdItems_RowLoaded(ByVal Bookmark As Variant)

'Set the error states on the individual columns that can have errors

End Sub

Private Sub optComplete_Click(Index As Integer)

cmdSearch.Value = True

End Sub

Function CheckReadyForTX() As Boolean

Dim temp As Boolean

temp = True

'If grdItems.Columns("masterarrived").Text = "" Or UCase(Right(grdItems.Columns("masterarrived").Text, 2)) = "ER" Then temp = False
'If grdItems.Columns("broadcastvisionmade").Text = "" Or UCase(Right(grdItems.Columns("broadcastvisionmade").Text, 2)) = "ER" Then temp = False
'If grdItems.Columns("senttosoundhouse").Text = "" Or UCase(Right(grdItems.Columns("senttosoundhouse").Text, 2)) = "ER" Then temp = False
'If Val(grdItems.Columns("englishaudioneeded").Text) <> 0 Then
'    If grdItems.Columns("englishaudioarrived").Text = "" Or UCase(Right(grdItems.Columns("englishaudioarrived").Text, 2)) = "ER" Then temp = False
'End If
'If Val(grdItems.Columns("frenchaudioneeded").Text) <> 0 Then
'    If grdItems.Columns("frenchaudioarrived").Text = "" Or UCase(Right(grdItems.Columns("frenchaudioarrived").Text, 2)) = "ER" Then temp = False
'End If
'If Val(grdItems.Columns("brazillianportaudioneeded").Text) <> 0 Then
'    If grdItems.Columns("brazillianportaudioarrived").Text = "" Or UCase(Right(grdItems.Columns("brazillianportaudioarrived").Text, 2)) = "ER" Then temp = False
'End If

CheckReadyForTX = temp

End Function
