VERSION 5.00
Begin VB.Form frmGetRollcallNote 
   Caption         =   "Rollcall Note"
   ClientHeight    =   1035
   ClientLeft      =   1995
   ClientTop       =   1395
   ClientWidth     =   7335
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmGetRollcallNote.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   1035
   ScaleWidth      =   7335
   Begin VB.CommandButton cmbOK 
      Caption         =   "OK"
      Height          =   315
      Left            =   3060
      TabIndex        =   2
      Top             =   600
      Width           =   1275
   End
   Begin VB.TextBox txtNote 
      BackColor       =   &H00C0FFFF&
      Height          =   315
      Left            =   2280
      TabIndex        =   0
      ToolTipText     =   "Hit return to accept the offered barcode, or type or scan a new one"
      Top             =   120
      Width           =   4935
   End
   Begin VB.Label Label1 
      Caption         =   "Type a note for these Rollcall Settings"
      Height          =   495
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1995
   End
End
Attribute VB_Name = "frmGetRollcallNote"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmbOK_Click()
Me.Hide
End Sub
Private Sub txtNote_GotFocus()
txtNote.SelStart = 0
txtNote.SelLength = Len(txtNote.Text)
End Sub
Private Sub txtNote_KeyPress(KeyAscii As Integer)
If KeyAscii = vbKeyEscape Then
    txtNote.Text = ""
    KetAscii = 0
    Me.Hide
ElseIf KeyAscii = vbKeyReturn Then
    KeyAscii = 0
    Me.Hide
End If
End Sub
