VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Begin VB.Form frmNoCharge 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "No Charge This Job?"
   ClientHeight    =   3225
   ClientLeft      =   7470
   ClientTop       =   5580
   ClientWidth     =   5385
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3225
   ScaleWidth      =   5385
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   315
      Left            =   4140
      TabIndex        =   4
      ToolTipText     =   "Close this form"
      Top             =   2820
      Width           =   1155
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save"
      Height          =   315
      Left            =   2880
      TabIndex        =   3
      ToolTipText     =   "Save changes to this job"
      Top             =   2820
      Width           =   1155
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   675
      Left            =   4440
      Picture         =   "frmNoCharge.frx":0000
      ScaleHeight     =   675
      ScaleWidth      =   735
      TabIndex        =   8
      Top             =   120
      Width           =   735
   End
   Begin VB.TextBox txtReason 
      BorderStyle     =   0  'None
      Height          =   1155
      Left            =   1440
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      ToolTipText     =   "Reason"
      Top             =   1560
      Width           =   3855
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbOurContact 
      Height          =   255
      Left            =   1440
      TabIndex        =   0
      ToolTipText     =   "No charge requested by"
      Top             =   120
      Width           =   2895
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BevelColorFrame =   -2147483644
      CheckBox3D      =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   4868
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   16761024
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbReasonCode 
      Height          =   255
      Left            =   1440
      TabIndex        =   1
      ToolTipText     =   "Reason Code"
      Top             =   540
      Width           =   2895
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   4868
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "YOU MUST ENTER A SPECIFIC REASON FOR THIS NO CHARGE!"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   60
      TabIndex        =   9
      Top             =   1080
      Width           =   5235
   End
   Begin VB.Label lblCaption 
      Caption         =   "Reason Code"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   44
      Left            =   60
      TabIndex        =   7
      Top             =   540
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Requested By"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   12
      Left            =   60
      TabIndex        =   6
      Top             =   120
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Reason"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   120
      TabIndex        =   5
      Top             =   1560
      Width           =   1155
   End
End
Attribute VB_Name = "frmNoCharge"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()
Me.Tag = "CANCELLED"
Me.Hide

End Sub

Private Sub cmdSave_Click()

If txtReason.Text = "" And g_intKidsCoContractRoutines <> 1 Then
    MsgBox "Please enter a reason for this no charge!", vbExclamation
    Exit Sub
End If

If cmbReasonCode.Text = "" Then
    MsgBox "Please enter a reason code for this no charge!", vbExclamation
    Exit Sub
End If
If cmbOurContact.Text = "" Then
    MsgBox "Please enter who requested this no charge!", vbExclamation
    Exit Sub
End If


Me.Hide
End Sub

Private Sub Form_Load()
CenterForm Me
PopulateCombo "ourcontact", cmbOurContact
PopulateCombo "nochargereasoncode", cmbReasonCode

End Sub
