VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmEditCostingRow 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Edit Costing Row"
   ClientHeight    =   5025
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   10965
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5025
   ScaleWidth      =   10965
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSAdodcLib.Adodc adoCompanyCosts 
      Height          =   330
      Left            =   9120
      Top             =   240
      Visible         =   0   'False
      Width           =   1740
      _ExtentX        =   3069
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   2
      LockType        =   1
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   65535
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCompanyCosts"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdPreviousCosts 
      Bindings        =   "frmEditCostingRow.frx":0000
      Height          =   4275
      Left            =   6600
      TabIndex        =   44
      Top             =   120
      Width           =   4275
      _Version        =   196617
      ForeColorEven   =   0
      BackColorOdd    =   16772351
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   7541
      _ExtentY        =   7541
      _StockProps     =   79
      Caption         =   "Previous Costs For This Company / Code"
   End
   Begin VB.CommandButton cmdCalculateTotals 
      Caption         =   "Calculate Totals"
      Height          =   315
      Left            =   120
      TabIndex        =   42
      Top             =   4620
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.TextBox txtRateCardPrice 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H00404040&
      Height          =   315
      Left            =   4620
      Locked          =   -1  'True
      TabIndex        =   33
      Top             =   2700
      Width           =   1755
   End
   Begin VB.TextBox txtRateCardGross 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   315
      Left            =   4620
      Locked          =   -1  'True
      TabIndex        =   32
      Top             =   4080
      Width           =   1755
   End
   Begin VB.TextBox txtRateCardVAT 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   315
      Left            =   4620
      Locked          =   -1  'True
      TabIndex        =   31
      Top             =   3660
      Width           =   1755
   End
   Begin VB.TextBox txtRateCardNet 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   315
      Left            =   4620
      Locked          =   -1  'True
      TabIndex        =   30
      Top             =   3240
      Width           =   1755
   End
   Begin VB.TextBox txtNetTotal 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFC0C0&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1380
      Locked          =   -1  'True
      TabIndex        =   8
      Top             =   3240
      Width           =   1755
   End
   Begin VB.TextBox txtVATTotal 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0E0FF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1380
      Locked          =   -1  'True
      TabIndex        =   9
      Top             =   3660
      Width           =   1755
   End
   Begin VB.TextBox txtGrossTotal 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1380
      Locked          =   -1  'True
      TabIndex        =   10
      Top             =   4080
      Width           =   1755
   End
   Begin MSAdodcLib.Adodc adoCostCode 
      Height          =   330
      Left            =   4740
      Top             =   120
      Visible         =   0   'False
      Width           =   1740
      _ExtentX        =   3069
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   2
      LockType        =   1
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   65535
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCostCode"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox txtPosition 
      BackColor       =   &H8000000F&
      Height          =   315
      Left            =   1380
      TabIndex        =   43
      Top             =   180
      Width           =   855
   End
   Begin VB.TextBox txtDiscount 
      Alignment       =   1  'Right Justify
      Height          =   315
      Left            =   1380
      TabIndex        =   6
      Top             =   2280
      Width           =   1335
   End
   Begin VB.TextBox txtUnitCharge 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   1380
      TabIndex        =   7
      Top             =   2700
      Width           =   1755
   End
   Begin VB.TextBox txtAccountCode 
      BackColor       =   &H8000000F&
      Height          =   315
      Left            =   4980
      Locked          =   -1  'True
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   600
      Width           =   1515
   End
   Begin VB.ComboBox cmbUnit 
      Height          =   315
      Left            =   3360
      TabIndex        =   5
      Top             =   1860
      Width           =   1335
   End
   Begin VB.TextBox txtTime 
      Alignment       =   1  'Right Justify
      Height          =   315
      Left            =   1380
      TabIndex        =   4
      Top             =   1860
      Width           =   1335
   End
   Begin VB.TextBox txtQuantity 
      Alignment       =   1  'Right Justify
      Height          =   315
      Left            =   1380
      TabIndex        =   3
      Top             =   1440
      Width           =   1335
   End
   Begin VB.TextBox txtDescription 
      Height          =   315
      Left            =   1380
      TabIndex        =   2
      Top             =   1020
      Width           =   5115
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCostCode 
      Bindings        =   "frmEditCostingRow.frx":001E
      Height          =   315
      Left            =   1380
      TabIndex        =   0
      Top             =   600
      Width           =   2475
      DataFieldList   =   "cetacode"
      _Version        =   196617
      BackColorOdd    =   15790335
      Columns(0).Width=   3200
      _ExtentX        =   4366
      _ExtentY        =   556
      _StockProps     =   93
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      DataFieldToDisplay=   "cetacode"
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   315
      Left            =   9720
      TabIndex        =   12
      Top             =   4620
      Width           =   1155
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   315
      Left            =   8460
      TabIndex        =   11
      Top             =   4620
      Width           =   1155
   End
   Begin VB.Label lblJobID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   4920
      TabIndex        =   46
      Top             =   4680
      Visible         =   0   'False
      Width           =   1635
   End
   Begin VB.Label lblCompanyID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   3660
      TabIndex        =   45
      Top             =   120
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Unit Charge"
      ForeColor       =   &H00404040&
      Height          =   255
      Index           =   24
      Left            =   3360
      TabIndex        =   41
      Top             =   2700
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      Caption         =   "�"
      Height          =   255
      Index           =   23
      Left            =   4380
      TabIndex        =   40
      Top             =   2700
      Width           =   195
   End
   Begin VB.Label lblCaption 
      Caption         =   "Gross Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   255
      Index           =   22
      Left            =   3360
      TabIndex        =   39
      Top             =   4080
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "VAT"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   255
      Index           =   21
      Left            =   3360
      TabIndex        =   38
      Top             =   3660
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Net Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   255
      Index           =   20
      Left            =   3360
      TabIndex        =   37
      Top             =   3240
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      Caption         =   "�"
      Height          =   255
      Index           =   19
      Left            =   4380
      TabIndex        =   36
      Top             =   3240
      Width           =   195
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      Caption         =   "�"
      Height          =   255
      Index           =   17
      Left            =   4380
      TabIndex        =   35
      Top             =   3660
      Width           =   195
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      Caption         =   "�"
      Height          =   255
      Index           =   16
      Left            =   4380
      TabIndex        =   34
      Top             =   4080
      Width           =   195
   End
   Begin VB.Line Line2 
      BorderWidth     =   2
      X1              =   60
      X2              =   10860
      Y1              =   4500
      Y2              =   4500
   End
   Begin VB.Line Line1 
      BorderWidth     =   2
      X1              =   120
      X2              =   3120
      Y1              =   3120
      Y2              =   3120
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      Caption         =   "�"
      Height          =   255
      Index           =   14
      Left            =   1140
      TabIndex        =   29
      Top             =   4080
      Width           =   195
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      Caption         =   "�"
      Height          =   255
      Index           =   13
      Left            =   1140
      TabIndex        =   28
      Top             =   3660
      Width           =   195
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      Caption         =   "�"
      Height          =   255
      Index           =   12
      Left            =   1140
      TabIndex        =   27
      Top             =   3240
      Width           =   195
   End
   Begin VB.Label lblCaption 
      Caption         =   "Net Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   120
      TabIndex        =   26
      Top             =   3240
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "VAT"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   7
      Left            =   120
      TabIndex        =   25
      Top             =   3660
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Gross Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   8
      Left            =   120
      TabIndex        =   24
      Top             =   4080
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      Caption         =   "�"
      Height          =   255
      Index           =   15
      Left            =   1140
      TabIndex        =   23
      Top             =   2700
      Width           =   195
   End
   Begin VB.Label lblCaption 
      Caption         =   "%"
      Height          =   255
      Index           =   18
      Left            =   2760
      TabIndex        =   22
      Top             =   2340
      Width           =   315
   End
   Begin VB.Label lblCaption 
      Caption         =   "Position"
      Height          =   255
      Index           =   11
      Left            =   120
      TabIndex        =   21
      Top             =   180
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Discount"
      Height          =   255
      Index           =   10
      Left            =   120
      TabIndex        =   20
      Top             =   2280
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Unit Charge"
      Height          =   255
      Index           =   9
      Left            =   120
      TabIndex        =   19
      Top             =   2700
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Unit"
      Height          =   255
      Index           =   5
      Left            =   2880
      TabIndex        =   18
      Top             =   1860
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Time"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   17
      Top             =   1860
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Qty"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   16
      Top             =   1440
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Description"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   15
      Top             =   1020
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Nominal Code"
      Height          =   255
      Index           =   1
      Left            =   3960
      TabIndex        =   14
      Top             =   600
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Code"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   13
      Top             =   600
      Width           =   915
   End
End
Attribute VB_Name = "frmEditCostingRow"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub cmbCostCode_Click()

txtAccountCode.Text = cmbCostCode.Columns(2).Text
txtUnitCharge.Text = cmbCostCode.Columns(3).Text
txtDescription.Text = cmbCostCode.Columns(1).Text
txtPosition.Text = cmbCostCode.Columns(5).Text
cmbUnit.Text = cmbCostCode.Columns(4).Text

txtRateCardPrice.Text = cmbCostCode.Columns(3).Text

If Val(txtQuantity.Text) = 0 Then txtQuantity.Text = "1"
If Val(txtTime.Text) = 0 Then txtTime.Text = "1"

CalculateTotals

'show the previous costs for this cost code with this client
adoCompanyCosts.ConnectionString = g_strConnection
adoCompanyCosts.RecordSource = "SELECT job.jobID as 'Job ID', job.startdate AS 'Job Date', costing.unit AS 'Unit', ((costing.total / costing.quantity) / costing.fd_time) as 'Price' FROM job INNER JOIN costing ON job.jobID = costing.jobID WHERE job.companyID = '" & lblCompanyID.Caption & "' AND costing.costcode = '" & QuoteSanitise(cmbCostCode.Text) & "' AND job.fd_status <> 'Cancelled' ORDER BY job.startdate DESC;"
adoCompanyCosts.Refresh

grdPreviousCosts.Columns(0).Width = 800
grdPreviousCosts.Columns(1).Width = 1000
grdPreviousCosts.Columns(2).Width = 800
grdPreviousCosts.Columns(3).Width = 900


End Sub

Sub CalculateTotals()

Dim l_dblUnitCharge As Double
Dim l_sngDiscount   As Single
Dim l_sngQuantity   As Single
Dim l_sngTime       As Single
Dim l_dblTotal      As Double
Dim l_dblVAT        As Double
Dim l_dblGrossTotal As Double

'pick up the numbers
l_dblUnitCharge = Val(txtUnitCharge.Text)
l_sngDiscount = Val(txtDiscount.Text)
l_sngQuantity = Val(txtQuantity.Text)
l_sngTime = Val(txtTime.Text)

'work out before any discount
l_dblTotal = (l_dblUnitCharge * l_sngQuantity * l_sngTime)

'check the discount
If l_sngDiscount <> 0 Then
    l_dblTotal = (l_dblTotal / 100) * (100 - l_sngDiscount)
End If

'get the VAT
Dim l_intDoNotChargeVAT As Integer
l_intDoNotChargeVAT = GetData("job", "donotchargevat", "jobID", Val(lblJobID.Caption))

Dim l_sngVATRate As Single

If l_intDoNotChargeVAT <> 0 Then
    l_sngVATRate = 0
Else
    l_sngVATRate = GetVATRate(Format(GetData("company", "vatcode", "companyID", GetData("job", "companyID", "jobID", Val(lblJobID.Caption)))))
End If

l_dblVAT = l_dblTotal * (l_sngVATRate / 100)

'get the gross total (inc VAT)
l_dblGrossTotal = l_dblTotal + l_dblVAT
txtNetTotal.Text = Format(l_dblTotal, "#0.00")
txtVATTotal.Text = Format(l_dblVAT, "#0.00")
txtGrossTotal.Text = Format(l_dblGrossTotal, "#0.00")

Dim l_dblRateCardPrice  As Double
Dim l_dblRateCardNet    As Double
Dim l_dblRateCardVAT    As Double
Dim l_dblRateCardGross  As Double

'this will update the ratecard values - if this code is a virtual code
If IsVirtualCode(cmbCostCode.Text) Then
    l_dblRateCardPrice = l_dblUnitCharge
    l_dblRateCardNet = l_dblTotal
End If

l_dblRateCardPrice = Val(txtRateCardPrice.Text)
l_dblRateCardNet = (l_dblRateCardPrice * l_sngQuantity * l_sngTime)
l_dblRateCardVAT = RoundToCurrency(l_dblRateCardNet * (l_sngVATRate / 100))
l_dblRateCardGross = l_dblRateCardNet + l_dblRateCardVAT

txtRateCardPrice.Text = Format(l_dblRateCardPrice, "#0.00")
txtRateCardNet.Text = Format(l_dblRateCardNet, "#0.00")
txtRateCardVAT.Text = Format(l_dblRateCardVAT, "#0.00")
txtRateCardGross.Text = Format(l_dblRateCardGross, "#0.00")

End Sub

Private Sub cmbCostCode_GotFocus()
HighLite cmbCostCode
End Sub

Private Sub cmbUnit_GotFocus()
HighLite cmbUnit
End Sub

Private Sub cmdCalculateTotals_Click()
CalculateTotals
End Sub

Private Sub cmdCancel_Click()
Me.Tag = "CANCELLED"
Me.Hide

End Sub

Private Sub cmdOK_Click()
Me.Hide

End Sub

Private Sub Form_Load()

'populate the combo box
Dim l_intLoop As Integer

For l_intLoop = 12 To 15
    lblCaption(l_intLoop).Caption = g_strCurrency
Next

'and the units
PopulateCombo "unit", cmbUnit

'get the cost codes
Dim l_strSQL As String
l_strSQL = "SELECT cetacode, description, nominalcode, price1, unit, fd_orderby FROM ratecard WHERE companyID = 0 ORDER BY cetacode;"
adoCostCode.RecordSource = l_strSQL
adoCostCode.ConnectionString = g_strConnection
adoCostCode.Refresh

cmbCostCode.Columns(0).Width = 1500
cmbCostCode.Columns(1).Width = 3000
cmbCostCode.Columns(2).Width = 1300
cmbCostCode.Columns(3).Width = 1000
cmbCostCode.Columns(4).Width = 1000
cmbCostCode.Columns(5).Width = 1300

End Sub



Private Sub Text1_KeyPress(KeyAscii As Integer)
If KeyAscii = vbKeyReturn Then
    KeyAscii = 0
    CalculateTotals
End If
End Sub

Private Sub Text1_LostFocus()

End Sub

Private Sub txtDescription_GotFocus()
HighLite txtDescription
End Sub

Private Sub txtDiscount_GotFocus()
HighLite txtDiscount
End Sub

Private Sub txtDiscount_KeyPress(KeyAscii As Integer)
If KeyAscii = vbKeyReturn Then
    KeyAscii = 0
    CalculateTotals
End If
End Sub

Private Sub txtDiscount_LostFocus()
CalculateTotals
End Sub

Private Sub txtQuantity_GotFocus()
HighLite txtQuantity
End Sub

Private Sub txtQuantity_KeyPress(KeyAscii As Integer)
If KeyAscii = vbKeyReturn Then
    KeyAscii = 0
    CalculateTotals
End If
End Sub

Private Sub txtQuantity_LostFocus()
CalculateTotals
End Sub

Private Sub txtTime_GotFocus()
HighLite txtTime
End Sub

Private Sub txtTime_KeyPress(KeyAscii As Integer)
If KeyAscii = vbKeyReturn Then
    KeyAscii = 0
    CalculateTotals
End If
End Sub

Private Sub txtTime_LostFocus()
CalculateTotals
End Sub

Private Sub txtUnitCharge_GotFocus()
HighLite txtUnitCharge
End Sub

Private Sub txtUnitCharge_KeyPress(KeyAscii As Integer)
If KeyAscii = vbKeyReturn Then
    KeyAscii = 0
    CalculateTotals
End If
End Sub

Private Sub txtUnitCharge_LostFocus()
CalculateTotals
End Sub
