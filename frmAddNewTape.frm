VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmAddNewTape 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create New Library Record"
   ClientHeight    =   3720
   ClientLeft      =   7545
   ClientTop       =   3870
   ClientWidth     =   3885
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3720
   ScaleWidth      =   3885
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkKeepFormOpen 
      Caption         =   "Add multiple items"
      Height          =   255
      Left            =   120
      TabIndex        =   15
      Top             =   2760
      Width           =   1695
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "Add"
      Height          =   315
      Left            =   2640
      TabIndex        =   6
      Top             =   2760
      Width           =   1155
   End
   Begin VB.TextBox txtInput 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      Height          =   315
      Index           =   1
      Left            =   1200
      TabIndex        =   4
      Top             =   1800
      Width           =   2595
   End
   Begin MSComCtl2.DTPicker datMadeOnDate 
      Height          =   315
      Left            =   1200
      TabIndex        =   3
      Top             =   1380
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      Format          =   77660161
      CurrentDate     =   38477
   End
   Begin VB.TextBox txtInput 
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'None
      Height          =   315
      Index           =   0
      Left            =   1200
      TabIndex        =   5
      ToolTipText     =   "Scan barcode here to add to library database"
      Top             =   2340
      Width           =   2595
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   315
      Left            =   2640
      TabIndex        =   8
      Top             =   3300
      Width           =   1155
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   315
      Left            =   1380
      TabIndex        =   7
      Top             =   3300
      Width           =   1155
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbFormat 
      Height          =   315
      Left            =   1200
      TabIndex        =   0
      ToolTipText     =   "Choose Category"
      Top             =   120
      Width           =   2595
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   5424
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4577
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbLocation 
      Height          =   315
      Left            =   1200
      TabIndex        =   2
      ToolTipText     =   "Choose Category"
      Top             =   960
      Width           =   2595
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   5424
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4577
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbStockCode 
      Height          =   315
      Left            =   1200
      TabIndex        =   1
      ToolTipText     =   "Choose Category"
      Top             =   540
      Width           =   2595
      BevelWidth      =   0
      DataFieldList   =   "Column 0"
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   5424
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4577
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000002&
      Index           =   1
      X1              =   120
      X2              =   3780
      Y1              =   3180
      Y2              =   3180
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000002&
      Index           =   0
      X1              =   120
      X2              =   3780
      Y1              =   2220
      Y2              =   2220
   End
   Begin VB.Label Label1 
      Caption         =   "Stock Code"
      Height          =   255
      Index           =   5
      Left            =   120
      TabIndex        =   14
      Top             =   540
      Width           =   915
   End
   Begin VB.Label Label1 
      Caption         =   "Project #"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   13
      Top             =   1800
      Width           =   915
   End
   Begin VB.Label Label1 
      Caption         =   "Made On"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   12
      Top             =   1380
      Width           =   915
   End
   Begin VB.Label Label1 
      Caption         =   "Location "
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   11
      Top             =   960
      Width           =   915
   End
   Begin VB.Label Label1 
      Caption         =   "Format"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   10
      Top             =   120
      Width           =   915
   End
   Begin VB.Label Label1 
      Caption         =   "Barcode"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   9
      Top             =   2340
      Width           =   915
   End
End
Attribute VB_Name = "frmAddNewTape"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmbFormat_Click()
cmbStockCode.RemoveAll
PopulateCombo "stockcodesforformat" & cmbFormat.Text, cmbStockCode
End Sub

Private Sub cmbFormat_GotFocus()
HighLite cmbFormat
End Sub

Private Sub cmbLocation_GotFocus()
HighLite cmbLocation
End Sub

Private Sub cmbStockCode_GotFocus()
HighLite cmbStockCode
End Sub

Private Sub cmdAdd_Click()

Dim l_lngLibraryID As Long
l_lngLibraryID = CreateNewLibraryItem(cmbFormat.Text, cmbStockCode.Text, cmbLocation.Text, datMadeOnDate.Value, Val(txtInput(1).Text), txtInput(0).Text)

Const l_strMsg = "Library item could not be created "

Select Case l_lngLibraryID
Case -1
    MsgBox l_strMsg & " due to the project number being invalid.", vbExclamation
Case -2
    MsgBox l_strMsg & " due to the barcode already existing in the library database.", vbExclamation
Case Else
    'put the cursor back in the barcode box, and allow more tapes to be added
    txtInput(0).SetFocus
End Select

'keep the form open if required
If frmAddNewTape.chkKeepFormOpen.Value <> 0 Then Exit Sub

'close the form
cmdOK.Value = True

End Sub

Private Sub cmdCancel_Click()
Me.Tag = "CANCELLED"
Me.Hide
End Sub

Private Sub cmdOK_Click()
Me.Hide
End Sub

Private Sub Command1_Click()


End Sub

Private Sub Form_Load()
PopulateCombo "format", cmbFormat
PopulateCombo "location", cmbLocation
datMadeOnDate.Value = Date
CenterForm Me
End Sub

Private Sub txtInput_GotFocus(Index As Integer)
HighLite txtInput(Index)
End Sub

Private Sub txtInput_KeyPress(Index As Integer, KeyAscii As Integer)
Select Case Index
Case 0
    If KeyAscii = 13 Then
        KeyAscii = 0
        cmdAdd_Click
        HighLite txtInput(0)
    End If
Case 1
End Select
End Sub
