VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmPurchaseOrder 
   Caption         =   "Purchase Order"
   ClientHeight    =   10065
   ClientLeft      =   2085
   ClientTop       =   3090
   ClientWidth     =   14115
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPurchaseOrder.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   10065
   ScaleWidth      =   14115
   WindowState     =   2  'Maximized
   Begin VB.OptionButton optCurrency 
      Caption         =   "Euros"
      Height          =   255
      Index           =   2
      Left            =   12660
      TabIndex        =   118
      Top             =   2340
      Width           =   975
   End
   Begin VB.OptionButton optCurrency 
      Caption         =   "Dollars"
      Height          =   255
      Index           =   1
      Left            =   11640
      TabIndex        =   117
      Top             =   2340
      Width           =   975
   End
   Begin VB.OptionButton optCurrency 
      Caption         =   "Sterling"
      Height          =   255
      Index           =   0
      Left            =   10560
      TabIndex        =   116
      Top             =   2340
      Value           =   -1  'True
      Width           =   975
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnSupplierRates 
      Bindings        =   "frmPurchaseOrder.frx":08CA
      Height          =   2235
      Left            =   5940
      TabIndex        =   65
      Top             =   3480
      Width           =   7395
      DataFieldList   =   "partreference"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16051436
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   2434
      Columns(0).Caption=   "Reference"
      Columns(0).Name =   "partreference"
      Columns(0).DataField=   "partreference"
      Columns(0).FieldLen=   256
      Columns(1).Width=   6959
      Columns(1).Caption=   "Description"
      Columns(1).Name =   "description"
      Columns(1).DataField=   "description"
      Columns(1).FieldLen=   256
      Columns(2).Width=   2223
      Columns(2).Caption=   "Price"
      Columns(2).Name =   "price0"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "price0"
      Columns(2).DataType=   6
      Columns(2).NumberFormat=   "�0.00"
      Columns(2).FieldLen=   256
      _ExtentX        =   13044
      _ExtentY        =   3942
      _StockProps     =   77
      DataFieldToDisplay=   "partreference"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnOurReference 
      Bindings        =   "frmPurchaseOrder.frx":08E9
      Height          =   2235
      Left            =   2100
      TabIndex        =   115
      Top             =   3360
      Width           =   5535
      DataFieldList   =   "code"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16051436
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3016
      Columns(0).Caption=   "Our Ref"
      Columns(0).Name =   "code"
      Columns(0).DataField=   "code"
      Columns(0).FieldLen=   256
      Columns(1).Width=   6085
      Columns(1).Caption=   "Description"
      Columns(1).Name =   "description"
      Columns(1).DataField=   "description"
      Columns(1).FieldLen=   256
      _ExtentX        =   9763
      _ExtentY        =   3942
      _StockProps     =   77
      DataFieldToDisplay=   "code"
   End
   Begin VB.CommandButton cmdLaunch 
      Appearance      =   0  'Flat
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   4
      Left            =   7440
      MaskColor       =   &H00FFFFFF&
      Style           =   1  'Graphical
      TabIndex        =   111
      ToolTipText     =   "Allows the adding of a job ID, even after a purchase order has been completed."
      Top             =   120
      UseMaskColor    =   -1  'True
      Width           =   330
   End
   Begin VB.CommandButton cmdLaunch 
      Appearance      =   0  'Flat
      Height          =   330
      Index           =   3
      Left            =   9960
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmPurchaseOrder.frx":0907
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Edit product details"
      Top             =   1200
      UseMaskColor    =   -1  'True
      Width           =   330
   End
   Begin VB.PictureBox picDeliveryAddress 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      ForeColor       =   &H80000008&
      Height          =   3855
      Left            =   5340
      ScaleHeight     =   3825
      ScaleWidth      =   4395
      TabIndex        =   86
      Top             =   2220
      Visible         =   0   'False
      Width           =   4425
      Begin VB.CommandButton cmdClearDeliveryAddress 
         Caption         =   "Clear"
         Height          =   315
         Left            =   1920
         TabIndex        =   104
         ToolTipText     =   "Clear the form"
         Top             =   3420
         Width           =   1155
      End
      Begin VB.CommandButton cmdShowDeliveryAddress 
         Caption         =   "OK"
         Height          =   315
         Index           =   1
         Left            =   3180
         TabIndex        =   103
         Top             =   3420
         Width           =   1125
      End
      Begin VB.TextBox txtCountry 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   3300
         MaxLength       =   50
         TabIndex        =   93
         ToolTipText     =   "Country of Despatch"
         Top             =   2580
         Width           =   975
      End
      Begin VB.TextBox txtPostCode 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   960
         MaxLength       =   10
         TabIndex        =   92
         ToolTipText     =   "Postcode of Despatch"
         Top             =   2580
         Width           =   1155
      End
      Begin VB.TextBox txtAddress 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   975
         Left            =   960
         MaxLength       =   255
         MultiLine       =   -1  'True
         TabIndex        =   91
         ToolTipText     =   "Address of Despatch"
         Top             =   1500
         Width           =   3315
      End
      Begin VB.CheckBox chkUseCompany 
         Caption         =   "Display company address list"
         Height          =   195
         Left            =   960
         TabIndex        =   90
         ToolTipText     =   "Should the Client List Addresses be used? If unchecked, you will be viewing the delivery address list."
         Top             =   360
         Width           =   2715
      End
      Begin VB.TextBox txtTelephone 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   960
         MaxLength       =   255
         MultiLine       =   -1  'True
         TabIndex        =   89
         ToolTipText     =   "Telephone number of Despatch"
         Top             =   3000
         Width           =   3315
      End
      Begin VB.CommandButton cmdLaunch 
         Appearance      =   0  'Flat
         Height          =   315
         Index           =   2
         Left            =   2220
         MaskColor       =   &H00FFFFFF&
         Picture         =   "frmPurchaseOrder.frx":0D2F
         Style           =   1  'Graphical
         TabIndex        =   88
         ToolTipText     =   "Show on-line map (using multimap)"
         Top             =   2580
         UseMaskColor    =   -1  'True
         Width           =   300
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbDeliverToCompany 
         Bindings        =   "frmPurchaseOrder.frx":1141
         Height          =   315
         Left            =   960
         TabIndex        =   94
         ToolTipText     =   "Company Name for Despatch"
         Top             =   660
         Width           =   3315
         DataFieldList   =   "name"
         ListAutoValidate=   0   'False
         BevelType       =   0
         _Version        =   196617
         BorderStyle     =   0
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   7
         Columns(0).Width=   5609
         Columns(0).Caption=   "Company Name"
         Columns(0).Name =   "name"
         Columns(0).DataField=   "name"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "address"
         Columns(1).Name =   "address"
         Columns(1).DataField=   "address"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "postcode"
         Columns(2).Name =   "postcode"
         Columns(2).DataField=   "postcode"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "country"
         Columns(3).Name =   "country"
         Columns(3).DataField=   "country"
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Caption=   "attentionof"
         Columns(4).Name =   "attentionof"
         Columns(4).DataField=   "attentionof"
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Caption=   "telephone"
         Columns(5).Name =   "telephone"
         Columns(5).DataField=   "telephone"
         Columns(5).FieldLen=   256
         Columns(6).Width=   3201
         Columns(6).Caption=   "theID"
         Columns(6).Name =   "theID"
         Columns(6).DataField=   "theID"
         Columns(6).FieldLen=   256
         _ExtentX        =   5847
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "name"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbDeliverToContact 
         Bindings        =   "frmPurchaseOrder.frx":115A
         Height          =   315
         Left            =   960
         TabIndex        =   95
         ToolTipText     =   "For the Attention of?"
         Top             =   1080
         Width           =   3315
         DataFieldList   =   "name"
         BevelType       =   0
         _Version        =   196617
         BorderStyle     =   0
         ColumnHeaders   =   0   'False
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   5609
         Columns(0).Caption=   "Company Name"
         Columns(0).Name =   "name"
         Columns(0).DataField=   "name"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "companyID"
         Columns(1).Name =   "companyID"
         Columns(1).DataField=   "companyID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "contactID"
         Columns(2).Name =   "contactID"
         Columns(2).DataField=   "contactID"
         Columns(2).FieldLen=   256
         _ExtentX        =   5847
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "name"
      End
      Begin VB.Label lblDeliverToCompanyID 
         Appearance      =   0  'Flat
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   4680
         TabIndex        =   102
         Top             =   840
         Width           =   435
      End
      Begin VB.Label lblCaption 
         BackStyle       =   0  'Transparent
         Caption         =   "Country"
         Height          =   315
         Index           =   34
         Left            =   2580
         TabIndex        =   101
         Top             =   2580
         Width           =   615
      End
      Begin VB.Label lblCaption 
         BackStyle       =   0  'Transparent
         Caption         =   "Post Code"
         Height          =   315
         Index           =   31
         Left            =   120
         TabIndex        =   100
         Top             =   2580
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         BackStyle       =   0  'Transparent
         Caption         =   "Address"
         Height          =   315
         Index           =   29
         Left            =   120
         TabIndex        =   99
         Top             =   1500
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         BackStyle       =   0  'Transparent
         Caption         =   "Send To"
         Height          =   315
         Index           =   28
         Left            =   120
         TabIndex        =   98
         Top             =   660
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         BackStyle       =   0  'Transparent
         Caption         =   "Contact"
         Height          =   315
         Index           =   27
         Left            =   120
         TabIndex        =   97
         Top             =   1080
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         BackStyle       =   0  'Transparent
         Caption         =   "Telephone"
         Height          =   315
         Index           =   26
         Left            =   120
         TabIndex        =   96
         Top             =   3000
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         BackColor       =   &H80000002&
         Caption         =   " Delivery Address For This Order"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000009&
         Height          =   285
         Index           =   33
         Left            =   0
         TabIndex        =   87
         Top             =   0
         Width           =   4815
      End
   End
   Begin VB.CommandButton cmdShowDeliveryAddress 
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   4200
      TabIndex        =   85
      Top             =   1560
      Width           =   285
   End
   Begin VB.CheckBox chkDeliverTo 
      Alignment       =   1  'Right Justify
      Caption         =   "Deliver to different address"
      Height          =   195
      Left            =   1260
      TabIndex        =   82
      Top             =   1590
      Width           =   2835
   End
   Begin VB.PictureBox picReturn 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   60
      ScaleHeight     =   435
      ScaleWidth      =   10395
      TabIndex        =   73
      Top             =   2220
      Visible         =   0   'False
      Width           =   10395
      Begin VB.ComboBox cmbReturnMethod 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   6240
         TabIndex        =   76
         ToolTipText     =   "The return by time"
         Top             =   60
         Width           =   1695
      End
      Begin VB.ComboBox cmbEndTime 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   3420
         TabIndex        =   75
         Text            =   "17:30"
         ToolTipText     =   "Time the job is due to end"
         Top             =   60
         Width           =   1035
      End
      Begin VB.ComboBox cmbReturnFor 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   8520
         TabIndex        =   74
         Top             =   60
         Width           =   1695
      End
      Begin MSComCtl2.DTPicker datReturnDate 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   3
         EndProperty
         Height          =   285
         Left            =   1200
         TabIndex        =   77
         ToolTipText     =   "The return by date"
         Top             =   60
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   503
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   83623937
         CurrentDate     =   37870
      End
      Begin VB.Label lblCaption 
         Caption         =   "Return By"
         Height          =   285
         Index           =   11
         Left            =   60
         TabIndex        =   81
         Top             =   60
         Width           =   1035
      End
      Begin VB.Label lblCaption 
         Caption         =   "Time"
         Height          =   285
         Index           =   32
         Left            =   2880
         TabIndex        =   80
         Top             =   120
         Width           =   495
      End
      Begin VB.Label lblCaption 
         Caption         =   "Return Method"
         Height          =   285
         Index           =   12
         Left            =   4980
         TabIndex        =   79
         Top             =   60
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "For"
         Height          =   285
         Index           =   13
         Left            =   8040
         TabIndex        =   78
         Top             =   60
         Width           =   375
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnCostCodes 
      Bindings        =   "frmPurchaseOrder.frx":1173
      Height          =   2235
      Left            =   780
      TabIndex        =   64
      Top             =   3180
      Width           =   3075
      DataFieldList   =   "code"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16051436
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3016
      Columns(0).Caption=   "Budget Code"
      Columns(0).Name =   "code"
      Columns(0).DataField=   "code"
      Columns(0).FieldLen=   256
      Columns(1).Width=   6085
      Columns(1).Caption=   "Description"
      Columns(1).Name =   "description"
      Columns(1).DataField=   "description"
      Columns(1).FieldLen=   256
      _ExtentX        =   5424
      _ExtentY        =   3942
      _StockProps     =   77
      DataFieldToDisplay=   "code"
   End
   Begin VB.TextBox txtAuthorisationNumber 
      Appearance      =   0  'Flat
      BackColor       =   &H0080C0FF&
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   1260
      TabIndex        =   0
      Top             =   120
      Width           =   1275
   End
   Begin VB.ComboBox cmbDeliveryFor 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   8580
      TabIndex        =   60
      Top             =   1920
      Width           =   1695
   End
   Begin VB.ComboBox cmbStartTime 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   3480
      TabIndex        =   57
      Text            =   "09:30"
      ToolTipText     =   "Time the job is due to commence"
      Top             =   1920
      Width           =   1035
   End
   Begin VB.CommandButton cmdLaunch 
      Appearance      =   0  'Flat
      Height          =   330
      Index           =   1
      Left            =   4560
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmPurchaseOrder.frx":118E
      Style           =   1  'Graphical
      TabIndex        =   56
      ToolTipText     =   "Open this company's details"
      Top             =   840
      UseMaskColor    =   -1  'True
      Width           =   330
   End
   Begin VB.CommandButton cmdLaunch 
      Appearance      =   0  'Flat
      Height          =   330
      Index           =   0
      Left            =   4560
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmPurchaseOrder.frx":15C1
      Style           =   1  'Graphical
      TabIndex        =   55
      ToolTipText     =   "Send email"
      Top             =   1200
      UseMaskColor    =   -1  'True
      Width           =   330
   End
   Begin VB.PictureBox picHistory 
      BorderStyle     =   0  'None
      Height          =   2115
      Left            =   10560
      ScaleHeight     =   2115
      ScaleWidth      =   3195
      TabIndex        =   46
      Top             =   120
      Width           =   3195
      Begin VB.Label lblAuthRequestUser 
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1020
         TabIndex        =   72
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "The initials of the user who modified this job"
         Top             =   600
         Width           =   435
      End
      Begin VB.Label lblReceivedUser 
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1020
         TabIndex        =   71
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "The initials of the user who recieved this job in Accounts"
         Top             =   1200
         Width           =   435
      End
      Begin VB.Label lblReceivedDate 
         BackColor       =   &H80000005&
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "dddd, d MMMM yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   0
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1560
         TabIndex        =   70
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "When this job wasrecieved in Accounts"
         Top             =   1200
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Received"
         Height          =   255
         Index           =   15
         Left            =   0
         TabIndex        =   69
         Top             =   1230
         Width           =   975
      End
      Begin VB.Label lblCaption 
         Caption         =   "Authorised"
         Height          =   255
         Index           =   21
         Left            =   0
         TabIndex        =   52
         Top             =   930
         Width           =   915
      End
      Begin VB.Label lblAuthorisedDate 
         BackColor       =   &H80000005&
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "dddd, d MMMM yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   0
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1560
         TabIndex        =   17
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "When this job was sent to Accounts"
         Top             =   900
         Width           =   1575
      End
      Begin VB.Label lblAuthorisedUser 
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1020
         TabIndex        =   16
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "The initials of the user who sent this job to Accounts"
         Top             =   900
         Width           =   435
      End
      Begin VB.Label lblCaption 
         Caption         =   "Created"
         Height          =   255
         Index           =   20
         Left            =   0
         TabIndex        =   51
         Top             =   30
         Width           =   855
      End
      Begin VB.Label lblCreatedDate 
         BackColor       =   &H80000005&
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "dddd, d MMMM yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   0
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1560
         TabIndex        =   12
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "When this job was booked"
         Top             =   0
         Width           =   1575
      End
      Begin VB.Label lblCreatedUser 
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1020
         TabIndex        =   11
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "The initials of the user who booked this job"
         Top             =   0
         Width           =   435
      End
      Begin VB.Label lblCaption 
         Caption         =   "Auth Request"
         Height          =   255
         Index           =   19
         Left            =   0
         TabIndex        =   50
         Top             =   630
         Width           =   1035
      End
      Begin VB.Label lblRequestedDate 
         BackColor       =   &H80000005&
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "dddd, d MMMM yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   0
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1560
         TabIndex        =   15
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "Authorisation requested by"
         Top             =   600
         Width           =   1575
      End
      Begin VB.Label lblModifiedUser 
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1020
         TabIndex        =   13
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "The initials of the user who modified this job"
         Top             =   300
         Width           =   435
      End
      Begin VB.Label lblModifiedDate 
         BackColor       =   &H80000005&
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "dddd, d MMMM yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   0
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1560
         TabIndex        =   14
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "When this job was last modified"
         Top             =   300
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Modified"
         Height          =   255
         Index           =   18
         Left            =   0
         TabIndex        =   49
         Top             =   330
         Width           =   855
      End
      Begin VB.Label lblCancelledUser 
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1020
         TabIndex        =   20
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "The initials of the user who cancelled this job"
         Top             =   1800
         Width           =   435
      End
      Begin VB.Label lblCancelledDate 
         BackColor       =   &H80000005&
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "dddd, d MMMM yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   0
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1560
         TabIndex        =   21
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "When this job was cancelled"
         Top             =   1800
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Cancelled"
         Height          =   255
         Index           =   17
         Left            =   0
         TabIndex        =   48
         Top             =   1830
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "A/C Auth"
         Height          =   255
         Index           =   14
         Left            =   0
         TabIndex        =   47
         Top             =   1530
         Width           =   855
      End
      Begin VB.Label lblAccountsAuthorisedDate 
         BackColor       =   &H80000005&
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "dddd, d MMMM yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2057
            SubFormatType   =   0
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1560
         TabIndex        =   19
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "When this job was invoiced"
         Top             =   1500
         Width           =   1575
      End
      Begin VB.Label lblAccountsAuthorisedUser 
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1020
         TabIndex        =   18
         Tag             =   "CLEARFIELDS"
         ToolTipText     =   "The initials of the user who invoiced this job"
         Top             =   1500
         Width           =   435
      End
   End
   Begin VB.ComboBox cmbDepartment 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6300
      Style           =   2  'Dropdown List
      TabIndex        =   6
      ToolTipText     =   "Department"
      Top             =   480
      Width           =   3975
   End
   Begin VB.ComboBox cmbOrderType 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1260
      Style           =   2  'Dropdown List
      TabIndex        =   2
      ToolTipText     =   "Purchase order type"
      Top             =   480
      Width           =   3255
   End
   Begin VB.TextBox txtNotes 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'None
      Height          =   1515
      Left            =   660
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   23
      ToolTipText     =   "Notes for the job"
      Top             =   6000
      Width           =   5475
   End
   Begin VB.PictureBox picFooter 
      BorderStyle     =   0  'None
      Height          =   1935
      Left            =   2520
      ScaleHeight     =   1935
      ScaleWidth      =   11235
      TabIndex        =   39
      Top             =   6000
      Width           =   11235
      Begin VB.CommandButton cmdEditNotes 
         Caption         =   "Edit Notes"
         Height          =   315
         Left            =   3780
         TabIndex        =   114
         ToolTipText     =   "Clear the form"
         Top             =   1200
         Width           =   2415
      End
      Begin VB.CommandButton cmdCancelAndCopy 
         Caption         =   "Cancel and Copy"
         Height          =   315
         Left            =   660
         TabIndex        =   113
         ToolTipText     =   "Create a copy of this order"
         Top             =   1620
         Width           =   1755
      End
      Begin VB.CommandButton cmdCopyAsNew 
         Caption         =   "Duplicate"
         Height          =   315
         Left            =   2520
         TabIndex        =   112
         ToolTipText     =   "Create a copy of this order"
         Top             =   1620
         Width           =   1155
      End
      Begin VB.CommandButton cmdPDF 
         Caption         =   "PDF"
         Height          =   315
         Left            =   5040
         TabIndex        =   105
         ToolTipText     =   "Print this job"
         Top             =   1620
         Width           =   1155
      End
      Begin VB.TextBox txtStatus 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   7620
         Locked          =   -1  'True
         TabIndex        =   83
         ToolTipText     =   "The title for the job"
         Top             =   1140
         Width           =   3615
      End
      Begin VB.CommandButton cmdItemsReceived 
         Caption         =   "Items Received"
         Enabled         =   0   'False
         Height          =   255
         Left            =   3780
         TabIndex        =   68
         ToolTipText     =   "Clear the form"
         Top             =   300
         Width           =   2415
      End
      Begin VB.CommandButton cmdEnterCompletionDetails 
         Caption         =   "Enter Completion  Details"
         Enabled         =   0   'False
         Height          =   255
         Left            =   3780
         TabIndex        =   67
         ToolTipText     =   "Clear the form"
         Top             =   600
         Width           =   2415
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   3780
         TabIndex        =   66
         ToolTipText     =   "Clear the form"
         Top             =   1620
         Width           =   1155
      End
      Begin VB.CommandButton cmdGetAuthorisationNumber 
         Caption         =   "Request Authorisation Number"
         Height          =   255
         Left            =   3780
         TabIndex        =   63
         ToolTipText     =   "Clear the form"
         Top             =   0
         Width           =   2415
      End
      Begin VB.TextBox txtInvoiceTotal 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H008080FF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   9540
         Locked          =   -1  'True
         TabIndex        =   25
         ToolTipText     =   "Invoiced total for the job"
         Top             =   360
         Width           =   1695
      End
      Begin VB.TextBox txtTotal 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   7620
         Locked          =   -1  'True
         TabIndex        =   24
         ToolTipText     =   "Grand total for the job"
         Top             =   360
         Width           =   1695
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Cancel Order"
         Height          =   315
         Left            =   7560
         TabIndex        =   27
         ToolTipText     =   "Cancel this job"
         Top             =   1620
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   6300
         TabIndex        =   26
         ToolTipText     =   "Print this job"
         Top             =   1620
         Width           =   1155
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   10080
         TabIndex        =   29
         ToolTipText     =   "Close this form"
         Top             =   1620
         Width           =   1155
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         Height          =   315
         Left            =   8820
         TabIndex        =   28
         ToolTipText     =   "Save changes to this job"
         Top             =   1620
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Current Status"
         Height          =   285
         Index           =   25
         Left            =   7620
         TabIndex        =   84
         Top             =   900
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Invoiced Total"
         Height          =   255
         Index           =   3
         Left            =   9540
         TabIndex        =   45
         Top             =   60
         Width           =   1395
      End
      Begin VB.Label lblCaption 
         Caption         =   "Grand Total"
         Height          =   255
         Index           =   22
         Left            =   7620
         TabIndex        =   42
         Top             =   60
         Width           =   975
      End
   End
   Begin VB.ComboBox cmbDeliveryMethod 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6300
      TabIndex        =   4
      ToolTipText     =   "The delivery deadline time"
      Top             =   1920
      Width           =   1695
   End
   Begin VB.TextBox txtPurchaseHeaderID 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000011&
      Height          =   285
      Left            =   3780
      TabIndex        =   1
      ToolTipText     =   "The unique identifier for this order"
      Top             =   120
      Width           =   735
   End
   Begin VB.TextBox txtJobID 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   6300
      Locked          =   -1  'True
      TabIndex        =   5
      ToolTipText     =   "A valid CETA Job ID"
      Top             =   120
      Width           =   1035
   End
   Begin VB.TextBox txtSupplierReference 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   6300
      TabIndex        =   7
      ToolTipText     =   "The clients order reference number"
      Top             =   840
      Width           =   3975
   End
   Begin VB.TextBox txtTitle 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   6300
      TabIndex        =   10
      ToolTipText     =   "The title for the job"
      Top             =   1560
      Width           =   3975
   End
   Begin MSComCtl2.DTPicker datDeliveryDate 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   285
      Left            =   1260
      TabIndex        =   3
      ToolTipText     =   "The delivery deadline date"
      Top             =   1920
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   503
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   83623937
      CurrentDate     =   37870
   End
   Begin MSAdodcLib.Adodc adoPurchaseOrderDetail 
      Height          =   330
      Left            =   120
      Top             =   9540
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   2
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoPurchaseOrderDetail"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   285
      Left            =   1260
      TabIndex        =   53
      ToolTipText     =   "The company this job is for"
      Top             =   840
      Width           =   3255
      BevelWidth      =   0
      DataFieldList   =   "name"
      BevelType       =   0
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "accountcode"
      Columns(2).Name =   "accountcode"
      Columns(2).DataField=   "accountcode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "telephone"
      Columns(3).Name =   "telephone"
      Columns(3).DataField=   "telephone"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "cetaclientcode"
      Columns(4).Name =   "cetaclientcode"
      Columns(4).DataField=   "cetaclientcode"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "accountstatus"
      Columns(5).Name =   "accountstatus"
      Columns(5).DataField=   "accountstatus"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "fax"
      Columns(6).Name =   "fax"
      Columns(6).DataField=   "fax"
      Columns(6).FieldLen=   256
      _ExtentX        =   5741
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbContact 
      Height          =   285
      Left            =   1260
      TabIndex        =   54
      ToolTipText     =   "The contact for this job"
      Top             =   1200
      Width           =   3255
      DataFieldList   =   "name"
      BevelType       =   0
      _Version        =   196617
      BorderStyle     =   0
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   5424
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "telephone"
      Columns(1).Name =   "telephone"
      Columns(1).DataField=   "telephone"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "contactID"
      Columns(2).Name =   "contactID"
      Columns(2).DataField=   "contactID"
      Columns(2).FieldLen=   256
      _ExtentX        =   5741
      _ExtentY        =   503
      _StockProps     =   93
      ForeColor       =   -2147483640
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin MSAdodcLib.Adodc adoCostCodes 
      Height          =   330
      Left            =   120
      Top             =   9060
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCostCodes"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoSupplierRates 
      Height          =   330
      Left            =   120
      Top             =   8580
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoSupplierRates"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbProduct 
      Height          =   255
      Left            =   6300
      TabIndex        =   8
      ToolTipText     =   "The product on this job"
      Top             =   1200
      Width           =   3555
      BevelWidth      =   0
      DataFieldList   =   "name"
      BevelType       =   0
      _Version        =   196617
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      CheckBox3D      =   0   'False
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   8438015
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "productID"
      Columns(0).Name =   "productID"
      Columns(0).DataField=   "productID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   5212
      Columns(1).Caption=   "Name"
      Columns(1).Name =   "name"
      Columns(1).DataField=   "name"
      Columns(1).FieldLen=   256
      _ExtentX        =   6271
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   8438015
      DataFieldToDisplay=   "name"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbProject 
      Height          =   255
      Left            =   8700
      TabIndex        =   109
      ToolTipText     =   "The name of this project. In the previous version of CETA, this was known as the 'Special Job Number'"
      Top             =   120
      Width           =   1575
      BevelWidth      =   0
      DataFieldList   =   "projectnumber"
      BevelType       =   0
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      CheckBox3D      =   0   'False
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   12648447
      RowHeight       =   423
      Columns.Count   =   9
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "projectID"
      Columns(0).Name =   "projectID"
      Columns(0).DataField=   "projectID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   2566
      Columns(1).Caption=   "Project Number"
      Columns(1).Name =   "name"
      Columns(1).DataField=   "projectnumber"
      Columns(1).FieldLen=   256
      Columns(2).Width=   4604
      Columns(2).Caption=   "Product"
      Columns(2).Name =   "productname"
      Columns(2).DataField=   "productname"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3757
      Columns(3).Caption=   "Company"
      Columns(3).Name =   "companyname"
      Columns(3).DataField=   "companyname"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "Contact"
      Columns(4).Name =   "contactname"
      Columns(4).DataField=   "contactname"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "Status"
      Columns(5).Name =   "status"
      Columns(5).DataField=   "fd_status"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "productID"
      Columns(6).Name =   "productID"
      Columns(6).DataField=   "productID"
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "companyID"
      Columns(7).Name =   "companyID"
      Columns(7).DataField=   "companyID"
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "contactID"
      Columns(8).Name =   "contactID"
      Columns(8).DataField=   "contactID"
      Columns(8).FieldLen=   256
      _ExtentX        =   2778
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   12648447
      DataFieldToDisplay=   "projectnumber"
   End
   Begin MSAdodcLib.Adodc adoOurReference 
      Height          =   330
      Left            =   3600
      Top             =   9480
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoOurReference"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdOrderDetail 
      Bindings        =   "frmPurchaseOrder.frx":19F9
      Height          =   3135
      Left            =   120
      TabIndex        =   22
      Top             =   2760
      Width           =   13650
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      BackColorOdd    =   12640511
      RowHeight       =   423
      Columns.Count   =   17
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "purchaseheaderID"
      Columns(0).Name =   "purchaseheaderID"
      Columns(0).DataField=   "purchaseheaderID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "resourcescheduleID"
      Columns(1).Name =   "resourcescheduleID"
      Columns(1).DataField=   "resourcescheduleID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   2275
      Columns(2).Caption=   "Budget Code"
      Columns(2).Name =   "cetacode"
      Columns(2).DataField=   "cetacode"
      Columns(2).FieldLen=   256
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   13233660
      Columns(3).Width=   1561
      Columns(3).Caption=   "Our Ref"
      Columns(3).Name =   "ourreference"
      Columns(3).DataField=   "ourreference"
      Columns(3).FieldLen=   256
      Columns(4).Width=   1614
      Columns(4).Caption=   "Time"
      Columns(4).Name =   "fd_time"
      Columns(4).DataField=   "fd_time"
      Columns(4).DataType=   9
      Columns(4).FieldLen=   256
      Columns(5).Width=   1296
      Columns(5).Caption=   "Qty"
      Columns(5).Name =   "quantity"
      Columns(5).DataField=   "quantity"
      Columns(5).DataType=   3
      Columns(5).FieldLen=   256
      Columns(6).Width=   2011
      Columns(6).Caption=   "Part Ref"
      Columns(6).Name =   "suppliercode"
      Columns(6).DataField=   "suppliercode"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   6112
      Columns(7).Caption=   "Description"
      Columns(7).Name =   "description"
      Columns(7).DataField=   "description"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   20000
      Columns(7).VertScrollBar=   -1  'True
      Columns(8).Width=   1746
      Columns(8).Caption=   "Unit Charge"
      Columns(8).Name =   "unitcharge"
      Columns(8).Alignment=   1
      Columns(8).DataField=   "unitcharge"
      Columns(8).DataType=   6
      Columns(8).NumberFormat=   "�#.00"
      Columns(8).FieldLen=   256
      Columns(9).Width=   1455
      Columns(9).Caption=   "Discount"
      Columns(9).Name =   "discount"
      Columns(9).DataField=   "discount"
      Columns(9).FieldLen=   256
      Columns(10).Width=   2302
      Columns(10).Caption=   "Total"
      Columns(10).Name=   "total"
      Columns(10).Alignment=   1
      Columns(10).DataField=   "total"
      Columns(10).DataType=   6
      Columns(10).NumberFormat=   "�#.00"
      Columns(10).FieldLen=   256
      Columns(11).Width=   1588
      Columns(11).Caption=   "Qty Rec'd"
      Columns(11).Name=   "quantityreceived"
      Columns(11).DataField=   "quantityreceived"
      Columns(11).FieldLen=   256
      Columns(11).HasBackColor=   -1  'True
      Columns(11).BackColor=   11926999
      Columns(12).Width=   2223
      Columns(12).Caption=   "Invoice Number"
      Columns(12).Name=   "supplierinvoicenumber"
      Columns(12).DataField=   "supplierinvoicenumber"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(12).Locked=   -1  'True
      Columns(12).HasBackColor=   -1  'True
      Columns(12).BackColor=   8421631
      Columns(13).Width=   2223
      Columns(13).Caption=   "Invoice Amount"
      Columns(13).Name=   "supplierinvoiceamount"
      Columns(13).Alignment=   1
      Columns(13).DataField=   "supplierinvoiceamount"
      Columns(13).DataType=   6
      Columns(13).NumberFormat=   "�#.00"
      Columns(13).FieldLen=   256
      Columns(13).Locked=   -1  'True
      Columns(13).HasBackColor=   -1  'True
      Columns(13).BackColor=   8421631
      Columns(14).Width=   2196
      Columns(14).Caption=   "Invoice Date"
      Columns(14).Name=   "supplierinvoicedate"
      Columns(14).DataField=   "supplierinvoicedate"
      Columns(14).DataType=   7
      Columns(14).FieldLen=   256
      Columns(14).Locked=   -1  'True
      Columns(14).HasBackColor=   -1  'True
      Columns(14).BackColor=   8421631
      Columns(15).Width=   3200
      Columns(15).Caption=   "Discrepency Reason"
      Columns(15).Name=   "discrepencyreason"
      Columns(15).DataField=   "discrepencyreason"
      Columns(15).FieldLen=   256
      Columns(15).Locked=   -1  'True
      Columns(15).HasBackColor=   -1  'True
      Columns(15).BackColor=   8421631
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "purchasedetailID"
      Columns(16).Name=   "purchasedetailID"
      Columns(16).DataField=   "purchasedetailID"
      Columns(16).DataType=   3
      Columns(16).FieldLen=   256
      Columns(16).Locked=   -1  'True
      _ExtentX        =   24077
      _ExtentY        =   5530
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblProjectID 
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   3720
      TabIndex        =   110
      Tag             =   "CLEARFIELDS"
      Top             =   8760
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Project #"
      Height          =   285
      Index           =   35
      Left            =   7920
      TabIndex        =   108
      Top             =   120
      Width           =   675
   End
   Begin VB.Label lblProductID 
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   3720
      TabIndex        =   107
      Tag             =   "CLEARFIELDS"
      Top             =   9120
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Product"
      Height          =   255
      Index           =   46
      Left            =   5040
      TabIndex        =   106
      Top             =   1200
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Authorisation #"
      Height          =   285
      Index           =   24
      Left            =   120
      TabIndex        =   62
      Top             =   120
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "For"
      Height          =   285
      Index           =   23
      Left            =   8100
      TabIndex        =   61
      Top             =   1920
      Width           =   375
   End
   Begin VB.Label lblCaption 
      Caption         =   "Delivery Method"
      Height          =   285
      Index           =   7
      Left            =   5040
      TabIndex        =   59
      Top             =   1920
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Time"
      Height          =   285
      Index           =   30
      Left            =   2940
      TabIndex        =   58
      Top             =   1980
      Width           =   495
   End
   Begin VB.Label lblDepartmentID 
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   2460
      TabIndex        =   44
      Tag             =   "CLEARFIELDS"
      Top             =   9600
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Notes"
      Height          =   255
      Index           =   9
      Left            =   120
      TabIndex        =   43
      Top             =   6000
      Width           =   495
   End
   Begin VB.Label lblCompanyID 
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   2460
      TabIndex        =   41
      Tag             =   "CLEARFIELDS"
      Top             =   8760
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblContactID 
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   2460
      TabIndex        =   40
      Tag             =   "CLEARFIELDS"
      Top             =   9120
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Department"
      Height          =   285
      Index           =   10
      Left            =   5040
      TabIndex        =   38
      Top             =   480
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Purchase ID"
      ForeColor       =   &H80000011&
      Height          =   285
      Index           =   1
      Left            =   2640
      TabIndex        =   37
      Top             =   120
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job ID"
      Height          =   285
      Index           =   0
      Left            =   5040
      TabIndex        =   36
      Top             =   120
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Supplier"
      Height          =   285
      Index           =   2
      Left            =   120
      TabIndex        =   35
      Top             =   840
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Contact"
      Height          =   285
      Index           =   5
      Left            =   120
      TabIndex        =   34
      Top             =   1200
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Supplier Ref"
      Height          =   285
      Index           =   4
      Left            =   5040
      TabIndex        =   33
      Top             =   840
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      Height          =   285
      Index           =   8
      Left            =   5040
      TabIndex        =   32
      Top             =   1560
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Delivery By"
      Height          =   285
      Index           =   6
      Left            =   120
      TabIndex        =   31
      Top             =   1920
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Order Type"
      Height          =   285
      Index           =   16
      Left            =   120
      TabIndex        =   30
      Top             =   480
      Width           =   1035
   End
End
Attribute VB_Name = "frmPurchaseOrder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim m_strBudgetCode As String

Private Sub cmbCompany_Click()

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

adoSupplierRates.RecordSource = "SELECT partreference, description, price0 FROM supplierrate WHERE companyID = '" & lblCompanyID.Caption & "' ORDER BY description, partreference;"
adoSupplierRates.ConnectionString = g_strConnection
adoSupplierRates.Refresh


End Sub

Private Sub cmbCompany_DropDown()

Dim l_strSQL As String
l_strSQL = "SELECT name, accountcode, telephone, companyID FROM company WHERE name LIKE '" & QuoteSanitise(cmbCompany.Text) & "%' AND issupplier = 1 ORDER BY name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmbContact_Click()
lblContactID.Caption = cmbContact.Columns("contactID").Text
End Sub

Private Sub cmbContact_DropDown()

If lblCompanyID.Caption = "" Then
    NoCompanySelectedMessage
    Exit Sub
End If

Dim l_strSQL As String

l_strSQL = "SELECT company.companyID, contact.contactID, contact.name, contact.telephone, employee.jobtitle FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((company.companyID)=" & lblCompanyID.Caption & ")) ORDER BY contact.name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbContact.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmbDeliverToCompany_Click()

lblDeliverToCompanyID.Caption = cmbDeliverToCompany.Columns("theID").Text

txtAddress.Text = cmbDeliverToCompany.Columns("address").Text
txtPostCode.Text = cmbDeliverToCompany.Columns("postcode").Text
txtCountry.Text = cmbDeliverToCompany.Columns("country").Text
txtTelephone.Text = cmbDeliverToCompany.Columns("telephone").Text
If chkUseCompany.Value = False Then
    cmbContact.Text = cmbDeliverToCompany.Columns("attentionof").Text
End If



End Sub

Private Sub cmbDeliverToCompany_DropDown()

'populate the company drop down (data bound)
Dim l_strSQL As String

If chkUseCompany.Value = False Then
    l_strSQL = "SELECT deliveryaddressID as theID, deliverycompanyname as name, address, postcode, country, attentionof, telephone FROM deliveryaddress ORDER BY deliverycompanyname"
Else
    l_strSQL = "SELECT companyID as theID, name, address, postcode, country, '' as attentionof, telephone FROM company ORDER BY name"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbDeliverToCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing


End Sub

Private Sub cmbDeliverToContact_DropDown()

If lblDeliverToCompanyID.Caption = "" Then
    MsgBox "Please select a company before trying to select a contact", vbInformation
    cmbCompany.SetFocus
    Exit Sub
End If

Dim l_strSQL As String

l_strSQL = "SELECT company.companyID, contact.contactID, contact.name, contact.telephone, employee.jobtitle FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((company.name)='" & cmbDeliverToCompany.Text & "')) ORDER BY contact.name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbDeliverToContact.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmbDeliveryToCompany_DropDown()



End Sub

Private Sub cmbDeliveryToCompany_InitColumnProps()

End Sub

Private Sub cmbOrderType_Click()
If cmbOrderType.Text = "Hire" Or cmbOrderType.Text = "Freelance / Crew" Then
    picReturn.Visible = True
    datReturnDate.Value = datDeliveryDate.Value
    cmbEndTime.Text = "17:30"
Else
    picReturn.Visible = False
    datReturnDate.Value = Null
    cmbEndTime.Text = ""
End If
End Sub

Private Sub cmbProduct_Click()
lblProductID.Caption = cmbProduct.Columns("productID").Text


End Sub

Private Sub cmbProduct_DropDown()

Dim l_strSQL As String
l_strSQL = "SELECT name, productID FROM product WHERE name LIKE '" & QuoteSanitise(cmbProduct.Text) & "%' AND fd_status <> 'Cancelled' AND fd_status <> 'Hidden' AND fd_status <> 'Completed' ORDER BY name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbProduct.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub


Private Sub cmbProject_Click()

If cmbProject.Rows = 0 Then Exit Sub

lblProjectID.Caption = cmbProject.Columns("projectID").Text
txtTitle.Text = GetData("project", "subtitle", "projectID", Val(lblProjectID.Caption))

cmbProduct.Text = cmbProject.Columns("productname").Text
lblProductID.Caption = cmbProject.Columns("productID").Text

End Sub

Private Sub cmbProject_DropDown()

If cmbProject.Tag = "STOP" Then Exit Sub

Dim l_strSQL As String
l_strSQL = "SELECT project.projectnumber, project.projectID, project.companyname, project.contactname, project.fd_status, product.name as productname, product.productID, project.companyID, project.contactID FROM project LEFT JOIN product ON project.productID = product.productID WHERE (projectnumber LIKE '" & QuoteSanitise(cmbProject.Text) & "%') AND (project.fd_status IS NULL OR (project.fd_status <> 'COMPLETED' AND project.fd_status <> 'CANCEL' AND project.fd_status <> 'INVOICED')) ORDER BY projectnumber;"
Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbProject.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmdCancel_Click()

If Not CheckAccess("/cancelpurchaseorder") Then
    Exit Sub
End If

If Not IsNumeric(txtPurchaseHeaderID.Text) Then
    NoPurchaseOrderSelectedMessage
    Exit Sub
End If

Dim l_intMessage As Integer
l_intMessage = MsgBox("Do you really want to cancel this purchase order?", vbYesNo + vbQuestion)
If l_intMessage = vbNo Then Exit Sub


CancelPurchaseOrder Val(txtPurchaseHeaderID.Text)

ShowPurchaseOrder Val(txtPurchaseHeaderID.Text)


End Sub

Private Sub cmdCancelAndCopy_Click()

Dim l_lngPurchaseOrderID As Long
l_lngPurchaseOrderID = Val(txtPurchaseHeaderID.Text)

cmdCopyAsNew_Click

'check that the user didnt cancel
If Val(txtPurchaseHeaderID.Text) <> l_lngPurchaseOrderID Then CancelPurchaseOrder l_lngPurchaseOrderID

End Sub

Private Sub cmdClear_Click()
ClearFields Me
ShowPurchaseOrder -1
End Sub

Private Sub cmdClearDeliveryAddress_Click()

txtAddress.Text = ""
txtPostCode.Text = ""
txtCountry.Text = ""
txtTelephone.Text = ""
cmbDeliverToContact.Text = ""
lblDeliverToCompanyID.Caption = ""
cmbDeliverToCompany.Text = ""
chkDeliverTo.Value = vbUnchecked

End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdCopyAsNew_Click()




Dim l_intMsg As Integer
l_intMsg = MsgBox("Are you sure you want to create a NEW copy of this order?", vbQuestion + vbOKCancel)
If l_intMsg = vbCancel Then Exit Sub

Dim l_lngOriginalPurchaseHeaderID As Long
Dim l_lngNewPurchaseHeaderID As Long

l_lngOriginalPurchaseHeaderID = Val(txtPurchaseHeaderID.Text)

If l_lngOriginalPurchaseHeaderID = 0 Then Exit Sub


txtAuthorisationNumber.Text = ""
txtPurchaseHeaderID.Text = ""

cmdSave_Click

l_lngNewPurchaseHeaderID = Val(txtPurchaseHeaderID.Text)

CopyPurchaseLines l_lngOriginalPurchaseHeaderID, l_lngNewPurchaseHeaderID
 
ShowPurchaseOrder l_lngNewPurchaseHeaderID

End Sub

Private Sub cmdEditNotes_Click()

If Val(txtPurchaseHeaderID.Text) = 0 Then
    NoPurchaseOrderSelectedMessage
    Exit Sub
End If
    
    
frmTextEdit.txtTextToEdit.Text = txtNotes.Text
frmTextEdit.Show vbModal

Dim l_strSQL As String
l_strSQL = "UPDATE purchaseheader SET notes1 = '" & QuoteSanitise(frmTextEdit.txtTextToEdit.Text) & "' WHERE purchaseheaderID = '" & txtPurchaseHeaderID.Text & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

txtNotes.Text = frmTextEdit.txtTextToEdit.Text

Unload frmTextEdit
Set frmTextEdit = Nothing


End Sub

Private Sub cmdEnterCompletionDetails_Click()
CompletePurchaseOrder Val(txtPurchaseHeaderID.Text)
ShowPurchaseOrder Val(txtPurchaseHeaderID.Text)

End Sub

Private Sub cmdGetAuthorisationNumber_Click()

If Val(txtPurchaseHeaderID.Text) = 0 Then
    NoPurchaseOrderSelectedMessage
    Exit Sub
End If

Dim l_intMsg As Integer
l_intMsg = MsgBox("Are you sure you want to request authorisation for this order?" & vbCrLf & vbCrLf & "Is everything correct?", vbQuestion + vbYesNo)
If l_intMsg = vbNo Then Exit Sub

If lblCancelledDate.Caption <> "" Then
    MsgBox "You can not authorise cancelled orders.", vbExclamation
    Exit Sub
End If


If g_optRequireBudgetCodesBeforeAuthorisation = 1 Then
    Dim l_strSQL As String
    l_strSQL = "SELECT Count(purchasedetailID) FROM purchasedetail WHERE (cetacode IS NULL or cetacode = '') AND purchaseheaderID = '" & txtPurchaseHeaderID.Text & "';"
    
    If GetCount(l_strSQL) > 0 Then
        MsgBox "You can not complete this purchase order while there are missing budget codes", vbExclamation
        Exit Sub
    End If
End If

If GetTotalCostsForPurchaseOrder(txtPurchaseHeaderID.Text, "total") = 0 Then
    l_intMsg = MsgBox("Your order has a total value of 0. Orders with 0 value make budgeting impossible for department heads and management." & vbCrLf & vbCrLf & "Would you like to go back and enter some prices now?", vbYesNo + vbQuestion)
    If l_intMsg = vbYes Then Exit Sub
End If

Dim l_lngAuth As Long

l_lngAuth = RequestPurchaseOrderAuthorisation(Val(txtPurchaseHeaderID.Text))

ShowPurchaseOrder Val(txtPurchaseHeaderID.Text)

End Sub

Private Sub cmdItemsReceived_Click()

Dim l_intMsg As Integer
l_intMsg = MsgBox("Were ALL items received correctly?", vbYesNoCancel + vbQuestion)
Dim l_strSQL As String

Dim l_lngPurchaseHeaderID As Long
l_lngPurchaseHeaderID = Val(txtPurchaseHeaderID.Text)

Select Case l_intMsg
Case vbYes
    l_strSQL = "UPDATE purchasedetail SET quantityreceived = quantity WHERE purchaseheaderID = '" & l_lngPurchaseHeaderID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError

Case vbNo
    MsgBox "Please enter the quantities received directly in to the details grid", vbInformation
    
Case vbCancel
    Exit Sub
    
End Select

l_strSQL = "UPDATE purchaseheader SET fd_status = 'Goods received', receiveddate = '" & FormatSQLDate(Now) & "', receiveduser = '" & g_strUserInitials & "' WHERE purchaseheaderID = '" & l_lngPurchaseHeaderID & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

ShowPurchaseOrder l_lngPurchaseHeaderID

End Sub

Private Sub cmdJobDespatches_Click()

End Sub

Private Sub cmdLaunch_Click(Index As Integer)
Select Case Index
Case 1 'company
    'Dim l_strWebsite As String
    'l_strWebsite = GetData("company", "website", "companyID", lblCompanyID.Caption)
    'OpenBrowser l_strWebsite
    
    ShowCompany Val(lblCompanyID.Caption)
    
Case 0 'contact
    
    If Val(lblContactID.Caption) > 0 Then EmailContact lblContactID.Caption
Case 2
    Dim l_strSearch  As String
    l_strSearch = "http://www.multimap.com/map/places.cgi?db=pc&place=" & Replace(txtPostCode.Text, " ", "")
    OpenBrowser l_strSearch
Case 4

    'allow the adding of a job ID even after the PO is completed
    
    'If txtStatus.Text <> "Completed" Then
    '    MsgBox "You only need to use this button when the order is already completed", vbExclamation
    '    Exit Sub
    'End If
    
    If Val(txtPurchaseHeaderID.Text) = 0 Then
        NoPurchaseOrderSelectedMessage
        Exit Sub
    End If
    
    'check if its completed, and if it is check the access code
    If Not CheckAccess("/addjobidtopurchaseorderaftercompletion") And txtStatus.Text = "Completed" Then Exit Sub
    
    Dim l_strMsg As String
    
    l_strMsg = InputBox("Please enter the new job ID to associate this order with", "Link to job", txtJobID.Text)
    
    If l_strMsg = "" Or l_strMsg = txtJobID.Text Then Exit Sub
    
    If IsNumeric(l_strMsg) And IsJobID(Val(l_strMsg)) Then
        'save the job ID and the project details back to this order
        
        Dim l_lngProjectNumber As Long, l_lngProjectID As Long
        l_lngProjectNumber = GetData("job", "projectnumber", "jobID", l_strMsg)
        l_lngProjectID = GetData("job", "projectID", "jobID", l_strMsg)
        
        SetData "purchaseheader", "jobID", "purchaseheaderID", txtPurchaseHeaderID.Text, l_strMsg
        SetData "purchaseheader", "projectnumber", "purchaseheaderID", txtPurchaseHeaderID.Text, l_lngProjectNumber
        SetData "purchaseheader", "projectID", "purchaseheaderID", txtPurchaseHeaderID.Text, l_lngProjectID
    Else
    
        MsgBox "You have not entered a valid job ID", vbExclamation
    End If
        
        
    

End Select

End Sub

Private Sub cmdPDF_Click()

If Val(txtPurchaseHeaderID.Text) = 0 Then
    NoPurchaseOrderSelectedMessage
End If

If GetData("purchaseheader", "authorisationnumber", "purchaseheaderID", txtPurchaseHeaderID.Text) = 0 Then
    MsgBox "You can not print without getting authorisation of this order first.", vbExclamation
    Exit Sub
End If

If Not CheckAccess("/printpurchaseorderreport") Then Exit Sub

Dim l_strSelectionFormula As String
l_strSelectionFormula = "{purchaseheader.purchaseheaderID} = " & txtPurchaseHeaderID.Text

Dim l_strReportFile As String

If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID)), "/nopricesonorders") > 0 Then
    l_strReportFile = g_strLocationOfCrystalReportFiles & "purchaseorder_noprices.rpt"
ElseIf optCurrency(1).Value = True Then
    l_strReportFile = g_strLocationOfCrystalReportFiles & "purchaseorder_dollars.rpt"
ElseIf optCurrency(2).Value = True Then
    l_strReportFile = g_strLocationOfCrystalReportFiles & "purchaseorder_euros.rpt"
Else
    l_strReportFile = g_strLocationOfCrystalReportFiles & "purchaseorder.rpt"
End If

l_strSelectionFormula = "{purchaseheader.purchaseheaderID} = " & txtPurchaseHeaderID.Text

ReportToPDF l_strReportFile, l_strSelectionFormula, ""


End Sub

Private Sub cmdPrint_Click()

If Val(txtPurchaseHeaderID.Text) = 0 Then
    NoPurchaseOrderSelectedMessage
End If

If GetData("purchaseheader", "authorisationnumber", "purchaseheaderID", txtPurchaseHeaderID.Text) = 0 Then
    MsgBox "You can not print without getting authorisation of this order first.", vbExclamation
    Exit Sub
End If

If Not CheckAccess("/printpurchaseorderreport") Then Exit Sub

Dim l_strSelectionFormula As String
l_strSelectionFormula = "{purchaseheader.purchaseheaderID} = " & txtPurchaseHeaderID.Text

Dim l_strReportFile As String

If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID)), "/nopricesonorders") > 0 Then
    l_strReportFile = g_strLocationOfCrystalReportFiles & "purchaseorder_noprices.rpt"
ElseIf optCurrency(1).Value = True Then
    l_strReportFile = g_strLocationOfCrystalReportFiles & "purchaseorder_dollars.rpt"
ElseIf optCurrency(2).Value = True Then
    l_strReportFile = g_strLocationOfCrystalReportFiles & "purchaseorder_euros.rpt"
Else
    l_strReportFile = g_strLocationOfCrystalReportFiles & "purchaseorder.rpt"
End If

l_strSelectionFormula = "{purchaseheader.purchaseheaderID} = " & txtPurchaseHeaderID.Text

PrintCrystalReport l_strReportFile, l_strSelectionFormula, g_blnPreviewReport

End Sub

Private Sub cmdSave_Click()

'check a few things before saving!

If Val(lblCompanyID.Caption) = 0 Then
    NoCompanySelectedMessage
    Exit Sub
End If

If Val(lblContactID.Caption) = 0 Then
    NoContactSelectedMessage
    Exit Sub
End If

If cmbDepartment.Text = "" Then
    MsgBox "Please select a valid department before saving", vbExclamation
    Exit Sub
End If

If LCase(cmbDepartment.Text) = "facilities" Then
    If Val(cmbProject.Text) = 0 Then
        MsgBox "You must select a valid project before saving this purchase order!", vbExclamation
        Exit Sub
    End If
End If

If LCase(cmbOrderType.Text) = "capex" Then
    If Not CheckAccess("/createcapexorder") Then
        MsgBox "You do not have the appropriate security setting to be allowed to save Capital Expenditure orders.", vbExclamation, "/createcapexorder"
        Exit Sub
    End If
End If

If LCase(cmbOrderType.Text) = "service contract" Then
    If Not CheckAccess("/createcontractorder") Then
        MsgBox "You do not have the appropriate security setting to be allowed to save Service Contract orders.", vbExclamation, "/createcontractorder"
        Exit Sub
    End If
End If

If IsNull(datDeliveryDate.Value) Then
    MsgBox "Please select a valid delivery date (even if estimated)", vbExclamation
    Exit Sub
End If

If cmbStartTime.Text = "" Then
    MsgBox "Please select a valid delivery time (even if estimated)", vbExclamation
    Exit Sub
End If

SavePurchaseOrder
ShowPurchaseOrder Val(txtPurchaseHeaderID.Text)

End Sub

Private Sub cmdShowDeliveryAddress_Click(Index As Integer)

picDeliveryAddress.Visible = Not picDeliveryAddress.Visible

If cmbDeliverToCompany.Text <> "" Then chkDeliverTo.Value = vbChecked

cmbDeliveryFor.Text = cmbDeliverToContact.Text

End Sub

Private Sub Command1_Click()

End Sub

Private Sub ddnSupplierRates_Click()

'grdOrderDetail.Columns("suppliercode").Text = ddnSupplierRates.Columns("suppliercode").Text
grdOrderDetail.Columns("description").Text = ddnSupplierRates.Columns("description").Text
grdOrderDetail.Columns("unitcharge").Text = ddnSupplierRates.Columns("price0").Text
If grdOrderDetail.Columns("quantity").Text = "" Then grdOrderDetail.Columns("quantity").Text = "1"

End Sub

Private Sub Form_Activate()
If Me.Tag = "refresh" Then
    'adoCompany.Refresh
    Me.Tag = ""
End If
End Sub

Private Sub Form_Load()

MakeLookLikeOffice Me

Me.MousePointer = vbHourglass

l_strSQL = "SELECT * FROM purchasedetail WHERE purchaseheaderID = '-1'"

adoPurchaseOrderDetail.ConnectionString = g_strConnection
adoPurchaseOrderDetail.RecordSource = l_strSQL
adoPurchaseOrderDetail.Refresh

If g_optShowTimeColumnInPurchaseOrder = 1 Then
    grdOrderDetail.Columns("fd_time").Visible = True
    grdOrderDetail.Columns("fd_time").Width = 734
Else
    grdOrderDetail.Columns("fd_time").Visible = False
End If

PopulateCombo "purchaseordertype", cmbOrderType
PopulateCombo "despatchmethod", cmbDeliveryMethod
PopulateCombo "despatchmethod", cmbReturnMethod
PopulateCombo "department", cmbDepartment
PopulateCombo "times", cmbStartTime
PopulateCombo "times", cmbEndTime
PopulateCombo "users", cmbDeliveryFor
PopulateCombo "users", cmbReturnFor

l_strSQL = "SELECT code, description FROM budget ORDER BY code;"

adoCostCodes.ConnectionString = g_strConnection
adoCostCodes.RecordSource = l_strSQL
adoCostCodes.Refresh

Dim l_strDepartment As String
l_strDepartment = GetData("cetauser", "department", "cetauserID", g_lngUserID)
If l_strDepartment <> "" Then cmbDepartment.ListIndex = GetListIndex(cmbDepartment, l_strDepartment)

Me.MousePointer = vbDefault

End Sub

Private Sub Form_Resize()
On Error Resume Next

picFooter.Top = Me.ScaleHeight - picFooter.Height - 120
picFooter.Left = Me.ScaleWidth - picFooter.Width - 120

grdOrderDetail.Width = Me.ScaleWidth - 60 - grdOrderDetail.Left - 120
grdOrderDetail.Height = Me.ScaleHeight - grdOrderDetail.Top - 120 - picFooter.Height - 120

picHistory.Left = Me.ScaleWidth - picHistory.Width - 120

txtNotes.Top = picFooter.Top
lblCaption(9).Top = picFooter.Top

End Sub

Private Sub Form_Unload(Cancel As Integer)
If GetLoadedJobID <> -1 Then
    If frmJob.txtJobID.Text <> "" Then frmJob.tab1_Click 6
End If
End Sub

Private Sub grdOrderDetail_AfterInsert(RtnDispErrMsg As Integer)
adoPurchaseOrderDetail.Refresh
End Sub

Private Sub grdOrderDetail_AfterUpdate(RtnDispErrMsg As Integer)
txtTotal.Text = Format(GetTotalCostsForPurchaseOrder(Val(txtPurchaseHeaderID.Text), "total"), "CURRENCY")

End Sub

Private Sub grdOrderDetail_BeforeUpdate(Cancel As Integer)

If Val(txtPurchaseHeaderID.Text) = 0 Then
    NoPurchaseOrderSelectedMessage
    Cancel = True
    Exit Sub
End If

Dim l_intQuantity As Double, l_dblUnitCharge As Double, l_dblTotal As Double, l_sngDiscount As Single

If cmbOrderType.Text = "CAPEX" Then
    If grdOrderDetail.Columns("cetacode").Text = "" Then
        MsgBox "You must enter a budget code for CAPEX orders", vbExclamation
        Cancel = True
        Exit Sub
    End If
    
    If grdOrderDetail.Columns("ourreference").Text = "" Then
        MsgBox "You must enter a reference code / line number (our reference) for CAPEX orders", vbExclamation
        Cancel = True
        Exit Sub
    End If

End If

grdOrderDetail.Columns("purchaseheaderID").Text = Val(txtPurchaseHeaderID.Text)

If grdOrderDetail.Columns("description").Text = "" And grdOrderDetail.Columns("cetacode").Text = "DELIVERY" Then grdOrderDetail.Columns("description").Text = "Delivery"

l_intQuantity = Val(grdOrderDetail.Columns("quantity").Text)
l_sngDiscount = Val(grdOrderDetail.Columns("discount").Text)

l_dblUnitCharge = Val(grdOrderDetail.Columns("unitcharge").Text)

If g_optShowTimeColumnInPurchaseOrder = 0 Or grdOrderDetail.Columns("fd_time").Text = "" Then
    grdOrderDetail.Columns("fd_time").Text = 1
End If

l_dblTime = Val(grdOrderDetail.Columns("fd_time").Text)

l_dblTotal = (l_intQuantity * l_dblUnitCharge * l_dblTime)

l_dblTotal = l_dblTotal - ((l_dblTotal / 100) * l_sngDiscount)
'l_dblTotal = Int((l_dblTotal * 100) + 0.5) / 100

l_dblTotal = RoundToCurrency(l_dblTotal)

'l_dblTotal = (l_intQuantity * l_dblUnitCharge)
'l_dblTotal = Int((l_dblTotal * 100) + 0.5) / 100

grdOrderDetail.Columns("total").Text = l_dblTotal

End Sub

Private Sub SSIndexTab1_Click(PreviousTab As Integer)

End Sub

Private Sub grdOrderDetail_InitColumnProps()
grdOrderDetail.Columns("cetacode").DropDownHwnd = ddnCostCodes.hWnd

grdOrderDetail.Columns("ourreference").DropDownHwnd = ddnOurReference.hWnd

grdOrderDetail.Columns("suppliercode").DropDownHwnd = ddnSupplierRates.hWnd

End Sub

Private Sub SSOleDBCombo1_DropDown()

End Sub

Private Sub SSOleDBCombo1_InitColumnProps()

End Sub

Private Sub grdOrderDetail_LostFocus()
grdOrderDetail.Update
End Sub

Private Sub grdOrderDetail_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
If LastCol = 2 Then

    If grdOrderDetail.Columns(2).Text = "" Then
        grdOrderDetail.Columns(2).Text = m_strBudgetCode
    Else
        m_strBudgetCode = grdOrderDetail.Columns(2).Text
    End If
End If

If grdOrderDetail.Col < 0 Then Exit Sub

If grdOrderDetail.Columns(grdOrderDetail.Col).Name = "ourreference" Then
    adoOurReference.RecordSource = "SELECT description AS code, information AS description FROM xref WHERE category = 'ourreference' AND format = '" & grdOrderDetail.Columns("cetacode").Text & "' ORDER BY code;"
    adoOurReference.ConnectionString = g_strConnection
    adoOurReference.Refresh
End If
End Sub

Private Sub txtAuthorisationNumber_KeyPress(KeyAscii As Integer)

If KeyAscii = vbKeyReturn Then
    KeyAscii = 0
    Dim l_lngPurchaseHeaderID  As Long
    l_lngPurchaseHeaderID = GetData("purchaseheader", "purchaseheaderID", "authorisationnumber", txtAuthorisationNumber.Text)
    If l_lngPurchaseHeaderID > 0 Then
        ShowPurchaseOrder l_lngPurchaseHeaderID
    Else
        ShowPurchaseOrder -1
    End If
End If
End Sub

Private Sub txtPurchaseHeaderID_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then
    KeyAscii = 0
    'if a number has been entered
    If IsNumeric(txtPurchaseHeaderID.Text) Then
        'show the order
        ShowPurchaseOrder Val(txtPurchaseHeaderID.Text)
    End If
End If
End Sub
