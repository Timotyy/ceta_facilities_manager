Attribute VB_Name = "modDespatch"
Option Explicit
Sub PrintDespatchLabel(ByVal lp_lngDespatchID As Long, ByVal lp_intPage As Integer, ByVal lp_intTotalPages As Integer)

 'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Screen.MousePointer = vbHourglass
    
    Dim crxApplication As CRAXDRT.Application
    Dim crxReport As CRAXDRT.Report
    
    Set crxApplication = New CRAXDRT.Application
    
    Set crxReport = crxApplication.OpenReport(GetUNCNameNT(GetUNCNameNT(g_strLocationOfCrystalReportFiles & "deliverylabel.rpt")))
    
    crxReport.FormulaFields.Item(1).Text = Chr(34) & CStr(lp_intPage) & Chr(34)
    crxReport.FormulaFields.Item(2).Text = Chr(34) & CStr(lp_intTotalPages) & Chr(34)
    
    crxReport.RecordSelectionFormula = "{despatch.despatchID} = " & lp_lngDespatchID
    
    crxReport.DiscardSavedData
    
    crxReport.PrintOut False
  
    Set crxReport = Nothing
    
    DoEvents
    
    Screen.MousePointer = vbDefault
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    

PROC_ERR:
    
    Screen.MousePointer = vbDefault
    MsgBox Err.Description, vbExclamation
    Resume PROC_EXIT
    
    'TVCodeTools ErrorHandlerEnd

End Sub


Function CopyJobDeliveryAddressDetails(ByVal lp_lngSourceJobID As Long, ByVal lp_lngDestinationJobID As Long)


'copy the info from one job to another
Dim l_rstOriginalJob As ADODB.Recordset
Dim l_rstNewJob As ADODB.Recordset

'get the original job's details
Dim l_strSQL As String
Dim l_strFieldsToCopy As String

l_strFieldsToCopy = "deliverycompanyname, deliverycontactname, deliveryaddress, deliverypostcode, deliverycountry, deliverytelephone, collectioncompanyname, collectioncontactname, collectionaddress, collectionpostcode, collectioncountry, collectiontelephone "
l_strSQL = "SELECT " & l_strFieldsToCopy & " FROM job WHERE jobID = '" & lp_lngSourceJobID & "';"

Set l_rstOriginalJob = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

'allow adding of a new job
l_strSQL = "SELECT " & l_strFieldsToCopy & " FROM job WHERE jobID = '" & lp_lngDestinationJobID & "';"

Set l_rstNewJob = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

Dim l_intLoop As Integer




For l_intLoop = 0 To l_rstOriginalJob.Fields.Count - 1
    If l_rstOriginalJob.Fields(l_intLoop).Name <> "jobID" Then
    
        l_rstNewJob(l_intLoop) = l_rstOriginalJob(l_intLoop)
        
    
    End If
    
Next

l_rstNewJob.Update

l_rstNewJob.Close
Set l_rstNewJob = Nothing

l_rstOriginalJob.Close
Set l_rstOriginalJob = Nothing


End Function

Function AddRowToDespatchNote(lp_lngDespatchID As Long, lp_lngProjectNumber As Long, lp_lngJobID As Long, lp_intQuantity As Integer, lp_strCopyType As String, lp_strFormat As String, lp_strVideoStandard As String, lp_strBarcode As String, lp_strTitle As String, lp_strSubTitle As String, lp_intPackageNumber As Integer, lp_lngItemID As Long, lp_strStatus As String, lp_strSerialNumber As String, lp_strGeneric As String, Optional lp_strSeries As String, Optional lp_strSet As String, Optional lp_strEpisode As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    'add the new line
    
    l_strSQL = "INSERT INTO despatchdetail "
    l_strSQL = l_strSQL & "(despatchID, projectnumber, jobID, quantity, copytype, format, videostandard, barcode, title, subtitle, packagenumber, itemID, fd_status, cdate, cuser, returneddate, generic, serialnumber, series, seriesset, episode, itemnotes ) VALUES ("
    
    l_strSQL = l_strSQL & "'" & lp_lngDespatchID & "', "
    l_strSQL = l_strSQL & "'" & lp_lngProjectNumber & "', "
    l_strSQL = l_strSQL & "'" & lp_lngJobID & "', "
    l_strSQL = l_strSQL & "'" & lp_intQuantity & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strCopyType) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strFormat) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strVideoStandard) & "', "
    l_strSQL = l_strSQL & "'" & lp_strBarcode & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strTitle) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strSubTitle) & "', "
    l_strSQL = l_strSQL & "'" & lp_intPackageNumber & "',"
    l_strSQL = l_strSQL & "'" & lp_lngItemID & "',"
    l_strSQL = l_strSQL & "'" & lp_strStatus & "',"
    
    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now()) & "',"
    l_strSQL = l_strSQL & "'" & g_strUserInitials & "',"
    
    If UCase(lp_strStatus) = "DESPATCHED" Or UCase(lp_strStatus) = "PREPARED" Then
        l_strSQL = l_strSQL & "NULL,"
    Else
        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "',"
    End If
    l_strSQL = l_strSQL & "'" & lp_strGeneric & "',"
    l_strSQL = l_strSQL & "'" & lp_strSerialNumber & "',"
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strSeries) & "',"
    l_strSQL = l_strSQL & "'" & lp_strSet & "',"
    l_strSQL = l_strSQL & "'" & lp_strEpisode & "',"
    l_strSQL = l_strSQL & "'');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub CompleteDespatch(lp_lngDespatchID As Long)

Dim l_intMsg As Integer

If g_optDontPromptForConfirmationOnCompletionOfDespatches = 0 Then
    l_intMsg = MsgBox("Are you sure you want to complete this despatch? It will be marked as completed and will not appear on the despatch list.", vbQuestion + vbYesNo)
    If l_intMsg = vbNo Then Exit Sub
End If

Dim l_strSQL As String
Dim l_strSQL2 As String


Dim l_lngJobID  As String
l_lngJobID = GetData("despatch", "jobID", "despatchID", lp_lngDespatchID)

Dim l_strStatus As String
l_strStatus = GetData("job", "fd_status", "jobID", l_lngJobID)

If l_strStatus = "Sent To Accounts" Then
    MsgBox "This job has been sent to accounts so you can not update the job status. Please see your supervisor!", vbExclamation
End If

'update the actual despatch record
l_strSQL = "UPDATE despatch SET despatcheddate = '" & FormatSQLDate(Now) & "', despatcheduser = '" & g_strUserInitials & "' WHERE despatchID = '" & lp_lngDespatchID & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

Dim l_strTypeOfJob As String
'l_strTypeOfJob = UCase(GetData("job", "jobtype", "jobID", l_lngJobID))
l_strTypeOfJob = UCase(GetData("despatch", "typeofjob", "despatchID", lp_lngDespatchID))


Dim l_strDirection As String
l_strDirection = UCase(GetData("despatch", "direction", "despatchID", lp_lngDespatchID))

'if the job is a hire job, update the resource schedule and job too
If l_strTypeOfJob = "HIRE" Or l_strTypeOfJob = "EDIT" Then

    Select Case l_strDirection
    Case "OUT"
        UpdateDespatchStatus lp_lngDespatchID, "Despatched"
        l_strSQL = "UPDATE resourceschedule SET fd_status = 'Despatched' WHERE jobID = '" & l_lngJobID & "' AND fd_status <> 'Cancelled';"
        l_strSQL2 = "UPDATE job SET fd_status = 'Despatched' WHERE jobID = '" & l_lngJobID & "' AND fd_status <> 'Cancelled';"
        AddJobHistory Val(l_lngJobID), "Despatched In Despatch"
    Case "IN"
        UpdateDespatchStatus lp_lngDespatchID, "Returned"
        l_strSQL = "UPDATE resourceschedule SET fd_status = 'Returned' WHERE jobID = '" & l_lngJobID & "' AND fd_status <> 'Cancelled';"
        l_strSQL2 = "UPDATE job SET fd_status = 'Returned' WHERE jobID = '" & l_lngJobID & "' AND fd_status <> 'Cancelled';"
        AddJobHistory Val(l_lngJobID), "Returned In Despatch"
    End Select
    
    
    'only update the job's status if it is not hold cost, costed, sent to accounts etc....?
    If GetStatusNumber(l_strStatus) < 8 Then
        
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        ExecuteSQL l_strSQL2, g_strExecuteError
        CheckForSQLError
        
    End If
    
End If

End Sub

Public Sub UpdateDespatchForPurchaseOrder(lp_lngPurchaseHeaderID As Long)
    'this function needs to check to see what kind of order this PO is, and
    'then needs to either create or update the despatch database with its details
    'accordingly. there are 2 despatch entries for a hire, 1 for a purchase
    
    'also check to see if there are already any entries for this purchase
    'order in the despatch table.
    
    
    Dim l_rstPurchaseHeader As ADODB.Recordset
    Dim l_strSQL As String
    l_strSQL = "SELECT * FROM purchaseheader WHERE purchaseheaderID = '" & lp_lngPurchaseHeaderID & "';"
    
    Set l_rstPurchaseHeader = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If Not l_rstPurchaseHeader.EOF Then
    
        
        If l_rstPurchaseHeader("fd_status") = "New" Then GoTo PROC_CLOSEDATA
        If Val(Format(l_rstPurchaseHeader("delivertodifferentaddress"))) <> 0 And g_optCreateDNotesForExternalDeliveryPurchaseOrders = 0 Then GoTo PROC_CLOSEDATA
        
        Dim l_lngPurchaseAuthorisationNumber    As Long
        Dim l_strPurchaseOrderType              As String
        Dim l_lngIncomingDespatchID             As Long
        Dim l_lngOutgoingDespatchID             As Long
        Dim l_lngJobID As Long, l_strIncomingMethod As String, l_strOutgoingMethod As String, l_lngSupplierID As Long, l_strSupplierName As String
        Dim l_strContactName As String, l_lngContactID As Long, l_strAddress As String, l_strPostCode As String, l_strTelephone As String
        Dim l_strCountry As String, l_datDespatchDeadline As Date, l_datReturnDeadline As Date, l_intUrgent As Integer, l_strLocation As String
        Dim l_strEmail As String, l_datJobTime  As Date, l_strDeliverFor As String, l_strReturnFor As String
        
        Dim l_strTitle                          As String
        Dim l_lngProjectNumber                  As Long
        
        'pick up all the values required from the header...
        l_strTitle = Format(l_rstPurchaseHeader("title"))
        l_lngProjectNumber = Format(l_rstPurchaseHeader("projectnumber"))
        l_lngPurchaseAuthorisationNumber = Val(Format(l_rstPurchaseHeader("authorisationnumber")))
        l_strPurchaseOrderType = l_rstPurchaseHeader("ordertype")
        l_lngIncomingDespatchID = Val(Format(l_rstPurchaseHeader("incomingdespatchID")))
        l_lngOutgoingDespatchID = Val(Format(l_rstPurchaseHeader("outgoingdespatchID")))
        l_lngJobID = Format(l_rstPurchaseHeader("jobID"))
        l_strIncomingMethod = Format(l_rstPurchaseHeader("deliverymethod"))
        l_strOutgoingMethod = Format(l_rstPurchaseHeader("returnmethod"))
        l_lngSupplierID = Format(l_rstPurchaseHeader("companyID"))
        l_strSupplierName = Format(l_rstPurchaseHeader("companyname"))
        l_strContactName = Format(l_rstPurchaseHeader("contactname"))
        l_lngContactID = Format(l_rstPurchaseHeader("contactID"))
        If Not IsNull(l_rstPurchaseHeader("deliverydate")) Then l_datDespatchDeadline = l_rstPurchaseHeader("deliverydate")
        If Not IsNull(l_rstPurchaseHeader("returndate")) Then l_datReturnDeadline = l_rstPurchaseHeader("returndate")
        
        
        l_strAddress = GetData("company", "address", "companyID", l_lngSupplierID)
        l_strPostCode = GetData("company", "postcode", "companyID", l_lngSupplierID)
        l_strTelephone = GetData("company", "telephone", "companyID", l_lngSupplierID)
        l_strCountry = GetData("company", "country", "companyID", l_lngSupplierID)
        l_strEmail = GetData("contact", "email", "contactID", l_lngContactID)
        
        l_datJobTime = GetData("job", "startdate", "jobID", l_lngJobID)
        l_strDeliverFor = Format(l_rstPurchaseHeader("deliveryfor"))
        l_strReturnFor = Format(l_rstPurchaseHeader("returnfor"))
        
        If Val(Format(l_rstPurchaseHeader("delivertodifferentaddress"))) <> 0 Then
            l_strLocation = "External"
            l_strIncomingMethod = "Ext: " & l_strIncomingMethod
            l_strOutgoingMethod = "Ext: " & l_strOutgoingMethod
        End If
        
        GoTo PROC_CREATEDESPATCHLINES
    
    Else
        MsgBox "Could not locate the purchase order specified to update the despatch (Auth# " & l_lngPurchaseAuthorisationNumber & ")", vbExclamation
        GoTo PROC_CLOSEDATA
    End If
    
    Exit Sub


PROC_CREATEDESPATCHLINES:

    'regardless of the type of PO, there is always an incoming despatch
    l_lngIncomingDespatchID = SaveDespatch(l_lngIncomingDespatchID, "Purchase", l_lngProjectNumber, l_lngJobID, "IN", l_strIncomingMethod, l_lngSupplierID, l_strSupplierName, _
        l_strContactName, l_lngContactID, l_strAddress, l_strPostCode, l_strTelephone, l_strCountry, _
        l_datJobTime, l_strTitle, l_datDespatchDeadline, l_intUrgent, "", l_strLocation, "", 0, l_strEmail, lp_lngPurchaseHeaderID, 0, "", "", l_strDeliverFor, "")
                                    
    SetData "purchaseheader", "incomingdespatchID", "purchaseheaderID", lp_lngPurchaseHeaderID, l_lngIncomingDespatchID
    
    
    'if its a Hire purchase order, then it will be going back, so create the outgoing despatch too
    If l_strPurchaseOrderType = "Hire" Then
        l_lngOutgoingDespatchID = SaveDespatch(l_lngOutgoingDespatchID, "Purchase", l_lngProjectNumber, l_lngJobID, "OUT", l_strOutgoingMethod, l_lngSupplierID, l_strSupplierName, _
            l_strContactName, l_lngContactID, l_strAddress, l_strPostCode, l_strTelephone, l_strCountry, _
            l_datJobTime, l_strTitle, l_datReturnDeadline, l_intUrgent, "", l_strLocation, "", 0, l_strEmail, lp_lngPurchaseHeaderID, 0, "", "", l_strReturnFor, "")
    
        SetData "purchaseheader", "outgoingdespatchID", "purchaseheaderID", lp_lngPurchaseHeaderID, l_lngOutgoingDespatchID
    
    End If
    
    Dim l_rstPurchaseOrderDetail As New ADODB.Recordset
    
    l_strSQL = "DELETE FROM despatchdetail WHERE despatchID = '" & l_lngIncomingDespatchID & "' OR despatchID = '" & l_lngOutgoingDespatchID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_strSQL = "SELECT * FROM purchasedetail WHERE purchaseheaderID = '" & lp_lngPurchaseHeaderID & "' ORDER BY purchasedetailID;"
    Set l_rstPurchaseOrderDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    Do While Not l_rstPurchaseOrderDetail.EOF
        AddRowToDespatchNote l_lngIncomingDespatchID, l_lngProjectNumber, l_lngJobID, l_rstPurchaseOrderDetail("quantity"), "ORDER", "", "", "", l_rstPurchaseOrderDetail("description"), "", 1, 0, "", "", ""
        If l_strPurchaseOrderType = "Hire" Then
            AddRowToDespatchNote l_lngOutgoingDespatchID, l_lngProjectNumber, l_lngJobID, l_rstPurchaseOrderDetail("quantity"), "ORDER", "", "", "", l_rstPurchaseOrderDetail("description"), "", 1, 0, "", "", ""
        End If
        l_rstPurchaseOrderDetail.MoveNext
    Loop
    
    l_rstPurchaseOrderDetail.Close
    Set l_rstPurchaseOrderDetail = Nothing
    
    GoTo PROC_CLOSEDATA
    
Exit Sub

PROC_CLOSEDATA:
    l_rstPurchaseHeader.Close
    Set l_rstPurchaseHeader = Nothing
    Exit Sub
    
End Sub

Function DespatchHireResource(lp_lngResourceScheduleID As Long, lp_lngDespatchID As Long)

'check use security
If Not CheckAccess("/despatchresource") Then Exit Function

Dim l_strDirection As String
Dim l_lngProjectNumber As Long
Dim l_lngJobID As Long
Dim l_strResourceType  As String
Dim l_strBarcode As String
Dim l_strTitle As String
Dim l_strSubTitle As String
Dim l_strDestination As String
Dim l_strFormat As String
Dim l_strStandard As String
Dim l_strStatus As String
Dim l_strReason As String
Dim l_strCurrentStatus As String
Dim l_lngCurrentJobID As Long
Dim l_lngResourceScheduleID As Long
Dim l_strNewResourceScheduleStatus As String
Dim l_strSerialNumber  As String


'l_strBarcode = GetData("resource", "barcode", "resourceID", lp_lngResourceID)
l_strSerialNumber = GetData("resourceschedule", "serialnumber", "resourcescheduleID", lp_lngResourceScheduleID)
l_strDirection = GetData("despatch", "direction", "despatchID", lp_lngDespatchID)

l_lngJobID = GetData("despatch", "jobID", "despatchID", lp_lngDespatchID)

'first, check if this resource is on this job.
If UCase(l_strDirection) = "OUT" Then 'despatching
    l_strStatus = "Out"
    l_strNewResourceScheduleStatus = "Prepared"
Else 'returning
    l_strStatus = "Unchecked"
    l_strNewResourceScheduleStatus = "Returned"
    
    'return on the outbound despatch note
    Dim l_lngDespatchDetailID As Long
    l_lngDespatchDetailID = ReturnDespatchedItem(l_strBarcode, l_lngJobID)
    
End If

l_lngProjectNumber = GetData("despatch", "projectnumber", "despatchID", lp_lngDespatchID)
'l_strBarcode = GetData("resource", "barcode", "resourceID", lp_lngResourceID)
l_strTitle = GetData("resourceschedule", "hiredescription", "resourcescheduleID", lp_lngResourceScheduleID)
l_strSubTitle = GetData("resourceschedule", "role", "resourcescheduleID", lp_lngResourceScheduleID)

'it is doubtful that the hire company should go on to the dnote
'l_strSubTitle = GetData("company", "name", "companyID", GetData("resourceschedule", "hiresupplierID", "resourcescheduleID", lp_lngResourceScheduleID))



DespatchHireResource = AddRowToDespatchNote(lp_lngDespatchID, l_lngProjectNumber, l_lngJobID, 1, l_strResourceType, l_strFormat, "", l_strBarcode, l_strTitle, l_strSubTitle, 1, lp_lngResourceScheduleID, IIf(UCase(l_strDirection) = "OUT", "Despatched", "Returned"), l_strSerialNumber, "No")

If lp_lngResourceScheduleID <> 0 Then
    If g_optUpdateResourceScheduleStatusOnDespatch <> 0 Then SetData "resourceschedule", "fd_status", "resourcescheduleID", lp_lngResourceScheduleID, l_strNewResourceScheduleStatus
End If

Exit Function

PROC_NO_DESPATCH:

MsgBox l_strReason, vbExclamation
Exit Function


End Function
Function DespatchJobDetailItem(ByVal lp_lngDespatchID As Long, ByVal lp_lngJobDetailID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/despatchjobdetailitem") Then
        Exit Function
    End If
    
    If lp_lngDespatchID = 0 Then
        Exit Function
    End If
    
    'first check to see library item exists
    If RecordExists("jobdetail", "jobdetailID", lp_lngJobDetailID) Then
        
        Dim l_strSQL As String
        l_strSQL = "SELECT * FROM jobdetail WHERE jobdetailID = '" & lp_lngJobDetailID & "'"
        
        Dim l_rstJobDetail As New ADODB.Recordset
        Set l_rstJobDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
        
        Dim l_lngJobID As Long
        Dim l_lngProjectNumber As Long
        Dim l_intQuantity As Integer
        Dim l_strCopyType As String
        Dim l_strBarcode As String
        Dim l_strFormat As String
        Dim l_strVideoStandard As String
        Dim l_strTitle As String
        Dim l_strSubTitle As String
        Dim l_intPackageNumber As Integer
        
        With l_rstJobDetail
            l_strBarcode = Format(.Fields("librarybarcode"))
            l_intQuantity = Format(.Fields("quantity"))
            l_strCopyType = GetCopyType(.Fields("copytype"))
            l_strFormat = Format(.Fields("format"))
            l_strVideoStandard = Format(.Fields("videostandard"))
            
            l_lngJobID = .Fields("jobID")
            l_lngProjectNumber = GetData("job", "projectnumber", "jobID", l_lngJobID)
            
            If g_optUseJobDetailsWhenAddingJobDetailLinesToDespatch = 0 Then
                l_strTitle = Format(.Fields("description"))
                l_strSubTitle = Format(.Fields("clocknumber"))
            Else
                l_strTitle = GetData("job", "productname", "jobID", l_lngJobID)
                If l_strTitle = "" Then
                    l_strTitle = GetData("job", "title1", "jobID", l_lngJobID)
                    l_strSubTitle = GetData("job", "title2", "jobID", l_lngJobID)
                Else
                    l_strSubTitle = GetData("job", "title1", "jobID", l_lngJobID)
                End If
            End If
            
            l_intPackageNumber = 1
            
        End With
        
        l_rstJobDetail.Close
        Set l_rstJobDetail = Nothing
        
        DespatchJobDetailItem = AddRowToDespatchNote(lp_lngDespatchID, l_lngProjectNumber, l_lngJobID, l_intQuantity, l_strCopyType, l_strFormat, l_strVideoStandard, l_strBarcode, l_strTitle, l_strSubTitle, l_intPackageNumber, 0, "", "", "")
    Else
        MsgBox "Could not locate the job detail item", vbExclamation
    End If
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function DespatchResource(lp_lngDespatchID As Long, lp_lngResourceID As Long, Optional ByVal lp_intBoxNumber As Integer)

'check use security
If Not CheckAccess("/despatchresource") Then Exit Function

Dim l_strDirection As String
Dim l_lngProjectNumber As Long
Dim l_lngJobID As Long
Dim l_strResourceType  As String
Dim l_strBarcode As String
Dim l_strTitle As String
Dim l_strSubTitle As String
Dim l_strDestination As String
Dim l_strFormat As String
Dim l_strStandard As String
Dim l_strStatus As String
Dim l_strReason As String
Dim l_strCurrentStatus As String
Dim l_lngCurrentJobID As Long
Dim l_lngResourceScheduleID As Long
Dim l_strNewResourceScheduleStatus As String
Dim l_strSerialNumber  As String
Dim l_strGeneric  As String

If lp_lngResourceID = 0 Then
    MsgBox "This is not a valid resource", vbExclamation
    Exit Function
End If

'get some resource information
l_strBarcode = GetData("resource", "barcode", "resourceID", lp_lngResourceID)
l_strSerialNumber = GetData("resource", "serialnumber", "resourceID", lp_lngResourceID)
l_strCurrentStatus = GetData("resource", "fd_status", "resourceID", lp_lngResourceID)
l_lngCurrentJobID = GetData("resource", "jobID", "resourceID", lp_lngResourceID)
l_strDirection = GetData("despatch", "direction", "despatchID", lp_lngDespatchID)
l_strSubTitle = GetData("resource", "subcategory1", "resourceID", lp_lngResourceID)

'get the new PAT testing date
CheckPATDate lp_lngResourceID

'pick up the job ID of this despatch
l_lngJobID = GetData("despatch", "jobID", "despatchID", lp_lngDespatchID)


'first, check if this resource is on this job.
l_lngResourceScheduleID = GetDataSQL("SELECT TOP 1 resourcescheduleID FROM resourceschedule WHERE resourceID = '" & lp_lngResourceID & "' AND jobID = '" & l_lngJobID & "'")

If l_lngResourceScheduleID = 0 Then
    'this item is not on this job.
    If Not IsGeneric(lp_lngResourceID) Then
        'item is not generic, so should it be allowed to be added, as it was never booked?
        If l_strDirection = "OUT" And g_optAllowNonBookedItemsInTech = 0 Then
            MsgBox "This item was not booked on to this job so it can not be despatched.", vbExclamation
            Exit Function
        End If
    End If
End If

If UCase(l_strDirection) = "OUT" Then 'despatching
    
    If l_lngCurrentJobID <> 0 Then
        Dim l_intMsg As Integer
        l_intMsg = MsgBox("This item is already out on project number " & GetData("job", "projectnumber", "jobID", l_lngCurrentJobID) & " (Client: " & GetData("job", "companyname", "jobID", l_lngCurrentJobID) & " - Job ID: " & l_lngCurrentJobID & ")." & vbCrLf & vbCrLf & "Do you want to show that collection note?", vbYesNo + vbExclamation)
        
        If l_intMsg = vbYes Then
        
            Dim l_lngDNoteNumber As Long
            l_lngDNoteNumber = GetData("job", "collectiondnotenumber", "jobID", l_lngCurrentJobID)
            
'            frmTechJob.txtDespatchID.Text = l_lngDNoteNumber
'            frmTechJob.txtDespatchID_KeyPress 13
'            frmTechJob.Show
            
            
            Exit Function
        End If
        
    End If
    
    l_strDestination = GetData("despatch", "companyname", "despatchID", lp_lngDespatchID)
    l_strStatus = "Out"
    l_strNewResourceScheduleStatus = "Prepared"
    
    If Not IsGeneric(lp_lngResourceID) Then ' check if generic
        If UCase(l_strCurrentStatus) <> "AVAILABLE" And l_strCurrentStatus <> "" Then
            l_strReason = "The resource status is currently '" & l_strCurrentStatus & "'. It can not be despatched until it is made available."
            GoTo PROC_NO_DESPATCH
        End If
    End If

Else 'returning

    If Not IsGeneric(lp_lngResourceID) Then ' check if generic
        If l_lngCurrentJobID <> l_lngJobID And l_lngCurrentJobID <> 0 Then
            l_intMsg = MsgBox("This item is already out on project number " & GetData("job", "projectnumber", "jobID", l_lngCurrentJobID) & " (Client: " & GetData("job", "companyname", "jobID", l_lngCurrentJobID) & " - Job ID: " & l_lngCurrentJobID & ")." & vbCrLf & vbCrLf & "Do you want to show that collection note?", vbYesNo + vbExclamation)
                
            If l_intMsg = vbYes Then
                l_lngDNoteNumber = GetData("job", "collectiondnotenumber", "jobID", l_lngCurrentJobID)
                
'                frmTechJob.txtDespatchID.Text = l_lngDNoteNumber
'                frmTechJob.txtDespatchID_KeyPress 13
'                frmTechJob.Show
                
                
                Exit Function
            End If
            
            Exit Function
        End If
    End If
    
    l_strDestination = GetData("despatch", "location", "despatchID", lp_lngDespatchID)
    l_strStatus = "Unchecked"
    l_strNewResourceScheduleStatus = "Returned"
    
    If Not IsGeneric(lp_lngResourceID) Then ' check if generic
        If UCase(l_strCurrentStatus) <> "OUT" And l_strCurrentStatus <> "" Then
            l_strReason = "The resource is currently not set as being out."
            GoTo PROC_NO_DESPATCH
        End If
    End If
    
    'return on the outbound despatch note
    Dim l_lngDespatchDetailID As Long
    l_lngDespatchDetailID = ReturnDespatchedItem(l_strBarcode, l_lngJobID)
    
  '  l_lngDespatchDetailID = GetDataSQL("SELECT despatchdetailID FROM despatchdetail WHERE despatchID = '" & l_lngDeliveryDNoteID & "' AND itemID = '" & lp_lngResourceID & "' AND (fd_status = 'Prepared' OR fd_status = 'Despatched');")

    If l_lngDespatchDetailID < 1 Then
        MsgBox "There are no items left to return of this type.", vbExclamation
        Exit Function
    End If
    
End If

l_strResourceType = GetData("resource", "subcategory1", "resourceID", lp_lngResourceID)
l_lngProjectNumber = GetData("despatch", "projectnumber", "despatchID", lp_lngDespatchID)
l_strGeneric = GetData("resource", "generic", "resourceID", lp_lngResourceID)
'l_strBarcode = GetData("resource", "barcode", "resourceID", lp_lngResourceID)
l_strTitle = GetData("resource", "name", "resourceID", lp_lngResourceID)
l_strSubTitle = GetData("resource", "description", "resourceID", lp_lngResourceID)
l_strFormat = GetData("resource", "format", "resourceID", lp_lngResourceID)
l_strStandard = GetData("resource", "standard", "resourceID", lp_lngResourceID)

DespatchResource = AddRowToDespatchNote(lp_lngDespatchID, l_lngProjectNumber, l_lngJobID, 1, l_strResourceType, l_strFormat, "", l_strBarcode, l_strTitle, l_strSubTitle, lp_intBoxNumber, lp_lngResourceID, IIf(UCase(l_strDirection) = "OUT", "Prepared", "Returned"), l_strSerialNumber, l_strGeneric)

If Not IsGeneric(lp_lngResourceID) Then
    If UCase(l_strDirection) = "IN" Then
        SetData "resource", "location", "resourceID", lp_lngResourceID, l_strDestination
        SetData "resource", "jobID", "resourceID", lp_lngResourceID, Null
        SetData "resource", "lastreturneddate", "resourceID", lp_lngResourceID, FormatSQLDate(Now)
        SetData "resource", "lastreturneduser", "resourceID", lp_lngResourceID, g_strUserInitials
    Else
        SetData "resource", "location", "resourceID", lp_lngResourceID, l_lngProjectNumber & " - " & l_strDestination
        SetData "resource", "jobID", "resourceID", lp_lngResourceID, l_lngJobID
        SetData "resource", "lastprepareddate", "resourceID", lp_lngResourceID, FormatSQLDate(Now)
        SetData "resource", "lastprepareduser", "resourceID", lp_lngResourceID, g_strUserInitials
    End If
    
    SetData "resource", "fd_status", "resourceID", lp_lngResourceID, l_strStatus
End If

If l_lngResourceScheduleID <> 0 Then
    If g_optUpdateResourceScheduleStatusOnDespatch <> 0 Then
        SetData "resourceschedule", "fd_status", "resourcescheduleID", l_lngResourceScheduleID, l_strNewResourceScheduleStatus
    End If
End If


Exit Function

PROC_NO_DESPATCH:

MsgBox l_strReason, vbExclamation
Exit Function

End Function
Function ReturnDespatchedItem(ByVal l_strBarcode As String, ByVal l_lngJobID As Long) As Long

Dim l_strSQL As String
l_strSQL = "SELECT * FROM despatchdetail WHERE barcode = '" & l_strBarcode & "' AND jobID = '" & l_lngJobID & "' AND returneddate IS NULL ORDER BY despatchdetailID;"

Dim l_rstDespatchedItems As New ADODB.Recordset
Set l_rstDespatchedItems = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

Dim l_lngReturnedItem  As Long

If Not l_rstDespatchedItems.EOF Then
    l_rstDespatchedItems.MoveFirst
    l_lngReturnedItem = l_rstDespatchedItems("despatchdetailID").Value
    
    l_strSQL = "UPDATE despatchdetail SET fd_status = 'Returned', returneddate = '" & FormatSQLDate(Now) & "', returneduser = '" & g_strUserInitials & "' WHERE despatchdetailID = '" & l_lngReturnedItem & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
Else
    l_lngReturnedItem = 0
End If

l_rstDespatchedItems.Close
Set l_rstDespatchedItems = Nothing


ReturnDespatchedItem = l_lngReturnedItem



End Function


Function GetDespatchIDForJob(lp_lngJobID As Long, lp_strDirection As String) As Long

Dim l_strSQL As String

l_strSQL = "SELECT despatchID FROM despatch WHERE jobID = '" & lp_lngJobID & "' AND direction = '" & lp_strDirection & "' AND (purchaseauthorisationnumber IS NULL OR purchaseauthorisationnumber = '' or purchaseauthorisationnumber = 0) ORDER BY purchaseauthorisationnumber;"
Dim l_rstDespatch As New ADODB.Recordset

Set l_rstDespatch = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If Not l_rstDespatch.EOF Then
    l_rstDespatch.MoveFirst
    GetDespatchIDForJob = Format(l_rstDespatch("despatchID"))
Else
    GetDespatchIDForJob = 0
End If

l_rstDespatch.Close
Set l_rstDespatch = Nothing

End Function
Function GetItemCountForDespatch(lp_lngDespatchID As Long) As Long
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    l_strSQL = "SELECT COUNT(despatchID) FROM despatchdetail WHERE despatchID = '" & lp_lngDespatchID & "'"
    
    
    GetItemCountForDespatch = GetCount(l_strSQL)
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetNextDespatchNumber(lp_strDirection)

Dim l_lngDespatchNumber As Long

If g_optUseDifferentRangeForInAndOutDespatchNumbers <> 0 Then
    l_lngDespatchNumber = GetNextSequence("despatchnumber-" & LCase(lp_strDirection))
Else
    l_lngDespatchNumber = GetNextSequence("despatchnumber")
End If

GetNextDespatchNumber = l_lngDespatchNumber

End Function

Function SaveDespatch(lp_lngDespatchID As Long, lp_strTypeOfJob As String, lp_lngProjectNumber As Long, lp_lngJobID As Long, lp_strDirection As String, lp_strMethod As String, lp_lngCompanyID As Long, lp_strCompanyName As String, lp_strContactName As String, lp_lngContactID As Long, lp_strAddress As String, lp_strPostCode As String, lp_strTelephone As Variant, lp_strCountry As String, lp_datJobTime As Variant, lp_strDescription As String, lp_datDeadlineDate As Variant, lp_intFlagUrgent As Integer, lp_strNotes As String, lp_strLocation As String, lp_strBillToCompany As String, lp_lngBillToCompanyID As Long, lp_strEmail As String, lp_lngPurchaseHeaderID As Long, lp_dblCharge As Double, lp_strDeliveredBy As String, lp_strCourierReference As String, lp_strDeliveryFor As String, lp_strCostCode As String)
    
    On Error GoTo DespatchVTJob_Error
    
    'this function creates or updates a record in despatch with the most up to date job details.
    
    Dim l_lngProjectID As Long, l_lngDespatchNumber As Long
    
    'work out the correct project number etc
    If Val(lp_lngProjectNumber) = 0 And Val(lp_lngJobID) <> 0 Then
        l_lngProjectID = GetData("job", "projectID", "jobID", lp_lngJobID)
        lp_lngProjectNumber = GetData("job", "projectnumber", "jobID", lp_lngJobID)
    ElseIf Val(lp_lngProjectNumber) <> 0 Then
        l_lngProjectID = GetData("project", "projectID", "projectnumber", lp_lngProjectNumber)
    Else
        l_lngProjectID = 0
    End If
    
    'l_lngProjectID = GetData("project", "projectID", "projectnumber", lp_lngProjectNumber)
    
    Dim lp_strSQL As String
    Dim l_lngPurchaseAuthorisationNumber As Long
    
    If lp_strTypeOfJob = "" Then lp_strTypeOfJob = GetData("job", "jobtype", "jobID", lp_lngJobID)
    
    'get the p/o details
    If lp_lngPurchaseHeaderID <> 0 Then
        l_lngPurchaseAuthorisationNumber = GetData("purchaseheader", "authorisationnumber", "purchaseheaderID", lp_lngPurchaseHeaderID)
        lp_strTypeOfJob = "Purchase"
    End If
    
    If lp_lngDespatchID = 0 Then
    
        l_lngDespatchNumber = GetNextDespatchNumber(lp_strDirection)
        
        'this is a new despatch so create one
        lp_strSQL = "INSERT INTO despatch "
        lp_strSQL = lp_strSQL & "(despatchnumber, createddate, createduser, projectID, projectnumber, jobID, companyID, companyname, address, postcode, telephone, country, contactname, contactID, jobtime, direction, deliverymethod, description, deadlinedate, flagurgent, typeofjob, modifieddate, modifieduser, location, billtocompanyname, billtocompanyID, notes, email, purchaseheaderID, charge, deliveredby, courierreference, purchaseauthorisationnumber, costcode, deliveryfor ) VALUES ("
        lp_strSQL = lp_strSQL & "'" & l_lngDespatchNumber & "', "
        lp_strSQL = lp_strSQL & "'" & FormatSQLDate(Now) & "', "
        lp_strSQL = lp_strSQL & "'" & g_strUserInitials & "', "
        lp_strSQL = lp_strSQL & "'" & l_lngProjectID & "', "
        lp_strSQL = lp_strSQL & "'" & lp_lngProjectNumber & "', "
        lp_strSQL = lp_strSQL & "'" & lp_lngJobID & "', "
        lp_strSQL = lp_strSQL & "'" & lp_lngCompanyID & "', "
        lp_strSQL = lp_strSQL & "'" & QuoteSanitise(lp_strCompanyName) & "', "
        lp_strSQL = lp_strSQL & "'" & QuoteSanitise(lp_strAddress) & "', "
        lp_strSQL = lp_strSQL & "'" & QuoteSanitise(lp_strPostCode) & "', "
        lp_strSQL = lp_strSQL & "'" & QuoteSanitise(lp_strTelephone) & "', "
        lp_strSQL = lp_strSQL & "'" & QuoteSanitise(lp_strCountry) & "', "
        lp_strSQL = lp_strSQL & "'" & QuoteSanitise(lp_strContactName) & "', "
        lp_strSQL = lp_strSQL & "'" & lp_lngContactID & "', "
        If lp_datJobTime = "" Then
            lp_strSQL = lp_strSQL & "'NULL', "
        Else
            lp_strSQL = lp_strSQL & "'" & FormatSQLDate(lp_datJobTime) & "', "
        End If
        lp_strSQL = lp_strSQL & "'" & QuoteSanitise((lp_strDirection)) & "', "
        lp_strSQL = lp_strSQL & "'" & QuoteSanitise(lp_strMethod) & "', "
        lp_strSQL = lp_strSQL & "'" & QuoteSanitise(Left(lp_strDescription, 50)) & "', "
        lp_strSQL = lp_strSQL & "'" & FormatSQLDate(lp_datDeadlineDate) & "', "
        lp_strSQL = lp_strSQL & "'" & lp_intFlagUrgent & "', "
        lp_strSQL = lp_strSQL & "'" & lp_strTypeOfJob & "', "
        lp_strSQL = lp_strSQL & "'" & FormatSQLDate(Now()) & "', "
        lp_strSQL = lp_strSQL & "'" & g_strUserInitials & "',"
        lp_strSQL = lp_strSQL & "'" & QuoteSanitise(lp_strLocation) & "',"
        lp_strSQL = lp_strSQL & "'" & QuoteSanitise(lp_strBillToCompany) & "',"
        lp_strSQL = lp_strSQL & "'" & lp_lngBillToCompanyID & "',"
        lp_strSQL = lp_strSQL & "'" & QuoteSanitise(lp_strNotes) & "',"
        lp_strSQL = lp_strSQL & "'" & QuoteSanitise(lp_strEmail) & "',"
        lp_strSQL = lp_strSQL & "'" & lp_lngPurchaseHeaderID & "',"
        lp_strSQL = lp_strSQL & "'" & lp_dblCharge & "',"
        lp_strSQL = lp_strSQL & "'" & QuoteSanitise(lp_strDeliveredBy) & "',"
        lp_strSQL = lp_strSQL & "'" & QuoteSanitise(UCase(lp_strCourierReference)) & "',"
        lp_strSQL = lp_strSQL & "'" & l_lngPurchaseAuthorisationNumber & "',"
        lp_strSQL = lp_strSQL & "'" & QuoteSanitise(lp_strCostCode) & "',"
        lp_strSQL = lp_strSQL & "'" & QuoteSanitise(lp_strDeliveryFor) & "')"
        
    Else
        'edit an existing record
        lp_strSQL = "UPDATE despatch SET "
        lp_strSQL = lp_strSQL & "projectID = '" & l_lngProjectID & "', "
        lp_strSQL = lp_strSQL & "projectnumber = '" & lp_lngProjectNumber & "', "
        lp_strSQL = lp_strSQL & "jobID = '" & lp_lngJobID & "', "
        lp_strSQL = lp_strSQL & "companyID = '" & lp_lngCompanyID & "', "
        lp_strSQL = lp_strSQL & "companyname = '" & QuoteSanitise(lp_strCompanyName) & "', "
        lp_strSQL = lp_strSQL & "address = '" & QuoteSanitise(lp_strAddress) & "', "
        lp_strSQL = lp_strSQL & "contactID = '" & lp_lngContactID & "', "
        lp_strSQL = lp_strSQL & "contactname = '" & QuoteSanitise(lp_strContactName) & "', "
        lp_strSQL = lp_strSQL & "postcode = '" & lp_strPostCode & "', "
        lp_strSQL = lp_strSQL & "telephone = '" & lp_strTelephone & "', "
        lp_strSQL = lp_strSQL & "country = '" & lp_strCountry & "', "
        lp_strSQL = lp_strSQL & "jobtime = '" & lp_datJobTime & "', "
        lp_strSQL = lp_strSQL & "direction = '" & QuoteSanitise(lp_strDirection) & "', "
        lp_strSQL = lp_strSQL & "deliverymethod = '" & QuoteSanitise(lp_strMethod) & "', "
        lp_strSQL = lp_strSQL & "description = '" & QuoteSanitise((lp_strDescription)) & "', "
        lp_strSQL = lp_strSQL & "deadlinedate = '" & FormatSQLDate(lp_datDeadlineDate) & "', "
        lp_strSQL = lp_strSQL & "flagurgent = '" & lp_intFlagUrgent & "', "
        lp_strSQL = lp_strSQL & "typeofjob = '" & lp_strTypeOfJob & "', "
        lp_strSQL = lp_strSQL & "modifieddate = " & "'" & FormatSQLDate(Now()) & "', "
        lp_strSQL = lp_strSQL & "modifieduser = '" & g_strUserInitials & "', "
        lp_strSQL = lp_strSQL & "location = '" & QuoteSanitise(lp_strLocation) & "', "
        lp_strSQL = lp_strSQL & "billtocompanyname = '" & lp_strBillToCompany & "', "
        lp_strSQL = lp_strSQL & "billtocompanyID = '" & lp_lngBillToCompanyID & "', "
        lp_strSQL = lp_strSQL & "notes = '" & QuoteSanitise(lp_strNotes) & "', "
        lp_strSQL = lp_strSQL & "email = '" & QuoteSanitise(lp_strEmail) & "', "
        lp_strSQL = lp_strSQL & "purchaseheaderID = '" & lp_lngPurchaseHeaderID & "', "
        lp_strSQL = lp_strSQL & "charge = '" & lp_dblCharge & "', "
        lp_strSQL = lp_strSQL & "deliveredby = '" & QuoteSanitise(lp_strDeliveredBy) & "', "
        lp_strSQL = lp_strSQL & "courierreference = '" & QuoteSanitise(UCase(lp_strCourierReference)) & "', "
        lp_strSQL = lp_strSQL & "costcode = '" & QuoteSanitise(lp_strCostCode) & "', "
        lp_strSQL = lp_strSQL & "deliveryfor = '" & QuoteSanitise(lp_strDeliveryFor) & "' "
        lp_strSQL = lp_strSQL & " WHERE despatchID = '" & lp_lngDespatchID & "';"
    End If
    
    ExecuteSQL lp_strSQL, g_strExecuteError
    
    CheckForSQLError
    
    If lp_lngDespatchID = 0 Then
        lp_lngDespatchID = g_lngLastID
        If lp_lngJobID <> 0 Then AddJobHistory lp_lngJobID, "Created Despatch (" & lp_lngDespatchID & ")"
    Else
        If lp_lngJobID <> 0 Then AddJobHistory lp_lngJobID, "Updated Despatch (" & lp_lngDespatchID & ")"
    End If
    
    SaveDespatch = lp_lngDespatchID
    
    Exit Function
DespatchVTJob_Error:
    
    MsgBox Err.Description, vbExclamation
    SaveDespatch = False
    Exit Function
    
End Function
Function IsBarcodeOnDespatch(lp_lngDespatchID As Long, lp_strBarcode) As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_lngResourceID As Long
    l_lngResourceID = GetData("resource", "resourceID", "barcode", lp_strBarcode)

    If IsGeneric(l_lngResourceID) Then
        IsBarcodeOnDespatch = False
        Exit Function
    End If

    Dim l_strSQL As String
    l_strSQL = "SELECT TOP 1 despatchdetailID FROM despatchdetail WHERE despatchID = '" & lp_lngDespatchID & "' AND barcode = '" & lp_strBarcode & "'"
    
    If Val(GetDataSQL(l_strSQL)) > 0 Then
        IsBarcodeOnDespatch = True
    Else
        IsBarcodeOnDespatch = False
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function IsBarcodeAlreadyAudited(lp_strShelf As String, lp_strBarcode) As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    l_strSQL = "SELECT TOP 1 libraryID FROM library WHERE shelf = '" & lp_strShelf & "' AND barcode = '" & lp_strBarcode & "'"
    
    If Val(GetDataSQL(l_strSQL)) > 0 Then
        IsBarcodeAlreadyAudited = True
    Else
        IsBarcodeAlreadyAudited = False
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub NoDespatchSelectedMessage()
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    MsgBox "Please select a valid (saved) despatch note first", vbExclamation
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub ShowDespatch(ByVal lp_strCriterea As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/showdespatch") Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    DoEvents
    
    If IsFormLoaded("frmDespatch") Then
        frmDespatch.Show
        frmDespatch.WindowState = vbMaximized
        frmDespatch.ZOrder 0
        
    End If
    
    Dim l_strSQL As String
    Dim l_datSpecifiedDate As Date
    
    Select Case lp_strCriterea
            
        Case "mdibuttonclick"
            l_strSQL = "SELECT * FROM despatch WHERE (despatcheddate Is NULL)"
        Case "incomplete"
            l_strSQL = "SELECT * FROM despatch WHERE (despatcheddate Is NULL)"
        Case "complete"
            l_strSQL = "SELECT * FROM despatch WHERE (despatcheddate Is Not NULL)"
        Case "all"
            l_strSQL = "SELECT * FROM despatch WHERE (despatchID > 0)"
    End Select
    
    If Not IsNull(frmDespatch.datDeadlineDate.Value) Then
        l_datSpecifiedDate = Format(frmDespatch.datDeadlineDate.Value, vbShortDateFormat) & " 00:00"
        l_strSQL = l_strSQL & " AND (deadlinedate BETWEEN '" & FormatSQLDate(l_datSpecifiedDate) & "' AND '" & FormatSQLDate(DateAdd("d", 1, l_datSpecifiedDate)) & "')"
    End If
    
    'select the type of despatches to show
    
    
    Dim l_strSQLTypeOfJob As String
    
    
    
    If frmDespatch.chkShow(0).Value <> 0 Then 'hire
        l_strSQLTypeOfJob = " ((typeofjob LIKE 'HIRE')"
    End If

    If frmDespatch.chkShow(1).Value <> 0 Then 'dubbing
        If l_strSQLTypeOfJob = "" Then
            l_strSQLTypeOfJob = " ((typeofjob LIKE 'DUBBING') OR (typeofjob LIKE 'VT')"
        Else
            l_strSQLTypeOfJob = l_strSQLTypeOfJob & "OR (typeofjob LIKE 'DUBBING') OR (typeofjob LIKE 'VT')"
        End If
    End If
    
    If frmDespatch.chkShow(2).Value <> 0 Then 'dubbing
        If l_strSQLTypeOfJob = "" Then
            l_strSQLTypeOfJob = " ((typeofjob LIKE 'EDIT')"
        Else
            l_strSQLTypeOfJob = l_strSQLTypeOfJob & "OR (typeofjob LIKE 'EDIT')"
        End If
    End If
    
    If frmDespatch.chkShow(3).Value <> 0 Then 'purchase
        If l_strSQLTypeOfJob = "" Then
            l_strSQLTypeOfJob = "  ((typeofjob LIKE 'PURCHASE')"
        Else
            l_strSQLTypeOfJob = l_strSQLTypeOfJob & "OR (typeofjob LIKE 'PURCHASE')"
        End If
    End If
    
    
    If l_strSQLTypeOfJob <> "" Then l_strSQLTypeOfJob = l_strSQLTypeOfJob & ")"
    
    '-----------------------------------------
    
    If frmDespatch.cmbOrderBy(0).Text = "cmbOrderBy" Then
        frmDespatch.cmbOrderBy(0).Text = "deadlinedate"
    End If
    
    If frmDespatch.cmbOrderBy(1).Text = "cmbOrderBy" Then
        frmDespatch.cmbOrderBy(1).Text = "deadlinedate"
    End If
    
    
    Dim l_conJobHistory As ADODB.Connection
    Dim l_rstJobHistory As ADODB.Recordset
    
    Dim l_strSort As String
    l_strSort = " ORDER BY despatch." & frmDespatch.cmbOrderBy(0).Text & " " & frmDespatch.cmbDirection(0).Text
    
    frmDespatch.adoDelivery(0).RecordSource = l_strSQL & " AND (direction = 'OUT')"
    If l_strSQLTypeOfJob <> "" Then frmDespatch.adoDelivery(0).RecordSource = frmDespatch.adoDelivery(0).RecordSource & " AND " & l_strSQLTypeOfJob
    frmDespatch.adoDelivery(0).RecordSource = frmDespatch.adoDelivery(0).RecordSource & l_strSort
    frmDespatch.adoDelivery(0).ConnectionString = g_strConnection
    frmDespatch.adoDelivery(0).Refresh
    
    l_strSort = " ORDER BY despatch." & frmDespatch.cmbOrderBy(1).Text & " " & frmDespatch.cmbDirection(1).Text
    frmDespatch.adoDelivery(1).RecordSource = l_strSQL & " AND (direction = 'IN')"
    If l_strSQLTypeOfJob <> "" Then frmDespatch.adoDelivery(1).RecordSource = frmDespatch.adoDelivery(1).RecordSource & " AND " & l_strSQLTypeOfJob
    frmDespatch.adoDelivery(1).RecordSource = frmDespatch.adoDelivery(1).RecordSource & l_strSort
    frmDespatch.adoDelivery(1).ConnectionString = g_strConnection
    
    frmDespatch.adoDelivery(1).Refresh
    
    frmDespatch.Show
    frmDespatch.ZOrder 0
    
    Screen.MousePointer = vbDefault
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub
Sub ShowDespatchDetail(ByVal lp_lngDespatchID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/showdespatchdetail") Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    Dim l_strSQL As String
    
    l_strSQL = "SELECT * FROM despatch WHERE despatchID = '" & lp_lngDespatchID & "'"
    
    Dim l_rstDespatch As New ADODB.Recordset
    Set l_rstDespatch = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    CheckForSQLError
    
    If Not l_rstDespatch.EOF Then
        
        l_rstDespatch.MoveFirst
        
        With frmJobDespatch
            .cmbDeliveryFor.Text = Format(l_rstDespatch("deliveryfor"))
            .txtDespatchNumber.Text = Format(l_rstDespatch("despatchnumber"))
            .txtProjectNumber.Text = Format(l_rstDespatch("projectnumber")) 'GetData("project", "projectnumber", "projectID", l_rstDespatch("projectID"))
            .txtCourierReference.Text = Format(l_rstDespatch("courierreference"))
            .txtNotes.Text = Trim(" " & l_rstDespatch("notes"))
            .cmbCompany.Text = Format(l_rstDespatch("companyname"))
            .cmbJobID.Text = Format(l_rstDespatch("jobID"))
            .lblCompanyID.Caption = Format(l_rstDespatch("companyID"))
            .cmbContact.Text = Format(l_rstDespatch("contactname"))
            .lblContactID.Caption = Format(l_rstDespatch("contactID"))
            .txtAddress.Text = Format(l_rstDespatch("address"))
            .txtPostCode.Text = Format(l_rstDespatch("postcode"))
            .txtTelephone.Text = Format(l_rstDespatch("telephone"))
            .txtCountry.Text = Format(l_rstDespatch("country"))
            
             frmJobDespatch.txtProduct.Text = GetData("job", "productname", "jobID", .cmbJobID.Text)
            
'populate the tags too, so we can check for modifications
            .cmbCompany.Tag = Format(l_rstDespatch("companyname"))
            .cmbJobID.Tag = Format(l_rstDespatch("jobID"))
            .cmbContact.Tag = Format(l_rstDespatch("contactname"))
            .txtAddress.Tag = Format(l_rstDespatch("address"))
            .txtPostCode.Tag = Format(l_rstDespatch("postcode"))
            .txtTelephone.Tag = Format(l_rstDespatch("telephone"))
            .txtCountry.Tag = Format(l_rstDespatch("country"))
            
            .cmbBillToCompany.Text = Format(l_rstDespatch("billtocompanyname"))
            .lblBillToCompanyID.Caption = Format(l_rstDespatch("billtocompanyID"))
            If IsNull(l_rstDespatch("deadlinedate")) Then
                .datDeadlineDate.Value = Null
                .cmbDeadlineTime.Text = ""
            Else
                .datDeadlineDate.Value = Format(l_rstDespatch("deadlinedate"), vbShortDateFormat)
            End If
            .cmbDeadlineTime.Text = Format(l_rstDespatch("deadlinedate"), "HH:NN")
            .txtDespatchID.Text = lp_lngDespatchID
            .txtDescription.Text = Format(l_rstDespatch("description"))
            .cmbDirection.Text = Format(l_rstDespatch("direction"))
            .txtDespatchedOn.Text = Format(l_rstDespatch("despatcheddate"))
            .txtDespatchedUser.Text = Format(l_rstDespatch("despatcheduser"))
            .cmbMethod.Text = Format(l_rstDespatch("deliverymethod"))
            .cmbLocation.Text = Format(l_rstDespatch("location"))
            .txtCharge.Text = Format(l_rstDespatch("charge"), "#.00")
            .txtPurchaseAuthorisationNumber.Text = Format(GetData("purchaseheader", "authorisationnumber", "purchaseheaderID", l_rstDespatch("purchaseheaderID")))
            .txtEmail.Text = Format(l_rstDespatch("email"))
            .cmbDeliveredBy.Text = Format(l_rstDespatch("deliveredby"))
            .cmbCostCode.Text = Format(l_rstDespatch("costcode"))
        End With
    Else
        ClearFields frmJobDespatch
    End If
    
    l_rstDespatch.Close
    Set l_rstDespatch = Nothing
    
    
    frmJobDespatch.lblItemCount.Caption = GetItemCountForDespatch(lp_lngDespatchID)
    
    frmJobDespatch.adoDespatchDetail.RecordSource = "SELECT * FROM despatchdetail WHERE despatchID = '" & lp_lngDespatchID & "' ORDER BY packagenumber, -despatchdetailID "
    frmJobDespatch.adoDespatchDetail.ConnectionString = g_strConnection
    frmJobDespatch.adoDespatchDetail.Refresh
    
    If UCase(frmJobDespatch.cmbDirection.Text) = "OUT" Then
        
        l_strSQL = "SELECT resource.name AS 'Resource', resourceschedule.serialnumber AS 'Serial No',  resource.barcode AS 'Barcode', resourceschedule.fd_status AS 'Status', resourceschedule.resourceID, contactID, hiredescription, resourcescheduleID FROM resourceschedule LEFT JOIN resource ON resourceschedule.resourceID = resource.resourceID WHERE resourceschedule.jobID = " & frmJobDespatch.cmbJobID.Text & " ORDER BY resourcescheduleID"
        frmJobDespatch.grdResourceSchedule.Caption = "Items Booked On Job"
    Else
    
        l_strSQL = "SELECT title AS 'Resource', serialnumber AS 'Serial No', barcode AS 'Barcode', fd_status AS 'Status', despatchdetailID FROM despatchdetail INNER JOIN despatch ON despatchdetail.despatchID = despatch.despatchID WHERE despatch.jobID LIKE '" & frmJobDespatch.cmbJobID.Text & "' AND despatch.direction = 'OUT';"
    
        'l_strSQL = "SELECT title AS 'Resource', despatchdetail.fd_status , barcode AS 'Barcode' FROM despatchdetail INNER JOIN despatch ON despatch.despatchID = despatchdetail.despatchID WHERE despatch.direction = 'OUT' AND despatch.jobID = '" & frmJobDespatch.txtJobID.Text & "';"
        frmJobDespatch.grdResourceSchedule.Caption = "Items Despatched On Job"
    End If
    
    frmJobDespatch.adoResourceSchedule.RecordSource = l_strSQL
    frmJobDespatch.adoResourceSchedule.ConnectionString = g_strConnection
    frmJobDespatch.adoResourceSchedule.Refresh
    
    
    
    
    frmJobDespatch.grdResourceSchedule.Columns(0).Width = 1500
    frmJobDespatch.grdResourceSchedule.Columns(0).DataField = "Resource"
    
    frmJobDespatch.grdResourceSchedule.Columns(1).Width = 1000
    frmJobDespatch.grdResourceSchedule.Columns(1).DataField = "Serial No"
    
    frmJobDespatch.grdResourceSchedule.Columns(2).Width = 1000
    frmJobDespatch.grdResourceSchedule.Columns(2).DataField = "Barcode"
    
    frmJobDespatch.grdResourceSchedule.Columns(3).Width = 1000
    frmJobDespatch.grdResourceSchedule.Columns(3).DataField = "Status"
    
    
    
    Dim l_strJobType As String
    l_strJobType = GetData("job", "jobtype", "jobID", frmJobDespatch.cmbJobID.Text)
    
    If UCase(l_strJobType) = "HIRE" Or UCase(l_strJobType) = "EDIT" Then
        frmJobDespatch.optWhatToScan(1).Value = True
    
        If UCase(GetData("cetauser", "department", "cetauserID", g_lngUserID)) = "ENGINEERING" Then
            frmJobDespatch.chkExpand.Value = 1
        End If
    End If
    

    
    frmJobDespatch.Show
    frmJobDespatch.ZOrder 0
    
    Screen.MousePointer = vbDefault
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub


Sub SaveDeliveryAddress(lp_strCompany As String, lp_strContact As String, lp_strAddress As String, lp_strPostCode As String, lp_strTelephone As String, lp_strCountry As String, lp_strDeliveryID As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    
    If lp_strDeliveryID = "NEW" Then
        l_strSQL = "INSERT INTO deliveryaddress (deliverycompanyname, attentionof, address, postcode, telephone, country) VALUES ("
        l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strCompany) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strContact) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strAddress) & "', "
        l_strSQL = l_strSQL & "'" & lp_strPostCode & "', "
        l_strSQL = l_strSQL & "'" & lp_strTelephone & "', "
        l_strSQL = l_strSQL & "'" & lp_strCountry & "') "
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    Else
        l_strSQL = "UPDATE deliveryaddress SET "
        l_strSQL = l_strSQL & "deliverycompanyname = '" & QuoteSanitise(lp_strCompany) & "', "
        l_strSQL = l_strSQL & "attentionof = '" & QuoteSanitise(lp_strContact) & "', "
        l_strSQL = l_strSQL & "address = '" & QuoteSanitise(lp_strAddress) & "', "
        l_strSQL = l_strSQL & "postcode = '" & lp_strPostCode & "', "
        l_strSQL = l_strSQL & "telephone = '" & lp_strTelephone & "', "
        l_strSQL = l_strSQL & "country = '" & lp_strCountry & "' "
        l_strSQL = l_strSQL & "WHERE deliveryaddressID = " & lp_strDeliveryID
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub
Sub ShowDespatchForJob(ByVal lp_lngJobID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/showdespatchforjob") Then
        Exit Sub
    End If
    
'    If IsFormLoaded("frmJobDespatch") Then
'        frmJobDespatch.Show
'        frmJobDespatch.ZOrder 0
'        Exit Sub
'    End If
    
    If lp_lngJobID <= 0 Then
        ClearFields frmJobDespatch
    End If
    
    If lp_lngJobID <> 0 Then
        
        frmJobDespatch.adoDelivery.RecordSource = "SELECT * FROM despatch WHERE jobID = " & lp_lngJobID & " ORDER BY deadlinedate ASC"
        frmJobDespatch.adoDelivery.ConnectionString = g_strConnection
        frmJobDespatch.adoDelivery.Refresh
        
        frmJobDespatch.adoJobDetail.RecordSource = "SELECT * FROM jobdetail WHERE jobID = " & lp_lngJobID & " ORDER BY fd_orderby, jobdetailID"
        frmJobDespatch.adoJobDetail.ConnectionString = g_strConnection
        frmJobDespatch.adoJobDetail.Refresh
        
        Dim l_datDeadlineDate As Date
        l_datDeadlineDate = GetData("job", "deadlinedate", "jobID", lp_lngJobID)
        
        If l_datDeadlineDate <> 0 Then
            frmJobDespatch.datDeadlineDate.Value = Format(l_datDeadlineDate, vbShortDateFormat)
            frmJobDespatch.cmbDeadlineTime.Text = Format(l_datDeadlineDate, "HH:nn")
        End If
        
        frmJobDespatch.cmbJobID.Text = lp_lngJobID
        
        frmJobDespatch.txtProduct.Text = GetData("job", "productname", "jobID", lp_lngJobID)
        
        frmJobDespatch.txtProjectNumber.Text = GetJobProjectNumber(lp_lngJobID)
        
        Dim l_strJobType As String
        l_strJobType = GetData("job", "jobtype", "jobID", lp_lngJobID)
        
        If UCase(l_strJobType) = "HIRE" Then
            frmJobDespatch.optWhatToScan(1).Value = True
        End If
        
        
        
    End If
    
    
    
    frmJobDespatch.Show
    frmJobDespatch.ZOrder 0
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub
Sub ShowDespatchForProject(ByVal lp_lngProjectNumber As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/showdespatchforproject") Then
        Exit Sub
    End If
    
    If lp_lngProjectNumber <= 0 Then
        ClearFields frmJobDespatch
    End If
    
    If lp_lngProjectNumber <> 0 Then
        
        Dim l_lngProjectID As Long
        l_lngProjectID = GetData("project", "projectID", "projectnumber", lp_lngProjectNumber)
        
        If l_lngProjectID <> 0 Then
            
            frmJobDespatch.adoDelivery.RecordSource = "SELECT * FROM despatch WHERE projectID = " & l_lngProjectID & " ORDER BY deadlinedate ASC"
            frmJobDespatch.adoDelivery.ConnectionString = g_strConnection
            frmJobDespatch.adoDelivery.Refresh
             
            Dim l_datDeadlineDate As Date
            l_datDeadlineDate = GetData("project", "enddate", "projectID", l_lngProjectID)
            
            If l_datDeadlineDate <> 0 Then
                frmJobDespatch.datDeadlineDate.Value = Format(l_datDeadlineDate, vbShortDateFormat)
                frmJobDespatch.cmbDeadlineTime.Text = Format(l_datDeadlineDate, "HH:nn")
            End If
        
        End If
        
    End If
    
    
    
    frmJobDespatch.Show
    frmJobDespatch.ZOrder 0
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub
Sub ShowDespatchSearch()
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/showdespatchsearch") Then
        Exit Sub
    End If
    
    frmSearchDespatch.Show
    frmSearchDespatch.ZOrder 0
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub
Public Sub UpdateDespatchForJob(lp_lngJobID As Long)

If lp_lngJobID = 0 Then Exit Sub

'checks for existing despatch, and updates the details accordingly...

Dim l_strIncomingMethod As String
Dim l_strOutgoingMethod As String
Dim l_strTitle As String
Dim l_lngDespatchID  As Long
Dim l_strSQL As String
Dim l_datDespatchDate As Date
Dim l_datReturnDate As Date
Dim l_strJobStatus As String
Dim l_strJobType As String
Dim l_lngDespatchDespatchID As Long
Dim l_lngCollectionDespatchID As Long
Dim l_strDespatchCompanyName As String, l_strDespatchContactName As String, l_strDespatchCompanyAddress As String, l_strDespatchCompanyPostCode As String, l_strDespatchCompanyCountry As String, l_strDespatchCompanyTelephone As String
Dim l_strCollectionCompanyName As String, l_strCollectionContactName As String, l_strCollectionCompanyAddress As String, l_strCollectionCompanyPostCode As String, l_strCollectionCompanyCountry As String, l_strCollectionCompanyTelephone As String
Dim l_lngProjectNumber As Long
Dim l_strDeliveryAddressDetailsSQL As String
Dim l_strCollectionAddressDetailsSQL As String


'open a recordset to get all the details from the job
Dim l_rstJob As New ADODB.Recordset
Dim l_conJob As New ADODB.Connection

'connect
l_conJob.Open g_strConnection
l_rstJob.Open "SELECT * FROM job WHERE jobID = '" & lp_lngJobID & "';", l_conJob, adOpenStatic, adLockReadOnly

'pick up the defaults from the job
l_strIncomingMethod = Format(l_rstJob("incomingdespatchmethod"))
l_strOutgoingMethod = Format(l_rstJob("outgoingdespatchmethod"))
l_strJobStatus = Format(l_rstJob("fd_status"))
l_strTitle = GetJobDescription(lp_lngJobID)
l_strJobType = LCase(Format(l_rstJob("jobtype")))
l_strTitle = UCase(l_strJobType) & ": " & l_strTitle
l_lngProjectNumber = Val(Format(l_rstJob("projectnumber")))

'get despatch specific information
l_lngDespatchDespatchID = Val(Format(l_rstJob("deliverydnotenumber")))
l_strDespatchCompanyName = Format(l_rstJob("deliverycompanyname"))
l_strDespatchContactName = Format(l_rstJob("deliverycontactname"))
l_strDespatchCompanyAddress = Format(l_rstJob("deliveryaddress"))
l_strDespatchCompanyPostCode = Format(l_rstJob("deliverypostcode"))
l_strDespatchCompanyCountry = Format(l_rstJob("deliverycountry"))
l_strDespatchCompanyTelephone = Format(l_rstJob("deliverytelephone"))

'get collection specific information
l_lngCollectionDespatchID = Val(Format(l_rstJob("collectiondnotenumber")))
l_strCollectionCompanyName = Format(l_rstJob("collectioncompanyname"))
l_strCollectionContactName = Format(l_rstJob("collectioncontactname"))
l_strCollectionCompanyAddress = Format(l_rstJob("collectionaddress"))
l_strCollectionCompanyPostCode = Format(l_rstJob("collectionpostcode"))
l_strCollectionCompanyCountry = Format(l_rstJob("collectioncountry"))
l_strCollectionCompanyTelephone = Format(l_rstJob("collectiontelephone"))

'if this job is cancelled, then ensure that the dnote gets cancelled
If LCase(l_strJobStatus) = "cancelled" Then l_strTitle = "CANCELLED"

'is this a hire job, and has the start date been specified
If (l_strJobType = "hire" Or l_strJobType = "edit") And Not IsNull(l_rstJob("startdate")) Then
    
    'pick up the dates
    l_datDespatchDate = Format(l_rstJob("startdate"))
    l_datReturnDate = Format(l_rstJob("enddate"))
    
    '\\first do the outgoing despatch

    'check for existing despatches
    If l_lngDespatchDespatchID = 0 Then
        l_lngDespatchID = GetDespatchIDForJob(lp_lngJobID, "OUT")
    Else
        l_lngDespatchID = l_lngDespatchDespatchID
    End If
                    
    'there is no delivery note for this job, so create one
    If l_lngDespatchID = 0 Then
        'if it is a hire job and the status is valid
        If l_strJobType = "hire" And LCase(l_strJobStatus) <> "pencil" And LCase(l_strJobStatus) <> "2nd pencil" And LCase(l_strJobStatus) <> "cancelled" Then
            'create a new despatch entry
            l_lngDespatchID = SaveDespatch(l_lngDespatchID, "Hire", l_lngProjectNumber, lp_lngJobID, "OUT", l_strOutgoingMethod, l_rstJob("companyID"), l_strDespatchCompanyName, l_strDespatchContactName, l_rstJob("contactID"), l_strDespatchCompanyAddress, l_strDespatchCompanyPostCode, l_strDespatchCompanyTelephone, l_strDespatchCompanyCountry, l_rstJob("startdate"), l_strTitle, l_datDespatchDate, 0, "", "", "", 0, GetData("contact", "email", "contactID", l_rstJob("contactID")), 0, 0, "", "", "", "DEL - HIRE")
        
            AddJobHistory lp_lngJobID, "Auto-Update Created Delivery: " & l_strDespatchCompanyName & ", " & l_strDespatchCompanyAddress & ", " & l_strDespatchCompanyPostCode
        End If
    Else
        'the despatch entry already exists, so just update it.
        l_strDeliveryAddressDetailsSQL = "companyname = '" & QuoteSanitise(Format(l_rstJob("deliverycompanyname"))) & "', contactname = '" & QuoteSanitise(Format(l_rstJob("deliverycontactname"))) & "', address = '" & QuoteSanitise(Format(l_rstJob("deliveryaddress"))) & "', postcode = '" & QuoteSanitise(Format(l_rstJob("deliverypostcode"))) & "', country = '" & QuoteSanitise(Format(l_rstJob("deliverycountry"))) & "', telephone = '" & QuoteSanitise(Format(l_rstJob("deliverytelephone"))) & "'"
        l_strSQL = "UPDATE despatch SET " & l_strDeliveryAddressDetailsSQL & ", deliverymethod = '" & QuoteSanitise(l_strOutgoingMethod) & "', description = '" & QuoteSanitise(l_strTitle) & "', deadlinedate = '" & FormatSQLDate(l_datDespatchDate) & "', projectnumber = '" & l_lngProjectNumber & "' WHERE despatchID = '" & l_lngDespatchID & "';"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        AddJobHistory lp_lngJobID, "Auto-Update Updated Delivery: " & l_strDespatchCompanyName & ", " & l_strDespatchCompanyAddress & ", " & l_strDespatchCompanyPostCode
    End If
    
    'update the job with the despatch ID
    l_strSQL = "UPDATE job SET deliverydnotenumber = '" & l_lngDespatchID & "' WHERE jobID = '" & lp_lngJobID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    
    
    '\\now do the incoming despatch
    
    'check for existing despatches
    If l_lngCollectionDespatchID = 0 Then
        l_lngDespatchID = GetDespatchIDForJob(lp_lngJobID, "IN")
    Else
        l_lngDespatchID = l_lngCollectionDespatchID
    End If
    
    'there is no collection note for this job, so create one
    If l_lngDespatchID = 0 Then
        'if it is a hire, and the job is valid
        If l_strJobType = "hire" And LCase(l_strJobStatus) <> "pencil" And LCase(l_strJobStatus) <> "2nd pencil" And LCase(l_strJobStatus) <> "cancelled" Then
            'create a new despatch entry
            l_lngDespatchID = SaveDespatch(l_lngDespatchID, "Hire", l_lngProjectNumber, lp_lngJobID, "IN", l_strIncomingMethod, l_rstJob("companyID"), l_strCollectionCompanyName, l_strCollectionContactName, l_rstJob("contactID"), l_strCollectionCompanyAddress, l_strCollectionCompanyPostCode, l_strCollectionCompanyTelephone, l_strCollectionCompanyCountry, l_rstJob("startdate"), l_strTitle, l_datDespatchDate, 0, "", "", "", 0, GetData("contact", "email", "contactID", l_rstJob("contactID")), 0, 0, "", "", "", "DEL - HIRE")
            
            AddJobHistory lp_lngJobID, "Auto-Update Created Collection: " & l_strCollectionCompanyName & ", " & l_strCollectionCompanyAddress & ", " & l_strCollectionCompanyPostCode
            
        End If
    Else
        'there is already a delivery note so update it
        l_strCollectionAddressDetailsSQL = "companyname = '" & QuoteSanitise(Format(l_rstJob("collectioncompanyname"))) & "', contactname = '" & QuoteSanitise(Format(l_rstJob("collectioncontactname"))) & "', address = '" & QuoteSanitise(Format(l_rstJob("collectionaddress"))) & "', postcode = '" & QuoteSanitise(Format(l_rstJob("collectionpostcode"))) & "', country = '" & QuoteSanitise(Format(l_rstJob("collectioncountry"))) & "', telephone = '" & QuoteSanitise(Format(l_rstJob("collectiontelephone"))) & "'"
        l_strSQL = "UPDATE despatch SET " & l_strCollectionAddressDetailsSQL & ", deliverymethod = '" & l_strIncomingMethod & "', description = '" & l_strTitle & "', deadlinedate = '" & FormatSQLDate(l_datReturnDate) & "',projectnumber = '" & l_lngProjectNumber & "' WHERE despatchID = '" & l_lngDespatchID & "'"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        AddJobHistory lp_lngJobID, "Auto-Update Updated Collection: " & l_strCollectionCompanyName & ", " & l_strCollectionCompanyAddress & ", " & l_strCollectionCompanyPostCode
    End If

    'update the job with the despatch ID
    l_strSQL = "UPDATE job SET collectiondnotenumber = '" & l_lngDespatchID & "' WHERE jobID = '" & lp_lngJobID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError

    

End If

l_rstJob.Close
Set l_rstJob = Nothing

l_conJob.Close
Set l_conJob = Nothing




End Sub
















Sub UpdateDespatchStatus(ByVal lp_lngDespatchID As Long, ByVal lp_strStatus As String)


Dim l_strSQL As String
l_strSQL = "UPDATE despatchdetail SET fd_status = '" & lp_strStatus & "' WHERE despatchID = '" & lp_lngDespatchID & "';"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError


End Sub


