VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmMakeADub 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Library Event Transfer"
   ClientHeight    =   7320
   ClientLeft      =   3675
   ClientTop       =   5595
   ClientWidth     =   12810
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7320
   ScaleWidth      =   12810
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdLoadLibrary 
      Caption         =   "Show Library Details"
      Height          =   315
      Left            =   10680
      TabIndex        =   10
      Top             =   3540
      Width           =   2055
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   11400
      TabIndex        =   9
      Top             =   6900
      Width           =   1335
   End
   Begin VB.CommandButton cmdRefreshEvents 
      Caption         =   "REFRESH EVENTS"
      Height          =   315
      Left            =   120
      TabIndex        =   8
      Top             =   6900
      Visible         =   0   'False
      Width           =   1875
   End
   Begin VB.CommandButton cmdNewLibraryItem 
      Caption         =   "New Library Item"
      Height          =   315
      Left            =   4320
      TabIndex        =   6
      Top             =   3540
      Width           =   2055
   End
   Begin VB.TextBox txtDestinationBarcode 
      Height          =   315
      Left            =   1680
      TabIndex        =   4
      Top             =   3540
      Width           =   2355
   End
   Begin VB.TextBox txtBarcode 
      Height          =   315
      Left            =   1680
      TabIndex        =   0
      Top             =   120
      Width           =   2355
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdEvents 
      Bindings        =   "frmMakeADub.frx":0000
      Height          =   2835
      Left            =   120
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "The Tape Events List"
      Top             =   540
      Width           =   12615
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      BackColorOdd    =   12648447
      RowHeight       =   423
      Columns.Count   =   18
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "eventID"
      Columns(0).Name =   "eventID"
      Columns(0).DataField=   "eventID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   2117
      Columns(1).Caption=   "Event Date"
      Columns(1).Name =   "eventdate"
      Columns(1).DataField=   "eventdate"
      Columns(1).DataType=   7
      Columns(1).FieldLen=   256
      Columns(2).Width=   2196
      Columns(2).Caption=   "Start"
      Columns(2).Name =   "timecodestart"
      Columns(2).DataField=   "timecodestart"
      Columns(2).FieldLen=   256
      Columns(2).Style=   1
      Columns(3).Width=   2196
      Columns(3).Caption=   "Stop"
      Columns(3).Name =   "timecodestop"
      Columns(3).DataField=   "timecodestop"
      Columns(3).FieldLen=   256
      Columns(3).Style=   1
      Columns(4).Width=   1323
      Columns(4).Caption=   "Duration"
      Columns(4).Name =   "length"
      Columns(4).DataField=   "fd_length"
      Columns(4).FieldLen=   256
      Columns(5).Width=   4657
      Columns(5).Caption=   "Title"
      Columns(5).Name =   "eventtitle"
      Columns(5).DataField=   "eventtitle"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   50
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "libraryID"
      Columns(6).Name =   "libraryID"
      Columns(6).DataField=   "libraryID"
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "Date"
      Columns(7).Name =   "createddate"
      Columns(7).DataField=   "cdate"
      Columns(7).DataType=   7
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "Editor"
      Columns(8).Name =   "editor1"
      Columns(8).DataField=   "editor1"
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "Assistant"
      Columns(9).Name =   "editor2"
      Columns(9).DataField=   "editor2"
      Columns(9).FieldLen=   256
      Columns(10).Width=   1931
      Columns(10).Caption=   "Type"
      Columns(10).Name=   "eventtype"
      Columns(10).DataField=   "eventtype"
      Columns(10).FieldLen=   256
      Columns(11).Width=   3201
      Columns(11).Caption=   "Clock"
      Columns(11).Name=   "clocknumber"
      Columns(11).DataField=   "clocknumber"
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "Sound Lay"
      Columns(12).Name=   "soundlay"
      Columns(12).DataField=   "soundlay"
      Columns(12).DataType=   7
      Columns(12).FieldLen=   256
      Columns(13).Width=   1958
      Columns(13).Caption=   "Aspect"
      Columns(13).Name=   "aspectratio"
      Columns(13).DataField=   "aspectratio"
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "Source Library ID"
      Columns(14).Name=   "sourcelibraryID"
      Columns(14).DataField=   "sourcelibraryID"
      Columns(14).FieldLen=   256
      Columns(15).Width=   3201
      Columns(15).Caption=   "Notes"
      Columns(15).Name=   "notes"
      Columns(15).DataField=   "notes"
      Columns(15).FieldLen=   256
      Columns(16).Width=   1905
      Columns(16).Caption=   "Job ID"
      Columns(16).Name=   "jobID"
      Columns(16).DataField=   "jobID"
      Columns(16).FieldLen=   256
      Columns(17).Width=   1905
      Columns(17).Caption=   "Project #"
      Columns(17).Name=   "projectnumber"
      Columns(17).DataField=   "projectnumber"
      Columns(17).FieldLen=   256
      _ExtentX        =   22251
      _ExtentY        =   5001
      _StockProps     =   79
      Caption         =   "Events"
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdDestinationEvents 
      Bindings        =   "frmMakeADub.frx":0019
      Height          =   2835
      Left            =   120
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "The Tape Events List"
      Top             =   3960
      Width           =   12615
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      BackColorOdd    =   12648447
      RowHeight       =   423
      Columns.Count   =   18
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "eventID"
      Columns(0).Name =   "eventID"
      Columns(0).DataField=   "eventID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   2117
      Columns(1).Caption=   "Event Date"
      Columns(1).Name =   "eventdate"
      Columns(1).DataField=   "eventdate"
      Columns(1).DataType=   7
      Columns(1).FieldLen=   256
      Columns(2).Width=   2196
      Columns(2).Caption=   "Start"
      Columns(2).Name =   "timecodestart"
      Columns(2).DataField=   "timecodestart"
      Columns(2).FieldLen=   256
      Columns(2).Style=   1
      Columns(3).Width=   2196
      Columns(3).Caption=   "Stop"
      Columns(3).Name =   "timecodestop"
      Columns(3).DataField=   "timecodestop"
      Columns(3).FieldLen=   256
      Columns(3).Style=   1
      Columns(4).Width=   1323
      Columns(4).Caption=   "Duration"
      Columns(4).Name =   "length"
      Columns(4).DataField=   "fd_length"
      Columns(4).FieldLen=   256
      Columns(5).Width=   4657
      Columns(5).Caption=   "Title"
      Columns(5).Name =   "eventtitle"
      Columns(5).DataField=   "eventtitle"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   50
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "libraryID"
      Columns(6).Name =   "libraryID"
      Columns(6).DataField=   "libraryID"
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "Date"
      Columns(7).Name =   "createddate"
      Columns(7).DataField=   "cdate"
      Columns(7).DataType=   7
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "Editor"
      Columns(8).Name =   "editor1"
      Columns(8).DataField=   "editor1"
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "Assistant"
      Columns(9).Name =   "editor2"
      Columns(9).DataField=   "editor2"
      Columns(9).FieldLen=   256
      Columns(10).Width=   1931
      Columns(10).Caption=   "Type"
      Columns(10).Name=   "eventtype"
      Columns(10).DataField=   "eventtype"
      Columns(10).FieldLen=   256
      Columns(11).Width=   3201
      Columns(11).Caption=   "Clock"
      Columns(11).Name=   "clocknumber"
      Columns(11).DataField=   "clocknumber"
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "Sound Lay"
      Columns(12).Name=   "soundlay"
      Columns(12).DataField=   "soundlay"
      Columns(12).DataType=   7
      Columns(12).FieldLen=   256
      Columns(13).Width=   1958
      Columns(13).Caption=   "Aspect"
      Columns(13).Name=   "aspectratio"
      Columns(13).DataField=   "aspectratio"
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "Source Library ID"
      Columns(14).Name=   "sourcelibraryID"
      Columns(14).DataField=   "sourcelibraryID"
      Columns(14).FieldLen=   256
      Columns(15).Width=   3201
      Columns(15).Caption=   "Notes"
      Columns(15).Name=   "notes"
      Columns(15).DataField=   "notes"
      Columns(15).FieldLen=   256
      Columns(16).Width=   1905
      Columns(16).Caption=   "Job ID"
      Columns(16).Name=   "jobID"
      Columns(16).DataField=   "jobID"
      Columns(16).FieldLen=   256
      Columns(17).Width=   1905
      Columns(17).Caption=   "Project #"
      Columns(17).Name=   "projectnumber"
      Columns(17).DataField=   "projectnumber"
      Columns(17).FieldLen=   256
      _ExtentX        =   22251
      _ExtentY        =   5001
      _StockProps     =   79
      Caption         =   "Events"
      BackColor       =   -2147483643
   End
   Begin MSAdodcLib.Adodc adoEvents1 
      Height          =   330
      Left            =   6960
      Top             =   120
      Visible         =   0   'False
      Width           =   2100
      _ExtentX        =   3704
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoEvents"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoEvents2 
      Height          =   330
      Left            =   6900
      Top             =   3480
      Visible         =   0   'False
      Width           =   2100
      _ExtentX        =   3704
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoEvents"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label lblLibraryID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   9180
      TabIndex        =   7
      Top             =   3540
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label lblCaption 
      Caption         =   "Destination Barcode"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   5
      Top             =   3540
      Width           =   1815
   End
   Begin VB.Label lblCaption 
      Caption         =   "Source Barcode"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmMakeADub"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdLoadLibrary_Click()
ShowLibrary Val(lblLibraryID.Caption)
End Sub

Private Sub cmdNewLibraryItem_Click()

Dim l_strBarcode As String
l_strBarcode = NewLibraryItem()

If l_strBarcode <> "" Then txtDestinationBarcode.Text = l_strBarcode
txtDestinationBarcode_KeyPress vbKeyReturn

End Sub

Private Sub cmdRefreshEvents_Click()
adoEvents2.Refresh
End Sub

Private Sub grdDestinationEvents_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Button = 2 Then
    g_CurrentlySelectedEventID = Val(grdDestinationEvents.Columns("eventid").Text)
    Me.PopupMenu MDIForm1.mnuHiddenEventsOptions
End If

End Sub

Private Sub grdEvents_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Button = 2 Then
    g_CurrentlySelectedEventID = Val(grdEvents.Columns("eventid").Text)
    Me.PopupMenu MDIForm1.mnuHiddenEventsOptions
End If
End Sub

Private Sub Text1_Change()

End Sub

Private Sub txtBarcode_GotFocus()
HighLite txtBarcode
End Sub

Private Sub txtBarcode_KeyPress(KeyAscii As Integer)

If KeyAscii = vbKeyReturn Then
    adoEvents1.ConnectionString = g_strConnection
    adoEvents1.RecordSource = "SELECT * FROM tapeevents WHERE libraryID = '" & GetData("library", "libraryID", "barcode", txtBarcode.Text) & "' ORDER BY timecodestart;"
    adoEvents1.Refresh
End If

End Sub

Private Sub txtDestinationBarcode_GotFocus()
HighLite txtDestinationBarcode
End Sub

Public Sub txtDestinationBarcode_KeyPress(KeyAscii As Integer)
If KeyAscii = vbKeyReturn Then

    Dim l_lngLibraryID As Long
    l_lngLibraryID = GetData("library", "libraryID", "barcode", txtDestinationBarcode.Text)
    lblLibraryID.Caption = l_lngLibraryID
    
    adoEvents2.ConnectionString = g_strConnection
    adoEvents2.RecordSource = "SELECT * FROM tapeevents WHERE libraryID = '" & l_lngLibraryID & "';"
    adoEvents2.Refresh
    
End If
End Sub
