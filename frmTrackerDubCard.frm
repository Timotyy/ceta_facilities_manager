VERSION 5.00
Begin VB.Form frmTrackerDubCard 
   Caption         =   "Tracker Dub Cards"
   ClientHeight    =   10020
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   18570
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10020
   ScaleWidth      =   18570
   StartUpPosition =   3  'Windows Default
   Begin CETA_Facilties_Manager.UniText txtLeft 
      Height          =   1935
      Index           =   1
      Left            =   1620
      TabIndex        =   20
      Top             =   900
      Width           =   7395
      _ExtentX        =   13044
      _ExtentY        =   3413
      Alignment       =   0
      Appearance      =   0
      BackColor       =   -2147483643
      BorderStyle     =   2
      CaptureEnter    =   -1  'True
      CaptureEsc      =   0   'False
      CaptureTab      =   0   'False
      Enabled         =   -1  'True
      FileCodepage    =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   -2147483640
      HideSelection   =   -1  'True
      Locked          =   0   'False
      MaxLength       =   -1
      MouseIcon       =   "frmTrackerDubCard.frx":0000
      MousePointer    =   0
      MultiLine       =   -1  'True
      PasswordChar    =   ""
      RightToLeft     =   0   'False
      ScrollBars      =   2
      Text            =   "frmTrackerDubCard.frx":001C
      UseEvents       =   -1  'True
   End
   Begin VB.CheckBox chkAutoProcess 
      Caption         =   "AutoProcess"
      Height          =   375
      Left            =   1980
      TabIndex        =   16
      Top             =   9300
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear"
      Height          =   375
      Left            =   12600
      TabIndex        =   15
      ToolTipText     =   "Save this File Record"
      Top             =   9480
      Width           =   1275
   End
   Begin VB.CommandButton cmdSaveAndProcess 
      Caption         =   "Process"
      Height          =   375
      Left            =   13980
      TabIndex        =   5
      ToolTipText     =   "Save this File Record"
      Top             =   9480
      Width           =   1635
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   17100
      TabIndex        =   4
      ToolTipText     =   "Close this form"
      Top             =   9480
      Width           =   1275
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save"
      Height          =   375
      Left            =   15720
      TabIndex        =   3
      ToolTipText     =   "Save this File Record"
      Top             =   9480
      Width           =   1275
   End
   Begin VB.ComboBox cmbLanguage 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   1620
      TabIndex        =   1
      ToolTipText     =   "The Clip Codec"
      Top             =   60
      Width           =   2655
   End
   Begin CETA_Facilties_Manager.UniText txtHead 
      Height          =   735
      Index           =   1
      Left            =   6360
      TabIndex        =   17
      Top             =   120
      Width           =   7395
      _ExtentX        =   13044
      _ExtentY        =   1296
      Alignment       =   0
      Appearance      =   0
      BackColor       =   -2147483643
      BorderStyle     =   2
      CaptureEnter    =   -1  'True
      CaptureEsc      =   0   'False
      CaptureTab      =   0   'False
      Enabled         =   -1  'True
      FileCodepage    =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   -2147483640
      HideSelection   =   -1  'True
      Locked          =   0   'False
      MaxLength       =   -1
      MouseIcon       =   "frmTrackerDubCard.frx":003C
      MousePointer    =   0
      MultiLine       =   -1  'True
      PasswordChar    =   ""
      RightToLeft     =   0   'False
      ScrollBars      =   0
      Text            =   "frmTrackerDubCard.frx":0058
      UseEvents       =   -1  'True
   End
   Begin CETA_Facilties_Manager.UniText txtHead 
      Height          =   735
      Index           =   2
      Left            =   6360
      TabIndex        =   18
      Top             =   3180
      Width           =   7395
      _ExtentX        =   13044
      _ExtentY        =   1296
      Alignment       =   0
      Appearance      =   0
      BackColor       =   -2147483643
      BorderStyle     =   2
      CaptureEnter    =   -1  'True
      CaptureEsc      =   0   'False
      CaptureTab      =   0   'False
      Enabled         =   -1  'True
      FileCodepage    =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   -2147483640
      HideSelection   =   -1  'True
      Locked          =   0   'False
      MaxLength       =   -1
      MouseIcon       =   "frmTrackerDubCard.frx":0078
      MousePointer    =   0
      MultiLine       =   -1  'True
      PasswordChar    =   ""
      RightToLeft     =   0   'False
      ScrollBars      =   0
      Text            =   "frmTrackerDubCard.frx":0094
      UseEvents       =   -1  'True
   End
   Begin CETA_Facilties_Manager.UniText txtHead 
      Height          =   735
      Index           =   3
      Left            =   6300
      TabIndex        =   19
      Top             =   6300
      Width           =   7395
      _ExtentX        =   13044
      _ExtentY        =   1296
      Alignment       =   0
      Appearance      =   0
      BackColor       =   -2147483643
      BorderStyle     =   2
      CaptureEnter    =   -1  'True
      CaptureEsc      =   0   'False
      CaptureTab      =   0   'False
      Enabled         =   -1  'True
      FileCodepage    =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   -2147483640
      HideSelection   =   -1  'True
      Locked          =   0   'False
      MaxLength       =   -1
      MouseIcon       =   "frmTrackerDubCard.frx":00B4
      MousePointer    =   0
      MultiLine       =   -1  'True
      PasswordChar    =   ""
      RightToLeft     =   0   'False
      ScrollBars      =   0
      Text            =   "frmTrackerDubCard.frx":00D0
      UseEvents       =   -1  'True
   End
   Begin CETA_Facilties_Manager.UniText txtLeft 
      Height          =   1935
      Index           =   2
      Left            =   1620
      TabIndex        =   21
      Top             =   3960
      Width           =   7395
      _ExtentX        =   13044
      _ExtentY        =   3413
      Alignment       =   0
      Appearance      =   0
      BackColor       =   -2147483643
      BorderStyle     =   2
      CaptureEnter    =   -1  'True
      CaptureEsc      =   0   'False
      CaptureTab      =   0   'False
      Enabled         =   -1  'True
      FileCodepage    =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   -2147483640
      HideSelection   =   -1  'True
      Locked          =   0   'False
      MaxLength       =   -1
      MouseIcon       =   "frmTrackerDubCard.frx":00F0
      MousePointer    =   0
      MultiLine       =   -1  'True
      PasswordChar    =   ""
      RightToLeft     =   0   'False
      ScrollBars      =   2
      Text            =   "frmTrackerDubCard.frx":010C
      UseEvents       =   -1  'True
   End
   Begin CETA_Facilties_Manager.UniText txtLeft 
      Height          =   1935
      Index           =   3
      Left            =   1620
      TabIndex        =   22
      Top             =   7080
      Width           =   7395
      _ExtentX        =   13044
      _ExtentY        =   3413
      Alignment       =   0
      Appearance      =   0
      BackColor       =   -2147483643
      BorderStyle     =   2
      CaptureEnter    =   -1  'True
      CaptureEsc      =   0   'False
      CaptureTab      =   0   'False
      Enabled         =   -1  'True
      FileCodepage    =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   -2147483640
      HideSelection   =   -1  'True
      Locked          =   0   'False
      MaxLength       =   -1
      MouseIcon       =   "frmTrackerDubCard.frx":012C
      MousePointer    =   0
      MultiLine       =   -1  'True
      PasswordChar    =   ""
      RightToLeft     =   0   'False
      ScrollBars      =   2
      Text            =   "frmTrackerDubCard.frx":0148
      UseEvents       =   -1  'True
   End
   Begin CETA_Facilties_Manager.UniText txtRight 
      Height          =   1935
      Index           =   1
      Left            =   10980
      TabIndex        =   23
      Top             =   900
      Width           =   7395
      _ExtentX        =   13044
      _ExtentY        =   3413
      Alignment       =   0
      Appearance      =   0
      BackColor       =   -2147483643
      BorderStyle     =   2
      CaptureEnter    =   -1  'True
      CaptureEsc      =   0   'False
      CaptureTab      =   0   'False
      Enabled         =   -1  'True
      FileCodepage    =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   -2147483640
      HideSelection   =   -1  'True
      Locked          =   0   'False
      MaxLength       =   -1
      MouseIcon       =   "frmTrackerDubCard.frx":0168
      MousePointer    =   0
      MultiLine       =   -1  'True
      PasswordChar    =   ""
      RightToLeft     =   0   'False
      ScrollBars      =   2
      Text            =   "frmTrackerDubCard.frx":0184
      UseEvents       =   -1  'True
   End
   Begin CETA_Facilties_Manager.UniText txtRight 
      Height          =   1935
      Index           =   2
      Left            =   10980
      TabIndex        =   24
      Top             =   3960
      Width           =   7395
      _ExtentX        =   13044
      _ExtentY        =   3413
      Alignment       =   0
      Appearance      =   0
      BackColor       =   -2147483643
      BorderStyle     =   2
      CaptureEnter    =   -1  'True
      CaptureEsc      =   0   'False
      CaptureTab      =   0   'False
      Enabled         =   -1  'True
      FileCodepage    =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   -2147483640
      HideSelection   =   -1  'True
      Locked          =   0   'False
      MaxLength       =   -1
      MouseIcon       =   "frmTrackerDubCard.frx":01A4
      MousePointer    =   0
      MultiLine       =   -1  'True
      PasswordChar    =   ""
      RightToLeft     =   0   'False
      ScrollBars      =   2
      Text            =   "frmTrackerDubCard.frx":01C0
      UseEvents       =   -1  'True
   End
   Begin CETA_Facilties_Manager.UniText txtRight 
      Height          =   1935
      Index           =   3
      Left            =   10980
      TabIndex        =   25
      Top             =   7080
      Width           =   7395
      _ExtentX        =   13044
      _ExtentY        =   3413
      Alignment       =   0
      Appearance      =   0
      BackColor       =   -2147483643
      BorderStyle     =   2
      CaptureEnter    =   -1  'True
      CaptureEsc      =   0   'False
      CaptureTab      =   0   'False
      Enabled         =   -1  'True
      FileCodepage    =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   -2147483640
      HideSelection   =   -1  'True
      Locked          =   0   'False
      MaxLength       =   -1
      MouseIcon       =   "frmTrackerDubCard.frx":01E0
      MousePointer    =   0
      MultiLine       =   -1  'True
      PasswordChar    =   ""
      RightToLeft     =   0   'False
      ScrollBars      =   2
      Text            =   "frmTrackerDubCard.frx":01FC
      UseEvents       =   -1  'True
   End
   Begin VB.Label Label1 
      Caption         =   $"frmTrackerDubCard.frx":021C
      ForeColor       =   &H000000FF&
      Height          =   855
      Left            =   13920
      TabIndex        =   26
      Top             =   60
      Width           =   4455
   End
   Begin VB.Label lblCaption 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Section 3 Actor / Person"
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   8
      Left            =   9540
      TabIndex        =   14
      Top             =   7440
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Section 3 Role / Character / Job"
      ForeColor       =   &H80000008&
      Height          =   735
      Index           =   7
      Left            =   180
      TabIndex        =   13
      Top             =   7440
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Section 3 Head"
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   6
      Left            =   4920
      TabIndex        =   12
      Top             =   6360
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Section 2 Actor / Person"
      ForeColor       =   &H80000008&
      Height          =   555
      Index           =   5
      Left            =   9540
      TabIndex        =   11
      Top             =   4380
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Section 2 Role / Character / Job"
      ForeColor       =   &H80000008&
      Height          =   855
      Index           =   4
      Left            =   180
      TabIndex        =   10
      Top             =   4380
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Section 2 Head"
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   3
      Left            =   5040
      TabIndex        =   9
      Top             =   3240
      Width           =   1335
   End
   Begin VB.Label lblCaption 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Section 1 Actor / Person"
      ForeColor       =   &H80000008&
      Height          =   555
      Index           =   2
      Left            =   9600
      TabIndex        =   8
      Top             =   1200
      Width           =   1335
   End
   Begin VB.Label lblCaption 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Section 1 Role / Character / Job"
      ForeColor       =   &H80000008&
      Height          =   495
      Index           =   1
      Left            =   240
      TabIndex        =   7
      Top             =   1200
      Width           =   1335
   End
   Begin VB.Label lblCaption 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Section 1 Head"
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   0
      Left            =   4980
      TabIndex        =   6
      Top             =   120
      Width           =   1335
   End
   Begin VB.Label lblCaption 
      Caption         =   "Language"
      Height          =   255
      Index           =   115
      Left            =   180
      TabIndex        =   2
      Top             =   120
      Width           =   1275
   End
   Begin VB.Label lbltracker_itemID 
      BackColor       =   &H0080FFFF&
      Height          =   255
      Left            =   9600
      TabIndex        =   0
      Top             =   9480
      Width           =   975
   End
End
Attribute VB_Name = "frmTrackerDubCard"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmbLanguage_GotFocus()

PopulateCombo "language", cmbLanguage
HighLite cmbLanguage

End Sub

Private Sub cmdClear_Click()

If MsgBox("Are you sure", vbYesNo + vbDefaultButton2) = vbYes Then
    ClearFields Me
End If

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdSave_Click()

If MsgBox("Saving this form may cause foreign characters and symbols to be lost." & vbCrLf & "Do you want to save", vbYesNo + vbDefaultButton2, "Save Dub Card Info") = vbNo Then Exit Sub

If SaveDubcard Then
    cmdClose_Click
End If

End Sub

Private Sub cmdSaveAndProcess_Click()

If ProcessDubcard = True Then
    cmdClose_Click
End If

End Sub

Private Sub Form_Activate()

Dim l_strSQL As String, l_rstDubcard As ADODB.Recordset

If lbltracker_itemID.Caption <> "" Then
    'load the existing data for this dubcard
    
    l_strSQL = "SELECT * FROM tracker_dubcard WHERE tracker_itemID = " & lbltracker_itemID.Caption & " AND Language = '" & QuoteSanitise(frmTrackeriTunes.grdDubcards.Columns("Language").Text) & "';"
    Set l_rstDubcard = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    If l_rstDubcard.RecordCount > 0 Then
        l_rstDubcard.MoveFirst
        cmbLanguage.Text = Trim(" " & l_rstDubcard("language"))
        Do While Not l_rstDubcard.EOF
            Select Case l_rstDubcard("SectionNumber")
                Case 1
                    Select Case l_rstDubcard("SectionPart")
                        Case "left"
                            txtLeft(1).Text = Trim(" " & l_rstDubcard("TextData"))
                        Case "right"
                            txtRight(1).Text = Trim(l_rstDubcard("TextData"))
                        Case "head"
                            txtHead(1).Text = Trim(" " & l_rstDubcard("TextData"))
                    End Select
                Case 2
                    Select Case l_rstDubcard("SectionPart")
                        Case "left"
                            txtLeft(2).Text = Trim(" " & l_rstDubcard("TextData"))
                        Case "right"
                            txtRight(2).Text = Trim(" " & l_rstDubcard("TextData"))
                        Case "head"
                            txtHead(2).Text = Trim(" " & l_rstDubcard("TextData"))
                    End Select
                Case 3
                    Select Case l_rstDubcard("SectionPart")
                        Case "left"
                            txtLeft(3).Text = Trim(" " & l_rstDubcard("TextData"))
                        Case "right"
                            txtRight(3).Text = Trim(" " & l_rstDubcard("TextData"))
                        Case "head"
                            txtHead(3).Text = Trim(" " & l_rstDubcard("TextData"))
                    End Select
            End Select
            l_rstDubcard.MoveNext
        Loop
    End If
    l_rstDubcard.Close
    Set l_rstDubcard = Nothing
    
End If

If chkAutoProcess.Value <> 0 Then
    chkAutoProcess.Value = 0
    cmdSaveAndProcess.Value = True
End If

End Sub

Private Sub Form_Load()

CenterForm Me

End Sub

Private Function SaveDubcard() As Boolean

Dim l_strSQL As String, l_lngSectionNumber As Long, l_lngSectionPart As Long

If lbltracker_itemID.Caption = "" Then Exit Function

For l_lngSectionNumber = 1 To 3

    For l_lngSectionPart = 1 To 3
    
        Select Case l_lngSectionPart
            Case 1 'head
                If Trim(" " & GetDataSQL("SELECT TOP 1 tracker_dubcard_ID FROM tracker_dubcard WHERE tracker_itemID = " & lbltracker_itemID.Caption & " AND Language = '" & QuoteSanitise(cmbLanguage.Text) & "' AND SectionNumber = " & l_lngSectionNumber & " AND SectionPart = 'head'")) <> "" Then
                    If txtHead(l_lngSectionNumber).Text <> "" Then
                        l_strSQL = "UPDATE tracker_Dubcard SET TextData = '" & QuoteSanitise(Trim(txtHead(l_lngSectionNumber).Text)) & "' WHERE tracker_itemID = " & lbltracker_itemID.Caption & " AND language = '" & QuoteSanitise(cmbLanguage.Text) & "' AND SectionNumber = " & l_lngSectionNumber & " AND SectionPart = 'head'"
                    Else
                        l_strSQL = "UPDATE tracker_Dubcard SET TextData = Null WHERE tracker_itemID = " & lbltracker_itemID.Caption & " AND language = '" & QuoteSanitise(cmbLanguage.Text) & "' AND SectionNumber = " & l_lngSectionNumber & " AND SectionPart = 'head'"
                    End If
                Else
                    If txtHead(l_lngSectionNumber).Text <> "" Then
                        l_strSQL = "INSERT INTO tracker_dubcard (tracker_itemID, Language, SectionNumber, SectionPart, TextData, cuser, muser) VALUES ("
                        l_strSQL = l_strSQL & lbltracker_itemID.Caption & ", "
                        l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbLanguage.Text) & "', "
                        l_strSQL = l_strSQL & l_lngSectionNumber & ", "
                        l_strSQL = l_strSQL & "'head', "
                        l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(txtHead(l_lngSectionNumber).Text)) & "', "
                        l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strFullUserName) & "', "
                        l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strFullUserName) & "')"
                    Else
                        l_strSQL = ""
                    End If
                End If
            Case 2 'left
                If Trim(" " & GetDataSQL("SELECT TOP 1 tracker_dubcard_ID FROM tracker_dubcard WHERE tracker_itemID = " & lbltracker_itemID.Caption & " AND Language = '" & QuoteSanitise(cmbLanguage.Text) & "' AND SectionNumber = " & l_lngSectionNumber & " AND SectionPart = 'left'")) <> "" Then
                    If txtLeft(l_lngSectionNumber).Text <> "" Then
                        l_strSQL = "UPDATE tracker_Dubcard SET TextData = '" & QuoteSanitise(Trim(txtLeft(l_lngSectionNumber).Text)) & "' WHERE tracker_itemID = " & lbltracker_itemID.Caption & " AND language = '" & QuoteSanitise(cmbLanguage.Text) & "' AND SectionNumber = " & l_lngSectionNumber & " AND SectionPart = 'left'"
                    Else
                        l_strSQL = "UPDATE tracker_Dubcard SET TextData = Null WHERE tracker_itemID = " & lbltracker_itemID.Caption & " AND language = '" & QuoteSanitise(cmbLanguage.Text) & "' AND SectionNumber = " & l_lngSectionNumber & " AND SectionPart = 'left'"
                    End If
                Else
                    If txtLeft(l_lngSectionNumber).Text <> "" Then
                        l_strSQL = "INSERT INTO tracker_dubcard (tracker_itemID, Language, SectionNumber, SectionPart, TextData, cuser, muser) VALUES ("
                        l_strSQL = l_strSQL & lbltracker_itemID.Caption & ", "
                        l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbLanguage.Text) & "', "
                        l_strSQL = l_strSQL & l_lngSectionNumber & ", "
                        l_strSQL = l_strSQL & "'left', "
                        l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(txtLeft(l_lngSectionNumber).Text)) & "', "
                        l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strFullUserName) & "', "
                        l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strFullUserName) & "')"
                    Else
                        l_strSQL = ""
                    End If
                End If
            Case 3 'right
                If Trim(" " & GetDataSQL("SELECT TOP 1 tracker_dubcard_ID FROM tracker_dubcard WHERE tracker_itemID = " & lbltracker_itemID.Caption & " AND Language = '" & QuoteSanitise(cmbLanguage.Text) & "' AND SectionNumber = " & l_lngSectionNumber & " AND SectionPart = 'right'")) <> "" Then
                    If txtRight(l_lngSectionNumber).Text <> "" Then
                        l_strSQL = "UPDATE tracker_Dubcard SET TextData = '" & QuoteSanitise(Trim(txtRight(l_lngSectionNumber).Text)) & "' WHERE tracker_itemID = " & lbltracker_itemID.Caption & " AND language = '" & QuoteSanitise(cmbLanguage.Text) & "' AND SectionNumber = " & l_lngSectionNumber & " AND SectionPart = 'right'"
                    Else
                        l_strSQL = "UPDATE tracker_Dubcard SET TextData = Null WHERE tracker_itemID = " & lbltracker_itemID.Caption & " AND language = '" & QuoteSanitise(cmbLanguage.Text) & "' AND SectionNumber = " & l_lngSectionNumber & " AND SectionPart = 'right'"
                    End If
                Else
                    If txtRight(l_lngSectionNumber).Text <> "" Then
                        l_strSQL = "INSERT INTO tracker_dubcard (tracker_itemID, Language, SectionNumber, SectionPart, TextData, cuser, muser) VALUES ("
                        l_strSQL = l_strSQL & lbltracker_itemID.Caption & ", "
                        l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbLanguage.Text) & "', "
                        l_strSQL = l_strSQL & l_lngSectionNumber & ", "
                        l_strSQL = l_strSQL & "'right', "
                        l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(txtRight(l_lngSectionNumber).Text)) & "', "
                        l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strFullUserName) & "', "
                        l_strSQL = l_strSQL & "'" & QuoteSanitise(g_strFullUserName) & "')"
                    Else
                        l_strSQL = ""
                    End If
                End If
        End Select
        If l_strSQL <> "" Then
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            If CheckForSQLError Then
                Exit Function
            End If
        End If
    Next
Next

SaveDubcard = True

End Function

Private Function ProcessDubcard() As Boolean

Dim l_strSQL As String, l_lngTrackerID As Long
    
'l_strSQL = "exec CreateDubCard @tracker_itemID=" & lbltracker_itemID.Caption & ", @Language='" & QuoteSanitise(cmbLanguage.Text) & "', @TranscodeRequestID=" & l_lngTrackerID
'Debug.Print l_strSQL
'ExecuteSQL l_strSQL, g_strExecuteError
'CheckForSQLError

l_strSQL = "INSERT INTO transcoderequest (cetauserID, transcodesystem, segmentreference, transcodespecID, Dubcard_Tracker_itemID, Dubcard_Language, leavehidden, overwriteifexisting, status, cetauser, savedate) VALUES ("
l_strSQL = l_strSQL & g_lngUserID & ", "
l_strSQL = l_strSQL & "'Vantage_API', "
If GetData("tracker_itunes_item", "iTunesOrderType", "tracker_itunes_itemID", lbltracker_itemID.Caption) = "Disney Dub Cards" Then
    l_strSQL = l_strSQL & "'" & Replace(UCase(QuoteSanitise(GetData("tracker_itunes_item", "Title", "tracker_itunes_itemID", lbltracker_itemID.Caption))), " ", "_") & "_"
    l_strSQL = l_strSQL & UCase(QuoteSanitise(GetData("tracker_itunes_item", "Episode", "tracker_itunes_itemID", lbltracker_itemID.Caption))) & "-" & QuoteSanitise(cmbLanguage.Text) & "-Dubcard', "
Else
    l_strSQL = l_strSQL & "'" & UCase(QuoteSanitise(Mid(frmTrackeriTunes.grdItems.Columns("EIDR").Text, InStr(frmTrackeriTunes.grdItems.Columns("EIDR").Text, "/") + 1) & "-" & QuoteSanitise(cmbLanguage.Text) & "-Dubcard")) & "', "
End If
l_strSQL = l_strSQL & GetDataSQL("SELECT transcodespecID FROM transcodespec WHERE transcodename = 'Disney Dub Card Construction 23.98fps' AND transcodesystem = 'VANTAGE_API'") & ", "
l_strSQL = l_strSQL & lbltracker_itemID.Caption & ", "
l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbLanguage.Text) & "', "
l_strSQL = l_strSQL & "1, 1, (SELECT transcodestatusID FROM transcodestatus WHERE description = 'Requested'), "
l_strSQL = l_strSQL & "'" & g_strUserInitials & "', getdate());"

Debug.Print l_strSQL
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError
l_lngTrackerID = g_lngLastID

ProcessDubcard = True

End Function

