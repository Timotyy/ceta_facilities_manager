Attribute VB_Name = "modFunctions"
Option Explicit

' Declares for querying Windows version
Declare Function SHGetSpecialFolderLocation Lib "Shell32.dll" _
  (ByVal hwndOwner As Long, ByVal nFolder As Long, _
  pidl As ITEMIDLIST) As Long

Declare Function SHGetPathFromIDList Lib "Shell32.dll" _
  Alias "SHGetPathFromIDListA" (ByVal pidl As Long, _
  ByVal pszPath As String) As Long

Public Type SH_ITEMID
    cb As Long
    abID As Byte
End Type

Public Type ITEMIDLIST
    mkid As SH_ITEMID
End Type

Public Type iTunes_Structure
    Provider As String
    Subtype As String
    VendorID As String
    Language As String
    Rating As String
    OriginalLocale As String
    EpisodeProductionNumber As String
    Title As String
    Studio_Release_Title As String
    ContainerID As String
    ContainerPosition As String
    ReleaseDate As String
    Copyright As String
    Synopsis As String
    PreviewStartTime As String
    Territory As String
    SalesStartDate As String
    ClearedForSale As String
End Type

Public Type DADC_Structure
    CVCode As String
    AlphaKey As String
    KitID As String
End Type

Public Const MAX_PATH As Integer = 260

Const VER_PLATFORM_WIN32s = 0          'Win32s on Windows 3.1
Const VER_PLATFORM_WIN32_WINDOWS = 1   'Win32 on Windows 95
Const VER_PLATFORM_WIN32_NT = 2        'Win32 on Windows NT

Type OSVERSIONINFO
   dwOSVersionInfoSize As Long
   dwMajorVersion As Long
   dwMinorVersion As Long
   dwBuildNumber As Long
   dwPlatformId As Long
   szCSDVersion As String * 128
End Type

Private Declare Function GetVersionEx Lib "kernel32" _
   Alias "GetVersionExA" (lpVersionInformation As OSVERSIONINFO) As Long

' Declare for Registry functions

Const HKEY_CLASSES_ROOT = &H80000000
Const HKEY_CURRENT_USER = &H80000001
Const HKEY_LOCAL_MACHINE = &H80000002
Const HKEY_USERS = &H80000003
Const HKEY_PERFORMANCE_DATA = &H80000004
Const HKEY_CURRENT_CONFIG = &H80000005
Const HKEY_DYN_DATA = &H80000006

Private Declare Function RegCloseKey Lib "advapi32.dll" _
  (ByVal hKey As Long) As Long
  
Private Declare Function RegOpenKeyEx Lib "advapi32.dll" _
   Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey _
   As String, ByVal ulOptions As Long, ByVal samDesired _
   As Long, phkResult As Long) As Long
   
Private Declare Function RegQueryValue Lib "advapi32.dll" Alias _
  "RegQueryValueA" (ByVal hKey As Long, ByVal lpSubKey As _
   String, ByVal lpValue As String, lpcbValue As Long) As Long

' Note that if you declare lpData as String, then it is
' necessary to pass it with ByVal
Private Declare Function RegQueryValueEx Lib "advapi32.dll" _
  Alias "RegQueryValueExA" (ByVal hKey As Long, _
   ByVal lpValueName As String, ByVal lpReserved As Long, _
   lpType As Long, lpData As Any, lpcbData As Long) As Long
  
   Private Declare Function RegEnumKey Lib "advapi32.dll" _
   Alias "RegEnumKeyA" (ByVal hKey As Long, ByVal dwIndex _
   As Long, ByVal lpName As String, ByVal cbName As Long) _
   As Long

Private Declare Function RegEnumValue Lib "advapi32.dll" _
   Alias "RegEnumValueA" (ByVal hKey As Long, ByVal dwIndex _
   As Long, ByVal lpValueName As String, lpcbValueName _
   As Long, ByVal lpReserved As Long, lpType As Long, _
   ByVal lpData As String, lpcbData As Long) As Long
   
Private Declare Function RegOpenKey Lib "advapi32.dll" _
  Alias "RegOpenKeyA" (ByVal hKey As Long, _
  ByVal lpSubKey As String, phkResult As Long) As Long

Private Declare Function GetComputerName Lib "kernel32" _
   Alias "GetComputerNameA" (ByVal lpBuffer As String, _
   nSize As Long) As Long

Private Declare Function WNetGetConnection Lib _
   "mpr.dll" Alias "WNetGetConnectionA" (ByVal lpszLocalName _
   As String, ByVal lpszRemoteName As String, _
   cbRemoteName As Long) As Long

Private Const CB_FINDSTRING = &H14C
Private Const CB_ERR = -1

'API functions
Public Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Public Declare Function ShellExecute Lib "Shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

'used when checking if job is safe to send to accounts
Enum JobSafeForPostingLevel
    vbuJobSafe = -1
    vbuJobNotSafeCostingRows = 2
    vbuJobNotSafeCompanyAccountCode = 3
    vbuJobNotSafeNoPONumber = 4
    vbuJobNotSafePOTooLong = 5
    vbuJobNotSafeSAPcode = 6
    vbJobNotSafeMediaPulseProfiles = 7
End Enum

'Constants for topmost.
Public Const HWND_TOPMOST = -1
Public Const HWND_NOTOPMOST = -2
Public Const SWP_NOMOVE = &H2
Public Const SWP_NOSIZE = &H1
Public Const SWP_NOACTIVATE = &H10
Public Const SWP_SHOWWINDOW = &H40
Public Const Flags = SWP_NOMOVE Or SWP_NOSIZE

Public Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, ByVal CX As Long, ByVal CY As Long, ByVal wFlags As Long) As Long
Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long

Private Const MAX_COMPUTERNAME_LENGTH As Long = 15&

Private Const SWP_FRAMECHANGED = &H20
Private Const SWP_SHOWME = SWP_FRAMECHANGED Or SWP_NOMOVE Or SWP_NOSIZE

Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" _
            (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Public Const GWL_EXSTYLE = (-20)
Public Const WS_EX_TRANSPARENT = &H20&

Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" ( _
  ByVal hWnd As Long, _
  ByVal nIndex As Long _
) As Long

Declare Sub Sleep Lib "kernel32" (ByVal milliseconds As Long)

Private Const WS_EX_CLIENTEDGE = &H200
Private Const WS_EX_STATICEDGE = &H20000

Private Const SWP_NOOWNERZORDER = &H200
Private Const SWP_NOZORDER = &H4

'chris tate-davies - my public variables
Public g_CurrentlySelectedEventID As Long

' The Desktop - virtual folder
Public Const CSIDL_DESKTOP = &H0
' Program Files
Public Const CSIDL_PROGRAMS = 2
' Control Panel - virtual folder
Public Const CSIDL_CONTROLS = 3
' Printers - virtual folder
Public Const CSIDL_PRINTERS = 4
' My Documents
Public Const CSIDL_DOCUMENTS = 5
' Favorites
Public Const CSIDL_FAVORITES = 6
' Startup Folder
Public Const CSIDL_STARTUP = 7
' Recent Documents
Public Const CSIDL_RECENT = 8
' Send To Folder
Public Const CSIDL_SENDTO = 9
' Recycle Bin - virtual folder
Public Const CSIDL_BITBUCKET = 10
' Start Menu
Public Const CSIDL_STARTMENU = 11
' Desktop folder
Public Const CSIDL_DESKTOPFOLDER = 16
' My Computer - virtual folder
Public Const CSIDL_DRIVES = 17
' Network Neighbourhood - virtual folder
Public Const CSIDL_NETWORK = 18
' NetHood Folder
Public Const CSIDL_NETHOOD = 19
' Fonts folder
Public Const CSIDL_FONTS = 20
' ShellNew folder
Public Const CSIDL_SHELLNEW = 21

Private Type STARTUPINFO
   cb As Long
   lpReserved As String
   lpDesktop As String
   lpTitle As String
   dwX As Long
   dwY As Long
   dwXSize As Long
   dwYSize As Long
   dwXCountChars As Long
   dwYCountChars As Long
   dwFillAttribute As Long
   dwFlags As Long
   wShowWindow As Integer
   cbReserved2 As Integer
   lpReserved2 As Long
   hStdInput As Long
   hStdOutput As Long
   hStdError As Long
End Type

Private Type PROCESS_INFORMATION
   hProcess As Long
   hThread As Long
   dwProcessID As Long
   dwThreadId As Long
End Type

Private Declare Function GetExitCodeProcess Lib "kernel32" _
   (ByVal hProcess As Long, lpExitCode As Long) As Long

Private Const NORMAL_PRIORITY_CLASS = &H20&
Private Const INFINITE = -1&

Private Declare Function CreateProcessA Lib "kernel32" (ByVal _
   lpApplicationName As String, ByVal lpCommandLine As String, ByVal _
   lpProcessAttributes As Long, ByVal lpThreadAttributes As Long, _
   ByVal bInheritHandles As Long, ByVal dwCreationFlags As Long, _
   ByVal lpEnvironment As Long, ByVal lpCurrentDirectory As String, _
   lpStartupInfo As STARTUPINFO, lpProcessInformation As _
   PROCESS_INFORMATION) As Long

Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal _
   hHandle As Long, ByVal dwMilliseconds As Long) As Long

Private Declare Function CloseHandle Lib "kernel32" ( _
    ByVal hObject As Long) As Long

'EASendmail Constants
Const ConnectNormal = 0
Const ConnectSSLAuto = 1
Const ConnectSTARTTLS = 2
Const ConnectDirectSSL = 3
Const ConnectTryTLS = 4

Function ObjectExists(ByVal l_Class$) As Integer

On Error Resume Next

'check to see if the class is open
Dim l_objObject As Object
Set l_objObject = GetObject(, l_Class$)

'setup the flag
ObjectExists = True

'if the class is not open
If Err > 0 And Err = 429 Then
    'reset the flag
    ObjectExists = False

    'stop it
    Exit Function
End If

End Function

Public Function SendOutlookEmail(ByVal lp_strEmailTo As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, Optional lp_strCCAddress As String, Optional lp_strBCCAddress As String, Optional lp_blnRawText As Boolean, Optional lp_strFromAddress As String)

'Dim oOApp As Outlook.Application
'Dim oOMail As Outlook.MailItem
'
'Set oOApp = CreateObject("Outlook.Application")
'Set oOMail = oOApp.CreateItem(olMailItem)
'
'With oOMail
'    .To = lp_strEmailTo
'    .Subject = lp_strSubject
'    If lp_blnRawText = True Then
'        .BodyFormat = olFormatPlain
'        .Body = lp_strBody
'    Else
'        .BodyFormat = olFormatHTML
'        .HTMLBody = lp_strBody
'    End If
'    .CC = lp_strCCAddress
'    .BCC = lp_strBCCAddress
'    If lp_strFromAddress <> "" Then .ReplyRecipients.Add lp_strFromAddress
'    If lp_strAttachment <> "" Then .Attachments.Add lp_strAttachment
'    .Display
'End With

Dim l_strSQL As String, l_lngMessageID As Long

l_strSQL = "INSERT INTO emailmessage (emailTO, emailCC, emailBCC, emailsubject, emailbodyhtml, emailbodyplain, cdate, cuser, mdate, muser, emailAttachmentPath, emailfrom) VALUES ("

l_strSQL = l_strSQL & "'" & lp_strEmailTo & "', "
If lp_strCCAddress <> "" Then l_strSQL = l_strSQL & "'" & lp_strCCAddress & "', " Else l_strSQL = l_strSQL & "NULL, "
If lp_strBCCAddress <> "" Then l_strSQL = l_strSQL & "'" & lp_strBCCAddress & "', " Else l_strSQL = l_strSQL & "NULL, "
l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strSubject) & "', "
If lp_blnRawText = False Then
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strBody) & "', NULL, "
Else
    l_strSQL = l_strSQL & "NULL, '" & QuoteSanitise(lp_strBody) & "', "
End If

l_strSQL = l_strSQL & "getdate(), '" & g_strUserInitials & "', getdate(), '" & g_strUserInitials & "', "
If lp_strAttachment <> "" Then
    l_strSQL = l_strSQL & "'" & lp_strAttachment & "', "
Else
    l_strSQL = l_strSQL & "NULL, "
End If
If lp_strFromAddress <> "" Then
    l_strSQL = l_strSQL & "'" & lp_strFromAddress & "');"
Else
    l_strSQL = l_strSQL & "'" & g_strUserEmailAddress & "');"
End If

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_lngMessageID = g_lngLastID

Shell App.Path & "\CetaMail.exe " & l_lngMessageID, vbNormalFocus

End Function

Public Function ShowBBCAliasList()
If Not CheckAccess("/showbbcaliaslist") Then Exit Function

frmBBCAlias.Show

End Function

Public Function ShowBBCTapeRenumberLookup()

If Not CheckAccess("/showbbctaperenumberlookup") Then Exit Function
frmBBCTapeRenumberLookup.Show

End Function

Public Function fGetSpecialFolder(CSIDL As Long) As String
    Dim sPath As String
    Dim IDL As ITEMIDLIST
    '
    ' Retrieve info about system folders such as the
    ' "Desktop" folder. Info is stored in the IDL structure.
    '
    fGetSpecialFolder = ""
    If SHGetSpecialFolderLocation(MDIForm1.hWnd, CSIDL, IDL) = 0 Then
        '
        ' Get the path from the ID list, and return the folder.
        '
        sPath = Space$(MAX_PATH)
        If SHGetPathFromIDList(ByVal IDL.mkid.cb, ByVal sPath) Then
            fGetSpecialFolder = Left$(sPath, InStr(sPath, vbNullChar) - 1) & "\"
        End If
    End If
End Function

Sub CheckPATDate(lp_lngResourceID As Long)

'CheckPATDate lp_lngResourceID

Dim l_datPATDate As Date
Dim l_intMsg As Integer

l_datPATDate = GetData("resource", "patdate", "resourceID", lp_lngResourceID)



'no date at all
If Not IsDate(l_datPATDate) Or l_datPATDate = "00:00:00" Then
    
    l_datPATDate = Date
    
    l_intMsg = MsgBox("There is no PAT date specified for this resource. Do you want to specify it now?", vbYesNo + vbExclamation)
    If l_intMsg = vbYes Then
        GoTo PROC_SpecifyDate
    Else
        GoTo PROC_Quit
    End If
    
End If

'date has expired
If DateDiff("y", l_datPATDate, Date) > 1 Then
    l_intMsg = MsgBox("The PAT date specified for this resource as expired. Do you want to specify a new one now?", vbYesNo + vbExclamation)
    If l_intMsg = vbYes Then
        GoTo PROC_SpecifyDate
    Else
        GoTo PROC_Quit
    End If
End If

'quit
GoTo PROC_Quit



PROC_SpecifyDate:

Dim l_datNewPatDate As Variant
l_datNewPatDate = GetDate(l_datPATDate)

If IsDate(l_datNewPatDate) Then
    SetData "resource", "patdate", "resourceID", lp_lngResourceID, FormatSQLDate(l_datNewPatDate)
    CreateResourceHistory lp_lngResourceID, "PAT Date Changed", "Date updated from: " & GetData("resource", "patdate", "resourceID", lp_lngResourceID) & " to " & l_datNewPatDate
End If

PROC_Quit:


Exit Sub

End Sub

Sub ShowResourceHistory(ByVal lp_lngResourceID As Long)

With frmResourceHistory

    .adoResourceHistory.ConnectionString = g_strConnection
    .adoResourceHistory.RecordSource = "SELECT cdate AS 'Date', cuser AS 'User', logtype AS 'Type', description AS 'Description' FROM resourcehistory WHERE resourceID = '" & lp_lngResourceID & "' ORDER BY cdate;"
    .adoResourceHistory.Refresh
    
    .grdHistory.Columns(0).Width = 1200
    .grdHistory.Columns(1).Width = 800
    .grdHistory.Columns(2).Width = 2000
    .grdHistory.Columns(3).Width = 4000
    
    .grdHistory.RowHeight = 660

    CenterForm frmResourceHistory

    .Show vbModal
    
End With

Unload frmResourceHistory
Set frmResourceHistory = Nothing


End Sub


Function IsOverHeadCode(ByVal lp_strCodeToCheck As String) As Boolean

Dim l_strSQL As String
l_strSQL = "SELECT count(xrefID) FROM xref WHERE category LIKE 'overhead%' AND description = '" & lp_strCodeToCheck & "';"

Dim l_intCount As Integer
l_intCount = Val(GetDataSQL(l_strSQL))

If l_intCount > 0 Then
    IsOverHeadCode = True
Else
    IsOverHeadCode = False
End If

End Function

Sub IsInsuranceValidMessage(ByVal lp_lngCompanyID As Long)

    If g_optShowCompanyInsuranceOutDate = 0 Then Exit Sub

    Dim l_datInsuranceDate As Date
    Dim l_strMsg As String
    
    l_datInsuranceDate = GetData("company", "insuranceoutdate", "companyID", lp_lngCompanyID)
    
    If l_datInsuranceDate = 0 Then
        l_strMsg = "There is no specified insurance date for this company."
    ElseIf IsDate(l_datInsuranceDate) Then
        If DateDiff("d", l_datInsuranceDate, Date) > 0 Then
            l_strMsg = "The insurance for this company has expired (" & l_datInsuranceDate & ")."
        End If
    Else
        l_strMsg = "There is no specified insurance date for this company."
    End If
    
    If l_strMsg <> "" Then MsgBox l_strMsg, vbExclamation, "Insurance Problem"
    

End Sub



Sub CreateResourceHistory(ByVal lp_lngResourceID As Long, ByVal lp_strLogType As String, ByVal lp_strDescription As String)

'resourcehistoryID  int(11)       (NULL)             NO      PRI     (NULL)   auto_increment  select,insert,update,references
'resourceID         int(11)       (NULL)             YES             (NULL)                   select,insert,update,references
'cdate              datetime      (NULL)             YES             (NULL)                   select,insert,update,references
'cuser              varchar(10)   latin1_swedish_ci  YES             (NULL)                   select,insert,update,references
'logtype            varchar(50)   latin1_swedish_ci  YES             (NULL)                   select,insert,update,references
'description        text          latin1_swedish_ci  YES             (NULL)                   select,insert,update,references
'counter01          double        (NULL)             YES             (NULL)                   select,insert,update,references
'counter02          double        (NULL)             YES             (NULL)                   select,insert,update,references
'counter03          double        (NULL)             YES             (NULL)                   select,insert,update,references
'counter04          double        (NULL)             YES             (NULL)                   select,insert,update,references
'text01             varchar(255)  latin1_swedish_ci  YES             (NULL)                   select,insert,update,references
'text02             varchar(255)  latin1_swedish_ci  YES             (NULL)                   select,insert,update,references
'text03             varchar(255)  latin1_swedish_ci  YES             (NULL)                   select,insert,update,references
'text04             varchar(255)  latin1_swedish_ci  YES             (NULL)                   select,insert,update,references

Dim l_strSQL As String
l_strSQL = "INSERT INTO resourcehistory (resourceID, cdate, cuser, logtype, description) VALUES"
l_strSQL = l_strSQL & "('" & lp_lngResourceID & "', '" & FormatSQLDate(Now) & "', '" & g_strUserInitials & "', '" & QuoteSanitise(lp_strLogType) & "', '" & QuoteSanitise(lp_strDescription) & "');"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError



End Sub

Sub ApplyBookedTimesToMasterJob(lp_lngMasterJobID As Long, lp_strStartTime As String, lp_strEndTime As String)

'sam / feb 2006
'this routine changes just the times on all jobs which are part of a master job (ie. have been repeated).


Dim l_strSQL As String

l_strSQL = "SELECT jobID, startdate, enddate FROM job WHERE jobID = " & lp_lngMasterJobID & " OR masterjobID = " & lp_lngMasterJobID

Dim l_rstJobs As New ADODB.Recordset
Dim l_conJobs As New ADODB.Connection

l_conJobs.Open g_strConnection
l_rstJobs.Open l_strSQL, l_conJobs, adOpenDynamic, adLockReadOnly

If Not l_rstJobs.EOF Then l_rstJobs.MoveFirst

Dim l_lngJobID As Long, l_datStartTime As Date, l_datEndTime As Date

Do While Not l_rstJobs.EOF

    l_lngJobID = l_rstJobs("jobID")

    l_datStartTime = Format(l_rstJobs("startdate"), vbShortDateFormat) & " " & lp_strStartTime
    l_datEndTime = Format(l_rstJobs("enddate"), vbShortDateFormat) & " " & lp_strEndTime
    
    'save the booked times
    ApplyBookedTimes l_lngJobID, l_datStartTime, l_datEndTime

    l_rstJobs.MoveNext

Loop

l_rstJobs.Close
Set l_rstJobs = Nothing

l_conJobs.Close
Set l_conJobs = Nothing

MsgBox "Repeat update complete", vbInformation

End Sub

Function FindMasterJobID(lp_lngJobID As Long) As Long

'sam/feb 2006
'this function checks to find out if this job is part of, or is the master record of a repeat.

Dim l_strSQL As String, l_lngMasterJobID As Long

l_strSQL = "SELECT masterjobID FROM job WHERE (jobID = " & lp_lngJobID & " OR masterjobID = " & lp_lngJobID & ") AND (masterjobID IS NOT NULL) AND (masterjobID <> 0)"

Dim l_rstJobs As New ADODB.Recordset
Dim l_conJobs As New ADODB.Connection

l_conJobs.Open g_strConnection
l_rstJobs.Open l_strSQL, l_conJobs, adOpenDynamic, adLockReadOnly

If Not l_rstJobs.EOF Then
    l_rstJobs.MoveFirst
    l_lngMasterJobID = l_rstJobs("masterjobID")
Else
    l_lngMasterJobID = lp_lngJobID
End If

l_rstJobs.Close
Set l_rstJobs = Nothing

l_conJobs.Close
Set l_conJobs = Nothing


FindMasterJobID = l_lngMasterJobID



End Function



Function GetDimension(ByVal lp_strNominalCode As String) As String

Dim l_strDimension As String
l_strDimension = GetDataSQL("SELECT TOP 1 information FROM xref WHERE category = 'dimensionsforaccounts' AND description = '" & lp_strNominalCode & "';")

If l_strDimension = "" Then l_strDimension = "UNKNOWN"

GetDimension = l_strDimension

End Function

Function GetDate(Optional lp_datDefaultDate As Date) As Variant

'this function pops up a calendar form and returns a date (selected by the user)

If IsDate(lp_datDefaultDate) Then
    frmCalendar.datCalendar.Value = lp_datDefaultDate
Else
    frmCalendar.datCalendar.Value = Now
End If

frmCalendar.Show vbModal

If frmCalendar.Tag = "CANCELLED" Then
    GetDate = ""
Else
    GetDate = frmCalendar.datCalendar.Value
End If

Unload frmCalendar
Set frmCalendar = Nothing

End Function

Sub ResizeFontAndPrint(lp_strTextToPrint As String, lp_conControlToOutput As Control, lp_sngLineWidth As Single, lp_sngLineHeight As Single, lp_intJustify As Integer, lp_sngCurrentX As Single, lp_sngCurrentY As Single)

Dim l_strTextToPrint    As String
Dim l_sngTextWidth      As Single
Dim l_sngTextHeight     As Single
Dim l_sngLabelWidth     As Single
Dim l_sngLabelHeight    As Single
Dim l_blnExit           As Boolean

'pick up the label width
l_sngLabelWidth = lp_sngLineWidth
l_sngLabelHeight = lp_sngLineHeight

'pick up the required text
l_strTextToPrint = lp_strTextToPrint

'get the text width with the current font
l_sngTextWidth = lp_conControlToOutput.TextWidth(l_strTextToPrint)
l_sngTextHeight = lp_conControlToOutput.TextHeight(l_strTextToPrint)

lp_conControlToOutput.FontSize = 10

'loop around until it fits
Do Until l_blnExit = True
    
    'check for the height first
    If l_sngTextHeight > l_sngLabelHeight - 5 Then
        lp_conControlToOutput.FontSize = lp_conControlToOutput.FontSize - 0.5
    Else
        
        If l_sngTextWidth > l_sngLabelWidth Then
            'make the fontsize a bit smaller
            lp_conControlToOutput.FontSize = lp_conControlToOutput.FontSize - 0.5
            
        ElseIf l_sngTextWidth < l_sngLabelWidth Then
            'make the fontsize a bit bigger
            lp_conControlToOutput.FontSize = lp_conControlToOutput.FontSize + 0.5
        End If
    End If
    
    'get the text width with the current font
    l_sngTextWidth = lp_conControlToOutput.TextWidth(l_strTextToPrint)
    l_sngTextHeight = lp_conControlToOutput.TextHeight(l_strTextToPrint)

    
    'check to see if we are done
    If (l_sngTextWidth <= (l_sngLabelWidth)) Or lp_conControlToOutput.FontSize < 2 Then
        'quit out of the loop as it fits
        l_blnExit = True
        Exit Do
    End If
    
Loop

'where should it print
Select Case lp_intJustify
Case vbCenter
    lp_conControlToOutput.CurrentX = lp_sngCurrentX + ((l_sngLabelWidth / 2) - (l_sngTextWidth / 2))
    
Case vbLeftJustify
    lp_conControlToOutput.CurrentX = lp_sngCurrentX + 30

Case vbRightJustify
    lp_conControlToOutput.CurrentX = lp_sngCurrentX + (l_sngLabelWidth - 30 - l_sngTextWidth)

End Select
 
'get the right height
lp_conControlToOutput.CurrentY = lp_sngCurrentY
 
'enter the text
lp_conControlToOutput.Print l_strTextToPrint

End Sub



Function GenerateRandomKey(lp_intLengthOfString As Integer) As String

    Dim l_lngRandom As Long
    Dim l_strRandom As String
    
    Randomize
    
    Do Until Len(l_strRandom) = lp_intLengthOfString
        
        l_lngRandom = (127 * Rnd)
        
        If l_lngRandom > 64 And l_lngRandom < 90 Then
            l_strRandom = l_strRandom & Chr(l_lngRandom)
        ElseIf l_lngRandom > 97 And l_lngRandom < 122 Then
            l_strRandom = l_strRandom & Chr(l_lngRandom)
        ElseIf l_lngRandom > 47 And l_lngRandom < 58 Then
            l_strRandom = l_strRandom & Chr(l_lngRandom)
        End If
        
    Loop
    
    GenerateRandomKey = l_strRandom

End Function

Function DuplicateResourceScheduleForJob(lp_lngSourceJobID As Long, lp_lngDestinationJobID As Long, lp_lngRepeatBatchID As Long)


'copy the info from one job to another
Dim l_rstOriginalJob As ADODB.Recordset
Dim l_rstNewJob As ADODB.Recordset


Dim l_datStartTime As Date
Dim l_datEndTime As Date

Dim l_datStartDate As Date
Dim l_datEndDate As Date


l_datStartDate = GetData("job", "startdate", "jobID", lp_lngDestinationJobID)
l_datEndDate = GetData("job", "enddate", "jobID", lp_lngDestinationJobID)

'get the original job's details
Dim l_strSQL As String
l_strSQL = "SELECT * FROM resourceschedule WHERE jobID = '" & lp_lngSourceJobID & "';"

Set l_rstOriginalJob = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

'allow adding of a new job
l_strSQL = "SELECT * FROM resourceschedule WHERE jobID = '-1';"

Set l_rstNewJob = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

Dim l_intLoop As Integer
Dim l_lngConflict As Long
    
Do While Not l_rstOriginalJob.EOF
    

    
    l_datStartTime = Format(l_datStartDate, "dd/mm/yyyy") & " " & Format(l_rstOriginalJob("starttime"), "HH:NN")
    l_datEndTime = Format(l_datEndDate, "dd/mm/yyyy") & " " & Format(l_rstOriginalJob("endtime"), "HH:NN")


    If Not IsGeneric(l_rstOriginalJob("resourceID")) Then
        l_lngConflict = IsConflict(lp_lngDestinationJobID, l_rstOriginalJob("resourceID"), l_datStartTime, l_datEndTime, True)
    
        'If IsStaff(lp_lngResourceID) Then
            Dim l_strHoliday As String
            l_strHoliday = IsHoliday(l_rstOriginalJob("resourceID"), Format(l_datStartTime, "dd/mm/yyyy"), Format(l_datEndTime, "dd/mmm/yyyy"))
            If l_strHoliday <> "" Then
                MsgBox "This person (" & GetData("resource", "name", "resourceID", l_rstOriginalJob("resourceID")) & ") is marked as " & l_strHoliday & " during this period (starting @ " & l_datStartTime & ")." & vbCrLf & vbCrLf & "At this point, CFM will only warn you about this! Please adjust your bookings manually!!", vbExclamation
                'CreateNewResourceSchedule = False
                'Exit Function
            End If
        'End If
    
    End If
        
    l_rstNewJob.AddNew
    
    l_rstNewJob("jobID") = lp_lngDestinationJobID
    
    For l_intLoop = 0 To l_rstOriginalJob.Fields.Count - 1
        If l_rstOriginalJob.Fields(l_intLoop).Name <> "jobID" And l_rstOriginalJob.Fields(l_intLoop).Name <> "resourcescheduleID" Then
        
            l_rstNewJob(l_intLoop) = l_rstOriginalJob(l_intLoop)
        
        End If
    Next
    
    'blank out fields which are specific to the ORIGINAL job only
    
    
    l_rstNewJob("repeatbatchID") = lp_lngRepeatBatchID
    l_rstNewJob("starttime") = l_datStartTime
    l_rstNewJob("endtime") = l_datEndTime
    
    l_rstNewJob("createddate") = FormatSQLDate(Now)
    l_rstNewJob("createduser") = g_strUserInitials
    
    l_rstNewJob("modifieduser") = Null
    l_rstNewJob("modifieddate") = Null
    
    l_rstNewJob("actualstarttime") = Null
    l_rstNewJob("actualendtime") = Null
    l_rstNewJob("actualhoursused") = Null
    
    l_rstNewJob("conflictingresourcescheduleID") = l_lngConflict
    
    l_rstNewJob("fd_status") = "Repeat"
    
    l_rstNewJob.Update
    
    SetData "job", "flagrepeatconflict", "jobID", lp_lngDestinationJobID, l_lngConflict

    l_rstOriginalJob.MoveNext

Loop

'MsgBox g_lngLastID, vbExclamation

'close the original record
l_rstOriginalJob.Close
Set l_rstOriginalJob = Nothing

'close the new record
l_rstNewJob.Close
Set l_rstNewJob = Nothing



End Function



Sub SplitResourceSchedule(lp_lngResourceScheduleID As Long)

If Not CheckAccess("/splitresourceschedule") Then Exit Sub

frmSplitResourceSchedule.lblResourceScheduleID.Caption = lp_lngResourceScheduleID


End Sub

Sub ShowHolidayForResource(lp_lngResourceID As Long, lp_datCalendarDate As Date)

If Not CheckAccess("/showholiday") Then Exit Sub

frmHumanResource.datDateSelect.Value = lp_datCalendarDate
frmHumanResource.txtResourceID.Text = lp_lngResourceID
frmHumanResource.txtResourceName.Text = GetData("resource", "name", "resourceID", lp_lngResourceID)
frmHumanResource.cmdLoadHoliday.Value = True
frmHumanResource.Show 1
Unload frmHumanResource
Set frmHumanResource = Nothing
End Sub

Function NewMsgBox(ByVal lp_strTitle As String, ByVal lp_strQuestion As String, ByVal lp_strButton1 As String, ByVal lp_strButton2 As String, ByVal lp_strButton3 As String) As Integer

frmNewMsgBox.Caption = lp_strTitle
frmNewMsgBox.lblCaption.Caption = lp_strQuestion

frmNewMsgBox.cmdResponse(0).Caption = lp_strButton1
frmNewMsgBox.cmdResponse(1).Caption = lp_strButton2
frmNewMsgBox.cmdResponse(2).Caption = lp_strButton3

frmNewMsgBox.cmdResponse(0).Visible = IIf(lp_strButton1 = "", False, True)
frmNewMsgBox.cmdResponse(1).Visible = IIf(lp_strButton2 = "", False, True)
frmNewMsgBox.cmdResponse(2).Visible = IIf(lp_strButton3 = "", False, True)

frmNewMsgBox.Show vbModal
    
NewMsgBox = frmNewMsgBox.Tag

Unload frmNewMsgBox
Set frmNewMsgBox = Nothing

End Function

Sub SaveColumnLayout(lp_grdDataGridName As SSOleDBGrid, lp_strGridLayoutName As String, Optional lp_blnSaveAsDefault As Boolean)

Exit Sub

Dim l_strSQL As String
Dim l_intColumns As Integer

'delete any existing column layouts

If lp_blnSaveAsDefault = True Then
    l_strSQL = "DELETE FROM columnlayout WHERE layoutname = '" & lp_strGridLayoutName & "' AND cuser = 'DEFAULT';"
Else
    l_strSQL = "DELETE FROM columnlayout WHERE layoutname = '" & lp_strGridLayoutName & "' AND cuser = '" & g_strUserInitials & "';"
End If
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

'loop through each column and add a record for each
For l_intColumns = 0 To lp_grdDataGridName.Columns.Count - 1
    
    l_strSQL = "INSERT INTO columnlayout (layoutname, columnnumber, columnname, columncaption, columnwidth, columndatafield, columndatatype, columnformat, columnbackcolour, columnforecolour, columnvisible, cuser) VALUES ("
    l_strSQL = l_strSQL & "'" & lp_strGridLayoutName & "', "
    l_strSQL = l_strSQL & "'" & l_intColumns & "', "
    l_strSQL = l_strSQL & "'" & lp_grdDataGridName.Columns(l_intColumns).Name & "', "
    l_strSQL = l_strSQL & "'" & lp_grdDataGridName.Columns(l_intColumns).Caption & "', "
    l_strSQL = l_strSQL & "'" & lp_grdDataGridName.Columns(l_intColumns).Width & "', "
    l_strSQL = l_strSQL & "'" & lp_grdDataGridName.Columns(l_intColumns).DataField & "', "
    l_strSQL = l_strSQL & "'" & lp_grdDataGridName.Columns(l_intColumns).DataType & "', "
    l_strSQL = l_strSQL & "'" & lp_grdDataGridName.Columns(l_intColumns).NumberFormat & "', "
    l_strSQL = l_strSQL & "'" & lp_grdDataGridName.Columns(l_intColumns).BackColor & "', "
    l_strSQL = l_strSQL & "'" & lp_grdDataGridName.Columns(l_intColumns).ForeColor & "', "
    l_strSQL = l_strSQL & "'" & lp_grdDataGridName.Columns(l_intColumns).Visible & "', "
    If lp_blnSaveAsDefault = True Then
        l_strSQL = l_strSQL & "'DEFAULT');"
    Else
        l_strSQL = l_strSQL & "'" & g_strUserInitials & "');"
    End If
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
Next

End Sub


Function GetFieldNames(lp_strGridLayoutName As String)

Dim l_strSQL As String

'find any existing column layouts
l_strSQL = "SELECT * FROM columnlayout WHERE layoutname = '" & lp_strGridLayoutName & "' AND cuser = '" & g_strUserInitials & "' AND columnvisible = 'True' ORDER BY columnnumber;"

PROC_Retry:

Dim l_conJobHistory As ADODB.Connection
Dim l_rstJobHistory As ADODB.Recordset
Dim l_strFields As String

Set l_conJobHistory = New ADODB.Connection
Set l_rstJobHistory = New ADODB.Recordset

l_conJobHistory.ConnectionString = g_strConnection
l_conJobHistory.Open

With l_rstJobHistory
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conJobHistory, adOpenDynamic
End With

l_rstJobHistory.ActiveConnection = Nothing

l_conJobHistory.Close
Set l_conJobHistory = Nothing

If Not l_rstJobHistory.EOF Then
    l_rstJobHistory.MoveFirst
Else
    Dim l_intRetry As Integer
    If l_intRetry = 1 Then
        MsgBox "There was a problem loading this grids column layout. Please see your supervisor", vbExclamation
        l_strFields = " * "
        GoTo PROC_Close
        
    End If
    l_strSQL = "SELECT * FROM columnlayout WHERE layoutname = '" & lp_strGridLayoutName & "' AND cuser = 'DEFAULT' ORDER BY columnnumber;"
    l_intRetry = 1
    l_rstJobHistory.Close
    GoTo PROC_Retry
    
End If



Do While Not l_rstJobHistory.EOF
    If l_rstJobHistory("columndatafield") <> "" And l_rstJobHistory("columnvisible") = "True" Then l_strFields = l_strFields & l_rstJobHistory("columndatafield") & ", "
    l_rstJobHistory.MoveNext
Loop

l_strFields = Left(l_strFields, Len(l_strFields) - 2)

PROC_Close:

l_rstJobHistory.Close
Set l_rstJobHistory = Nothing

GetFieldNames = l_strFields
End Function

Sub LoadColumnLayout(lp_grdDataGridName As SSOleDBGrid, lp_strGridLayoutName As String)

'Exit Sub

Dim l_strSQL As String
Dim l_intColumns As Integer

'find any existing column layouts
l_strSQL = "SELECT * FROM columnlayout WHERE layoutname = '" & lp_strGridLayoutName & "' AND cuser = '" & g_strUserInitials & "' AND columnvisible = 'True' ORDER BY columnnumber;"

PROC_Retry:

Dim l_conJobHistory As ADODB.Connection
Dim l_rstJobHistory As ADODB.Recordset

Set l_conJobHistory = New ADODB.Connection
Set l_rstJobHistory = New ADODB.Recordset

l_conJobHistory.ConnectionString = g_strConnection
l_conJobHistory.Open

With l_rstJobHistory
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conJobHistory, adOpenDynamic
End With

l_rstJobHistory.ActiveConnection = Nothing

l_conJobHistory.Close
Set l_conJobHistory = Nothing

If Not l_rstJobHistory.EOF Then
    l_rstJobHistory.MoveFirst
Else
    Dim l_intRetry As Integer
    If l_intRetry = 1 Then
        MsgBox "There was a problem loading this grids column layout. Please see your supervisor", vbExclamation
        Exit Sub
    End If
    l_strSQL = "SELECT * FROM columnlayout WHERE layoutname = '" & lp_strGridLayoutName & "' AND cuser = 'DEFAULT' AND columnvisible = 'True' ORDER BY columnnumber;"
    l_intRetry = 1
    l_rstJobHistory.Close
    GoTo PROC_Retry
    
End If

Dim l_intColumnNumber As Integer

'For l_intColumnNumber = 0 To lp_grdDataGridName.Columns.Count - 1
'    lp_grdDataGridName.Columns(l_intColumnNumber).Name = "COL" & l_intColumnNumber

'Next

DoEvents

If Not l_rstJobHistory.EOF Then l_rstJobHistory.MoveFirst

l_intColumnNumber = 0

Do While Not l_rstJobHistory.EOF


    l_intColumnNumber = l_rstJobHistory("columnnumber")
    
    'If l_intColumnNumber < lp_grdDataGridName.Columns.Count Then
    
    If l_rstJobHistory("columnvisible") = "True" Then
        'lp_grdDataGridName.Columns.Add l_intColumnNumber
        lp_grdDataGridName.Columns(l_intColumnNumber).Name = l_rstJobHistory("columnname")
        lp_grdDataGridName.Columns(l_intColumnNumber).Caption = l_rstJobHistory("columncaption")
        lp_grdDataGridName.Columns(l_intColumnNumber).Width = l_rstJobHistory("columnwidth")
        lp_grdDataGridName.Columns(l_intColumnNumber).DataField = Right(l_rstJobHistory("columndatafield"), Len(l_rstJobHistory("columndatafield")) - InStr(l_rstJobHistory("columndatafield"), "."))
        lp_grdDataGridName.Columns(l_intColumnNumber).DataType = l_rstJobHistory("columndatatype")
        lp_grdDataGridName.Columns(l_intColumnNumber).NumberFormat = l_rstJobHistory("columnformat")
        lp_grdDataGridName.Columns(l_intColumnNumber).Visible = GetFlag(l_rstJobHistory("columnvisible"))
        lp_grdDataGridName.Columns(l_intColumnNumber).BackColor = l_rstJobHistory("columnbackcolour")
        lp_grdDataGridName.Columns(l_intColumnNumber).ForeColor = l_rstJobHistory("columnforecolour")
        
    
    End If
    
    l_rstJobHistory.MoveNext
    'l_intColumnNumber = l_intColumnNumber + 1
    
Loop

lp_grdDataGridName.Refresh

l_rstJobHistory.Close
Set l_rstJobHistory = Nothing

End Sub

Function AddJob(lp_strJobAllocation As String, lp_strJobType As String, lp_strStatus As String, lp_strOrderReference As String, _
                        lp_strProductName As String, lp_lngProductID As Long, lp_lngProjectID As Long, lp_strTitle1 As String, _
                        lp_strTitle2 As String, lp_strCompanyName As String, lp_lngCompanyID As Long, lp_strContactName As String, _
                        lp_lngContactID As Long, lp_strTelephone As String, lp_strFax As String, lp_datDeadline As Variant, lp_intFlagDetailOnInvoice As Integer, _
                        lp_intFlagEmail As Integer, lp_intFlagQuote As Integer, lp_intFlagHold As Integer, lp_intFlagUrgent As Integer, _
                        lp_strOurContact As String, lp_datStartDate As Variant, lp_datEndDate As Variant, lp_strVideoStandard As String, lp_strDealType As String, lp_dblDealAmount As Double, lp_strAllocatedTo As String, _
                        lp_lngMasterJobID As Long, lp_lngRepeatBatchID As Long, lp_intFlagTemporary As Integer) As Long

'this job simply adds a job to the database
    
    Dim l_strSQL As String
    Dim l_lngJobID As Long
    
PROC_GET_JOB_ID:
    
    l_lngJobID = GetNextSequence("jobID")

    'check to see if this job has already been
    'allocated, if it has then get the next one
    If GetData("job", "jobID", "jobID", l_lngJobID) <> 0 Then GoTo PROC_GET_JOB_ID

    l_strSQL = "INSERT INTO job ("
    l_strSQL = l_strSQL & "jobID, "
    l_strSQL = l_strSQL & "joballocation, "
    l_strSQL = l_strSQL & "jobtype, "
    l_strSQL = l_strSQL & "fd_status, "
    l_strSQL = l_strSQL & "createddate, "
    l_strSQL = l_strSQL & "createduser, "
    l_strSQL = l_strSQL & "orderreference, "
    l_strSQL = l_strSQL & "productname, "
    l_strSQL = l_strSQL & "productID, "
    l_strSQL = l_strSQL & "projectID, "
    l_strSQL = l_strSQL & "projectnumber, "
    l_strSQL = l_strSQL & "title1, "
    l_strSQL = l_strSQL & "title2, "
    l_strSQL = l_strSQL & "companyname, "
    l_strSQL = l_strSQL & "companyID, "
    l_strSQL = l_strSQL & "contactname, "
    l_strSQL = l_strSQL & "contactID, "
    l_strSQL = l_strSQL & "telephone, "
    l_strSQL = l_strSQL & "fax, "
    l_strSQL = l_strSQL & "deadlinedate, "
    l_strSQL = l_strSQL & "flagdetailoninvoice, "
    l_strSQL = l_strSQL & "flagemail, "
    l_strSQL = l_strSQL & "flagquote, "
    l_strSQL = l_strSQL & "flaghold, "
    l_strSQL = l_strSQL & "flagurgent, "
    l_strSQL = l_strSQL & "ourcontact, "
    l_strSQL = l_strSQL & "startdate, "
    l_strSQL = l_strSQL & "enddate, "
    l_strSQL = l_strSQL & "videostandard, "
    l_strSQL = l_strSQL & "repeatbatchID, "
    l_strSQL = l_strSQL & "flagtemporary, "
    l_strSQL = l_strSQL & "allocatedto, "
    l_strSQL = l_strSQL & "dealtype, "
    l_strSQL = l_strSQL & "dealprice, "
    l_strSQL = l_strSQL & "masterjobID) VALUES ("
    
    ' --------------
    
    l_strSQL = l_strSQL & "'" & l_lngJobID & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strJobAllocation) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strJobType) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strStatus) & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strOrderReference) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strProductName) & "', "
    l_strSQL = l_strSQL & "'" & lp_lngProductID & "', "
    l_strSQL = l_strSQL & "'" & lp_lngProjectID & "', "
    l_strSQL = l_strSQL & "'" & GetData("project", "projectnumber", "projectID", lp_lngProjectID) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strTitle1) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strTitle2) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strCompanyName) & "', "
    l_strSQL = l_strSQL & "'" & lp_lngCompanyID & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strContactName) & "', "
    l_strSQL = l_strSQL & "'" & lp_lngContactID & "', "
    l_strSQL = l_strSQL & "'" & lp_strTelephone & "', "
    l_strSQL = l_strSQL & "'" & lp_strFax & "', "
    
    If Not IsNull(lp_datDeadline) Then
        l_strSQL = l_strSQL & "'" & FormatSQLDate(lp_datDeadline) & "', "
    Else
        l_strSQL = l_strSQL & "NULL, "
    End If
    
    l_strSQL = l_strSQL & "'" & lp_intFlagDetailOnInvoice & "', "
    l_strSQL = l_strSQL & "'" & lp_intFlagEmail & "', "
    l_strSQL = l_strSQL & "'" & lp_intFlagQuote & "', "
    l_strSQL = l_strSQL & "'" & lp_intFlagHold & "', "
    l_strSQL = l_strSQL & "'" & lp_intFlagUrgent & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strOurContact) & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(lp_datStartDate) & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(lp_datEndDate) & "', "
    l_strSQL = l_strSQL & "'" & lp_strVideoStandard & "', "
    l_strSQL = l_strSQL & "'" & IIf(IsNull(lp_lngRepeatBatchID), Null, lp_lngRepeatBatchID) & "',"
    l_strSQL = l_strSQL & "'" & lp_intFlagTemporary & "',"
    l_strSQL = l_strSQL & "'" & lp_strAllocatedTo & "',"
    l_strSQL = l_strSQL & "'" & lp_strDealType & "',"
    l_strSQL = l_strSQL & "'" & lp_dblDealAmount & "',"
    l_strSQL = l_strSQL & "'" & IIf(IsNull(lp_lngMasterJobID), Null, lp_lngMasterJobID) & "')"
    '--------------------
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    AddJob = l_lngJobID
    

End Function
                
                
' Private function that does the work under Windows NT
Public Function GetUNCNameNT(pathName As String) As String

Dim hKey As Long
Dim hKey2 As Long
Dim exitFlag As Boolean
Dim i As Double
Dim errCode As Long
Dim rootKey As String
Dim Key As String
Dim computerName As String
Dim lComputerName As Long
Dim stPath As String
Dim firstLoop As Boolean
Dim Ret As Boolean

' first, verify whether the disk is connected to the network
If Mid(pathName, 2, 1) = ":" Then
   Dim UNCName As String
   Dim lenUNC As Long

   UNCName = String$(520, 0)
   lenUNC = 520
   errCode = WNetGetConnection(Left(pathName, 2), UNCName, lenUNC)

   If errCode = 0 Then
      UNCName = Trim(Left$(UNCName, InStr(UNCName, _
        vbNullChar) - 1))
      GetUNCNameNT = UNCName & Mid(pathName, 3)
      Exit Function
   End If
End If

' else, scan the registry looking for shared resources
'(NT version)
computerName = String$(255, 0)
lComputerName = Len(computerName)
errCode = GetComputerName(computerName, lComputerName)
If errCode <> 1 Then
   GetUNCNameNT = pathName
   Exit Function
End If

computerName = Trim(Left$(computerName, InStr(computerName, _
   vbNullChar) - 1))
rootKey = "SYSTEM\CurrentControlSet\Services\LanmanServer\Shares"
errCode = RegOpenKey(HKEY_LOCAL_MACHINE, rootKey, hKey)

If errCode <> 0 Then
   GetUNCNameNT = pathName
   Exit Function
End If

firstLoop = True

Do Until exitFlag
   Dim szValue As String
   Dim szValueName As String
   Dim cchValueName As Long
   Dim dwValueType As Long
   Dim dwValueSize As Long

   szValueName = String(1024, 0)
   cchValueName = Len(szValueName)
   szValue = String$(500, 0)
   dwValueSize = Len(szValue)

   ' loop on "i" to access all shared DLLs
   ' szValueName will receive the key that identifies an element
   errCode = RegEnumValue(hKey, i#, szValueName, _
       cchValueName, 0, dwValueType, szValue, dwValueSize)

   If errCode <> 0 Then
      If Not firstLoop Then
         exitFlag = True
      Else
         i = -1
         firstLoop = False
      End If
   Else
      stPath = GetPath(szValue)
      If firstLoop Then
         Ret = (UCase(stPath) = UCase(pathName))
         stPath = ""
      Else
         Ret = (UCase(stPath) = UCase(Left$(pathName, _
        Len(stPath))))
         stPath = Mid$(pathName, Len(stPath))
      End If
      If Ret Then
         exitFlag = True
         szValueName = Left$(szValueName, cchValueName)
         GetUNCNameNT = "\\" & computerName & "\" & _
            szValueName & stPath
      End If
   End If
   i = i + 1
Loop

RegCloseKey hKey
If GetUNCNameNT = "" Then GetUNCNameNT = pathName

End Function

' support routine
Private Function GetPath(st As String) As String
   Dim pos1 As Long, pos2 As Long, pos3 As Long
   Dim stPath As String

   pos1 = InStr(st, "Path")
   If pos1 > 0 Then
      pos2 = InStr(pos1, st, vbNullChar)
      stPath = Mid$(st, pos1, pos2 - pos1)
      pos3 = InStr(stPath, "=")
      If pos3 > 0 Then
         stPath = Mid$(stPath, pos3 + 1)
         GetPath = stPath
      End If
   End If
End Function

Public Sub AddContactToProjectTeam(lp_lngProjectID As Long, lp_lngJobID As Long, lp_lngDefaultCompanyID As Long)
    'first we need to select a contact
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    l_strSQL = "SELECT name, accountcode, telephone, companyID FROM company ORDER BY name"
    
    frmSelectContact.adoCompany.ConnectionString = g_strConnection
    frmSelectContact.adoCompany.RecordSource = l_strSQL
    frmSelectContact.adoCompany.Refresh
    frmSelectContact.lblCompanyID.Caption = lp_lngDefaultCompanyID
    frmSelectContact.cmbCompany.Text = GetData("company", "name", "companyID", lp_lngDefaultCompanyID)
    
    frmSelectContact.Show vbModal
    
    If frmSelectContact.Tag <> "CANCEL" Then
        
        'add the record to the team table
        
        Dim l_lngContactID As Long
        Dim l_strRole As String
        
        l_lngContactID = frmSelectContact.lblContactID.Caption
        l_strRole = frmSelectContact.cmbRole.Text
        
        l_strSQL = "INSERT INTO team "
        l_strSQL = l_strSQL & "(cuser, cdate, projectID, jobID, contactID, role) VALUES ("
        l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now()) & "', "
        l_strSQL = l_strSQL & "'" & lp_lngProjectID & "', "
        l_strSQL = l_strSQL & "'" & lp_lngJobID & "', "
        l_strSQL = l_strSQL & "'" & l_lngContactID & "', "
        l_strSQL = l_strSQL & "'" & l_strRole & "') "
        
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        AddJobHistory lp_lngJobID, "Added team (" & frmSelectContact.cmbContact.Text & ")"
        
    End If
    
    Unload frmSelectContact
    Set frmSelectContact = Nothing
    
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub AddJobHistory(lp_lngJobID As Long, lp_strDescription As String)

    'add a history line to a job
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    
    If g_dbtype = "mysql" Then
        l_strSQL = "INSERT INTO jobhistory (jobID, cuser, cdate, description, servertime) VALUES ('" & lp_lngJobID & "', '" & g_strUserInitials & "', '" & FormatSQLDate(Now) & "', '" & QuoteSanitise(lp_strDescription) & "', Now())"
    Else
        l_strSQL = "INSERT INTO jobhistory (jobID, cuser, cdate, description, servertime) VALUES ('" & lp_lngJobID & "', '" & g_strUserInitials & "', '" & FormatSQLDate(Now) & "', '" & QuoteSanitise(lp_strDescription) & "', GETdate())"
    End If
        
    
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub
Sub AddHistoryLog(ByVal lp_strLogType As String, ByVal lp_lngLogTypeID As Long, ByVal lp_strDescription As String)
    
    'add a history line
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    
    If g_dbtype = "mysql" Then
        l_strSQL = "INSERT INTO log (logtype, logtypeID, cuser, cdate, cuserID, logcontent) VALUES"
        l_strSQL = l_strSQL & " ('" & lp_strLogType & "', '" & lp_lngLogTypeID & "', '" & g_strUserInitials & "', Now(), '" & g_lngUserID & "', '" & QuoteSanitise(lp_strDescription) & "');"
    Else
        l_strSQL = "INSERT INTO log (logtype, logtypeID, cuser, cdate, cuserID, logcontent) VALUES"
        l_strSQL = l_strSQL & " ('" & lp_strLogType & "', '" & lp_lngLogTypeID & "', '" & g_strUserInitials & "', GetDate(), '" & g_lngUserID & "', '" & QuoteSanitise(lp_strDescription) & "');"
    End If
        
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Public Function AddOfficeBorder(ByVal l_conControl As Control)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If TypeOf l_conControl Is Timer Or TypeOf l_conControl Is CommandButton Or TypeOf l_conControl Is Shape Or TypeOf l_conControl Is TabStrip Or TypeOf l_conControl Is ListBox Or TypeOf l_conControl Is Label Or TypeOf l_conControl Is Line Or TypeOf l_conControl Is Command Or TypeOf l_conControl Is ComboBox Or TypeOf l_conControl Is OptionButton Or TypeOf l_conControl Is Image Or TypeOf l_conControl Is Frame Or TypeOf l_conControl Is SSOleDBCombo Or TypeOf l_conControl Is DTPicker Or TypeOf l_conControl Is CheckBox Or TypeOf l_conControl Is SSTab Or TypeOf l_conControl Is CrystalActiveXReportViewer Or TypeOf l_conControl Is Adodc Or TypeOf l_conControl Is PictureBox Then
        Exit Function
    Else
        
        With l_conControl
            Dim RetVal As Long
            RetVal = GetWindowLong(.hWnd, GWL_EXSTYLE)
            RetVal = RetVal Or WS_EX_STATICEDGE And Not WS_EX_CLIENTEDGE
            SetWindowLong .hWnd, GWL_EXSTYLE, RetVal
            SetWindowPos .hWnd, 0, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE _
            Or SWP_NOOWNERZORDER Or SWP_NOZORDER Or SWP_FRAMECHANGED
        End With
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub AddResourceToJob(lp_lngJobID As Long, lp_strDefaultGroup As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/addresourcetojob") Then
        Exit Sub
    End If
    
    'check if this job can have a resource added to it.
    
    Dim l_intStatusNumber As Integer
    l_intStatusNumber = GetStatusNumber(GetData("job", "fd_status", "jobID", lp_lngJobID))
    
    If l_intStatusNumber < 3 Then
        
        frmAddResource.lblJobID.Caption = lp_lngJobID
        frmAddResource.cmbCategory.Text = lp_strDefaultGroup
        frmAddResource.cmbCategory_Click
        frmAddResource.Show vbModal
        Unload frmAddResource
        Set frmAddResource = Nothing
        
        
    Else
        MsgBox "You can not add a resource to this job, as it has already passed the completed phase.", vbExclamation
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Function GetTextPart(ByVal lp_Text$, ByVal lp_Part%, lp_Delimiter$)

On Error GoTo TextPart_Error

Dim l_CurrentPart%, l_Text$, l_Position%

For l_CurrentPart% = 1 To lp_Part%
    l_Position% = InStr(lp_Text$, lp_Delimiter$)
    If l_Position% = 0 Then l_Position% = Len(lp_Text$) + 1
    l_Text$ = Left(lp_Text$, l_Position% - 1)
    If Len(lp_Text$) > 1 Then lp_Text$ = Right(lp_Text$, (Len(lp_Text$) - l_Position%))
Next

GetTextPart = l_Text$

Exit Function


TextPart_Error:

MsgBox Err.Description
Resume Next

End Function


Sub AlertDoesNothing()
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    MsgBox "This function has not yet been implemented", vbCritical
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub ApplyActualHours(lp_lngJobID As Long, lp_sngActualHours As Single, Optional lp_lngResourceScheduleID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/applyactualtimes") Then
        Exit Sub
    End If
    
    Dim l_intStatusNumber As Integer
    l_intStatusNumber = GetStatusNumber(GetData("job", "fd_status", "jobID", lp_lngJobID))
    
    If l_intStatusNumber < 5 Then
        
        Dim l_strSQL As String
        If lp_lngResourceScheduleID = 0 Then
            l_strSQL = "UPDATE resourceschedule SET actualhoursused = '" & lp_sngActualHours & "' WHERE jobID = '" & lp_lngJobID & "';"
        Else
            l_strSQL = "UPDATE resourceschedule SET actualhoursused = '" & lp_sngActualHours & "' WHERE resourcescheduleID = '" & lp_lngResourceScheduleID & "';"
        End If
        
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        AddJobHistory lp_lngJobID, "Applied Chargable Hours"
        
    Else
        MsgBox "You can not change the actual times for this job, as it has already been costed.", vbExclamation
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub ApplyActualTimes(lp_lngJobID As Long, lp_datStartDate As Date, lp_datEndDate As Date)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    
    'is the user allowed to do this?
    If Not CheckAccess("/applyactualtimes") Then
        Exit Sub
    End If
    
    'check times are valid.
    If DateDiff("n", lp_datStartDate, lp_datEndDate) < 1 Then
        MsgBox "You must specify valid times. The start time can not be the same time or after the end time.", vbExclamation
        Exit Sub
    End If
    
    'get the project ID... if there is one!
    If GetData("job", "projectnumber", "jobID", lp_lngJobID) = 0 Then
    
        'if not, dont allow the user to enter the actual times!
        MsgBox "You can not enter times for this job as it has no Project Number. Has this job been through the confirmed status?", vbExclamation
        Exit Sub
    End If
    
    'update the resource schedule items with the actual start and end times...
    l_strSQL = "UPDATE resourceschedule SET actualstarttime = '" & FormatSQLDate(lp_datStartDate) & "', actualendtime = '" & FormatSQLDate(lp_datEndDate) & "' WHERE jobID = '" & lp_lngJobID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'set the job actuals
    SetData "job", "actualstartdate", "jobID", lp_lngJobID, FormatSQLDate(lp_datStartDate)
    SetData "job", "actualenddate", "jobID", lp_lngJobID, FormatSQLDate(lp_datEndDate)
    
    'add the note to the costing notes
    InsertNote lp_lngJobID, "notes4", "Applied Actuals - " & lp_datStartDate & " - " & lp_datEndDate
    
    'add a history line to the jobnd
    AddJobHistory lp_lngJobID, "Applied Actual Hours"
    
    'pick up the job type (hire etc)
    
    Dim l_strJobType As String
    l_strJobType = UCase(GetData("job", "jobtype", "jobID", lp_lngJobID))
    
    If l_strJobType <> "HIRE" Then
        'save job as completed
        UpdateJobStatus lp_lngJobID, "Completed", , True
    End If
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub UpdateResourceScheduleTimes(lp_lngResourceScheduleID As Long, lp_lngJobID As Long, lp_datStartDate As Date, lp_datEndDate As Date)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/updateresourceschedule") Then
        Exit Sub
    End If
    
    'check times are valid.
    If DateDiff("n", lp_datStartDate, lp_datEndDate) < 1 Then
        MsgBox "You must specify valid times. The start time can not be the same time or after the end time.", vbExclamation
        Exit Sub
    End If
    
    Dim l_lngResourceID As Long
    l_lngResourceID = GetData("resourceschedule", "resourceID", "resourcescheduleID", lp_lngResourceScheduleID)
    
    Dim l_strStatus As String
    l_strStatus = GetData("resourceschedule", "fd_status", "resourcescheduleID", lp_lngResourceScheduleID)
    
    Dim l_datOldStartTime   As Date
    Dim l_datOldEndTime     As Date
    
    l_datOldStartTime = GetData("resourceschedule", "starttime", "resourcescheduleID", lp_lngResourceScheduleID)
    l_datOldEndTime = GetData("resourceschedule", "endtime", "resourcescheduleID", lp_lngResourceScheduleID)
    
    Dim l_datOldBookingStartTime As Date
    Dim l_datOldBookingEndTime As Date
    
    l_datOldBookingStartTime = GetData("job", "startdate", "jobID", lp_lngJobID)
    l_datOldBookingEndTime = GetData("job", "enddate", "jobID", lp_lngJobID)
    
    Dim l_intMsg As Integer
    
    If l_datOldBookingStartTime <> l_datOldStartTime Or l_datOldBookingEndTime <> l_datOldEndTime Then
        l_intMsg = MsgBox("The resource you are scheduling (" & GetData("resource", "name", "resourceID", l_lngResourceID) & ") does not have the standard job times. Are you sure you want to change its details?", vbYesNo + vbQuestion, "Non-Standard Times")
        If l_intMsg = vbNo Then Exit Sub
    End If
    
    
    If IsDate(lp_datStartDate) = True And IsDate(lp_datEndDate) = True And GetStatusNumber(l_strStatus) < 3 Then
        If IsConflict(lp_lngJobID, l_lngResourceID, CDate(lp_datStartDate), CDate(lp_datEndDate), True) Then
    
            'If lp_blnDontPrompt <> True Then
        
                l_intMsg = NewMsgBox("Conflicting Resource Schedule", "A resource you are scheduling (" & GetData("resource", "name", "resourceID", l_lngResourceID) & ") is conflicting with another job." & vbCrLf & vbCrLf & "Do you want to make it a second pencil?", "Yes - 2nd Pencil", "No - Leave as is", "Cancel")
                
                Select Case l_intMsg
                Case 0
                    l_strStatus = "2nd Pencil"
    
                Case 1
    
                Case 2
                    GoTo PROC_EXIT
                End Select
            'End If
        End If
    End If
    
    If l_strStatus = "2nd Pencil" Then SetData "resourceschedule", "fd_status", "resourcescheduleID", lp_lngResourceScheduleID, l_strStatus
    
    SetData "resourceschedule", "starttime", "resourcescheduleID", lp_lngResourceScheduleID, FormatSQLDate(lp_datStartDate)
    SetData "resourceschedule", "endtime", "resourcescheduleID", lp_lngResourceScheduleID, FormatSQLDate(lp_datEndDate)
    
    Dim l_strResourceName As String
    l_strResourceName = GetData("resource", "name", "resourceID", GetData("resourceschedule", "resourceID", "resourcescheduleID", lp_lngResourceScheduleID))
    
    'save job as completed
    'AddJobHistory lp_lngJobID, "Updated Resource Times (" & l_strResourceName & ")"
    'add a history record
    
    AddJobHistory lp_lngJobID, "Updated Resource Times (" & l_strResourceName & ") (From: " & Format(l_datOldStartTime, "dd/mm/yy hh:nn") & "/" & Format(l_datOldEndTime, "dd/mm/yy hh:nn") & " to " & Format(lp_datStartDate, "dd/mm/yy hh:nn") & "/" & Format(lp_datEndDate, "dd/mm/yy hh:nn") & ")"
        
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Public Sub AutoCreateDubbings(lp_lngJobID As Long)


    Dim l_lngResourceScheduleID As Long
    Dim l_strFormat As String
    Dim l_strStandard As String
    Dim l_strCopyType As String
    Dim l_intMachineTime As Integer
    Dim l_strDescription As String
    Dim l_blnCopies As Boolean
    Dim l_strSQL As String
    Dim l_intQuantity As Integer
    Dim l_strAspectRatio As String
    
    l_strSQL = "SELECT * FROM temporaryresourceschedule INNER JOIN resource ON temporaryresourceschedule.resourceID = resource.resourceID WHERE (resource.category != 'STAFF' AND resource.category != 'PEOPLE') AND createduser = '" & g_strUserInitials & "' ORDER BY temporaryresourcescheduleID"
    
    Dim l_rstResourceSchedule As ADODB.Recordset
    Set l_rstResourceSchedule = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    'cycle through and add each item, if required
    'go back and cost all the stock on the job
    If Not l_rstResourceSchedule.EOF Then
        l_rstResourceSchedule.MoveFirst
    End If
    
    Dim l_lngCounter As Long
    
    Do While Not l_rstResourceSchedule.EOF
        
        If Trim(" " & l_rstResourceSchedule("linestandard")) <> "" Then
            l_strStandard = l_rstResourceSchedule("linestandard")
            If Trim(" " & l_rstResourceSchedule("framerate")) <> "" Then l_strStandard = l_strStandard & "-" & l_rstResourceSchedule("framerate")
            If Trim(" " & l_rstResourceSchedule("scanpattern")) <> "" Then l_strStandard = l_strStandard & "-" & l_rstResourceSchedule("scanpattern")
        Else
            l_strStandard = GetData("resource", "standard", "resourceID", l_rstResourceSchedule("resourceID"))
        End If
        
        l_strAspectRatio = Trim(" " & l_rstResourceSchedule("aspectratio"))
        
        l_strFormat = GetData("resource", "format", "resourceID", l_rstResourceSchedule("resourceID"))
        
        
        If g_optCalculateRunningTimeFromSchedule = 1 Then
            l_intMachineTime = DateDiff("n", l_rstResourceSchedule("starttime"), l_rstResourceSchedule("endtime"))
        End If
        
        If l_strFormat = "" Then
            MsgBox "The resource you have booked (" & GetData("resource", "name", "resourceID", l_rstResourceSchedule("resourceID")) & ") does not have information about it's format.", vbInformation
        End If
            
        
        If l_strStandard = "" Then
            MsgBox "The resource you have booked (" & GetData("resource", "name", "resourceID", l_rstResourceSchedule("resourceID")) & ") does not have information about it's standard.", vbInformation
        End If
                        
            
        
        If l_strFormat <> "" Then
            
            l_intQuantity = 1
            
            
            
            l_lngResourceScheduleID = l_rstResourceSchedule("temporaryresourcescheduleID")
            
            
            'this is a converter - rather than a VTR etc (ARC/UKON etc)
            If l_strAspectRatio = "SPECIAL" Then
                l_strDescription = "Use Of " & l_strStandard
                l_strCopyType = "O"
                l_strFormat = l_strStandard
                l_strStandard = ""
                l_strAspectRatio = ""
            Else
            
                If l_blnCopies = False Then
                    l_strCopyType = "M"
                    l_strDescription = UCase(GetData("job", "title1", "jobID", lp_lngJobID))
                Else
                    l_strCopyType = "C"
                    
                    If g_optUseTitleInCopy = 1 Then
                        l_strDescription = UCase(GetData("job", "title1", "jobID", lp_lngJobID))
                    Else
                        l_strDescription = "Record to " & l_strFormat
                    End If
                End If
            End If
        
            If l_strDescription = "" Then
                l_strDescription = CStr(frmJob.txtTitle.Text)
                If l_strDescription = "" Then l_strDescription = "Please enter a description"
            End If
            
            
            Dim l_strAliasStandard As String
            l_strAliasStandard = GetDataSQL("SELECT TOP 1 description FROM xref WHERE category = 'videostandard' AND descriptionalias = '" & QuoteSanitise(l_strStandard) & "';")
            
            If l_strAliasStandard = "" Then l_strAliasStandard = l_strStandard
            
                        
            'add the dubbing lines (this is in a loop)
            CreateDubbingLine l_lngResourceScheduleID, lp_lngJobID, l_strCopyType, l_strDescription, l_strFormat, l_strAliasStandard, l_intQuantity, l_intMachineTime, l_lngCounter, l_strAspectRatio
            
            'now do the copies
            l_blnCopies = True
            
            l_lngCounter = l_lngCounter + 1
            
        End If
        
        'cycle
        l_rstResourceSchedule.MoveNext
    Loop

    l_rstResourceSchedule.Close
    Set l_rstResourceSchedule = Nothing
    
    

End Sub

Private Sub CreateDubbingLine(lp_lngResourceScheduleID As Long, lp_lngJobID As Long, lp_strCopyType As String, _
lp_strDescription As String, lp_strFormat As String, lp_strStandard As String, lp_intQuantity As Integer, lp_intMachineTime As Integer, lp_lngCounter As Long, lp_strAspectRatio As String)

    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    lp_strDescription = QuoteSanitise(lp_strDescription)
    
    
    Dim l_strSQL  As String
    
    'add the new line
    l_strSQL = "INSERT INTO jobdetail "
    l_strSQL = l_strSQL & "(resourcescheduleID, jobID, copytype, description, format, videostandard, quantity, runningtime, aspectratio, fd_orderby) VALUES ("
    
    l_strSQL = l_strSQL & "'" & lp_lngResourceScheduleID & "', "
    l_strSQL = l_strSQL & "'" & lp_lngJobID & "', "
    l_strSQL = l_strSQL & "'" & lp_strCopyType & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strDescription) & "', "
    l_strSQL = l_strSQL & "'" & lp_strFormat & "', "
    l_strSQL = l_strSQL & "'" & lp_strStandard & "', "
    l_strSQL = l_strSQL & "'" & lp_intQuantity & "', "
    l_strSQL = l_strSQL & "'" & lp_intMachineTime & "', "
    l_strSQL = l_strSQL & "'" & lp_strAspectRatio & "', "
    l_strSQL = l_strSQL & "'" & lp_lngCounter & "')"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'return the new costingID
    'CreateDubbingLine = g_lngLastID
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
End Sub

Function IsProjectLiveForProduct(lp_strProductName As String) As Boolean

Dim l_strSQL As String
l_strSQL = "SELECT COUNT(projectID) FROM project LEFT JOIN product ON project.productID = product.productID WHERE product.name LIKE '" & QuoteSanitise(lp_strProductName) & "%' AND project.fd_status <> 'CANCELLED' AND project.fd_status <> 'SENT TO ACCOUNTS' AND project.fd_status <> 'COMPLETED' AND project.fd_status <> 'CANCEL' AND project.fd_status <> 'INVOICED' AND project.fd_status <> 'INVOICED' ;"

Dim l_rstSearch As New ADODB.Recordset

Set l_rstSearch = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If l_rstSearch(0) > 0 Then
    IsProjectLiveForProduct = True
Else
    IsProjectLiveForProduct = False
End If

l_rstSearch.Close
Set l_rstSearch = Nothing

End Function

Sub UpdateAllResourceSchedulesForJob(ByVal lp_lngJobID As Long, ByVal lp_datNewStartTime As Date, ByVal lp_datNewEndTime As Date)

Dim l_strSQL As String, l_lngResourceScheduleID As Long, l_rstSearch As New ADODB.Recordset

l_strSQL = "SELECT resourcescheduleID FROM resourceschedule WHERE jobID = '" & lp_lngJobID & "';"

Set l_rstSearch = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

Do While Not l_rstSearch.EOF
    l_lngResourceScheduleID = l_rstSearch("resourcescheduleID")
    UpdateResourceScheduleTimes l_lngResourceScheduleID, lp_lngJobID, lp_datNewStartTime, lp_datNewEndTime
    l_rstSearch.MoveNext
Loop

l_rstSearch.Close
Set l_rstSearch = Nothing

End Sub
Sub ApplyBookedTimes(lp_lngJobID As Long, lp_datStartDate As Date, lp_datEndDate As Date)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/applybookedtimes") Then
        Exit Sub
    End If
    
    'check times are valid.
    If DateDiff("n", lp_datStartDate, lp_datEndDate) < 1 Then
        MsgBox "You must specify valid times. The start time can not be the same as or after the end time.", vbExclamation
        Exit Sub
    End If
    
    Dim l_datOldStartTime As Date
    Dim l_datOldEndTime As Date
    
    l_datOldStartTime = GetData("job", "startdate", "jobID", lp_lngJobID)
    l_datOldEndTime = GetData("job", "enddate", "jobID", lp_lngJobID)
    
    Dim l_intStatusNumber As Integer
    l_intStatusNumber = GetStatusNumber(GetData("job", "fd_status", "jobID", lp_lngJobID))
    
    If l_intStatusNumber < 3 Then
    
        'should really loop through the resource bookings and check for conflicts here
        
        Dim l_strSQL As String
        'l_strSQL = "UPDATE resourceschedule SET starttime = '" & FormatSQLDate(lp_datStartDate) & "', endtime = '" & FormatSQLDate(lp_datEndDate) & "' WHERE jobID = '" & lp_lngJobID & "';"
        
        'ExecuteSQL l_strSQL, g_strExecuteError
        'CheckForSQLError
        
        UpdateAllResourceSchedulesForJob lp_lngJobID, lp_datStartDate, lp_datEndDate
        
        'update the job table
        SetData "job", "startdate", "jobID", lp_lngJobID, FormatSQLDate(lp_datStartDate)
        SetData "job", "enddate", "jobID", lp_lngJobID, FormatSQLDate(lp_datEndDate)
    
        'add a history record
        AddJobHistory lp_lngJobID, "Applied Booked Times (From: " & Format(l_datOldStartTime, "dd/mm/yy hh:nn") & "/" & Format(l_datOldEndTime, "dd/mm/yy hh:nn") & " to " & Format(lp_datStartDate, "dd/mm/yy hh:nn") & "/" & Format(lp_datEndDate, "dd/mm/yy hh:nn") & ")"
        
        l_strSQL = "UPDATE despatch SET jobtime = '" & FormatSQLDate(lp_datStartDate) & "' WHERE jobID = '" & lp_lngJobID & "';"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    
        UpdateDespatchForJob lp_lngJobID
    
    Else
        MsgBox "You can not apply different booked times to this job, as it has already been completed", vbExclamation
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

'
'
Public Sub AutoMatch(cbComboBox As ComboBox, KeyAscii As Integer)
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim Idx As Long
    Dim pos As Integer
    Dim strSearch As String
    
    With cbComboBox
        If KeyAscii = vbKeyBack Then
            If .SelStart > 1 Then
                strSearch = Mid(.Text, 1, .SelStart - 1)
            Else
                strSearch = ""
                .Text = ""
            End If
        Else
            strSearch = Mid(.Text, 1, .SelStart) & Chr(KeyAscii)
        End If
        
        'Call SendMessage to get the Index of an item (if any)
        Idx = SendMessage(.hWnd, CB_FINDSTRING, -1, ByVal strSearch)
        'If idx equals CB_ERR, then the search failed
        If Idx > CB_ERR Then            'Store the caret position
            pos = Len(strSearch)
            .Text = .List(Idx)
            .ListIndex = Idx            ' Re-highlight the trailing text
            .SelStart = pos
            .SelLength = Len(.Text) - pos
            KeyAscii = 0
        End If
    End With
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub


Sub AuthoriseSundry(lp_lngSundryCostID As Long)

If Not CheckAccess("/authorisesundry") Then Exit Sub

Dim l_strSQL As String

l_strSQL = "UPDATE sundrycost SET fd_status = 'Authorised', authoriseddate = '" & FormatSQLDate(Now) & "', authoriseduser = '" & g_strUserInitials & "' WHERE sundrycostID = '" & lp_lngSundryCostID & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

Dim l_lngJobID As Long
l_lngJobID = GetData("sundrycost", "jobID", "sundrycostID", lp_lngSundryCostID)
If l_lngJobID <> 0 Then SetData "job", "flagnewsundries", "jobID", l_lngJobID, "-1"

End Sub

Sub ApproveSundry(lp_lngSundryCostID As Long)

If Not CheckAccess("/approvesundry") Then Exit Sub

Dim l_strSQL As String

If Not IsDate(GetData("sundrycost", "authoriseddate", "sundrycostID", lp_lngSundryCostID)) Then

    If Not CheckAccess("/authorisesundry", True) Then
        MsgBox "This sundry has not yet been authorised, and you dont have permission to do it.", vbExclamation
        Exit Sub
    End If
       
    'dont bother requesting a job ID for non-project based jobs
    If Not IsOverHeadCode(frmSundry.cmbProject.Text) Then

        Dim l_strContact As String
        l_strContact = GetData("job", "ourcontact", "jobID", GetData("sundrycost", "jobID", "sundrycostID", lp_lngSundryCostID))
        If l_strContact = "" Then
            l_strContact = "unknown"
        End If
        
        Dim l_intMessage As Integer
        l_intMessage = MsgBox("This sundry cost has not yet been authorised. The contact for this job is " & l_strContact & ", do you want to authorise it now?", vbQuestion + vbYesNo)
        If l_intMessage = vbNo Then
            Exit Sub
        End If
    
    Else
        If IsOverHeadCode(frmSundry.cmbProject.Text) And frmSundry.cmbOverheads.Text = "" Then
            SetData "sundrycost", "overheadcode", "sundrycostID", lp_lngSundryCostID, frmSundry.cmbProject.Text
            frmSundry.cmbOverheads.Text = frmSundry.cmbProject.Text
        End If
    End If
        
    AuthoriseSundry lp_lngSundryCostID

Else

    If IsOverHeadCode(frmSundry.cmbProject.Text) And frmSundry.cmbOverheads.Text = "" Then
        SetData "sundrycost", "overheadcode", "sundrycostID", lp_lngSundryCostID, frmSundry.cmbProject.Text
        frmSundry.cmbOverheads.Text = frmSundry.cmbProject.Text
    End If

End If

DoEvents
DBEngine.Idle

If GetData("sundrycost", "overheadcode", "sundrycostID", lp_lngSundryCostID) = "" Then
    MsgBox "You can not approve this sundry as it does not yet have an overhead code.", vbExclamation
    If Not IsFormLoaded("frmSundry") Then ShowSundryCost lp_lngSundryCostID
    Exit Sub
End If

l_strSQL = "UPDATE sundrycost SET fd_status = 'Approved', approveddate = '" & FormatSQLDate(Now) & "', approveduser = '" & g_strUserInitials & "' WHERE sundrycostID = '" & lp_lngSundryCostID & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Sub


Function AutomaticLogin(Optional lp_strUserName As String, Optional lp_strPassword As String)
    
    On Error GoTo AutomaticLogin_Error
    
    Dim sBuffer As String
    Dim lSize As Long
    
    Dim l_strUserName As String
    
    sBuffer = Space$(255)
    lSize = Len(sBuffer)
    
    'call API function to get windows user name
    Call GetUserName(sBuffer, lSize)
    
    'make sure there is a user name
    If lSize > 0 Then
        l_strUserName = Left$(sBuffer, lSize)
    Else
        l_strUserName = vbNullString
    End If
    
    Dim l_blnResult As Boolean
    
    'for some reason this picks up a funny charactes as well as the user name
    'chop it off the end
    l_strUserName = Left(l_strUserName, Len(l_strUserName) - 1)
    
    'check user name (if there is one) and try to login
    If l_strUserName <> "" Then
        l_blnResult = CheckLogon(l_strUserName, "")
    End If
    
    'If unsuccessful check the command line for login credentials and try those
    If l_blnResult = False Then
        l_blnResult = CheckLogon(lp_strUserName, lp_strPassword)
    End If
    
    'if unsucessful, then log in manually
    If l_blnResult = False Then
        Logout
    End If
    
    
    
    Exit Function
    
AutomaticLogin_Error:
    
    MsgBox Err.Description, vbExclamation
    Logout
    
    Exit Function
    
End Function

Sub CancelJob(lp_lngJobID As Long)
    
Dim l_strSQL As String, l_lngCompanyID As Long, l_rstFiles As ADODB.Recordset
    
'TVCodeTools ErrorEnablerStart
On Error GoTo PROC_ERR
'TVCodeTools ErrorEnablerEnd

Dim l_intResponse As Integer

If Not CheckAccess("/canceljob") Then
    Exit Sub
End If

l_intResponse = MsgBox("Are you sure you wish to cancel job " & lp_lngJobID & "?", vbYesNo + vbQuestion, "Cancelling Job...")

If l_intResponse = vbYes Then
    
    SetData "job", "cancelleddate", "jobid", lp_lngJobID, FormatSQLDate(Now)
    SetData "job", "cancelleduser", "jobID", lp_lngJobID, g_strUserInitials
    
    'DELETE ALL THE COSTINGS
    l_strSQL = "UPDATE costing SET total = 0, vat = 0, totalincludingvat = 0, inflatedtotal = 0, inflatedvat = 0, inflatedtotalincludingvat = 0, refundedtotal = 0, refundedvat = 0, refundedtotalincludingvat = 0 WHERE jobID = '" & lp_lngJobID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'DELETE ANY DISNEY TRACKER INFO
    l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
    If InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/disney") > 0 Then
        l_strSQL = "DELETE FROM disneytracker WHERE companyID = " & l_lngCompanyID & " AND jobID = " & lp_lngJobID & ";"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    End If
    
    'CANCEL THE PO
    l_strSQL = "UPDATE purchaseheader SET cancelleddate = '" & FormatSQLDate(Now()) & "', cancelleduser = '" & g_strUserInitials & "' WHERE jobID = '" & lp_lngJobID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'REMOVE VALUE FROM THE PO LINES
    If g_dbtype = "mysql" Then
        l_strSQL = "UPDATE purchasedetail INNER JOIN purchaseheader ON purchasedetail.purchaseheaderID = purchaseheader.purchaseheaderID SET unitcharge = 0, total = 0, vat = 0, totalincludingvat = 0 WHERE purchaseheader.jobID = '" & lp_lngJobID & "';"
    Else
        l_strSQL = "UPDATE purchasedetail SET unitcharge = 0, total = 0, vat = 0, totalincludingvat = 0 FROM purchasedetail INNER JOIN purchaseheader ON purchasedetail.purchaseheaderID = purchaseheader.purchaseheaderID WHERE purchaseheader.jobID = '" & lp_lngJobID & "';"
    End If
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'CANCEL THE DESPATCH
    l_strSQL = "UPDATE despatch SET description = 'Job Cancelled By " & g_strUserInitials & "', deliverymethod = 'Cancelled' WHERE jobID = '" & lp_lngJobID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'UpdateDespatchForJob lp_lngJobID
    
    'Media Checks - Check whether any files were made for this job, and if so offer to Dump them to AutoDelete
    If GetCETASetting("UseAutoDeleteWhenFinalisingJobs") <> 0 Then
        If InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/NoAutoDelete") <= 0 Then
            l_strSQL = "SELECT count(eventID) FROM vwEventsWithLibraryFormat WHERE system_deleted = 0 AND format = 'DISCSTORE' AND jobID = " & lp_lngJobID & ";"
            If GetCount(l_strSQL) > 0 Then
                If MsgBox("There are " & GetCount(l_strSQL) & " files made for this job." & vbCrLf & "Should these files be moved to the AutoDelete process", vbYesNo) = vbYes Then
                    l_strSQL = "SELECT eventID, libraryID FROM vwEventsWithLibraryFormat WHERE system_deleted = 0 AND format = 'DISCSTORE' AND jobID = " & lp_lngJobID & ";"
                    Set l_rstFiles = ExecuteSQL(l_strSQL, g_strExecuteError)
                    If l_rstFiles.RecordCount > 0 Then
                        l_rstFiles.MoveFirst
                        Do While Not l_rstFiles.EOF
                            l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, RequestName, RequesterEmail) VALUES ("
                            l_strSQL = l_strSQL & l_rstFiles("eventID") & ", "
                            l_strSQL = l_strSQL & l_rstFiles("libraryID") & ", "
                            l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move to AutoDelete") & ", "
                            l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                            Debug.Print l_strSQL
                            ExecuteSQL l_strSQL, g_strExecuteError
                            CheckForSQLError
                            l_rstFiles.MoveNext
                        Loop
                    End If
                    l_rstFiles.Close
                    Set l_rstFiles = Nothing
                Else
                    If MsgBox("Are you sure that you don't want to AutoDelete these files", vbYesNo + vbDefaultButton2) = vbNo Then
                        l_strSQL = "SELECT eventID, libraryID FROM vwEventsWithLibraryFormat WHERE system_deleted = 0 AND format = 'DISCSTORE' AND jobID = " & lp_lngJobID & ";"
                        Set l_rstFiles = ExecuteSQL(l_strSQL, g_strExecuteError)
                        If l_rstFiles.RecordCount > 0 Then
                            l_rstFiles.MoveFirst
                            Do While Not l_rstFiles.EOF
                                l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, RequestName, RequesterEmail) VALUES ("
                                l_strSQL = l_strSQL & l_rstFiles("eventID") & ", "
                                l_strSQL = l_strSQL & l_rstFiles("libraryID") & ", "
                                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move to AutoDelete") & ", "
                                l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                                Debug.Print l_strSQL
                                ExecuteSQL l_strSQL, g_strExecuteError
                                CheckForSQLError
                                l_rstFiles.MoveNext
                            Loop
                        End If
                        l_rstFiles.Close
                        Set l_rstFiles = Nothing
                    End If
                End If
            End If
        End If
    End If
    
    
    UpdateJobStatus lp_lngJobID, "Cancelled", , True
    
    'add a history line to the jobn
    AddJobHistory lp_lngJobID, "CANCELLED JOB"
    
    ShowJob lp_lngJobID, 0, False
'    ShowJob lp_lngJobID, 0, False
    
End If


'TVCodeTools ErrorHandlerStart

PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub CenterForm(l_frmForm As Form)

On Error Resume Next
    
    
    If l_frmForm.WindowState = 1 Or l_frmForm.WindowState = 2 Then Exit Sub
    
    With l_frmForm
        If .MDIChild = True Then
            .Top = (MDIForm1.ScaleHeight - .Height) / 2
            .Left = (MDIForm1.ScaleWidth - .Width) / 2
        Else
            .Top = (Screen.Height - .Height) / 2
            .Left = (Screen.Width - .Width) / 2
        End If
    End With
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:

    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Function ChangeJobStatus(lp_lngJobID As Long) As String
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/changejobstatus") Then
        Exit Function
    End If
    
    Dim l_intStatusNumber As Integer
    Dim l_strStatus As String
    
    l_strStatus = GetData("job", "fd_status", "jobID", lp_lngJobID)
    
    l_intStatusNumber = GetStatusNumber(l_strStatus)
    
    Dim l_strJobType As String
    l_strJobType = GetData("job", "jobtype", "jobID", lp_lngJobID)
    
    'check that the job is not being re-instated from cancelled.
    'check that the project is open still - and dont allow
    ' jobs on closed projects to be opened again
    
    If l_strStatus = "Cancelled" Then
        Dim l_lngProjectID As Long
        l_lngProjectID = GetData("job", "projectID", "jobID", lp_lngJobID)
        
        If l_lngProjectID > 0 Then
            If Not IsProjectValid(l_lngProjectID) Then
                MsgBox "Just what do you think you are doing?!" & vbCrLf & vbCrLf & "You can not reinstate this job. The project it is attached to is now closed.", vbExclamation
                Exit Function
            End If
        End If
    End If
    
    If l_strStatus = "Sent To Accounts" Or l_strStatus = "Costed" Then
        If Not CheckAccess("/changesenttoaccounts") Then
            MsgBox "This job has already been Invoiced.", vbInformation, "Cannot modify status..."
            Exit Function
        End If
    End If
    
    frmJobStatusSelect.lblOriginalStatus.Caption = l_strStatus
    frmJobStatusSelect.sldStatus.Value = l_intStatusNumber
    frmJobStatusSelect.Show vbModal
    
    If frmJobStatusSelect.Tag = "xcancel" Then
        ChangeJobStatus = l_strStatus
        Exit Function
    End If
    
    l_strStatus = frmJobStatusSelect.Tag
    
    UpdateJobStatus lp_lngJobID, l_strStatus
    UpdateDespatchForJob lp_lngJobID
    
    ChangeJobStatus = l_strStatus
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    

End Function
Sub PrintJobSheet(lp_lngJobID As Long, Optional lp_strJobSheetReport As String)

Dim l_strReportToPrint  As String

If Not CheckAccess("/printjobsheet") Then Exit Sub

Dim l_strStatus As String
l_strStatus = GetData("job", "fd_status", "jobID", lp_lngJobID)

If GetStatusNumber(l_strStatus) = 0 Then
    MsgBox "You can not print a job sheet for jobs of this status", vbExclamation
    Exit Sub
End If

If LCase(g_strWhenToAllocate) = "print job sheet" Then
    'here is where we should allocate the job number
    AllocateProjectNumber lp_lngJobID
End If

If lp_strJobSheetReport <> "" Then
    l_strReportToPrint = g_strLocationOfCrystalReportFiles & lp_strJobSheetReport
Else
    l_strReportToPrint = g_strLocationOfCrystalReportFiles & "jobsheet.rpt"
End If

'increase the number of prints of this job

Dim l_intTotalPrints As Integer
l_intTotalPrints = Val(GetData("job", "totalprints", "jobID", lp_lngJobID))

SetData "job", "totalprints", "jobID", lp_lngJobID, l_intTotalPrints + 1
SetData "job", "lastprintdate", "jobID", lp_lngJobID, FormatSQLDate(Now)
SetData "job", "lastprintuser", "jobID", lp_lngJobID, g_strUserInitials

PrintCrystalReport l_strReportToPrint, "{job.jobID} = " & lp_lngJobID, g_blnPreviewReport

'add a history line to the jobn
AddJobHistory lp_lngJobID, "Printed Job Sheet"


End Sub

Function AllocateProjectNumber(lp_lngJobID As Long) As Long

Dim l_lngProjectNumber As Long
Dim l_strAllocation As String
Dim l_lngProjectID As Long

'pick up the allocation defaults
l_strAllocation = GetData("job", "joballocation", "jobID", lp_lngJobID)
l_lngProjectNumber = Val(GetData("job", "projectnumber", "jobID", lp_lngJobID))

'if conditions are correct (no project number and allocation
'type = automatic) then save new project number...
If l_lngProjectNumber = 0 And LCase(GetData("allocation", "allocationtype", "name", l_strAllocation)) = "automatic" Then
    'get the next project number
    l_lngProjectNumber = GetNextProjectNumber(l_strAllocation)
    'l_lngProjectID = SaveProject(0, 0, 0, "", "", 0, 0, "", "", "", "", "", "", l_lngProjectNumber, l_strAllocation, "", "")
    
    SetData "job", "projectID", "jobID", lp_lngJobID, l_lngProjectID
    SetData "job", "projectnumber", "jobID", lp_lngJobID, l_lngProjectNumber
    
    Dim l_strSQL As String
    l_strSQL = "INSERT INTO projectallocation (jobID, projectnumber, projectID, cdate, cuser) VALUES ('" & lp_lngJobID & "','" & l_lngProjectNumber & "','" & l_lngProjectID & "','" & FormatSQLDate(Now) & "','" & g_strUserInitials & "');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'check that the despatch entries for this job show the project number
    UpdateProjectNumbersInDespatch lp_lngJobID
    
End If

End Function
Public Function CheckAccess(lp_strAccesscode As String, Optional lp_intDontShowMessage As Integer) As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If (InStr(UCase(g_strUserAccessCode), UCase(lp_strAccesscode)) <> 0) Or (InStr(UCase(g_strUserAccessCode), "/SUPERUSER") <> 0) Then
        If InStr(UCase(g_strUserAccessCode), UCase("--" & lp_strAccesscode)) = 0 Then
            CheckAccess = True
        Else
        
            'check the security template for the correct
            
            GoTo PROC_CheckSecurityTemplate
        
            GoTo PROC_BlockedAccess
        End If
    Else

    GoTo PROC_CheckSecurityTemplate

PROC_BlockedAccess:

        If Val(lp_intDontShowMessage) = 0 Then
            MsgBox "You do not have the required access rights to use this function." & vbCrLf & vbCrLf & "If you need to use this feature, the access code you need to give to your CETA Administrator is: " & lp_strAccesscode, vbExclamation, "No Access - " & lp_strAccesscode
        End If
    End If
    
    Exit Function

PROC_CheckSecurityTemplate:

    Dim l_lngSecurityTemplateUserID  As Long
    l_lngSecurityTemplateUserID = GetData("cetauser", "securitytemplate", "cetauserID", g_lngUserID)
    
    If l_lngSecurityTemplateUserID > 0 Then
        Dim l_strSecurityAccessCode As String
        l_strSecurityAccessCode = GetData("cetauser", "accesscode", "cetauserID", l_lngSecurityTemplateUserID)
        
        CheckAccess = False
    
        If (InStr(UCase(l_strSecurityAccessCode), UCase(lp_strAccesscode)) <> 0) Or (InStr(UCase(l_strSecurityAccessCode), "/SUPERUSER") <> 0) Then
            If InStr(UCase(l_strSecurityAccessCode), UCase("--" & lp_strAccesscode)) = 0 Then
                CheckAccess = True
                Exit Function
            End If
        End If
    End If
    
    GoTo PROC_BlockedAccess
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function


Function CheckDatabaseConnection() As Boolean
    
On Error GoTo CheckDatabaseConnection_Error

Dim l_strSQL As String
l_strSQL = "SELECT count(username) FROM cetauser"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
    .CursorLocation = adUseClient
    .LockType = adLockBatchOptimistic
    .CursorType = adOpenDynamic
    .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

l_conSearch.Close
Set l_conSearch = Nothing

CheckDatabaseConnection = True

Exit Function
    
CheckDatabaseConnection_Error:
    g_strExecuteError = Err.Description
    CheckDatabaseConnection = False
    Exit Function
    
End Function

Function CheckForSQLError() As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If InStr(UCase(g_strExecuteError), "*ERROR*") <> 0 Then
        CheckForSQLError = True
        'Debug.Print g_strDebugSQLString
        Debug.Print g_strExecuteError
        Err.Raise 20001, "CheckForSQLError", g_strExecuteError
        
    Else
        CheckForSQLError = False
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description, vbExclamation
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function CheckJobDubbingsAreCompleted(lp_lngJobID As Long) As Boolean

Dim l_strSQL As String
l_strSQL = "SELECT COUNT(jobdetailID) FROM jobdetail WHERE (jobdetail.jobID = '" & lp_lngJobID & "') AND (jobdetail.completeddate IS NULL) AND (jobdetail.copytype <> 'M' AND jobdetail.copytype <> 'T' AND jobdetail.copytype <> 'O')"

Dim l_intCount As Integer

    Dim l_rstCount As New ADODB.Recordset
    Set l_rstCount = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    If l_rstCount(0) = 0 Then
        CheckJobDubbingsAreCompleted = True
    Else
        CheckJobDubbingsAreCompleted = False
    End If
    l_rstCount.Close
    Set l_rstCount = Nothing



End Function

Function CheckPasswordStrength(ByVal lp_strPassword As String) As Boolean

'this function checks a password for valid strength - as set up in the settings
Dim l_intPasswordLength As Integer
l_intPasswordLength = Len(lp_strPassword)

If l_intPasswordLength < g_intPasswordLength Then
    CheckPasswordStrength = False
    Exit Function
End If

'A' is 65 and 'Z' is 90

Dim l_intPasswordLoop As Integer

Dim l_intTotalUpperCase As Integer
Dim l_intTotalNumbers As Integer
Dim l_strCurrentChar As String

For l_intPasswordLoop = 0 To l_intPasswordLength - 1
        
    l_strCurrentChar = Mid(lp_strPassword, l_intPasswordLoop + 1, 1)
    
    Debug.Print l_strCurrentChar
    
    'it is a number?
    If IsNumeric(l_strCurrentChar) Then l_intTotalNumbers = l_intTotalNumbers + 1
    
    'it is an uppercase?
    If Asc(l_strCurrentChar) >= 65 And Asc(l_strCurrentChar) <= 90 Then l_intTotalUpperCase = l_intTotalUpperCase + 1


Next

If l_intTotalNumbers < g_intPasswordNumbers Then
    CheckPasswordStrength = False
    Exit Function
End If

If l_intTotalUpperCase < g_intPasswordUpperCase Then
    CheckPasswordStrength = False
    Exit Function
End If

CheckPasswordStrength = True

End Function

Function CheckLogon(lp_strUserName As String, lp_strPassword As String) As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strConnectionDisplay As String, Count As Long
    
    Count = InStr(g_strConnection, "SERVER")
    l_strConnectionDisplay = Mid(g_strConnection, Count)
    Count = InStr(l_strConnectionDisplay, "DATABASE")
    l_strConnectionDisplay = Left(l_strConnectionDisplay, Count - 1)
    
    If InStr(lp_strUserName, Chr(39)) <> 0 Or InStr(lp_strPassword, Chr(39)) <> 0 Then
        CheckLogon = False
        Exit Function
    End If
    
    If g_optUseMD5Passwords = 1 Then
        If lp_strPassword <> "" Then lp_strPassword = MD5Encrypt(lp_strPassword)
    End If
    
    
    Dim l_strSQL As String
    
    l_strSQL = "SELECT *, cast(accesscode as ntext) as accesscode_1 FROM cetauser WHERE username = '" & lp_strUserName & "' "
    
    If lp_strPassword = "" Then
        l_strSQL = l_strSQL & "AND fd_password = 'nopassword'"
    Else
        l_strSQL = l_strSQL & "AND fd_password = '" & lp_strPassword & "'"
    End If
    
    Dim l_rstUser As New ADODB.Recordset
    Set l_rstUser = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    If CheckForSQLError Then
        GoTo CheckLogon_Error
    End If
    
    If l_rstUser.EOF Then
        g_strUserName = ""
        g_strUserAccessCode = ""
        g_strUserInitials = ""
        g_strFullUserName = ""
        g_strUserEmailAddress = ""
        g_lngUserID = 0
        g_datLastChangedPasswordDate = vbEmpty
        g_intPasswordNeverExpires = 0
        CheckLogon = False
    Else
        g_strUserName = lp_strUserName
        g_strUserAccessCode = Trim(" " & l_rstUser("accesscode"))
        g_strUserInitials = Format(l_rstUser("initials"))
        g_strFullUserName = Format(l_rstUser("fullname"))
        g_strUserEmailAddress = Format(l_rstUser("email"))
        g_lngUserID = l_rstUser("cetauserID")
        g_intPasswordNeverExpires = GetFlag(l_rstUser("passwordneverexpires"))
        If IsDate(l_rstUser("lastchangedpassworddate")) Then
            g_datLastChangedPasswordDate = l_rstUser("lastchangedpassworddate")
        Else
            g_datLastChangedPasswordDate = vbEmpty
        End If
        g_strWorkstation = CurrentMachineName
        CheckLogon = True
    End If
    
    l_rstUser.Close
    Set l_rstUser = Nothing
    
    If CheckLogon = True Then
        If Not CheckAccess("/allowlogon") Then
            
            CheckLogon = False
        End If
    End If
    
    If CheckLogon = True Then
    
        'this checks the users last changed password date
    
        If g_intPasswordChangeEvery > 0 And g_intPasswordNeverExpires = 0 Then
            Dim l_lngDaysSinceLastChange As Long
            l_lngDaysSinceLastChange = DateDiff("d", g_datLastChangedPasswordDate, Date)
            
            Dim l_lngDaysRemaining As Long
            l_lngDaysRemaining = g_intPasswordChangeEvery - l_lngDaysSinceLastChange
            
            Select Case l_lngDaysRemaining
            Case Is < 0
                'this account should now lock
                MsgBox "Sorry, your account has expired. Please check with your CETA manager to set a new password.", vbExclamation
                
                CheckLogon = False
                
            Case 1 To 5
                'this account should warn the user to change
                
                Dim l_intMsg As Integer
                l_intMsg = MsgBox("You have " & l_lngDaysRemaining & IIf(l_lngDaysRemaining > 1, " days", " day") & " remaining to change your password. Do you want to change it now?", vbYesNo + vbCritical)
                
                If l_intMsg = vbYes Then
                    ChangeMyPassword
                End If
                
            Case Is > 5
                'do nothing
                
            End Select
            
        End If
    End If
    
    
    If CheckLogon = True Then
        l_strSQL = "INSERT INTO session (userID, username, workstation, logindate) VALUES ("
        l_strSQL = l_strSQL & "'" & g_lngUserID & "', "
        l_strSQL = l_strSQL & "'" & g_strUserName & "', "
        l_strSQL = l_strSQL & "'" & g_strWorkstation & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "') "
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        g_lngSessionID = g_lngLastID
        MDIForm1.Caption = "CETA Facilities Management " & App.Major & "." & App.Minor & "." & App.Revision & " - [Logged in as: " & g_strFullUserName & "]" & " " & l_strConnectionDisplay
    End If
    
    Exit Function
CheckLogon_Error:
    MsgBox "An error occured while trying to log on. The error received was: " & vbCrLf & vbCrLf & g_strExecuteError, vbExclamation
    Set l_rstUser = Nothing
    CheckDatabaseConnection
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume 'PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function CheckPassword(l_strUserName As String) As Boolean
    
    'check the currently logged in user password and check it
    'against the database to ensure they are who they say they
    'are
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strDatabasePassword As String
    Dim l_strUserPassword As String
    
    'get the current password from the database
    l_strDatabasePassword = GetData("cetauser", "fd_password", "username", l_strUserName)
    
    'make sure they haven't got "nopassword" which is an auto login thing
    If UCase(l_strDatabasePassword) = "NOPASSWORD" Then
        
        'if they do then get out and set the value to cool
        CheckPassword = False
        
        'tell them they are very naughty
        MsgBox "You do not currently have a password set, please set one before attempting this function", vbExclamation
        
        Exit Function
        
    End If
    
    'show the password form
    frmUserPassword.Show vbModal
    
    'keep the password
    l_strUserPassword = frmUserPassword.Tag
    
    'unload the form
    Unload frmUserPassword
    Set frmUserPassword = Nothing
    
        
    If g_optUseMD5Passwords = 1 Then
        Dim oMD5 As New CMD5
        Set oMD5 = New CMD5
        l_strUserPassword = oMD5.MD5(l_strUserPassword)
        Set oMD5 = Nothing
    End If
    
    'if not then check what they typed is the same as the database
    If UCase(l_strDatabasePassword) <> UCase(l_strUserPassword) Then
        
        'if not then return false
        CheckPassword = False
        
        'tell them they are a monkey
        MsgBox "That is the incorrect password for this user, '" & l_strUserName & "'", vbExclamation
        
    Else
        
        'everything is cool
        CheckPassword = True
        
    End If
    
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetListIndex(lp_conControl As Control, lp_strTextToFind As String) As Integer

Dim l_intLoop As Integer
For l_intLoop = 0 To lp_conControl.ListCount - 1
    If lp_conControl.List(l_intLoop) = lp_strTextToFind Then
        GoTo PROC_FOUND_TEXT
    End If
Next

GetListIndex = -1

Exit Function

PROC_FOUND_TEXT:

GetListIndex = l_intLoop
Exit Function

End Function

Sub ClearFields(lp_frmForm As Form)
    
    On Error Resume Next
    
    Dim l_intLoop As Integer
    
    For l_intLoop = 0 To lp_frmForm.Controls.Count - 1
        If lp_frmForm.Controls(l_intLoop).Tag <> "NOCLEAR" Then
            If TypeOf lp_frmForm.Controls(l_intLoop) Is TextBox Then
                lp_frmForm.Controls(l_intLoop).Text = ""
            ElseIf TypeOf lp_frmForm.Controls(l_intLoop) Is ComboBox Then
                If lp_frmForm.Controls(l_intLoop).Tag <> "TIME" Then
                    If lp_frmForm.Controls(l_intLoop).Tag = "CLEAR" Then
                        lp_frmForm.Controls(l_intLoop).Text = ""
                        lp_frmForm.Controls(l_intLoop).Clear
                    ElseIf lp_frmForm.Controls(l_intLoop).Style <> 2 Then
                        lp_frmForm.Controls(l_intLoop).Text = ""
                    End If
                End If
            ElseIf TypeOf lp_frmForm.Controls(l_intLoop) Is DTPicker Then
                lp_frmForm.Controls(l_intLoop).Value = Date
                If lp_frmForm.Controls(l_intLoop).Tag = "NOCHECK" Then
                    lp_frmForm.Controls(l_intLoop).Value = Null
                End If
            ElseIf TypeOf lp_frmForm.Controls(l_intLoop) Is SSOleDBCombo Then
                lp_frmForm.Controls(l_intLoop).Text = ""
            ElseIf TypeOf lp_frmForm.Controls(l_intLoop) Is Label Then
                If Val(lp_frmForm.Controls(l_intLoop).Tag) <> 0 Then
                    lp_frmForm.Controls(l_intLoop).Caption = "Custom Fld " & lp_frmForm.Controls(l_intLoop).Tag
                ElseIf InStr(UCase(lp_frmForm.Controls(l_intLoop).Tag), "CLEARFIELDS") <> 0 Then
                    lp_frmForm.Controls(l_intLoop).Caption = ""
                End If
            ElseIf TypeOf lp_frmForm.Controls(l_intLoop) Is CheckBox Then
                Debug.Print lp_frmForm.Controls(l_intLoop).Name
                If InStr(lp_frmForm.Controls(l_intLoop).Tag, "SET") <= 0 Then
                    lp_frmForm.Controls(l_intLoop).Value = 0
                Else
                    lp_frmForm.Controls(l_intLoop).Value = 1
                End If
            ElseIf TypeOf lp_frmForm.Controls(l_intLoop) Is OptionButton Then
                If InStr(lp_frmForm.Controls(l_intLoop).Tag, "SET") <= 0 Then
                    Debug.Print lp_frmForm.Controls(l_intLoop).Name
                    lp_frmForm.Controls(l_intLoop).Value = False
                Else
                    lp_frmForm.Controls(l_intLoop).Value = True
                End If
            End If
        End If
    Next
    
End Sub

Sub TerseCompleteJobDetailItem(lp_lngJobDetailID As Long)

If Not CheckAccess("/completejobdetailitem") Then Exit Sub

Dim l_lngJobID As Long, l_strSQL As String

l_lngJobID = GetData("jobdetail", "jobID", "jobdetailID", lp_lngJobDetailID)

SetData "jobdetail", "completeddate", "jobdetailID", lp_lngJobDetailID, FormatSQLDate(Now)
SetData "jobdetail", "completeduser", "jobdetailID", lp_lngJobDetailID, g_strUserInitials
SetData "disneytracker", "workdone", "jobdetailID", lp_lngJobDetailID, -1

'it should check here to see if it is the last item on the job, and the whole job shoud
'get completed if it is.
l_strSQL = "SELECT count(jobdetailID) FROM jobdetail WHERE jobID = '" & l_lngJobID & "' AND copytype <> 'M' AND copytype <> 'T' AND completeddate IS NULL"
Dim l_rstCount As New ADODB.Recordset
Set l_rstCount = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

Dim l_intCount As Integer
l_intCount = l_rstCount(0)

If l_intCount = 0 Then
    'this job is completed
    Dim l_intMessage As Integer
    
    UpdateJobStatus l_lngJobID, "VT Done"
    ShowJob Val(l_lngJobID), g_intDefaultTab, False
'    ShowJob Val(l_lngJobID), g_intDefaultTab, False
    
    If g_optCompleteJobsWhenVTCompleted = 1 Then

            'check to see if this is a VT only type job?
            
        If Left(LCase(GetData("job", "jobtype", "jobID", l_lngJobID)), 3) = "dub" Then

            l_intMessage = MsgBox("You have completed the last dubbing item on this job. Do you want to mark the job as completed?", vbQuestion + vbYesNo)
            If l_intMessage = vbYes Then
                    
                
                Dim l_datActualEndDate As Variant
                l_datActualEndDate = GetData("job", "actualenddate", "jobID", l_lngJobID)
                If Not IsDate(l_datActualEndDate) Then
                    SetData "job", "actualenddate", "jobID", l_lngJobID, FormatSQLDate(Now)
                End If
                
                UpdateJobStatus l_lngJobID, "Completed"
            End If
        
        End If
        
    End If
End If

l_rstCount.Close
Set l_rstCount = Nothing

End Sub

Sub TerseRejectJobDetailItem(lp_lngJobDetailID As Long, lp_blnClear As Boolean)

Dim l_datStart As Date, l_datFinish As Date, l_lngCount As Long, l_rst As ADODB.Recordset

If Not CheckAccess("/rejectjobdetailitem") Then Exit Sub

If lp_blnClear = True Then
    If GetData("jobdetail", "rejecteddate", "jobdetailID", lp_lngJobDetailID) <> 0 Then
        l_datStart = GetData("jobdetail", "rejecteddate", "jobdetailID", lp_lngJobDetailID)
        l_datFinish = Now
        l_lngCount = DateDiff("d", l_datStart, l_datFinish)
        SetData "jobdetail", "offrejecteddate", "jobdetailID", lp_lngJobDetailID, FormatSQLDate(l_datFinish)
        SetData "jobdetail", "daysonrejected", "jobdetailID", lp_lngJobDetailID, Val(Trim(" " & GetData("jobdetail", "daysonrejected", "jobdetailID", lp_lngJobDetailID))) + l_lngCount
        If GetData("jobdetail", "Masterarrived", "jobdetailID", lp_lngJobDetailID) <> 0 And GetData("jobdetail", "DateTapeSentBack", "jobdetailID", lp_lngJobDetailID) = 0 Then
            Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=" & GetData("jobdetail", "TargetDateCountdown", "jobdetailID", lp_lngJobDetailID), g_strExecuteError)
            SetData "jobdetail", "Targetdate", "jobdetailID", lp_lngJobDetailID, FormatSQLDate(l_rst(0))
            l_rst.Close
            Set l_rst = Nothing
        End If
    End If
    SetData "jobdetail", "rejecteddate", "jobdetailID", lp_lngJobDetailID, Null
    SetData "jobdetail", "rejecteduser", "jobdetailID", lp_lngJobDetailID, Null
Else
    SetData "jobdetail", "rejecteddate", "jobdetailID", lp_lngJobDetailID, FormatSQLDate(Now)
    SetData "jobdetail", "rejecteduser", "jobdetailID", lp_lngJobDetailID, g_strUserInitials
    If GetData("jobdetail", "FirstRejectedDate", "jobdetailID", lp_lngJobDetailID) = 0 Then SetData "Jobdetail", "FirstRejectedDate", "jobdetailID", lp_lngJobDetailID, FormatSQLDate(Now)
    SetData "jobdetail", "targetdate", "jobdetailID", lp_lngJobDetailID, Null
    SetData "jobdetail", "offrejecteddate", "jobdetailID", lp_lngJobDetailID, Null
End If

End Sub

Function CompleteJobDetailItem(lp_lngJobDetailID As Long) As Boolean

If Not CheckAccess("/completejobdetailitem") Then Exit Function

Dim l_lngJobID As Long
Dim l_lngCompletedID As Long
Dim l_strNotesToAdd As String

'frmCompleteJob.lblJobDetailID.Caption = lp_lngJobDetailID
'frmCompleteJob.cmdLoadCompletionDetails.Value = True
'frmCompleteJob.Show vbModal

If frmCompleteJob.Tag = "CANCELLED" Then
    CompleteJobDetailItem = False
Else
    CompleteJobDetailItem = True
    
    With frmCompleteJob
    
        l_lngJobID = Val(.lblJobID.Caption)
        l_lngCompletedID = SaveCompletedDetails(Val(.lblJobID.Caption), Val(.lblJobDetailID.Caption), .lstOperator.List(.lstOperator.ListIndex), Val(.txtDuration.Text), Val(.txtItems.Text), .cmbStockType.Text, .txtNotes.Text, Val(.chkArc.Value), Val(.chkFlash.Value), Val(.chkLegal.Value), Val(.chkConversion.Value), .cmbSourceMachine.Text, .cmbDestinationMachine.Text, Val(.txtDownTime.Text), Val(.txtQuantity.Text))
        l_strNotesToAdd = .txtJobDetails.Text
        If l_strNotesToAdd <> "" Then InsertNote .lblJobID.Caption, "notes4", l_strNotesToAdd 'accounts
    End With
    
    Dim l_strSQL As String
    l_strSQL = "UPDATE jobdetail SET runningtime = '" & Format(frmCompleteJob.txtRunningTimeMinutes.Text, "00") & ":" & Format(frmCompleteJob.txtRunningTimeSeconds.Text, "00") & "', stock1type = '" & frmCompleteJob.cmbStockType.Text & "', stock1ours = '" & frmCompleteJob.txtQuantity.Text & "' WHERE jobdetailID = '" & lp_lngJobDetailID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'it should check here to see if it is the last item on the job, and the whole job shoud
    'get completed if it is.
    l_strSQL = "SELECT count(jobdetailID) FROM jobdetail WHERE jobID = '" & l_lngJobID & "' AND copytype <> 'M' AND copytype <> 'T' AND completeddate IS NULL"
    Dim l_rstCount As New ADODB.Recordset
    Set l_rstCount = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    Dim l_intCount As Integer
    l_intCount = l_rstCount(0)
    
    If l_intCount = 0 Then
        'this job is completed
        Dim l_intMessage As Integer
        
        UpdateJobStatus l_lngJobID, "VT Done"
        
        If g_optCompleteJobsWhenVTCompleted = 1 Then

                'check to see if this is a VT only type job?
                
            If Left(LCase(GetData("job", "jobtype", "jobID", l_lngJobID)), 3) = "dub" Then

                l_intMessage = MsgBox("You have completed the last dubbing item on this job. Do you want to mark the job as completed?", vbQuestion + vbYesNo)
                If l_intMessage = vbYes Then
                        
                    
                    Dim l_datActualEndDate As Variant
                    l_datActualEndDate = GetData("job", "actualenddate", "jobID", l_lngJobID)
                    If Not IsDate(l_datActualEndDate) Then
                        SetData "job", "actualenddate", "jobID", l_lngJobID, FormatSQLDate(Now)
                    End If
                    
                    UpdateJobStatus l_lngJobID, "Completed"
                End If
            
            End If
            
        End If
    End If
    
    l_rstCount.Close
    Set l_rstCount = Nothing
    
End If

Unload frmCompleteJob
Set frmCompleteJob = Nothing

End Function
Function SaveCompletedDetails(lp_lngJobID As Long, lp_lngJobDetailID As Long, lp_strOperatorName As String, lp_dblOperatorDuration As Double, lp_intItemCount As Integer, lp_strStockType As String, lp_strNotes As String, lp_intUsedArc As Integer, lp_intUsedFlash As Integer, lp_intUsedLegal As Integer, lp_intUsedConversion As Integer, lp_strSourceMachine As String, lp_strDestinationMachine As String, lp_dblDownTime As Double, lp_sngQuantity As Single) As Long

'first, delete any other completed information

Dim l_strSQL As String

l_strSQL = "DELETE FROM completed WHERE jobdetailID = '" & lp_lngJobDetailID & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

'now add the new line
    l_strSQL = "INSERT INTO completed ("
    l_strSQL = l_strSQL & "jobID, "
    l_strSQL = l_strSQL & "jobdetailID, "
    l_strSQL = l_strSQL & "operatorduration, "
    l_strSQL = l_strSQL & "itemcount, "
    l_strSQL = l_strSQL & "quantity, "
    l_strSQL = l_strSQL & "stocktype, "
    l_strSQL = l_strSQL & "usedarc, "
    l_strSQL = l_strSQL & "usedflash, "
    l_strSQL = l_strSQL & "usedlegal, "
    l_strSQL = l_strSQL & "usedconversion, "
    l_strSQL = l_strSQL & "notes, "
    l_strSQL = l_strSQL & "completeddate, "
    l_strSQL = l_strSQL & "completeduser, "
    l_strSQL = l_strSQL & "sourcemachine, "
    l_strSQL = l_strSQL & "destinationmachine, "
    l_strSQL = l_strSQL & "downtime) VALUES ("
    
    ' --------------
    
    l_strSQL = l_strSQL & "'" & lp_lngJobID & "', "
    l_strSQL = l_strSQL & "'" & lp_lngJobDetailID & "', "
    l_strSQL = l_strSQL & "'" & lp_dblOperatorDuration & "', "
    l_strSQL = l_strSQL & "'" & lp_intItemCount & "', "
    l_strSQL = l_strSQL & "'" & lp_sngQuantity & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strStockType) & "', "
    l_strSQL = l_strSQL & "'" & lp_intUsedArc & "', "
    l_strSQL = l_strSQL & "'" & lp_intUsedFlash & "', "
    l_strSQL = l_strSQL & "'" & lp_intUsedLegal & "', "
    l_strSQL = l_strSQL & "'" & lp_intUsedConversion & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strNotes) & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now()) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strOperatorName) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strSourceMachine) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strDestinationMachine) & "', "
    l_strSQL = l_strSQL & "'" & lp_dblDownTime & "')"
    '--------------------
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'tick off the job detail line
    l_strSQL = "UPDATE jobdetail SET completeddate = '" & FormatSQLDate(Now) & "', completeduser = '" & QuoteSanitise(lp_strOperatorName) & "' WHERE jobdetailID = '" & lp_lngJobDetailID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    AddJobHistory lp_lngJobID, "Completed VT (" & lp_sngQuantity & "x" & frmCompleteJob.lblFormat.Caption & ")"
    
    SaveCompletedDetails = g_lngLastID

End Function

Sub CopyQuoteLines(lp_lngSourceQuoteID As Long, lp_lngDestinationQuoteID As Long)

Dim l_conMain As New ADODB.Connection
l_conMain.Open g_strConnection

Dim l_strSQL As String
l_strSQL = "SELECT * FROM quotedetail WHERE quoteID = '" & lp_lngSourceQuoteID & "';"

Dim l_rstSourceDetail As New ADODB.Recordset
l_rstSourceDetail.Open l_strSQL, l_conMain, adOpenForwardOnly, adLockReadOnly

Do While Not l_rstSourceDetail.EOF
    
    l_strSQL = "INSERT INTO quotedetail ("
    l_strSQL = l_strSQL & "quoteID, "
    l_strSQL = l_strSQL & "cetacode, "
    l_strSQL = l_strSQL & "quotedunitprice, "
    l_strSQL = l_strSQL & "quotedunits, "
    l_strSQL = l_strSQL & "discount, "
    l_strSQL = l_strSQL & "quotedtotal, "
    l_strSQL = l_strSQL & "actualunits, "
    l_strSQL = l_strSQL & "actualtotal, "
    l_strSQL = l_strSQL & "chargedtotal, "
    l_strSQL = l_strSQL & "description, "
    l_strSQL = l_strSQL & "cdate, "
    l_strSQL = l_strSQL & "cuser, "
    l_strSQL = l_strSQL & "fd_order, "
    l_strSQL = l_strSQL & "ratecardunitcharge, "
    l_strSQL = l_strSQL & "ratecardtotal, "
    l_strSQL = l_strSQL & "ratecardcategory, "
    l_strSQL = l_strSQL & "ratecardorderby) VALUES ("
    

    l_strSQL = l_strSQL & "'" & lp_lngDestinationQuoteID & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("cetacode") & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("quotedunitprice") & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("quotedunits") & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("discount") & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("quotedtotal") & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("actualunits") & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("actualtotal") & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("chargedtotal") & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstSourceDetail("description")) & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("fd_order") & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("ratecardunitcharge") & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("ratecardtotal") & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstSourceDetail("ratecardcategory")) & "', "
    l_strSQL = l_strSQL & "'" & l_rstSourceDetail("ratecardorderby") & "');"

    
    l_conMain.Execute l_strSQL
    
    l_rstSourceDetail.MoveNext
    
Loop

l_rstSourceDetail.Close
Set l_rstSourceDetail = Nothing

End Sub

Function DuplicateJob(lp_lngJobID As Long, Optional lp_strType As String) As Long

'copy the info from one job to another
Dim l_rstOriginalJob As ADODB.Recordset
Dim l_rstNewJob As ADODB.Recordset

'get the original job's details
Dim l_strSQL As String
l_strSQL = "SELECT * FROM job WHERE jobID = '" & lp_lngJobID & "';"

Set l_rstOriginalJob = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

'allow adding of a new job
l_strSQL = "SELECT * FROM job WHERE jobID = '-1';"

Set l_rstNewJob = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

Dim l_intLoop As Integer

Dim l_lngBatchNumber As String
l_lngBatchNumber = GetNextSequence("repeatbatchnumber")

Dim l_strProjectStatus  As String
l_strProjectStatus = GetData("project", "fd_status", "projectnumber", l_rstOriginalJob("projectnumber"))

'if this project is closed, then stop this repeat
Select Case l_strProjectStatus
Case "COMPLETED", "CANCELLED", "CANCEL", "INVOICED", "Sent To Accounts"
    MsgBox "You can not repeat this job as the project is closed. Please create your first job manually using the correct (new?) project number and then use that job as the basis for your repeat.", vbExclamation
    l_rstNewJob.Close
    Set l_rstNewJob = Nothing
    l_rstOriginalJob.Close
    Set l_rstOriginalJob = Nothing
    Exit Function
End Select


l_rstNewJob.AddNew



For l_intLoop = 0 To l_rstOriginalJob.Fields.Count - 1
    If l_rstOriginalJob.Fields(l_intLoop).Name <> "jobID" Then
    
        l_rstNewJob(l_intLoop) = l_rstOriginalJob(l_intLoop)
        
    
    End If
    
Next
'blank out fields which are specific to the ORIGINAL job only

l_rstNewJob("repeatbatchID") = l_lngBatchNumber

l_rstNewJob("createddate") = FormatSQLDate(Now)
l_rstNewJob("createduser") = g_strUserInitials

l_rstNewJob("modifieduser") = Null
l_rstNewJob("modifieddate") = Null

l_rstNewJob("senttoaccountsdate") = Null
l_rstNewJob("senttoaccountsuser") = Null
l_rstNewJob("senttoaccountsbatchnumber") = Null
l_rstNewJob("holdcostdate") = Null
l_rstNewJob("holdcostuser") = Null
l_rstNewJob("costeddate") = Null
l_rstNewJob("costeduser") = Null
l_rstNewJob("totalprints") = Null
l_rstNewJob("lastprintdate") = Null
l_rstNewJob("lastprintuser") = Null

l_rstNewJob("completeddate") = Null
l_rstNewJob("completeduser") = Null
l_rstNewJob("completeduserID") = Null

l_rstNewJob("actualstartdate") = Null
l_rstNewJob("actualenddate") = Null

l_rstNewJob("costingsheetID") = Null
l_rstNewJob("addedtocostingsheetdate") = Null
l_rstNewJob("addedtocostingsheetuser") = Null

'Credit notes need to retain the original Invoice number - but otherwise it should be cleared.
If lp_strType <> "CREDIT" Then
    l_rstNewJob("invoicenumber") = Null
    l_rstNewJob("invoiceddate") = Null
    l_rstNewJob("invoiceduser") = Null
    l_rstNewJob("invoiceduserID") = Null
End If

l_rstNewJob("vtstartdate") = Null
l_rstNewJob("vtendtime") = Null
l_rstNewJob("vtstartuser") = Null
l_rstNewJob("vtenduser") = Null
l_rstNewJob("operatorduration") = Null
l_rstNewJob("sourcemachine") = Null
l_rstNewJob("destinationmachine") = Null
l_rstNewJob("setuptime") = Null
l_rstNewJob("qctime") = Null
l_rstNewJob("downtime") = Null

l_rstNewJob("creditnotenumber") = Null
l_rstNewJob("creditnotebatchnumber") = Null
l_rstNewJob("creditnotedate") = Null
l_rstNewJob("creditnoteuser") = Null
l_rstNewJob("creditnoteuserID") = Null
l_rstNewJob("creditsenttoaccountsdate") = Null
l_rstNewJob("creditsenttoaccountsuser") = Null
l_rstNewJob("refundedcreditnotenumber") = Null

l_rstNewJob.Update

'MsgBox g_lngLastID, vbExclamation

'close the original record
l_rstOriginalJob.Close
Set l_rstOriginalJob = Nothing

'close the new record
l_rstNewJob.Close
Set l_rstNewJob = Nothing

Dim l_lngJobID As Long
l_lngJobID = Val(GetData("job", "jobID", "repeatbatchID", l_lngBatchNumber))

AddJobHistory l_lngJobID, "Duplicated job from ID: " & lp_lngJobID

DuplicateJob = l_lngJobID

End Function

 
Function CreateNewResourceSchedule(ByVal lp_lngJobID As Long, ByVal lp_lngResourceID As Long, ByVal lp_datStartTime As Date, ByVal lp_datEndTime As Date, ByVal lp_strStatus As String, Optional l_lngConflictingResourceScheduleID As Long, Optional lp_blnDontShowMessages As Boolean, Optional lp_lngRepeatBatchID As Long, Optional lp_intShowOnQuote As Integer, Optional lp_lngMasterResourceScheduleID As Long) As Long
   'first check for coinflicts
   'TVCodeTools ErrorEnablerStart
   On Error GoTo PROC_ERR
   'TVCodeTools ErrorEnablerEnd

   Dim l_lngConflict As Long

PROC_Retry:
        
    'this checks to see if the job started in the past.
    'if it did, and the job is a HIRE job - it asks if they want to add the resource
    'but with the start date starting today
    If UCase(GetData("job", "jobtype", "jobID", lp_lngJobID)) = "HIRE" Then
        If DateDiff("h", lp_datStartTime, Now) > 1 Then
            Dim l_intMsg As Integer
            l_intMsg = MsgBox("As this is a hire job, do you want to add this resource using todays date (" & SnapTime(Now) & ") as the start date of the job?", vbYesNo + vbQuestion)
            If l_intMsg = vbYes Then
                lp_datStartTime = SnapTime(Now())
            End If
        End If
    End If

   If Not IsGeneric(lp_lngResourceID) Then
       l_lngConflict = IsConflict(lp_lngJobID, lp_lngResourceID, lp_datStartTime, lp_datEndTime, True)

       'If IsStaff(lp_lngResourceID) Then
           Dim l_strHoliday As String
           l_strHoliday = IsHoliday(lp_lngResourceID, Format(lp_datStartTime, "dd/mm/yyyy"), Format(lp_datEndTime, "dd/mmm/yyyy"))
           If l_strHoliday <> "" Then
               MsgBox "This person (" & GetData("resource", "name", "resourceID", lp_lngResourceID) & ") is marked as " & l_strHoliday & "during this period (starting @ " & lp_datStartTime & ")." & vbCrLf & vbCrLf & "At this point, CFM will only warn you about this! Please adjust your bookings manually!!", vbExclamation
               'CreateNewResourceSchedule = False
               'Exit Function
           End If
       'End If

   End If

   If l_lngConflict > 0 And GetStatusNumber(lp_strStatus) > 1 Then
       MsgBox "You can not add this resource to this job, as it will cause a conflict with '" & GetData("job", "companyname", "jobID", GetData("resourceschedule", "jobID", "resourcescheduleID", l_lngConflict)) & "' at " & GetData("resourceschedule", "starttime", "resourcescheduleID", l_lngConflict) & " and your job has already been confirmed.", vbExclamation
       'for hire jobs, we should allow this resource to be added with todays date as the start date
        
       Exit Function
   End If

   'brings back the resource schedule ID

   Dim l_strSQL As String
   Dim l_intPencilLevel  As Integer

   Dim l_strSerialNumber As String
   l_strSerialNumber = GetData("resource", "serialnumber", "resourceID", lp_lngResourceID)

Addresource:

   Dim l_lngConflictingJobID  As Long
   Dim l_strResourceRole As String
   Dim l_strResourceName As String


   l_strResourceRole = GetData("resource", "subcategory1", "resourceID", lp_lngResourceID)

   l_strResourceName = GetData("resource", "name", "resourceID", lp_lngResourceID)

   If LCase(Left(l_strResourceName, 4)) = "hire" Then
       l_strSerialNumber = ""

   End If

   If lp_strStatus = "Despatched" Or lp_strStatus = "Prepared" Or lp_strStatus = "Returned" Or lp_strStatus = "Completed" Then
       lp_strStatus = "Confirmed"
   End If

   If l_lngConflict = False Or lp_strStatus = "2nd Pencil" Then

       'insert a row in the job detail
        
        Dim l_intGeneric As Integer
        
        If IsGeneric(lp_lngResourceID) Then
            l_intGeneric = 1
        Else
            l_intGeneric = 0
        End If
       
       l_strSQL = "INSERT INTO resourceschedule (jobID, resourcename, createduser,createddate, resourceID, starttime, endtime, fd_status, role, pencillevel,conflictingresourcescheduleID, repeatbatchID, serialnumber,masterresourcescheduleID, showonquote, generic) VALUES ('" & lp_lngJobID & "', '" & QuoteSanitise(l_strResourceName) & "', '" & g_strUserInitials & "', '" & FormatSQLDate(Now) & "', '" & lp_lngResourceID & "', '" & FormatSQLDate(lp_datStartTime) & "', '" & FormatSQLDate(lp_datEndTime) & "', '" & lp_strStatus & "', '" & QuoteSanitise(l_strResourceRole) & "', '" & l_intPencilLevel & "', '" & l_lngConflictingResourceScheduleID & "', '" & lp_lngRepeatBatchID & "', '" & QuoteSanitise(l_strSerialNumber) & "', '" & lp_lngMasterResourceScheduleID & "', '" & lp_intShowOnQuote & "', '" & l_intGeneric & "')"
       ExecuteSQL l_strSQL, g_strExecuteError
       CheckForSQLError

       CreateNewResourceSchedule = g_lngLastID

       AddJobHistory lp_lngJobID, "Added Resource (" & l_strResourceName & ")"
       If l_lngConflictingResourceScheduleID <> 0 Then AddJobHistory lp_lngJobID, "Resource Conflicts (" & l_strResourceName & ") with jobID: " & l_lngConflictingJobID & " - Added as " & lp_strStatus

   Else
       'this is conflicting, so make a note of it - regardless of status
       l_lngConflictingResourceScheduleID = l_lngConflict

       If lp_blnDontShowMessages <> True Then

           'conflict found - prompt for action (for now, just 2nd pencil)


           l_lngConflictingJobID = GetData("resourceschedule", "jobID", "resourcescheduleID", l_lngConflict)

           l_intMsg = MsgBox("A resource you are about to schedule (" & l_strResourceName & ") is conflicting with another job (ID: " & l_lngConflictingJobID & ")." & vbCrLf & vbCrLf & "Do you want to make it a second pencil?", vbYesNoCancel + vbQuestion)
           Select Case l_intMsg
           Case vbYes

               lp_strStatus = "2nd Pencil"
               GoTo Addresource
           Case vbNo

               l_lngConflict = False
               GoTo Addresource
           Case vbCancel
               CreateNewResourceSchedule = False
           End Select
       Else
           l_lngConflict = False
           GoTo Addresource
       End If

   End If


   'TVCodeTools ErrorHandlerStart
PROC_EXIT:
   Exit Function

PROC_ERR:
   MsgBox Err.Description
   Resume PROC_EXIT
   'TVCodeTools ErrorHandlerEnd

End Function
Public Function CurrentMachineName() As String
    
        
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim lSize As Long
    Dim sBuffer As String
    sBuffer = Space$(MAX_COMPUTERNAME_LENGTH + 1)
    lSize = Len(sBuffer)
    
    If GetComputerName(sBuffer, lSize) Then
        CurrentMachineName = Left$(sBuffer, lSize)
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function DecodeMacroSearch(lp_strMacroSearch As String) As String
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_intDaysFromMonday  As Integer
    l_intDaysFromMonday = Weekday(Date) - 1
    
    
    'first get the syntax from the description
    l_strSQL = GetData("xref", "information", "description", lp_strMacroSearch)
    
    l_strSQL = Replace(l_strSQL, "{userinitials}", g_strUserInitials)
    l_strSQL = Replace(l_strSQL, "{today}", FormatSQLDate(Date))
    l_strSQL = Replace(l_strSQL, "{tomorrow}", FormatSQLDate(DateAdd("d", 1, Date)))
    l_strSQL = Replace(l_strSQL, "{yesterday}", FormatSQLDate(DateAdd("d", -1, Date)))
    l_strSQL = Replace(l_strSQL, "{thismonday}", FormatSQLDate(DateAdd("d", l_intDaysFromMonday, Date)))
    
    DecodeMacroSearch = l_strSQL
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub DeleteAllMyTemporaryRepeats(lp_lngRepeatBatchID As Long)

If lp_lngRepeatBatchID = 0 Then Exit Sub

Dim l_strSQL As String
l_strSQL = "DELETE FROM job WHERE repeatbatchID = '" & lp_lngRepeatBatchID & "' AND fd_status = 'Repeat';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_strSQL = "DELETE FROM resourceschedule WHERE repeatbatchID = '" & lp_lngRepeatBatchID & "' AND fd_status = 'Repeat';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Sub

Function DeleteCompany(lp_lngCompanyID As Long) As Boolean

If lp_lngCompanyID = 0 Then
    MsgBox "Invalid company selected", vbExclamation
    Exit Function
End If

If Not CheckAccess("/deletecompany") Then Exit Function

Dim l_intJobs As Integer
l_intJobs = GetCount("SELECT count(jobID) FROM job WHERE companyID = '" & lp_lngCompanyID & "';")

Dim l_intCancelledJobs As Integer
l_intCancelledJobs = GetCount("SELECT count(jobID) FROM job WHERE companyID = '" & lp_lngCompanyID & "' AND fd_status = 'Cancelled';")

Dim l_intMsg As Integer
l_intMsg = MsgBox("Do you really want to deactivate this company? " & vbCrLf & vbCrLf & IIf(l_intJobs > 0, "There are " & l_intJobs & " jobs in the system for them (" & l_intCancelledJobs & " cancelled).", "There are no jobs booked for them."), vbQuestion + vbYesNo)

If l_intMsg = vbYes Then
    
    Dim l_strSQL As String
    
    l_strSQL = "UPDATE company SET system_active = 0 WHERE companyID = '" & lp_lngCompanyID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
'    l_strSQL = "DELETE FROM employee WHERE companyID = '" & lp_lngCompanyID & "';"
'    ExecuteSQL l_strSQL, g_strExecuteError
'    CheckForSQLError

    DeleteCompany = True
    
End If

End Function

Function DeleteContact()
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/deletecontact") Then
        Exit Function
    End If
    
    If Val(frmContact.txtContactID.Text) = 0 Then
        Exit Function
    End If
    
    Dim l_contactID As Long
    Dim l_strSQL As String
    Dim l_intResponse As Integer
    
    l_contactID = Val(frmContact.txtContactID.Text)
    
    If l_contactID = 475 Then
        SendSMTPMail g_strAdministratorEmailAddress, g_strAdministratorEmailName, "Attempt to delete contact 475", "", "By " & g_strFullUserName & " on " & g_strWorkstation & " at " & Now, True, "", ""
        MsgBox "Cannot Delete Contact", vbCritical, "Error"
        Exit Function
    End If
    
    l_intResponse = MsgBox("Do you really wish to deactivate contact '" & frmContact.cmbContact.Text & "'", vbYesNo + vbQuestion, "Delete Contact")
    
    If l_intResponse = vbNo Then
        
        Exit Function
        
    Else
        
        'Deactivate all employee entries for this contact ID
        
        l_strSQL = "UPDATE employee SET system_active = 0 WHERE contactID = '" & l_contactID & "'"

        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError

        'Then finally deactivate the contact
        l_strSQL = "UPDATE contact SET system_active = 0 WHERE contactID = '" & l_contactID & "'"
        
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        ClearFields frmContact
        ShowContact 0
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function IsGeneric(lp_lngResourceID As Long) As Boolean

IsGeneric = GetFlag(GetData("resource", "generic", "resourceID", lp_lngResourceID))

End Function


Sub ShowEquipmentManagement()

If Not CheckAccess("/showequipmentmanagement") Then Exit Sub

frmEquipment.Show


End Sub

Public Sub DrawShadow(lp_conControl As Control, lp_lngColour As Long, lp_lngTop As Long, lp_lngLeft As Long, lp_lngBottom As Long, lp_lngRight As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_lngPreviousColor As Long
    Dim l_lngShadowOffset As Long
    
    Dim l_intLoop As Integer
    Dim l_intColourValue As Integer
    
    lp_conControl.DrawMode = vbMaskPen
    'For l_intLoop = 60 To 0 Step -5
        
    '    l_intColourValue = 30 + (l_intLoop * 4)
        
        l_lngPreviousColor = RGB(180, 180, 180)
        
        'just use black
    '    l_lngPreviousColor = RGB(l_intColourValue, l_intColourValue, l_intColourValue)
        
        'set the shadow distance
    '    l_lngShadowOffset = l_intLoop
        
        l_lngShadowOffset = 30
        
        'draw on the box
        
        lp_conControl.Line (lp_lngLeft + l_lngShadowOffset, lp_lngTop + l_lngShadowOffset)-(lp_lngRight + l_lngShadowOffset, lp_lngBottom + l_lngShadowOffset), l_lngPreviousColor, BF
        
    'Next
    
    lp_conControl.DrawMode = vbCopyPen
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub DuplicateJobDetailRow(lp_lngJobDetailID As Long, lp_lngJobID As Long)
    
    'select the jobdetail record
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_lngOrderby As Long
    Dim l_lngNewJobDetailID As Long
    l_strSQL = "SELECT * FROM jobdetail WHERE jobdetailID = '" & lp_lngJobDetailID & "'"
    
    'select the row
    Dim l_rstOriginalJobDetail As New ADODB.Recordset
    Set l_rstOriginalJobDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    l_strSQL = "SELECT * FROM jobdetail WHERE jobdetailID = '-1'"
    
    Dim l_rstNewJobDetail As New ADODB.Recordset
    Set l_rstNewJobDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    CheckForSQLError
    
    l_rstNewJobDetail.AddNew
    
    Dim l_intFields As Integer
    'loop through all the jobdetail fields
    For l_intFields = 0 To l_rstOriginalJobDetail.Fields.Count - 1
        Select Case l_rstNewJobDetail.Fields(l_intFields).Name
            Case "jobdetailID"
                'do nothing
            Case "jobID"
                'insert the new jobID
                l_rstNewJobDetail.Fields(l_intFields).Value = lp_lngJobID
            Case "fd_orderby"
                l_strSQL = "SELECT COUNT(jobID) FROM jobdetail WHERE jobID = '" & GetData("jobdetail", "jobID", "jobdetailID", lp_lngJobDetailID) & "'"
                l_lngOrderby = GetCount(l_strSQL)
                l_rstNewJobDetail.Fields(l_intFields).Value = l_lngOrderby
            Case Else
                l_rstNewJobDetail.Fields(l_intFields).Value = l_rstOriginalJobDetail(l_intFields).Value
        End Select
    Next
    'save the record
    l_rstNewJobDetail.Update
    
    l_rstNewJobDetail.Close
    l_rstOriginalJobDetail.Close
    
    l_strSQL = "SELECT jobdetailID FROM jobdetail WHERE jobID = '" & lp_lngJobID & "' AND fd_orderby = '" & l_lngOrderby & "'"
    l_lngNewJobDetailID = GetCount(l_strSQL)
    
    
    l_strSQL = "SELECT * FROM extendedjobdetail WHERE jobdetailID = '" & lp_lngJobDetailID & "'"
    
    'select the row
    
    Set l_rstOriginalJobDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    l_strSQL = "SELECT * FROM extendedjobdetail WHERE jobdetailID = '-1'"
    
    Set l_rstNewJobDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    CheckForSQLError
    
    If Not l_rstOriginalJobDetail.EOF Then
        
        l_rstNewJobDetail.AddNew
        
        'loop through all the jobdetail fields
        For l_intFields = 0 To l_rstOriginalJobDetail.Fields.Count - 1
            Select Case l_rstNewJobDetail.Fields(l_intFields).Name
                Case "extendedjobdetailID"
                    'do nothing
                Case "jobdetailID"
                    l_rstNewJobDetail.Fields(l_intFields).Value = l_lngNewJobDetailID
                Case "jobID"
                    'insert the new jobID
                    l_rstNewJobDetail.Fields(l_intFields).Value = lp_lngJobID
                Case Else
                    l_rstNewJobDetail.Fields(l_intFields).Value = l_rstOriginalJobDetail(l_intFields).Value
            End Select
        Next
        'save the record
        l_rstNewJobDetail.Update
        
    End If
    
    l_rstNewJobDetail.Close
    l_rstOriginalJobDetail.Close
    
    Set l_rstOriginalJobDetail = Nothing
    Set l_rstNewJobDetail = Nothing
    
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Public Sub SpellCheck(TextObj As TextBox)

Dim objSpellCheck As Object, tmpClipBoardText As String

tmpClipBoardText = Clipboard.GetText                     'Save any existing text in the Clipboard
                                                         ' to a temporary variable
On Error GoTo ErrorHandler
 
If TextObj.Text = "" Then Exit Sub                       'Nothing to check
 
Set objSpellCheck = CreateObject("Word.Application")     'Set up reference to WORD
objSpellCheck.Visible = False                            'Hide WORD from user
objSpellCheck.Documents.Add                              'Open New Document (hidden)
Clipboard.Clear
Clipboard.SetText TextObj.Text                           'Copy Text to Clipboard
objSpellCheck.Selection.Paste                            'Paste Text into WORD
objSpellCheck.ActiveDocument.CheckSpelling               'Activate the Spell Checker
objSpellCheck.Visible = False                            'Hide WORD from user
objSpellCheck.ActiveDocument.Select                      'Select the corrected text
objSpellCheck.Selection.Cut                              'Cut the text To Clipboard
TextObj.Text = Clipboard.GetText                         'Paste corrected text back into TextObj.Text
 
ErrorHandler:

If Err.Number > 0 Then
   MsgBox "Error: " & Err.Number & "  " & Err.Description, vbExclamation, "Spell Check Error"
End If

'Clean up and finish

objSpellCheck.ActiveDocument.Close SaveChanges:=0      'Close & DO NOT save changes
objSpellCheck.Quit 'Quit
Set objSpellCheck = Nothing

Clipboard.SetText tmpClipBoardText                   'Restore original text to Clipboard
 
End Sub

Public Function EmailContact(lp_lngContactID As Long) As String

'this function will send email to the specified contact
    
'if the contact has no email address, the user will be prompted to add one
'this should ensure that the data is populated with the least hassle

    Dim l_intMsg As Integer
    Dim l_strEmail As String
    
    l_strEmail = GetData("contact", "email", "contactID", lp_lngContactID)
        
    If l_strEmail <> "" Then GoTo Proc_Check_Email
    
    l_intMsg = MsgBox("This contact does not have an email address. Do you want to specify one now?", vbQuestion + vbYesNo)
    
    If l_intMsg = vbNo Then GoTo Proc_Cancel
    
Proc_Enter_Email:
    
    'have the user enter an email address for this contact
    
    l_strEmail = InputBox("Please enter the new VALID email address for " & GetData("contact", "name", "contactID", lp_lngContactID), "Specify contact email adress", l_strEmail)
    
Proc_Check_Email:
    
    'checks for a valid email address. this could be elaborated...
    'on - checking position of @ and . etc.
    
    If InStr(l_strEmail, "@") = 0 Or InStr(l_strEmail, ".") = 0 Then
        l_intMsg = MsgBox("The email address entered is not valid", vbRetryCancel + vbExclamation)
        If l_intMsg = vbRetry Then
            GoTo Proc_Enter_Email
        Else
            GoTo Proc_Cancel
        End If
    End If
    
Proc_Save_Email:
    
    'saves the email address back to the contact record.
    'if user permission doesnt exist then tell the user
    
    If CheckAccess("/savecontact", True) = 0 Then
        MsgBox "You do not have permission to edit this contacts email address. The address you have entered will not be saved.", vbExclamation
        GoTo Proc_Send_Email
    End If
    
    'save the data
    
    SetData "contact", "email", "contactID", lp_lngContactID, l_strEmail

Proc_Send_Email:
    
    'send the email using the shell32 command
    
    OpenBrowser "mailto:" & l_strEmail
    EmailContact = l_strEmail
    Exit Function
    
Proc_Cancel:

    'get out of this function
    
    EmailContact = ""
    Exit Function

End Function

Public Function ExecuteSQL(ByVal lp_strSQL As String, ByRef lp_strMessage As String) As ADODB.Recordset
    'call the executeSQL function from the DLL/CLS
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim a As New clsDataFunctions
    Set ExecuteSQL = a.ExecuteSQL(lp_strSQL, lp_strMessage)
    
    Set a = Nothing
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function ExitCETA(Optional ByVal lp_blnDontPrompt As Boolean)
    'This function gets run if the 'EXIT' command is selected on the files menu of the MDI form, and also
    'by the timer event, if the time has run out on a shutdown.
    'However, if the close icon is selected on the MDI form, then the only way to catch that is with the
    'queryunload event on the MDI form itself, because the unload process has already started - thus parts of this sub are duplicated there.
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If (g_optCloseSystemDown = False) And (InStr(GetData("xref", "descriptionalias", "description", CurrentMachineName), "/quickexit") = 0) Then
        If lp_blnDontPrompt <> True Then
            Dim l_intResponse As Integer
            l_intResponse = MsgBox("This will end your CETA session. Do you really want to exit?", vbQuestion + vbYesNo)
            If l_intResponse = vbNo Then
                MDIForm1.Show
                ExitCETA = False
                Exit Function
            End If
        End If
    End If
    ExitCETA = True
    SetData "session", "logoutdate", "sessionID", g_lngSessionID, FormatSQLDate(Now)
    Unload frmShutdownWarning
    End
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Sub FadeVertical(ByVal Pic As PictureBox, ByVal start_r As Single, ByVal start_g As Single, ByVal start_b As Single, ByVal end_r As Single, ByVal end_g As Single, ByVal end_b As Single, ByVal start_y, ByVal end_y)
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim hgt As Single
    Dim wid As Single
    Dim r As Single
    Dim G As Single
    Dim b As Single
    Dim dr As Single
    Dim dg As Single
    Dim db As Single
    Dim Y As Single
    
    wid = Pic.ScaleWidth
    hgt = end_y - start_y
    dr = (end_r - start_r) / hgt
    dg = (end_g - start_g) / hgt
    db = (end_b - start_b) / hgt
    r = start_r
    G = start_g
    b = start_b
    For Y = start_y To end_y
        Pic.Line (0, Y)-(wid, Y), RGB(r, G, b)
        r = r + dr
        G = G + dg
        b = b + db
    Next Y
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

'function to re-format dates to the MySQL format
Public Function FormatSQLDate(lDate As Variant)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    Select Case g_dbtype
        Case "mysql"
            FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Format(Hour(lDate), "00") & ":" & Format(Minute(lDate), "00") & ":" & Format(Second(lDate), "00")
        Case "mssql"
            FormatSQLDate = Month(lDate) & "/" & Day(lDate) & "/" & Year(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
        Case "access"
            FormatSQLDate = lDate
    End Select
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetCopyType(lp_strCopyType As String) As String
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Select Case UCase(lp_strCopyType)
        Case "M"
            GetCopyType = "MASTER"
        Case "C"
            GetCopyType = "COPY"
        Case "O"
            GetCopyType = "OTHER"
    End Select
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function GetCount(lp_strSQL As String) As Double
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim a As New clsDataFunctions
    GetCount = a.GetCount(lp_strSQL)
    Set a = Nothing
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, Optional lp_ReturnTrueNulls As Boolean) As Variant
    'call the executeSQL function from the DLL/CLS
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim a As New clsDataFunctions
    If lp_ReturnTrueNulls = True Then
        GetData = a.GetData(lp_strTableName, lp_strFieldToReturn, lp_strFieldToSearch, lp_varCriterea, True)
    Else
        GetData = a.GetData(lp_strTableName, lp_strFieldToReturn, lp_strFieldToSearch, lp_varCriterea)
    End If
    
    Set a = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetDataSQL(lp_strSQL As String)

    'call the executeSQL function from the DLL/CLS
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim a As New clsDataFunctions
    GetDataSQL = a.GetDataSQL(lp_strSQL)
    
    Set a = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd

End Function

Function GetDuration(lp_datStartTime As Date, lp_datEndTime As Date) As String
    
    'Function to Calculate the duration in hours
    'and mins between two dates
    
    'Put into duration as text
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_intTotalMins
    Dim l_intTotalHours
    Dim l_intTotalMinutes
    
    If lp_datEndTime = 0 Then
        lp_datEndTime = lp_datStartTime
    End If
    
    l_intTotalMins = DateDiff("n", lp_datStartTime, lp_datEndTime)
    l_intTotalHours = Int(l_intTotalMins / 60)
    l_intTotalMinutes = (l_intTotalMins - (l_intTotalHours * 60))
    
    'If hours >24 then just show days
    If l_intTotalHours > 24 Then
        GetDuration = DateDiff("d", lp_datStartTime, lp_datEndTime) & " Days"
    Else
        
        If l_intTotalHours = 1 Then
            l_intTotalHours = l_intTotalHours & " hr "
        ElseIf l_intTotalHours = 0 Then
            l_intTotalHours = ""
        Else
            l_intTotalHours = l_intTotalHours & " hrs "
        End If
        
        If l_intTotalMinutes = 1 Then
            l_intTotalMinutes = l_intTotalMinutes & " min"
        ElseIf l_intTotalMinutes = 0 Then
            l_intTotalMinutes = ""
        Else
            l_intTotalMinutes = l_intTotalMinutes & " mins"
        End If
        
        GetDuration = l_intTotalHours & l_intTotalMinutes
        
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetFlag(lp_intFlagValue As Variant)
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Select Case lp_intFlagValue
        Case 1, -1, True, "TRUE", "True", "true", "YES", "Yes", "yes"
            GetFlag = 1
        Case 0, "FALSE", "False", "false", "no", "NO", "No"
            GetFlag = 0
        Case Else
            GetFlag = 0
    End Select
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetFormatForStockCode(lp_strStockCode As String) As String

Dim l_strSQL As String
l_strSQL = "SELECT format FROM xref WHERE category = 'stockcode' AND description = '" & lp_strStockCode & "';"

Dim l_rstXRef  As New ADODB.Recordset

Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If Not l_rstXRef.EOF Then
    GetFormatForStockCode = Format(l_rstXRef("format"))
Else
    GetFormatForStockCode = ""
End If

l_rstXRef.Close
Set l_rstXRef = Nothing

End Function

Function GetJobDescription(lp_lngJobID As Long) As String
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_rstDetails As New ADODB.Recordset
    Dim l_strTextToAdd As String
    
    If LCase(Left(GetData("job", "jobtype", "jobID", lp_lngJobID), 3)) = "dub" Then
        
        Set l_rstDetails = ExecuteSQL("SELECT copytype, format, quantity, description FROM jobdetail WHERE jobID = " & lp_lngJobID, g_strExecuteError)
        
        If Not l_rstDetails.EOF Then
            
            
            Dim l_rstDetailCount As New ADODB.Recordset
            Set l_rstDetailCount = ExecuteSQL("SELECT count(copytype) FROM jobdetail WHERE jobID = " & lp_lngJobID, g_strExecuteError)
            CheckForSQLError
            
            Select Case l_rstDetailCount(0)
                Case 2
                    
                    l_rstDetails.MoveFirst
                    l_strTextToAdd = Trim(l_rstDetails("format")) & " -> "
                    l_rstDetails.MoveNext
                    l_strTextToAdd = l_strTextToAdd & Trim(l_rstDetails("format"))
                    If l_rstDetails("quantity") > 1 Then
                        l_strTextToAdd = l_strTextToAdd & " x " & l_rstDetails("quantity")
                    End If
                Case 3
                    l_rstDetails.MoveFirst
                    l_strTextToAdd = Trim(l_rstDetails("format")) & " -> "
                    l_rstDetails.MoveNext
                    l_strTextToAdd = l_strTextToAdd & Trim(l_rstDetails("format"))
                    If l_rstDetails("quantity") > 1 Then
                        l_strTextToAdd = l_strTextToAdd & " x " & l_rstDetails("quantity")
                    End If
                    l_rstDetails.MoveNext
                    l_strTextToAdd = l_strTextToAdd & " + " & Trim(l_rstDetails("format"))
                    If l_rstDetails("quantity") > 1 Then
                        l_strTextToAdd = l_strTextToAdd & " x " & l_rstDetails("quantity")
                    End If
                    
                Case Else
                    l_strTextToAdd = "See job details"
            End Select
            
            l_rstDetailCount.Close
            Set l_rstDetailCount = Nothing
            
        End If
        
        l_rstDetails.Close
    
    Else
    
    
        Set l_rstDetails = ExecuteSQL("SELECT resource.name FROM resourceschedule INNER JOIN resource ON resource.resourceID = resourceschedule.resourceID WHERE resourceschedule.jobID = '" & lp_lngJobID & "' ORDER BY resourcescheduleID", g_strExecuteError)
        CheckForSQLError
        
        If Not l_rstDetails.EOF Then
            
            l_rstDetails.MoveFirst
            Do While Not l_rstDetails.EOF
                
                l_strTextToAdd = l_strTextToAdd & Trim(l_rstDetails("name")) & " / "
                
                l_rstDetails.MoveNext
            Loop
            
        End If
        
        l_rstDetails.Close
    
    
    
    End If
    
    
    Set l_rstDetails = Nothing
    
    GetJobDescription = l_strTextToAdd
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function
Function GetJobToolTip(lp_lngJobID As Long) As String
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_rstDetails As New ADODB.Recordset
    Dim l_strTextToAdd As String
    
    Set l_rstDetails = ExecuteSQL("SELECT projectnumber, jobID, companyname, productname, title1, title2, startdate, enddate FROM job WHERE jobID = " & lp_lngJobID & " ORDER BY fd_orderby, jobdetailID", g_strExecuteError)
    
    If Not l_rstDetails.EOF Then
        
        With l_rstDetails
            
            'build the tool tip
            l_strTextToAdd = l_strTextToAdd & "Project: " & Format(.Fields("projectnumber")) & Chr(10)
            l_strTextToAdd = l_strTextToAdd & "Job ID: " & Format(.Fields("jobID")) & Chr(10)
            l_strTextToAdd = l_strTextToAdd & "Company: " & Format(.Fields("companyname")) & Chr(10)
            l_strTextToAdd = l_strTextToAdd & "Product: " & Format(.Fields("productname")) & Chr(10)
            l_strTextToAdd = l_strTextToAdd & "Title: " & Format(.Fields("title1")) & Chr(10)
            l_strTextToAdd = l_strTextToAdd & "Sub Title: " & Format(.Fields("title2")) & Chr(10)
            l_strTextToAdd = l_strTextToAdd & "Start Date: " & Format(.Fields("startdate"), vbShortDateFormat) & Chr(10)
            l_strTextToAdd = l_strTextToAdd & "End Date: " & Format(.Fields("enddate"), vbShortDateFormat)
        End With
        
    Else
        l_strTextToAdd = ""
    End If
    
    l_rstDetails.Close
    Set l_rstDetails = Nothing
    
    GetJobToolTip = l_strTextToAdd
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function IsFormLoaded(lp_strName As String) As Boolean
    
    Dim l_objForm As Variant
    
    'actually loop through every loaded form
    For l_objForm = 0 To Forms.Count - 1
        
        
        If Forms(l_objForm).Name = lp_strName Then
            
            IsFormLoaded = True
            Exit Function
            
        End If
        
    Next
    
End Function

Public Function GetLoadedJobID() As Long
    
    'routine to search through the forms and find the job details form and if
    'it exists, return the currently loaded ID
    'chris tate-davies / 07-09-2004
    
    'return a default
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    GetLoadedJobID = -1
    
    'variable to loop through the forms
    Dim l_objForm As Variant
    
    'actually loop through every loaded form
    For l_objForm = 0 To Forms.Count - 1
        
        'if the caption of the form is the Job Details form then she's the baby we are looking for
        If Left(UCase(Forms(l_objForm).Caption), 11) = "JOB DETAILS" Then
            
            'if there is a valid number (always a long integer)
            If Val(Forms(l_objForm).txtJobID.Text) <> 0 Then
                
                'return that id. this is the loaded job
                GetLoadedJobID = Forms(l_objForm).txtJobID.Text
                
            End If
            
        End If
        
    Next
    
    'and return code to whensever we came from
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetNextJobOrderValue(lp_lngJobID As Long) As Integer

    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_lngGetNextJobOrderValue As Long
    
    l_strSQL = "SELECT fd_orderby FROM jobdetail WHERE jobID = '" & lp_lngJobID & "' ORDER BY fd_orderby"
    
    Dim l_rsSequence As ADODB.Recordset
    Set l_rsSequence = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    CheckForSQLError
    
    If Not l_rsSequence.EOF Then
        l_rsSequence.MoveLast
        l_lngGetNextJobOrderValue = l_rsSequence("fd_orderby") + 1
    Else
        l_lngGetNextJobOrderValue = 0
    End If
    
    l_rsSequence.Close
    Set l_rsSequence = Nothing
    
    GetNextJobOrderValue = l_lngGetNextJobOrderValue
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd

End Function

Function GetNextSequence(lp_strSequenceName As String) As Long
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_lngGetNextSequence  As Long
    l_strSQL = "SELECT * FROM sequence WHERE sequencename = '" & lp_strSequenceName & "';"
    
    Dim l_rsSequence As ADODB.Recordset
    Set l_rsSequence = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    CheckForSQLError
    
    If Not l_rsSequence.EOF Then
        l_rsSequence.MoveFirst
        l_lngGetNextSequence = l_rsSequence("sequencevalue")
        
        l_strSQL = "UPDATE sequence SET sequencevalue = '" & l_lngGetNextSequence + 1 & "', muser = '" & g_strUserInitials & "', mdate = '" & FormatSQLDate(Now()) & "' WHERE sequencename = '" & lp_strSequenceName & "';"
        
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
    Else
        
        Dim l_strSequenceNumber As String
PROC_Input_Number:
        l_strSequenceNumber = InputBox("There is currently no default value for a sequence of " & lp_strSequenceName & ". Please specify the initial value.", "No default value")
        If Not IsNumeric(l_strSequenceNumber) Then
            MsgBox "Please enter a valid numeric, positive integer.", vbExclamation
            GoTo PROC_Input_Number
        Else
            l_rsSequence.AddNew
            l_rsSequence("sequencename") = lp_strSequenceName
            l_rsSequence("sequencevalue") = Val(l_strSequenceNumber) + 1
            l_rsSequence("muser") = g_strUserInitials
            l_rsSequence("mdate") = Now()
            l_rsSequence.Update
            l_lngGetNextSequence = Val(l_strSequenceNumber)
        End If
    End If
    
    l_rsSequence.Close
    Set l_rsSequence = Nothing
    
    GetNextSequence = l_lngGetNextSequence
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function


Function GetNextProjectNumber(lp_strAllocation) As Long
    'TVCodeTools ErrorEnablerStart
   ' On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_lngGetNextSequence  As Long
    l_strSQL = "SELECT nextnumber, lastallocationuser, lastallocationdate FROM allocation WHERE name = '" & lp_strAllocation & "';"
    
    Dim l_rsSequence As ADODB.Recordset
    Set l_rsSequence = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    CheckForSQLError
    
    If Not l_rsSequence.EOF Then
        l_rsSequence.MoveFirst
        l_lngGetNextSequence = l_rsSequence("nextnumber")
        l_rsSequence("nextnumber") = l_lngGetNextSequence + 1
        l_rsSequence("lastallocationuser") = g_strUserInitials
        l_rsSequence("lastallocationdate") = Now()
        l_rsSequence.Update
    End If
    
    l_rsSequence.Close
    Set l_rsSequence = Nothing
    
    GetNextProjectNumber = l_lngGetNextSequence
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd

End Function
Function GetQuotedInfo(ByVal lp_lngProjectID As Long, ByVal lp_strCETACode As String, ByRef lpr_dblQuotedUnits As Double, ByRef lpr_dblQuotedUnitPrice As Double)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_lngQuoteID As Long
    l_lngQuoteID = Val(GetData("project", "quoteID", "projectID", lp_lngProjectID))
    
    If l_lngQuoteID <> 0 Then
        
        Dim l_dblQuotedUnits As Double
        
        Dim l_strSQL As String
        l_strSQL = "SELECT quotedunits, quotedunitprice FROM quotedetail WHERE (quoteID = '" & l_lngQuoteID & "') AND (cetacode = '" & lp_strCETACode & "');"
        
        Dim l_rstQuoteDetail As New ADODB.Recordset
        
        Set l_rstQuoteDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
        CheckForSQLError
        
        If Not l_rstQuoteDetail.EOF Then
            
            lpr_dblQuotedUnits = l_rstQuoteDetail("quotedunits")
            lpr_dblQuotedUnitPrice = l_rstQuoteDetail("quotedunitprice")
            
        End If
        
        l_rstQuoteDetail.Close
        Set l_rstQuoteDetail = Nothing
        
    Else
        lpr_dblQuotedUnitPrice = 0
        lpr_dblQuotedUnits = 0
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetQuotedRate(lp_lngQuoteID As Long, lp_strCostCode As String)

    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_dblQuotedUnitPrice As Double
    
    Dim l_strSQL As String
    l_strSQL = "SELECT quotedunitprice FROM quotedetail WHERE (quoteID = '" & lp_lngQuoteID & "') AND (cetacode = '" & lp_strCostCode & "');"
    
    Dim l_rstQuoteDetail As New ADODB.Recordset
    
    Set l_rstQuoteDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If Not l_rstQuoteDetail.EOF Then
        l_dblQuotedUnitPrice = l_rstQuoteDetail("quotedunitprice")
    End If
    
    l_rstQuoteDetail.Close
    Set l_rstQuoteDetail = Nothing
        
    GetQuotedRate = l_dblQuotedUnitPrice
        

    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    

End Function

Function GetReplacementTab(lp_intTabToFind As Integer) As Integer
    
'TVCodeTools ErrorEnablerStart
On Error GoTo PROC_ERR
'TVCodeTools ErrorEnablerEnd

Dim l_intLoop As Integer
For l_intLoop = 0 To frmJob.tab1.Tabs - 2
    If frmJob.tabReplacementTabs.TabCaption(l_intLoop) = frmJob.tab1.TabCaption(lp_intTabToFind) Then
        GetReplacementTab = l_intLoop
        Exit For
    End If
Next
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function
Function GetResourceLocationColour(ByVal lp_strResourceLocation As String) As Long
    'this function needs to check the location and return the HEX colour value
    If IsInternalLocation(lp_strResourceLocation) Then
        GetResourceLocationColour = &HFF8080
    Else
        GetResourceLocationColour = vbRed
    End If
End Function

Function GetResourceStatusColour(ByVal lp_strResourceStatus As Variant) As Long

Select Case UCase(lp_strResourceStatus)
Case "AVAILABLE", Null, ""
    GetResourceStatusColour = vbBlack
Case "UNAVAILABLE"
    GetResourceStatusColour = vbRed
Case "OUT"
    GetResourceStatusColour = RGB(220, 110, 70)
Case "UNCHECKED"
    GetResourceStatusColour = RGB(0, 210, 0)
Case "PREPARED"
    GetResourceStatusColour = vbMagenta
Case Else
    GetResourceStatusColour = vbCyan
End Select

End Function

Function IsInternalLocation(ByVal lp_strLocation As String) As Boolean

If GetDataSQL("SELECT TOP 1 description FROM xref WHERE category = 'location' AND description = '" & QuoteSanitise(lp_strLocation) & "';") <> "" Then
    IsInternalLocation = True
End If

End Function

Function GetResourceBookingColour(ByVal lp_strResourceStatus As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Select Case UCase(lp_strResourceStatus)
        Dim l_strColour As Long
        
        Case "PENCIL"
            l_strColour = g_lngColourPencil
        Case "SUBMITTED"
            l_strColour = g_lngColour2ndPencil
        Case "CONFIRMED"
            l_strColour = g_lngColourConfirmed
        Case "MASTERS HERE"
            l_strColour = g_lngColourMastersHere
        Case "SCHEDULED"
            l_strColour = g_lngColourScheduled
        Case "IN PROGRESS"
            l_strColour = g_lngColourInProgress
        Case "VT DONE"
            l_strColour = g_lngColourVTDone
        Case "PENDING"
            l_strColour = g_lngColourOnHold
        Case "PRINTED", "COMPLETED", "COMPLETE"
            l_strColour = g_lngColourCompleted
        Case "HOLD COST"
            l_strColour = g_lngColourHoldCost
        Case "COSTED"
            l_strColour = g_lngColourCosted
        Case "INVOICED", "INVOICE", "SENT TO ACCOUNTS"
            l_strColour = g_lngColourSentToAccounts
        Case "NO CHARGE"
        
            'dont show no charged on screen in edit suites
            If LCase(g_strUserName) = "edit-s" Or LCase(g_strUserName) = "telecine" Then
                l_strColour = g_lngColourHoldCost
            Else
                l_strColour = vbRed
            End If
            
        Case "CANCELLED", "CANCEL"
            l_strColour = g_lngColourCancelled
        Case "UNAVAIL", "UNAVAILABLE"
            l_strColour = g_lngColourUnavailable
        Case "MEETING", "AMEETING"
            l_strColour = g_lngColourMeeting
        Case "QUICK"
            l_strColour = g_lngColourQuick
    'hire colours
        Case "PREPARED"
            l_strColour = g_lngColourPrepared
        Case "CHECKED"
            l_strColour = g_lngColourCompleted
        Case "DESPATCHED"
            l_strColour = g_lngColourDespatched
        Case "RETURNED"
            l_strColour = g_lngColourReturned
        'Case "UNCHECKED"
        '    l_strColour = g_lngColourUnchecked
        Case Else
            l_strColour = &HFFFFFF
    End Select
    
    GetResourceBookingColour = l_strColour
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetStatusNumber(lp_strStatus As String) As Integer
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_intStatusNumber As Integer
    Select Case LCase(lp_strStatus)
        Case "pencil"
            l_intStatusNumber = 0
        Case "submitted"
            l_intStatusNumber = 1
        Case "confirmed"
            l_intStatusNumber = 2
        Case "despatched"
            l_intStatusNumber = 2
        Case "prepared"
            l_intStatusNumber = 2
        Case "masters here"
            l_intStatusNumber = 3
        Case "scheduled"
            l_intStatusNumber = 4
        Case "in progress"
            l_intStatusNumber = 5
        Case "pending"
            l_intStatusNumber = 6
        Case "vt done"
            l_intStatusNumber = 7
        Case "completed"
            l_intStatusNumber = 8
        Case "hold cost"
            l_intStatusNumber = 9
        Case "costed"
            l_intStatusNumber = 10
        Case "sent to accounts", "no charge"
            l_intStatusNumber = 11
    End Select
    GetStatusNumber = l_intStatusNumber
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetTotalCostsForQuote(lp_lngQuoteID As Long, lp_strField As String, Optional lp_strCategory As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_rstTotal As New ADODB.Recordset
    
    l_strSQL = "SELECT SUM(" & lp_strField & ") FROM quotedetail LEFT JOIN ratecard ON ratecard.cetacode = quotedetail.cetacode WHERE quotedetail.quoteID = '" & lp_lngQuoteID & "'"
    
    If lp_strCategory <> "" Then
        l_strSQL = l_strSQL & " AND category = '" & lp_strCategory & "'"
    End If
    
    Set l_rstTotal = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    CheckForSQLError
    
    GetTotalCostsForQuote = Format(l_rstTotal(0))
    
    l_rstTotal.Close
    Set l_rstTotal = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function
Sub HighLite(lp_conControl As Control)
    'selects the text in a text box
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    lp_conControl.SelStart = 0
    lp_conControl.SelLength = Len(lp_conControl.Text)
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub HoldJob(lp_lngJobID As Long, lp_intHold As Integer)
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/holdjob") Then
        Exit Sub
    End If
    SetData "job", "flaghold", "jobID", lp_lngJobID, lp_intHold
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub


Function GetKitIDFromProductCode(lp_strProductCode As String) As Long

Dim l_lngKitID As Long
l_lngKitID = GetData("kit", "kitID", "productcode", lp_strProductCode)

GetKitIDFromProductCode = l_lngKitID

End Function

Sub BookKitItems(lp_lngKitID As Long, lp_lngJobID As Long, lp_blnQuickie As Boolean)

    Dim l_rstKitDetails As New ADODB.Recordset
    Dim l_rstResources As New ADODB.Recordset
    
    Dim l_strSQL As String
    Dim l_intQuantityRequired As Integer
    Dim l_intQuantityBooked As Integer
    Dim l_strProductCode As String
    Dim l_datStartTime As Date
    Dim l_datEndTime As Date
    Dim l_strStatus As String
    Dim l_intShow As Integer
    Dim l_lngMasterResourceScheduleID As Long
    Dim l_lngResourceScheduleID As Long
    
    
    l_strSQL = "SELECT * FROM kitcontents WHERE kitID = '" & lp_lngKitID & "' ORDER BY fd_orderby;"
    
    Set l_rstKitDetails = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    'pick up the job values
    l_datStartTime = GetData("job", "startdate", "jobID", lp_lngJobID)
    l_datEndTime = GetData("job", "enddate", "jobID", lp_lngJobID)
    l_strStatus = GetData("job", "fd_status", "jobID", lp_lngJobID)
    
    If Not l_rstKitDetails.EOF Then l_rstKitDetails.MoveFirst
    
    'loop through each item and find resources of that product.
    Do While Not l_rstKitDetails.EOF
    
        'get the kit item details
        l_intQuantityRequired = l_rstKitDetails("quantity")
        l_strProductCode = l_rstKitDetails("productcode")
        l_intShow = IIf(l_rstKitDetails("showonquote") = "Yes", -1, 0)
    
        l_strSQL = "SELECT resourceID from resource WHERE productcode = '" & l_strProductCode & "';"
        
        Set l_rstResources = ExecuteSQL(l_strSQL, g_strExecuteError)
        CheckForSQLError
        
        Dim l_intLoop  As Integer
        
        Do While Not l_rstResources.EOF
        
            For l_intLoop = 0 To l_intQuantityRequired - 1
            
                If IsConflict(lp_lngJobID, l_rstResources("resourceID"), l_datStartTime, l_datEndTime, False) And Not IsGeneric(l_rstResources("resourceID")) Then
                    l_rstResources.MoveNext
                Else
                    
                    l_lngResourceScheduleID = CreateNewResourceSchedule(lp_lngJobID, l_rstResources("resourceID"), l_datStartTime, l_datEndTime, l_strStatus, , True, , l_intShow, l_lngMasterResourceScheduleID)
                    
                    If l_lngMasterResourceScheduleID = 0 Then
                        l_lngMasterResourceScheduleID = l_lngResourceScheduleID
                    End If
                    
                    If l_intLoop >= l_intQuantityRequired - 1 Then GoTo PROC_FOUND_ITEM
                    
Proc_Try_Next:
                
                End If
                    
            Next

        
        Loop
    
            

        
        
        MsgBox "Couldnt find item - " & l_strProductCode, vbExclamation
PROC_FOUND_ITEM:
        
        l_rstResources.Close
        l_rstKitDetails.MoveNext
        
    Loop
    
    Set l_rstResources = Nothing
    
    l_rstKitDetails.Close
    Set l_rstKitDetails = Nothing


End Sub

Sub InsertNote(lp_lngJobID As Long, lp_strField As String, lp_strNote As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/insertnote") Then
        Exit Sub
    End If
    
    If lp_strNote = "" Then
        Exit Sub
    End If
    
    Dim l_strTempNote As String
    Dim l_strExistingNotes As String
    Dim l_strNewNotes As String
    l_strExistingNotes = GetData("job", lp_strField, "jobID", lp_lngJobID)
    
    l_strTempNote = lp_strNote
    If g_optAppendInitialsAndDateToNotes <> 0 Then
        l_strTempNote = l_strTempNote & " [" & g_strUserInitials & " @ " & Now & "]"
    End If
    
    If l_strExistingNotes <> "" Then
        l_strNewNotes = l_strTempNote & vbCrLf & l_strExistingNotes
    Else
        l_strNewNotes = l_strTempNote
    End If
    
    SetData "job", lp_strField, "jobID", lp_lngJobID, l_strNewNotes
    
    AddJobHistory lp_lngJobID, "Notes(" & lp_strField & "): " & lp_strNote
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Public Function IsConflict(lp_lngJobID As Long, lp_lngResourceID As Long, lp_datStartDate As Date, lp_datEndDate As Date, lp_blnExcludeThisJob As Boolean) As Long
    
    'checks for a conflict and returns the resourcescheduleID
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If lp_blnExcludeThisJob = True Then
        Dim l_strExtraSQL As String
        l_strExtraSQL = "and (jobID <> '" & lp_lngJobID & "')"
    End If
    
    Dim l_strSQL As String
    l_strSQL = "SELECT resourcescheduleID FROM resourceschedule WHERE (resourceID = '" & lp_lngResourceID & "') AND (starttime < '" & FormatSQLDate(lp_datEndDate) & "' AND endtime > '" & FormatSQLDate(lp_datStartDate) & "') and (fd_status <> 'Cancelled') and (fd_status <> 'Repeat') and (resourcetype IS Null or resourcetype = '') " & l_strExtraSQL
    
    Dim l_rstConflict As ADODB.Recordset
    Set l_rstConflict = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    CheckForSQLError
    
    If l_rstConflict.EOF Then
        IsConflict = False
    Else
        l_rstConflict.MoveFirst
        IsConflict = l_rstConflict("resourcescheduleID")
    End If
    
    l_rstConflict.Close
    Set l_rstConflict = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function IsHoliday(lp_lngResourceID As Long, lp_datStartDate As Date, lp_datEndDate As Date) As String

    'checks for a conflict and returns the resourcescheduleID
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    l_strSQL = "SELECT resourceID, holidaytype FROM holiday WHERE (resourceID = '" & lp_lngResourceID & "') AND (holidaydate <= '" & FormatSQLDate(lp_datEndDate) & "' AND holidaydate >= '" & FormatSQLDate(lp_datStartDate) & "')"
    
    Dim l_rstConflict As ADODB.Recordset
    Set l_rstConflict = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    CheckForSQLError
    
    If l_rstConflict.EOF Then
        IsHoliday = ""
    Else
        l_rstConflict.MoveFirst
        IsHoliday = l_rstConflict("holidaytype")
    End If
    
    l_rstConflict.Close
    Set l_rstConflict = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd


End Function

Public Function IsJobID(lp_lngJobID As Long) As Boolean

    'checks to see if the specified project ID can be used
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_rstProject As ADODB.Recordset
    l_strSQL = "SELECT * FROM job WHERE jobID = '" & lp_lngJobID & "';"
    
    Set l_rstProject = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    'check the project exists
    
    If l_rstProject.EOF Then
        IsJobID = False
    Else
        
        'check the status
        
        Select Case LCase(Trim(" " & l_rstProject("fd_status")))
            Case "cancelled"
                IsJobID = False
            Case Else
                IsJobID = True
        End Select
        
    End If
    
    l_rstProject.Close
    Set l_rstProject = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd


End Function


Public Function IsProjectValid(lp_lngProjectID As Long) As Boolean
    
    'checks to see if the specified project ID can be used
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_rstProject As ADODB.Recordset
    l_strSQL = "SELECT fd_status FROM project WHERE projectID = '" & lp_lngProjectID & "';"
    
    Set l_rstProject = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    'check the project exists
    
    If l_rstProject.EOF Then
        IsProjectValid = False
    Else
        
        'check the status
        
        Select Case LCase(Trim(" " & l_rstProject("fd_status")))
            Case "completed", "cancelled", "hold"
                IsProjectValid = False
            Case Else
                IsProjectValid = True
        End Select
        
    End If
    
    l_rstProject.Close
    Set l_rstProject = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function JobSafeForPosting(lp_lngJobID As Long, Optional lp_blnDisneyCheck As Boolean) As JobSafeForPostingLevel
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String, rs As ADODB.Recordset
    
    'check the job for the client, and the client account code
    
    Dim l_strCompanyAccountCode As String
'    If g_intWebInvoicing = 1 Then
'        l_strCompanyAccountCode = GetData("company", "salesledgercode", "companyID", GetData("accountstransaction", "companyID", "accountstransactionID", lp_lngJobID))
'    Else
        l_strCompanyAccountCode = GetData("company", "salesledgercode", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID))
'    End If
    
    If l_strCompanyAccountCode = "" Then
        JobSafeForPosting = vbuJobNotSafeCompanyAccountCode
        Exit Function
    End If
    
    Dim l_strSAPcustomercode As String
    l_strSAPcustomercode = GetData("company", "SAPcustomercode", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID))
    If l_strSAPcustomercode = "" Then
        JobSafeForPosting = vbuJobNotSafeSAPcode
        Exit Function
    End If
    
    Dim l_strOrderNumber As String
    l_strOrderNumber = GetData("job", "orderreference", "jobID", lp_lngJobID)
    If Len(l_strOrderNumber) > 50 Then
        JobSafeForPosting = vbuJobNotSafePOTooLong
        Exit Function
    End If
    
    l_strSQL = "SELECT COUNT(costingID) FROM costing WHERE jobID = '" & lp_lngJobID & "' AND (accountcode IS NULL OR accountcode = '')"
    Dim l_intInvalidCostingLines As Integer
    l_intInvalidCostingLines = GetCount(l_strSQL)
    
    If l_intInvalidCostingLines > 0 Then
        JobSafeForPosting = vbuJobNotSafeCostingRows
    Else
        JobSafeForPosting = vbuJobSafe
    End If
    
    If Not IsMissing(lp_blnDisneyCheck) Then
        If lp_blnDisneyCheck = True Then
            l_strSQL = "SELECT COUNT(costingID) FROM costing LEFT JOIN Ratecard ON costing.costcode = ratecard.cetacode WHERE jobID = '" & lp_lngJobID & "' AND (MPServiceProfileNo IS NULL)"
            Dim l_intInvalidMediaPulseProfiles As Integer
            l_intInvalidMediaPulseProfiles = GetCount(l_strSQL)
            If l_intInvalidMediaPulseProfiles > 0 Then
                Set rs = ExecuteSQL("Select * from costing where jobID = " & lp_lngJobID & ";", g_strExecuteError)
                If rs.RecordCount > 0 Then
                    rs.MoveFirst
                    Do While Not rs.EOF
                        l_intInvalidMediaPulseProfiles = GetData("ratecard", "MPServiceProfileNo", "cetacode", rs("costcode"))
                        If l_intInvalidMediaPulseProfiles = 0 Then
                            l_intInvalidMediaPulseProfiles = GetDataSQL("SELECT MPServiceProfileNo FROM ratecard WHERE cetacode = '" & rs("costcode") & "' AND companyID = " & GetData("job", "companyID", "jobID", lp_lngJobID))
                        End If
                        If l_intInvalidMediaPulseProfiles = 0 Then
                            JobSafeForPosting = vbJobNotSafeMediaPulseProfiles
                        End If
                        rs.MoveNext
                    Loop
                End If
                rs.Close
                Set rs = Nothing
            Else
                JobSafeForPosting = vbuJobSafe
            End If
        End If
    End If
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function JustDate(lp_datDate As Date) As Date
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_datNewDate As Date
    l_datNewDate = Format(lp_datDate, vbShortDateFormat)
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function LockControl(objX As Object, cLock As Boolean)
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If cLock Then
        LockWindowUpdate objX.hWnd
    Else
        LockWindowUpdate 0
        objX.Refresh
    End If
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub Logout()
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    SetData "session", "logoutdate", "sessionID", g_lngSessionID, FormatSQLDate(Now)
    
    frmLogin.Show vbModal
    
    Unload frmLogin
    Set frmLogin = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub MakeLookLikeOffice(lp_frmApplyTo As Form)
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_intLoop As Integer
    For l_intLoop = 0 To lp_frmApplyTo.Controls.Count - 1
        AddOfficeBorder (lp_frmApplyTo.Controls(l_intLoop))
    Next
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub MatchText(TextControl As Control, ListControl As Control, KeyAscii As Integer, Column%)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim counter%
    Dim SelPosition%
    Dim NewText$
    Dim ExtraLength%
    Dim StandardControl%
    
    Select Case KeyAscii
        Case 13, 26, 24, 3, 22 'Return, Undo, Cut, Copy, Paste
            Exit Sub
    End Select
    
    If TypeOf ListControl Is SSOleDBCombo Then
        StandardControl% = False
    ElseIf TypeOf ListControl Is SSOleDBDropDown Then
        StandardControl% = False
    ElseIf TypeOf ListControl Is SSOleDBGrid Then
        StandardControl% = False
    Else
        StandardControl% = True
    End If
    
    Do
        If StandardControl% = False Then
            If counter% = ListControl.Rows Then
                Exit Do
            End If
        Else
            If counter% = ListControl.ListCount Then
                Exit Do
            End If
        End If
        
        'The equivalent of deleting the highlighted bit off of the end
        If TextControl.SelLength > 0 Then
            'If the whole text is selected
            If KeyAscii = 8 And TextControl.SelStart + TextControl.SelLength = Len(TextControl.Text) Then
                Exit Sub
            End If
            TextControl.SelText = ""
            If KeyAscii = 8 Then
                KeyAscii = 0
            End If
        End If
        
        
        'If a character is being deleted from somewhere in the middle of the text
        'This makes sure the box deletes the correct character
        SelPosition% = TextControl.SelStart
        If KeyAscii = 8 And SelPosition% > 0 And SelPosition% <> Len(TextControl.Text) Then
            TextControl.Text = Left(TextControl.Text, SelPosition% - 1) & Right(TextControl.Text, Len(TextControl.Text) - SelPosition%)
            TextControl.SelStart = SelPosition% - 1
            KeyAscii = 0
        End If
        
        If KeyAscii = 0 Then
            NewText$ = Left(TextControl.Text, TextControl.SelStart) & Right(TextControl.Text, Len(TextControl.Text) - TextControl.SelStart)
            ExtraLength% = 0
        Else
            NewText$ = Left(TextControl.Text, TextControl.SelStart) & Chr(KeyAscii) & Right(TextControl.Text, Len(TextControl.Text) - TextControl.SelStart)
            ExtraLength% = 1
        End If
        
        
        If StandardControl% = False Then
            ListControl.Row = counter%
            If UCase(Left(ListControl.Columns(Column%).Text, Len(TextControl.Text) + ExtraLength%)) = UCase(NewText$) Then
                TextControl.SelText = Chr(KeyAscii)
                KeyAscii = 0
                SelPosition% = TextControl.SelStart
                TextControl.Text = ListControl.ColText(Column%)
                TextControl.SelStart = SelPosition%
                TextControl.SelLength = Len(TextControl.Text) - SelPosition%
                Exit Do
            End If
        Else
            If UCase(Left(ListControl.List(counter%), Len(TextControl.Text) + ExtraLength%)) = UCase(NewText$) Then
                TextControl.SelText = Chr(KeyAscii)
                KeyAscii = 0
                SelPosition% = TextControl.SelStart
                TextControl.Text = ListControl.List(counter%)
                TextControl.SelStart = SelPosition%
                TextControl.SelLength = Len(TextControl.Text) - SelPosition%
                Exit Do
            End If
        End If
        
        counter% = counter% + 1
    Loop
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub NoCompanySelectedMessage()
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    MsgBox "Please select a valid company first", vbExclamation
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub NoContactSelectedMessage()
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    MsgBox "Please select a valid contact first", vbExclamation
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub InvalidEmailAddressMessage()

    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    MsgBox "The email address provided is not valid", vbExclamation
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub
Sub NoJobSelectedMessage()
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    MsgBox "Please select a valid job first", vbExclamation
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub


Sub NoProductSelectedMessage()
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    MsgBox "Please select a valid product first", vbExclamation
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub NoProjectSelectedMessage()
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    MsgBox "Please select a valid project first", vbExclamation
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub


' Open the default browser on a given URL
' Returns True if successful, False otherwise

Public Function OpenBrowser(ByVal url As String) As Boolean
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim res As Long
    
    If InStr(url, ".") = 0 Then
        MsgBox "Invalid web / email address. Please check the address and try again.", vbInformation
        Exit Function
    End If
    
    ' it is mandatory that the URL is prefixed with http:// or https://
    If InStr(1, url, "http", vbTextCompare) <> 1 And InStr(1, url, "mailto:", vbTextCompare) <> 1 Then
        url = "http://" & url
    End If
    
    res = ShellExecute(0&, "open", url, vbNullString, vbNullString, vbNormalFocus)
    OpenBrowser = (res > 32)
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function SaveAssociation(ByVal lp_lngAssociationID As Long, ByVal lp_strAssociationType As String, lp_lngMasterID As Long, lp_strAssociation As String, lp_lngAssociateID As Long, lp_strComments As String) As Long

If Not CheckAccess("/saveassociation") Then Exit Function

Dim l_strSQL As String

If lp_lngAssociationID = 0 Then
    'add a new association
    l_strSQL = "INSERT INTO association ("
    'fields to insert
    l_strSQL = l_strSQL & "cdate, cuserID, cuser) VALUES ("
    
    'values to insert
    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
    l_strSQL = l_strSQL & "'" & g_lngUserID & "', "
    l_strSQL = l_strSQL & "'" & g_strUserInitials & "')"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    lp_lngAssociationID = g_lngLastID
    
End If

l_strSQL = "UPDATE association SET "
l_strSQL = l_strSQL & " associationtype = '" & QuoteSanitise(lp_strAssociationType) & "', "
l_strSQL = l_strSQL & " masterID = '" & lp_lngMasterID & "', "
l_strSQL = l_strSQL & " associateID = '" & lp_lngAssociateID & "', "
l_strSQL = l_strSQL & " association = '" & QuoteSanitise(lp_strAssociation) & "', "
l_strSQL = l_strSQL & " comments = '" & QuoteSanitise(lp_strComments) & "', "
l_strSQL = l_strSQL & " muser = '" & QuoteSanitise(g_strUserInitials) & "', "
l_strSQL = l_strSQL & " muserID = '" & g_lngUserID & "', "
l_strSQL = l_strSQL & " mdate = '" & FormatSQLDate(Now) & "' "

l_strSQL = l_strSQL & " WHERE associationID = '" & lp_lngAssociationID & "';"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

SaveAssociation = lp_lngAssociationID

End Function

Public Sub PopulateCombo(lp_strCategory As String, lp_conControl As Control, Optional lp_strCriterea As String, Optional lp_blnClearFirst As Boolean)

Dim Count As Long

On Error GoTo PROC_CheckListError
    
    If lp_conControl.ListCount > 0 Then
        If lp_blnClearFirst = False Then
            Exit Sub
        Else
            For Count = 0 To lp_conControl.ListCount - 1
                lp_conControl.RemoveItem (0)
            Next
        End If
    End If
    GoTo PROC_Continue
    
PROC_TryRowCount:
    If lp_conControl.Rows > 0 Then
        If lp_blnClearFirst = False Then
            Exit Sub
        Else
            For Count = 0 To lp_conControl.Rows - 1
                lp_conControl.RemoveItem (0)
            Next
        End If
    End If
    GoTo PROC_Continue

PROC_CheckListError:
    If Err.Number = 438 Then
        GoTo PROC_TryRowCount
    End If
    
PROC_Continue:
    
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    
    Dim lp_strCategory1 As String
    Dim l_rstXRef As ADODB.Recordset
           
    LockControl lp_conControl, True
    
    Screen.MousePointer = vbHourglass
    
    Dim l_strTempCaption As String
    l_strTempCaption = MDIForm1.Caption
    
    Dim l_intLoop As Integer
    Dim l_strSQL As String
    Dim l_rstResource As New ADODB.Recordset
            
    Select Case LCase(lp_strCategory)
        
        Case "trackerfields"
        
            lp_conControl.AddItem "requiredby"
            lp_conControl.AddItem "headertext1"
            lp_conControl.AddItem "headertext2"
            lp_conControl.AddItem "headertext3"
            lp_conControl.AddItem "headertext4"
            lp_conControl.AddItem "headertext5"
            lp_conControl.AddItem "headertext6"
            lp_conControl.AddItem "headertext7"
            lp_conControl.AddItem "headertext8"
            lp_conControl.AddItem "headertext9"
            lp_conControl.AddItem "headertext10"
            lp_conControl.AddItem "headerint1"
            lp_conControl.AddItem "headerint2"
            lp_conControl.AddItem "headerint3"
            lp_conControl.AddItem "headerint4"
            lp_conControl.AddItem "headerint5"
            lp_conControl.AddItem "headerdate1"
            lp_conControl.AddItem "headerdate2"
            lp_conControl.AddItem "headerdate3"
            lp_conControl.AddItem "headerdate4"
            lp_conControl.AddItem "headerdate5"
            lp_conControl.AddItem "headerflag1"
            lp_conControl.AddItem "headerflag2"
            lp_conControl.AddItem "headerflag3"
            lp_conControl.AddItem "headerflag4"
            lp_conControl.AddItem "headerflag5"
            lp_conControl.AddItem "stagefield1"
            lp_conControl.AddItem "stagefield2"
            lp_conControl.AddItem "stagefield3"
            lp_conControl.AddItem "stagefield4"
            lp_conControl.AddItem "stagefield5"
            lp_conControl.AddItem "stagefield6"
            lp_conControl.AddItem "stagefield7"
            lp_conControl.AddItem "stagefield8"
            lp_conControl.AddItem "stagefield9"
            lp_conControl.AddItem "stagefield10"
            lp_conControl.AddItem "stagefield11"
            lp_conControl.AddItem "stagefield12"
            lp_conControl.AddItem "stagefield13"
            lp_conControl.AddItem "stagefield14"
            lp_conControl.AddItem "stagefield15"
            lp_conControl.AddItem "stagefield16"
            lp_conControl.AddItem "stagefield17"
            lp_conControl.AddItem "stagefield18"
            lp_conControl.AddItem "stagefield19"
            lp_conControl.AddItem "stagefield20"
            lp_conControl.AddItem "stagefield21"
            lp_conControl.AddItem "stagefield22"
            lp_conControl.AddItem "stagefield23"
            lp_conControl.AddItem "stagefield24"
            lp_conControl.AddItem "stagefield25"
            lp_conControl.AddItem "conclusionfield1"
            lp_conControl.AddItem "conclusionfield2"
            lp_conControl.AddItem "conclusionfield3"
            lp_conControl.AddItem "conclusionfield4"
            lp_conControl.AddItem "conclusionfield5"
            
        Case "itunessubtype"
        
                l_strSQL = "SELECT DISTINCT description, forder FROM xref WHERE category = '" & QuoteSanitise(lp_strCategory) & "' AND information = '" & lp_strCriterea & "' ORDER BY forder, description"
                Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
                
                If Not l_rstXRef.EOF Then
                    l_rstXRef.MoveFirst
                End If
                
                lp_conControl.AddItem ""
                
                Do While Not l_rstXRef.EOF
                    lp_conControl.AddItem Trim(" " & l_rstXRef("description"))
                    l_rstXRef.MoveNext
                Loop
                
                l_rstXRef.Close
                Set l_rstXRef = Nothing
                        
        Case "audiosupplier"
            lp_conControl.AddItem "N/A"
            lp_conControl.AddItem "TAPE"
            lp_conControl.AddItem "FILE"
            lp_conControl.AddItem "TOYA"
            lp_conControl.AddItem "SDI"
            lp_conControl.AddItem "VSI"
            lp_conControl.AddItem "IMS"
        
        Case "countries"
            l_strSQL = "SELECT country FROM iso2a ORDER BY country;"
            Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            If Not l_rstResource.EOF Then
                l_rstResource.MoveFirst
                Do While Not l_rstResource.EOF
                    lp_conControl.AddItem l_rstResource("country")
                    l_rstResource.MoveNext
                Loop
            End If
            l_rstResource.Close
            Set l_rstResource = Nothing
            
        Case "supplier"
            l_strSQL = "SELECT textentry FROM customfieldxref WHERE customfield = 1 AND companyID = " & lp_strCriterea & " ORDER BY textentry;"
            Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            If Not l_rstResource.EOF Then
                l_rstResource.MoveFirst
                Do While Not l_rstResource.EOF
                    lp_conControl.AddItem l_rstResource("textentry")
                    l_rstResource.MoveNext
                Loop
            End If
            l_rstResource.Close
            Set l_rstResource = Nothing
            
        Case "tape-file"
            lp_conControl.AddItem "TAPE"
            lp_conControl.AddItem "FILE"
        
        Case "ebu-loudness"
            lp_conControl.AddItem "Compliant"
            lp_conControl.AddItem "Not Compliant"
            lp_conControl.AddItem "Not Present"
            lp_conControl.AddItem "Not Checked"

        Case "encodepasses"
            lp_conControl.AddItem "1"
            lp_conControl.AddItem "2"
        
        Case "interlace"
            lp_conControl.AddItem ""
            lp_conControl.AddItem "Interlace"
            lp_conControl.AddItem "Progressive"
        
        Case "cbrvbr"
            lp_conControl.AddItem ""
            lp_conControl.AddItem "CBR"
            lp_conControl.AddItem "VBR"
        
        Case "companyassociation"
            lp_conControl.AddItem "Owner"
            lp_conControl.AddItem "Agency"
            lp_conControl.AddItem "Partner"
            
        Case "true-false"
            lp_conControl.AddItem "true"
            lp_conControl.AddItem "false"
            
        Case "transactiontype"
        
            lp_conControl.AddItem "Invoice"
            lp_conControl.AddItem "Deferred"
            lp_conControl.AddItem "Credit Note"
            lp_conControl.AddItem "Payment"
        
        Case "ourreference"
        
            l_strSQL = "SELECT description AS code, information AS description FROM xref WHERE category = 'ourreference' AND format = '" & lp_strCriterea & "' AND (fd_status <> 'DELETED' and fd_status <> 'SOLD') ORDER BY code, description;"
            
            Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            
            If Not l_rstResource.EOF Then l_rstResource.MoveFirst
            Do While Not l_rstResource.EOF
                lp_conControl.AddItem Format(l_rstResource("code")) & vbTab & Format(l_rstResource("description"))
                l_rstResource.MoveNext
            Loop
            
            l_rstResource.Close
            Set l_rstResource = Nothing
            
        Case "discstores"
        
            l_strSQL = "SELECT barcode FROM library WHERE format = 'discstore' AND system_deleted = 0 ORDER BY barcode"
            
            Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            
            If Not l_rstResource.EOF Then l_rstResource.MoveFirst
            Do While Not l_rstResource.EOF
                lp_conControl.AddItem Trim(" " & l_rstResource("barcode"))
                l_rstResource.MoveNext
            Loop
            
            l_rstResource.Close
            Set l_rstResource = Nothing

        Case "discstoresnobfs"
        
            l_strSQL = "SELECT barcode FROM library WHERE format = 'discstore' AND system_deleted = 0 AND barcode NOT LIKE'BFS%' ORDER BY barcode"
            
            Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            
            If Not l_rstResource.EOF Then l_rstResource.MoveFirst
            Do While Not l_rstResource.EOF
                lp_conControl.AddItem Trim(" " & l_rstResource("barcode"))
                l_rstResource.MoveNext
            Loop
            
            l_rstResource.Close
            Set l_rstResource = Nothing

        Case "restorediscstores"
        
            l_strSQL = "SELECT barcode FROM library WHERE format = 'discstore' AND system_deleted = 0 AND version LIKE '/mnt%' and fd_status IS NULL AND NoRestores = 0 ORDER BY barcode"
            
            Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            
            If Not l_rstResource.EOF Then l_rstResource.MoveFirst
            Do While Not l_rstResource.EOF
                lp_conControl.AddItem Trim(" " & l_rstResource("barcode"))
                l_rstResource.MoveNext
            Loop
            
            l_rstResource.Close
            Set l_rstResource = Nothing

        Case "autodeletediscstores"
        
            l_strSQL = "SELECT barcode FROM library WHERE format = 'discstore' AND system_deleted = 0 AND CanBeUsedForAutoDelete <> 0 ORDER BY barcode"
            
            Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            
            If Not l_rstResource.EOF Then l_rstResource.MoveFirst
            Do While Not l_rstResource.EOF
                lp_conControl.AddItem Trim(" " & l_rstResource("barcode"))
                l_rstResource.MoveNext
            Loop
            
            l_rstResource.Close
            Set l_rstResource = Nothing

        Case "box"
        
            l_strSQL = "SELECT barcode FROM library WHERE format = 'LIBRARYBOX' ORDER BY barcode"
            
            Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            
            If Not l_rstResource.EOF Then l_rstResource.MoveFirst
            Do While Not l_rstResource.EOF
                lp_conControl.AddItem Trim(" " & l_rstResource("barcode"))
                l_rstResource.MoveNext
            Loop
            
            l_rstResource.Close
            Set l_rstResource = Nothing
            
        Case "productstatus"
            
            lp_conControl.AddItem "Cancelled"
            lp_conControl.AddItem "Current"
            lp_conControl.AddItem "Completed"
            lp_conControl.AddItem "Hidden"
        
        Case "allocatedto"
        
            l_strSQL = "SELECT fullname FROM cetauser WHERE department = 'Engineering' OR department = 'Shoots' ORDER BY fullname"

            Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            
            If Not l_rstResource.EOF Then l_rstResource.MoveFirst
            Do While Not l_rstResource.EOF
                lp_conControl.AddItem Format(l_rstResource("fullname"))
                l_rstResource.MoveNext
            Loop
            
            l_rstResource.Close
            Set l_rstResource = Nothing
            
            
            Set l_rstResource = ExecuteSQL("SELECT name FROM resource WHERE (categorycode LIKE '%/engineer')  AND (fd_status <> 'DELETED' and fd_status <> 'SOLD')  ORDER BY name", g_strExecuteError)
            CheckForSQLError
            
            If Not l_rstResource.EOF Then
                l_rstResource.MoveFirst
                Do While Not l_rstResource.EOF
                    lp_conControl.AddItem Format(l_rstResource("name"))
                    l_rstResource.MoveNext
                Loop
            End If
            
            l_rstResource.Close
            Set l_rstResource = Nothing
            
        
        
        Case "kit"
        
            l_strSQL = "SELECT name FROM kit ORDER BY name"
            
            Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            
            If Not l_rstResource.EOF Then l_rstResource.MoveFirst
            Do While Not l_rstResource.EOF
                lp_conControl.AddItem Format(l_rstResource("name"))
                l_rstResource.MoveNext
            Loop
            
            l_rstResource.Close
            Set l_rstResource = Nothing
        
        Case "users"
            
            lp_conControl.AddItem "N/A"
            lp_conControl.AddItem "Unknown"
            l_strSQL = "SELECT fullname FROM cetauser WHERE securitytemplate <> 120 ORDER BY fullname"
            
            Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            
            If Not l_rstResource.EOF Then l_rstResource.MoveFirst
            Do While Not l_rstResource.EOF
                lp_conControl.AddItem Format(l_rstResource("fullname"))
                l_rstResource.MoveNext
            Loop
            
            l_rstResource.Close
            Set l_rstResource = Nothing
            
        Case "department"
            l_strSQL = "SELECT name FROM department ORDER BY name"
            
            Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            
            If Not l_rstResource.EOF Then l_rstResource.MoveFirst
            Do While Not l_rstResource.EOF
                lp_conControl.AddItem Format(l_rstResource("name"))
                l_rstResource.MoveNext
            Loop
            
            l_rstResource.Close
            Set l_rstResource = Nothing
            
        Case "operator"
            
            If g_intTapeOperatorsFromCetauserTable = 1 Then
                l_strSQL = "SELECT fullname as name FROM cetauser WHERE securitytemplate <> 120 AND listasoperator <> 0 ORDER BY fullname;"
            Else
                l_strSQL = "SELECT name FROM resource WHERE categorycode LIKE '%/operator%'  AND (fd_status <> 'DELETED' and fd_status <> 'SOLD') ORDER BY name;"
            End If
            
            Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
             '   lp_conControl.AddItem ""
            If Not l_rstResource.EOF Then l_rstResource.MoveFirst
            Do While Not l_rstResource.EOF
                lp_conControl.AddItem Format(l_rstResource("name"))
                l_rstResource.MoveNext
            Loop
            
            l_rstResource.Close
            Set l_rstResource = Nothing
            
        Case "mediaservices"
            
            If g_intTapeOperatorsFromCetauserTable = 1 Then
                l_strSQL = "SELECT fullname as name FROM cetauser WHERE securitytemplate <> 120 AND listasmediaservices <> 0 ORDER BY fullname;"
            Else
                l_strSQL = "SELECT name FROM resource WHERE categorycode LIKE '%/operator%'  AND (fd_status <> 'DELETED' and fd_status <> 'SOLD') ORDER BY name;"
            End If
            
            Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
             '   lp_conControl.AddItem ""
            If Not l_rstResource.EOF Then l_rstResource.MoveFirst
            Do While Not l_rstResource.EOF
                lp_conControl.AddItem Format(l_rstResource("name"))
                l_rstResource.MoveNext
            Loop
            
            l_rstResource.Close
            Set l_rstResource = Nothing
            
        Case "signoffissues"
            
            l_strSQL = "SELECT fullname as name FROM cetauser WHERE securitytemplate <> 120 AND signoffissues <> 0 ORDER BY fullname;"
            
            Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
             '   lp_conControl.AddItem ""
            If Not l_rstResource.EOF Then l_rstResource.MoveFirst
            Do While Not l_rstResource.EOF
                lp_conControl.AddItem Format(l_rstResource("name"))
                l_rstResource.MoveNext
            Loop
            
            l_rstResource.Close
            Set l_rstResource = Nothing
            
        Case "investigateissues"
            
            l_strSQL = "SELECT fullname as name FROM cetauser WHERE securitytemplate <> 120 AND investigateissues <> 0 ORDER BY fullname;"
            
            Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
             '   lp_conControl.AddItem ""
            If Not l_rstResource.EOF Then l_rstResource.MoveFirst
            Do While Not l_rstResource.EOF
                lp_conControl.AddItem Format(l_rstResource("name"))
                l_rstResource.MoveNext
            Loop
            
            l_rstResource.Close
            Set l_rstResource = Nothing
            
        Case "deliveredby"
            l_strSQL = "SELECT name FROM resource WHERE categorycode LIKE '%/despatch%' AND (fd_status <> 'DELETED' and fd_status <> 'SOLD') ORDER BY name"
            
            Set l_rstResource = ExecuteSQL(l_strSQL, g_strExecuteError)
            CheckForSQLError
            
            If Not l_rstResource.EOF Then l_rstResource.MoveFirst
            Do While Not l_rstResource.EOF
                lp_conControl.AddItem Format(l_rstResource("name"))
                l_rstResource.MoveNext
            Loop
            
            l_rstResource.Close
            Set l_rstResource = Nothing
            
            Dim l_rstMachine As New ADODB.Recordset
            
            Set l_rstMachine = ExecuteSQL("SELECT description FROM xref WHERE category = 'despatchmethod' AND format IS NULL ORDER BY description", g_strExecuteError)
            CheckForSQLError
            
            If Not l_rstMachine.EOF Then
                l_rstMachine.MoveFirst
                Do While Not l_rstMachine.EOF
                    lp_conControl.AddItem Format(l_rstMachine("description"))
                    l_rstMachine.MoveNext
                Loop
            End If
            
            l_rstMachine.Close
            Set l_rstMachine = Nothing
            
        
        Case "jobstatus"
        
            lp_conControl.AddItem "2nd Pencil"
            lp_conControl.AddItem "Pencil"
            lp_conControl.AddItem "Confirmed"
            lp_conControl.AddItem "Completed"
            lp_conControl.AddItem "Hold Cost"
            lp_conControl.AddItem "Costed"
            lp_conControl.AddItem "Sent To Accounts"
            lp_conControl.AddItem "Cancelled"
            lp_conControl.AddItem "Unavailable"
            lp_conControl.AddItem "Meeting"
        
        Case "dealtype"
            lp_conControl.AddItem "Rate Card"
            'lp_conControl.AddItem "Ask Our Contact!" 'removed by SME @ DAS/MPC request.
            lp_conControl.AddItem "Deal"
            lp_conControl.AddItem "F.O.C."
            
        Case "purchaseordertype"
            lp_conControl.AddItem "Purchase"
            lp_conControl.AddItem "CapEx"
            lp_conControl.AddItem "Service Contract"
            lp_conControl.AddItem "Hire"
            lp_conControl.AddItem "Freelance / Crew"
            
            
        Case "display"
            lp_conControl.AddItem "1 Day"
            lp_conControl.AddItem "2 Days"
            lp_conControl.AddItem "1 Week"
            lp_conControl.AddItem "2 Weeks"
            lp_conControl.AddItem "1 Month"
            lp_conControl.AddItem "40 Days"
            lp_conControl.AddItem "70 Days"
            lp_conControl.AddItem "6 Months"
            
        Case "times"
            For l_intLoop = 20 To 23
                lp_conControl.AddItem Format(l_intLoop, "00") & ":00"
                lp_conControl.AddItem Format(l_intLoop, "00") & ":15"
                lp_conControl.AddItem Format(l_intLoop, "00") & ":30"
                lp_conControl.AddItem Format(l_intLoop, "00") & ":45"
            Next
            For l_intLoop = 0 To 19
                lp_conControl.AddItem Format(l_intLoop, "00") & ":00"
                lp_conControl.AddItem Format(l_intLoop, "00") & ":15"
                lp_conControl.AddItem Format(l_intLoop, "00") & ":30"
                lp_conControl.AddItem Format(l_intLoop, "00") & ":45"
            Next
        Case "ratecard"
            For l_intLoop = 0 To 9
                lp_conControl.AddItem l_intLoop
            Next
            
        Case "xreftypes"
            
            l_strSQL = "SELECT DISTINCT category FROM xref order by category;"
            Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            With lp_conControl
                
                l_rstXRef.MoveFirst
                Do While Not l_rstXRef.EOF
                    If l_rstXRef("category") = "location" Then
                        If CheckAccess("/showlocationincombosettings", True) Then .AddItem "location"
                    Else
                        .AddItem Trim(" " & l_rstXRef("category"))
                    End If
                    l_rstXRef.MoveNext
                Loop
'                .AddItem "accountstatus"
'                .AddItem "afd"
'                .AddItem "allocation"
'                .AddItem "aspectratio"
'                '.AddItem "association"
'                .AddItem "audiocodec"
'                .AddItem "audiostandard"
'                .AddItem "audiotracks"
'                .AddItem "audiotrackdepth"
'                .AddItem "audiomapping"
'                .AddItem "BusinessUnit"
'                .AddItem "BatonProfileName"
'                .AddItem "clipactions"
'                .AddItem "clipcodec"
'                .AddItem "clipdelivery"
'                .AddItem "clipformat"
'                .AddItem "clippurpose"
'                .AddItem "clipstreamtype"
'                .AddItem "cliptype"
'                .AddItem "contractrejectitem"
'                .AddItem "contractrejectreason"
'                .AddItem "copytype"
'                .AddItem "copytypeoptions"
'                .AddItem "costingsheetreport"
'                .AddItem "DADCActiveRatio"
'                .AddItem "DADCPictFormat"
'                .AddItem "DADCForcedSubtitle"
'                .AddItem "dadclogging"
'                .AddItem "dadclanguages"
'                .AddItem "dadcaudiotypes"
'                .AddItem "dadcaudiocontent"
'                .AddItem "dadcdistlogos"
'                .AddItem "dadcoperators"
'                .AddItem "daspectratio"
'                .AddItem "datefieldsjob"
'                .AddItem "datefieldspurchase"
'                .AddItem "datefieldsquote"
'                '.AddItem "department"
'                .AddItem "dealunit"
'                .AddItem "deliverycostcodes"
'                .AddItem "despatchmethod"
'                .AddItem "dformat"
'                .AddItem "digiparameters"
'                .AddItem "digitaldeliverymethod"
'                .AddItem "DigitalDeliveryPlatform"
'                .AddItem "DigitalDestinationType"
'                .AddItem "digitaloptions"
'                .AddItem "dimensionsforaccounts"
'                .AddItem "discoveryaudiocontent"
'                .AddItem "dsoundchannel"
'                .AddItem "dtimecode"
'                .AddItem "dvideostandard"
'                .AddItem "equipmentunavailablestage"
'                .AddItem "event"
'                .AddItem "fault"
'                .AddItem "faultaction"
'                .AddItem "faultgrade"
'                .AddItem "faulttype"
'                .AddItem "ffmpeg_explicit_version"
'                .AddItem "fileversion"
'                .AddItem "format"
'                .AddItem "framerate"
'                .AddItem "genre"
'                .AddItem "geometry"
'                .AddItem "holiday"
'                .AddItem "inclusive"
'                .AddItem "IssueRejectionCategory"
'                .AddItem "IssueRejectionType"
'                .AddItem "IssueRejectionAccountability"
'                .AddItem "IssueRejectionReason"
'                .AddItem "IssueInternalDepartment"
'                .AddItem "IssueType"
'                .AddItem "iTunesAdvisorySystem"
'                .AddItem "iTunesAdvisory"
'                .AddItem "iTunesAvailabilityReason"
'                .AddItem "iTUnesBilling"
'                .AddItem "iTunesGenre"
'                .AddItem "iTunesCueUsage"
'                .AddItem "iTunesLocale"
'                .AddItem "iTunesProductCleared"
'                .AddItem "iTunesProvider"
'                .AddItem "iTunesRating"
'                .AddItem "iTunesRole"
'                .AddItem "iTunesFilmRole"
'                .AddItem "iTunesType"
'                .AddItem "iTunesSubtitlesType"
'                .AddItem "iTunesSubType"
'                .AddItem "iTunesVodTypes"
'                .AddItem "iTunesVideoType"
'                .AddItem "jobsearch"
'                .AddItem "jobtype"
'                .AddItem "jobdigitaltype"
'                .AddItem "jobstatus"
'                .AddItem "kidscolangs"
'                .AddItem "language"
'                .AddItem "librarysearch"
'                .AddItem "librarytitle"
'                .AddItem "linestandard"
'                If CheckAccess("/showlocationincombosettings", True) Then .AddItem "location"
                '.AddItem "machine"
'                .AddItem "maxdomeartworktype"
'                .AddItem "maxdomebilling"
'                .AddItem "maxdomeprovider"
'                .AddItem "managementreport"
'                .AddItem "MSRequestType"
'                .AddItem "nochargereasoncode"
'                .AddItem "noisereduction"
'                .AddItem "other"
'                .AddItem "ourreference"
'                .AddItem "overheads-food"
'                .AddItem "overheads-taxi"
'                .AddItem "pipelineunit"
'                .AddItem "portalheaderpages"
'                .AddItem "portalmainpages"
'                .AddItem "portalpurchaseprofilegroups"
'                .AddItem "portaltitlestub"
'                .AddItem "priority"
'                .AddItem "programareas"
'                .AddItem "projectreport"
'                .AddItem "projectstatus"
'                .AddItem "playoutoptions"
'                .AddItem "quotestatus"
'                .AddItem "resourcecategory"
'                .AddItem "role"
'                .AddItem "series"
'                .AddItem "shelf"
'                .AddItem "shineversion"
'                .AddItem "soundchannel"
'                .AddItem "SPETextInPicture"
'                .AddItem "SPEAudioContent"
'                .AddItem "standardlabeltext"
'                .AddItem "stockcode"
'                .AddItem "stockmanufacturer"
'                .AddItem "subcategoryareas"
'                .AddItem "subcategoryequipment"
'                .AddItem "subcategorypeople"
'                .AddItem "sundrytype"
'                .AddItem "technicallogentrytype"
'                .AddItem "timecode"
'                .AddItem "title"
'                .AddItem "trackercommenttypes"
'                .AddItem "trackerdataformats"
'                .AddItem "transcodesystem"
'                .AddItem "TWDC-Title"
'                .AddItem "TWDC-Language"
'                .AddItem "unit"
'                .AddItem "version"
'                .AddItem "videostandard"
'                .AddItem "view"
'                .AddItem "vodplatform"
'                .AddItem "workflowprogress"
'                .AddItem "workstation"
'                .AddItem "wwoffice"
                l_rstXRef.Close
                Set l_rstXRef = Nothing
            End With
            
            
        Case "itunespackagecharacterroles"
        
            Dim l_rstRoles As ADODB.Recordset
            
            Set l_rstRoles = ExecuteSQL("SELECT reference_ID FROM vwiTunesRolesAndPackages WHERE iTunes_PackageID = " & lp_strCriterea & " ORDER BY reference_ID;", g_strExecuteError)
            CheckForSQLError
            
            If Not l_rstRoles.EOF Then
                l_rstRoles.MoveFirst
                Do While Not l_rstRoles.EOF
                    lp_conControl.AddItem Format(l_rstRoles("reference_ID"))
                    l_rstRoles.MoveNext
                Loop
            End If
            
            l_rstRoles.Close
            Set l_rstRoles = Nothing
            
        Case "ourcontact"
            
            Dim l_rstProducers As ADODB.Recordset
            
            Set l_rstProducers = ExecuteSQL("SELECT name FROM resource WHERE (subcategory1 LIKE 'producer') OR (subcategory1 LIKE 'bookings') OR (categorycode LIKE '%/producer%') AND (fd_status <> 'DELETED') ORDER BY name;", g_strExecuteError)
            CheckForSQLError
            
            If Not l_rstProducers.EOF Then
                l_rstProducers.MoveFirst
                Do While Not l_rstProducers.EOF
                    lp_conControl.AddItem Format(l_rstProducers("name"))
                    l_rstProducers.MoveNext
                Loop
            End If
            
            l_rstProducers.Close
            Set l_rstProducers = Nothing
            
        Case "machine"
            
            Set l_rstMachine = ExecuteSQL("SELECT name FROM resource WHERE categorycode LIKE '%/machine%' ORDER BY name", g_strExecuteError)
            CheckForSQLError
            
            If Not l_rstMachine.EOF Then
                l_rstMachine.MoveFirst
                Do While Not l_rstMachine.EOF
                    lp_conControl.AddItem Format(l_rstMachine("name"))
                    l_rstMachine.MoveNext
                Loop
            End If
            
            l_rstMachine.Close
            Set l_rstMachine = Nothing
            
            
        Case "despatchmethod"
            
            Set l_rstMachine = ExecuteSQL("SELECT description FROM xref WHERE category = 'despatchmethod' AND format = 'method' ORDER BY description", g_strExecuteError)
            CheckForSQLError
            
            If Not l_rstMachine.EOF Then
                l_rstMachine.MoveFirst
                Do While Not l_rstMachine.EOF
                    lp_conControl.AddItem Format(l_rstMachine("description"))
                    l_rstMachine.MoveNext
                Loop
            End If
            
            l_rstMachine.Close
            Set l_rstMachine = Nothing
            
            
            
        Case "fields-job"
            l_strSQL = "SELECT * FROM job WHERE jobID = '-1'"
            
            Dim l_rstJob As New ADODB.Recordset
            Set l_rstJob = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            For l_intLoop = 0 To l_rstJob.Fields.Count - 1
                lp_conControl.AddItem "job." & Format(l_rstJob.Fields(l_intLoop).Name)
            Next
            
            l_rstJob.Close
            Set l_rstJob = Nothing
            
            
        Case "fields-library"
            l_strSQL = "SELECT * FROM library WHERE libraryID = '-1'"
            
            Dim l_rstLibrary As New ADODB.Recordset
            Set l_rstLibrary = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            For l_intLoop = 0 To l_rstLibrary.Fields.Count - 1
                lp_conControl.AddItem "library." & Format(l_rstLibrary.Fields(l_intLoop).Name)
            Next
            
            l_rstLibrary.Close
            Set l_rstLibrary = Nothing
        
        Case "fields-quote"
            l_strSQL = "SELECT * FROM quote WHERE quoteID = '-1'"
            
            Dim l_rstQuote As New ADODB.Recordset
            Set l_rstQuote = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            For l_intLoop = 0 To l_rstQuote.Fields.Count - 1
                lp_conControl.AddItem "quote." & Format(l_rstQuote.Fields(l_intLoop).Name)
            Next
            
            l_rstQuote.Close
            Set l_rstQuote = Nothing
        
        Case "fields-purchase"
            l_strSQL = "SELECT * FROM purchaseheader WHERE quoteID = '-1'"
            
            Dim l_rstPurchase As New ADODB.Recordset
            Set l_rstPurchase = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            For l_intLoop = 0 To l_rstQuote.Fields.Count - 1
                lp_conControl.AddItem "purchaseheader." & Format(l_rstPurchase.Fields(l_intLoop).Name)
            Next
            
            l_rstPurchase.Close
            Set l_rstPurchase = Nothing
                        
        Case "allocation", "projectallocationonly"
            l_strSQL = "SELECT name FROM allocation "
            If lp_strCategory = "projectallocationonly" Then l_strSQL = l_strSQL & " WHERE allocationtype <> 'Automatic'"
            l_strSQL = l_strSQL & " ORDER BY name"
            
            Dim l_rstAllocation As New ADODB.Recordset
            Set l_rstAllocation = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            If Not l_rstAllocation.EOF Then
                l_rstAllocation.MoveFirst
            
                Do While Not l_rstAllocation.EOF
                    lp_conControl.AddItem l_rstAllocation("name")
                    l_rstAllocation.MoveNext
                Loop
                
            Else
                If g_intDisableProjects = 0 Then MsgBox "There are no allocation groups set up. Please ask your supervisor to set them up before using this package.", vbExclamation
            End If
            
            
            l_rstAllocation.Close
            Set l_rstAllocation = Nothing
            
        Case "linestandard"
            
            l_strSQL = "SELECT description FROM xref WHERE category = 'linestandard'  ORDER BY description;"
            
            Dim l_rstLineStandard As New ADODB.Recordset
            Set l_rstLineStandard = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            If Not l_rstLineStandard.EOF Then
                l_rstLineStandard.MoveFirst
            
                Do While Not l_rstLineStandard.EOF
                    lp_conControl.AddItem l_rstLineStandard("description")
                    l_rstLineStandard.MoveNext
                Loop
                
             End If
            
            
            l_rstLineStandard.Close
            Set l_rstLineStandard = Nothing
            
        Case "framerate"
        
            l_strSQL = "SELECT description FROM xref WHERE category = 'framerate' AND format = '" & lp_strCriterea & "' ORDER BY forder, description;"
            
            Dim l_rstFrameRate As New ADODB.Recordset
            Set l_rstFrameRate = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            If Not l_rstFrameRate.EOF Then
                l_rstFrameRate.MoveFirst
            
                Do While Not l_rstFrameRate.EOF
                    lp_conControl.AddItem l_rstFrameRate("description")
                    l_rstFrameRate.MoveNext
                Loop
                
             End If
            
            
            l_rstFrameRate.Close
            Set l_rstFrameRate = Nothing
            
        Case "portalpurchaseprofilegroups"
        
            l_strSQL = "SELECT information FROM xref WHERE category = 'portalpurchaseprofilegroups' ORDER BY forder, information;"
            
            Dim l_rstProfileGroups As New ADODB.Recordset
            Set l_rstProfileGroups = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            If Not l_rstProfileGroups.EOF Then
                l_rstProfileGroups.MoveFirst
            
                Do While Not l_rstProfileGroups.EOF
                    lp_conControl.AddItem l_rstProfileGroups("information")
                    l_rstProfileGroups.MoveNext
                Loop
                
             End If
            
            
            l_rstProfileGroups.Close
            Set l_rstProfileGroups = Nothing
            
        Case "datefrequency"
        
            lp_conControl.AddItem "day"
            lp_conControl.AddItem "week"
            lp_conControl.AddItem "fortnight"
            lp_conControl.AddItem "month"
            
        Case "internal/external"
        
            lp_conControl.AddItem "External"
            lp_conControl.AddItem "Internal"
            
        Case "resourcecategory"
        
            
            l_strSQL = "SELECT DISTINCT category FROM resource ORDER BY category"
            
            Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            If Not l_rstXRef.EOF Then
                l_rstXRef.MoveFirst
            End If
            
            Do While Not l_rstXRef.EOF
                lp_conControl.AddItem Trim(" " & l_rstXRef("category"))
                l_rstXRef.MoveNext
            Loop
            
            l_rstXRef.Close
            Set l_rstXRef = Nothing
        
        
        Case "fileoperations"
        
            l_strSQL = "SELECT description FROM event_file_request_type ORDER BY description"
            
            Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            If Not l_rstXRef.EOF Then
                l_rstXRef.MoveFirst
            End If
            
            lp_conControl.AddItem "All Items"
'            lp_conControl.AddItem "All DIVA Archives"
'            lp_conControl.AddItem "All DIVA Restores"
            
            Do While Not l_rstXRef.EOF
                lp_conControl.AddItem Trim(" " & l_rstXRef("description"))
                l_rstXRef.MoveNext
            Loop
            
            l_rstXRef.Close
            Set l_rstXRef = Nothing
        
        Case "resourcesubcategory"
        
            
            l_strSQL = "SELECT DISTINCT subcategory1 FROM resource ORDER BY subcategory1"
            
            Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            If Not l_rstXRef.EOF Then
                l_rstXRef.MoveFirst
            End If
            
            Do While Not l_rstXRef.EOF
                lp_conControl.AddItem Trim(" " & l_rstXRef("subcategory1"))
                l_rstXRef.MoveNext
            Loop
            
            l_rstXRef.Close
            Set l_rstXRef = Nothing
        
        Case "svenskhddtitle"
            
            l_strSQL = "SELECT DISTINCT seriestitle FROM tracker_hdd_item "
            If lp_strCriterea <> "" Then
                l_strSQL = l_strSQL & "WHERE " & lp_strCriterea
            End If
            l_strSQL = l_strSQL & "ORDER BY seriestitle;"
            
            Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            If Not l_rstXRef.EOF Then
                l_rstXRef.MoveFirst
            End If
            
            Do While Not l_rstXRef.EOF
                lp_conControl.AddItem Trim(" " & l_rstXRef("seriestitle"))
                l_rstXRef.MoveNext
            Loop
            
            l_rstXRef.Close
            Set l_rstXRef = Nothing
        
        Case "svenskcomponenttype"
            
            l_strSQL = "SELECT DISTINCT componenttype FROM tracker_svensk_item "
            If lp_strCriterea <> "" Then
                l_strSQL = l_strSQL & "WHERE " & lp_strCriterea
            End If
            l_strSQL = l_strSQL & "ORDER BY componenttype;"
            
            Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            If Not l_rstXRef.EOF Then
                l_rstXRef.MoveFirst
            End If
            
            Do While Not l_rstXRef.EOF
                lp_conControl.AddItem Trim(" " & l_rstXRef("componenttype"))
                l_rstXRef.MoveNext
            Loop
            
            l_rstXRef.Close
            Set l_rstXRef = Nothing
        
        Case "svenskdepartment"
            
            l_strSQL = "SELECT DISTINCT SFDepartment FROM tracker_svensk_item "
            If lp_strCriterea <> "" Then
                l_strSQL = l_strSQL & "WHERE " & lp_strCriterea
            End If
            l_strSQL = l_strSQL & "ORDER BY SFDepartment;"
            
            Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            If Not l_rstXRef.EOF Then
                l_rstXRef.MoveFirst
            End If
            
            Do While Not l_rstXRef.EOF
                lp_conControl.AddItem Trim(" " & l_rstXRef("SFDepartment"))
                l_rstXRef.MoveNext
            Loop
            
            l_rstXRef.Close
            Set l_rstXRef = Nothing
        
        Case "svenskformat"
            
            l_strSQL = "SELECT DISTINCT format FROM tracker_svensk_item "
            If lp_strCriterea <> "" Then
                l_strSQL = l_strSQL & "WHERE " & lp_strCriterea
            End If
            l_strSQL = l_strSQL & "ORDER BY format;"
            
            Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            If Not l_rstXRef.EOF Then
                l_rstXRef.MoveFirst
            End If
            
            Do While Not l_rstXRef.EOF
                lp_conControl.AddItem Trim(" " & l_rstXRef("format"))
                l_rstXRef.MoveNext
            Loop
            
            l_rstXRef.Close
            Set l_rstXRef = Nothing
        
        Case "svenskprogramtype"
            
            l_strSQL = "SELECT DISTINCT programtype FROM tracker_svensk_item "
            If lp_strCriterea <> "" Then
                l_strSQL = l_strSQL & "WHERE " & lp_strCriterea
            End If
            l_strSQL = l_strSQL & "ORDER BY programtype;"
            
            Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            If Not l_rstXRef.EOF Then
                l_rstXRef.MoveFirst
            End If
            
            Do While Not l_rstXRef.EOF
                lp_conControl.AddItem Trim(" " & l_rstXRef("programtype"))
                l_rstXRef.MoveNext
            Loop
            
            l_rstXRef.Close
            Set l_rstXRef = Nothing
        
        Case "svenskrightsowner"
            
            l_strSQL = "SELECT DISTINCT rightsowner FROM tracker_svensk_item "
            If lp_strCriterea <> "" Then
                l_strSQL = l_strSQL & "WHERE " & lp_strCriterea
            End If
            l_strSQL = l_strSQL & "ORDER BY rightsowner;"
            
            Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            If Not l_rstXRef.EOF Then
                l_rstXRef.MoveFirst
            End If
            
            Do While Not l_rstXRef.EOF
                lp_conControl.AddItem Trim(" " & l_rstXRef("rightsowner"))
                l_rstXRef.MoveNext
            Loop
            
            l_rstXRef.Close
            Set l_rstXRef = Nothing
        
        Case "svenskprojectmanager"
            
            l_strSQL = "SELECT DISTINCT projectmanager FROM tracker_svensk_item "
            If lp_strCriterea <> "" Then
                l_strSQL = l_strSQL & "WHERE " & lp_strCriterea
            End If
            l_strSQL = l_strSQL & "ORDER BY projectmanager;"
            
            Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            If Not l_rstXRef.EOF Then
                l_rstXRef.MoveFirst
            End If
            
            Do While Not l_rstXRef.EOF
                lp_conControl.AddItem Trim(" " & l_rstXRef("projectmanager"))
                l_rstXRef.MoveNext
            Loop
            
            l_rstXRef.Close
            Set l_rstXRef = Nothing
        
        Case "svensktitle"
            
            l_strSQL = "SELECT DISTINCT title FROM tracker_svensk_item "
            If lp_strCriterea <> "" Then
                l_strSQL = l_strSQL & "WHERE title IS NOT NULL " & lp_strCriterea
            End If
            l_strSQL = l_strSQL & "ORDER BY title;"
            
            Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            If Not l_rstXRef.EOF Then
                l_rstXRef.MoveFirst
            End If
            
            Count = 0
            Do While Not l_rstXRef.EOF 'And Count < 45
                lp_conControl.AddItem Trim(" " & l_rstXRef("Title"))
                l_rstXRef.MoveNext
                Count = Count + 1
            Loop
            
            l_rstXRef.Close
            Set l_rstXRef = Nothing
        
        Case "svenskspecialproject"
            
            l_strSQL = "SELECT DISTINCT specialproject FROM tracker_svensk_item "
            If lp_strCriterea <> "" Then
                l_strSQL = l_strSQL & "WHERE specialproject IS NOT NULL " & lp_strCriterea
            End If
            l_strSQL = l_strSQL & "ORDER BY specialproject;"
            
            Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
            
            CheckForSQLError
            
            If Not l_rstXRef.EOF Then
                l_rstXRef.MoveFirst
            End If
            
            Count = 0
            Do While Not l_rstXRef.EOF 'And Count < 45
                lp_conControl.AddItem Trim(" " & l_rstXRef("specialproject"))
                l_rstXRef.MoveNext
                Count = Count + 1
            Loop
            
            l_rstXRef.Close
            Set l_rstXRef = Nothing

        Case "yes-no"
        
            lp_conControl.AddItem "YES"
            lp_conControl.AddItem "NO"
            
        Case Else
            
            If Left(lp_strCategory, 11) = "subcategory" Then
                                          
           
                lp_strCategory1 = Right(lp_strCategory, Len(lp_strCategory) - 11)
                
                l_strSQL = "SELECT DISTINCT subcategory1 FROM resource WHERE category = '" & lp_strCategory1 & "' ORDER BY subcategory1"
                
                Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
                
                CheckForSQLError
                
                If Not l_rstXRef.EOF Then
                    l_rstXRef.MoveFirst
                End If
                
                Do While Not l_rstXRef.EOF
                    lp_conControl.AddItem Trim(" " & l_rstXRef("subcategory1"))
                    l_rstXRef.MoveNext
                Loop
                
                l_rstXRef.Close
                Set l_rstXRef = Nothing
                
                
            ElseIf Left(lp_strCategory, 7) = "subcat2" Then
                                          
           
                lp_strCategory1 = Right(lp_strCategory, Len(lp_strCategory) - 7)
                
                l_strSQL = "SELECT DISTINCT subcategory2 FROM resource WHERE subcategory1 = '" & lp_strCategory1 & "' ORDER BY subcategory2"
                
                Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
                
                CheckForSQLError
                
                If Not l_rstXRef.EOF Then
                    l_rstXRef.MoveFirst
                End If
                
                Do While Not l_rstXRef.EOF
                    lp_conControl.AddItem Trim(" " & l_rstXRef("subcategory2"))
                    l_rstXRef.MoveNext
                Loop
                
                l_rstXRef.Close
                Set l_rstXRef = Nothing
                
                
            ElseIf Left(lp_strCategory, 19) = "stockcodesforformat" Then
            
            
                lp_strCategory1 = Right(lp_strCategory, Len(lp_strCategory) - 19)
                
                l_strSQL = "SELECT DISTINCT description FROM xref WHERE category = 'stockcode' AND format = '" & lp_strCategory1 & "' ORDER BY description"
                
                Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
                
                CheckForSQLError
                
                If Not l_rstXRef.EOF Then
                    l_rstXRef.MoveFirst
                End If
                
                Do While Not l_rstXRef.EOF
                    lp_conControl.AddItem Trim(" " & l_rstXRef("description"))
                    l_rstXRef.MoveNext
                Loop
                
                l_rstXRef.Close
                Set l_rstXRef = Nothing
                
            Else
                
                l_strSQL = "SELECT DISTINCT description, forder FROM xref WHERE category = '" & QuoteSanitise(lp_strCategory) & "' " & lp_strCriterea & " ORDER BY forder, description"
                
                
                Set l_rstXRef = ExecuteSQL(l_strSQL, g_strExecuteError)
                
                CheckForSQLError
                
                If Not l_rstXRef.EOF Then
                    l_rstXRef.MoveFirst
                End If
                
                lp_conControl.AddItem ""
                
                Do While Not l_rstXRef.EOF
                    lp_conControl.AddItem Trim(" " & l_rstXRef("description"))
                    l_rstXRef.MoveNext
                Loop
                
                l_rstXRef.Close
                Set l_rstXRef = Nothing
                
                
            End If
            
    End Select
    
    
    
    
    LockControl lp_conControl, False
    DoEvents
    
    'MDIForm1.Caption = l_strTempCaption
    Screen.MousePointer = vbDefault
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    Resume Next
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub AddResourceToView(lp_lngResourceID As Long, lp_lngViewID As Long)

Dim l_strSQL As String
l_strSQL = "INSERT INTO resourceview (resourceID, viewID, fd_orderby, height) VALUES (" & lp_lngResourceID & ", " & lp_lngViewID & ", '" & GetCount("SELECT count(resourceviewID) FROM resourceview WHERE viewID = '" & lp_lngViewID & "';") + 1 & "','580');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Sub
Sub RemoveResourceFromView(lp_lngResourceViewID As Long)

Dim l_lngResourceOrder As Integer
l_lngResourceOrder = GetData("resourceview", "fd_orderby", "resourceviewID", lp_lngResourceViewID)

Dim l_lngViewID As Long
l_lngViewID = GetData("resourceview", "viewID", "resourceviewID", lp_lngResourceViewID)

Dim l_strSQL As String

l_strSQL = "UPDATE resourceview SET fd_orderby = fd_orderby -1 WHERE fd_orderby > '" & l_lngResourceOrder & "' AND viewID = '" & l_lngViewID & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_strSQL = "DELETE FROM resourceview WHERE resourceviewID = '" & lp_lngResourceViewID & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError


End Sub

Function PostNewCustomersToAccounts() As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_rstNewCustomers As ADODB.Recordset
    Dim l_strSQL As String, l_lngBatchNumber As Long
    Dim cnn2 As ADODB.Connection, rst As ADODB.Recordset
    Dim ACCNUMBER As String
    
    l_strSQL = "SELECT * FROM company WHERE postedtoaccountsdate IS NULL AND iscustomer = 1 and system_active <> 0;"
    Set l_rstNewCustomers = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    If Not l_rstNewCustomers.EOF Then
        'get the next batch number
        l_lngBatchNumber = GetNextSequence("batchnumber")
        
        l_rstNewCustomers.MoveFirst
        Do While Not l_rstNewCustomers.EOF
            If Trim(" " & l_rstNewCustomers("salesledgercode")) = "" Then
                MsgBox "Customer " & l_rstNewCustomers("accountcode") & " has no Sales Ledger Code set. Batch Posting Aborted.", vbCritical, "Error Posting Invoices"
                PostNewCustomersToAccounts = False
                l_rstNewCustomers.Close
                Set l_rstNewCustomers = Nothing
                Exit Function
            End If
            ' Do the traditional posting anyway
            l_strSQL = "INSERT INTO batchcompany (batchnumber, code, clientname, address1, postcode, telephone, fax, vatnumber, ecvatstatus, eccountry, ecvat, ratecard, ready, clientnumber, BusinessUnit, email) VALUES ("
            l_strSQL = l_strSQL & l_lngBatchNumber & ", "
            l_strSQL = l_strSQL & "'" & l_rstNewCustomers("salesledgercode") & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstNewCustomers("name")) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstNewCustomers("address")) & "', "
            l_strSQL = l_strSQL & "'" & l_rstNewCustomers("postcode") & "', "
            l_strSQL = l_strSQL & "'" & l_rstNewCustomers("telephone") & "', "
            l_strSQL = l_strSQL & "'" & l_rstNewCustomers("fax") & "', "
            Select Case l_rstNewCustomers("vatcode")
                Case "G"
                    l_strSQL = l_strSQL & "'" & Mid(l_rstNewCustomers("vatnumber"), 3, Len(l_rstNewCustomers("vatnumber")) - 2) & "', "
                    l_strSQL = l_strSQL & "'G', "
                    l_strSQL = l_strSQL & "'" & Left(l_rstNewCustomers("vatnumber"), 2) & "', "
                    l_strSQL = l_strSQL & "'E', "
                Case "7"
                    l_strSQL = l_strSQL & "'', "
                    l_strSQL = l_strSQL & "'7', "
                    l_strSQL = l_strSQL & "'', "
                    l_strSQL = l_strSQL & "'O', "
                Case Else
                    l_strSQL = l_strSQL & "'', "
                    l_strSQL = l_strSQL & "'1', "
                    l_strSQL = l_strSQL & "'GB', "
                    l_strSQL = l_strSQL & "'U', "
            End Select
            l_strSQL = l_strSQL & "'" & l_rstNewCustomers("ratecard") & "', "
            l_strSQL = l_strSQL & "'READY', "
            l_strSQL = l_strSQL & "'" & l_rstNewCustomers("companyID") & "', "
            l_strSQL = l_strSQL & "'" & l_rstNewCustomers("BusinessUnit") & "', "
            l_strSQL = l_strSQL & "'" & l_rstNewCustomers("email") & "') "
            
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
                
            If g_intPostToPriority <> 0 Then
            
                'Then also do the priority posting
                Set cnn2 = New ADODB.Connection
                cnn2.ConnectionString = "DRIVER=SQL Server;SERVER=172.19.4.232;DATABASE=rreur;UID=RReuropeSQL;PWD=Re1234"
                cnn2.Open
                
                Set rst = cnn2.Execute("SELECT ACCNAME FROM HSLOADACCOUNTS WHERE ORIGACCNAME = '" & l_rstNewCustomers("salesledgercode") & "';")
                If rst.EOF Then
                    rst.Close
                    l_strSQL = "INSERT INTO HSLOADACCOUNTS (TYPE, DNAME, ACCNAME, ACCDES, ADDRESS, ZIP, PHONE, FAX, VATNUM, SPEC1, COUNTRYNAME, SPEC3, SPEC6, DETAILS, EMAIL) VALUES ('R', 'CETA', "
                    l_strSQL = l_strSQL & "'" & Trim(l_rstNewCustomers("salesledgercode")) & "', "
                    l_strSQL = l_strSQL & "'" & Trim(QuoteSanitise(StrReverse(l_rstNewCustomers("name")))) & "', "
                    l_strSQL = l_strSQL & "'" & Right(Trim(QuoteSanitise(StrReverse(Replace(l_rstNewCustomers("address"), vbNewLine, " ")))), 120) & "', "
                    l_strSQL = l_strSQL & "'" & Trim(l_rstNewCustomers("postcode")) & "', "
                    l_strSQL = l_strSQL & "'" & Trim(l_rstNewCustomers("telephone")) & "', "
                    l_strSQL = l_strSQL & "'" & Trim(l_rstNewCustomers("fax")) & "', "
                    Select Case l_rstNewCustomers("vatcode")
                        Case "G"
                            l_strSQL = l_strSQL & "'" & Trim(Mid(l_rstNewCustomers("vatnumber"), 3, Len(l_rstNewCustomers("vatnumber")) - 2)) & "', "
                            l_strSQL = l_strSQL & "'000', "
                            l_strSQL = l_strSQL & "'" & Trim(Left(l_rstNewCustomers("vatnumber"), 2)) & "', "
                            l_strSQL = l_strSQL & "'E', "
                        Case "7"
                            l_strSQL = l_strSQL & "'', "
                            l_strSQL = l_strSQL & "'800', "
                            l_strSQL = l_strSQL & "'', "
                            l_strSQL = l_strSQL & "'O', "
                        Case Else
                            l_strSQL = l_strSQL & "'', "
                            l_strSQL = l_strSQL & "'100', "
                            l_strSQL = l_strSQL & "'GB', "
                            l_strSQL = l_strSQL & "'U', "
                    End Select
                    l_strSQL = l_strSQL & "'" & StrReverse(Trim(" " & l_rstNewCustomers("Businessunit"))) & "', "
                    l_strSQL = l_strSQL & "'READY', "
                    l_strSQL = l_strSQL & "'" & Trim(l_rstNewCustomers("email")) & "') "
                    
                Else
                    ACCNUMBER = rst("ACCNAME")
                    l_strSQL = "UPDATE HSLOADACCOUNTS SET ACCNAME = '" & ACCNUMBER & "', ORIGACCNAME = '" & l_rstNewCustomers("salesledgercode") & "' WHERE ACCNAME = '" & l_rstNewCustomers("salesledgercode") & "';"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    rst.Close
                    l_strSQL = "UPDATE HSLOADACCOUNTS SET "
                    l_strSQL = l_strSQL & "ADDRESS = '" & Right(Trim(QuoteSanitise(StrReverse(Replace(l_rstNewCustomers("address"), vbNewLine, " ")))), 120) & "', "
                    l_strSQL = l_strSQL & "ZIP = '" & Trim(QuoteSanitise(l_rstNewCustomers("postcode"))) & "', "
                    l_strSQL = l_strSQL & "PHONE = '" & Trim(QuoteSanitise(l_rstNewCustomers("telephone"))) & "', "
                    l_strSQL = l_strSQL & "FAX = '" & Trim(QuoteSanitise(l_rstNewCustomers("fax"))) & "', "
                    Select Case l_rstNewCustomers("vatcode")
                        Case "G"
                            l_strSQL = l_strSQL & "VATNUM = '" & Trim(Mid(l_rstNewCustomers("vatnumber"), 3, Len(l_rstNewCustomers("vatnumber")) - 2)) & "', "
                            l_strSQL = l_strSQL & "SPEC1 = '000', "
                            l_strSQL = l_strSQL & "COUNTRYNAME = '" & Trim(Left(l_rstNewCustomers("vatnumber"), 2)) & "', "
                            l_strSQL = l_strSQL & "SPEC3 = 'E', "
                        Case "7"
                            l_strSQL = l_strSQL & "VATNUM = '', "
                            l_strSQL = l_strSQL & "SPEC1 = '800', "
                            l_strSQL = l_strSQL & "COUNTRYNAME = '', "
                            l_strSQL = l_strSQL & "SPEC3 = 'O', "
                        Case Else
                            l_strSQL = l_strSQL & "VATNUM = '', "
                            l_strSQL = l_strSQL & "SPEC1 = '100', "
                            l_strSQL = l_strSQL & "COUNTRYNAME = 'GB', "
                            l_strSQL = l_strSQL & "SPEC3 = 'U', "
                    End Select
                    l_strSQL = l_strSQL & "SPEC6 = '" & StrReverse(Trim(" " & l_rstNewCustomers("BusinessUnit"))) & "', "
                    l_strSQL = l_strSQL & "DETAILS = 'READY', "
                    l_strSQL = l_strSQL & "EMAIL = '" & l_rstNewCustomers("email") & "', "
                    l_strSQL = l_strSQL & "CUSTLOADED = '' "
                    l_strSQL = l_strSQL & "SPECLOADED = '' "
                    l_strSQL = l_strSQL & "WHERE ACCNAME = '" & ACCNUMBER & "';"
                End If
                
                Debug.Print l_strSQL
                'Post it to our local copy of the table for checking purposes
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                'Then post it to Israel and close that connection
                cnn2.Execute l_strSQL
                cnn2.Close
                Set rst = Nothing
                Set cnn2 = Nothing
                
            End If
            
            SetData "company", "postedtoaccountsdate", "companyID", l_rstNewCustomers("companyID"), Format(Now, "MM/DD/YYYY")
            l_rstNewCustomers.MoveNext
        Loop
        'Print a report of what we've done
        PrintCrystalReport g_strLocationOfCrystalReportFiles & "\BatchCustomer.rpt", "{batchcompany.batchnumber} = " & l_lngBatchNumber, False
        ExportCrystalReport g_strLocationOfCrystalReportFiles & "\BatchCustomer.rpt", "{batchcompany.batchnumber} = " & l_lngBatchNumber, g_strUploadInvoiceLocation & l_lngBatchNumber & ".xls"
        
    End If
    
    l_rstNewCustomers.Close
    Set l_rstNewCustomers = Nothing
    
    'TVCodeTools ErrorHandlerStart

    PostNewCustomersToAccounts = True

PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    PostNewCustomersToAccounts = False
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub PrintCostingPage(lp_lngJobID As Long, lp_intCopiesToPrint As Integer)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_lngInvoiceNumber  As Long
    Dim l_strReportToPrint As String
    Dim l_strSelectionFormula As String
    Dim l_intTwoTierPricing As Integer
    Dim l_lngCompanyID As Long
    Dim l_lngCreditNoteNumber As Long
    Dim l_lngItemID As Long
            
            
    If Not CheckAccess("/printcostingpage") Then
        Exit Sub
    End If
    
    If Val(lp_lngJobID) > 0 Then
        
        l_lngInvoiceNumber = GetData("job", "invoicenumber", "jobID", lp_lngJobID)
        l_lngCreditNoteNumber = GetData("job", "creditnotenumber", "jobID", lp_lngJobID)
        
        If l_lngInvoiceNumber = 0 And l_lngCreditNoteNumber = 0 Then
            
            MsgBox "Problem with invoice. This job doesnt have an invoice number!", vbExclamation
            Exit Sub
        End If

        If UCase(GetData("job", "jobtype", "jobID", lp_lngJobID)) = "CREDIT" Then
            If InStr(GetData("company", "cetaclientcode", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID)), "/vdmsinvoice") > 0 Then
                l_strReportToPrint = g_strLocationOfCrystalReportFiles & "creditnote_vdms_temp.rpt"
            Else
                l_strReportToPrint = g_strLocationOfCrystalReportFiles & "creditnote.rpt"
            End If
            l_strSelectionFormula = "{job.jobID} = " & lp_lngJobID
            l_lngItemID = l_lngCreditNoteNumber
        Else
            If InStr(GetData("company", "cetaclientcode", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID)), "/invoicewellcome") > 0 Then
                l_strReportToPrint = g_strLocationOfCrystalReportFiles & "invoicewithsummary.rpt"
            ElseIf InStr(GetData("company", "cetaclientcode", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID)), "/rrsatinvoice") > 0 Then
                l_strReportToPrint = g_strLocationOfCrystalReportFiles & "invoice_rrsat.rpt"
            ElseIf InStr(GetData("company", "cetaclientcode", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID)), "/vdmsinvoice") > 0 Then
                l_strReportToPrint = g_strLocationOfCrystalReportFiles & "invoice_vdms_temp.rpt"
            Else
                l_strReportToPrint = g_strLocationOfCrystalReportFiles & "invoice.rpt"
            End If
            l_strSelectionFormula = "{job.invoicenumber} = " & l_lngInvoiceNumber
            l_lngItemID = l_lngInvoiceNumber
        End If

        If CLng(lp_intCopiesToPrint) > 0 Then PrintCrystalReport l_strReportToPrint, l_strSelectionFormula, g_blnPreviewReport, CLng(lp_intCopiesToPrint)
        ReportToPDF l_strReportToPrint, l_strSelectionFormula, g_strPDFLocation & "\Invoices\" & l_lngItemID & ".pdf"
        l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
        If InStr(GetData("Company", "cetaclientcode", "companyID", l_lngCompanyID), "/makepdfinvoices") > 0 Then
            If GetData("Company", "invoiceemail", "companyID", l_lngCompanyID) <> "" Then
                SendOutlookEmail GetData("Company", "invoiceemail", "companyID", l_lngCompanyID), "Invoice Number " & l_lngItemID, "A PDF Invoice is attached.", g_strPDFLocation & "\Invoices\" & l_lngItemID & ".pdf"
            Else
                SendOutlookEmail GetData("Company", "email", "companyID", l_lngCompanyID), "Invoice Number " & l_lngItemID, "A PDF Invoice is attached.", g_strPDFLocation & "\Invoices\" & l_lngItemID & ".pdf"
            End If
        End If
        
        If GetData("setting", "value", "name", "VDMSEmailForInvoices") <> "" Then
            SendSMTPMail GetData("setting", "value", "name", "VDMSEmailForInvoices"), "", "VDMS Acton " & IIf(UCase(GetData("job", "jobtype", "jobID", lp_lngJobID)) = "CREDIT", "Credit Note ", "Invoice ") & l_lngItemID & _
            " Raised", g_strPDFLocation & "\Invoices\" & l_lngItemID & ".pdf", "Document is attached", True, "", "", g_strCETAAdministratorEmail
        End If
        
        l_intTwoTierPricing = InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/two-tier-pricing")
        
        If l_intTwoTierPricing <> 0 Then
            
            l_strReportToPrint = g_strLocationOfCrystalReportFiles & "\refcred.rpt"
            
            l_strSelectionFormula = "{costing.invoicenumber} = " & l_lngInvoiceNumber
            l_lngItemID = GetData("job", "refundedcreditnotenumber", "jobID", lp_lngJobID)
            PrintCrystalReport l_strReportToPrint, l_strSelectionFormula, g_blnPreviewReport, CLng(lp_intCopiesToPrint)
            ReportToPDF l_strReportToPrint, l_strSelectionFormula, g_strPDFLocation & "\Invoices\" & l_lngItemID & ".pdf"
            
        End If
        
    Else
        NoJobSelectedMessage
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub PrintCreditNote(lp_lngJobID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/printcreditnote") Then
        Exit Sub
    End If
    
    If Val(lp_lngJobID) > 0 Then
        
        Dim l_lngCreditNoteNumber  As Long
        l_lngCreditNoteNumber = GetData("job", "creditnotenumber", "jobID", lp_lngJobID)
        
        Dim l_strReportToPrint As String
        Dim l_strSelectionFormula As String
        
        l_strReportToPrint = g_strLocationOfCrystalReportFiles & "\credit.rpt"
        l_strSelectionFormula = "{costing.creditnotenumber} = " & l_lngCreditNoteNumber
        
        PrintCrystalReport l_strReportToPrint, l_strSelectionFormula, False
        
        'add a history line to the jobn
        AddJobHistory lp_lngJobID, "Printed Credit Note"
        
    Else
        NoJobSelectedMessage
    End If
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub


Sub AllocateClipIDNumbers(ByVal lp_lngJobID As Long)

    Dim l_rstClips As New ADODB.Recordset
    Dim l_con As New ADODB.Connection
    Dim l_strSQL As String
    Dim l_intCounter As Integer
    Dim l_lngJobDetailID As Long
    Dim l_intTheCount  As Integer
    Dim l_intLoop  As Integer
       
    Debug.Print "START: " & Timer
       
    'get how many are already allocated
    l_strSQL = "SELECT Count(jobdetailID) FROM jobdetail WHERE jobID = '" & lp_lngJobID & "'and (clipID IS NOT NULL AND clipID <> '')"
    l_intCounter = GetCount(l_strSQL)
    
    'print to debug
    Debug.Print l_intCounter
    
    Debug.Print "GOT COUNT: " & Timer
    
    'get all the records which dont have a clipID
    l_strSQL = "SELECT jobdetailID FROM jobdetail WHERE (jobID = '" & lp_lngJobID & "') AND (clipID IS NULL OR ClipID = '') ORDER BY fd_orderby"
    
    'open the connection
    l_con.Open g_strConnection
    
    Debug.Print "CONNECTION OPEN: " & Timer
    
    l_rstClips.Open l_strSQL, l_con, adOpenForwardOnly, adLockReadOnly
    
    Debug.Print "RECORDSET OPEN: " & Timer
    
    
    'move to the first record
    If Not l_rstClips.EOF Then
        l_rstClips.MoveFirst
    End If
    
    Debug.Print "MOVED TO FIRST CLIP: " & Timer
    
    'get how many (probably none) are already allocated
    l_strSQL = "SELECT Count(jobdetailID) FROM jobdetail WHERE (jobID = '" & lp_lngJobID & "')  AND (clipID IS NULL OR ClipID = '') ORDER BY fd_orderby"
    l_intTheCount = GetCount(l_strSQL)
    
    Debug.Print "GOT COUNT 2: " & Timer
    
    Do While Not l_rstClips.EOF
        'increment the counter
        
        l_intCounter = l_intCounter + 1
        
        l_lngJobDetailID = l_rstClips("jobdetailID")
        
        
        'update the clipID field!
        l_strSQL = "UPDATE jobdetail SET clipID = '" & lp_lngJobID & "_" & Format(l_intCounter, "000") & "' WHERE jobdetailID = '" & l_lngJobDetailID & "';"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        Debug.Print "UPDATED CLIP " & lp_lngJobID & "_" & Format(l_intCounter, "000") & ": " & Timer
        
        'loop
        
        l_rstClips.MoveNext
    
    Loop

    'close the connections
    l_rstClips.Close
    Set l_rstClips = Nothing
    
    Debug.Print "CLOSED RECORDSET: " & Timer

    l_con.Close
    Set l_con = Nothing
    
    Debug.Print "CLOSED CONNECTION: " & Timer
    
    'Nothing
    Beep

End Sub

Sub ExportCrystalReport(ByRef lp_strRPTFileName As String, ByRef lp_strSelectionFormula As String, ByRef lp_strExportFileName As String)
    
    Screen.MousePointer = vbHourglass
    
    Dim crxApplication As CRAXDRT.Application
    Dim crxReport As CRAXDRT.Report
    
    Set crxApplication = New CRAXDRT.Application
    
    Set crxReport = crxApplication.OpenReport(GetUNCNameNT(lp_strRPTFileName))
    
    If Len(lp_strSelectionFormula) > 0 Then
        crxReport.RecordSelectionFormula = lp_strSelectionFormula
    End If
    
    crxReport.DiscardSavedData
    
    crxReport.ExportOptions.DestinationType = crEDTDiskFile
    crxReport.ExportOptions.FormatType = crEFTExcelDataOnly
    crxReport.ExportOptions.DiskFileName = lp_strExportFileName
    crxReport.Export False

    'Dim l_intLoop  As Integer
    'For l_intLoop = 0 To 10000
    '    DoEvents
    'Next
     
    Set crxReport = Nothing
    crxApplication.CanClose
    Set crxApplication = Nothing
    
    DoEvents
    
    Screen.MousePointer = vbDefault

End Sub

Sub ExportCrystalReportAsPDF(ByRef lp_strRPTFileName As String, ByRef lp_strExportFileName As String)

    Screen.MousePointer = vbHourglass
    
    Dim crxApplication As CRAXDRT.Application
    Dim crxReport As CRAXDRT.Report
    
    Set crxApplication = New CRAXDRT.Application
    Set crxReport = crxApplication.OpenReport(GetUNCNameNT(lp_strRPTFileName))
    
    crxReport.DiscardSavedData
    
    crxReport.ExportOptions.DestinationType = crEDTDiskFile
    crxReport.ExportOptions.FormatType = crEFTPortableDocFormat
    crxReport.ExportOptions.DiskFileName = lp_strExportFileName
    crxReport.ExportOptions.ExcelUseConstantColumnWidth = False
    crxReport.ExportOptions.ExcelUseFormatInDataOnly = True
    crxReport.Export False

    'Dim l_intLoop  As Integer
    'For l_intLoop = 0 To 10000
    '    DoEvents
    'Next
     
    Set crxReport = Nothing
    crxApplication.CanClose
    Set crxApplication = Nothing
    
    DoEvents
    
    Screen.MousePointer = vbDefault

End Sub

Sub PrintCrystalReport(ByRef lp_strRPTFileName As String, ByRef lp_strSelectionFormula As String, ByRef lp_blnPreview As Boolean, Optional ByRef lp_lngCopies As Long, Optional ByRef lp_strPrinterToPrintTo As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Screen.MousePointer = vbHourglass
    
    Dim crxApplication As CRAXDRT.Application
    Dim crxReport As CRAXDRT.Report
    
    Set crxApplication = New CRAXDRT.Application
    
    Set crxReport = crxApplication.OpenReport(GetUNCNameNT(lp_strRPTFileName))
    
    If Len(lp_strSelectionFormula) > 0 Then
        crxReport.RecordSelectionFormula = lp_strSelectionFormula
    End If
    
    'Check whether a number of copies was sent, and if not set to 1
    If lp_lngCopies = 0 Then
        lp_lngCopies = 1
    End If
    
    If lp_blnPreview = True Then lp_lngCopies = 1
    
    crxReport.DiscardSavedData
    
    If lp_blnPreview = False Then
        
        If lp_strPrinterToPrintTo <> "" Then
            crxReport.PrinterSetup 0
            
        End If
        
        crxReport.PrintOut False, lp_lngCopies
        
    Else

               
        frmCrystalPreview.CRViewer91.ReportSource = crxReport
        frmCrystalPreview.CRViewer91.ViewReport
        frmCrystalPreview.WindowState = vbMaximized
        
        
        Do While frmCrystalPreview.CRViewer91.IsBusy            'ZOOM METHOD DOES NOT WORK WHILE
            DoEvents                                            'REPORT IS LOADING, SO WE MUST PAUSE
        Loop                                                    'WHILE REPORT LOADS.
         
        frmCrystalPreview.CRViewer91.Zoom 100
        
        Screen.MousePointer = vbDefault
        frmCrystalPreview.Show vbModal
        
    End If
    
    'Dim l_intLoop  As Integer
    'For l_intLoop = 0 To 10000
    '    DoEvents
    'Next
     
    Set crxReport = Nothing
    crxApplication.CanClose
    Set crxApplication = Nothing
    
    DoEvents
    
    Screen.MousePointer = vbDefault
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    

PROC_ERR:
    
    Screen.MousePointer = vbDefault
     If InStr(lp_strRPTFileName, "quote") = 0 Then
        MsgBox Err.Description, vbExclamation
        'TVCodeTools ErrorHandlerEnd
    End If
    
    Resume PROC_EXIT
    
    'TVCodeTools ErrorHandlerEnd
    
End Sub


Sub PreviewProjectReport(ByVal lp_lngProjectNumber As Long, ByVal lp_strProjectReportName As String)

If Not CheckAccess("/printproject") Then Exit Sub

Dim l_strReportFileName As String, l_blnPreview As Boolean
l_strReportFileName = GetData("xref", "information", "description", lp_strProjectReportName)

If l_strReportFileName = "" Then Exit Sub

Dim l_strSelectionFormula As String

Select Case LCase(l_strReportFileName)

Case "sessionlist.rpt"
    
    l_strSelectionFormula = "{job.fd_status} <> 'Cancelled' AND "
    l_strSelectionFormula = l_strSelectionFormula & "{job.fd_status} <> 'Hold Cost' AND "
    l_strSelectionFormula = l_strSelectionFormula & "{job.fd_status} <> 'Costed' AND "
    l_strSelectionFormula = l_strSelectionFormula & "{job.fd_status} <> 'Sent To Accounts' AND "
    l_strSelectionFormula = l_strSelectionFormula & "{job.fd_status} <> 'Invoice' AND "
    l_strSelectionFormula = l_strSelectionFormula & "{job.fd_status} <> 'No Charge' AND "
    l_strSelectionFormula = l_strSelectionFormula & "{job.fd_status} <> 'Invoiced' AND "
    l_strSelectionFormula = l_strSelectionFormula & "{job.fd_status} <> 'Repeat' AND "
    l_strSelectionFormula = l_strSelectionFormula & "{job.fd_status} <> 'Completed' AND "
    l_strSelectionFormula = l_strSelectionFormula & "{job.fd_status} <> 'Cancel' AND "
    l_strSelectionFormula = l_strSelectionFormula & "{job.fd_status} <> 'Cancelled' AND "
    l_strSelectionFormula = l_strSelectionFormula & "{project.projectnumber} = " & lp_lngProjectNumber
Case "transactionsummary.rpt"
     l_strSelectionFormula = "{project.projectnumber} = " & lp_lngProjectNumber

Case Else
    l_strSelectionFormula = "{job.projectnumber} = " & lp_lngProjectNumber
    l_strSelectionFormula = l_strSelectionFormula & " AND {job.fd_status} <> 'Cancel' AND "
    l_strSelectionFormula = l_strSelectionFormula & "{job.fd_status} <> 'Cancelled' AND "
    l_strSelectionFormula = l_strSelectionFormula & "{job.fd_status} <> 'No Charge'"
End Select

PrintCrystalReport g_strLocationOfCrystalReportFiles & l_strReportFileName, l_strSelectionFormula, True



End Sub


Sub PrintCrystalReportUsingSQL(lp_strRPTFileName As String, lp_strSQL As String, lp_blnPreview As Boolean, Optional lp_strSort As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Screen.MousePointer = vbHourglass
    
    Dim crxApplication As New CRAXDRT.Application
    Dim crxReport As New CRAXDRT.Report
    
    Set crxReport = crxApplication.OpenReport(GetUNCNameNT(lp_strRPTFileName))
    
    crxReport.SQLQueryString = RemoveSelectFields(lp_strSQL)
    
    crxReport.DiscardSavedData
    
    If lp_blnPreview = False Then
        
        crxReport.PrintOut False
    Else
        
        frmCrystalPreview.CRViewer91.ReportSource = crxReport
        
        frmCrystalPreview.CRViewer91.ViewReport
        frmCrystalPreview.WindowState = vbMaximized
        frmCrystalPreview.Show vbModal
        frmCrystalPreview.CRViewer91.Zoom 100
    End If
    
    Set crxReport = Nothing
    crxApplication.CanClose
    Set crxApplication = Nothing
    
    Screen.MousePointer = vbDefault
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Screen.MousePointer = vbDefault
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub PrintCrystalReportUsingCleanSQL(lp_strRPTFileName As String, lp_strSQL As String, lp_blnPreview As Boolean)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Screen.MousePointer = vbHourglass
    
    Dim crxApplication As New CRAXDRT.Application
    Dim crxReport As New CRAXDRT.Report
    
    Set crxReport = crxApplication.OpenReport(GetUNCNameNT(lp_strRPTFileName))
    
    crxReport.SQLQueryString = lp_strSQL
    
    crxReport.DiscardSavedData
    
    If lp_blnPreview = False Then
        
        crxReport.PrintOut False
    Else
        
        frmCrystalPreview.CRViewer91.ReportSource = crxReport
        
        frmCrystalPreview.CRViewer91.ViewReport
        frmCrystalPreview.WindowState = vbMaximized
        frmCrystalPreview.Show vbModal
        frmCrystalPreview.CRViewer91.Zoom 100
    End If
    
    Set crxReport = Nothing
    crxApplication.CanClose
    Set crxApplication = Nothing
    
    Screen.MousePointer = vbDefault
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Screen.MousePointer = vbDefault
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub


Function RemoveSelectFields(lp_strSQLString As String) As String


Dim l_strSQL As String, l_intStartPos As Integer, l_intEndPos As Integer, l_intLenToKeep As Integer

l_intStartPos = InStr(UCase(lp_strSQLString), "FROM")
l_intEndPos = InStr(UCase(lp_strSQLString), "ORDER BY")

If l_intEndPos = 0 Then
    l_intEndPos = Len(lp_strSQLString)
End If

l_intLenToKeep = l_intEndPos - l_intStartPos + 1

l_strSQL = Mid(lp_strSQLString, l_intStartPos, l_intLenToKeep)


RemoveSelectFields = "SELECT * " & l_strSQL

End Function

Sub PrintJobCostingPage(lp_lngJobID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/printjobcostingpage") Then
        Exit Sub
    End If
    
    If Val(lp_lngJobID) > 0 Then
        
        Dim l_strReportToPrint As String
        Dim l_strSelectionFormula As String
        
       
        l_strSelectionFormula = "{job.jobID} = " & lp_lngJobID
        

        If UCase(GetData("job", "jobtype", "jobID", lp_lngJobID)) = "CREDIT" Then
            l_strReportToPrint = g_strLocationOfCrystalReportFiles & "draftcredit.rpt"

        Else
            l_strReportToPrint = g_strLocationOfCrystalReportFiles & "jobcosting.rpt"

        End If
        
        
        PrintCrystalReport l_strReportToPrint, l_strSelectionFormula, g_blnPreviewReport
        
        
        'add a history line to the jobn
        AddJobHistory lp_lngJobID, "Printed Job Costing"
        
    Else
        NoJobSelectedMessage
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub PrintGroupCostingSheet(lp_lngCostingSheetID As Long, Optional lp_strReportType As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/printgroupcostingsheet") Then
        Exit Sub
    End If
    
    If Val(lp_lngCostingSheetID) > 0 Then
        
        Dim l_strReportToPrint As String
        Dim l_strSelectionFormula As String
        
        If lp_strReportType = "" Then
            l_strReportToPrint = g_strLocationOfCrystalReportFiles & "\groupcosting01.rpt"
        Else
            Dim l_strReportFileName As String
            l_strReportFileName = GetData("xref", "information", "description", lp_strReportType)
            l_strReportToPrint = g_strLocationOfCrystalReportFiles & "\" & l_strReportFileName
        End If
        
        l_strSelectionFormula = "{costingsheet.costingsheetID} = " & lp_lngCostingSheetID
        
        Dim l_strSQL As String
        
        PrintCrystalReport l_strReportToPrint, l_strSelectionFormula, g_blnPreviewReport
        
    Else
        MsgBox "Invalid costing sheet selected.", vbExclamation
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub PromptForCompletionDetails(lp_lngJobID As Long)
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    MsgBox "This feature has not been implemented yet", vbInformation
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub


Sub SendEmailFromJobID(ByVal lp_lngJobID As Long)

Dim l_strEmailAddress As String
Dim l_lngContactID As Long
Dim l_strTitle As String
Dim l_strOrderNumber As String
Dim l_strCommand As String

l_lngContactID = GetData("job", "contactID", "jobID", lp_lngJobID)
l_strEmailAddress = GetData("contact", "email", "contactID", l_lngContactID)
l_strTitle = GetData("job", "title1", "jobid", lp_lngJobID)
l_strOrderNumber = GetData("job", "orderreference", "jobID", lp_lngJobID)

If l_strEmailAddress <> "" Then
    l_strCommand = "mailto:" & l_strEmailAddress & "?subject=MX1 Job Completed " & lp_lngJobID & "&body=MX1 Job Number: " & lp_lngJobID & ", "
    If l_strOrderNumber <> "" Then l_strCommand = l_strCommand & "Order Number: " & l_strOrderNumber & ", "
    l_strCommand = l_strCommand & "Title: " & l_strTitle & " has been completed, and will be despatched as previously arranged. " & "Regards, " & g_strFullUserName
    OpenBrowser l_strCommand
End If

End Sub

Function QuoteSanitise(ByVal lp_strText As Variant) As String
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strNewText As String
    Dim l_strOldText As String
    Dim l_intLoop As Long
    l_strNewText = ""
    
    If IsNull(lp_strText) Then
        l_strOldText = ""
        l_strNewText = l_strOldText
        lp_strText = ""
    Else
        l_strOldText = lp_strText
    End If
    
    If InStr(lp_strText, "'") <> 0 Or InStr(lp_strText, """") <> 0 Then
        
        For l_intLoop = 1 To Len(l_strOldText)
            
            Select Case Mid(l_strOldText, l_intLoop, 1)
                
                Case "'"
                    l_strNewText = l_strNewText & "''"
                Case Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
            
            End Select
            
        Next
    Else
        l_strNewText = lp_strText
    End If
    
    If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
        l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
    End If
    
    QuoteSanitise = l_strNewText
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function DoubleQuoteSanitise(ByVal lp_strText As Variant) As String
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strNewText As String
    Dim l_strOldText As String
    Dim l_intLoop As Long
    l_strNewText = ""
    
    If IsNull(lp_strText) Then
        l_strOldText = ""
        l_strNewText = l_strOldText
        lp_strText = ""
    Else
        l_strOldText = lp_strText
    End If
    
    If InStr(lp_strText, """") <> 0 Then
        
        For l_intLoop = 1 To Len(l_strOldText)
            
            Select Case Mid(l_strOldText, l_intLoop, 1)
                
                Case """"
                    l_strNewText = l_strNewText & """" & """"
                Case Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
            
            End Select
            
        Next
    Else
        l_strNewText = lp_strText
    End If
    
    If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
        l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
    End If
    
    DoubleQuoteSanitise = l_strNewText
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    
End Function
Function XMLSanitise(ByVal lp_strText As Variant) As String
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strNewText As String
    Dim l_strOldText As String
    Dim l_intLoop As Integer
    l_strNewText = ""
    
    If IsNull(lp_strText) Then
        l_strOldText = ""
        l_strNewText = l_strOldText
        lp_strText = ""
    Else
        l_strOldText = lp_strText
    End If
    
    If InStr(lp_strText, "<") <> 0 Or InStr(lp_strText, ">") <> 0 Or InStr(lp_strText, "&") <> 0 Then
        
        For l_intLoop = 1 To Len(l_strOldText)
            
            Select Case Mid(l_strOldText, l_intLoop, 1)
            
                Case ">"
                    l_strNewText = l_strNewText & "&gt;"
                Case "<"
                    l_strNewText = l_strNewText & "&lt;"
                Case "&"
                    l_strNewText = l_strNewText & "&amp;"
                Case Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
            
            End Select
        
        Next
    Else
        l_strNewText = lp_strText
    End If
    
    If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
        l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
    End If
    
    XMLSanitise = l_strNewText
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function RecordExists(lp_strTable As String, lp_strFieldToSearch As String, lp_varCriteria As Variant) As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim a As New clsDataFunctions
    
    RecordExists = a.RecordExists(lp_strTable, lp_strFieldToSearch, lp_varCriteria)
    
    Set a = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Sub RemoveContactFromProjectTeam(lp_lngTeamID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_lngContactID As Long
    Dim l_strRole As String
    
    l_lngContactID = GetData("team", "contactID", "teamID", lp_lngTeamID)
    
    If l_lngContactID = 0 Then Exit Sub
    
    l_strRole = GetData("team", "role", "teamID", lp_lngTeamID)
    
    
    Dim l_strContactName As String
    l_strContactName = GetData("contact", "name", "contactID", l_lngContactID)
    
    
    Dim l_intQuestion As Integer
    l_intQuestion = MsgBox("Do you really want to remove " & l_strContactName & " (" & l_strRole & ") from this project team?", vbQuestion + vbOKCancel)
    
    If l_intQuestion <> vbCancel Then
        
        'add the record to the team table
        Dim l_strSQL As String
        
        l_strSQL = "DELETE FROM team "
        l_strSQL = l_strSQL & "WHERE teamID = "
        l_strSQL = l_strSQL & "'" & lp_lngTeamID & "';"
        
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
    End If
    
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub RemoveEmployee(lp_lngEmployeeID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/removeemployeerecord") Then
        Exit Sub
    End If
    
    Dim l_strSQL As String
    
    l_strSQL = "DELETE FROM employee WHERE employeeID = " & lp_lngEmployeeID
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Public Sub RemoveResourceFromJob(lp_lngResourceScheduleID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/removeresourcefromjob") Then
        Exit Sub
    End If
    
    'check if this job can have a resource added to it.
    
    Dim l_intStatusNumber As Integer, l_lngJobID As Long
    l_lngJobID = GetData("resourceschedule", "jobID", "resourcescheduleID", lp_lngResourceScheduleID)
    l_intStatusNumber = GetStatusNumber(GetData("job", "fd_status", "jobID", l_lngJobID))
    
    If l_intStatusNumber < 3 Then
        
        Dim l_intQuestion As Integer
        Dim l_strResourceName As String
        
        Dim l_strResourceType As String
        l_strResourceType = GetData("resourceschedule", "resourcetype", "resourcescheduleID", lp_lngResourceScheduleID)
        
        If l_strResourceType = "FREELANCE" Then
            l_strResourceName = GetData("contact", "name", "contactID", GetData("resourceschedule", "contactID", "resourcescheduleID", lp_lngResourceScheduleID))
        Else
            l_strResourceName = GetData("resource", "name", "resourceID", GetData("resourceschedule", "resourceID", "resourcescheduleID", lp_lngResourceScheduleID))
        End If
        l_intQuestion = MsgBox("Do you really want to remove this item (" & l_strResourceName & ") from this job?", vbQuestion + vbOKCancel)
        
        If l_intQuestion = vbOK Then
            Dim l_strSQL As String
            l_strSQL = "DELETE FROM resourceschedule WHERE resourcescheduleID = '" & lp_lngResourceScheduleID & "'"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            
        End If
        
        AddJobHistory l_lngJobID, "Removed Resource (" & l_strResourceName & ")"
    Else
        MsgBox "You can not remove a resource from this job, as it has already passed the completed phase.", vbExclamation
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub ReportToPDF(lp_strRPTFileName As String, lp_strSelectionFormula As String, lp_strExportFileName As String)
    
    'need to create a PDF of the review - dont forget to insert the barcode later
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim CrystalApp As New CRAXDRT.Application
    Dim rptReport As New CRAXDRT.Report
    
    Set rptReport = CrystalApp.OpenReport(lp_strRPTFileName)
    
    rptReport.ExportOptions.DestinationType = crEDTDiskFile
    
    rptReport.ExportOptions.FormatType = crEFTPortableDocFormat
    rptReport.RecordSelectionFormula = lp_strSelectionFormula
    
    If lp_strExportFileName = "" Then
        rptReport.ExportOptions.PromptForExportOptions
    Else
        rptReport.ExportOptions.DiskFileName = lp_strExportFileName
    End If
    
    rptReport.Export False
    
    Set rptReport = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    
    If InStr(lp_strRPTFileName, "quote") = 0 Then
        MsgBox Err.Description
        'TVCodeTools ErrorHandlerEnd
    End If
    
    Resume PROC_EXIT
End Sub

Sub ReportToXLS(lp_strRPTFileName As String, lp_strSelectionFormula As String, lp_strExportFileName As String)
    'need to create a XLS of the Despatch
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim CrystalApp As New CRAXDRT.Application
    Dim rptReport As New CRAXDRT.Report
    
    Set rptReport = CrystalApp.OpenReport(lp_strRPTFileName)
    
    rptReport.ExportOptions.DestinationType = crEDTDiskFile
    
    rptReport.ExportOptions.FormatType = crEFTExcelDataOnly
    rptReport.RecordSelectionFormula = lp_strSelectionFormula
    
    If lp_strExportFileName = "" Then
        rptReport.ExportOptions.PromptForExportOptions
    Else
        rptReport.ExportOptions.DiskFileName = lp_strExportFileName
        rptReport.ExportOptions.ExcelChopPageHeader = True
        rptReport.ExportOptions.ExcelMaintainColumnAlignment = True
        rptReport.ExportOptions.ExcelUseConstantColumnWidth = False
        rptReport.ExportOptions.ExcelUseFormatInDataOnly = False
    End If
    
    rptReport.Export False
    
    Set rptReport = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    
    If InStr(lp_strRPTFileName, "quote") = 0 Then
        MsgBox Err.Description
        'TVCodeTools ErrorHandlerEnd
    End If
    
    Resume PROC_EXIT

End Sub

Sub ResetSentToAccountsOnJob(lp_lngJobID As Long, lp_strType As String)
    
    'clears the fields which flag the job as being ready to send to accounts
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Select Case lp_strType
        Case "INVOICE"
            SetData "job", "senttoaccountsdate", "jobID", lp_lngJobID, Null
            SetData "job", "senttoaccountsuser", "jobID", lp_lngJobID, Null
        Case "CREDIT"
            Dim l_lngCreditNoteNumber As Long
            l_lngCreditNoteNumber = GetData("job", "creditnotenumber", "jobID", lp_lngJobID)
            SetData "invoiceheader", "senttoaccountsdate", "creditnotenumber", l_lngCreditNoteNumber, Null
        Case Else
            SetData "job", "senttoBBCdate", "jobID", lp_lngJobID, Null
    End Select
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Function ResourceTemporarilyBooked(lp_lngResourceID As Long) As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_intResourceBooked As Integer
    l_strSQL = "SELECT COUNT(resourceID) FROM temporaryresourceschedule WHERE resourceID = '" & lp_lngResourceID & "' AND createduser = '" & g_strUserInitials & "'"
    l_intResourceBooked = GetCount(l_strSQL)
    
    If l_intResourceBooked > 0 Then
        ResourceTemporarilyBooked = True
    Else
        ResourceTemporarilyBooked = False
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function SaveCompany()
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/savecompany") Then
        Exit Function
    End If
    
    frmCompany.MousePointer = vbHourglass
    
    Dim l_strSQL As String, l_blnNewFlag As Boolean
    
    With frmCompany
        
        If Val(.txtCompanyID.Text) = 0 Then
            l_strSQL = "INSERT INTO company (createddate, createduser, accountcode, marketcode) VALUES ("
            
            'values to insert
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
            l_strSQL = l_strSQL & "'" & .txtAccountCode.Text & "', "
            l_strSQL = l_strSQL & "'" & .txtMarketCode.Text & "')"
            
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            
            .txtCompanyID.Text = g_lngLastID
            .chkInactiveCompany.Value = 1
            
            l_blnNewFlag = True
        Else
            l_blnNewFlag = False
        End If
        
        l_strSQL = "UPDATE company SET "
        
        l_strSQL = l_strSQL & "muser = '" & g_strUserInitials & "', "
        l_strSQL = l_strSQL & "mdate = '" & FormatSQLDate(Now) & "', "
        
        If .txtBookingsAlert.Text <> "" Then
            l_strSQL = l_strSQL & "messagebookings = '" & QuoteSanitise(.txtBookingsAlert.Text) & "', "
        Else
            l_strSQL = l_strSQL & "messagebookings = NULL, "
        End If
        
        If .txtCostingsAlert.Text <> "" Then
            l_strSQL = l_strSQL & "messagecostings = '" & QuoteSanitise(.txtCostingsAlert.Text) & "', "
        Else
            l_strSQL = l_strSQL & "messagecostings = NULL, "
        End If
        
        l_strSQL = l_strSQL & "masterdiscount = '" & .txtDiscount.Text & "', "
        l_strSQL = l_strSQL & "creditlimit = '" & .txtCreditLimit.Text & "', "
        l_strSQL = l_strSQL & "name = '" & QuoteSanitise(.cmbCompany.Text) & "', "
        l_strSQL = l_strSQL & "address = '" & QuoteSanitise(.txtAddress.Text) & "', "
        l_strSQL = l_strSQL & "postcode = '" & .txtPostCode.Text & "', "
        l_strSQL = l_strSQL & "country = '" & .txtCountry.Text & "', "
        l_strSQL = l_strSQL & "telephone = '" & .txtTelephone.Text & "', "
        l_strSQL = l_strSQL & "fax = '" & .txtFax.Text & "', "
        l_strSQL = l_strSQL & "email = '" & .txtEmail.Text & "', "
        l_strSQL = l_strSQL & "website = '" & .txtWebsite.Text & "', "
        l_strSQL = l_strSQL & "marketcode = '" & .txtMarketCode.Text & "', "
        l_strSQL = l_strSQL & "tradecode = '" & .txtTradeCode.Text & "', "
        l_strSQL = l_strSQL & "notes1 = '" & QuoteSanitise(.txtComments.Text) & "', "
        l_strSQL = l_strSQL & "vatcode = '" & .txtVATCode.Text & "', "
        l_strSQL = l_strSQL & "vatnumber = '" & .txtVATNumber.Text & "', "
        l_strSQL = l_strSQL & "cetaclientcode = '" & .txtClientCode.Text & "', "
        l_strSQL = l_strSQL & "accountcode = '" & .txtAccountCode.Text & "', "
        l_strSQL = l_strSQL & "salesledgercode = '" & .txtSalesLedgerCode.Text & "', "
        l_strSQL = l_strSQL & "SageCustomerReference = '" & .txtSageCustomerReference.Text & "', "
        l_strSQL = l_strSQL & "BusinessUnit = '" & .cmbBusinessUnit.Text & "', "
        l_strSQL = l_strSQL & "jcacontactemail = '" & .txtJCAContactEmail.Text & "', "
        l_strSQL = l_strSQL & "progressrefname = '" & .txtProgressRefName.Text & "', "
        l_strSQL = l_strSQL & "generictrackertitle = '" & .txtTrackerTitle.Text & "', "
        l_strSQL = l_strSQL & "portalpasswordqualitystring = '" & .txtportalpasswordqualitystring.Text & "', "
        l_strSQL = l_strSQL & "portalpasswordqualityexplanation = '" & .txtportalpasswordqualityexplanation.Text & "', "
        l_strSQL = l_strSQL & "portalpasswordlifetime = " & Val(.txtPasswordLifetime.Text) & ", "
        l_strSQL = l_strSQL & "portaldownloadcode = '" & .txtDownloadCode.Text & "', "
        l_strSQL = l_strSQL & "portaluploadcode = '" & .txtUploadCode.Text & "', "
        l_strSQL = l_strSQL & "DTBillingCompanyID = " & Val(.txtDTBillingCompanyID.Text) & ", "
        l_strSQL = l_strSQL & "AS11BillingCompanyID = " & Val(.txtAS11BillingCompanyID.Text) & ", "
        
        If Not IsNull(.datAccountStart.Value) Then
            l_strSQL = l_strSQL & "accountstartdate = '" & FormatSQLDate(.datAccountStart.Value) & "', "
        Else
            l_strSQL = l_strSQL & "accountstartdate = NULL, "
        End If
        
        l_strSQL = l_strSQL & "accountstatus = '" & .cmbAccountStatus.Text & "', "
        
        If Not IsNull(.datPostedToAccounts.Value) Then
            l_strSQL = l_strSQL & "postedtoaccountsdate = '" & FormatSQLDate(.datPostedToAccounts.Value) & "', "
        Else
            l_strSQL = l_strSQL & "postedtoaccountsdate = NULL, "
        End If
        
        If Val(.cmbRateCard.Text) = 0 And .chkSupplier.Value = 0 Then .cmbRateCard.Text = g_intBaseRateCard
        l_strSQL = l_strSQL & "ratecard = '" & Val(.cmbRateCard.Text) & "', "
        l_strSQL = l_strSQL & "registeredoffice = '" & .txtRegisteredOffice.Text & "', "
        l_strSQL = l_strSQL & "source = '" & .txtSource.Text & "', "
        
        l_strSQL = l_strSQL & "invoicecompanyname = '" & QuoteSanitise(.txtInvoiceCompany.Text) & "', "
        l_strSQL = l_strSQL & "invoiceaddress = '" & QuoteSanitise(.txtInvoiceAddress.Text) & "', "
        l_strSQL = l_strSQL & "invoicepostcode = '" & .txtInvoicePostCode.Text & "', "
        l_strSQL = l_strSQL & "invoicecountry = '" & .txtInvoiceCountry.Text & "', "
        l_strSQL = l_strSQL & "invoiceemail = '" & .txtInvoiceEmail.Text & "', "
        
        l_strSQL = l_strSQL & "isTVDistributionTeam = '" & .chkTVDistribution.Value & "', "
        l_strSQL = l_strSQL & "isStudiosTeam = '" & .chkTVStudios.Value & "', "
        l_strSQL = l_strSQL & "isIndependentsTeam = '" & .chkTVIndependents.Value & "', "
        
        l_strSQL = l_strSQL & "LatimerLock = '" & .chkLatimerLock.Value & "', "
        
        If .txtMediaPulseCompanyID.Text <> "" Then
            l_strSQL = l_strSQL & "MediaPulseCompanyID = " & Val(.txtMediaPulseCompanyID.Text) & ", "
        Else
            l_strSQL = l_strSQL & "MediaPulseCompanyID = Null, "
        End If
        If .txtMediaPulseJobID.Text <> "" Then
            l_strSQL = l_strSQL & "MediaPulseJobID = " & Val(.txtMediaPulseJobID.Text) & ", "
        Else
            l_strSQL = l_strSQL & "MediaPulseJobID = Null, "
        End If
        
        l_strSQL = l_strSQL & "iscustomer = '" & .chkClient.Value & "', "
        l_strSQL = l_strSQL & "issupplier = '" & .chkSupplier.Value & "', "
        l_strSQL = l_strSQL & "isprospective = '" & .chkProspective.Value & "', "
        l_strSQL = l_strSQL & "isCPGlobalCustomer = '" & .chkCPGlobal.Value & "', "
        l_strSQL = l_strSQL & "system_active = '" & .chkInactiveCompany.Value & "', "
        
        'notes
        l_strSQL = l_strSQL & "officeclientnotes = '" & QuoteSanitise(.txtOfficeClientNotes.Text) & "', "
        l_strSQL = l_strSQL & "despatchnotes = '" & QuoteSanitise(.txtDespatchNotes.Text) & "', "
        l_strSQL = l_strSQL & "operatornotes = '" & QuoteSanitise(.txtOperatorNotes.Text) & "', "
        l_strSQL = l_strSQL & "producernotes = '" & QuoteSanitise(.txtProducerNotes.Text) & "', "
        l_strSQL = l_strSQL & "accountsnotes = '" & QuoteSanitise(.txtAccountsNotes.Text) & "', "
        
        'custom field labels and other portal web stuff
        l_strSQL = l_strSQL & "customfield1label = '" & QuoteSanitise(.txtlabel1.Text) & "', "
        l_strSQL = l_strSQL & "customfield2label = '" & QuoteSanitise(.txtlabel2.Text) & "', "
        l_strSQL = l_strSQL & "customfield3label = '" & QuoteSanitise(.txtlabel3.Text) & "', "
        l_strSQL = l_strSQL & "customfield4label = '" & QuoteSanitise(.txtlabel4.Text) & "', "
        l_strSQL = l_strSQL & "customfield5label = '" & QuoteSanitise(.txtlabel5.Text) & "', "
        l_strSQL = l_strSQL & "customfield6label = '" & QuoteSanitise(.txtlabel6.Text) & "', "
        l_strSQL = l_strSQL & "portalemailsubject = '" & QuoteSanitise(.txtEmailSubject.Text) & "', "
        l_strSQL = l_strSQL & "portalemailbody = '" & QuoteSanitise(.txtEmailBody.Text) & "', "
        l_strSQL = l_strSQL & "portalwelcomemessage = '" & QuoteSanitise(.txtWelcomeMessage.Text) & "', "
        l_strSQL = l_strSQL & "portalordernumberfieldname = '" & QuoteSanitise(.txtOrderNumberFieldName.Text) & "', "
        l_strSQL = l_strSQL & "portalcompanyname = '" & QuoteSanitise(.txtPortalCompanyName.Text) & "', "
        
        If .txtHeaderPage.Text <> "" Then
            l_strSQL = l_strSQL & "portalheaderpage = '" & .txtHeaderPage.Text & "', "
            SetData "portaluser", "portalheaderpage", "companyID", .txtCompanyID.Text, .txtHeaderPage.Text
        Else
            l_strSQL = l_strSQL & "portalheaderpage = '" & g_strDefaultHeaderPage & "', "
            SetData "portaluser", "portalheaderpage", "companyID", .txtCompanyID.Text, g_strDefaultHeaderPage
        End If
        
        If .txtFooterPage.Text <> "" Then
            l_strSQL = l_strSQL & "portalfooterpage = '" & .txtFooterPage.Text & "', "
            SetData "portaluser", "portalfooterpage", "companyID", .txtCompanyID.Text, .txtFooterPage.Text
        Else
            l_strSQL = l_strSQL & "portalfooterpage = '" & g_strDefaultFooterPage & "', "
            SetData "portaluser", "portalfooterpage", "companyID", .txtCompanyID.Text, g_strDefaultFooterPage
        End If
        
        l_strSQL = l_strSQL & "portaladminlink1 = '" & .txtAdminLink(1).Text & "', "
        l_strSQL = l_strSQL & "portaladminlink2 = '" & .txtAdminLink(2).Text & "', "
        l_strSQL = l_strSQL & "portaladminlink3 = '" & .txtAdminLink(3).Text & "', "
        l_strSQL = l_strSQL & "portaladmintitle1 = '" & .txtAdminTitle(1).Text & "', "
        l_strSQL = l_strSQL & "portaladmintitle2 = '" & .txtAdminTitle(2).Text & "', "
        l_strSQL = l_strSQL & "portaladmintitle3 = '" & .txtAdminTitle(3).Text & "', "
        l_strSQL = l_strSQL & "portaladmintext1 = '" & .txtAdminText(1).Text & "', "
        l_strSQL = l_strSQL & "portaladmintext2 = '" & .txtAdminText(2).Text & "', "
        l_strSQL = l_strSQL & "portaladmintext3 = '" & .txtAdminText(3).Text & "', "
        
        l_strSQL = l_strSQL & "MX1360CustomerID = '" & .txtMX1360CustomerID.Text & "', "
        l_strSQL = l_strSQL & "MX1360Username = '" & .txtMX1360Username.Text & "', "
        l_strSQL = l_strSQL & "MX1360password = '" & .txtMX1360password.Text & "', "
        l_strSQL = l_strSQL & "MX1360ProxyTranscodeID = '" & .txtMX1360ProxyTranscodeID.Text & "', "
        
        If .txtPortalURL.Text <> "" Then
            l_strSQL = l_strSQL & "portalwebsiteurl = '" & .txtPortalURL.Text & "', "
        Else
            l_strSQL = l_strSQL & "portalwebsiteurl = '" & g_strDefaultPortalAddress & "', "
        End If
        
        If .txtMainPage.Text <> "" Then
            l_strSQL = l_strSQL & "portalmainpage = '" & .txtMainPage.Text & "' "
            If .chkUpdate.Value <> 0 Then
                SetData "portaluser", "portalmainpage", "companyID", .txtCompanyID.Text, .txtMainPage.Text
                .chkUpdate.Value = 0
            End If
        Else
            l_strSQL = l_strSQL & "portalmainpage = '" & g_strDefaultMainPage & "' "
            If .chkUpdate.Value <> 0 Then
                SetData "portaluser", "portalmainpage", "companyID", .txtCompanyID.Text, g_strDefaultMainPage
                .chkUpdate.Value = 0
            End If
        End If
        
        
        l_strSQL = l_strSQL & " WHERE companyID = " & .txtCompanyID.Text
        
        
        l_strSQL = l_strSQL & ";"
        
    End With
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    
    CheckForSQLError
    
    If Val(frmCompany.txtCompanyID.Text) = 0 Then
        frmCompany.adoCompany.Refresh
        frmCompany.txtCompanyID.Text = g_lngLastID
        frmCompany.txtCreatedDate.Text = Date
        
    End If
    
    If l_blnNewFlag = True Then
        SendSMTPMail g_strQuotesEmailAddress, g_strQuotesEmailName, "New Company Saved", "", "Company Name: " & frmCompany.cmbCompany.Text & vbCrLf & "Company Number: " & frmCompany.txtCompanyID.Text & vbCrLf & "Sales Ledger Code: " & frmCompany.txtSalesLedgerCode.Text & vbCrLf, True, "", ""
    End If
    ShowCompany Val(frmCompany.txtCompanyID.Text)

    frmCompany.MousePointer = vbDefault
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function SaveContact() As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/savecontact") Then
        SaveContact = False
        Exit Function
    End If
    
    frmContact.MousePointer = vbHourglass
    
    Dim l_strSQL As String, l_blnNewFlag As Boolean
    
    With frmContact
        
        If Val(.txtContactID.Text) = 0 Then
            l_strSQL = "INSERT INTO contact ("
            'fields to insert
            l_strSQL = l_strSQL & "cdate, "
            l_strSQL = l_strSQL & "cuser, "
            l_strSQL = l_strSQL & "system_active, "
            l_strSQL = l_strSQL & "name, "
            l_strSQL = l_strSQL & "firstname, "
            l_strSQL = l_strSQL & "lastname, "
            l_strSQL = l_strSQL & "initials, "
            l_strSQL = l_strSQL & "title, "
            l_strSQL = l_strSQL & "address, "
            l_strSQL = l_strSQL & "postcode, "
            l_strSQL = l_strSQL & "country, "
            l_strSQL = l_strSQL & "telephone, "
            l_strSQL = l_strSQL & "fax, "
            l_strSQL = l_strSQL & "email, "
            l_strSQL = l_strSQL & "mobile, "
            l_strSQL = l_strSQL & "notes1) VALUES ("
            
            'values to insert
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
            l_strSQL = l_strSQL & "1, "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.cmbContact.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.txtFirstName.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.txtLastName.Text) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.txtInitials.Text) & "', "
            l_strSQL = l_strSQL & "'" & .cmbTitle.Text & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(.txtAddress.Text) & "', "
            l_strSQL = l_strSQL & "'" & .txtPostCode.Text & "', "
            l_strSQL = l_strSQL & "'" & .txtCountry.Text & "', "
            l_strSQL = l_strSQL & "'" & .txtTelephone.Text & "', "
            l_strSQL = l_strSQL & "'" & .txtFax.Text & "', "
            l_strSQL = l_strSQL & "'" & .txtEmail.Text & "', "
            l_strSQL = l_strSQL & "'" & .txtMobile.Text & "', "
            l_strSQL = l_strSQL & "'" & .txtComments.Text & "')"
            
            l_blnNewFlag = True
            
        Else
            l_strSQL = "UPDATE contact SET "
            l_strSQL = l_strSQL & "mdate = " & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "muser = " & "'" & g_strUserInitials & "', "
            l_strSQL = l_strSQL & "name = '" & QuoteSanitise(.cmbContact.Text) & "', "
            l_strSQL = l_strSQL & "firstname = '" & QuoteSanitise(.txtFirstName.Text) & "', "
            l_strSQL = l_strSQL & "lastname = '" & QuoteSanitise(.txtLastName.Text) & "', "
            l_strSQL = l_strSQL & "initials = '" & QuoteSanitise(.txtInitials.Text) & "', "
            l_strSQL = l_strSQL & "title = '" & QuoteSanitise(.cmbTitle.Text) & "', "
            l_strSQL = l_strSQL & "address = '" & QuoteSanitise(.txtAddress.Text) & "', "
            l_strSQL = l_strSQL & "postcode = '" & .txtPostCode.Text & "', "
            l_strSQL = l_strSQL & "country = '" & .txtCountry.Text & "', "
            l_strSQL = l_strSQL & "telephone = '" & .txtTelephone.Text & "', "
            l_strSQL = l_strSQL & "fax = '" & .txtFax.Text & "', "
            l_strSQL = l_strSQL & "email = '" & .txtEmail.Text & "', "
            l_strSQL = l_strSQL & "mobile = '" & .txtMobile.Text & "', "
            l_strSQL = l_strSQL & "notes1 = '" & .txtComments.Text & "', "
            l_strSQL = l_strSQL & "system_active = '" & .chkInactive.Value & "'"
            l_strSQL = l_strSQL & " WHERE contactID = " & .txtContactID.Text
            l_blnNewFlag = False
        End If
        
        l_strSQL = l_strSQL & ";"
        
    End With
    
    ExecuteSQL l_strSQL, g_strExecuteError
    
    CheckForSQLError
    
    If Val(frmContact.txtContactID.Text) = 0 Then
        If frmContact.adoContact.RecordSource <> "" Then
            frmContact.adoContact.Refresh
        End If
        frmContact.txtContactID.Text = g_lngLastID
        frmContact.chkInactive.Value = 1
    End If
    
    If l_blnNewFlag = True Then
        SendSMTPMail g_strQuotesEmailAddress, g_strQuotesEmailName, "New Contact Saved", "", "Name: " & frmContact.cmbContact.Text & vbCrLf & "Contact Number: " & frmContact.txtContactID.Text, True, "", ""
    End If
    
    frmContact.MousePointer = vbDefault
    
    'TVCodeTools ErrorHandlerStart
    
    SaveContact = True
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    SaveContact = False
    Exit Function
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub SaveEmployeeRecord(lp_lngCompanyID As Long, lp_lngContactID As Long, lp_strJobTitle As String, lp_strUserName As String, lp_strPassword As String, lp_strCETACode As String, lp_strUserSettings As String, portaladminlink1 As String, portaladminlink2 As String, portaladminlink3 As String, portaladmintitle1 As String, portaladmintitle2 As String, portaladmintitle3 As String, portaladmintext1 As String, portaladmintext2 As String, portaladmintext3 As String, FailedLogins As Long, SecurityLock As Long, PasswordLastChanged As Variant, ForcePort33001 As Variant)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/saveemployeerecord") Then
        Exit Sub
    End If
    
    If lp_lngCompanyID = 0 Then
        Exit Sub
    End If
    
    Dim l_strSQL As String, l_rstCheckUserName As ADODB.Recordset
    
    l_strSQL = "DELETE FROM employee WHERE companyID = '" & lp_lngCompanyID & "' AND contactID = '" & lp_lngContactID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    If lp_strUserName <> "" Then
        l_strSQL = "SELECT username FROM employee WHERE username = '" & lp_strUserName & "';"
        Set l_rstCheckUserName = ExecuteSQL(l_strSQL, g_strExecuteError)
        CheckForSQLError
        
        If l_rstCheckUserName.RecordCount > 0 Then
            MsgBox "Cannot save Employee record - Username already exists for another Contact and Company", vbCritical
            l_rstCheckUserName.Close
            Set l_rstCheckUserName = Nothing
            GoTo PROC_EXIT
        End If
        l_rstCheckUserName.Close
        Set l_rstCheckUserName = Nothing
    End If
       
    l_strSQL = "INSERT INTO employee (companyID, contactID, jobtitle, username, password, usersettings, portaladminlink1, portaladminlink2, portaladminlink3, portaladmintitle1, portaladmintitle2, portaladmintitle3, portaladmintext1, portaladmintext2, portaladmintext3, cetacode, securitylock, failedlogins, forceport33001, passwordlastreset) VALUES ('" & lp_lngCompanyID & "', '" & lp_lngContactID & "', '" & lp_strJobTitle & "', "
    If lp_strUserName <> "" Then
        l_strSQL = l_strSQL & "'" & lp_strUserName & "',"
    Else
        l_strSQL = l_strSQL & "NULL, "
    End If
    If lp_strPassword <> "" Then
        l_strSQL = l_strSQL & "'" & lp_strPassword & "',"
    Else
        l_strSQL = l_strSQL & "NULL,"
    End If
    If IsNumeric(lp_strUserSettings) Then
        l_strSQL = l_strSQL & "'" & Val(lp_strUserSettings) & "',"
    Else
        l_strSQL = l_strSQL & "NULL,"
    End If
    If portaladminlink1 <> "" Then
        l_strSQL = l_strSQL & "'" & portaladminlink1 & "',"
    Else
        l_strSQL = l_strSQL & "NULL,"
    End If
    If portaladminlink2 <> "" Then
        l_strSQL = l_strSQL & "'" & portaladminlink2 & "',"
    Else
        l_strSQL = l_strSQL & "NULL,"
    End If
    If portaladminlink3 <> "" Then
        l_strSQL = l_strSQL & "'" & portaladminlink3 & "',"
    Else
        l_strSQL = l_strSQL & "NULL,"
    End If
    If portaladmintitle1 <> "" Then
        l_strSQL = l_strSQL & "'" & portaladmintitle1 & "',"
    Else
        l_strSQL = l_strSQL & "NULL,"
    End If
    If portaladmintitle2 <> "" Then
        l_strSQL = l_strSQL & "'" & portaladmintitle2 & "',"
    Else
        l_strSQL = l_strSQL & "NULL,"
    End If
    If portaladmintitle3 <> "" Then
        l_strSQL = l_strSQL & "'" & portaladmintitle3 & "',"
    Else
        l_strSQL = l_strSQL & "NULL,"
    End If
    If portaladmintext1 <> "" Then
        l_strSQL = l_strSQL & "'" & portaladmintext1 & "',"
    Else
        l_strSQL = l_strSQL & "NULL,"
    End If
    If portaladmintext2 <> "" Then
        l_strSQL = l_strSQL & "'" & portaladmintext2 & "',"
    Else
        l_strSQL = l_strSQL & "NULL,"
    End If
    If portaladmintext3 <> "" Then
        l_strSQL = l_strSQL & "'" & portaladmintext3 & "',"
    Else
        l_strSQL = l_strSQL & "NULL,"
    End If
    If lp_strCETACode <> "" Then
        l_strSQL = l_strSQL & "'" & lp_strCETACode & "', "
    Else
        l_strSQL = l_strSQL & "NULL, "
    End If
    l_strSQL = l_strSQL & Val(SecurityLock) & ", "
    l_strSQL = l_strSQL & Val(FailedLogins) & ", "
    If ForcePort33001 <> 0 Then
        l_strSQL = l_strSQL & "1, "
    Else
        l_strSQL = l_strSQL & "0, "
    End If
    If IsDate(PasswordLastChanged) Then
        l_strSQL = l_strSQL & "'" & FormatSQLDate(PasswordLastChanged) & "');"
    Else
        l_strSQL = l_strSQL & "0);"
    End If
    
    ExecuteSQL l_strSQL, g_strExecuteError
    
    CheckForSQLError
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Function SaveJob()
    
    Dim l_lngJobID As Long
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/savejob") Then
        Exit Function
    End If
    
    If Val(frmJob.lblCompanyID.Caption) = 0 Then
        NoCompanySelectedMessage
        Exit Function
    End If
    
    If Val(frmJob.lblContactID.Caption) = 0 Then
        NoContactSelectedMessage
        Exit Function
    End If
    
    Dim l_strSQL As String
    
    Dim l_strAccountStatus As String
    
    l_strAccountStatus = GetData("company", "accountstatus", "companyID", frmJob.lblCompanyID.Caption)
    
    If g_optStopSavingJobsWhenCompanyOnHold = 1 Then
        If l_strAccountStatus = "HOLD" Then
            MsgBox "Jobs cannot be created or saved for companies on HOLD status.", vbCritical, "Job Not Saved"
            Exit Function
        End If
    End If
    
    
    frmJob.MousePointer = vbHourglass
    
    With frmJob
        
        Dim l_datDeadline As Variant
        If Not IsNull(.datDeadlineDate.Value) Then
            l_datDeadline = Format(.datDeadlineDate.Value, vbShortDateFormat) & " " & .cmbDeadlineTime.Text
        Else
            l_datDeadline = Null
        End If
        
        If Val(.txtJobID.Text) = 0 Then
            
            If GetData("company", "LatimerLock", "companyID", .lblCompanyID.Caption) <> 0 Then
                MsgBox "New Jobs cannot be created or saved for companies that have been Migrated to Latimer Road.", vbCritical, "Job Not Saved"
            Exit Function
            
            End If
    
            'check that the project number is valid
            If Val(frmJob.lblProjectID.Caption) <> 0 Then
                
                Dim l_strProjectStatus As String
                l_strProjectStatus = GetData("project", "fd_status", "projectID", frmJob.lblProjectID.Caption)
                
                Select Case UCase(l_strProjectStatus)
                Case "INVOICED", "SENT TO ACCOUNTS", "INVOICE", "CANCELLED", "COMPLETE"
                    MsgBox "You can not book a job for this project as it is set as " & l_strProjectStatus, vbExclamation
                    Exit Function
                End Select
            End If
            
            Dim l_strStatus As String, l_strSeriesID As String
            
            .chkOnHold.Value = vbChecked
            
            If Left(UCase(.cmbJobType.Text), 3) = "DUB" Then
                If g_optBookDubsAsConfirmedStatus = 0 Then
                    l_strStatus = "Pencil"
                Else
                    l_strStatus = "Submitted"
                End If
            Else
                l_strStatus = "Pencil"
            End If
            
            If Left(UCase(.cmbJobType.Text), 7) = "INVOICE" Then
                l_strStatus = "Hold Cost"
            End If
            
            l_strSQL = "INSERT INTO job (createddate, createduser, createduserID, fd_status) VALUES ('" & FormatSQLDate(Now) & "', '" & g_strUserInitials & "', " & g_lngUserID & ", '" & l_strStatus & "');"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
                
            .txtJobID.Text = g_lngLastID
            
            l_lngJobID = g_lngLastID
                
            AddJobHistory l_lngJobID, "Booked (" & l_strStatus & ")"
            
            If Left(LCase(frmJob.cmbJobType.Text), 3) = "dub" Then
                'this will loop through the resource bookings and create
                'dubbing lines according to the formats of the resources
                AutoCreateDubbings l_lngJobID
            End If
            
            'import any notes associated with this company
            ImportCompanyNotes frmJob.lblCompanyID.Caption, l_lngJobID
            
            GoTo SaveDetails
            
            'l_lngJobID = AddJob(.cmbAllocation.Text, .cmbJobType.Text, "Pencil", .txtOrderReference.Text, .cmbProduct.Text, Val(.lblProductID.Caption), _
            '        Val(.lblProjectID.Caption), .txtTitle.Text, .txtSubTitle.Text, .cmbCompany.Text, .lblCompanyID.Caption, .cmbContact.Text, .lblContactID.Caption, _
            '        .txtTelephone.Text, .txtFax.Text, l_datDeadline, .chkJobDetailOnInvoice.Value, .chkSendEmailOnCompletion.Value, _
            '        .chkQuote.Value, .chkOnHold.Value, .chkUrgent.Value, .cmbOurContact.Text, l_datStartDate, l_datEndDate, .cmbVideoStandard.Text, .cmbDealType.Text, Val(.txtDealPrice.Text), .cmbAllocatedTo.Text, 0, 0, 0)
                    
            If LCase(g_strWhenToAllocate) = "book job" Then
                'here is where we should allocate the job number
                AllocateProjectNumber l_lngJobID
            ElseIf LCase(g_strWhenToAllocate) = "confirm job" And LCase(l_strStatus) = "confirmed" Then
                AllocateProjectNumber l_lngJobID
            End If
        Else
        
SaveDetails:
        
            l_lngJobID = Val(.txtJobID.Text)
            
            l_strSQL = "UPDATE job SET "
            
            l_strSQL = l_strSQL & "donotchargevat = '" & Val(.chkNoVAT.Value) & "', "
            
            'l_strSQL = l_strSQL & "invoicenotes = '" & QuoteSanitise(.txtInvoiceNotes.Text) & "', "
            l_strSQL = l_strSQL & "invoicetext = '" & QuoteSanitise(.txtInvoiceText.Text) & "', "
            If Not IsNull(.datInvoiceDate.Value) Then
                l_strSQL = l_strSQL & "invoiceddate = '" & FormatSQLDate(.datInvoiceDate.Value) & "', "
            Else
                l_strSQL = l_strSQL & "invoiceddate = Null, "
            End If
            l_strSQL = l_strSQL & "joballocation = '" & QuoteSanitise(.cmbAllocation.Text) & "', "
            l_strSQL = l_strSQL & "jobtype = '" & QuoteSanitise(.cmbJobType.Text) & "', "
            l_strSQL = l_strSQL & "orderreference = '" & QuoteSanitise(.txtOrderReference.Text) & "', "
            l_strSQL = l_strSQL & "customercontractreference = '" & QuoteSanitise(.txtClientContractReference.Text) & "', "
            l_strSQL = l_strSQL & "allocatedto = '" & QuoteSanitise(.txtAllocatedTo.Text) & "', "
            l_strSQL = l_strSQL & "productname = '" & QuoteSanitise(.cmbProduct.Text) & "', "
            l_strSQL = l_strSQL & "productID = '" & Val(.lblProductID.Caption) & "', "
            l_strSQL = l_strSQL & "projectnumber = '" & Val(.cmbProject.Text) & "', "
            l_strSQL = l_strSQL & "projectID = '" & Val(.lblProjectID.Caption) & "', "
            If Val(.txtSeriesID.Text) <> 0 Then
                l_strSQL = l_strSQL & "seriesID = " & Val(.txtSeriesID.Text) & ", "
            Else
                l_strSeriesID = ""
                On Error Resume Next
                If .adoMedia(1).RecordSource <> "" Then
                    If .adoMedia(1).Recordset.RecordCount > 0 Then
                        .adoMedia(1).Recordset.MoveFirst
                        Do While Not .adoMedia(1).Recordset.EOF
                            If Val(GetData("library", "seriesID", "libraryID", .adoMedia(1).Recordset("LibraryID"))) <> 0 Then
                                l_strSeriesID = GetData("library", "seriesID", "libraryID", .adoMedia(1).Recordset("LibraryID"))
                                .adoMedia(1).Recordset.MoveLast
                            End If
                            .adoMedia(1).Recordset.MoveNext
                        Loop
                    ElseIf .adoMedia(4).Recordset.RecordCount > 0 Then
                        .adoMedia(4).Recordset.MoveFirst
                        Do While Not .adoMedia(4).Recordset.EOF
                            If GetData("events", "seriesID", "eventID", .adoMedia(4).Recordset("eventID")) <> "" Then
                                l_strSeriesID = GetData("events", "seriesID", "eventID", .adoMedia(4).Recordset("eventID"))
                                .adoMedia(4).Recordset.MoveLast
                            End If
                            .adoMedia(4).Recordset.MoveNext
                        Loop
                    End If
                End If
                On Error GoTo PROC_ERR

                If Val(l_strSeriesID) <> 0 Then
                    l_strSQL = l_strSQL & "seriesID = " & Val(l_strSeriesID) & ", "
                Else
                    l_strSQL = l_strSQL & "seriesID = NULL, "
                End If
            End If
            l_strSQL = l_strSQL & "title1 = '" & Replace(QuoteSanitise(.txtTitle.Text), vbTab, " ") & "', "
            l_strSQL = l_strSQL & "title2 = '" & Replace(QuoteSanitise(.txtSubtitle.Text), vbTab, " ") & "', "
            l_strSQL = l_strSQL & "operatorduration = " & Val(.txtTimeRequired.Text) & ", "
            l_strSQL = l_strSQL & "companyname = '" & QuoteSanitise(.cmbCompany.Text) & "', "
            l_strSQL = l_strSQL & "EmailNotificationSubjectLine = '" & QuoteSanitise(.txtNotificationSubjectLine.Text) & "', "
            l_strSQL = l_strSQL & "EmailNotificationMessageBody = '" & QuoteSanitise(.txtNotificationMessageBody.Text) & "', "
            l_strSQL = l_strSQL & "companyID = '" & Val(.lblCompanyID.Caption) & "', "
            l_strSQL = l_strSQL & "contactname = '" & QuoteSanitise(.cmbContact.Text) & "', "
            l_strSQL = l_strSQL & "contactID = '" & Val(.lblContactID.Caption) & "', "
            l_strSQL = l_strSQL & "telephone = '" & .txtTelephone.Text & "', "
            l_strSQL = l_strSQL & "fax = '" & .txtFax.Text & "', "
            l_strSQL = l_strSQL & "incomingdespatchmethod = '" & .cmbIncomingMethod.Text & "', "
            l_strSQL = l_strSQL & "outgoingdespatchmethod = '" & .cmbOutgoingMethod.Text & "', "
            l_strSQL = l_strSQL & "dealtype = '" & .cmbDealType.Text & "', "
            l_strSQL = l_strSQL & "dealprice = '" & Val(.txtDealPrice.Text) & "', "
            
            If Not IsNull(l_datDeadline) Then
                l_strSQL = l_strSQL & "deadlinedate = '" & FormatSQLDate(l_datDeadline) & "', "
            Else
                l_strSQL = l_strSQL & "deadlinedate = NULL, "
            End If
            
            l_strSQL = l_strSQL & "flagdetailoninvoice = '" & .chkJobDetailOnInvoice.Value & "', "
            l_strSQL = l_strSQL & "flagemail = '" & .chkSendEmailOnCompletion.Value & "', "
            l_strSQL = l_strSQL & "flagquote = '" & .chkQuote.Value & "', "
            l_strSQL = l_strSQL & "dowholeseries = '" & .chkDoWholeSeries.Value & "', "
            l_strSQL = l_strSQL & "donotdeliveryet = '" & .chkDoNotDeliver.Value & "', "
            
            'Thing for MX1 to fiddle with fd_status for quotes.
            If g_optJCASystemVariables <> 0 Then
                Dim l_strCurrentStatus As String
                l_strCurrentStatus = GetData("job", "fd_status", "jobid", l_lngJobID)
                If .chkQuote.Value <> 0 Then
                    If l_strCurrentStatus = "Confirmed" Or l_strCurrentStatus = "Masters Here" Or l_strCurrentStatus = "Submitted" Then
                        l_strSQL = l_strSQL & "fd_status = 'Pencil', "
                    End If
                Else
                    If l_strCurrentStatus = "Pencil" Then
                        l_strSQL = l_strSQL & "fd_status = 'Confirmed', "
                    End If
                End If
            End If
            
            l_strSQL = l_strSQL & "nochargejob = '" & .chkNoChargeJob.Value & "', "
            l_strSQL = l_strSQL & "flaghold = '" & .chkOnHold.Value & "', "
            l_strSQL = l_strSQL & "flagurgent = '" & .chkUrgent.Value & "', "
            l_strSQL = l_strSQL & "ourcontact = '" & QuoteSanitise(.cmbOurContact.Text) & "', "
            l_strSQL = l_strSQL & "modifieddate = '" & FormatSQLDate(Now()) & "', "
            l_strSQL = l_strSQL & "videostandard = '" & .cmbVideoStandard.Text & "',"
            l_strSQL = l_strSQL & "modifieduser = '" & g_strUserInitials & "', "
            l_strSQL = l_strSQL & "modifieduserID = " & g_lngUserID & ", "
            l_strSQL = l_strSQL & "deliverycompanyname = '" & QuoteSanitise(.cmbDeliveryCompany.Text) & "', "
            l_strSQL = l_strSQL & "deliverycontactname = '" & QuoteSanitise(.cmbDeliveryContact.Text) & "', "
            l_strSQL = l_strSQL & "deliveryaddress = '" & QuoteSanitise(.txtDeliveryAddress.Text) & "', "
            l_strSQL = l_strSQL & "deliverypostcode = '" & QuoteSanitise(.txtDeliveryPostCode.Text) & "', "
            l_strSQL = l_strSQL & "deliverycountry = '" & QuoteSanitise(.txtDeliveryCountry.Text) & "', "
            l_strSQL = l_strSQL & "deliverytelephone = '" & QuoteSanitise(.txtDeliveryTelephone.Text) & "', "
            
            l_strSQL = l_strSQL & "collectioncompanyname = '" & QuoteSanitise(.cmbCollectionCompany.Text) & "', "
            l_strSQL = l_strSQL & "collectioncontactname = '" & QuoteSanitise(.cmbCollectionContact.Text) & "', "
            l_strSQL = l_strSQL & "collectionaddress = '" & QuoteSanitise(.txtCollectionAddress.Text) & "', "
            l_strSQL = l_strSQL & "collectionpostcode = '" & QuoteSanitise(.txtCollectionPostCode.Text) & "', "
            l_strSQL = l_strSQL & "collectioncountry = '" & QuoteSanitise(.txtCollectionCountry.Text) & "', "
            l_strSQL = l_strSQL & "collectiontelephone = '" & QuoteSanitise(.txtCollectionTelephone.Text) & "', "
            
            l_strSQL = l_strSQL & "DADCSeries = " & IIf(.txtDADCSeriesID.Text <> "", "'" & .txtDADCSeriesID.Text & "', ", "Null, ")
            l_strSQL = l_strSQL & "DADCLegacyOracTVAENumber = " & IIf(.txtDADCLegacyOracTVAENumber.Text <> "", "'" & .txtDADCLegacyOracTVAENumber.Text & "', ", "Null, ")
            
'            l_strSQL = l_strSQL & "DADCProjectNumber = '" & QuoteSanitise(.txtDADCProjectNumber.Text) & "', "
'            l_strSQL = l_strSQL & "DADCProjectManager = '" & QuoteSanitise(.txtDADCProjectManager.Text) & "', "
'            l_strSQL = l_strSQL & "DADCSeries = '" & QuoteSanitise(.txtDADCSeries.Text) & "', "
'            l_strSQL = l_strSQL & "DADCEpisodeNumber = '" & QuoteSanitise(.txtDADCEpisodeNumber.Text) & "', "
'            l_strSQL = l_strSQL & "DADCEpisodeTitle = '" & QuoteSanitise(.txtDADCEpisodeTitle.Text) & "', "
'            l_strSQL = l_strSQL & "DADCRightsOwner = '" & QuoteSanitise(.txtDADCRightsOwner.Text) & "', "
'            l_strSQL = l_strSQL & "DADCJobTitle = '" & QuoteSanitise(.txtDADCJobTitle.Text) & "', "
            'l_strSQL = l_strSQL & "DADCSvenskTrackerInvoice = '" & .chkSvenskTrackerInvoice.Value & "', "
            
            'l_strSQL = l_strSQL & "dealunit = '" & QuoteSanitise(.cmbDealUnit.Text) & "', "
            
            l_strSQL = l_strSQL & "notes4 = '" & QuoteSanitise(.txtAccounts.Text) & "'"
            l_strSQL = l_strSQL & " WHERE jobID = '" & Val(.txtJobID.Text) & "'"
            
            l_strSQL = l_strSQL & ";"
            
            ExecuteSQL l_strSQL, g_strExecuteError
            
            CheckForSQLError
            
            'Delete costings if it is a nocharge job
            If .chkNoChargeJob.Value <> 0 Then
                l_strSQL = "DELETE FROM costing WHERE jobID = " & Val(.txtJobID.Text) & ";"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            End If
            
            'frmJob.lblModifiedDate.Caption = Now()
            'frmJob.lblModifiedUser.Caption = g_strUserInitials
            AddJobHistory Val(.txtJobID.Text), "Modified"
                            
            l_strSQL = "UPDATE extendedjob SET "
            l_strSQL = l_strSQL & "bbcbookingnumber = '" & .txtBBCBookingNumber.Text & "', "
            l_strSQL = l_strSQL & "bbcbusinessarea = '" & .txtBBCBusinessArea.Text & "', "
            l_strSQL = l_strSQL & "bbcbusinessareacode = '" & .txtBBCBusinessAreaCode.Text & "', "
            l_strSQL = l_strSQL & "bbcchargecode = '" & .txtBBCChargeCode.Text & "', "
            l_strSQL = l_strSQL & "bbccountrycode = '" & .txtBBCCountryCode.Text & "', "
            l_strSQL = l_strSQL & "bbccustomername = '" & .txtBBCCustomername.Text & "', "
            l_strSQL = l_strSQL & "bbcnominalcode = '" & .txtBBCNominalCode.Text & "' "
            l_strSQL = l_strSQL & "WHERE jobid = '" & Val(.txtJobID.Text) & "';"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            
        End If
        
    End With
    
    
    
    With frmJob
        Dim l_lngDespatchID As Long
        If LCase(.cmbJobType.Text) = "hire" Or LCase(.cmbJobType.Text) = "edit" Then
            'create an outward despatch
            
            UpdateDespatchForJob Val(.txtJobID.Text)
            
            'l_lngDespatchID = GetDespatchIDForJob(l_lngJobID, "OUT")
            'l_lngDespatchID = SaveDespatch(l_lngDespatchID, "Hire", Val(.cmbProject.Text), Val(.txtJobID.Text), "OUT", "", .lblCompanyID.Caption, .cmbCompany.Text, .cmbContact.Text, .lblContactID.Caption, GetData("company", "address", "companyID", .lblCompanyID.Caption), GetData("company", "postcode", "companyID", .lblCompanyID.Caption), .txtTelephone.Text, GetData("company", "country", "companyID", .lblCompanyID.Caption), Format(.datStartDate.Value, vbShortDateFormat) & " " & .cmbStartTime.Text, .txtTitle.Text, Format(.datStartDate.Value, vbShortDateFormat) & " " & .cmbStartTime.Text, 0, "", "", "", 0, GetData("contact", "email", "contactID", .lblContactID.Caption), 0, 0, "", "", "", "DEL - HIRE")
            
            'l_lngDespatchID = GetDespatchIDForJob(l_lngJobID, "IN")
            'l_lngDespatchID = SaveDespatch(l_lngDespatchID, "Hire", Val(.cmbProject.Text), Val(.txtJobID.Text), "IN", "", .lblCompanyID.Caption, .cmbCompany.Text, .cmbContact.Text, .lblContactID.Caption, GetData("company", "address", "companyID", .lblCompanyID.Caption), GetData("company", "postcode", "companyID", .lblCompanyID.Caption), .txtTelephone.Text, GetData("company", "country", "companyID", .lblCompanyID.Caption), Format(.datEndDate.Value, vbShortDateFormat) & " " & .cmbEndTime.Text, .txtTitle.Text, Format(.datEndDate, vbShortDateFormat) & " " & .cmbEndTime.Text, 0, "", "Basement", "", 0, GetData("contact", "email", "contactID", .lblContactID.Caption), 0, 0, "", "", "", "DEL - HIRE")
            
        End If
    End With
    
    frmJob.MousePointer = vbDefault
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

'Function SaveMediaJob()
'
'    Dim l_lngJobID As Long
'
'    'TVCodeTools ErrorEnablerStart
'    On Error GoTo PROC_ERR
'    'TVCodeTools ErrorEnablerEnd
'
'    If Not CheckAccess("/savejob") Then
'        Exit Function
'    End If
'
'    If Val(frmJobMedia.lblCompanyID.Caption) = 0 Then
'        NoCompanySelectedMessage
'        Exit Function
'    End If
'
'    If Val(frmJobMedia.lblContactID.Caption) = 0 Then
'        NoContactSelectedMessage
'        Exit Function
'    End If
'
'    Dim l_strSQL As String
'
'    Dim l_strAccountStatus As String
'
'    l_strAccountStatus = GetData("company", "accountstatus", "companyID", frmJobMedia.lblCompanyID.Caption)
'
'    If g_optStopSavingJobsWhenCompanyOnHold = 1 Then
'        If l_strAccountStatus = "HOLD" Then
'            MsgBox "Jobs cannot be created or saved for companies on HOLD status.", vbCritical, "Job Not Saved"
'            Exit Function
'        End If
'    End If
'
'
'    frmJobMedia.MousePointer = vbHourglass
'
'    With frmJobMedia
'
'        Dim l_datDeadline As Variant
'        If Not IsNull(.datDeadlineDate.Value) Then
'            l_datDeadline = Format(.datDeadlineDate.Value, vbShortDateFormat) & " " & .cmbDeadlineTime.Text
'        Else
'            l_datDeadline = Null
'        End If
'
'        If Val(.txtJobID.Text) = 0 Then
'
'
'
'            'check that the project number is valid
'            If Val(frmJobMedia.lblProjectID.Caption) <> 0 Then
'
'                Dim l_strProjectStatus As String
'                l_strProjectStatus = GetData("project", "fd_status", "projectID", frmJobMedia.lblProjectID.Caption)
'
'                Select Case UCase(l_strProjectStatus)
'                Case "INVOICED", "SENT TO ACCOUNTS", "INVOICE", "CANCELLED", "COMPLETE"
'                    MsgBox "You can not book a job for this project as it is set as " & l_strProjectStatus, vbExclamation
'                    Exit Function
'                End Select
'            End If
'
'            Dim l_strStatus As String
'
'            If Left(UCase(.cmbJobType.Text), 3) = "DUB" Then
'                If g_optBookDubsAsConfirmedStatus = 0 Then
'                    l_strStatus = "Pencil"
'                Else
'                    l_strStatus = "Confirmed"
'                End If
'            Else
'                l_strStatus = "Pencil"
'            End If
'
'            If Left(UCase(.cmbJobType.Text), 7) = "INVOICE" Then
'                l_strStatus = "Hold Cost"
'            End If
'
'            l_strSQL = "INSERT INTO job (createddate, createduser, fd_status) VALUES ('" & FormatSQLDate(Now) & "', '" & g_strUserInitials & "', '" & l_strStatus & "');"
'            ExecuteSQL l_strSQL, g_strExecuteError
'            CheckForSQLError
'
'            .txtJobID.Text = g_lngLastID
'
'            l_lngJobID = g_lngLastID
'
'            AddJobHistory l_lngJobID, "Booked (" & l_strStatus & ")"
'
'            If Left(LCase(frmJobMedia.cmbJobType.Text), 3) = "dub" Then
'                'this will loop through the resource bookings and create
'                'dubbing lines according to the formats of the resources
'                AutoCreateDubbings l_lngJobID
'            End If
'
'            'import any notes associated with this company
'            ImportCompanyNotes frmJobMedia.lblCompanyID.Caption, l_lngJobID
'
'            GoTo SaveDetails
'
'            'l_lngJobID = AddJob(.cmbAllocation.Text, .cmbJobType.Text, "Pencil", .txtOrderReference.Text, .cmbProduct.Text, Val(.lblProductID.Caption), _
'            '        Val(.lblProjectID.Caption), .txtTitle.Text, .txtSubTitle.Text, .cmbCompany.Text, .lblCompanyID.Caption, .cmbContact.Text, .lblContactID.Caption, _
'            '        .txtTelephone.Text, .txtFax.Text, l_datDeadline, .chkJobDetailOnInvoice.Value, .chkSendEmailOnCompletion.Value, _
'            '        .chkQuote.Value, .chkOnHold.Value, .chkUrgent.Value, .cmbOurContact.Text, l_datStartDate, l_datEndDate, .cmbVideoStandard.Text, .cmbDealType.Text, Val(.txtDealPrice.Text), .cmbAllocatedTo.Text, 0, 0, 0)
'
'            If LCase(g_strWhenToAllocate) = "book job" Then
'                'here is where we should allocate the job number
'                AllocateProjectNumber l_lngJobID
'            ElseIf LCase(g_strWhenToAllocate) = "confirm job" And LCase(l_strStatus) = "confirmed" Then
'                AllocateProjectNumber l_lngJobID
'            End If
'        Else
'
'SaveDetails:
'
'            l_lngJobID = Val(.txtJobID.Text)
'
'            l_strSQL = "UPDATE job SET "
'
'            'l_strSQL = l_strSQL & "invoicenotes = '" & QuoteSanitise(.txtInvoiceNotes.Text) & "', "
'            l_strSQL = l_strSQL & "joballocation = '" & QuoteSanitise(.cmbAllocation.Text) & "', "
'            l_strSQL = l_strSQL & "jobtype = '" & QuoteSanitise(.cmbJobType.Text) & "', "
'            l_strSQL = l_strSQL & "orderreference = '" & QuoteSanitise(.txtOrderReference.Text) & "', "
'            l_strSQL = l_strSQL & "productname = '" & QuoteSanitise(.cmbProduct.Text) & "', "
'            l_strSQL = l_strSQL & "productID = '" & Val(.lblProductID.Caption) & "', "
'            l_strSQL = l_strSQL & "projectnumber = '" & Val(.cmbProject.Text) & "', "
'            l_strSQL = l_strSQL & "projectID = '" & Val(.lblProjectID.Caption) & "', "
'            l_strSQL = l_strSQL & "title1 = '" & QuoteSanitise(.txtTitle.Text) & "', "
'            l_strSQL = l_strSQL & "title2 = '" & QuoteSanitise(.txtSubtitle.Text) & "', "
'            l_strSQL = l_strSQL & "companyname = '" & QuoteSanitise(.cmbCompany.Text) & "', "
'            l_strSQL = l_strSQL & "companyID = '" & Val(.lblCompanyID.Caption) & "', "
'            l_strSQL = l_strSQL & "contactname = '" & QuoteSanitise(.cmbContact.Text) & "', "
'            l_strSQL = l_strSQL & "contactID = '" & Val(.lblContactID.Caption) & "', "
'            l_strSQL = l_strSQL & "telephone = '" & .txtTelephone.Text & "', "
'            l_strSQL = l_strSQL & "fax = '" & .txtFax.Text & "', "
'
'            If Not IsNull(l_datDeadline) Then
'                l_strSQL = l_strSQL & "deadlinedate = '" & FormatSQLDate(l_datDeadline) & "', "
'            Else
'                l_strSQL = l_strSQL & "deadlinedate = NULL, "
'            End If
'
'            l_strSQL = l_strSQL & "flagdetailoninvoice = '" & .chkJobDetailOnInvoice.Value & "', "
'            l_strSQL = l_strSQL & "flagemail = '" & .chkSendEmailOnCompletion.Value & "', "
'            l_strSQL = l_strSQL & "flagquote = '" & .chkQuote.Value & "', "
'
'
'            'Thing for MX1 to fiddle with fd_status for quotes.
'            If g_optJCASystemVariables <> 0 Then
'                Dim l_strCurrentStatus As String
'                l_strCurrentStatus = GetData("job", "fd_status", "jobid", l_lngJobID)
'                If .chkQuote.Value <> 0 Then
'                    If l_strCurrentStatus = "Confirmed" Then
'                        l_strSQL = l_strSQL & "fd_status = 'Pencil', "
'                    End If
'                Else
'                    If l_strCurrentStatus = "Pencil" Then
'                        l_strSQL = l_strSQL & "fd_status = 'Confirmed', "
'                    End If
'                End If
'            End If
'
'            l_strSQL = l_strSQL & "flaghold = '" & .chkOnHold.Value & "', "
'            l_strSQL = l_strSQL & "flagurgent = '" & .chkUrgent.Value & "', "
'            l_strSQL = l_strSQL & "modifieddate = '" & FormatSQLDate(Now()) & "', "
'            l_strSQL = l_strSQL & "modifieduser = '" & g_strUserInitials & "' "
'
'            l_strSQL = l_strSQL & " WHERE jobID = '" & Val(.txtJobID.Text) & "'"
'
'            l_strSQL = l_strSQL & ";"
'
'            ExecuteSQL l_strSQL, g_strExecuteError
'
'            CheckForSQLError
'
'            AddJobHistory Val(.txtJobID.Text), "Modified"
'
'        End If
'
'    End With
'
'
'
'    With frmJobMedia
'        Dim l_lngDespatchID As Long
'        If LCase(.cmbJobType.Text) = "hire" Or LCase(.cmbJobType.Text) = "edit" Then
'            'create an outward despatch
'
'            UpdateDespatchForJob Val(.txtJobID.Text)
'
'            'l_lngDespatchID = GetDespatchIDForJob(l_lngJobID, "OUT")
'            'l_lngDespatchID = SaveDespatch(l_lngDespatchID, "Hire", Val(.cmbProject.Text), Val(.txtJobID.Text), "OUT", "", .lblCompanyID.Caption, .cmbCompany.Text, .cmbContact.Text, .lblContactID.Caption, GetData("company", "address", "companyID", .lblCompanyID.Caption), GetData("company", "postcode", "companyID", .lblCompanyID.Caption), .txtTelephone.Text, GetData("company", "country", "companyID", .lblCompanyID.Caption), Format(.datStartDate.Value, vbShortDateFormat) & " " & .cmbStartTime.Text, .txtTitle.Text, Format(.datStartDate.Value, vbShortDateFormat) & " " & .cmbStartTime.Text, 0, "", "", "", 0, GetData("contact", "email", "contactID", .lblContactID.Caption), 0, 0, "", "", "", "DEL - HIRE")
'
'            'l_lngDespatchID = GetDespatchIDForJob(l_lngJobID, "IN")
'            'l_lngDespatchID = SaveDespatch(l_lngDespatchID, "Hire", Val(.cmbProject.Text), Val(.txtJobID.Text), "IN", "", .lblCompanyID.Caption, .cmbCompany.Text, .cmbContact.Text, .lblContactID.Caption, GetData("company", "address", "companyID", .lblCompanyID.Caption), GetData("company", "postcode", "companyID", .lblCompanyID.Caption), .txtTelephone.Text, GetData("company", "country", "companyID", .lblCompanyID.Caption), Format(.datEndDate.Value, vbShortDateFormat) & " " & .cmbEndTime.Text, .txtTitle.Text, Format(.datEndDate, vbShortDateFormat) & " " & .cmbEndTime.Text, 0, "", "Basement", "", 0, GetData("contact", "email", "contactID", .lblContactID.Caption), 0, 0, "", "", "", "DEL - HIRE")
'
'        End If
'    End With
'
'    frmJobMedia.MousePointer = vbDefault
'
'    'TVCodeTools ErrorHandlerStart
'PROC_EXIT:
'    Exit Function
'
'PROC_ERR:
'    MsgBox Err.Description
'    Resume PROC_EXIT
'    'TVCodeTools ErrorHandlerEnd
'
'End Function
'
Function SaveJobNotes(lp_lngJobID As Long, lp_strNotes1 As String, lp_strNotes2 As String, lp_strNotes3 As String, lp_strNotes4 As String) As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/savejobnotes") Then
        Exit Function
    End If
    Dim l_strSQL  As String
    
    l_strSQL = "UPDATE job SET "
    
    l_strSQL = l_strSQL & "notes1 = '" & lp_strNotes1 & "', "
    l_strSQL = l_strSQL & "notes2 = '" & lp_strNotes2 & "', "
    l_strSQL = l_strSQL & "notes3 = '" & lp_strNotes3 & "', "
    l_strSQL = l_strSQL & "notes4 = '" & lp_strNotes4 & "' "
    
    l_strSQL = l_strSQL & "WHERE jobID = " & lp_lngJobID
    
    ExecuteSQL l_strSQL, g_strExecuteError
    
    CheckForSQLError
    
    SaveJobNotes = True
    Exit Function
    
SaveJobNotes_Error:
    MsgBox "Could not save notes. The error received was: " & g_strExecuteError, vbExclamation
    SaveJobNotes = False
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub ImportCompanyNotes(lp_lngCompanyID As Long, lp_lngJobID As Long)

Dim l_strNotes As String

l_strNotes = GetData("company", "officeclientnotes", "companyID", lp_lngCompanyID)
If l_strNotes <> "" Then InsertNote lp_lngJobID, "notes1", l_strNotes

l_strNotes = GetData("company", "despatchnotes", "companyID", lp_lngCompanyID)
If l_strNotes <> "" Then InsertNote lp_lngJobID, "notes3", l_strNotes

l_strNotes = GetData("company", "operatornotes", "companyID", lp_lngCompanyID)
If l_strNotes <> "" Then InsertNote lp_lngJobID, "notes2", l_strNotes

l_strNotes = GetData("company", "producernotes", "companyID", lp_lngCompanyID)
If l_strNotes <> "" Then InsertNote lp_lngJobID, "notes5", l_strNotes

l_strNotes = GetData("company", "accountsnotes", "companyID", lp_lngCompanyID)
If l_strNotes <> "" Then InsertNote lp_lngJobID, "notes4", l_strNotes


End Sub

Function SaveProject(ByVal lp_lngProjectID As Long, ByVal lp_lngProductID As Long, ByVal lp_lngQuoteID As Long, ByVal lp_strTitle As String, ByVal lp_strSubTitle As String, ByVal lp_lngCompanyID As Long, ByVal lp_lngContactID As Long, ByVal lp_strTelephone As String, ByVal lp_strNotes As String, ByVal lp_datStartDate As Variant, ByVal lp_datEndDate As Variant, ByVal lp_strFax As String, ByVal lp_strStatus As String, ByVal lp_lngProjectNumber As Long, ByVal lp_strAllocation As String, ByVal lp_strOurContact As String, ByVal lp_strVideoStandard As String) As Long

    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/saveproject") Then
        SaveProject = -1
        Exit Function
    End If
    
    Dim l_strSQL As String
    
    Dim l_strCompanyName As String
    Dim l_strContactName As String
    l_strCompanyName = GetData("company", "name", "companyID", lp_lngCompanyID)
    l_strContactName = GetData("contact", "name", "contactID", lp_lngContactID)
    
    
    If lp_lngProjectNumber = 0 Then lp_lngProjectNumber = GetNextProjectNumber(lp_strAllocation)
    
    If lp_lngProjectID = 0 Then
        'new quote
        
        'work out what the next project number is for this allocation group

        
        l_strSQL = "INSERT INTO project ("
        
        l_strSQL = l_strSQL & "cdate, "
        l_strSQL = l_strSQL & "cuser, "
        l_strSQL = l_strSQL & "projectnumber, "
        l_strSQL = l_strSQL & "productID, "
        l_strSQL = l_strSQL & "quoteID, "
        l_strSQL = l_strSQL & "companyID, "
        l_strSQL = l_strSQL & "companyname, "
        l_strSQL = l_strSQL & "contactID, "
        l_strSQL = l_strSQL & "contactname, "
        l_strSQL = l_strSQL & "title, "
        l_strSQL = l_strSQL & "subtitle, "
        l_strSQL = l_strSQL & "startdate, "
        l_strSQL = l_strSQL & "enddate, "
        l_strSQL = l_strSQL & "telephone, "
        l_strSQL = l_strSQL & "fax, "
        l_strSQL = l_strSQL & "fd_status, "
        l_strSQL = l_strSQL & "allocation, "
        l_strSQL = l_strSQL & "ourcontact, "
        l_strSQL = l_strSQL & "videostandard, "
        l_strSQL = l_strSQL & "description) VALUES ("
        
        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
        l_strSQL = l_strSQL & "'" & lp_lngProjectNumber & "', "
        l_strSQL = l_strSQL & "'" & lp_lngProductID & "', "
        l_strSQL = l_strSQL & "'" & lp_lngQuoteID & "', "
        l_strSQL = l_strSQL & "'" & lp_lngCompanyID & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strCompanyName) & "', "
        l_strSQL = l_strSQL & "'" & lp_lngContactID & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strContactName) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strTitle) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strSubTitle) & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(lp_datStartDate) & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(lp_datEndDate) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strTelephone) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strFax) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strStatus) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strAllocation) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strOurContact) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strVideoStandard) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strNotes) & "')"
    
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
            
    Else
        'edit existing quote
        l_strSQL = "UPDATE project SET "
        
        l_strSQL = l_strSQL & "mdate = " & "'" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "muser = " & "'" & g_strUserInitials & "', "
        l_strSQL = l_strSQL & "productID = " & "'" & lp_lngProductID & "', "
        l_strSQL = l_strSQL & "projectnumber = " & "'" & lp_lngProjectNumber & "', "
        l_strSQL = l_strSQL & "quoteID = " & "'" & lp_lngQuoteID & "', "
        l_strSQL = l_strSQL & "companyID = " & "'" & lp_lngCompanyID & "', "
        l_strSQL = l_strSQL & "companyname = " & "'" & QuoteSanitise(l_strCompanyName) & "', "
        l_strSQL = l_strSQL & "contactID = " & "'" & lp_lngContactID & "', "
        l_strSQL = l_strSQL & "contactname = " & "'" & QuoteSanitise(l_strContactName) & "', "
        l_strSQL = l_strSQL & "title = " & "'" & QuoteSanitise(lp_strTitle) & "', "
        l_strSQL = l_strSQL & "subtitle = " & "'" & QuoteSanitise(lp_strSubTitle) & "', "
        l_strSQL = l_strSQL & "startdate = " & "'" & FormatSQLDate(lp_datStartDate) & "', "
        l_strSQL = l_strSQL & "enddate = " & "'" & FormatSQLDate(lp_datEndDate) & "', "
        l_strSQL = l_strSQL & "telephone = " & "'" & QuoteSanitise(lp_strTelephone) & "', "
        l_strSQL = l_strSQL & "fax = " & "'" & QuoteSanitise(lp_strFax) & "', "
        l_strSQL = l_strSQL & "fd_status = " & "'" & QuoteSanitise(lp_strStatus) & "', "
        l_strSQL = l_strSQL & "allocation = " & "'" & QuoteSanitise(lp_strAllocation) & "', "
        l_strSQL = l_strSQL & "ourcontact = " & "'" & QuoteSanitise(lp_strOurContact) & "', "
        l_strSQL = l_strSQL & "videostandard = " & "'" & QuoteSanitise(lp_strVideoStandard) & "', "
        l_strSQL = l_strSQL & "description = " & "'" & QuoteSanitise(lp_strNotes) & "' "
        
        l_strSQL = l_strSQL & " WHERE projectID = '" & lp_lngProjectID & "';"
    
    
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        
    
    End If
    
    'return the quote ID
    If lp_lngProjectID = 0 Then
        SaveProject = g_lngLastID
    Else
        SaveProject = lp_lngProjectID
    End If
    
    'now run through and update any existing bookings with the new project name
    
    'if we have to update them
    'change all the jobs that have this product to show the new name
    'l_strSQL = "UPDATE job SET projectnumber = '" & QuoteSanitise(lp_strTitle) & "' WHERE projectID = '" & lp_lngProjectID & "';"
    
    'run it
    'ExecuteSQL l_strSQL, g_strExecuteError

    'CheckForSQLError
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function


Sub SaveSticky(lp_datDateToSave As Date, lp_strText As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    l_strSQL = "DELETE FROM sticky WHERE datetoshow = '" & FormatSQLDate(lp_datDateToSave) & "'"
    ExecuteSQL l_strSQL, g_strExecuteError
    
    CheckForSQLError
    
    l_strSQL = "INSERT INTO sticky (datetoshow, contents) VALUES ('" & FormatSQLDate(lp_datDateToSave) & "', '" & QuoteSanitise(lp_strText) & "')"
    ExecuteSQL l_strSQL, g_strExecuteError
    
    CheckForSQLError
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub



Function ChangeMyPassword() As Boolean

frmChangePassword.Show vbModal

If frmChangePassword.Tag = "CHANGED" Then
    ChangeMyPassword = True
Else
    ChangeMyPassword = False
End If

Unload frmChangePassword
Set frmChangePassword = Nothing

End Function

Sub SaveSundry(ByVal lp_lngSundryID As Long, ByVal lp_strSundryType As String, ByVal lp_lngJobID As Long, ByVal lp_lngProjectNumber As Long, ByVal lp_datDateOrdered As Date, ByVal lp_datDateWanted As Date, _
                ByVal lp_strPickupTime As String, ByVal lp_strAccountNumber As String, ByVal lp_strPassenger As String, ByVal lp_strLocation As String, ByVal lp_strDestination As String, _
                ByVal lp_strComments As String, ByVal lp_strRequestedBy As String, ByVal lp_strEnteredBy As String, ByVal lp_strDetails As String, ByVal lp_strSupplier As String, ByVal lp_dblAmount As Double, ByVal lp_intPersonal As Integer, lp_strOverhead As String)
                
Dim l_strSQL As String

'create a new sundry if this is a new one
'then use the same code to edit afterwards
If lp_lngSundryID = 0 Then
    l_strSQL = "INSERT INTO sundrycost (cuser, cdate, fd_status) VALUES ('" & g_strUserInitials & "','" & FormatSQLDate(Now) & "', 'New')"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    lp_lngSundryID = g_lngLastID
    
    If lp_lngJobID <> 0 Then
        AddJobHistory lp_lngJobID, "Added Sundry Cost"
    End If
End If

l_strSQL = "UPDATE sundrycost SET "
If lp_lngJobID <> -1 Then l_strSQL = l_strSQL & "jobID = '" & lp_lngJobID & "', "
If lp_lngProjectNumber <> -1 Then l_strSQL = l_strSQL & "projectnumber = '" & lp_lngProjectNumber & "', "
l_strSQL = l_strSQL & "sundrytype = '" & lp_strSundryType & "', "
l_strSQL = l_strSQL & "dateordered = '" & FormatSQLDate(lp_datDateOrdered) & "', "
l_strSQL = l_strSQL & "daterequired = '" & FormatSQLDate(lp_datDateWanted) & "', "
l_strSQL = l_strSQL & "amount = '" & lp_dblAmount & "', "
l_strSQL = l_strSQL & "comments = '" & QuoteSanitise(lp_strComments) & "', "
l_strSQL = l_strSQL & "requestedby = '" & QuoteSanitise(lp_strRequestedBy) & "', "
l_strSQL = l_strSQL & "enteredby = '" & QuoteSanitise(lp_strEnteredBy) & "', "
l_strSQL = l_strSQL & "pickuptime = '" & QuoteSanitise(lp_strPickupTime) & "', "
l_strSQL = l_strSQL & "accountnumber = '" & QuoteSanitise(lp_strAccountNumber) & "', "
l_strSQL = l_strSQL & "passengers = '" & QuoteSanitise(lp_strPassenger) & "', "
l_strSQL = l_strSQL & "location = '" & QuoteSanitise(lp_strLocation) & "', "
l_strSQL = l_strSQL & "supplier = '" & QuoteSanitise(lp_strSupplier) & "', "
l_strSQL = l_strSQL & "overheadcode = '" & QuoteSanitise(lp_strOverhead) & "', "
l_strSQL = l_strSQL & "personal = '" & lp_intPersonal & "', "
l_strSQL = l_strSQL & "destination = '" & lp_strDestination & "', "

l_strSQL = l_strSQL & "details = '" & QuoteSanitise(lp_strDetails) & "', "
l_strSQL = l_strSQL & "muser = '" & QuoteSanitise(g_strUserInitials) & "', "
l_strSQL = l_strSQL & "mdate = '" & FormatSQLDate(Now) & "' "

l_strSQL = l_strSQL & "WHERE sundrycostID = '" & lp_lngSundryID & "';"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

SetData "job", "flagnewsundries", "jobID", lp_lngJobID, "-1"

End Sub

'Sub DeleteUser(lp_lngUserID As Long)
'
'If Not CheckAccess("/deleteuser") Then Exit Sub
'
'Dim l_strSQL As String
'l_strSQL = "DELETE FROM cetauser WHERE cetauserID = '" & lp_lngUserID & "';"
'
'ExecuteSQL l_strSQL, g_strExecuteError
'CheckForSQLError
'
'End Sub
'
Function MD5Encrypt(ByVal lp_strStringToEncrypt As String) As String

Dim oMD5 As New CMD5
Set oMD5 = New CMD5

Dim l_strEncryptedString As String
l_strEncryptedString = oMD5.MD5(lp_strStringToEncrypt)

Set oMD5 = Nothing

MD5Encrypt = l_strEncryptedString

End Function

Function SaveUser(lp_lngCetauserID As Long, lp_strUserName As String, lp_strPassword As String, lp_strMobileLoginCode As String, lp_strFullname As String, lp_StrInitials As String, lp_strAccesscode As String, lp_strEmail As String, lp_strTelephone As String, lp_strDepartment As String, lp_dblMaxPOValue As Double, lp_strDefaultView As String, lp_strDefaultJobType As String, lp_strDefaultDisplay As String, lp_strDefaultAllocation As String, lp_strDefaultJobTab As String, lp_lngSecurityTemplate As Long, lp_strJobTitle As String, lp_intPasswordNeverExpires As Integer, lp_intListAsOperator As Integer, lp_intListAsMediaServices As Integer, lp_intInvestigateIssues, lp_intSignoffIssues As Integer)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    
    If lp_strPassword <> "nopassword" Then
        If g_optUseMD5Passwords = 1 Then
            Dim l_strPassword As String
            l_strPassword = lp_strPassword
            If lp_strPassword <> "" Then lp_strPassword = MD5Encrypt(lp_strPassword)
        End If
    End If
    
    Dim l_strSQL As String
    frmUserInformation.MousePointer = vbHourglass
    
    If lp_lngCetauserID = 0 Then
        
        l_strSQL = "INSERT INTO cetauser ("
        l_strSQL = l_strSQL & "createddate, "
        l_strSQL = l_strSQL & "jobtitle, "
        l_strSQL = l_strSQL & "securitytemplate, "
        l_strSQL = l_strSQL & "defaultjobtab, "
        l_strSQL = l_strSQL & "defaultallocation, "
        l_strSQL = l_strSQL & "defaultview, "
        l_strSQL = l_strSQL & "defaultjobtype, "
        l_strSQL = l_strSQL & "defaultdisplay, "
        l_strSQL = l_strSQL & "listasoperator, "
        l_strSQL = l_strSQL & "listasmediaservices, "
        l_strSQL = l_strSQL & "investigateissues, "
        l_strSQL = l_strSQL & "signoffissues, "
        l_strSQL = l_strSQL & "username, "
        l_strSQL = l_strSQL & "fd_password, "
        l_strSQL = l_strSQL & "mobilelogincode, "
        l_strSQL = l_strSQL & "fullname, "
        l_strSQL = l_strSQL & "initials, "
        l_strSQL = l_strSQL & "accesscode, "
        l_strSQL = l_strSQL & "email, "
        l_strSQL = l_strSQL & "department, "
        l_strSQL = l_strSQL & "purchaseorderlimit, "
        l_strSQL = l_strSQL & "passwordneverexpires, "
        l_strSQL = l_strSQL & "lastchangedpassworddate, "
        l_strSQL = l_strSQL & "telephone) VALUES ("
        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now()) & "', "
        l_strSQL = l_strSQL & "'" & lp_strJobTitle & "', "
        l_strSQL = l_strSQL & "'" & lp_lngSecurityTemplate & "', "
        l_strSQL = l_strSQL & "'" & lp_strDefaultJobTab & "', "
        l_strSQL = l_strSQL & "'" & lp_strDefaultAllocation & "', "
        l_strSQL = l_strSQL & "'" & lp_strDefaultView & "', "
        l_strSQL = l_strSQL & "'" & lp_strDefaultJobType & "', "
        l_strSQL = l_strSQL & "'" & lp_strDefaultDisplay & "', "
        l_strSQL = l_strSQL & "'" & lp_intListAsOperator & "', "
        l_strSQL = l_strSQL & "'" & lp_intListAsMediaServices & "', "
        l_strSQL = l_strSQL & "'" & lp_intInvestigateIssues & "', "
        l_strSQL = l_strSQL & "'" & lp_intSignoffIssues & "', "
        l_strSQL = l_strSQL & "'" & lp_strUserName & "', "
        l_strSQL = l_strSQL & "'" & lp_strPassword & "', "
        l_strSQL = l_strSQL & "'" & lp_strMobileLoginCode & "', "
        l_strSQL = l_strSQL & "'" & lp_strFullname & "', "
        l_strSQL = l_strSQL & "'" & lp_StrInitials & "', "
        l_strSQL = l_strSQL & "'" & lp_strAccesscode & "', "
        l_strSQL = l_strSQL & "'" & lp_strEmail & "', "
        l_strSQL = l_strSQL & "'" & lp_strDepartment & "', "
        l_strSQL = l_strSQL & "'" & lp_dblMaxPOValue & "', "
        l_strSQL = l_strSQL & "'" & lp_intPasswordNeverExpires & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "'" & lp_strTelephone & "')"
        
    Else
        
        'Save user record to ceta user table in SQL
        
        l_strSQL = "UPDATE cetauser SET "
        l_strSQL = l_strSQL & "modifieddate = '" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "jobtitle = '" & lp_strJobTitle & "', "
        l_strSQL = l_strSQL & "securitytemplate= '" & lp_lngSecurityTemplate & "', "
        l_strSQL = l_strSQL & "defaultjobtab = '" & QuoteSanitise(lp_strDefaultJobTab) & "', "
        l_strSQL = l_strSQL & "defaultallocation = '" & QuoteSanitise(lp_strDefaultAllocation) & "', "
        l_strSQL = l_strSQL & "defaultview = '" & QuoteSanitise(lp_strDefaultView) & "', "
        l_strSQL = l_strSQL & "defaultjobtype = '" & QuoteSanitise(lp_strDefaultJobType) & "', "
        l_strSQL = l_strSQL & "defaultdisplay = '" & QuoteSanitise(lp_strDefaultDisplay) & "', "
        l_strSQL = l_strSQL & "listasoperator = '" & lp_intListAsOperator & "', "
        l_strSQL = l_strSQL & "listasmediaservices = '" & lp_intListAsMediaServices & "', "
        l_strSQL = l_strSQL & "investigateissues = '" & lp_intInvestigateIssues & "', "
        l_strSQL = l_strSQL & "signoffissues = '" & lp_intSignoffIssues & "', "
        l_strSQL = l_strSQL & "username = '" & lp_strUserName & "', "
        
        'only change if the password has changed. This needs the MP5 conversion you see.
        If lp_strPassword <> "" Then
            l_strSQL = l_strSQL & "fd_password = '" & lp_strPassword & "', "
            l_strSQL = l_strSQL & "lastchangedpassworddate = '" & FormatSQLDate(Now) & "', "
        End If
        
        l_strSQL = l_strSQL & "mobilelogincode = '" & lp_strMobileLoginCode & "', "
        l_strSQL = l_strSQL & "fullname = '" & QuoteSanitise(lp_strFullname) & "', "
        l_strSQL = l_strSQL & "initials = '" & lp_StrInitials & "', "
        l_strSQL = l_strSQL & "accesscode = '" & lp_strAccesscode & "', "
        l_strSQL = l_strSQL & "email = '" & lp_strEmail & "', "
        l_strSQL = l_strSQL & "department = '" & lp_strDepartment & "', "
        l_strSQL = l_strSQL & "purchaseorderlimit = '" & lp_dblMaxPOValue & "', "
        l_strSQL = l_strSQL & "passwordneverexpires = '" & lp_intPasswordNeverExpires & "', "
        l_strSQL = l_strSQL & "telephone = '" & lp_strTelephone & "' "
        l_strSQL = l_strSQL & "WHERE cetauserID = '" & lp_lngCetauserID & "'"
        
        
    End If
    
    l_strSQL = l_strSQL & ";"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    
    If lp_lngCetauserID = 0 Then
        
        'send an email to the user about their new account
        If lp_strEmail <> "" Then
        
            Dim l_strEmailBody As String
            Dim l_strAdminEmailName As String
            
            l_strAdminEmailName = GetData("cetauser", "fullname", "email", g_strCETAAdministratorEmail)
            
            l_strEmailBody = "Dear " & lp_strFullname & ","
            l_strEmailBody = l_strEmailBody & vbCrLf
            l_strEmailBody = l_strEmailBody & vbCrLf & "Welcome to CETA Facilities Manager."
            l_strEmailBody = l_strEmailBody & vbCrLf
            l_strEmailBody = l_strEmailBody & vbCrLf & "Your default credentials are as follows:"
            l_strEmailBody = l_strEmailBody & vbCrLf
            l_strEmailBody = l_strEmailBody & vbCrLf & "Logon Name:  " & lp_strUserName
            l_strEmailBody = l_strEmailBody & vbCrLf & "Password:    " & l_strPassword
            l_strEmailBody = l_strEmailBody & vbCrLf
            l_strEmailBody = l_strEmailBody & vbCrLf & "If you have any queries, please contact:"
            l_strEmailBody = l_strEmailBody & vbCrLf
            l_strEmailBody = l_strEmailBody & vbCrLf & l_strAdminEmailName & " (The CETA Administrator)"
            l_strEmailBody = l_strEmailBody & vbCrLf
            l_strEmailBody = l_strEmailBody & vbCrLf & "or press the F1 key at any time in CFM to log a request."
            
            SendSMTPMail lp_strEmail, lp_strFullname, "Welcome To CETA Facilites Manager", "", l_strEmailBody, False, g_strCETAAdministratorEmail, l_strAdminEmailName
        
        
        End If
        
        lp_lngCetauserID = g_lngLastID
    
    
    End If
    
    
    
    SaveUser = lp_lngCetauserID
    
    frmUserInformation.MousePointer = vbDefault
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function SendJobToAccounts(lp_lngJobID As Long, lp_strType As String, lp_lngBatchNumber As Long)

'declare all required variables
Dim l_strCompanyAccountNumber As String, l_lngInvoiceNumber As Long, l_datInvoiceDate As Date, l_datInvoicePayDate As Date, l_strOrderNumber As String, l_strVATCode As String
Dim l_curTotal As Currency, l_curVAT As Currency, l_curTotalIncVAT As Currency, l_strItemAccountCode As String, l_strGeneralNotes As String, l_strCetaCode As String, l_strCetaStockCode As String, l_strCetaDescription As String
Dim l_strItemStockCode As String, l_curStockTotal As Currency, l_curStockVAT As Currency, l_curStockTotalIncVAT As Currency
Dim l_strCompanyName As String

Dim l_lngCompanyID As Long
Dim l_strSQL As String, l_strType As String
Dim l_strPriorityCode As String, l_strItemStockPriorityCode As String

SendJobToAccounts = True

l_strType = lp_strType 'This gets changed during the fuction, and the calling routine needs for the parameter it sent to not change.

'get the company ID as it is needed to work out other values
l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
l_strCompanyName = GetData("job", "companyname", "jobID", lp_lngJobID)

'get the company account number from company table - needs to be the salesledger code, not thew normal company code
l_strCompanyAccountNumber = GetData("company", "salesledgercode", "companyID", l_lngCompanyID)

Select Case l_strType
Case "INVOICE"
    l_strType = "SI"
    l_lngInvoiceNumber = GetData("job", "invoicenumber", "jobID", lp_lngJobID)
    l_datInvoiceDate = GetData("job", "invoiceddate", "jobID", lp_lngJobID)
    l_strSQL = "SELECT * FROM costing WHERE ((creditnotenumber IS NULL) OR (creditnotenumber = 0)) AND (jobID = '" & lp_lngJobID & "') ORDER BY costingID"
Case "CREDIT"
    l_strType = "SC"
    l_lngInvoiceNumber = GetData("job", "creditnotenumber", "jobID", lp_lngJobID)
    l_datInvoiceDate = GetData("job", "creditnotedate", "jobID", lp_lngJobID)
    l_strSQL = "SELECT * FROM costing WHERE jobID = '" & lp_lngJobID & "'"
End Select

'add 30 days to invoice date for pay date
l_datInvoiceDate = Format(l_datInvoiceDate, "dd/mm/yyyy")
l_datInvoicePayDate = DateAdd("d", 30, l_datInvoiceDate)

'order number - if this is blank then send the customer contactname for the job

l_strOrderNumber = GetData("job", "orderreference", "jobID", lp_lngJobID)
If l_strOrderNumber = "" Then l_strOrderNumber = GetData("job", "contactname", "jobID", lp_lngJobID)
'vat code
l_strVATCode = GetData("company", "vatcode", "companyID", l_lngCompanyID)
If l_strVATCode = "" Then l_strVATCode = "1"

Select Case l_strVATCode
    Case "", "1"
        l_strVATCode = "T1"
    Case "G"
        l_strVATCode = "T4"
    Case "7"
        l_strVATCode = "T0"
End Select

'general notes
l_strGeneralNotes = GetData("job", "notes1", "jobID", lp_lngJobID)

'open up the costing table and loop through
Dim l_rstCosting As New ADODB.Recordset
Set l_rstCosting = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

'if there are records, then move to the first one
If Not l_rstCosting.EOF Then l_rstCosting.MoveFirst

'start the loop
Do While Not l_rstCosting.EOF
    'pick up values for each line
    
    'standard fields
    If lp_strType = "INVOICE" Then
        l_curTotal = l_rstCosting("total")
        l_curVAT = l_rstCosting("vat")
        l_curTotalIncVAT = l_rstCosting("totalincludingvat")
        l_curStockTotal = Val(Trim(" " & l_rstCosting("includedstocktotal")))
        l_curStockVAT = Val(Trim(" " & l_rstCosting("includedstockvat")))
        l_curStockTotalIncVAT = Val(Trim(" " & l_rstCosting("includedstocktotalincludingvat")))
    Else
        l_curTotal = 0 - l_rstCosting("total")
        l_curVAT = 0 - l_rstCosting("vat")
        l_curTotalIncVAT = 0 - l_rstCosting("totalincludingvat")
        l_curStockTotal = 0 - Val(Trim(" " & l_rstCosting("includedstocktotal")))
        l_curStockVAT = 0 - Val(Trim(" " & l_rstCosting("includedstockvat")))
        l_curStockTotalIncVAT = 0 - Val(Trim(" " & l_rstCosting("includedstocktotalincludingvat")))
    End If
    
    l_strItemAccountCode = l_rstCosting("accountcode")
    l_strItemStockCode = Trim(" " & l_rstCosting("includedstocknominal"))
    l_strCetaCode = l_rstCosting("costcode")
    l_strCetaDescription = Trim(" " & l_rstCosting("description"))
    If l_strItemStockCode <> "" Then l_strCetaStockCode = GetData("ratecard", "cetacode", "nominalcode", l_strItemStockCode)
    l_strPriorityCode = Trim(" " & l_rstCosting("VDMS_Charge_Code"))
    l_strItemStockPriorityCode = Trim(" " & l_rstCosting("includedstock_VDMS_Charge_Code"))
    If l_strItemStockPriorityCode <> "" Then l_strItemStockPriorityCode = GetData("ratecard", "cetacode", "VDMS_Charge_Code", l_strItemStockCode)
    
    'create a new record in the batch table
    If AddLineToBatch(lp_lngJobID, l_strType, l_strCompanyAccountNumber, l_strCompanyName, l_lngInvoiceNumber, l_datInvoiceDate, l_datInvoicePayDate, l_strOrderNumber, l_strVATCode, l_curTotal, l_curVAT, l_curTotalIncVAT, l_strItemAccountCode, l_strCetaCode, l_strCetaDescription, l_strGeneralNotes, lp_lngBatchNumber, l_strItemStockCode, l_strCetaStockCode, l_curStockTotal, l_curStockVAT, l_curStockTotalIncVAT, l_strPriorityCode, l_strItemStockPriorityCode) = False Then
        SendJobToAccounts = False
        Exit Function
    End If
        
    'move on to the next costing record
    l_rstCosting.MoveNext
Loop

'close the recordset
l_rstCosting.Close
Set l_rstCosting = Nothing

'is this a credit or an invoice? update the correct fields

'set the job and invoice header tables to include the batch number

Select Case lp_strType
Case "INVOICE"
    SetData "job", "senttoaccountsbatchnumber", "jobID", lp_lngJobID, lp_lngBatchNumber
    l_strSQL = "UPDATE invoiceheader SET batchnumber = '" & lp_lngBatchNumber & "', senttoaccountsdate = '" & FormatSQLDate(Now) & "', senttoaccountsuser ='" & g_strUserInitials & "' WHERE jobID = '" & lp_lngJobID & "' AND invoicetype = 'INVOICE'"

Case "CREDIT"
    SetData "job", "creditnotebatchnumber", "jobID", lp_lngJobID, lp_lngBatchNumber
    SetData "job", "creditsenttoaccountsdate", "jobID", lp_lngJobID, FormatSQLDate(Now)
    SetData "job", "creditsenttoaccountsuser", "jobID", lp_lngJobID, g_strUserInitials
    l_strSQL = "UPDATE invoiceheader SET batchnumber = '" & lp_lngBatchNumber & "', senttoaccountsdate = '" & FormatSQLDate(Now) & "', senttoaccountsuser ='" & g_strUserInitials & "' WHERE jobID = '" & lp_lngJobID & "' AND invoicetype = 'CREDIT'"
End Select

'update the rows
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Function
Function AddLineToBatch(lp_lngJobID As Long, lp_strType As String, lp_strCompanyAccountCode As String, lp_strCompanyName As String, lp_lngInvoiceNumber As Long, _
                            lp_datInvoiceDate As Date, lp_datInvoicePayDate As Date, lp_strOrderNumber As String, lp_strVATCode As String, _
                            lp_curTotal As Currency, lp_curVAT As Currency, lp_curTotalIncVAT As Currency, lp_strAccountCode As String, lp_strCETACode As String, lp_strCetaDescription As String, _
                            lp_strGeneralNotes As String, lp_lngBatchNumber As Long, Optional lp_strStockCode As String, Optional lp_strCetaStockCode As String, _
                            Optional lp_curStockTotal As Currency, Optional lp_curStockVAT As Currency, Optional lp_curStockTotalIncVAT As Currency, Optional lp_strPriorityCode As String, Optional lp_strItemStockPriorityCode As String) As Boolean

If lp_curTotalIncVAT <> lp_curTotal + lp_curVAT Then
    AddLineToBatch = False
    Exit Function
End If

Dim l_rstBatchJob As New ADODB.Recordset
Dim l_strNewString As String
Dim l_strSQL As String
Dim l_strPriorityCode As String, l_strPriorityStockCode As String, l_strSAPcustomercode As String
Dim l_lngCompanyID As Long, l_strBusinessUnit As String
Dim l_strSageCustomerReference As String, l_lngSageCode As Long, l_lngSageStockCode As Long

On Error GoTo PROC_ERROR_LINE

If lp_curTotal <> 0 Then
    
    'Add an item to the batchsage table, for this item.
    l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
    l_strSageCustomerReference = GetData("company", "SageCustomerReference", "companyID", l_lngCompanyID)
    l_strSAPcustomercode = GetData("company", "SAPcustomercode", "companyID", l_lngCompanyID)
    l_strBusinessUnit = GetData("company", "BusinessUnit", "companyID", l_lngCompanyID)
    l_lngSageCode = GetData("ProfitCentre5", "VDMS_Charge_Code", "ProfitCentre5ID", lp_strAccountCode)
    
    l_strSQL = "INSERT INTO batchsage (Type, AccountReference, NominalAccountReference, ItemDate, Reference, Details, NetAmount, TaxCode, TaxAmount, ExchangeRate, ExtraReference) VALUES ("
    l_strSQL = l_strSQL & "'" & lp_strType & "', "
    l_strSQL = l_strSQL & "'" & l_strSageCustomerReference & "', "
    l_strSQL = l_strSQL & l_lngSageCode & ", "
    l_strSQL = l_strSQL & "'" & Format(lp_datInvoiceDate, "YYYY-MM-DD") & "', "
    l_strSQL = l_strSQL & lp_lngInvoiceNumber & ", "
    l_strSQL = l_strSQL & "'" & g_strBatchPrefix & lp_lngBatchNumber & "', "
    l_strSQL = l_strSQL & Format(lp_curTotal - lp_curStockTotal, "#.00") & ", "
    l_strSQL = l_strSQL & "'" & lp_strVATCode & "', "
    l_strSQL = l_strSQL & Format(lp_curVAT - lp_curStockVAT, "#.00") & ", "
    l_strSQL = l_strSQL & "1, "
    l_strSQL = l_strSQL & "'" & Left(QuoteSanitise(lp_strOrderNumber), 30) & "');"
    
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    If lp_curStockTotal <> 0 Then
        l_lngSageStockCode = GetData("ProfitCentre5", "VDMS_Charge_Code", "ProfitCentre5ID", lp_strStockCode)
        l_strSQL = "INSERT INTO batchsage (Type, AccountsReference, NominalAccountReference, ItemDate, Reference, Details, NetAmount, TaxCode, TaxAmount, ExchangeRate, ExtraReference) VALUES ("
        l_strSQL = l_strSQL & "'" & lp_strType & "', "
        l_strSQL = l_strSQL & "'" & l_strSageCustomerReference & "', "
        l_strSQL = l_strSQL & l_lngSageStockCode & ", "
        l_strSQL = l_strSQL & "'" & Format(lp_datInvoiceDate, "YYYY-MM-DD") & "', "
        l_strSQL = l_strSQL & lp_lngInvoiceNumber & ", "
        l_strSQL = l_strSQL & "'" & g_strBatchPrefix & lp_lngBatchNumber & "', "
        l_strSQL = l_strSQL & Format(lp_curStockTotal, "#.00") & ", "
        l_strSQL = l_strSQL & "'" & lp_strVATCode & "', "
        l_strSQL = l_strSQL & Format(lp_curStockVAT, "#.00") & ", "
        l_strSQL = l_strSQL & "1, "
        l_strSQL = l_strSQL & "'" & Left(QuoteSanitise(lp_strOrderNumber), 30) & "');"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    End If
    
    'open the batchjob table with no records and do the old style posting anyway
    l_strSQL = "SELECT * FROM batchjob WHERE batchjobID = -1"
    Set l_rstBatchJob = ExecuteSQL(l_strSQL, g_strExecuteError)
        
    If lp_curStockTotal <> 0 Then

        'There is an included stock component of this line - split it into two lines for accounts
        'add a line to the batch table for original line
        l_rstBatchJob.AddNew
        l_rstBatchJob("Type") = lp_strType
        l_rstBatchJob("Customer_Account_Code") = lp_strCompanyAccountCode
        l_rstBatchJob("SageCode") = l_lngSageCode
        l_rstBatchJob("Customer_Name") = lp_strCompanyName
        l_rstBatchJob("SAPcustomercode") = l_strSAPcustomercode
        l_rstBatchJob("SageCustomerReference") = l_strSageCustomerReference
        l_rstBatchJob("Invoice_Number") = lp_lngInvoiceNumber
        l_rstBatchJob("Invoice_Date") = lp_datInvoiceDate
        l_rstBatchJob("Invoice_Pay_Date") = lp_datInvoicePayDate
        l_rstBatchJob("Order_Number") = lp_strOrderNumber
        l_rstBatchJob("Vat_Code") = lp_strVATCode
        l_rstBatchJob("Total") = Format(lp_curTotal - lp_curStockTotal, "#.00")
        l_rstBatchJob("VAT") = Format(lp_curVAT - lp_curStockVAT, "#.00")
        l_rstBatchJob("Total_Inc_VAT") = Format(lp_curTotalIncVAT - lp_curStockTotalIncVAT, "#.00")
        l_rstBatchJob("Account_Code") = lp_strAccountCode
        
        l_rstBatchJob("companyID") = l_lngCompanyID
        l_rstBatchJob("BusinessUnit") = l_strBusinessUnit

        ' replace carriage returns with underscores
        l_strNewString = Replace(lp_strGeneralNotes, Chr(10), "_")
        ' replace hard returns with blanks
        l_strNewString = Replace(l_strNewString, Chr(13), "")
        ' Truncate to 50 characters
        l_strNewString = Trim(Left(l_strNewString, 50))

        l_rstBatchJob("Notes") = l_strNewString
        l_rstBatchJob("Batch_Number") = g_strBatchPrefix & lp_lngBatchNumber
        l_rstBatchJob("Collected_By_Accounts") = "READY"

        'save the record
        l_rstBatchJob.Update

        'add a line to the batch table for the stock
        l_rstBatchJob.AddNew
        l_rstBatchJob("Type") = lp_strType
        l_rstBatchJob("Customer_Account_Code") = lp_strCompanyAccountCode
        l_rstBatchJob("SAPcustomercode") = l_strSAPcustomercode
        l_rstBatchJob("SageCustomerReference") = l_strSageCustomerReference
        l_rstBatchJob("Customer_Name") = lp_strCompanyName
        l_rstBatchJob("Invoice_Number") = lp_lngInvoiceNumber
        l_rstBatchJob("Invoice_Date") = lp_datInvoiceDate
        l_rstBatchJob("Invoice_Pay_Date") = lp_datInvoicePayDate
        l_rstBatchJob("Order_Number") = lp_strOrderNumber
        l_rstBatchJob("Vat_Code") = lp_strVATCode
        l_rstBatchJob("Total") = Format(lp_curStockTotal, "#.00")
        l_rstBatchJob("VAT") = Format(lp_curStockVAT, "#.00")
        l_rstBatchJob("Total_Inc_VAT") = Format(lp_curStockTotalIncVAT, "#.00")
        l_rstBatchJob("Account_Code") = lp_strStockCode
        l_rstBatchJob("SageCode") = l_lngSageStockCode
        l_rstBatchJob("companyID") = l_lngCompanyID
        l_rstBatchJob("BusinessUnit") = l_strBusinessUnit

        ' replace carriage returns with underscores
        l_strNewString = Replace(lp_strGeneralNotes, Chr(10), "_")
        ' replace hard returns with blanks
        l_strNewString = Replace(l_strNewString, Chr(13), "")
        ' Truncate to 50 characters
        l_strNewString = Trim(Left(l_strNewString, 50))

        l_rstBatchJob("Notes") = l_strNewString
        l_rstBatchJob("Batch_Number") = g_strBatchPrefix & lp_lngBatchNumber
        l_rstBatchJob("Collected_By_Accounts") = "READY"

        'save the record
        l_rstBatchJob.Update

    Else

        'add a line to the batch table
        l_rstBatchJob.AddNew
        l_rstBatchJob("Type") = lp_strType
        l_rstBatchJob("Customer_Account_Code") = lp_strCompanyAccountCode
        l_rstBatchJob("Customer_Name") = lp_strCompanyName
        l_rstBatchJob("SAPcustomercode") = l_strSAPcustomercode
        l_rstBatchJob("SageCustomerReference") = l_strSageCustomerReference
        l_rstBatchJob("Invoice_Number") = lp_lngInvoiceNumber
        l_rstBatchJob("Invoice_Date") = lp_datInvoiceDate
        l_rstBatchJob("Invoice_Pay_Date") = lp_datInvoicePayDate
        l_rstBatchJob("Order_Number") = lp_strOrderNumber
        l_rstBatchJob("Vat_Code") = lp_strVATCode
        l_rstBatchJob("Total") = Format(lp_curTotal, "#.00")
        l_rstBatchJob("VAT") = Format(lp_curVAT, "#.00")
        l_rstBatchJob("Total_Inc_VAT") = Format(lp_curTotalIncVAT, "#.00")
        l_rstBatchJob("Account_Code") = lp_strAccountCode
        l_rstBatchJob("SageCode") = l_lngSageCode
        l_rstBatchJob("companyID") = l_lngCompanyID
        l_rstBatchJob("BusinessUnit") = l_strBusinessUnit

        ' replace carriage returns with underscores
        l_strNewString = Replace(lp_strGeneralNotes, Chr(10), "_")
        ' replace hard returns with blanks
        l_strNewString = Replace(l_strNewString, Chr(13), "")
        ' Truncate to 50 characters
        l_strNewString = Trim(Left(l_strNewString, 50))

        l_rstBatchJob("Notes") = l_strNewString
        l_rstBatchJob("Batch_Number") = g_strBatchPrefix & lp_lngBatchNumber
        l_rstBatchJob("Collected_By_Accounts") = "READY"

        'save the record
        l_rstBatchJob.Update

    End If
    
    'close the recordset
    l_rstBatchJob.Close
    Set l_rstBatchJob = Nothing
        
End If

AddLineToBatch = True

PROC_EXIT:

Exit Function

PROC_ERROR_LINE:

SendSMTPMail g_strAdministratorEmailAddress, "", "AddLineToBatch generated an error", "Error Number: " & Err.Number & vbCrLf & "Error Descrtipion: " & Err.Description & vbCrLf, "", True, "", "", ""
AddLineToBatch = False
Resume PROC_EXIT
        
End Function

Sub SendJobToSprockets(lp_lngJobID As Long, lp_lngBatchNumber As Long)

'Arrives her from SendtoCustomer, which deals with making Sprockets Batches.

Dim l_strSQL As String, l_rstCostings As ADODB.Recordset

l_strSQL = "SELECT job.companyname, job.invoicenumber, job.invoiceddate, costing.total, costing.costcode, job.senttosprocketsbatchnumber FROM job INNER JOIN costing on job.jobID = costing.jobID WHERE job.jobID = " & lp_lngJobID & _
    " AND (costing.costcode IN (SELECT description FROM xref WHERE category = 'dformat' AND videostandard = 'SPROCKETS') OR costing.costcode IN (SELECT format FROM xref WHERE category = 'OTHER' AND videostandard = 'SPROCKETS'));"

Set l_rstCostings = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If l_rstCostings.RecordCount > 0 Then
    l_rstCostings.MoveFirst
    Do While Not l_rstCostings.EOF
        l_strSQL = "INSERT INTO batchsprockets (jobID, companyname, originalinvoicenumber, originalinvoicedate, JCAcharged, sprocketsdue, cdate, batchnumber) VALUES ("
        l_strSQL = l_strSQL & lp_lngJobID & ", "
        l_strSQL = l_strSQL & "'" & l_rstCostings("companyname") & "', "
        l_strSQL = l_strSQL & "'" & l_rstCostings("invoicenumber") & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_rstCostings("invoiceddate")) & "', "
        If l_rstCostings("costcode") = "WEFILMDB" Then
            l_strSQL = l_strSQL & l_rstCostings("total") * 0.9 & ", "
            l_strSQL = l_strSQL & l_rstCostings("total") * 0.9 * (g_lngSprocketsPercentage / 100) & ", "
        Else
            l_strSQL = l_strSQL & l_rstCostings("total") & ", "
            l_strSQL = l_strSQL & l_rstCostings("total") * (g_lngSprocketsPercentage / 100) & ", "
        End If
        l_strSQL = l_strSQL & "getdate(), " & lp_lngBatchNumber & ");"
        
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        l_rstCostings("senttosprocketsbatchnumber") = lp_lngBatchNumber
        l_rstCostings.Update
        
        l_rstCostings.MoveNext
    Loop
End If

l_rstCostings.Close
Set l_rstCostings = Nothing

End Sub

Sub SendJobToBBCWW(lp_lngJobID As Long, lp_lngBatchNumber As Long)

'Arrives here from SendToCustomer, which deals with other customers differently from BBCWW.

Dim l_strTheFacilityNumber As String, l_strTheBookingNumber As String, l_strTheOrderNumber As String, l_strTheOrderDate As String
Dim l_strTheTitle As String, l_strTheCustomer As String, l_strTheChargeCode As String, l_strTheCountryCode As String
Dim l_strTheBusinessCode As String, l_strTheNominalCode As String, l_strTheProgrammeNumber As String, l_strTheInvoiceNumber As String
Dim l_strTheInvoiceDate As String, l_strTheItemCode As String, l_strTheItemQuantity As String, l_strTheDuration As String
Dim l_strTheBusinessArea As String, l_dblTheUnitCharge As Double, l_strOrderContactName As String, l_strTheCustomerName As String
Dim l_dblTotalNet As Double, l_strTotalVAT As String, l_strTotalGross As String, l_strTheCompany As String, l_lngCompanyID As Long
Dim l_dblReplayTotal As Double, l_dblDuplicationTotal As Double, l_dblConvertersTotal As Double
Dim l_dblARCTotal As Double, l_dblCeefaxTotal As Double, l_dblVHSTotal As Double
Dim l_strCopyType As String, l_strReplayFormat As String, l_strRecordFormat As String
Dim l_strTheAdditionalProcess As String, l_strTheSecondAdditionalProcess As String, l_strTheThirdAdditionalProcess As String
Dim l_strTheTotalDuration As String, l_strTheRealDuration As String
Dim l_blnMasterFlag As Boolean
Dim l_blnPostingFailed As Boolean
Dim l_strMasterBarcode As String, l_datDeadlineDate As Date, l_datMasterArrived As Date, l_datTargetDate As Date, l_datDateDelivered As Date, l_lngSLAMeasure As Long, l_rstSLA As ADODB.Recordset
Dim l_strSourceStandard As String, l_strSourceAspect As String, l_strCopyStandard As String, l_strCopyAspect As String, l_datCompletedDate As Date, l_strWaybillNumber As String

Dim l_rstCostingDetail As ADODB.Recordset, l_strSQL As String
Dim l_rstDetail As ADODB.Recordset

l_dblReplayTotal = 0: l_dblDuplicationTotal = 0: l_dblConvertersTotal = 0: l_dblTheUnitCharge = 0
l_dblARCTotal = 0: l_dblCeefaxTotal = 0: l_dblVHSTotal = 0: l_dblTotalNet = 0

l_strSQL = "SELECT * FROM jobdetail WHERE (jobID = " & lp_lngJobID & ") ORDER by fd_orderby, jobdetailID"

Set l_rstDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

l_blnPostingFailed = False
If Not l_rstDetail.EOF Then
    l_strTheFacilityNumber = GetData("extendedjob", "bbcbookingnumber", "jobID", lp_lngJobID)
    l_strTheCustomerName = GetData("extendedjob", "bbccustomername", "jobID", lp_lngJobID)
    l_strTheOrderDate = Left(GetData("job", "createddate", "jobID", lp_lngJobID), 10)
    l_strOrderContactName = GetData("job", "contactname", "jobID", lp_lngJobID)
    l_strTheItemCode = GetData("job", "notes1", "jobID", lp_lngJobID)
    
    l_rstDetail.MoveFirst
    l_blnMasterFlag = False
    'FIRSTLY Check that this isn't a "Special Inclusive Price", with only one unrelated costing line
    l_strSQL = "Select * from costing where jobID = " & lp_lngJobID & " and creditnotenumber is null"
    Set l_rstCostingDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    If l_rstCostingDetail.EOF Then
        l_blnPostingFailed = True
        l_rstCostingDetail.Close
    Else
        l_rstCostingDetail.MoveFirst
        If l_rstCostingDetail("copytype") <> "" Then
            'If it isn't, then close costings again, because its going to need to be reopened with different search criteria,
            'then process the job stepping through the job detail lines to get the groups.
            'Groups start with a master line or an inclusive line, then have possibly more master lines, Copy lines, and additional lines.
            'A new group starts with the first master or inclusive line after a line that wasn't a master.
            l_rstCostingDetail.Close
            Do While Not l_rstDetail.EOF
                If UCase(l_rstDetail("copytype")) = "M" Or UCase(l_rstDetail("copytype")) = "I" Or UCase(l_rstDetail("copytype")) = "O" Then
                    If l_blnMasterFlag = False Then
                        'Print The Current Group Out
                        If l_dblTotalNet <> 0 Then
                            l_strTheTotalDuration = Val(l_strTheDuration) * Val(l_strTheItemQuantity) / 60
                            Print #1, "MX1" & Chr(9);
                            Print #1, UCase(l_strTheCustomerName) & Chr(9);
                            Print #1, UCase(l_strOrderContactName) & Chr(9);
                            Print #1, lp_lngJobID & Chr(9);
                            Print #1, l_strTheOrderDate & Chr(9);
                            Print #1, l_strTheOrderNumber & Chr(9);
                            Print #1, l_strTheTitle & Chr(9);
                            Print #1, l_strReplayFormat & Chr(9);
                            Print #1, l_strRecordFormat & Chr(9);
                            Print #1, l_strTheAdditionalProcess & Chr(9);
                            Print #1, l_strTheSecondAdditionalProcess & Chr(9);
'                            Print #1, l_strTheThirdAdditionalProcess & Chr(9);
                            Print #1, l_strTheItemQuantity & Chr(9);
                            Print #1, l_strTheRealDuration & Chr(9);
                            Print #1, l_strTheTotalDuration & Chr(9);
                            Print #1, "1" & Chr(9);
                            Print #1, Format(l_dblTheUnitCharge, "#.00") & Chr(9);
                            Print #1, Format(l_dblTotalNet, "#.00") & Chr(9);
                            'The rest of the columns
                            Print #1, l_strTheChargeCode & Chr(9);
                            Print #1, l_strTheCountryCode & Chr(9);
                            Print #1, l_strTheNominalCode & Chr(9);
                            Print #1, l_strTheBusinessArea & Chr(9);
                            Print #1, l_strTheProgrammeNumber & Chr(9);
                            Print #1, l_strTheItemCode & Chr(9);
                            Print #1, l_strMasterBarcode & Chr(9);
                            Print #1, Format(l_datDeadlineDate, "YYYY-MM-DD HH:NN:SS") & Chr(9);
                            Print #1, Format(l_datMasterArrived, "YYYY-MM-DD HH:NN:SS") & Chr(9);
                            Print #1, Format(l_datTargetDate, "YYYY-MM-DD HH:NN:SS") & Chr(9);
                            Print #1, IIf(l_datDateDelivered <> 0, Format(l_datDateDelivered, "YYYY-MM-DD HH:NN:SS") & Chr(9), Chr(9));
                            Print #1, l_strWaybillNumber;
                            Print #1, l_lngSLAMeasure & Chr(9);
                            Print #1, l_strSourceStandard;
                            Print #1, l_strSourceAspect;
                            Print #1, l_strCopyStandard;
                            Print #1, l_strCopyAspect
                        End If
                        
                        'Process this master starting a new group
                        l_dblReplayTotal = 0: l_dblDuplicationTotal = 0: l_dblConvertersTotal = 0: l_dblTheUnitCharge = 0
                        l_dblARCTotal = 0: l_dblCeefaxTotal = 0: l_dblVHSTotal = 0: l_dblTotalNet = 0
                        l_strTheAdditionalProcess = ""
                        l_strTheSecondAdditionalProcess = ""
                        l_strTheThirdAdditionalProcess = ""
                        
                        'Check Included extra facilities
                        
                        If Left(l_rstDetail("format"), 4) = "WWSD" And InStr(l_rstDetail("format"), "HD") > 0 Then
                            If l_strTheAdditionalProcess = "" Then
                                l_strTheAdditionalProcess = "UP CONVERSION"
                            ElseIf l_strTheSecondAdditionalProcess = "" Then
                                l_strTheSecondAdditionalProcess = "UP CONVERSION"
                            ElseIf l_strTheThirdAdditionalProcess = "" Then
                                l_strTheThirdAdditionalProcess = "UP CONVERSION"
                            Else
                                l_strTheThirdAdditionalProcess = l_strTheThirdAdditionalProcess & ", " & "UP CONVERSION"
                            End If
                        End If
                        
                        If InStr(l_rstDetail("format"), "XC") > 0 Then
                            If l_strTheAdditionalProcess = "" Then
                                l_strTheAdditionalProcess = AdditionalConverter(l_rstDetail("format"))
                            ElseIf l_strTheSecondAdditionalProcess = "" Then
                                l_strTheSecondAdditionalProcess = AdditionalConverter(l_rstDetail("format"))
                            ElseIf l_strTheThirdAdditionalProcess = "" Then
                                l_strTheThirdAdditionalProcess = AdditionalConverter(l_rstDetail("format"))
                            Else
                                l_strTheThirdAdditionalProcess = l_strTheThirdAdditionalProcess & ", " & AdditionalConverter(l_rstDetail("format"))
                            End If
                        End If
                        If InStr(l_rstDetail("format"), "AC") > 0 Then
                            If l_strTheAdditionalProcess = "" Then
                                l_strTheAdditionalProcess = "ASPECT RATIO CONVERSION"
                            ElseIf l_strTheSecondAdditionalProcess = "" Then
                                l_strTheSecondAdditionalProcess = "ASPECT RATIO CONVERSION"
                            ElseIf l_strTheThirdAdditionalProcess = "" Then
                                l_strTheThirdAdditionalProcess = "ASPECT RATIO CONVERSION"
                            Else
                                l_strTheThirdAdditionalProcess = l_strTheThirdAdditionalProcess & ", ASPECT RATIO CONVERSION"
                            End If
                        End If
                        If Right(l_rstDetail("format"), 1) = "S" And Right(l_rstDetail("format"), 3) <> "VHS" Then
                            If l_strTheAdditionalProcess = "" Then
                                l_strTheAdditionalProcess = "CLOSED-CAPTION SUBTITLES"
                            ElseIf l_strTheSecondAdditionalProcess = "" Then
                                l_strTheSecondAdditionalProcess = "CLOSED-CAPTION SUBTITLES"
                            ElseIf l_strTheThirdAdditionalProcess = "" Then
                                l_strTheThirdAdditionalProcess = "CLOSED-CAPTION SUBTITLES"
                            Else
                                l_strTheThirdAdditionalProcess = l_strTheThirdAdditionalProcess & ", " & "CLOSED-CAPTION SUBTITLES"
                            End If
                        End If
                        If Left(l_rstDetail("format"), 2) = "WW" Then
                            If l_rstDetail("format") = "WWMEDMAN" Then
                                l_strTheAdditionalProcess = "HARD DRIVE"
                            ElseIf Mid(l_rstDetail("format"), Len(l_rstDetail("format")) - 4, 1) = "E" Or Mid(l_rstDetail("format"), Len(l_rstDetail("format")) - 5, 2) = "EM" Then
                                If l_strTheAdditionalProcess = "" Then
                                    l_strTheAdditionalProcess = "DOLBY E ENCODE"
                                ElseIf l_strTheSecondAdditionalProcess = "" Then
                                    l_strTheSecondAdditionalProcess = "DOLBY E ENCODE"
                                ElseIf l_strTheThirdAdditionalProcess = "" Then
                                    l_strTheThirdAdditionalProcess = "DOLBY E ENCODE"
                                Else
                                    l_strTheThirdAdditionalProcess = l_strTheThirdAdditionalProcess & ", " & "DOLBY E ENCODE"
                                End If
                            End If
                        End If
                        
                        l_strTheOrderNumber = GetData("extendedjobdetail", "bbcordernumber", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strTheCustomerName = GetData("extendedjobdetail", "bbccustomername", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strTheTitle = Trim(" " & l_rstDetail("description"))
                        l_strTheChargeCode = GetData("extendedjobdetail", "bbcchargecode", "jobdetailID", l_rstDetail("jobdetailID"))
                        If l_strTheChargeCode = "" Then l_strTheChargeCode = GetData("job", "orderreference", "jobID", lp_lngJobID)
                        l_strTheCountryCode = GetData("extendedjobdetail", "bbccountrycode", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strTheBusinessArea = GetData("extendedjobdetail", "bbcbusinessarea", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strTheNominalCode = GetData("extendedjobdetail", "bbcnominalcode", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strTheProgrammeNumber = GetData("extendedjobdetail", "bbcprogrammeID", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strSourceStandard = GetData("extendedjobdetail", "MasterVideoStandard", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strSourceAspect = GetData("extendedjobdetail", "MasterAspectRatio", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strCopyStandard = GetData("extendedjobdetail", "CopyVideoStandard", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strCopyAspect = GetData("extendedjobdetail", "CopyAspectRatio", "jobdetailID", l_rstDetail("jobdetailID"))
                        If l_rstDetail("copytype") = "M" Then
                            If l_rstDetail("format") = "OADVD" Then
                                l_strReplayFormat = "Offair Recording"
                            Else
                                l_strReplayFormat = "Additional " & GetData("extendedjobdetail", "bbcsourceformat", "jobdetailID", l_rstDetail("jobdetailID"))
                            End If
                        Else
                            l_strReplayFormat = GetData("extendedjobdetail", "bbcsourceformat", "jobdetailID", l_rstDetail("jobdetailID"))
                        End If
                        If l_strReplayFormat = "" Then l_strReplayFormat = GetData("jobdetail", "workflowdescription", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strTheRealDuration = Trim(" " & l_rstDetail("runningtime"))
                        l_strRecordFormat = GetData("extendedjobdetail", "bbcrecordformat", "jobdetailID", l_rstDetail("jobdetailID"))
                        If l_strRecordFormat = "" Then l_strRecordFormat = GetData("jobdetail", "workflowdescription", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strSQL = "Select * FROM costing WHERE jobdetailID = " & l_rstDetail("JobdetailID") & " and creditnotenumber is null"
                        Set l_rstCostingDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
                        CheckForSQLError
                        If Not l_rstCostingDetail.EOF Then
                            l_rstCostingDetail.MoveFirst
                            If l_rstCostingDetail("fd_time") <> 0 Then
                                l_strTheDuration = l_rstCostingDetail("fd_time")
                            Else
                                If l_rstDetail("copytype") = "I" Then
                                    If Val(Right(l_rstCostingDetail("costcode"), 3) / 15) = Int(Val(Right(l_rstCostingDetail("costcode"), 3) / 15)) Then
                                        l_strTheDuration = Val(Right(l_rstCostingDetail("costcode"), 3))
                                    Else
                                        l_strTheDuration = Val(Right(l_rstCostingDetail("costcode"), 3) - 3)
                                    End If
                                Else
                                    l_strTheDuration = 0
                                End If
                            End If
                            l_strTheItemQuantity = l_rstCostingDetail("quantity")
                            If l_strCopyType = "M" Then
                                l_dblReplayTotal = l_rstCostingDetail("total")
                                l_dblTheUnitCharge = l_rstCostingDetail("unitcharge") * l_rstCostingDetail("fd_time") / 60
                            Else
                                If l_rstDetail("format") = "WWSDVHS" Or l_rstDetail("format") = "WWSDSVHS" Or l_rstDetail("format") = "WWADVHS" _
                                Or l_rstDetail("format") = "WWDVDVHS" Or l_rstDetail("format") = "WWVHSVHS" Then
                                    l_dblVHSTotal = l_rstCostingDetail("total")
                                Else
                                    l_dblDuplicationTotal = l_rstCostingDetail("total")
                                End If
                                If l_rstCostingDetail("total") <> 0 Then l_dblTheUnitCharge = Val(l_rstCostingDetail("unitcharge"))
                            End If
                            l_dblTotalNet = l_rstCostingDetail("total")
                            l_rstCostingDetail.Close
                            If l_strCopyType = "M" Then l_blnMasterFlag = True
                        Else
                            l_blnPostingFailed = True
                        End If
                        l_strMasterBarcode = Trim(" " & l_rstDetail("MasterBarcode"))
                        l_datDeadlineDate = GetData("job", "deadlinedate", "jobID", lp_lngJobID)
                        l_strWaybillNumber = Trim(" " & l_rstDetail("WaybillNumber"))
                        If Not IsNull(l_rstDetail("DateMasterArrived")) Then l_datMasterArrived = l_rstDetail("DateMasterArrived")
                        If Not IsNull(l_rstDetail("Targetdate")) Then l_datTargetDate = l_rstDetail("Targetdate")
                        If Not IsNull(l_rstDetail("completeddate")) Then l_datCompletedDate = l_rstDetail("completeddate") Else l_datCompletedDate = 0
                        If Not IsNull(l_rstDetail("completeddate")) Then l_datDateDelivered = IIf(IsDate(l_rstDetail("DateDelivered")), l_rstDetail("datedelivered"), l_rstDetail("completeddate")) Else l_datDateDelivered = 0
                        Set l_rstSLA = ExecuteSQL("exec fn_getWorkingDaysForRange @fromdate='" & Format(l_datDateDelivered, "YYYY-MM-DD hh:nn:ss") & "', @todate='" & FormatSQLDate(l_datTargetDate) & "'", g_strExecuteError)
                        If Not IsNull(l_rstDetail("completeddate")) Then l_lngSLAMeasure = l_rstSLA(0) Else l_lngSLAMeasure = 0
                        l_rstSLA.Close
                    Else
                        'Add this Master to the group
                        l_strSQL = "Select * FROM costing WHERE jobdetailID = " & l_rstDetail("JobdetailID") & " and creditnotenumber is null"
                        Set l_rstCostingDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
                        CheckForSQLError
                        l_rstCostingDetail.MoveFirst
                        l_dblReplayTotal = l_dblReplayTotal + l_rstCostingDetail("total")
                        l_dblTheUnitCharge = l_dblTheUnitCharge + (l_rstCostingDetail("unitcharge") * l_rstCostingDetail("fd_time") / 60)
                        l_dblTotalNet = l_dblTotalNet + l_rstCostingDetail("total")
                        l_rstCostingDetail.Close
                    End If
                ElseIf l_rstDetail("copytype") = "C" Then
                    l_blnMasterFlag = False
                    'add this copy to the group
                    l_strSQL = "Select * FROM costing WHERE jobdetailID = " & l_rstDetail("JobdetailID") & " and creditnotenumber is null"
                    Set l_rstCostingDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
                    CheckForSQLError
                    l_rstCostingDetail.MoveFirst
                    'First check if there is a live group, and if not read all the data, because this was a parrallel job.
                    If l_strTheOrderNumber = "" Then
                        l_strTheOrderNumber = GetData("extendedjobdetail", "bbcordernumber", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strTheTitle = l_rstDetail("description")
                        l_strTheChargeCode = GetData("extendedjobdetail", "bbcchargecode", "jobdetailID", l_rstDetail("jobdetailID"))
                        If l_strTheChargeCode = "" Then l_strTheChargeCode = GetData("job", "orderreference", "jobID", lp_lngJobID)
                        l_strTheCountryCode = GetData("extendedjobdetail", "bbccountrycode", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strTheBusinessArea = GetData("extendedjobdetail", "bbcbusinessarea", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strTheNominalCode = GetData("extendedjobdetail", "bbcnominalcode", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strTheProgrammeNumber = GetData("extendedjobdetail", "bbcprogrammeID", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strSourceStandard = GetData("extendedjobdetail", "MasterVideoStandard", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strSourceAspect = GetData("extendedjobdetail", "MasterAspectRatio", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strCopyStandard = GetData("extendedjobdetail", "CopyVideoStandard", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strCopyAspect = GetData("extendedjobdetail", "CopyAspectRatio", "jobdetailID", l_rstDetail("jobdetailID"))
                        l_strTheDuration = l_rstDetail("runningtime")
                        l_strRecordFormat = GetData("extendedjobdetail", "bbcrecordformat", "jobdetailID", l_rstDetail("jobdetailID"))
                        If l_strRecordFormat = "" Then
                            l_strRecordFormat = GetData("jobdetail", "workflowdescription", "jobdetailID", l_rstDetail("jobdetailID"))
                        Else
                            l_strRecordFormat = "Additional " & l_strRecordFormat
                        End If
                        l_strTheItemQuantity = l_rstCostingDetail("quantity")
                    End If
                    Do While Not l_rstCostingDetail.EOF
                        If l_rstCostingDetail("copytype") = "C" Then
                            If l_rstDetail("format") = "VHS" Or l_rstDetail("format") = "VHSM" Or l_rstDetail("format") = "SVHS" Then
                                l_dblVHSTotal = l_dblVHSTotal + l_rstCostingDetail("total")
                                l_dblTotalNet = l_dblTotalNet + l_rstCostingDetail("total")
                                l_dblTheUnitCharge = l_dblTheUnitCharge + (l_rstCostingDetail("unitcharge") * l_rstCostingDetail("fd_time") / 60)
                            Else
                                l_dblDuplicationTotal = l_dblDuplicationTotal + l_rstCostingDetail("total")
                                l_dblTotalNet = l_dblTotalNet + l_rstCostingDetail("total")
                                l_dblTheUnitCharge = l_dblTheUnitCharge + (l_rstCostingDetail("unitcharge") * l_rstCostingDetail("fd_time") / 60)
                            End If
                        ElseIf l_rstCostingDetail("copytype") = "S" Then
                            If l_rstDetail("format") = "VHS" Or l_rstDetail("format") = "VHSM" Or l_rstDetail("format") = "SVHS" Then
                                l_dblVHSTotal = l_dblVHSTotal + l_rstCostingDetail("total")
                                l_dblTotalNet = l_dblTotalNet + l_rstCostingDetail("total")
                                l_dblTheUnitCharge = l_dblTheUnitCharge + l_rstCostingDetail("unitcharge")
                            Else
                                l_dblDuplicationTotal = l_dblDuplicationTotal + l_rstCostingDetail("total")
                                l_dblTotalNet = l_dblTotalNet + l_rstCostingDetail("total")
                                l_dblTheUnitCharge = l_dblTheUnitCharge + l_rstCostingDetail("unitcharge")
                            End If
                        End If
                        l_rstCostingDetail.MoveNext
                    Loop
                    l_rstCostingDetail.Close
                ElseIf l_rstDetail("copytype") = "A" Then
                    l_blnMasterFlag = False
                    'Add this Additional Line to the group
                    l_strSQL = "Select * FROM costing WHERE jobdetailID = " & l_rstDetail("JobdetailID") & " and creditnotenumber is null"
                    Set l_rstCostingDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
                    CheckForSQLError
                    l_rstCostingDetail.MoveFirst
                    Do While Not l_rstCostingDetail.EOF
                        If l_rstCostingDetail("copytype") = "A" Then
                            If l_strTheAdditionalProcess = "" Then
                                l_strTheAdditionalProcess = l_rstCostingDetail("description")
                            ElseIf l_strTheSecondAdditionalProcess = "" Then
                                l_strTheSecondAdditionalProcess = l_rstCostingDetail("description")
                            ElseIf l_strTheThirdAdditionalProcess = "" Then
                                l_strTheThirdAdditionalProcess = l_rstCostingDetail("description")
                            Else
                                l_strTheThirdAdditionalProcess = l_strTheThirdAdditionalProcess & ", " & l_rstCostingDetail("description")
                            End If
                            If l_rstCostingDetail("costcode") = "CONV" Or l_rstCostingDetail("costcode") = "B-CONV" Or l_rstCostingDetail("costcode") = "UCON" Or l_rstCostingDetail("costcode") = "UPCONV" Then
                                l_dblConvertersTotal = l_dblConvertersTotal + l_rstCostingDetail("total")
                                l_dblTotalNet = l_dblTotalNet + l_rstCostingDetail("total")
                                l_dblTheUnitCharge = l_dblTheUnitCharge + (l_rstCostingDetail("unitcharge") * l_rstCostingDetail("fd_time") / 60)
                            ElseIf l_rstCostingDetail("costcode") = "SUBT" Then
                                l_dblCeefaxTotal = l_dblCeefaxTotal + l_rstCostingDetail("total")
                                l_dblTotalNet = l_dblTotalNet + l_rstCostingDetail("total")
                                l_dblTheUnitCharge = l_dblTheUnitCharge + (l_rstCostingDetail("unitcharge") * l_rstCostingDetail("fd_time") / 60)
                            ElseIf l_rstCostingDetail("costcode") = "ARATIO" Then
                                l_dblARCTotal = l_dblARCTotal + l_rstCostingDetail("total")
                                l_dblTotalNet = l_dblTotalNet + l_rstCostingDetail("total")
                                l_dblTheUnitCharge = l_dblTheUnitCharge + (l_rstCostingDetail("unitcharge") * l_rstCostingDetail("fd_time") / 60)
                            Else
                                l_dblDuplicationTotal = l_dblDuplicationTotal + l_rstCostingDetail("total")
                                l_dblTotalNet = l_dblTotalNet + l_rstCostingDetail("total")
                                l_dblTheUnitCharge = l_dblTheUnitCharge + (l_rstCostingDetail("unitcharge") * l_rstCostingDetail("fd_time") / 60)
                            End If
                        ElseIf l_rstCostingDetail("copytype") = "S" Then
                            If l_rstDetail("format") = "VHS" Or l_rstDetail("format") = "VHSM" Or l_rstDetail("format") = "SVHS" Then
                                l_dblVHSTotal = l_dblVHSTotal + l_rstCostingDetail("total")
                                l_dblTotalNet = l_dblTotalNet + l_rstCostingDetail("total")
                                l_dblTheUnitCharge = l_dblTheUnitCharge + l_rstCostingDetail("unitcharge")
                            Else
                                l_dblDuplicationTotal = l_dblDuplicationTotal + l_rstCostingDetail("total")
                                l_dblTotalNet = l_dblTotalNet + l_rstCostingDetail("total")
                                l_dblTheUnitCharge = l_dblTheUnitCharge + l_rstCostingDetail("unitcharge")
                            End If
                        End If
                        l_rstCostingDetail.MoveNext
                    Loop
                    l_rstCostingDetail.Close
'                ElseIf l_rstDetail("copytype") = "O" Then
'                    'We now get others under inclusive pricing for extra VHS and DVD copies.
'                    l_blnMasterFlag = False
'                    'Add this Additional Line to the group
'                    l_strSQL = "Select * FROM costing WHERE jobdetailID = " & l_rstDetail("JobdetailID") & " and creditnotenumber is null"
'                    Set l_rstCostingDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
'                    CheckForSQLError
'                    l_rstCostingDetail.MoveFirst
'                    Do While Not l_rstCostingDetail.EOF
'                        If l_strTheAdditionalProcess = "" Then
'                            l_strTheAdditionalProcess = l_rstCostingDetail("costcode")
'                        ElseIf l_strTheSecondAdditionalProcess = "" Then
'                            l_strTheSecondAdditionalProcess = l_rstCostingDetail("costcode")
'                        ElseIf l_strTheThirdAdditionalProcess = "" Then
'                            l_strTheThirdAdditionalProcess = l_rstCostingDetail("costcode")
'                        Else
'                            l_strTheThirdAdditionalProcess = l_strTheThirdAdditionalProcess & ", " & l_rstCostingDetail("costcode")
'                        End If
'                        l_dblDuplicationTotal = l_dblDuplicationTotal + l_rstCostingDetail("total")
'                        l_dblTotalNet = l_dblTotalNet + l_rstCostingDetail("total")
'                        l_dblTheUnitCharge = l_dblTheUnitCharge + (l_rstCostingDetail("unitcharge") * l_rstCostingDetail("fd_time") / 60)
'                        l_rstCostingDetail.MoveNext
'                    Loop
                End If
                
                l_rstDetail.MoveNext
            Loop
        Else
            'A group containing the unaligned line - probably a "Special Inclusive Price"
            l_blnMasterFlag = False
            l_dblReplayTotal = 0: l_dblDuplicationTotal = 0: l_dblConvertersTotal = 0: l_dblTheUnitCharge = 0
            l_dblARCTotal = 0: l_dblCeefaxTotal = 0: l_dblVHSTotal = 0: l_dblTotalNet = 0
            l_strTheOrderNumber = GetData("extendedjobdetail", "bbcordernumber", "jobdetailID", l_rstDetail("jobdetailID"))
            l_strTheTitle = l_rstDetail("description")
            l_strTheChargeCode = GetData("extendedjobdetail", "bbcchargecode", "jobdetailID", l_rstDetail("jobdetailID"))
            l_strTheCountryCode = GetData("extendedjobdetail", "bbccountrycode", "jobdetailID", l_rstDetail("jobdetailID"))
            l_strTheBusinessArea = GetData("extendedjobdetail", "bbcbusinessarea", "jobdetailID", l_rstDetail("jobdetailID"))
            l_strTheNominalCode = GetData("extendedjobdetail", "bbcnominalcode", "jobdetailID", l_rstDetail("jobdetailID"))
            l_strTheProgrammeNumber = GetData("extendedjobdetail", "bbcprogrammeID", "jobdetailID", l_rstDetail("jobdetailID"))
            l_strTheDuration = l_rstCostingDetail("fd_time")
            l_strTheRealDuration = l_rstDetail("runningtime")
            l_strTheItemQuantity = l_rstCostingDetail("quantity")
            l_dblTheUnitCharge = l_rstCostingDetail("unitcharge")
            l_dblTotalNet = l_rstCostingDetail("total")
            l_rstCostingDetail.Close
        End If
        l_rstDetail.Close
        'Print The Current Group Out because it is the last one hanging over
        If l_dblTotalNet <> 0 Then
            l_strTheTotalDuration = Val(l_strTheDuration) * Val(l_strTheItemQuantity) / 60
            Print #1, "MX1" & Chr(9);
            Print #1, UCase(l_strTheCustomerName) & Chr(9);
            Print #1, UCase(l_strOrderContactName) & Chr(9);
            Print #1, lp_lngJobID & Chr(9);
            Print #1, l_strTheOrderDate & Chr(9);
            Print #1, l_strTheOrderNumber & Chr(9);
            Print #1, l_strTheTitle & Chr(9);
            Print #1, l_strReplayFormat & Chr(9);
            Print #1, l_strRecordFormat & Chr(9);
            Print #1, l_strTheAdditionalProcess & Chr(9);
            Print #1, l_strTheSecondAdditionalProcess & Chr(9);
'            Print #1, l_strTheThirdAdditionalProcess & Chr(9);
            Print #1, l_strTheItemQuantity & Chr(9);
            Print #1, l_strTheRealDuration & Chr(9);
            Print #1, l_strTheTotalDuration & Chr(9);
            Print #1, "1" & Chr(9);
            Print #1, Format(l_dblTheUnitCharge, "#.00") & Chr(9);
            Print #1, Format(l_dblTotalNet, "#.00") & Chr(9);
            'The rest of the columns
            Print #1, l_strTheChargeCode & Chr(9);
            Print #1, l_strTheCountryCode & Chr(9);
            Print #1, l_strTheNominalCode & Chr(9);
            Print #1, l_strTheBusinessArea & Chr(9);
            Print #1, l_strTheProgrammeNumber & Chr(9);
            Print #1, l_strTheItemCode & Chr(9);
            Print #1, l_strMasterBarcode & Chr(9);
            Print #1, Format(l_datDeadlineDate, "YYYY-MM-DD HH:NN:SS") & Chr(9);
            Print #1, Format(l_datMasterArrived, "YYYY-MM-DD HH:NN:SS") & Chr(9);
            Print #1, Format(l_datTargetDate, "YYYY-MM-DD HH:NN:SS") & Chr(9);
            Print #1, IIf(l_datDateDelivered <> 0, Format(l_datDateDelivered, "YYYY-MM-DD HH:NN:SS") & Chr(9), Chr(9));
            Print #1, l_strWaybillNumber;
            Print #1, l_lngSLAMeasure & Chr(9);
            Print #1, l_strSourceStandard;
            Print #1, l_strSourceAspect;
            Print #1, l_strCopyStandard;
            Print #1, l_strCopyAspect
        End If
    End If
Else
    l_blnPostingFailed = True
End If

'Set the BatchNumber, or clear the setting date if it failed
If l_blnPostingFailed = False Then
    SetData "job", "senttoBBCbatchnumber", "jobID", lp_lngJobID, lp_lngBatchNumber
Else
    SetData "job", "senttoBBCDate", "jobID", lp_lngJobID, Null
    MsgBox "Job " & lp_lngJobID & "Failed to Post - Manually correct this job before reposting.", vbInformation, "Posting Failure"
End If

End Sub

Function SetData(lp_strTableName As String, lp_strFieldToEdit As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, lp_varValue As Variant) As Variant
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim a As New clsDataFunctions
    SetData = a.SetData(lp_strTableName, lp_strFieldToEdit, lp_strFieldToSearch, lp_varCriterea, lp_varValue)
    Set a = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub SetDiscount(lp_lngCompanyID As Long, lp_strCETACode As String, lp_sngDiscount As Single)
    
    'first delete any discounts for this company/code
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    l_strSQL = "DELETE FROM discount WHERE companyID = " & lp_lngCompanyID & " AND cetacode = '" & lp_strCETACode & "'"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    
    CheckForSQLError
    
    l_strSQL = "INSERT INTO discount (companyID, cetacode, discount) VALUES ('" & lp_lngCompanyID & "','" & lp_strCETACode & "','" & lp_sngDiscount & "')"
    ExecuteSQL l_strSQL, g_strExecuteError
    
    CheckForSQLError
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub SetToolBar(lp_strPosition As String)
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Select Case lp_strPosition
        Case "top"
            MDIForm1.barHeader.Align = 1
            MDIForm1.mnuToolbar_Top.Checked = True
            MDIForm1.mnuToolbar_Left.Checked = False
            MDIForm1.mnuToolBar_Right.Checked = False
            MDIForm1.mnuToolbar_Bottom.Checked = False
            
        Case "left"
            MDIForm1.barHeader.Align = 3
            MDIForm1.mnuToolbar_Top.Checked = False
            MDIForm1.mnuToolbar_Left.Checked = True
            MDIForm1.mnuToolBar_Right.Checked = False
            MDIForm1.mnuToolbar_Bottom.Checked = False
            
        Case "right"
            MDIForm1.barHeader.Align = 4
            MDIForm1.mnuToolbar_Top.Checked = False
            MDIForm1.mnuToolbar_Left.Checked = False
            MDIForm1.mnuToolBar_Right.Checked = True
            MDIForm1.mnuToolbar_Bottom.Checked = False
            
        Case "bottom"
            MDIForm1.barHeader.Align = 2
            MDIForm1.mnuToolbar_Top.Checked = False
            MDIForm1.mnuToolbar_Left.Checked = False
            MDIForm1.mnuToolBar_Right.Checked = False
            MDIForm1.mnuToolbar_Bottom.Checked = True
            
        Case "hidden"
            MDIForm1.barHeader.Visible = Not MDIForm1.barHeader.Visible
            MDIForm1.mnuToolbar_Hidden.Checked = Not MDIForm1.mnuToolbar_Hidden.Checked
            
        Case "textbottom"
            MDIForm1.mnuToolBar_AlignBottom.Checked = True
            MDIForm1.mnuToolBar_AlignRight.Checked = False
            MDIForm1.barHeader.TextAlignment = tbrTextAlignBottom
            
        Case "textright"
            MDIForm1.mnuToolBar_AlignBottom.Checked = False
            MDIForm1.mnuToolBar_AlignRight.Checked = True
            MDIForm1.barHeader.TextAlignment = tbrTextAlignRight
            
            
    End Select
    
    
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub ShowComboBoxOptions()
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/showcombooptions") Then
        Exit Sub
    End If
    
    frmComboBoxOptions.Show
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Function GetViewID(lp_strViewName As String)
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    GetViewID = GetData("xref", "xrefID", "description", lp_strViewName)
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub ShowCompany(ByVal lp_lngCompanyID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/showcompany") Then
        Exit Sub
    End If
    
   ' If IsFormLoaded("frmCompany") Then
   '     frmCompany.Show
   '     frmCompany.ZOrder 0
   '     Exit Sub
   ' End If
    
    If lp_lngCompanyID <> 0 Then
        
        Dim l_strSQL As String
        
        l_strSQL = "SELECT * FROM company WHERE companyID = " & lp_lngCompanyID
        
        Dim l_rstCompany As New ADODB.Recordset
        Set l_rstCompany = ExecuteSQL(l_strSQL, "")
        
        If Not l_rstCompany.EOF Then
            
            With frmCompany
                .txtDiscount.Text = Trim(" " & l_rstCompany("masterdiscount"))
                .txtCreditLimit.Text = Trim(" " & l_rstCompany("creditlimit"))
                .txtCompanyID.Text = l_rstCompany("companyID")
                .cmbCompany.Text = Trim(" " & l_rstCompany("name"))
                .txtAddress.Text = Trim(" " & l_rstCompany("address"))
                .txtPostCode.Text = Trim(" " & l_rstCompany("postcode"))
                .txtCountry.Text = Trim(" " & l_rstCompany("country"))
                .txtTelephone.Text = Trim(" " & l_rstCompany("telephone"))
                .txtFax.Text = Trim(" " & l_rstCompany("fax"))
                .txtEmail.Text = Trim(" " & l_rstCompany("email"))
                .txtWebsite.Text = Trim(" " & l_rstCompany("website"))
                .txtMarketCode.Text = Trim(" " & l_rstCompany("marketcode"))
                .txtTradeCode.Text = Trim(" " & l_rstCompany("tradecode"))
                .txtComments.Text = Trim(" " & l_rstCompany("notes1"))
                .txtVATCode.Text = Trim(" " & l_rstCompany("vatcode"))
                .txtVATNumber.Text = Trim(" " & l_rstCompany("vatnumber"))
                .txtDownloadCode.Text = Trim(" " & l_rstCompany("portaldownloadcode"))
                .txtUploadCode.Text = Trim(" " & l_rstCompany("portaluploadcode"))
                
                .txtCreatedDate.Text = Format(l_rstCompany("createddate"))
                .txtCreatedBy.Text = Trim(" " & l_rstCompany("createduser"))
                .txtModifiedDate.Text = Format(l_rstCompany("mdate"))
                .txtModifiedBy.Text = Trim(" " & l_rstCompany("muser"))
                
                .txtClientCode.Text = Trim(" " & l_rstCompany("cetaclientcode"))
                .txtAccountCode.Text = Trim(" " & l_rstCompany("accountcode"))
                .txtSalesLedgerCode.Text = Trim(" " & l_rstCompany("salesledgercode"))
                .txtSageCustomerReference.Text = Trim(" " & l_rstCompany("SageCustomerReference"))
                .datAccountStart.Value = Format(l_rstCompany("accountstartdate"))
                .cmbAccountStatus.Text = Trim(" " & l_rstCompany("accountstatus"))
                .datPostedToAccounts.Value = Format(l_rstCompany("postedtoaccountsdate"))
                .cmbRateCard.Text = Trim(" " & l_rstCompany("ratecard"))
                .txtRegisteredOffice.Text = Trim(" " & l_rstCompany("registeredoffice"))
                .txtSource.Text = Trim(" " & l_rstCompany("source"))
                .txtInvoiceCompany.Text = Trim(" " & l_rstCompany("invoicecompanyname"))
                .txtInvoiceAddress.Text = Trim(" " & l_rstCompany("invoiceaddress"))
                .txtInvoicePostCode.Text = Trim(" " & l_rstCompany("invoicepostcode"))
                .txtInvoiceCountry.Text = Trim(" " & l_rstCompany("invoicecountry"))
                .txtInvoiceEmail.Text = Trim(" " & l_rstCompany("invoiceemail"))
                .chkClient.Value = GetFlag(l_rstCompany("iscustomer"))
                .chkSupplier.Value = GetFlag(l_rstCompany("issupplier"))
                .chkProspective.Value = GetFlag(l_rstCompany("isprospective"))
                .chkCPGlobal.Value = GetFlag(l_rstCompany("isCPGlobalCustomer"))
                .chkTVDistribution.Value = GetFlag(l_rstCompany("isTVDistributionTeam"))
                .chkTVStudios.Value = GetFlag(l_rstCompany("isStudiosTeam"))
                .chkTVIndependents.Value = GetFlag(l_rstCompany("isIndependentsTeam"))
                .txtJCAContactEmail.Text = Trim(" " & l_rstCompany("jcacontactemail"))
                .txtProgressRefName.Text = Trim(" " & l_rstCompany("progressrefname"))
                .txtTrackerTitle.Text = Trim(" " & l_rstCompany("generictrackertitle"))
                .txtDTBillingCompanyID.Text = Trim(" " & l_rstCompany("DTBillingCompanyID"))
                .txtAS11BillingCompanyID.Text = Trim(" " & l_rstCompany("AS11BillingCompanyID"))
                .chkLatimerLock.Value = GetFlag(l_rstCompany("LatimerLock"))
                .txtMediaPulseCompanyID.Text = Trim(" " & l_rstCompany("MediaPulseCompanyID"))
                .txtMediaPulseJobID.Text = Trim(" " & l_rstCompany("MediaPulseJobID"))

                .cmbBusinessUnit.Text = Trim(" " & l_rstCompany("BusinessUnit"))
                
                .txtportalpasswordqualitystring.Text = Trim(" " & l_rstCompany("portalpasswordqualitystring"))
                .txtportalpasswordqualityexplanation.Text = Trim(" " & l_rstCompany("portalpasswordqualityexplanation"))
                .txtPasswordLifetime.Text = Val(l_rstCompany("portalpasswordlifetime"))
                
                .txtCostingsAlert.Text = Trim(" " & l_rstCompany("messagecostings"))
                .txtBookingsAlert.Text = Trim(" " & l_rstCompany("messagebookings"))
                
                .txtOfficeClientNotes.Text = Trim(" " & l_rstCompany("officeclientnotes"))
                .txtDespatchNotes.Text = Trim(" " & l_rstCompany("despatchnotes"))
                .txtOperatorNotes.Text = Trim(" " & l_rstCompany("operatornotes"))
                .txtProducerNotes.Text = Trim(" " & l_rstCompany("producernotes"))
                .txtAccountsNotes.Text = Trim(" " & l_rstCompany("accountsnotes"))
                
                .txtlabel1.Text = Trim(" " & l_rstCompany("customfield1label"))
                .txtlabel2.Text = Trim(" " & l_rstCompany("customfield2label"))
                .txtlabel3.Text = Trim(" " & l_rstCompany("customfield3label"))
                .txtlabel4.Text = Trim(" " & l_rstCompany("customfield4label"))
                .txtlabel5.Text = Trim(" " & l_rstCompany("customfield5label"))
                .txtlabel6.Text = Trim(" " & l_rstCompany("customfield6label"))
                .txtHeaderPage.Text = Trim(" " & l_rstCompany("portalheaderpage"))
                .txtFooterPage.Text = Trim(" " & l_rstCompany("portalfooterpage"))
                .txtPortalURL.Text = Trim(" " & l_rstCompany("portalwebsiteurl"))
                .txtMainPage.Text = Trim(" " & l_rstCompany("portalmainpage"))
                .txtEmailSubject.Text = Trim(" " & l_rstCompany("portalemailsubject"))
                .txtEmailBody.Text = Trim(" " & l_rstCompany("portalemailbody"))
                .txtWelcomeMessage.Text = Trim(" " & l_rstCompany("portalwelcomemessage"))
                .txtOrderNumberFieldName.Text = Trim(" " & l_rstCompany("portalordernumberfieldname"))
                .txtPortalCompanyName.Text = Trim(" " & l_rstCompany("portalcompanyname"))
                .txtAdminLink(1).Text = Trim(" " & l_rstCompany("portaladminlink1"))
                .txtAdminLink(2).Text = Trim(" " & l_rstCompany("portaladminlink2"))
                .txtAdminLink(3).Text = Trim(" " & l_rstCompany("portaladminlink3"))
                .txtAdminTitle(1).Text = Trim(" " & l_rstCompany("portaladmintitle1"))
                .txtAdminTitle(2).Text = Trim(" " & l_rstCompany("portaladmintitle2"))
                .txtAdminTitle(3).Text = Trim(" " & l_rstCompany("portaladmintitle3"))
                .txtAdminText(1).Text = Trim(" " & l_rstCompany("portaladmintext1"))
                .txtAdminText(2).Text = Trim(" " & l_rstCompany("portaladmintext2"))
                .txtAdminText(3).Text = Trim(" " & l_rstCompany("portaladmintext3"))
                .txtMX1360CustomerID.Text = Trim(" " & l_rstCompany("MX1360CustomerID"))
                .txtMX1360Username.Text = Trim(" " & l_rstCompany("MX1360Username"))
                .txtMX1360password.Text = Trim(" " & l_rstCompany("MX1360password"))
                .txtMX1360ProxyTranscodeID.Text = Trim(" " & l_rstCompany("MX1360ProxyTranscodeID"))
                
                .chkInactiveCompany.Value = GetFlag(l_rstCompany("system_active"))
                
                .tabDetails_Click 0
                
                .adoPortalTitleStubs.RecordSource = "SELECT * FROM xref WHERE category = 'portaltitlestub' AND information = '" & lp_lngCompanyID & "' ORDER BY description;"
                .adoPortalTitleStubs.ConnectionString = g_strConnection
                .adoPortalTitleStubs.Refresh
                
            End With
        Else
            If lp_lngCompanyID <> -1 Then
                MsgBox "Could not locate company record", vbExclamation
            End If
            ClearFields frmCompany
        End If
        
        l_rstCompany.Close
        Set l_rstCompany = Nothing
        
    Else
        If frmCompany.txtCompanyID.Text <> "" Then
            frmCompany.Show
            frmCompany.ZOrder 0
            Exit Sub
        End If
        ClearFields frmCompany
    End If
    
    l_strSQL = "SELECT company.companyID, contact.name, contact.email, contact.contactID, contact.telephone, employee.jobtitle, employee.employeeID FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE company.companyID = " & lp_lngCompanyID & " AND contact.system_active = 1 ORDER BY contact.name;"
    
    frmCompany.adoContact.ConnectionString = g_strConnection
    frmCompany.adoContact.RecordSource = l_strSQL
    frmCompany.adoContact.Refresh
    
    l_strSQL = "SELECT companycontactID, companyID, name, email FROM companycontact WHERE (((companyID)=" & lp_lngCompanyID & ")) ORDER BY name;"
    
    frmCompany.adoCompanyContact.ConnectionString = g_strConnection
    frmCompany.adoCompanyContact.RecordSource = l_strSQL
    frmCompany.adoCompanyContact.Refresh
    
    l_strSQL = "SELECT * FROM tracker_itemchoice WHERE companyID = " & lp_lngCompanyID & " ORDER BY field_order;"
    
    frmCompany.adoTrackerChoices.ConnectionString = g_strConnection
    frmCompany.adoTrackerChoices.RecordSource = l_strSQL
    frmCompany.adoTrackerChoices.Refresh
    
    l_strSQL = "SELECT * FROM master_tracker_itemchoice WHERE companyID = " & lp_lngCompanyID & ";"
    
    frmCompany.adoMaterialTrackerChoices.ConnectionString = g_strConnection
    frmCompany.adoMaterialTrackerChoices.RecordSource = l_strSQL
    frmCompany.adoMaterialTrackerChoices.Refresh
    
    If frmCompany.txtSource.Text <> "" Then
        l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = '" & frmCompany.txtSource.Text & "' ORDER BY trackermessageID, email;"
        frmCompany.adoTrackerNotifications.RecordSource = l_strSQL
        frmCompany.adoTrackerNotifications.ConnectionString = g_strConnection
        frmCompany.adoTrackerNotifications.Refresh
    Else
        l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = 'NOGROUPSPECIFIED' ORDER BY trackermessageID, email;"
        frmCompany.adoTrackerNotifications.RecordSource = l_strSQL
        frmCompany.adoTrackerNotifications.ConnectionString = g_strConnection
        frmCompany.adoTrackerNotifications.Refresh
    End If
    
    frmCompany.Show
    frmCompany.ZOrder 0
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub ShowContact(ByVal lp_lngContactID As Long, Optional lp_lngCompanyID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/showcontact") Then
        Exit Sub
    End If
    
    If lp_lngContactID <> 0 Then
        
        Dim l_strSQL As String
        
        l_strSQL = "SELECT * FROM contact WHERE contactID = " & lp_lngContactID
        
        Dim l_rstContact As New ADODB.Recordset
        Set l_rstContact = ExecuteSQL(l_strSQL, "")
        
        If Not l_rstContact.EOF Then
            
            With frmContact
                .txtContactID.Text = l_rstContact("contactID")
                .cmbContact.Text = Trim(" " & l_rstContact("name"))
                .txtFirstName.Text = Trim(" " & l_rstContact("firstname"))
                .txtInitials.Text = Trim(" " & l_rstContact("initials"))
                .txtLastName.Text = Trim(" " & l_rstContact("lastname"))
                .cmbTitle.Text = Trim(" " & l_rstContact("title"))
                .txtAddress.Text = Trim(" " & l_rstContact("address"))
                .txtPostCode.Text = Trim(" " & l_rstContact("postcode"))
                .txtCountry.Text = Trim(" " & l_rstContact("country"))
                .txtTelephone.Text = Trim(" " & l_rstContact("telephone"))
                .txtFax.Text = Trim(" " & l_rstContact("fax"))
                .txtEmail.Text = Trim(" " & l_rstContact("email"))
                .txtMobile.Text = Trim(" " & l_rstContact("mobile"))
                .txtComments.Text = Trim(" " & l_rstContact("notes1"))
                .chkFreelancer.Value = GetFlag(l_rstContact("flagfreelance"))
                .chkMailshot.Value = GetFlag(l_rstContact("flagmailshot"))
                .chkInactive.Value = GetFlag(l_rstContact("system_active"))
                
            End With
        Else
            If lp_lngContactID <> -1 Then
                MsgBox "Could not locate contact record", vbExclamation
            End If
            
            ClearFields frmContact
            
            If lp_lngCompanyID <> 0 Then
                frmContact.lblCompanyID.Caption = lp_lngCompanyID
                frmContact.cmdLoadContactsForCompany.Value = True
                
            End If
            
        End If
        
        l_rstContact.Close
        Set l_rstContact = Nothing
        
        l_strSQL = "SELECT employee.employeeID, company.companyID, company.name, employee.jobtitle, employee.username, employee.password, employee.cetacode, employee.usersettings, employee.portaladminlink1, employee.portaladminlink2, employee.portaladminlink3, employee.portaladmintitle1, employee.portaladmintitle2, employee.portaladmintitle3, employee.portaladmintext1, employee.portaladmintext2, employee.portaladmintext3, employee.securitylock, employee.failedlogins, employee.passwordlastreset, employee.forceport33001 FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((contact.contactID)=" & lp_lngContactID & ")) ORDER BY company.name;"
        
        frmContact.adoCompanyAttach.ConnectionString = g_strConnection
        frmContact.adoCompanyAttach.RecordSource = l_strSQL
        frmContact.adoCompanyAttach.Refresh
        
    Else
        If frmContact.txtContactID.Text <> "" Then
            frmContact.Show
            frmContact.ZOrder 0
            Exit Sub
        End If
        
        l_strSQL = "SELECT contact.contactID, company.name as companyname, contact.name, contact.system_active FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID ORDER BY contact.name;"
        
        frmContact.adoContactList.ConnectionString = g_strConnection
        frmContact.adoContactList.RecordSource = l_strSQL
        frmContact.adoContactList.Refresh
        
        ClearFields frmContact
    End If
    
    frmContact.Show
    frmContact.ZOrder 0
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Function GetJobProjectID(lp_lngJobID As Long)

Dim l_lngProjectID As Long

l_lngProjectID = GetData("job", "projectID", "jobID", lp_lngJobID)

GetJobProjectID = l_lngProjectID

End Function


Function GetJobProjectNumber(lp_lngJobID As Long)

Dim l_lngProjectID As Long
Dim l_lngProjectNumber As Long

GetJobProjectNumber = GetData("job", "projectnumber", "jobID", lp_lngJobID)

'

'l_lngProjectID = GetData("job", "projectID", "jobID", lp_lngJobID)'

'If l_lngProjectID <> 0 Then
'    l_lngProjectNumber = GetData("project", "projectnumber", "projectID", l_lngProjectID)
'End If

'GetJobProjectNumber = l_lngProjectNumber

End Function
Function GetJobVideoStandard(lp_lngJobID As Long) As String

Dim l_lngProjectID As Long
l_lngProjectID = GetJobProjectID(lp_lngJobID)

If l_lngProjectID <> 0 Then
    Dim l_strVideoStandard As String
    Dim l_lngQuoteID As Long
    l_lngQuoteID = GetData("project", "quoteID", "projectID", l_lngProjectID)
    If l_lngQuoteID > 0 Then
        l_strVideoStandard = GetData("quote", "videostandard", "quoteID", l_lngQuoteID)
    End If
End If

GetJobVideoStandard = l_strVideoStandard

End Function

Function GetJobDirector(lp_lngJobID As Long) As String

Dim l_lngProjectID As Long
l_lngProjectID = GetJobProjectID(lp_lngJobID)

If l_lngProjectID <> 0 Then
    Dim l_strDirector As String
    Dim l_lngQuoteID As Long
    l_lngQuoteID = GetData("project", "quoteID", "projectID", l_lngProjectID)
    If l_lngQuoteID > 0 Then
        l_strDirector = GetData("quote", "productiondirectorname", "quoteID", l_lngQuoteID)
    End If
End If

GetJobDirector = l_strDirector


End Function

Sub MoveItemUpInView(l_lngViewID As Long, l_lngOrderby As Long, l_lngResourceViewID As Long)

If Not CheckAccess("/moveitemsinviews") Then Exit Sub

Dim l_strSQL As String
l_strSQL = "UPDATE resourceview SET fd_orderby = fd_orderby + 1 WHERE viewID = '" & l_lngViewID & "' AND fd_orderby = '" & l_lngOrderby - 1 & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_strSQL = "UPDATE resourceview SET fd_orderby = fd_orderby - 1 WHERE resourceviewID = '" & l_lngResourceViewID & "' "
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError


End Sub

Sub MoveItemDownInView(l_lngViewID As Long, l_lngOrderby As Long, l_lngResourceViewID As Long)

If Not CheckAccess("/moveitemsinviews") Then Exit Sub

Dim l_strSQL As String
l_strSQL = "UPDATE resourceview SET fd_orderby = fd_orderby - 1 WHERE viewID = '" & l_lngViewID & "' AND fd_orderby = '" & l_lngOrderby + 1 & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_strSQL = "UPDATE resourceview SET fd_orderby = fd_orderby + 1 WHERE resourceviewID = '" & l_lngResourceViewID & "' "
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Sub

Sub ShowJob(ByVal lp_lngJobID As Long, ByVal lp_intTabToShow As Integer, lp_blnSwitchTab As Boolean)
    
Dim counter As Integer

    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/showjobdetail") Then
        Exit Sub
    End If


    If lp_lngJobID = 0 Then
        If Val(frmJob.txtJobID.Text) > 0 Then
            lp_lngJobID = Val(frmJob.txtJobID.Text)
        End If
    End If

    If lp_lngJobID <> 0 Then
    
'        Dim l_intCostingsLocked As Integer
        Dim temptab As Integer
'        l_intCostingsLocked = GetData("job", "costingslocked", "jobID", lp_lngJobID)
        
        Screen.MousePointer = vbHourglass
        
        frmJob.Tag = "LOADING"
        
        If lp_lngJobID <> -1 And lp_lngJobID <> -2 Then
            frmJob.Show
        Else
            frmJob.Hide
        End If
        
'        DoEvents
        
        Dim l_strSQL As String
        
        l_strSQL = "SELECT * FROM job WHERE jobID = " & lp_lngJobID
        
        Dim l_rstJob As New ADODB.Recordset
        Set l_rstJob = ExecuteSQL(l_strSQL, g_strExecuteError)
        
        CheckForSQLError
        
        If Not l_rstJob.EOF Then
            
            l_rstJob.MoveFirst
            
            Dim l_strVideoStandard As String
            Dim l_strDirector As String
            
            l_strVideoStandard = Format(l_rstJob("videostandard"))
            If l_strVideoStandard = "" Then l_strVideoStandard = GetJobVideoStandard(lp_lngJobID)
            l_strDirector = GetJobDirector(lp_lngJobID)
            
            Dim l_strJobType As String
            l_strJobType = Format(l_rstJob("jobtype"))
            
            Dim l_datInvoicedDate As Variant
            l_datInvoicedDate = l_rstJob("invoiceddate")
            
            With frmJob
            
                Dim l_blnShowTab As Boolean
                l_blnShowTab = CheckAccess("/costjob", True)
                
                .tabReplacementTabs.TabVisible(5) = l_blnShowTab
                
                
                l_blnShowTab = CheckAccess("/viewinvoicetab", True)
                .tabReplacementTabs.TabVisible(6) = l_blnShowTab
            
                l_blnShowTab = CheckAccess("/showjobpurchaseorders", True)
                .tabReplacementTabs.TabVisible(4) = l_blnShowTab
                
                l_blnShowTab = CheckAccess("/viewjobmedia", True)
                .tabReplacementTabs.TabVisible(2) = l_blnShowTab
                
                If Not IsNull((l_rstJob("invoiceddate"))) Then
                    .datInvoiceDate.Value = Format(l_rstJob("invoiceddate"))
                Else
                    .datInvoiceDate.Value = Null
                End If
                    
         
                .chkNoVAT.Value = Val(Trim(" " & l_rstJob("donotchargevat")))
                
                'new delivery note number fields
                .txtDeliveryDNoteNumber.Text = GetData("despatch", "despatchnumber", "despatchID", Format(l_rstJob("deliverydnotenumber")))
                .txtCollectionDNoteNumber.Text = GetData("despatch", "despatchnumber", "despatchID", Format(l_rstJob("collectiondnotenumber")))
                
                l_blnShowTab = CheckAccess("/viewjobprojecttab", True)
                .tabReplacementTabs.TabVisible(9) = l_blnShowTab
                
                
                .txtCreated.Text = Format(l_rstJob("createddate"), "DD/MM/YYYY HH:NN") & " by " & l_rstJob("createduser")
                .txtVTStartedBy.Text = l_rstJob("vtstartdate") & " by " & l_rstJob("vtstartuser")
                If .txtVTStartedBy.Text <> "" Then
                    .txtVTStartedBy.BackColor = vbLightGreen
                Else
                    .txtVTStartedBy.BackColor = vbButtonFace
                End If
                
                .cmbVideoStandard.Text = l_strVideoStandard
                
                .txtJobID.Text = l_rstJob("jobID")
                
                '.tab1.Tab = g_intDefaultTab
                
                .txtAccountCode.Text = GetData("company", "accountcode", "companyID", l_rstJob("companyID"))
                .txtClientStatus.Text = GetData("company", "accountstatus", "companyID", l_rstJob("companyID"))
                
                If UCase(.txtClientStatus.Text) = "HOLD" Or UCase(.txtClientStatus.Text) = "CASH" Then
                    .txtClientStatus.BackColor = vbRed
                Else
                    .txtClientStatus.BackColor = vbWindowBackground
                End If
                
'                DoEvents
                
                'printed details
                .lblTotalPrints.Caption = Format(l_rstJob("totalprints"))
                .lblPrintedBy.Caption = Format(l_rstJob("lastprintuser"))
                
                .lblSentToAccountsDate(0).Caption = Format(l_rstJob("senttoaccountsdate"))
                .lblSentToAccountsBatchNumber.Caption = Format(l_rstJob("senttoaccountsbatchnumber"))
                
                .txtMasterJobID.Text = Format(l_rstJob("masterjobID"))
                .txtOrderReference.Text = Trim(" " & l_rstJob("orderreference"))
                .txtClientContractReference.Text = Trim(" " & l_rstJob("customercontractreference"))
                .txtAllocatedTo.Text = Trim(" " & l_rstJob("allocatedto"))
                .txtSeriesID.Text = Trim(" " & l_rstJob("seriesID"))
                .txtTitle.Text = Format(l_rstJob("title1"))
                .txtSubtitle.Text = Format(l_rstJob("title2"))
                .txtTimeRequired = Trim(" " & l_rstJob("operatorduration"))
                .cmbCompany.Text = Format(l_rstJob("companyname"))
                .cmbContact.Text = Format(l_rstJob("contactname"))
                .txtTelephone.Text = Format(l_rstJob("telephone"))
                .txtFax.Text = Format(l_rstJob("fax"))
                .txtNotificationSubjectLine.Text = Trim(" " & l_rstJob("EmailNotificationSubjectLine"))
                .txtNotificationMessageBody.Text = Trim(" " & l_rstJob("EmailNotificationMessageBody"))
                
                .cmbIncomingMethod.Text = Format(l_rstJob("incomingdespatchmethod"))
                .cmbOutgoingMethod.Text = Format(l_rstJob("outgoingdespatchmethod"))
                
                .cmbProject.Text = Format(l_rstJob("projectnumber"))
                If Val(.cmbProject.Text) = 0 Then .cmbProject.Text = ""
                .cmbOurContact.Text = Format(l_rstJob("ourcontact"))
                
                .lblProjectID.Caption = Format(l_rstJob("projectID"))
                
                .datDeadlineDate.Value = Format(l_rstJob("deadlinedate"), vbShortDateFormat)
                .cmbDeadlineTime.Text = Format(l_rstJob("deadlinedate"), vbTimeFormat)
                                
                .cmbDealType.Text = Format(l_rstJob("dealtype"))
                .txtDealPrice.Text = Format(l_rstJob("dealprice"))
                '.cmbDealUnit.Text = Format(l_rstJob("dealunit"))
                
'                'fix the deal price...
'                If Not CheckAccess("/changedealpriceonjob", True) Then
'                    .picMovable1.Enabled = False
'                Else
'                    .picMovable1.Enabled = True
'                End If
'
'                'fix the deal price...
'                If Not CheckAccess("/viewdealpriceonjob", True) Then
'                    .picMovable1.Visible = False
'                Else
'                    .picMovable1.Visible = True
'                End If
'
'                DoEvents
                
                'enable the change status button
                .cmdLaunch(5).Enabled = True
                
                'new fields for scheduling (mpc stuff)
                .cmbProduct.Text = Format(l_rstJob("productname"))
                .lblProductID.Caption = Format(l_rstJob("productID"))
                
                .chkJobDetailOnInvoice.Value = GetFlag(l_rstJob("flagdetailoninvoice"))
                .chkSendEmailOnCompletion.Value = GetFlag(l_rstJob("flagemail"))
                .chkQuote.Value = GetFlag(l_rstJob("flagquote"))
                .chkOnHold.Value = GetFlag(l_rstJob("flaghold"))
                .chkUrgent.Value = GetFlag(l_rstJob("flagurgent"))
                .chkDoWholeSeries.Value = GetFlag(l_rstJob("dowholeseries"))
                .chkDoNotDeliver.Value = GetFlag(l_rstJob("donotdeliveryet"))
                If GetFlag(l_rstJob("redojob")) <> 0 Then
                    .lblRedo.Visible = True
                Else
                    .lblRedo.Visible = False
                End If
                .chkNoChargeJob.Value = GetFlag(l_rstJob("nochargejob"))
                
                '.Caption = "Job Details - [Showing Job: " & lp_lngJobID & "]"
                
                .lblCompanyID.Caption = Format(l_rstJob("companyID"))
                .lblContactID.Caption = Format(l_rstJob("contactID"))
                
                .cmbAllocation.Text = Format(l_rstJob!joballocation)
                .cmbAllocation_Click
                
                .lblInvoiceNumber.Caption = Format(l_rstJob("invoicenumber"))
                .lblCreditNoteNumber.Caption = Format(l_rstJob("creditnotenumber"))
                
                .txtOfficeClient.Text = Trim(" " & l_rstJob("notes1"))
                .txtOperator.Text = Trim(" " & l_rstJob("notes2"))
                .txtDespatch.Text = Trim(" " & l_rstJob("notes3"))
                .txtAccounts.Text = Trim(" " & l_rstJob("notes4"))
                .txtProducerNotes.Text = Trim(" " & l_rstJob("notes5"))
                
                If .txtOfficeClient.Text & .txtOperator.Text & .txtDespatch.Text & .txtProducerNotes.Text <> "" Then
                    .tabReplacementTabs.TabCaption(3) = "Notes*"
                Else
                    .tabReplacementTabs.TabCaption(3) = "Notes"
                End If
                
'                DoEvents
                
                'show the job status
                .txtStatus.Text = Trim(" " & l_rstJob("fd_status"))
                'make sure it is the correct colour
                .txtStatus.BackColor = CLng(GetResourceBookingColour(CStr(.txtStatus.Text)))
                
                'Make the VT Operations button do the right thing.
                If UCase(.txtStatus.Text) = "CONFIRMED" Or UCase(.txtStatus.Text) = "MASTERS HERE" Then
                    .cmdOperations.Caption = "Start"
                    .cmdOperations.Visible = True
                ElseIf UCase(.txtStatus.Text) = "IN PROGRESS" Then
                    .cmdOperations.Caption = "Done"
                    .cmdOperations.Visible = True
                Else
                    .cmdOperations.Visible = False
                End If
               
                .txtCostingSheet.Text = Format(l_rstJob("costingsheetID"))
                
                'if there is a costing sheet ID then disable the text box
                .txtCostingSheet.Enabled = IIf(Val(.txtCostingSheet.Text) = 0, True, False)
                
                Dim l_intTwoTierPricing As Integer
                l_intTwoTierPricing = InStr(GetData("company", "cetaclientcode", "companyID", l_rstJob("companyID")), "/two-tier-pricing")
                
                If l_intTwoTierPricing <> 0 Then
                    .lblTwoTier.Caption = "Two-Tier Costing Enabled"
                Else
                    .lblTwoTier.Caption = ""
                End If
                
                .cmbJobType.Text = Format(l_rstJob("jobtype"))
                .lblJobID.Caption = lp_lngJobID
                
                
        
                .cmbDeliveryCompany.Text = Format(l_rstJob.Fields("deliverycompanyname"))
                .cmbDeliveryContact.Text = Format(l_rstJob.Fields("deliverycontactname"))
                .txtDeliveryAddress.Text = Format(l_rstJob.Fields("deliveryaddress"))
                .txtDeliveryPostCode.Text = Format(l_rstJob.Fields("deliverypostcode"))
                .txtDeliveryCountry.Text = Format(l_rstJob.Fields("deliverycountry"))
                .txtDeliveryTelephone.Text = Format(l_rstJob.Fields("deliverytelephone"))
                
                .cmbCollectionCompany.Text = Format(l_rstJob.Fields("collectioncompanyname"))
                .cmbCollectionContact.Text = Format(l_rstJob.Fields("collectioncontactname"))
                .txtCollectionAddress.Text = Format(l_rstJob.Fields("collectionaddress"))
                .txtCollectionPostCode.Text = Format(l_rstJob.Fields("collectionpostcode"))
                .txtCollectionCountry.Text = Format(l_rstJob.Fields("collectioncountry"))
                .txtCollectionTelephone.Text = Format(l_rstJob.Fields("collectiontelephone"))
                
                .txtDADCLegacyOracTVAENumber = Trim(" " & l_rstJob("DADCLegacyOracTVAENumber"))
                .txtDADCSeriesID = Trim(" " & l_rstJob("DADCSeries"))
                
'                .txtDADCEpisodeNumber.Text = Trim(" " & l_rstJob.Fields("DADCEpisodeNumber"))
'                .txtDADCEpisodeTitle.Text = Trim(" " & l_rstJob.Fields("DADCEpisodeTitle"))
'                .txtDADCSeries.Text = Trim(" " & l_rstJob.Fields("DADCSeries"))
'                .txtDADCProjectNumber.Text = Trim(" " & l_rstJob.Fields("DADCProjectNumber"))
'                .txtDADCProjectManager.Text = Trim(" " & l_rstJob.Fields("DADCProjectManager"))
'                .txtDADCRightsOwner.Text = Trim(" " & l_rstJob.Fields("DADCRightsOwner"))
'                .txtDADCJobTitle.Text = Trim(" " & l_rstJob.Fields("DADCJobTitle"))
                '.chkSvenskTrackerInvoice.Value = GetFlag(l_rstJob.Fields("DADCSvenskTrackerInvoice"))
                
                If Format(l_rstJob("nochargerequestedcode")) <> "" Then
                    .cmdNoCharge.FontBold = True
                Else
                    .cmdNoCharge.FontBold = False
                End If
                
'                DoEvents
                
                'do the two new invoice text fields.
                .txtInvoiceNotes.Text = Trim(" " & l_rstJob("invoicenotes"))
                .txtInvoiceText.Text = Trim(" " & l_rstJob("invoicetext"))
            
                l_strSQL = "SELECT cdate, cuser, description FROM jobhistory WHERE jobID = " & lp_lngJobID & " ORDER BY jobhistoryID DESC"
            
                frmJob.adoHistory.ConnectionString = g_strConnection
                frmJob.adoHistory.RecordSource = l_strSQL
                frmJob.adoHistory.Refresh
                
                temptab = .tab1.Tab
                .tab1.Tab = 2
'                DoEvents
                .tab1.Tab = temptab
                
            End With

            
        Else
            If lp_lngJobID <> -1 And lp_lngJobID <> -2 Then
                MsgBox "Could not locate job record", vbExclamation
            End If
            'disable the change status button
            frmJob.cmdLaunch(5).Enabled = True
            ClearFields frmJob
                        
            frmJob.lblJobID.Caption = ""
            
            'frmJob.tab1.Tab = g_intDefaultTab
            'frmJob.tabReplacementTabs.Tab = GetReplacementTab(g_intDefaultTab)
            
            frmJob.cmbAllocation.Enabled = True
            frmJob.cmbAllocation.Text = g_strDefaultJobAllocation
            
            Dim l_strDefault As String
            
            l_strDefault = GetData("cetauser", "defaultjobtype", "cetauserID", g_lngUserID)
            If l_strDefault <> "" Then
                frmJob.cmbJobType.Text = l_strDefault
            Else
                frmJob.cmbJobType.Text = g_strDefaultJobType
            End If
            
            l_strDefault = GetData("cetauser", "defaultallocation", "cetauserID", g_lngUserID)
            If l_strDefault <> "" Then
                frmJob.cmbAllocation.Text = l_strDefault
            Else
                frmJob.cmbAllocation.Text = g_strDefaultJobAllocation
            End If
            
            
            If g_optUserIsNotDefaultOurContact = 1 Then
                frmJob.cmbOurContact.Text = g_strFullUserName
                If Not frmJob.cmbOurContact.IsItemInList Then frmJob.cmbOurContact.Text = ""
            End If

            
            frmJob.lblJobID.Caption = ""
            
            If GetStatusNumber(frmJob.txtStatus.Text) < 3 Then frmJob.cmbAllocation_Click
            
        End If
        
        l_rstJob.Close
        Set l_rstJob = Nothing
        
    Else
    
        ClearFields frmJob
    
        frmJob.cmbOurContact.DroppedDown = True
        frmJob.cmbOurContact.DroppedDown = False
        frmJob.cmbOurContact.Text = g_strFullUserName
        frmJob.cmdOperations.Visible = False
        
        If Not frmJob.cmbOurContact.IsItemInList Then frmJob.cmbOurContact.Text = ""
        frmJob.tab1.Tab = 0

    
        If frmJob.Visible = True Then
            
            If frmJob.txtJobID.Text = "" Then
            
                frmJob.lblJobID.Caption = ""
                'frmJob.tab1.Tab = lp_intTabToShow
                'frmJob.tabReplacementTabs.Tab = GetReplacementTab(lp_intTabToShow)
                frmJob.cmbAllocation.Enabled = True
                
                
                l_strDefault = GetData("cetauser", "defaultallocation", "cetauserID", g_lngUserID)
                If l_strDefault <> "" Then
                    frmJob.cmbAllocation.Text = l_strDefault
                Else
                    frmJob.cmbAllocation.Text = g_strDefaultJobAllocation
                End If
                
                l_strDefault = GetData("cetauser", "defaultjobtype", "cetauserID", g_lngUserID)
                
                If l_strDefault <> "" Then
                    frmJob.cmbJobType.Text = l_strDefault
                Else
                    frmJob.cmbJobType.Text = g_strDefaultJobType
                End If
                
                If UCase(frmJob.cmbJobType) = "HIRE" Then
                    frmJob.txtTitle.Text = "HIRE - TBC"
                End If

                frmJob.cmbAllocation_Click
            End If
            
            frmJob.Show
            frmJob.ZOrder 0
            
            GoTo Proc_Hide_Core_Functions
            
            Exit Sub
            
            'disable the change status button
            frmJob.cmdLaunch(5).Enabled = False
            
            'frmJob.Caption = "Job Details"
            
            frmJob.lblJobID.Caption = ""
            frmJob.datDeadlineDate.Value = Null
            frmJob.cmbAllocation_Click
        
        End If
        

    End If
    
'    DoEvents
    
    'disable / enable the controls
    With frmJob
        If UCase(.txtStatus.Text) = "COSTED" Or UCase(.txtStatus.Text) = "SENT TO ACCOUNTS" Or UCase(.txtStatus.Text) = "HOLD COST" Or UCase(.txtStatus.Text) = "CANCELLED" _
            Or UCase(.txtStatus.Text) = "NO CHARGE" Then
            
'            With .grdDetail
'                .AllowAddNew = False
'                .AllowDelete = False
'                .AllowUpdate = False
'            End With
'
'            With .grdCostingDetail
'                .AllowAddNew = False
'                .AllowDelete = False
'                .AllowUpdate = False
'            End With

            .grdDetail.AllowAddNew = False
            .grdDetail.AllowDelete = False
            .grdDetail.AllowUpdate = False

            .grdCostingDetail.AllowAddNew = False
            .grdCostingDetail.AllowDelete = False
            .grdCostingDetail.AllowUpdate = False
            
            .cmdReCost.Enabled = False
            .grdSundryCosts.Enabled = False
            
'            If g_intWebInvoicing = 1 Then
'                .cmdReinstate.Enabled = False
'                .cmdCreditNote.Enabled = False
'            Else
            If UCase(.txtStatus.Text) = "COSTED" Or UCase(.txtStatus.Text) = "SENT TO ACCOUNTS" Then
                Select Case .txtStatus.Text
                    Case "Sent To Accounts"
                        .cmdReinstate.Enabled = False
                        .cmdCreditNote.Enabled = True
                    Case "Costed"
                        .cmdCreditNote.Enabled = False
                        If Not CheckAccess("/reinstatejob", True) Then
                            .cmdReinstate.Enabled = False
                        Else
                            .cmdReinstate.Enabled = True
                        End If
                End Select
            Else
                .cmdReinstate.Enabled = True
            End If
            
'            If UCase(.txtStatus.Text) = "COSTED" Or UCase(.txtStatus.Text) = "SENT TO ACCOUNTS" Or UCase(.txtStatus.Text) = "CANCELLED" Or UCase(.txtStatus.Text) = "NO CHARGE" Or g_intWebInvoicing = 1 Then
            If UCase(.txtStatus.Text) = "COSTED" Or UCase(.txtStatus.Text) = "SENT TO ACCOUNTS" Or UCase(.txtStatus.Text) = "CANCELLED" Or UCase(.txtStatus.Text) = "NO CHARGE" Then
                .cmdInvoice.Enabled = False
                .cmdNoCharge.Enabled = False
                .cmdMarkFaulty.Enabled = True
            Else
                .cmdInvoice.Enabled = True
                .cmdNoCharge.Enabled = True
                .cmdMarkFaulty.Enabled = False
            End If
            
            .cmdSave.Enabled = False
            .cmdCancelJob.Enabled = False
            
            'If .txtStatus.Text <> "Hold Cost" Then .cmdMinimumCharge.Enabled = False
            
            .cmdMoveUp.Enabled = False
            .cmdMoveDown.Enabled = False
            .cmdDuplicateRow.Enabled = False
            
            
            If .txtStatus.Text <> "Hold Cost" Then
                .cmdReplaceDespatch.Enabled = False
                .cmdReplaceOffice.Enabled = False
                .cmdReplaceOperator.Enabled = False
                .cmdReplaceProducerNotes.Enabled = False
            End If
            
            .cmdAddLibraryEvent(0).Enabled = False
            .cmdAddLibraryEvent(1).Enabled = False
            .datDeadlineDate.Enabled = False
            .cmbDeadlineTime.Enabled = False
            .cmdAddDespatchNotes.Enabled = False
'            DoEvents
            
        Else
            
'            If Val(GetDataSQL("SELECT accountstransactionID FROM costing WHERE jobID = " & lp_lngJobID & " AND accountstransactionID IS NOT NULL AND accountstransactionID <> 0;")) <> 0 Then l_intCostingsLocked = 1
'            If l_intCostingsLocked <> 0 Then
                
'                With .grdDetail
'                .AllowAddNew = False
'                .AllowDelete = False
'                .AllowUpdate = False
'                End With
'                .grdDetail.AllowAddNew = False
'                .grdDetail.AllowDelete = False
'                .grdDetail.AllowUpdate = False
'
'                .grdCostingDetail.AllowAddNew = False
'                .grdCostingDetail.AllowDelete = False
'                .grdCostingDetail.AllowUpdate = False
'
'            Else
'                With .grdDetail
'                    .AllowAddNew = True
'                    .AllowDelete = True
'                    .AllowUpdate = True
'                End With
                .grdDetail.AllowAddNew = True
                .grdDetail.AllowDelete = True
                .grdDetail.AllowUpdate = True
            
                .grdCostingDetail.AllowAddNew = True
                .grdCostingDetail.AllowDelete = True
                .grdCostingDetail.AllowUpdate = True
                
'            End If
           
            
'            If (UCase(l_strJobType) = "HIRE" And Not IsNull(l_datInvoicedDate)) Then ' Or l_intCostingsLocked <> 0
'                With .grdCostingDetail
'                    .AllowAddNew = False
'                    .AllowDelete = False
'                    .AllowUpdate = False
'                End With
'                .grdCostingDetail.Enabled = False
'            Else
'SkipCostingGrid:
'                With .grdCostingDetail
'                    .AllowAddNew = True
'                    .AllowDelete = True
'                    .AllowUpdate = True
'                End With
'                .grdCostingDetail.Enabled = True
'            End If
            
'            If g_intWebInvoicing = 1 Then
'                .cmdReinstate.Enabled = False
'                .cmdInvoice.Enabled = False
'                .cmdNoCharge.Enabled = False
'            Else
                .cmdReinstate.Enabled = True
                .cmdInvoice.Enabled = True
                .cmdNoCharge.Enabled = True
'            End If
            .cmdMarkFaulty.Enabled = False
            .cmdSave.Enabled = True
            .cmdCancelJob.Enabled = True
            '.cmdMinimumCharge.Enabled = True
            .cmdReCost.Enabled = True
            .cmdMoveUp.Enabled = True
            .cmdMoveDown.Enabled = True
            .cmdDuplicateRow.Enabled = True
            
            '.grdMedia.Enabled = True
            .grdSundryCosts.Enabled = True
            .cmdReplaceDespatch.Enabled = True
            .cmdReplaceOffice.Enabled = True
            .cmdReplaceOperator.Enabled = True
            .cmdReplaceProducerNotes.Enabled = True
            
            .cmdAddLibraryEvent(0).Enabled = True
            .cmdAddLibraryEvent(1).Enabled = True
            .datDeadlineDate.Enabled = True
            .cmbDeadlineTime.Enabled = True
            .cmdAddDespatchNotes.Enabled = True
            .cmdCreditNote.Enabled = False
'            DoEvents
        End If
        
        
'        If g_intWebInvoicing = 1 Then
'            If GetDataSQL("SELECT COUNT(costingID) FROM costing WHERE accountstransactionID <> 0 AND jobID = " & lp_lngJobID & ";") < GetDataSQL("SELECT COUNT(costingID) FROM costing WHERE jobID = " & lp_lngJobID & ";") Then
'                'Not all costing lines are on invoices under the new system...so we need to switch costing editing and detail editing back on for the itemns that are not yet invoiced...somehow...
'                .grdCostingDetail.Enabled = True
'                .grdDetail.Enabled = True
'            End If
'        End If
        
        g_optLockSystem = GetCETASetting("LockSystem")
        If (g_optLockSystem And 8) = 8 Then
            .cmdInvoice.Enabled = False
            .cmdCreditNote.Enabled = False
        End If
        
    End With
        
    '######################
    
    If Val(frmJob.lblCompanyID.Caption) = 570 Then
        Dim l_rstExt As ADODB.Recordset
        Set l_rstExt = ExecuteSQL("SELECT * from extendedjob where jobID = " & lp_lngJobID, g_strExecuteError)
        CheckForSQLError
        If l_rstExt.RecordCount <> 0 Then
            l_rstExt.MoveFirst
            With frmJob
                .txtBBCBookingNumber.Text = Trim(" " & l_rstExt("bbcbookingnumber"))
                .txtBBCBusinessArea.Text = Trim(" " & l_rstExt("bbcbusinessarea"))
                .txtBBCBusinessAreaCode.Text = Trim(" " & l_rstExt("bbcbusinessareacode"))
                .txtBBCChargeCode.Text = Trim(" " & l_rstExt("bbcchargecode"))
                .txtBBCCountryCode.Text = Trim(" " & l_rstExt("bbccountrycode"))
                .txtBBCCustomername.Text = Trim(" " & l_rstExt("bbccustomername"))
                .txtBBCNominalCode.Text = Trim(" " & l_rstExt("bbcnominalcode"))
                
                .adoExtendedJobDetail.ConnectionString = g_strConnection
                .adoExtendedJobDetail.RecordSource = "SELECT * from extendedjobdetail INNER JOIN jobdetail ON extendedjobdetail.jobdetailID = jobdetail.jobdetailID WHERE jobdetail.jobid = '" & lp_lngJobID & "';"
                .adoExtendedJobDetail.Refresh
            End With
        End If
        l_rstExt.Close
        Set l_rstExt = Nothing
    
    End If
                        
    If InStr(GetData("company", "cetaclientcode", "companyID", Val(frmJob.lblCompanyID.Caption)), "/BillThroughMP") > 0 Then
        frmJob.cmdInvoice.Caption = "Send To MP"
    Else
        frmJob.cmdInvoice.Caption = "Finalise"
    End If

'    DoEvents
    
    '########################################################################################
    
    l_strSQL = "SELECT * FROM jobdetail WHERE jobID = " & lp_lngJobID & " ORDER BY fd_orderby, jobdetailID"
    
    frmJob.adoJobDetail.ConnectionString = g_strConnection
    frmJob.adoJobDetail.RecordSource = l_strSQL
    frmJob.adoJobDetail.Refresh
    
    If frmJob.lblCompanyID.Caption <> "" Then
        l_strSQL = "SELECT * from jobdetailunbilled WHERE companyID = " & Val(frmJob.lblCompanyID.Caption) & " ORDER BY completeddate;"
        frmJob.adoUnbilled.RecordSource = l_strSQL
        frmJob.adoUnbilled.ConnectionString = g_strConnection
        frmJob.adoUnbilled.Refresh
        
        l_strSQL = "SELECT * from progressitem WHERE companyID = " & Val(frmJob.lblCompanyID.Caption) & " AND finished <> 0 and (billed is null or billed = 0);"
        frmJob.adoProgress.RecordSource = l_strSQL
        frmJob.adoProgress.ConnectionString = g_strConnection
        frmJob.adoProgress.Refresh
    End If
    
    frmJob.adoJobContact.ConnectionString = g_strConnection
    frmJob.adoJobContact.RecordSource = "SELECT * FROM jobcontact WHERE jobID = " & lp_lngJobID & " ORDER BY name;"
    frmJob.adoJobContact.Refresh
    
    frmJob.adoCompanyContact.ConnectionString = g_strConnection
    frmJob.adoCompanyContact.RecordSource = "SELECT * FROM companycontact WHERE companyID = " & Val(frmJob.lblCompanyID.Caption) & " ORDER BY name;"
    frmJob.adoCompanyContact.Refresh
    
    'Set the Inclusive and Other drop downs to use items from the COmpany if there are any
    Dim l_strGroupCode As String
    l_strGroupCode = GetData("company", "source", "companyID", Val(frmJob.lblCompanyID.Caption))
    If l_strGroupCode <> "" Then
        l_strSQL = "SELECT description, format FROM xref WHERE category = 'inclusive' AND descriptionalias = '" & l_strGroupCode & "' ORDER BY forder, format"
        frmJob.adoInclusive.ConnectionString = g_strConnection
        frmJob.adoInclusive.RecordSource = l_strSQL
        frmJob.adoInclusive.Refresh
        
        frmJob.ddnOthers.RemoveAll
        
        PopulateCombo "other", frmJob.ddnOthers, " AND descriptionalias = '" & l_strGroupCode & "'"
        
        If l_strGroupCode = "DADC" Then
            frmJob.txtDADCSeriesID.Visible = True
            frmJob.txtDADCLegacyOracTVAENumber.Visible = True
            frmJob.lblCaption(37).Visible = True
            frmJob.lblCaption(38).Visible = True
        Else
            frmJob.txtDADCSeriesID.Visible = False
            frmJob.txtDADCLegacyOracTVAENumber.Visible = False
            frmJob.lblCaption(37).Visible = False
            frmJob.lblCaption(38).Visible = False
        End If
    
    Else
    
        l_strSQL = "SELECT description, format FROM xref WHERE category = 'inclusive' AND (descriptionalias IS NULL OR descriptionalias = '" & l_strGroupCode & "') ORDER BY forder, format"
        frmJob.adoInclusive.ConnectionString = g_strConnection
        frmJob.adoInclusive.RecordSource = l_strSQL
        frmJob.adoInclusive.Refresh
        
        frmJob.ddnOthers.RemoveAll
        
        PopulateCombo "other", frmJob.ddnOthers, " AND (descriptionalias IS NULL OR descriptionalias = '" & l_strGroupCode & "')"
    
        frmJob.txtDADCSeriesID.Visible = False
        frmJob.txtDADCLegacyOracTVAENumber.Visible = False
        frmJob.lblCaption(37).Visible = False
        frmJob.lblCaption(38).Visible = False
    
    End If
        
    If frmJob.grdDetail.AllowUpdate = True Then
        'update the order by if not already correct
        With frmJob.adoJobDetail.Recordset
            If .RecordCount > 0 Then
                Dim l_intOrderBy As Integer
                .MoveFirst
                If .Fields("fd_orderby") <> "0" Then
                    Do While Not .EOF
                        .Fields("fd_orderby") = l_intOrderBy
                        .Update
                        .MoveNext
                        l_intOrderBy = l_intOrderBy + 1
                    Loop
                    .MoveFirst
                End If
            End If
        End With
    End If
    
    'this stops all tapes being displayed when no job is loaded
    If lp_lngJobID = 0 Then
        lp_lngJobID = -1
    End If
    
'    DoEvents
    
    l_strSQL = "SELECT * FROM resourceschedule WHERE jobID = " & lp_lngJobID & " ORDER BY resourcescheduleID"
    
    frmJob.adoResourceSchedule.ConnectionString = g_strConnection
    frmJob.adoResourceSchedule.RecordSource = l_strSQL
    frmJob.adoResourceSchedule.Refresh
    
    'l_strSQL = "SELECT * FROM costing WHERE ((creditnotenumber IS NULL) OR (creditnotenumber = 0)) AND (jobID = '" & lp_lngJobID & "') ORDER BY ratecardorderby, costingID"
    
    'frmJob.adoCosting.ConnectionString = g_strConnection
    'frmJob.adoCosting.RecordSource = l_strSQL
    'frmJob.adoCosting.Refresh
    
'    DoEvents
    
    

    
    '########################################################################################
    
    RefreshJobHistory lp_lngJobID
    
    '########################################################################################
    
    Screen.MousePointer = vbDefault
    
    'If frmJob.txtJobID.Text <> "" Then
    '    frmJob.tab1.Tab = lp_intTabToShow
    'End If
    
    frmJob.cmbJobType_Click
    
    Dim l_strUserDefaultTab As String
    l_strUserDefaultTab = GetData("cetauser", "defaultjobtab", "initials", g_strUserInitials)
    If l_strUserDefaultTab = "" Then l_strUserDefaultTab = "Schedule"
    
    If Left(UCase(frmJob.cmbJobType.Text), 3) = "DUB" Then l_strUserDefaultTab = "Dubbing"
    
    Select Case l_strUserDefaultTab
    Case "Schedule"
        lp_intTabToShow = 7
    Case "Dubbing"
        lp_intTabToShow = 0
    Case "Media"
        lp_intTabToShow = 2
    Case "Costing"
        lp_intTabToShow = 1
    End Select
    
    
    'frmJob.cmbJobType_Click
    
    frmJob.cmdLaunch(5).Enabled = GetFlag(CheckAccess("/changejobstatus", True))
    
    If CheckAccess("/jobsaveenabled", True) Then frmJob.cmdSave.Enabled = True
    
'    DoEvents
    
    If lp_lngJobID <> -2 Then
        frmJob.Show
        frmJob.adoJobDetail.Caption = frmJob.adoJobDetail.Recordset.RecordCount & " Job Lines"
        frmJob.cmbAllocation_Click
        If GetStatusNumber(frmJob.txtStatus.Text) < 2 Then frmJob.cmbJobType_Click
        frmJob.ZOrder 0
    
        CenterForm frmJob
    End If
    
    If lp_blnSwitchTab = True Then
        frmJob.tab1.Tab = lp_intTabToShow
        If frmJob.tabReplacementTabs.Visible = True Then frmJob.tabReplacementTabs.Tab = GetReplacementTab(lp_intTabToShow)
    End If
    
    frmJob.Tag = ""
    
    GoTo Proc_Hide_Core_Functions

Exit Sub

Proc_Hide_Core_Functions:

    'core functionality disable
    If g_intDisablePurchaseOrders = 1 Then
        frmJob.tabReplacementTabs.TabVisible(4) = False
    End If
    
    If g_intDisableSundry = 1 Then
        frmJob.tabReplacementTabs.TabVisible(7) = False
    End If
    
    If g_intDisableSchedule = 1 Then
        frmJob.tabReplacementTabs.TabVisible(0) = False
    End If
    
    If g_intDisableProjects = 1 Then
        'frmJob.tabReplacementTabs.TabVisible(9) = False
        frmJob.lblCaption(33).Visible = False
        frmJob.cmbProject.Visible = False
        frmJob.cmbAllocation.Visible = False
        frmJob.lblCaption(49).Visible = False
    End If
    
    If g_intDisableProducts = 1 Then
        frmJob.cmbProduct.Visible = False
        frmJob.lblCaption(46).Visible = False
    End If
    
    If g_optUseClipID = 0 Then
        frmJob.grdDetail.Columns("clipID").Visible = False
        frmJob.grdDetail.Columns("emailto").Visible = False
    End If
    
    If g_intDisableDetailedDubbingCompletion = 1 Then
        frmJob.grdDetail.Columns("completeddate").Visible = False
        frmJob.grdDetail.Columns("completeduser").Visible = False
    End If

    If g_optHideSoundChannelsOnVTDetailGrid = 1 Then
        frmJob.grdDetail.Columns("ch1").Visible = False
        frmJob.grdDetail.Columns("ch2").Visible = False
        frmJob.grdDetail.Columns("ch3").Visible = False
        frmJob.grdDetail.Columns("ch4").Visible = False
    End If
    
    
    If g_intDisableProducts = 1 And g_intDisableProjects = 1 Then
        frmJob.lblCaption(8).Top = frmJob.lblCaption(49).Top
        frmJob.txtTitle.Top = frmJob.cmbAllocation.Top
        frmJob.lblCaption(9).Top = frmJob.lblCaption(33).Top
        frmJob.txtSubtitle.Top = frmJob.cmbProject.Top
    End If
    
    If g_intDisableOurContact = 1 Then
        frmJob.cmbOurContact.Visible = False
        frmJob.lblCaption(12).Visible = False
    End If
    
    If g_intDisableJobVideoStandard = 1 Then
        frmJob.cmbVideoStandard.Visible = False
        frmJob.lblCaption(44).Visible = False
    End If

    If g_optHideHireDespatchTab = 1 And g_intDisableJobVideoStandard = 1 And g_intDisableOurContact = 1 Then
    '    frmJob.picMovable1.Top = 420
    '    frmJob.picMovable1.Left = 9660
        frmJob.fraJobOptions.Visible = True
    End If
    
    If g_intDisableJobOperatorCommentsOnDubbingTab = 1 Then
        frmJob.picScheduleFooter(1).Visible = False
    End If
    
    If g_optJobFormMaximised = 1 Then
        frmJob.WindowState = vbMaximized
    End If
    
    
PROC_Hide_Other_Things:


    'hide some tabs from the hire system
    If LCase(l_strJobType) = "hire" Then
        frmJob.tabReplacementTabs.TabVisible(1) = False
        frmJob.tabReplacementTabs.TabVisible(2) = False
        'frmJob.tabReplacementTabs.TabVisible(7) = False
    Else
        frmJob.tabReplacementTabs.TabVisible(1) = True
        frmJob.tabReplacementTabs.TabVisible(2) = True
        'frmJob.tabReplacementTabs.TabVisible(7) = True
    
    End If
        
    'Hide the Wellcome tab and the Svensk tab
    frmJob.tabReplacementTabs.TabVisible(13) = False
    frmJob.tabReplacementTabs.TabVisible(4) = False
    
    'hide the invoice tab if not ready for invoicing
    'If LCase(frmJob.txtStatus.Text) <> "costed" And LCase(frmJob.txtStatus.Text) <> "sent to accounts" And LCase(frmJob.txtStatus.Text) <> "no charge" Then
    '    frmJob.tabReplacementTabs.TabVisible(6) = False
    'End If
    
'    'hide the project tab if not a project job
'    If LCase(GetData("allocation", "allocationtype", "name", frmJob.cmbAllocation.Text)) = "automatic" Then
'        frmJob.tabReplacementTabs.TabVisible(9) = False
'    End If
'
    
    Exit Sub
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume Next
    'TVCodeTools ErrorHandlerEnd
    
End Sub
'Sub ShowMediaJob(ByVal lp_lngJobID As Long)
'
'    'TVCodeTools ErrorEnablerStart
'    On Error GoTo PROC_ERR
'    'TVCodeTools ErrorEnablerEnd
'
'    If Not CheckAccess("/showjobdetail") Then
'        Exit Sub
'    End If
'
'
'    If lp_lngJobID = 0 Then
'        If Val(frmJobMedia.txtJobID.Text) > 0 Then
'            lp_lngJobID = Val(frmJobMedia.txtJobID.Text)
'        End If
'    End If
'
'    If lp_lngJobID <> 0 Then
'
'
'        Dim l_intCostingsLocked As Integer
'        l_intCostingsLocked = GetData("job", "costingslocked", "jobID", lp_lngJobID)
'
'
'        Screen.MousePointer = vbHourglass
'
'        frmJobMedia.Tag = "LOADING"
'
'        If lp_lngJobID <> -1 And lp_lngJobID <> -2 Then
'            frmJobMedia.Show
'        Else
'            frmJobMedia.Hide
'        End If
'
'        DoEvents
'
'        Dim l_strSQL As String
'
'        l_strSQL = "SELECT * FROM job WHERE jobID = " & lp_lngJobID
'
'        Dim l_rstJob As New ADODB.Recordset
'        Set l_rstJob = ExecuteSQL(l_strSQL, g_strExecuteError)
'
'        CheckForSQLError
'
'        If Not l_rstJob.EOF Then
'
'            l_rstJob.MoveFirst
'
'            Dim l_strVideoStandard As String
'            Dim l_strDirector As String
'
'            l_strVideoStandard = Format(l_rstJob("videostandard"))
'            If l_strVideoStandard = "" Then l_strVideoStandard = GetJobVideoStandard(lp_lngJobID)
'            l_strDirector = GetJobDirector(lp_lngJobID)
'
'            Dim l_strJobType As String
'            l_strJobType = Format(l_rstJob("jobtype"))
'
'            Dim l_datInvoicedDate As Variant
'            l_datInvoicedDate = l_rstJob("invoiceddate")
'
'            With frmJobMedia
'
'                Dim l_blnShowTab As Boolean
'
'                .datDeadlineDate.Value = Format(l_rstJob("deadlinedate"), vbShortDateFormat)
'                .cmbDeadlineTime.Text = Format(l_rstJob("deadlinedate"), vbTimeFormat)
'
'                .txtCreated.Text = Format(l_rstJob("createddate"), "DD/MM/YYYY HH:NN") & " by " & l_rstJob("createduser")
'
'                .txtJobID.Text = l_rstJob("jobID")
'
'                .txtAccountCode.Text = GetData("company", "accountcode", "companyID", l_rstJob("companyID"))
'                .txtClientStatus.Text = GetData("company", "accountstatus", "companyID", l_rstJob("companyID"))
'
'                If UCase(.txtClientStatus.Text) = "HOLD" Or UCase(.txtClientStatus.Text) = "CASH" Then
'                    .txtClientStatus.BackColor = vbRed
'                Else
'                    .txtClientStatus.BackColor = vbWindowBackground
'                End If
'
'                DoEvents
'
'                'printed details
'
'                .txtOrderReference.Text = Trim(" " & l_rstJob("orderreference"))
'                .txtTitle.Text = Format(l_rstJob("title1"))
'                .txtSubtitle.Text = Format(l_rstJob("title2"))
'                .cmbCompany.Text = Format(l_rstJob("companyname"))
'                .cmbContact.Text = Format(l_rstJob("contactname"))
'                .txtTelephone.Text = Format(l_rstJob("telephone"))
'                .txtFax.Text = Format(l_rstJob("fax"))
'
'                .cmbProject.Text = Format(l_rstJob("projectnumber"))
'                If Val(.cmbProject.Text) = 0 Then .cmbProject.Text = ""
'
'                .lblProjectID.Caption = Format(l_rstJob("projectID"))
'
'                DoEvents
'
'                'enable the change status button
'                .cmdLaunch(5).Enabled = True
'
'                'new fields for scheduling (mpc stuff)
'                .cmbProduct.Text = Format(l_rstJob("productname"))
'                .lblProductID.Caption = Format(l_rstJob("productID"))
'
'                .chkJobDetailOnInvoice.Value = GetFlag(l_rstJob("flagdetailoninvoice"))
'                .chkSendEmailOnCompletion.Value = GetFlag(l_rstJob("flagemail"))
'                .chkQuote.Value = GetFlag(l_rstJob("flagquote"))
'                .chkOnHold.Value = GetFlag(l_rstJob("flaghold"))
'                .chkUrgent.Value = GetFlag(l_rstJob("flagurgent"))
'
'                '.Caption = "Job Details - [Showing Job: " & lp_lngJobID & "]"
'
'                .lblCompanyID.Caption = Format(l_rstJob("companyID"))
'                .lblContactID.Caption = Format(l_rstJob("contactID"))
'
'                .cmbAllocation.Text = Format(l_rstJob!joballocation)
'                .cmbAllocation_Click
'
'                .txtOfficeClient.Text = Trim(" " & l_rstJob("notes1"))
'                .txtOperator.Text = Trim(" " & l_rstJob("notes2"))
'                .txtDespatch.Text = Trim(" " & l_rstJob("notes3"))
'                .txtProducerNotes.Text = Trim(" " & l_rstJob("notes5"))
'
'                DoEvents
'
'                'show the job status
'                .txtStatus.Text = Trim(" " & l_rstJob("fd_status"))
'                'make sure it is the correct colour
'                .txtStatus.BackColor = CLng(GetResourceBookingColour(CStr(.txtStatus.Text)))
'
'                .cmbJobType.Text = Format(l_rstJob("jobtype"))
'                .lblJobID.Caption = lp_lngJobID
'
'                DoEvents
'
'
'                Dim l_lngJobID As Long
'                l_lngJobID = Val(.txtJobID.Text)
'
'                If l_lngJobID = 0 Then
'                '    NoJobSelectedMessage
'                '    txtJobID.SetFocus
'                    Exit Sub
'                End If
'
'
'                'Show the relevant Clip Media for this job
'                Dim l_curTotalSize As Currency
'                l_strSQL = "SELECT * FROM events WHERE 1=1 AND jobID = " & l_lngJobID & " ORDER BY clipfilename"
'                .adoMedia.ConnectionString = g_strConnection
'                .adoMedia.RecordSource = l_strSQL
'                .adoMedia.Refresh
'                l_curTotalSize = 0
'                If .adoMedia.Recordset.RecordCount > 0 Then
'                    .adoMedia.Recordset.MoveFirst
'                    Do While Not .adoMedia.Recordset.EOF
'                        If Not IsNull(.adoMedia.Recordset("bigfilesize")) Then l_curTotalSize = l_curTotalSize + .adoMedia.Recordset("bigfilesize")
'                        .adoMedia.Recordset.MoveNext
'                    Loop
'                End If
'                .lblTotalMediaSize.Caption = Format(Int((l_curTotalSize / 1024 / 1024 / 1024) + 0.99999999), "#,##0")
'            End With
'
'        Else
'            If lp_lngJobID <> -1 And lp_lngJobID <> -2 Then
'                MsgBox "Could not locate job record", vbExclamation
'            End If
'            'disable the change status button
'            frmJobMedia.cmdLaunch(5).Enabled = True
'            ClearFields frmJobMedia
'
'            frmJobMedia.lblJobID.Caption = ""
'
'            'frmJobMedia.tab1.Tab = g_intDefaultTab
'            'frmJobMedia.tabReplacementTabs.Tab = GetReplacementTab(g_intDefaultTab)
'
'            frmJobMedia.cmbAllocation.Enabled = True
'            frmJobMedia.cmbAllocation.Text = g_strDefaultJobAllocation
'
'            Dim l_strDefault As String
'
'            l_strDefault = GetData("cetauser", "defaultjobtype", "cetauserID", g_lngUserID)
'            If l_strDefault <> "" Then
'                frmJobMedia.cmbJobType.Text = l_strDefault
'            Else
'                frmJobMedia.cmbJobType.Text = g_strDefaultJobType
'            End If
'
'            l_strDefault = GetData("cetauser", "defaultallocation", "cetauserID", g_lngUserID)
'            If l_strDefault <> "" Then
'                frmJobMedia.cmbAllocation.Text = l_strDefault
'            Else
'                frmJobMedia.cmbAllocation.Text = g_strDefaultJobAllocation
'            End If
'
'            frmJobMedia.lblJobID.Caption = ""
'
'            If GetStatusNumber(frmJobMedia.txtStatus.Text) < 3 Then frmJobMedia.cmbAllocation_Click
'
'        End If
'
'        l_rstJob.Close
'        Set l_rstJob = Nothing
'
'    Else
'
'        ClearFields frmJobMedia
'
'        If frmJobMedia.Visible = True Then
'
'
'
'
'
'
'            If frmJobMedia.txtJobID.Text = "" Then
'
'                frmJobMedia.lblJobID.Caption = ""
'                'frmJobMedia.tab1.Tab = lp_intTabToShow
'                'frmJobMedia.tabReplacementTabs.Tab = GetReplacementTab(lp_intTabToShow)
'                frmJobMedia.cmbAllocation.Enabled = True
'
'
'                l_strDefault = GetData("cetauser", "defaultallocation", "cetauserID", g_lngUserID)
'                If l_strDefault <> "" Then
'                    frmJobMedia.cmbAllocation.Text = l_strDefault
'                Else
'                    frmJobMedia.cmbAllocation.Text = g_strDefaultJobAllocation
'                End If
'
'                l_strDefault = GetData("cetauser", "defaultjobtype", "cetauserID", g_lngUserID)
'
'                If l_strDefault <> "" Then
'                    frmJobMedia.cmbJobType.Text = l_strDefault
'                Else
'                    frmJobMedia.cmbJobType.Text = g_strDefaultJobType
'                End If
'
'                If UCase(frmJobMedia.cmbJobType) = "HIRE" Then
'                    frmJobMedia.txtTitle.Text = "HIRE - TBC"
'                End If
'
'                frmJobMedia.cmbAllocation_Click
'            End If
'
'            frmJobMedia.Show
'            frmJobMedia.ZOrder 0
'
'            GoTo Proc_Hide_Core_Functions
'
'            Exit Sub
'
'            'disable the change status button
'            frmJobMedia.cmdLaunch(5).Enabled = False
'
'            'frmJobMedia.Caption = "Job Details"
'
'            frmJobMedia.lblJobID.Caption = ""
'            frmJobMedia.cmbAllocation_Click
'
'        End If
'
'
'    End If
'
'    DoEvents
'
'    'disable / enable the controls
'    With frmJobMedia
'        If .txtStatus.Text = "Costed" Or .txtStatus.Text = "Sent To Accounts" Or .txtStatus.Text = "Hold Cost" Or .txtStatus.Text = "Cancelled" Or .txtStatus.Text = "No Charge" Then
'
'            With .grdJobDetail
'                .AllowAddNew = False
'                .AllowDelete = False
'                .AllowUpdate = False
'            End With
'
'            .cmdSave.Enabled = False
'
'            'If .txtStatus.Text <> "Hold Cost" Then .cmdMinimumCharge.Enabled = False
'
'            If .txtStatus.Text <> "Hold Cost" Then
'                .cmdReplaceDespatch.Enabled = False
'                .cmdReplaceOffice.Enabled = False
'                .cmdReplaceOperator.Enabled = False
'                .cmdReplaceProducerNotes.Enabled = False
'            End If
'
'        Else
'
'SkipCostingGrid:
'
'            .cmdSave.Enabled = True
'
'        End If
'
'    End With
'
'    DoEvents
'
'    '########################################################################################
'
'    l_strSQL = "SELECT * FROM jobdetail WHERE jobID = " & lp_lngJobID & " ORDER BY fd_orderby, jobdetailID"
'
'    frmJobMedia.adoJobDetail.ConnectionString = g_strConnection
'    frmJobMedia.adoJobDetail.RecordSource = l_strSQL
'    frmJobMedia.adoJobDetail.Refresh
'
'    'update the order by if not already correct
'    With frmJobMedia.adoJobDetail.Recordset
'        If .RecordCount > 0 Then
'            Dim l_intOrderBy As Integer
'            .MoveFirst
'            If .Fields("fd_orderby") <> "0" Then
'                Do While Not .EOF
'                    .Fields("fd_orderby") = l_intOrderBy
'                    .Update
'                    .MoveNext
'                    l_intOrderBy = l_intOrderBy + 1
'                Loop
'            End If
'        End If
'    End With
'
'    l_strSQL = "SELECT jobmediaspecID, jobID, action, fd_order FROM jobmediaspec WHERE jobID = " & lp_lngJobID & " ORDER BY fd_order, jobmediaspecID;"
'    frmJobMedia.adoMediaActions.ConnectionString = g_strConnection
'    frmJobMedia.adoMediaActions.RecordSource = l_strSQL
'    frmJobMedia.adoMediaActions.Refresh
'
'    'this stops all tapes being displayed when no job is loaded
'    If lp_lngJobID = 0 Then
'        lp_lngJobID = -1
'    End If
'
'    DoEvents
'
'    Screen.MousePointer = vbDefault
'
'    'If frmJobMedia.txtJobID.Text <> "" Then
'    '    frmJobMedia.tab1.Tab = lp_intTabToShow
'    'End If
'
'    frmJobMedia.cmbJobType_Click
'
'    Dim l_strUserDefaultTab As String
'    l_strUserDefaultTab = GetData("cetauser", "defaultjobtab", "initials", g_strUserInitials)
'    If l_strUserDefaultTab = "" Then l_strUserDefaultTab = "Schedule"
'
'    If Left(UCase(frmJobMedia.cmbJobType.Text), 3) = "DUB" Then l_strUserDefaultTab = "Dubbing"
'
'    'frmJobMedia.cmbJobType_Click
'
'    frmJobMedia.cmdLaunch(5).Enabled = GetFlag(CheckAccess("/changejobstatus", True))
'
'    If CheckAccess("/jobsaveenabled", True) Then frmJobMedia.cmdSave.Enabled = True
'
'    DoEvents
'
'    If lp_lngJobID <> -2 Then
'        frmJobMedia.Show
'        frmJobMedia.cmbAllocation_Click
'        If GetStatusNumber(frmJobMedia.txtStatus.Text) < 2 Then frmJobMedia.cmbJobType_Click
'        frmJobMedia.ZOrder 0
'
'
'
'        CenterForm frmJobMedia
'    End If
'
'    frmJobMedia.Tag = ""
'
'    GoTo Proc_Hide_Core_Functions
'
'Exit Sub
'
'Proc_Hide_Core_Functions:
'
'    'core functionality disable
'
'PROC_Hide_Other_Things:
'
'    If g_optJobFormMaximised = 1 Then
'        frmJobMedia.WindowState = vbMaximized
'    End If
'
'
'    Exit Sub
'
'    'TVCodeTools ErrorHandlerStart
'PROC_EXIT:
'    Exit Sub
'
'PROC_ERR:
'    MsgBox Err.Description
'    Resume PROC_EXIT
'    'TVCodeTools ErrorHandlerEnd
'
'End Sub
'
Sub RefreshJobHistory(lp_lngJobID As Long)

    Dim l_strSQL As String
    
    l_strSQL = "SELECT cdate, cuser, description FROM jobhistory WHERE jobID = " & lp_lngJobID & " ORDER BY jobhistoryID DESC"
    
    frmJob.adoHistory.ConnectionString = g_strConnection
    frmJob.adoHistory.RecordSource = l_strSQL
    frmJob.adoHistory.Refresh
    
    frmJob.grdHistory.Refresh
        
End Sub

Sub RefreshProjectTeam(lp_grdProjectTeam As Control, lp_lngProjectID As Long, Optional lp_lngJobID As Long)

    Dim l_strSQL As String
    l_strSQL = "SELECT team.role AS 'Role', contact.name AS 'Contact Name', team.teamID AS 'TeamID' "
    l_strSQL = l_strSQL & "FROM team INNER JOIN contact ON team.contactID = contact.contactID "
    l_strSQL = l_strSQL & "WHERE (projectID = '" & lp_lngProjectID & "') "
    If lp_lngJobID <> 0 Then l_strSQL = l_strSQL & "OR (jobID = '" & lp_lngJobID & "') "
    l_strSQL = l_strSQL & "ORDER BY role, contact.name;"
    
    Dim l_conProjectTeam As ADODB.Connection
    Dim l_rstProjectTeam As ADODB.Recordset
    
    Set l_conProjectTeam = New ADODB.Connection
    Set l_rstProjectTeam = New ADODB.Recordset
    
    l_conProjectTeam.ConnectionString = g_strConnection
    l_conProjectTeam.Open
    
    With l_rstProjectTeam
         .CursorLocation = adUseClient
         .LockType = adLockBatchOptimistic
         .CursorType = adOpenDynamic
         .Open l_strSQL, l_conProjectTeam, adOpenDynamic
    End With
    
    l_rstProjectTeam.ActiveConnection = Nothing
    
    Set lp_grdProjectTeam.DataSource = l_rstProjectTeam
    
    l_conProjectTeam.Close
    Set l_conProjectTeam = Nothing
    
    lp_grdProjectTeam.Columns(2).Visible = False
    
End Sub

Sub NoChargeJob(lp_lngJobID As Long, Optional lp_blnKidsCo As Boolean)

If lp_blnKidsCo = False Then

    frmNoCharge.cmbOurContact.Text = GetData("job", "nochargerequestedby", "jobID", lp_lngJobID)
    frmNoCharge.cmbReasonCode.Text = GetData("job", "nochargerequestedcode", "jobID", lp_lngJobID)
    frmNoCharge.txtReason.Text = GetData("job", "nochargerequestedreason", "jobID", lp_lngJobID)
    
    frmNoCharge.Show vbModal
    
    Screen.MousePointer = vbHourglass
    
    If frmNoCharge.Tag <> "CANCELLED" Then
        ApplyNoChargeToJob lp_lngJobID, frmNoCharge.cmbOurContact.Text, frmNoCharge.cmbReasonCode.Text, frmNoCharge.txtReason.Text
    End If
        
    Unload frmNoCharge
    Set frmNoCharge = Nothing

Else
    ApplyNoChargeToJob lp_lngJobID, g_strUserName, "Kidsco Contract", ""
End If
Screen.MousePointer = vbDefault

End Sub
Sub NoChargeCostingSheet(lp_lngCostingSheetID As Long)

frmNoCharge.cmbOurContact.Text = GetData("job", "nochargerequestedby", "costingsheetID", lp_lngCostingSheetID)
frmNoCharge.cmbReasonCode.Text = GetData("job", "nochargerequestedcode", "costingsheetID", lp_lngCostingSheetID)
frmNoCharge.txtReason.Text = GetData("job", "nochargerequestedreason", "costingsheetID", lp_lngCostingSheetID)

frmNoCharge.Show vbModal

If frmNoCharge.Tag <> "CANCELLED" Then

    Dim l_rstJobs As New ADODB.Recordset
    Dim l_strSQL As String
    
    l_strSQL = "SELECT jobID FROM job WHERE costingsheetID = '" & lp_lngCostingSheetID & "';"
    Set l_rstJobs = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If Not l_rstJobs.EOF Then l_rstJobs.MoveFirst
    
    Do While Not l_rstJobs.EOF
        ApplyNoChargeToJob l_rstJobs("jobID"), frmNoCharge.cmbOurContact.Text, frmNoCharge.cmbReasonCode.Text, frmNoCharge.txtReason.Text
        
        l_rstJobs.MoveNext
    Loop
    
    l_rstJobs.Close
    Set l_rstJobs = Nothing
    
    SetData "costingsheet", "fd_status", "costingsheetID", lp_lngCostingSheetID, "No Charge"

End If

Unload frmNoCharge
Set frmNoCharge = Nothing

End Sub
Sub ShowJobListing(ByVal lp_strCriterea As String)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/showjoblisting") Then
        Exit Sub
    End If
    
    'If IsFormLoaded("frmJobListing") Then
    '    frmJobListing.Show
    '    frmJobListing.ZOrder 0
    '    Exit Sub
    'End If
    
    Dim lp_strWhereSQL As String, lp_strHideDemo As String
    
    If frmJobListing.chkHideDemo.Value <> 0 Then
        lp_strHideDemo = " AND job.companyID > 100 "
    Else
        lp_strHideDemo = ""
    End If
    
    Dim l_blnAllowUpdates As Boolean
    
    On Error Resume Next
    
    g_strDefaultJobType = GetData("cetauser", "defaultjobtype", "cetauserID", g_lngUserID)
    
    DoEvents
    
    
    If Left(LCase(g_strDefaultJobType), 3) <> "dub" Then
        frmJobListing.grdJobListing.Columns("dubbing requirements").Visible = False
        frmJobListing.grdJobListing.Columns("vt started").Visible = False
        frmJobListing.grdJobListing.Columns("op").Visible = False
        DoEvents
    Else
        frmJobListing.grdJobListing.Columns("dubbing requirements").Visible = True
        frmJobListing.grdJobListing.Columns("vt started").Visible = True
        frmJobListing.grdJobListing.Columns("op").Visible = True
    End If

    On Error GoTo PROC_ERR
    
    Select Case LCase(lp_strCriterea)
        Case "mdibuttonclick"
            'If frmJobListing.adoJobDetail.RecordSource <> "" Then
            '    frmJobListing.Show
            '    frmJobListing.adoJobListing.Refresh
            '    frmJobListing.ZOrder 0
            '    Exit Sub
            'End If
            If frmJobListing.tabFilter.Visible = False Then
                frmJobListing.tabReplacement.Tab = 3
                lp_strWhereSQL = " (job.fd_status = 'Completed')"
            Else
                frmJobListing.tabFilter.Tab = 0
                lp_strWhereSQL = " (job.completeddate Is null) AND (job.cancelleddate Is null) AND (job.flagquote <> " & GetFlag(1) & ")"
            End If
            
            l_blnAllowUpdates = False
            
        Case "outstanding"
            lp_strWhereSQL = " (job.completeddate Is null) AND (job.cancelleddate Is null) AND (job.flagquote Is Null OR job.flagquote <> " & GetFlag(1) & ") AND (job.fd_status) <> 'No Charge' AND (job.fd_status) <> 'Sent to Accounts'"
            If frmJobListing.tabFilter.Tab <> 0 Then frmJobListing.tabFilter.Tab = 0
            l_blnAllowUpdates = True
        Case "completed."
            lp_strWhereSQL = " (job.completeddate Is Not null) AND (job.cancelleddate Is null) AND (job.invoiceddate Is null) AND (job.creditnotedate is null) AND (job.senttoaccountsdate Is null) AND (job.flagquote Is Null OR job.flagquote <> " & GetFlag(1) & ") AND (job.fd_status) <> 'No Charge'"
            If frmJobListing.tabFilter.Tab <> 1 Then frmJobListing.tabFilter.Tab = 1
            l_blnAllowUpdates = True
        Case "invoiced"
            lp_strWhereSQL = " (job.invoiceddate Is Not null) AND (job.creditnotedate Is null) AND (job.senttoaccountsdate Is null) AND (job.cancelleddate Is null) AND (job.flagquote Is Null OR job.flagquote <> " & GetFlag(1) & ") AND (job.fd_status) <> 'No Charge'"
            If frmJobListing.tabFilter.Tab <> 2 Then frmJobListing.tabFilter.Tab = 2
            l_blnAllowUpdates = False
        Case "credited"
            lp_strWhereSQL = " (job.creditnotedate Is Not null) AND (job.senttoaccountsdate Is null) AND (job.cancelleddate Is null) AND (job.flagquote Is Null OR job.flagquote <> " & GetFlag(1) & ") AND (job.fd_status) <> 'No Charge'"
            If frmJobListing.tabFilter.Tab <> 3 Then frmJobListing.tabFilter.Tab = 3
            l_blnAllowUpdates = False
        Case "senttoaccounts"
            lp_strWhereSQL = " ((job.senttoaccountsdate Is Not null)) AND (job.cancelleddate Is null) AND (job.flagquote Is Null OR job.flagquote <> " & GetFlag(1) & ")"
            If frmJobListing.tabFilter.Tab <> 4 Then frmJobListing.tabFilter.Tab = 4
            l_blnAllowUpdates = False
        Case "no charge", "nocharge"
           lp_strWhereSQL = " (job.fd_status = 'No Charge')"
            If frmJobListing.tabFilter.Tab <> 5 Then frmJobListing.tabFilter.Tab = 5
            l_blnAllowUpdates = False
        Case "quote"
            lp_strWhereSQL = " (job.flagquote = " & GetFlag(1) & ") AND (job.cancelleddate Is null)"
            If frmJobListing.tabFilter.Tab <> 6 Then frmJobListing.tabFilter.Tab = 6
            l_blnAllowUpdates = False
        Case "abandoned"
            lp_strWhereSQL = " (job.cancelleddate Is Not null)"
            If frmJobListing.tabFilter.Tab <> 7 Then frmJobListing.tabFilter.Tab = 7
            l_blnAllowUpdates = False
                
            
            'the new types for facilities etc
        Case "2nd pencil"
            lp_strWhereSQL = " (job.fd_status = '2nd Pencil')"
            l_blnAllowUpdates = True
        Case "pencil"
            lp_strWhereSQL = " (job.fd_status = 'Pencil')"
            l_blnAllowUpdates = True
        Case "confirmed / prepared / despatched"
            lp_strWhereSQL = " (job.fd_status = 'Confirmed' OR job.fd_status = 'Prepared' OR job.fd_status = 'Despatched')"
            l_blnAllowUpdates = True
        Case "completed / returned"
            lp_strWhereSQL = " (job.fd_status = 'Completed' or job.fd_status = 'Returned')"
            l_blnAllowUpdates = False
        Case "hold cost"
            lp_strWhereSQL = " (job.fd_status = 'Hold Cost')"
            l_blnAllowUpdates = False
        Case "costed"
            lp_strWhereSQL = " (job.fd_status = 'Costed')"
            l_blnAllowUpdates = False
        Case "sent to accounts"
            lp_strWhereSQL = " (job.fd_status = 'Sent To Accounts')"
            l_blnAllowUpdates = False
        Case "cancelled"
            lp_strWhereSQL = " (job.fd_status = 'Cancelled')"
            l_blnAllowUpdates = False
        Case "all"
            lp_strWhereSQL = " (job.jobID <> -1) AND (job.flagquote <> " & GetFlag(1) & ")"
            If frmJobListing.tabFilter.Tab <> 8 Then
                frmJobListing.tabFilter.Tab = 8
            End If
            l_blnAllowUpdates = False

        Case "dubbing control"
        
            lp_strWhereSQL = " (job.cancelleddate IS NULL) AND (jobdetail.copytype <> 'M') AND (jobdetail.copytype <> 'T') AND (jobdetail.copytype <> 'O') "
            
            
            frmJobListing.chkOnlyJobsWithDubs.Value = 1
            l_blnAllowUpdates = False
            
            frmJobListing.grdJobListing.Columns("dubbing requirements").Visible = True
            frmJobListing.grdJobListing.Columns("vt started").Visible = True
            frmJobListing.grdJobListing.Columns("op").Visible = True
    
            If g_optUseSingleCompletionScreenForDubbing = 1 Then l_blnAllowUpdates = True
    
    End Select
    
    If frmJobListing.chkOnlyJobsWithDubs.Value = 1 Then
        frmJobListing.grdJobListing.Columns("dubbing requirements").Visible = True
        frmJobListing.grdJobListing.Columns("vt started").Visible = True
        frmJobListing.grdJobListing.Columns("op").Visible = True
    End If
    
    frmJobListing.grdJobListing.AllowUpdate = l_blnAllowUpdates
    frmJobListing.cmdCompleteJob.Enabled = l_blnAllowUpdates
    
    With frmJobListing
        
        If .cmbCompany.Text <> "" Then
            lp_strWhereSQL = lp_strWhereSQL & " AND (job.companyname Like '" & QuoteSanitise(.cmbCompany.Text) & "%') "
        End If
        
        If .txtSeriesIDSearch.Text <> "" Then
            lp_strWhereSQL = lp_strWhereSQL & " AND (job.seriesID = " & Val(.txtSeriesIDSearch.Text) & ") "
        End If
        
        If .cmbAllocation.Text <> "" Then
            lp_strWhereSQL = lp_strWhereSQL & " AND (job.joballocation Like '" & QuoteSanitise(.cmbAllocation.Text) & "%') "
        End If
        
        If .cmbAllocatedTo.Text <> "" Then
            lp_strWhereSQL = lp_strWhereSQL & " AND (job.allocatedto Like '" & QuoteSanitise(.cmbAllocatedTo.Text) & "%') "
        End If
        
        
        If .cmbJobType.Text <> "" Then
            lp_strWhereSQL = lp_strWhereSQL & " AND (job.jobtype Like '" & QuoteSanitise(.cmbJobType.Text) & "%') "
        End If
        
        If .txtTitleSearch.Text <> "" Then
            lp_strWhereSQL = lp_strWhereSQL & " AND (job.title1 Like '" & QuoteSanitise(.txtTitleSearch.Text) & "%') "
        End If
        
        If .txtSubtitleSearch.Text <> "" Then
            lp_strWhereSQL = lp_strWhereSQL & " AND (job.title2 Like '" & QuoteSanitise(.txtSubtitleSearch.Text) & "%') "
        End If
        
        If .txtJobIDFrom.Text <> "" Then
            lp_strWhereSQL = lp_strWhereSQL & " AND (job.jobID >= '" & .txtJobIDFrom.Text & "') "
        End If
        
        If .txtJobIDTo.Text <> "" Then
            lp_strWhereSQL = lp_strWhereSQL & " AND (job.jobID <= '" & .txtJobIDTo.Text & "') "
        End If
        
        If .txtProjectIDFrom.Text <> "" Then
            lp_strWhereSQL = lp_strWhereSQL & " AND (job.projectnumber >= '" & .txtProjectIDFrom.Text & "') "
        End If
        
        If .txtProjectIDTo.Text <> "" Then
            lp_strWhereSQL = lp_strWhereSQL & " AND (job.projectnumber  <= '" & .txtProjectIDTo.Text & "') "
        End If
        
        If .cmbOurContact.Text <> "" Then lp_strWhereSQL = lp_strWhereSQL & " AND (job.ourcontact Like '" & QuoteSanitise(.cmbOurContact.Text) & "%') "

        
        If Not IsNull(.datStartDateFrom.Value) Then
            Dim l_datDeadLineFrom As Date
            l_datDeadLineFrom = Format(.datStartDateFrom.Value, vbShortDateFormat)
            lp_strWhereSQL = lp_strWhereSQL & " AND (job.startdate >= '" & FormatSQLDate(l_datDeadLineFrom & " 00:00") & "') "
        End If
        
        If Not IsNull(.datStartDateTo.Value) Then
            Dim l_datDeadLineTo As Date
            l_datDeadLineTo = Format(.datStartDateTo.Value, vbShortDateFormat)
            lp_strWhereSQL = lp_strWhereSQL & " AND (job.startdate <= '" & FormatSQLDate(l_datDeadLineTo & " 23:59") & "') "
        End If
        
        If Not IsNull(.datDeadlineFrom.Value) Then
            l_datDeadLineFrom = Format(.datDeadlineFrom.Value, vbShortDateFormat)
            lp_strWhereSQL = lp_strWhereSQL & " AND (job.deadlinedate >= '" & FormatSQLDate(l_datDeadLineFrom & " 00:00") & "') "
        End If
        
        If Not IsNull(.datDeadlineTo.Value) Then
            l_datDeadLineTo = Format(.datDeadlineTo.Value, vbShortDateFormat)
            lp_strWhereSQL = lp_strWhereSQL & " AND (job.deadlinedate <= '" & FormatSQLDate(l_datDeadLineTo & " 23:59") & "') "
        End If
        
        
        If .txtBookedBy.Text <> "" Then
            lp_strWhereSQL = lp_strWhereSQL & " AND (createduser  = '" & .txtBookedBy.Text & "') "
        End If
        
    End With
    
    frmJobListing.MousePointer = vbHourglass
    
    'set up the ADO data controls
    Dim l_strSQL As String
    
    Dim l_strFields As String, l_strFields2 As String
   
    l_strFields = "job.jobID, job.jobdigitaltype, job.projectnumber, job.ourcontact, job.operatorduration, job.allocatedto, job.deadlinedate, job.donotdeliveryet, job.createduser, job.startdate, job.enddate, 'See Job Detail' as requirements, job.productname, job.title1, job.seriesID, job.companyname, job.fd_status, job.vtstartdate, job.vtstartuser, job.completeddate, job.completeduser, job.flaghold "
    l_strFields2 = "job.jobID, job.jobdigitaltype, job.projectnumber, job.ourcontact, job.operatorduration, job.allocatedto, job.deadlinedate, job.donotdeliveryet, job.createduser, job.startdate, job.enddate, job.productname, job.title1, job.seriesID, job.companyname, job.fd_status, job.vtstartdate, job.vtstartuser, job.completeddate, job.completeduser, job.flaghold "
    
    lp_strWhereSQL = lp_strWhereSQL & lp_strHideDemo
    
    If frmJobListing.chkOnlyJobsWithDubs.Value = 1 Then
        l_strSQL = "SELECT " & l_strFields & ", COUNT(copytype) FROM job INNER JOIN jobdetail ON job.jobID = jobdetail.jobID WHERE " & lp_strWhereSQL
        
        If LCase(lp_strCriterea) = "dubbing control" Then
            l_strSQL = l_strSQL & " AND (jobdetail.completeddate IS NULL) AND (job.fd_status <> 'Pencil') AND (job.fd_status <> '2nd Pencil') AND (job.fd_status <> 'Cancelled') "
        End If
        
        l_strSQL = l_strSQL & " GROUP BY " & l_strFields2
    Else
        If frmJobListing.chkShowCostingTotal.Value = 1 Then
            l_strSQL = "SELECT " & l_strFields & ", sum(costing.total) as Total, 'Expr1:' as Requirements FROM job LEFT JOIN project ON job.projectID = project.projectID LEFT JOIN costing ON job.jobID = costing.jobID WHERE " & lp_strWhereSQL & " GROUP BY " & l_strFields2
        Else
            l_strSQL = "SELECT " & l_strFields & ", 'Expr1:' as Requirements FROM job WHERE " & lp_strWhereSQL
        End If
    End If
    
    If LCase(frmJobListing.cmbOrderBy.Text) = "cmborderby" Or LCase(frmJobListing.cmbOrderBy.Text) = "" Then frmJobListing.cmbOrderBy.Text = "deadlinedate"
    l_strSQL = l_strSQL & " ORDER BY " & frmJobListing.cmbOrderBy.Text & " " & frmJobListing.cmbDirection.Text
    
    'Dim l_conJobListing As ADODB.Connection
    'Dim l_rstJobListing As ADODB.Recordset
    
    'Set l_conJobListing = New ADODB.Connection
    'Set l_rstJobListing = New ADODB.Recordset
    
    'l_conJobListing.ConnectionString = g_strConnection
    'l_conJobListing.Open
    
    'With l_rstJobListing
    '     .CursorLocation = adUseClient
    '     .LockType = adLockBatchOptimistic
    '     .CursorType = adOpenDynamic
    '     .Open l_strSQL, l_conJobListing, adOpenDynamic
    'End With
    
    'l_rstJobListing.ActiveConnection = Nothing
    
    ''LoadColumnLayout frmJobListing.grdJobListing, "Job Listing"
    'Set frmJobListing.grdJobListing.DataSource = l_rstJobListing
'    LoadColumnLayout frmJobListing.grdJobListing, "Job Listing"
    
    'l_conJobListing.Close
    'Set l_conJobListing = Nothing

    Debug.Print lp_strWhereSQL
    frmJobListing.adoJobListing.RecordSource = l_strSQL
    frmJobListing.adoJobListing.ConnectionString = g_strConnection
    frmJobListing.adoJobListing.Refresh
    
    'l_strSQL = "SELECT COUNT(jobID) FROM job WHERE " & frmJobListing.grdJobListing.Rows
    
    'Dim l_lngCount As Long
    'l_lngCount = GetCount(l_strSQL)
   
    frmJobListing.Caption = "Job Listing - " & frmJobListing.grdJobListing.Rows & " rows"
    
    frmJobListing.adoJobListing.Caption = frmJobListing.grdJobListing.Rows & " rows"
    
    If g_optHideHireDespatchTab = 1 And g_intDisableOurContact = 1 Then
        frmJobListing.grdJobListing.Columns("allocatedto").Visible = False
    End If
    
    'stop errors when modal forms are open
    On Error Resume Next
    
    frmJobListing.Show
    frmJobListing.ZOrder 0
    
    frmJobListing.MousePointer = vbDefault
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

'Sub ShowJobNotes(lp_lngJobID As Long)
'
'    'TVCodeTools ErrorEnablerStart
'    On Error GoTo PROC_ERR
'    'TVCodeTools ErrorEnablerEnd
'
'    If Not CheckAccess("/showjobnotes") Then
'        Exit Sub
'    End If
'
'    frmJobNotes.Caption = "Job Notes - [Showing Job: " & lp_lngJobID & "]"
'
'    frmJobNotes.txtOfficeClient.Text = GetData("job", "notes1", "jobID", lp_lngJobID)
'    frmJobNotes.txtOfficeClient.Tag = frmJobNotes.txtOfficeClient.Text
'
'    frmJobNotes.txtOperator.Text = GetData("job", "notes2", "jobID", lp_lngJobID)
'    frmJobNotes.txtOperator.Tag = frmJobNotes.txtOperator.Text
'
'    frmJobNotes.txtDespatch.Text = GetData("job", "notes3", "jobID", lp_lngJobID)
'    frmJobNotes.txtDespatch.Tag = frmJobNotes.txtDespatch.Text
'
'    frmJobNotes.txtAccounts.Text = GetData("job", "notes4", "jobID", lp_lngJobID)
'    frmJobNotes.txtAccounts.Tag = frmJobNotes.txtAccounts.Text
'
'    frmJobNotes.lblJobID.Caption = lp_lngJobID
'
'    frmJobNotes.Show
'
'
'    'TVCodeTools ErrorHandlerStart
'PROC_EXIT:
'    Exit Sub
'
'PROC_ERR:
'    MsgBox Err.Description
'    Resume PROC_EXIT
'    'TVCodeTools ErrorHandlerEnd
'
'End Sub
'
Sub ShowJobSearch()
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/showjobsearch") Then
        Exit Sub
    End If
    
    frmSearchJob.Show
    frmSearchJob.ZOrder 0
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub ShowComplaints()

    frmComplaint.Show
    frmComplaint.ZOrder 0

End Sub
Sub ShowPrinterSetup()
    
    If Not CheckAccess("/showprintersetup") Then
        Exit Sub
    End If
    
    On Error Resume Next
    With MDIForm1.dlgMain
        .DialogTitle = "Page Setup"
        .CancelError = True
        .ShowPrinter
    End With
    
End Sub

Sub ShowSundry()
If Not CheckAccess("/showsundry") Then Exit Sub

frmSundryManagement.Show
frmSundryManagement.ZOrder 0
End Sub

Sub ShowSundryCost(lp_lngSundryCostID As Long)

Dim l_strSQL As String
l_strSQL = "SELECT * FROM sundrycost WHERE sundrycostID = '" & lp_lngSundryCostID & "';"

Dim l_rstQuote As ADODB.Recordset
Set l_rstQuote = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

frmSundry.lblSundryID.Caption = l_rstQuote("sundrycostID")

frmSundry.datSundryDateOrdered.Value = Format(l_rstQuote("dateordered"), vbShortDateFormat)
frmSundry.cmbSundryTimeOrdered.Text = Format(l_rstQuote("dateordered"), "HH:nn")

frmSundry.datSundryDateWanted.Value = Format(l_rstQuote("daterequired"), vbShortDateFormat)
frmSundry.cmbSundryTimeWanted.Text = Format(l_rstQuote("daterequired"), "HH:nn")

frmSundry.txtSundryAmount.Text = l_rstQuote("amount")
frmSundry.chkPersonal.Value = Val(Format(l_rstQuote("personal")))
frmSundry.txtSundryComments.Text = Format(l_rstQuote("comments"))
frmSundry.cmbRequestedBy.Text = Format(l_rstQuote("requestedby"))
frmSundry.ddnSundryEnteredBy.Text = Format(l_rstQuote("enteredby"))

frmSundry.txtSundrySupplier.Text = Format(l_rstQuote("supplier"))
frmSundry.txtSundryDetails.Text = Format(l_rstQuote("details"))

frmSundry.cmbOverheads.Text = Format(l_rstQuote("overheadcode"))

frmSundry.txtSundryPickupTime.Text = Format(l_rstQuote("pickuptime"))
frmSundry.txtSundryAccountNumber.Text = Format(l_rstQuote("accountnumber"))
frmSundry.txtSundryPassenger.Text = Format(l_rstQuote("passengers"))
frmSundry.txtSundryLocation.Text = Format(l_rstQuote("location"))
frmSundry.txtSundryDestination.Text = Format(l_rstQuote("destination"))

If Format(l_rstQuote("sundrytype")) = "Food" Then
    frmSundry.tabSundry.Tab = 1
Else
    frmSundry.tabSundry.Tab = 0
End If

Dim l_lngJobID As Long
Dim l_lngProjectNumber As Long

l_lngJobID = Format(l_rstQuote("jobID"))
l_lngProjectNumber = Format(l_rstQuote("projectnumber"))

frmSundry.cmbJobID.Text = l_lngJobID
frmSundry.cmbProject.Text = l_lngProjectNumber

l_rstQuote.Close
Set l_rstQuote = Nothing

frmSundry.Show vbModal

If frmSundry.Tag <> "CANCELLED" Then
    With frmSundry
        SaveSundry Val(.lblSundryID.Caption), IIf(.tabSundry.Caption = "Food / Client Costs", "Food", "Taxi"), Val(.cmbJobID.Text), Val(.cmbProject.Text), .datSundryDateOrdered.Value & " " & .cmbSundryTimeOrdered.Text, .datSundryDateWanted & " " & .cmbSundryTimeWanted, .txtSundryPickupTime.Text, .txtSundryAccountNumber.Text, .txtSundryPassenger.Text, _
                .txtSundryLocation.Text, .txtSundryDestination.Text, .txtSundryComments.Text, .cmbRequestedBy.Text, .ddnSundryEnteredBy.Text, .txtSundryDetails.Text, .txtSundrySupplier.Text, Val(.txtSundryAmount.Text), .chkPersonal.Value, .cmbOverheads.Text
    End With
End If

Unload frmSundry
Set frmSundry = Nothing

End Sub

Function GetPercentageDiscount(lp_dblValue1 As Double, lp_dblValue2 As Double) As Single
If lp_dblValue1 = 0 Then Exit Function
If lp_dblValue1 = lp_dblValue2 Then
    GetPercentageDiscount = 0
Else
    GetPercentageDiscount = 100 - ((100 / lp_dblValue1) * lp_dblValue2)
End If

End Function

Sub SendJobToCustomer(lp_lngJobID As Long, lp_strType As String, lp_lngBatchNumber As Long)

Dim l_strTheFacilityNumber As String, l_strTheBookingNumber As String, l_strTheOrderNumber As String, l_strTheOrderDate As String
Dim l_strTheTitle As String, l_strTheSubtitle As String, l_strTheCustomer As String, l_strTheChargeCode As String, l_strTheCountryCode As String
Dim l_strTheBusinessCode As String, l_strTheNominalCode As String, l_strTheProgrammeNumber As String, l_strTheInvoiceNumber As String
Dim l_strTheInvoiceDate As String, l_strTheItemCode As String, l_strTheItemQuantity As String, l_strTheDuration As String
Dim l_strTheBusinessArea As String, l_strTheUnitCharge  As String, l_strOrderContactName As String, l_strTheSeriesID As String
Dim l_dblTotalNet As Double, l_dblTotalVAT As Double, l_dblTotalGross As Double, l_strTheCompany As String, l_lngCompanyID As Long
Dim l_strCopyType As String, l_strJobNotes As String, l_strCostingDescription As String
Dim l_strTheProjectManager As String, l_strTheProjectNumber As String

Dim l_rstCostingDetail As ADODB.Recordset, l_strSQL As String
Dim l_blnAllLines As Boolean

'Arrives here with lp_strType either set to BBCWW, BBCTV, or EntsRights - this routine then posts that job to the already open batch file

Select Case lp_strType

Case "BBCWW"
    'BBCWW Jobs are processed differently - we need a different function to deal with them.
    SendJobToBBCWW lp_lngJobID, lp_lngBatchNumber
    
Case "Sprockets"
    SendJobToSprockets lp_lngJobID, lp_lngBatchNumber
    
Case "SONYDADC", "Svensk", "DADC"
    SendJobToDADC lp_lngJobID, lp_strType, lp_lngBatchNumber

Case "BFI"
    SendJobToBFI lp_lngJobID, lp_strType, lp_lngBatchNumber
    
Case "CETAtoMP"
    SendJobToMP lp_lngJobID, lp_strType

Case Else
    
    If lp_strType = "SHINE" Or lp_strType = "EASTENDERS" Then
        l_blnAllLines = True
    Else
        l_blnAllLines = False
    End If
    
    l_strSQL = "SELECT * FROM costing WHERE (jobID = " & lp_lngJobID & ") AND (creditnotenumber IS Null or creditnotenumber = 0) ORDER by costingID"
    
    Set l_rstCostingDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If Not l_rstCostingDetail.EOF Then
        'Not BBC Worldwide - need to get the salesledgercode of the customer for the text file
        'get the company ID as it is needed to work out other values
        l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
        l_strTheInvoiceNumber = GetData("job", "invoicenumber", "jobID", lp_lngJobID)
        l_strTheInvoiceDate = Format(GetData("job", "invoiceddate", "jobID", lp_lngJobID), "dd/mm/yyyy")
        l_strTheOrderNumber = GetData("job", "orderreference", "jobid", lp_lngJobID)
        If l_strTheOrderNumber = "" Then l_strTheOrderNumber = lp_lngJobID
        
        l_strJobNotes = CRLFSanitise(GetData("job", "notes2", "jobid", lp_lngJobID))
        l_strTheTitle = GetData("job", "title1", "jobID", lp_lngJobID)
        If InStr(l_strTheTitle, Chr(9)) > 0 Then l_strTheTitle = Replace(l_strTheTitle, Chr(9), " ")
        If InStr(l_strTheTitle, vbCrLf) > 0 Then l_strTheTitle = Replace(l_strTheTitle, vbCrLf, " ")
        l_strTheSubtitle = GetData("job", "title2", "jobID", lp_lngJobID)
        If InStr(l_strTheSubtitle, Chr(9)) > 0 Then l_strTheSubtitle = Replace(l_strTheSubtitle, Chr(9), " ")
        If InStr(l_strTheSubtitle, vbCrLf) > 0 Then l_strTheSubtitle = Replace(l_strTheSubtitle, vbCrLf, " ")
        l_strTheSeriesID = GetData("job", "seriesID", "jobID", lp_lngJobID)

        l_strOrderContactName = GetData("job", "contactname", "jobID", lp_lngJobID)
        
        l_dblTotalNet = 0
        l_dblTotalVAT = 0
        l_dblTotalGross = 0
        l_rstCostingDetail.MoveFirst
        Do While Not l_rstCostingDetail.EOF
            
            If l_blnAllLines = True Then

                Print #1, l_strTheOrderNumber & Chr(9);
                Print #1, l_strOrderContactName & Chr(9);
                If lp_strType = "BBCMG" Then
                    Print #1, l_strTheSubtitle & Chr(9);
                    Print #1, l_strTheTitle & Chr(9);
                Else
                    Print #1, Trim(l_strTheTitle) & Chr(9);
                    Print #1, Trim(l_strTheSubtitle) & Chr(9);
                End If
                Print #1, l_strTheInvoiceNumber & Chr(9);
                If lp_strType = "SHINE" Then Print #1, l_strTheSeriesID & Chr(9);
                l_strCostingDescription = l_rstCostingDetail("description")
                If InStr(l_strCostingDescription, Chr(9)) > 0 Then l_strCostingDescription = Replace(l_strCostingDescription, Chr(9), " ")
                If InStr(l_strCostingDescription, vbCrLf) > 0 Then l_strCostingDescription = Replace(l_strCostingDescription, vbCrLf, " ")
                If Left(l_strCostingDescription, 2) = "- " Then
                    Print #1, Mid(l_strCostingDescription, 3) & Chr(9);
                Else
                    Print #1, l_strCostingDescription & Chr(9);
                End If
                Print #1, l_rstCostingDetail("costcode") & Chr(9);
                Print #1, GetData("jobdetail", "workflowdescription", "jobdetailID", l_rstCostingDetail("jobdetailID")) & Chr(9);
                Print #1, l_rstCostingDetail("quantity") & Chr(9);
                Print #1, l_rstCostingDetail("fd_time") & Chr(9);
                Print #1, Format(l_rstCostingDetail("unitcharge"), "#.00") & Chr(9);
                Print #1, Format(l_rstCostingDetail("total"), "#.00") & Chr(9);
                Print #1, Format(l_rstCostingDetail("vat"), "#.00") & Chr(9);
                Print #1, Format(l_rstCostingDetail("totalincludingvat"), "#.00") & Chr(9)
            
            Else
            
                l_dblTotalNet = l_dblTotalNet + l_rstCostingDetail("total")
                l_dblTotalVAT = l_dblTotalVAT + l_rstCostingDetail("vat")
                l_dblTotalGross = l_dblTotalGross + l_rstCostingDetail("totalincludingvat")
            
            End If
            
            l_rstCostingDetail.MoveNext
        
        Loop
        
    End If
    
    If l_blnAllLines = False Then
    
        Print #1, l_strTheOrderNumber & Chr(9);
        Print #1, l_strOrderContactName & Chr(9);
        If lp_strType = "BBCMG" Then
            Print #1, l_strTheSubtitle & Chr(9);
            Print #1, l_strTheTitle & Chr(9);
        Else
            Print #1, l_strTheTitle & Chr(9);
            Print #1, l_strTheSubtitle & Chr(9);
            Print #1, l_strJobNotes & Chr(9);
        End If
        Print #1, l_strTheInvoiceNumber & Chr(9);
        If lp_strType <> "BBCMG" Then
            Print #1, Format(l_dblTotalNet, "#.00") & Chr(9);
            Print #1, Format(l_dblTotalVAT, "#.00") & Chr(9);
            Print #1, Format(l_dblTotalGross, "#.00") & Chr(9)
        Else
            Print #1, Format(l_dblTotalNet, "#.00") & Chr(9)
        End If
    
    End If
    
    'Set the BatchNumber
    SetData "job", "senttoBBCbatchnumber", "jobID", lp_lngJobID, lp_lngBatchNumber

End Select

End Sub

Sub SendJobToMP(lp_lngJobID, lp_strType)

Dim l_strTheFacilityNumber As String, l_strTheBookingNumber As String, l_strTheOrderNumber As String, l_strTheOrderDate As String
Dim l_strTheTitle As String, l_strTheSubtitle As String, l_strTheCustomer As String, l_strTheChargeCode As String, l_strTheCountryCode As String
Dim l_strTheBusinessCode As String, l_strTheNominalCode As String, l_strTheProgrammeNumber As String, l_strTheInvoiceNumber As String
Dim l_strTheInvoiceDate As String, l_strTheItemCode As String, l_strTheItemQuantity As String, l_strTheDuration As String
Dim l_strTheBusinessArea As String, l_strTheUnitCharge  As String, l_strOrderContactName As String, l_strTheSeriesID As String
Dim l_dblTotalNet As Double, l_dblTotalVAT As Double, l_dblTotalGross As Double, l_strTheCompany As String, l_lngMediaPulseCompanyID As Long, l_lngCompanyID As Long
Dim l_strCopyType As String, l_strJobNotes As String, l_strCostingDescription As String
Dim l_strTheProjectManager As String, l_strTheProjectNumber As String, l_lngMediaPulseJobID As Long, l_lngMPServiceProfileNo As Long

Dim l_rstCostingDetail As ADODB.Recordset, l_strSQL As String
Dim l_blnAllLines As Boolean
Dim l_lngTrackerID As Long, l_rsTrackerItem As ADODB.Recordset, l_strFilename As String, l_strTapeFormat As String, l_strDuration  As String

'Arrives here with lp_strType either set to BBCWW, BBCTV, or EntsRights - this routine then posts that job to the already open batch file

l_strSQL = "SELECT * FROM costing WHERE (jobID = " & lp_lngJobID & ") AND (creditnotenumber IS Null or creditnotenumber = 0) ORDER by costingID"

Set l_rstCostingDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If Not l_rstCostingDetail.EOF Then
    'Not BBC Worldwide - need to get the salesledgercode of the customer for the text file
    'get the company ID as it is needed to work out other values
    l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
    l_lngMediaPulseCompanyID = GetData("company", "MediaPulseCompanyID", "companyID", l_lngCompanyID)
    l_lngMediaPulseJobID = GetData("company", "MediaPulseJobID", "companyID", l_lngCompanyID)
    l_strTheInvoiceNumber = GetData("job", "invoicenumber", "jobID", lp_lngJobID)
    l_strTheInvoiceDate = Format(GetData("job", "invoiceddate", "jobID", lp_lngJobID), "dd/mm/yyyy")
    l_strTheOrderNumber = GetData("job", "orderreference", "jobid", lp_lngJobID)
    If l_strTheOrderNumber = "" Then l_strTheOrderNumber = "CETA_Job_" & lp_lngJobID
    
    l_strJobNotes = CRLFSanitise(GetData("job", "notes2", "jobid", lp_lngJobID))
    l_strTheTitle = GetData("job", "title1", "jobID", lp_lngJobID)
    If InStr(l_strTheTitle, Chr(9)) > 0 Then l_strTheTitle = Replace(l_strTheTitle, Chr(9), " ")
    If InStr(l_strTheTitle, vbCrLf) > 0 Then l_strTheTitle = Replace(l_strTheTitle, vbCrLf, " ")
    If InStr(l_strTheTitle, vbLf) > 0 Then l_strTheTitle = Replace(l_strTheTitle, vbLf, " ")
    If InStr(l_strTheTitle, vbCr) > 0 Then l_strTheTitle = Replace(l_strTheTitle, vbCr, " ")
    l_strTheSubtitle = GetData("job", "title2", "jobID", lp_lngJobID)
    If InStr(l_strTheSubtitle, Chr(9)) > 0 Then l_strTheSubtitle = Replace(l_strTheSubtitle, Chr(9), " ")
    If InStr(l_strTheSubtitle, vbCrLf) > 0 Then l_strTheSubtitle = Replace(l_strTheSubtitle, vbCrLf, " ")
    If InStr(l_strTheSubtitle, vbLf) > 0 Then l_strTheSubtitle = Replace(l_strTheSubtitle, vbLf, " ")
    If InStr(l_strTheSubtitle, vbCr) > 0 Then l_strTheSubtitle = Replace(l_strTheSubtitle, vbCr, " ")
    l_strTheSeriesID = GetData("job", "seriesID", "jobID", lp_lngJobID)

    l_strOrderContactName = GetData("job", "contactname", "jobID", lp_lngJobID)
    
'    l_dblTotalNet = 0
'    l_dblTotalVAT = 0
'    l_dblTotalGross = 0

    Print #1, """Ceta JobID"",""Customer"",""Job"",""Order Description"",""Contact; "",""Client PO"",""Profile Header"",""Service Qty"",""Dur"",""Billing Description"""
    l_rstCostingDetail.MoveFirst
    Do While Not l_rstCostingDetail.EOF
        
        l_strCostingDescription = l_rstCostingDetail("description")
        l_lngMPServiceProfileNo = GetData("ratecard", "MPServiceProfileNo", "cetacode", l_rstCostingDetail("costcode"))
        If l_lngMPServiceProfileNo = 0 Then
            l_lngMPServiceProfileNo = GetDataSQL("SELECT MPServiceProfileNo FROM ratecard WHERE cetacode = '" & l_rstCostingDetail("costcode") & "' AND companyID = " & l_lngCompanyID)
        End If
        On Error Resume Next
        l_strFilename = Left(l_strCostingDescription, InStr(l_strCostingDescription, " - ") - 1)
        On Error GoTo 0
        Set l_rsTrackerItem = ExecuteSQL("SELECT * FROM tracker_item WHERE companyID = 1244 AND jobID = " & lp_lngJobID & " AND itemfilename = '" & QuoteSanitise(l_strFilename) & "';", g_strExecuteError)
        If l_rsTrackerItem.RecordCount = 1 Then
            l_strTapeFormat = l_rsTrackerItem("headertext4")
            l_strDuration = Trim(" " & l_rsTrackerItem("TimecodeDuration"))
        Else
            l_strTapeFormat = ""
            l_strDuration = ""
        End If
        l_rsTrackerItem.Close
        Set l_rsTrackerItem = Nothing
        Print #1, lp_lngJobID & ",";
        Print #1, l_lngMediaPulseCompanyID & ",";
        Print #1, l_lngMediaPulseJobID & ",";
        Print #1, """" & DoubleQuoteSanitise(Trim(l_strTheTitle)) & " // " & DoubleQuoteSanitise(Trim(l_strTheSubtitle)) & """,";
        Print #1, """" & DoubleQuoteSanitise(l_strOrderContactName) & """,";
        Print #1, """" & DoubleQuoteSanitise(l_strTheOrderNumber) & """,";
        Print #1, l_lngMPServiceProfileNo & ",";
        Print #1, l_rstCostingDetail("quantity") & ",";
        Print #1, l_rstCostingDetail("fd_time") & ",";
        Print #1, """" & DoubleQuoteSanitise(l_strCostingDescription) & ""","
        
'        Print #1, Format(l_rstCostingDetail("unitcharge"), "#.00") & Chr(9);
'        Print #1, Format(l_rstCostingDetail("total"), "#.00") & Chr(9);
'        Print #1, Format(l_rstCostingDetail("vat"), "#.00") & Chr(9);
'        Print #1, Format(l_rstCostingDetail("totalincludingvat"), "#.00") & Chr(9)
        
        l_rstCostingDetail.MoveNext
    
    Loop
    
End If

End Sub

Sub SendJobToBFI(lp_lngJobID As Long, lp_strType As String, lp_lngBatchNumber As Long)

Dim l_strTheFacilityNumber As String, l_strTheBookingNumber As String, l_strTheOrderNumber As String, l_strTheOrderDate As String
Dim l_strTheTitle As String, l_strTheSubtitle As String, l_strTheCustomer As String, l_strTheChargeCode As String, l_strTheCountryCode As String
Dim l_strTheBusinessCode As String, l_strTheNominalCode As String, l_strTheProgrammeNumber As String, l_strTheInvoiceNumber As String
Dim l_strTheInvoiceDate As String, l_strTheItemCode As String, l_strTheItemQuantity As String, l_strTheDuration As String
Dim l_strTheBusinessArea As String, l_strTheUnitCharge  As String, l_strOrderContactName As String, l_strTheSeriesID As String
Dim l_dblTotalNet As Double, l_dblTotalVAT As Double, l_dblTotalGross As Double, l_strTheCompany As String, l_lngCompanyID As Long
Dim l_strCopyType As String, l_strJobNotes As String, l_strCostingDescription As String
Dim l_strTheProjectManager As String, l_strTheProjectNumber As String

Dim l_rstCostingDetail As ADODB.Recordset, l_strSQL As String
Dim l_blnAllLines As Boolean
Dim l_lngTrackerID As Long, l_rsTrackerItem As ADODB.Recordset, l_strFilename As String, l_strTapeFormat As String, l_strDuration  As String

'Arrives here with lp_strType either set to BBCWW, BBCTV, or EntsRights - this routine then posts that job to the already open batch file

l_strSQL = "SELECT * FROM costing WHERE (jobID = " & lp_lngJobID & ") AND (creditnotenumber IS Null or creditnotenumber = 0) ORDER by costingID"

Set l_rstCostingDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If Not l_rstCostingDetail.EOF Then
    'Not BBC Worldwide - need to get the salesledgercode of the customer for the text file
    'get the company ID as it is needed to work out other values
    l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
    l_strTheInvoiceNumber = GetData("job", "invoicenumber", "jobID", lp_lngJobID)
    l_strTheInvoiceDate = Format(GetData("job", "invoiceddate", "jobID", lp_lngJobID), "dd/mm/yyyy")
    l_strTheOrderNumber = GetData("job", "orderreference", "jobid", lp_lngJobID)
    If l_strTheOrderNumber = "" Then l_strTheOrderNumber = lp_lngJobID
    
    l_strJobNotes = CRLFSanitise(GetData("job", "notes2", "jobid", lp_lngJobID))
    l_strTheTitle = GetData("job", "title1", "jobID", lp_lngJobID)
    If InStr(l_strTheTitle, Chr(9)) > 0 Then l_strTheTitle = Replace(l_strTheTitle, Chr(9), " ")
    If InStr(l_strTheTitle, vbCrLf) > 0 Then l_strTheTitle = Replace(l_strTheTitle, vbCrLf, " ")
    l_strTheSubtitle = GetData("job", "title2", "jobID", lp_lngJobID)
    If InStr(l_strTheSubtitle, Chr(9)) > 0 Then l_strTheSubtitle = Replace(l_strTheSubtitle, Chr(9), " ")
    If InStr(l_strTheSubtitle, vbCrLf) > 0 Then l_strTheSubtitle = Replace(l_strTheSubtitle, vbCrLf, " ")
    l_strTheSeriesID = GetData("job", "seriesID", "jobID", lp_lngJobID)

    l_strOrderContactName = GetData("job", "contactname", "jobID", lp_lngJobID)
    
    l_dblTotalNet = 0
    l_dblTotalVAT = 0
    l_dblTotalGross = 0
    l_rstCostingDetail.MoveFirst
    Do While Not l_rstCostingDetail.EOF
        
        l_strCostingDescription = l_rstCostingDetail("description")
        On Error Resume Next
        l_strFilename = Left(l_strCostingDescription, InStr(l_strCostingDescription, " - ") - 1)
        On Error GoTo 0
        Set l_rsTrackerItem = ExecuteSQL("SELECT * FROM tracker_item WHERE companyID = 1244 AND jobID = " & lp_lngJobID & " AND itemfilename = '" & QuoteSanitise(l_strFilename) & "';", g_strExecuteError)
        If l_rsTrackerItem.RecordCount = 1 Then
            l_strTapeFormat = l_rsTrackerItem("headertext4")
            l_strDuration = Trim(" " & l_rsTrackerItem("TimecodeDuration"))
        Else
            l_strTapeFormat = ""
            l_strDuration = ""
        End If
        l_rsTrackerItem.Close
        Set l_rsTrackerItem = Nothing
        Print #1, l_strTheOrderNumber & Chr(9);
        Print #1, l_strOrderContactName & Chr(9);
        Print #1, Trim(l_strTheTitle) & Chr(9);
        Print #1, Trim(l_strTheSubtitle) & Chr(9);
        Print #1, l_strTheInvoiceNumber & Chr(9);
        If InStr(l_strCostingDescription, Chr(9)) > 0 Then l_strCostingDescription = Replace(l_strCostingDescription, Chr(9), " ")
        If InStr(l_strCostingDescription, vbCrLf) > 0 Then l_strCostingDescription = Replace(l_strCostingDescription, vbCrLf, " ")
        If Left(l_strCostingDescription, 2) = "- " Then
            Print #1, Mid(l_strCostingDescription, 3) & Chr(9);
        Else
            Print #1, l_strCostingDescription & Chr(9);
        End If
        Print #1, l_strTapeFormat & Chr(9);
        Print #1, l_rstCostingDetail("costcode") & Chr(9);
        Print #1, GetData("jobdetail", "workflowdescription", "jobdetailID", l_rstCostingDetail("jobdetailID")) & Chr(9);
        Print #1, l_rstCostingDetail("quantity") & Chr(9);
        Print #1, l_strDuration & Chr(9);
        Print #1, Format(l_rstCostingDetail("unitcharge"), "#.00") & Chr(9);
        Print #1, Format(l_rstCostingDetail("total"), "#.00") & Chr(9);
        Print #1, Format(l_rstCostingDetail("vat"), "#.00") & Chr(9);
        Print #1, Format(l_rstCostingDetail("totalincludingvat"), "#.00") & Chr(9)
        
        l_rstCostingDetail.MoveNext
    
    Loop
    
End If

'Set the BatchNumber
SetData "job", "senttoBBCbatchnumber", "jobID", lp_lngJobID, lp_lngBatchNumber

End Sub

Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strAttachment As String, _
ByVal lp_strBody As String, ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, _
Optional ByVal lp_strBCCAddress As String, Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & QuoteSanitise(lp_strEmailFromAddress) & "', ", "Null, ")
SQL = SQL & "'" & g_strUserInitials & "', "
SQL = SQL & "'" & g_strUserInitials & "', "
SQL = SQL & "1);"

Debug.Print SQL
ExecuteSQL SQL, g_strExecuteError
CheckForSQLError

End Sub
Sub SendSMTPMailOLD(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strAttachment As String, ByVal lp_strBody As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

On Error GoTo SentSMTPMail_Error

If g_strSMTPServer = "" Then
    If lp_blnDontShowErrors <> True Then MsgBox "There is no SMTP server set up. Please configure one before trying to send emails", vbExclamation
    Exit Sub
End If

If g_strUserEmailAddress = "" Then
    If lp_blnDontShowErrors <> True Then MsgBox "You can not send email via CFM until your email address is entered against your logon details", vbExclamation
    Exit Sub
End If

Dim poSendmail As clsSendMail

Set poSendmail = New clsSendMail


If Not poSendmail.IsValidEmailAddress(lp_strEmailToAddress) Then
    If lp_blnDontShowErrors <> True Then MsgBox "This is not a valid email address. Unable to send mail", vbExclamation
    GoTo Close_SMTP
End If

If g_optTrySMTPPing = 1 Then
    If Not poSendmail.Ping(g_strSMTPServer) Then
        If lp_blnDontShowErrors <> True Then MsgBox "Can not ping the SMTP server. Unable to send mail", vbExclamation
        GoTo Close_SMTP
    End If
End If


If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

Screen.MousePointer = vbHourglass

poSendmail.SMTPHost = g_strSMTPServer
poSendmail.SMTPPort = 25
poSendmail.UseAuthentication = False

If g_strSMTPUserName <> "" Then
    poSendmail.UseAuthentication = True
    poSendmail.UserName = g_strSMTPUserName
    poSendmail.Password = g_strSMTPPassword
End If

poSendmail.Connect

If lp_strEmailFromAddress <> "" Then
    poSendmail.From = lp_strEmailFromAddress
Else
    poSendmail.From = g_strUserEmailAddress
End If
poSendmail.FromDisplayName = g_strFullUserName
poSendmail.Recipient = lp_strEmailToAddress
poSendmail.RecipientDisplayName = lp_strEmailToName
poSendmail.ReplyToAddress = g_strUserEmailAddress
poSendmail.Subject = lp_strSubject
poSendmail.Attachment = lp_strAttachment     ' file attachment(s), optional
poSendmail.Message = lp_strBody

If lp_strCopyToEmailAddress <> "" Then
    poSendmail.CcRecipient = lp_strCopyToEmailAddress
    poSendmail.CcDisplayName = lp_strCopyToName
End If

If lp_strBCCAddress <> "" Then
    poSendmail.BccRecipient = lp_strBCCAddress
End If

poSendmail.Send


poSendmail.Disconnect

Screen.MousePointer = vbDefault


Close_SMTP:

Set poSendmail = Nothing

Screen.MousePointer = vbDefault

Exit Sub


SentSMTPMail_Error:

If lp_blnDontShowErrors = False Then
    MsgBox "Error: " & Err.Number & vbCrLf & vbCrLf & Err.Description, vbExclamation
End If

End Sub

Function UpdateTotal(lp_dblTotal As Double) As Single
'this works out a percentage and returns it.

frmUpdateTotal.txtOldTotal.Text = lp_dblTotal
frmUpdateTotal.txtNewTotal.Text = lp_dblTotal
frmUpdateTotal.txtDifferencePercent.Text = "0"

frmUpdateTotal.Show vbModal

If frmUpdateTotal.txtDifferencePercent.Text <> "CANCEL" Then
    UpdateTotal = Val(frmUpdateTotal.txtDifferencePercent.Text)
Else
    UpdateTotal = -999
End If

Unload frmUpdateTotal
Set frmUpdateTotal = Nothing

End Function

Sub ShowRateCard(ByVal lp_strRatesToShow As String, ByVal lp_varCompanyID As Variant)
    
    Dim l_lngCompanyID As Long
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/showratecard") Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    l_lngCompanyID = Val(lp_varCompanyID)
    If lp_varCompanyID <> "" Then
        frmRateCard.cmbCompany.Text = GetData("company", "name", "companyID", l_lngCompanyID)
        frmRateCard.lblCompanyID.Caption = l_lngCompanyID
    End If
    
    Dim l_strSQL As String
    Dim l_intLoop  As Integer
    Dim l_strFilter As String
    
    l_strFilter = "ratecardID > 0 "
    
    For l_intLoop = 0 To 5
        If frmRateCard.txtFilter(l_intLoop).Text <> "" Then
            l_strFilter = l_strFilter & " AND " & frmRateCard.txtFilter(l_intLoop).DataField & " LIKE '" & QuoteSanitise(frmRateCard.txtFilter(l_intLoop).Text) & "%' "
        
        End If
    Next
    
    If frmRateCard.optPrinting(1).Value = True Then
        l_strFilter = l_strFilter & " AND NOT (cetacode LIKE 'WW%' OR cetacode LIKE 'SK%' OR cetacode LIKE 'FP%')"
    ElseIf frmRateCard.optPrinting(2).Value = True Then
        l_strFilter = l_strFilter & " AND cetacode LIKE 'WW%' "
    ElseIf frmRateCard.optPrinting(3).Value = True Then
        l_strFilter = l_strFilter & " AND cetacode LIKE 'SK%' "
    ElseIf frmRateCard.optPrinting(4).Value = True Then
        l_strFilter = l_strFilter & " AND cetacode LIKE 'FP%' "
    ElseIf frmRateCard.optPrinting(5).Value = True Then
        l_strFilter = l_strFilter & " AND NOT (cetacode LIKE 'WW%' OR cetacode LIKE 'SK%' OR cetacode LIKE 'FP%' OR cetacode LIKE 'CLIP%')"
    ElseIf frmRateCard.optPrinting(6).Value = True Then
        l_strFilter = l_strFilter & " AND cetacode LIKE 'DADC%' "
    End If
    
    If UCase(lp_strRatesToShow) = "HIRE" Then
        l_strSQL = "SELECT fd_orderby, ratecardID, companyID, category, cetacode, nominalcode, description, unit, noratecardprice, price0, eachrate, hourrate, halfdayrate, dayrate, weekrate, monthrate, '' AS discount FROM ratecard WHERE " & l_strFilter & " AND companyID = '" & l_lngCompanyID & "' ORDER BY " & frmRateCard.cmbOrderBy.Text & " " & frmRateCard.cmbDirection.Text
    Else
    
        If frmRateCard.cmbOrderBy.Text = "" Then frmRateCard.cmbOrderBy.Text = "nominalcode"
        l_strSQL = "SELECT fd_orderby, ratecardID, companyID, category, cetacode, VDMS_Charge_Code, MPServiceProfileNo, nominalcode, shinecode, DisneyNumber, description, unit, minimumcharge, noratecardprice, price0, price1, price2, price3, price4, price5, price6, price7, price8, price9, '' AS 'discount', includedstocknominalcode, includedstockprice FROM ratecard WHERE " & l_strFilter
        If lp_varCompanyID <> "" Then
            l_strSQL = l_strSQL & " AND companyID = '" & l_lngCompanyID & "'"
        End If
        l_strSQL = l_strSQL & " ORDER BY " & frmRateCard.cmbOrderBy.Text & " " & frmRateCard.cmbDirection.Text
    End If
    
    frmRateCard.adoRateCard.RecordSource = l_strSQL
    frmRateCard.adoRateCard.ConnectionString = g_strConnection
    frmRateCard.adoRateCard.Refresh
    If lp_varCompanyID = "" Then
        frmRateCard.grdRateCard.Columns("companyID").Visible = True
    Else
        frmRateCard.grdRateCard.Columns("companyID").Visible = False
    End If
    
    frmRateCard.adoRateCard.Caption = frmRateCard.adoRateCard.Recordset.RecordCount & " Records"
    frmRateCard.Show
    frmRateCard.ZOrder 0
    
    Screen.MousePointer = vbDefault
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub ShowSettings()
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/showsettings") Then
        Exit Sub
    End If
    frmSettings.Show vbModal
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub ShowUser(lp_lngUserID As Long)
    
    On Error GoTo ShowUser_Error
    
    If Not CheckAccess("/showuser") Then
        Exit Sub
    End If
    
    Dim l_strSQL As String
    Dim l_rstUser As New ADODB.Recordset
    Dim l_strAccesscode As String
    l_strSQL = "SELECT * FROM cetauser WHERE cetauserID = " & lp_lngUserID
    Set l_rstUser = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    ClearFields frmUserInformation
    
    CheckForSQLError
    
    With frmUserInformation
    
        If Not l_rstUser.EOF Then
        
            .chkPasswordNeverExpires.Value = GetFlag(l_rstUser("passwordneverexpires"))
            .txtUserID.Text = Format(l_rstUser("cetauserID"))
            .txtUserName.Text = Format(l_rstUser("username"))
            .txtFullName.Text = Format(l_rstUser("fullname"))
            .txtInitials.Text = Format(l_rstUser("initials"))
            .txtTelephone.Text = Format(l_rstUser("telephone"))
            .txtEmail.Text = Format(l_rstUser("email"))
            .chkListAsOperator.Value = GetFlag(l_rstUser("listasoperator"))
            .chkInvestigateIssues.Value = GetFlag(l_rstUser("investigateissues"))
            .chkSignoffIssues.Value = GetFlag(l_rstUser("signoffissues"))
            .chkListAsMediaServices.Value = GetFlag(l_rstUser("listasmediaservices"))
            '.txtPassword.Text = Format(l_strPassword)
            '.txtConfirm.Text = Format(l_strPassword)
            .txtMaxPOValue.Text = Format(l_rstUser("purchaseorderlimit"))
            .txtAccessCode.Text = Trim(" " & l_rstUser("accesscode"))
            .cmbDepartment.ListIndex = GetListIndex(.cmbDepartment, Format(l_rstUser("department")))
            .cmbDefaultJobTab.ListIndex = GetListIndex(.cmbDefaultJobTab, Format(l_rstUser("defaultjobtab")))
            .cmbDefaultView.ListIndex = GetListIndex(.cmbDefaultView, Format(l_rstUser("defaultview")))
            .cmbDefaultJobType.ListIndex = GetListIndex(.cmbDefaultJobType, Format(l_rstUser("defaultjobtype")))
            .cmbDefaultDisplay.ListIndex = GetListIndex(.cmbDefaultDisplay, Format(l_rstUser("defaultdisplay")))
            .cmbDefaultAllocation.ListIndex = GetListIndex(.cmbDefaultAllocation, Format(l_rstUser("defaultallocation")))
            l_strAccesscode = Trim(" " & l_rstUser("accesscode"))
            .cmbSecurityTemplate.Text = GetData("cetauser", "fullname", "cetauserID", Format(l_rstUser("securitytemplate")))
            .txtJobTitle.Text = Format(l_rstUser("jobtitle"))
            .lblCreatedDate.Caption = Format(l_rstUser("createddate"), "dd/mm/yyyy hh:nn")
            .lblModifiedDate.Caption = Format(l_rstUser("modifieddate"), "dd/mm/yyyy hh:nn")
            .lblLastChangedPasswordDate.Caption = Format(l_rstUser("lastchangedpassworddate"), "dd/mm/yyyy")
            .txtMobileLoginCode.Text = Trim(" " & l_rstUser("mobilelogincode"))
        
        Else
            .chkPasswordNeverExpires.Value = 0
            .cmbDepartment.ListIndex = GetListIndex(.cmbDepartment, "")
            .cmbDefaultJobTab.ListIndex = GetListIndex(.cmbDefaultJobTab, "")
            .cmbDefaultView.ListIndex = GetListIndex(.cmbDefaultView, "")
            .cmbDefaultJobType.ListIndex = GetListIndex(.cmbDefaultJobType, "")
            .cmbDefaultDisplay.ListIndex = GetListIndex(.cmbDefaultDisplay, "")
            .cmbDefaultAllocation.ListIndex = GetListIndex(.cmbDefaultAllocation, "")
        End If
    
    End With
    
    l_rstUser.Close
    Set l_rstUser = Nothing
    
    Dim l_intLoop As Integer
    ' Loop through all controls on form
    For l_intLoop = 0 To frmUserInformation.Controls.Count - 1
        'If the control is a check box
        If TypeOf frmUserInformation.Controls(l_intLoop) Is CheckBox Then
            If frmUserInformation.Controls(l_intLoop).Tag <> "" Then
                If InStr(l_strAccesscode, frmUserInformation.Controls(l_intLoop).Tag) <> 0 Then
                    frmUserInformation.Controls(l_intLoop).Value = 1
                Else
                    frmUserInformation.Controls(l_intLoop).Value = 0
                End If
            End If
        End If
    Next
    
    l_strSQL = "SELECT logindate AS 'Login', logoutdate AS 'Logout', workstation AS 'PC' FROM session WHERE userID = '" & lp_lngUserID & "' ORDER BY logindate DESC;"
    frmUserInformation.adoSessions.ConnectionString = g_strConnection
    frmUserInformation.adoSessions.RecordSource = l_strSQL
    frmUserInformation.adoSessions.Refresh
    
    l_strSQL = "SELECT count(userID) FROM session WHERE logindate IS NOT NULL AND logoutdate IS NOT NULL AND userID = '" & lp_lngUserID & "'"
    frmUserInformation.lblSessions.Caption = GetCount(l_strSQL) & " completed sessions."
    
    frmUserInformation.grdSession.Columns(0).Width = 1900
    frmUserInformation.grdSession.Columns(1).Width = 1900
    frmUserInformation.grdSession.Columns(2).Width = 1900
    
    Exit Sub
ShowUser_Error:
    
    MsgBox "Could not display user. The error received was: " & vbCrLf & vbCrLf & g_strExecuteError, vbExclamation
    
End Sub

Sub ShowUserInformation()
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/showuserinformation") Then
        Exit Sub
    End If
    frmUserInformation.Show
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Function SnapTime(lp_datDateToSnap)
    
    ' SnapTime = DateToSnap
    ' Exit Function
    '*****************************************
    ' Snap Time Function
    ' Purpose : Snaps any date to nearest 15
    '           min interval Rounds up / down etc
    '*****************************************
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim Current As Integer
    Dim Mins As Integer
    
    Current = Minute(lp_datDateToSnap)
    If Current = 0 Or Current = 15 Or Current = 30 Then
        Mins = 0
    ElseIf Current >= 1 And Current <= 14 Then
        Mins = 15 - Current
    ElseIf Current >= 16 And Current <= 29 Then
        Mins = 30 - Current
    ElseIf Current >= 31 And Current <= 44 Then
        Mins = 45 - Current
    ElseIf Current >= 46 And Current <= 59 Then
        Mins = 60 - Current
    End If
    
    SnapTime = DateAdd("n", Mins, lp_datDateToSnap)
    
    
    'Current = Second(lp_datDateToSnap)
    
    
    'SnapTime = DateAdd("s", -Current, lp_datDateToSnap)
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function SundryExists(l_lngJobID As Long, lp_strCostCode As String) As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_rstSundry As New ADODB.Recordset
    Set l_rstSundry = ExecuteSQL("select * from costing where jobID = '" & l_lngJobID & "' AND copytype = 'X' AND costcode = '" & lp_strCostCode & "';", g_strExecuteError)
    If l_rstSundry.EOF = True Or l_rstSundry.BOF = True Then
        SundryExists = False
    Else
        SundryExists = True
    End If
    l_rstSundry.Close
    Set l_rstSundry = Nothing
    
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub SystemLockedMessage()
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    'Msgbox "The CETA system has been locked, and can not be used at the moment. Please try again later.", vbSystemModal + vbExclamation
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub Time_check(Control As Control, KeyAscii As Integer)

If KeyAscii = 46 Then KeyAscii = 58
If Control.ActiveCell.SelStart = 9 And KeyAscii <> 8 Then
    KeyAscii = 0
    Exit Sub
End If

If Control.ActiveCell.SelLength > 1 And KeyAscii = 8 Then
    KeyAscii = 0
    Exit Sub
End If

If Control.ActiveCell.SelStart = 0 And KeyAscii = 8 Then Exit Sub

Select Case Control.ActiveCell.SelStart 'Position Control of formating of Timecode.

Case 2 And KeyAscii <> 8, 5 And KeyAscii <> 8
        
    Control.ActiveCell.SelStart = Control.ActiveCell.SelStart + 1
    If KeyAscii = Asc(":") Then
        KeyAscii = 0
    Else
        Control.ActiveCell.SelLength = 1
    End If

Case Else
    If KeyAscii = 58 Then KeyAscii = 0: Exit Sub
    
    If KeyAscii = 8 And Control.ActiveCell.SelStart <> 0 Then
        Select Case Control.ActiveCell.SelStart
            Case 3, 6
                Control.ActiveCell.SelStart = Control.ActiveCell.SelStart - 1
                KeyAscii = 0
                Exit Sub
            End Select
            Control.ActiveCell.SelStart = Control.ActiveCell.SelStart - 1
            Control.ActiveCell.SelLength = 1
            Control.ActiveCell.SelText = "0"
            Control.ActiveCell.SelStart = Control.ActiveCell.SelStart - 1
            Control.ActiveCell.SelLength = 0
            KeyAscii = 0
            Exit Sub
    End If
    Control.ActiveCell.SelLength = 1
End Select

Select Case KeyAscii
    Case 48 To 57   'VALIDATE THE TIME CODE.
        Select Case Control.ActiveCell.SelStart
            Case 0  ' Check 1st digit of Hours is not > 2
                If KeyAscii > 50 Then Beep: KeyAscii = 0: Exit Sub

            Case 1  ' Check 2nd digit of Hours is not > 3 if 20 something hours.
                If Val(Mid$(Control.ActiveCell.Text, 1, 1)) = 2 Then
                    If KeyAscii > 51 Then Beep: KeyAscii = 0: Exit Sub
                End If
            Case 3  ' Check Minutes
                If KeyAscii > 53 Then Beep: KeyAscii = 0: Exit Sub
            Case 6  ' Check Seconds
                If KeyAscii > 53 Then Beep: KeyAscii = 0: Exit Sub

        End Select
    
    Case 58, 8
    Case Else
        KeyAscii = 0
        Exit Sub
End Select

End Sub

Public Sub UpdateJobStatus(ByVal lp_lngJobID As Long, ByVal lp_strStatus As String, Optional ByVal lp_lngResourceScheduleID As Long, Optional lp_blnDontPrompt As Boolean, Optional lp_blnDateAlreadySet As Boolean)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strOriginalJobStatus  As String, l_strRevert As String
    Dim l_intMessage As Integer
    Dim l_strPassword As String
    Dim l_strEmailTo As String, l_strEmailBody As String, l_lngBookedBy As Long, l_strEmailCC As String, l_lngCompanyID As Long
    
    l_strOriginalJobStatus = GetData("job", "fd_status", "jobID", lp_lngJobID)
    
    'unless you are in the accounts department, you can not edit a "sent to accounts" job
    If l_strOriginalJobStatus = "Sent To Accounts" And lp_strStatus <> "Sent To Accounts" Then
        If GetData("cetauser", "department", "initials", g_strUserInitials) <> "Accounts" And CheckAccess("/superuser", True) = False Then
            MsgBox "This job (ID# " & lp_lngJobID & ") has already been sent to accounts. You can not modify it as it may have been invoiced.", vbCritical
            Exit Sub
        End If
    End If
    
    If GetStatusNumber(l_strOriginalJobStatus) > GetStatusNumber(lp_strStatus) And lp_blnDontPrompt <> True Then
        'this job's status has been set back.
        
        'if this isnt a confirmed being changed to a 2nd pencil....
        If Not (l_strOriginalJobStatus = "Completed" And lp_strStatus = "Confirmed") And Not (l_strOriginalJobStatus = "Completed" And lp_strStatus = "Masters Here") And Not (l_strOriginalJobStatus = "Masters Here" And lp_strStatus = "Confirmed") And Not (l_strOriginalJobStatus = "Confirmed" And lp_strStatus = "Submitted") And Not (l_strOriginalJobStatus = "Pencil" And lp_strStatus = "2nd Pencil") And Not (l_strOriginalJobStatus = "Submitted" And lp_strStatus = "Pencil") Then
            If Not CheckAccess("/revertjobstatus") Then Exit Sub
        End If
        
        l_intMessage = MsgBox("Are you sure you want to revert the status of this job (ID# " & lp_lngJobID & ")?", vbQuestion + vbYesNo)
        If l_intMessage = vbNo Then Exit Sub
        l_strRevert = " (Revert)"
    Else
        If l_strOriginalJobStatus = "Quick" And lp_strStatus <> "Cancelled" Then
            'quick bookings can ONLY go to pencil
            Dim l_strReason As String
            If CheckJobBasics(lp_lngJobID, l_strReason) = False Then
                MsgBox "You can not change this job (ID# " & lp_lngJobID & ") status as" & l_strReason, vbExclamation
                Exit Sub
            End If
            lp_strStatus = "Pencil"
            l_intMessage = MsgBox("You can only make a quick booking in to a pencil." & vbCrLf & vbCrLf & "Do you want to do this?", vbOKCancel + vbQuestion)
            If l_intMessage = vbCancel Then Exit Sub
        
        Else
            'check to see if the user is jumping over a stage - ie. pencil to complete
            If UCase(GetData("job", "jobtype", "jobID", lp_lngJobID)) <> "HIRE" And UCase(GetData("job", "jobtype", "jobID", lp_lngJobID)) <> "EDIT" Then
                If GetStatusNumber(l_strOriginalJobStatus) <> GetStatusNumber(lp_strStatus) - 1 And GetStatusNumber(l_strOriginalJobStatus) <> GetStatusNumber(lp_strStatus) And lp_strStatus <> "Cancelled" And lp_blnDontPrompt <> True Then
                    If g_intAllowBigStatusJumps <> 1 Then
                        MsgBox "You can not move this job (ID# " & lp_lngJobID & ") to this status, as it has not yet gone through the previous stage", vbExclamation
                        Exit Sub
                    End If
                End If
            End If
        End If
    End If
    
    Select Case LCase(lp_strStatus)
    Case "confirmed"
        If LCase(g_strWhenToAllocate) = "confirm job" Then
        
            Dim l_strMessage As String
            'check job can be confirmed
            If Not CanConfirmJob(lp_lngJobID, l_strMessage) Then
                
                MsgBox l_strMessage, vbExclamation
                Exit Sub
                
            End If
            
           AllocateProjectNumber lp_lngJobID
    
        End If
     
        'send an email to the person this job is assigned to
        CheckAllocationEmail lp_lngJobID
     
    Case "hold cost"
    
        SetData "job", "holdcostdate", "jobID", lp_lngJobID, FormatSQLDate(Now)
        SetData "job", "holdcostuser", "jobID", lp_lngJobID, g_strUserInitials
            
    Case "costed"
    
        SetData "job", "costeddate", "jobID", lp_lngJobID, FormatSQLDate(Now)
        SetData "job", "costeduser", "jobID", lp_lngJobID, g_strUserInitials
        
    Case "sent to accounts"
        
        SetData "job", "senttoaccountsuser", "jobID", lp_lngJobID, g_strUserInitials
        If lp_blnDateAlreadySet <> True Then
            SetData "job", "vtendtime", "jobID", lp_lngJobID, FormatSQLDate(Now)
            SetData "job", "vtenduser", "jobID", lp_lngJobID, g_strUserInitials
        End If
        
    Case "vt done"
        
        SetData "job", "vtendtime", "jobID", lp_lngJobID, FormatSQLDate(Now)
        l_lngBookedBy = GetData("job", "createduserID", "jobID", lp_lngJobID)
        l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
        'Send iemail to the person who booked the job
        l_strEmailTo = GetData("cetauser", "email", "cetauserID", l_lngBookedBy)
        l_strEmailCC = "uk-mediaservices-VIE@visualdatamedia.com"
        If GetData("company", "isTVDistributionTeam", "companyID", l_lngCompanyID) <> 0 Then l_strEmailCC = l_strEmailCC & IIf(l_strEmailCC <> "", ";", "") & "uk_tv_distribution-VIE@visualdatamedia.com"
        If GetData("company", "isIndependentsTeam", "companyID", l_lngCompanyID) <> 0 Then l_strEmailCC = l_strEmailCC & IIf(l_strEmailCC <> "", ";", "") & "uk_tv_independents-VIE@visualdatamedia.com"
        If GetData("company", "isStudiosTeam", "companyID", l_lngCompanyID) <> 0 Then l_strEmailCC = l_strEmailCC & IIf(l_strEmailCC <> "", ";", "") & "uk_tv_studios-VIE@visualdatamedia.com"
        l_strEmailCC = l_strEmailCC & IIf(l_strEmailCC <> "", ";", "") & "uk-mediaservices-VIE@visualdatamedia.com"
        l_strEmailBody = "Job: " & lp_lngJobID & vbCrLf
        l_strEmailBody = l_strEmailBody & "Client: " & GetData("job", "companyname", "jobID", lp_lngJobID) & vbCrLf
        l_strEmailBody = l_strEmailBody & "P/O #: " & GetData("job", "orderreference", "jobID", lp_lngJobID) & vbCrLf
        l_strEmailBody = l_strEmailBody & "Title: " & GetData("job", "title1", "jobID", lp_lngJobID) & vbCrLf
        l_strEmailBody = l_strEmailBody & "Subtitle: " & GetData("job", "title2", "jobID", lp_lngJobID) & vbCrLf
        l_strEmailBody = l_strEmailBody & "Deadline: " & Format(GetData("job", "deadlinedate", "jobID", lp_lngJobID), "YYYY-MM-DD HH:NN:SS") & vbCrLf
        l_strEmailBody = l_strEmailBody & "Client Contact: " & GetData("job", "contactname", "jobID", lp_lngJobID) & vbCrLf
        l_strEmailBody = l_strEmailBody & lp_strStatus & " by: " & g_strFullUserName & vbCrLf
        l_strEmailBody = l_strEmailBody & "Jobsheet Created by: " & GetData("cetauser", "fullname", "cetauserID", GetData("job", "createduserID", "jobID", lp_lngJobID)) & vbCrLf
        SendSMTPMail l_strEmailTo, "", "Job: " & lp_lngJobID & " for: " & GetData("company", "name", "companyID", l_lngCompanyID) & " marked " & lp_strStatus & " by " & g_strFullUserName, "", l_strEmailBody, True, l_strEmailCC, "", g_strAdministratorEmailAddress, True
        
    Case "pending"
        l_lngBookedBy = GetData("job", "createduserID", "jobID", lp_lngJobID)
        l_strEmailTo = GetData("cetauser", "email", "cetauserID", l_lngBookedBy)
        If l_strEmailTo = "" Then l_strEmailTo = g_strBookingsEmailAddress
        'Check the MX1 contact email for the company, and if it exists send email to them too
        If GetData("company", "jcacontactemail", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID)) <> "" Then
            l_strEmailCC = GetData("company", "jcacontactemail", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID))
        Else
            l_strEmailCC = ""
        End If
        l_strEmailBody = "Job: " & lp_lngJobID & vbCrLf & "Client: " & GetData("job", "companyname", "jobID", lp_lngJobID) & vbCrLf & "Title: " _
            & GetData("job", "title1", "jobID", lp_lngJobID) & vbCrLf & lp_strStatus & " by: " & g_strFullUserName
        frmTextEdit.Caption = "Please give the reason that the job has been rejected"
        frmTextEdit.txtTextToEdit.Text = ""
        frmTextEdit.Show vbModal
        If frmTextEdit.txtTextToEdit.Text <> "" Then
            l_strEmailBody = l_strEmailBody & vbCrLf & vbCrLf & frmTextEdit.txtTextToEdit.Text
        End If
        SendSMTPMail l_strEmailTo, "", "Job: " & lp_lngJobID & " has been marked " & lp_strStatus & " by " & g_strFullUserName, "", l_strEmailBody, True, l_strEmailCC, "", g_strAdministratorEmailAddress & ";" & g_strManagerEmailAddress, True
        Unload frmTextEdit
    
    End Select
    
    '##########################################################################################
    
    Dim l_strSQL As String
    l_strSQL = "UPDATE job SET fd_status = '" & lp_strStatus & "' WHERE jobID = '" & lp_lngJobID & "';"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    If UCase(lp_strStatus) <> "CANCELLED" Then
        l_strSQL = "UPDATE job SET cancelleddate = NULL, cancelleduser = NULL WHERE jobID = '" & lp_lngJobID & "';"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    End If
    
    
    'now loop through all the resource schedule items, and if not conflicting, update them too
    
    Dim l_rstResourceSchedule As New ADODB.Recordset
    
    l_strSQL = "SELECT resourcescheduleID, starttime, endtime, fd_status, resourceID FROM resourceschedule WHERE jobID = '" & lp_lngJobID & "' ORDER BY resourcescheduleID;"
    
    Set l_rstResourceSchedule = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    If Not l_rstResourceSchedule.EOF Then
        l_rstResourceSchedule.MoveFirst
    End If
    
    Dim l_lngResourceScheduleID As Long
    
    'dont prompt for conflicts when the job is being confirmed....???
    If GetStatusNumber(lp_strStatus) >= 2 Then lp_blnDontPrompt = True
    
    Do While Not l_rstResourceSchedule.EOF
        
        l_lngResourceScheduleID = l_rstResourceSchedule("resourcescheduleID")
        
        Dim l_strCurrentResourceStatus As String
        
        l_strCurrentResourceStatus = GetData("resourceschedule", "fd_status", "resourcescheduleID", l_lngResourceScheduleID)
        
        If UCase(lp_strStatus) <> "PREPARED" Then
             UpdateResourceScheduleStatus l_lngResourceScheduleID, lp_strStatus, lp_blnDontPrompt
        Else
        
            If UCase(l_strCurrentResourceStatus) <> "RETURNED" Then UpdateResourceScheduleStatus l_lngResourceScheduleID, lp_strStatus, lp_blnDontPrompt
        End If
        
        l_rstResourceSchedule.MoveNext
    Loop
    
    UpdateDespatchForJob lp_lngJobID

    
    'add a history line to the jobn
    AddJobHistory lp_lngJobID, lp_strStatus & l_strRevert
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:

    l_rstResourceSchedule.Close
    Set l_rstResourceSchedule = Nothing
    
    
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub CheckAllocationEmail(ByVal lp_lngJobID As Long)

'do we want to notify anyone about this new booking?
            
Dim l_strEmail As String
Dim l_strAllocatedTo  As String
Dim l_strCompanyName As String
Dim l_strKitList As String
Dim l_datJobStartTime As Date


l_strAllocatedTo = GetData("job", "allocatedto", "jobID", lp_lngJobID)
l_strCompanyName = GetData("job", "companyname", "jobID", lp_lngJobID)
l_datJobStartTime = GetData("job", "startdate", "jobID", lp_lngJobID)
l_strKitList = GetJobDescription(lp_lngJobID)


If l_strAllocatedTo = "" Then Exit Sub

l_strEmail = "Dear " & l_strAllocatedTo & ","
l_strEmail = l_strEmail & vbCrLf
l_strEmail = l_strEmail & vbCrLf
l_strEmail = l_strEmail & "A job has been confirmed and allocated to you."
l_strEmail = l_strEmail & vbCrLf
l_strEmail = l_strEmail & vbCrLf
l_strEmail = l_strEmail & "Company:    " & l_strCompanyName
l_strEmail = l_strEmail & vbCrLf
l_strEmail = l_strEmail & "Job ID:     " & lp_lngJobID
l_strEmail = l_strEmail & vbCrLf
l_strEmail = l_strEmail & "Start Time: " & l_datJobStartTime
l_strEmail = l_strEmail & vbCrLf
l_strEmail = l_strEmail & vbCrLf
l_strEmail = l_strEmail & "Kit List:"
l_strEmail = l_strEmail & vbCrLf
l_strEmail = l_strEmail & vbCrLf & vbTab
l_strEmail = l_strEmail & Replace(l_strKitList, " / ", vbCrLf & vbTab)

Dim l_strEmailAddress As String
l_strEmailAddress = GetData("cetauser", "email", "fullname", l_strAllocatedTo)

If l_strEmailAddress <> "" Then SendSMTPMail l_strEmailAddress, l_strAllocatedTo, l_strAllocatedTo & " - New CFM Job Allocation", "", l_strEmail, False, g_strUserEmailAddress, g_strFullUserName




End Sub

Sub UpdateResourceScheduleStatus(lp_lngResourceScheduleID As Long, lp_strStatus As String, Optional lp_blnDontPrompt As Boolean)

Dim l_datStartTime As Variant
Dim l_datEndTime As Variant
Dim l_strStatus As String
Dim l_lngResourceID As Long
Dim l_lngJobID As Long

l_datStartTime = GetData("resourceschedule", "starttime", "resourcescheduleID", lp_lngResourceScheduleID)
l_datEndTime = GetData("resourceschedule", "endtime", "resourcescheduleID", lp_lngResourceScheduleID)
l_lngResourceID = GetData("resourceschedule", "resourceID", "resourcescheduleID", lp_lngResourceScheduleID)
l_lngJobID = GetData("resourceschedule", "jobID", "resourcescheduleID", lp_lngResourceScheduleID)
l_strStatus = lp_strStatus

If l_lngResourceID <> 0 Then
If IsDate(l_datStartTime) = True And IsDate(l_datEndTime) = True And GetStatusNumber(l_strStatus) < 3 Then
    If IsConflict(l_lngJobID, l_lngResourceID, CDate(l_datStartTime), CDate(l_datEndTime), True) And (lp_blnDontPrompt <> True) Then
        'do something here to try and resolve the conflict!
        'otherwise, as a last resort, second pencil the item
        'conflict found - prompt for action (for now, just 2nd pencil)
        
        Dim l_intMsg As Integer
        l_intMsg = NewMsgBox("Conflicting Resource Schedule", "A resource you are scheduling (" & GetData("resource", "name", "resourceID", l_lngResourceID) & ") is conflicting with another job." & vbCrLf & vbCrLf & "Do you want to make it a second pencil?", "Yes - 2nd Pencil", "No - Leave as is", "Cancel")
        
        Select Case l_intMsg
        Case 0
            l_strStatus = "2nd Pencil"

        Case 1

        Case 2
            GoTo PROC_EXIT
        End Select
    End If
End If
End If
SetData "resourceschedule", "fd_status", "resourcescheduleID", lp_lngResourceScheduleID, l_strStatus


    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    
    
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd

End Sub


Sub UpdateProjectNumbersInDespatch(lp_lngJobID As Long)

    Dim l_lngProjectID As Long, l_lngProjectNumber  As Long
    
    'work out the correct project number etc
    l_lngProjectID = GetData("job", "projectID", "jobID", lp_lngJobID)
    l_lngProjectNumber = GetData("job", "projectnumber", "jobID", lp_lngJobID)
    
    Dim l_strSQL As String
    l_strSQL = "UPDATE despatch SET projectID = '" & l_lngProjectID & "', projectnumber = '" & l_lngProjectNumber & "' WHERE jobID = '" & lp_lngJobID & "';"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
   
   
End Sub

Public Sub UpdateSundryCostings(l_lngJobID As Long, l_lngCompanyID As Long)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If Not CheckAccess("/collectsundrycosts") Then Exit Sub
    
    
    Dim l_rstSundry As New ADODB.Recordset
    Dim l_strSQL As String
    Dim l_sngVATRate As Single
    Dim l_dblTotal As Double
    Dim l_dblVAT As Double
    Dim l_dblIncVAT As Double
    Dim l_strCategory As String
    Dim l_strRateCardOrderBy As String
    
    l_strSQL = "SELECT SUM(amount), sundrytype FROM sundrycost WHERE jobID = '" & l_lngJobID & "' and authoriseduser <> '' GROUP BY sundrytype;"
    
    Set l_rstSundry = ExecuteSQL(l_strSQL, g_strExecuteError)
    
    If Not l_rstSundry.EOF Then l_rstSundry.MoveFirst
    
    Do While Not l_rstSundry.EOF
        
        l_sngVATRate = GetVATRate(Format(GetData("company", "vatcode", "companyID", l_lngCompanyID)))
        
        l_dblTotal = l_rstSundry(0)
        
        If g_intSundryMarkup <> 0 Then
            l_dblTotal = l_dblTotal + ((l_dblTotal / 100) * g_intSundryMarkup)
        End If
        
        l_dblTotal = RoundToCurrency(l_dblTotal)
        
        l_dblVAT = l_dblTotal * ((l_sngVATRate / 100))
        l_dblVAT = RoundToCurrency(l_dblVAT)
        
        l_dblIncVAT = l_dblTotal + l_dblVAT
        
        Dim l_strNominalcode As String
        Dim l_strDescription As String
        Dim l_strCostCode As String
        
        l_strCostCode = GetDataSQL("SELECT TOP 1 format FROM xref WHERE category = 'sundrytype' AND description = '" & Format(l_rstSundry("sundrytype")) & "';")
        
        l_strNominalcode = GetData("ratecard", "nominalcode", "cetacode", l_strCostCode)
        l_strDescription = GetData("ratecard", "description", "cetacode", l_strCostCode)
        l_strCategory = GetData("ratecard", "category", "cetacode", l_strCostCode)
        l_strRateCardOrderBy = GetData("ratecard", "fd_orderby", "cetacode", l_strCostCode)
        
        If SundryExists(l_lngJobID, l_strCostCode) = False Then
            l_strSQL = "INSERT INTO costing (copytype, jobID, costcode, quantity, unitcharge, total, vat, totalincludingvat, fd_time, description, accountcode, ratecardprice, ratecardtotal, ratecardcategory, ratecardorderby) VALUES ('X', '" & l_lngJobID & "','" & l_strCostCode & "','1','" & l_dblTotal & "','" & l_dblTotal & "','" & l_dblVAT & "','" & l_dblIncVAT & "','1','" & l_strDescription & "','" & l_strNominalcode & "', '" & l_dblTotal & "', '" & l_dblTotal & "', '" & l_strCategory & "', '" & l_strRateCardOrderBy & "');"
        Else
            l_strSQL = "UPDATE costing SET copytype = 'X', unitcharge = '" & l_dblTotal & "', total = '" & l_dblTotal & "', vat = '" & l_dblVAT & "', totalincludingvat = '" & l_dblIncVAT & "', fd_time = '1', description = '" & l_strDescription & "', accountcode = '" & l_strNominalcode & "', ratecardprice = '" & l_dblTotal & "', ratecardtotal = '" & l_dblTotal & "', ratecardcategory = '" & l_strCategory & "', ratecardorderby = '" & l_strRateCardOrderBy & "' WHERE jobID = '" & l_lngJobID & "' AND costcode = '" & l_strCostCode & "';"
        End If
        
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        l_rstSundry.MoveNext
    Loop
    
    l_rstSundry.Close
    Set l_rstSundry = Nothing
        
    SetData "job", "flagnewsundries", "jobID", l_lngJobID, "0"
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub
Function CanConfirmJob(ByVal lp_lngJobID As Long, ByRef lp_strReason As String) As Boolean

'in order to confirm a job, there must be certain things done...

'1. check that there are no 2nd pencil bookings
Dim l_strSQL As String

l_strSQL = "SELECT COUNT(resourcescheduleID) FROM resourceschedule WHERE jobID = '" & lp_lngJobID & "' AND fd_status = '2nd Pencil';"

Dim l_lngCount As Long
l_lngCount = GetCount(l_strSQL)

lp_strReason = "You can not confirm this job (ID# " & lp_lngJobID & ") because"

If l_lngCount > 0 Then
    CanConfirmJob = False
    lp_strReason = lp_strReason & " there are still 2nd Pencil resource schedules"
    Exit Function
End If

If CheckJobBasics(lp_lngJobID, lp_strReason) = False Then
    Exit Function
End If

If GetData("job", "jobtype", "jobID", lp_lngJobID) = "" Or GetData("job", "jobtype", "jobID", lp_lngJobID) = "Quickie" Then
    CanConfirmJob = False
    lp_strReason = lp_strReason & " the job type is invalid"
    Exit Function
End If

Dim l_lngCompanyID As Long
l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)

Dim l_strClientStatus As String
l_strClientStatus = GetData("company", "accountstatus", "companyID", l_lngCompanyID) '

If UCase(l_strClientStatus) = "HOLD" Then
    lp_strReason = lp_strReason & " this account is currently set to " & l_strClientStatus & "."
    Exit Function
End If

If g_optNeedAccountNumberToConfirm = 1 Then
    Dim l_strClientAccountNumber As String
    l_strClientAccountNumber = GetData("company", "accountcode", "companyID", l_lngCompanyID) '
    If UCase(l_strClientAccountNumber) = "" Then
    lp_strReason = lp_strReason & " the company has no account number."
        Exit Function
    End If
End If

Dim l_strJobType As String
l_strJobType = UCase(GetData("job", "jobtype", "jobID", lp_lngJobID))


'do they want to force users to complete the methods? only for hire and edit jobs
If g_optRequireDeliveryMethodsOnHireAndEditJobs And (l_strJobType = "EDIT" Or l_strJobType = "HIRE") Then

    Dim l_strOutGoingDeliveryMethod As String
    Dim l_strIncomingDeliveryMethod As String

    l_strIncomingDeliveryMethod = GetData("job", "IncomingDespatchMethod", "jobID", lp_lngJobID)
    l_strOutGoingDeliveryMethod = GetData("job", "OutgoingDespatchMethod", "jobID", lp_lngJobID)

    If l_strOutGoingDeliveryMethod = "" Then
        lp_strReason = lp_strReason & " you have not specified the outgoing delivery method"
        Exit Function
    End If

    If l_strIncomingDeliveryMethod = "" Then
        lp_strReason = lp_strReason & " you have not specified the incoming delivery method"
        Exit Function
    End If

End If

CanConfirmJob = True

Exit Function

End Function



Function CheckJobBasics(ByVal lp_lngJobID As Long, lp_strReason As String) As Boolean

If GetData("job", "companyID", "jobID", lp_lngJobID) = 0 Then
    CheckJobBasics = False
    lp_strReason = lp_strReason & " there is no selected company"
    Exit Function
End If

If GetData("job", "contactID", "jobID", lp_lngJobID) = 0 Then
    CheckJobBasics = False
    lp_strReason = lp_strReason & " there is no selected contact"
    Exit Function
End If

If g_optRequireProductBeforeSaving <> 0 And GetData("job", "productID", "jobID", lp_lngJobID) = 0 Then
    CheckJobBasics = False
    lp_strReason = lp_strReason & " there is no selected product"
    Exit Function
End If

If GetData("job", "joballocation", "jobID", lp_lngJobID) = "" Then
    CheckJobBasics = False
    lp_strReason = lp_strReason & " the project type is invalid"
    Exit Function
End If

CheckJobBasics = True

End Function

Sub UpdateTemporaryScheduleTimes(lp_datDragStartTime As Date, lp_datDragEndTime As Date)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    l_strSQL = "UPDATE temporaryresourceschedule SET starttime = '" & FormatSQLDate(lp_datDragStartTime) & "', endtime = '" & FormatSQLDate(lp_datDragEndTime) & "' WHERE createduser = '" & g_strUserInitials & "'"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub


Function MightBeDate(ByVal lp_string As Variant) As String

Dim l_String As String, l_strER As String
l_String = lp_string

If UCase(l_String) Like "*ER" Then
    l_String = Left(l_String, Len(l_String) - 2)
    l_strER = " ER"
Else
    l_strER = ""
End If

If IsDate(l_String) Then
    MightBeDate = ConvertDate(l_String) & l_strER
Else
    MightBeDate = l_String & l_strER
End If

End Function

Public Function ConvertDate(ByVal l_Date As String)

    If l_Date <> "" Then
        ConvertDate = Trim(Day(l_Date) & " " & Left(MonthName(Month(l_Date)), 3) & " " & Right(Year(l_Date), 2))
    End If
      
End Function

Public Function IsProgramReadyToWrap(lp_strTranscoded As String, lp_varEditNeeded As Variant, lp_strEditPassed As String, lp_strEnglisharrived As String, _
    lp_strHungarianarrived As String, lp_strPolisharrived As String, lp_strRomanianarrived As String, lp_strTurkisharrived As String, _
    lp_strRussianarrived As String, lp_strArabicarrived As String, lp_strSpanisharrived As String, lp_strPortugesearrived As String, _
    lp_strGermanarrived As String, lp_strNorwegianarrived As String, lp_strSwedisharrived As String, lp_strFrencharrived As String, _
    lp_strDanisharrived As String, lp_strItalianarrived As String, lp_strEnglishmonoarrived As String, lp_strCroatianarrived As String, _
    lp_strSerbianarrived As String, lp_strCzecharrived As String, lp_strCantonesearrived As String, lp_strMandarinarrived As String, _
    lp_strHebrewarrived As String, lp_strGreekarrived As String, lp_strVietnamesearrived As String, lp_strLatamportarrived As String, _
    lp_strLatamspaarrived As String, lp_strMacedonianarrived As String, lp_strDutcharrived As String, lp_strIndonesianarrived As String, lp_lngCompanyID As Long) As String

Dim l_lngCompanyID As Long
Dim l_rstLanguageSet As ADODB.Recordset
Dim l_strSQL As String

IsProgramReadyToWrap = "YES"

'Check the transcoded field
If Not TestStatus(lp_strTranscoded) Then IsProgramReadyToWrap = "NO"

'Check the Edit fields

If Not IsNull(lp_varEditNeeded) And lp_varEditNeeded <> 0 Then
    If Not TestStatus(lp_strEditPassed) Then IsProgramReadyToWrap = "NO"
End If
    
l_strSQL = "SELECT language FROM trackerlanguageset WHERE companyID = " & lp_lngCompanyID & ";"
Set l_rstLanguageSet = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If Not l_rstLanguageSet.EOF Then
    l_rstLanguageSet.MoveFirst
    Do While Not l_rstLanguageSet.EOF
        Select Case l_rstLanguageSet("language")
            Case "English"
                If Not TestStatus(lp_strEnglisharrived) Then IsProgramReadyToWrap = "NO"
            Case "Polish"
                If Not TestStatus(lp_strPolisharrived) Then IsProgramReadyToWrap = "NO"
            Case "Hungarian"
                If Not TestStatus(lp_strHungarianarrived) Then IsProgramReadyToWrap = "NO"
            Case "Romanian"
                If Not TestStatus(lp_strRomanianarrived) Then IsProgramReadyToWrap = "NO"
            Case "Turkish"
                If Not TestStatus(lp_strTurkisharrived) Then IsProgramReadyToWrap = "NO"
            Case "Russian"
                If Not TestStatus(lp_strRussianarrived) Then IsProgramReadyToWrap = "NO"
            Case "Arabic"
                If Not TestStatus(lp_strArabicarrived) Then IsProgramReadyToWrap = "NO"
            Case "Castilian Spanish"
                If Not TestStatus(lp_strSpanisharrived) Then IsProgramReadyToWrap = "NO"
            Case "Portugese"
                If Not TestStatus(lp_strPortugesearrived) Then IsProgramReadyToWrap = "NO"
            Case "German"
                If Not TestStatus(lp_strGermanarrived) Then IsProgramReadyToWrap = "NO"
            Case "Norwegian"
                If Not TestStatus(lp_strNorwegianarrived) Then IsProgramReadyToWrap = "NO"
            Case "Swedish"
                If Not TestStatus(lp_strSwedisharrived) Then IsProgramReadyToWrap = "NO"
            Case "French"
                If Not TestStatus(lp_strFrencharrived) Then IsProgramReadyToWrap = "NO"
            Case "Danish"
                If Not TestStatus(lp_strDanisharrived) Then IsProgramReadyToWrap = "NO"
            Case "Italian"
                If Not TestStatus(lp_strItalianarrived) Then IsProgramReadyToWrap = "NO"
            Case "English (Mono)"
                If Not TestStatus(lp_strEnglishmonoarrived) Then IsProgramReadyToWrap = "NO"
            Case "Croatian"
                If Not TestStatus(lp_strCroatianarrived) Then IsProgramReadyToWrap = "NO"
            Case "Serbian"
                If Not TestStatus(lp_strSerbianarrived) Then IsProgramReadyToWrap = "NO"
            Case "Czech"
                If Not TestStatus(lp_strCzecharrived) Then IsProgramReadyToWrap = "NO"
            Case "Cantonese"
                If Not TestStatus(lp_strCantonesearrived) Then IsProgramReadyToWrap = "NO"
            Case "Mandarin"
                If Not TestStatus(lp_strMandarinarrived) Then IsProgramReadyToWrap = "NO"
            Case "Hebrew"
                If Not TestStatus(lp_strHebrewarrived) Then IsProgramReadyToWrap = "NO"
            Case "Greek"
                If Not TestStatus(lp_strGreekarrived) Then IsProgramReadyToWrap = "NO"
            Case "Vietnamese"
                If Not TestStatus(lp_strVietnamesearrived) Then IsProgramReadyToWrap = "NO"
            Case "La-Am Portugese"
                If Not TestStatus(lp_strLatamportarrived) Then IsProgramReadyToWrap = "NO"
            Case "La-Am Spanish"
                If Not TestStatus(lp_strLatamspaarrived) Then IsProgramReadyToWrap = "NO"
            Case "Macedonian"
                If Not TestStatus(lp_strMacedonianarrived) Then IsProgramReadyToWrap = "NO"
            Case "Dutch"
                If Not TestStatus(lp_strDutcharrived) Then IsProgramReadyToWrap = "NO"
            Case "Indonesian"
                If Not TestStatus(lp_strIndonesianarrived) Then IsProgramReadyToWrap = "NO"
        End Select
        
        l_rstLanguageSet.MoveNext
    Loop
End If
l_rstLanguageSet.Close
Set l_rstLanguageSet = Nothing
    
End Function

Public Function TestStatus(lp_varQueryfield As Variant) As Boolean

TestStatus = True

If lp_varQueryfield = "" Then TestStatus = False
If UCase(lp_varQueryfield) Like "*ER" Then TestStatus = False

End Function

Public Function ValidateEmail(ByVal lp_strEmail As String) As Boolean
    
    Dim l_expValidEmailExpression As RegExp
    Set l_expValidEmailExpression = New RegExp
'    l_expValidEmailExpression.Pattern = "^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*\s+<(\w[-._\w]*\w@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
    l_expValidEmailExpression.Pattern = "^[a-zA-Z0-9][a-zA-Z0-9'\.\_-]*[a-zA-Z0-9]*@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
    l_expValidEmailExpression.Pattern = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,24}|[0-9]{1,3})(\]?)$"
    If l_expValidEmailExpression.Test(lp_strEmail) = True Then
        ValidateEmail = True
    Else
        ValidateEmail = False
    End If

End Function

Public Function ValidateEnglishCharacters(ByRef lp_strEmail As String) As Boolean
    
    Replace lp_strEmail, "�", "'"
    Dim l_expValidEmailExpression As RegExp
    Set l_expValidEmailExpression = New RegExp
'    l_expValidEmailExpression.Pattern = "^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*\s+<(\w[-._\w]*\w@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
    l_expValidEmailExpression.Pattern = "^[A-Za-z0-9 \t\r\n\v\f\]\[!#$%&�'()*+,\.:;?@_`�\-]+$"
    If l_expValidEmailExpression.Test(lp_strEmail) = True Or lp_strEmail = "" Then
        ValidateEnglishCharacters = True
    Else
        ValidateEnglishCharacters = False
    End If

End Function

Public Function ValidateHTTP(ByVal lp_strEmail As String) As Boolean
    
    Dim l_expValidEmailExpression As RegExp
    Set l_expValidEmailExpression = New RegExp
'    l_expValidEmailExpression.Pattern = "^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*\s+<(\w[-._\w]*\w@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
    l_expValidEmailExpression.Pattern = "^[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
    If l_expValidEmailExpression.Test(lp_strEmail) = True Then
        ValidateHTTP = True
    Else
        ValidateHTTP = False
    End If

End Function

Public Sub ArchiveTrackerProgram(lp_lngTrackerProgramID As Long)

Dim cnn As ADODB.Connection, l_strSQL As String

Set cnn = New ADODB.Connection
cnn.Open g_strConnection

If lp_lngTrackerProgramID <> 0 Then
    
    cnn.Execute "SET IDENTITY_INSERT trackerhistoryarchived ON;"
    l_strSQL = "INSERT INTO trackerhistoryarchived (trackerhistoryID, trackerprogramID, itemconcerned, itemcomments, itemdate, ourcontact, chargeablerejection, "
    l_strSQL = l_strSQL & "chargeableresend, chargeablefix, timetaken, chargeabletimetaken, howmanyfixed, sendemail) "
    l_strSQL = l_strSQL & "SELECT trackerhistoryID, trackerprogramID, itemconcerned, itemcomments, itemdate, ourcontact, chargeablerejection, chargeableresend, "
    l_strSQL = l_strSQL & "chargeablefix, timetaken, chargeabletimetaken, howmanyfixed, sendemail FROM trackerhistory "
    l_strSQL = l_strSQL & "WHERE trackerprogramID = " & lp_lngTrackerProgramID & ";"
    cnn.Execute l_strSQL
    cnn.Execute "DELETE FROM trackerhistory WHERE trackerprogramID = " & lp_lngTrackerProgramID & ";"
    cnn.Execute "SET IDENTITY_INSERT trackerhistoryarchived OFF;"
    
    cnn.Execute "SET IDENTITY_INSERT trackerprogramarchived ON;"
    l_strSQL = "INSERT INTO trackerprogramarchived  (trackerprogramID , companyID, Title, episode, SubTitle, trackercomment, supplier, matid, reqdeliverydate, txdate, "
    l_strSQL = l_strSQL & "runningtime, receivedvideotape, wmvavailable, wmvclipID, editedwmvclipID, receivedmp4, receivedmp2, videochecked, videoingested, transcoded, restoredfrombackup, "
    l_strSQL = l_strSQL & "complianceviewed, complianceviewedby, compliancerequired, compliancecertbeforeedit, compliancecertafteredit, compliancedone, complianceapproved, complianceapprovedby, "
    l_strSQL = l_strSQL & "programmesynopsis, reasonforedit, complianceapprovedtobedone, estimateduration, completeforVOD, episodetitle, suggestedbreaks, extracomments, editinstructions, "
    l_strSQL = l_strSQL & "senttosoundhouses, englisharrived, hungariansupplier, hungarianarrived, polishsupplier, polisharrived, romaniansupplier, romanianarrived, "
    l_strSQL = l_strSQL & "russiansupplier, russianarrived, turkishsupplier, turkisharrived, arabicsupplier, arabicarrived, spanishsupplier, spanisharrived, germansupplier, germanarrived, "
    l_strSQL = l_strSQL & "norwegiansupplier, norwegianarrived, swedishsupplier, swedisharrived, frenchsupplier, frencharrived, danishsupplier, danisharrived, italiansupplier, italianarrived, "
    l_strSQL = l_strSQL & "englishmonoarrived , croatiansupplier, croatianarrived, serbiansupplier, serbianarrived, czechsupplier, czecharrived, "
    l_strSQL = l_strSQL & "cantonesesupplier, cantonesearrived, mandarinsupplier, mandarinarrived, greeksupplier, greekarrived, "
    l_strSQL = l_strSQL & "hebrewsupplier, hebrewarrived, vietnamesesupplier, vietnamesearrived, latamportsupplier, latamportarrived, latamspasupplier, latamspaarrived, "
    l_strSQL = l_strSQL & "macedoniansupplier, macedonianarrived, dutchsupplier, dutcharrived, indonesiansupplier, indonesianarrived, "
    l_strSQL = l_strSQL & "audiosynccheck, wrapandsend, wrapandsendasdate, resend, resendasdate, Duration, passedbyNOC, failedbyNOC, additionalNOCemail, additionalRRSATemail, "
    l_strSQL = l_strSQL & "secondsend, secondsenddate, secondpass, secondfail, secondnotarrive, secondcomments, "
    l_strSQL = l_strSQL & "notarrivedatNOC, commentsfromNOC, workabandoned, chargedseparately, chargedrunningtime, readytowrap ) "
    l_strSQL = l_strSQL & "SELECT trackerprogramID, companyID, Title, episode, SubTitle, trackercomment, supplier, matid, reqdeliverydate, txdate, "
    l_strSQL = l_strSQL & "runningtime, receivedvideotape, wmvavailable, wmvclipID, editedwmvclipID, receivedmp4, receivedmp2, videochecked, videoingested, transcoded, restoredfrombackup, "
    l_strSQL = l_strSQL & "complianceviewed, complianceviewedby, compliancerequired, compliancecertbeforeedit, compliancecertafteredit, compliancedone, complianceapproved, complianceapprovedby, "
    l_strSQL = l_strSQL & "programmesynopsis, reasonforedit, complianceapprovedtobedone, estimateduration, completeforVOD, episodetitle, suggestedbreaks, extracomments, editinstructions, "
    l_strSQL = l_strSQL & "senttosoundhouses, englisharrived, hungariansupplier, hungarianarrived, polishsupplier, polisharrived, romaniansupplier, romanianarrived, "
    l_strSQL = l_strSQL & "russiansupplier, russianarrived, turkishsupplier, turkisharrived, arabicsupplier, arabicarrived, spanishsupplier, spanisharrived, germansupplier, germanarrived, "
    l_strSQL = l_strSQL & "norwegiansupplier, norwegianarrived, swedishsupplier, swedisharrived, frenchsupplier, frencharrived, danishsupplier, danisharrived, italiansupplier, italianarrived, "
    l_strSQL = l_strSQL & "englishmonoarrived , croatiansupplier, croatianarrived, serbiansupplier, serbianarrived, czechsupplier, czecharrived, "
    l_strSQL = l_strSQL & "cantonesesupplier, cantonesearrived, mandarinsupplier, mandarinarrived, greeksupplier, greekarrived, "
    l_strSQL = l_strSQL & "hebrewsupplier, hebrewarrived, vietnamesesupplier, vietnamesearrived, latamportsupplier, latamportarrived, latamspasupplier, latamspaarrived, "
    l_strSQL = l_strSQL & "macedoniansupplier, macedonianarrived, dutchsupplier, dutcharrived, indonesiansupplier, indonesianarrived, "
    l_strSQL = l_strSQL & "audiosynccheck, wrapandsend, wrapandsendasdate, resend, resendasdate, Duration, passedbyNOC, failedbyNOC, additionalNOCemail, additionalRRSATemail, "
    l_strSQL = l_strSQL & "secondsend, secondsenddate, secondpass, secondfail, secondnotarrive, secondcomments, "
    l_strSQL = l_strSQL & "notarrivedatNOC, commentsfromNOC, workabandoned, chargedseparately, chargedrunningtime, readytowrap "
    l_strSQL = l_strSQL & "From trackerprogram "
    l_strSQL = l_strSQL & "WHERE trackerprogramID = " & lp_lngTrackerProgramID & ";"
    cnn.Execute l_strSQL
    cnn.Execute "DELETE FROM trackerprogram WHERE trackerprogramID = " & lp_lngTrackerProgramID & ";"
    cnn.Execute "SET IDENTITY_INSERT trackerprogramarchived OFF;"
End If
cnn.Close
Set cnn = Nothing

End Sub

Public Sub UnArchiveTrackerProgram(lp_lngTrackerProgramID As Long)

Dim cnn As ADODB.Connection, l_strSQL As String

Set cnn = New ADODB.Connection
cnn.Open g_strConnection

If lp_lngTrackerProgramID <> 0 Then
    
    cnn.Execute "SET IDENTITY_INSERT trackerhistory ON;"
    l_strSQL = "INSERT INTO trackerhistory (trackerhistoryID, trackerprogramID, itemconcerned, itemcomments, itemdate, ourcontact, chargeablerejection, "
    l_strSQL = l_strSQL & "chargeableresend, chargeablefix, timetaken, chargeabletimetaken, howmanyfixed, sendemail) "
    l_strSQL = l_strSQL & "SELECT trackerhistoryID, trackerprogramID, itemconcerned, itemcomments, itemdate, ourcontact, chargeablerejection, chargeableresend, "
    l_strSQL = l_strSQL & "chargeablefix, timetaken, chargeabletimetaken, howmanyfixed, sendemail FROM trackerhistoryarchived "
    l_strSQL = l_strSQL & "WHERE trackerprogramID = " & lp_lngTrackerProgramID & ";"
    cnn.Execute l_strSQL
    cnn.Execute "DELETE FROM trackerhistoryarchived WHERE trackerprogramID = " & lp_lngTrackerProgramID & ";"
    cnn.Execute "SET IDENTITY_INSERT trackerhistory OFF;"
    
    cnn.Execute "SET IDENTITY_INSERT trackerprogram ON;"
    l_strSQL = "INSERT INTO trackerprogram (trackerprogramID , companyID, Title, episode, SubTitle, trackercomment, supplier, matid, reqdeliverydate, txdate, "
    l_strSQL = l_strSQL & "runningtime , receivedvideotape, wmvavailable, wmvclipID, editedwmvclipID, receivedmp4, receivedmp2, videochecked, videoingested, transcoded, restoredfrombackup, "
    l_strSQL = l_strSQL & "complianceviewed, complianceviewedby, compliancerequired, compliancecertbeforeedit, compliancecertafteredit, compliancedone, complianceapproved, complianceapprovedby, "
    l_strSQL = l_strSQL & "programmesynopsis, reasonforedit, complianceapprovedtobedone, estimateduration, completeforVOD, episodetitle, suggestedbreaks, extracomments, editinstructions, "
    l_strSQL = l_strSQL & "senttosoundhouses, englisharrived, hungariansupplier, hungarianarrived, polishsupplier, polisharrived, romaniansupplier, romanianarrived, "
    l_strSQL = l_strSQL & "russiansupplier, russianarrived, turkishsupplier, turkisharrived, arabicsupplier, arabicarrived, spanishsupplier, spanisharrived, germansupplier, germanarrived, "
    l_strSQL = l_strSQL & "norwegiansupplier, norwegianarrived, swedishsupplier , swedisharrived, frenchsupplier, frencharrived, danishsupplier, danisharrived, italiansupplier, italianarrived, "
    l_strSQL = l_strSQL & "englishmonoarrived , croatiansupplier, croatianarrived, serbiansupplier, serbianarrived, czechsupplier, czecharrived, "
    l_strSQL = l_strSQL & "cantonesesupplier, cantonesearrived, mandarinsupplier, mandarinarrived, greeksupplier, greekarrived, "
    l_strSQL = l_strSQL & "hebrewsupplier, hebrewarrived, vietnamesesupplier, vietnamesearrived, latamportsupplier, latamportarrived, latamspasupplier, latamspaarrived, "
    l_strSQL = l_strSQL & "macedoniansupplier, macedonianarrived, dutchsupplier, dutcharrived, indonesiansupplier, indonesianarrived, "
    l_strSQL = l_strSQL & "audiosynccheck , wrapandsend, wrapandsendasdate, resend, resendasdate, Duration, passedbyNOC, failedbyNOC, additionalNOCemail, additionalRRSATemail, "
    l_strSQL = l_strSQL & "secondsend, secondsenddate, secondpass, secondfail, secondnotarrive, secondcomments, "
    l_strSQL = l_strSQL & "notarrivedatNOC, commentsfromNOC, workabandoned, chargedseparately, chargedrunningtime, readytowrap ) "
    l_strSQL = l_strSQL & "SELECT trackerprogramID , companyID, Title, episode, SubTitle, trackercomment, supplier, matid, reqdeliverydate, txdate, "
    l_strSQL = l_strSQL & "runningtime , receivedvideotape, wmvavailable, wmvclipID, editedwmvclipID, receivedmp4, receivedmp2, videochecked, videoingested, transcoded, restoredfrombackup, "
    l_strSQL = l_strSQL & "complianceviewed, complianceviewedby, compliancerequired, compliancecertbeforeedit, compliancecertafteredit, compliancedone, complianceapproved, complianceapprovedby, "
    l_strSQL = l_strSQL & "programmesynopsis, reasonforedit, complianceapprovedtobedone, estimateduration, completeforVOD, episodetitle, suggestedbreaks, extracomments, editinstructions, "
    l_strSQL = l_strSQL & "senttosoundhouses, englisharrived, hungariansupplier, hungarianarrived, polishsupplier, polisharrived, romaniansupplier, romanianarrived, "
    l_strSQL = l_strSQL & "russiansupplier, russianarrived, turkishsupplier, turkisharrived, arabicsupplier, arabicarrived, spanishsupplier, spanisharrived, germansupplier, germanarrived, "
    l_strSQL = l_strSQL & "norwegiansupplier, norwegianarrived, swedishsupplier , swedisharrived, frenchsupplier, frencharrived, danishsupplier, danisharrived, italiansupplier, italianarrived, "
    l_strSQL = l_strSQL & "englishmonoarrived , croatiansupplier, croatianarrived, serbiansupplier, serbianarrived, czechsupplier, czecharrived, "
    l_strSQL = l_strSQL & "cantonesesupplier, cantonesearrived, mandarinsupplier, mandarinarrived, greeksupplier, greekarrived, "
    l_strSQL = l_strSQL & "hebrewsupplier, hebrewarrived, vietnamesesupplier, vietnamesearrived, latamportsupplier, latamportarrived, latamspasupplier, latamspaarrived, "
    l_strSQL = l_strSQL & "macedoniansupplier, macedonianarrived, dutchsupplier, dutcharrived, indonesiansupplier, indonesianarrived, "
    l_strSQL = l_strSQL & "audiosynccheck , wrapandsend, wrapandsendasdate, resend, resendasdate, Duration, passedbyNOC, failedbyNOC, additionalNOCemail, additionalRRSATemail, "
    l_strSQL = l_strSQL & "secondsend, secondsenddate, secondpass, secondfail, secondnotarrive, secondcomments, "
    l_strSQL = l_strSQL & "notarrivedatNOC, commentsfromNOC, workabandoned, chargedseparately, chargedrunningtime, readytowrap "
    l_strSQL = l_strSQL & "From trackerprogramarchived "
    l_strSQL = l_strSQL & "WHERE trackerprogramID = " & lp_lngTrackerProgramID & ";"
    cnn.Execute l_strSQL
    cnn.Execute "DELETE FROM trackerprogramarchived WHERE trackerprogramID = " & lp_lngTrackerProgramID & ";"
    cnn.Execute "SET IDENTITY_INSERT trackerprogram OFF;"
End If
cnn.Close
Set cnn = Nothing

End Sub

Sub CopyTrackerItem(lp_lngTracker_itemID As Long)

Dim l_rstOld As ADODB.Recordset, l_strSQL As String

l_strSQL = "SELECT * FROM tracker_item WHERE tracker_itemID = " & lp_lngTracker_itemID & ";"
Set l_rstOld = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If l_rstOld.RecordCount > 0 Then
    l_strSQL = "INSERT INTO tracker_item (companyID, itemreference, itemfilename, duration, gbsent, barcode, headertext1, headertext2, headertext3, headertext4, headertext5, headertext6, headertext7, headertext8, headertext9, "
    l_strSQL = l_strSQL & "headerint1, headerint2, headerint3, headerint4, headerint5, "
    l_strSQL = l_strSQL & "headerdate1, headerdate2, headerdate3, headerdate4, headerdate5, "
    l_strSQL = l_strSQL & "headerflag1, headerflag2, headerflag3, headerflag4, headerflag5) VALUES ("
    l_strSQL = l_strSQL & l_rstOld("companyID") & ", "
    If Not IsNull(l_rstOld("itemreference")) Then
        l_strSQL = l_strSQL & "'" & l_rstOld("itemreference") & "', "
    Else
        l_strSQL = l_strSQL & "NULL, "
    End If
    If Not IsNull(l_rstOld("itemfilename")) Then
        l_strSQL = l_strSQL & "'" & l_rstOld("itemfilename") & "', "
    Else
        l_strSQL = l_strSQL & "NULL, "
    End If
    If Not IsNull(l_rstOld("duration")) Then
        l_strSQL = l_strSQL & l_rstOld("duration") & ", "
    Else
        l_strSQL = l_strSQL & "NULL, "
    End If
    If Not IsNull(l_rstOld("gbsent")) Then
        l_strSQL = l_strSQL & l_rstOld("gbsent") & ", "
    Else
        l_strSQL = l_strSQL & "NULL, "
    End If
    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("barcode"))) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headertext1"))) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headertext2"))) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headertext3"))) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headertext4"))) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headertext5"))) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headertext6"))) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headertext7"))) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headertext8"))) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headertext9"))) & "', "
    l_strSQL = l_strSQL & "'" & Trim(" " & l_rstOld("headerint1")) & "', "
    l_strSQL = l_strSQL & "'" & Trim(" " & l_rstOld("headerint2")) & "', "
    l_strSQL = l_strSQL & "'" & Trim(" " & l_rstOld("headerint3")) & "', "
    l_strSQL = l_strSQL & "'" & Trim(" " & l_rstOld("headerint4")) & "', "
    l_strSQL = l_strSQL & "'" & Trim(" " & l_rstOld("headerint5")) & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(Trim(" " & l_rstOld("headerdate1"))) & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(Trim(" " & l_rstOld("headerdate2"))) & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(Trim(" " & l_rstOld("headerdate3"))) & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(Trim(" " & l_rstOld("headerdate4"))) & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(Trim(" " & l_rstOld("headerdate5"))) & "', "
    l_strSQL = l_strSQL & "'" & Trim(" " & l_rstOld("headerflag1")) & "', "
    l_strSQL = l_strSQL & "'" & Trim(" " & l_rstOld("headerflag2")) & "', "
    l_strSQL = l_strSQL & "'" & Trim(" " & l_rstOld("headerflag3")) & "', "
    l_strSQL = l_strSQL & "'" & Trim(" " & l_rstOld("headerflag4")) & "', "
    l_strSQL = l_strSQL & "'" & Trim(" " & l_rstOld("headerflag5")) & "'"
    l_strSQL = l_strSQL & ");"
    
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
End If

l_rstOld.Close
Set l_rstOld = Nothing

End Sub

Sub CopyDisneyTrackerItem(lp_lngTracker_itemID As Long)

Dim l_rstOld As ADODB.Recordset, l_strSQL As String

'l_strSQL = "SELECT * FROM disneytracker WHERE disneytrackerID = " & lp_lngTracker_itemID & ";"
'Set l_rstOld = ExecuteSQL(l_strSQL, g_strExecuteError)
'CheckForSQLError
'
'If l_rstOld.RecordCount > 0 Then
'    l_strSQL = "INSERT INTO disneytracker (companyID, itemreference, itemfilename, duration, gbsent, headertext1, headertext2, headertext3, headertext4, headertext5, "
'    l_strSQL = l_strSQL & "headerint1, headerint2, headerint3, headerint4, headerint5) VALUES ("
'    l_strSQL = l_strSQL & l_rstOld("companyID") & ", "
'    If Not IsNull(l_rstOld("itemreference")) Then
'        l_strSQL = l_strSQL & "'" & l_rstOld("itemreference") & "', "
'    Else
'        l_strSQL = l_strSQL & "NULL, "
'    End If
'    If Not IsNull(l_rstOld("itemfilename")) Then
'        l_strSQL = l_strSQL & "'" & l_rstOld("itemfilename") & "', "
'    Else
'        l_strSQL = l_strSQL & "NULL, "
'    End If
'    If Not IsNull(l_rstOld("duration")) Then
'        l_strSQL = l_strSQL & l_rstOld("duration") & ", "
'    Else
'        l_strSQL = l_strSQL & "NULL, "
'    End If
'    If Not IsNull(l_rstOld("gbsent")) Then
'        l_strSQL = l_strSQL & l_rstOld("gbsent") & ", "
'    Else
'        l_strSQL = l_strSQL & "NULL, "
'    End If
'    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headertext1"))) & "', "
'    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headertext2"))) & "', "
'    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headertext3"))) & "', "
'    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headertext4"))) & "', "
'    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headertext5"))) & "', "
'    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headerint1"))) & "', "
'    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headerint2"))) & "', "
'    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headerint3"))) & "', "
'    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headerint4"))) & "', "
'    l_strSQL = l_strSQL & "'" & QuoteSanitise(Trim(" " & l_rstOld("headerint5"))) & "'"
'    l_strSQL = l_strSQL & ");"
'
'    Debug.Print l_strSQL
'    ExecuteSQL l_strSQL, g_strExecuteError
'    CheckForSQLError
'End If
'
'l_rstOld.Close
'Set l_rstOld = Nothing

End Sub

Public Sub CopyTrackerProgamToNewCompany(lp_lngTrackerProgramID As Long, lp_lngNewCompanyID As Long)

If lp_lngTrackerProgramID = 0 Then Exit Sub
If lp_lngNewCompanyID = 0 Then Exit Sub

Dim l_rstOldTrackerProgram As New ADODB.Recordset, l_strSQL As String, l_rstNewTrackerProgram As New ADODB.Recordset

l_strSQL = "SELECT * FROM trackerprogram WHERE trackerprogramID = " & lp_lngTrackerProgramID & ";"
Set l_rstOldTrackerProgram = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

l_strSQL = "SELECT * FROM trackerprogram WHERE trackerprogramID = -1;"
Set l_rstNewTrackerProgram = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

l_rstNewTrackerProgram.AddNew

Dim l_intFields As Integer

For l_intFields = 0 To l_rstOldTrackerProgram.Fields.Count - 1
    Select Case l_rstNewTrackerProgram.Fields(l_intFields).Name
        Case "trackerprogramID", "wrapandsend", "passedbyNOC", "FailedbyNOC", "notarrivedbyNOC", "commentsfromNOC", "resend", "audiosynccheck", _
                "senttosoundhouses", "readytowrap", "wrapandsendasdate", "resendasdate", "completeforVOD", "additionalNOCemail", "additionalRRSTemail", _
                "secondsend", "secondsenddate", "secondresend", "secondresenddate", "secondpass", "secondfail", "secondnotarrive", "secondcomments"
            'do nothing
        Case "companyID"
            l_rstNewTrackerProgram.Fields(l_intFields).Value = lp_lngNewCompanyID
        Case Else
            l_rstNewTrackerProgram.Fields(l_intFields).Value = l_rstOldTrackerProgram.Fields(l_intFields).Value
    End Select
Next

l_rstNewTrackerProgram.Update
l_rstNewTrackerProgram.Close
l_rstOldTrackerProgram.Close

Set l_rstNewTrackerProgram = Nothing
Set l_rstOldTrackerProgram = Nothing

End Sub

Public Sub CopyProgressItemToTracker(lp_lngProgressItemID As Long, lp_lngCompanyID As Long)

If lp_lngProgressItemID = 0 Then Exit Sub
If lp_lngCompanyID = 0 Then Exit Sub

Dim l_rstOldProgressItem As New ADODB.Recordset, l_strSQL As String, l_rstNewTrackerItem As New ADODB.Recordset

l_strSQL = "SELECT * FROM progressitem WHERE progressitemID = " & lp_lngProgressItemID & ";"
Set l_rstOldProgressItem = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

l_strSQL = "SELECT * FROM tracker_item WHERE tracker_itemID = -1;"
Set l_rstNewTrackerItem = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

l_rstNewTrackerItem.AddNew

l_rstNewTrackerItem("companyID") = lp_lngCompanyID
l_rstNewTrackerItem("headertext1") = l_rstOldProgressItem("title")
l_rstNewTrackerItem("itemreference") = l_rstOldProgressItem("ISAN")
l_rstNewTrackerItem("headertext2") = l_rstOldProgressItem("ISAN")
l_rstNewTrackerItem("duration") = l_rstOldProgressItem("runningtime")
l_rstNewTrackerItem("headerint1") = l_rstOldProgressItem("batchnumber")


l_rstNewTrackerItem.Update
l_rstNewTrackerItem.Close
l_rstOldProgressItem.Close

Set l_rstNewTrackerItem = Nothing
Set l_rstOldProgressItem = Nothing
Beep

End Sub

Public Function MakeJobDetailLine(ByVal lp_lngJobID As Long, lp_strCopyType As String, lp_strDescription As String, lp_lngQuantity As Long, _
    lp_strFormat As String, lp_lngRunningtime As Long, lp_datDate As Date, lp_strUser As String, lp_lngRecordCount As Long, lp_lngChargeRealMinutes As Long, _
    Optional lp_strStockCode As String, Optional lp_lngStockQuantity As Long, Optional lp_strLastCommentFromTracker As String, Optional lp_varLastCommentDate As Variant, _
    Optional lp_strLastCommentCuser As String, Optional lp_varPortalUserID As Variant, Optional lp_varSkibblyCustomerID As Variant, Optional lp_varClipID As Variant, _
    Optional lp_varHDFlag As Variant, Optional lp_varProjectManager As Variant, Optional lp_varProjectNumber As Variant, _
    Optional lp_varSeries As Variant, Optional lp_varEpisode As Variant, Optional lp_varEpisodeTitle As Variant, Optional lp_varRightsOwner As Variant, _
    Optional lp_varDADCJobTitle As Variant, _
    Optional lp_varWorkflowID As Variant, Optional lp_varCVCode As Variant, Optional lp_varLegacyOracTVAENumber As Variant, Optional lp_varBFIPONumber As Variant, _
    Optional lp_varTrackerID As Variant) As Variant

Dim l_strSQL As String, l_strWorkflowDescription As String

Select Case lp_strCopyType
    Case "M", "C", "A"
        l_strWorkflowDescription = GetDataSQL("SELECT TOP 1 information from xref WHERE category = 'dformat' and description = '" & lp_strFormat & "';")
    Case "I"
        l_strWorkflowDescription = GetDataSQL("SELECT TOP 1 description from xref WHERE category = 'inclusive' and format = '" & lp_strFormat & "';")
    Case "O"
        l_strWorkflowDescription = GetDataSQL("SELECT TOP 1 description from xref WHERE category = 'other' and format = '" & lp_strFormat & "';")
    Case Else
        l_strWorkflowDescription = ""
End Select

l_strSQL = "INSERT INTO jobdetail (jobID, copytype, description, workflowdescription, quantity, format, runningtime, completeddate, completeduser, fd_orderby, chargerealminutes"
If lp_strStockCode <> "" Then
    l_strSQL = l_strSQL & ", stock1type, stock1ours"
End If
If lp_strLastCommentFromTracker <> "" Then
    l_strSQL = l_strSQL & ", lastcommentfromtracker, lastcommentdate, lastcommentcuser"
End If
If Not IsMissing(lp_varPortalUserID) And IsNumeric(lp_varPortalUserID) Then
    If Val(lp_varPortalUserID) <> 0 Then
        l_strSQL = l_strSQL & ", portaluserID, skibblycustomerID, clipID"
    End If
End If
If Not IsMissing(lp_varHDFlag) And IsNumeric(lp_varHDFlag) Then
    If Val(lp_varHDFlag) <> 0 Then
        l_strSQL = l_strSQL & ", hdflag"
    End If
End If
If Not IsMissing(lp_varProjectManager) And Not IsNull(lp_varProjectManager) Then
    l_strSQL = l_strSQL & ", DADCProjectManager"
End If
If Not IsMissing(lp_varProjectNumber) And Not IsNull(lp_varProjectNumber) Then
    l_strSQL = l_strSQL & ", DADCProjectNumber"
End If
If Not IsMissing(lp_varSeries) And Not IsNull(lp_varSeries) Then
    l_strSQL = l_strSQL & ", DADCSeries"
End If
If Not IsMissing(lp_varEpisode) And Not IsNull(lp_varEpisode) Then
    l_strSQL = l_strSQL & ", DADCEpisodeNumber"
End If
If Not IsMissing(lp_varEpisodeTitle) And Not IsNull(lp_varEpisodeTitle) Then
    l_strSQL = l_strSQL & ", DADCEpisodeTitle"
End If
If Not IsMissing(lp_varRightsOwner) And Not IsNull(lp_varRightsOwner) Then
    l_strSQL = l_strSQL & ", DADCRightsOwner"
End If
If Not IsMissing(lp_varDADCJobTitle) And Not IsNull(lp_varDADCJobTitle) Then
    l_strSQL = l_strSQL & ", DADCJobTitle"
End If
If Not IsMissing(lp_varWorkflowID) And Not IsNull(lp_varWorkflowID) And IsNumeric(lp_varWorkflowID) Then
    l_strSQL = l_strSQL & ", DADCWorkflowID"
End If
If Not IsMissing(lp_varCVCode) And Not IsNull(lp_varCVCode) Then
    l_strSQL = l_strSQL & ", DADCCVCode"
End If
If Not IsMissing(lp_varLegacyOracTVAENumber) And Not IsNull(lp_varLegacyOracTVAENumber) Then
    l_strSQL = l_strSQL & ", DADCLegacyOracTVAENumber"
End If
If Not IsMissing(lp_varBFIPONumber) And Not IsNull(lp_varBFIPONumber) Then
    l_strSQL = l_strSQL & ", SpecialProject"
End If
If Not IsMissing(lp_varTrackerID) And Not IsNull(lp_varTrackerID) Then
    l_strSQL = l_strSQL & ", DADC_Tracker_ID"
End If
l_strSQL = l_strSQL & ") VALUES ("
l_strSQL = l_strSQL & lp_lngJobID & ", "
l_strSQL = l_strSQL & "'" & lp_strCopyType & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strDescription) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strWorkflowDescription) & "', "
l_strSQL = l_strSQL & "'" & lp_lngQuantity & "', "
l_strSQL = l_strSQL & "'" & lp_strFormat & "', "
l_strSQL = l_strSQL & "'" & lp_lngRunningtime & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(lp_datDate) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(lp_strUser) & "', "
l_strSQL = l_strSQL & "'" & lp_lngRecordCount & "', "
l_strSQL = l_strSQL & "'" & lp_lngChargeRealMinutes & "'"
If lp_strStockCode <> "" Then
    l_strSQL = l_strSQL & ", '" & lp_strStockCode & "', "
    l_strSQL = l_strSQL & lp_lngStockQuantity
End If
If lp_strLastCommentFromTracker <> "" Then
    l_strSQL = l_strSQL & ", '" & QuoteSanitise(lp_strLastCommentFromTracker) & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(lp_varLastCommentDate) & "', "
    l_strSQL = l_strSQL & "'" & lp_strLastCommentCuser & "'"
End If
If IsNumeric(lp_varPortalUserID) Then
    If Val(lp_varPortalUserID) <> 0 Then
       l_strSQL = l_strSQL & ", " & Val(lp_varPortalUserID) & ", "
       l_strSQL = l_strSQL & Val(lp_varSkibblyCustomerID) & ", "
       l_strSQL = l_strSQL & Val(lp_varClipID)
    End If
End If
If IsNumeric(lp_varHDFlag) Then
    If Val(lp_varHDFlag) <> 0 Then
       l_strSQL = l_strSQL & ", " & Val(lp_varHDFlag)
    End If
End If
If Not IsMissing(lp_varProjectManager) And Not IsNull(lp_varProjectManager) Then
    l_strSQL = l_strSQL & ", '" & QuoteSanitise(lp_varProjectManager) & "'"
End If
If Not IsMissing(lp_varProjectNumber) And Not IsNull(lp_varProjectNumber) Then
    l_strSQL = l_strSQL & ", '" & lp_varProjectNumber & "'"
End If
If Not IsMissing(lp_varSeries) And Not IsNull(lp_varSeries) Then
    l_strSQL = l_strSQL & ", '" & lp_varSeries & "'"
End If
If Not IsMissing(lp_varEpisode) And Not IsNull(lp_varEpisode) Then
    l_strSQL = l_strSQL & ", '" & lp_varEpisode & "'"
End If
If Not IsMissing(lp_varEpisodeTitle) And Not IsNull(lp_varEpisodeTitle) Then
    l_strSQL = l_strSQL & ", '" & QuoteSanitise(lp_varEpisodeTitle) & "'"
End If
If Not IsMissing(lp_varRightsOwner) And Not IsNull(lp_varRightsOwner) Then
    l_strSQL = l_strSQL & ", '" & QuoteSanitise(lp_varRightsOwner) & "'"
End If
If Not IsMissing(lp_varDADCJobTitle) And Not IsNull(lp_varDADCJobTitle) Then
    l_strSQL = l_strSQL & ", '" & QuoteSanitise(lp_varDADCJobTitle) & "'"
End If
If Not IsMissing(lp_varWorkflowID) And Not IsNull(lp_varWorkflowID) And IsNumeric(lp_varWorkflowID) Then
    l_strSQL = l_strSQL & ", " & lp_varWorkflowID
End If
If Not IsMissing(lp_varCVCode) And Not IsNull(lp_varCVCode) Then
    l_strSQL = l_strSQL & ", '" & QuoteSanitise(lp_varCVCode) & "'"
End If
If Not IsMissing(lp_varLegacyOracTVAENumber) And Not IsNull(lp_varLegacyOracTVAENumber) Then
    l_strSQL = l_strSQL & ", '" & QuoteSanitise(lp_varLegacyOracTVAENumber) & "'"
End If
If Not IsMissing(lp_varBFIPONumber) And Not IsNull(lp_varBFIPONumber) Then
    l_strSQL = l_strSQL & ", '" & lp_varBFIPONumber & "'"
End If
If Not IsMissing(lp_varTrackerID) And Not IsNull(lp_varTrackerID) Then
    l_strSQL = l_strSQL & ", '" & lp_varTrackerID & "'"
End If
l_strSQL = l_strSQL & "); "

Debug.Print l_strSQL

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

MakeJobDetailLine = g_lngLastID

End Function

Public Function RndPassword(lp_lngVLength As Long)
    'This function will generate a random strong password of variable
    'length.
    'This script is provided under the Creative Commons license located
    'at http://creativecommons.org/licenses/by-nc/2.5/ . It may not
    'be used for commercial purposes with out the expressed written consent
    'of NateRice.com
    
    Dim X As Long, l_lngVChar As Long, l_strPassword As String
    l_strPassword = ""
    For X = 1 To lp_lngVLength
        Randomize
        
        l_lngVChar = GetRdm
        
        l_strPassword = l_strPassword & Chr(l_lngVChar)
    Next

RndPassword = l_strPassword

End Function

Public Function GetRdm()
    
    Dim intRdm As Long
    intRdm = 58
    Do While intRdm > 57 And intRdm < 65 Or intRdm > 90 And intRdm < 97
        intRdm = Int((122 - 49) * Rnd + 48)
    Loop
    
    GetRdm = intRdm

End Function

Public Function AdditionalConverter(lp_strFormat As String) As String

If Left(lp_strFormat, 4) = "WWSD" Then
    AdditionalConverter = "STANDARDS CONVERSION"
ElseIf InStr(Mid(lp_strFormat, 5), "HD") > 0 Then
    AdditionalConverter = "CROSS-CONVERSION"
Else
    AdditionalConverter = "DOWN CONVERSION, STANDARDS CONVERSTION"
End If
    
End Function

Public Function fb_bst(pd_dt As Date) As Boolean
  Const cs_fac = "%fb_bst, "
  Dim ls_eom As String
  If Not IsDate(pd_dt) Then fb_bst = False: Exit Function
  Select Case Month(pd_dt)
  Case 4, 5, 6, 7, 8, 9
    fb_bst = True
  Case 1, 2, 11, 12
    fb_bst = False
  Case 3
    fb_bst = False
    If Day(pd_dt) < 25 Then Exit Function
    ls_eom = "31/03/" & Year(pd_dt)
    If Weekday(pd_dt) - Weekday(ls_eom) > 0 Then Exit Function
    If Hour(pd_dt) < 1 Then Exit Function
    fb_bst = True
  Case 10
    fb_bst = True
    If Day(pd_dt) < 25 Then Exit Function
    ls_eom = "31/10/" & Year(pd_dt)
    If Weekday(pd_dt) - Weekday(ls_eom) > 0 Then Exit Function
'    If Hour( pd_dt ) < 1 Then Exit Function
    fb_bst = False
  End Select
End Function

Public Function WordWrap(ByRef Text As String, ByVal Width As Long, Optional ByRef CountLines As Long) As String  ' by Donald, donald@xbeat.net, 20040913
  Dim i As Long
  Dim lenLine As Long
  Dim posBreak As Long
  Dim cntBreakChars As Long
  Dim abText() As Byte
  Dim abTextOut() As Byte
  Dim ubText As Long

  ' no fooling around
  If Width <= 0 Then
    CountLines = 0
    Exit Function
  End If
  If Len(Text) <= Width Then  ' no need to wrap
    CountLines = 1
    WordWrap = Text
    Exit Function
  End If
  
  abText = StrConv(Text, vbFromUnicode)
  ubText = UBound(abText)
  ReDim abTextOut(ubText * 3) 'dim to potential max
  
  For i = 0 To ubText
    Select Case abText(i)
    Case 32, 45 'space, hyphen
      posBreak = i
    Case 13
      lenLine = 0
    Case Else
    End Select
    
    abTextOut(i + cntBreakChars) = abText(i)
    lenLine = lenLine + 1
    
    If lenLine > Width Then
      If posBreak > 0 Then
        ' don't break at the very end
        If posBreak = ubText Then Exit For
        ' wrap after space, hyphen
        abTextOut(posBreak + cntBreakChars + 1) = 13  'CR
        abTextOut(posBreak + cntBreakChars + 2) = 10  'LF
        i = posBreak
        posBreak = 0
      Else
        ' cut word
        abTextOut(i + cntBreakChars) = 13     'CR
        abTextOut(i + cntBreakChars + 1) = 10 'LF
        i = i - 1
      End If
      cntBreakChars = cntBreakChars + 2
      lenLine = 0
    End If
  Next
  
  CountLines = cntBreakChars \ 2 + 1
  
  ReDim Preserve abTextOut(ubText + cntBreakChars)
  WordWrap = StrConv(abTextOut, vbUnicode)
  
End Function

Public Sub ShowNotices()

Dim l_strSQL As String, l_rstNotices As ADODB.Recordset
Dim l_datUserCreated As Date, returnval As Variant

l_datUserCreated = GetData("cetauser", "createddate", "cetauserID", g_lngUserID)
l_strSQL = "SELECT * FROM cetanotice WHERE "
l_strSQL = l_strSQL & "datecreated >= '" & FormatSQLDate(l_datUserCreated) & "' ORDER BY datecreated;"
Set l_rstNotices = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError
If l_rstNotices.RecordCount > 0 Then
    l_rstNotices.MoveFirst
    Do While Not l_rstNotices.EOF
        returnval = GetDataSQL("SELECT TOP 1 dateread FROM cetanoticeread WHERE cetauserID = " & g_lngUserID & " AND cetanoticeID = " & l_rstNotices("cetanoticeID") & ";")
        If returnval = 0 Then
            frmCetaNotices.lblcetanoticeID.Caption = l_rstNotices("cetanoticeID")
            frmCetaNotices.txtNotice.Text = l_rstNotices("notice")
            frmCetaNotices.Show vbModal
            Unload frmCetaNotices
        End If
        l_rstNotices.MoveNext
    
    Loop
End If

End Sub

Function CRLFSanitise(lp_strText As String) As String

    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strNewText As String
    Dim l_strOldText As String
    Dim l_intLoop As Integer
    l_strNewText = ""
    
    If IsNull(lp_strText) Then
        l_strOldText = ""
        l_strNewText = l_strOldText
        lp_strText = ""
    Else
        l_strOldText = lp_strText
    End If
    
    If InStr(lp_strText, vbCrLf) <> 0 Or InStr(lp_strText, Chr(10)) <> 0 Or InStr(lp_strText, Chr(13)) <> 0 Then
        
        For l_intLoop = 1 To Len(l_strOldText)
            
            Select Case Mid(l_strOldText, l_intLoop, 1)
                
                Case vbCrLf, Chr(10), Chr(13)
                    'Nothing
                Case Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
            
            End Select
            
        Next
    Else
        l_strNewText = lp_strText
    End If
    
    CRLFSanitise = l_strNewText
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function WildcardChange(lp_strText As String, lp_strOldChar As String, lp_strNewChar As String) As String

    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strNewText As String
    Dim l_strOldText As String
    Dim l_intLoop As Integer
    l_strNewText = ""
    
    If IsNull(lp_strText) Then
        l_strOldText = ""
        l_strNewText = l_strOldText
        lp_strText = ""
    Else
        l_strOldText = lp_strText
    End If
    
    If InStr(lp_strText, lp_strOldChar) <> 0 Then
        
        For l_intLoop = 1 To Len(l_strOldText)
            
            Select Case Mid(l_strOldText, l_intLoop, 1)
                
                Case lp_strOldChar
                    l_strNewText = l_strNewText & lp_strNewChar
                Case Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
            
            End Select
            
        Next
    Else
        l_strNewText = lp_strText
    End If
    
'    If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
'        l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
'    End If
    
    WildcardChange = l_strNewText
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function UnicodeMsgBox(Prompt As String, _
   Optional Buttons As VbMsgBoxStyle = vbOKOnly, _
   Optional Title As String) As VbMsgBoxResult

   Dim WshShell As Object
   Set WshShell = CreateObject("WScript.Shell")
   UnicodeMsgBox = WshShell.Popup(Prompt, 0&, Title, Buttons)
   Set WshShell = Nothing
End Function

Public Sub CreateNewTrackerLine(lp_lngTrackerID As Long)

'Creates a Redo copy of a DADC Tracker Line, from being given the original Tracker ID
Dim l_strReference As String, l_strTitle As String, l_strSubTitle As String, l_strBarcode As String, l_strAlphaBusiness As String, l_strContentVersionCode As String, l_lngContentVersionID As Long, l_strLegacyLineStandard As String, l_strImageAspectRatio As String
Dim l_strLegacyPictureElement As String, l_strLanguage As String, l_strSubtitlesLanguage As String, l_strAudioConfirguration As String, l_lngEpisode As Long, l_lngCRID As Long, l_datDueDate As Date
Dim l_lngExternalAlphaKey As Long, l_lngWorkflowID As Long, l_strDurationEstimate As String, l_lngBatchNumber As Long, l_strFormat As String, l_strAudioConfiguration As String
Dim l_strSQL As String

l_lngCRID = GetData("tracker_dadc_item", "dbbcrid", "tracker_dadc_itemID", lp_lngTrackerID)
l_strBarcode = GetData("tracker_dadc_item", "newbarcode", "tracker_dadc_itemID", lp_lngTrackerID)
l_strAlphaBusiness = GetData("tracker_dadc_item", "externalalphaID", "tracker_dadc_itemID", lp_lngTrackerID)
l_strContentVersionCode = GetData("tracker_dadc_item", "contentversioncode", "tracker_dadc_itemID", lp_lngTrackerID)
l_strTitle = GetData("tracker_dadc_item", "title", "tracker_dadc_itemID", lp_lngTrackerID)
l_strSubTitle = GetData("tracker_dadc_item", "subtitle", "tracker_dadc_itemID", lp_lngTrackerID)
l_lngEpisode = GetData("tracker_dadc_item", "episode", "tracker_dadc_itemID", lp_lngTrackerID)
l_strFormat = GetData("library", "format", "barcode", l_strBarcode)
l_strLegacyLineStandard = ""
l_strImageAspectRatio = GetData("tracker_dadc_item", "imageaspectratio", "tracker_dadc_itemID", lp_lngTrackerID)
l_strLegacyPictureElement = ""
l_strLanguage = GetData("tracker_dadc_item", "language", "tracker_dadc_itemID", lp_lngTrackerID)
If l_strLanguage = "None" Then l_strLanguage = ""
l_strSubtitlesLanguage = GetData("tracker_dadc_item", "subtitleslanguage", "tracker_dadc_itemID", lp_lngTrackerID)
l_strAudioConfiguration = GetData("tracker_dadc_item", "audioconfiguration", "tracker_dadc_itemID", lp_lngTrackerID)
l_strDurationEstimate = ""
l_datDueDate = DateAdd("d", 1, Now)
l_lngBatchNumber = Format(l_datDueDate, "ddmmyy")
l_lngExternalAlphaKey = GetData("tracker_dadc_item", "externalalphakey", "tracker_dadc_itemID", lp_lngTrackerID)
l_lngContentVersionID = GetData("tracker_dadc_item", "contentversionID", "tracker_dadc_itemID", lp_lngTrackerID)
l_lngWorkflowID = GetData("tracker_dadc_item", "workflowID", "tracker_dadc_itemID", lp_lngTrackerID)
If l_strAlphaBusiness <> "" Then
    l_strReference = l_strAlphaBusiness & "_" & SanitiseBarcode(l_strBarcode) & "_" & l_lngExternalAlphaKey & "_" & l_lngWorkflowID
Else
    l_strReference = l_strContentVersionCode & "_" & SanitiseBarcode(l_strBarcode) & "_" & l_lngContentVersionID & "_" & l_lngWorkflowID
End If
If l_lngWorkflowID = 0 Then
    l_strReference = ""
End If

l_strSQL = "INSERT INTO tracker_dadc_item (cdate, mdate, companyID, itemreference, title, subtitle, barcode, newbarcode, format, externalalphaID, originalexternalalphaID, contentversioncode, contentversionID, legacylinestandard, imageaspectratio, legacypictureelement, language, "
l_strSQL = l_strSQL & "subtitleslanguage, audioconfiguration, episode, dbbcrid, duedateupload, externalalphakey, workflowID, durationestimate, batch, priority, complaintredoitem) VALUES ("
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "1261, "
If l_strReference <> "" Then
    l_strSQL = l_strSQL & "'" & l_strReference & "', "
Else
    l_strSQL = l_strSQL & "NULL, "
End If
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSubTitle) & "', "
l_strSQL = l_strSQL & "'" & UCase(l_strBarcode) & "', "
l_strSQL = l_strSQL & "'" & UCase(l_strBarcode) & "', "
l_strSQL = l_strSQL & "'" & l_strFormat & "', "
l_strSQL = l_strSQL & "'" & l_strAlphaBusiness & "', "
l_strSQL = l_strSQL & "'" & l_strAlphaBusiness & "', "
l_strSQL = l_strSQL & "'" & l_strContentVersionCode & "', "
l_strSQL = l_strSQL & l_lngContentVersionID & ", "
l_strSQL = l_strSQL & "'" & l_strLegacyLineStandard & "', "
l_strSQL = l_strSQL & "'" & l_strImageAspectRatio & "', "
l_strSQL = l_strSQL & "'" & l_strLegacyPictureElement & "', "
l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
l_strSQL = l_strSQL & "'" & l_strSubtitlesLanguage & "', "
l_strSQL = l_strSQL & "'" & l_strAudioConfiguration & "', "
l_strSQL = l_strSQL & l_lngEpisode & ", "
l_strSQL = l_strSQL & l_lngCRID & ", "
l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDueDate) & "', "
l_strSQL = l_strSQL & l_lngExternalAlphaKey & ", "
l_strSQL = l_strSQL & l_lngWorkflowID & ", "
l_strSQL = l_strSQL & Int(Val(l_strDurationEstimate)) & ", "
l_strSQL = l_strSQL & l_lngBatchNumber & ", "
l_strSQL = l_strSQL & "1, 1);"
Debug.Print l_strSQL
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError
    
End Sub

Public Sub CreateNewiTunesTrackerLine(lp_lngTrackerID As Long)

'Creates a Redo copy of a iTunes Tracker Line, from being given the original Tracker ID
Dim l_strReference As String, l_strTitle As String, l_strEpisodeTitle As String, l_strProjectManager As String, l_lngProjectManagerContactID As Long, l_lngCompanyID As Long
Dim l_lngPriority As String, l_strSeries As String, l_strEpisode As String, l_datDueDate As Date, l_strProjectNumber As String, l_strOrderNumber As String
Dim l_striTunesOrderType As String, l_striTunesSubmissionType As String, l_striTunesVendorID As String
Dim l_strSQL As String, l_strVolume As String
Dim l_strFeatureAudioSpec As String, l_strTrailerAudioSpec, l_strTrailerBIS, l_strTrailerCreate, l_strChaptering As String
Dim l_strFeatureLanguage1 As String, l_strFeatureLanguage2 As String, l_strFeatureLanguage3 As String, l_strFeatureLanguage4 As String
Dim l_strArtworkLanguage1 As String, l_strArtworkLanguage2 As String, l_strArtworkLanguage3 As String, l_strArtworkLanguage4 As String
Dim l_strTrailerLanguage1 As String, l_strTrailerLanguage2 As String, l_strTrailerLanguage3 As String, l_strTrailerLanguage4 As String
Dim l_strSubsLanguage1 As String, l_strSubsLanguage2 As String, l_strSubsLanguage3 As String, l_strSubsLanguage4 As String

l_strReference = GetData("tracker_iTunes_item", "itemreference", "tracker_iTunes_itemID", lp_lngTrackerID)
l_lngCompanyID = GetData("tracker_iTunes_item", "companyID", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strTitle = GetData("tracker_iTunes_item", "title", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strOrderNumber = GetData("tracker_iTunes_item", "ordernumber", "tracker_iTunes_itemID", lp_lngTrackerID)
l_striTunesVendorID = GetData("tracker_iTunes_item", "iTunesVendorID", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strSeries = GetData("tracker_iTunes_item", "series", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strVolume = GetData("tracker_iTunes_item", "volume", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strEpisode = GetData("tracker_iTunes_item", "episode", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strEpisodeTitle = GetData("tracker_iTunes_item", "episodetitle", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strProjectManager = GetData("tracker_iTunes_item", "ProjectManager", "tracker_iTunes_itemID", lp_lngTrackerID)
l_lngProjectManagerContactID = GetData("tracker_iTunes_item", "ProjectManagercontactID", "tracker_iTunes_itemID", lp_lngTrackerID)
l_lngPriority = GetData("tracker_iTunes_item", "priority", "tracker_iTunes_itemID", lp_lngTrackerID)
l_striTunesSubmissionType = GetData("tracker_iTunes_item", "iTunesSubmissionType", "tracker_iTunes_itemID", lp_lngTrackerID)
l_striTunesOrderType = GetData("tracker_iTunes_item", "iTunesOrderType", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strFeatureAudioSpec = GetData("tracker_iTunes_item", "FeatureAudioSpec", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strTrailerAudioSpec = GetData("tracker_iTunes_item", "TrailerAudioSpec", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strTrailerBIS = GetData("tracker_iTunes_item", "TrailerBIS", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strTrailerCreate = GetData("tracker_iTunes_item", "TrailerCreate", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strChaptering = GetData("tracker_iTunes_item", "Chaptering", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strFeatureLanguage1 = GetData("tracker_iTunes_item", "FeatureLanguage1", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strFeatureLanguage2 = GetData("tracker_iTunes_item", "FeatureLanguage2", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strFeatureLanguage3 = GetData("tracker_iTunes_item", "FeatureLanguage3", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strFeatureLanguage4 = GetData("tracker_iTunes_item", "FeatureLanguage4", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strTrailerLanguage1 = GetData("tracker_iTunes_item", "TrailerLanguage1", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strTrailerLanguage2 = GetData("tracker_iTunes_item", "TrailerLanguage2", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strTrailerLanguage3 = GetData("tracker_iTunes_item", "TrailerLanguage3", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strTrailerLanguage4 = GetData("tracker_iTunes_item", "TrailerLanguage4", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strArtworkLanguage1 = GetData("tracker_iTunes_item", "ArtworkLanguage1", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strArtworkLanguage2 = GetData("tracker_iTunes_item", "ArtworkLanguage2", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strArtworkLanguage3 = GetData("tracker_iTunes_item", "ArtworkLanguage3", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strArtworkLanguage4 = GetData("tracker_iTunes_item", "ArtworkLanguage4", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strSubsLanguage1 = GetData("tracker_iTunes_item", "SubsLanguage1", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strSubsLanguage2 = GetData("tracker_iTunes_item", "SubsLanguage2", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strSubsLanguage3 = GetData("tracker_iTunes_item", "SubsLanguage3", "tracker_iTunes_itemID", lp_lngTrackerID)
l_strSubsLanguage4 = GetData("tracker_iTunes_item", "SubsLanguage4", "tracker_iTunes_itemID", lp_lngTrackerID)

l_datDueDate = DateAdd("d", 1, Now)

l_strSQL = "INSERT INTO tracker_iTunes_item (cdate, mdate, companyID, itemreference, projectmanager, projectmanagercontactID, priority, title, episodetitle, series, volume, ordernumber, iTunesVendorID, "
l_strSQL = l_strSQL & "FeatureAudioSpec, TrailerAudioSpec, TrailerBIS, TrailerCreate, Chaptering, "
l_strSQL = l_strSQL & "FeatureLanguage1 , FeatureLanguage2, FeatureLanguage3, FeatureLanguage4, ArtworkLanguage1, ArtworkLanguage2, ArtworkLanguage3, ArtworkLanguage4, "
l_strSQL = l_strSQL & "TrailerLanguage1, TrailerLanguage2, TrailerLanguage3, TrailerLanguage4, SubsLanguage1, SubsLanguage2, SubsLanguage3, SubsLanguage4, "
l_strSQL = l_strSQL & "iTunesOrderType, iTunesSubmissionType, episode, requiredby, complaintredoitem) VALUES ("
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & l_lngCompanyID & ", "
If l_strReference <> "" Then
    l_strSQL = l_strSQL & "'" & l_strReference & "', "
Else
    l_strSQL = l_strSQL & "NULL, "
End If
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strProjectManager) & "', "
l_strSQL = l_strSQL & l_lngProjectManagerContactID & ", "
l_strSQL = l_strSQL & l_lngPriority & ", "
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
l_strSQL = l_strSQL & "'" & l_strSeries & "', "
l_strSQL = l_strSQL & "'" & l_strVolume & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strEpisodeTitle) & "', "
l_strSQL = l_strSQL & "'" & l_strOrderNumber & "', "
l_strSQL = l_strSQL & "'" & l_striTunesVendorID & "', "
l_strSQL = l_strSQL & "'" & l_strFeatureAudioSpec & "', "
l_strSQL = l_strSQL & "'" & l_strTrailerAudioSpec & "', "
l_strSQL = l_strSQL & "'" & l_strTrailerBIS & "', "
l_strSQL = l_strSQL & "'" & l_strTrailerCreate & "', "
l_strSQL = l_strSQL & "'" & l_strChaptering & "', "
l_strSQL = l_strSQL & "'" & l_strFeatureLanguage1 & "', "
l_strSQL = l_strSQL & "'" & l_strFeatureLanguage2 & "', "
l_strSQL = l_strSQL & "'" & l_strFeatureLanguage3 & "', "
l_strSQL = l_strSQL & "'" & l_strFeatureLanguage4 & "', "
l_strSQL = l_strSQL & "'" & l_strArtworkLanguage1 & "', "
l_strSQL = l_strSQL & "'" & l_strArtworkLanguage2 & "', "
l_strSQL = l_strSQL & "'" & l_strArtworkLanguage3 & "', "
l_strSQL = l_strSQL & "'" & l_strArtworkLanguage4 & "', "
l_strSQL = l_strSQL & "'" & l_strTrailerLanguage1 & "', "
l_strSQL = l_strSQL & "'" & l_strTrailerLanguage2 & "', "
l_strSQL = l_strSQL & "'" & l_strTrailerLanguage3 & "', "
l_strSQL = l_strSQL & "'" & l_strTrailerLanguage4 & "', "
l_strSQL = l_strSQL & "'" & l_strSubsLanguage1 & "', "
l_strSQL = l_strSQL & "'" & l_strSubsLanguage2 & "', "
l_strSQL = l_strSQL & "'" & l_strSubsLanguage3 & "', "
l_strSQL = l_strSQL & "'" & l_strSubsLanguage4 & "', "
l_strSQL = l_strSQL & "'" & l_striTunesOrderType & "', "
l_strSQL = l_strSQL & "'" & l_striTunesSubmissionType & "', "
l_strSQL = l_strSQL & "'" & l_strEpisode & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDueDate) & "', "
l_strSQL = l_strSQL & "1);"
Debug.Print l_strSQL
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError
    
End Sub

Public Function CreateNewSvenskTrackerLine(lp_lngTrackerID As Long) As Long

'Creates a Redo copy of a Svensk Tracker Line, from being given the original Tracker ID
Dim l_strReference As String, l_strTitle As String, l_strSubTitle As String, l_strBarcode As String, l_strRightsOwner As String, l_strProjectManager As String, l_lngProjectManagerContactID As Long, l_lngCompanyID As Long
Dim l_lngPriority As String, l_strSeries As String, l_strLanguage As String, l_strEpisode As String, l_datDueDate As Date, l_lngConform As Long, l_strProjectNumber As String, l_strUniqueID As String, l_strAlphaDisplayCode As String
Dim l_strComponentType As String, l_strProgramType As String, l_lngTextlessRequired As Long, l_lngMERequired As Long, l_lngME51Required As Long, l_lngProxyRequired As Long, l_lngiTunesRequired As Long
Dim l_strSQL As String

l_strReference = GetData("tracker_svensk_item", "itemreference", "tracker_svensk_itemID", lp_lngTrackerID)
l_lngCompanyID = GetData("tracker_svensk_item", "companyID", "tracker_svensk_itemID", lp_lngTrackerID)
l_strTitle = GetData("tracker_svensk_item", "title", "tracker_svensk_itemID", lp_lngTrackerID)
l_strProjectNumber = GetData("tracker_svensk_item", "projectnumber", "tracker_svensk_itemID", lp_lngTrackerID)
l_strAlphaDisplayCode = GetData("tracker_svensk_item", "AlphaDisplayCode", "tracker_svensk_itemID", lp_lngTrackerID)
l_strSeries = GetData("tracker_svensk_item", "series", "tracker_svensk_itemID", lp_lngTrackerID)
l_strEpisode = GetData("tracker_svensk_item", "episode", "tracker_svensk_itemID", lp_lngTrackerID)
l_strSubTitle = GetData("tracker_svensk_item", "subtitle", "tracker_svensk_itemID", lp_lngTrackerID)
l_strRightsOwner = GetData("tracker_svensk_item", "RightsOwner", "tracker_svensk_itemID", lp_lngTrackerID)
l_strProjectManager = GetData("tracker_svensk_item", "ProjectManager", "tracker_svensk_itemID", lp_lngTrackerID)
l_lngProjectManagerContactID = GetData("tracker_svensk_item", "ProjectManagercontactID", "tracker_svensk_itemID", lp_lngTrackerID)
l_lngPriority = GetData("tracker_svensk_item", "urgent", "tracker_svensk_itemID", lp_lngTrackerID)
l_strLanguage = GetData("tracker_svensk_item", "language", "tracker_svensk_itemID", lp_lngTrackerID)
If l_strLanguage = "None" Then l_strLanguage = ""
l_lngTextlessRequired = GetData("tracker_svensk_item", "textlessrequired", "tracker_svensk_itemID", lp_lngTrackerID)
l_lngMERequired = GetData("tracker_svensk_item", "MErequired", "tracker_svensk_itemID", lp_lngTrackerID)
l_lngME51Required = GetData("tracker_svensk_item", "ME51required", "tracker_svensk_itemID", lp_lngTrackerID)
l_lngProxyRequired = GetData("tracker_svensk_item", "Proxyreq", "tracker_svensk_itemID", lp_lngTrackerID)
l_lngiTunesRequired = GetData("tracker_svensk_item", "itunes", "tracker_svensk_itemID", lp_lngTrackerID)
l_strProgramType = GetData("tracker_svensk_item", "ProgramType", "tracker_svensk_itemID", lp_lngTrackerID)
l_strComponentType = GetData("tracker_svensk_item", "ComponentType", "tracker_svensk_itemID", lp_lngTrackerID)
l_lngConform = GetData("tracker_svensk_item", "conform", "tracker_svensk_itemID", lp_lngTrackerID)
l_strUniqueID = GetData("tracker_svensk_item", "uniqueID", "tracker_svensk_itemID", lp_lngTrackerID)

l_datDueDate = DateAdd("d", 1, Now)

l_strSQL = "INSERT INTO tracker_svensk_item (cdate, mdate, companyID, itemreference, AlphaDisplayCode, rightsowner, projectmanager, projectmanagercontactID, urgent, title, subtitle, series, barcode, language, projectnumber, "
l_strSQL = l_strSQL & "uniqueID, componenttype, programtype, episode, duedate, textlessrequired, MERequired, ME51Required, ProxyReq, itunes, conform, complaintredoitem) VALUES ("
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & l_lngCompanyID & ", "
If l_strReference <> "" Then
    l_strSQL = l_strSQL & "'" & l_strReference & "', "
Else
    l_strSQL = l_strSQL & "NULL, "
End If
If l_strAlphaDisplayCode <> "" Then
    l_strSQL = l_strSQL & "'" & l_strAlphaDisplayCode & "', "
Else
    l_strSQL = l_strSQL & "NULL, "
End If
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strRightsOwner) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strProjectManager) & "', "
l_strSQL = l_strSQL & l_lngProjectManagerContactID & ", "
l_strSQL = l_strSQL & l_lngPriority & ", "
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSubTitle) & "', "
l_strSQL = l_strSQL & "'" & l_strSeries & "', "
l_strSQL = l_strSQL & "'" & UCase(l_strBarcode) & "', "
l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
l_strSQL = l_strSQL & "'" & l_strProjectNumber & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strUniqueID) & "', "
l_strSQL = l_strSQL & "'" & l_strComponentType & "', "
l_strSQL = l_strSQL & "'" & l_strProgramType & "', "
l_strSQL = l_strSQL & "'" & l_strEpisode & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDueDate) & "', "
l_strSQL = l_strSQL & l_lngTextlessRequired & ", "
l_strSQL = l_strSQL & l_lngMERequired & ", "
l_strSQL = l_strSQL & l_lngME51Required & ", "
l_strSQL = l_strSQL & l_lngProxyRequired & ", "
l_strSQL = l_strSQL & l_lngiTunesRequired & ", "
l_strSQL = l_strSQL & l_lngConform & ", "
l_strSQL = l_strSQL & "1);"
Debug.Print l_strSQL
ExecuteSQL l_strSQL, g_strExecuteError
If CheckForSQLError = False Then
    CreateNewSvenskTrackerLine = g_lngLastID
Else
    CreateNewSvenskTrackerLine = 0
End If

End Function

Function SlashBackToForward(lp_strText As Variant) As String

    Dim l_strNewText As String
    Dim l_strOldText As String
    Dim l_intLoop As Integer
    l_strNewText = ""
    
    If IsNull(lp_strText) Then
        l_strOldText = ""
        l_strNewText = l_strOldText
        lp_strText = ""
    Else
        l_strOldText = lp_strText
    End If
    
    If InStr(lp_strText, "\") <> 0 Then
        
        For l_intLoop = 1 To Len(l_strOldText)
            
            Select Case Mid(l_strOldText, l_intLoop, 1)
                
                Case "\"
                    l_strNewText = l_strNewText & "/"
                
                Case Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
            
            End Select
            
        Next
    Else
        l_strNewText = lp_strText
    End If
    
    SlashBackToForward = l_strNewText
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function SlashForwardToBack(lp_strText As Variant) As String

    Dim l_strNewText As String
    Dim l_strOldText As String
    Dim l_intLoop As Integer
    l_strNewText = ""
    
    If IsNull(lp_strText) Then
        l_strOldText = ""
        l_strNewText = l_strOldText
        lp_strText = ""
    Else
        l_strOldText = lp_strText
    End If
    
    If InStr(lp_strText, "/") <> 0 Then
        
        For l_intLoop = 1 To Len(l_strOldText)
            
            Select Case Mid(l_strOldText, l_intLoop, 1)
                
                Case "/"
                    l_strNewText = l_strNewText & "\"
                
                Case Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
            
            End Select
            
        Next
    Else
        l_strNewText = lp_strText
    End If
    
    SlashForwardToBack = l_strNewText
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function ShowiTunesPackage(lp_lngiTunesPackageID As Long)

If lp_lngiTunesPackageID <> 0 Then
    frmiTunesPackage.lblSearchPackage.Caption = lp_lngiTunesPackageID
    frmiTunesPackage.optFilter(0).Value = True
    frmiTunesPackage.Show
    frmiTunesPackage.ZOrder 0
End If

End Function

Sub SendJobToDADC(lp_lngJobID As Long, lp_strType As String, lp_lngBatchNumber As Long)

Dim l_strTheFacilityNumber As String, l_strTheBookingNumber As String, l_strTheOrderNumber As String, l_strTheOrderDate As String
Dim l_strTheTitle As String, l_strTheSubtitle As String, l_strTheCustomer As String, l_strTheChargeCode As String, l_strTheCountryCode As String
Dim l_strTheBusinessCode As String, l_strTheNominalCode As String, l_strTheProgrammeNumber As String, l_strTheInvoiceNumber As String
Dim l_strTheInvoiceDate As String, l_strTheItemCode As String, l_strTheItemQuantity As String, l_strTheDuration As String
Dim l_strTheBusinessArea As String, l_strTheUnitCharge  As String, l_strOrderContactName As String, l_strTheSeriesID As String
Dim l_dblTotalNet As Double, l_dblTotalVAT As Double, l_dblTotalGross As Double, l_strTheCompany As String, l_lngCompanyID As Long
Dim l_strCopyType As String, l_strJobNotes As String, l_strCostingDescription As String
Dim l_strTheProjectManager As String, l_strTheProjectNumber As String

Dim l_rstCostingDetail As ADODB.Recordset, l_strSQL As String
Dim l_blnAllLines As Boolean

'Arrives here with lp_strType either set to BBCWW, BBCTV, or EntsRights - this routine then posts that job to the already open batch file

l_strSQL = "SELECT * FROM costing WHERE (jobID = " & lp_lngJobID & ") AND (creditnotenumber IS Null or creditnotenumber = 0) ORDER by costingID"

Set l_rstCostingDetail = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError

If Not l_rstCostingDetail.EOF Then
    'Not BBC Worldwide - need to get the salesledgercode of the customer for the text file
    'get the company ID as it is needed to work out other values
    
    'Changes itroducd so that this batch file can be sent while the job is at 'Hold Cost'
    
    l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
    l_strTheInvoiceNumber = lp_lngJobID 'GetData("job", "invoicenumber", "jobID", lp_lngJobID)
    l_strTheInvoiceDate = Format(Now, "dd/mm/yyyy") 'Format(GetData("job", "invoiceddate", "jobID", lp_lngJobID), "dd/mm/yyyy")
    l_strTheOrderNumber = GetData("job", "orderreference", "jobid", lp_lngJobID)
    If l_strTheOrderNumber = "" Then l_strTheOrderNumber = lp_lngJobID
    
    l_strJobNotes = CRLFSanitise(GetData("job", "notes2", "jobid", lp_lngJobID))
    l_strTheTitle = GetData("job", "title1", "jobID", lp_lngJobID)
    If InStr(l_strTheTitle, Chr(9)) > 0 Then l_strTheTitle = Replace(l_strTheTitle, Chr(9), " ")
    If InStr(l_strTheTitle, vbCrLf) > 0 Then l_strTheTitle = Replace(l_strTheTitle, vbCrLf, " ")
    l_strTheSubtitle = GetData("job", "title2", "jobID", lp_lngJobID)
    If InStr(l_strTheSubtitle, Chr(9)) > 0 Then l_strTheSubtitle = Replace(l_strTheSubtitle, Chr(9), " ")
    If InStr(l_strTheSubtitle, vbCrLf) > 0 Then l_strTheSubtitle = Replace(l_strTheSubtitle, vbCrLf, " ")
    l_strTheSeriesID = GetData("job", "seriesID", "jobID", lp_lngJobID)

    l_strOrderContactName = GetData("job", "contactname", "jobID", lp_lngJobID)
    
    l_dblTotalNet = 0
    l_dblTotalVAT = 0
    l_dblTotalGross = 0
    l_rstCostingDetail.MoveFirst
    Do While Not l_rstCostingDetail.EOF
        

        Print #1, l_strTheOrderNumber & Chr(9);
        Print #1, l_strOrderContactName & Chr(9);
        Print #1, Trim(l_strTheTitle) & Chr(9);
        Print #1, Trim(l_strTheSubtitle) & Chr(9);
        Print #1, GetData("jobdetail", "DADCWorkflowID", "jobdetailID", l_rstCostingDetail("jobdetailID")) & Chr(9);
        Print #1, GetData("jobdetail", "DADCCVCode", "jobdetailID", l_rstCostingDetail("jobdetailID")) & Chr(9);
        Print #1, l_strTheInvoiceNumber & Chr(9);
        If lp_strType = "Svensk" Or lp_strType = "DADC" Then
            Print #1, l_strTheInvoiceDate & Chr(9);
            Print #1, GetData("jobdetail", "DADCProjectManager", "jobdetailID", l_rstCostingDetail("jobdetailID")) & Chr(9);
            Print #1, GetData("jobdetail", "DADCProjectNumber", "jobdetailID", l_rstCostingDetail("jobdetailID")) & Chr(9);
            Print #1, GetData("jobdetail", "DADCJobTitle", "jobdetailID", l_rstCostingDetail("jobdetailID")) & Chr(9);
            Print #1, GetData("jobdetail", "DADCSeries", "jobdetailID", l_rstCostingDetail("jobdetailID")) & Chr(9);
            Print #1, GetData("jobdetail", "DADCEpisodeNumber", "jobdetailID", l_rstCostingDetail("jobdetailID")) & Chr(9);
            Print #1, GetData("jobdetail", "DADCEpisodeTitle", "jobdetailID", l_rstCostingDetail("jobdetailID")) & Chr(9);
            Print #1, GetData("jobdetail", "DADCRightsOwner", "jobdetailID", l_rstCostingDetail("jobdetailID")) & Chr(9);
        End If
        l_strCostingDescription = l_rstCostingDetail("description")
        If InStr(l_strCostingDescription, Chr(9)) > 0 Then l_strCostingDescription = Replace(l_strCostingDescription, Chr(9), " ")
        If InStr(l_strCostingDescription, vbCrLf) > 0 Then l_strCostingDescription = Replace(l_strCostingDescription, vbCrLf, " ")
        If Left(l_strCostingDescription, 2) = "- " Then
            Print #1, Mid(l_strCostingDescription, 3) & Chr(9);
        Else
            Print #1, l_strCostingDescription & Chr(9);
        End If
        Print #1, l_rstCostingDetail("costcode") & Chr(9);
        Print #1, GetData("jobdetail", "workflowdescription", "jobdetailID", l_rstCostingDetail("jobdetailID")) & Chr(9);
        If lp_strType = "SONYDADC" And GetData("jobdetail", "lastcommentfromtracker", "jobdetailID", l_rstCostingDetail("jobdetailID")) <> "" Then
            Print #1, GetData("jobdetail", "lastcommentdate", "jobdetailID", l_rstCostingDetail("jobdetailID")) & "," & _
            GetData("jobdetail", "lastcommentcuser", "jobdetailID", l_rstCostingDetail("jobdetailID")) & " - " & _
            GetData("jobdetail", "lastcommentfromtracker", "jobdetailID", l_rstCostingDetail("jobdetailID")) & Chr(9);
        ElseIf lp_strType = "SONYDADC" Then
            Print #1, Chr(9);
        End If
        Print #1, l_rstCostingDetail("quantity") & Chr(9);
        Print #1, l_rstCostingDetail("fd_time") & Chr(9);
        Print #1, Format(l_rstCostingDetail("unitcharge"), "#.00") & Chr(9);
        Print #1, Format(l_rstCostingDetail("total"), "#.00") & Chr(9);
        Print #1, Format(l_rstCostingDetail("vat"), "#.00") & Chr(9);
        Print #1, Format(l_rstCostingDetail("totalincludingvat"), "#.00") & Chr(9);
        Print #1, GetData("jobdetail", "DADCLegacyOracTVAENumber", "jobdetailID", l_rstCostingDetail("jobdetailID")) & Chr(9);
        Print #1, GetData("jobdetail", "DADCSeries", "jobdetailID", l_rstCostingDetail("jobdetailID")) & Chr(9)

        l_rstCostingDetail.MoveNext
    
    Loop
    
End If

'Set the BatchNumber
SetData "job", "senttoBBCbatchnumber", "jobID", lp_lngJobID, lp_lngBatchNumber

End Sub

Function IsItHere(lp_strLocation As String) As Boolean

Dim l_blnIsIt As Boolean, l_varTemp As Variant
l_blnIsIt = False

If IsNumeric(lp_strLocation) Then
    
    'Numeric locations are job numbers - it is booked out to a job number - this is an internal location
    l_blnIsIt = True

Else
    
    l_varTemp = GetDataSQL("SELECT TOP 1 format FROM xref WHERE category = 'location' and description = '" & lp_strLocation & "';")
    Select Case l_varTemp
        'List of locations that are defined as 'Here' - not including the ones that will be going away
        Case "ONSITE"
            l_blnIsIt = True
        Case Else
            l_blnIsIt = False
    
    End Select

End If

IsItHere = l_blnIsIt

End Function

Function GetCETASetting(lp_strSettingName As String) As Variant

GetCETASetting = GetData("setting", "value", "name", lp_strSettingName)

End Function

Function SetCETASetting(lp_strSettingName As String, lp_varSettingValue As Variant) As Variant

SetCETASetting = SetData("setting", "value", "name", lp_strSettingName, lp_varSettingValue)

End Function

Public Function GetNewTargetDate() As Date

Dim l_rst As ADODB.Recordset
Set l_rst = ExecuteSQL("exec fn_getWorkingDaysFromDate @fromdate='" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', @workingdays=1", g_strExecuteError)
GetNewTargetDate = l_rst(0)
l_rst.Close

End Function

Public Function GetOsBitness() As String
    Dim ProcessorSet As Object
    Dim CPU As Object

    Set ProcessorSet = GetObject("Winmgmts:"). _
        ExecQuery("SELECT * FROM Win32_Processor")
    For Each CPU In ProcessorSet
        GetOsBitness = CStr(CPU.AddressWidth)
    Next
End Function

Public Function ExecCmd(lp_strCommand) As Long
   Dim proc As PROCESS_INFORMATION
   Dim start As STARTUPINFO
   Dim Returncode As Long

   ' Initialize the STARTUPINFO structure:
   start.cb = Len(start)

   ' Start the shelled application:
   Returncode = CreateProcessA(vbNullString, lp_strCommand, 0&, 0&, 1&, _
      NORMAL_PRIORITY_CLASS, 0&, vbNullString, start, proc)

   ' Wait for the shelled application to finish:
      Returncode = WaitForSingleObject(proc.hProcess, INFINITE)
      Call GetExitCodeProcess(proc.hProcess, Returncode)
      Call CloseHandle(proc.hThread)
      Call CloseHandle(proc.hProcess)
      ExecCmd = Returncode
End Function

Public Function NoSpaces(lp_string As String) As String

Dim temp As String
temp = Trim(lp_string)

NoSpaces = ""
If InStr(temp, " ") > 0 Then
    Do While InStr(temp, " ") > 0
        NoSpaces = Left(temp, InStr(temp, " "))
        temp = Mid(temp, InStr(temp, " ") + 1)
    Loop
Else
    NoSpaces = temp
End If

End Function

Public Sub NotifyByEMail(lp_strBatchReport As String)

Dim rs As ADODB.Recordset, SQL As String

SQL = "SELECT email, fullname FROM trackernotification WHERE contractgroup = 'MX1' and trackermessageID = 28;"
Set rs = ExecuteSQL(SQL, g_strExecuteError)

If rs.RecordCount > 0 Then
    rs.MoveFirst
    Do While Not rs.EOF
        SendSMTPMail rs("email"), rs("fullname"), "MX1 Posting to Accounts", "A Posting Batch has been sent to Accounts.", lp_strBatchReport, True, "", "", "thart@visualdatamedia.com"
        rs.MoveNext
    Loop
End If
rs.Close
Set rs = Nothing

End Sub

