VERSION 5.00
Begin VB.Form frmLabels 
   Caption         =   "Labels"
   ClientHeight    =   7065
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7515
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLabels.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7065
   ScaleWidth      =   7515
   StartUpPosition =   1  'CenterOwner
   Begin VB.OptionButton Option1 
      Caption         =   "DG Stationary"
      Height          =   315
      Index           =   3
      Left            =   300
      TabIndex        =   53
      ToolTipText     =   "USe BBC stationary"
      Top             =   5340
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.Timer timFormClose 
      Interval        =   20000
      Left            =   240
      Top             =   6240
   End
   Begin VB.CommandButton cmdPreview 
      Caption         =   "Preview Labels"
      Height          =   315
      Left            =   1140
      TabIndex        =   52
      ToolTipText     =   "Print the Labels"
      Top             =   6600
      Width           =   1215
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   22
      Left            =   4440
      TabIndex        =   50
      ToolTipText     =   "SubTitle for the Labels"
      Top             =   480
      Width           =   975
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   21
      Left            =   6420
      TabIndex        =   47
      ToolTipText     =   "SubTitle for the Labels"
      Top             =   480
      Width           =   975
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   20
      Left            =   2460
      TabIndex        =   46
      ToolTipText     =   "SubTitle for the Labels"
      Top             =   480
      Width           =   975
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Blank Stationary"
      Height          =   315
      Index           =   2
      Left            =   300
      TabIndex        =   45
      ToolTipText     =   "USe BBC stationary"
      Top             =   4980
      Width           =   3375
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   19
      Left            =   5340
      TabIndex        =   44
      ToolTipText     =   "A8 Contents for the Labels"
      Top             =   6120
      Width           =   2055
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   18
      Left            =   5340
      TabIndex        =   43
      Text            =   " "
      ToolTipText     =   "A7 Contents for the Labels"
      Top             =   5700
      Width           =   2055
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   17
      Left            =   5340
      TabIndex        =   42
      ToolTipText     =   "A6 Contents for the Labels"
      Top             =   5280
      Width           =   2055
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   16
      Left            =   5340
      TabIndex        =   41
      ToolTipText     =   "A5 Contents for the Labels"
      Top             =   4860
      Width           =   2055
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   15
      Left            =   5340
      TabIndex        =   40
      ToolTipText     =   "A4 Contents for the Labels"
      Top             =   4440
      Width           =   2055
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   14
      Left            =   5340
      TabIndex        =   39
      ToolTipText     =   "A3 Contents for the Labels"
      Top             =   4020
      Width           =   2055
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   13
      Left            =   5340
      TabIndex        =   38
      ToolTipText     =   "A2 Contents for the Labels"
      Top             =   3600
      Width           =   2055
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   12
      Left            =   5340
      TabIndex        =   37
      ToolTipText     =   "A1 Contents for the Labels"
      Top             =   3180
      Width           =   2055
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   4020
      TabIndex        =   36
      ToolTipText     =   "Close this form"
      Top             =   6600
      Width           =   1215
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print Labels"
      Height          =   315
      Left            =   2580
      TabIndex        =   35
      ToolTipText     =   "Print the Labels"
      Top             =   6600
      Width           =   1215
   End
   Begin VB.OptionButton Option1 
      Caption         =   "BBC Stationary"
      Height          =   315
      Index           =   1
      Left            =   300
      TabIndex        =   34
      ToolTipText     =   "USe BBC stationary"
      Top             =   4620
      Width           =   3375
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Normal Stationary"
      Height          =   315
      Index           =   0
      Left            =   300
      TabIndex        =   33
      ToolTipText     =   "Use JCA stationary"
      Top             =   4260
      Width           =   3375
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Don't Print Barcodes"
      Height          =   315
      Left            =   300
      TabIndex        =   32
      ToolTipText     =   "Don't print Barcode numbers on the labels"
      Top             =   5760
      Width           =   3375
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   11
      Left            =   5340
      TabIndex        =   24
      ToolTipText     =   "Format for the Labels"
      Top             =   2580
      Width           =   2055
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   10
      Left            =   5340
      TabIndex        =   23
      ToolTipText     =   "Picture Ratio for the Labels"
      Top             =   2160
      Width           =   2055
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   9
      Left            =   5340
      TabIndex        =   18
      ToolTipText     =   "Geometric Aspect for the Labels"
      Top             =   1740
      Width           =   2055
   End
   Begin VB.TextBox Text1 
      Enabled         =   0   'False
      Height          =   315
      Index           =   8
      Left            =   2460
      TabIndex        =   17
      Top             =   3840
      Width           =   1875
   End
   Begin VB.TextBox Text1 
      Enabled         =   0   'False
      Height          =   315
      Index           =   7
      Left            =   2460
      TabIndex        =   16
      Top             =   3420
      Width           =   1875
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   6
      Left            =   2460
      TabIndex        =   15
      ToolTipText     =   "Order Number for the Labels"
      Top             =   3000
      Width           =   1875
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   5
      Left            =   2460
      TabIndex        =   14
      Text            =   " "
      ToolTipText     =   "Job Number for the Labels"
      Top             =   2580
      Width           =   1875
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   4
      Left            =   2460
      TabIndex        =   13
      ToolTipText     =   "Duration for the Labels"
      Top             =   2160
      Width           =   1875
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   3
      Left            =   2460
      TabIndex        =   12
      ToolTipText     =   "Video Standard for the Labels"
      Top             =   1740
      Width           =   1875
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   2
      Left            =   2460
      TabIndex        =   3
      ToolTipText     =   "Version for the Labels"
      Top             =   1320
      Width           =   4935
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   1
      Left            =   2460
      TabIndex        =   2
      ToolTipText     =   "SubTitle for the Labels"
      Top             =   900
      Width           =   4935
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   0
      Left            =   2460
      TabIndex        =   0
      ToolTipText     =   "Title for the Labels"
      Top             =   60
      Width           =   4935
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Set"
      Height          =   255
      Index           =   22
      Left            =   3660
      TabIndex        =   51
      Top             =   540
      Width           =   675
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Episode"
      Height          =   255
      Index           =   21
      Left            =   5640
      TabIndex        =   49
      Top             =   540
      Width           =   735
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Series"
      Height          =   255
      Index           =   20
      Left            =   120
      TabIndex        =   48
      Top             =   540
      Width           =   1995
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Audio 8"
      Height          =   255
      Index           =   19
      Left            =   4440
      TabIndex        =   31
      Top             =   6180
      Width           =   795
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Audio 7"
      Height          =   255
      Index           =   18
      Left            =   4440
      TabIndex        =   30
      Top             =   5760
      Width           =   795
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Audio 6"
      Height          =   255
      Index           =   17
      Left            =   4440
      TabIndex        =   29
      Top             =   5340
      Width           =   795
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Audio 5"
      Height          =   255
      Index           =   16
      Left            =   4440
      TabIndex        =   28
      Top             =   4920
      Width           =   795
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Audio 4"
      Height          =   255
      Index           =   15
      Left            =   4440
      TabIndex        =   27
      Top             =   4500
      Width           =   795
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Audio 3"
      Height          =   255
      Index           =   14
      Left            =   4440
      TabIndex        =   26
      Top             =   4080
      Width           =   795
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Audio 2"
      Height          =   255
      Index           =   13
      Left            =   4440
      TabIndex        =   25
      Top             =   3660
      Width           =   795
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Audio 1"
      Height          =   255
      Index           =   12
      Left            =   4440
      TabIndex        =   22
      Top             =   3240
      Width           =   795
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Format"
      Height          =   255
      Index           =   11
      Left            =   4440
      TabIndex        =   21
      Top             =   2640
      Width           =   795
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Ratio"
      Height          =   255
      Index           =   10
      Left            =   4500
      TabIndex        =   20
      Top             =   2220
      Width           =   735
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Aspect"
      Height          =   255
      Index           =   9
      Left            =   4560
      TabIndex        =   19
      Top             =   1800
      Width           =   675
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Barcode String"
      Height          =   255
      Index           =   8
      Left            =   120
      TabIndex        =   11
      Top             =   3900
      Width           =   1995
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Barcode"
      Height          =   255
      Index           =   7
      Left            =   120
      TabIndex        =   10
      Top             =   3480
      Width           =   1995
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Customer Order Number"
      Height          =   255
      Index           =   6
      Left            =   120
      TabIndex        =   9
      Top             =   3060
      Width           =   1995
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Job Number"
      Height          =   255
      Index           =   5
      Left            =   120
      TabIndex        =   8
      Top             =   2640
      Width           =   1995
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Duration"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   7
      Top             =   2220
      Width           =   1995
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Standard"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   6
      Top             =   1800
      Width           =   1995
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Version"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   5
      Top             =   1320
      Width           =   1995
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Sub Title"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   4
      Top             =   960
      Width           =   1995
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Main Title"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1995
   End
End
Attribute VB_Name = "frmLabels"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim m_intMinutesToCloseForm As Integer

Private Sub cmdClose_Click()
Me.Hide
End Sub

Private Sub cmdPreview_Click()

Dim l_strReportToPrint As String, l_strSQL As String, l_lngID As Long

g_blnPreviewReport = True

If Option1(1) = True Then
    l_strReportToPrint = g_strLocationOfCrystalReportFiles & "label2.rpt"
ElseIf Option1(3) = True Then
    l_strReportToPrint = g_strLocationOfCrystalReportFiles & "label_DG.rpt"
Else
    l_strReportToPrint = g_strLocationOfCrystalReportFiles & "label.rpt"
End If

'load up the labelprint table with the values from the form

l_strSQL = "INSERT INTO labelprinting ("
l_strSQL = l_strSQL & "cuser, cdate, field0, field1, field2, field3, field4, field5, field6, field7, "
l_strSQL = l_strSQL & "field8, field9, field10, field11, field12, field13, field14, field15, "
l_strSQL = l_strSQL & "field16, field17, field18, field19, field20, field21, field22) VALUES ("

l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(0).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(1).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(2).Text) & "', "
l_strSQL = l_strSQL & "'" & Text1(3).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(4).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(5).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(6).Text & "', "
If Check1.Value = False Then
    l_strSQL = l_strSQL & "'" & Text1(7).Text & "', "
    l_strSQL = l_strSQL & "'" & Text1(8).Text & "', "
Else
    l_strSQL = l_strSQL & "' ', "
    l_strSQL = l_strSQL & "' ', "
End If
l_strSQL = l_strSQL & "'" & Text1(9).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(10).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(11).Text & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(12).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(13).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(14).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(15).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(16).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(17).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(18).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(19).Text) & "', "
l_strSQL = l_strSQL & "'" & Text1(20).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(21).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(22).Text & "')"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_lngID = g_lngLastID

'print the report
PrintCrystalReport l_strReportToPrint, "{labelprinting.labelprintingID} =" & Str(l_lngID), g_blnPreviewReport

g_blnPreviewReport = False

End Sub

Private Sub cmdPrint_Click()

Dim l_strReportToPrint As String, l_strSQL As String, l_lngID As Long

'work out which report to print
If Option1(1) = True Then
    l_strReportToPrint = g_strLocationOfCrystalReportFiles & "label2.rpt"
ElseIf Option1(3) = True Then
    l_strReportToPrint = g_strLocationOfCrystalReportFiles & "label_DG.rpt"
Else
    l_strReportToPrint = g_strLocationOfCrystalReportFiles & "label.rpt"
End If

'load up the labelprint table with the values from the form

l_strSQL = "INSERT INTO labelprinting ("
l_strSQL = l_strSQL & "cuser, cdate, field0, field1, field2, field3, field4, field5, field6, field7, "
l_strSQL = l_strSQL & "field8, field9, field10, field11, field12, field13, field14, field15, "
l_strSQL = l_strSQL & "field16, field17, field18, field19, field20, field21, field22) VALUES ("

l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(0).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(1).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(2).Text) & "', "
l_strSQL = l_strSQL & "'" & Text1(3).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(4).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(5).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(6).Text & "', "
If Check1.Value = False Then
    l_strSQL = l_strSQL & "'" & Text1(7).Text & "', "
    l_strSQL = l_strSQL & "'" & Text1(8).Text & "', "
Else
    l_strSQL = l_strSQL & "' ', "
    l_strSQL = l_strSQL & "' ', "
End If
l_strSQL = l_strSQL & "'" & Text1(9).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(10).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(11).Text & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(12).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(13).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(14).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(15).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(16).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(17).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(18).Text) & "', "
l_strSQL = l_strSQL & "'" & QuoteSanitise(Text1(19).Text) & "', "
l_strSQL = l_strSQL & "'" & Text1(20).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(21).Text & "', "
l_strSQL = l_strSQL & "'" & Text1(22).Text & "')"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_lngID = g_lngLastID

'print the report
PrintCrystalReport l_strReportToPrint, "{labelprinting.labelprintingID} =" & Str(l_lngID), g_blnPreviewReport

'get rid of that labelprint record again.

l_strSQL = "DELETE FROM labelprinting WHERE labelprintingID = " & Str(l_lngID)
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Sub

Private Sub timFormClose_Timer()

g_optCloseSystemDown = GetFlag(GetData("setting", "value", "name", "CloseSystemDown"))

If g_optCloseSystemDown <> 0 Then Unload Me

End Sub
