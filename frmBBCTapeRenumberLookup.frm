VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmBBCTapeRenumberLookup 
   Caption         =   "BBC Tape Renumber Lookup"
   ClientHeight    =   6870
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12825
   Icon            =   "frmBBCTapeRenumberLookup.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6870
   ScaleWidth      =   12825
   WindowState     =   2  'Maximized
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   240
      TabIndex        =   1
      Top             =   5520
      Width           =   12315
      Begin VB.TextBox txtAlias 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1500
         TabIndex        =   6
         Top             =   120
         Width           =   2895
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11100
         TabIndex        =   4
         ToolTipText     =   "Close this form"
         Top             =   120
         Width           =   1155
      End
      Begin VB.TextBox txtSearch 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7020
         TabIndex        =   3
         Top             =   120
         Width           =   2655
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9840
         TabIndex        =   2
         ToolTipText     =   "Close this form"
         Top             =   120
         Width           =   1155
      End
      Begin VB.Label Label1 
         Caption         =   "Original Barcode"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   7
         Top             =   180
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Corrected Barcode"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   5460
         TabIndex        =   5
         Top             =   180
         Width           =   1455
      End
   End
   Begin MSAdodcLib.Adodc adoBBCBarcodeCorrection 
      Height          =   330
      Left            =   120
      Top             =   120
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoBBCBarcodeCorrection"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdBBCBarcodeCorrection 
      Bindings        =   "frmBBCTapeRenumberLookup.frx":08CA
      Height          =   4515
      Left            =   120
      TabIndex        =   0
      ToolTipText     =   "The Options on a Combo Box"
      Top             =   120
      Width           =   8220
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AllowDelete     =   -1  'True
      BackColorOdd    =   16777152
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   5
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "bbcaliasID"
      Columns(0).Name =   "bbc_barcode_correctionID"
      Columns(0).DataField=   "bbc_barcode_correctionID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3519
      Columns(1).Caption=   "Original Barcode"
      Columns(1).Name =   "original_barcode"
      Columns(1).DataField=   "original_barcode"
      Columns(1).FieldLen=   255
      Columns(2).Width=   3519
      Columns(2).Caption=   "Corrected Barcode"
      Columns(2).Name =   "corrected_barcode"
      Columns(2).DataField=   "corrected_barcode"
      Columns(2).FieldLen=   50
      Columns(3).Width=   3200
      Columns(3).Caption=   "Date Created"
      Columns(3).Name =   "date_created"
      Columns(3).DataField=   "date_created"
      Columns(3).DataType=   7
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "Created By"
      Columns(4).Name =   "created_by"
      Columns(4).DataField=   "created_by"
      Columns(4).FieldLen=   256
      _ExtentX        =   14499
      _ExtentY        =   7964
      _StockProps     =   79
      Caption         =   "BBC Barcode Correction"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmBBCTapeRenumberLookup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdSearch_Click()

Dim l_strSQL As String

l_strSQL = "SELECT * FROM bbc_barcode_correction"

l_strSQL = l_strSQL & " WHERE 1=1 "
If txtAlias.Text <> "" Then l_strSQL = l_strSQL & " AND original_barcode LIKE '" & txtAlias.Text & "%'"
If txtSearch.Text <> "" Then l_strSQL = l_strSQL & " AND corrected_barcode LIKE '" & txtSearch.Text & "%'"

l_strSQL = l_strSQL & "  ORDER BY original_barcode"

Debug.Print l_strSQL
adoBBCBarcodeCorrection.ConnectionString = g_strConnection
adoBBCBarcodeCorrection.RecordSource = l_strSQL
adoBBCBarcodeCorrection.Refresh

adoBBCBarcodeCorrection.Caption = adoBBCBarcodeCorrection.Recordset.RecordCount & " items"

End Sub

Private Sub Form_Load()

cmdSearch.Value = True

End Sub

Private Sub Form_Resize()

Frame1.Top = Me.ScaleHeight - Frame1.Height - 120
Frame1.Left = Me.ScaleWidth - Frame1.Width - 120

grdBBCBarcodeCorrection.Height = Me.ScaleHeight - cmdClose.Height - 120 - 120
'grdBBCAlias.Width = Me.ScaleWidth - 120 - 120

End Sub

Private Sub grdBBCBarcodeCorrection_AfterDelete(RtnDispErrMsg As Integer)
adoBBCBarcodeCorrection.Caption = adoBBCBarcodeCorrection.Recordset.RecordCount & " items"
End Sub
