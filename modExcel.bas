Attribute VB_Name = "modExcel"
Option Explicit
'the purpose of this module is to have variables pointing to the
'different areas of the Star sheet. This enables us to change the
'setup if BBC change the layout.
Public Const g_cntBookedByRow% = 5
Public Const g_cntBookedByColumn% = 6

Public Const g_cntAccountNumberRow% = 3
Public Const g_cntAccountNumberColumn% = 2

Public Const g_cntTelephoneRow% = 6
Public Const g_cntTelephoneColumn% = 6
Public g_objOutlook As Object

Public Function GetInclusiveCode(lp_strRecordFormat As String, lp_strMasterFormat As String, lp_strMasterStandard As String, lp_strRecordStandard As String) As String

GetInclusiveCode = ""

If (lp_strMasterFormat = "DVDROM" Or lp_strMasterFormat = "BWAV" Or lp_strMasterFormat = "WAV") And (lp_strRecordFormat = "DVDROM" Or lp_strRecordFormat = "BWAV" Or lp_strRecordFormat = "WAV") Then
    GetInclusiveCode = "WWDVDCOPY"
    Exit Function
End If

If lp_strMasterFormat Like "HD*" Then
    
    'HD Originated codes.
    GetInclusiveCode = "WW"
    
    'First part is masterformat
    If lp_strMasterFormat = "HDD5" Then GetInclusiveCode = GetInclusiveCode & "HD"
    If lp_strMasterFormat = "HDCAM" Then GetInclusiveCode = GetInclusiveCode & "HD"
    If lp_strMasterFormat = "HDCAM-SR" Then GetInclusiveCode = GetInclusiveCode & "HD"
    
ElseIf lp_strMasterFormat = "DVD" Then
    
    'DVD & VHS copying
    If lp_strRecordFormat = "DVD" Then GetInclusiveCode = "WWDVD"

ElseIf lp_strMasterFormat = "DBETA" Then
    
    GetInclusiveCode = "WWSDDB"

ElseIf lp_strMasterFormat = "BETASP" Then
    
    GetInclusiveCode = "WWSDDB"

ElseIf lp_strMasterFormat = "D3" Then
    
    GetInclusiveCode = "WWSDD3"

ElseIf lp_strMasterFormat = "C-FORM" Then
    
    GetInclusiveCode = "WWSD1C"

ElseIf lp_strMasterFormat = "B-FORM" Then
    
    GetInclusiveCode = "WWSD1B"

ElseIf lp_strMasterFormat = "HI8DAT" Then

    GetInclusiveCode = "WWAUDDA88"
    
ElseIf lp_strMasterFormat = "DAT" Then

    GetInclusiveCode = "WWAUDDAT"

ElseIf lp_strMasterFormat = "Q-INCH" Then

    GetInclusiveCode = "WWAUDQI"

ElseIf lp_strMasterFormat = "FILE" Then

    If lp_strRecordFormat Like "HD*" Then
        GetInclusiveCode = "WWHDMED"
    Else
        GetInclusiveCode = "WWSDMED"
    End If

End If

If lp_strRecordFormat = "HDCAM-SR" Then GetInclusiveCode = GetInclusiveCode & "HDSR"
If lp_strRecordFormat = "HDCAM" Then GetInclusiveCode = GetInclusiveCode & "HDCAM"
If lp_strRecordFormat = "HDD5" Then GetInclusiveCode = GetInclusiveCode & "HDD5"
If lp_strRecordFormat = "DBETA" Then GetInclusiveCode = GetInclusiveCode & "DB"
If lp_strRecordFormat = "MPEG-IMX" Then GetInclusiveCode = GetInclusiveCode & "IMX"
If lp_strRecordFormat = "D2" Then GetInclusiveCode = GetInclusiveCode & "D2"
If lp_strRecordFormat = "D3" Then GetInclusiveCode = GetInclusiveCode & "D3"
If lp_strRecordFormat = "D5" Then GetInclusiveCode = GetInclusiveCode & "D5"
If lp_strRecordFormat = "DVC" Then GetInclusiveCode = GetInclusiveCode & "DV25"
If lp_strRecordFormat = "DVCAM" Then GetInclusiveCode = GetInclusiveCode & "DVCAM"
If lp_strRecordFormat = "DVCPRO50" Then GetInclusiveCode = GetInclusiveCode & "DV50"
If lp_strRecordFormat = "BETASP" Then GetInclusiveCode = GetInclusiveCode & "SP"
If lp_strRecordFormat = "BETASX" Then GetInclusiveCode = GetInclusiveCode & "SX"
If lp_strRecordFormat = "HI8DAT" Then GetInclusiveCode = GetInclusiveCode & "DA88"
If lp_strRecordFormat = "DAT" Then GetInclusiveCode = GetInclusiveCode & "DAT"
If lp_strRecordFormat = "BVU" Then GetInclusiveCode = GetInclusiveCode & "BVU"
If lp_strRecordFormat = "BVUSP" Then GetInclusiveCode = GetInclusiveCode & "BVUSP"
If lp_strRecordFormat = "UMATIC" Then GetInclusiveCode = GetInclusiveCode & "U"
If lp_strRecordFormat = "SVHS" Then GetInclusiveCode = GetInclusiveCode & "SVHS"
If lp_strRecordFormat = "VHS" Then GetInclusiveCode = GetInclusiveCode & "VHS"
If lp_strRecordFormat = "DVD" Then GetInclusiveCode = GetInclusiveCode & "DVD"
If lp_strRecordFormat = "CDR" Then GetInclusiveCode = GetInclusiveCode & "CDR"
If lp_strRecordFormat = "AUDCAS" Then GetInclusiveCode = GetInclusiveCode & "CAS"
If lp_strRecordFormat = "MEDIA" Then GetInclusiveCode = "WWSDENC"

'Then add an "X" if crossconverter needed
If lp_strMasterFormat Like "HD*" And Not (lp_strRecordFormat Like "HD*") And Not (lp_strRecordFormat = "XDCAM-HD") And Not (lp_strRecordFormat = "DVC-PRO-HD") Then
    If InStr(lp_strMasterStandard, "50") > 0 And lp_strRecordStandard Like "625*" Then
        'do nothing
    ElseIf InStr(lp_strMasterStandard, "59") > 0 And lp_strRecordStandard Like "525*" Then
        'do nothing
    Else
        GetInclusiveCode = GetInclusiveCode & "XC"
    End If
ElseIf lp_strMasterStandard <> lp_strRecordStandard Then
    GetInclusiveCode = GetInclusiveCode & "XC"
End If

If GetInclusiveCode = "" Then GetInclusiveCode = "ASK"

End Function

Public Function Countcommas(lp_strText As String) As Integer

'go through the string passed and retrieve the number of commas, this will
'give us the amount of masters on this order line
Dim l_LetterPosition%, l_WordLength%, l_comma%

'set the length of the word so we can loop though it
l_WordLength% = Len(lp_strText)

'reset the number of commas found so far
l_comma% = 0

'loop through each letter in the word
For l_LetterPosition% = 1 To l_WordLength% Step 1

    'is it a comma?
    If Mid(lp_strText, l_LetterPosition%, 1) = "," Then
    
        'increment the comma count
        l_comma% = l_comma% + 1
        
    End If
    
'keep going
Next

Countcommas = l_comma%

End Function

Public Function GetAlias(lp_strText As String, Optional lp_strField As String) As String

'make sure we don't get balnks
If lp_strText = "" Then

    'return a blank
    GetAlias = ""
    
    'and exit
    Exit Function
    
End If

Dim i&
For i& = 1 To Len(lp_strText)
    If Mid$(lp_strText, i&, 1) = Chr$(34) Then
        Mid$(lp_strText, i&, 1) = Chr$(39)
    End If
Next

'get the alias of some text from the system, i.e. if there is some text
'in the spreadsheet, match it to an item in the alias table
Dim l_dynAlias As ADODB.Recordset
Dim l_sql$

l_sql$ = "SELECT * FROM bbcalias WHERE typedstring = '" & QuoteSanitise(lp_strText) & "'"
'create a recordset to get the value out
Set l_dynAlias = ExecuteSQL(l_sql$, g_strExecuteError)
CheckForSQLError

'check if there is a match
If l_dynAlias.RecordCount <= 0 Then

    'ask the user if they want to add a new alias
    Dim l_AddAlias%, l_strMessage As String
    
    If lp_strField = "" Then
        l_AddAlias% = MsgBox("There are no matching aliases for '" & lp_strText & "'. Do you want to create one?", vbQuestion + vbYesNo, "No match")
    Else
        l_AddAlias% = MsgBox("There are no matching aliases for '" & lp_strText & "' as " & lp_strField & ". Do you want to create one?", vbQuestion + vbYesNo, "No match")
    End If
    
    'if they do want to then
    If l_AddAlias% = vbYes Then
    
        'ask them what to add in
        Dim l_NewAlias$
        l_NewAlias$ = InputBox("Please enter the text that will replace '" & lp_strText & "' whenever it is imported.", "New string", lp_strText)
        
        'if they haven't cancelled the input box
        If l_NewAlias$ <> "" Then
        
            'add a new record into the Alias table
            l_dynAlias.AddNew
            
            'add in the string that was passed
            l_dynAlias("typedString") = Left$(lp_strText, 255)
            
            'and the alias
            l_dynAlias("bbcAlias") = Left$(l_NewAlias$, 50)
            
            'update the record
            l_dynAlias.Update
            
            'retrieve the thang
            GetAlias = l_NewAlias$
        End If
    
    Else
    
        'they don't want to add one, so use the original text
        GetAlias = lp_strText
        
    End If
        
Else

    'there is a match, so use the alias instead of the spreadsheet text
    GetAlias = l_dynAlias("bbcAlias")
    
End If

'close the recordset
l_dynAlias.Close: Set l_dynAlias = Nothing

End Function

Public Function GetXLData(lp_lngRow As Long, lp_Column As Long)

'declare a returning variable
Dim l_strReturnString As String

'get the data from the cell
On Error Resume Next
l_strReturnString = oWorksheet.Cells(lp_lngRow, lp_Column).Value
On Error GoTo 0

'send back the string
GetXLData = l_strReturnString

End Function

Public Function GetXLDataText(lp_lngRow As Long, lp_Column As Long)

'declare a returning variable
Dim l_strReturnString As String

'get the data from the cell
On Error Resume Next
l_strReturnString = oWorksheet.Cells(lp_lngRow, lp_Column).Text
On Error GoTo 0

'send back the string
GetXLDataText = l_strReturnString

End Function
Public Function SmartGetObject(l_strClassname$) As Integer

Set g_objOutlook = GetObject("", l_strClassname$)

If Err.Number = 429 Then
    
    Set g_objOutlook = GetObject(, l_strClassname$)
    g_objOutlook.Visible = 1

ElseIf Err.Number > 0 Then
    
    SmartGetObject = False
    Exit Function
    
End If

SmartGetObject = True



End Function


Public Sub SetStatus(lp_strText As String)

Dim Sheet1 As Excel.Worksheet
'sets the text in the cell on the spreadsheet that tells the user whats going on...
Sheet1.Cells(8, 3) = lp_strText

'let the computer catch up
DoEvents

End Sub

Public Function Process_Sound(lp_strText As String) As String

Process_Sound = ""
If lp_strText Like "*FINAL*L*" Or lp_strText Like "*FINAL*R*" Then Process_Sound = "St-Fin"
If lp_strText Like "*MAIN*L*" Or lp_strText Like "*MAIN*R*" Then Process_Sound = "St-Fin"
If lp_strText Like "*FINAL*MONO*" Or lp_strText Like "*MONO*MAIN*" Or lp_strText Like "*MAIN*MONO*" Then Process_Sound = "MONO"
If lp_strText Like "*M&E*L*" Or lp_strText Like "*M&E*R*" Then Process_Sound = "St-M&E"
If lp_strText Like "*M&E*MONO*" Or lp_strText Like "*MONO*M&E*" Then Process_Sound = "M&E"

End Function

Public Function GetContactID(ByRef lp_strContactName As String, Optional lp_lngCompanyID As Long) As Long

    'check a name has been passed
    If Trim(lp_strContactName) = "" Then
        GetContactID = 0
        Exit Function
    End If

    If lp_lngCompanyID = 0 Then lp_lngCompanyID = 570
    'declare whats needed
    Dim l_rstSettings As New ADODB.Recordset
    Dim l_strSQL As String
    Dim l_lngContactID As Long
    
    'design the SQL (#570 is the ID for BBC Worldwide International Ops Limited)
    l_strSQL = "SELECT contact.contactID FROM contact INNER JOIN employee ON contact.contactID = employee.contactID"
    l_strSQL = l_strSQL & " WHERE contact.name LIKE '" & QuoteSanitise(lp_strContactName) & "' "
    l_strSQL = l_strSQL & " AND employee.companyID = '" & lp_lngCompanyID & "';"
        
    'open the recordset
    Set l_rstSettings = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    'check a record was found
    If Not l_rstSettings.EOF Then
        l_rstSettings.MoveFirst
        
        'pick up the contactID
        l_lngContactID = l_rstSettings("contactID")
        
    Else
        'set the contact ID to be 0
        l_lngContactID = 0
    End If
    
    'close the recordset
    l_rstSettings.Close
    Set l_rstSettings = Nothing
        
    'return the correct value
    GetContactID = l_lngContactID
    
End Function

