VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmSelectContact 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Please select a new team member and their role"
   ClientHeight    =   1605
   ClientLeft      =   9105
   ClientTop       =   5115
   ClientWidth     =   4920
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1605
   ScaleWidth      =   4920
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   315
      Left            =   3660
      TabIndex        =   4
      ToolTipText     =   "Close this form"
      Top             =   1200
      Width           =   1155
   End
   Begin VB.CommandButton cmdSelect 
      Caption         =   "Select"
      Height          =   315
      Left            =   2400
      TabIndex        =   3
      ToolTipText     =   "Select current contact"
      Top             =   1200
      Width           =   1155
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbContact 
      Bindings        =   "frmSelectContact.frx":0000
      Height          =   255
      Left            =   1860
      TabIndex        =   1
      ToolTipText     =   "The contact for this company"
      Top             =   480
      Width           =   2955
      DataFieldList   =   "name"
      BevelType       =   0
      _Version        =   196617
      BorderStyle     =   0
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   5424
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "telephone"
      Columns(1).Name =   "telephone"
      Columns(1).DataField=   "telephone"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "contactID"
      Columns(2).Name =   "contactID"
      Columns(2).DataField=   "contactID"
      Columns(2).FieldLen=   256
      _ExtentX        =   5212
      _ExtentY        =   450
      _StockProps     =   93
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      DataFieldToDisplay=   "name"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Bindings        =   "frmSelectContact.frx":0019
      Height          =   255
      Left            =   1860
      TabIndex        =   0
      ToolTipText     =   "The company this contact is associated with"
      Top             =   120
      Width           =   2955
      BevelWidth      =   0
      DataFieldList   =   "name"
      BevelType       =   0
      _Version        =   196617
      BorderStyle     =   0
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "accountcode"
      Columns(2).Name =   "accountcode"
      Columns(2).DataField=   "accountcode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "telephone"
      Columns(3).Name =   "telephone"
      Columns(3).DataField=   "telephone"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "cetaclientcode"
      Columns(4).Name =   "cetaclientcode"
      Columns(4).DataField=   "cetaclientcode"
      Columns(4).FieldLen=   256
      _ExtentX        =   5212
      _ExtentY        =   450
      _StockProps     =   93
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      DataFieldToDisplay=   "name"
   End
   Begin MSAdodcLib.Adodc adoContact 
      Height          =   330
      Left            =   2880
      Top             =   3000
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoContact"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoCompany 
      Height          =   330
      Left            =   2880
      Top             =   2640
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCompany"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbRole 
      Height          =   255
      Left            =   1860
      TabIndex        =   2
      ToolTipText     =   "Contacts Role"
      Top             =   840
      Width           =   2955
      DataFieldList   =   "Column 0"
      BevelType       =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   12904430
      RowHeight       =   423
      Columns(0).Width=   5424
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).FieldLen=   256
      _ExtentX        =   5212
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.Image Image1 
      Height          =   240
      Left            =   120
      Picture         =   "frmSelectContact.frx":0032
      Top             =   120
      Width           =   240
   End
   Begin VB.Label lblCompanyID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Tag             =   "CLEARFIELDS"
      Top             =   2160
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblContactID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   300
      TabIndex        =   8
      Tag             =   "CLEARFIELDS"
      Top             =   1800
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Role"
      Height          =   255
      Index           =   0
      Left            =   720
      TabIndex        =   7
      Top             =   840
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      Height          =   255
      Index           =   2
      Left            =   720
      TabIndex        =   6
      Top             =   120
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Contact"
      Height          =   255
      Index           =   5
      Left            =   720
      TabIndex        =   5
      Top             =   480
      Width           =   1035
   End
End
Attribute VB_Name = "frmSelectContact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmbCompany_Click()
lblCompanyID.Caption = cmbCompany.Columns("companyID").Text
End Sub

Private Sub cmbContact_Click()

lblContactID.Caption = cmbContact.Columns("contactID").Text

End Sub

Private Sub cmbContact_DropDown()

If lblCompanyID.Caption = "" Then
    NoCompanySelectedMessage
    Exit Sub
End If

Dim l_strSQL As String

l_strSQL = "SELECT contact.contactID, contact.name, contact.telephone, employee.jobtitle FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((company.companyID)=" & lblCompanyID.Caption & ")) ORDER BY contact.name;"

adoContact.ConnectionString = g_strConnection
adoContact.RecordSource = l_strSQL
adoContact.Refresh
End Sub

Private Sub cmdCancel_Click()
Me.Tag = "CANCEL"
Me.Hide
End Sub

Private Sub cmdSelect_Click()

'need to check for valid input here...

Me.Hide
End Sub

Private Sub Form_Load()

MakeLookLikeOffice Me

Dim l_intLoop As Integer

For l_intLoop = 0 To Me.Controls.Count - 1
    AddOfficeBorder (Me.Controls(l_intLoop))
Next

'populate the company drop down (data bound)
Dim l_strSQL As String
l_strSQL = "SELECT name, accountcode, telephone, companyID FROM company ORDER BY name"

adoCompany.ConnectionString = g_strConnection
adoCompany.RecordSource = l_strSQL
adoCompany.Refresh

PopulateCombo "role", cmbRole

End Sub
