VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmPurchaseCompletion 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Complete Purchase Order"
   ClientHeight    =   5895
   ClientLeft      =   3015
   ClientTop       =   6975
   ClientWidth     =   13560
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5895
   ScaleWidth      =   13560
   ShowInTaskbar   =   0   'False
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   Begin MSAdodcLib.Adodc adoOurReference 
      Height          =   330
      Left            =   9000
      Top             =   3960
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoOurReference"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnOurReference 
      Bindings        =   "frmPurcaseCompletion.frx":0000
      Height          =   2235
      Left            =   7320
      TabIndex        =   15
      Top             =   2580
      Width           =   5535
      DataFieldList   =   "code"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16051436
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3016
      Columns(0).Caption=   "Our Ref"
      Columns(0).Name =   "code"
      Columns(0).DataField=   "code"
      Columns(0).FieldLen=   256
      Columns(1).Width=   6085
      Columns(1).Caption=   "Description"
      Columns(1).Name =   "description"
      Columns(1).DataField=   "description"
      Columns(1).FieldLen=   256
      _ExtentX        =   9763
      _ExtentY        =   3942
      _StockProps     =   77
      DataFieldToDisplay=   "code"
   End
   Begin MSComCtl2.DTPicker datInvoiceDate 
      Height          =   315
      Left            =   12180
      TabIndex        =   2
      ToolTipText     =   "Invoice date"
      Top             =   120
      Width           =   1275
      _ExtentX        =   2249
      _ExtentY        =   556
      _Version        =   393216
      CalendarBackColor=   8421631
      CalendarTitleBackColor=   8421631
      Format          =   59899905
      CurrentDate     =   38414
   End
   Begin VB.TextBox txtInvoiceNumber 
      Appearance      =   0  'Flat
      BackColor       =   &H008080FF&
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   9780
      TabIndex        =   1
      ToolTipText     =   "Invoice number"
      Top             =   120
      Width           =   1275
   End
   Begin VB.CommandButton cmdNoDiscrepencies 
      Caption         =   "No Discrepencies - Complete Invoice Details As Per Order"
      Height          =   315
      Left            =   9000
      TabIndex        =   3
      ToolTipText     =   "Complete invoice"
      Top             =   540
      Width           =   4455
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Height          =   315
      Left            =   12300
      TabIndex        =   5
      ToolTipText     =   "Close form"
      Top             =   5460
      Width           =   1155
   End
   Begin VB.CommandButton cmdComplete 
      Caption         =   "Complete"
      Height          =   315
      Left            =   11040
      TabIndex        =   4
      ToolTipText     =   "Complete form"
      Top             =   5460
      Width           =   1155
   End
   Begin VB.TextBox txtPurchaseHeaderID 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000011&
      Height          =   285
      Left            =   1260
      Locked          =   -1  'True
      TabIndex        =   8
      ToolTipText     =   "The unique identifier for this order"
      Top             =   540
      Width           =   735
   End
   Begin VB.TextBox txtAuthorisationNumber 
      Appearance      =   0  'Flat
      BackColor       =   &H0080C0FF&
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   1260
      Locked          =   -1  'True
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Authorisation number"
      Top             =   120
      Width           =   1275
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnCostCodes 
      Bindings        =   "frmPurcaseCompletion.frx":001E
      Height          =   2235
      Left            =   480
      TabIndex        =   6
      Top             =   1920
      Width           =   5535
      DataFieldList   =   "code"
      ListAutoValidate=   0   'False
      _Version        =   196617
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16051436
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3016
      Columns(0).Caption=   "Budget Code"
      Columns(0).Name =   "code"
      Columns(0).DataField=   "code"
      Columns(0).FieldLen=   256
      Columns(1).Width=   6085
      Columns(1).Caption=   "Description"
      Columns(1).Name =   "description"
      Columns(1).DataField=   "description"
      Columns(1).FieldLen=   256
      _ExtentX        =   9763
      _ExtentY        =   3942
      _StockProps     =   77
      DataFieldToDisplay=   "code"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdOrderDetail 
      Bindings        =   "frmPurcaseCompletion.frx":0039
      Height          =   4335
      Left            =   60
      TabIndex        =   7
      Top             =   960
      Width           =   13395
      _Version        =   196617
      AllowAddNew     =   -1  'True
      BackColorOdd    =   12640511
      RowHeight       =   423
      ExtraHeight     =   185
      Columns.Count   =   16
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "purchaseheaderID"
      Columns(0).Name =   "purchaseheaderID"
      Columns(0).DataField=   "purchaseheaderID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "resourcescheduleID"
      Columns(1).Name =   "resourcescheduleID"
      Columns(1).DataField=   "resourcescheduleID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   1852
      Columns(2).Caption=   "Budget Code"
      Columns(2).Name =   "cetacode"
      Columns(2).DataField=   "cetacode"
      Columns(2).FieldLen=   256
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   13233660
      Columns(3).Width=   1799
      Columns(3).Caption=   "Our Ref"
      Columns(3).Name =   "ourreference"
      Columns(3).DataField=   "ourreference"
      Columns(3).FieldLen=   256
      Columns(4).Width=   1085
      Columns(4).Caption=   "Qty"
      Columns(4).Name =   "quantity"
      Columns(4).DataField=   "quantity"
      Columns(4).DataType=   3
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   1879
      Columns(5).Caption=   "Part Ref"
      Columns(5).Name =   "suppliercode"
      Columns(5).DataField=   "suppliercode"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   3836
      Columns(6).Caption=   "Description"
      Columns(6).Name =   "description"
      Columns(6).DataField=   "description"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   1270
      Columns(7).Caption=   "Unit �"
      Columns(7).Name =   "unitcharge"
      Columns(7).Alignment=   1
      Columns(7).DataField=   "unitcharge"
      Columns(7).DataType=   6
      Columns(7).NumberFormat=   "�#.00"
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   1058
      Columns(8).Caption=   "Disc %"
      Columns(8).Name =   "discount"
      Columns(8).DataField=   "discount"
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(9).Width=   2011
      Columns(9).Caption=   "Total �"
      Columns(9).Name =   "total"
      Columns(9).Alignment=   1
      Columns(9).DataField=   "total"
      Columns(9).DataType=   6
      Columns(9).NumberFormat=   "�#.00"
      Columns(9).FieldLen=   256
      Columns(9).Locked=   -1  'True
      Columns(10).Width=   1429
      Columns(10).Caption=   "Received"
      Columns(10).Name=   "quantityreceived"
      Columns(10).DataField=   "quantityreceived"
      Columns(10).FieldLen=   256
      Columns(10).HasBackColor=   -1  'True
      Columns(10).BackColor=   11926999
      Columns(11).Width=   1535
      Columns(11).Caption=   "Invoice #"
      Columns(11).Name=   "supplierinvoicenumber"
      Columns(11).DataField=   "supplierinvoicenumber"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(11).HasBackColor=   -1  'True
      Columns(11).BackColor=   8421631
      Columns(12).Width=   1905
      Columns(12).Caption=   "Invoice �"
      Columns(12).Name=   "supplierinvoiceamount"
      Columns(12).Alignment=   1
      Columns(12).DataField=   "supplierinvoiceamount"
      Columns(12).DataType=   6
      Columns(12).NumberFormat=   "�#.00"
      Columns(12).FieldLen=   256
      Columns(12).HasBackColor=   -1  'True
      Columns(12).BackColor=   8421631
      Columns(13).Width=   1852
      Columns(13).Caption=   "Invoice Date"
      Columns(13).Name=   "supplierinvoicedate"
      Columns(13).DataField=   "supplierinvoicedate"
      Columns(13).DataType=   7
      Columns(13).FieldLen=   256
      Columns(13).Style=   4
      Columns(13).HasBackColor=   -1  'True
      Columns(13).BackColor=   8421631
      Columns(14).Width=   2805
      Columns(14).Caption=   "Discrepency Reason"
      Columns(14).Name=   "discrepencereason"
      Columns(14).DataField=   "discrepencyreason"
      Columns(14).FieldLen=   256
      Columns(14).HasBackColor=   -1  'True
      Columns(14).BackColor=   8421631
      Columns(15).Width=   3200
      Columns(15).Caption=   "Purchase Detail ID"
      Columns(15).Name=   "purchasedetailID"
      Columns(15).DataField=   "purchasedetailID"
      Columns(15).DataType=   3
      Columns(15).FieldLen=   256
      _ExtentX        =   23627
      _ExtentY        =   7646
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoPurchaseOrderDetail 
      Height          =   330
      Left            =   480
      Top             =   5640
      Visible         =   0   'False
      Width           =   3075
      _ExtentX        =   5424
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoPurchaseOrderDetail"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoCostCodes 
      Height          =   330
      Left            =   540
      Top             =   5520
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCostCodes"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label Label2 
      Caption         =   $"frmPurcaseCompletion.frx":005E
      ForeColor       =   &H00FF0000&
      Height          =   435
      Left            =   60
      TabIndex        =   14
      Top             =   5340
      Width           =   10935
   End
   Begin VB.Label Label1 
      Caption         =   $"frmPurcaseCompletion.frx":018D
      ForeColor       =   &H00FF0000&
      Height          =   795
      Left            =   2640
      TabIndex        =   13
      Top             =   60
      Width           =   6255
   End
   Begin VB.Label lblCaption 
      Caption         =   "Invoice Date"
      Height          =   285
      Index           =   2
      Left            =   11160
      TabIndex        =   12
      Top             =   120
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Invoice #"
      Height          =   285
      Index           =   0
      Left            =   9000
      TabIndex        =   11
      Top             =   120
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Caption         =   "Purchase ID"
      ForeColor       =   &H80000011&
      Height          =   285
      Index           =   1
      Left            =   120
      TabIndex        =   10
      Top             =   540
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Authorisation #"
      Height          =   285
      Index           =   24
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   1275
   End
End
Attribute VB_Name = "frmPurchaseCompletion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdCancel_Click()
Me.Tag = "CANCELLED"
Me.Hide
End Sub

Private Sub cmdComplete_Click()

Dim l_strSQL As String
l_strSQL = "SELECT Count(purchasedetailID) FROM purchasedetail WHERE (cetacode IS NULL or cetacode = '') AND purchaseheaderID = '" & txtPurchaseHeaderID.Text & "';"

If GetCount(l_strSQL) > 0 Then
    MsgBox "You can not complete this purchase order while there are missing budget codes", vbExclamation
    Exit Sub
End If

l_strSQL = "SELECT Count(purchasedetailID) FROM purchasedetail WHERE (quantityreceived IS NULL or quantityreceived = '') AND purchaseheaderID = '" & txtPurchaseHeaderID.Text & "';"

If GetCount(l_strSQL) > 0 Then
    Dim l_intMsg As Integer
    l_intMsg = MsgBox("There are still 0 value 'quantity received' fields. Are you sure you want to complete this order?", vbQuestion + vbYesNo)
    If l_intMsg = vbNo Then Exit Sub
End If

l_strSQL = "SELECT Count(purchasedetailID) FROM purchasedetail WHERE (supplierinvoicenumber IS NULL or supplierinvoicenumber = '') AND purchaseheaderID = '" & txtPurchaseHeaderID.Text & "';"

If GetCount(l_strSQL) > 0 Then
    MsgBox "You can not complete this purchase order while there are still incompleted 'invoice number' fields", vbExclamation
    Exit Sub
End If

l_strSQL = "SELECT Count(purchasedetailID) FROM purchasedetail WHERE ((quantity <> quantityreceived) AND (discrepencyreason IS NULL or discrepencyreason = '')) AND purchaseheaderID = '" & txtPurchaseHeaderID.Text & "';"

If GetCount(l_strSQL) > 0 Then
    MsgBox "You can not complete this purchase order while there are quantity discrepencies without reasons", vbExclamation
    Exit Sub
End If

Me.Hide

End Sub

Private Sub cmdNoDiscrepencies_Click()

If txtInvoiceNumber.Text = "" Then
    MsgBox "You must enter an invoice number before completing a purchase order", vbExclamation
    Exit Sub
End If

Dim l_intMsg As Integer
l_intMsg = MsgBox("Are you sure there are no discrepencies, and that you want to complete this purchase order?", vbYesNo + vbQuestion)
If l_intMsg = vbNo Then Exit Sub

l_intMsg = NoPurchaseOrderDiscrepencies(Val(txtPurchaseHeaderID.Text), txtInvoiceNumber.Text, datInvoiceDate.Value)

If l_intMsg <> 0 Then adoPurchaseOrderDetail.Refresh

End Sub

Private Sub ddnOurReference_DropDown()

adoOurReference.RecordSource = "SELECT description AS code, information AS description FROM xref WHERE category = 'ourreference' AND format = '" & grdOrderDetail.Columns("cetacode").Text & "' ORDER BY code, description;"
adoOurReference.ConnectionString = g_strConnection
adoOurReference.Refresh

End Sub

Private Sub Form_Load()


'pick up all the budget codes from the budget table
l_strSQL = "SELECT code, description FROM budget ORDER BY code;"

adoCostCodes.ConnectionString = g_strConnection
adoCostCodes.RecordSource = l_strSQL
adoCostCodes.Refresh


'set the invoice date to be today
datInvoiceDate.Value = Date

'stick the form in the middle of the screen
CenterForm Me

End Sub

Private Sub grdOrderDetail_BtnClick()

'change this if we add more buttons

Dim l_datInvoiceDate As Variant
Dim l_strSQL As String
Dim l_lngPurchaseDetailID As Long

Dim l_sngQuantityReceived As Single
Dim l_strInvoiceNumber As String
Dim l_dblInvoiceAmount As Double
Dim l_strDiscrepency As String
Dim l_strBudgetCode As String
Dim l_strOurReference As String

l_strBudgetCode = CStr(grdOrderDetail.Columns("cetacode").Text)
l_strOurReference = CStr(grdOrderDetail.Columns("ourreference").Text)

l_sngQuantityReceived = Val(grdOrderDetail.Columns("quantityreceived").Text)
l_strInvoiceNumber = CStr(grdOrderDetail.Columns("supplierinvoicenumber").Text)
l_dblInvoiceAmount = Val(grdOrderDetail.Columns("supplierinvoiceamount").Text)
l_strDiscrepency = CStr(grdOrderDetail.Columns("discrepencereason").Text)

'get the actual record ID which we need to edit
l_lngPurchaseDetailID = Val(grdOrderDetail.Columns("PurchaseDetailID").Text)

'is it valid?
If l_lngPurchaseDetailID = 0 Then Exit Sub


'make sure the other values in the grid are saved
l_strSQL = "UPDATE purchasedetail SET cetacode = '" & QuoteSanitise(l_strBudgetCode) & "', ourreference = '" & QuoteSanitise(l_strOurReference) & "', supplierinvoiceamount = '" & l_dblInvoiceAmount & "', supplierinvoicenumber = '" & QuoteSanitise(l_strInvoiceNumber) & "', quantityreceived = '" & l_sngQuantityReceived & "', discrepencyreason = '" & QuoteSanitise(l_strDiscrepency) & "' WHERE purchasedetailID = '" & l_lngPurchaseDetailID & "';"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError


'as the user for the date
If Not IsDate(grdOrderDetail.Columns("supplierinvoicedate").Text) Then
    l_datInvoiceDate = GetDate(Date)
Else
    'save this invoiced date to the p/o details
    l_strSQL = "UPDATE purchasedetail SET supplierinvoicedate = NULL WHERE purchasedetailID = '" & l_lngPurchaseDetailID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'refresh the grid
    adoPurchaseOrderDetail.Refresh
    
    Exit Sub
End If

If IsDate(l_datInvoiceDate) Then
    'save this invoiced date to the p/o details
    l_strSQL = "UPDATE purchasedetail SET supplierinvoicedate = '" & FormatSQLDate(l_datInvoiceDate) & "' WHERE purchasedetailID = '" & l_lngPurchaseDetailID & "';"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'refresh the grid
    adoPurchaseOrderDetail.Refresh

End If




End Sub

Private Sub grdOrderDetail_InitColumnProps()
grdOrderDetail.Columns("cetacode").DropDownHwnd = ddnCostCodes.hWnd
grdOrderDetail.Columns("ourreference").DropDownHwnd = ddnOurReference.hWnd

End Sub

Private Sub grdOrderDetail_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
On Error Resume Next
If grdOrderDetail.Columns(grdOrderDetail.Col).Name = "ourreference" Then
    adoOurReference.RecordSource = "SELECT description AS code, information AS description FROM xref WHERE category = 'ourreference' AND format = '" & grdOrderDetail.Columns("cetacode").Text & "' ORDER BY code, description;"
    adoOurReference.ConnectionString = g_strConnection
    adoOurReference.Refresh
End If
End Sub
