VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMD5Grid 
   Caption         =   "Make MD5 of Grid Items"
   ClientHeight    =   1980
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6270
   Icon            =   "frmMD5Grid.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   1980
   ScaleWidth      =   6270
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdMD5Checksum 
      Caption         =   "Make MD5"
      Height          =   330
      Left            =   3480
      TabIndex        =   12
      Top             =   240
      Width           =   1215
   End
   Begin VB.CommandButton cmdAbortMD5 
      Caption         =   "Abort"
      Height          =   330
      Left            =   4800
      TabIndex        =   11
      Top             =   240
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox txtClipfilename 
      Height          =   315
      Left            =   1440
      TabIndex        =   5
      ToolTipText     =   "The clip filename"
      Top             =   780
      Width           =   3915
   End
   Begin VB.TextBox txtAltFolder 
      Height          =   315
      Left            =   1440
      TabIndex        =   4
      ToolTipText     =   "Alternative file location if not in client number folder"
      Top             =   1140
      Width           =   4575
   End
   Begin VB.TextBox txtClipID 
      BackColor       =   &H00C0FFFF&
      Height          =   315
      Left            =   1440
      TabIndex        =   0
      ToolTipText     =   "Scan or Type the Tape Barcode"
      Top             =   60
      Width           =   1635
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   315
      Left            =   1440
      TabIndex        =   8
      Top             =   1500
      Visible         =   0   'False
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   556
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.TextBox txtMD5Checksum 
      Height          =   315
      Left            =   1440
      TabIndex        =   9
      ToolTipText     =   "Alternative file location if not in client number folder"
      Top             =   1500
      Width           =   4575
   End
   Begin VB.PictureBox RSPChecksum1 
      Height          =   480
      Left            =   6840
      ScaleHeight     =   420
      ScaleWidth      =   1140
      TabIndex        =   13
      Top             =   780
      Width           =   1200
   End
   Begin VB.Label lblCaption 
      Caption         =   "MD5 Checksum"
      Height          =   255
      Index           =   1
      Left            =   60
      TabIndex        =   10
      Top             =   1560
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Filename"
      Height          =   255
      Index           =   13
      Left            =   60
      TabIndex        =   7
      Top             =   840
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Folder"
      Height          =   255
      Index           =   23
      Left            =   60
      TabIndex        =   6
      Top             =   1200
      Width           =   915
   End
   Begin VB.Label lblLibraryID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   1440
      TabIndex        =   3
      Tag             =   "CLEARFIELDS"
      Top             =   420
      Width           =   1635
   End
   Begin VB.Label lblCaption 
      Caption         =   "Library ID"
      Height          =   255
      Index           =   14
      Left            =   60
      TabIndex        =   2
      Top             =   480
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Clip ID"
      Height          =   255
      Index           =   0
      Left            =   60
      TabIndex        =   1
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "frmMD5Grid"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdAbortMD5_Click()

RSPChecksum1.CancelExecution
cmdAbortMD5.Visible = False
Unload Me

End Sub

Private Sub cmdMD5Checksum_Click()

Dim l_strNetworkPath As String, l_strTotalPath As String, l_lngFilenumber As Long, l_curFileSize As Currency, l_curFilePos As Currency, l_strTempMD5 As String

Select Case cmdMD5Checksum.Caption

    Case "Make MD5"
    
        cmdMD5Checksum.Caption = "Pause"
        cmdAbortMD5.Visible = True
        
        'Check that the alt location hasn't got a drive letter or a UNC path at the start of it.
        If txtAltFolder.Text Like "\\*" Or Mid(txtAltFolder.Text, 2, 1) = ":" Then
            MsgBox "Alt Location has either drive letter or UNC network reference. Please fix this", vbOKOnly, "Problem..."
            cmdMD5Checksum.Caption = "Make MD5"
            Exit Sub
        End If
        
        If GetData("library", "format", "libraryID", lblLibraryID.Caption) = "DISCSTORE" Then
        
            l_strNetworkPath = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption)
            l_strNetworkPath = l_strNetworkPath & "\" & txtAltFolder.Text
            l_strTotalPath = l_strNetworkPath & "\" & txtClipfilename.Text
            
            ProgressBar1.Visible = True
            'lblMD5ChecksumFile.Caption = l_strNetworkPath
            RSPChecksum1.NewCheckSumFile "md5", l_strTotalPath
            
        Else
        
            MsgBox "Checksums can only be made if the file is in one of the RRmedia DISCSTORES", vbOKOnly, "Cannot Make Checksum"
            cmdMD5Checksum.Caption = "Make MD5"
            cmdAbortMD5.Visible = False
            Unload Me
            
        End If

    Case "Pause"
    
        cmdMD5Checksum.Caption = "Resume"
        RSPChecksum1.PauseExecution
        
    Case "Resume"
    
        cmdMD5Checksum.Caption = "Pause"
        RSPChecksum1.ResumeExecution

End Select

End Sub

Private Sub Form_Activate()

cmdMD5Checksum.Value = True

End Sub

Private Sub RSPChecksum1_Finished(CheckSumString As String)

cmdMD5Checksum.Caption = "Make MD5"

cmdAbortMD5.Visible = False
If CheckSumString <> "" Then
    txtMD5Checksum.Text = CheckSumString
End If

ProgressBar1.Visible = False

SetData "events", "md5checksum", "eventID", txtClipID.Text, txtMD5Checksum.Text

Unload Me

End Sub

Private Sub RSPChecksum1_Progress(Value As Long)

ProgressBar1.Value = Value

End Sub
